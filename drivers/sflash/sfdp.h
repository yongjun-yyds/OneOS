/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        sfdp.h
 *
 * @brief       This file provides struct definition and sfdp functions declaration.
 *
 * @revision
 * Date         Author          Notes
 * 2021-11-11   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __SFDP_H__
#define __SFDP_H__

#include <spi.h>

struct os_sfdp_head
{
    uint8_t str[4]; /* SFDP */
    uint8_t minor;
    uint8_t major;
    uint8_t nph;
    uint8_t proto;
};

struct os_sfdp_param_head
{
    uint8_t id_lsb;
    uint8_t minor;
    uint8_t major;
    uint8_t length; /* in double words */

    uint8_t ptp[3];
    uint8_t id_msb;
};

struct os_sfdp_param
{
    struct os_sfdp_param_head head;

    uint32_t *table;
};

struct os_sfdp
{
    struct os_sfdp_head   head;
    struct os_sfdp_param *params;
    struct os_sflash_info info;
};

int  os_sfdp_decode(const uint8_t *sfdp_buff, struct os_sfdp *sfdp);
void os_sfdp_init_sflash(struct os_sflash *sflash, struct os_sfdp *sfdp);

#endif
