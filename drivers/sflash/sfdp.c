/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        sfdp.c
 *
 * @brief       This file provides functions for sfdp.
 *
 * @revision
 * Date         Author          Notes
 * 2021-11-11   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <device.h>
#include <os_memory.h>
#include <os_errno.h>
#include <arch_interrupt.h>
#include <os_task.h>
#include <os_util.h>
#include <os_assert.h>
#include <string.h>
#include <driver.h>
#include <pin.h>

#include "sflash.h"
#include "sfdp.h"
#include "sfdp_basic.h"

int os_sfdp_decode(const uint8_t *sfdp_buff, struct os_sfdp *sfdp)
{
    int i;

    /* 1. sfdp head */

    sfdp->head = *(struct os_sfdp_head *)sfdp_buff;

    if (strncmp((const char *)sfdp->head.str, "SFDP", 4))
        return -1;

    OS_SFLASH_LOG("\r\n");
    OS_SFLASH_LOG("SFDP head:\r\n");

    OS_SFLASH_LOG("Signature: %c%c%c%c\r\n",
                  sfdp->head.str[0],
                  sfdp->head.str[1],
                  sfdp->head.str[2],
                  sfdp->head.str[3]);

    OS_SFLASH_LOG("Revision: %d.%d\r\n", sfdp->head.major, sfdp->head.minor);
    OS_SFLASH_LOG("NPH: %d\r\n", sfdp->head.nph);
    OS_SFLASH_LOG("Protocol: %02x\r\n", sfdp->head.proto);

    /* 2. param head */

    sfdp->params = os_calloc(sfdp->head.nph + 1, sizeof(struct os_sfdp_param));

    OS_ASSERT(sfdp->params != OS_NULL);

    for (i = 0; i < sfdp->head.nph + 1; i++)
    {
        struct os_sfdp_param *param = &sfdp->params[i];

        param->head = *(struct os_sfdp_param_head *)(sfdp_buff + 8 + 8 * i);

        uint32_t pid = (param->head.id_msb << 8) | param->head.id_lsb;
        uint32_t ptp = (param->head.ptp[2] << 16) | (param->head.ptp[1] << 8) | param->head.ptp[0];

        OS_SFLASH_LOG("\r\n");
        OS_SFLASH_LOG("Param head(%d):\r\n", i);
        OS_SFLASH_LOG("Revision: %d.%d\r\n", param->head.major, param->head.minor);
        OS_SFLASH_LOG("ID: %04x\r\n", pid);
        OS_SFLASH_LOG("Length: %d DWORDS\r\n", param->head.length);
        OS_SFLASH_LOG("PTP: 0x%x\r\n", ptp);

        param->table = os_aligned_malloc(4, param->head.length * 4);

        OS_ASSERT(param->table != OS_NULL);

        memcpy(param->table, sfdp_buff + ptp, param->head.length * 4);

#ifdef OS_SFLASH_DEBUG
        hex_dump((unsigned char *)param->table, param->head.length * 4);
#endif
        /* 3. param table */
        if (pid == 0xff00)
        {
            os_sfdp_decode_basic(sfdp, param);
        }
    }

    return 0;
}

void os_sfdp_init_sflash(struct os_sflash *sflash, struct os_sfdp *sfdp)
{
    int i;

    for (i = 0; i < sfdp->head.nph + 1; i++)
    {
        struct os_sfdp_param *param = &sfdp->params[i];

        uint32_t pid = (param->head.id_msb << 8) | param->head.id_lsb;

        if (pid == 0xff00)
        {
            os_sfdp_basic_init(sflash, sfdp, param);
        }
    }
}
