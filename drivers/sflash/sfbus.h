/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        sfbus.h
 *
 * @brief       This file provides struct definition and sfbus functions declaration.
 *
 * @revision
 * Date         Author          Notes
 * 2021-11-11   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __SFBUS_H__
#define __SFBUS_H__

#include <spi.h>

#define OS_XSPI_DATA_DIR_HOST_TO_DEVICE (0)
#define OS_XSPI_DATA_DIR_DEVICE_TO_HOST (1)

enum os_xspi_message_cfg_type
{
    OS_XSPI_MESSAGE_CFG_TYPE_NONE,
    OS_XSPI_MESSAGE_CFG_TYPE_READ,
    OS_XSPI_MESSAGE_CFG_TYPE_WRITE,
    OS_XSPI_MESSAGE_CFG_TYPE_ERASE,
    OS_XSPI_MESSAGE_CFG_TYPE_ID,
    OS_XSPI_MESSAGE_CFG_TYPE_UNLOCK,
    OS_XSPI_MESSAGE_CFG_TYPE_STATUS,
};

struct os_xspi_message_cfg
{
    uint8_t type;

    uint8_t instruction;
    uint8_t instruction_lines;

    uint8_t address_size;
    uint8_t address_lines;

    uint8_t alternate;
    uint8_t alternate_size;
    uint8_t alternate_lines;

    uint8_t dummy_cycles;

    uint8_t data_lines;

    /* write: page size, erase: block size */
    os_ubase_t priv;
};

struct os_xspi_message
{
    struct os_xspi_message_cfg xcfg;

    uint32_t address;

    uint32_t data_size;

    int data_dir;

    uint8_t *data;
};

#define xmessage_instruction(xmessage)       xmessage->xcfg.instruction
#define xmessage_instruction_lines(xmessage) xmessage->xcfg.instruction_lines
#define xmessage_address_size(xmessage)      xmessage->xcfg.address_size
#define xmessage_address_lines(xmessage)     xmessage->xcfg.address_lines
#define xmessage_alternate(xmessage)         xmessage->xcfg.alternate
#define xmessage_alternate_size(xmessage)    xmessage->xcfg.alternate_size
#define xmessage_alternate_lines(xmessage)   xmessage->xcfg.alternate_lines
#define xmessage_dummy_cycles(xmessage)      xmessage->xcfg.dummy_cycles
#define xmessage_data_lines(xmessage)        xmessage->xcfg.data_lines

#define xmessage_address(xmessage)   xmessage->address
#define xmessage_data_size(xmessage) xmessage->data_size
#define xmessage_data_dir(xmessage)  xmessage->data_dir
#define xmessage_data(xmessage)      xmessage->data

struct os_sfbus;

struct os_sfbus_ops
{
    int (*configure)(struct os_sfbus *sfbus, struct os_spi_configuration *configuration);
    int (*transfer)(struct os_sfbus *sfbus, struct os_xspi_message *xmessage);
};

struct os_sfbus
{
    struct os_device parent;

    uint8_t                 mode;
    const struct os_sfbus_ops *ops;

    os_mutex_id       lock;
    struct os_sflash *owner;

    os_ubase_t support_1_line : 1;
    os_ubase_t support_2_line : 1;
    os_ubase_t support_4_line : 1;
    os_ubase_t support_8_line : 1;

    void *priv;
};

struct os_sfbus *os_sfbus_attach(const char *bus_name, int cs);
os_err_t         os_sfbus_xspi_register(struct os_sfbus *sfbus, const char *name, const struct os_sfbus_ops *ops);
int              os_sfbus_transfer(struct os_sflash *sflash, struct os_xspi_message *xmessage);

#endif
