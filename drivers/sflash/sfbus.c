/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        sfbus.c
 *
 * @brief       This file provides functions for sfbus.
 *
 * @revision
 * Date         Author          Notes
 * 2021-11-11   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <device.h>
#include <os_memory.h>
#include <os_errno.h>
#include <arch_interrupt.h>
#include <os_task.h>
#include <os_util.h>
#include <os_assert.h>
#include <string.h>
#include <stdio.h>
#include <driver.h>
#include <pin.h>

#include "sflash.h"

static os_err_t sfbus_spi_configure(struct os_sfbus *sfbus, struct os_spi_configuration *configuration)
{
    os_spi_configure(sfbus->priv, configuration);

    return OS_SUCCESS;
}

static int sfbus_spi_transfer(struct os_sfbus *sfbus, struct os_xspi_message *xmessage)
{
    struct os_spi_message *msg_list = os_calloc(5, sizeof(struct os_spi_message));
    struct os_spi_message *msg_head = msg_list;
    struct os_spi_message *msg_tail = msg_list;

    int i;

    uint8_t *dummy = OS_NULL;
    uint8_t  addrs[5];

    if (msg_list == OS_NULL)
    {
        os_kprintf("sfbus_spi_xfer msg list no memory.\r\n");
        return OS_NOMEM;
    }

    /* prepare 5 step xspi message */
    if (xmessage_instruction_lines(xmessage) != 0)
    {
        OS_ASSERT(xmessage_instruction_lines(xmessage) == 1);

        msg_head->length   = 1;
        msg_head->send_buf = &xmessage_instruction(xmessage);
        msg_head->recv_buf = OS_NULL;
        msg_head++;
    }

    if (xmessage_address_lines(xmessage) != 0)
    {
        OS_ASSERT(xmessage_address_lines(xmessage) == 1);
        OS_ASSERT(xmessage_address_size(xmessage) == 3 || xmessage_address_size(xmessage) == 4);

        msg_head->length = xmessage_address_size(xmessage);

        /* swap addr bytes(msb) */
        for (i = 0; i < xmessage_address_size(xmessage); i++)
            addrs[i] = xmessage_address(xmessage) >> ((xmessage_address_size(xmessage) - 1 - i) * 8);

        msg_head->send_buf = addrs;
        msg_head->recv_buf = OS_NULL;
        msg_head++;
    }

    if (xmessage_alternate_lines(xmessage) != 0)
    {
        OS_ASSERT(xmessage_alternate_lines(xmessage) == 1);

        msg_head->length   = xmessage_alternate_size(xmessage);
        msg_head->send_buf = &xmessage_alternate(xmessage);
        msg_head->recv_buf = OS_NULL;
        msg_head++;
    }

    if (xmessage_dummy_cycles(xmessage) != 0)
    {
        OS_ASSERT(xmessage_dummy_cycles(xmessage) % 8 == 0);

        dummy = os_calloc(1, xmessage_dummy_cycles(xmessage) / 8);

        if (dummy == OS_NULL)
        {
            os_free(msg_list);
            os_kprintf("sfbus_spi_xfer dummy no memory.\r\n");
            return OS_NOMEM;
        }

        msg_head->length   = xmessage_dummy_cycles(xmessage) / 8;
        msg_head->send_buf = dummy;
        msg_head->recv_buf = OS_NULL;
        msg_head++;
    }

    if (xmessage_data_lines(xmessage) != 0)
    {
        OS_ASSERT(xmessage_data_lines(xmessage) == 1);

        msg_head->length = xmessage_data_size(xmessage);

        if (xmessage_data_dir(xmessage) == OS_XSPI_DATA_DIR_HOST_TO_DEVICE)
        {
            msg_head->send_buf = xmessage_data(xmessage);
        }
        else if (xmessage_data_dir(xmessage) == OS_XSPI_DATA_DIR_DEVICE_TO_HOST)
        {
            msg_head->recv_buf = xmessage_data(xmessage);
        }
        else
        {
            OS_ASSERT(OS_FALSE);
        }

        msg_head++;
    }

    /* transfer */
    msg_tail->cs_take = OS_TRUE;

    while (msg_tail + 1 != msg_head)
    {
        msg_tail->next = msg_tail + 1;
        msg_tail++;
    }

    msg_tail->cs_release = OS_TRUE;

    os_spi_transfer_message(sfbus->priv, msg_list);

    if (dummy != OS_NULL)
    {
        os_free(dummy);
    }

    os_free(msg_list);

    return OS_SUCCESS;
}

const static struct os_sfbus_ops sfbus_spi_ops = {
    .configure = sfbus_spi_configure,
    .transfer  = sfbus_spi_transfer,
};

static os_err_t os_sfbus_spi_attach(struct os_sfbus *sfbus, const char *name, int cs)
{
    char spi_dev_name[32];
    char sfbus_name[32];

    snprintf(spi_dev_name, sizeof(spi_dev_name) - 1, "%s.sfbus", name);
    snprintf(sfbus_name, sizeof(spi_dev_name) - 1, "sfbus.%s", name);

    os_hw_spi_device_attach(name, spi_dev_name, cs);

    sfbus->priv = (struct os_spi_device *)os_device_open_s(spi_dev_name);

    if (sfbus->priv == OS_NULL)
    {
        os_kprintf("open spi bus failed: %s\r\n", name);
        return OS_FAILURE;
    }

    sfbus->ops   = &sfbus_spi_ops;
    sfbus->owner = OS_NULL;

    sfbus->lock = os_mutex_create(OS_NULL, OS_NULL, OS_FALSE);

    return os_device_register(&sfbus->parent, sfbus_name);
}

os_err_t os_sfbus_xspi_register(struct os_sfbus *sfbus, const char *name, const struct os_sfbus_ops *ops)
{
    char sfbus_name[32] = "sfbus.";

    strcat(sfbus_name, name);

    sfbus->ops   = ops;
    sfbus->owner = OS_NULL;

    sfbus->lock = os_mutex_create(OS_NULL, OS_NULL, OS_FALSE);

    os_device_register(&sfbus->parent, sfbus_name);

    return OS_SUCCESS;
}

struct os_sfbus *os_sfbus_attach(const char *bus_name, int cs)
{
    char sfbus_name[32] = "sfbus.";

    strcat(sfbus_name, bus_name);

    struct os_device *bus;
    struct os_sfbus  *sfbus = OS_NULL;

    if (cs >= 0)
    {
        os_pin_mode(cs, PIN_MODE_OUTPUT);
        os_pin_write(cs, PIN_HIGH);
    }

    /* bus allready attach */
    bus = os_device_open_s(sfbus_name);

    if (bus != OS_NULL)
    {
        return (struct os_sfbus *)bus;
    }

    /* spi bus attach */
    bus = os_device_find(bus_name);

    if (bus != OS_NULL)
    {
        OS_ASSERT(bus->type == OS_DEVICE_TYPE_SPIBUS);

        sfbus = os_calloc(1, sizeof(struct os_sfbus));

        OS_ASSERT(sfbus != OS_NULL);

        sfbus->support_1_line = OS_TRUE;

        os_sfbus_spi_attach(sfbus, bus_name, cs);

        os_device_open_s(sfbus_name);
    }

    return sfbus;
}

os_err_t os_sflash_configure(struct os_sflash *sflash, struct os_spi_configuration *cfg)
{
    OS_ASSERT(sflash != OS_NULL);

    sflash->config.data_width = cfg->data_width;
    sflash->config.mode       = cfg->mode & OS_SPI_MODE_MASK;
    sflash->config.max_hz     = cfg->max_hz;

    return OS_SUCCESS;
}

int os_sfbus_transfer(struct os_sflash *sflash, struct os_xspi_message *xmessage)
{
    struct os_sfbus *sfbus = sflash->sfbus;

    os_mutex_lock(sfbus->lock, OS_WAIT_FOREVER);

    if (sfbus->owner != sflash)
    {
        sfbus->owner = sflash;
        sfbus->ops->configure(sfbus, &sflash->config);
    }

    if (sflash->cs >= 0)
    {
        os_pin_write(sflash->cs, PIN_LOW);
    }

    int ret = sfbus->ops->transfer(sfbus, xmessage);

    if (sflash->cs >= 0)
    {
        os_pin_write(sflash->cs, PIN_HIGH);
    }

    os_mutex_unlock(sfbus->lock);

    return ret;
}
