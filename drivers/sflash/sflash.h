/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        sflash.h
 *
 * @brief       This file provides struct definition and sflash functions declaration.
 *
 * @revision
 * Date         Author          Notes
 * 2021-11-11   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __SFLASH_H__
#define __SFLASH_H__

#include "sfbus.h"

#ifdef OS_SFLASH_DEBUG
#define OS_SFLASH_LOG(...) os_kprintf(__VA_ARGS__)
#else
#define OS_SFLASH_LOG(...)
#endif

struct os_sflash_commands
{
    struct os_xspi_message_cfg status;
    struct os_xspi_message_cfg unlock;
    struct os_xspi_message_cfg read;
    struct os_xspi_message_cfg write;
    struct os_xspi_message_cfg erase;
};

struct os_sflash_info
{
    uint8_t  mf;
    uint8_t  id;
    const char *name;

    const struct os_xspi_message_cfg *supported_cmds;
    int                               supported_cmds_nr;

    uint32_t capacity;
    uint32_t page_size;

    uint8_t addr_bytes;
};

struct os_sflash
{
    struct os_device parent;

    struct os_sfbus *sfbus;

    struct os_spi_configuration config;

    int cs;

    const struct os_sflash_info *info;

    struct os_sflash_commands cmds;
};

#define OS_SFLASH_INFO static OS_USED OS_SECTION("os_sflash_info") const struct os_sflash_info

struct os_sflash *os_sflash_init(const char *bus_name, int cs);
int               os_sflash_configure(struct os_sflash *sflash, struct os_spi_configuration *cfg);
int               os_sflash_read_page(struct os_sflash *sflash, uint32_t offset, uint8_t *buf, os_size_t size);
int  os_sflash_write_page(struct os_sflash *sflash, uint32_t offset, const uint8_t *buf, os_size_t size);
int  os_sflash_erase_block(struct os_sflash *sflash, uint32_t offset);
void sflash_write_unlock(struct os_sflash *sflash);

#endif
