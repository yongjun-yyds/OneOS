/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        eeprom.h
 *
 * @brief       this file implements i2c bus eeprom driver
 *
 * @details
 *
 * @revision
 * Date          Author          Notes
 * 2020-02-20    OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __EEPROM_H__
#define __EEPROM_H__

#include <os_types.h>
#include <os_stddef.h>

/*address inupt pin lelve:A2A1A0*/
#define ADDR_INPUT_A2A1A0_000 0x00
#define ADDR_INPUT_A2A1A0_001 0x01
#define ADDR_INPUT_A2A1A0_010 0x02
#define ADDR_INPUT_A2A1A0_011 0x03
#define ADDR_INPUT_A2A1A0_100 0x04
#define ADDR_INPUT_A2A1A0_101 0x05
#define ADDR_INPUT_A2A1A0_110 0x06
#define ADDR_INPUT_A2A1A0_111 0x07

struct os_eeprom_info
{
    char      *flash_name;
    char      *bus_name;
    os_size_t  capacity;
    os_size_t  page_size;
    uint8_t addr_input;
    os_size_t  write_cycle;
};
typedef struct os_eeprom_info os_eeprom_info_t;

#define OS_EEPROM_INFO static OS_USED OS_SECTION("os_eeprom_info") const os_eeprom_info_t

#endif /*__EEPROM_H__*/
