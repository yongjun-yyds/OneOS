/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        eeprom.c
 *
 * @brief       this file implements eeprom operations
 * @details
 *
 * @revision
 * Date          Author          Notes
 * 2020-02-20    OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include <board.h>
#include <drv_cfg.h>
#include <arch_interrupt.h>
#include <os_memory.h>
#include <drv_log.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <fal/fal.h>
#include <eeprom.h>

#define bswap_16(a)     ((((uint16_t)(a) << 8) & 0xff00) | (((uint16_t)(a) >> 8) & 0xff))
#define set_bit(x, n)   (x | (1 << n))
#define clear_bit(x, n) (x & (~(1 << n)))
#define get_bit(x, n)   ((x >> n) & 0x01)

#define EEPROM_ADDR_HEAD (0x50)
#define DBG_TAG          "eeprom"

struct eeprom
{
    struct os_i2c_client   client;
    fal_flash_t            flash;
    struct os_eeprom_info *info;
};

struct data_addr
{
    uint8_t  width;
    uint16_t addr;
};

static int eeprom_bus_init(struct eeprom *epm)
{
    epm->client.bus = (struct os_i2c_bus_device *)os_device_open_s(epm->info->bus_name);
    if (epm->client.bus == OS_NULL)
    {
        LOG_E(DBG_TAG, "invalid i2c bus.");
        return OS_INVAL;
    }

    epm->client.client_addr = EEPROM_ADDR_HEAD | epm->info->addr_input;

    return OS_SUCCESS;
}

static void eeprom_set_extend_addr_bit(struct eeprom *epm, uint32_t addr, uint8_t bit)
{
    uint16_t client_addr = epm->client.client_addr;

    if (get_bit(addr, bit) == 0x01)
    {
        epm->client.client_addr = set_bit(client_addr, bit);
    }
    else
    {
        epm->client.client_addr = clear_bit(client_addr, bit);
    }
}

static void eeprom_calculate_address(struct eeprom *epm, uint32_t addr, struct data_addr *dat_addr)
{
    uint8_t  i;
    int32_t  cap_shift;
    uint8_t  quot;
    uint8_t  remain;
    uint32_t ext_addr;

    cap_shift = os_ffs(epm->info->capacity) - 1;

    quot   = cap_shift >> 3;
    remain = cap_shift & 0x07;

    if (quot <= 1)
    {
        dat_addr->width = 1;
        dat_addr->addr  = (uint16_t)(addr & 0xff);
    }
    else if (quot == 2)
    {
        dat_addr->width = 2;
        dat_addr->addr  = (uint16_t)bswap_16(addr & 0xffff);
    }
    else
    {
        os_kprintf("unsupported capacity.\r\n");
    }

    if (remain > 0 && remain < 4)
    {
        ext_addr = addr >> (8 * quot);

        for (i = 0; i < remain; i++)
        {
            eeprom_set_extend_addr_bit(epm, ext_addr, i);
        }
    }
}

static int eeprom_read_bytes(struct eeprom *epm, uint8_t *buff, uint32_t addr, uint32_t size)
{
    struct data_addr dat_addr;

    eeprom_calculate_address(epm, addr, &dat_addr);

    return os_i2c_client_read((struct os_i2c_client *)&(epm->client),
                              (uint32_t)(dat_addr.addr),
                              dat_addr.width,
                              buff,
                              size);
}

static int eeprom_write_page(struct eeprom *epm, uint8_t *buff, uint32_t addr, uint32_t size)
{
    int              ret;
    struct data_addr dat_addr;

    if (size > epm->info->page_size)
    {
        return OS_FAILURE;
    }

    eeprom_calculate_address(epm, addr, &dat_addr);

    ret = os_i2c_client_write((struct os_i2c_client *)&(epm->client),
                              (uint32_t)(dat_addr.addr),
                              dat_addr.width,
                              (uint8_t *)buff,
                              size);
    if (ret != OS_SUCCESS)
    {
        return OS_EIO;
    }

    /*it takes tWR between STOP and START*/
    os_task_msleep(epm->info->write_cycle);

    return OS_SUCCESS;
}

static int eeprom_erase_block(struct eeprom *epm, uint32_t addr)
{
    int         ret = OS_SUCCESS;
    uint8_t *buff;

    if ((addr & (epm->info->page_size - 1)) != 0)
    {
        return OS_FAILURE;
    }

    buff = os_calloc(1, epm->info->page_size);

    if (buff == OS_NULL)
    {
        LOG_E(DBG_TAG, "no memery");
        return OS_NOMEM;
    }

    memset(buff, 0xff, epm->info->page_size);

    ret = eeprom_write_page(epm, buff, addr, epm->info->page_size);

    os_free(buff);

    if (ret != OS_SUCCESS)
    {
        return OS_EIO;
    }

    return OS_SUCCESS;
}

static int fal_eeprom_read_page(fal_flash_t *flash, uint32_t page_addr, uint8_t *buff, uint32_t page_nr)
{
    uint32_t offset = page_addr * flash->page_size;
    uint32_t size   = page_nr * flash->page_size;

    struct eeprom *epm = os_container_of(flash, struct eeprom, flash);

    return eeprom_read_bytes(epm, buff, offset, size);
}

static int fal_eeprom_write_page(fal_flash_t *flash, uint32_t page_addr, const uint8_t *buff, uint32_t page_nr)
{
    uint32_t i, count = 0;
    uint32_t offset = page_addr * flash->page_size;
    uint32_t size   = flash->page_size;

    struct eeprom *epm = os_container_of(flash, struct eeprom, flash);

    for (i = 0; i < page_nr; i++)
    {
        count += eeprom_write_page(epm, (uint8_t *)(buff + i * size), offset + i * size, flash->page_size);
    }

    return count;
}

static int fal_eeprom_erase_block(fal_flash_t *flash, uint32_t page_addr, uint32_t page_nr)
{
    int i;
    int ret = 0;

    uint32_t    size = flash->page_size;
    struct eeprom *epm  = os_container_of(flash, struct eeprom, flash);

    for (i = 0; i < page_nr; i++)
    {
        ret += eeprom_erase_block(epm, page_addr + i * size);
    }

    return ret;
}

static int eeprom_register_fal(fal_flash_t *flash)
{
    fal_flash_t   *fal_flash = flash;
    struct eeprom *epm       = os_container_of(flash, struct eeprom, flash);

    strncpy(fal_flash->name, epm->info->flash_name, sizeof(fal_flash->name));

    fal_flash->capacity   = epm->info->capacity;
    fal_flash->block_size = epm->info->page_size;
    fal_flash->page_size  = epm->info->page_size;

    fal_flash->ops.read_page   = fal_eeprom_read_page;
    fal_flash->ops.write_page  = fal_eeprom_write_page;
    fal_flash->ops.erase_block = fal_eeprom_erase_block;

    return fal_flash_register(fal_flash);
}

static int eeprom_probe(void)
{
    uint8_t     index;
    struct eeprom *epm;
    os_err_t       ret = OS_SUCCESS;

#if defined(__CC_ARM) || defined(__CLANG_ARM) /* ARM MDK Compiler */
    extern const int os_eeprom_info$$Base;
    extern const int os_eeprom_info$$Limit;

    const struct os_eeprom_info *table = (struct os_eeprom_info *)&os_eeprom_info$$Base;
    int                          num   = (struct os_eeprom_info *)&os_eeprom_info$$Limit - table;

#elif defined(__ICCARM__) || defined(__ICCRX__) /* for IAR Compiler */
    const struct os_eeprom_info *table = (struct os_eeprom_info *)__section_begin("os_eeprom_info");
    int                          num   = (struct os_eeprom_info *)__section_end("os_eeprom_info") - table;
#elif defined(__GNUC__)                         /* for GCC Compiler */
    extern const int             __os_eeprom_info_start;
    extern const int             __os_eeprom_info_end;
    const struct os_eeprom_info *table = (struct os_eeprom_info *)&__os_eeprom_info_start;
    int                          num   = (struct os_eeprom_info *)&__os_eeprom_info_end - table;
#endif

    for (index = 0; index < num; index++)
    {
        epm = os_calloc(1, sizeof(struct eeprom));
        OS_ASSERT(epm);

        epm->info = (struct os_eeprom_info *)&table[index];

        ret = eeprom_bus_init(epm);
        OS_ASSERT(ret == OS_SUCCESS);

        ret = eeprom_register_fal(&(epm->flash));
        OS_ASSERT(ret == OS_SUCCESS);
    }

    return ret;
}

OS_INIT_CALL(eeprom_probe, OS_INIT_LEVEL_DEVICE, OS_INIT_SUBLEVEL_MIDDLE);
