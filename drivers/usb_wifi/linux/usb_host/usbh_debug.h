#ifndef _USBH_DEBUG_H
#define _USBH_DEBUG_H


#include "stdio.h"
#include <driver.h>

#define USB_PRINT_LVL  USB_PRINT_OFF


#define USB_PRINT_OFF       0  /* usb print off*/
#define USB_HALT_LVL        1  /* system is unusable*/
#define USB_EMERG_LVL       2  /* system is unusable*/
#define USB_ALERT_LVL       3  /* action must be taken immediately*/
#define USB_CRIT_LVL        4  /* critical conditions*/
#define USB_ERR_LVL         5  /* rror conditions*/
#define USB_WARNING_LVL     6  /* warning conditions*/
#define USB_NOTICE_LVL      7  /* normal but significant condition*/
#define USB_INFO_LVL        8  /* informational*/
#define USB_DEBUG_LVL       9  /* debug-level messages*/


#define USB_HALT_STR        "<halt>"    /* system is unusable*/
#define USB_EMERG_STR       "<emerge>"  /* system is unusable*/
#define USB_ALERT_STR       "<alert>"   /* action must be taken immediately*/
#define USB_CRIT_STR        "<crit>"    /* critical conditions*/
#define USB_ERR_STR         "<err>"     /* rror conditions*/
#define USB_WARNING_STR     "<warn>"    /* warning conditions*/
#define USB_NOTICE_STR      "<notice>"  /* normal but significant condition*/
#define USB_INFO_STR        "<info>"    /* informational*/
#define USB_DEBUG_STR       "<debug>"   /* debug-level messages*/


#define usb_printk(level, level_str, dev, format, arg...) \
    ({ if (USB_PRINT_LVL >= level) os_kprintf(level_str "%s: " format ,  \
        ((dev) != NULL)?(dev)->name:"" , ## arg);})
    
#define usb_halt(dev, format, arg...) \
        do{usb_printk(USB_HALT_LVL, USB_HALT_STR , dev , "%s " format , __func__, ## arg);while(1);}while(0)
#define usb_emerg(dev, format, arg...) \
    usb_printk(USB_EMERG_LVL, USB_EMERG_STR , dev , "%s "format , __func__, ## arg)
#define usb_alert(dev, format, arg...) \
    usb_printk(USB_ALERT_LVL, USB_ALERT_STR , dev , format , ## arg)
#define usb_crit(dev, format, arg...) \
    usb_printk(USB_CRIT_LVL, USB_CRIT_STR , dev , format , ## arg)
#define usb_err(dev, format, arg...) \
    usb_printk(USB_ERR_LVL, USB_ERR_STR , dev , format , ## arg)
#define usb_warn(dev, format, arg...) \
    usb_printk(USB_WARNING_LVL, USB_WARNING_STR , dev , format , ## arg)
#define usb_notice(dev, format, arg...) \
    usb_printk(USB_NOTICE_LVL, USB_NOTICE_STR , dev , format , ## arg)
#define usb_info(dev, format, arg...) \
    usb_printk(USB_INFO_LVL, USB_INFO_STR , dev , format , ## arg)
#define usb_dbg(dev, format, arg...) \
    usb_printk(USB_DEBUG_LVL, USB_DEBUG_STR , dev , format , ## arg)


#define USBH_DEBUG_LEVEL USBH_DEBUG_ERROR

#ifndef USBH_DEBUG_LEVEL
#define USBH_DEBUG_LEVEL USBH_DEBUG_ERROR
#endif

#define USBH_DEBUG_OFF     0
#define USBH_DEBUG_ERROR   1
#define USBH_DEBUG_TRACE   2


#define USBH_TRACE(...)   ((USBH_DEBUG_LEVEL >= USBH_DEBUG_TRACE)  ? (void)(os_kprintf(__VA_ARGS__)) : (void)0)
#define USBH_DBG(...)   ((USBH_DEBUG_LEVEL >= USBH_DEBUG_ERROR)  ? (void)(os_kprintf(__VA_ARGS__)) : (void)0)




#endif
