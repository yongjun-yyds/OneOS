#ifndef _TIMER_H
#define _TIMER_H

#include "os_timer.h"
#include "stdint.h"
#include "oneos_config.h"
#include "os_clock.h"
#include <driver.h>
#include "delay.h"

#define HZ   OS_TICK_PER_SECOND

struct timer_list {
    os_timer_id tmr;
    char *name;
    unsigned long expires;

    void (*function)(unsigned long);
    unsigned long data;
};

//#define init_timer(timer) _init_timer(#)

//#define mod_timer(timer, expires) _mod_timer(#timer, timer, expires)

#define del_timer_sync(t) del_timer(t)
int mod_timer(struct timer_list *timer, unsigned long expires);

void init_timer(struct timer_list *timer);

void add_timer(struct timer_list *timer);
int timer_pending(struct timer_list * timer);

int del_timer(struct timer_list *timer);

void msleep(unsigned int msecs);




#endif
