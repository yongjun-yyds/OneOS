#include <os_task.h>
#include <driver.h>
#include "usbh_debug.h"
#include "string.h"
#include "linux_timer.h"
#include "misc_cvt.h"





unsigned int TimerTotalCtr = 0;

int timer_pending(struct timer_list * timer);

static void timer_call_back(void *p_arg)
{
    struct timer_list *timer = p_arg;

    USBH_TRACE("timer_call_back timer:%p\r\n",timer);

    timer->function(timer->data);
}

int mod_timer(struct timer_list *timer, unsigned long expires)
{
    os_err_t err;
    int ret = 1;

    //actived tmr
	if (timer->expires == expires && timer_pending(timer))
		return 1; 
    
    expires = expires - os_tick_get_value();

    expires = (expires * OS_TICK_PER_SECOND + (OS_TICK_PER_SECOND - 1))/ OS_TICK_PER_SECOND;

    if (os_timer_check_exist(timer->tmr))
    {
        if (os_timer_is_active(timer->tmr))
        {
            err = os_timer_stop(timer->tmr);
            if(err != OS_SUCCESS)
            {
                USBH_DBG("mod_timer os_timer_stop Failed %d\r\n",err);
            }
        }
        os_timer_destroy(timer->tmr);
    }
    
    timer->tmr = os_timer_create(OS_NULL, "", timer_call_back, timer, expires, OS_TIMER_FLAG_ONE_SHOT);
    if(timer->tmr == OS_NULL)
    {
        USBH_DBG("mod_timer os_timer_init Failed %d\r\n",err);
    }

    err = os_timer_set_timeout_ticks(timer->tmr, expires);
    if(err != OS_SUCCESS)
    {
        USBH_DBG("mod_timer os_timer_set_timeout_ticks Failed %d\r\n",err);
    }

    err = os_timer_start(timer->tmr);
    if(err != OS_SUCCESS)
    {
        USBH_DBG("mod_timer os_timer_start Failed %d\r\n",err);
    }

    return ret;
}




void init_timer(struct timer_list *timer)
{
    os_ubase_t  level;
    os_err_t    err;
    
    os_spin_lock_irqsave(&gs_device_lock, &level);
    TimerTotalCtr++;
    os_spin_unlock_irqrestore(&gs_device_lock, level);
    
    timer->tmr = os_timer_create(OS_NULL, "", timer_call_back, timer, 10, OS_TIMER_FLAG_ONE_SHOT);
    if(timer->tmr == OS_NULL)
    {
        USBH_DBG("init_timer os_timer_init Failed %d\r\n",err);
    }

}


void add_timer(struct timer_list *timer)
{
    BUG_ON(timer_pending(timer));
    mod_timer(timer, timer->expires);
}



int timer_pending(struct timer_list * timer)
{
    os_bool_t state;
    
    if (timer->tmr != OS_NULL)
    {
        if (os_timer_check_exist(timer->tmr))
        {
            state = os_timer_is_active(timer->tmr) ? OS_TRUE : OS_FALSE;
        }
        else
            state = OS_FALSE;
    }
    else
        state = OS_FALSE;
    
    if (state)
        return 1;
    else
        return 0;
}

int del_timer(struct timer_list *timer)
{
    os_ubase_t level;

    if (timer_pending(timer)) 
    {
        os_timer_stop(timer->tmr);
    }

    os_spin_lock_irqsave(&gs_device_lock, &level);
    TimerTotalCtr--;
    os_spin_unlock_irqrestore(&gs_device_lock, level);

    if (os_timer_check_exist(timer->tmr))
    {
        os_timer_destroy(timer->tmr);
    }

    return 1;
}

/**
 * msleep - sleep safely even with waitqueue interruptions
 * @msecs: Time in milliseconds to sleep for
 */
void msleep(unsigned int msecs)
{
    os_err_t err;
    
    err = os_task_msleep(msecs);
    if(err != OS_SUCCESS)
        USBH_DBG("msleep error:%d\r\n",err);
}
