#include "os_mutex.h"
#include "stdio.h"
#include "mutex.h"

void _mutex_init(char *name, struct mutex *mutex)
{
    os_err_t err;
    

#if OS_CFG_DBG_EN > 0u
    os_ubase_t level;
    //The procedure _mutex_init()-->OSMutexCreate()-->OS_MutexDbgListAdd() is dangerous,
    //because some linux driver don't call mutex_destroy(), the mutex instance will
    //still exsit in OSMutexDbgListPtr after freed. Alway set OSMutexDbgListPtr to zero.
    os_spin_lock_irqsave(&gs_device_lock, &level);
    OSMutexDbgListPtr = NULL;
    os_spin_unlock_irqrestore(&gs_device_lock, level);
#endif
    mutex->mutex =  os_mutex_create(OS_NULL, name, OS_FALSE);

    if(mutex->mutex == OS_NULL)
    {
        os_kprintf("_mutex_init failed:%d hang!!\r\n",err);
        while(1);
    }
}

void mutex_destroy(struct mutex *mutex)
{
    os_err_t err;
    
    err = os_mutex_destroy(mutex->mutex);
    
    if(err != OS_SUCCESS)
    {
        os_kprintf("mutex_destroy failed:%d hang!!\r\n",err);
        while(1);
    }    
}

void mutex_lock(struct mutex *lock)
{
    os_err_t err;

    err = os_mutex_lock(lock->mutex, OS_WAIT_FOREVER);
    if (err != OS_SUCCESS)
    {
        os_kprintf("mutex_lock failed:%d hang!!\r\n", err);
        while(1);
    }
    
}

void mutex_unlock(struct mutex *lock)
{
    os_err_t err;

    os_mutex_unlock(lock->mutex);
    if(err != OS_SUCCESS)
    {
        os_kprintf("mutex_unlock failed:%d hang!!\r\n",err);
        while(1);
    }
}





