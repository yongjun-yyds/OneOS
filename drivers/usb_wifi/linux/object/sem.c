#include <arch_interrupt.h>
#include "usbh_debug.h"
#include "errno_ext.h"
#include "sem.h"
#include "kthread.h"
#include "misc_cvt.h"



unsigned int SemaTotalCtr = 0;



void _sema_init(char *name, struct semaphore *sem, int val)
{
    os_err_t err;
    os_ubase_t level;

    os_spin_lock_irqsave(&gs_device_lock, &level);
    SemaTotalCtr++;
#if OS_CFG_DBG_EN > 0u   
    //The procedure _sema_init()-->OSSemCreate()-->OS_SemDbgListAdd() is dangerous,
    //because some linux driver don't call sema_destroy(), the semaphore instance will
    //still exsit in OSSemDbgListPtr after freed. Alway set OSSemDbgListPtr to zero.
    OSSemDbgListPtr = NULL;
#endif
    os_spin_unlock_irqrestore(&gs_device_lock, level);

    sem->p_os_sem = os_semaphore_create(OS_NULL, name, val, OS_SEM_MAX_VALUE);
    if(!(sem->p_os_sem))
    {
        USBH_DBG("sema_init OSSemCreate Failed %d\r\n",err);
    }

}



int _down_interruptible(struct semaphore *sem)
{
    os_err_t err;
    struct task_struct *task;
#if 0
    os_schedule_lock();
    task  = container_of(os_get_current_task(),struct task_struct,tcb);
    if(task->is_killed)
    {
        os_schedule_unlock();
        USBH_TRACE("_down_interruptible %s is_killed\r\n",task->name);
        return -EINTR;
    }
    os_schedule_unlock();
#endif         

    err = os_semaphore_wait(sem->p_os_sem, OS_WAIT_FOREVER);
    if(err == OS_SUCCESS)
        return 0;

    if(err == OS_TIMEOUT)
    {
        USBH_TRACE("_down_interruptible OS_ERR_PEND_ABORT %s may be killed\r\n",task->name);
        return -EINTR;
    }
    else
    {
        USBH_DBG("down_interruptible OSSemPend Failed %d\r\n",err);
        return -EINTR;
    }
}


void _up(struct semaphore *sem)
{
    os_err_t err;
    
    err = os_semaphore_post(sem->p_os_sem);
    if(err != OS_SUCCESS)
    {
        USBH_DBG("_up OSSemPost Failed %s %d\r\n", os_semaphore_get_name(sem->p_os_sem), err);
    }
}




void _sema_destroy(struct semaphore *sem)
{
    os_err_t err;

    os_schedule_lock();
    SemaTotalCtr--;
    os_schedule_unlock();

    err = os_semaphore_destroy(sem->p_os_sem);
    if(err != OS_SUCCESS){
        USBH_DBG("_sema_destroy OSSemPend Failed %d\r\n",err);
    }

}


/**
 * down_trylock - try to acquire the semaphore, without waiting
 * @sem: the semaphore to be acquired
 *
 * Try to acquire the semaphore atomically.  Returns 0 if the mutex has
 * been acquired successfully or 1 if it it cannot be acquired.
 *
 * NOTE: This return value is inverted from both spin_trylock and
 * mutex_trylock!  Be careful about this when converting code.
 *
 * Unlike mutex_trylock, this function can be used from interrupt context,
 * and the semaphore can be released by any task or interrupt.
 */
int down_trylock(struct semaphore *sem)
{
    os_err_t err;

    err = os_semaphore_wait(sem->p_os_sem, OS_NO_WAIT);
    if(err == OS_SUCCESS)
        return 0;
    else if(err == OS_TIMEOUT)
        return 1;
    else
    {
        USBH_DBG("down_trylock err:%d should not occur\r\n",err);
        return 1;
    }
}


/**
 * down - acquire the semaphore
 * @sem: the semaphore to be acquired
 *
 * Acquires the semaphore.  If no more tasks are allowed to acquire the
 * semaphore, calling this function will put the task to sleep until the
 * semaphore is released.
 *
 * Use of this function is deprecated, please use down_interruptible() or
 * down_killable() instead.
 */
void down(struct semaphore *sem)
{
    os_err_t err;
    
    err = os_semaphore_wait(sem->p_os_sem, OS_WAIT_FOREVER);
    if(err != OS_SUCCESS)
    {
        USBH_DBG("down OSSemPend Failed %d\r\n",err);
    }
}

