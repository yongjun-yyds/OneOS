#include <arch_interrupt.h>
#include "atomic.h"
#include <os_spinlock.h>

OS_DEFINE_SPINLOCK(atomic_lock);


int atomic_sub_return(int i, atomic_t *v)
{
    int ret;
    os_ubase_t level;

    os_spin_lock_irqsave(&atomic_lock, &level);

    v->counter -= i;
    ret = v->counter;

    os_spin_unlock_irqrestore(&atomic_lock, level);

    return ret;
}

int atomic_add_return(int i, atomic_t *v)
{
    int ret;
    os_ubase_t level;

    os_spin_lock_irqsave(&atomic_lock, &level);
    
    ret = v->counter;
    v->counter += i;
    
    os_spin_unlock_irqrestore(&atomic_lock, level);

    return ret;
}








