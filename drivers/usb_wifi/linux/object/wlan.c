#include <driver.h>
#include "wlan_ethernetif.h"
#include "shell.h"
#include "wlan.h"
#include "wireless.h"
#include "rt_config.h"

#include "usbh_linux.h"
#include "net_cvt.h"
#include "memory.h"
#include "tasklet.h"

#include <net_dev.h>
#include <oneos_lwip.h>

#define USBWIFI_MAC_ADDR_LEN    6
#define ETHERNET_DEFAULT_MTU    1500

uint8_t usbwifi_default_mac[6] = {0xc0, 0xa8, 0xb9, 0x12, 0x34, 0x33};
typedef struct usbwifi_dev
{
    struct os_net_device net_dev;
    uint8_t           dev_addr[USBWIFI_MAC_ADDR_LEN];
} usbwifi_dev_t;

static usbwifi_dev_t *gs_usbwifi_dev;

static os_err_t usbwifi_get_macaddr(struct os_net_device *net_dev, uint8_t *addr)
{
    memcpy(addr, gs_usbwifi_dev->dev_addr, USBWIFI_MAC_ADDR_LEN);

    return 0;
}
const static struct os_net_device_ops usbwifi_ops = {
    .init       = OS_NULL,
    .deinit     = OS_NULL,
    .send       = OS_NULL,
    .get_mac    = usbwifi_get_macaddr,
    .set_filter = OS_NULL,
};

/**
  * @brief  Initializes the lwIP stack
  * @param  None
  * @retval None
  */
void usbwifi_init(void)
{
    uint8_t      *pub_addr;
    int           rc;
    int           i;
    struct netif *usbwifi_netif;

    pub_addr = usbwifi_default_mac;

    gs_usbwifi_dev = malloc(sizeof(usbwifi_dev_t));
    if (OS_NULL == gs_usbwifi_dev)
    {
        os_kprintf("usbwifi dev malloc failed!\r\n");
        return;
    }
    memset(gs_usbwifi_dev, 0, sizeof(usbwifi_dev_t));

    for (i = 0; i < 6; i++)
    {
        gs_usbwifi_dev->dev_addr[i] = pub_addr[i];
    }

    gs_usbwifi_dev->net_dev.info.MTU = ETHERNET_DEFAULT_MTU;
    gs_usbwifi_dev->net_dev.info.MRU = ETHERNET_DEFAULT_MTU;

    gs_usbwifi_dev->net_dev.info.mode = (os_net_mode_t) OS_TRUE;

    gs_usbwifi_dev->net_dev.info.linkstatus = OS_FALSE;

    gs_usbwifi_dev->net_dev.info.intf_type = net_dev_intf_ether;
    gs_usbwifi_dev->net_dev.ops            = &usbwifi_ops;

    rc = os_net_device_register(&gs_usbwifi_dev->net_dev, "usbwifi");
    if (OS_SUCCESS != rc)
    {
        os_kprintf("usbwifi os_net_device_register failed\r\n");
    }

    os_task_msleep(1000);

    usbwifi_netif = os_lwip_get_netif_by_devname("usbwifi");
    if (usbwifi_netif == NULL)
    {
        os_kprintf("get usbwifi netif failed\r\n");
    }
    wlan_ethernetif_init(usbwifi_netif);
}

void wireless_send_event(struct net_device *	dev,
			 unsigned int		cmd,
			 union iwreq_data *	wrqu,
			 char *			extra)
{
    
    printf("wireless_send_event[0x%x]",cmd);


    if(cmd == IWEVCUSTOM)
    {
        printf("[0x%x] %s\r\n",wrqu->data.flags,extra);
        switch (wrqu->data.flags) 
        {
            case IW_STA_LINKUP_EVENT_FLAG:
                os_net_linkchange(&gs_usbwifi_dev->net_dev, OS_TRUE);
                break;
            case IW_STA_LINKDOWN_EVENT_FLAG:
                os_net_linkchange(&gs_usbwifi_dev->net_dev, OS_FALSE);
                break;                  
        }

    }   
}

extern int rtusb_init(void);
extern const struct usb_device_id rtusb_dev_id[];
extern struct net_device *_pnet_device;

#define	IFF_UP		0x1

static void ralink_hotplug_call_back(struct usb_interface *intf, void *arg, int is_connect)
{
    #if 1
    struct net_device *net_dev = _pnet_device;

    os_ubase_t level;    
    int ret;

    
    if(is_connect)
    {
        /* usb device is connected*/   

        /* If a net device is registered, open it*/
        if(_pnet_device)
        {
            _pnet_device->flags |= IFF_UP;
            _pnet_device->usb_dev = interface_to_usbdev(intf);

            ret = (*(_pnet_device->open))(_pnet_device);
            if(ret)
            {
                os_kprintf("USBH_ProbeUSB _pnet_device->open Failed:%d\r\n",ret);
                return;
            }
        }
    }
    else
    {
        /* usb device is disconnected*/ 
        
        /* Close net device */
        if(_pnet_device)
        {
            os_spin_lock_irqsave(&gs_device_lock, &level);
            while(net_dev->status)
            {
                os_spin_unlock_irqrestore(&gs_device_lock, level);
                os_task_msleep(10);
                os_spin_lock_irqsave(&gs_device_lock, &level);
            }
            _pnet_device = NULL;
            os_spin_unlock_irqrestore(&gs_device_lock, level); 

            (net_dev->stop)(net_dev);
        }
    }
    #endif
}

struct usb_hotplug usb_dev = 
{
    .id = rtusb_dev_id,
    .call_back = ralink_hotplug_call_back,
    .arg = NULL
};

int wlan_init(void)
{
    usbwifi_init();

    /* Create tasklet task used for usb wireless driver */
    tasklet_task_init();
    
    /* Initializes usb host */
    usb_init();
    
    /* Register ralink usb wireless driver */
    rtusb_init();
    /* Add hotplug function that will be called if usb device plugged and match the usb_device_id*/
    usb_hotplug_add(&usb_dev);

    /* If all usb driver are initialized, start usb host. */
    usb_start();
    
    return OS_SUCCESS;
}

OS_INIT_CALL(wlan_init, OS_INIT_LEVEL_APPLICATION, OS_INIT_SUBLEVEL_HIGH);
