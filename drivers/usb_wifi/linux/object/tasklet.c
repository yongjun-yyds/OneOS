#include "usbh_debug.h"
#include "usb_conf.h"
#include "irq.h"
#include "tasklet.h"
#include "bitops.h"




#define TASKLET_STATE_UNSCHED     0
#define TASKLET_STATE_SCHED       1


os_task_id TaskletActionTCB;
uint32_t TaskletAction_TaskStk[KERNEL_THREAD_STK_SIZE];

void tasklet_action(void *p_arg);

void tasklet_task_init()
{
    TaskletActionTCB = os_task_create(OS_NULL, &TaskletAction_TaskStk[0], KERNEL_THREAD_STK_SIZE, "Tasklet Action", (void (*)(void *arg))tasklet_action, NULL,  TASKLET_ACTION_TASK_PRIO);
    if (TaskletActionTCB == OS_NULL)
    {
        USBH_DBG("tasklet_task_init fail\r\n");
        return;
    }
    
    os_task_startup(TaskletActionTCB);
    
}

#include <os_mq.h>

os_msgqueue_id      Tasklet_ProbeQ;
os_msgqueue_dummy_t Tasklet_ProbeQ_dummy;

#define MQ_MAX_MSG      5
#define MSG_SIZE        8
#define MSG_HEADER_SIZE 8
#define MQ_POLL_SIZE    (MQ_MAX_MSG * (MSG_SIZE + MSG_HEADER_SIZE))
static char mq_pool[MQ_POLL_SIZE];

void tasklet_schedule(struct tasklet_struct *t)
{
    os_err_t err;
    uint32_t mq_buf[2];

    os_schedule_lock();
    if(t->state == TASKLET_STATE_SCHED)
    {
        os_schedule_unlock();
        return;
    }
    t->state = TASKLET_STATE_SCHED;
    os_schedule_unlock();
    
    mq_buf[0] = (uint32_t)t;
    err = os_msgqueue_send(Tasklet_ProbeQ, &mq_buf[0], sizeof(mq_buf), OS_WAIT_FOREVER);
    if(err!= OS_SUCCESS)
    {
        USBH_DBG("USBH_ConnectProbeCallback  OSQPost failed:%d\r\n",err);
    }
    if(err != OS_SUCCESS)
        USBH_DBG("tasklet_schedule OSTaskQPost Failed %d\r\n",err);
    
}


void tasklet_init(struct tasklet_struct *t,
		  void (*func)(unsigned long), unsigned long data)
{
// 	t->next = NULL;
	t->state = TASKLET_STATE_UNSCHED;
	atomic_set(&t->count, 0);
	t->func = func;
	t->data = data;
}


void tasklet_action(void *p_arg)
{
    os_err_t err;
    struct tasklet_struct *t;
    uint32_t msg_mq[2];
    os_size_t recv_size = 0;
    
    Tasklet_ProbeQ = os_msgqueue_create_static(&Tasklet_ProbeQ_dummy, "Tasklet_ProbeQ", &mq_pool[0], MQ_POLL_SIZE, MSG_SIZE);

    while(1)
    {
        err = os_msgqueue_recv(Tasklet_ProbeQ, &msg_mq[0], sizeof(msg_mq), OS_WAIT_FOREVER, &recv_size);
        t = (struct tasklet_struct *)msg_mq[0];
        if(err != OS_SUCCESS)
        {
            USBH_DBG("tasklet_schedule OSTaskQPend Failed %d\r\n",err);
            continue;
        }
        if(!t){
            USBH_DBG("tasklet_schedule t == NULL\r\n");
            continue;
        }

        os_schedule_lock();
        if(t->state == TASKLET_STATE_UNSCHED)
            USBH_DBG("tasklet_action Error at %d\r\n",__LINE__);
        t->state = TASKLET_STATE_UNSCHED;
        os_schedule_unlock();      
        
        t->func(t->data);
    }

}

void flush_scheduled_work(void)
{
    USBH_DBG("flush_scheduled_work\r\n");
//	flush_workqueue(keventd_wq);
}


// static int inline tasklet_state_test_and_set(unsigned long *addr,unsigned long val)
// {
//     unsigned long temp_val;
//     temp_val = *addr;    
//     *addr = val;  
//     return (temp_val == val); 
// }

//in t->func(t->data), don't restart tasklet_schedule,oterwise tasklet_kill will not exit.
void tasklet_kill(struct tasklet_struct *t)
{    
	if (os_is_irq_active())
		USBH_DBG("tasklet_kill Attempt to kill tasklet from interrupt\r\n");

    os_schedule_lock();
	while (t->state == TASKLET_STATE_SCHED){
        t->state = TASKLET_STATE_SCHED; 
		do {
            os_schedule_unlock();
            os_task_msleep(5);
            os_schedule_lock();  
		} while (t->state == TASKLET_STATE_SCHED);
	}
    os_schedule_unlock();
    
    
//	while (test_and_set_bit(TASKLET_STATE_SCHED, &t->state)) {
//		do {
//			yield();
//		} while (test_bit(TASKLET_STATE_SCHED, &t->state));
//	}
//	tasklet_unlock_wait(t);
//	clear_bit(TASKLET_STATE_SCHED, &t->state);
}



