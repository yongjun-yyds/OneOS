/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        nand_devices.c
 *
 * @details
 *
 * @revision
 * Date          Author          Notes
 * 2020-07-22    OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <string.h>
#include "oneos_config.h"
#include "nand.h"

static const struct nand_device_info nand_info_table[] = {
/*
hardecc.addr_offset need make sure has enough continuous space to store hardecc, normally 4-bytes
*/

/* H27x4G */
#ifdef BSP_NAND_H27U4G8F2D
    {
        .name                            = "H27U4G8F2D",
        .id                              = 0xADDC9095,
        .bus_width                       = 8,
        .page_size                       = 2048,
        .spare_size                      = 64,
        .block_size                      = 64,
        .plane_size                      = 2048,
        .plane_nr                        = 2,
        .badflag_info.inpage_offset      = 0,
        .badflag_info.first_page_offset  = 0,
        .badflag_info.second_page_offset = 1,
        .hardecc_info.inpage_offset      = 1
    },
#endif

#ifdef BSP_NAND_H27S4G8F2D
    {
        .name                            = "H27S4G8F2D",
        .id                              = 0xADAC9015,
        .bus_width                       = 8,
        .page_size                       = 2048,
        .spare_size                      = 64,
        .block_size                      = 64,
        .plane_size                      = 2048,
        .plane_nr                        = 2,
        .badflag_info.inpage_offset      = 0,
        .badflag_info.first_page_offset  = 0,
        .badflag_info.second_page_offset = 1,
        .hardecc_info.inpage_offset      = 1
    },
#endif

#ifdef BSP_NAND_H27U4G6F2D
    {
        .name                            = "H27U4G6F2D",
        .id                              = 0xADCC90D5,
        .bus_width                       = 16,
        .page_size                       = 2048,
        .spare_size                      = 64,
        .block_size                      = 64,
        .plane_size                      = 2048,
        .plane_nr                        = 2,
        .badflag_info.inpage_offset      = 0,
        .badflag_info.first_page_offset  = 0,
        .badflag_info.second_page_offset = 1,
        .hardecc_info.inpage_offset      = 1
    },
#endif

#ifdef BSP_NAND_H27S4G6F2D
    {
        .name                            = "H27S4G6F2D",
        .id                              = 0xADBC9055,
        .bus_width                       = 16,
        .page_size                       = 2048,
        .spare_size                      = 64,
        .block_size                      = 64,
        .plane_size                      = 2048,
        .plane_nr                        = 2,
        .badflag_info.inpage_offset      = 0,
        .badflag_info.first_page_offset  = 0,
        .badflag_info.second_page_offset = 1,
        .hardecc_info.inpage_offset      = 1
    },
#endif

/* GD9Fx1G */
#ifdef BSP_NAND_GD9FU1G8F2A
    {
        .name                            = "GD9FU1G8F2A",
        .id                              = 0xC8F1801D,
        .bus_width                       = 8,
        .page_size                       = 2048,
        .spare_size                      = 128,
        .block_size                      = 64,
        .plane_size                      = 1024,
        .plane_nr                        = 1,
        .badflag_info.inpage_offset      = 0,
        .badflag_info.first_page_offset  = 0,
        .badflag_info.second_page_offset = 63,
        .hardecc_info.inpage_offset      = 1
    },
#endif

#ifdef BSP_NAND_GD9FS1G8F2A
    {
        .name                            = "GD9FS1G8F2A",
        .id                              = 0xC8A18015,
        .bus_width                       = 8,
        .page_size                       = 2048,
        .spare_size                      = 128,
        .block_size                      = 64,
        .plane_size                      = 1024,
        .plane_nr                        = 1,
        .badflag_info.inpage_offset      = 0,
        .badflag_info.first_page_offset  = 0,
        .badflag_info.second_page_offset = 63,
        .hardecc_info.inpage_offset      = 1
    },
#endif

#ifdef BSP_NAND_GD9FU1G6F2A
    {
        .name                            = "GD9FU1G6F2A",
        .id                              = 0xC8C1805D,
        .bus_width                       = 16,
        .page_size                       = 2048,
        .spare_size                      = 128,
        .block_size                      = 64,
        .plane_size                      = 1024,
        .plane_nr                        = 1,
        .badflag_info.inpage_offset      = 0,
        .badflag_info.first_page_offset  = 0,
        .badflag_info.second_page_offset = 63,
        .hardecc_info.inpage_offset      = 1
    },
#endif

#ifdef BSP_NAND_GD9FS1G6F2A
    {
        .name                            = "GD9FS1G6F2A",
        .id                              = 0xC8B18055,
        .bus_width                       = 16,
        .page_size                       = 2048,
        .spare_size                      = 128,
        .block_size                      = 64,
        .plane_size                      = 1024,
        .plane_nr                        = 1,
        .badflag_info.inpage_offset      = 0,
        .badflag_info.first_page_offset  = 0,
        .badflag_info.second_page_offset = 63,
        .hardecc_info.inpage_offset      = 1
    },
#endif

/* MT29F4G */
#ifdef BSP_NAND_MT29F4G08ABADA
    {
        .name                            = "MT29F4G08ABADA",
        .id                              = 0x2CDC9095,
        .bus_width                       = 8,
        .page_size                       = 2048,
        .spare_size                      = 64,
        .block_size                      = 64,
        .plane_size                      = 2048,
        .plane_nr                        = 2,
        .badflag_info.inpage_offset      = 0,
        .badflag_info.first_page_offset  = 0,
        .badflag_info.second_page_offset = 1,
        .hardecc_info.inpage_offset      = 1
    },
#endif
};

static const int nand_info_table_size = ARRAY_SIZE(nand_info_table);

const struct nand_device_info *get_nand_info_by_id(uint32_t id)
{
    int i;

    for (i = 0; i < nand_info_table_size; i++)
    {
        if (nand_info_table[i].id == id)
        {
            return &nand_info_table[i];
        }
    }

    return OS_NULL;
}

const struct nand_device_info *get_nand_info_by_name(const char *name)
{
    int i;

    for (i = 0; i < nand_info_table_size; i++)
    {
        if (strcmp(nand_info_table[i].name, name) == 0)
        {
            return &nand_info_table[i];
        }
    }

    return OS_NULL;
}
