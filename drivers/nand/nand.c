/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        nand.c
 *
 * @details
 *
 * @revision
 * Date          Author          Notes
 * 2020-07-22    OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <string.h>
#include <arch_interrupt.h>
#include <arch_misc.h>
#include <os_task.h>
#include <device.h>
#include <os_memory.h>
#include <os_util.h>
#include <os_assert.h>
#include <drv_cfg.h>
#include <drv_log.h>

#define DRV_EXT_LVL DBG_EXT_INFO
#define DRV_EXT_TAG "nand"
#include <dlog.h>

#ifdef OS_USING_FAL

#include <fal/fal.h>

static int fal_nand_read_page(fal_flash_t *flash, uint32_t page_addr, uint8_t *buff, uint32_t page_nr)
{
    os_nand_device_t *nand = flash->priv;

    return os_nand_read_page(nand, page_addr, buff, page_nr);
}

static int fal_nand_write_page(fal_flash_t *flash, uint32_t page_addr, const uint8_t *buff, uint32_t page_nr)
{
    os_nand_device_t *nand = flash->priv;

    return os_nand_write_page(nand, page_addr, buff, page_nr);
}

static int fal_nand_erase_block(fal_flash_t *flash, uint32_t page_addr, uint32_t page_nr)
{
    int         i, ret;
    uint32_t block_addr, block_nr;

    os_nand_device_t *nand = flash->priv;

    if ((page_addr % nand->cfg.info.block_size != 0) || (page_nr % nand->cfg.info.block_size != 0))
    {
        LOG_E(DRV_EXT_TAG, "erase offset or size error!");
        return -1;
    }

    block_addr = page_addr / nand->cfg.info.block_size;
    block_nr   = page_nr / nand->cfg.info.block_size;

    for (i = 0; i < block_nr; i++)
    {
        ret = os_nand_erase_block(nand, block_addr + i);
        if (ret != 0)
        {
            return ret;
        }
    }

    return 0;
}

static int fal_nand_flash_register(os_nand_device_t *nand)
{
    fal_flash_t *fal_flash = os_calloc(1, sizeof(fal_flash_t));

    if (fal_flash == OS_NULL)
    {
        LOG_E(DRV_EXT_TAG, "fal nand mem leak %s", device_name(&nand->parent));
        return -1;
    }

    memcpy(fal_flash->name, device_name(&nand->parent), min(FAL_DEV_NAME_MAX - 1, strlen(device_name(&nand->parent))));
    fal_flash->name[min(FAL_DEV_NAME_MAX - 1, strlen(device_name(&nand->parent)))] = 0;

    fal_flash->capacity   = nand->cfg.capacity;
    fal_flash->block_size = nand->cfg.info.page_size * nand->cfg.info.block_size;
    fal_flash->page_size  = nand->cfg.info.page_size;

    fal_flash->ops.read_page   = fal_nand_read_page;
    fal_flash->ops.write_page  = fal_nand_write_page;
    fal_flash->ops.erase_block = fal_nand_erase_block;

    fal_flash->priv = nand;

    return fal_flash_register(fal_flash);
}
#endif

os_err_t os_nand_device_register(os_nand_device_t *nand, const char *name, uint32_t id)
{
    const struct nand_device_info *info = OS_NULL;

    OS_ASSERT(nand != OS_NULL);

    info = get_nand_info_by_id(id);
    if (info == OS_NULL)
    {
        LOG_E(DRV_EXT_TAG, "can not find nand with id: 0x%08x", id);
        return OS_FAILURE;
    }

    memcpy(&nand->cfg.info, info, sizeof(struct nand_device_info));
    nand->cfg.info.badflag_info.mark_data = 0x55;
    nand->cfg.info.badflag_info.length    = 0x01;

    nand->cfg.page_shift  = os_ffs(nand->cfg.info.page_size) - 1;
    nand->cfg.block_shift = nand->cfg.page_shift + os_ffs(nand->cfg.info.block_size) - 1;
    nand->cfg.plane_shift = nand->cfg.block_shift + os_ffs(nand->cfg.info.plane_size) - 1;

    nand->cfg.page_mask  = (nand->cfg.info.block_size - 1) << nand->cfg.page_shift;
    nand->cfg.block_mask = (nand->cfg.info.plane_size - 1) << nand->cfg.block_shift;
    nand->cfg.plane_mask = (nand->cfg.info.plane_nr - 1) << nand->cfg.plane_shift;

    nand->cfg.capacity = nand->cfg.info.page_size * nand->cfg.info.block_size * nand->cfg.info.plane_size * nand->cfg.info.plane_nr;

    if (nand->ops->config_hardecc && nand->ops->control_hardecc && nand->ops->get_hardecc)
    {
        if (nand->ops->config_hardecc(nand) == OS_SUCCESS)
        {
            nand->hardecc_flag = OS_TRUE;
        }
        else
        {
            nand->hardecc_flag = OS_FALSE;
        }
    }
    else
    {
        nand->hardecc_flag = OS_FALSE;
    }

    nand->cfg.oob_len =
        nand->cfg.info.spare_size - nand->cfg.info.badflag_info.length - nand->cfg.info.hardecc_info.length;
    nand->page_buff = os_calloc(1, nand->cfg.info.page_size);
    OS_ASSERT(nand->page_buff);
    nand->spare_buff = os_calloc(1, nand->cfg.info.spare_size);
    OS_ASSERT(nand->spare_buff);

    nand->parent.ops  = OS_NULL;
    nand->parent.type = OS_DEVICE_TYPE_MTD;
    os_device_register(&nand->parent, name);

    LOG_D(DRV_EXT_TAG,
          "nand device register success %s(%s):\r\n"
          "    nand  id   : %08X\r\n"
          "    page  size : %d\r\n"
          "    spare size : %d\r\n"
          "    block size : %d\r\n"
          "    plane size : %d\r\n"
          "    plane count: %d\r\n"
          "    total size : %d MB",
          name,
          nand->cfg.info.name,
          nand->cfg.info.id,
          nand->cfg.info.page_size,
          nand->cfg.info.spare_size,
          nand->cfg.info.block_size,
          nand->cfg.info.plane_size,
          nand->cfg.info.plane_nr,
          nand->cfg.capacity / 0x100000);

#ifdef OS_USING_FAL
    return fal_nand_flash_register(nand);
#else
    return OS_SUCCESS;
#endif
}

os_err_t os_nand_read_page(os_nand_device_t *nand, uint32_t page_addr, uint8_t *buff, uint32_t page_nr)
{
    return nand->ops->read_page(nand, page_addr, buff, page_nr);
}

os_err_t os_nand_write_page(os_nand_device_t *nand, uint32_t page_addr, const uint8_t *buff, uint32_t page_nr)
{
    return nand->ops->write_page(nand, page_addr, buff, page_nr);
}

os_err_t os_nand_read_spare(os_nand_device_t *nand, uint32_t page_addr, uint8_t *buff, uint32_t spare_nr)
{
    return nand->ops->read_spare(nand, page_addr, buff, spare_nr);
}

os_err_t os_nand_write_spare(os_nand_device_t *nand, uint32_t page_addr, const uint8_t *buff, uint32_t spare_nr)
{
    return nand->ops->write_spare(nand, page_addr, buff, spare_nr);
}

os_err_t os_nand_erase_block(os_nand_device_t *nand, uint32_t block_no)
{
    return nand->ops->erase_block(nand, block_no * nand->cfg.info.block_size);
}

uint32_t os_nand_get_status(os_nand_device_t *nand)
{
    return nand->ops->get_status(nand);
}

void nand_page2addr(os_nand_device_t *nand, uint32_t page_addr, os_nand_addr_t *nand_addr)
{
    uint32_t addr = page_addr << nand->cfg.page_shift;

    nand_addr->plane = (addr & nand->cfg.plane_mask) >> nand->cfg.plane_shift;
    nand_addr->block = (addr & nand->cfg.block_mask) >> nand->cfg.block_shift;
    nand_addr->page  = (addr & nand->cfg.page_mask) >> nand->cfg.page_shift;
}

uint32_t nand_addr2page(os_nand_device_t *nand, os_nand_addr_t *nand_addr)
{
    uint32_t page_addr;

    page_addr = nand_addr->plane << nand->cfg.plane_shift;
    page_addr |= nand_addr->block << nand->cfg.block_shift;
    page_addr |= nand_addr->page << nand->cfg.page_shift;

    return page_addr;
}

os_err_t os_nand_badflag_check(os_nand_device_t *nand, uint32_t block_no)
{
    os_err_t    ret;
    uint8_t  badflag_buf = 0;
    uint32_t page_addr   = block_no * nand->cfg.info.block_size + nand->cfg.info.badflag_info.first_page_offset;

    ret = nand->ops->read_spare(nand, page_addr, nand->spare_buff, 1);
    if (ret != OS_SUCCESS)
    {
        LOG_E(DRV_EXT_TAG, "nand read spare failed");
        return OS_FAILURE;
    }

    memcpy(&badflag_buf,
           nand->spare_buff + nand->cfg.info.badflag_info.inpage_offset,
           nand->cfg.info.badflag_info.length);

    if (badflag_buf != 0xFF)
    {
        LOG_E(DRV_EXT_TAG, "nand block %d bad", block_no);
        return OS_FAILURE;
    }

    page_addr = block_no * nand->cfg.info.block_size + nand->cfg.info.badflag_info.second_page_offset;

    ret = nand->ops->read_spare(nand, page_addr, nand->spare_buff, 1);
    if (ret != OS_SUCCESS)
    {
        LOG_E(DRV_EXT_TAG, "nand read spare failed");
        return OS_FAILURE;
    }

    memcpy(&badflag_buf,
           nand->spare_buff + nand->cfg.info.badflag_info.inpage_offset,
           nand->cfg.info.badflag_info.length);

    if (badflag_buf != 0xFF)
    {
        LOG_E(DRV_EXT_TAG, "nand block %d bad", block_no);
        return OS_FAILURE;
    }

    return OS_SUCCESS;
}

os_err_t os_nand_badflag_mark(os_nand_device_t *nand, uint32_t block_no)
{
    os_err_t    ret;
    uint32_t page_addr = block_no * nand->cfg.info.block_size + nand->cfg.info.badflag_info.first_page_offset;

    memset(nand->spare_buff, 0xFF, nand->cfg.info.spare_size);

    memcpy(nand->spare_buff + nand->cfg.info.badflag_info.inpage_offset,
           &nand->cfg.info.badflag_info.mark_data,
           nand->cfg.info.badflag_info.length);

    ret = nand->ops->write_spare(nand, page_addr, nand->spare_buff, 1);
    if (ret != OS_SUCCESS)
    {
        LOG_E(DRV_EXT_TAG, "nand write spare failed");
        return OS_FAILURE;
    }

    page_addr = block_no * nand->cfg.info.block_size + nand->cfg.info.badflag_info.second_page_offset;

    memset(nand->spare_buff, 0xFF, nand->cfg.info.spare_size);

    memcpy(nand->spare_buff + nand->cfg.info.badflag_info.inpage_offset,
           &nand->cfg.info.badflag_info.mark_data,
           nand->cfg.info.badflag_info.length);

    ret = nand->ops->write_spare(nand, page_addr, nand->spare_buff, 1);
    if (ret != OS_SUCCESS)
    {
        LOG_E(DRV_EXT_TAG, "nand write spare failed");
        return OS_FAILURE;
    }

    return ret;
}

os_err_t os_nand_read(os_nand_device_t *nand,
                      uint32_t       page_addr,
                      uint8_t       *buff,
                      uint32_t       buff_len,
                      uint8_t       *oob,
                      uint32_t       oob_len)
{
    os_err_t    ret;
    uint32_t hardecc_cal = 0;
    uint32_t hardecc_get = 0;

    if (((buff == OS_NULL) && (oob == OS_NULL)) || ((buff_len == 0) && (oob_len == 0)))
    {
        return OS_INVAL;
    }

    if ((buff_len > nand->cfg.info.page_size) || (oob_len > nand->cfg.oob_len))
    {
        LOG_E(DRV_EXT_TAG, "nandflash read size out of range!");
        return OS_FAILURE;
    }

    if (buff != OS_NULL)
    {
        if (nand->hardecc_flag)
        {
            nand->ops->control_hardecc(nand, OS_TRUE);
        }

        if (buff_len == nand->cfg.info.page_size)
        {
            ret = nand->ops->read_page(nand, page_addr, buff, 1);
        }
        else
        {
            ret = nand->ops->read_page(nand, page_addr, nand->page_buff, 1);
            memcpy(buff, nand->page_buff, buff_len);
        }

        if (nand->hardecc_flag)
        {
            nand->ops->get_hardecc(nand, &hardecc_cal);
            nand->ops->control_hardecc(nand, OS_FALSE);
        }

        if (ret != OS_SUCCESS)
        {
            LOG_E(DRV_EXT_TAG, "nand read page failed!");
            return OS_FAILURE;
        }
    }

    ret = nand->ops->read_spare(nand, page_addr, nand->spare_buff, 1);
    if (ret != OS_SUCCESS)
    {
        LOG_E(DRV_EXT_TAG, "nand read spare failed!");
        return OS_FAILURE;
    }

    for (int i = 0; i < nand->cfg.info.spare_size; i++)
    {
        if ((i >= nand->cfg.info.badflag_info.inpage_offset) &&
            (i < nand->cfg.info.badflag_info.inpage_offset + nand->cfg.info.badflag_info.length))
        {
        }
        else if ((i >= nand->cfg.info.hardecc_info.inpage_offset) &&
                 (i < nand->cfg.info.hardecc_info.inpage_offset + nand->cfg.info.hardecc_info.length))
        {
            hardecc_get = (uint32_t) * ((uint32_t *)(nand->spare_buff + i));
            i += nand->cfg.info.hardecc_info.length;
        }
        else
        {
            if (oob != OS_NULL)
            {
                *oob++ = *(nand->spare_buff + i);
            }
        }
    }

    if (buff && (hardecc_cal != hardecc_get) && nand->hardecc_flag)
    {
        LOG_E(DRV_EXT_TAG, "nand read page data hardecc err %p", page_addr);
        return OS_FAILURE;
    }

#ifdef NAND_FLASH_DEBUG
    os_kprintf("read page_addr %d \r\n", page_addr);

    if (buff && buff_len)
    {
        os_kprintf("read buff %p len %d \r\n", buff, buff_len);
        hex_dump((uint8_t *)buff, buff_len);
    }

    if (oob && oob_len)
    {
#if 0
        os_kprintf("oob %p len %d \r\n", oob, oob_len);
        hex_dump((uint8_t *)oob, oob_len);
#endif
        os_kprintf("read oob %p len %d \r\n", nand->spare_buff, nand->cfg.info.spare_size);
        hex_dump((uint8_t *)nand->spare_buff, nand->cfg.info.spare_size);
    }
#endif

    return ret;
}
os_err_t os_nand_write(os_nand_device_t *nand,
                       uint32_t       page_addr,
                       const uint8_t *buff,
                       uint32_t       buff_len,
                       const uint8_t *oob,
                       uint32_t       oob_len)
{
    os_err_t    ret;
    uint32_t hardecc_cal = 0;

    if (((buff == OS_NULL) && (oob == OS_NULL)) || ((buff_len == 0) && (oob_len == 0)))
    {
        return OS_INVAL;
    }

    if ((buff_len > nand->cfg.info.page_size) || (oob_len > nand->cfg.oob_len))
    {
        LOG_E(DRV_EXT_TAG, "nandflash write size out of range");
        return OS_FAILURE;
    }

    if (buff != OS_NULL)
    {
        if (nand->hardecc_flag)
        {
            nand->ops->control_hardecc(nand, OS_TRUE);
        }

        if (buff_len == nand->cfg.info.page_size)
        {
            ret = nand->ops->write_page(nand, page_addr, buff, 1);
        }
        else
        {
            memset(nand->page_buff, 0xFF, nand->cfg.info.page_size);
            memcpy(nand->page_buff, buff, buff_len);
            ret = nand->ops->write_page(nand, page_addr, nand->page_buff, 1);
        }

        if (nand->hardecc_flag)
        {
            nand->ops->get_hardecc(nand, &hardecc_cal);
            nand->ops->control_hardecc(nand, OS_FALSE);
        }

        if (ret != OS_SUCCESS)
        {
            LOG_E(DRV_EXT_TAG, "nand write page failed!");
            return OS_FAILURE;
        }
    }

    if (oob != OS_NULL)
    {
        memset(nand->spare_buff, 0xFF, nand->cfg.info.spare_size);
        for (int i = 0; i < nand->cfg.info.spare_size; i++)
        {
            if ((i >= nand->cfg.info.badflag_info.inpage_offset) &&
                (i < nand->cfg.info.badflag_info.inpage_offset + nand->cfg.info.badflag_info.length))
            {
                *(nand->spare_buff + i) = 0xFF;
            }
            else if ((i >= nand->cfg.info.hardecc_info.inpage_offset) &&
                     (i < nand->cfg.info.hardecc_info.inpage_offset + nand->cfg.info.hardecc_info.length))
            {
                *(uint32_t *)(nand->spare_buff + i) = hardecc_cal;
                i += nand->cfg.info.hardecc_info.length;
            }
            else
            {
                *(nand->spare_buff + i) = *oob++;
            }
        }

        ret = nand->ops->write_spare(nand, page_addr, nand->spare_buff, 1);
        if (ret != OS_SUCCESS)
        {
            LOG_E(DRV_EXT_TAG, "nand write spare failed!");
            return OS_FAILURE;
        }
    }

#ifdef NAND_FLASH_DEBUG
    os_kprintf("write page_addr %d \r\n", page_addr);

    if (buff && buff_len)
    {
        os_kprintf("write buff %p len %d \r\n", buff, buff_len);
        hex_dump((uint8_t *)buff, buff_len);
    }

    if (oob && oob_len)
    {
#if 0
        os_kprintf("oob %p len %d \r\n", oob, oob_len);
        hex_dump((uint8_t *)oob, oob_len);
#endif
        os_kprintf("write oob %p len %d \r\n", nand->spare_buff, nand->cfg.info.spare_size);
        hex_dump((uint8_t *)nand->spare_buff, nand->cfg.info.spare_size);
    }
#endif

    return ret;
}
