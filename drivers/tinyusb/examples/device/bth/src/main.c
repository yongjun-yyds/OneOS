/**
 ***********************************************************************************************************************
 * Copyright (c) 2022, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        main.c
 *
 * @brief       This file provides functions for sn configuration.
 *
 * @revision
 * Date         Author          Notes
 * 2022-06-16   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "bsp/board.h"
#include "tusb.h"

#include <os_task.h>


/*------------- MAIN -------------*/
int tusb_example_main(void)
{
  // wait for 'ble_hci_trans_cfg_ll' to complete.
  // 'tud_bt_hci_cmd_cb' shouldn't be used until 'ble_hci_usb_rx_cmd_ll_cb' has been inited.
  os_task_msleep(10);

  board_init();

  tusb_init();

  while (1)
  {
    tud_task(); // tinyusb device task
  }

  return 0;
}

//--------------------------------------------------------------------+
// Device callbacks
//--------------------------------------------------------------------+

// Invoked when device is mounted
void tud_mount_cb(void)
{
  ;
}

// Invoked when device is unmounted
void tud_umount_cb(void)
{
  ;
}

// Invoked when usb bus is suspended
// remote_wakeup_en : if host allow us  to perform remote wakeup
// Within 7ms, device must draw an average of current less than 2.5 mA from bus
void tud_suspend_cb(bool remote_wakeup_en)
{
  (void) remote_wakeup_en;
}

// Invoked when usb bus is resumed
void tud_resume_cb(void)
{
  ;
}
