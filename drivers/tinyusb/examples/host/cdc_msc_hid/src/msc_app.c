/* 
 * The MIT License (MIT)
 *
 * Copyright (c) 2019 Ha Thach (tinyusb.org)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

#include "tusb.h"

#if CFG_TUH_MSC

/* block device */

#include <block/block_device.h>

static CFG_TUSB_MEM_SECTION uint8_t tusb_msc_wrbuf[512];

static os_blk_device_t tusb_msc_host;
static os_semaphore_id  tusb_msc_sem_start;
static os_semaphore_id  tusb_msc_sem_end;

static uint32_t tusb_msc_read_block_addr;
static uint8_t *tusb_msc_read_buff;

static uint32_t tusb_msc_write_block_addr;
static uint8_t *tusb_msc_write_buff;

//--------------------------------------------------------------------+
// MACRO TYPEDEF CONSTANT ENUM DECLARATION
//--------------------------------------------------------------------+
static CFG_TUSB_MEM_SECTION scsi_inquiry_resp_t inquiry_resp;
static uint8_t tu_dev_addr = 0xff;

bool inquiry_complete_cb(uint8_t dev_addr, msc_cbw_t const* cbw, msc_csw_t const* csw)
{
  if (csw->status != 0)
  {
    printf("Inquiry failed\r\n");
    return false;
  }

  // Print out Vendor ID, Product ID and Rev
  printf("%.8s %.16s rev %.4s\r\n", inquiry_resp.vendor_id, inquiry_resp.product_id, inquiry_resp.product_rev);

  // Get capacity of device
  uint32_t const block_count = tuh_msc_get_block_count(dev_addr, cbw->lun);
  uint32_t const block_size = tuh_msc_get_block_size(dev_addr, cbw->lun);

  tusb_msc_host.geometry.block_size = block_size;
  tusb_msc_host.geometry.capacity   = block_size * block_count;

  printf("Disk Size: %lu MB\r\n", block_count / ((1024*1024)/block_size));
  printf("Block Count = %lu, Block Size: %lu\r\n", block_count, block_size);

  if (block_size != 512)
  {
    os_kprintf("only support block_size 512, not support %d\r\n", block_size);
  }

  return true;
}

//------------- IMPLEMENTATION -------------//
OS_USED void tuh_msc_mount_cb(uint8_t dev_addr)
{
  tu_dev_addr = dev_addr;

  printf("A MassStorage device is mounted %d\r\n", dev_addr);

  uint8_t const lun = 0;
  tuh_msc_inquiry(dev_addr, lun, &inquiry_resp, inquiry_complete_cb);
//
//  //------------- file system (only 1 LUN support) -------------//
//  uint8_t phy_disk = dev_addr-1;
//  disk_initialize(phy_disk);
//
//  if ( disk_is_ready(phy_disk) )
//  {
//    if ( f_mount(phy_disk, &fatfs[phy_disk]) != FR_OK )
//    {
//      puts("mount failed");
//      return;
//    }
//
//    f_chdrive(phy_disk); // change to newly mounted drive
//    f_chdir("/"); // root as current dir
//
//    cli_init();
//  }
}

OS_USED void tuh_msc_umount_cb(uint8_t dev_addr)
{
  (void) dev_addr;
  tu_dev_addr = 0xff;
  printf("A MassStorage device is unmounted %d\r\n", dev_addr);

//  uint8_t phy_disk = dev_addr-1;
//
//  f_mount(phy_disk, NULL); // unmount disk
//  disk_deinitialize(phy_disk);
//
//  if ( phy_disk == f_get_current_drive() )
//  { // active drive is unplugged --> change to other drive
//    for(uint8_t i=0; i<CFG_TUH_DEVICE_MAX; i++)
//    {
//      if ( disk_is_ready(i) )
//      {
//        f_chdrive(i);
//        cli_init(); // refractor, rename
//      }
//    }
//  }
}

static bool tuh_msc_read_complete_cb(uint8_t dev_addr, msc_cbw_t const* cbw, msc_csw_t const* csw)
{
    memcpy(tusb_msc_read_buff, tusb_msc_wrbuf, 512);
    tusb_msc_read_buff = OS_NULL;
    os_semaphore_post(tusb_msc_sem_end);
    return true;
}

static bool tuh_msc_write_complete_cb(uint8_t dev_addr, msc_cbw_t const* cbw, msc_csw_t const* csw)
{
    tusb_msc_write_buff = OS_NULL;
    os_semaphore_post(tusb_msc_sem_end);
    return true;
}

void msc_task(void)
{
    if (tu_dev_addr == 0xff)
    {
        //os_kprintf("1");
        return;
    }

    if (!tuh_msc_mounted(tu_dev_addr))
    {
        //os_kprintf("2");
        return;
    }

    if (!tuh_msc_ready(tu_dev_addr))
    {
        //os_kprintf("3");
        return;
    }

    if (tusb_msc_read_buff != OS_NULL)
    {
        tuh_msc_read10(tu_dev_addr, 0, tusb_msc_wrbuf, tusb_msc_read_block_addr, 1, tuh_msc_read_complete_cb);
    }
    else if (tusb_msc_write_buff != OS_NULL)
    {
        memcpy(tusb_msc_wrbuf, tusb_msc_write_buff, 512);
        tuh_msc_write10(tu_dev_addr, 0, tusb_msc_wrbuf, tusb_msc_write_block_addr, 1, tuh_msc_write_complete_cb);
    }
}

static int tusb_host_msc_blk_read_block(os_blk_device_t *blk, uint32_t block_addr, uint8_t *buff, uint32_t block_nr)
{
    int i;

    for (i = 0; i < block_nr; i++)
    {
        os_semaphore_wait(tusb_msc_sem_start, OS_WAIT_FOREVER);

        tusb_msc_read_block_addr = block_addr;
        tusb_msc_read_buff       = buff;
        
        os_semaphore_wait(tusb_msc_sem_end, OS_WAIT_FOREVER);
        os_semaphore_post(tusb_msc_sem_start);

        block_addr++;
        buff += 512;
    }

    return OS_SUCCESS;
}

static int tusb_host_msc_blk_write_block(os_blk_device_t *blk, uint32_t block_addr, const uint8_t *buff, uint32_t block_nr)
{
    int i;

    for (i = 0; i < block_nr; i++)
    {
        os_semaphore_wait(tusb_msc_sem_start, OS_WAIT_FOREVER);

        tusb_msc_write_block_addr = block_addr;
        tusb_msc_write_buff       = (uint8_t *)buff;
        
        os_semaphore_wait(tusb_msc_sem_end, OS_WAIT_FOREVER);
        os_semaphore_post(tusb_msc_sem_start);

        block_addr++;
        buff += 512;
    }

    return OS_SUCCESS;
}

const static struct os_blk_ops tusb_host_msc_blk_ops = {
    .read_block   = tusb_host_msc_blk_read_block,
    .write_block  = tusb_host_msc_blk_write_block,
};

static os_err_t tusb_host_msc_init(void)
{
    tusb_msc_sem_start = os_semaphore_create(OS_NULL, OS_NULL, 1, 1);
    tusb_msc_sem_end   = os_semaphore_create(OS_NULL, OS_NULL, 0, 1);

    tusb_msc_host.geometry.block_size = 512;
    tusb_msc_host.geometry.capacity   = 2 * 1024 * 1024 * 1024ULL;

    tusb_msc_host.blk_ops = &tusb_host_msc_blk_ops;
    return block_device_register(&tusb_msc_host, "msc0");
}

OS_INIT_CALL(tusb_host_msc_init, OS_INIT_LEVEL_COMPONENT, OS_INIT_SUBLEVEL_LOW);

#endif
