/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        es7210.c
 *
 * @brief       This file implements audio driver for es7210.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <device.h>
#include <os_types.h>
#include <audio.h>
#include <i2s.h>

#include "es7210.h"

#define DBG_EXT_TAG "ES7210"

struct es7210_reg_list {
    unsigned char reg_addr;
    unsigned char reg_val;
};
static const struct es7210_reg_list es7210_power_up[] = {
    { 0x00, 0x32 },
    { 0x0D, 0x09 },
    { 0x09, 0x30 },
    { 0x0A, 0x30 },
    { 0x23, 0x2a },
    { 0x22, 0x0a },
    { 0x21, 0x2a },
    { 0x20, 0x0a },
    { 0x08, 0x14 },
    { 0x11, 0x60 },
    { 0x12, 0x00 },
    { 0x40, 0xC3 },
    { 0x41, 0x70 },
    { 0x42, 0x70 },
    { 0x43, 0x1A },
    { 0x44, 0x1A },
    { 0x45, 0x1A },
    { 0x46, 0x1A },
    { 0x47, 0x08 },
    { 0x48, 0x08 },
    { 0x49, 0x08 },
    { 0x4A, 0x08 },
    { 0x07, 0x20 },
    { 0x02, 0xC1 },
    { 0x06, 0x00 },
    { 0x4B, 0x0F },
    { 0x4C, 0x0F },
    { 0x00, 0x71 },
    { 0x00, 0x41 },
};

static const struct es7210_reg_list es7210_power_down[] = {
    { 0x06, 0x00 },
    { 0x4b, 0xff },
    { 0x4c, 0xff },
    { 0x0b, 0xd0 },
    { 0x40, 0x80 },
    { 0x01, 0x7f },
    { 0x06, 0x07 },
};

static int32_t es7210_reg_write(os_device_t *dev, uint8_t reg_addr, uint8_t val)
{
    os_err_t          ret;
    struct os_i2c_msg msgs[1] = {0};
    uint8_t           buff[2] = {0};

    OS_ASSERT(dev != OS_NULL);

    buff[0] = reg_addr;
    buff[1] = val;

    msgs[0].addr  = BSP_ES7210_I2C_ADDR;
    msgs[0].flags = OS_I2C_WR;
    msgs[0].buf   = buff;
    msgs[0].len   = 2;

    ret = os_i2c_transfer((struct os_i2c_bus_device *)dev, msgs, 1);
    if (ret == 1)
    {
        ret = OS_SUCCESS;
    }
    else
    {
        ret = OS_FAILURE;
    }

    return ret;
}

static uint32_t es7210_reg_read(os_device_t *dev, uint8_t reg_addr, uint8_t *val)
{
    struct os_i2c_msg msg[2] = {0};
    uint32_t          ret = 0;
    
    OS_ASSERT(dev != OS_NULL);

    msg[0].addr  = BSP_ES7210_I2C_ADDR;
    msg[0].flags = OS_I2C_WR;
    msg[0].len   = 1;
    msg[0].buf   = &reg_addr;

    msg[1].addr  = BSP_ES7210_I2C_ADDR;
    msg[1].flags = OS_I2C_RD;
    msg[1].len   = 1;
    msg[1].buf   = val;

    ret = os_i2c_transfer((struct os_i2c_bus_device *)dev, msg, 2);
    if (ret == 1)
    {
        ret = OS_SUCCESS;
    }
    else
    {
        ret = OS_FAILURE;
    }

    return ret;
}

static inline int32_t reg_set_bit(os_device_t *dev, uint8_t reg, uint8_t ops, uint8_t mask)
{
    uint8_t val = 0;
    int ret = es7210_reg_read(dev, reg, &val);

    if (ret != 0) {
        return -1;
    }

    val |= mask << ops;

    return es7210_reg_write(dev, reg, val);
}

static inline int32_t reg_clear_bit(os_device_t *dev, uint8_t reg, uint8_t ops, uint8_t mask)
{
    uint8_t val = 0;
    int ret = es7210_reg_read(dev, reg, &val);

    if (ret != 0) {
        return -1;
    }

    val &= ~(mask << ops);

    return es7210_reg_write(dev, reg, val);
}

static inline int32_t es7210_update_bits(os_device_t *dev, unsigned char reg, unsigned char mask, unsigned char value)
{
    uint8_t val_old, val_new;

    es7210_reg_read(dev, reg, &val_old);
    val_new = (val_old & ~mask) | (value & mask);

    if (val_new != val_old) {
        es7210_reg_write(dev, reg, val_new);
    }

    return 0;
}

static inline int32_t es7210_reset(os_device_t *dev)
{
    int32_t ret = 0;
    ret = es7210_reg_write(dev, ES7210_RESET_CTL_REG00, 0xFF);
    os_task_msleep(10);
    return ret;
}

static inline int32_t es7210_i2s_mode(os_device_t *dev, es7210_i2s_mode_t i2s_mode)
{
    int32_t ret = 0;

    if (i2s_mode == ES7210_I2S_MODE_MASTER) {
        ret = es7210_update_bits(dev, ES7210_MODE_CFG_REG08, 0x1, 0x1);
    } else {
        ret = es7210_update_bits(dev, ES7210_MODE_CFG_REG08, 0x1, 0x0);
    }

    return ret;
}

static inline int32_t es7210_i2s_protocol(os_device_t *dev, es7210_protocol_t i2s_protocol)
{
    int32_t ret = 0;

    switch (i2s_protocol) {
        case ES7210_NORMAL_I2S:
            ret |= es7210_update_bits(dev, ES7210_SDP_CFG1_REG11, 0x03, 0x00);
            break;

        case ES7210_NORMAL_LSB_JUSTIFIED:
            ret |= es7210_update_bits(dev, ES7210_SDP_CFG1_REG11, 0x03, 0x01);
            break;

        default:
            ret |= es7210_update_bits(dev, ES7210_SDP_CFG1_REG11, 0x03, 0x00);
            break;
    }

    return ret;
}

static inline int32_t es7210_i2s_data_len(os_device_t *dev, es7210_data_len_t i2s_data_len)
{
    int32_t ret = 0;

    switch (i2s_data_len) {
        case ES7210_16BIT_LENGTH:
            ret = es7210_update_bits(dev, ES7210_SDP_CFG1_REG11, 0xe0, 0x60);
            break;

        case ES7210_18BIT_LENGTH:
            ret = es7210_update_bits(dev, ES7210_SDP_CFG1_REG11, 0xe0, 0x40);
            break;

        case ES7210_20BIT_LENGTH:
            ret = es7210_update_bits(dev, ES7210_SDP_CFG1_REG11, 0xe0, 0x20);
            break;

        case ES7210_24BIT_LENGTH:
            ret = es7210_update_bits(dev, ES7210_SDP_CFG1_REG11, 0xe0, 0x00);
            break;

        case ES7210_32BIT_LENGTH:
            ret = es7210_update_bits(dev, ES7210_SDP_CFG1_REG11, 0xe0, 0x80);
            break;

        default:
            ret = es7210_update_bits(dev, ES7210_SDP_CFG1_REG11, 0xe0, 0x60);
            break;
    }

    return ret;
}

static inline int32_t es7210_clk_divider(os_device_t *dev, es7210_i2s_sclk_freq_t i2s_sclk_freq, es7210_mclk_freq_t i2s_mclk_freq)
{
    int32_t ret = 0;
    int32_t div = 0;

    div = i2s_mclk_freq / i2s_sclk_freq;
    ret = es7210_reg_write(dev, ES7210_MST_CLK_CTL_REG03, 0x00U);
    ret = es7210_reg_write(dev, ES7210_MST_LRCDIVH_REG04, (uint8_t)(div >> 8U));
    ret = es7210_reg_write(dev, ES7210_MST_LRCDIVL_REG05, (uint8_t)div);

    return ret;
}

int32_t es7210_mic3_set_mute(os_device_t *dev, bool en)
{
    if (en) {
        return es7210_update_bits(dev, ES7210_ADC34_MUTE_REG14, 0x01, 0x01);
    }

    return es7210_update_bits(dev, ES7210_ADC34_MUTE_REG14, 0x01, 0x00);
}

int32_t es7210_mic4_set_mute(os_device_t *dev, bool en)
{
    if (en) {
        return es7210_update_bits(dev, ES7210_ADC34_MUTE_REG14, 0x02, 0x01);
    }

    return es7210_update_bits(dev, ES7210_ADC34_MUTE_REG14, 0x02, 0x00);
}

int32_t es7210_mic1_set_mute(os_device_t *dev, bool en)
{
    if (en) {
        return es7210_update_bits(dev, ES7210_ADC12_MUTE_REG15, 0x01, 0x01);
    }

    return es7210_update_bits(dev, ES7210_ADC12_MUTE_REG15, 0x01, 0x00);
}

int32_t es7210_mic2_set_mute(os_device_t *dev, bool en)
{
    if (en) {
        return es7210_update_bits(dev, ES7210_ADC12_MUTE_REG15, 0x02, 0x01);
    }

    return es7210_update_bits(dev, ES7210_ADC12_MUTE_REG15, 0x02, 0x00);
}

int32_t es7210_adc1_set_gain(os_device_t *dev, unsigned char gain)
{
    int32_t ret = 0;

    ret = es7210_reg_write(dev, ES7210_ADC1_MAX_GAIN_REG1E, gain);

    return ret;
}

int32_t es7210_adc2_set_gain(os_device_t *dev, unsigned char gain)
{
    int32_t ret = 0;

    ret = es7210_reg_write(dev, ES7210_ADC2_MAX_GAIN_REG1D, gain);

    return ret;
}

int32_t es7210_adc3_set_gain(os_device_t *dev, unsigned char gain)
{
    int32_t ret = 0;

    ret = es7210_reg_write(dev, ES7210_ADC3_MAX_GAIN_REG1C, gain);

    return ret;
}

int32_t es7210_adc4_set_gain(os_device_t *dev, unsigned char gain)
{
    int32_t ret = 0;

    ret = es7210_reg_write(dev, ES7210_ADC4_MAX_GAIN_REG1B, gain);

    return ret;
}

int32_t es7210_mic1_set_gain(os_device_t *dev, unsigned char gain)
{
    if (gain > 14) {
        gain = 14;
    }

    es7210_update_bits(dev, ES7210_MIC1_GAIN_REG43, 0x0F, gain);
    return 0;
}

int32_t es7210_mic2_set_gain(os_device_t *dev, unsigned char gain)
{
    if (gain > 14) {
        gain = 14;
    }

    es7210_update_bits(dev, ES7210_MIC2_GAIN_REG44, 0x0F, gain);
    return 0;
}

int32_t es7210_mic3_set_gain(os_device_t *dev, unsigned char gain)
{
    if (gain > 14) {
        gain = 14;
    }

    es7210_update_bits(dev, ES7210_MIC3_GAIN_REG45, 0x0F, gain);
    return 0;
}

int32_t es7210_mic4_set_gain(os_device_t *dev, unsigned char gain)
{
    if (gain > 14) {
        gain = 14;
    }

    es7210_update_bits(dev, ES7210_MIC4_GAIN_REG46, 0x0F, gain);
    return 0;
}

int32_t es7210_init(os_device_t *dev, es7210_config_t *es7210_config)
{
    int32_t ret;
    uint32_t cnt;
    es7210_reset(dev);

    for (cnt = 0; cnt < sizeof(es7210_power_up) / sizeof(es7210_power_up[0]); cnt++) {
        ret = es7210_reg_write(dev, es7210_power_up[cnt].reg_addr, es7210_power_up[cnt].reg_val);

        if (ret != 0) {
            return -1;
        }
    }

    es7210_i2s_mode(dev, es7210_config->i2s_mode);
    es7210_i2s_protocol(dev, es7210_config->i2s_protocol);

    es7210_i2s_data_len(dev, es7210_config->data_len);

    if (es7210_config->i2s_mode == ES7210_I2S_MODE_MASTER) {
        es7210_clk_divider(dev, es7210_config->i2s_sclk_freq, es7210_config->mclk_freq);
    }

    es7210_adc1_set_gain(dev, 0xC0);
    es7210_mic1_set_gain(dev, 0xC0);
    es7210_mic2_set_mute(dev, true);

    return ret;
}

int32_t es7210_uninit(os_device_t *dev)
{
    int32_t ret;
    uint32_t cnt;

    for (cnt = 0; cnt < sizeof(es7210_power_down) / sizeof(es7210_power_down[0]); cnt++) {
        ret = es7210_reg_write(dev, es7210_power_down[cnt].reg_addr, es7210_power_down[cnt].reg_val);

        if (ret != 0) {
            return -1;
        }
    }

    return ret;
}

void es7210_read_all_register(os_device_t *dev, uint8_t *buf)
{
    for (uint32_t cnt = 0; cnt < 77U; cnt++) {
        es7210_reg_read(dev, cnt, &buf[cnt]);
    }
}

struct es7210_recorder_device
{
    struct os_audio_device    audio;
    struct os_audio_configure read_config;

    es7210_config_t           es7210_config;

    os_device_t *cfg_bus;
    os_device_t *data_bus;
};

static os_err_t audio_es7210_recorder_config(struct os_audio_device *audio, struct os_audio_caps *caps)
{
    os_err_t result = OS_SUCCESS;
    struct es7210_recorder_device *aduio_dev;

    OS_ASSERT(audio != OS_NULL);

    aduio_dev = (struct es7210_recorder_device *)audio->parent.user_data;

    switch (caps->config_type)
    {
        case AUDIO_PARAM_CMD:
        os_device_control(aduio_dev->data_bus, OS_AUDIO_CMD_SET_FRQ, &caps->udata.config.samplerate);
        os_device_control(aduio_dev->data_bus, OS_AUDIO_CMD_SET_CHANNEL, &caps->udata.config.channels);

        aduio_dev->read_config.channels   = caps->udata.config.channels;
        aduio_dev->read_config.samplebits = caps->udata.config.samplebits;
        aduio_dev->read_config.recorde_fifo_size =
            caps->udata.config.samplerate * caps->udata.config.channels * 2 * 20 / 1000;

        LOG_D(DBG_EXT_TAG, "set samplerate %d", aduio_dev->read_config.channels);
            break;
        default:
            result = OS_FAILURE;
            break;
    }

    return result;
}

static os_err_t audio_es7210_recorder_init(struct os_audio_device *audio)
{
    os_err_t result = OS_SUCCESS;
    OS_ASSERT(audio != OS_NULL);

    struct es7210_recorder_device *aduio_dev;
    aduio_dev = (struct es7210_recorder_device *)audio->parent.user_data;

    result = es7210_init(aduio_dev->cfg_bus, &aduio_dev->es7210_config);

    return result;
}

static os_err_t audio_es7210_recorder_deinit(struct os_audio_device *audio)
{
    OS_ASSERT(audio != OS_NULL);

    struct es7210_recorder_device *aduio_dev = (struct es7210_recorder_device *)audio;

    return es7210_uninit(aduio_dev->cfg_bus);
}

static os_err_t audio_es7210_recorder_start(struct os_audio_device *audio)
{
    struct es7210_recorder_device *aduio_dev;

    OS_ASSERT(audio != OS_NULL);
    aduio_dev = (struct es7210_recorder_device *)audio->parent.user_data;

    LOG_D(DBG_EXT_TAG, "open sound device");

    os_device_control(aduio_dev->data_bus, OS_AUDIO_CMD_RX_ENABLE, (void *)aduio_dev->read_config.recorde_fifo_size);

    return OS_SUCCESS;
}

static os_err_t audio_es7210_recorder_stop(struct os_audio_device *audio)
{
    struct es7210_recorder_device *aduio_dev;

    OS_ASSERT(audio != OS_NULL);
    aduio_dev = (struct es7210_recorder_device *)audio->parent.user_data;

    os_device_control(aduio_dev->data_bus, OS_AUDIO_CMD_RX_DISABLE, OS_NULL);

    return OS_SUCCESS;
}

os_size_t audio_es7210_receive(struct os_audio_device *audio, void *readBuf, os_size_t size)
{
    struct es7210_recorder_device *aduio_dev;

    OS_ASSERT(audio != OS_NULL);

    aduio_dev = (struct es7210_recorder_device *)audio->parent.user_data;

    return os_device_read_nonblock(aduio_dev->data_bus, 0, readBuf, size);
}

const static struct os_audio_ops es7210_recorder_ops = {
    .getcaps   = OS_NULL,
    .configure = audio_es7210_recorder_config,
    .init      = audio_es7210_recorder_init,
    .deinit    = audio_es7210_recorder_deinit,
    .start     = audio_es7210_recorder_start,
    .stop      = audio_es7210_recorder_stop,
    .transmit  = OS_NULL,
    .receive   = audio_es7210_receive,
};

int os_hw_audio_es7210_recorder_init(void)
{
    struct es7210_recorder_device *es7210_recorder_dev = os_calloc(1, sizeof(struct es7210_recorder_device));

    es7210_recorder_dev->read_config.samplerate = 48000U;
    es7210_recorder_dev->read_config.channels   = 2;
    es7210_recorder_dev->read_config.samplebits = 16;
    es7210_recorder_dev->read_config.recorde_fifo_size =
    es7210_recorder_dev->read_config.samplerate * es7210_recorder_dev->read_config.channels * 2 * 20 / 1000;

    es7210_recorder_dev->es7210_config.i2s_rate = ES7210_I2S_SAMPLE_RATE_48000;
    es7210_recorder_dev->es7210_config.i2s_mode = ES7210_I2S_MODE_SLAVE;
    es7210_recorder_dev->es7210_config.i2s_protocol = ES7210_NORMAL_I2S;
    es7210_recorder_dev->es7210_config.data_len = ES7210_16BIT_LENGTH;
    es7210_recorder_dev->es7210_config.i2s_sclk_freq = ES7210_I2S_SCLK_48FS;
    es7210_recorder_dev->es7210_config.mclk_freq = ES7210_MCLK_256FS;

    es7210_recorder_dev->audio.ops = &es7210_recorder_ops;

    es7210_recorder_dev->cfg_bus = os_device_open_s(BSP_ES7210_I2C_BUS);
    if (es7210_recorder_dev->cfg_bus == OS_NULL)
    {
        LOG_E(DBG_EXT_TAG, "can not find the config device!\r\n");
        return OS_FAILURE;
    }

    es7210_recorder_dev->data_bus = os_device_open_s(BSP_ES7210_DATA_BUS);
    if (es7210_recorder_dev->data_bus == OS_NULL)
    {
        LOG_E(DBG_EXT_TAG, "can not find the data device!\r\n");
        return OS_FAILURE;
    }

    os_audio_recorder_register(&es7210_recorder_dev->audio, "recorder_es7210", es7210_recorder_dev);

    return OS_SUCCESS;
}

#if defined(OS_USING_I2S_FOR_RECORDER)
OS_INIT_CALL(os_hw_audio_es7210_recorder_init, OS_INIT_LEVEL_DEVICE, OS_INIT_SUBLEVEL_LOW);
#endif
