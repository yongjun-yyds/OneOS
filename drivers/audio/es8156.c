/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        es8156.c
 *
 * @brief       This file implements audio driver for es8156.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <device.h>
#include <os_types.h>
#include <audio.h>
#include <i2s.h>

#include "es8156.h"

#define DBG_EXT_TAG "ES8156"

struct es8156_reg_list {
    unsigned char reg_addr;
    unsigned char reg_val;
};

static const struct es8156_reg_list es8156_power_up_cfg[] = {
    { 0x20, 0x2A },
    { 0x21, 0x3C },
    { 0x22, 0x08 },
    { 0x23, 0xCA },
    { 0x0A, 0x01 },
    { 0x0B, 0x01 },
    { 0x11, 0x30 },
    { 0x14, 0xBF },
    { 0x01, 0x21 },
    { 0x02, 0x04 },
    { 0x0D, 0x14 },
    { 0x08, 0x3F },
    { 0x00, 0x02 },
    { 0x00, 0x03 },
    { 0x25, 0x20 },
};

static const struct es8156_reg_list es8156_power_down_cfg[] = {
    { 0x14, 0x00 },
    { 0x19, 0x72 },
    { 0x21, 0x1F },
    { 0x22, 0x02 },
    { 0x25, 0x21 },
    { 0x25, 0x01 },
    { 0x25, 0x87 },
    { 0x08, 0x00 },
};

static int32_t es8156_reg_write(os_device_t *dev, uint8_t reg_addr, uint8_t val)
{
    os_err_t          ret;
    struct os_i2c_msg msgs[1] = {0};
    uint8_t           buff[2] = {0};

    OS_ASSERT(dev != OS_NULL);

    buff[0] = reg_addr;
    buff[1] = val;

    msgs[0].addr  = BSP_ES8156_I2C_ADDR;
    msgs[0].flags = OS_I2C_WR;
    msgs[0].buf   = buff;
    msgs[0].len   = 2;

    ret = os_i2c_transfer((struct os_i2c_bus_device *)dev, msgs, 1);
    if (ret == 1)
    {
        ret = OS_SUCCESS;
    }
    else
    {
        ret = OS_FAILURE;
    }
    
    return ret;
}

static uint32_t es8156_reg_read(os_device_t *dev, uint8_t reg_addr, uint8_t *val)
{
    struct os_i2c_msg msg[2] = {0};
    uint32_t          ret = 0;

    OS_ASSERT(dev != OS_NULL);

    msg[0].addr  = BSP_ES8156_I2C_ADDR;
    msg[0].flags = OS_I2C_WR;
    msg[0].len   = 1;
    msg[0].buf   = &reg_addr;

    msg[1].addr  = BSP_ES8156_I2C_ADDR;
    msg[1].flags = OS_I2C_RD;
    msg[1].len   = 1;
    msg[1].buf   = val;

    ret = os_i2c_transfer((struct os_i2c_bus_device *)dev, msg, 2);
    if (ret == 1)
    {
        ret = OS_SUCCESS;
    }
    else
    {
        ret = OS_FAILURE;
    }

    return ret;
}

static inline int32_t reg_set_bit(os_device_t *dev, uint8_t reg, uint8_t ops, uint8_t mask)
{
    uint8_t val = 0;
    int ret = es8156_reg_read(dev, reg, &val);

    if (ret != 0) {
        return -1;
    }

    val |= mask << ops;

    return es8156_reg_write(dev, reg, val);
}

static inline int32_t reg_clear_bit(os_device_t *dev, uint8_t reg, uint8_t ops, uint8_t mask)
{
    uint8_t val = 0;
    int ret = es8156_reg_read(dev, reg, &val);

    if (ret != 0) {
        return -1;
    }

    val &= ~(mask << ops);

    return es8156_reg_write(dev, reg, val);
}

static inline int32_t es8156_update_bits(os_device_t *dev, unsigned char reg, unsigned char mask, unsigned char value)
{
    uint8_t val_old, val_new;

    es8156_reg_read(dev, reg, &val_old);
    val_new = (val_old & ~mask) | (value & mask);

    if (val_new != val_old) {
        es8156_reg_write(dev, reg, val_new);
    }

    return 0;
}

static inline int32_t es8156_i2s_mode(os_device_t *dev, es8156_i2s_mode_t i2s_mode)
{
    int32_t ret = 0;

    if (i2s_mode == ES8156_I2S_MODE_MASTER) {
        ret = es8156_update_bits(dev, ES8156_MODE_CONFIG_REG02, 0x1, 1U);
    } else {
        ret = es8156_update_bits(dev, ES8156_MODE_CONFIG_REG02, 0x1, 0U);
    }

    return ret;

}

static inline int32_t es8156_i2s_protocol(os_device_t *dev, es8156_protocol_t i2s_protocol)
{
    int32_t ret = 0;

    switch (i2s_protocol) {
        case ES8156_NORMAL_I2S:
            ret = es8156_update_bits(dev, ES8156_SDP_CFG1_REG11, 0x03, 0x00);
            break;

        case ES8156_NORMAL_LSB_JUSTIFIED:
            ret = es8156_update_bits(dev, ES8156_SDP_CFG1_REG11, 0x03, 0x01);
            break;

        default:
            ret = es8156_update_bits(dev, ES8156_SDP_CFG1_REG11, 0x03, 0x00);
            break;
    }

    return ret;
}

static inline int32_t es8156_i2s_data_len(os_device_t *dev, es8156_data_len_t i2s_data_len)
{
    int32_t ret = 0;

    switch (i2s_data_len) {
        case ES8156_16BIT_LENGTH:
            ret = es8156_update_bits(dev, ES8156_SDP_CFG1_REG11, 0x30, 0x30);
            break;

        case ES8156_18BIT_LENGTH:
            ret = es8156_update_bits(dev, ES8156_SDP_CFG1_REG11, 0x70, 0x20);
            break;

        case ES8156_20BIT_LENGTH:
            ret = es8156_update_bits(dev, ES8156_SDP_CFG1_REG11, 0x70, 0x10);
            break;

        case ES8156_24BIT_LENGTH:
            ret = es8156_update_bits(dev, ES8156_SDP_CFG1_REG11, 0x70, 0x00);
            break;

        case ES8156_32BIT_LENGTH:
            ret = es8156_update_bits(dev, ES8156_SDP_CFG1_REG11, 0x70, 0x40);
            break;

        default:
            ret = es8156_update_bits(dev, ES8156_SDP_CFG1_REG11, 0x70, 0x60);
            break;
    }

    return ret;
}

static inline int32_t es8156_clk_divider(os_device_t *dev, es8156_i2s_sclk_freq_t i2s_sclk_freq, es8156_mclk_freq_t i2s_mclk_freq)
{
    int32_t ret = 0;
    ret |= es8156_update_bits(dev, ES8156_M_CLK_CTL_REG05, 0x7f, i2s_sclk_freq);
    ret |= es8156_reg_write(dev, ES8156_M_LRCK_DIVH_REG03, (uint8_t)(i2s_mclk_freq >> 8U));
    ret |= es8156_reg_write(dev, ES8156_M_LRCK_DIVL_REG04, (uint8_t)i2s_mclk_freq);
    return ret;
}

static inline int32_t es8156_set_sample_rate(os_device_t *dev, es8156_i2s_sample_rate_t rate)
{
    int32_t ret = 0;

    switch (rate) {
        case ES8156_I2S_SAMPLE_RATE_48000:
            ret |= es8156_update_bits(dev, ES8156_MODE_CONFIG_REG02, 0x2U, 0U);
            ret |= es8156_update_bits(dev, ES8156_DAC_NS_REG10, 0x7fU, 64U);
            ret |= es8156_update_bits(dev, ES8156_MAIN_CLK_REG01, 0x1fU, 1U);
            ret |= es8156_update_bits(dev, ES8156_MAIN_CLK_REG01, 0xc0U, 0U);
            ret |= es8156_update_bits(dev, ES8156_MISC_CTL1_REG07, 0x70U, 1U);
            break;

        case ES8156_I2S_SAMPLE_RATE_32000:
            ret |= es8156_update_bits(dev, ES8156_MODE_CONFIG_REG02, 0x2U, 0U);
            ret |= es8156_update_bits(dev, ES8156_DAC_NS_REG10, 0x7fU, 64U);
            ret |= es8156_update_bits(dev, ES8156_MAIN_CLK_REG01, 0xffU, 0x43U);
            ret |= es8156_update_bits(dev, ES8156_MISC_CTL1_REG07, 0x70U, 1U);
            break;

        case ES8156_I2S_SAMPLE_RATE_44100:
            ret |= es8156_update_bits(dev, ES8156_MODE_CONFIG_REG02, 0x2U, 0U);
            ret |= es8156_update_bits(dev, ES8156_DAC_NS_REG10, 0x7fU, 64U);
            ret |= es8156_update_bits(dev, ES8156_MAIN_CLK_REG01, 0xffU, 1U);
            ret |= es8156_update_bits(dev, ES8156_MISC_CTL1_REG07, 0x70U, 1U);
            break;

        default:
            break;
    }

    return ret;
}


int32_t es8156_set_left_channel_mute(os_device_t *dev, bool en)
{
    if (en) {
        return es8156_update_bits(dev, ES8156_MUTE_CTL_REG13, 0x04, 0x01);
    }

    return es8156_update_bits(dev, ES8156_MUTE_CTL_REG13, 0x04, 0x00);
}

int32_t es8156_set_right_channel_mute(os_device_t *dev, bool en)
{
    if (en) {
        return es8156_update_bits(dev, ES8156_MUTE_CTL_REG13, 0x02, 0x01);
    }

    return es8156_update_bits(dev, ES8156_MUTE_CTL_REG13, 0x02, 0x00);
}

int32_t es8156_set_software_mute(os_device_t *dev, bool en)
{
    if (en) {
        return es8156_update_bits(dev, ES8156_MUTE_CTL_REG13, 0x10, 0x00);
    }

    return es8156_update_bits(dev, ES8156_MUTE_CTL_REG13, 0x10, 0x10);
}

int32_t es8156_set_volume(os_device_t *dev, unsigned char gain)
{
    return es8156_reg_write(dev, ES8156_VOL_CTL_REG14, gain);
}


void es8156_read_all_register(os_device_t *dev, uint8_t *buf)
{
    for (uint32_t cnt = 0; cnt < 38U; cnt ++) {
        es8156_reg_read(dev, cnt, &buf[cnt]);
    }
}

void es8156_all_data_left_channel(os_device_t *dev)
{
    es8156_reg_write(dev, ES8156_MISC_CTL3_REG18, 0x10);
}

void es8156_all_data_right_channel(os_device_t *dev)
{
    es8156_reg_write(dev, ES8156_MISC_CTL3_REG18, 0x20);
}

int32_t es8156_init(os_device_t *dev, es8156_config_t *es8156_config)
{
    int32_t ret;
    uint32_t cnt;

    for (cnt = 0; cnt < sizeof(es8156_power_up_cfg) / sizeof(es8156_power_up_cfg[0]); cnt++)
    {
        ret = es8156_reg_write(dev, es8156_power_up_cfg[cnt].reg_addr, es8156_power_up_cfg[cnt].reg_val);
        if (ret != 0) 
        {
            return -1;
        }
    }

    ret |= es8156_i2s_mode(dev, es8156_config->i2s_mode);
    ret |= es8156_i2s_protocol(dev, es8156_config->i2s_protocol);
    ret |= es8156_i2s_data_len(dev, es8156_config->data_len);
    ret |= es8156_set_sample_rate(dev, es8156_config->i2s_rate);

    if (es8156_config->i2s_mode == ES8156_I2S_MODE_MASTER) 
    {
        ret |= es8156_clk_divider(dev, es8156_config->i2s_sclk_freq, es8156_config->mclk_freq);
    }

    return ret;
}

int32_t es8156_uninit(os_device_t *dev)
{
    int32_t ret;
    uint32_t cnt;

    for (cnt = 0; cnt < sizeof(es8156_power_down_cfg) / sizeof(es8156_power_down_cfg[0]); cnt++) 
    {
        ret = es8156_reg_write(dev, es8156_power_down_cfg[cnt].reg_addr, es8156_power_down_cfg[cnt].reg_val);
        if (ret != 0) {
            return -1;
        }
    }

    return ret;
}

struct es8156_player_device
{
    struct os_audio_device    audio;
    struct os_audio_configure replay_config;
    uint8_t                   volume;

    es8156_config_t           es8156_config;

    os_device_t              *cfg_bus;
    os_device_t              *data_bus;
};

static os_err_t audio_es8156_data_tx_done(os_device_t *dev, struct os_device_cb_info *info)
{
    if (dev->user_data != OS_NULL)
    {
        struct es8156_player_device *es8156_player_dev = dev->user_data;

        struct os_device_cb_info *dev_info = os_list_first_entry(&es8156_player_dev->audio.parent.cb_heads[OS_DEVICE_CB_TYPE_TX],
                                                                 struct os_device_cb_info,
                                                                 list);

        dev_info->data = info->data;

        os_device_send_notify(&es8156_player_dev->audio.parent);

        return OS_SUCCESS;
    }

    return OS_ENOSYS;
}

static os_err_t audio_es8156_player_init(struct os_audio_device *audio)
{
    os_err_t result = OS_SUCCESS;
    OS_ASSERT(audio != OS_NULL);

    struct es8156_player_device *aduio_dev;
    aduio_dev = (struct es8156_player_device *)audio->parent.user_data;

    result = es8156_init(aduio_dev->cfg_bus, &aduio_dev->es8156_config);

    return result;
}

static os_err_t audio_es8156_player_deinit(struct os_audio_device *audio)
{
    OS_ASSERT(audio != OS_NULL);

    struct es8156_player_device *aduio_dev = (struct es8156_player_device *)audio;

    return es8156_uninit(aduio_dev->cfg_bus);
}

static os_err_t audio_es8156_player_start(struct os_audio_device *audio)
{
    struct es8156_player_device *aduio_dev;

    OS_ASSERT(audio != OS_NULL);
    aduio_dev = (struct es8156_player_device *)audio->parent.user_data;

    LOG_D(DBG_EXT_TAG, "open sound device");

    os_device_control(aduio_dev->data_bus, OS_AUDIO_CMD_TX_ENABLE, OS_NULL);

    return OS_SUCCESS;
}

static os_err_t audio_es8156_player_stop(struct os_audio_device *audio)
{
    struct es8156_player_device *aduio_dev;

    OS_ASSERT(audio != OS_NULL);
    aduio_dev = (struct es8156_player_device *)audio->parent.user_data;

    os_device_control(aduio_dev->data_bus, OS_AUDIO_CMD_TX_DISABLE, OS_NULL);

    return OS_SUCCESS;
}

os_size_t audio_es8156_transmit(struct os_audio_device *audio, const void *writeBuf, os_size_t size)
{
    struct es8156_player_device *aduio_dev;

    OS_ASSERT(audio != OS_NULL);

    aduio_dev = (struct es8156_player_device *)audio->parent.user_data;

    return os_device_write_nonblock(aduio_dev->data_bus, 0, (uint8_t *)writeBuf, size);
}

static os_err_t audio_es8156_player_config(struct os_audio_device *audio, struct os_audio_caps *caps)
{
    os_err_t result = OS_SUCCESS;
    struct es8156_player_device *aduio_dev;

    OS_ASSERT(audio != OS_NULL);

    aduio_dev = (struct es8156_player_device *)audio->parent.user_data;

    switch (caps->config_type)
    {
        case AUDIO_VOLUME_CMD:
        {
            uint8_t volume = (caps->udata.value > 255) ? 255 : caps->udata.value;
            es8156_set_volume(aduio_dev->cfg_bus,volume);
            aduio_dev->volume = volume;
            LOG_D(DBG_EXT_TAG, "set volume %d", volume);
            break;
        }
        case AUDIO_PARAM_CMD:
        {
            os_device_control(aduio_dev->data_bus, OS_AUDIO_CMD_SET_FRQ, &caps->udata.config.samplerate);
            os_device_control(aduio_dev->data_bus, OS_AUDIO_CMD_SET_CHANNEL, &caps->udata.config.channels);

            aduio_dev->replay_config.samplerate = caps->udata.config.samplerate;
            aduio_dev->replay_config.channels   = caps->udata.config.channels;
            aduio_dev->replay_config.samplebits = caps->udata.config.samplebits;
            LOG_D(DBG_EXT_TAG, "set samplerate %d", aduio_dev->replay_config.samplerate);

            break;
        }
        default:
            result = OS_FAILURE;
            break;
    }

    return result;
}


const static struct os_audio_ops es8156_player_ops = {
    .getcaps   = OS_NULL,
    .configure = audio_es8156_player_config,
    .init      = audio_es8156_player_init,
    .deinit    = audio_es8156_player_deinit,
    .start     = audio_es8156_player_start,
    .stop      = audio_es8156_player_stop,
    .transmit  = audio_es8156_transmit,
    .receive   = OS_NULL,
};

int os_hw_audio_es8156_player_init(void)
{
    struct es8156_player_device *es8156_player_dev = os_calloc(1, sizeof(struct es8156_player_device));

    es8156_player_dev->replay_config.channels   = 2;
    es8156_player_dev->replay_config.samplebits = 16;
    es8156_player_dev->replay_config.samplerate = 44100;
    es8156_player_dev->volume                   = 30;

    es8156_player_dev->es8156_config.i2s_rate = ES8156_I2S_SAMPLE_RATE_48000;
    es8156_player_dev->es8156_config.i2s_mode = ES8156_I2S_MODE_SLAVE;
    es8156_player_dev->es8156_config.i2s_protocol = ES8156_NORMAL_I2S;
    es8156_player_dev->es8156_config.data_len = ES8156_16BIT_LENGTH;
    es8156_player_dev->es8156_config.i2s_sclk_freq = ES8156_I2S_SCLK_48FS;
    es8156_player_dev->es8156_config.mclk_freq = ES8156_MCLK_256FS;

    es8156_player_dev->audio.ops = &es8156_player_ops;

    es8156_player_dev->cfg_bus = os_device_open_s(BSP_ES8156_I2C_BUS);
    if (es8156_player_dev->cfg_bus == OS_NULL)
    {
        LOG_E(DBG_EXT_TAG, "can not find the config device!\r\n");
        return OS_FAILURE;
    }

    es8156_player_dev->data_bus = os_device_open_s(BSP_ES8156_DATA_BUS);
    if (es8156_player_dev->data_bus == OS_NULL)
    {
        LOG_E(DBG_EXT_TAG, "can not find the data device!\r\n");
        return OS_FAILURE;
    }
    
    es8156_player_dev->data_bus->user_data = es8156_player_dev;

    struct os_device_cb_info *info = os_calloc(1, sizeof(struct os_device_cb_info));
    info->type                     = OS_DEVICE_CB_TYPE_TX;
    info->cb                       = audio_es8156_data_tx_done;
    os_device_control(es8156_player_dev->data_bus, OS_DEVICE_CTRL_SET_CB, info);

    os_audio_player_register(&es8156_player_dev->audio, "player_es8156", es8156_player_dev);

    return OS_SUCCESS;
}

#if defined(OS_USING_I2S_FOR_PLAYER)
OS_INIT_CALL(os_hw_audio_es8156_player_init, OS_INIT_LEVEL_DEVICE, OS_INIT_SUBLEVEL_LOW);
#endif
