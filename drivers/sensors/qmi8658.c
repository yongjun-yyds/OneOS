/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        qmi8658.c
 *
 * @brief       This file provides functions for qmi8658.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <stdlib.h>
#include "oneos_config.h"
#include "os_errno.h"
#include "os_memory.h"
#include "device.h"
#include "i2c.h"
#include "sensor.h"

#include "qmi8658.h"

#define M_PI            (3.14159265358979323846f)
#define ONE_G           (9.807f)
#define QFABS(x)        (((x)<0.0f)?(-1.0f*(x)):(x))

struct qmi8658_device
{
    os_device_t *bus;
    uint8_t      id;
    uint8_t      i2c_addr;

    qmi8658_config cfg;
    unsigned short ssvt_a;
    unsigned short ssvt_g;
};

static struct qmi8658_device *gs_qmi8658_dev = OS_NULL;

unsigned char qmi8658_write_reg(struct qmi8658_device *dev, unsigned char reg, unsigned char value)
{
    os_err_t          ret;
    uint8_t        buf[2];
    struct os_i2c_msg msg;

    buf[0] = reg;
    buf[1] = value;

    if (dev->bus->type == OS_DEVICE_TYPE_I2CBUS)
    {
        msg.addr  = dev->i2c_addr;    /* slave address */
        msg.flags = OS_I2C_WR;        /* write flag */
        msg.buf   = buf;              /* Send data pointer */
        msg.len   = 2;

        if (os_i2c_transfer((struct os_i2c_bus_device *)dev->bus, &msg, 1) == 1)
        {
            ret = OS_SUCCESS;
        }
        else
        {
            ret = OS_FAILURE;
        }
    }
 
    return ret;
}

unsigned char qmi8658_write_regs(struct qmi8658_device *dev, unsigned char reg, unsigned char *value, unsigned char len)
{
    os_err_t          ret;
    uint8_t       *buf;
    struct os_i2c_msg msg;

    buf = os_malloc(len+1);
    if(buf == NULL)
    {
        return OS_FAILURE;
    }

    *buf = reg;
    memcpy(buf+1, value, len);

    if (dev->bus->type == OS_DEVICE_TYPE_I2CBUS)
    {
        msg.addr  = dev->i2c_addr;    /* slave address */
        msg.flags = OS_I2C_WR;        /* write flag */
        msg.buf   = buf;              /* Send data pointer */
        msg.len   = len + 1;

        if (os_i2c_transfer((struct os_i2c_bus_device *)dev->bus, &msg, 1) == 1)
        {
            ret = OS_SUCCESS;
        }
        else
        {
            ret = OS_FAILURE;
        }
    }

    os_free(buf);

    return ret;
}

unsigned char qmi8658_read_reg(struct qmi8658_device *dev, unsigned char reg, unsigned char* buf, unsigned short len)
{
    os_err_t          ret;
    struct os_i2c_msg msg[2];

    if (dev->bus->type == OS_DEVICE_TYPE_I2CBUS)
    {
        msg[0].addr  = dev->i2c_addr;    /* slave address */
        msg[0].flags = OS_I2C_WR;        /* write flag */
        msg[0].buf   = &reg;              /* Send data pointer */
        msg[0].len   = 1;

        msg[1].addr  = dev->i2c_addr;    /* slave address */
        msg[1].flags = OS_I2C_RD;        /* read flag */
        msg[1].buf   = buf;              /* receive data pointer */
        msg[1].len   = len;

        if (os_i2c_transfer((struct os_i2c_bus_device *)dev->bus, msg, 2) == 1)
        {
            ret = OS_SUCCESS;
        }
        else
        {
            ret = OS_FAILURE;
        }
    }

    return ret;
}

void qmi8658_delay(unsigned int ms)
{
    os_task_msleep(ms);
}

void qmi8658_config_acc(struct qmi8658_device *dev, enum qmi8658_AccRange range, enum qmi8658_AccOdr odr, enum qmi8658_LpfConfig lpfEnable, enum qmi8658_StConfig stEnable)
{
    unsigned char ctl_dada;

    switch(range)
    {
        case Qmi8658AccRange_2g:
            dev->ssvt_a = (1 << 14);
            break;
        case Qmi8658AccRange_4g:
            dev->ssvt_a = (1 << 13);
            break;
        case Qmi8658AccRange_8g:
            dev->ssvt_a = (1 << 12);
            break;
        case Qmi8658AccRange_16g:
            dev->ssvt_a = (1 << 11);
            break;
        default: 
            range = Qmi8658AccRange_8g;
            dev->ssvt_a = (1 << 12);
    }

    if(stEnable == Qmi8658St_Enable)
    {
        ctl_dada = (unsigned char)range|(unsigned char)odr|0x80;
    }
    else
    {
        ctl_dada = (unsigned char)range|(unsigned char)odr;
    }

    qmi8658_write_reg(dev, Qmi8658Register_Ctrl2, ctl_dada);
    qmi8658_read_reg(dev, Qmi8658Register_Ctrl5, &ctl_dada, 1);
    ctl_dada &= 0xf0;
    if(lpfEnable == Qmi8658Lpf_Enable)
    {
        ctl_dada |= A_LSP_MODE_3;
        ctl_dada |= 0x01;
    }
    else
    {
        ctl_dada &= ~0x01;
    }

    qmi8658_write_reg(dev, Qmi8658Register_Ctrl5,ctl_dada);
}

void qmi8658_config_gyro(struct qmi8658_device *dev, enum qmi8658_GyrRange range, enum qmi8658_GyrOdr odr, enum qmi8658_LpfConfig lpfEnable, enum qmi8658_StConfig stEnable)
{
    unsigned char ctl_dada; 

    switch (range)
    {
        case Qmi8658GyrRange_16dps:
            dev->ssvt_g = 2048;
            break;
        case Qmi8658GyrRange_32dps:
            dev->ssvt_g = 1024;
            break;
        case Qmi8658GyrRange_64dps:
            dev->ssvt_g = 512;
            break;
        case Qmi8658GyrRange_128dps:
            dev->ssvt_g = 256;
            break;
        case Qmi8658GyrRange_256dps:
            dev->ssvt_g = 128;
            break;
        case Qmi8658GyrRange_512dps:
            dev->ssvt_g = 64;
            break;
        case Qmi8658GyrRange_1024dps:
            dev->ssvt_g = 32;
            break;
        case Qmi8658GyrRange_2048dps:
            dev->ssvt_g = 16;
            break;
        default: 
            range = Qmi8658GyrRange_512dps;
            dev->ssvt_g = 64;
            break;
    }

    if(stEnable == Qmi8658St_Enable)
    {
        ctl_dada = (unsigned char)range | (unsigned char)odr | 0x80;
    }
    else
    {
        ctl_dada = (unsigned char)range | (unsigned char)odr;
    }

    qmi8658_write_reg(dev, Qmi8658Register_Ctrl3, ctl_dada);

    qmi8658_read_reg(dev, Qmi8658Register_Ctrl5, &ctl_dada,1);

    ctl_dada &= 0x0f;
    if(lpfEnable == Qmi8658Lpf_Enable)
    {
        ctl_dada |= G_LSP_MODE_3;
        ctl_dada |= 0x10;
    }
    else
    {
        ctl_dada &= ~0x10;
    }
    
    qmi8658_write_reg(dev, Qmi8658Register_Ctrl5,ctl_dada);
}

float _qmi8658_read_temp(struct qmi8658_device *dev)
{
    unsigned char buf[2];
    short temp = 0;
    float temp_f = 0;

    qmi8658_read_reg(dev, Qmi8658Register_Tempearture_L, buf, 2);
    temp = ((short)buf[1] << 8) | buf[0];
    temp_f = (float)temp * 1000.0f / 256.0f;

    return temp_f;
}

void _qmi8658_read_acc(struct qmi8658_device *dev, struct sensor_3_axis *acc)
{
    unsigned char buf_reg[6];
    short         raw_acc_xyz[3];

    qmi8658_read_reg(dev, Qmi8658Register_Ax_L, buf_reg, 6);
    raw_acc_xyz[0] = (short)((unsigned short)(buf_reg[1] << 8) | ( buf_reg[0]));
    raw_acc_xyz[1] = (short)((unsigned short)(buf_reg[3] << 8) | ( buf_reg[2]));
    raw_acc_xyz[2] = (short)((unsigned short)(buf_reg[5] << 8) | ( buf_reg[4]));

    /* mg */
    acc->x = (float)(raw_acc_xyz[0]*1000.0f) / dev->ssvt_a;
    acc->y = (float)(raw_acc_xyz[1]*1000.0f) / dev->ssvt_a;
    acc->z = (float)(raw_acc_xyz[2]*1000.0f) / dev->ssvt_a;
}

void _qmi8658_read_gyro(struct qmi8658_device *dev, struct sensor_3_axis *gyro)
{
    unsigned char buf_reg[6];
    short         raw_gyro_xyz[3];

    qmi8658_read_reg(dev, Qmi8658Register_Gx_L, buf_reg, 6);
    raw_gyro_xyz[0] = (short)((unsigned short)(buf_reg[1] << 8) | ( buf_reg[0]));
    raw_gyro_xyz[1] = (short)((unsigned short)(buf_reg[3] << 8) | ( buf_reg[2]));
    raw_gyro_xyz[2] = (short)((unsigned short)(buf_reg[5] << 8) | ( buf_reg[4]));

    /* dps */
    gyro->x = (float)(raw_gyro_xyz[0]*1000.0f) / dev->ssvt_g;
    gyro->y = (float)(raw_gyro_xyz[1]*1000.0f) / dev->ssvt_g;
    gyro->z = (float)(raw_gyro_xyz[2]*1000.0f) / dev->ssvt_g;
}

void qmi8658_enableSensors(struct qmi8658_device *dev, unsigned char enableFlags)
{
    qmi8658_write_reg(dev, Qmi8658Register_Ctrl7, enableFlags);

    dev->cfg.enSensors = enableFlags & 0x03;

    qmi8658_delay(1);
}

void qmi8658_on_demand_cali(struct qmi8658_device *dev)
{
    qmi8658_write_reg(dev, Qmi8658Register_Reset, 0xb0);
    qmi8658_delay(10);
    qmi8658_write_reg(dev, Qmi8658Register_Ctrl9, (unsigned char)qmi8658_Ctrl9_Cmd_On_Demand_Cali);
    qmi8658_delay(2200);
    qmi8658_write_reg(dev, Qmi8658Register_Ctrl9, (unsigned char)qmi8658_Ctrl9_Cmd_NOP);
    qmi8658_delay(100);
}

void qmi8658_config_reg(struct qmi8658_device *dev, unsigned char low_power)
{
    qmi8658_enableSensors(dev, QMI8658_DISABLE_ALL);
    if(low_power)
    {
        dev->cfg.enSensors = QMI8658_ACC_ENABLE;
        dev->cfg.accRange = Qmi8658AccRange_8g;
        dev->cfg.accOdr = Qmi8658AccOdr_LowPower_21Hz;
        dev->cfg.gyrRange = Qmi8658GyrRange_1024dps;
        dev->cfg.gyrOdr = Qmi8658GyrOdr_250Hz;
    }
    else
    {
        dev->cfg.enSensors = QMI8658_ACCGYR_ENABLE;
        dev->cfg.accRange = Qmi8658AccRange_8g;
        dev->cfg.accOdr = Qmi8658AccOdr_250Hz;
        dev->cfg.gyrRange = Qmi8658GyrRange_1024dps;
        dev->cfg.gyrOdr = Qmi8658GyrOdr_250Hz;
    }

    if(dev->cfg.enSensors & QMI8658_ACC_ENABLE)
    {
        qmi8658_config_acc(dev,dev->cfg.accRange, dev->cfg.accOdr, Qmi8658Lpf_Disable, Qmi8658St_Disable);
    }

    if(dev->cfg.enSensors & QMI8658_GYR_ENABLE)
    {
        qmi8658_config_gyro(dev, dev->cfg.gyrRange, dev->cfg.gyrOdr, Qmi8658Lpf_Disable, Qmi8658St_Disable);
    }
}

unsigned char _qmi8658_get_id(struct qmi8658_device *dev)
{
    unsigned char qmi8658_chip_id = 0x00;
    unsigned char qmi8658_revision_id = 0x00;
    unsigned char qmi8658_slave[2] = {QMI8658_SLAVE_ADDR_L, QMI8658_SLAVE_ADDR_H};
    int retry = 0;
    unsigned char iCount = 0;
    unsigned char firmware_id[3];
    unsigned char uuid[6];
    unsigned int uuid_low, uuid_high;

    retry = 0;
    while((qmi8658_chip_id != 0x05) && (retry++ < 5))
    {
        qmi8658_read_reg(dev, Qmi8658Register_WhoAmI, &qmi8658_chip_id, 1);
    }

    if(qmi8658_chip_id == 0x05)
    {
        qmi8658_on_demand_cali(dev);

        dev->cfg.ctrl8_value = 0xc0;
        /* QMI8658_INT1_ENABLE, QMI8658_INT2_ENABLE */
        qmi8658_write_reg(dev, Qmi8658Register_Ctrl1, 0x60 | QMI8658_INT2_ENABLE | QMI8658_INT1_ENABLE);
        qmi8658_read_reg(dev, Qmi8658Register_Revision, &qmi8658_revision_id, 1);
        qmi8658_read_reg(dev, Qmi8658Register_firmware_id, firmware_id, 3);
        qmi8658_read_reg(dev, Qmi8658Register_uuid, uuid, 6);
        qmi8658_write_reg(dev, Qmi8658Register_Ctrl7, 0x00);
        qmi8658_write_reg(dev, Qmi8658Register_Ctrl8, dev->cfg.ctrl8_value);
    }

    return qmi8658_chip_id;
}

static os_size_t _qmi8658_polling_get_data(os_sensor_t sensor, struct os_sensor_data *data)
{
    struct sensor_3_axis acce;
    struct sensor_3_axis gyro;

    if (sensor->info.type == OS_SENSOR_CLASS_ACCE)
    {
        _qmi8658_read_acc(gs_qmi8658_dev, &acce);

        data->type = OS_SENSOR_CLASS_ACCE;
        data->data.acce.x = acce.x;
        data->data.acce.y = acce.y;
        data->data.acce.z = acce.z;
        data->timestamp = os_sensor_get_ts();
    }
    else if (sensor->info.type == OS_SENSOR_CLASS_GYRO)
    {
        _qmi8658_read_gyro(gs_qmi8658_dev, &gyro);

        data->type = OS_SENSOR_CLASS_GYRO;
        data->data.gyro.x = gyro.x;
        data->data.gyro.y = gyro.y;
        data->data.gyro.z = gyro.z;
        data->timestamp = os_sensor_get_ts();
    }
    else if (sensor->info.type == OS_SENSOR_CLASS_TEMP)
    {
        float temp = _qmi8658_read_temp(gs_qmi8658_dev);

        data->type = OS_SENSOR_CLASS_GYRO;
        data->data.temp = temp;
        data->timestamp = os_sensor_get_ts();
    }

    return 1;
}


static os_size_t qmi8658_fetch_data(struct os_sensor_device *sensor, void *buf, os_size_t len)
{
    OS_ASSERT(buf);

    if (sensor->config.mode == OS_SENSOR_MODE_POLLING)
    {
        return _qmi8658_polling_get_data(sensor, buf);
    }
    else
    {
        return 0;
    }
}

static os_err_t qmi8658_control(struct os_sensor_device *sensor, int cmd, void *args)
{
    os_err_t result = OS_SUCCESS;

    switch (cmd)
    {
    case OS_SENSOR_CTRL_GET_ID:
        *(uint8_t *)args = gs_qmi8658_dev->id;
        break;
    case OS_SENSOR_CTRL_SET_RANGE:
    case OS_SENSOR_CTRL_SET_ODR:
    case OS_SENSOR_CTRL_SET_MODE:
    case OS_SENSOR_CTRL_SET_POWER:
    case OS_SENSOR_CTRL_SELF_TEST:
        result = OS_INVAL;
        break;
    default:
        return OS_FAILURE;
    }
    return result;
}


static struct os_sensor_ops sensor_ops =
{
    qmi8658_fetch_data,
    qmi8658_control,
};

static int qmi8658_init(void)
{
    os_err_t               ret = OS_SUCCESS;
    os_sensor_t            sensor = OS_NULL;
    struct qmi8658_device *qmi8658_dev;

    qmi8658_dev = (struct qmi8658_device*)os_calloc(1, sizeof(struct qmi8658_device));
    if (qmi8658_dev == OS_NULL)
    {
        ret= OS_NOMEM;
        goto error;
    }

    qmi8658_dev->bus = os_device_find(OS_QMI8658_I2C_BUS_NAME);
    if (qmi8658_dev->bus == OS_NULL)
    {
        ret= OS_NODEV;
        goto error;
    }

    if (qmi8658_dev->bus->type == OS_DEVICE_TYPE_I2CBUS)
    {
        qmi8658_dev->i2c_addr = OS_QMI8658_I2C_ADDR;
    }
    else
    {
        ret= OS_NODEV;
        goto error;
    }

    qmi8658_dev->id = _qmi8658_get_id(qmi8658_dev);

    if(qmi8658_dev->id == 0x05)
    {
        qmi8658_config_reg(qmi8658_dev, 0);
        qmi8658_enableSensors(qmi8658_dev, qmi8658_dev->cfg.enSensors);
    }
    else
    {
        os_kprintf("qmi8658_init fail\n");
        ret = OS_FAILURE;
        goto error;
    }

    gs_qmi8658_dev = qmi8658_dev;

#ifdef PKG_USING_QMI8658_ACCE
    /* accelerometer sensor register */
    {
        sensor = os_calloc(1, sizeof(struct os_sensor_device));
        OS_ASSERT(sensor != OS_NULL);

        sensor->info.type       = OS_SENSOR_CLASS_ACCE;
        sensor->info.vendor     = OS_SENSOR_VENDOR_QST;
        sensor->info.model      = "qmi8658_acc";
        sensor->info.unit       = OS_SENSOR_UNIT_MG;
        sensor->info.intf_type  = OS_SENSOR_INTF_I2C;
        sensor->info.range_max  = 16000;
        sensor->info.range_min  = 2000;
        sensor->info.period_min = 5;

        sensor->config.intf.dev_name  = OS_QMI8658_I2C_BUS_NAME;
        sensor->config.intf.type      = OS_SENSOR_INTF_I2C;
        sensor->config.intf.user_data = (void*)OS_QMI8658_I2C_ADDR;
        sensor->config.mode           = OS_SENSOR_MODE_POLLING;
        sensor->config.odr            = 250; /* hz */
        sensor->config.power          = OS_SENSOR_POWER_NORMAL;
        sensor->config.range          = 8000; /* mg */

        sensor->ops = &sensor_ops;

        ret = os_hw_sensor_register(sensor, "qmi8658", OS_NULL);
        OS_ASSERT(ret == OS_SUCCESS);
    }
#endif
#ifdef PKG_USING_QMI8658_GYRO
    /* gyroscope sensor register */
    {
        sensor = os_calloc(1, sizeof(struct os_sensor_device));
        OS_ASSERT(sensor != OS_NULL);

        sensor->info.type       = OS_SENSOR_CLASS_GYRO;
        sensor->info.vendor     = OS_SENSOR_VENDOR_QST;
        sensor->info.model      = "qmi8658_gyro";
        sensor->info.unit       = OS_SENSOR_UNIT_MDPS;
        sensor->info.intf_type  = OS_SENSOR_INTF_I2C;
        sensor->info.range_max  = 2048000;
        sensor->info.range_min  = 16000;
        sensor->info.period_min = 5;

        sensor->config.intf.dev_name  = OS_QMI8658_I2C_BUS_NAME;
        sensor->config.intf.type      = OS_SENSOR_INTF_I2C;
        sensor->config.intf.user_data = (void*)OS_QMI8658_I2C_ADDR;
        sensor->config.mode           = OS_SENSOR_MODE_POLLING;
        sensor->config.odr            = 250; /* hz */
        sensor->config.power          = OS_SENSOR_POWER_NORMAL;
        sensor->config.range          = 1024000; /* mdps */

        sensor->ops = &sensor_ops;

        ret = os_hw_sensor_register(sensor, "qmi8658", OS_NULL);
        OS_ASSERT(ret == OS_SUCCESS);
    }
#endif
#ifdef PKG_USING_QMI8658_TEMP
    /* temperature sensor register */
    {
        sensor = os_calloc(1, sizeof(struct os_sensor_device));
        OS_ASSERT(sensor != OS_NULL);

        sensor->info.type       = OS_SENSOR_CLASS_TEMP;
        sensor->info.vendor     = OS_SENSOR_VENDOR_QST;
        sensor->info.model      = "qmi8658_temp";
        sensor->info.unit       = OS_SENSOR_UNIT_MDCELSIUS;
        sensor->info.intf_type  = OS_SENSOR_INTF_I2C;
        sensor->info.range_max  = 85000;
        sensor->info.range_min  = -40000;
        sensor->info.period_min = 1000 * 1 / 8; /* ms * 1/hz */

        sensor->config.intf.dev_name  = OS_QMI8658_I2C_BUS_NAME;
        sensor->config.intf.type      = OS_SENSOR_INTF_I2C;
        sensor->config.intf.user_data = (void*)OS_QMI8658_I2C_ADDR;

        sensor->ops = &sensor_ops;

        ret = os_hw_sensor_register(sensor, "qmi8658", OS_NULL);
        OS_ASSERT(ret == OS_SUCCESS);
    }
#endif

    return ret;
error:
    os_free(qmi8658_dev);
    return ret;
}

OS_INIT_CALL(qmi8658_init, OS_INIT_LEVEL_DEVICE, OS_INIT_SUBLEVEL_LOW);
