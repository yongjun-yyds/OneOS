/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        ht240s030.h
 *
 * @brief       This file provides ht240s030 marco definition and function declaration.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __HT240S030_H__
#define __HT240S030_H__

#include <oneos_config.h>

#define HT240S030_PWR_PIN OS_HT240S030_PWR_PIN
#define HT240S030_DC_PIN  OS_HT240S030_DC_PIN
#define HT240S030_RES_PIN OS_HT240S030_RES_PIN

#define LCD_RST_H    os_pin_write(HT240S030_RES_PIN, PIN_HIGH)
#define LCD_RST_L    os_pin_write(HT240S030_RES_PIN, PIN_LOW)

#define LCD_DCK_DATA os_pin_write(HT240S030_DC_PIN, PIN_HIGH)
#define LCD_DCK_CMD  os_pin_write(HT240S030_DC_PIN, PIN_LOW)

#define LCD_A_H      os_pin_write(HT240S030_DC_PIN, PIN_HIGH)
#define LCD_A_L      os_pin_write(HT240S030_DC_PIN, PIN_LOW)

#endif /* __HT240S030_H__ */
