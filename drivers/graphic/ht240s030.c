/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        ht240s030.c
 *
 * @brief       This file provides ht240s030 driver functions.
 *
 * @revision
 * Date         Author          Notes
 * 2023-05-27   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include <device.h>
#include <os_memory.h>
#include <os_task.h>
#include <graphic/graphic.h>
#include "drv_gpio.h"
#include "drv_spi.h"

#include "ht240s030.h"

#define DBG_TAG "ht240s030"

#define HT240S030_W 240

struct ht240s030_lcd
{
    os_graphic_t          graphic;
    struct os_spi_device *spi_dev;
};

static struct ht240s030_lcd *ht240s030 = OS_NULL;

static os_err_t ht240s030_write_cmd(const uint8_t cmd)
{
    os_size_t len;

    LCD_DCK_CMD;

    len = os_spi_send(ht240s030->spi_dev, &cmd, 1);

    if (len != 1)
    {
        LOG_I(DBG_TAG, "ht240s030_write_cmd error. %d", len);
        return OS_FAILURE;
    }
    else
    {
        return OS_SUCCESS;
    }
}

static os_err_t ht240s030_write_data(const uint8_t *buf, os_size_t len)
{
    os_err_t ret = 0;

    LCD_DCK_DATA;

    ret = os_spi_send(ht240s030->spi_dev, buf, len);

    if (ret != len)
    {
        LOG_I(DBG_TAG, "ht240s030_write_data error. %d", len);
        return OS_FAILURE;
    }
    else
    {
        return OS_SUCCESS;
    }
}

static void ht240s030_address_set(uint16_t Xstart, uint16_t Xend, uint16_t Ystart, uint16_t Yend)
{
    uint8_t data[4] = {0};

    ht240s030_write_cmd(0x2A);
    data[0] = Xstart >> 8;
    data[1] = Xstart;
    data[2] = Xend >> 8;
    data[3] = Xend;
    ht240s030_write_data(data, 4);

    ht240s030_write_cmd(0x2B);
    data[0] = Ystart >> 8;
    data[1] = Ystart;
    data[2] = Yend >> 8;
    data[3] = Yend;
    ht240s030_write_data(data, 4);

    ht240s030_write_cmd(0x3A);
    data[0] = 0x65;
    ht240s030_write_data(data,1);

    ht240s030_write_cmd(0x2C);
}

void ht240s030_fill_color(uint16_t Xstart, uint16_t Xend, uint16_t Ystart, uint16_t Yend, uint16_t color)
{
    uint8_t buff[HT240S030_W * 2];
    uint32_t i, remain, w, h;

    w = Xend - Xstart + 1;
    h = Yend - Ystart + 1;

    ht240s030_address_set(Xstart, Xend, Ystart, Yend);

    for (i = 0; i < HT240S030_W * 2; i += 2)
    {
        buff[i]     = color >> 8;
        buff[i + 1] = color;
    }

    remain = h * w;

    while(remain > 0)
    {
        if (remain > HT240S030_W)
        {
            ht240s030_write_data(buff, HT240S030_W * 2);
            remain -= HT240S030_W;
        }
        else
        {
            ht240s030_write_data(buff, remain * 2);
            remain = 0;
        }
    }

    ht240s030_write_cmd(0x29);

}

static void ht240s030_switch_on(void)
{
#if OS_HT240S030_PWR_PIN_ACTIVE == 1
    os_pin_write(HT240S030_PWR_PIN, PIN_HIGH);
#else
    os_pin_write(HT240S030_PWR_PIN, PIN_LOW);
#endif
}

static void ht240s030_switch_off(void)
{
#if OS_HT240S030_PWR_PIN_ACTIVE == 1
    os_pin_write(HT240S030_PWR_PIN, PIN_LOW);
#else
    os_pin_write(HT240S030_PWR_PIN, PIN_HIGH);
#endif
}

void ht240s030_init(void)
{
    uint8_t data[20] = {0};
    struct os_spi_configuration cfg;

    os_hw_spi_device_attach(OS_HT240S030_SPI_BUS_NAME, OS_HT240S030_SPI_BUS_NAME "_lcd", OS_HT240S030_SPI_CS_PIN);

    ht240s030->spi_dev = (struct os_spi_device *)os_device_find(OS_HT240S030_SPI_BUS_NAME "_lcd");
    OS_ASSERT(ht240s030->spi_dev);

    /* spi bus configuration */
    {
        cfg.data_width = 8;
        cfg.mode       = OS_SPI_MASTER | OS_SPI_MODE_0 | OS_SPI_MSB;
        cfg.max_hz     = 42 * 1000 * 1000;
        os_spi_configure(ht240s030->spi_dev, &cfg);

        os_spi_take_bus(ht240s030->spi_dev);
        os_spi_release(ht240s030->spi_dev);
        os_spi_release_bus(ht240s030->spi_dev);
    }

    /* gpio initialisation */
    os_pin_mode(HT240S030_DC_PIN, PIN_MODE_OUTPUT);
    os_pin_mode(HT240S030_RES_PIN, PIN_MODE_OUTPUT);
    os_pin_mode(HT240S030_PWR_PIN, PIN_MODE_OUTPUT);

    LCD_A_L;
    os_task_msleep(100);

    LCD_RST_H;
    os_task_msleep(100);

    LCD_RST_L;
    os_task_msleep(100);

    LCD_RST_H;
    os_task_msleep(100);

    ht240s030_write_cmd(0x11);
    os_task_msleep(100);

    /* display and color format setting */
    ht240s030_write_cmd(0x36);
    data[0] = 0xC0;
    ht240s030_write_data(data,1);
    os_task_msleep(20);

    ht240s030_write_cmd(0x3a);
    data[0] = 0x65;
    ht240s030_write_data(data,1);
    os_task_msleep(20);

    /* HT240S030V Frame rate setting */
    ht240s030_write_cmd(0xb2);
    data[0] = 0x0c;
    data[1] = 0x0c;
    data[2] = 0x00;
    data[3] = 0x33;
    data[4] = 0x33;
    ht240s030_write_data(data, 5);
    os_task_msleep(20);

    ht240s030_write_cmd(0xb7);
    data[0] = 0x35;
    ht240s030_write_data(data, 1);
    os_task_msleep(20);

    /* HT240S030V Power setting */
    ht240s030_write_cmd(0xbb);
    data[0] = 0x35;
    ht240s030_write_data(data, 1);
    os_task_msleep(20);

    ht240s030_write_cmd(0xc0);
    data[0] = 0x2c;
    ht240s030_write_data(data, 1);
    os_task_msleep(20);

    ht240s030_write_cmd(0xc2);
    data[0] = 0x01;
    ht240s030_write_data(data, 1);
    os_task_msleep(20);

    ht240s030_write_cmd(0xc3);
    data[0] = 0x0b;
    ht240s030_write_data(data, 1);
    os_task_msleep(20);

    ht240s030_write_cmd(0xc4);
    data[0] = 0x20;
    ht240s030_write_data(data, 1);
    os_task_msleep(20);

    ht240s030_write_cmd(0xc6);
    data[0] = 0x0f;
    ht240s030_write_data(data, 1);
    os_task_msleep(20);

    ht240s030_write_cmd(0xd0);
    data[0] = 0xa4;
    data[1] = 0xa1;
    ht240s030_write_data(data, 2);
    os_task_msleep(20);

    /* HT240S030V gamma setting */
    ht240s030_write_cmd(0xe0);
    data[0] = 0xd0;
    data[1] = 0x00;
    data[2] = 0x02;
    data[3] = 0x07;
    data[4] = 0x0b;
    data[5] = 0x1a;
    data[6] = 0x31;
    data[7] = 0x54;
    data[8] = 0x40;
    data[9] = 0x29;
    data[10] = 0x12;
    data[11] = 0x12;
    data[12] = 0x12;
    data[13] = 0x17;
    ht240s030_write_data(data, 14);
    os_task_msleep(20);

    ht240s030_write_cmd(0xe1);
    data[0] = 0xd0;
    data[1] = 0x00;
    data[2] = 0x02;
    data[3] = 0x07;
    data[4] = 0x05;
    data[5] = 0x25;
    data[6] = 0x2d;
    data[7] = 0x44;
    data[8] = 0x45;
    data[9] = 0x1c;
    data[10] = 0x18;
    data[11] = 0x16;
    data[12] = 0x1c;
    data[13] = 0x1d;
    ht240s030_write_data(data, 14);
    os_task_msleep(20);

    ht240s030_fill_color(0, 240, 0, 320, 0x0000);
}

static void ht240s030_display_on(struct os_device *dev, os_bool_t on_off)
{
    if (on_off)
        ht240s030_switch_on();
    else
        ht240s030_switch_off();
}

static void ht240s030_frame_flush(struct os_device *dev)
{
    os_graphic_t *graphic = (os_graphic_t *)dev;
    OS_ASSERT(graphic->info.framebuffer_curr);

    ht240s030_address_set(0, graphic->info.width, 0, graphic->info.height);
    os_pin_write(HT240S030_DC_PIN, PIN_HIGH);
    os_spi_send(ht240s030->spi_dev, graphic->info.framebuffer_curr, graphic->info.framebuffer_num);
}

static void ht240s030_display_area(struct os_device *dev, os_graphic_area_t *area)
{
    uint16_t   color;
    os_graphic_t *graphic = (os_graphic_t *)dev;

    if (area->buffer)
    {
        ht240s030_address_set(area->x, area->x + area->w - 1, area->y, area->y + area->h - 1);
        os_pin_write(HT240S030_DC_PIN, PIN_HIGH);
        os_spi_send(ht240s030->spi_dev, area->buffer, area->w * area->h * graphic->info.bytes_per_pixel);
    }
    else
    {
        color = OS_COLOR_TO16(area->color);
        ht240s030_fill_color(area->x, area->x + area->w - 1, area->y, area->y + area->h - 1, color);
    }
}

const static struct os_graphic_ops ops = {
    .display_on   = ht240s030_display_on,
    .display_area = ht240s030_display_area,
    .frame_flush  = ht240s030_frame_flush,
};

static int os_hw_lcd_init(void)
{
    os_err_t ret;

    ht240s030 = os_calloc(1, sizeof(struct ht240s030_lcd));
    OS_ASSERT(ht240s030);

    ht240s030_init();

    ht240s030->graphic.ops = &ops;

    ret = os_graphic_register("lcd", &ht240s030->graphic);

    os_kprintf("ht240s030 lcd found.\r\n");

    return ret;
}

OS_INIT_CALL(os_hw_lcd_init, OS_INIT_LEVEL_DEVICE, OS_INIT_SUBLEVEL_HIGH);
