/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        atk_tflcd9341.c
 *
 * @brief       This file provides atk_tflcd9341 driver functions.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include <board.h>
#include <os_memory.h>
#include <graphic/graphic.h>
#include <shell.h>
#include <string.h>
#include <clocksource.h>
#include <dlog.h>
#include "atk_tflcd9341.h"

#define DBG_TAG "LCD"

struct atk_lcd
{
    os_graphic_t graphic;
};

struct atk_lcd_info lcd_dev = {
    .dir     = 0,
    .width   = OS_GRAPHIC_LCD_WIDTH,
    .height  = OS_GRAPHIC_LCD_HEIGHT,
    .wramcmd = 0x2C,
    .setxcmd = 0x2A,
    .setycmd = 0x2B,
};

struct atk_lcd_mem *lcd = (struct atk_lcd_mem *)LCD_BASE;

static void __LCD_WR_REG(uint16_t regval)
{
    os_clocksource_ndelay(1000);
    lcd->reg = regval;
}

static void __LCD_WR_DATA(uint16_t data)
{
    os_clocksource_ndelay(1000);
    lcd->ram = data;
}

static uint16_t __LCD_RD_DATA(void)
{
    uint16_t ram;
    ram = lcd->ram;
    return ram;
}

static void __LCD_WriteReg(uint16_t LCD_Reg, uint16_t LCD_RegValue)
{
    lcd->reg = LCD_Reg;
    lcd->ram = LCD_RegValue;
}

static void __LCD_WriteRAM_Prepare(void)
{
    lcd->reg = lcd_dev.wramcmd;
}

static void lcd_set_cursor(uint16_t Xpos, uint16_t Ypos)
{

    __LCD_WR_REG(lcd_dev.setxcmd);
    __LCD_WR_DATA(Xpos >> 8);
    __LCD_WR_DATA(Xpos & 0xFF);
    __LCD_WR_REG(lcd_dev.setycmd);
    __LCD_WR_DATA(Ypos >> 8);
    __LCD_WR_DATA(Ypos & 0xFF);
}
static void lcd_clear(uint32_t color)
{
    uint32_t index      = 0;
    uint32_t totalpoint = lcd_dev.width;

    totalpoint *= lcd_dev.height;
    lcd_set_cursor(0x00, 0x0000);
    __LCD_WriteRAM_Prepare();
    for (index = 0; index < totalpoint; index++)
    {
        lcd->ram = color;
    }
}

static void lcd_fill_color(uint16_t sx, uint16_t sy, uint16_t ex, uint16_t ey, uint32_t color)
{
    uint16_t i, j;
    uint16_t xlen = 0;
    xlen             = ex - sx + 1;
    for (i = sy; i <= ey; i++)
    {
        lcd_set_cursor(sx, i);
        __LCD_WriteRAM_Prepare();
        for (j = 0; j < xlen; j++)
        {
            lcd->ram = color;
        }
    }
}

static void lcd_read_id(void)
{
    uint8_t id[2];

    __LCD_WR_REG(0xD3);

    /*dummy read*/
    __LCD_RD_DATA();
    __LCD_RD_DATA();

    id[0] = __LCD_RD_DATA();
    id[1] = __LCD_RD_DATA();

    lcd_dev.id = (id[0] << 8) | id[1];
    LOG_I(DBG_TAG, "lcd id:0x%x", lcd_dev.id);
}

static void lcd_scan_direction(uint8_t dir)
{
    uint16_t regval = 0;
    uint16_t dirreg = 0;
    uint16_t temp;

    switch (dir)
    {
    case L2R_U2D:
        regval |= (0 << 7) | (0 << 6) | (0 << 5);
        break;
    case L2R_D2U:
        regval |= (1 << 7) | (0 << 6) | (0 << 5);
        break;
    case R2L_U2D:
        regval |= (0 << 7) | (1 << 6) | (0 << 5);
        break;
    case R2L_D2U:
        regval |= (1 << 7) | (1 << 6) | (0 << 5);
        break;
    case U2D_L2R:
        regval |= (0 << 7) | (0 << 6) | (1 << 5);
        break;
    case U2D_R2L:
        regval |= (0 << 7) | (1 << 6) | (1 << 5);
        break;
    case D2U_L2R:
        regval |= (1 << 7) | (0 << 6) | (1 << 5);
        break;
    case D2U_R2L:
        regval |= (1 << 7) | (1 << 6) | (1 << 5);
        break;
    }
    dirreg = 0x36;
    regval |= 0x08;

    __LCD_WriteReg(dirreg, regval);

    if (regval & 0x20)
    {
        if (lcd_dev.width < lcd_dev.height)
        {
            temp           = lcd_dev.width;
            lcd_dev.width  = lcd_dev.height;
            lcd_dev.height = temp;
        }
    }
    else
    {
        if (lcd_dev.width > lcd_dev.height)
        {
            temp           = lcd_dev.width;
            lcd_dev.width  = lcd_dev.height;
            lcd_dev.height = temp;
        }
    }

    __LCD_WR_REG(lcd_dev.setxcmd);
    __LCD_WR_DATA(0);
    __LCD_WR_DATA(0);
    __LCD_WR_DATA((lcd_dev.width - 1) >> 8);
    __LCD_WR_DATA((lcd_dev.width - 1) & 0xFF);
    __LCD_WR_REG(lcd_dev.setycmd);
    __LCD_WR_DATA(0);
    __LCD_WR_DATA(0);
    __LCD_WR_DATA((lcd_dev.height - 1) >> 8);
    __LCD_WR_DATA((lcd_dev.height - 1) & 0xFF);
}

static void lcd_display_direction(uint8_t dir)
{
    lcd_dev.dir = dir;
    if (dir == 0)
    {
        lcd_dev.width  = 240;
        lcd_dev.height = 320;
    }
    else
    {
        lcd_dev.width  = 320;
        lcd_dev.height = 240;
    }
    lcd_scan_direction(DFT_SCAN_DIR);
}

static void lcd_init(void)
{

    __LCD_WR_REG(0xCF);
    __LCD_WR_DATA(0x00);
    __LCD_WR_DATA(0xC1);
    __LCD_WR_DATA(0x30);
    __LCD_WR_REG(0xED);
    __LCD_WR_DATA(0x64);
    __LCD_WR_DATA(0x03);
    __LCD_WR_DATA(0x12);
    __LCD_WR_DATA(0x81);
    __LCD_WR_REG(0xE8);
    __LCD_WR_DATA(0x85);
    __LCD_WR_DATA(0x10);
    __LCD_WR_DATA(0x7A);
    __LCD_WR_REG(0xCB);
    __LCD_WR_DATA(0x39);
    __LCD_WR_DATA(0x2C);
    __LCD_WR_DATA(0x00);
    __LCD_WR_DATA(0x34);
    __LCD_WR_DATA(0x02);
    __LCD_WR_REG(0xF7);
    __LCD_WR_DATA(0x20);
    __LCD_WR_REG(0xEA);
    __LCD_WR_DATA(0x00);
    __LCD_WR_DATA(0x00);
    /*Power control*/
    __LCD_WR_REG(0xC0);
    /*VRH[5:0]*/
    __LCD_WR_DATA(0x1B);
    __LCD_WR_REG(0xC1);
    __LCD_WR_DATA(0x01);
    __LCD_WR_REG(0xC5);
    __LCD_WR_DATA(0x30);
    __LCD_WR_DATA(0x30);
    __LCD_WR_REG(0xC7);
    __LCD_WR_DATA(0xB7);
    __LCD_WR_REG(0x36);
    __LCD_WR_DATA(0x48);
    __LCD_WR_REG(0x3A);
    __LCD_WR_DATA(0x55);
    __LCD_WR_REG(0xB1);
    __LCD_WR_DATA(0x00);
    __LCD_WR_DATA(0x1A);
    __LCD_WR_REG(0xB6);
    __LCD_WR_DATA(0x0A);
    __LCD_WR_DATA(0xA2);
    __LCD_WR_REG(0xF2);
    __LCD_WR_DATA(0x00);
    __LCD_WR_REG(0x26);
    __LCD_WR_DATA(0x01);
    __LCD_WR_REG(0xE0);
    __LCD_WR_DATA(0x0F);
    __LCD_WR_DATA(0x2A);
    __LCD_WR_DATA(0x28);
    __LCD_WR_DATA(0x08);
    __LCD_WR_DATA(0x0E);
    __LCD_WR_DATA(0x08);
    __LCD_WR_DATA(0x54);
    __LCD_WR_DATA(0xA9);
    __LCD_WR_DATA(0x43);
    __LCD_WR_DATA(0x0A);
    __LCD_WR_DATA(0x0F);
    __LCD_WR_DATA(0x00);
    __LCD_WR_DATA(0x00);
    __LCD_WR_DATA(0x00);
    __LCD_WR_DATA(0x00);
    __LCD_WR_REG(0xE1);
    __LCD_WR_DATA(0x00);
    __LCD_WR_DATA(0x15);
    __LCD_WR_DATA(0x17);
    __LCD_WR_DATA(0x07);
    __LCD_WR_DATA(0x11);
    __LCD_WR_DATA(0x06);
    __LCD_WR_DATA(0x2B);
    __LCD_WR_DATA(0x56);
    __LCD_WR_DATA(0x3C);
    __LCD_WR_DATA(0x05);
    __LCD_WR_DATA(0x10);
    __LCD_WR_DATA(0x0F);
    __LCD_WR_DATA(0x3F);
    __LCD_WR_DATA(0x3F);
    __LCD_WR_DATA(0x0F);
    __LCD_WR_REG(0x2B);
    __LCD_WR_DATA(0x00);
    __LCD_WR_DATA(0x00);
    __LCD_WR_DATA(0x01);
    __LCD_WR_DATA(0x3f);
    __LCD_WR_REG(0x2A);
    __LCD_WR_DATA(0x00);
    __LCD_WR_DATA(0x00);
    __LCD_WR_DATA(0x00);
    __LCD_WR_DATA(0xef);
    __LCD_WR_REG(0x11);

    os_task_msleep(120);
    __LCD_WR_REG(0x29);
    lcd_display_direction(0);

    lcd_clear(0xFFFF);
}

static void lcd_back_light_set(uint8_t on)
{
    os_pin_mode(BSP_ATK9341_BL_PIN, PIN_MODE_OUTPUT);

    if (on)
        os_pin_write(BSP_ATK9341_BL_PIN, 1);
    else
        os_pin_write(BSP_ATK9341_BL_PIN, 0);
}

void lcd_fill_window(uint16_t sx, uint16_t sy, uint16_t ex, uint16_t ey, uint16_t *data)
{
    uint16_t height, width;
    uint16_t i, j;
    width = ex - sx + 1;            /* 得到填充的宽度 */
    height = ey - sy + 1;           /* 高度 */

    for (i = 0; i < height; i++)
    {
        lcd_set_cursor(sx, sy + i); /* 设置光标位置 */
        __LCD_WriteRAM_Prepare();    /* 开始写入GRAM */

        for (j = 0; j < width; j++)
        {
            lcd->ram = data[i * width + j]; /* 写入数据 */
        }
    }
}

static void atk_tflcd9341_display_area(struct os_device *dev, os_graphic_area_t *area)
{
    uint16_t color;

    if (area->buffer)
    {
        lcd_fill_window(area->x, area->y, area->x + area->w - 1, area->y + area->h - 1, (uint16_t *)area->buffer);
    }
    else
    {
        color = OS_COLOR_TO16(area->color);
        lcd_fill_color(area->x, area->y, area->x + area->w - 1, area->y + area->h - 1, color);
    }
}

const static struct os_graphic_ops ops = {
    .display_on   = OS_NULL,
    .display_area = atk_tflcd9341_display_area,
    .frame_flush  = OS_NULL,
};

static void __os_hw_lcd_init(void)
{
    lcd_back_light_set(1);

    lcd_read_id();

    lcd_init();
}

static int os_hw_lcd_init(void)
{
    struct atk_lcd *lcd;

    lcd = os_calloc(1, sizeof(struct atk_lcd));

    OS_ASSERT(lcd);

    __os_hw_lcd_init();

    lcd->graphic.ops = &ops;

    os_graphic_register("lcd", &lcd->graphic);

    LOG_I(DBG_TAG, "atk tflcd9341 found.");

    return OS_SUCCESS;
}

OS_INIT_CALL(os_hw_lcd_init, OS_INIT_LEVEL_DEVICE, OS_INIT_SUBLEVEL_LOW);
