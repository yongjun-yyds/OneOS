/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        st7789vw.c
 *
 * @brief       This file provides st7789vw driver functions.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <device.h>
#include <os_memory.h>
#include <os_task.h>
#include <graphic/graphic.h>
#include "st7789vw.h"
#include "drv_gpio.h"
#include "drv_spi.h"

#define DBG_TAG "LCD"

#include <dlog.h>

#define ST7789_PWR_PIN OS_ST7789VW_PWR_PIN
#define ST7789_DC_PIN  OS_ST7789VW_DC_PIN
#define ST7789_RES_PIN OS_ST7789VW_RES_PIN

struct st7789_lcd
{
    os_graphic_t          graphic;
    struct os_spi_device *spi_dev;
};

static struct st7789_lcd *st7789 = OS_NULL;

static os_err_t st7789_write_cmd(const uint8_t cmd)
{
    os_size_t len;

    os_pin_write(ST7789_DC_PIN, PIN_LOW);

    len = os_spi_send(st7789->spi_dev, &cmd, 1);

    if (len != 1)
    {
        LOG_I(DBG_TAG, "st7789_write_cmd error. %d", len);
        return OS_FAILURE;
    }
    else
    {
        return OS_SUCCESS;
    }
}

static os_err_t st7789_write_data(const uint8_t data)
{
    os_size_t len;

    os_pin_write(ST7789_DC_PIN, PIN_HIGH);

    len = os_spi_send(st7789->spi_dev, &data, 1);

    if (len != 1)
    {
        LOG_I(DBG_TAG, "st7789_write_data error. %d", len);
        return OS_FAILURE;
    }
    else
    {
        return OS_SUCCESS;
    }
}

static os_err_t st7789_write_half_word(const uint16_t da)
{
    os_size_t len;
    char      data[2] = {0};

    data[0] = da >> 8;
    data[1] = da;

    os_pin_write(ST7789_DC_PIN, PIN_HIGH);
    len = os_spi_send(st7789->spi_dev, data, 2);
    if (len != 2)
    {
        LOG_I(DBG_TAG, "st7789_write_half_word error. %d", len);
        return OS_FAILURE;
    }
    else
    {
        return OS_SUCCESS;
    }
}

static void st7789_switch_on(void)
{
#if OS_ST7789VW_PWR_PIN_ACTIVE == 1
    os_pin_write(ST7789_PWR_PIN, PIN_HIGH);
#else
    os_pin_write(ST7789_PWR_PIN, PIN_LOW);
#endif
}

static void st7789_switch_off(void)
{
#if OS_ST7789VW_PWR_PIN_ACTIVE == 1
    os_pin_write(ST7789_PWR_PIN, PIN_LOW);
#else
    os_pin_write(ST7789_PWR_PIN, PIN_HIGH);
#endif
}

/* lcd enter the minimum power consumption mode and backlight off. */
void st7789_enter_sleep(void)
{
    st7789_switch_off();
    os_task_msleep(5);
    st7789_write_cmd(0x10);
}
/* lcd turn off sleep mode and backlight on. */
void st7789_exit_sleep(void)
{
    st7789_switch_on();
    os_task_msleep(5);
    st7789_write_cmd(0x11);
    os_task_msleep(120);
}

/**
 ***********************************************************************************************************************
 * @brief           Set drawing area
 *
 * @param[in]       x1       start of x position
 * @param[in]       y1       start of y position
 * @param[in]       x2       end of x position
 * @param[in]       y2       end of y position
 *
 * @return          void
 ***********************************************************************************************************************
 */
void st7789_address_set(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2)
{
    st7789_write_cmd(0x2a);
    st7789_write_data(x1 >> 8);
    st7789_write_data(x1);
    st7789_write_data(x2 >> 8);
    st7789_write_data(x2);

    st7789_write_cmd(0x2b);
    st7789_write_data(y1 >> 8);
    st7789_write_data(y1);
    st7789_write_data(y2 >> 8);
    st7789_write_data(y2);

    st7789_write_cmd(0x2C);
}

static void st7789_fill_color(uint16_t x, uint16_t y, uint16_t w, uint16_t h, uint16_t color)
{
    int32_t  i, j;
    uint8_t  data[2] = {0};
    uint8_t *buf     = OS_NULL;
    int32_t  size, num, remain;

    data[0] = color >> 8;
    data[1] = color;
    size    = w * h * 2;

    st7789_address_set(x, y, x + w - 1, y + h - 1);

    buf = (uint8_t *)os_calloc(1, ST7789_SEND_BUF_SIZE);
    if (buf)
    {
        /* fast fill */
        remain = size;
        while (remain > 0)
        {
            num    = remain > ST7789_SEND_BUF_SIZE ? ST7789_SEND_BUF_SIZE : remain;
            remain = remain - num;

            for (i = 0; i < num; i += 2)
            {
                buf[i]     = data[0];
                buf[i + 1] = data[1];
            }

            os_pin_write(ST7789_DC_PIN, PIN_HIGH);
            os_spi_send(st7789->spi_dev, buf, num);
        }
        os_free(buf);
    }
    else
    {
        for (i = 0; i < h; i++)
        {
            for (j = 0; j < w; j++)
                st7789_write_half_word(color);
        }
    }
}

static void st7789_gpio_init(void)
{
    os_pin_mode(ST7789_DC_PIN, PIN_MODE_OUTPUT);
    os_pin_mode(ST7789_RES_PIN, PIN_MODE_OUTPUT);
    os_pin_mode(ST7789_PWR_PIN, PIN_MODE_OUTPUT);

    st7789_switch_off();

    os_pin_write(ST7789_RES_PIN, PIN_HIGH);
    os_task_msleep(50);
    os_pin_write(ST7789_RES_PIN, PIN_LOW);
    /* wait at least 100ms for reset */
    os_task_msleep(100);
    os_pin_write(ST7789_RES_PIN, PIN_HIGH);
    os_task_msleep(150);
}

static int st7789_hw_config(void)
{
    struct os_spi_configuration cfg;

    st7789->spi_dev = (struct os_spi_device *)os_device_find(OS_ST7789VW_SPI_BUS_NAME "_lcd");
    OS_ASSERT(st7789->spi_dev);

    cfg.data_width = 8;
    if (OS_ST7789VW_SPI_BUS_MODE == 0)
    {
        cfg.mode = OS_SPI_MASTER | OS_SPI_MODE_0 | OS_SPI_MSB | OS_SPI_3WIRE;
    }
    else
    {
        cfg.mode = OS_SPI_MASTER | OS_SPI_MODE_3 | OS_SPI_MSB | OS_SPI_3WIRE;
    }

    cfg.max_hz = 42 * 1000 * 1000; /* 42M,SPI max 42MHz,lcd 4-wire spi */

    os_spi_configure(st7789->spi_dev, &cfg);

    return OS_SUCCESS;
}

static int st7789_init(void)
{
    os_hw_spi_device_attach(OS_ST7789VW_SPI_BUS_NAME, OS_ST7789VW_SPI_BUS_NAME "_lcd", OS_ST7789VW_SPI_CS_PIN);
    st7789_hw_config();
    st7789_gpio_init();

    /* Memory Data Access Control */
    st7789_write_cmd(0x36);
    st7789_write_data(0x00);
    /* RGB 5-6-5-bit  */
    st7789_write_cmd(0x3A);
    st7789_write_data(0x65);
    /* Porch Setting */
    st7789_write_cmd(0xB2);
    st7789_write_data(0x0C);
    st7789_write_data(0x0C);
    st7789_write_data(0x00);
    st7789_write_data(0x33);
    st7789_write_data(0x33);
    /*  Gate Control */
    st7789_write_cmd(0xB7);
    st7789_write_data(0x35);
    /* VCOM Setting */
    st7789_write_cmd(0xBB);
    st7789_write_data(0x19);
    /* LCM Control */
    st7789_write_cmd(0xC0);
    st7789_write_data(0x2C);
    /* VDV and VRH Command Enable */
    st7789_write_cmd(0xC2);
    st7789_write_data(0x01);
    /* VRH Set */
    st7789_write_cmd(0xC3);
    st7789_write_data(0x12);
    /* VDV Set */
    st7789_write_cmd(0xC4);
    st7789_write_data(0x20);
    /* Frame Rate Control in Normal Mode */
    st7789_write_cmd(0xC6);
    st7789_write_data(0x0F);
    /* Power Control 1 */
    st7789_write_cmd(0xD0);
    st7789_write_data(0xA4);
    st7789_write_data(0xA1);
    /* Positive Voltage Gamma Control */
    st7789_write_cmd(0xE0);
    st7789_write_data(0xD0);
    st7789_write_data(0x04);
    st7789_write_data(0x0D);
    st7789_write_data(0x11);
    st7789_write_data(0x13);
    st7789_write_data(0x2B);
    st7789_write_data(0x3F);
    st7789_write_data(0x54);
    st7789_write_data(0x4C);
    st7789_write_data(0x18);
    st7789_write_data(0x0D);
    st7789_write_data(0x0B);
    st7789_write_data(0x1F);
    st7789_write_data(0x23);
    /* Negative Voltage Gamma Control */
    st7789_write_cmd(0xE1);
    st7789_write_data(0xD0);
    st7789_write_data(0x04);
    st7789_write_data(0x0C);
    st7789_write_data(0x11);
    st7789_write_data(0x13);
    st7789_write_data(0x2C);
    st7789_write_data(0x3F);
    st7789_write_data(0x44);
    st7789_write_data(0x51);
    st7789_write_data(0x2F);
    st7789_write_data(0x1F);
    st7789_write_data(0x1F);
    st7789_write_data(0x20);
    st7789_write_data(0x23);
    /* Display Inversion On */
    st7789_write_cmd(0x21);
    /* Sleep Out */
    st7789_write_cmd(0x11);

    /* wait for power stability */
    os_task_msleep(100);

    /* clear the screen */
    st7789_fill_color(0, 0, ST7789_W, ST7789_H, 0xFFFF);

    /* display on */
    st7789_switch_on();

    st7789_write_cmd(0x29);

    return OS_SUCCESS;
}

static void st7789_display_on(struct os_device *dev, os_bool_t on_off)
{
    if (on_off)
        st7789_switch_on();
    else
        st7789_switch_off();
}

static void st7789_frame_flush(struct os_device *dev)
{
    os_graphic_t *graphic = (os_graphic_t *)dev;
    OS_ASSERT(graphic->info.framebuffer_curr);

    st7789_address_set(0, 0, graphic->info.width - 1, graphic->info.height - 1);
    os_pin_write(ST7789_DC_PIN, PIN_HIGH);
    os_spi_send(st7789->spi_dev, graphic->info.framebuffer_curr, graphic->info.framebuffer_num);
}

static void st7789_display_area(struct os_device *dev, os_graphic_area_t *area)
{
    uint16_t   color;
    os_graphic_t *graphic = (os_graphic_t *)dev;

    if (area->buffer)
    {
        st7789_address_set(area->x, area->y, area->x + area->w - 1, area->y + area->h - 1);
        os_pin_write(ST7789_DC_PIN, PIN_HIGH);
        os_spi_send(st7789->spi_dev, area->buffer, area->w * area->h * graphic->info.bytes_per_pixel);
    }
    else
    {
        color = OS_COLOR_TO16(area->color);
        st7789_fill_color(area->x, area->y, area->w, area->h, color);
    }
}

const static struct os_graphic_ops ops = {
    .display_on   = st7789_display_on,
    .display_area = st7789_display_area,
    .frame_flush  = st7789_frame_flush,
};

static int os_hw_lcd_init(void)
{
    st7789 = os_calloc(1, sizeof(struct st7789_lcd));
    OS_ASSERT(st7789);

    st7789_init();

    st7789->graphic.ops = &ops;

    os_graphic_register("lcd", &st7789->graphic);

    os_kprintf("st7789 lcd found.\r\n");

    return OS_SUCCESS;
}

OS_INIT_CALL(os_hw_lcd_init, OS_INIT_LEVEL_DEVICE, OS_INIT_SUBLEVEL_HIGH);
