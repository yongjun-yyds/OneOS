/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        graphic.h
 *
 * @brief       This file includes definitions and declarations for graphic device.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef _GRAPHIC_H_
#define _GRAPHIC_H_

#include <device.h>
#include "graphic_conf.h"
#include "oneos_config.h"

enum
{
    OS_GRAPHIC_CTRL_POWERON,  /* power on graphic device */
    OS_GRAPHIC_CTRL_POWEROFF, /* power off graphic device */
    OS_GRAPHIC_CTRL_GET_INFO, /* get graphic device info */

    OS_GRAPHIC_CTRL_DISP_AREA, /* display area */

    OS_GRAPHIC_CTRL_FRAME_NEXT,  /* get next frame buffer */
    OS_GRAPHIC_CTRL_FRAME_SET,   /* set current frame buffer */
    OS_GRAPHIC_CTRL_FRAME_FILL,  /* fill current frame buffer */
    OS_GRAPHIC_CTRL_FRAME_FLUSH, /* flush frame buffer to display */

    OS_GRAPHIC_CTRL_DRAW_POINT,
    OS_GRAPHIC_CTRL_DRAW_LINE,
    OS_GRAPHIC_CTRL_DRAW_RECT,
    OS_GRAPHIC_CTRL_DRAW_CIRCLE,
    OS_GRAPHIC_CTRL_DRAW_SOLID_RECT,
    OS_GRAPHIC_CTRL_DRAW_SOLID_CIRCLE,
    OS_GRAPHIC_CTRL_DRAW_IMAGE,
    OS_GRAPHIC_CTRL_DRAW_CHAR,
    OS_GRAPHIC_CTRL_DRAW_STRING,
    OS_GRAPHIC_CTRL_DRAW_SET_TXT_CURSOR,
    OS_GRAPHIC_CTRL_DRAW_SET_TXT_COLOR,
    OS_GRAPHIC_CTRL_DRAW_ADD_FONT,
    OS_GRAPHIC_CTRL_DRAW_SET_FONT,
    OS_GRAPHIC_CTRL_DRAW_CLEAR,

    OS_GRAPHIC_CTRL_GPU_BLEND,          /* gpu blend */
    OS_GRAPHIC_CTRL_GPU_ALPHAMAP_BLEND, /* gpu blend */
    OS_GRAPHIC_CTRL_GPU_BLIT,           /* gpu blit */
    OS_GRAPHIC_CTRL_GPU_WAIT,           /* gpu wait finish */
};

#define OS_COLOR_WHITE  OS_COLOR(0xFF, 0xFF, 0xFF)
#define OS_COLOR_SILVER OS_COLOR(0xC0, 0xC0, 0xC0)
#define OS_COLOR_GRAY   OS_COLOR(0x80, 0x80, 0x80)
#define OS_COLOR_BLACK  OS_COLOR(0x00, 0x00, 0x00)
#define OS_COLOR_RED    OS_COLOR(0xFF, 0x00, 0x00)
#define OS_COLOR_YELLOW OS_COLOR(0xFF, 0xFF, 0x00)
#define OS_COLOR_GREEN  OS_COLOR(0x00, 0x80, 0x00)
#define OS_COLOR_BLUE   OS_COLOR(0x00, 0x00, 0xFF)
#define OS_COLOR_PURPLE OS_COLOR(0x80, 0x00, 0x80)
#define OS_COLOR_ORANGE OS_COLOR(0xFF, 0xA5, 0x00)

#define OS_COLOR_SET_R16(rr) (uint16_t)((rr >> 3) & 0x1FU)
#define OS_COLOR_SET_G16(gg) (uint16_t)((gg >> 2) & 0x3FU)
#define OS_COLOR_SET_B16(bb) (uint16_t)((bb >> 3) & 0x1FU)

#define OS_COLOR_BIT_5TO8(x) ((x * 263 + 7) >> 5)
#define OS_COLOR_BIT_6TO8(x) ((x * 259 + 3) >> 6)

struct os_color32
{
    uint8_t b;
    uint8_t g;
    uint8_t r;
    uint8_t a;
};

struct os_color24
{
    uint8_t b;
    uint8_t g;
    uint8_t r;
};

struct os_color16
{
    uint16_t b : 5;
    uint16_t g : 6;
    uint16_t r : 5;
};

#if defined(OS_GRAPHIC_LCD_FORMAT_RGB565)
typedef struct os_color16 os_color_t;
#define OS_COLOR(r, g, b) ((os_color_t){OS_COLOR_SET_B16(b), OS_COLOR_SET_G16(g), OS_COLOR_SET_R16(r)})
#define OS_COLOR_FROM16(v)                                                                                             \
    ((os_color_t){(uint16_t)(v)&0x1FU, (uint16_t)(v) >> 5 & 0x3FU, (uint16_t)(v) >> 11 & 0x1FU})
#define OS_COLOR_FROM32(v) ((os_color_t){OS_COLOR_SET_B16(v), OS_COLOR_SET_G16(v >> 8), OS_COLOR_SET_R16(v >> 16)})
#define OS_COLOR_TO16(c)   ((uint16_t)(c.r) << 11 | (uint16_t)(c.g) << 5 | (uint16_t)(c.b))
#define OS_COLOR_TO32(c)                                                                                               \
    (0xFF000000 | (uint32_t)OS_COLOR_BIT_5TO8(c.r) << 16 | (uint32_t)OS_COLOR_BIT_6TO8(c.g) << 8 |               \
     (uint32_t)OS_COLOR_BIT_5TO8(c.b))
#elif defined(OS_GRAPHIC_LCD_FORMAT_RGB888)
typedef struct os_color24 os_color_t;
#define OS_COLOR(r, g, b) ((os_color_t){b, g, r})
#elif defined(OS_GRAPHIC_LCD_FORMAT_ARGB8888)
typedef struct os_color32 os_color_t;
#define OS_COLOR(r, g, b) ((os_color_t){b, g, r, 0xFF})
#else
#error Unsupported color format
#endif

struct os_graphic_ops;
struct os_graphic_draw_ops;

typedef struct os_graphic_info
{
    uint8_t  pixel_format;    /* Graphic format. */
    uint8_t  bits_per_pixel;  /* Bits per pixel. */
    uint8_t  bytes_per_pixel; /* Bytes per pixel. */
    uint16_t bytes_per_line;  /* Bytes per line. */

    uint16_t width;  /* Width of graphic device. */
    uint16_t height; /* Height of graphic device. */

    uint8_t *framebuffer[2];   /* Internal frame buffer pointer */
    uint8_t  framebuffer_num;  /* Internal frame buffer number */
    uint32_t framebuffer_size; /* Size of each frame buffer */
    uint8_t *framebuffer_curr; /* Current frame buffer pointer */
} os_graphic_info_t;

typedef struct os_graphic_device
{
    os_device_t parent;

    struct os_graphic_info            info;
    const struct os_graphic_ops      *ops;
    const struct os_graphic_draw_ops *draw_ops;
} os_graphic_t;

typedef struct os_graphic_area
{
    uint16_t x; /* x coordinate. */
    uint16_t y; /* y coordinate. */
    uint16_t w; /* Width. */
    uint16_t h; /* Height.*/
    os_color_t  color;
    uint8_t *buffer;
} os_graphic_area_t;

typedef struct os_device_gpu_info
{
    char       *dest;
    void       *src;
    int32_t  src_format;
    int32_t  sbpl;
    int32_t  width;
    int32_t  height;
    int32_t  alpha;
    uint32_t color;
    uint8_t  yuv_sampling_mode;
} os_graphic_gpu_t;

struct os_graphic_ops
{
    void (*display_on)(struct os_device *dev, os_bool_t on_off);
    void (*frame_flush)(struct os_device *dev);
    void (*display_area)(struct os_device *dev, os_graphic_area_t *area);
#ifdef OS_GRAPHIC_GPU_ENABLE
    void (*gpu_blend)(struct os_device *dev, struct os_device_gpu_info *gpu_info);
    void (*gpu_alphamap_blend)(struct os_device *dev, struct os_device_gpu_info *gpu_info);
    void (*gpu_blit)(struct os_device *dev, struct os_device_gpu_info *gpu_info);
    void (*gpu_wait_finish)(struct os_device *dev);
#endif
};

#ifdef OS_GRAPHIC_DRAW_ENABLE

typedef struct os_graphic_pos
{
    uint16_t x;
    uint16_t y;
} os_graphic_pos_t;

typedef struct os_graphic_point
{
    int16_t x;
    int16_t y;
    os_color_t color;
} os_graphic_point_t;

typedef struct os_graphic_line
{
    int16_t x1;
    int16_t y1;
    int16_t x2;
    int16_t y2;
    os_color_t color;
} os_graphic_line_t;

typedef struct os_graphic_rect
{
    int16_t x;
    int16_t y;
    int16_t w;
    int16_t h;
    os_color_t color;
} os_graphic_rect_t;

typedef struct os_graphic_circle
{
    int16_t x;
    int16_t y;
    int16_t r;
    os_color_t color;
} os_graphic_circle_t;

typedef struct os_graphic_font
{
    const char    *name;
    uint8_t     width;
    uint8_t     height;
    uint8_t     bytes;
    os_list_node_t list;
    void (*get_char)(int32_t index, uint8_t *buf, int32_t buf_size);
} os_graphic_font_t;

struct os_graphic_draw_ops
{
    void (*draw_point)(os_graphic_t *graphic, os_graphic_point_t *point);
    void (*draw_line)(os_graphic_t *graphic, os_graphic_line_t *line);
    void (*draw_rect)(os_graphic_t *graphic, os_graphic_rect_t *rect);
    void (*draw_circle)(os_graphic_t *graphic, os_graphic_circle_t *circle);
    void (*draw_solid_rect)(os_graphic_t *graphic, os_graphic_rect_t *rect);
    void (*draw_solid_circle)(os_graphic_t *graphic, os_graphic_circle_t *circle);
    void (*draw_image)(os_graphic_t *graphic, os_graphic_area_t *circle);

    void (*draw_char)(os_graphic_t *graphic, int32_t index);
    void (*draw_string)(os_graphic_t *graphic, uint8_t *str);

    void (*set_txt_cursor)(os_graphic_t *graphic, os_graphic_pos_t *pos);
    void (*set_txt_color)(os_graphic_t *graphic, os_color_t *color);
    os_err_t (*set_font)(os_graphic_t *graphic, char *name);
    os_err_t (*add_font)(os_graphic_t *graphic, os_graphic_font_t *font);

    void (*clear)(os_graphic_t *graphic, os_color_t *color);
};

#endif

os_err_t os_graphic_add_framebuffer(struct os_device *dev, uint8_t *fb, uint32_t size);
os_err_t os_graphic_register(const char *name, os_graphic_t *graphic);

#endif /* _GRAPHIC_H_ */
