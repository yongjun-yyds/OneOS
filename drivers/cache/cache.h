/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        cache.h
 *
 * @brief       this file implements cache related definitions and declarations
 *
 * @details
 *
 * @revision
 * Date          Author          Notes
 * 2022-07-18    OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __DRIVER_CACHE_H_
#define __DRIVER_CACHE_H_

#include <os_stddef.h>

#ifdef ARCH_ARM_CORTEX_M7
#include <arch_cache.h>

OS_INLINE void os_clean_invalid_dcache(void *addr, int32_t dsize)
{
    os_cpu_dcache_ops(OS_HW_CACHE_FLUSH | OS_HW_CACHE_INVALIDATE, (void *)addr, (int32_t)dsize);
}

OS_INLINE void os_clean_dcache(void *addr, int32_t dsize)
{
    os_cpu_dcache_ops(OS_HW_CACHE_FLUSH, (void *)addr, (int32_t)dsize);
}

OS_INLINE void os_invalid_dcache(void *addr, int32_t dsize)
{
    os_cpu_dcache_ops(OS_HW_CACHE_INVALIDATE, (void *)addr, (int32_t)dsize);
}

#else

OS_INLINE void os_clean_invalid_dcache(void *addr, int32_t dsize)
{
}

OS_INLINE void os_clean_dcache(void *addr, int32_t dsize)
{
}

OS_INLINE void os_invalid_dcache(void *addr, int32_t dsize)
{
}

#endif

#endif /* __DRIVER_CACHE_H_ */
