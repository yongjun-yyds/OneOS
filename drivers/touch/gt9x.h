/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        gt9x.h
 *
 * @brief       gt9x
 *
 * @details     gt9x
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __GT9X_H_
#define __GT9X_H_

#include <touch/touch.h>

#define GT9X_ADDR_LENGTH    (2)
#define GT9X_MAX_TOUCH      (10)
#define GT9X_POINT_INFO_NUM (8)

#define GT9X_ADDRESS_HIGH (OS_GT9X_I2C_ADDR_HIGH)
#define GT9X_ADDRESS_LOW  (OS_GT9X_I2C_ADDR_LOW)

#define GT9X_COMMAND (0x8040)

#define GT9X_PRODUCT_ID  (0x8140)
#define GT9X_READ_STATUS (0x814E)

#define GT9X_POINT1_REG (0x814F)
#define GT9X_POINT2_REG (0X8157)
#define GT9X_POINT3_REG (0X815F)
#define GT9X_POINT4_REG (0X8167)
#define GT9X_POINT5_REG (0X816F)

enum chip_type
{
    GT911 = 0,
    GT9157,
    GT917S,
    GT5688,
    GT9147,
    GT9271,
};

struct chip_info
{
    enum chip_type    type;
    uint16_t       cfg_addr;
    const uint8_t *cfg_table;
    uint8_t        cfg_length;
};

#endif
