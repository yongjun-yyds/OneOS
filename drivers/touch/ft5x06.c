/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        ft5x06.c
 *
 * @brief       ft5x06
 *
 * @details     ft5x06
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include <os_clock.h>
#include <drv_gpio.h>
#include <ft5x06.h>

#define LOG_TAG "ft5x06"
#define DBG_LVL DBG_INFO
#include <drv_log.h>

#define TOUCH_POINT_GET_EVENT(T) ((touch_event_t)((T).XH >> 6))
#define TOUCH_POINT_GET_ID(T)    ((T).YH >> 4)
#define TOUCH_POINT_GET_X(T)     ((((T).XH & 0x0f) << 8) | (T).XL)
#define TOUCH_POINT_GET_Y(T)     ((((T).YH & 0x0f) << 8) | (T).YL)

struct ft5x06_touch
{
    os_touch_t                touch_device;
    struct os_i2c_bus_device *i2c_bus;
    uint16_t               i2c_addr;
    uint16_t               id;
    /* field holding the current number of simultaneous active touches */
    uint8_t currActiveTouchNb;
    /* field holding the touch index currently managed */
    uint8_t currActiveTouchIdx;
};

typedef struct _ft5x06_touch_point
{
    uint8_t XH;
    uint8_t XL;
    uint8_t YH;
    uint8_t YL;
    uint8_t RESERVED[2];
} ft5x06_touch_point_t;

typedef struct _ft5x06_touch_data
{
    uint8_t              GEST_ID;
    uint8_t              TD_STATUS;
    ft5x06_touch_point_t TOUCH[FT5X06_MAX_TOUCHES];
} ft5x06_touch_data_t;

static void I2C_IO_Write(struct ft5x06_touch *ft5x06, uint8_t reg, uint8_t value)
{
    unsigned char buff[2] = {reg, value};
    os_i2c_master_send(ft5x06->i2c_bus, ft5x06->i2c_addr, 0, buff, 2);
}

uint16_t I2C_IO_ReadMultiple(struct ft5x06_touch *ft5x06, uint8_t reg, uint8_t *buffer, uint16_t length)
{
    os_i2c_master_send(ft5x06->i2c_bus, ft5x06->i2c_addr, 0, &reg, 1);
    os_i2c_master_recv(ft5x06->i2c_bus, ft5x06->i2c_addr, 0, buffer, length);
    return 0;
}

static void I2C_IO_Delay(uint32_t delay)
{
    os_task_msleep(delay);
}

static void ft5x06_irq_handler(void *args)
{
    os_touch_irq_notify((os_touch_t *)args);
}

static void ft5x06_Start(struct ft5x06_touch *ft5x06)
{
    I2C_IO_Write(ft5x06, 0, 0);
    I2C_IO_Delay(100);
}

static void ft5x06_GetXY(struct ft5x06_touch *ft5x06, uint16_t *X, uint16_t *Y, touch_event_t *event)
{
    uint8_t             regAddress = 1;
    touch_event_t       touch_event;
    ft5x06_touch_data_t point;

    /* Read X and Y positions */
    I2C_IO_ReadMultiple(ft5x06, regAddress, (uint8_t *)&point, sizeof(ft5x06_touch_data_t));

    touch_event = TOUCH_POINT_GET_EVENT(point.TOUCH[0]);

    /* Update coordinates only if there is touch detected */
    if (touch_event != kTouch_Reserved)
    {
        *X                         = TOUCH_POINT_GET_X(point.TOUCH[0]);
        *Y                         = TOUCH_POINT_GET_Y(point.TOUCH[0]);
        *event                     = touch_event;
        ft5x06->currActiveTouchIdx = 1;
    }
}

static os_size_t ft5x06_read_point(struct os_touch_device *touch, struct os_touch_data *data, os_size_t read_num)
{
    uint16_t      input_x = 0;
    uint16_t      input_y = 0;
    touch_event_t event   = kTouch_Reserved;

    struct ft5x06_touch *ft5x06 = (struct ft5x06_touch *)touch;

    ft5x06_GetXY(ft5x06, &input_x, &input_y, &event);

    switch (event)
    {
    case kTouch_Down:
        data->event = OS_TOUCH_EVENT_DOWN;
        break;
    case kTouch_Up:
        data->event = OS_TOUCH_EVENT_UP;
        break;
    case kTouch_Contact:
        data->event = OS_TOUCH_EVENT_MOVE;
        break;
    default:
        data->event = OS_TOUCH_EVENT_NONE;
        return 0;
    }

    data->timestamp    = os_touch_get_ts();
    data->track_id     = 0;
    data->x_coordinate = input_x;
    data->y_coordinate = input_y;

    return 1;
}

static os_err_t ft5x06_control(struct os_touch_device *device, int cmd, void *data)
{
    switch (cmd)
    {
    case OS_TOUCH_CTRL_GET_ID:
        break;
    case OS_TOUCH_CTRL_GET_INFO:
        *(struct os_touch_info *)data = device->info;
        break;
    case OS_TOUCH_CTRL_SET_MODE: /* change int trig type */
        break;
    case OS_TOUCH_CTRL_ENABLE_INT:
        os_pin_irq_enable(OS_FT5X06_IRQ_PIN, PIN_IRQ_ENABLE);
        break;
    case OS_TOUCH_CTRL_DISABLE_INT:
        os_pin_irq_enable(OS_FT5X06_IRQ_PIN, PIN_IRQ_DISABLE);
        break;
    default:
        break;
    }

    return OS_SUCCESS;
}

static struct os_touch_ops ft5x06_touch_ops = {
    .touch_readpoint = ft5x06_read_point,
    .touch_control   = ft5x06_control,
};

static int os_hw_ft5x06_init(void)
{
    struct ft5x06_touch *ft5x06 = os_calloc(1, sizeof(struct ft5x06_touch));
    OS_ASSERT(ft5x06);

    ft5x06->i2c_addr = OS_FT5X06_I2C_ADDR;
    ft5x06->i2c_bus  = os_i2c_bus_device_find(OS_FT5X06_I2C_BUS_NAME);
    OS_ASSERT(ft5x06->i2c_bus);

    ft5x06_Start(ft5x06);

    os_touch_t *touch_device = &ft5x06->touch_device;

    /* register touch device */
    touch_device->info.type      = OS_TOUCH_TYPE_CAPACITANCE;
    touch_device->info.vendor    = OS_TOUCH_VENDOR_UNKNOWN;
    touch_device->info.point_num = 1;
    touch_device->info.range_x   = OS_GRAPHIC_LCD_WIDTH;
    touch_device->info.range_y   = OS_GRAPHIC_LCD_HEIGHT;
    touch_device->ops            = &ft5x06_touch_ops;

    os_hw_touch_register(touch_device, "touch", ft5x06);

    /* set irq handle */
    os_pin_mode(OS_FT5X06_IRQ_PIN, PIN_MODE_INPUT_PULLUP);
    os_pin_attach_irq(OS_FT5X06_IRQ_PIN, PIN_IRQ_MODE_RISING, ft5x06_irq_handler, (void *)touch_device);
    os_pin_irq_enable(OS_FT5X06_IRQ_PIN, PIN_IRQ_DISABLE);
    return 0;
}

OS_INIT_CALL(os_hw_ft5x06_init, OS_INIT_LEVEL_COMPONENT, OS_INIT_SUBLEVEL_LOW);
