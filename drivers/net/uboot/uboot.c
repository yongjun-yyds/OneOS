/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_eth.c
 *
 * @brief       This file implements eth driver.
 *
 * @revision
 * Date         Author          Notes
 * 2023-02-06   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <os_memory.h>
#include <net_dev.h>
#include "board.h"

static os_list_node_t uboot_eth_list = OS_LIST_INIT(uboot_eth_list);

static os_err_t os_uboot_eth_init(struct os_net_device *net_dev)
{
    return OS_SUCCESS;
}

static os_err_t os_uboot_eth_deinit(struct os_net_device *net_dev)
{
    uhal_eth_deinit();
    return OS_SUCCESS;
}

static os_err_t os_uboot_eth_send(struct os_net_device *net_dev, uint8_t *data, int len)
{
    uhal_eth_send(data, len);
    return OS_SUCCESS;
}

static os_err_t os_uboot_eth_get_macaddr(struct os_net_device *net_dev, uint8_t *addr)
{
    memcpy(addr, uhal_eth_get_ethaddr(), OS_NET_MAC_LENGTH);
    return OS_SUCCESS;
}

const static struct os_net_device_ops uboot_eth_ops = {
    .init       = os_uboot_eth_init,
    .deinit     = os_uboot_eth_deinit,
    .send       = os_uboot_eth_send,
    .get_mac    = os_uboot_eth_get_macaddr,
};

static os_err_t os_uboot_eth_poll(struct napi_struct *napi, int budget)
{
    int i;
    int length;
    uint8_t *data;
    struct os_uboot_eth *udev = (struct os_uboot_eth *)napi->dev;

    if (udev->enable_irq)
        udev->enable_irq(udev, OS_FALSE);

    for (i = 0; i < budget; i++)
    {
        length = uhal_eth_recv(&data);
        if (length <= 0)
        {
            napi_complete_done(napi, i);
            break;
        }
        
        os_net_rx_data(napi->dev, data, length);
        uhal_eth_free_rx_buf(data, length);
    }

    if (udev->enable_irq)
        udev->enable_irq(udev, OS_TRUE);

    return i;
}

void uboot_net_rx_notify(struct os_uboot_eth *udev)
{
    napi_schedule(&udev->napi);
}

static void uboot_net_timer_callback(void *parameter)
{
    struct os_uboot_eth *udev = parameter;
    napi_schedule(&udev->napi);
}

int uboot_net_dev_register(struct os_uboot_eth *udev, const char *name)
{
    int ret;

    udev->net_dev.ops = &uboot_eth_ops;
    os_list_add(&uboot_eth_list, &udev->list);

    ret = os_net_device_register(&udev->net_dev, name);
    OS_ASSERT(ret == OS_SUCCESS);

    /* napi */
    netif_napi_add(&udev->net_dev, &udev->napi, os_uboot_eth_poll, NAPI_POLL_WEIGHT);

    /* timer poll */
    if (udev->enable_irq == OS_NULL)
    {
        udev->timer = os_timer_create(OS_NULL,
                                      name, 
                                      uboot_net_timer_callback, 
                                      udev, 
                                      (OS_TICK_PER_SECOND + 999) / 1000, 
                                      OS_TIMER_FLAG_PERIODIC);
        os_timer_start(udev->timer);
    }
    
    return ret;
}

