/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_eth.c
 *
 * @brief       This file implements eth driver.
 *
 * @revision
 * Date         Author          Notes
 * 2023-05-12   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef NET_UBOOT_H_
#define NET_UBOOT_H_

#include <os_timer.h>

struct os_uboot_eth
{
    struct os_net_device net_dev;
    struct napi_struct napi;
    os_timer_id timer;
    os_list_node_t list;
    os_err_t (*enable_irq)(struct os_uboot_eth *udev, int enable);
};

void uhal_eth_init(void);
void uhal_eth_deinit(void);
int uhal_eth_send(void *packet, int length);
int uhal_eth_recv_check(void);
int uhal_eth_recv(unsigned char **buff);
int uhal_eth_free_rx_buf(unsigned char *buff, int length);
unsigned char *uhal_eth_get_ethaddr(void);
int uhal_eth_link_status(void);

int uboot_net_dev_register(struct os_uboot_eth *udev, const char *name);
void uboot_net_rx_notify(struct os_uboot_eth *udev);

#endif

