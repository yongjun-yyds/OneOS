/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        wlan_dev.h
 *
 * @brief       This file implements wwd interface.
 *
 * @revision
 * Date         Author          Notes
 * 2021-08-12   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#ifndef __WLAN_DEV_H__
#define __WLAN_DEV_H__

#include <os_types.h>
#include <device.h>
#include "net_dev.h"

#define OS_WLAN_FLAG_STA_ONLY (0x1 << 0)
#define OS_WLAN_FLAG_AP_ONLY  (0x1 << 1)

#define OS_WLAN_EVET_AP_START         0x01
#define OS_WLAN_EVET_AP_STOP          0x02
#define OS_WLAN_EVET_STA_START        0x03
#define OS_WLAN_EVET_STA_STOP         0x04
#define OS_WLAN_EVET_AP_ASSOCIATED    0x05
#define OS_WLAN_EVET_AP_DISASSOCIATED 0x06
#define OS_WLAN_EVET_JOIN             0x07
#define OS_WLAN_EVET_LEAVE            0x08
#define OS_WLAN_EVET_SCAN             0x09
#define OS_WLAN_EVET_NONE             0x00

typedef enum
{
    OS_WLAN_SECURITY_OPEN = 0,
    OS_WLAN_SECURITY_WEP_PSK,
    OS_WLAN_SECURITY_WEP_SHARED,
    OS_WLAN_SECURITY_WPA_TKIP_PSK,
    OS_WLAN_SECURITY_WPA_AES_PSK,
    OS_WLAN_SECURITY_WPA_MIXED_PSK,
    OS_WLAN_SECURITY_WPA2_AES_PSK,
    OS_WLAN_SECURITY_WPA2_TKIP_PSK,
    OS_WLAN_SECURITY_WPA2_MIXED_PSK,
    OS_WLAN_SECURITY_WPA_TKIP_ENT,
    OS_WLAN_SECURITY_WPA_AES_ENT,
    OS_WLAN_SECURITY_WPA_MIXED_ENT,
    OS_WLAN_SECURITY_WPA2_TKIP_ENT,
    OS_WLAN_SECURITY_WPA2_AES_ENT,
    OS_WLAN_SECURITY_WPA2_MIXED_ENT,
    OS_WLAN_SECURITY_IBSS_OPEN,
    OS_WLAN_SECURITY_WPS_OPEN,
    OS_WLAN_SECURITY_WPS_SECURE,
    OS_WLAN_SECURITY_UNKNOWN,
    OS_WLAN_SECURITY_FORCE_32_BIT,
    OS_WLAN_SECURITY_MAX,
} os_wlan_security_t;

typedef enum
{
    OS_WLAN_COUNTRY_CHINA = 0,
    OS_WLAN_COUNTRY_UNITED_STATES,
    OS_WLAN_COUNTRY_UNKNOW
} os_wlan_country_code_t;

typedef enum
{
    OS_802_11_BAND_5GHZ    = 0,          /* Denotes 5GHz radio band   */
    OS_802_11_BAND_2_4GHZ  = 1,          /* Denotes 2.4GHz radio band */
    OS_802_11_BAND_UNKNOWN = 0x7fffffff, /* unknown */
} os_wlan_802_11_band_t;

typedef enum
{
    OS_BSS_TYPE_INFRASTRUCTURE = 0,
    OS_BSS_TYPE_ADHOC          = 1,
    OS_BSS_TYPE_ANY            = 2,
    OS_BSS_TYPE_UNKNOWN        = -1
} os_wlan_bss_type_t;

typedef struct os_wlan_ssid
{
    uint8_t len;
    char       val[OS_WLAN_SSID_MAX_LENGTH + 1];
} os_wlan_ssid_t;

struct os_wlan_info
{
    const char            *ssid;
    const char            *password;
    os_wlan_security_t     security;
    os_wlan_country_code_t country;
    uint8_t             channel;
};

struct os_wlan_scan_info
{
    os_wlan_ssid_t        ssid;
    uint8_t            bssid[6];
    int16_t            signal_strength;
    uint32_t           max_data_rate;
    os_wlan_bss_type_t    bss_type;
    os_wlan_security_t    security;
    uint8_t            channel;
    os_wlan_802_11_band_t band;
    os_bool_t             on_channel;
};

struct os_wlan_scan_result
{
    struct os_wlan_scan_info *scan_info;
    uint32_t               max_num;
    uint32_t               count;
};

struct os_wlan_device;

struct os_wlan_device_ops
{
    os_err_t (*join)(struct os_wlan_device *wlan_dev);
    os_err_t (*check_join_status)(struct os_wlan_device *wlan_dev);
    os_err_t (*leave)(struct os_wlan_device *wlan_dev);
    os_err_t (*wlan_scan)(struct os_wlan_device *wlan, uint32_t msec, struct os_wlan_scan_result *scan_result);
    os_err_t (*wlan_scan_stop)(struct os_wlan_device *wlan);

    os_err_t (*start)(struct os_wlan_device *wlan_dev);
    os_err_t (*stop)(struct os_wlan_device *wlan_dev);
    void (*irq_handler)(void *param);

    os_err_t (*init)(struct os_wlan_device *wlan_dev);
    os_err_t (*deinit)(struct os_wlan_device *wlan_dev);
    os_err_t (*send)(struct os_wlan_device *wlan_dev, uint8_t *buff, uint32_t size);
    os_err_t (*get_mac)(struct os_wlan_device *wlan_dev, uint8_t *addr);
    os_err_t (*set_filter)(struct os_wlan_device *wlan_dev, uint8_t *addr, os_bool_t enable);
};

struct os_wlan_device
{
    struct os_net_device net_dev;
    const struct os_wlan_device_ops *ops;
    void *drv_dev;
    uint32_t flags;
    struct os_wlan_info info;
    struct os_wlan_scan_result *scan_result;
};

os_err_t os_wlan_start(struct os_wlan_device *wlan_dev);
os_err_t os_wlan_stop(struct os_wlan_device *wlan_dev);
void     os_wlan_irq_handler(void *device);

os_err_t
os_wlan_join(struct os_wlan_device *wlan_dev, const char *ssid, const char *password, os_wlan_security_t security);
os_err_t                    os_wlan_leave(struct os_wlan_device *wlan_dev);
os_err_t                    os_wlan_check_join_status(struct os_wlan_device *wlan_dev);
struct os_wlan_scan_result *os_wlan_scan(struct os_wlan_device *wlan_dev, uint32_t msec, uint32_t max_num);
os_err_t                    os_wlan_scan_clean_result(struct os_wlan_device *wlan_dev);
os_err_t                    os_wlan_scan_stop(struct os_wlan_device *wlan_dev);

os_err_t os_wlan_event_callback(struct os_wlan_device *wlan_dev, os_ubase_t event, void *args);
os_err_t os_wlan_net_register(struct os_wlan_device *wlan_dev, const char *name);
#endif
