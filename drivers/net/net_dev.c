/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        net_dev.c
 *
 * @brief       This file provides functions for registering can device.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include <os_errno.h>
#include <os_assert.h>
#include <os_memory.h>
#include <os_clock.h>
#include <string.h>
#include <dlog.h>
#include "dma_ram.h"

#include <net_dev.h>
#include <stdlib.h>

#ifdef OS_NET_PROTOCOL_LWIP 
#include <oneos_lwip.h>
#endif

#define EXT_LVL DBG_EXT_INFO
#define EXT_TAG "net_dev"
#include <drv_log.h>

static os_err_t os_net_device_init(struct os_net_device *net_dev)
{
#ifdef OS_NET_PROTOCOL_LWIP
    if (os_net_protocol_lwip_init(net_dev) != OS_SUCCESS)
    {
        return OS_FAILURE;
    }
#endif

    if (net_dev->ops->init)
    {
        if (net_dev->ops->init(net_dev) != OS_SUCCESS)
        {
            return OS_FAILURE;
        }
    }

    return OS_SUCCESS;
}

static os_err_t os_net_device_deinit(struct os_net_device *net_dev)
{
    if (net_dev->ops->deinit)
    {
        if (net_dev->ops->deinit(net_dev) != OS_SUCCESS)
        {
            return OS_FAILURE;
        }
    }

#ifdef OS_NET_PROTOCOL_LWIP
    if (os_net_protocol_lwip_deinit(net_dev) != OS_SUCCESS)
    {
        return OS_FAILURE;
    }
#endif

    return OS_SUCCESS;
}

os_err_t os_net_linkchange(struct os_net_device *net_dev, os_bool_t status)
{
    if (status)
        LOG_I(EXT_TAG, "net link up %s", net_dev->dev.name);
    else
        LOG_I(EXT_TAG, "net link down %s", net_dev->dev.name);

    if (net_dev->info.linkstatus != status)
    {
        net_dev->info.linkstatus = status;

        if ((net_dev) && (net_dev->protocol_ops) && (net_dev->protocol_ops->linkchange))
        {
            return net_dev->protocol_ops->linkchange(net_dev, status);
        }
    }

    return OS_NOSYS;
}

static os_err_t get_mac_byte(char *string, uint8_t *value)
{
    uint8_t i = 0;

    for (i = 0; i < 2; i++)
    {
        if (((*string) >= 'A') && ((*string) <= 'F'))
        {
            *value += (uint8_t)(*string - 55);
        }
        else if (((*string) >= 'a') && ((*string) <= 'f'))
        {
            *value += (uint8_t)(*string - 87);
        }
        else if (((*string) >= '0') && ((*string) <= '9'))
        {
            *value += (uint8_t)(*string - 48);
        }
        else
        {
            return OS_FAILURE;
        }

        if (i == 0)
        {
            *value = (*value) * 16;
        }

        string++;
    }

    return OS_SUCCESS;
}

os_err_t os_net_get_mac_string2addr(char *string, uint8_t *addr)
{
    uint8_t i     = 0;
    uint8_t value = 0;

    if (string && addr)
    {
        for (i = 0; i < OS_NET_MAC_LENGTH - 1; i++)
        {
            if (*(string + 2 + 3 * i) != '.')
            {
                return OS_FAILURE;
            }
        }

        for (i = 0; i < OS_NET_MAC_LENGTH; i++)
        {
            value = 0;

            if (get_mac_byte(string, &value) != OS_SUCCESS)
            {
                return OS_FAILURE;
            }

            *addr = value;

            addr++;
            string += 3;
        }

        return OS_SUCCESS;
    }

    return OS_FAILURE;
}

os_err_t os_net_set_filter(struct os_net_device *net_dev, uint8_t *addr, os_bool_t enable)
{
    if ((net_dev) && (net_dev->ops->set_filter) && (addr))
    {
        return net_dev->ops->set_filter(net_dev, addr, enable);
    }

    return OS_NOSYS;
}

os_err_t os_net_get_macaddr(struct os_net_device *net_dev, uint8_t *addr)
{
    if ((net_dev) && (net_dev->ops->get_mac) && (addr))
    {
        return net_dev->ops->get_mac(net_dev, addr);
    }

    return OS_NOSYS;
}

os_err_t os_net_tx_data(struct os_net_device *net_dev, uint8_t *data, int len)
{
    OS_ASSERT(net_dev->ops->send != OS_NULL);
    return net_dev->ops->send(net_dev, data, len);
}

os_err_t os_net_rx_data(struct os_net_device *net_dev, uint8_t *data, int len)
{
    OS_ASSERT(net_dev->protocol_ops->recv != OS_NULL);
    return net_dev->protocol_ops->recv(net_dev, data, len);
}

os_err_t os_net_device_register(struct os_net_device *net_dev, const char *name)
{
    OS_ASSERT(net_dev != OS_NULL);
    OS_ASSERT(net_dev->ops != OS_NULL);

    net_dev->tx_buff = malloc(net_dev->info.MTU);
    OS_ASSERT(net_dev->tx_buff != OS_NULL);

    net_dev->rx_buff = malloc(net_dev->info.MRU);
    OS_ASSERT(net_dev->rx_buff != OS_NULL);

    if (os_net_device_init(net_dev) != OS_SUCCESS)
    {
        LOG_E(EXT_TAG, "net device init failed %s", name);
        return OS_FAILURE;
    }

    net_dev->dev.type = OS_DEVICE_TYPE_NETIF;
    if (os_device_register(&net_dev->dev, name) != OS_SUCCESS)
    {
        return OS_FAILURE;
    }

    return OS_SUCCESS;
}

os_err_t os_net_device_unregister(struct os_net_device *net_dev)
{    
    if (net_dev->tx_buff)
    {
        free(net_dev->tx_buff);
    }

    if (net_dev->rx_buff)
    {
        free(net_dev->rx_buff);
    }

    return os_net_device_deinit(net_dev);
}

#ifdef OS_USING_NET_NAPI

/* napi */
static OS_DEFINE_SPINLOCK(net_dev_lock);
static int netdev_budget = OS_NET_NAPI_BUDGET;
static unsigned int netdev_budget_usecs = OS_NET_NAPI_BUDGET_USECS;
static os_list_node_t net_poll_list = OS_LIST_INIT(net_poll_list);

/* napi task */
static os_task_id net_dev_rx_task;
static os_semaphore_id net_dev_rx_sem;

void netif_napi_add(struct os_net_device *dev, struct napi_struct *napi,
        int (*poll)(struct napi_struct *napi, int budget), int weight)
{
    os_ubase_t irq_save;
    
    napi->poll = poll;
    napi->weight = weight;
    napi->dev = dev;

    os_spin_lock_irqsave(&net_dev_lock, &irq_save);
    os_list_add(&net_poll_list, &napi->list);
    os_spin_unlock_irqrestore(&net_dev_lock, irq_save);
}

os_bool_t napi_schedule_prep(struct napi_struct *n)
{
    os_ubase_t irq_save;
    os_spin_lock_irqsave(&net_dev_lock, &irq_save);
    n->state |= NAPIF_STATE_SCHED;
    os_spin_unlock_irqrestore(&net_dev_lock, irq_save);
    return OS_TRUE;
}

void __napi_schedule(struct napi_struct *n)
{
    os_semaphore_post(net_dev_rx_sem);
}

void napi_schedule(struct napi_struct *n)
{
    if (napi_schedule_prep(n))
        __napi_schedule(n);
}

os_bool_t napi_complete_done(struct napi_struct *n, int work_done)
{
    os_ubase_t irq_save;
    os_spin_lock_irqsave(&net_dev_lock, &irq_save);
    n->state &= ~NAPIF_STATE_SCHED;
    os_spin_unlock_irqrestore(&net_dev_lock, irq_save);
    return OS_TRUE;
}

static int napi_poll(struct napi_struct *n, os_list_node_t *repoll)
{
    int work = 0;
    int weight = n->weight;

    if (n->state | NAPI_STATE_SCHED)
    {
        work = n->poll(n, weight);
    }

    return work;
}

static void net_rx_action(void)
{
    int budget = netdev_budget;
    unsigned long time_limit = os_tick_get_value() + os_tick_from_ms(netdev_budget_usecs / 1000);

    os_list_node_t list = OS_LIST_INIT(list);
    os_list_node_t repoll = OS_LIST_INIT(repoll);

    os_spin_lock_irq_disable(&net_dev_lock);
    os_list_splice_init(&list, &net_poll_list);
    os_spin_unlock_irq_enable(&net_dev_lock);

    while (1)
    {
        int work, work_total = 0;
        struct napi_struct *n;
        struct napi_struct *tmp;
        
        os_list_for_each_entry_safe(n, tmp, &list, struct napi_struct, list)
        {
            work = napi_poll(n, &repoll);
            work_total += work;
            budget -= work;
            if (budget <= 0 || time_after_eq(os_tick_get_value(), time_limit))
            {
                goto out;
            }
        }

        if (work_total == 0)
            break;
    }

out:
    os_list_splice_init(&list, &repoll);
    os_spin_lock_irq_disable(&net_dev_lock);
    os_list_splice(&net_poll_list, &list);
    os_spin_unlock_irq_enable(&net_dev_lock);
}

static void os_net_rx_task_entry(void *parameter)
{
    while (1)
    {
        os_semaphore_wait(net_dev_rx_sem, OS_WAIT_FOREVER);
        net_rx_action();
    }
}

static int os_net_dev_init(void)
{
    net_dev_rx_sem = os_semaphore_create(OS_NULL, "net/rx", 0, OS_SEM_MAX_VALUE);
    OS_ASSERT(net_dev_rx_sem != OS_NULL);
    
    net_dev_rx_task = os_task_create(OS_NULL,
                                     OS_NULL,
                                     OS_NET_RX_TASK_STACK_SIZE,
                                     "net/rx",
                                     os_net_rx_task_entry,
                                     OS_NULL,
                                     OS_NET_RX_TASK_PRIORITY);
    OS_ASSERT(net_dev_rx_task != OS_NULL);
    return os_task_startup(net_dev_rx_task);
}

OS_INIT_CALL(os_net_dev_init, OS_INIT_LEVEL_DEVICE, OS_INIT_SUBLEVEL_MIDDLE);
#endif /* OS_USING_NET_NAPI */

