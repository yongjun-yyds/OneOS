/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        net_dev.h
 *
 * @brief       This file provides functions for registering can device.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __NET_DEV_H__
#define __NET_DEV_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <os_sem.h>
#include <os_errno.h>
#include <os_mb.h>
#include <os_task.h>
#include <device.h>
#include <driver.h>

struct os_net_device;

#define NAPI_POLL_WEIGHT 64

typedef enum
{
    net_dev_intf_ether = 0,
    net_dev_intf_bt    = 1,
    net_dev_intf_br    = 2,
    net_dev_intf_unknown
} os_net_intf_type_t;

typedef enum
{
    net_dev_mode_ap  = 0,
    net_dev_mode_sta = 1,
    net_dev_mode_unknown
} os_net_mode_t;

struct os_net_device_ops
{
    os_err_t (*init)(struct os_net_device *net_dev);
    os_err_t (*deinit)(struct os_net_device *net_dev);
    os_err_t (*send)(struct os_net_device *net_dev, uint8_t *data, int len);
    os_err_t (*get_mac)(struct os_net_device *net_dev, uint8_t *addr);
    os_err_t (*set_filter)(struct os_net_device *net_dev, uint8_t *buff, os_bool_t enable);
};

struct os_net_protocol_ops
{
    os_err_t (*init)(struct os_net_device *net_dev);
    os_err_t (*deinit)(struct os_net_device *net_dev);
    os_err_t (*recv)(struct os_net_device *net_dev, uint8_t *data, int len);
    os_err_t (*linkchange)(struct os_net_device *net_dev, os_bool_t status);
};

struct os_net_device_info
{
    uint32_t MTU;
    uint32_t MRU;

    os_net_mode_t      mode;
    os_bool_t          linkstatus;
    os_net_intf_type_t intf_type;

    char      *ip_addr;
    char      *gw_addr;
    char      *ip_mask;
    uint8_t dhcp_start_id;
    uint8_t dhcp_end_id;
};

struct os_net_device
{
    os_device_t dev;
    struct os_net_device_info info;
    const struct os_net_device_ops   *ops;
    const struct os_net_protocol_ops *protocol_ops;
    void *tx_buff;
    void *rx_buff;
    void *userdata;
};

os_err_t os_net_get_macaddr(struct os_net_device *net_dev, uint8_t *addr);
os_err_t os_net_tx_data(struct os_net_device *net_dev, uint8_t *data, int len);
os_err_t os_net_set_filter(struct os_net_device *net_dev, uint8_t *addr, os_bool_t enable);

os_err_t os_net_linkchange(struct os_net_device *net_dev, os_bool_t status);
os_err_t os_net_rx_data(struct os_net_device *net_dev, uint8_t *data, int len);
os_err_t os_net_rx_notify(struct os_net_device *net_dev);
os_err_t os_net_get_mac_string2addr(char *string, uint8_t *addr);

os_err_t os_net_device_unregister(struct os_net_device *net_dev);
os_err_t os_net_device_register(struct os_net_device *net_dev, const char *name);

#ifdef OS_USING_NET_NAPI
struct napi_struct
{    
    int weight;
    unsigned long state;
    os_list_node_t list;
    struct os_net_device *dev;
    int (*poll)(struct napi_struct *napi, int budget);
};

enum {
    NAPI_STATE_SCHED,   /* Poll is scheduled */
    NAPI_STATE_MISSED,  /* reschedule a napi */
    NAPI_STATE_DISABLE, /* Disable pending */
    NAPI_STATE_NPSVC,   /* Netpoll - don't dequeue from poll_list */
    NAPI_STATE_HASHED,  /* In NAPI hash (busy polling possible) */
    NAPI_STATE_NO_BUSY_POLL,/* Do not add in napi_hash, no busy polling */
    NAPI_STATE_IN_BUSY_POLL,/* sk_busy_loop() owns this NAPI */
};

enum {
    NAPIF_STATE_SCHED	 = BIT(NAPI_STATE_SCHED),
    NAPIF_STATE_MISSED	 = BIT(NAPI_STATE_MISSED),
    NAPIF_STATE_DISABLE	 = BIT(NAPI_STATE_DISABLE),
    NAPIF_STATE_NPSVC	 = BIT(NAPI_STATE_NPSVC),
    NAPIF_STATE_HASHED	 = BIT(NAPI_STATE_HASHED),
    NAPIF_STATE_NO_BUSY_POLL = BIT(NAPI_STATE_NO_BUSY_POLL),
    NAPIF_STATE_IN_BUSY_POLL = BIT(NAPI_STATE_IN_BUSY_POLL),
};

void netif_napi_add(struct os_net_device *dev, struct napi_struct *napi,
        int (*poll)(struct napi_struct *napi, int budget), int weight);
os_bool_t napi_schedule_prep(struct napi_struct *n);
void __napi_schedule(struct napi_struct *n);
void napi_schedule(struct napi_struct *n);
os_bool_t napi_complete_done(struct napi_struct *n, int work_done);

#endif

#ifdef OS_USING_NET_UBOOT
#include "uboot.h"
#endif

#ifdef __cplusplus
}
#endif

#endif
