/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        wlan_dev.c
 *
 * @brief       This file implements wlan driver.
 *
 * @revision
 * Date         Author          Notes
 * 2021-08-12   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <os_memory.h>
#include <os_types.h>
#include <os_errno.h>
#include <os_assert.h>
#include <dlog.h>
#include <string.h>
#include <device.h>

#include "wlan_dev.h"
#include "net_dev.h"

#define DRV_EXT_LVL DBG_EXT_INFO
#define DRV_EXT_TAG "wlan_dev"
#include <drv_log.h>

os_err_t os_wlan_event_callback(struct os_wlan_device *wlan_dev, os_ubase_t event, void *args)
{
    switch (event)
    {
    case OS_WLAN_EVET_JOIN:
    case OS_WLAN_EVET_AP_START:
        os_net_linkchange(&wlan_dev->net_dev, OS_TRUE);
        break;
    case OS_WLAN_EVET_LEAVE:
    case OS_WLAN_EVET_AP_STOP:
        os_net_linkchange(&wlan_dev->net_dev, OS_FALSE);
        break;
    case OS_WLAN_EVET_AP_ASSOCIATED:
    case OS_WLAN_EVET_AP_DISASSOCIATED:
        break;
    default:
        LOG_D(DRV_EXT_TAG, "event: UNKNOWN");
        break;
    }

    return OS_SUCCESS;
}

os_err_t os_wlan_start(struct os_wlan_device *wlan_dev)
{
    if (wlan_dev == OS_NULL)
    {
        return OS_EIO;
    }

    if (wlan_dev->ops->start)
    {
        return wlan_dev->ops->start(wlan_dev);
    }

    return OS_FAILURE;
}

os_err_t os_wlan_stop(struct os_wlan_device *wlan_dev)
{
    if (wlan_dev == OS_NULL)
    {
        return OS_EIO;
    }

    if (wlan_dev->ops->stop)
    {
        return wlan_dev->ops->stop(wlan_dev);
    }

    return OS_SUCCESS;
}

void os_wlan_irq_handler(void *device)
{
    struct os_wlan_device *wlan_dev = (struct os_wlan_device *)device;

    if (device == OS_NULL)
    {
        return;
    }

    if (wlan_dev->ops->irq_handler)
    {
        wlan_dev->ops->irq_handler(wlan_dev);
    }
}

/* clang-format off */
os_err_t os_wlan_join(struct os_wlan_device *wlan_dev, const char *ssid, const char *password, os_wlan_security_t security)
{
    if (wlan_dev == OS_NULL)
    {
        return OS_EIO;
    }

    if (security >= OS_WLAN_SECURITY_MAX)
    {
        return OS_EIO;
    }

    wlan_dev->info.ssid     = ssid;
    wlan_dev->info.password = password;
    wlan_dev->info.security = security;

    if (wlan_dev->ops->join)
    {
        return wlan_dev->ops->join(wlan_dev);
    }

    return OS_SUCCESS;
}
/* clang-format on */

os_err_t os_wlan_check_join_status(struct os_wlan_device *wlan_dev)
{
    if (wlan_dev == OS_NULL)
    {
        return OS_EIO;
    }

    if (wlan_dev->ops->check_join_status)
    {
        return wlan_dev->ops->check_join_status(wlan_dev);
    }

    return OS_SUCCESS;
}

os_err_t os_wlan_leave(struct os_wlan_device *wlan_dev)
{
    if (wlan_dev == OS_NULL)
    {
        return OS_EIO;
    }

    if (wlan_dev->ops->leave)
    {
        return wlan_dev->ops->leave(wlan_dev);
    }

    return OS_SUCCESS;
}

/* clang-format off */
struct os_wlan_scan_result *os_wlan_scan(struct os_wlan_device *wlan_dev, uint32_t msec, uint32_t max_num)
{
    if ((wlan_dev == OS_NULL) || (wlan_dev->ops->wlan_scan == OS_NULL))
    {
        return OS_NULL;
    }

    wlan_dev->scan_result = os_calloc(1, sizeof(struct os_wlan_scan_result) + max_num * sizeof(struct os_wlan_scan_info));
    if (wlan_dev->scan_result == OS_NULL)
    {
        return OS_NULL;
    }

    wlan_dev->scan_result->count   = 0;
    wlan_dev->scan_result->max_num = max_num;
    wlan_dev->scan_result->scan_info = (struct os_wlan_scan_info *)((os_ubase_t)wlan_dev->scan_result + sizeof(struct os_wlan_scan_result));

    if (wlan_dev->ops->wlan_scan(wlan_dev, msec, wlan_dev->scan_result) != OS_SUCCESS)
    {
        os_free(wlan_dev->scan_result);
        wlan_dev->scan_result = OS_NULL;
    }

    return wlan_dev->scan_result;
}
/* clang-format on */

os_err_t os_wlan_scan_clean_result(struct os_wlan_device *wlan_dev)
{
    if (wlan_dev == OS_NULL)
    {
        return OS_EIO;
    }

    if (wlan_dev->scan_result)
    {
        os_free(wlan_dev->scan_result);
        wlan_dev->scan_result = OS_NULL;
    }

    return OS_SUCCESS;
}

os_err_t os_wlan_scan_stop(struct os_wlan_device *wlan_dev)
{
    if (wlan_dev == OS_NULL)
    {
        return OS_EIO;
    }

    if (wlan_dev->ops->wlan_scan_stop)
    {
        return wlan_dev->ops->wlan_scan_stop(wlan_dev);
    }

    return OS_SUCCESS;
}

static os_err_t os_wlan_send(struct os_net_device *net_dev, uint8_t *buff, int size)
{
    struct os_wlan_device *wlan_dev = (struct os_wlan_device *)net_dev;
    return wlan_dev->ops->send(wlan_dev, buff, size);
}

static os_err_t os_wlan_get_mac(struct os_net_device *net_dev, uint8_t *addr)
{
    struct os_wlan_device *wlan_dev = (struct os_wlan_device *)net_dev;

    if (wlan_dev->ops->get_mac)
    {
        return wlan_dev->ops->get_mac(wlan_dev, addr);
    }

    return OS_SUCCESS;
}

static os_err_t os_wlan_set_filter(struct os_net_device *net_dev, uint8_t *addr, os_bool_t enable)
{
    struct os_wlan_device *wlan_dev = (struct os_wlan_device *)net_dev;

    if (wlan_dev->ops->set_filter)
    {
        return wlan_dev->ops->set_filter(wlan_dev, addr, enable);
    }

    return OS_SUCCESS;
}

static os_err_t os_wlan_init(struct os_net_device *net_dev)
{
    struct os_wlan_device *wlan_dev = (struct os_wlan_device *)net_dev;

    if (wlan_dev->ops->init)
    {
        return wlan_dev->ops->init(wlan_dev);
    }

    return OS_SUCCESS;
}

static os_err_t os_wlan_deinit(struct os_net_device *net_dev)
{
    struct os_wlan_device *wlan_dev = (struct os_wlan_device *)net_dev;

    if (wlan_dev->ops->deinit)
    {
        return wlan_dev->ops->deinit(wlan_dev);
    }

    return OS_SUCCESS;
}

const static struct os_net_device_ops wlan_net_dev_ops = {
    .init       = os_wlan_init,
    .deinit     = os_wlan_deinit,
    .send       = os_wlan_send,
    .get_mac    = os_wlan_get_mac,
    .set_filter = os_wlan_set_filter,
};

os_err_t os_wlan_net_register(struct os_wlan_device *wlan_dev, const char *name)
{
    if ((wlan_dev == OS_NULL) || (wlan_dev->ops == OS_NULL))
    {
        return OS_FAILURE;
    }

    wlan_dev->net_dev.ops = &wlan_net_dev_ops;

    if (os_net_device_register(&wlan_dev->net_dev, name) != OS_SUCCESS)
    {
        return OS_FAILURE;
    }

    return OS_SUCCESS;
}
