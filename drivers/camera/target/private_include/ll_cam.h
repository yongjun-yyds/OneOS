// Copyright 2010-2020 Espressif Systems (Shanghai) PTE LTD
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <stdint.h>
#include "include/camera.h"
#include <os_sem.h>
#include <os_mq.h>
#include <os_mb.h>
#include <os_task.h>
//#include "esp_private/periph_ctrl.h"

#define CAMERA_DBG_PIN_ENABLE 0

#define CAM_CHECK(a, str, ret) if (!(a)) {                                          \
        LOG_E(DRV_EXT_TAG,"%s(%d): %s", __FUNCTION__, __LINE__, str);                    \
        return (ret);                                                               \
        }

#define CAM_CHECK_GOTO(a, str, lab) if (!(a)) {                                     \
        LOG_E(DRV_EXT_TAG,"%s(%d): %s", __FUNCTION__, __LINE__, str);                    \
        LOG_E(DRV_EXT_TAG,"%s(%d): %s", __FUNCTION__, __LINE__, str);                    \
        goto lab;                                                                   \
        }

#define LCD_CAM_DMA_NODE_BUFFER_MAX_SIZE  (4096)//(0xFFFF*4)

typedef enum {
    CAM_SEM_DMA_BUF0 = 0,
    CAM_SEM_DMA_BUF1,
    CAM_SEM_VSYNC_BUF0,
    CAM_SEM_VSYNC_BUF1,
    CAM_SEM_VSYNC_END,
} cam_sem_t;

typedef enum {
    CAM_STATE_IDLE = 0,
    CAM_STATE_READ_BUF = 1,
} cam_state_t;

typedef struct {
    camera_fb_t fb;
    uint8_t en;
    //for RGB/YUV modes
    size_t fb_offset;
} cam_frame_t;

typedef struct {
    uint32_t dma_bytes_per_item;
    uint32_t dma_buffer_size;
    uint32_t dma_half_buffer_size;
    uint32_t dma_half_buffer_cnt;
    uint32_t dma_node_buffer_size;
    uint32_t dma_node_cnt;
    uint32_t frame_copy_cnt;

    //for JPEG mode
    uint8_t  *dma_buffer;

    cam_frame_t *frames;

    os_mailbox_dummy_t *irq_sem;
    uint8_t   irq_type;
    char *event_name;
    //os_msgqueue_t *frame_buffer_queue;
    os_mailbox_dummy_t*frame_buffer_queue;
    char *frame_name;
    os_task_id task_handle;

    uint8_t dma_num;//ESP32-S3

    uint8_t jpeg_mode;
    uint8_t vsync_pin;
    uint8_t vsync_invert;
    uint32_t frame_cnt;
    uint32_t recv_size;
    bool swap_data;
    bool psram_mode;

    //for RGB/YUV modes
    uint16_t width;
    uint16_t height;
    uint8_t in_bytes_per_pixel;
    uint8_t fb_bytes_per_pixel;
    uint32_t fb_size;

    cam_state_t state;
} cam_obj_t;


os_err_t ll_cam_dma_stop(cam_obj_t *cam);
bool ll_cam_start(cam_obj_t *cam, int frame_pos);
os_err_t ll_cam_config(cam_obj_t *cam, const camera_config_t *config);
os_err_t ll_cam_deinit(cam_obj_t *cam);
void ll_cam_vsync_intr_enable(cam_obj_t *cam, bool en);
os_err_t ll_cam_set_pin(cam_obj_t *cam, const camera_config_t *config);
os_err_t cam_dcmi_irq_init(void);
void ll_cam_do_vsync(cam_obj_t *cam);
uint8_t ll_cam_get_dma_align(cam_obj_t *cam);
bool ll_cam_dma_sizes(cam_obj_t *cam);
size_t ll_cam_memcpy(cam_obj_t *cam, uint8_t *out, const uint8_t *in, size_t len);
os_err_t ll_cam_set_sample_mode(cam_obj_t *cam, pixformat_t pix_format, uint32_t xclk_freq_hz, uint16_t sensor_pid);

// implemented in cam_hal
void ll_cam_send_event(cam_obj_t *cam, cam_sem_t cam_sem);
