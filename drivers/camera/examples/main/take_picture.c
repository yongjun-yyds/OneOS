/**
 * This example takes a picture every 5s and print its size on serial monitor.
 */
// ================================ CODE ======================================

#include <dlog.h>
#include <string.h>
#include "include/camera.h"
#include <drv_gpio.h>
#include <oneos_config.h>
#include <os_task.h>
#include <os_stddef.h>
#include <shell.h>

#define CAM_PIN_PWDN 1002  //power down is not used
#define CAM_PIN_RESET 15 //software reset will be performed
#define CAM_PIN_XCLK -1
#define CAM_PIN_SIOD 20
#define CAM_PIN_SIOC 19

#define CAM_PIN_D7 GET_PIN(B,9)
#define CAM_PIN_D6 GET_PIN(B,8)
#define CAM_PIN_D5 GET_PIN(D,3)
#define CAM_PIN_D4 GET_PIN(C,11)
#define CAM_PIN_D3 GET_PIN(C,9)
#define CAM_PIN_D2 GET_PIN(C,8)
#define CAM_PIN_D1 GET_PIN(C,7)
#define CAM_PIN_D0 GET_PIN(C,6)
#define CAM_PIN_VSYNC GET_PIN(B,7)
#define CAM_PIN_HREF GET_PIN(H,8)
#define CAM_PIN_PCLK GET_PIN(A,6)

static const char *TAG = "example:take_picture";

static camera_config_t camera_config = {
    .pin_pwdn = CAM_PIN_PWDN,
    .pin_reset = CAM_PIN_RESET,
    .pin_xclk = CAM_PIN_XCLK,
    .pin_sscb_sda = CAM_PIN_SIOD,
    .pin_sscb_scl = CAM_PIN_SIOC,

    .pin_d7 = CAM_PIN_D7,
    .pin_d6 = CAM_PIN_D6,
    .pin_d5 = CAM_PIN_D5,
    .pin_d4 = CAM_PIN_D4,
    .pin_d3 = CAM_PIN_D3,
    .pin_d2 = CAM_PIN_D2,
    .pin_d1 = CAM_PIN_D1,
    .pin_d0 = CAM_PIN_D0,
    .pin_vsync = CAM_PIN_VSYNC,
    .pin_href = CAM_PIN_HREF,
    .pin_pclk = CAM_PIN_PCLK,

    //XCLK 20MHz or 10MHz for OV2640 double FPS (Experimental)
    .xclk_freq_hz = 20000000,
    .sscb_name = SCCB_I2C_PORT,
    .pixel_format = PIXFORMAT_RGB565, //YUV422,GRAYSCALE,RGB565,JPEG
    .frame_size = FRAMESIZE_QVGA,    //QQVGA-UXGA Do not use sizes above QVGA when not JPEG

    .jpeg_quality = 12, //0-63 lower number means higher quality
    .fb_count = 1,       //if more than one, i2s runs in continuous mode. Use only with JPEG
    .grab_mode = CAMERA_GRAB_WHEN_EMPTY,
};

os_err_t init_camera(int argc, char *argv[])
{
    camera_config_t *cam_config = &camera_config;
    os_err_t err;
    if (argc != 2)
    {
        LOG_W(TAG, "usage: cam_init <mode>");
        LOG_W(TAG, "       cam_init jpeg/rgb");
        return OS_FAILURE;
    }

    if(strcmp("jpeg",argv[1]) == 0)
    {
        cam_config->pixel_format = PIXFORMAT_JPEG;
    }
    else if(strcmp("rgb",argv[1]) == 0)
    {
        cam_config->pixel_format = PIXFORMAT_RGB565;
    }
    else if(strcmp("yuv",argv[1]) == 0)
    {
        cam_config->pixel_format = PIXFORMAT_YUV422;
    }
    else
    {
        LOG_W(TAG, " not recognize type:must be jpeg or rgb");
        return OS_FAILURE;
    }
    err = esp_camera_init(cam_config);
    if (err != OS_SUCCESS)
    {
        LOG_E(TAG, "Camera Init Failed");
        return err;
    }
    LOG_I(TAG, "Camera Init ok");
    
    return OS_SUCCESS;
}
SH_CMD_EXPORT(cam_init, init_camera, "init_camera:default mode rgb");

static os_err_t deinit_camera(int argc, char *argv[])
{
    esp_camera_deinit();
    return OS_SUCCESS;

}

SH_CMD_EXPORT(cam_deinit, deinit_camera, "deinit_camera");

#if 0  
void app_main(void *parameter)
{
    char *pic_mode[]={"\0","jpeg"};

    if(OS_SUCCESS != init_camera(2,pic_mode)) {
        return;
    }
 while (1)
    {
        LOG_I(TAG, "Taking picture...");

        camera_fb_t *pic = esp_camera_fb_get();

        // use pic->buf to access the image
        if(pic!=NULL)
        {
            LOG_I(TAG, "Picture taken! Its size was: 0x%x bytes", pic->len);

            if(camera_config.pixel_format !=PIXFORMAT_JPEG )
            {
                //extern void data_to_lcd(camera_fb_t *cam,uint8_t mode);
                //data_to_lcd(pic,0);
            }
            else
            {
#if 1

                extern void send_jpeg_by_uart(camera_fb_t *cam);
                send_jpeg_by_uart(pic);
#else
                extern void send_jpeg_by_eth(camera_fb_t *cam);
                send_jpeg_by_eth(pic);
#endif
            }

            esp_camera_fb_return(pic);
        }

        os_task_msleep(10);
    }

}

static os_err_t create_cam_app(void)
{
    os_task_id task;
    task = os_task_create(OS_NULL, OS_NULL, 4096, "cam", app_main, NULL, 3);
    OS_ASSERT(task);
    os_task_startup(task);
    return OS_SUCCESS;
}
OS_INIT_CALL(create_cam_app, OS_INIT_LEVEL_APPLICATION, OS_INIT_SUBLEVEL_LOW);
#endif
