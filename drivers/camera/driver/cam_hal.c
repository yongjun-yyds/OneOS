// Copyright 2010-2020 Espressif Systems (Shanghai) PTE LTD
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <stdio.h>
#include <string.h>
#include "ll_cam.h"
#include "cam_hal.h"
#include <dlog.h>
#include <clocksource.h>
#include <stdlib.h>
#include <os_memory.h>
#include <drv_dcmi.h>
#include <os_clock.h>

#define DRV_EXT_TAG "cam_hal"

static cam_obj_t *cam_obj = NULL;

static const uint32_t JPEG_SOI_MARKER = 0xFFD8FF;    // written in little-endian for esp32
static const uint16_t JPEG_EOI_MARKER = 0xD9FF;      // written in little-endian for esp32

int cam_verify_jpeg_soi(const uint8_t *inbuf, uint32_t length)
{
    uint32_t sig = *((uint32_t *)inbuf) & 0xFFFFFF;
    if (sig != JPEG_SOI_MARKER)
    {
        for (uint32_t i = 0; i < length; i++)
        {
            sig = *((uint32_t *)(&inbuf[i])) & 0xFFFFFF;
            if (sig == JPEG_SOI_MARKER)
            {
                LOG_I(DRV_EXT_TAG, "SOI: %d", i);
                return i;
            }
        }
        LOG_I(DRV_EXT_TAG, "NO-SOI");
        return -1;
    }
    return 0;
}

int cam_verify_jpeg_eoi(const uint8_t *inbuf, uint32_t length)
{
    int         offset = -1;
    uint8_t *dptr   = (uint8_t *)inbuf;
    while (dptr < inbuf + length)
    {
        uint16_t sig = *((uint16_t *)dptr);
        if (JPEG_EOI_MARKER == sig)
        {
            offset = dptr - inbuf + 2;
            LOG_I(DRV_EXT_TAG, "EOI: %d", length - (offset + 2));
            return offset;
        }
        dptr++;
    }
    return -1;
}


/*
int cam_verify_jpeg_eoi(const uint8_t *inbuf, uint32_t length)
{
    int         offset = -1;
    uint8_t *dptr   = (uint8_t *)inbuf + length - 2;
    while (dptr > inbuf)
    {
        uint16_t sig = *((uint16_t *)dptr);
        if (JPEG_EOI_MARKER == sig)
        {
            offset = dptr - inbuf;
            LOG_I(DRV_EXT_TAG, "EOI: %d", length - (offset + 2));
            return offset;
        }
        dptr--;
    }
    return -1;
}
*/
static bool cam_get_next_frame(int *frame_pos)
{
    if (!cam_obj->frames[*frame_pos].en)
    {
        for (int x = 0; x < cam_obj->frame_cnt; x++)
        {
            if (cam_obj->frames[x].en)
            {
                *frame_pos = x;
                return OS_TRUE;
            }
        }
    }
    else
    {
        return OS_TRUE;
    }
    return OS_FALSE;
}

static os_bool_t cam_start_frame(int *frame_pos)
{
    if (cam_get_next_frame(frame_pos))
    {
        cam_start();
        if (!ll_cam_dma_start(cam_obj))
        {
            return OS_TRUE;
        }
    }

    return OS_FALSE;
}

void ll_cam_send_event(cam_obj_t *cam, cam_sem_t cam_sem)
{
    if (os_mailbox_send(cam_obj->irq_sem, (os_ubase_t)cam_sem, 0) != OS_SUCCESS)
    {
        cam->state = CAM_STATE_IDLE;
    }
}

// Copy frame from DMA dma_buffer to frame dma_buffer
static os_err_t dma_data_to_fb(camera_fb_t *fb_t, os_ubase_t cam_sem)
{
    OS_ASSERT(cam_obj != OS_NULL);

    os_err_t ret = OS_SUCCESS;
    /* check if the receive length is exceed the fb_size */
    if (cam_obj->fb_size < (fb_t->len + cam_obj->dma_half_buffer_size))
    {
        LOG_W(DRV_EXT_TAG, "FB-OVF,fb space is too small");
        ll_cam_dma_stop(cam_obj);
        cam_obj->state = CAM_STATE_IDLE;
        ret            = OS_NOMEM;
    }
    else /* copy the data from the dma_buffer */
    {
        fb_t->len += ll_cam_memcpy(cam_obj,
                                   &fb_t->buf[fb_t->len],
                                   (cam_obj->dma_buffer + cam_obj->dma_half_buffer_size * (cam_sem % 2)),
                                   cam_obj->dma_half_buffer_size);
    }
    return ret;
}

static os_err_t send_fb_to_app(os_ubase_t fb_addr)
{
    os_err_t ret = OS_SUCCESS;

    if (os_mailbox_send(cam_obj->frame_buffer_queue, fb_addr, OS_NO_WAIT) != OS_SUCCESS)
    {
        // pop frame buffer from the queue
        camera_fb_t *fb2 = NULL;
        if (os_mailbox_recv(cam_obj->frame_buffer_queue, (os_ubase_t *)&fb2, 0) == OS_SUCCESS)
        {
            // push the new frame to the end of the queue
            if (os_mailbox_send(cam_obj->frame_buffer_queue, fb_addr, 0) != OS_SUCCESS)
            {
                LOG_E(DRV_EXT_TAG, "FBQ-SND");
                ret = OS_FAILURE;
            }
            // free the popped buffer
            cam_give(fb2);
        }
        else
        {
            // queue is full and we could not pop a frame from it
            LOG_E(DRV_EXT_TAG, "FBQ-RCV");
            ret = OS_FAILURE;
        }
    }
    return ret;
}

#if 0
static void cam_task(void *arg)
{
    int        cnt       = 0;
    int        frame_pos = 0;
    os_ubase_t cam_sem;

    cam_obj->state = CAM_STATE_IDLE;
    if (cam_obj->state == CAM_STATE_IDLE)
    {
        if (cam_start_frame(&frame_pos))
        {
            cam_obj->frames[frame_pos].fb.len = 0;
            cam_obj->state                    = CAM_STATE_READ_BUF;
        }
        cnt = 0;
    }

    while (1)
    {
        os_mailbox_recv(cam_obj->irq_sem, &cam_sem, OS_WAIT_FOREVER);

        if (cam_obj->state == CAM_STATE_IDLE)
        {
            if (cam_sem > CAM_SEM_DMA_BUF1)
            {
                if (cam_start_frame(&frame_pos))
                {
                    cam_obj->frames[frame_pos].fb.len = 0;
                    cam_obj->state                    = CAM_STATE_READ_BUF;
                    cnt                               = 0;
                }
            }
            continue;
        }

        else
        {
            camera_fb_t *frame_buffer_t = &cam_obj->frames[frame_pos].fb;
            if (cnt == 0)
            {
                uint64_t us                                     = (uint64_t)os_clocksource_gettime();
                cam_obj->frames[frame_pos].fb.timestamp.tv_sec  = us / 1000000UL;
                cam_obj->frames[frame_pos].fb.timestamp.tv_usec = us % 1000000UL;
            }

            if (cam_sem < CAM_SEM_VSYNC_BUF0)    // dma_irq
            {
                if (dma_data_to_fb(frame_buffer_t, cam_sem) != OS_SUCCESS)
                    continue;
                /* Check for JPEG SOI in the first buffer. stop if not found */
                if (cam_obj->jpeg_mode && cnt == 0 &&
                    (cam_verify_jpeg_soi(frame_buffer_t->buf, frame_buffer_t->len) < 0))
                {
                    ll_cam_dma_stop(cam_obj);
                    cam_obj->state = CAM_STATE_IDLE;
                    continue;
                }
                cnt++;
            }
            else    // frame irq
            {
                cam_obj->frames[frame_pos].en = 0;

                if (!cam_obj->jpeg_mode && frame_buffer_t->len != cam_obj->fb_size)
                {
                    cam_obj->frames[frame_pos].en = 1;
                    LOG_E(DRV_EXT_TAG, "FB-SIZE: (recv)%u != %u(fb)", frame_buffer_t->len, cam_obj->fb_size);
                }

               if (cnt && !cam_obj->frames[frame_pos].en)    // notify the app
               {
                    if (send_fb_to_app((os_ubase_t)frame_buffer_t) != OS_SUCCESS)
                        cam_obj->frames[frame_pos].en = 1;

                    if (!cam_get_next_frame(&frame_pos))
                    {
                        ll_cam_dma_stop(cam_obj);
                        cam_obj->state = CAM_STATE_IDLE;
                    }
                    else
                    {

                        cam_start();
                        ll_cam_dma_stop(cam_obj);
                        ll_cam_dma_start(cam_obj);
                        
                        cam_obj->frames[frame_pos].fb.len = 0;
                    }
              }
             cnt = 0;   
           }
       }
   }
}
#endif
static void cam_task(void *arg)
{
    int        frame_pos = 0;
    os_ubase_t cam_sem;

    cam_obj->state = CAM_STATE_IDLE;
    if (cam_obj->state == CAM_STATE_IDLE)
    {
        if (cam_start_frame(&frame_pos))
        {
            cam_obj->frames[frame_pos].fb.len = 0;
            cam_obj->state                    = CAM_STATE_READ_BUF;
        }
    }

    while (1)
    {
        os_mailbox_recv(cam_obj->irq_sem, &cam_sem, OS_WAIT_FOREVER);

        if (cam_obj->state == CAM_STATE_IDLE)
        {
            if (cam_sem > CAM_SEM_DMA_BUF1)
            {
                if (cam_start_frame(&frame_pos))
                {
                    cam_obj->frames[frame_pos].fb.len = 0;
                    cam_obj->state                    = CAM_STATE_READ_BUF;
                }
            }
            continue;
        }

        else
        {
            camera_fb_t *frame_buffer_t = &cam_obj->frames[frame_pos].fb;

            if (cam_sem < CAM_SEM_VSYNC_BUF0)    // dma_irq
            {
                if (dma_data_to_fb(frame_buffer_t, cam_sem) != OS_SUCCESS)
                    continue;
            }
            else    // frame irq
            {
                cam_obj->frames[frame_pos].en = 0;

                if (!cam_obj->jpeg_mode && frame_buffer_t->len != cam_obj->fb_size)
                {
                    cam_obj->frames[frame_pos].en = 1;
                    LOG_E(DRV_EXT_TAG, "FB-SIZE: (recv)%u != %u(fb)", frame_buffer_t->len, cam_obj->fb_size);
                }
                if(cam_obj->jpeg_mode)
                {
                    if(cam_verify_jpeg_soi(frame_buffer_t->buf, frame_buffer_t->len) <0 || 
                        cam_verify_jpeg_eoi(frame_buffer_t->buf, frame_buffer_t->len)<0 )
                            cam_obj->frames[frame_pos].en = 1;
                }

                if (!cam_obj->frames[frame_pos].en)    // notify the app
                {
                    if (send_fb_to_app((os_ubase_t)frame_buffer_t) != OS_SUCCESS)
                        cam_obj->frames[frame_pos].en = 1;
                }
                if (!cam_get_next_frame(&frame_pos))
                {
                    ll_cam_dma_stop(cam_obj);
                    cam_obj->state = CAM_STATE_IDLE;
                }
                else
                {
                    cam_start();
                    ll_cam_dma_stop(cam_obj);
                    ll_cam_dma_start(cam_obj);
                    
                    cam_obj->frames[frame_pos].fb.len = 0;
                }
           }
       }
   }
}



static uint8_t dcmi_line_buf[2][LCD_CAM_DMA_NODE_BUFFER_MAX_SIZE];
static os_err_t cam_dma_config(const camera_config_t *config)
{
    bool ret = ll_cam_dma_sizes(cam_obj);
    if (0 == ret)
    {
        return OS_FAILURE;
    }

    cam_obj->dma_node_cnt = (cam_obj->dma_buffer_size) / cam_obj->dma_node_buffer_size;    // Number of DMA nodes
    cam_obj->frame_copy_cnt =
        cam_obj->recv_size / cam_obj->dma_half_buffer_size;    // Number of interrupted copies, ping-pong copy

    LOG_I(DRV_EXT_TAG,
          "buffer_size: %d, half_buffer_size: %d, node_buffer_size: %d, node_cnt: %d, total_cnt: %d",
          cam_obj->dma_buffer_size,
          cam_obj->dma_half_buffer_size,
          cam_obj->dma_node_buffer_size,
          cam_obj->dma_node_cnt,
          cam_obj->frame_copy_cnt);

    cam_obj->dma_buffer = NULL;

    cam_obj->frames = (cam_frame_t *)os_calloc(1, cam_obj->frame_cnt * sizeof(cam_frame_t));
    CAM_CHECK(cam_obj->frames != NULL, "frames malloc failed", OS_FAILURE);

    uint8_t dma_align = 0;
    size_t  fb_size   = cam_obj->fb_size;
    if (cam_obj->psram_mode)
    {
        dma_align = ll_cam_get_dma_align(cam_obj);
        if (cam_obj->fb_size < cam_obj->recv_size)
        {
            fb_size = cam_obj->recv_size;
        }
    }

    /* Allocate memory for frame buffer */
    size_t alloc_size = fb_size * sizeof(uint8_t) + dma_align;
    for (int x = 0; x < cam_obj->frame_cnt; x++)
    {
        cam_obj->frames[x].fb_offset = 0;
        cam_obj->frames[x].en        = 0;
        LOG_I(DRV_EXT_TAG, "Allocating %d Byte", alloc_size);
        cam_obj->frames[x].fb.buf = (uint8_t *)os_dma_malloc_align(alloc_size, BSP_CACHE_LINE_SIZE);
        CAM_CHECK(cam_obj->frames[x].fb.buf != NULL, "frame buffer malloc failed", OS_FAILURE);

        cam_obj->frames[x].en = 1;
    }

    cam_obj->dma_buffer = (uint8_t *)&dcmi_line_buf;
    if (NULL == cam_obj->dma_buffer)
    {
        LOG_E(DRV_EXT_TAG,
              "%s(%d): DMA buffer %d Byte malloc failed",
              __FUNCTION__,
              __LINE__,
              cam_obj->dma_buffer_size);
        return OS_FAILURE;
    }

    return OS_SUCCESS;
}

os_err_t cam_init(const camera_config_t *config)
{
    CAM_CHECK(NULL != config, "config pointer is invalid", OS_EMPTY);

    os_err_t ret = OS_SUCCESS;
    cam_obj      = (cam_obj_t *)os_calloc(1, sizeof(cam_obj_t));
    CAM_CHECK(NULL != cam_obj, "lcd_cam object malloc error", OS_NOMEM);

    cam_obj->swap_data    = 0;
    cam_obj->vsync_pin    = config->pin_vsync;
    cam_obj->vsync_invert = true;

    ll_cam_set_pin(cam_obj, config);
    ret = ll_cam_config(cam_obj, config);
    CAM_CHECK_GOTO(ret == OS_SUCCESS, "ll_cam initialize failed", err);

    LOG_I(DRV_EXT_TAG, "cam init ok");
    return OS_SUCCESS;

err:
    free(cam_obj);
    cam_obj = NULL;
    return OS_FAILURE;
}

os_err_t cam_config(const camera_config_t *config, framesize_t frame_size, uint16_t sensor_pid)
{
    CAM_CHECK(NULL != config, "config pointer is invalid", OS_FAILURE);
    os_err_t ret = OS_SUCCESS;

    ret = ll_cam_set_sample_mode(cam_obj, (pixformat_t)config->pixel_format, config->xclk_freq_hz, sensor_pid);

    cam_obj->jpeg_mode  = config->pixel_format == PIXFORMAT_JPEG;
    cam_obj->psram_mode = (config->xclk_freq_hz == 16000000);

    cam_obj->frame_cnt = config->fb_count;
    cam_obj->width     = resolution[frame_size].width;
    cam_obj->height    = resolution[frame_size].height;

    if (cam_obj->jpeg_mode)
    {
        cam_obj->recv_size = cam_obj->width * cam_obj->height / 2;
        cam_obj->fb_size   = cam_obj->recv_size;
    }
    else
    {
        cam_obj->recv_size = cam_obj->width * cam_obj->height * cam_obj->in_bytes_per_pixel;
        cam_obj->fb_size   = cam_obj->width * cam_obj->height * cam_obj->fb_bytes_per_pixel;
    }

    size_t frame_buffer_queue_len = cam_obj->frame_cnt;
    ret                           = cam_dma_config(config);
    CAM_CHECK_GOTO(ret == OS_SUCCESS, "cam_dma_config failed", err);

    cam_obj->irq_sem = os_mailbox_create_dynamic("irq_sem", 2);

    CAM_CHECK_GOTO(cam_obj->irq_sem != NULL, "irq_sem create failed", err);

    if (config->grab_mode == CAMERA_GRAB_LATEST && cam_obj->frame_cnt > 1)
    {
        frame_buffer_queue_len = cam_obj->frame_cnt - 1;
    }

    cam_obj->frame_buffer_queue = os_mailbox_create_dynamic("frame_buf", frame_buffer_queue_len);
    CAM_CHECK_GOTO(cam_obj->frame_buffer_queue != NULL, "frame_buffer_queue create failed", err);

    ret = cam_dcmi_irq_init();
    CAM_CHECK_GOTO(ret == OS_SUCCESS, "cam intr alloc failed", err);

    cam_obj->task_handle = os_task_create(OS_NULL, OS_NULL, 4096, "cam_task", cam_task, NULL, 3);
    OS_ASSERT(cam_obj->task_handle);
    os_task_startup(cam_obj->task_handle);

    LOG_I(DRV_EXT_TAG, "cam config ok");
    return OS_SUCCESS;

err:
    cam_deinit();
    return OS_FAILURE;
}

os_err_t cam_deinit(void)
{
    if (!cam_obj)
    {
        os_kprintf("deinit error\r\n");
        return OS_FAILURE;
    }

    ll_cam_dma_stop(cam_obj);
    cam_stop();
    if (cam_obj->task_handle)
    {
        os_task_destroy(cam_obj->task_handle);
    }
    if (cam_obj->irq_sem)
    {
        // os_sem_destroy(cam_obj->irq_sem);
        os_mailbox_destroy(cam_obj->irq_sem);
    }
    if (cam_obj->frame_buffer_queue)
    {
        os_mailbox_destroy(cam_obj->frame_buffer_queue);
    }
    if (cam_obj->dma_buffer)
    {
        os_dma_free_align(cam_obj->dma_buffer);
    }
    if (cam_obj->frames)
    {
        for (int x = 0; x < cam_obj->frame_cnt; x++)
        {
            os_dma_free_align(cam_obj->frames[x].fb.buf - cam_obj->frames[x].fb_offset);
        }
        free(cam_obj->frames);
    }

    ll_cam_deinit(cam_obj);

    free(cam_obj);
    cam_obj = NULL;
    os_kprintf("deinit ok\r\n");
    return OS_SUCCESS;
}

void cam_stop(void)
{
    dcmi_stop();
}

void cam_start(void)
{
    dcmi_start();
}

camera_fb_t *cam_take(uint32_t timeout)
{
    camera_fb_t *dma_buffer = NULL;
    uint32_t  start      = os_tick_get_value();
    os_mailbox_recv(cam_obj->frame_buffer_queue, (os_ubase_t *)&dma_buffer, timeout);
    if (dma_buffer)
    {
        if (cam_obj->jpeg_mode)
        {
            // find the end marker for JPEG. Data after that can be discarded
            int offset_s = cam_verify_jpeg_soi(dma_buffer->buf, dma_buffer->len);
            int offset_e = cam_verify_jpeg_eoi(dma_buffer->buf, dma_buffer->len);
            if (offset_s >= 0 && offset_e >= 0)
            {
                // adjust buffer length
                dma_buffer->len = offset_e + sizeof(JPEG_EOI_MARKER);
                return dma_buffer;
            }
            else
            {
                LOG_W(DRV_EXT_TAG, "NO-EOI,offset_s:%d,offset_e:%d",offset_s,offset_e);
                cam_give(dma_buffer);
                return cam_take(timeout - (os_tick_get_value() - start));    // recurse!!!!
            }
        }
        return dma_buffer;
    }
    else
    {
        LOG_W(DRV_EXT_TAG, "Failed to get the frame on time!");
    }
    return NULL;
}

void cam_give(camera_fb_t *dma_buffer)
{
    for (int x = 0; x < cam_obj->frame_cnt; x++)
    {
        if (&cam_obj->frames[x].fb == dma_buffer)
        {
            memset(dma_buffer->buf, 0, cam_obj->fb_size);
            os_clean_dcache(dma_buffer->buf, cam_obj->fb_size);
            cam_obj->frames[x].en = 1;
            break;
        }
    }
}
