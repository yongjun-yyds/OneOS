#ifndef __XCLK_H__
#define __XCLK_H__
#include <board.h>
#include "include/camera.h"
os_err_t xclk_timer_conf(int ledc_timer, int xclk_freq_hz);

os_err_t camera_enable_out_clock(camera_config_t* config);

void camera_disable_out_clock(void);
#endif
