// Copyright 2010-2020 Espressif Systems (Shanghai) PTE LTD
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "include/camera.h"
#include <os_errno.h>
#include <stdint.h>


#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Uninitialize the lcd_cam module
 *
 * @param handle Provide handle pointer to release resources
 *
 * @return
 *     - OS_SUCCESS Success
 *     - OS_FAILURE Uninitialize fail
 */
os_err_t cam_deinit(void);

/**
 * @brief Initialize the lcd_cam module
 *
 * @param config Configurations - see lcd_cam_config_t struct
 *
 * @return
 *     - OS_SUCCESS Success
 *     - OS_FAILURE Parameter error
 *     - OS_NOMEM No memory to initialize lcd_cam
 *     - OS_FAILURE Initialize fail
 */
os_err_t cam_init(const camera_config_t *config);
os_err_t cam_deinit(void);

os_err_t cam_config(const camera_config_t *config, framesize_t frame_size, uint16_t sensor_pid);
int cam_verify_jpeg_soi(const uint8_t *inbuf, uint32_t length);

void cam_stop(void);

void cam_start(void);

camera_fb_t *cam_take(uint32_t timeout);


void cam_give(camera_fb_t *dma_buffer);

#ifdef __cplusplus
}
#endif
