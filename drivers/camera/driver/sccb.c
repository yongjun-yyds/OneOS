/*
 * This file is part of the OpenMV project.
 * Copyright (c) 2013/2014 Ibrahim Abdelkader <i.abdalkader@gmail.com>
 * This work is licensed under the MIT license, see the file LICENSE for details.
 *
 * SCCB (I2C like) driver.
 *
 */
#include <stdbool.h>
#include <string.h>
#include "sccb.h"
#include "include/sensor.h"
#include <stdio.h>
#include "i2c.h"
#include <stdint.h>
#include <stdlib.h>
#include <os_errno.h>

#define DBG_TAG "drv.sccb"
struct os_i2c_client *cam_sccb;

int SCCB_Init(const char *sccb_name)
{
    cam_sccb = os_calloc(1, sizeof(struct os_i2c_client));
    if (cam_sccb == OS_NULL)
    {
        LOG_E(DBG_TAG, "cam_sccb amlloc faile");
        return OS_FAILURE;
    }

    cam_sccb->bus = os_i2c_bus_device_find(sccb_name);
    if (cam_sccb->bus == OS_NULL)
    {
        LOG_E(DBG_TAG, "cam_sccb i2c invalid [%s].", sccb_name);
        os_free(cam_sccb);
        return OS_FAILURE;
    }
    return OS_SUCCESS;
}

int SCCB_Deinit(void)
{
    cam_sccb->client_addr = 0;
    return 0;
}

uint8_t SCCB_Probe(uint16_t addr)
{
    cam_sccb->client_addr = addr;
    return addr;
}

uint8_t SCCB_Read(uint8_t slv_addr, uint8_t reg)
{
    uint8_t val = 0;
    os_i2c_client_write(cam_sccb, reg, 1, OS_NULL, 0);
    os_i2c_client_read(cam_sccb, 0, 0, &val, 1);
    return val;
}

uint8_t SCCB_Write(uint8_t slv_addr, uint8_t reg, uint8_t data)
{
    os_err_t   ret  = OS_SUCCESS;
    uint8_t buff = data;
    ret             = os_i2c_client_write(cam_sccb, reg, 1, &buff, 1);
    return ret;
}

uint8_t SCCB_Read16(uint8_t slv_addr, uint16_t reg)
{
    uint8_t data     = 0;
    uint16_t   reg_addr = (uint16_t)(reg >> 8 | reg << 8);
    os_i2c_client_write(cam_sccb, reg_addr, 2, OS_NULL, 0);
    os_i2c_client_read(cam_sccb, 0, 0, &data, 1);
    return data;
}

uint8_t SCCB_Write16(uint8_t slv_addr, uint16_t reg, uint8_t data)
{
    os_err_t   ret      = OS_SUCCESS;
    uint8_t buff     = data;
    uint16_t   reg_addr = (uint16_t)(reg >> 8 | reg << 8);
    ret                 = os_i2c_client_write(cam_sccb, reg_addr, 2, &buff, 1);
    return ret;
}
