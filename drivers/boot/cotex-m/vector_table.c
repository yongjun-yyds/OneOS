/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        vector_table.c
 *
 * @brief       This file provides vector_table for cortex-m.
 *
 * @revision
 * Date         Author          Notes
 * 2021-09-23   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <board.h>
#include <drv_common.h>
#include <arch_interrupt.h>

#ifdef OS_USING_IRQ_MONITOR
#include <cpu_monitor.h>
#endif

int OS_USED OS_SECTION("reserved_ram") rsv_ram_start = 0;

#define RESERVED_RAM_VECTOR_ADDR (&rsv_ram_start + 1)
#define RESERVED_RAM_VECTOR_HOOK (&rsv_ram_start + 2)

#if !defined(CORTEX_M_VECTORS_BASE) && !defined(CORTEX_M_STACK_END)
#if defined(__CC_ARM) || defined(__CLANG_ARM)
extern int __Vectors;
extern int STACK$$Limit;
extern int STACK$$Base;

#define CORTEX_M_VECTORS_BASE __Vectors
#define CORTEX_M_STACK_END    STACK$$Limit
#define CORTEX_M_STACK_SIZE   ((unsigned long)&STACK$$Limit - (unsigned long)&STACK$$Base)
#elif defined(__ICCARM__) || defined(__ICCRX__)
#error : not support iar
#elif defined(__GNUC__)
extern int g_pfnVectors;
extern int _sstack;
extern int _estack;

#define CORTEX_M_VECTORS_BASE g_pfnVectors
#define CORTEX_M_STACK_END    _estack
#define CORTEX_M_STACK_SIZE   ((unsigned long)&_estack - (unsigned long)&_sstack)
#endif
#endif

typedef void (*vector_entry)(void);

static const vector_entry vector_table[];

static void cortex_m_irq_hook(int irq)
{
#if defined(__CC_ARM) || defined(__CLANG_ARM)
    extern const int os_irq_hook$$Base;
    extern const int os_irq_hook$$Limit;

    const os_irq_hook *hook = (os_irq_hook *)&os_irq_hook$$Base;
    int                nr   = (os_irq_hook *)&os_irq_hook$$Limit - hook;

#elif defined(__ICCARM__) || defined(__ICCRX__) /* for IAR Compiler */
    const os_irq_hook *hook = (os_irq_hook *)__section_begin("os_irq_hook");
    int                nr   = (os_irq_hook *)__section_end("os_irq_hook") - hook;
#elif defined(__GNUC__)                         /* for GCC Compiler */
    extern const int   __os_irq_hook_start;
    extern const int   __os_irq_hook_end;
    const os_irq_hook *hook = (os_irq_hook *)&__os_irq_hook_start;
    int                nr   = (os_irq_hook *)&__os_irq_hook_end - hook;
#endif

    int i;

    for (i = 0; i < nr; i++)
    {
        hook[i](irq);
    }
}

static os_err_t cortex_m_set_vector(void)
{
#if defined(BSP_INCLUDE_VECTOR_TABLE) && !defined(ARCH_ARM_CORTEX_M0)
    SCB->VTOR = (uint32_t)vector_table;
#endif

    *(volatile uint32_t *)RESERVED_RAM_VECTOR_ADDR = (uint32_t)&CORTEX_M_VECTORS_BASE;

    *(volatile uint32_t *)RESERVED_RAM_VECTOR_HOOK = (uint32_t)&cortex_m_irq_hook;

    interrupt_stack_addr = &CORTEX_M_STACK_END;

    interrupt_stack_size = CORTEX_M_STACK_SIZE;

#ifndef BSP_USING_COMMON_INT
    SCB->VTOR = *(volatile uint32_t *)RESERVED_RAM_VECTOR_ADDR;
#endif

    return OS_SUCCESS;
}

OS_INIT_CALL(cortex_m_set_vector, OS_INIT_LEVEL_PRE_KERNEL_1, OS_INIT_SUBLEVEL_HIGH);

static void cortex_m_vector_entry(void)
{
    vector_entry *vtable;
    os_irq_hook   hook;

#ifdef OS_USING_IRQ_MONITOR
    cpu_usage_irq_monitor_start();
#endif

    int irq = os_irq_num();

    if (irq == 0)
    {
        irq = 1;
        cortex_m_set_vector();
    }
    else
    {
        hook = (os_irq_hook) * (uint32_t *)RESERVED_RAM_VECTOR_HOOK;
        hook(irq);
    }

    vtable = (vector_entry *)*(uint32_t *)RESERVED_RAM_VECTOR_ADDR;
    vtable[irq]();

    os_interrupt_stack_check();

#ifdef OS_USING_IRQ_MONITOR
    cpu_usage_irq_monitor_stop();
#endif
}

#ifdef BSP_INCLUDE_VECTOR_TABLE

#if defined(__CC_ARM)

/* clang-format off */
__asm static void ___PendSV_Handler(void)
{
    import rsv_ram_start

    ldr r0, =rsv_ram_start
    ldr r0, [r0, #4]
    ldr r0, [r0, #0x38]
    bx  r0
}

__asm static void ___NMI_Handler(void)
{
    import rsv_ram_start

    ldr r0, =rsv_ram_start
    ldr r0, [r0, #4]
    ldr r0, [r0, #0x08]
    bx  r0
}

__asm static void ___HardFault_Handler(void)
{
    import rsv_ram_start

    ldr r0, =rsv_ram_start
    ldr r0, [r0, #4]
    ldr r0, [r0, #0x0c]
    bx  r0
}

__asm static void ___MemManage_Handler(void)
{
    import rsv_ram_start

    ldr r0, =rsv_ram_start
    ldr r0, [r0, #4]
    ldr r0, [r0, #0x10]
    bx  r0
}

__asm static void ___BusFault_Handler(void)
{
    import rsv_ram_start

    ldr r0, =rsv_ram_start
    ldr r0, [r0, #4]
    ldr r0, [r0, #0x14]
    bx  r0
}

__asm static void ___UsageFault_Handler(void)
{
    import rsv_ram_start

    ldr r0, =rsv_ram_start
    ldr r0, [r0, #4]
    ldr r0, [r0, #0x18]
    bx  r0
}

#elif defined(__ICCARM__) || defined(__ICCRX__) /* for IAR Compiler */
#error : not support iar
#elif defined(__GNUC__) || defined(__CLANG_ARM)

void __PendSV_Handler(void)
{
    __asm__ __volatile__(
        ".global ___PendSV_Handler\n"
        ".type   ___PendSV_Handler, %function\n"
        "\n"
        "___PendSV_Handler:\n"
        "   ldr r0, =rsv_ram_start\n"
        "   ldr r0, [r0, #4]\n"
        "   ldr r0, [r0, #0x38]\n"
        "   bx  r0\n");
}

void __NMI_Handler(void)
{
    __asm__ __volatile__(
        ".global ___NMI_Handler\n"
        ".type   ___NMI_Handler, %function\n"
        "\n"
        "___NMI_Handler:\n"
        "   ldr r0, =rsv_ram_start\n"
        "   ldr r0, [r0, #4]\n"
        "   ldr r0, [r0, #0x08]\n"
        "   bx  r0\n");
}

void __HardFault_Handler(void)
{
    __asm__ __volatile__(
        ".global ___HardFault_Handler\n"
        ".type   ___HardFault_Handler, %function\n"
        "\n"
        "___HardFault_Handler:\n"
        "   ldr r0, =rsv_ram_start\n"
        "   ldr r0, [r0, #4]\n"
        "   ldr r0, [r0, #0x0c]\n"
        "   bx  r0\n");
}

void __MemManage_Handler(void)
{
    __asm__ __volatile__(
        ".global ___MemManage_Handler\n"
        ".type   ___MemManage_Handler, %function\n"
        "\n"
        "___MemManage_Handler:\n"
        "   ldr r0, =rsv_ram_start\n"
        "   ldr r0, [r0, #4]\n"
        "   ldr r0, [r0, #0x10]\n"
        "   bx  r0\n");
}

void __BusFault_Handler(void)
{
    __asm__ __volatile__(
        ".global ___BusFault_Handler\n"
        ".type   ___BusFault_Handler, %function\n"
        "\n"
        "___BusFault_Handler:\n"
        "   ldr r0, =rsv_ram_start\n"
        "   ldr r0, [r0, #4]\n"
        "   ldr r0, [r0, #0x14]\n"
        "   bx  r0\n");
}

void __UsageFault_Handler(void)
{
    __asm__ __volatile__(
        ".global ___UsageFault_Handler\n"
        ".type   ___UsageFault_Handler, %function\n"
        "\n"
        "___UsageFault_Handler:\n"
        "   ldr r0, =rsv_ram_start\n"
        "   ldr r0, [r0, #4]\n"
        "   ldr r0, [r0, #0x18]\n"
        "   bx  r0\n");
}

/* clang-format on */
void ___PendSV_Handler(void);
void ___NMI_Handler(void);
void ___HardFault_Handler(void);
void ___MemManage_Handler(void);
void ___BusFault_Handler(void);
void ___UsageFault_Handler(void);

#endif /* __CC_ARM */
#endif /* BSP_INCLUDE_VECTOR_TABLE */

static const vector_entry OS_USED OS_SECTION("vtor_table") vector_table[] = {
    (vector_entry)(&CORTEX_M_STACK_END), /* system stack */
    cortex_m_vector_entry,               /* reset */

#ifdef BSP_INCLUDE_VECTOR_TABLE
    (vector_entry)(&___NMI_Handler),
    (vector_entry)(&___HardFault_Handler),
    (vector_entry)(&___MemManage_Handler),
    (vector_entry)(&___BusFault_Handler),
    (vector_entry)(&___UsageFault_Handler),
    cortex_m_vector_entry,
    0,
    0,
    0,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    0,
    (vector_entry)(&___PendSV_Handler),
    cortex_m_vector_entry,

#ifdef BSP_VECTOR_TABLE_INCLUDE_EXTERNAL_INT
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
#if !defined(ARCH_ARM_CORTEX_M0) && !defined(ARCH_ARM_CORTEX_M0_PLUS)
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
    cortex_m_vector_entry,
#endif
#endif
#endif
};
