#ifndef _USB_DEVICE_H_
#define _USB_DEVICE_H_


#include "proto_usbd_def.h"

#ifdef ENABLE_USBD_CDC_CLASS
#include "usbd_cdc_desc.h"
#include "usbd_cdc.h"
#endif

struct usbd
{
    const char *name;
    USBD_DescriptorsTypeDef *usbd_desc;
    USBD_ClassTypeDef *usbd_class;
    uint32_t class_data_size;
    uint8_t interface_type;
};


#endif
