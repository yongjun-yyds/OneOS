/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with 
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_uart.c
 *
 * @brief       This file implements uart driver for nrf5
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include <stdio.h>
#include <bus/bus.h>
#include <dma/dma.h>
#include <board.h>
#include <os_memory.h>
#include "drv_uart.h"

#include "os_stddef.h"

#include "uart.h"
#include "uart_pub.h"
#include "icu_pub.h"
#include "drv_model_pub.h"

#ifndef REG_READ
#define REG_READ(addr)          (*((volatile UINT32 *)(addr)))
#endif

#ifndef REG_WRITE
#define REG_WRITE(addr, _data) 	(*((volatile UINT32 *)(addr)) = (_data))
#endif


typedef struct bk_uart
{
    struct os_serial_device    serial_dev;
    struct beken_uart_info     *huart;

    soft_dma_t                 sdma;

    uint8_t                 *rx_buff;
    uint32_t                 rx_index;
    uint32_t                 rx_size;

    const uint8_t           *tx_buff;
    uint32_t                 tx_count;
    uint32_t                 tx_size;

    os_list_node_t              list;
}bk_uart_t;

static os_list_node_t bk_uart_list = OS_LIST_INIT(bk_uart_list);

void beken_uart_isr_rx(unsigned char uport)
{
    struct bk_uart *uart = OS_NULL;

    os_list_for_each_entry(uart, &bk_uart_list, struct bk_uart, list)
    {
        if (uart->huart->port == uport){
            while(REG_READ(uart->huart->fifo_status) & FIFO_RD_READY)
            {
                uart->rx_buff[uart->rx_index++] = (uint8_t)uart_read_byte(uport);

                if (uart->rx_index == (uart->rx_size / 2))
                {
                    soft_dma_half_irq(&uart->sdma);
                }

                if (uart->rx_index == uart->rx_size)
                {
                    uart->rx_index = 0;
                    soft_dma_full_irq(&uart->sdma);
                }
            }
        }
    }
}

static uint32_t bk_sdma_int_get_index(soft_dma_t *dma)
{
    bk_uart_t *uart = os_container_of(dma, bk_uart_t, sdma);

    return uart->rx_index;
}

static os_err_t bk_sdma_int_init(soft_dma_t *dma)
{
    return OS_SUCCESS;
}

static os_err_t bk_sdma_int_start(soft_dma_t *dma, void *buff, uint32_t size)
{
    uint32_t  reg_val  = 0;
    bk_uart_t *uart    = os_container_of(dma, bk_uart_t, sdma);

    uart->rx_buff  = buff;
    uart->rx_index = 0;
    uart->rx_size  = size;

    reg_val = REG_READ(uart->huart->inter_reg_addr);
    reg_val |= (RX_FIFO_NEED_READ_EN | UART_RX_STOP_END_EN);
    REG_WRITE(uart->huart->inter_reg_addr, reg_val);

    sddev_control(ICU_DEV_NAME, CMD_ICU_INT_ENABLE, &uart->huart->irq_uart_bit);

    return OS_SUCCESS;
}

static uint32_t bk_sdma_int_stop(soft_dma_t *dma)
{
    uint32_t  reg_val  = 0;
    bk_uart_t *uart = os_container_of(dma, bk_uart_t, sdma);

    reg_val = REG_READ(uart->huart->inter_reg_addr);
    reg_val &= ~(RX_FIFO_NEED_READ_EN | UART_RX_STOP_END_EN);
    REG_WRITE(uart->huart->inter_reg_addr, reg_val);

    return uart->rx_index;
}

/* sdma callback */
static void bk_uart_sdma_callback(soft_dma_t *dma)
{
    bk_uart_t *uart = os_container_of(dma, bk_uart_t, sdma);
    
    os_hw_serial_isr_rxdone((struct os_serial_device *)uart);
}

static void bk_uart_sdma_init(struct bk_uart *uart, dma_ring_t *ring)
{
    soft_dma_t *dma = &uart->sdma;
    soft_dma_stop(dma);
    
    memset(&dma->hard_info, 0, sizeof(dma->hard_info));
    
    dma->hard_info.mode         = HARD_DMA_MODE_CIRCULAR;
    dma->hard_info.max_size     = 64 * 1024;
    dma->hard_info.flag         = HARD_DMA_FLAG_HALF_IRQ | HARD_DMA_FLAG_FULL_IRQ;
    dma->hard_info.data_timeout = uart_calc_byte_timeout_us(uart->serial_dev.config.baud_rate);

    dma->ops.get_index          = bk_sdma_int_get_index;
    dma->ops.dma_init           = bk_sdma_int_init;
    dma->ops.dma_start          = bk_sdma_int_start;
    dma->ops.dma_stop           = bk_sdma_int_stop;

    dma->cbs.dma_half_callback      = bk_uart_sdma_callback;
    dma->cbs.dma_full_callback      = bk_uart_sdma_callback;
    dma->cbs.dma_timeout_callback   = bk_uart_sdma_callback;

    soft_dma_init(dma);
    soft_dma_start(dma, ring);
    soft_dma_irq_enable(&uart->sdma, OS_TRUE);

}

static os_err_t bk_uart_init(struct os_serial_device *serial, struct serial_configure *cfg)
{
    uart_config_t config;
    struct bk_uart *uart;

    OS_ASSERT(serial != OS_NULL);
    OS_ASSERT(cfg != OS_NULL);

    uart = os_container_of(serial, struct bk_uart, serial_dev);
    if(uart->huart == OS_NULL)
        return OS_FAILURE;

    serial->config = *cfg;
    OS_ASSERT((serial->config.data_bits >= DATA_BITS_5) && \
            (serial->config.data_bits <= DATA_BITS_8));

    config.baud_rate = serial->config.baud_rate;
    config.data_width = serial->config.data_bits - DATA_BITS_5;
    config.parity = serial->config.parity;
    config.stop_bits = serial->config.stop_bits;
    config.flow_control = FLOW_CTRL_DISABLED;
    config.flags = 0;

    uart_hw_set_change(uart->huart->port, &config);

    bk_uart_sdma_init(uart, &serial->rx_fifo->ring);

    return OS_SUCCESS;
}

static int bk_uart_poll_send(struct os_serial_device *serial, const uint8_t *buff, os_size_t size)
{
    int i;
    struct bk_uart *uart;

    OS_ASSERT(serial != OS_NULL);

    uart = os_container_of(serial, struct bk_uart, serial_dev);

    OS_ASSERT(uart != OS_NULL);

    for (i = 0; i < size; i++)
    {
        os_base_t level = os_irq_lock();
        uart_write_byte(uart->huart->port, (char)buff[i]);
        os_irq_unlock(level);
    }

    return size;
}

void hal_bk_uart_uninit(uint32_t port)
{
    return;
}

static os_err_t bk_uart_deinit(struct os_serial_device *serial)
{
    struct bk_uart *uart;

    OS_ASSERT(serial != OS_NULL);

    uart = os_container_of(serial, struct bk_uart, serial_dev);
    if(uart->huart == OS_NULL)
        return OS_FAILURE;

    /* rx */
    hal_bk_uart_uninit(uart->huart->port);
    soft_dma_stop(&uart->sdma);

    /* tx */

    return OS_SUCCESS;
}

static const struct os_uart_ops bk_uart_ops = {
    .init       = bk_uart_init,
    .deinit     = bk_uart_deinit,

    .start_send = OS_NULL,
    .poll_send  = bk_uart_poll_send,   
};

int bk_uart_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    os_err_t    result  = 0;
    os_base_t   level;

    struct serial_configure config  = OS_SERIAL_CONFIG_DEFAULT;
    
    struct beken_uart_info *uart_info = (struct beken_uart_info *)dev->info;
    struct bk_uart *uart = os_calloc(1, sizeof(struct bk_uart));

    OS_ASSERT(uart);
    uart->huart = uart_info;

    struct os_serial_device *serial = &uart->serial_dev;

    serial->ops    = &bk_uart_ops;
    serial->config = config;

	if ( UART1_PORT == uart->huart->port){
		serial->config.baud_rate = BEKEN_UART1_BAUD;
	} else {
		serial->config.baud_rate = BEKEN_UART2_BAUD;
	}

    level = os_irq_lock();
    os_list_add_tail(&bk_uart_list, &uart->list);
    os_irq_unlock(level);

    result = os_hw_serial_register(serial, dev->name, NULL);
    
    OS_ASSERT(result == OS_SUCCESS);

    return result;

}

OS_DRIVER_INFO bk_uart_driver = {
    .name   = "uart_Type",
    .probe  = bk_uart_probe,
};

OS_DRIVER_DEFINE(bk_uart_driver, OS_INIT_LEVEL_PRE_DEVICE, OS_INIT_SUBLEVEL_HIGH);

#ifdef OS_USING_CONSOLE

static struct beken_uart_info *console_uart = OS_NULL;

void __os_hw_console_output(char *str)
{
    if (console_uart == OS_NULL )
        return;

    while (*str)
    {
        uart_write_byte(console_uart->port, *str);
        str++;
    }
}

static int bk_uart_early_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    if( !strcmp(dev->name, OS_CONSOLE_DEVICE_NAME) )
        console_uart = (struct beken_uart_info *)dev->info;

    return OS_SUCCESS;
}

OS_DRIVER_INFO bk_uart_early_driver = {
    .name   = "uart_Type",
    .probe  = bk_uart_early_probe,
};

OS_DRIVER_DEFINE(bk_uart_early_driver, OS_INIT_LEVEL_PRE_KERNEL_1, OS_INIT_SUBLEVEL_MIDDLE);

#endif
