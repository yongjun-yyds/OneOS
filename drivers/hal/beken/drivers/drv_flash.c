/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with 
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_flash.c
 *
 * @brief       This file provides beken flash read/write functions.
 *
 * @revision
 * Date         Author          Notes
 * 2020-12-29   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <os_task.h>
#include <os_mutex.h>
#include <os_types.h>


#ifdef BEKEN_USING_FLASH
#include "board.h"
#include "typedef.h"
#include "drv_flash.h"
#include "flash.h"
#include "fal.h"
#include <os_memory.h>
#include "ports/flash_info.c"

#include <dlog.h>

#define DBG_TAG "drv_flash"


static struct os_mutex flash_mutex;


int beken_flash_read(uint32_t address, void *data, uint32_t size)
{
    if (size == 0)
    {
        LOG_D(DBG_TAG, "flash read len is NULL\n");
        return -1;
    }
	LOG_E(DBG_TAG, "flash read lock\n");

    os_mutex_lock(&flash_mutex,OS_WAIT_FOREVER);
    flash_read(data, size, address);
    os_mutex_unlock(&flash_mutex);
    return 0;
}

int beken_flash_write(uint32_t address, const void *data, uint32_t size)
{
    if (size == 0)
    {
        LOG_D(DBG_TAG, "flash write len is NULL\n");
        return -1;
    }
	LOG_E(DBG_TAG, "flash write lock\n");

    os_mutex_lock(&flash_mutex,OS_WAIT_FOREVER);
    flash_write((char *)data, size, address);
    os_mutex_unlock(&flash_mutex);
    
    return 0;
}

int beken_flash_erase(uint32_t address)
{
	LOG_E(DBG_TAG, "flash erase lock\n");

    os_mutex_lock(&flash_mutex,OS_WAIT_FOREVER);
    address &= (0xFFF000);
    flash_ctrl(CMD_FLASH_ERASE_SECTOR, &address);
    os_mutex_unlock(&flash_mutex);
    
    return 0;
}

/*erase current sector*/
int beken_flash_erase_with_len(uint32_t address, uint32_t size)
{
	LOG_E(DBG_TAG, "flash erase_with_len lock\n");

    os_mutex_lock(&flash_mutex,OS_WAIT_FOREVER);
    address &= (0xFFF000);
    uint32_t cnt = 0;
    uint32_t addrtmp = address;
    for(cnt = 0; cnt<((size - 1) / 4096 + 1); cnt++)
    {
        addrtmp += cnt*4096;
        flash_ctrl(CMD_FLASH_ERASE_SECTOR, &addrtmp);
    }
    os_mutex_unlock(&flash_mutex);
    
    return 0;
}


static int beken_flash_init(void)
{
    return os_mutex_create(&flash_mutex, "flash", OS_FALSE);
}

OS_PREV_INIT(beken_flash_init, OS_INIT_SUBLEVEL_HIGH);

static int beken_flash_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    fal_flash_t *fal_flash = os_calloc(1, sizeof(fal_flash_t));

    if (fal_flash == OS_NULL)
    {
        os_kprintf("fal flash mem leak %s.\r\n", dev->name);
        return -1;
    }
    
    struct onchip_flash_info *flash_info = (struct onchip_flash_info *)dev->info;

    memcpy(fal_flash->name, dev->name, min(FAL_DEV_NAME_MAX - 1, strlen(dev->name)));
    
    fal_flash->name[min(FAL_DEV_NAME_MAX - 1, strlen(dev->name))] = 0;
    
    fal_flash->capacity   = flash_info->capacity;
    fal_flash->block_size = flash_info->block_size;
    fal_flash->page_size  = flash_info->page_size;
    
    fal_flash->ops.read_page   = beken_flash_read,
    fal_flash->ops.write_page  = beken_flash_write,
    fal_flash->ops.erase_block = beken_flash_erase,

    fal_flash->priv = flash_info;

    return fal_flash_register(fal_flash);
}

OS_DRIVER_INFO beken_flash_driver = 
{
    .name   = "Onchip_Flash_Type",
    .probe  = beken_flash_probe,
};

OS_DRIVER_DEFINE(beken_flash_driver, OS_INIT_LEVEL_DEVICE, OS_INIT_SUBLEVEL_HIGH);

#endif


