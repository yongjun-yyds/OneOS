#ifndef __DRV_FLASH_H__
#define __DRV_FLASH_H__

#include "typedef.h"
#include "flash_pub.h"
#include "os_types.h"

int beken_flash_read(uint32_t address, void *data, uint32_t size);
int beken_flash_write(uint32_t address, const void *data, uint32_t size);
int beken_flash_erase(uint32_t address);
int beken_flash_erase_with_len(uint32_t address, uint32_t size);
#endif
