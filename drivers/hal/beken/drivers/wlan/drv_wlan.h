#ifndef __DRV_WLAN_H__
#define __DRV_WLAN_H__

#include "wlan_dev.h"
#include "os_mutex.h"


#define MAX_ADDR_LEN            6

enum STATION_MODE
{
    NORMAL_MODE = 0,
    ADVANCED_MODE = 1,
};

enum CONNECT_STATE
{
    CONNECT_DONE = 0,
    CONNECT_DOING = 1,
    CONNECT_FAILED = 2,
};

struct beken_wifi_info
{
    uint32_t mac[MAX_ADDR_LEN];
    uint32_t state;       /* 0:done 1:doding 2:failed */
    uint32_t mode;        /* 0:normal 1:advanced */
	os_net_mode_t work_mode; /* work as a station or ap */
};

struct os_bk_wlan_device
{
    uint8_t mac[MAX_ADDR_LEN];
    struct os_wlan_device       wlan_sta_dev;
    struct os_wlan_device       wlan_ap_dev;
};

struct netif *wlan_get_sta_netif(void);
struct netif *wlan_get_uap_netif(void);

int beken_wlan_irq_lock(void);
void beken_wlan_irq_unlock(int int_flag);


#endif
