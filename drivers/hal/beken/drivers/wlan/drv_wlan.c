/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with 
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_wlan.c
 *
 * @brief       This file implements wlan driver for beken
 *
 * @revision
 * Date         Author          Notes
 * 2020-12-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include "include.h"
#include "lwip/opt.h"
#include "lwip/def.h"
#include "lwip/mem.h"
#include "lwip/pbuf.h"
#include "lwip/sys.h"
#include <lwip/stats.h>
#include <lwip/snmp.h>

#include <stdio.h>
#include <string.h>
#include "netif/etharp.h"
#include "lwip_netif_address.h"
#include "sa_station.h"
#include "drv_model_pub.h"
#include "mem_pub.h"
#include "common.h"
#include "hostapd_cfg.h"

#include "sk_intf.h"
#include "rw_pub.h"
#include "error.h"
#include "rtos_pub.h"
#include "param_config.h"
#include "wlan_ui_pub.h"

#include <os_task.h>
#include <os_clock.h>
#include <wlan_dev.h>
#include "os_types.h"
#include "bus.h"
#include "os_errno.h"
#include <dlog.h>

#include "drv_flash.h"
#include "drv_wlan.h"
#include "drv_wlan_fast_connect.h"

#include "uart_pub.h"
#include "ieee802_11_defs.h"
#include "wlan_ui_pub.h"
#include "net_param_pub.h"
#include "role_launch.h"
#include "app.h"

/* Define those to better describe your network interface. */
#define IFNAME0 'e'
#define IFNAME1 'n'

#define DRV_EXT_LVL DBG_EXT_INFO
#define DRV_EXT_TAG "wlan_dev"
#include <drv_log.h>

#define ETH_INTF_DEBUG  0
#if ETH_INTF_DEBUG
#define ETH_INTF_PRT      warning_prf
#define ETH_INTF_WARN     warning_prf
#define ETH_INTF_FATAL    fatal_prf
#else
#define ETH_INTF_PRT      null_prf
#define ETH_INTF_WARN     null_prf
#define ETH_INTF_FATAL    null_prf
#endif

#define OS_WLAN_DEVICE(eth) (struct os_wlan_device *)(eth)

struct os_wlan_buff
{
    void      *data;
    uint32_t len;
};

struct eth_device
{
    /* inherit from os_device */
    struct os_device parent;

    /* network interface for lwip */
    struct netif       *netif;

    uint16_t flags;
    uint8_t  link_changed;
    uint8_t  link_status;

    /* eth device interface */
    struct pbuf *(*eth_rx)(os_device_t *dev);
    os_err_t (*eth_tx)(os_device_t *dev, struct pbuf *p);
};

static char gs_dhcpd_server_ip[16] = "192.168.169.1";
char* DHCPD_SERVER_IP = gs_dhcpd_server_ip;

#ifdef BSP_USING_BK_STA
struct os_wlan_device       wlan_sta_dev;
#endif

#ifdef BSP_USING_BK_AP
struct os_wlan_device       wlan_ap_dev;
#endif

#define RT_WLAN_SSID_MAX_LEN 32
#define SCAN_WAIT_OUT_TIME 2000
static os_semaphore_id _g_scan_done_sem;
int start_connect_tick = 0;
int end_connect_tick = 0;
int g_beken_rssi = 0;

static int g_sta_status = 0;
static int g_ap_status = 0;


static wifi_country_t country_code_CN   = {.cc= "CN", .schan=1, .nchan=13, .max_tx_power=0, .policy=WIFI_COUNTRY_POLICY_MANUAL};
static wifi_country_t country_code_US   = {.cc= "US", .schan=1, .nchan=11, .max_tx_power=0, .policy=WIFI_COUNTRY_POLICY_MANUAL};
static wifi_country_t country_code_EP   = {.cc= "EP", .schan=1, .nchan=13, .max_tx_power=0, .policy=WIFI_COUNTRY_POLICY_MANUAL};
static wifi_country_t country_code_JP   = {.cc= "JP", .schan=1, .nchan=14, .max_tx_power=0, .policy=WIFI_COUNTRY_POLICY_MANUAL};
static wifi_country_t country_code_AU   = {.cc= "AU", .schan=1, .nchan=13, .max_tx_power=0, .policy=WIFI_COUNTRY_POLICY_MANUAL};


static os_err_t _wifi_easyjoin(os_device_t *dev, void *passwd);
static os_err_t beken_wlan_disconnect(struct os_wlan_device *wlan);

extern void *net_get_sta_handle(void);
extern void *net_get_uap_handle(void);
extern void wifi_get_mac_address(char *mac, u8 type);
extern void *net_get_netif_handle(uint8_t iface);
extern int bmsg_tx_sender(struct pbuf *p, uint32_t vif_idx);
extern void bk_wlan_status_register_cb(FUNC_1PARAM_PTR cb);
extern void dhcp_server_stop(void);
extern int wifi_set_mac_address(char *mac);
extern rw_evt_type mhdr_get_station_status(void);
extern os_bool_t os_wlan_is_connected(void);
extern os_bool_t os_wlan_ap_is_active(void);
extern int bk_wlan_power_save_set_level(BK_PS_LEVEL level);



//#define MINI_DUMP
//#define ETH_RX_DUMP
//#define ETH_TX_DUMP

#if defined(ETH_RX_DUMP) ||  defined(ETH_TX_DUMP)
static void packet_dump(const char *msg, const struct pbuf *p)
{
    const struct pbuf *q;
    uint32_t i, j;
    uint8_t *ptr;

    os_kprintf("%s %d byte\n", msg, p->tot_len);

#ifdef MINI_DUMP
    return;
#endif

    i = 0;
    for (q = p; q != OS_NULL; q = q->next)
    {
        ptr = q->payload;

        for (j = 0; j < q->len; j++)
        {
            if ((i % 8) == 0)
            {
                os_kprintf("  ");
            }
            if ((i % 16) == 0)
            {
                os_kprintf("\r\n");
            }
            os_kprintf("%02x ", *ptr);

            i++;
            ptr++;
        }
    }

    os_kprintf("\n\n");
}
#endif /* dump */

static os_err_t low_level_output(struct netif *netif, struct pbuf *p)
{
    int ret;
    err_t err = ERR_OK;
    uint8_t vif_idx = rwm_mgmt_get_netif2vif(netif);

#ifdef ETH_TX_DUMP
    packet_dump("TX dump", p);
#endif /* ETH_TX_DUMP */

    if (!netif_is_link_up(netif))
    {
        return ERR_IF;
    }

    ret = bmsg_tx_sender(p, (uint32_t)vif_idx);
    if (0 != ret)
    {
        err = ERR_TIMEOUT;
    }

    return err;
}

struct os_lwip_info
{
    struct netif netif;
    struct os_net_device *net_dev;
    os_list_node_t list;
};


struct netif* os_wlan_get_netif(struct os_wlan_device *wlan)
{
    struct os_lwip_info *lwip_info = (struct os_lwip_info *)wlan->net_dev.userdata;
	
    OS_ASSERT(lwip_info != OS_NULL);
	
	return &lwip_info->netif;
}

/**
 * OS LwIP Interface &bk_wlan_dev->wlan_sta_dev
 */
 
struct netif *wlan_get_sta_netif(void)
{
#ifdef BSP_USING_BK_STA
	return os_wlan_get_netif(&wlan_sta_dev);
#else
    return OS_NULL;
#endif
}

struct netif *wlan_get_uap_netif(void)
{
#ifdef BSP_USING_BK_AP
	return os_wlan_get_netif(&wlan_ap_dev);
#else
    return OS_NULL;
#endif
}

void ethernetif_input(int iface, struct pbuf *p)
{
    struct eth_hdr        *ethhdr   = OS_NULL;
    struct netif          *netif    = OS_NULL;
	struct os_wlan_device *dev      = OS_NULL;

#ifdef ETH_RX_DUMP
    packet_dump("RX dump", p);
#endif /* ETH_RX_DUMP */

    if (p->len <= SIZEOF_ETH_HDR)
    {
        pbuf_free(p);
        return;
    }
	
    netif = rwm_mgmt_get_vif2netif((uint8_t)iface);
    if (!netif)
    {
        pbuf_free(p);
        p = NULL;
        return;
    }

#ifdef BSP_USING_BK_STA
	if(netif == wlan_get_sta_netif())
		dev = &wlan_sta_dev;
#endif
#ifdef BSP_USING_BK_AP
	else if(netif == wlan_get_uap_netif())
		dev = &wlan_ap_dev;
#endif

	if (!dev)
    {
        LOG_E(DRV_EXT_TAG, "ethernetif_input no wlan device found %d\r\n", iface);
        pbuf_free(p);
        p = NULL;
        return;
    }

    /* points to packet payload, which starts with an Ethernet header */
    ethhdr = p->payload;

    switch (htons(ethhdr->type))
    {	
    /* IP or ARP packet? */
    case ETHTYPE_IP:
    case ETHTYPE_ARP:
#if PPPOE_SUPPORT
    /* PPPoE packet? */
    case ETHTYPE_PPPOEDISC:
    case ETHTYPE_PPPOE:
#endif /* PPPOE_SUPPORT */
        /* full packet send to tcpip_thread to process */
        if(os_net_rx_data(&dev->net_dev, p->payload, p->tot_len) != ERR_OK)
        {
            LWIP_DEBUGF(NETIF_DEBUG, ("ethernetif_input: IP input error\r\n"));
        }
        
        pbuf_free(p);
        p = NULL;
        break;

    case 0x888EU:
        ke_l2_packet_tx(p->payload, p->len, iface);
        pbuf_free(p);
        p = NULL;
        break;

    default:
        pbuf_free(p);
        p = NULL;
        break;
    }
}

// wlan event callbacks
static void wlan_event_handle(void *ctx)
{
    rw_evt_type event = *((rw_evt_type*)ctx);

    struct os_wlan_info info;
    struct os_wlan_buff user_buff;
    
    if ((event < 0) || (event > RW_EVT_MAX))
    {
        return;
    }
        
    LOG_D(DRV_EXT_TAG, "===wlan_event_handle:%d===\r\n",event);
    switch (event)
    {
#ifdef BSP_USING_BK_STA
    case RW_EVT_STA_CONNECTED:
        end_connect_tick = os_tick_get_value();
        LOG_E(DRV_EXT_TAG, "[wlan_connect]:start tick =  %d, connect done tick = %d, total = %d \n", start_connect_tick, end_connect_tick, end_connect_tick - start_connect_tick);
        os_wlan_event_callback(&wlan_sta_dev, OS_WLAN_EVET_JOIN, OS_NULL);
        break;

    case RW_EVT_STA_DISCONNECTED:
        LOG_E(DRV_EXT_TAG,"connect fail");
        os_wlan_event_callback(&wlan_sta_dev, OS_WLAN_EVET_LEAVE, OS_NULL);
        break;

    case RW_EVT_STA_CONNECT_FAILED:
        case RW_EVT_STA_PASSWORD_WRONG:
    case RW_EVT_STA_NO_AP_FOUND:
        break;
#endif

#ifdef BSP_USING_BK_AP
    case RW_EVT_AP_CONNECTED:
        user_buff.data = &info;
        user_buff.len = sizeof(struct os_wlan_info);
        break;

    case RW_EVT_AP_DISCONNECTED:
        user_buff.data = &info;
        user_buff.len = sizeof(struct os_wlan_info);
        break;

    case RW_EVT_AP_CONNECT_FAILED:
        break;

    case RW_EVT_AP_START_COMPLETE:
        os_wlan_event_callback(&wlan_ap_dev, OS_WLAN_EVET_AP_START, OS_NULL);
    break;

    case RW_EVT_AP_STOP_COMPLETE:
         os_wlan_event_callback(&wlan_ap_dev, OS_WLAN_EVET_AP_STOP, OS_NULL);
    break;
#endif

    default:
        break;
    }
}
static void scan_ap_callback(void *ctxt, uint8_t param)
{
    if (_g_scan_done_sem)
    {
		os_semaphore_post(_g_scan_done_sem);
        LOG_D(DRV_EXT_TAG, "release scan done semaphore \n");
    }
}

static int os_wlan_malloc_scan_result(struct os_wlan_scan_result **scan_result, int num)
{
    struct os_wlan_scan_result *_scan_result;
    int i;
    int result = OS_SUCCESS;

	_scan_result = *scan_result;

    _scan_result->count = num;
    _scan_result->scan_info = os_malloc(sizeof(struct os_wlan_info) * num);
    if (_scan_result->scan_info == OS_NULL)
    {
        LOG_E(DRV_EXT_TAG, "rt_scan_rst table malloc failed!\r\n");
        result = -OS_FAILURE;
        goto _exit;
    }
    os_memset(_scan_result->scan_info, 0, sizeof(struct os_wlan_info) * num);

    return OS_SUCCESS;
_exit:
	
    if (_scan_result->scan_info)
    {
        os_free(_scan_result->scan_info);
        _scan_result->scan_info = OS_NULL;
    }

    return -OS_FAILURE;
}

static const char *wlan_sec_type_string[] =
{
    "NONE",
    "WEP",
    "WPA-TKIP",
    "WPA-AES",
    "WPA2-TKIP",
    "WPA2-AES",
    "WPA2-MIX",
    "AUTO"
};

extern int wpa_get_psk(char *psk);
int _wifi_connect_done(void *ctx)
{
#ifdef BSP_USING_BK_STA
	 //_g_sta_info.state = CONNECT_DONE;
#endif

    return 0;
}

extern int bk_wlan_dtim_rf_ps_timer_start(void);
extern int bk_wlan_dtim_rf_ps_timer_pause(void);

static int _wifi_power_manager(int level)
{
 switch (level)
 {
    case 0:
    {
        #if CFG_USE_MCU_PS
        /* disable cpu sleep */
        bk_wlan_mcu_ps_mode_disable();
        #endif
        #if CFG_USE_STA_PS
        /* disable rf sleep */
        bk_wlan_dtim_rf_ps_mode_disable();
        /* pause rf timer */
        bk_wlan_dtim_rf_ps_timer_pause();
        #endif
        break;
    }

    case 1:
    {
        #if CFG_USE_MCU_PS
        /* enable cpu sleep */
        bk_wlan_mcu_ps_mode_enable();
        #endif
        #if CFG_USE_STA_PS
        /* disable rf sleep */
        bk_wlan_dtim_rf_ps_mode_disable();
        /* pause rf timer */
        bk_wlan_dtim_rf_ps_timer_pause();
        #endif
        break;
    }
    case 2:
    {
         #if CFG_USE_MCU_PS
        /* disable cpu sleep */
        bk_wlan_mcu_ps_mode_disable();
		 #endif
		#if CFG_USE_STA_PS
        /* enable rf sleep */
        bk_wlan_dtim_rf_ps_mode_enable();
        /* start rf timer */
        bk_wlan_dtim_rf_ps_timer_start();
		#endif
        break;
    }

    case 3:
    {
        #if CFG_USE_MCU_PS
    	/* enable cpu sleep */
        bk_wlan_mcu_ps_mode_enable();
        #endif
        #if CFG_USE_STA_PS
        /* enable rf sleep */
        bk_wlan_dtim_rf_ps_mode_enable();
        /* start rf timer */
        bk_wlan_dtim_rf_ps_timer_start();
        #endif
        break;
    }

    default:
        break;
 }
}

#if LWIP_IPV4 && LWIP_IGMP
static err_t igmp_mac_filter(struct netif *netif, const ip4_addr_t *ip4_addr, u8_t action)
{
    uint8_t mac[6];
    const uint8_t *p = (const uint8_t *)ip4_addr;

    mac[0] = 0x01;
    mac[1] = 0x00;
    mac[2] = 0x5E;
    mac[3] = *(p + 1) & 0x7F;
    mac[4] = *(p + 2);
    mac[5] = *(p + 3);

    if (1)
    {
        LOG_D(DRV_EXT_TAG, "%s %s %s ", __FUNCTION__, (action == NETIF_ADD_MAC_FILTER) ? "add" : "del", ip4addr_ntoa(ip4_addr));
        LOG_D(DRV_EXT_TAG, "%02X:%02X:%02X:%02X:%02X:%02X\n", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
    }

    return 0;
}
#endif /* LWIP_IPV4 && LWIP_IGMP */

#if LWIP_IPV6 && LWIP_IPV6_MLD
static err_t mld_mac_filter(struct netif *netif, const ip6_addr_t *ip6_addr, u8_t action)
{
    uint8_t mac[6];
    const uint8_t *p = (const uint8_t *)&ip6_addr->addr[3];

    mac[0] = 0x33;
    mac[1] = 0x33;
    mac[2] = *(p + 0);
    mac[3] = *(p + 1);
    mac[4] = *(p + 2);
    mac[5] = *(p + 3);

    if (1)
    {
        LOG_D(DRV_EXT_TAG, "%s %s %s ", __FUNCTION__, (action == NETIF_ADD_MAC_FILTER) ? "add" : "del", ip6addr_ntoa(ip6_addr));
        LOG_D(DRV_EXT_TAG, "%02X:%02X:%02X:%02X:%02X:%02X\n", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
    }

    return 0;
}
#endif /* LWIP_IPV6 && LWIP_IPV6_MLD */


static os_err_t beken_wlan_init(void)
{
    /* Initialize semaphore for scan */
    _g_scan_done_sem = os_semaphore_create(OS_NULL, "scan_done", 0, 1);

    return OS_SUCCESS;
}

static os_err_t beken_wlan_set_mode(struct os_wlan_device *wlan, os_net_mode_t mode)
{
    return OS_SUCCESS;
}

static void wlan_scan_done_handler(struct os_wlan_scan_result *scan_result)
{
    struct sta_scan_res *scan_rst_table;
    char scan_rst_ap_num = 0;
    int i;
    
    scan_rst_ap_num = bk_wlan_get_scan_ap_result_numbers();
    if (scan_rst_ap_num == 0)
    {
        LOG_E(DRV_EXT_TAG, "NULL AP \r\n");
        return;
    }

    LOG_D(DRV_EXT_TAG, "scan_rst_ap_num %d \r\n", scan_rst_ap_num);

    scan_rst_table = (struct sta_scan_res *)os_malloc(sizeof(struct sta_scan_res) * scan_rst_ap_num);
    if (scan_rst_table == OS_NULL)
    {
        LOG_D(DRV_EXT_TAG, "scan_rst_table malloc failed!\r\n");
        return;
    }

    bk_wlan_get_scan_ap_result(scan_rst_table, scan_rst_ap_num);

    scan_result->scan_info = (struct os_wlan_scan_info*)os_calloc(scan_rst_ap_num, sizeof(struct os_wlan_scan_info));

    scan_result->count = scan_rst_ap_num;

    for (i = 0; i < scan_rst_ap_num; i++)
    {
        os_strncpy(scan_result->scan_info[i].ssid.val, scan_rst_table[i].ssid, RT_WLAN_SSID_MAX_LEN);
		scan_result->scan_info[i].ssid.len         = strlen(scan_rst_table[i].ssid);
        os_memcpy(scan_result->scan_info[i].bssid, scan_rst_table[i].bssid, 6);
        scan_result->scan_info[i].channel          = scan_rst_table[i].channel;
        scan_result->scan_info[i].signal_strength  = scan_rst_table[i].level;

        LOG_I(DRV_EXT_TAG, "\033[36;22m ssid: %-32.*s  security: %-s\r\n", 32, scan_rst_table[i].ssid, wlan_sec_type_string[scan_rst_table[i].security]);

        switch (scan_rst_table[i].security)
        {
        case BK_SECURITY_TYPE_NONE:
            scan_result->scan_info[i].security = OS_WLAN_SECURITY_OPEN;
            break;

        case BK_SECURITY_TYPE_WEP:
            scan_result->scan_info[i].security = OS_WLAN_SECURITY_WEP_PSK;
            break;

        case BK_SECURITY_TYPE_WPA_TKIP:
            scan_result->scan_info[i].security = OS_WLAN_SECURITY_WPA_TKIP_PSK;
            break;

        case BK_SECURITY_TYPE_WPA_AES:
            scan_result->scan_info[i].security = OS_WLAN_SECURITY_WPA_AES_PSK;
            break;

        case BK_SECURITY_TYPE_WPA2_TKIP:
            scan_result->scan_info[i].security = OS_WLAN_SECURITY_WPA2_TKIP_PSK;
            break;

        case BK_SECURITY_TYPE_WPA2_AES:
            scan_result->scan_info[i].security = OS_WLAN_SECURITY_WPA2_AES_PSK;
            break;

        case BK_SECURITY_TYPE_WPA2_MIXED:
            scan_result->scan_info[i].security = OS_WLAN_SECURITY_WPA2_MIXED_PSK;
            break;

        default:
            scan_result->scan_info[i].security = OS_WLAN_SECURITY_WPA_AES_PSK;
            break;
        }
    }
    
    LOG_D(DRV_EXT_TAG, "\033[0m\r\n");

    if (scan_rst_table != NULL)
    {
        os_free(scan_rst_table);
        scan_rst_table = NULL;
    }

	return;
}

static os_err_t beken_wlan_scan(struct os_wlan_device *wlan, uint32_t msec, struct os_wlan_scan_result *scan_result)
{	
    os_err_t ret = OS_SUCCESS;
    
	bk_wlan_scan_ap_reg_cb(scan_ap_callback);
	LOG_D(DRV_EXT_TAG, "%s L%d %s cmd: case WIFI_SCAN!\r\n", __FILE__, __LINE__, __FUNCTION__);
	
	bk_wlan_start_scan();

	if (os_semaphore_wait(_g_scan_done_sem, os_tick_from_ms(SCAN_WAIT_OUT_TIME)) != OS_SUCCESS)
	{
		LOG_D(DRV_EXT_TAG, "Wait scan_done semaphore timeout \n");
	}

	wlan_scan_done_handler(scan_result);

	
#if CFG_ROLE_LAUNCH
	if(mhdr_get_station_status() == RW_EVT_STA_GOT_IP)
	{
		rl_pre_sta_set_status(RL_STATUS_STA_LAUNCHED);
	}
#endif

	return ret;
}

static os_err_t beken_wlan_scan_clean_result(struct os_wlan_device *wlan, struct os_wlan_scan_result *info)
{
    os_free(info->scan_info);

    return OS_SUCCESS;
}

static os_err_t beken_wlan_scan_stop(struct os_wlan_device *wlan)
{
	extern int bk_wlan_stop_scan(void);

	return bk_wlan_stop_scan();
}


os_err_t beken_wlan_sta_start(struct os_wlan_device *wlan_dev)
{  
    return OS_SUCCESS;
}

static os_err_t beken_wlan_join(struct os_wlan_device *wlan_dev)
{
	int ret = OS_SUCCESS;
#ifdef BSP_USING_BK_STA /* needed only by station  */
	network_InitTypeDef_st wNetConfig;
    const char *ssid = OS_NULL;
    int len;

	start_connect_tick = os_tick_get_value();
    LOG_D(DRV_EXT_TAG, "beken_wlan_join: start connect \n");

    ssid  = (char *)wlan_dev->info.ssid;
    
    os_memset(&wNetConfig, 0x0, sizeof(network_InitTypeDef_st));

    if ((ssid != NULL) && ('\0' != *ssid))
    {
        len = os_strlen(ssid);
        if (SSID_MAX_LEN < len)
        {
            LOG_E(DRV_EXT_TAG, "ssid name more than 32 Bytes\r\n");
            return -OS_FAILURE;
        }

        os_strncpy((char *)wNetConfig.wifi_ssid, ssid, sizeof(wNetConfig.wifi_ssid));
    }
    else
    {
        LOG_E(DRV_EXT_TAG, "ssid is null or bssid is invalid/disabled\r\n");
        return -OS_FAILURE;
    }


    if (sizeof(wNetConfig.wifi_key) < os_strlen(wlan_dev->info.password))
    {
        LOG_E(DRV_EXT_TAG, "wifi key is more than %d Bytes\r\n", sizeof(wNetConfig.wifi_key));
        return -OS_FAILURE;
    }
    
    os_strncpy((char *)wNetConfig.wifi_key,(char *)wlan_dev->info.password,sizeof(wNetConfig.wifi_key));

    wNetConfig.wifi_mode = BK_STATION;
    wNetConfig.dhcp_mode = DHCP_CLIENT;
    wNetConfig.wifi_retry_interval = 100;

    LOG_D(DRV_EXT_TAG, "beken_wlan_join: ssid:%.*s  key:%.*s\r\n",
                                            sizeof(wNetConfig.wifi_ssid), wNetConfig.wifi_ssid,
                                            sizeof(wNetConfig.wifi_key), wNetConfig.wifi_key);

    ret =  bk_wlan_start(&wNetConfig);
	if(OS_SUCCESS == ret)
		g_sta_status = 1;
#endif

	return ret;
}


void beken_wlan_set_ap_ip(const char* ip)
{
    if (strlen(ip) > 15)
    {
        LOG_E(DRV_EXT_TAG, "beken_wlan_set_ap_ip to set ap ip[%s] too long, please check\r\n", ip);
        return;
    }

    memset((void *)DHCPD_SERVER_IP, sizeof(gs_dhcpd_server_ip), 0);
    strcpy(DHCPD_SERVER_IP, ip);
}

static os_err_t beken_wlan_disconnect(struct os_wlan_device *wlan)
{
    os_net_mode_t mode;

#if CFG_ROLE_LAUNCH
    LAUNCH_REQ param;
#endif

    mode = wlan->net_dev.info.mode;

    if (mode == net_dev_mode_sta)
    {
    	if(g_sta_status) 
    	{
#if CFG_ROLE_LAUNCH
	        param.req_type = LAUNCH_REQ_DELIF_STA;
	        rl_sta_request_enter(&param, 0);
#else
	        bk_wlan_stop(BK_STATION);
#endif
			g_sta_status = 0;
    	}
		else
		{
			//os_wlan_dev_indicate_event_handle(&_g_sta_device, OS_WLAN_DEV_EVT_DISCONNECT, OS_NULL);
		}
    }
    else if (mode == net_dev_mode_ap)
    {
    	if(g_ap_status)
    	{	
	        bk_wlan_stop(BK_SOFT_AP);
			dhcp_server_stop();
			g_ap_status = 0;
    	}
		else
		{
			//os_wlan_dev_indicate_event_handle(&_g_ap_device, OS_WLAN_DEV_EVT_AP_STOP, OS_NULL);
		}
    }

    return OS_SUCCESS;
}


static os_err_t beken_wlan_set_channel (struct os_wlan_device *wlan, int channel)
{
    if (channel < 1 || channel >13)
    {
        LOG_E(DRV_EXT_TAG, "unsupport channel.[1~13]\r\n");
    }
        
	return  bk_wlan_set_channel(channel);
}

static int beken_wlan_get_channel(struct os_wlan_device *wlan)
{
	extern int bk_wlan_get_channel(void);

	return bk_wlan_get_channel();
}

static os_err_t beken_wlan_set_mac(struct os_wlan_device *wlan, uint8_t mac[])
{
	return wifi_set_mac_address(mac);
}

static os_err_t beken_wlan_get_mac(struct os_wlan_device *wlan_dev, uint8_t *mac)
{
    wifi_get_mac_address(mac, CONFIG_ROLE_STA);

    return OS_SUCCESS;
}

int beken_wlan_get_rssi(struct os_wlan_device *wlan)
{
	return g_beken_rssi;
}

os_err_t beken_wlan_set_powersave(struct os_wlan_device *wlan, int level)
{
	return bk_wlan_power_save_set_level(level);
}

int beken_wlan_get_powersave(struct os_wlan_device *wlan)
{
	extern BK_PS_LEVEL global_ps_level;

	return global_ps_level;
}

static int beken_wlan_send_raw_frame(struct os_wlan_device *wlan, void *buff, int len)
{
	return bk_wlan_send_80211_raw_frame(buff,len);
}

static int beken_wlan_send(struct os_wlan_device *wlan, void *buff)
{
	struct netif *netif = os_wlan_get_netif(wlan);

	return low_level_output(netif, (struct pbuf*)buff);
}

static int beken_wlan_recv(struct os_wlan_device *wlan, void *buff, int len)
{
	return OS_SUCCESS;
}


static void app_demo_softap_rw_connected_event_func(void)
{
    os_kprintf("--------wlan ap [no password] connected event callback ----------\r\n");
}

static os_err_t beken_wlan_softap(struct os_wlan_device *wlan_dev)
{
	int ret = OS_SUCCESS;
#ifdef BSP_USING_BK_AP /* needed only by ap  */
	network_InitTypeDef_st wNetConfig;
	const char *ssid = OS_NULL;
	int len;

	ssid = (char *)wlan_dev->info.ssid;
	os_memset(&wNetConfig, 0x0, sizeof(network_InitTypeDef_st));

	if (ssid == NULL)
	{
		LOG_E(DRV_EXT_TAG, "ssid is null\r\n");
		return -OS_FAILURE;
	}

	len = os_strlen(ssid);
	if (SSID_MAX_LEN < len)
	{
		LOG_E(DRV_EXT_TAG, "ssid name more than 32 Bytes\r\n");
		/* continue to use 32 bytes ssid, do not return err */
		len = SSID_MAX_LEN;
	}
	os_strncpy((char *)wNetConfig.wifi_ssid, ssid, sizeof(wNetConfig.wifi_ssid));

	if (sizeof(wNetConfig.wifi_key) < os_strlen(wlan_dev->info.password))
	{
		LOG_E(DRV_EXT_TAG, "wifi key is more than %d Bytes\r\n", sizeof(wNetConfig.wifi_key));
		return -OS_FAILURE;
	}
	os_strncpy((char *)wNetConfig.wifi_key,(char *)wlan_dev->info.password,sizeof(wNetConfig.wifi_key));


	wNetConfig.wifi_mode = BK_SOFT_AP;
	wNetConfig.dhcp_mode = DHCP_SERVER;
	wNetConfig.wifi_retry_interval = 100;

	os_strcpy((char *)wNetConfig.local_ip_addr, DHCPD_SERVER_IP);
	os_strcpy((char *)wNetConfig.net_mask, "255.255.255.0");
	os_strcpy((char *)wNetConfig.gateway_ip_addr, DHCPD_SERVER_IP);
	os_strcpy((char *)wNetConfig.dns_server_ip_addr, DHCPD_SERVER_IP);

	LOG_D(DRV_EXT_TAG, "_wifi_softap: ssid:%.*s key:%.*s\r\n", sizeof(wNetConfig.wifi_ssid), wNetConfig.wifi_ssid, sizeof(wNetConfig.wifi_key), wNetConfig.wifi_key);

    bk_wlan_ap_set_default_channel((uint8_t)wlan_dev->info.channel);
    
	ret = bk_wlan_start(&wNetConfig);
	
	if(OS_SUCCESS == ret)
		g_ap_status = 1;
	else
		return OS_FAILURE;

    
	
#endif
	return ret;
}

/*=================================================================================*/
const static struct  os_wlan_device_ops wlan_ops =
{
    .start                  = beken_wlan_softap,
    .stop                   = beken_wlan_disconnect,
    .get_mac                = beken_wlan_get_mac,
    .join                   = beken_wlan_join,
    .check_join_status      = OS_NULL,
    .leave                  = beken_wlan_disconnect,
    .irq_handler            = OS_NULL,
    .send                   = beken_wlan_send,
    .wlan_scan              = beken_wlan_scan,
    .wlan_scan_stop         = beken_wlan_scan_stop,
};


#include "icu.h"
#include "icu_pub.h"
#include "drv_model.h"
#include "arm_arch.h"

/* register wlan device */
static int beken_wlan_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
	struct beken_wifi_info *wifi_info_tmp = OS_NULL;
	wifi_info_tmp = (struct beken_wifi_info *)dev->info;
    
    beken_wlan_init();
    
    char temp_mac[6];
    wifi_get_mac_address(temp_mac, CONFIG_ROLE_NULL);

    if (net_dev_mode_sta == wifi_info_tmp->work_mode)
    {
#ifdef BSP_USING_BK_STA
        wlan_sta_dev.ops                    = &wlan_ops;
        wlan_sta_dev.flags                  = OS_WLAN_FLAG_STA_ONLY;
        wlan_sta_dev.net_dev.info.mode      = net_dev_mode_sta;
        wlan_sta_dev.net_dev.info.intf_type = net_dev_intf_ether;
        wlan_sta_dev.net_dev.info.MTU       = 1500;

        if (os_wlan_net_register(&wlan_sta_dev, dev->name) != OS_SUCCESS)
        {
            return OS_FAILURE;
        }
#endif
    }
    else
    {
#ifdef BSP_USING_BK_AP
        wlan_ap_dev.ops                     = &wlan_ops;
        wlan_ap_dev.flags                   = OS_WLAN_FLAG_AP_ONLY;
        wlan_ap_dev.net_dev.info.mode       = net_dev_mode_ap;
        wlan_ap_dev.net_dev.info.intf_type  = net_dev_intf_ether;
        wlan_ap_dev.net_dev.info.MTU        = 1500;
        
        wlan_ap_dev.info.security           = OS_WLAN_SECURITY_WPA2_AES_PSK;
        wlan_ap_dev.info.country            = OS_WLAN_COUNTRY_CHINA;
        
        if (os_wlan_net_register(&wlan_ap_dev, dev->name) != OS_SUCCESS)
        {
            return OS_FAILURE;
        }
    }
#endif

    bk_wlan_status_register_cb(wlan_event_handle);

    return OS_SUCCESS;
}


OS_DRIVER_INFO beken_wlan_driver = {
    .name   = "Wlan_Type",
    .probe  = beken_wlan_probe,
};
OS_DRIVER_DEFINE(beken_wlan_driver, OS_INIT_LEVEL_COMPONENT, OS_INIT_SUBLEVEL_LOW);

static int bk7231n_init(void)
{
#ifdef OS_USING_BK_AP
    struct os_wlan_device *wlan_dev = OS_NULL;

    wlan_dev = (struct os_wlan_device *)os_device_find(OS_WLAN_DEVICE_AP_NAME);
    if (wlan_dev == OS_NULL)
    {
        LOG_E(DRV_EXT_TAG, "wifi_dev cannot find!");
        return OS_SUCCESS;
    }    

    wlan_dev->info.ssid                  = BSP_USING_BK_AP_SSID;
    wlan_dev->info.password              = BSP_USING_BK_AP_PASSWORD;
    wlan_dev->info.security              = BSP_USING_BK_AP_SECURITY;
    wlan_dev->info.country               = OS_WLAN_COUNTRY_CHINA;
    wlan_dev->info.channel               = BSP_USING_BK_AP_CHANNEL;    
    
    os_wlan_start(wlan_dev);
#endif
}
OS_INIT_CALL(bk7231n_init, OS_INIT_LEVEL_APPLICATION, OS_INIT_SUBLEVEL_LOW);


/* init protocol and register to LWIP */
static int beken_wlan_prot_init(void)
{
    struct os_wlan_device* wlan   = OS_NULL;
    struct netif*		   netif  = OS_NULL;
        
    app_start();

    os_base_t level = os_irq_lock();
    
	/* set wifi work mode */
#ifdef BSP_USING_BK_STA
    /* beken LWIP special modifications */
    wlan = (struct os_wlan_device *)&wlan_sta_dev;
    netif = os_wlan_get_netif(wlan);
    netif->linkoutput =(netif_linkoutput_fn)low_level_output;	
#endif
	
#ifdef BSP_USING_BK_AP
 /* beken LWIP special modifications */
    wlan = (struct os_wlan_device *)&wlan_ap_dev;
    netif = os_wlan_get_netif(wlan);
    netif->linkoutput =(netif_linkoutput_fn)low_level_output;
#endif

    os_irq_unlock(level);

	return OS_SUCCESS;
}
OS_INIT_CALL(beken_wlan_prot_init, OS_INIT_LEVEL_APPLICATION, OS_INIT_SUBLEVEL_LOW);

// eof
