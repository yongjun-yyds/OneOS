#include <string.h>

#define BEKEN_USING_FLASH_DISK

#ifdef BEKEN_USING_FLASH_DISK

#include "drv_flash.h"

#define FLASH_BASE      (0x100000)
#define SECTOR_SIZE     (4096)
#define DISK_SIZE       (1024*1024)

static struct os_device flash_disk_device;

static os_size_t flash_disk_read(os_device_t dev,
                                 os_off_t pos,
                                 void *buffer,
                                 os_size_t size)
{
    beken_flash_read(FLASH_BASE + SECTOR_SIZE * pos, buffer, SECTOR_SIZE * size);
    return size;
}

static os_size_t flash_disk_write(os_device_t dev,
                                  os_off_t pos,
                                  const void *buffer,
                                  os_size_t size)
{
    int i = 0;

    for (i = 0; i < size; i++)
    {
        beken_flash_erase(FLASH_BASE + SECTOR_SIZE * (pos + i));
        beken_flash_write(FLASH_BASE + SECTOR_SIZE * (pos + i), buffer, SECTOR_SIZE);
    }

    return size;
}

static os_err_t flash_disk_control(os_device_t dev, int cmd, void *args)
{
    if (cmd == OS_DEVICE_CTRL_BLK_GETGEOME)
    {
        struct os_device_blk_geometry *geometry;

        geometry = (struct os_device_blk_geometry *)args;
        if (geometry == OS_NULL) return -OS_ERROR;

        geometry->bytes_per_sector = SECTOR_SIZE;
        geometry->sector_count = DISK_SIZE / SECTOR_SIZE;
        geometry->block_size = SECTOR_SIZE;
    }
    return OS_SUCCESS;
}

#ifdef OS_USING_DEVICE_OPS
const static struct os_device_ops flash_disk_ops =
{
    OS_NULL,
    OS_NULL,
    OS_NULL,
    flash_disk_read,
    flash_disk_write,
    flash_disk_control
};
#endif /* OS_USING_DEVICE_OPS */

static int os_hw_flash_disk_init(void)
{
    memset(&flash_disk_device, 0, sizeof(flash_disk_device));

    flash_disk_device.type    = OS_Device_Class_Block;
#ifdef OS_USING_DEVICE_OPS
    flash_disk_device.ops     = &flash_disk_ops;
#else
    flash_disk_device.init    = OS_NULL;
    flash_disk_device.open    = OS_NULL;
    flash_disk_device.close   = OS_NULL;
    flash_disk_device.read    = flash_disk_read;
    flash_disk_device.write   = flash_disk_write;
    flash_disk_device.control = flash_disk_control;
#endif /* OS_USING_DEVICE_OPS */

    /* register device */
    return os_device_register(&flash_disk_device, "disk0", \
                              OS_DEVICE_FLAG_RDWR | OS_DEVICE_FLAG_STANDALONE);
}
INIT_DEVICE_EXPORT(os_hw_flash_disk_init);

#endif