/**
****************************************************************************************
*
* @file driver.c
*
* @brief driver modules initialization
*
* Copyright (C) Beken Leonardo 2021
*
* $Rev: $
*
****************************************************************************************
*/

#include "include.h"
#include "driver_pub.h"
#include "dd_pub.h"
#include "drv_model_pub.h"

UINT32 soc_driver_init(void)
{    
    drv_model_init();

    g_dd_init();

    intc_init();
    
    return 0;
}

// eof
