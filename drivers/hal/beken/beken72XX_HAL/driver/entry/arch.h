/**
 ****************************************************************************************
 *
 * @file arch.h
 *
 * @brief This file contains the definitions of the macros and functions that are
 * architecture dependent.  The implementation of those is implemented in the
 * appropriate architecture directory.
 *
 * Copyright (C) RivieraWaves 2011-2016
 *
 ****************************************************************************************
 */
#ifndef _ARCH_H_
#define _ARCH_H_

/**
 ****************************************************************************************
 * @defgroup PLATFORM_DRIVERS PLATFORM_DRIVERS
 * @ingroup MACSW
 * @brief Declaration of the ATMEL AT91SAM261 architecture API.
 * @{
 ****************************************************************************************
 */

/**
 ****************************************************************************************
 * @defgroup ARCH_AT91SAM9261 ARCH_AT91SAM9261
 * @ingroup PLATFORM_DRIVERS
 * @brief Declaration of the ATMEL AT91SAM261 architecture API.
 * @{
 ****************************************************************************************
 */

/*
 * INCLUDE FILES
 ****************************************************************************************
 */
#include "generic.h"
#include "co_int.h"
#include "compiler.h"
#include "portmacro.h"
#include "drv_wlan.h"

#if (CFG_SUPPORT_RTT)
#include <os_task.h>
#endif

#ifndef GLOBAL_INT_START
#define GLOBAL_INT_START               portENABLE_INTERRUPTS
#endif // GLOBAL_INT_START

#ifndef GLOBAL_INT_STOP
#define GLOBAL_INT_STOP                portDISABLE_INTERRUPTS
#endif


#if (CFG_SUPPORT_RTT) 

extern os_task_id g_os_current_task;
extern os_mutex_id bk_mutex_dynamic;

extern void bk_mutex_init(void);

extern void bk_mutex_lock(void);

extern void  bk_mutex_unlock(void);

#define GLOBAL_INT_DECLARATION()   os_base_t bk_irq_level = -1;os_err_t lock_ret = OS_SUCCESS; bk_mutex_init()

#ifdef BSP_USE_RECURSIVE
#define GLOBAL_INT_DISABLE()  if (OS_TRUE == os_is_irq_active())                                    \
                                {                                                                   \
                                    bk_irq_level = os_irq_lock();                                   \
                                }                                                                   \
                                else                                                                \
                                {                                                                   \
                                    bk_irq_level = -1;                                              \
                                    lock_ret = os_mutex_recursive_lock(bk_mutex_dynamic, OS_NO_WAIT);\
                                }
                                    
#define GLOBAL_INT_RESTORE()      if (bk_irq_level != -1)                               \
                                    {                                                   \
                                        os_irq_unlock(bk_irq_level);                    \
                                    }                                                   \
                                    else                                                \
                                    {                                                   \
                                        if (lock_ret == OS_SUCCESS)                         \
                                            os_mutex_recursive_unlock(bk_mutex_dynamic);\
                                    }  

#else
#define GLOBAL_INT_DISABLE() 
#define GLOBAL_INT_RESTORE() 
#endif

#else
#define GLOBAL_INT_DECLARATION()   uint32_t fiq_tmp, irq_tmp
#define GLOBAL_INT_DISABLE()       do{                              \
										fiq_tmp = portDISABLE_FIQ();\
										irq_tmp = portDISABLE_IRQ();\
									}while(0)


#define GLOBAL_INT_RESTORE()       do{                         \
                                        if(!fiq_tmp)           \
                                        {                      \
                                            portENABLE_FIQ();  \
                                        }                      \
                                        if(!irq_tmp)           \
                                        {                      \
                                            portENABLE_IRQ();  \
                                        }                      \
                                   }while(0)
#endif

/*
 * CPU WORD SIZE
 ****************************************************************************************
 */
/// ARM is a 32-bit CPU
#define CPU_WORD_SIZE                  4

/*
 * CPU Endianness
 ****************************************************************************************
 */
/// ARM is little endian
#define CPU_LE                          1

#define ASSERT_REC(cond)
#define ASSERT_REC_VAL(cond, ret)
#define ASSERT_REC_NO_RET(cond)

#if !defined(ASSERT_ERR)
#define ASSERT_ERR(cond)                  ASSERT(cond)
#endif

#define ASSERT_ERR2(cond, param0, param1)
#if !defined(ASSERT_WARN)
#define ASSERT_WARN(cond)
#endif

/// @}
/// @}
#endif // _ARCH_H_
