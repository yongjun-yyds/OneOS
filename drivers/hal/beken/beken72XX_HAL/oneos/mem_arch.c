/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with 
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on 
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the 
 * specific language governing permissions and limitations under the License.
 *
 * @file        mem_arch.c
 *
 * @brief       adpt memory functions for beken drivers.
 *
 * @revision
 * Date         Author          Notes
 * 2020-12-18   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include "include.h"
#include "arm_arch.h"
#include <string.h>
#include <os_task.h>

extern void *os_malloc(os_size_t nbytes);
void *os_memset(void *b, int c, UINT32 len)
{
    return (void *)memset(b, c, (unsigned int)len);
}
void *os_memcpy(void *out, const void *in, UINT32 n)
{
    return memcpy(out, in, n);
}


#if (CFG_SUPPORT_RTT) && (CFG_SOC_NAME == SOC_BK7221U)
void *dtcm_malloc(size_t size)
{
	extern void *tcm_malloc(unsigned long size); 
    return (void *)tcm_malloc(size);
}
#endif

void * os_zalloc(size_t size)
{
	void *n = (void *)os_malloc(size);
	if (n)
		os_memset(n, 0, size);
	return n;
}

uint32_t os_memcmp(const void *area1, const void *area2, os_size_t count)
{
    const uint8_t *area1_tmp;
    const uint8_t *area2_tmp;
    uint32_t       ret;

    area1_tmp = (const uint8_t *)area1;
    area2_tmp = (const uint8_t *)area2;
    ret = 0;

    for ( ; count > 0; area1_tmp++, area2_tmp++, count--)
    {
        ret = *area1_tmp - *area2_tmp;
        if (ret != 0)
        {
            break;
        }
    }

    return ret;
}

int os_memcmp_const(const void *a, const void *b, size_t len)
{
    return os_memcmp(a, b, len);
}

void *os_memmove(void *dst, const void *src, os_size_t count)
{
    char       *dst_tmp;
    const char *src_tmp;

    dst_tmp = (char *)dst;
    src_tmp = (const char *)src;

    /* Copy backwards */
    if ((dst_tmp > src_tmp) && (dst_tmp < src_tmp + count))
    {
        dst_tmp += count;
        src_tmp += count;

        while (count--)
        {
            dst_tmp--;
            src_tmp--;
            *dst_tmp = *src_tmp;
        }
    }
    /* Copy forwards */
    else
    {
        while (count--)
        {
            *dst_tmp = *src_tmp;
            dst_tmp++;
            src_tmp++;
        }
    }

    return dst;
}

// EOF
