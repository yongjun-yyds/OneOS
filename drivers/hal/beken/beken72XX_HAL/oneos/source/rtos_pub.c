/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with 
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on 
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the 
 * specific language governing permissions and limitations under the License.
 *
 * @file        rtos_pub.c
 *
 * @brief       This file implements the oneos IPC functions for beken drivers
 *
 * @revision
 * Date         Author          Notes
 * 2020-12-18   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include "sys_rtos.h"
#include "error.h"
#include "rtos_pub.h"
#include "os_errno.h"
#include "mem_pub.h"

#include "os_types.h"
#include "os_util.h"
#include "os_sem.h"
#include "os_mutex.h"
#include <os_clock.h>
#include "os_timer.h"
#include "os_assert.h"


#define THREAD_TIMESLICE  5

#define RTOS_DEBUG        0
#if RTOS_DEBUG
#define RTOS_DBG(...)     os_printf("[RTOS]"),os_printf(__VA_ARGS__)
#else
#define RTOS_DBG(...)
#endif

os_mutex_id bk_mutex_dynamic = OS_NULL;

/******************************************************
 *               Function Definitions
 ******************************************************/

void bk_mutex_init(void)
{ 
    if (bk_mutex_dynamic == OS_NULL)
    {
        bk_mutex_dynamic = os_mutex_create(OS_NULL, "mutex_dynamic", OS_TRUE);
    }
}

OSStatus rtos_create_thread( beken_thread_t* thread, uint8_t priority, const char* name, 
                        beken_thread_function_t function, uint32_t stack_size, beken_thread_arg_t arg )
{
    if(thread == OS_NULL)
    {
        RTOS_DBG("can not create null thread\n");
        return kGeneralErr;
    }
    
    *thread = os_task_create(OS_NULL, OS_NULL, stack_size, name, function, arg, priority); 
    
    if(*thread != OS_NULL)
    {		
        os_task_startup(*thread);
    }
    else
    {
    	os_kprintf("create thread fail\n");
		
        RTOS_DBG("create thread fail\n");
        
        return kGeneralErr;
    }

    RTOS_DBG("create thread %s\n", name);
    
    return kNoErr;
}

OSStatus rtos_delete_thread( beken_thread_t* thread )
{
    if(thread != OS_NULL)
    { 
        os_task_destroy(*thread);
    }
    else
    {
        RTOS_DBG("thread == NULL, %s delete self NC\r\n", __FUNCTION__);
        return kGeneralErr;
    }

    RTOS_DBG("deleate thread\n");

    return kNoErr;

}

void rtos_thread_sleep(uint32_t seconds)
{
    os_task_msleep(seconds * 1000);
}

rt_sem_t rt_sem_create(const char *name, uint32_t value, uint8_t flag)
{
    rt_sem_t sem;
    os_err_t ret;

    OS_ASSERT(value < OS_SEM_MAX_VALUE);

    sem = (rt_sem_t)os_malloc(sizeof(struct rt_semaphore));
    if (OS_NULL == sem)
    {
        return RT_NULL;
    }

    if (OS_NULL == os_semaphore_create(&sem->os_sem, name, (uint16_t)value, OS_SEM_MAX_VALUE))
    {
        os_free(sem);
        return RT_NULL;
    }

    if (RT_IPC_FLAG_PRIO == flag)
    {
        ret = os_semaphore_set_wake_type(&sem->os_sem, OS_SEM_WAKE_TYPE_PRIO);
    }
    else
    {
        ret = os_semaphore_set_wake_type(&sem->os_sem, OS_SEM_WAKE_TYPE_FIFO);
    }
    if (OS_SUCCESS != ret)
    {
        return RT_NULL;
    }

    sem->is_static = OS_FALSE;

    return sem;
}

rt_err_t rt_sem_delete(rt_sem_t sem)
{
    os_err_t ret;
    OS_ASSERT(sem);
    OS_ASSERT(OS_FALSE == sem->is_static);

    ret = os_semaphore_destroy(&sem->os_sem);
    os_free(sem);
    if (OS_SUCCESS != ret)
    {
        return -RT_ERROR;
    }

    return RT_EOK;
}

rt_err_t rt_sem_take(rt_sem_t sem, int32_t time)
{
    os_tick_t timeout;
    os_err_t  ret;

    OS_ASSERT(sem);

    /*For OneOS,only support -1 for timeout,so set timeout is -1 when timeout is less than zero*/
    if (time < 0)
    {
        timeout = OS_WAIT_FOREVER;
    }
    else if (0 == time)
    {
        timeout = OS_NO_WAIT;
    }
    else
    {
        timeout = (os_tick_t)time;
    }

    ret = os_semaphore_wait(&sem->os_sem, timeout);
    if (OS_SUCCESS != ret)
    {
        if (OS_BUSY == ret || OS_TIMEOUT == ret)
        {
            return -RT_ETIMEOUT;
        }

        return -RT_ERROR;
    }

    return RT_EOK;
}

rt_err_t rt_sem_trytake(rt_sem_t sem)
{
    return rt_sem_take(sem, 0);
}

rt_err_t rt_sem_release(rt_sem_t sem)
{
    OS_ASSERT(sem);

    if (OS_SUCCESS != os_semaphore_post(&sem->os_sem))
    {
        return -RT_ERROR;
    }
    return RT_EOK;
}

OSStatus rtos_init_semaphore( beken_semaphore_t *semaphore, int maxCount )
{
    *semaphore = (struct rt_semaphore *) rt_sem_create("rtos_sem", 0, OS_NULL);

    return (*semaphore != OS_NULL) ? kNoErr : kGeneralErr;
}

OSStatus rtos_init_semaphore_ex( beken_semaphore_t *semaphore, const char *name, int maxCount, int initCount )
{
    *semaphore = (struct rt_semaphore *) rt_sem_create(name, initCount, OS_NULL);

    return (*semaphore != OS_NULL) ? kNoErr : kGeneralErr;
} 

OSStatus rtos_get_semaphore(beken_semaphore_t *semaphore, uint32_t timeout_ms )
{
    return rt_sem_take((struct rt_semaphore *)*semaphore, timeout_ms);
}

int rtos_get_sema_count(beken_semaphore_t *semaphore )
{
    RTOS_DBG("rtos_get_sema_count\n");
    os_semaphore_id sem = *semaphore;
    
    return sem->count_dummy;
}

int rtos_set_semaphore( beken_semaphore_t *semaphore)
{
    return  rt_sem_release((struct rt_semaphore *)*semaphore);
}

OSStatus rtos_deinit_semaphore(beken_semaphore_t *semaphore )
{
    return  rt_sem_delete((struct rt_semaphore *)*semaphore);
}

OSStatus rtos_init_mutex( beken_mutex_t* mutex )
{
    RTOS_DBG("rtos_init_mutex\n");

    /* Mutex uses priority inheritance */
    *mutex = os_mutex_create(OS_NULL, "rtos_mutex",OS_FALSE);
    
    if ( *mutex == OS_NULL )
    {
        return kGeneralErr;
    }

    return kNoErr;
}

OSStatus rtos_lock_mutex( beken_mutex_t* mutex)
{
    RTOS_DBG("rtos_lock_mutex\n");

    os_mutex_lock(*mutex, BEKEN_WAIT_FOREVER);

    return OS_SUCCESS;
}

OSStatus rtos_unlock_mutex( beken_mutex_t* mutex)
{
    RTOS_DBG("rtos_unlock_mutex\n");
     
    os_mutex_unlock(*mutex);

    return OS_SUCCESS;
}

OSStatus rtos_deinit_mutex( beken_mutex_t* mutex)
{
    if(*mutex != OS_NULL)
    {
        os_mutex_destroy(*mutex);

        *mutex = OS_NULL;
    }

    return OS_SUCCESS;
}

rt_mq_t rt_mq_create(const char *name, os_size_t msg_size, os_size_t max_msgs, uint8_t flag)
{
    rt_mq_t   mq;
    os_size_t pool_size;
    os_size_t align_msg_size;
    void     *msgpool;

    OS_ASSERT(msg_size >= OS_ALIGN_SIZE);
    OS_ASSERT(max_msgs > 0);

    mq = (rt_mq_t)os_malloc(sizeof(struct rt_messagequeue));
    if (OS_NULL == mq)
    {
        return RT_NULL;
    }

    align_msg_size = OS_ALIGN_UP(msg_size, OS_ALIGN_SIZE);
    pool_size      = max_msgs * (align_msg_size + sizeof(dummy_mq_msg_t));

    msgpool = os_malloc(pool_size);
    if (OS_NULL == msgpool)
    {
        os_free(mq);
        return RT_NULL;
    }

    if (OS_NULL == os_msgqueue_create_static(&mq->os_mq, name, msgpool, pool_size, msg_size))
    {
        os_free(msgpool);
        os_free(mq);

        return RT_NULL;
    }

    mq->is_static  = OS_FALSE;
    mq->start_addr = msgpool;

    return mq;
}

rt_err_t rt_mq_delete(rt_mq_t mq)
{
    os_err_t ret;

    OS_ASSERT(mq);
    OS_ASSERT(OS_FALSE == mq->is_static);
    OS_ASSERT(mq->start_addr);

    ret = os_msgqueue_destroy(&mq->os_mq);
    os_free(mq->start_addr);
    os_free(mq);
    if (OS_SUCCESS != ret)
    {
        return -RT_ERROR;
    }
    return RT_EOK;
}

rt_err_t rt_mq_send(rt_mq_t mq, void *buffer, os_size_t size)
{
    os_err_t ret;

    OS_ASSERT(mq);
    OS_ASSERT(buffer);
    OS_ASSERT(size);

    ret = os_msgqueue_send(&mq->os_mq, buffer, size, OS_NO_WAIT);
    if (OS_SUCCESS != ret)
    {
        if (OS_FULL == ret)
        {
            return -RT_EFULL;
        }

        return RT_ERROR;
    }

    return RT_EOK;
}

rt_err_t rt_mq_urgent(rt_mq_t mq, void *buffer, os_size_t size)
{
    os_err_t ret;

    OS_ASSERT(mq);
    OS_ASSERT(buffer);
    OS_ASSERT(size);

    ret = os_msgqueue_send_urgent(&mq->os_mq, buffer, size, OS_NO_WAIT);
    if (OS_SUCCESS != ret)
    {
        if (OS_FULL == ret)
        {
            return -RT_EFULL;
        }

        return RT_ERROR;
    }

    return RT_EOK;
}

rt_err_t rt_mq_recv(rt_mq_t mq, void *buffer, os_size_t size, int32_t timeout)
{
    os_size_t recv_size;
    os_tick_t timeout_tmp;
    os_err_t  ret;

    OS_ASSERT(mq);
    OS_ASSERT(buffer);
    OS_ASSERT(size);

    /*For OneOS,only support -1 for timeout,so set timeout is -1 when timeout is less than zero*/
    if (timeout < 0)
    {
        timeout_tmp = OS_WAIT_FOREVER;
    }
    else if (0 == timeout)
    {
        timeout_tmp = OS_NO_WAIT;
    }
    else
    {
        timeout_tmp = (os_tick_t)timeout;
    }

    ret = os_msgqueue_recv(&mq->os_mq, buffer, size, timeout_tmp, &recv_size);
    if (OS_SUCCESS != ret)
    {
        if (OS_EMPTY == ret || OS_TIMEOUT == ret)
        {
            return -RT_ETIMEOUT;
        }

        return -RT_ERROR;
    }

    return RT_EOK;
}

OSStatus rtos_init_queue(beken_queue_t *queue, const char* name, uint32_t message_size, uint32_t number_of_messages)
{
    *queue = rt_mq_create(name, message_size, number_of_messages, OS_NULL);

    return kNoErr;
}

OSStatus rtos_push_to_queue(beken_queue_t *queue, void* message, UINT32 timeout_ms)
{
    beken_queue_t mq = *queue;
    return rt_mq_send(mq, message, mq->os_mq.max_msg_size_dummy);
}

OSStatus rtos_push_to_queue_front(beken_queue_t* queue, void* message, uint32_t timeout_ms)
{
    beken_queue_t mq = *queue;
    
    os_kprintf("\nrtos_push_to_queue_front not implement!!!\n");

    return kGeneralErr;
}

OSStatus rtos_pop_from_queue(beken_queue_t* queue, void* message, uint32_t timeout_ms)
{
    beken_queue_t mq = *queue;
    return rt_mq_recv(mq, message, mq->os_mq.max_msg_size_dummy, os_tick_from_ms(timeout_ms));
}

OSStatus rtos_deinit_queue(beken_queue_t *queue)
{
    beken_queue_t mq = *queue;
    return rt_mq_delete(mq);
}

BOOL rtos_is_queue_empty(beken_queue_t *queue )
{
    beken_queue_t mq = *queue;

    if(mq->os_mq.used_msgs_dummy == 0)
    {
        return true;
    }

    return false;
}

BOOL rtos_is_queue_full(beken_queue_t* queue)
{
    beken_queue_t mq = *queue;

    if(mq->os_mq.queue_depth_dummy == 0)
    {
        return true;
    }

    return false;
}

OSStatus rtos_delay_milliseconds( uint32_t num_ms)
{
    os_task_msleep(num_ms);
} 

static void timer_oneshot_callback(void* parameter)
{
    beken2_timer_t *timer = (beken2_timer_t*)parameter;

    RTOS_DBG("one shot callback\n");

	if(BEKEN_MAGIC_WORD != timer->beken_magic)
	{
		return;
	}
    if (timer->function)
    {
        timer->function(timer->left_arg, timer->right_arg );
    }
}

OSStatus rtos_start_oneshot_timer( beken2_timer_t* timer)
{
    RTOS_DBG("oneshot_timer start %x\n",timer->handle);

    if(timer->handle != OS_NULL)
    {
        os_timer_start(timer->handle);   
        return OS_SUCCESS;
    }  

    return OS_FAILURE;
}

OSStatus rtos_stop_oneshot_timer(beken2_timer_t* timer)
{
    RTOS_DBG("oneshot_timer stop %x\n",timer->handle);

    if(timer->handle != OS_NULL)
    {
        os_timer_stop(timer->handle);
        return kNoErr;
    }

    return kGeneralErr;
}

BOOL rtos_is_oneshot_timer_init(beken2_timer_t* timer)
{
    RTOS_DBG("oneshot_timer is init \n");
    return timer->handle ? true : false;
}

BOOL rtos_is_oneshot_timer_running(beken2_timer_t* timer)
{
    os_timer_id *os_timer = (os_timer_id *)(timer->handle);

    RTOS_DBG("oneshot_timer is runing \n");
    return os_timer_is_active(*os_timer);
}

OSStatus rtos_oneshot_reload_timer( beken2_timer_t* timer)
{
    RTOS_DBG("reload oneshot_timer %x\n",timer->handle);

    if(rtos_is_oneshot_timer_running(timer))
    {
        os_timer_start(timer->handle);
        RTOS_DBG("timer is runing, set tick to 0 \r\n");
    }
    else
    {
        os_timer_start(timer->handle);
        RTOS_DBG("timer is stop, start timer \r\n");
    }
    return kNoErr;
}

OSStatus rtos_init_oneshot_timer( beken2_timer_t *timer, 
									uint32_t time_ms, 
									timer_2handler_t function,
									void* larg, 
									void* rarg )
{
	OSStatus ret = kNoErr;

    RTOS_DBG("create oneshot_timer \n");
    timer->function = function;
    timer->left_arg = larg;
    timer->right_arg = rarg;	
	timer->beken_magic = BEKEN_MAGIC_WORD;

    timer->handle = os_timer_create(OS_NULL, 
                                    "rtos_oneshot_time",
                                    timer_oneshot_callback,
                                    timer,
                                    os_tick_from_ms(time_ms),
                                    OS_TIMER_FLAG_ONE_SHOT);
    
    if ( timer->handle == NULL )
    {
        ret = kGeneralErr;
    }
    else
    {
        RTOS_DBG("create oneshot_timer %x\n",timer->handle);

    }
 
    return ret;
}

void rtos_deinit_free_beken_timer(os_timer_id t)
{
	/*
    ((beken2_timer_t *)(t->user_timer))->handle = 0;
    ((beken2_timer_t *)(t->user_timer))->function = 0;
    ((beken2_timer_t *)(t->user_timer))->left_arg = 0;
    ((beken2_timer_t *)(t->user_timer))->right_arg = 0;
    ((beken2_timer_t *)(t->user_timer))->beken_magic = 0;
    */
}

OSStatus rtos_deinit_oneshot_timer( beken2_timer_t* timer )
{
	OSStatus ret = kNoErr;

    RTOS_DBG("delete oneshot_timer %x\n",timer->handle);

    /* disable interrupt */
    //os_ubase_t level = os_irq_lock();
    
    os_timer_id *os_timer = (os_timer_id*)(timer->handle);
    
    if(os_timer_destroy(*(os_timer_id*)timer->handle) != OS_SUCCESS)
    {
        ret = OS_FAILURE;
    }
	else
	{
		timer->handle = 0;
		timer->function = 0;
		timer->left_arg = 0;
		timer->right_arg = 0;
		timer->beken_magic = 0;
	}
    /* enable interrupt */
    //os_irq_unlock(level);
    
    return ret;
}

static void timer_period_callback(void* parameter)
{
    beken_timer_t *timer = (beken_timer_t*)parameter;

    RTOS_DBG("period timer callback\n");

    if (timer->function)
    {
        timer->function(timer->arg);
    }
}

OSStatus rtos_start_timer( beken_timer_t* timer)
{
    RTOS_DBG("period_timer start \n");
    if(timer->handle != OS_NULL)
    {
        os_timer_start(timer->handle);
        return OS_SUCCESS;
    }

    return OS_FAILURE;
}

OSStatus rtos_stop_timer(beken_timer_t* timer)
{
    RTOS_DBG("period_timer stop \n");

    if(timer->handle != OS_NULL)
    {
        os_timer_stop(timer->handle);
        return OS_SUCCESS;
    }

    return OS_FAILURE;
}

BOOL rtos_is_timer_init(beken_timer_t* timer)
{
    RTOS_DBG("period_timer is init \n");
    return timer->handle ? true : false;
}

BOOL rtos_is_timer_running(beken_timer_t* timer)
{
    os_timer_id os_timer = (os_timer_id)(timer->handle);

    RTOS_DBG("period_timer is runing \n");
    return os_timer_is_active(os_timer);
}

OSStatus rtos_reload_timer( beken_timer_t* timer)
{
    RTOS_DBG("reload period timer\n");

    if(rtos_is_timer_running(timer))
    {
        os_timer_start(timer->handle);
        RTOS_DBG("timer is runing, set tick to 0 \r\n");
    }
    else
    {
        os_timer_start(timer->handle);
    }
    return kNoErr;
}

OSStatus rtos_change_period( beken_timer_t* timer, uint32_t time_ms)
{
    uint32_t timeout_value;

    timeout_value = os_tick_from_ms(time_ms);

    os_timer_start(timer->handle);

    return kNoErr;
}

OSStatus rtos_init_timer( beken_timer_t* timer, uint32_t time_ms, timer_handler_t function, void* arg)
{
	OSStatus ret = kNoErr;

    RTOS_DBG("create period_timer \n");
    timer->function = function;
    timer->arg      = arg;

    timer->handle = os_timer_create(OS_NULL,
                                    "rtos_period_time",
                                    timer_period_callback,
                                    timer,
                                    os_tick_from_ms(time_ms),
                                    OS_TIMER_FLAG_PERIODIC);
    
    if ( timer->handle == NULL )
    {
        ret = kNoErr;
    }
    else
    {
        //((os_timer_t*)(timer->handle))->user_timer = timer;
    }
  
    return ret;
}

OSStatus rtos_init_timer_ex( beken_timer_t* timer, const char* name, uint32_t time_ms, timer_handler_t function, void* arg)
{
	OSStatus ret = kNoErr;

    RTOS_DBG("create period_timer_ex \n");
    timer->function = function;
    timer->arg      = arg;

    timer->handle = os_timer_create(OS_NULL,
                                    name,
                                    timer_period_callback,
                                    timer,
                                    os_tick_from_ms(time_ms),
                                    OS_TIMER_FLAG_PERIODIC);
    
    if ( timer->handle == NULL )
    {
        ret = kNoErr;
    }
    else
    {
        //((rt_timer_t)(timer->handle))->user_timer = timer;
    }
 
    return ret;
}

OSStatus rtos_deinit_timer( beken_timer_t* timer )
{
	OSStatus ret = kNoErr;

    RTOS_DBG("delete period_timer \n");
    if(os_timer_destroy((os_timer_id)timer->handle) != OS_SUCCESS)
    {
        ret = kGeneralErr;
    }
	else
	{
		timer->handle = 0;
		timer->function = 0;
		timer->arg      = 0;
	}

    return ret;
}

uint32_t rtos_get_timer_expiry_time( beken_timer_t* timer )
{
    // TODO:
    os_kprintf("[TODO]:Note, rtos_get_timer_expiry_time is not implement \n");
    return 0;
}

uint32_t rtos_get_time(void)
{
    return os_tick_get_value();
}

uint32_t rtos_get_free_mem() {
    return 0;
}

os_ubase_t rt_hw_interrupt_disable(void)
{
    return 0;
}

void rt_hw_interrupt_enable(os_ubase_t level)
{
    return;
}

struct rt_hw_register
{
    uint32_t r0;
    uint32_t r1;
    uint32_t r2;
    uint32_t r3;
    uint32_t r4;
    uint32_t r5;
    uint32_t r6;
    uint32_t r7;
    uint32_t r8;
    uint32_t r9;
    uint32_t r10;
    uint32_t fp;
    uint32_t ip;
    uint32_t sp;
    uint32_t lr;
    uint32_t pc;
    uint32_t spsr;
    uint32_t cpsr;
};

#include "start_type_pub.h"

void os_free_loose(void *ptr)
{
    if(ptr)
    { 
        os_free(ptr);
    }
}

