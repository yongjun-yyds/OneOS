#ifndef _RELEASE_H_
#define _RELEASE_H_

#include "sdk_revision.h"

#define IP_11N_LIB_REV	"4.3.3"	/* The SW revision of IP 11n lib */
#define BLE_4X_LIB_REV	"4.2.1"	/* The SW revision of BLE4.x lib */
#define BLE_5X_LIB_REV	"5.1.4"	/* The SW revision of BLE5.x lib */
#define USB_2X_LIB_REV	"2.1.1"	/* The SW revision of USB2.x lib */

#endif
