/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        dev_hwtimer.c
 *
 * @brief       This file implements hwtimer driver for hc32
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <drv_hwtimer.h>

#ifdef OS_USING_TIMER_DRIVER
static const struct hc32_tim_info tim0_unit1_info = {
    .unit       = M4_TMR01,
    .clk_config = PWC_FCG2_PERIPH_TIM01,
    .channel    = Tim0_ChannelA,
    .irq_num    = Int001_IRQn,
    .irq_src    = INT_TMR01_GCMA,
};
OS_HAL_DEVICE_DEFINE("TIMER_Type", "tim1", tim0_unit1_info);

static const struct hc32_tim_info tim0_unit2_info = {
    .unit       = M4_TMR01,
    .clk_config = PWC_FCG2_PERIPH_TIM01,
    .channel    = Tim0_ChannelB,
    .irq_num    = Int002_IRQn,
    .irq_src    = INT_TMR01_GCMB,
};
OS_HAL_DEVICE_DEFINE("TIMER_Type", "tim2", tim0_unit2_info);
#endif
