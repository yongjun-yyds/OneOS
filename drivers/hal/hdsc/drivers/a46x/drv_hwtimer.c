/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_hwtimer.c
 *
 * @brief       This file implements timer driver for hc32.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <os_memory.h>
#include <timer/timer.h>
#include <drv_hwtimer.h>

static os_list_node_t hc32_timer_list = OS_LIST_INIT(hc32_timer_list);

#define TIM0_A_IRQ_HANDLER_DEFINE(tim0_unit)                                                              \
    static void TMR0_##tim0_unit##_CmpA_IrqHandler(void)                                                  \
    {                                                                                                     \
        struct hc32_timer *timer;                                                                         \
        if (TMR0_GetStatus(CM_TMR0_##tim0_unit, TMR0_FLAG_CMP_A) == SET)                         \
        {                                                                                                 \
            TMR0_ClearStatus(CM_TMR0_##tim0_unit, TMR0_FLAG_CMP_A);                                \
            os_list_for_each_entry(timer, &hc32_timer_list, struct hc32_timer, list)                      \
            {                                                                                             \
                if (timer->info->unit == CM_TMR0_##tim0_unit)                                             \
                {                                                                                         \
                    os_clockevent_isr((os_clockevent_t *)timer);                                          \
                    return;                                                                               \
                }                                                                                         \
            }                                                                                             \
        }                                                                                                 \
    }

TIM0_A_IRQ_HANDLER_DEFINE(1);
TIM0_A_IRQ_HANDLER_DEFINE(2);


#if defined(OS_USING_CLOCKSOURCE) || defined(OS_USING_CLOCKEVENT)
static uint64_t hc32_timer_read(void *clock)
{
    struct hc32_timer *timer;
    uint64_t           cnt;

    timer = (struct hc32_timer *)clock;

    cnt = TMR0_GetCountValue(timer->info->unit, timer->info->channel);
    
    return cnt; 
}
#endif

#ifdef OS_USING_CLOCKEVENT
static uint32_t get_clk_div(uint32_t prescaler)
{
    uint32_t clk_div; 

    /* The available frequency division factors are only 1,2,4,8,16,32,64,128,256,512,1024 */
    if (1 >= prescaler)
    {
        clk_div = TMR0_CLK_DIV1;
    }
    else if (2 >= prescaler)
    {
        clk_div = TMR0_CLK_DIV2;
    }
    else if (4 >= prescaler)
    {
        clk_div = TMR0_CLK_DIV4;
    }
    else if (8 >= prescaler)
    {
        clk_div = TMR0_CLK_DIV8;
    }
    else if (16 >= prescaler)
    {
        clk_div = TMR0_CLK_DIV16;
    }
    else if (32 >= prescaler)
    {
        clk_div = TMR0_CLK_DIV32;
    }
    else if (64 >= prescaler)
    {
        clk_div = TMR0_CLK_DIV64;
    }
    else if (128 >= prescaler)
    {
        clk_div = TMR0_CLK_DIV128;
    }
    else if (256 >= prescaler)
    {
        clk_div = TMR0_CLK_DIV256;
    }
    else if (512 >= prescaler)
    {
        clk_div = TMR0_CLK_DIV512;
    }
    else
    {
        clk_div = TMR0_CLK_DIV1024;
    }

    return clk_div;
}

static void hc32_timer_start(os_clockevent_t *ce, uint32_t prescaler, uint64_t count)
{
    struct hc32_timer      *timer;
    stc_tmr0_init_t         tim0_init;
    stc_irq_signin_config_t irq_cfg;

    OS_ASSERT(ce != OS_NULL);
    OS_ASSERT(prescaler != 0);
    OS_ASSERT(count != 0);

    timer = (struct hc32_timer *)ce;

    TMR0_StructInit(&tim0_init);

    tim0_init.u32ClockDiv = get_clk_div(prescaler);
    tim0_init.u16CompareValue = count;
    TMR0_Init(timer->info->unit, timer->info->channel, &tim0_init);

    switch(timer->info->unit_index)
    {
    case 1:
        irq_cfg.pfnCallback = &TMR0_1_CmpA_IrqHandler;
        break;
    case 2:
        irq_cfg.pfnCallback = &TMR0_2_CmpA_IrqHandler;
        break;
    default:
        os_kprintf("Timer0 unit_index error!");
        return;
    }
    irq_cfg.enIntSrc = timer->info->irq_src;
    irq_cfg.enIRQn   = timer->info->irq_num;
    INTC_IrqSignIn(&irq_cfg);

    NVIC_ClearPendingIRQ(irq_cfg.enIRQn);
    NVIC_SetPriority(irq_cfg.enIRQn, DDL_IRQ_PRIO_DEFAULT);
    NVIC_EnableIRQ(irq_cfg.enIRQn);

    /* Enable the specified interrupts of Timer2. */
    TMR0_IntCmd(timer->info->unit, TMR0_INT_CMP_A,  ENABLE);

    /* Start Timer2. */
    TMR0_Start(timer->info->unit, timer->info->channel);
}

static void hc32_timer_stop(os_clockevent_t *ce)
{
    struct hc32_timer *timer;

    OS_ASSERT(ce != OS_NULL);
    timer = (struct hc32_timer *)ce;

    INTC_IrqSignOut(timer->info->irq_num);
    NVIC_DisableIRQ(timer->info->irq_num);
    
    /* Disable the specified interrupts of Timer2. */
    TMR0_IntCmd(timer->info->unit, TMR0_INT_CMP_A, DISABLE);
    TMR0_Stop(timer->info->unit, timer->info->channel);
}

static const struct os_clockevent_ops hc32_tim_ops = {
    .start = hc32_timer_start,
    .stop  = hc32_timer_stop,
    .read  = hc32_timer_read,
};
#endif

static int hc32_tim_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    struct hc32_timer *timer;
    stc_tmr0_init_t    tim0_init;
    stc_clock_freq_t   stc_clk_freq;
    os_ubase_t         level;

    timer = os_calloc(1, sizeof(struct hc32_timer));
    OS_ASSERT(timer);

    timer->info = (struct hc32_tim_info *)dev->info;

    CLK_GetClockFreq(&stc_clk_freq);
    timer->freq = stc_clk_freq.u32Pclk1Freq;  

    /* Peripheral registers write unprotected */
    LL_PERIPH_WE(LL_PERIPH_ALL);
    /* Enable timer0 clock */
    FCG_Fcg2PeriphClockCmd(timer->info->clk_config, ENABLE);
    /* Peripheral registers write protected */
    LL_PERIPH_WP(LL_PERIPH_ALL);

    if (timer->info->mode == TIMER_MODE_TIM)
    {
#ifdef OS_USING_CLOCKSOURCE
        if (os_clocksource_best() == OS_NULL)
        {
            TMR0_StructInit(&tim0_init);

            tim0_init.u32ClockDiv     = TMR0_CLK_DIV32;
            tim0_init.u16CompareValue = 0xFFFFUL;
            TMR0_Init(timer->info->unit, timer->info->channel, &tim0_init);

            /* Start Timer2. */
            TMR0_Start(timer->info->unit, timer->info->channel);

            timer->clock.cs.rating = 160;
            timer->clock.cs.freq   = timer->freq / (1 << (tim0_init.u32ClockDiv >> TMR0_BCONR_CKDIVA_POS)); 
            timer->clock.cs.mask   = 0xFFFFUL;
            timer->clock.cs.read   = hc32_timer_read;

            os_clocksource_register(dev->name, &timer->clock.cs);
        }
        else
#endif
        {
#ifdef OS_USING_CLOCKEVENT
            timer->clock.ce.rating         = 320;
            timer->clock.ce.freq           = timer->freq;
            timer->clock.ce.mask           = 0xFFFFUL;

            /* The available frequency division factors are only 1,2,4,8,16,32,64,128,256,512,1024 */
            timer->clock.ce.prescaler_mask = 0xFFFUL;
            //timer->clock.ce.prescaler_bits = 16;
            
            timer->clock.ce.count_mask     = 0xFFFFUL;
            timer->clock.ce.count_bits     = 16;
            timer->clock.ce.feature        = OS_CLOCKEVENT_FEATURE_PERIOD;
            timer->clock.ce.min_nsec       = NSEC_PER_SEC / timer->clock.ce.freq;
            timer->clock.ce.ops            = &hc32_tim_ops;

            os_clockevent_register(dev->name, &timer->clock.ce);
#endif
        }
    }

    os_spin_lock_irqsave(&gs_device_lock, &level);
    os_list_add(&hc32_timer_list, &timer->list);
    os_spin_unlock_irqrestore(&gs_device_lock, level);

    return OS_SUCCESS;
}

OS_DRIVER_INFO hc32_tim_driver = {
    .name  = "TIMER_Type",
    .probe = hc32_tim_probe,
};

OS_DRIVER_DEFINE(hc32_tim_driver, OS_INIT_LEVEL_PRE_DEVICE, OS_INIT_SUBLEVEL_MIDDLE);

