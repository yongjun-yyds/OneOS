/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_usart.h
 *
 * @brief       The head file of usart driver for hc32
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __DRV_USART_H__
#define __DRV_USART_H__

#include "board.h"

#include "hc32_ll_usart.h"
#include "hc32_ll_gpio.h"
#include "hc32_ll_pwc.h"
#include "hc32_ll_interrupts.h"
#include "hc32_ll_fcg.h"
#include "hc32_ll.h"
#include "hc32_ll_dma.h"
#include "hc32_ll_aos.h"


#define _DMA_CH_REG_OFFSET(ch)              ((ch) * 0x40ul)
#define _DMA_CH_REG(reg_base, ch)           (*(volatile uint32_t *)((uint32_t)(reg_base) + _DMA_CH_REG_OFFSET(ch)))
#define READ_DMA_CH_REG(reg_base, ch)       (_DMA_CH_REG((reg_base), (ch)))

struct hc32_usart_info
{
    uint32_t          uart_device;
    CM_USART_TypeDef *idx;
    uint32_t          periph;
    CM_DMA_TypeDef   *dma_periph;
    uint8_t           dma_ch;
    uint32_t          dma_clk;
    IRQn_Type         rx_irqn;
    IRQn_Type         rx_err_irqn;
    IRQn_Type         dma_irqn;
    en_int_src_t      rx_int_Src;
    en_int_src_t      rx_err_int_Src;
    en_int_src_t      dma_int_Src;
    uint16_t          tx_port;
    uint16_t          tx_pin;
    uint16_t          tx_func;
    uint16_t          rx_port;
    uint16_t          rx_pin;
    uint16_t          rx_func;
    uint8_t           dma_support;
};

#endif /* __DRV_USART_H__ */
