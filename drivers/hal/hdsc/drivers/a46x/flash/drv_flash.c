/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_flash.c
 *
 * @brief       This file provides flash init/read/write/erase functions for hc32.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include "board.h"

#include "drv_flash.h"

/**
 * Read data from flash.
 * @note This operation's units is word.
 *
 * @param addr flash address
 * @param buf buffer to store read data
 * @param size read bytes size
 *
 * @return result
 */
int hc32_flash_read(uint32_t addr, uint8_t *buf, os_size_t size)
{
    size_t i;
    os_kprintf("flash_read_0\r\n");

    if ((addr + size) > HC32_FLASH_END_ADDRESS)
    {
        os_kprintf("read outrange flash size! addr is (0x%p)", (void *)(addr + size));
        return OS_INVAL;
    }

    for (i = 0; i < size; i++, buf++, addr++)
    {
        *buf = *(uint8_t *)addr;
    }
    return size;
}

/**
 * Write data to flash.
 * @note This operation's units is word.
 * @note This operation must after erase. @see flash_erase.
 *
 * @param addr flash address
 * @param buf the write data buffer
 * @param size write bytes size
 *
 * @return result
 */

int hc32_flash_write(uint32_t addr, const uint8_t *buf, os_size_t size)
{
    size_t   i, j;
    uint8_t *p         = (uint8_t *)buf;
    uint32_t writedata = 0;
    uint8_t  tempdata;

    if ((addr + size) > HC32_FLASH_END_ADDRESS)
    {
        os_kprintf("ERROR: write outrange flash size! addr is (0x%p)\n", (void *)(addr + size));
        return OS_INVAL;
    }

    if ((addr % 4) != 0)
    {
        os_kprintf("write addr must be 4-byte alignment\n");
        return OS_INVAL;
    }

    if (size < 1)
    {
        return OS_INVAL;
    }

    /* Register write enable for some required peripherals. */
    LL_PERIPH_WE(LL_PERIPH_PWC_CLK_RMU | LL_PERIPH_FCG | LL_PERIPH_EFM | LL_PERIPH_SRAM);
    /* Wait flash ready. */
    while (SET != EFM_GetStatus(EFM_FLAG_RDY)) {
        ;
    }
    /* EFM_FWMC write enable */
    EFM_FWMC_Cmd(ENABLE);

    for (i = 0; i < size;)
    {
        if ((size - i) < 4)
        {
            for (j = 0; (size - i) > 0; j++, i++)
            {
                tempdata = *p;
                writedata |= tempdata << (8 * j);
                p++;
            }
        }
        else
        {
            for (j = 0; j < 4; j++, i++)
            {
                tempdata = *p;
                writedata |= tempdata << (8 * j);
                p++;
            }
        }
        if (EFM_ProgramWord(addr, writedata) == LL_OK)
        {
            if(*(uint32_t *)addr != writedata)
            {
                EFM_FWMC_Cmd(DISABLE);
                /* Register write protected for some required peripherals. */
                LL_PERIPH_WP(LL_PERIPH_PWC_CLK_RMU | LL_PERIPH_FCG | LL_PERIPH_EFM | LL_PERIPH_SRAM);
                os_kprintf("write error\n");
                return OS_FAILURE;
            }
        }
        else
        {
            EFM_FWMC_Cmd(DISABLE);
            /* Register write protected for some required peripherals. */
            LL_PERIPH_WP(LL_PERIPH_PWC_CLK_RMU | LL_PERIPH_FCG | LL_PERIPH_EFM | LL_PERIPH_SRAM);
            os_kprintf("write error\n");
            return OS_FAILURE;
        }
        tempdata  = 0;
        writedata = 0;
        addr += 4;
    }

    EFM_FWMC_Cmd(DISABLE);
    /* Register write protected for some required peripherals. */
    LL_PERIPH_WP(LL_PERIPH_PWC_CLK_RMU | LL_PERIPH_FCG | LL_PERIPH_EFM | LL_PERIPH_SRAM);
    return size;
}

/**
 * Erase data on flash.
 * @note This operation is irreversible.
 * @note This operation's units is different which on many chips.
 *
 * @param addr flash address
 * @param size erase bytes size
 *
 * @return result
 */
int hc32_flash_erase(uint32_t addr, os_size_t size)
{
    uint16_t sector_start = 0;
    uint16_t sector_end   = 0;
    uint16_t sector_cnt   = 0;

    if ((addr + size) > HC32_FLASH_END_ADDRESS)
    {
        os_kprintf("ERROR: erase outrange flash size! addr is (0x%p)\n", (void *)(addr + size));
        return OS_INVAL;
    }

    addr = addr - addr % HC32_SECTOR_SIZE;

    sector_start = addr / HC32_SECTOR_SIZE;
    sector_end   = (addr + size) / HC32_SECTOR_SIZE + (((addr + size) % HC32_SECTOR_SIZE) ? 1 : 0);

    /* Register write enable for some required peripherals. */
    LL_PERIPH_WE(LL_PERIPH_PWC_CLK_RMU | LL_PERIPH_FCG | LL_PERIPH_EFM | LL_PERIPH_SRAM);
        /* Wait flash ready. */
    while (SET != EFM_GetStatus(EFM_FLAG_RDY)) {
        ;
    }
      /* EFM_FWMC write enable */
    EFM_FWMC_Cmd(ENABLE);

    for (sector_cnt = 0; sector_cnt < (sector_end - sector_start); sector_cnt++)
    {
        EFM_SectorErase((sector_start + sector_cnt) * HC32_SECTOR_SIZE);
    }
    EFM_FWMC_Cmd(DISABLE);
    /* Register write protected for some required peripherals. */
    LL_PERIPH_WP(LL_PERIPH_PWC_CLK_RMU | LL_PERIPH_FCG | LL_PERIPH_EFM | LL_PERIPH_SRAM);

    return size;
}

#if defined(OS_USING_FAL)

static int hc32_flash_init(fal_flash_t *flash)
{
    uint32_t    sysclk_freq = 0;
    uint32_t    latency     = 0;
    stc_clock_freq_t stc_clk_freq;

    CLK_GetClockFreq(&stc_clk_freq);
    sysclk_freq = stc_clk_freq.u32SysclkFreq;

    if (sysclk_freq <= 33000000)
    {
        latency = EFM_WAIT_CYCLE0;
    }
    else if ((sysclk_freq > 33000000) && (sysclk_freq <= 66000000))
    {
        latency = EFM_WAIT_CYCLE1;
    }
    else if ((sysclk_freq > 66000000) && (sysclk_freq <= 99000000))
    {
        latency = EFM_WAIT_CYCLE2;
    }
    else if ((sysclk_freq > 99000000) && (sysclk_freq <= 132000000))
    {
        latency = EFM_WAIT_CYCLE3;
    }
    else if ((sysclk_freq > 132000000) && (sysclk_freq <= 168000000))
    {
        latency = EFM_WAIT_CYCLE4;
    }
    else if ((sysclk_freq > 168000000) && (sysclk_freq <= 200000000))
    {
        latency = EFM_WAIT_CYCLE5;
    }

    /* Register write enable for some required peripherals. */
    LL_PERIPH_WE(LL_PERIPH_PWC_CLK_RMU | LL_PERIPH_FCG | LL_PERIPH_EFM | LL_PERIPH_SRAM);
    EFM_SetWaitCycle(latency);
    /* Register write protected for some required peripherals. */
    LL_PERIPH_WP(LL_PERIPH_PWC_CLK_RMU | LL_PERIPH_FCG | LL_PERIPH_EFM | LL_PERIPH_SRAM);

    /* Enable flash. Default value is Enable*/
    EFM_Cmd(EFM_CHIP_ALL, ENABLE);
    /* Wait flash ready. */
    while (SET != EFM_GetStatus(EFM_FLAG_RDY)) {
        ;
    }

    return OS_SUCCESS;
}

#include "fal_drv_flash.c"

#endif /* BSP_USING_ON_CHIP_FLASH */
