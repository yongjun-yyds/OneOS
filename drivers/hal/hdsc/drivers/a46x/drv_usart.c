/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_usart.c
 *
 * @brief       This file implements usart driver for hc32
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <drv_cfg.h>
#include <device.h>
#include <string.h>
#include <os_memory.h>
#include "drv_usart.h"

struct hc32_usart_info *console_usart = NULL;

static os_list_node_t hc32_uart_list = OS_LIST_INIT(hc32_uart_list);

/* uart driver */
typedef struct hc_uart
{
    struct os_serial_device serial_dev;
    struct hc32_usart_info *info;
    soft_dma_t              sdma;
    uint8_t                *rx_buff;
    uint32_t                rx_index;
    uint32_t                rx_size;
    os_list_node_t          list;
} hc_usart_t;

#ifndef UART1_USING_DMA

static void uart_isr(struct os_serial_device *serial)
{
    struct hc_uart *uart;

    OS_ASSERT(serial != OS_NULL);
    uart = os_container_of(serial, struct hc_uart, serial_dev);

    OS_ASSERT(uart != OS_NULL);

    if (SET == USART_GetStatus(uart->info->idx, USART_FLAG_RX_FULL))
    {

        uart->rx_buff[uart->rx_index++] = USART_ReadData(uart->info->idx);

        if (uart->rx_index == (uart->rx_size / 2))
        {
            soft_dma_half_irq(&uart->sdma);
        }

        if (uart->rx_index == uart->rx_size)
        {
            uart->rx_index = 0;
            soft_dma_full_irq(&uart->sdma);
        }
    }
}

static void UsartRxIrqCallback(void)
{
    struct hc_uart *uart;

    os_list_for_each_entry(uart, &hc32_uart_list, struct hc_uart, list)
    {
        if (uart->info->uart_device == 1)
        {
            uart_isr(&uart->serial_dev);
            break;
        }
    }
}

#endif

#ifdef UART1_USING_DMA
static void UsartErrIrqCallback(void)
{
    struct hc_uart *uart;

    os_list_for_each_entry(uart, &hc32_uart_list, struct hc_uart, list)
    {
        if (uart->info->uart_device == 1)
        {
            if (SET == USART_GetStatus(uart->info->idx, USART_FLAG_FRAME_ERR))
            {
                os_kprintf("UsartFrameErr!\r\n");
                USART_ClearStatus(uart->info->idx, USART_FLAG_FRAME_ERR);
            }
            if (SET == USART_GetStatus(uart->info->idx, USART_FLAG_PARITY_ERR))
            {
                os_kprintf("UsartParityErr!\r\n");
                USART_ClearStatus(uart->info->idx, USART_FLAG_PARITY_ERR);
            }
            if (SET == USART_GetStatus(uart->info->idx, USART_FLAG_OVERRUN))
            {
                os_kprintf("UsartOverrunErr!\r\n");
                USART_ClearStatus(uart->info->idx, USART_FLAG_OVERRUN);
            }
            break;
        }
    }
}
#endif
static void DmaTrnCpltIrqCallback(void)
{
    struct hc_uart *uart;

    os_list_for_each_entry(uart, &hc32_uart_list, struct hc_uart, list)
    {
        if (uart->info->uart_device == 1)
        {
            if(DMA_GetTransCompleteStatus(uart->info->dma_periph, DMA_FLAG_TC_CH0) == SET)
            {		
                DMA_ClearTransCompleteStatus(uart->info->dma_periph, DMA_FLAG_TC_CH0);
                soft_dma_half_irq(&uart->sdma);
            }
            break;
        }
    }
}


/* interrupt rx mode */
static uint32_t hc_sdma_int_get_index(soft_dma_t *dma)
{
    hc_usart_t *uart = os_container_of(dma, hc_usart_t, sdma);

    return uart->rx_index;
}

static os_err_t hc_sdma_int_start(soft_dma_t *dma, void *buff, uint32_t size)
{
    hc_usart_t *uart = os_container_of(dma, hc_usart_t, sdma);

    uart->rx_buff  = buff;
    uart->rx_index = 0;
    uart->rx_size  = size;

    USART_FuncCmd(uart->info->idx, USART_RX, ENABLE);
    USART_FuncCmd(uart->info->idx, USART_INT_RX, ENABLE);
    USART_FuncCmd(uart->info->idx, USART_TX, ENABLE);


    return OS_SUCCESS;
}

static uint32_t hc_sdma_int_stop(soft_dma_t *dma)
{
    hc_usart_t *uart = os_container_of(dma, hc_usart_t, sdma);

    USART_FuncCmd(uart->info->idx, USART_RX, ENABLE);
    USART_FuncCmd(uart->info->idx, USART_INT_RX, ENABLE);
    USART_FuncCmd(uart->info->idx, USART_TX, ENABLE);

    return uart->rx_index;
}

/* dma rx mode */
static uint32_t hc_sdma_dma_get_index(soft_dma_t *dma)
{
    hc_usart_t *uart = os_container_of(dma, hc_usart_t, sdma);

    return (uint32_t)READ_DMA_CH_REG(&uart->info->dma_periph->MONDAR0, uart->info->dma_ch) - (uint32_t)uart->rx_buff;

}

static os_err_t hc_sdma_dma_init(soft_dma_t *dma)
{
    return OS_SUCCESS;
}

static os_err_t hc_sdma_dma_start(soft_dma_t *dma, void *buff, uint32_t size)
{
		hc_usart_t               *uart;
    stc_dma_init_t           stcDmaInit;
    stc_dma_llp_init_t       stcDmaLlpInit;
    stc_irq_signin_config_t  stcIrqRegiCfg;
    static stc_dma_llp_descriptor_t  stcLlpDesc;

    uart = os_container_of(dma, hc_usart_t, sdma);

    uart->rx_buff = buff;

    /* Register write enable for some required peripherals. */
    LL_PERIPH_WE(LL_PERIPH_ALL);	
    /* DMA/AOS FCG enable */
    FCG_Fcg0PeriphClockCmd(uart->info->dma_clk, ENABLE);
    FCG_Fcg0PeriphClockCmd(FCG0_PERIPH_AOS, ENABLE);

    /* MCU Peripheral registers write protected */
    LL_PERIPH_WP(LL_PERIPH_ALL);

    /* USART_RX_DMA */
    DMA_StructInit(&stcDmaInit);
    stcDmaInit.u32IntEn = DMA_INT_ENABLE;
    stcDmaInit.u32BlockSize = 1UL;
    stcDmaInit.u32TransCount = size / 2;
    stcDmaInit.u32DataWidth = DMA_DATAWIDTH_8BIT;
    stcDmaInit.u32DestAddr = (uint32_t)(buff);
    stcDmaInit.u32SrcAddr = ((uint32_t)(&uart->info->idx->DR)+2ul);
    stcDmaInit.u32SrcAddrInc = DMA_SRC_ADDR_FIX;
    stcDmaInit.u32DestAddrInc = DMA_DEST_ADDR_INC;
    if(LL_OK == (DMA_Init(uart->info->dma_periph, uart->info->dma_ch, &stcDmaInit)))
        {
             (void)DMA_LlpStructInit(&stcDmaLlpInit);
             stcDmaLlpInit.u32State = DMA_LLP_ENABLE;
             stcDmaLlpInit.u32Mode  = DMA_LLP_WAIT;
             stcDmaLlpInit.u32Addr  = (uint32_t)&stcLlpDesc;
             DMA_LlpInit(uart->info->dma_periph, uart->info->dma_ch, &stcDmaLlpInit);
 
             stcLlpDesc.SARx   = stcDmaInit.u32SrcAddr;
             stcLlpDesc.DARx   = stcDmaInit.u32DestAddr;
             stcLlpDesc.DTCTLx = (stcDmaInit.u32TransCount << DMA_DTCTL_CNT_POS) | (stcDmaInit.u32BlockSize << DMA_DTCTL_BLKSIZE_POS);;
             stcLlpDesc.LLPx   = (uint32_t)&stcLlpDesc;
             stcLlpDesc.CHCTLx = stcDmaInit.u32SrcAddrInc | stcDmaInit.u32DestAddrInc | stcDmaInit.u32DataWidth |  \
                                 stcDmaInit.u32IntEn      | stcDmaLlpInit.u32State    | stcDmaLlpInit.u32Mode;
 
             DMA_ReconfigLlpCmd(uart->info->dma_periph, uart->info->dma_ch, ENABLE);
             DMA_ReconfigCmd(uart->info->dma_periph, ENABLE);
             AOS_SetTriggerEventSrc(AOS_DMA_RC, EVT_SRC_AOS_STRG);

            /* Set DMA block transfer complete IRQ */
            stcIrqRegiCfg.enIRQn      = uart->info->dma_irqn;
            stcIrqRegiCfg.pfnCallback = &DmaTrnCpltIrqCallback;
            stcIrqRegiCfg.enIntSrc    = uart->info->dma_int_Src;
            INTC_IrqSignIn(&stcIrqRegiCfg);

            /* NVIC setting */
            NVIC_SetPriority(stcIrqRegiCfg.enIRQn, 0);
            NVIC_ClearPendingIRQ(stcIrqRegiCfg.enIRQn);
            NVIC_EnableIRQ(stcIrqRegiCfg.enIRQn);

            AOS_SetTriggerEventSrc(AOS_DMA1_0, EVT_SRC_AOS_STRG);

            /* DMA module enable */
            DMA_Cmd(uart->info->dma_periph, ENABLE);

            DMA_TransCompleteIntCmd(uart->info->dma_periph, DMA_INT_TC_CH0, ENABLE);

            /* DMA channel enable */
            DMA_ChCmd(uart->info->dma_periph, uart->info->dma_ch, ENABLE);
        }

    USART_FuncCmd(uart->info->idx, USART_INT_RX, ENABLE);
    USART_FuncCmd(uart->info->idx, USART_RX, ENABLE);

    return OS_SUCCESS;
}

static uint32_t hc_sdma_dma_stop(soft_dma_t *dma)
{
    hc_usart_t *uart;

    uart = os_container_of(dma, hc_usart_t, sdma);

    DMA_Cmd(uart->info->dma_periph, DISABLE);

    return hc_sdma_dma_get_index(dma);
}

/* sdma callback */
static void hc_usart_sdma_callback(soft_dma_t *dma)
{
    hc_usart_t *uart = os_container_of(dma, hc_usart_t, sdma);

    os_hw_serial_isr_rxdone((struct os_serial_device *)uart);
}

static void hc_usart_sdma_init(struct hc_uart *uart, dma_ring_t *ring)
{
    soft_dma_t *dma = &uart->sdma;

    soft_dma_stop(dma);

    memset(&dma->hard_info, 0, sizeof(dma->hard_info));

    dma->hard_info.mode         = HARD_DMA_MODE_NORMAL;
    dma->hard_info.max_size     = 64 * 1024;
    dma->hard_info.data_timeout = uart_calc_byte_timeout_us(uart->serial_dev.config.baud_rate);

    if (!uart->info->dma_support)
    {
        dma->ops.get_index = hc_sdma_int_get_index;
        dma->ops.dma_init  = OS_NULL;
        dma->ops.dma_start = hc_sdma_int_start;
        dma->ops.dma_stop  = hc_sdma_int_stop;
    }
    else
    {
        dma->hard_info.flag |= HARD_DMA_FLAG_FULL_IRQ;
        dma->ops.get_index = hc_sdma_dma_get_index;
        dma->ops.dma_init  = hc_sdma_dma_init;
        dma->ops.dma_start = hc_sdma_dma_start;
        dma->ops.dma_stop  = hc_sdma_dma_stop;
    }

    dma->cbs.dma_half_callback    = hc_usart_sdma_callback;
    dma->cbs.dma_full_callback    = hc_usart_sdma_callback;
    dma->cbs.dma_timeout_callback = hc_usart_sdma_callback;

    soft_dma_init(dma);
    soft_dma_start(dma, ring);
    soft_dma_irq_enable(&uart->sdma, OS_TRUE);
}

static os_err_t hc_uart_init(struct os_serial_device *serial, struct serial_configure *cfg)
{
    struct hc_uart          *uart;
    stc_usart_uart_init_t   stcInitCfg;
    stc_irq_signin_config_t stcIrqRegiCfg;

    OS_ASSERT(serial != OS_NULL);
    OS_ASSERT(cfg != OS_NULL);

    uart = os_container_of(serial, struct hc_uart, serial_dev);


    /* MCU Peripheral registers write unprotected */
    LL_PERIPH_WE(LL_PERIPH_ALL);

    /* Configure USART RX/TX pin. */
    GPIO_SetFunc(uart->info->tx_port, uart->info->tx_pin, uart->info->tx_func);
    GPIO_SetFunc(uart->info->rx_port, uart->info->rx_pin, uart->info->rx_func);

    /* Enable peripheral clock */
    FCG_Fcg1PeriphClockCmd(uart->info->periph, ENABLE);

    /* Initialize UART. */
    USART_UART_StructInit(&stcInitCfg);
    stcInitCfg.u32ClockDiv       = USART_CLK_DIV1;
    stcInitCfg.u32CKOutput       = USART_CK_OUTPUT_ENABLE;
    stcInitCfg.u32Baudrate       = cfg->baud_rate;
    stcInitCfg.u32OverSampleBit  = USART_OVER_SAMPLE_8BIT;

    switch (cfg->stop_bits)
    {
    case STOP_BITS_1:
        stcInitCfg.u32StopBit = USART_STOPBIT_1BIT;
        break;
    case STOP_BITS_2:
        stcInitCfg.u32StopBit = USART_STOPBIT_2BIT;
        break;
    default:
        return OS_INVAL;
    }
    
    switch (cfg->parity)
    {
    case PARITY_NONE:
        stcInitCfg.u32Parity = USART_PARITY_NONE;
        break;
    case PARITY_ODD:
        stcInitCfg.u32Parity = USART_PARITY_ODD;
        break;
    case PARITY_EVEN:
        stcInitCfg.u32Parity = USART_PARITY_EVEN;
        break;
    default:
        return OS_INVAL;
    }

    switch (cfg->data_bits)
    {
    case DATA_BITS_8:
        stcInitCfg.u32DataWidth = USART_DATA_WIDTH_8BIT;
        break;
    case DATA_BITS_9:
        stcInitCfg.u32DataWidth = USART_DATA_WIDTH_9BIT;
        break;
    default:
        return OS_INVAL;
    }
    if (LL_OK != USART_UART_Init(uart->info->idx, &stcInitCfg, NULL)) 
    {
        os_kprintf("usart initialization failed!\r\n");
        return OS_FAILURE;
    }

#ifndef UART1_USING_DMA
    /* Register RX NoEmpty interrupt */
    stcIrqRegiCfg.enIRQn       = uart->info->rx_irqn;
    stcIrqRegiCfg.enIntSrc     = uart->info->rx_int_Src;
    stcIrqRegiCfg.pfnCallback  = &UsartRxIrqCallback;
    INTC_IrqSignIn(&stcIrqRegiCfg);

    NVIC_ClearPendingIRQ(stcIrqRegiCfg.enIRQn);
    NVIC_SetPriority(stcIrqRegiCfg.enIRQn, DDL_IRQ_PRIO_01);
    NVIC_EnableIRQ(stcIrqRegiCfg.enIRQn);
#endif
#if 0
    /* Register RX ERROR interrupt */
    stcIrqRegiCfg.enIRQn      = uart->info->rx_err_irqn;
    stcIrqRegiCfg.pfnCallback = &UsartErrIrqCallback;
    stcIrqRegiCfg.enIntSrc    = uart->info->rx_err_int_Src;
    INTC_IrqSignIn(&stcIrqRegiCfg);
    NVIC_SetPriority(stcIrqRegiCfg.enIRQn, DDL_IRQ_PRIO_00);
    NVIC_ClearPendingIRQ(stcIrqRegiCfg.enIRQn);
    NVIC_EnableIRQ(stcIrqRegiCfg.enIRQn);
#endif

    /* MCU Peripheral registers write protected */
    LL_PERIPH_WP(LL_PERIPH_ALL);

    /* Enable TX && RX && RX interrupt function */
    USART_FuncCmd(uart->info->idx, (USART_RX | USART_INT_RX | USART_TX), ENABLE);

    hc_usart_sdma_init(uart, &uart->serial_dev.rx_fifo->ring);

    return OS_SUCCESS;
}

static os_err_t hc_uart_deinit(struct os_serial_device *serial)
{
    OS_ASSERT(serial != OS_NULL);

    hc_usart_t *uart = os_container_of(serial, hc_usart_t, serial_dev);

    if (uart->info->dma_support)
    {
        NVIC_DisableIRQ(uart->info->rx_err_irqn);
        NVIC_DisableIRQ(uart->info->dma_irqn);
        
        DMA_DeInit(uart->info->dma_periph, uart->info->dma_ch);
        USART_DeInit(uart->info->idx);

        soft_dma_deinit(&uart->sdma);
        soft_dma_stop(&uart->sdma);
    }
    else
    {
        NVIC_DisableIRQ(uart->info->rx_irqn);
        NVIC_DisableIRQ(uart->info->rx_err_irqn);

        USART_DeInit(uart->info->idx);

        soft_dma_deinit(&uart->sdma);
        soft_dma_stop(&uart->sdma);
    }

    return OS_SUCCESS;
}

static int hc_uart_poll_send(struct os_serial_device *serial, const uint8_t *buff, os_size_t size)
{
    int              i;
    os_ubase_t       level;
    struct hc_uart   *uart;

    OS_ASSERT(serial != OS_NULL);
    uart = os_container_of(serial, struct hc_uart, serial_dev);

    OS_ASSERT(uart != OS_NULL);

    for (i = 0; i < size; i++)
    {
        os_spin_lock_irqsave(&gs_device_lock, &level);

        while (RESET == USART_GetStatus(uart->info->idx, USART_FLAG_TX_EMPTY))
        {
        }
        USART_WriteData(uart->info->idx, buff[i]);

        os_spin_unlock_irqrestore(&gs_device_lock, level);
    }
    while (RESET == USART_GetStatus(console_usart->idx, USART_FLAG_TX_CPLT))
        {
        }

    return size;
}

static const struct os_uart_ops hc_uart_ops = {
    .init   = hc_uart_init,
    .deinit = hc_uart_deinit,

    .poll_send = hc_uart_poll_send,
};

static int hc32_usart_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    os_ubase_t level;

    struct serial_configure config = OS_SERIAL_CONFIG_DEFAULT;

    struct hc_uart *usart;

    usart = os_calloc(1, sizeof(struct hc_uart));
    OS_ASSERT(usart);

    usart->info                     = (struct hc32_usart_info *)dev->info;
    struct os_serial_device *serial = &usart->serial_dev;

    os_spin_lock_irqsave(&gs_device_lock, &level);
    os_list_add_tail(&hc32_uart_list, &usart->list);
    os_spin_unlock_irqrestore(&gs_device_lock, level);

    serial->ops    = &hc_uart_ops;
    serial->config = config;

    /* register uart device */
    os_hw_serial_register(serial, dev->name, usart);

    return 0;
}

OS_DRIVER_INFO hc32_usart_driver = {
    .name  = "Usart_Type",
    .probe = hc32_usart_probe,
};

OS_DRIVER_DEFINE(hc32_usart_driver, OS_INIT_LEVEL_PRE_DEVICE, OS_INIT_SUBLEVEL_HIGH);


void __os_hw_console_output(char *str)
{
    int i;
    
    if (console_usart == OS_NULL)
    {
        return;
    }

    for (i = 0; i < strlen(str); i++)
    {
        while (RESET == USART_GetStatus(console_usart->idx, USART_FLAG_TX_EMPTY))
        {
        }
        USART_WriteData(console_usart->idx, str[i]);
    }
}

static os_err_t hc_uart_early_init(struct hc32_usart_info *usart)
{
    stc_usart_uart_init_t stcUartInit;

    /* MCU Peripheral registers write unprotected */
    LL_PERIPH_WE(LL_PERIPH_ALL);

    /* Configure USART RX/TX pin. */
    GPIO_SetFunc(usart->tx_port, usart->tx_pin, usart->tx_func);
    GPIO_SetFunc(usart->rx_port, usart->rx_pin, usart->rx_func);

    /* Enable peripheral clock */
    FCG_Fcg1PeriphClockCmd(usart->periph, ENABLE);

    /* Initialize UART. */
    (void)USART_UART_StructInit(&stcUartInit);
    stcUartInit.u32ClockDiv = USART_CLK_DIV64;
    stcUartInit.u32Baudrate = 115200UL;
    stcUartInit.u32OverSampleBit = USART_OVER_SAMPLE_8BIT;

    if (LL_OK != USART_UART_Init(usart->idx, &stcUartInit, NULL)) 
    {
        return OS_FAILURE;
    }

    /* MCU Peripheral registers write protected */
    LL_PERIPH_WP(LL_PERIPH_ALL);

    /* Enable RX/TX function */
    USART_FuncCmd(usart->idx, (USART_RX | USART_TX), ENABLE);

    return OS_SUCCESS;
}

static int hc32_uart_early_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    if (strcmp(dev->name, OS_CONSOLE_DEVICE_NAME))
    {
        return OS_SUCCESS;
    }
        
    console_usart = (struct hc32_usart_info *)dev->info;

    hc_uart_early_init(console_usart);

    return OS_SUCCESS;
}

OS_DRIVER_INFO hc32_uart_early_driver = {
    .name  = "Usart_Type",
    .probe = hc32_uart_early_probe,
};

OS_DRIVER_DEFINE(hc32_uart_early_driver, OS_INIT_LEVEL_PRE_KERNEL_1, OS_INIT_SUBLEVEL_LOW);

