/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_flash.c
 *
 * @brief       This file provides flash init/read/write/erase functions for hc32.
 *
 * @revision
 * Date         Author          Notes
 * 2021-06-10   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include "drv_flash.h"

/**
 * Read data from flash.
 * @note This operation's units is word.
 *
 * @param addr flash address
 * @param buf buffer to store read data
 * @param size read bytes size
 *
 * @return result
 */
int hc32_flash_read(uint32_t addr, uint8_t *buf, os_size_t size)
{
    size_t i;

    if ((addr + size) > HC32_FLASH_END_ADDRESS)
    {
        os_kprintf("read outrange flash size! addr is (0x%p)", (void *)(addr + size));
        return OS_INVAL;
    }

    for (i = 0; i < size; i++, buf++, addr++)
    {
        *buf = *(uint8_t *)addr;
    }

    return size;
}

/**
 * Write data to flash.
 * @note This operation's units is word.
 * @note This operation must after erase. @see flash_erase.
 *
 * @param addr flash address
 * @param buf the write data buffer
 * @param size write bytes size
 *
 * @return result
 */
int hc32_flash_write(uint32_t addr, const uint8_t *buf, os_size_t size)
{
    size_t   i;
    size_t   j;
    uint16_t sector_num = 0;
    uint8_t *p          = (uint8_t *)buf;
    uint32_t writedata  = 0;
    uint8_t  tempdata;

    if ((addr + size) > HC32_FLASH_END_ADDRESS)
    {
        os_kprintf("ERROR: write outrange flash size! addr is (0x%p)\n", (void *)(addr + size));
        return OS_INVAL;
    }

    if ((addr % 4) != 0)
    {
        os_kprintf("write addr must be 4-byte alignment\n");
        return OS_INVAL;
    }

    if (size < 1)
    {
        os_kprintf("write size < 1\n");
        return OS_INVAL;
    }

    /* Unlock all EFM registers */
    EFM_Unlock();
    /* Unlock EFM register: FWMC */
    EFM_FWMC_Unlock();
    
    for(i = 0; i < size;)
    {
        if((size - i) < 4)
        {
            for(j = 0; (size - i) > 0; j++,i++)
            {
                tempdata = *p;
                writedata |= tempdata << (8 * j);
                p++;
            }
        }
        else
        {
            for(j = 0; j < 4; j++,i++)
            {
                tempdata = *p;
                writedata |= tempdata << (8 * j);
                p++;
            }
        }
        
        sector_num = addr / HC32_SECTOR_SIZE;
        
        /* Sector disables write protection */
        (void)EFM_SectorCmd_Single(sector_num, Enable);
        
        if(EFM_SingleProgram(addr, writedata) == Ok)
        {
            if(*(uint32_t *)addr != writedata)
            {
                /* Lock EFM register: FWMC */
                EFM_FWMC_Lock();
                /* Lock all EFM registers */
                EFM_Lock();
                /* Sector Enable write protection */
                EFM_SectorCmd_Single(sector_num, Disable);
                os_kprintf("write error!\n");
                return OS_FAILURE;
            }
        }
        else
        {
            /* Lock EFM register: FWMC */
            EFM_FWMC_Lock();
            /* Lock all EFM registers */
            EFM_Lock();
            /* Sector Enable write protection */
            EFM_SectorCmd_Single(sector_num, Disable);
            os_kprintf("write error!\n");
            return OS_FAILURE;
        }
        
        /* Sector Enable write protection */
        EFM_SectorCmd_Single(sector_num, Disable);
        
        tempdata = 0;
        writedata = 0;
        addr += 4; 
    }

    /* Lock EFM register: FWMC */
    EFM_FWMC_Lock();
    /* Lock all EFM registers */
    EFM_Lock();
    
    return size;
}

/**
 * Erase data on flash.
 * @note This operation is irreversible.
 * @note This operation's units is different which on many chips.
 *
 * @param addr flash address
 * @param size erase bytes size
 *
 * @return result
 */
int hc32_flash_erase(uint32_t addr, os_size_t size)
{
    uint16_t sector_start = 0;
    uint16_t sector_end   = 0;
    uint16_t sector_cnt   = 0;
    uint8_t  ret          = 0;

    if ((addr + size) > HC32_FLASH_END_ADDRESS)
    {
        os_kprintf("ERROR: erase outrange flash size! addr is (0x%p)\n", (void*)(addr + size));
        return OS_INVAL;
    }

    sector_start = addr / HC32_SECTOR_SIZE;
    sector_end   = (addr + size) / HC32_SECTOR_SIZE + (((addr + size) % HC32_SECTOR_SIZE) ? 1 : 0);

    /* Unlock all EFM registers */
    EFM_Unlock();
    /* Unlock EFM register: FWMC */
    EFM_FWMC_Unlock();
   
    for (sector_cnt = 0; sector_cnt < (sector_end - sector_start); sector_cnt++)
    {
        /* Sector disables write protection */
        (void)EFM_SectorCmd_Single(sector_start + sector_cnt, Enable);
        
        ret = EFM_SectorErase((sector_start + sector_cnt) * HC32_SECTOR_SIZE);
        if(ret != Ok)
        {
            /* Lock EFM register: FWMC */
            EFM_FWMC_Lock();
            /* Lock all EFM registers */
            EFM_Lock();
            /* Sector Enable write protection */
            EFM_SectorCmd_Single(sector_start + sector_cnt, Disable);
            os_kprintf("erase error!\n");
            return OS_FAILURE;
        }
        
        /* Sector Enable write protection */
        EFM_SectorCmd_Single(sector_start + sector_cnt, Disable);
    }

    /* Lock EFM register: FWMC */
    EFM_FWMC_Lock();
    /* Lock all EFM registers */
    EFM_Lock();

    return size;
}

#if defined(OS_USING_FAL)

static int hc32_flash_init(void)
{
    uint32_t sysclk_freq = 0;
    uint32_t latency     = 0;
    
    stc_clk_freq_t stc_clk_freq;
    stc_efm_cfg_t  stcEfmCfg;

    /* EFM default config. */
    (void)EFM_StructInit(&stcEfmCfg);

    CLK_GetClockFreq(&stc_clk_freq);
    sysclk_freq = stc_clk_freq.sysclkFreq;

    /*
     * Clock <= 40MHZ             EFM_WAIT_CYCLE_0
     * 40MHZ < Clock <= 80MHZ     EFM_WAIT_CYCLE_1
     * 80MHZ < Clock <= 120MHZ    EFM_WAIT_CYCLE_2
     * 120MHZ < Clock <= 160MHZ   EFM_WAIT_CYCLE_3
     * 160MHZ < Clock <= 200MHZ   EFM_WAIT_CYCLE_4
     * 200MHZ < Clock <= 240MHZ   EFM_WAIT_CYCLE_5
     */

    if (sysclk_freq <= 40000000)
    {
        latency = EFM_WAIT_CYCLE_0;
    }
    else if ((sysclk_freq > 40000000) && (sysclk_freq <= 80000000))
    {
        latency = EFM_WAIT_CYCLE_1;
    }
    else if ((sysclk_freq > 80000000) && (sysclk_freq <= 120000000))
    {
        latency = EFM_WAIT_CYCLE_2;
    }
    else if ((sysclk_freq > 120000000) && (sysclk_freq <= 160000000))
    {
        latency = EFM_WAIT_CYCLE_3;
    }
    else if ((sysclk_freq > 160000000) && (sysclk_freq <= 200000000))
    {
        latency = EFM_WAIT_CYCLE_4;
    }
    else if ((sysclk_freq > 200000000) && (sysclk_freq <= 240000000))
    {
        latency = EFM_WAIT_CYCLE_5;
    }
    
    /* flash read wait cycle setting */
    stcEfmCfg.u32WaitCycle = latency;

    /* Unlock all EFM registers */
    EFM_Unlock();
    /* Unlock EFM register: FWMC */
    EFM_FWMC_Unlock();
    
    /* EFM config */
    EFM_Init(&stcEfmCfg);
    
    /* Lock EFM register: FWMC */
    EFM_FWMC_Lock();
    /* Lock all EFM registers */
    EFM_Lock();

    if(HC32_FLASH_SIZE == 1024 * 1024)
    {
        /* Wait flash ready. */
        while (Set != EFM_GetFlagStatus(EFM_FSR_RDY0))
        {
            ;
        }
    }
    else if(HC32_FLASH_SIZE == 2048 * 1024)
    {
        /* Wait flash ready. */
        while (Set != EFM_GetFlagStatus(EFM_FSR_RDY0) || Set != EFM_GetFlagStatus(EFM_FLAG_RDY1))
        {
            ;
        }
    }

    return OS_SUCCESS;
}

#include "fal_drv_flash.c"

#endif

