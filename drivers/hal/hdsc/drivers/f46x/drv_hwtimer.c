/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_hwtimer.c
 *
 * @brief       This file implements timer driver for hc32.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <os_memory.h>
#include <timer/timer.h>
#include <drv_hwtimer.h>

static os_list_node_t hc32_timer_list = OS_LIST_INIT(hc32_timer_list);

void Tim0_IRQHandler(void)
{
    struct hc32_timer *timer;

    if (Set == TIMER0_GetFlag(timer->info->unit, timer->info->channel))
    {
        TIMER0_ClearFlag(timer->info->unit, timer->info->channel);

        os_list_for_each_entry(timer, &hc32_timer_list, struct hc32_timer, list)
        {
            if ((timer->info->mode == TIMER_MODE_TIM) && (timer->info->unit == M4_TMR01))
            {
                os_clockevent_isr((os_clockevent_t *)timer);
                return;
            }
        }
    }
}

static uint64_t hc32_timer_read(void *clock)
{
    struct hc32_timer *timer;
    uint64_t           cnt;

    timer = (struct hc32_timer *)clock;

    cnt = TIMER0_GetCntReg(timer->info->unit, timer->info->channel);

    return cnt;
}

#ifdef OS_USING_CLOCKEVENT
en_tim0_clock_div_t get_clk_div(uint32_t prescaler)
{
    /* The available frequency division factors are only 1,2,4,8,16,32,64,128,256,512,1024 */
    if ((1 << Tim0_ClkDiv0) >= prescaler)
    {
        return Tim0_ClkDiv0;
    }
    else if ((1 << Tim0_ClkDiv2) >= prescaler)
    {
        return Tim0_ClkDiv2;
    }

    else if ((1 << Tim0_ClkDiv4) >= prescaler)
    {
        return Tim0_ClkDiv4;
    }

    else if ((1 << Tim0_ClkDiv8) >= prescaler)
    {
        return Tim0_ClkDiv8;
    }

    else if ((1 << Tim0_ClkDiv16) >= prescaler)
    {
        return Tim0_ClkDiv16;
    }

    else if ((1 << Tim0_ClkDiv32) >= prescaler)
    {
        return Tim0_ClkDiv32;
    }

    else if ((1 << Tim0_ClkDiv64) >= prescaler)
    {
        return Tim0_ClkDiv64;
    }

     else if ((1 << Tim0_ClkDiv128) >= prescaler)
    {
        return Tim0_ClkDiv128;
    }

    else if ((1 << Tim0_ClkDiv256) >= prescaler)
    {
        return Tim0_ClkDiv256;
    }

    else if ((1 << Tim0_ClkDiv512) >= prescaler)
    {
        return Tim0_ClkDiv512;
    }
    else
    {
        return Tim0_ClkDiv1024;
    }
}

static void hc32_timer_start(os_clockevent_t *ce, uint32_t prescaler, uint64_t count)
{
    struct hc32_timer      *timer;
    stc_irq_regi_conf_t    irq_cfg;
    stc_tim0_base_init_t   tim0_init;

    OS_ASSERT(ce != OS_NULL);
    OS_ASSERT(prescaler != 0);
    OS_ASSERT(count != 0);

    timer = (struct hc32_timer *)ce;

    MEM_ZERO_STRUCT(irq_cfg);
    MEM_ZERO_STRUCT(tim0_init);

    tim0_init.Tim0_CounterMode     = Tim0_Sync;
    tim0_init.Tim0_SyncClockSource = Tim0_Pclk1;
    tim0_init.Tim0_ClockDivision   = get_clk_div(prescaler);
    tim0_init.Tim0_CmpValue        = count;
    TIMER0_BaseInit(timer->info->unit, timer->info->channel, &tim0_init);

    /* Enable channel A interrupt */
    TIMER0_IntCmd(timer->info->unit,timer->info->channel, Enable);

    /* Register TMR_INI_GCMA Int to Vect.No.002 */
    irq_cfg.enIRQn = timer->info->irq_num;
    /* Select TIMER channalA interrupt as source */
    irq_cfg.enIntSrc = timer->info->irq_src;
    /* Callback function */
    irq_cfg.pfnCallback =&Tim0_IRQHandler;

    /* Registration IRQ */
    enIrqRegistration(&irq_cfg);
    /* Clear Pending */
    NVIC_ClearPendingIRQ(irq_cfg.enIRQn);
    /* Set priority */
    NVIC_SetPriority(irq_cfg.enIRQn, DDL_IRQ_PRIORITY_03);
    /* Enable NVIC */
    NVIC_EnableIRQ(irq_cfg.enIRQn);
	
    /* Enable the specified interrupts of Timer2. */
    TIMER0_IntCmd(timer->info->unit,timer->info->channel, Enable);

    /*start timer0*/
    TIMER0_Cmd(timer->info->unit, timer->info->channel, Enable);

}

static void hc32_timer_stop(os_clockevent_t *ce)
{
    struct hc32_timer *timer;

    OS_ASSERT(ce != OS_NULL);
    timer = (struct hc32_timer *)ce;

    enIrqResign(timer->info->irq_num);
    NVIC_DisableIRQ(timer->info->irq_num);

    /* Disable the specified interrupts of Timer0. */
    TIMER0_IntCmd(timer->info->unit,timer->info->channel, Disable);
    TIMER0_Cmd(timer->info->unit,timer->info->channel, Disable);
}

static const struct os_clockevent_ops hc32_tim_ops = {
    .start = hc32_timer_start,
    .stop  = hc32_timer_stop,
    .read  = hc32_timer_read,
};
#endif

static int hc32_tim_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    struct hc32_timer    *timer;
    stc_tim0_base_init_t  tim0_init;
    stc_clk_freq_t        stc_clk_freq;
    os_ubase_t            level;

    MEM_ZERO_STRUCT(tim0_init);

    timer = os_calloc(1, sizeof(struct hc32_timer));
    OS_ASSERT(timer);

    timer->info = (struct hc32_tim_info *)dev->info;

    /* Get pclk1 */
    CLK_GetClockFreq(&stc_clk_freq);
    timer->freq = stc_clk_freq.pclk1Freq;

    /* Timer0 peripheral enable */
    PWC_Fcg2PeriphClockCmd(timer->info->clk_config, Enable);

    if (timer->info->mode == TIMER_MODE_TIM)
    {
#ifdef OS_USING_CLOCKSOURCE
        if (os_clocksource_best() == OS_NULL)
        {
            tim0_init.Tim0_CounterMode =     Tim0_Sync;
            tim0_init.Tim0_SyncClockSource = Tim0_Pclk1;
            tim0_init.Tim0_ClockDivision =   Tim0_ClkDiv2;
            tim0_init.Tim0_CmpValue =        0xFFFFUL;
            TIMER0_BaseInit(timer->info->unit, timer->info->channel, &tim0_init);

            /*start timer0*/
            TIMER0_Cmd(timer->info->unit, timer->info->channel, Enable);

            timer->clock.cs.rating = 160;
            timer->clock.cs.freq   = timer->freq / (1 << tim0_init.Tim0_ClockDivision); 
            timer->clock.cs.mask   = 0xFFFFUL;
            timer->clock.cs.read   = hc32_timer_read;

            os_clocksource_register(dev->name, &timer->clock.cs);
        }
        else
#endif
        {
#ifdef OS_USING_CLOCKEVENT
            timer->clock.ce.rating         = 320;
            timer->clock.ce.freq           = timer->freq;
            timer->clock.ce.mask           = 0xFFFFUL;

            /* The available frequency division factors are only 1,2,4,8,16,32,64,128,256,512,1024 */
            timer->clock.ce.prescaler_mask = 0xFFFUL;
            timer->clock.ce.prescaler_bits = 16;
            timer->clock.ce.count_mask     = 0xFFFFUL;
            timer->clock.ce.count_bits     = 16;
            timer->clock.ce.feature        = OS_CLOCKEVENT_FEATURE_PERIOD;
            timer->clock.ce.min_nsec       = NSEC_PER_SEC / timer->clock.ce.freq;
            timer->clock.ce.ops            = &hc32_tim_ops;

            os_clockevent_register(dev->name, &timer->clock.ce);
#endif
        }
    }

    os_spin_lock_irqsave(&gs_device_lock, &level);
    os_list_add(&hc32_timer_list, &timer->list);
    os_spin_unlock_irqrestore(&gs_device_lock, level);

    return OS_SUCCESS;
}

OS_DRIVER_INFO hc32_tim_driver = {
    .name  = "TIMER_Type",
    .probe = hc32_tim_probe,
};

OS_DRIVER_DEFINE(hc32_tim_driver, OS_INIT_LEVEL_PRE_DEVICE, OS_INIT_SUBLEVEL_MIDDLE);
