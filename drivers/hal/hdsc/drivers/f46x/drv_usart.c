/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_usart.c
 *
 * @brief       This file implements usart driver for hc32
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <drv_cfg.h>
#include <device.h>
#include <string.h>
#include <os_memory.h>
#include "hc32f46x_gpio.h"
#include "hc32f46x_usart.h"
#include "hc32f46x_pwc.h"
#include "hc32f46x_interrupts.h"
#include "hc32f46x_dmac.h"
#include "hc32f46x_clk.h"
#include "drv_usart.h"
#include "board.h"

#ifdef BSP_USING_UART

struct hc32_usart_info *console_usart = NULL;

static os_list_node_t hc32_uart_list = OS_LIST_INIT(hc32_uart_list);

/* uart driver */
typedef struct hc_uart
{
    struct os_serial_device serial_dev;
    struct hc32_usart_info *info;
    soft_dma_t              sdma;
    uint8_t                *rx_buff;
    uint32_t                rx_index;
    uint32_t                rx_size;
    os_list_node_t          list;
} hc_usart_t;

#ifndef UART1_USING_DMA
static void uart_isr(struct hc_uart *uart)
{
    uart->rx_buff[uart->rx_index++] = USART_RecData(uart->info->idx);

    if (uart->rx_index == (uart->rx_size / 2))
    {
        soft_dma_half_irq(&uart->sdma);
    }

    if (uart->rx_index == uart->rx_size)
    {
        uart->rx_index = 0;
        soft_dma_full_irq(&uart->sdma);
    }
}

static void UsartRxIrqCallback(void)
{
    struct hc_uart *uart;

    os_list_for_each_entry(uart, &hc32_uart_list, struct hc_uart, list)
    {
        if (uart->info->uart_device == 1)
        {
            uart_isr(uart);
            break;
        }
    }
}
#endif

static void UsartErrIrqCallback(void)
{
    struct hc_uart *uart;

    os_list_for_each_entry(uart, &hc32_uart_list, struct hc_uart, list)
    {
        if (uart->info->uart_device == 1)
        {
            if (Set == USART_GetStatus(uart->info->idx, UsartFrameErr))
            {
                os_kprintf("UsartFrameErr!\r\n");
                USART_ClearStatus(uart->info->idx, UsartFrameErr);
            }
            if (Set == USART_GetStatus(uart->info->idx, UsartParityErr))
            {
                os_kprintf("UsartParityErr!\r\n");
                USART_ClearStatus(uart->info->idx, UsartParityErr);
            }
            if (Set == USART_GetStatus(uart->info->idx, UsartOverrunErr))
            {
                os_kprintf("UsartOverrunErr!\r\n");
                USART_ClearStatus(uart->info->idx, UsartOverrunErr);
            }
            break;
        }
    }
}

static void DmaTrnCpltIrqCallback(void)
{
    struct hc_uart *uart;

    os_list_for_each_entry(uart, &hc32_uart_list, struct hc_uart, list)
    {
        if (uart->info->uart_device == 1)
        {
            if(DMA_GetIrqFlag(uart->info->dma_periph, uart->info->dma_ch, TrnCpltIrq) == Set)
            {
                DMA_ClearIrqFlag(uart->info->dma_periph, uart->info->dma_ch, TrnCpltIrq);
                soft_dma_half_irq(&uart->sdma);
            }
            break;
        }
    }
}

/* interrupt rx mode */
static uint32_t hc_sdma_int_get_index(soft_dma_t *dma)
{
    hc_usart_t *uart = os_container_of(dma, hc_usart_t, sdma);

    return uart->rx_index;
}

static os_err_t hc_sdma_int_start(soft_dma_t *dma, void *buff, uint32_t size)
{
    hc_usart_t *uart = os_container_of(dma, hc_usart_t, sdma);

    uart->rx_buff  = buff;
    uart->rx_index = 0;
    uart->rx_size  = size;

    USART_FuncCmd(uart->info->idx, UsartRx, Enable);
    USART_FuncCmd(uart->info->idx, UsartRxInt, Enable);
    
    return OS_SUCCESS;
}

static uint32_t hc_sdma_int_stop(soft_dma_t *dma)
{
    hc_usart_t *uart = os_container_of(dma, hc_usart_t, sdma);

    USART_FuncCmd(uart->info->idx, UsartRxInt, Disable);
    USART_FuncCmd(uart->info->idx, UsartRx, Disable);

    return uart->rx_index;
}

/* dma rx mode */
static uint32_t hc_sdma_dma_get_index(soft_dma_t *dma)
{
    hc_usart_t *uart = os_container_of(dma, hc_usart_t, sdma);
    
    return (uint32_t)READ_DMA_CH_REG(&uart->info->dma_periph->MONDAR0, uart->info->dma_ch) - (uint32_t)uart->rx_buff;
}

static os_err_t hc_sdma_dma_init(soft_dma_t *dma)
{
    return OS_SUCCESS;
}

static os_err_t hc_sdma_dma_start(soft_dma_t *dma, void *buff, uint32_t size)
{ 
    hc_usart_t         *uart;
    stc_dma_config_t    stcDmaInit;
    stc_irq_regi_conf_t stcIrqRegiCfg;

    uart = os_container_of(dma, hc_usart_t, sdma);

    uart->rx_buff = buff;

    /* Enable peripheral clock */
    PWC_Fcg0PeriphClockCmd(uart->info->dma_clk, Enable);

    /* Enable DMA. */
    DMA_Cmd(uart->info->dma_periph, Enable);

    /* Initialize DMA. */
    MEM_ZERO_STRUCT(stcDmaInit);
    stcDmaInit.u16BlockSize           = 1u;                                   
    stcDmaInit.u16TransferCnt         = size / 2;
    stcDmaInit.u32SrcAddr             = ((uint32_t)(&uart->info->idx->DR)+2ul);
    stcDmaInit.u32DesAddr             = (uint32_t)(buff);                   
    stcDmaInit.stcDmaChCfg.enSrcInc   = AddressFix;                             
    stcDmaInit.stcDmaChCfg.enDesInc   = AddressIncrease;
    stcDmaInit.stcDmaChCfg.enIntEn    = Enable;
    stcDmaInit.stcDmaChCfg.enTrnWidth = Dma8Bit;
    DMA_InitChannel(uart->info->dma_periph, uart->info->dma_ch, &stcDmaInit);

    /* Enable the specified DMA channel. */
    DMA_ChannelCmd(uart->info->dma_periph, uart->info->dma_ch, Enable);

    /* Enable peripheral circuit trigger function. */
    PWC_Fcg0PeriphClockCmd(PWC_FCG0_PERIPH_AOS, Enable);

    /* Set DMA trigger source. */
    DMA_SetTriggerSrc(uart->info->dma_periph, uart->info->dma_ch, EVT_USART1_RI);

    /* Set DMA block transfer complete IRQ */
    stcIrqRegiCfg.enIRQn      = uart->info->dma_irqn;
    stcIrqRegiCfg.pfnCallback = &DmaTrnCpltIrqCallback;
    stcIrqRegiCfg.enIntSrc    = uart->info->dma_int_Src;
    enIrqRegistration(&stcIrqRegiCfg);
    NVIC_SetPriority(stcIrqRegiCfg.enIRQn, 0);
    NVIC_ClearPendingIRQ(stcIrqRegiCfg.enIRQn);
    NVIC_EnableIRQ(stcIrqRegiCfg.enIRQn);

    /* Clear DMA flag. */
    DMA_ClearIrqFlag(uart->info->dma_periph, uart->info->dma_ch, TrnCpltIrq);
    /* Enable the DMA transfer completion interrupt */
    DMA_EnableIrq(uart->info->dma_periph, uart->info->dma_ch, TrnCpltIrq);

    USART_FuncCmd(uart->info->idx, UsartRxInt, Enable);
    USART_FuncCmd(uart->info->idx, UsartRx, Enable);

    return OS_SUCCESS;
}

static uint32_t hc_sdma_dma_stop(soft_dma_t *dma)
{
    hc_usart_t *uart;

    uart = os_container_of(dma, hc_usart_t, sdma);

    DMA_Cmd(uart->info->dma_periph, Disable);

    return hc_sdma_dma_get_index(dma);
}

/* sdma callback */
static void hc_usart_sdma_callback(soft_dma_t *dma)
{
    hc_usart_t *uart = os_container_of(dma, hc_usart_t, sdma);

    os_hw_serial_isr_rxdone((struct os_serial_device *)uart);
}

static void hc_usart_sdma_init(struct hc_uart *uart, dma_ring_t *ring)
{
    soft_dma_t *dma = &uart->sdma;

    soft_dma_stop(dma);

    memset(&dma->hard_info, 0, sizeof(dma->hard_info));

    dma->hard_info.mode         = HARD_DMA_MODE_NORMAL;
    dma->hard_info.max_size     = 64 * 1024;
    dma->hard_info.data_timeout = uart_calc_byte_timeout_us(uart->serial_dev.config.baud_rate);

    if (!uart->info->dma_support)
    {
        dma->ops.get_index = hc_sdma_int_get_index;
        dma->ops.dma_init  = OS_NULL;
        dma->ops.dma_start = hc_sdma_int_start;
        dma->ops.dma_stop  = hc_sdma_int_stop;
    }
    else
    {
        dma->hard_info.flag |= HARD_DMA_FLAG_HALF_IRQ;
        dma->ops.get_index   = hc_sdma_dma_get_index;
        dma->ops.dma_init    = hc_sdma_dma_init;
        dma->ops.dma_start   = hc_sdma_dma_start;
        dma->ops.dma_stop    = hc_sdma_dma_stop;
    }

    dma->cbs.dma_half_callback    = hc_usart_sdma_callback;
    dma->cbs.dma_full_callback    = hc_usart_sdma_callback;
    dma->cbs.dma_timeout_callback = hc_usart_sdma_callback;

    soft_dma_init(dma);
    soft_dma_start(dma, ring);
    soft_dma_irq_enable(&uart->sdma, OS_TRUE);
}

static os_err_t hc_uart_init(struct os_serial_device *serial, struct serial_configure *cfg)
{
    struct hc_uart       *uart;
    stc_usart_uart_init_t usart_init_cfg;
    stc_irq_regi_conf_t   irq_register_cfg;

    OS_ASSERT(serial != OS_NULL);
    OS_ASSERT(cfg != OS_NULL);
    
    uart = os_container_of(serial, struct hc_uart, serial_dev);

    MEM_ZERO_STRUCT(usart_init_cfg);

    switch (cfg->stop_bits)
    {
    case STOP_BITS_1:
        usart_init_cfg.enStopBit = UsartOneStopBit;
        break;
    case STOP_BITS_2:
        usart_init_cfg.enStopBit = UsartTwoStopBit;
        break;
    default:
        return OS_INVAL;
    }
    
    switch (cfg->parity)
    {
    case PARITY_NONE:
        usart_init_cfg.enParity = UsartParityNone;
        break;
    case PARITY_ODD:
        usart_init_cfg.enParity = UsartParityOdd;
        break;
    case PARITY_EVEN:
        usart_init_cfg.enParity = UsartParityEven;
        break;
    default:
        return OS_INVAL;
    }

    switch (cfg->data_bits)
    {
    case DATA_BITS_8:
        usart_init_cfg.enDataLength = UsartDataBits8;
        break;
    case DATA_BITS_9:
        usart_init_cfg.enDataLength = UsartDataBits9;
        break;
    default:
        return OS_INVAL;
    }
   
    usart_init_cfg.enClkDiv     = UsartClkDiv_1;
    usart_init_cfg.enClkMode    = UsartIntClkCkNoOutput;
    usart_init_cfg.enDetectMode = UsartStartBitFallEdge;
    usart_init_cfg.enDirection  = UsartDataLsbFirst;
    usart_init_cfg.enHwFlow     = UsartRtsEnable;
    usart_init_cfg.enSampleMode = UsartSamleBit8;

    PWC_Fcg1PeriphClockCmd(uart->info->uart_clk, Enable);

    PORT_Unlock();
    /* Initialize USART IO */
    PORT_SetFunc(uart->info->tx_port, uart->info->tx_pin, uart->info->tx_func, Disable);
    PORT_SetFunc(uart->info->rx_port, uart->info->rx_pin, uart->info->rx_func, Disable);
    PORT_Lock();

    /* Initialize UART */
    if (USART_UART_Init(uart->info->idx, &usart_init_cfg) != Ok)
    {
        os_kprintf("Usart initialization failed!\r\n");
        return OS_FAILURE;
    }

    /* Set baudrate */
    if (USART_SetBaudrate(uart->info->idx, cfg->baud_rate) != Ok)
    {
        os_kprintf("Fail to set usart baud rate!\r\n");
        return OS_FAILURE;
    }

#ifndef UART1_USING_DMA
    /* Register RX NoEmpty interrupt */
    irq_register_cfg.enIRQn      = uart->info->rx_irqn;
    irq_register_cfg.pfnCallback = &UsartRxIrqCallback;
    irq_register_cfg.enIntSrc    = uart->info->rx_int_Src;
    enIrqRegistration(&irq_register_cfg);
    NVIC_SetPriority(irq_register_cfg.enIRQn, 0);
    NVIC_ClearPendingIRQ(irq_register_cfg.enIRQn);
    NVIC_EnableIRQ(irq_register_cfg.enIRQn);
#endif
    
    /* Register RX ERROR interrupt */
    irq_register_cfg.enIRQn      = uart->info->rx_err_irqn;
    irq_register_cfg.pfnCallback = &UsartErrIrqCallback;
    irq_register_cfg.enIntSrc    = uart->info->rx_err_int_Src;
    enIrqRegistration(&irq_register_cfg);
    NVIC_SetPriority(irq_register_cfg.enIRQn, DDL_IRQ_PRIORITY_DEFAULT);
    NVIC_ClearPendingIRQ(irq_register_cfg.enIRQn);
    NVIC_EnableIRQ(irq_register_cfg.enIRQn);

    USART_FuncCmd(uart->info->idx, UsartRxInt, Enable);
    USART_FuncCmd(uart->info->idx, UsartRx, Enable);
    USART_FuncCmd(uart->info->idx, UsartTx, Enable);

    hc_usart_sdma_init(uart, &uart->serial_dev.rx_fifo->ring);

    return OS_SUCCESS;
}

static os_err_t hc_uart_deinit(struct os_serial_device *serial)
{
    OS_ASSERT(serial != OS_NULL);

    hc_usart_t *uart = os_container_of(serial, hc_usart_t, serial_dev);

    if (uart->info->dma_support)
    {
        NVIC_DisableIRQ(uart->info->rx_err_irqn);
        NVIC_DisableIRQ(uart->info->dma_irqn);
        
        DMA_DeInit(uart->info->dma_periph, uart->info->dma_ch);
        USART_DeInit(uart->info->idx);

        soft_dma_deinit(&uart->sdma);
        soft_dma_stop(&uart->sdma);
    }
    else
    {
        NVIC_DisableIRQ(uart->info->rx_irqn);
        NVIC_DisableIRQ(uart->info->rx_err_irqn);

        USART_DeInit(uart->info->idx);

        soft_dma_deinit(&uart->sdma);
        soft_dma_stop(&uart->sdma);
    }

    return OS_SUCCESS;
}

static int hc_uart_poll_send(struct os_serial_device *serial, const uint8_t *buff, os_size_t size)
{
    int             i;
    os_ubase_t      level;
    struct hc_uart *uart;

    OS_ASSERT(serial != OS_NULL);
    uart = os_container_of(serial, struct hc_uart, serial_dev);

    OS_ASSERT(uart != OS_NULL);

    for (i = 0; i < size; i++)
    {
        os_spin_lock_irqsave(&gs_device_lock, &level);

        while (Reset == USART_GetStatus(uart->info->idx, UsartTxEmpty))
        {
        }
        USART_SendData(uart->info->idx, buff[i]);

        os_spin_unlock_irqrestore(&gs_device_lock, level);
    }
    while (Reset == USART_GetStatus(console_usart->idx, UsartTxComplete))
    {
    }

    return size;
}

static const struct os_uart_ops hc_uart_ops = {
    .init   = hc_uart_init,
    .deinit = hc_uart_deinit,

    .poll_send = hc_uart_poll_send,
};

static int hc32_usart_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    os_ubase_t level;

    struct serial_configure config = OS_SERIAL_CONFIG_DEFAULT;

    struct hc_uart *usart;

    usart = os_calloc(1, sizeof(struct hc_uart));
    OS_ASSERT(usart);

    usart->info                     = (struct hc32_usart_info *)dev->info;
    struct os_serial_device *serial = &usart->serial_dev;

    os_spin_lock_irqsave(&gs_device_lock, &level);
    os_list_add_tail(&hc32_uart_list, &usart->list);
    os_spin_unlock_irqrestore(&gs_device_lock, level);

    serial->ops    = &hc_uart_ops;
    serial->config = config;

    /* register uart device */
    os_hw_serial_register(serial, dev->name, usart);

    return 0;
}

OS_DRIVER_INFO hc32_usart_driver = {
    .name  = "Usart_Type",
    .probe = hc32_usart_probe,
};

OS_DRIVER_DEFINE(hc32_usart_driver, OS_INIT_LEVEL_PRE_DEVICE, OS_INIT_SUBLEVEL_HIGH);

/* clang-format off */
void __os_hw_console_output(char *str)
{
    int i;

    if (console_usart == OS_NULL)
    {
        return;
    }
        
    for (i = 0; i < strlen(str); i++)
    {
        while (Reset == USART_GetStatus(console_usart->idx, UsartTxEmpty))
        {
        }
        USART_SendData(console_usart->idx, str[i]);
    }
    while (Reset == USART_GetStatus(console_usart->idx, UsartTxComplete))
    {
    }
}
/* clang-format on */

static int hc32_usart_early_init(struct hc32_usart_info *usart)
{
    stc_usart_uart_init_t usart_init_cfg;

    MEM_ZERO_STRUCT(usart_init_cfg);

    usart_init_cfg.enStopBit    = UsartOneStopBit;
    usart_init_cfg.enParity     = UsartParityNone;
    usart_init_cfg.enDataLength = UsartDataBits8;
    usart_init_cfg.enClkDiv     = UsartClkDiv_1;
    usart_init_cfg.enClkMode    = UsartIntClkCkNoOutput;
    usart_init_cfg.enDetectMode = UsartStartBitFallEdge;
    usart_init_cfg.enDirection  = UsartDataLsbFirst;
    usart_init_cfg.enHwFlow     = UsartRtsEnable;
    usart_init_cfg.enSampleMode = UsartSamleBit8;

    PWC_Fcg1PeriphClockCmd(usart->uart_clk, Enable);

    PORT_Unlock();
    /* Initialize USART IO */
    PORT_SetFunc(usart->tx_port, usart->tx_pin, usart->tx_func, Disable);
    PORT_SetFunc(usart->rx_port, usart->rx_pin, usart->rx_func, Disable);
    PORT_Lock();

    /* Initialize UART */
    if (USART_UART_Init(usart->idx, &usart_init_cfg) != Ok)
    {
        os_kprintf("Usart early initialization failed!\r\n");
        return OS_FAILURE;
    }

    /* Set baudrate */
    if (USART_SetBaudrate(usart->idx, 115200u) != Ok)
    {
        os_kprintf("Usart failed to set baud rate early!\r\n");
        return OS_FAILURE;
    }

    USART_FuncCmd(usart->idx, UsartRx, Enable);
    USART_FuncCmd(usart->idx, UsartTx, Enable);

    return OS_SUCCESS;
}

static int hc32_usart_early_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    if (strcmp(dev->name, OS_CONSOLE_DEVICE_NAME))
    {
        return OS_SUCCESS;
    }
        
    console_usart = (struct hc32_usart_info *)dev->info;;

    hc32_usart_early_init(console_usart);

    return OS_SUCCESS;
}

OS_DRIVER_INFO hc32_usart_early_driver = {
    .name  = "Usart_Type",
    .probe = hc32_usart_early_probe,
};

OS_DRIVER_DEFINE(hc32_usart_early_driver, OS_INIT_LEVEL_PRE_KERNEL_1, OS_INIT_SUBLEVEL_LOW);
#endif

