/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_hwtimer.h
 *
 * @brief       This file provides functions declaration for hc32 timer driver.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __DRV_HWTIMER_H__
#define __DRV_HWTIMER_H__

#include <board.h>
#include <os_task.h>
#include <timer/clocksource.h>
#include <timer/clockevent.h>
#include <hc32f46x_timer0.h>  
#include <hc32f46x_interrupts.h>
#include <hc32f46x_pwc.h>
#include <hc32f46x_clk.h>

#define TIMER_MODE_TIM 0x00

struct hc32_tim_info
{
    M4_TMR0_TypeDef   *unit;
    uint32_t          clk_config;
    en_tim0_channel_t channel;
    IRQn_Type         irq_num;
    en_int_src_t      irq_src;
    uint8_t           mode;
};

struct hc32_timer
{
    union _clock
    {
        os_clocksource_t cs;
        os_clockevent_t  ce;
    } clock;

    struct hc32_tim_info *info;
    uint32_t              freq;
    os_list_node_t        list;

    void *user_data;
};

#endif
