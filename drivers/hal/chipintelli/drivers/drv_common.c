/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_common.c
 *
 * @brief       This file provides systick time init/IRQ and board init functions.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include <stdint.h>
#include <oneos_config.h>
#include <riscv_encoding.h>
#include <os_memory.h>
#include <os_clock.h>
#include <board.h>
#include "drv_common.h"
#include "platform_config.h"
#include "ci112x_core_eclic.h"
#include "ci112x_core_timer.h"

#include "os_errno.h"
#include "os_util.h"

#ifdef OS_USING_CLOCKSOURCE
#include <timer/clocksource.h>
#endif

#include <timer/hrtimer.h>

static volatile os_bool_t hardware_init_done = OS_FALSE;

static uint32_t s_ticks = 0;

static void ostick_config(uint32_t ticks)
{
    volatile uint64_t *mtime    = (uint64_t *)(TIMER_CTRL_ADDR + TIMER_MTIME);
    volatile uint64_t *mtimecmp = (uint64_t *)(TIMER_CTRL_ADDR + TIMER_MTIMECMP);

    s_ticks = ticks;

    eclic_irq_set_priority(MTIP_IRQ, 0x0, 16);
    eclic_global_interrupt_disable();

    clear_timer_value();
    uint64_t now = *mtime;
    now += ticks;
    *mtimecmp = now;
    clear_timer_stop();

    eclic_global_interrupt_enable();
    eclic_irq_enable(MTIP_IRQ);
}

void eclic_mtip_handler(void)
{
    volatile uint64_t *mtime    = (uint64_t *)(TIMER_CTRL_ADDR + TIMER_MTIME);
    volatile uint64_t *mtimecmp = (uint64_t *)(TIMER_CTRL_ADDR + TIMER_MTIMECMP);

    uint64_t now = *mtime;
    now += s_ticks;
    *mtimecmp = now;
    os_tick_increase();
}

int _printf(char *format, ...)
{
    os_kprintf(format);
    return 0;
}

static void vPortSetupMSIP(void)
{
    eclic_set_vmode(MSIP_IRQn);
    eclic_irq_set_priority(MSIP_IRQn, 0, 1);
    eclic_irq_enable(MSIP_IRQn);
}

os_err_t board_post_init(void)
{

#ifdef OS_USING_PIN
    os_hw_pin_init();
#endif
    vPortSetupMSIP();
    ostick_config(TIMER_FREQ / OS_TICK_PER_SECOND);
    return OS_SUCCESS;
}
OS_INIT_CALL(board_post_init, OS_INIT_LEVEL_POST_KERNEL, OS_INIT_SUBLEVEL_LOW);

extern void     enable_mcycle_minstret(void);
static os_err_t os_hw_board_init(void)
{
    hardware_init_done = OS_FALSE;

    extern void SystemInit(void);
    SystemInit();
    eclic_priority_group_set(ECLIC_PRIGROUP_LEVEL0_PRIO3);

    eclic_global_interrupt_enable();

    enable_mcycle_minstret();

    init_platform();

#if defined(OS_USING_HEAP)
    os_default_heap_init();
    os_default_heap_add((void *)HEAP_BEGIN, (os_size_t)HEAP_END - (os_size_t)HEAP_BEGIN, OS_MEM_ALG_DEFAULT);
#endif
    return OS_SUCCESS;
}
OS_INIT_CALL(os_hw_board_init, OS_INIT_LEVEL_PRE_KERNEL_1, OS_INIT_SUBLEVEL_HIGH);
