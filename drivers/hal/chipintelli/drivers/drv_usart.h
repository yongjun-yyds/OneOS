/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_usart.h
 *
 * @brief        This file provides functions declaration for usart driver.
 *
 * @revision
 * Date         Author          Notes
 * 2022-01-10   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#ifndef __DRV_USART_H__
#define __DRV_USART_H__
#include <os_types.h>

#include <ci112x_uart.h>
#include <ci112x_dma.h>
#include <oneos_config.h>

typedef struct ci_usart_info
{
    PinPad_Name      tx_pinpad;
    IOResue_FUNCTION tx_resue;

    PinPad_Name      rx_pinpad;
    IOResue_FUNCTION rx_resue;

    UART_TypeDef *husart;
    uint32_t      usart_irqn;

    os_bool_t dma_enable;

    DMACChannelx     tx_channel;
    DMAC_Peripherals tx_dma;

    DMACChannelx     rx_channel;
    DMAC_Peripherals rx_dma;

    uint32_t dma_uart_addr;
} ci_usart_info_t;

typedef struct ci_usart
{
    struct os_serial_device serial;
    struct ci_usart_info   *info;

    soft_dma_t  sdma;
    uint32_t sdma_hard_size;

    uint8_t *rx_buff;
    uint32_t rx_index;
    uint32_t rx_size;

    const uint8_t *tx_buff;
    uint32_t       tx_count;
    uint32_t       tx_size;

    os_list_node_t list;
} ci_usart_t;

#endif /* __DRV_USART_H__ */
