/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_gpio.c
 *
 * @brief       This file implements gpio driver for CH32.
 *
 * @revision
 * Date         Author          Notes
 * 2022-01-17   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <board.h>
#ifdef OS_USING_PIN

#define DRV_EXT_TAG "drv.gpio"
#include <dlog.h>

#include <driver.h>
#include "drv_gpio.h"
#include <os_types.h>
#include <os_stddef.h>

#define ITEM_NUM(items) sizeof(items) / sizeof(items[0])
gpio_base_t gpio_port_base[GPIO_PORT_MAX];

static struct pin_index indexs[GPIO_PIN_MAX] = {
    /*port     pinpull  pinpad       io_func */
    {GPIO0, {0}, 21, FIRST_FUNCTION},     // GPIO0[0]
    {GPIO0, {0}, 22, FIRST_FUNCTION},     // GPIO0[1]
    {GPIO0, {0}, 23, FIRST_FUNCTION},     // GPIO0[2]
    {GPIO0, {0}, 24, FIRST_FUNCTION},     // GPIO0[3]
    {GPIO0, {0}, 27, FIRST_FUNCTION},     // GPIO0[4]
    {GPIO0, {0}, 13, SECOND_FUNCTION},    // GPIO0[5]
    {GPIO0, {0}, 11, SECOND_FUNCTION},    // GPIO0[6]
    {GPIO0, {0}, 25, SECOND_FUNCTION},    // GPIO0[7]
    {GPIO1, {0}, 26, SECOND_FUNCTION},    // GPIO1[0]
    {GPIO1, {0}, 15, FIRST_FUNCTION},     // GPIO1[1]
    {GPIO1, {0}, 16, FIRST_FUNCTION},     // GPIO1[2]
    {GPIO1, {0}, 17, FIRST_FUNCTION},     // GPIO1[3]
    {GPIO1, {0}, 18, FIRST_FUNCTION},     // GPIO1[4]
    {GPIO1, {0}, 19, FIRST_FUNCTION},     // GPIO1[5]
    {GPIO1, {0}, 21, FIRST_FUNCTION},     // GPIO1[6]
    {GPIO1, {0}, 1, FIRST_FUNCTION},      // GPIO1[7]
    {GPIO2, {0}, 0, FIRST_FUNCTION},      // GPIO2[0]
    {GPIO2, {0}, 2, FIRST_FUNCTION},      // GPIO2[1]
    {GPIO2, {0}, 3, FIRST_FUNCTION},      // GPIO2[2]
    {GPIO2, {0}, 10, FIRST_FUNCTION},     // GPIO2[3]
    {GPIO2, {0}, 9, FIRST_FUNCTION},      // GPIO2[4]
    {GPIO2, {0}, 8, FIRST_FUNCTION},      // GPIO2[5]
    {GPIO2, {0}, 7, FIRST_FUNCTION},      // GPIO2[6]
    {GPIO2, {0}, 6, FIRST_FUNCTION},      // GPIO2[7]
    {GPIO3, {0}, 4, FIRST_FUNCTION},      // GPIO3[0]
    {GPIO3, {0}, 5, FIRST_FUNCTION},      // GPIO3[1]
    {GPIO3, {0}, 28, FIRST_FUNCTION},     // GPIO3[2]
};

struct pin_irq_map
{
    gpio_base_t port;
    IRQn_Type   irqno;
};

static const struct pin_irq_map pin_irq_map[] = {
    {GPIO0, GPIO0_IRQn},
    {GPIO1, GPIO1_IRQn},
    {GPIO2, GPIO2_IRQn},
    {GPIO3, GPIO3_IRQn},
};

static struct os_pin_irq_hdr pin_irq_hdr_tab[] = {
    {-1, 0, OS_NULL, OS_NULL}, {-1, 0, OS_NULL, OS_NULL}, {-1, 0, OS_NULL, OS_NULL}, {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL}, {-1, 0, OS_NULL, OS_NULL}, {-1, 0, OS_NULL, OS_NULL}, {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL}, {-1, 0, OS_NULL, OS_NULL}, {-1, 0, OS_NULL, OS_NULL}, {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL}, {-1, 0, OS_NULL, OS_NULL}, {-1, 0, OS_NULL, OS_NULL}, {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL}, {-1, 0, OS_NULL, OS_NULL}, {-1, 0, OS_NULL, OS_NULL}, {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL}, {-1, 0, OS_NULL, OS_NULL}, {-1, 0, OS_NULL, OS_NULL}, {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL}, {-1, 0, OS_NULL, OS_NULL}, {-1, 0, OS_NULL, OS_NULL}, {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
};

static struct pin_index *get_pin_index(os_base_t pin)
{
    struct pin_index *index = OS_NULL;

    if (pin < GPIO_PIN_MAX)
    {
        index = (struct pin_index *)&indexs[pin];
    }

    return index;
}

gpio_base_t get_pin_base(os_base_t pin)
{
    struct pin_index *index = get_pin_index(pin);

    if (index != OS_NULL)
    {
        return index->gpio_port;
    }
    return (gpio_base_t)OS_NULL;
}

static void _pin_write(struct os_device *dev, os_base_t pin, os_base_t value)
{
    const struct pin_index *index;

    index = get_pin_index(pin);
    if (index == OS_NULL)
    {
        return;
    }

    gpio_set_output_level_single(index->gpio_port, PIN_OFFSET(pin), value);
}

static int _pin_read(struct os_device *dev, os_base_t pin)
{
    int                     value;
    const struct pin_index *index;

    value = PIN_LOW;

    index = get_pin_index(pin);
    if (index == OS_NULL)
    {
        return value;
    }
    value = (int)gpio_get_input_level_single(index->gpio_port, PIN_OFFSET(pin));

    return value;
}

static void _pin_mode(struct os_device *dev, os_base_t pin, os_base_t mode)
{
    os_ubase_t         level;
    struct pin_index *index;
    gpio_base_t       gpio_base;
    uint8_t        pull_state;

    index = get_pin_index(pin);
    if (index == OS_NULL)
    {
        return;
    }

    if (mode == PIN_MODE_OUTPUT)
    {
        /* output setting */
        Scu_SetIOReuse(index->pinpad, index->io_function);
        Scu_SetIOPull(index->pinpad, DISABLE);
        gpio_set_output_mode(index->gpio_port, PIN_OFFSET(pin));
        pull_state = 0;
    }
    else if (mode == PIN_MODE_INPUT)
    {
        /* input setting: not pull. */
        Scu_SetIOReuse(index->pinpad, index->io_function);
        Scu_SetIOPull(index->pinpad, ENABLE);
        gpio_set_input_mode(index->gpio_port, PIN_OFFSET(pin));
        pull_state = 0;
    }
    else if (mode == PIN_MODE_INPUT_PULLUP)
    {
        /* input setting: pull up. */
        Scu_SetIOReuse(index->pinpad, index->io_function);
        Scu_SetIOPull(index->pinpad, ENABLE);
        gpio_set_input_mode(index->gpio_port, PIN_OFFSET(pin));
        pull_state = 1;
    }
    else if (mode == PIN_MODE_DISABLE)
    {
        return;
    }

    /* remeber the pull state. */
    os_spin_lock_irqsave(&gs_device_lock, &level);
    index->pin_pulls.pull_state = pull_state;
    os_spin_unlock_irqrestore(&gs_device_lock, level);
}

OS_INLINE int32_t bit2bitno(uint32_t bit)
{
    int i;
    for (i = 0; i < 32; i++)
    {
        if ((0x01 << i) == bit)
        {
            return i;
        }
    }
    return -1;
}

static os_err_t
_pin_attach_irq(struct os_device *device, int32_t pin, uint32_t mode, void (*hdr)(void *args), void *args)
{
    os_ubase_t level;

    os_spin_lock_irqsave(&gs_device_lock, &level);
    if (pin_irq_hdr_tab[pin].pin == pin && pin_irq_hdr_tab[pin].hdr == hdr && pin_irq_hdr_tab[pin].mode == mode &&
        pin_irq_hdr_tab[pin].args == args)
    {
        os_spin_unlock_irqrestore(&gs_device_lock, level);
        return OS_SUCCESS;
    }
    if (pin_irq_hdr_tab[pin].pin != -1)
    {
        os_spin_unlock_irqrestore(&gs_device_lock, level);
        return OS_BUSY;
    }
    pin_irq_hdr_tab[pin].pin  = pin;
    pin_irq_hdr_tab[pin].hdr  = hdr;
    pin_irq_hdr_tab[pin].mode = mode;
    pin_irq_hdr_tab[pin].args = args;
    os_spin_unlock_irqrestore(&gs_device_lock, level);

    return OS_SUCCESS;
}

static os_err_t _pin_detach_irq(struct os_device *device, int32_t pin)
{
    const struct pin_index *index;
    os_ubase_t               level;
    int32_t              irqindex = -1;
    irqindex                         = pin;
    os_spin_lock_irqsave(&gs_device_lock, &level);
    if (pin_irq_hdr_tab[irqindex].pin == -1)
    {
        os_spin_unlock_irqrestore(&gs_device_lock, level);
        return OS_SUCCESS;
    }
    pin_irq_hdr_tab[irqindex].pin  = -1;
    pin_irq_hdr_tab[irqindex].hdr  = OS_NULL;
    pin_irq_hdr_tab[irqindex].mode = 0;
    pin_irq_hdr_tab[irqindex].args = OS_NULL;

    os_spin_unlock_irqrestore(&gs_device_lock, level);

    return OS_SUCCESS;
}

static os_err_t _pin_irq_enable(struct os_device *device, os_base_t pin, uint32_t enabled)
{
    os_ubase_t                 level;
    const struct pin_irq_map *irqmap   = OS_NULL;
    uint32_t               irqindex = __PORT_INDEX(pin);
    uint32_t               pinindex = __PIN_INDEX(pin);
    gpio_trigger_t            mode;

    if (irqindex < 0 || irqindex >= ITEM_NUM(pin_irq_map))
    {
        return OS_NOSYS;
    }

    irqmap = &pin_irq_map[irqindex];
    os_spin_lock_irqsave(&gs_device_lock, &level);

    if (enabled == PIN_IRQ_ENABLE)
    {
        if (pin_irq_hdr_tab[pinindex].pin == -1)
        {
            os_spin_unlock_irqrestore(&gs_device_lock, level);
            return OS_NOSYS;
        }

        switch (pin_irq_hdr_tab[pinindex].mode)
        {
        case PIN_IRQ_MODE_RISING:
            mode = up_edges_trigger;
            break;
        case PIN_IRQ_MODE_FALLING:
            mode = down_edges_trigger;
            break;
        case PIN_IRQ_MODE_RISING_FALLING:
            mode = both_edges_trigger;
            break;
        case PIN_IRQ_MODE_HIGH_LEVEL:
            mode = high_level_trigger;
            break;
        case PIN_IRQ_MODE_LOW_LEVEL:
            mode = low_level_trigger;
            break;
        default:
            LOG_E(DRV_EXT_TAG, "not support pin mode!");
            return OS_FAILURE;
        }
        gpio_irq_trigger_config(pin_irq_map[irqindex].port, PIN_OFFSET(pin), mode);
        gpio_clear_irq_single(pin_irq_map[irqindex].port, PIN_OFFSET(pin));
        eclic_irq_enable(pin_irq_map[irqindex].irqno);
        os_spin_unlock_irqrestore(&gs_device_lock, level);
    }
    else if (enabled == PIN_IRQ_DISABLE)
    {
        irqmap = &pin_irq_map[irqindex];
        if (irqmap == OS_NULL)
        {
            return OS_NOSYS;
        }

        gpio_irq_mask(pin_irq_map[irqindex].port, PIN_OFFSET(pin));

        os_spin_unlock_irqrestore(&gs_device_lock, level);
    }
    return OS_SUCCESS;
}

#define GPIO_IRQ_CALLBACK_FUNC(pin)                                                                                    \
    void gpio_##pin##_irq_callback(void)                                                                               \
    {                                                                                                                  \
        if (gpio_get_irq_mask_status_single(indexs[pin].gpio_port, PIN_OFFSET(pin)) != RESET)                          \
        {                                                                                                              \
            if (pin_irq_hdr_tab[pin].hdr != OS_NULL)                                                                   \
                pin_irq_hdr_tab[pin].hdr(pin_irq_hdr_tab[pin].args);                                                   \
            gpio_clear_irq_single(indexs[pin].gpio_port, PIN_OFFSET(pin));                                             \
        }                                                                                                              \
        return;                                                                                                        \
    }

// gpio_irq_callback function
GPIO_IRQ_CALLBACK_FUNC(0);
GPIO_IRQ_CALLBACK_FUNC(1);
GPIO_IRQ_CALLBACK_FUNC(2);
GPIO_IRQ_CALLBACK_FUNC(3);
GPIO_IRQ_CALLBACK_FUNC(4);
GPIO_IRQ_CALLBACK_FUNC(5);
GPIO_IRQ_CALLBACK_FUNC(6);
GPIO_IRQ_CALLBACK_FUNC(7);
GPIO_IRQ_CALLBACK_FUNC(8);
GPIO_IRQ_CALLBACK_FUNC(9);
GPIO_IRQ_CALLBACK_FUNC(10);
GPIO_IRQ_CALLBACK_FUNC(11);
GPIO_IRQ_CALLBACK_FUNC(12);
GPIO_IRQ_CALLBACK_FUNC(13);
GPIO_IRQ_CALLBACK_FUNC(14);
GPIO_IRQ_CALLBACK_FUNC(15);
GPIO_IRQ_CALLBACK_FUNC(16);
GPIO_IRQ_CALLBACK_FUNC(17);
GPIO_IRQ_CALLBACK_FUNC(18);
GPIO_IRQ_CALLBACK_FUNC(19);
GPIO_IRQ_CALLBACK_FUNC(20);
GPIO_IRQ_CALLBACK_FUNC(21);
GPIO_IRQ_CALLBACK_FUNC(22);
GPIO_IRQ_CALLBACK_FUNC(23);
GPIO_IRQ_CALLBACK_FUNC(24);
GPIO_IRQ_CALLBACK_FUNC(25);
GPIO_IRQ_CALLBACK_FUNC(26);
GPIO_IRQ_CALLBACK_FUNC(27);
GPIO_IRQ_CALLBACK_FUNC(28);

#define GPIO_IRQ_CALLBACK(pin) gpio_##pin##_irq_callback

gpio_irq_callback_list_t irq_callbackfunc_node[GPIO_PIN_MAX] = {
    {GPIO_IRQ_CALLBACK(0), NULL},  {GPIO_IRQ_CALLBACK(1), NULL},  {GPIO_IRQ_CALLBACK(2), NULL},
    {GPIO_IRQ_CALLBACK(3), NULL},  {GPIO_IRQ_CALLBACK(4), NULL},  {GPIO_IRQ_CALLBACK(5), NULL},
    {GPIO_IRQ_CALLBACK(6), NULL},  {GPIO_IRQ_CALLBACK(7), NULL},  {GPIO_IRQ_CALLBACK(8), NULL},
    {GPIO_IRQ_CALLBACK(9), NULL},  {GPIO_IRQ_CALLBACK(10), NULL}, {GPIO_IRQ_CALLBACK(11), NULL},
    {GPIO_IRQ_CALLBACK(12), NULL}, {GPIO_IRQ_CALLBACK(13), NULL}, {GPIO_IRQ_CALLBACK(14), NULL},
    {GPIO_IRQ_CALLBACK(15), NULL}, {GPIO_IRQ_CALLBACK(16), NULL}, {GPIO_IRQ_CALLBACK(17), NULL},
    {GPIO_IRQ_CALLBACK(18), NULL}, {GPIO_IRQ_CALLBACK(19), NULL}, {GPIO_IRQ_CALLBACK(20), NULL},
    {GPIO_IRQ_CALLBACK(21), NULL}, {GPIO_IRQ_CALLBACK(22), NULL}, {GPIO_IRQ_CALLBACK(23), NULL},
    {GPIO_IRQ_CALLBACK(24), NULL}, {GPIO_IRQ_CALLBACK(25), NULL}, {GPIO_IRQ_CALLBACK(26), NULL},
    {GPIO_IRQ_CALLBACK(27), NULL}, {GPIO_IRQ_CALLBACK(28), NULL},
};

void __os_hw_pin_init(void)
{
    int i;

#if defined(HAL_GPIO0_BASE)
    Scu_SetDeviceGate((unsigned int)GPIO0, ENABLE);
#endif

#if defined(HAL_GPIO1_BASE)
    Scu_SetDeviceGate((unsigned int)GPIO1, ENABLE);
#endif

#if defined(HAL_GPIO2_BASE)
    Scu_SetDeviceGate((unsigned int)GPIO2, ENABLE);
#endif

#if defined(HAL_GPIO3_BASE)
    Scu_SetDeviceGate((unsigned int)GPIO3, ENABLE);
#endif

#if defined(HAL_GPIO4_BASE)
    Scu_SetDeviceGate((unsigned int)GPIO4, ENABLE);
#endif

    for (i = 0; i < GPIO_PIN_MAX; i++)
    {
        registe_gpio_callback(indexs[i].gpio_port, &irq_callbackfunc_node[i]);
    }
}

const static struct os_pin_ops ci_pin_ops = {
    .pin_mode       = _pin_mode,
    .pin_write      = _pin_write,
    .pin_read       = _pin_read,
    .pin_attach_irq = _pin_attach_irq,
    .pin_detach_irq = _pin_detach_irq,
    .pin_irq_enable = _pin_irq_enable,
};

int os_hw_pin_init(void)
{
    __os_hw_pin_init();

    return os_device_pin_register(0, &ci_pin_ops, OS_NULL);
}

#endif
