/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_hwtimer.c
 *
 * @brief       This file implements timer driver for stm32.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <board.h>
#include <os_memory.h>
#include <timer/timer.h>

#ifdef OS_USING_CLOCKSOURCE
#include <timer/clocksource.h>
#endif

#ifdef OS_USING_CLOCKEVENT
#include <timer/clockevent.h>
#endif

#include <drv_hwtimer.h>

#ifdef OS_USING_PWM
#include "drv_pwm.h"
#endif

#ifdef OS_USING_PULSE_ENCODER
#include "drv_pulse_encoder.h"
#endif

#define DRV_EXT_LVL DBG_EXT_INFO
#define DRV_EXT_TAG "drv.hwtimer"
#include <drv_log.h>

static os_list_node_t ci_timer_list = OS_LIST_INIT(ci_timer_list);

os_bool_t _timer_is_32b(timer_base_t base)
{
#ifdef IS_TIM_32B_COUNTER_INSTANCE
    return IS_TIM_32B_COUNTER_INSTANCE(base);
#else
    return OS_TRUE;
#endif
}

static uint32_t _timer_get_freq(timer_base_t base)
{
    return get_apb_clk();
}

static uint8_t _timer_mode_judge(timer_base_t base)
{
    return TIMER_MODE_TIM;
}

void _TIM_PeriodElapsedCallback(timer_base_t base)
{
    struct ci_timer *timer;

    os_list_for_each_entry(timer, &ci_timer_list, struct ci_timer, list)
    {
        if (timer->handle == base)
        {
#ifdef OS_USING_CLOCKEVENT
            if (timer->mode == TIMER_MODE_TIM)
            {
                timer_clear_irq(base);
                os_clockevent_isr((os_clockevent_t *)timer);
                return;
            }
#endif
            return;
        }
    }
}

#if 1
void TIMER0_IRQHandler(void)
{
    _TIM_PeriodElapsedCallback(TIMER0);
}

void TIMER1_IRQHandler(void)
{
    _TIM_PeriodElapsedCallback(TIMER1);
}

void TIMER2_IRQHandler(void)
{
    _TIM_PeriodElapsedCallback(TIMER2);
}

void TIMER3_IRQHandler(void)
{
    _TIM_PeriodElapsedCallback(TIMER3);
}
#endif
#if defined(OS_USING_CLOCKSOURCE) || defined(OS_USING_CLOCKEVENT)
static uint64_t _timer_read(void *clock)
{
    struct ci_timer *timer;
    uint64_t      count = 0;

    timer = (struct ci_timer *)clock;
    timer_get_count(timer->handle, (unsigned int*)&count);
    return ((0xffffffffull) - count);
}

#endif

#ifdef OS_USING_CLOCKEVENT
static void _timer_start(os_clockevent_t *ce, uint32_t prescaler, uint64_t count)
{
    struct ci_timer *timer;
    timer_base_t     base;
    timer_init_t     init;

    OS_ASSERT(prescaler != 0);
    OS_ASSERT(count != 0);

    timer = (struct ci_timer *)ce;
    base  = timer->handle;
    Scu_SetDeviceGate((unsigned int)base, ENABLE);
    init.mode = timer_count_mode_auto;
    if (prescaler <= 1)
        init.div = timer_clk_div_0;
    else if ((prescaler > 1) && (prescaler < 4))
        init.div = timer_clk_div_2;
    else if ((prescaler > 4) && (prescaler < 16))
        init.div = timer_clk_div_4;
    else if (prescaler >= 16)
        init.div = timer_clk_div_16;

    init.width = timer_iqr_width_2;
    init.count = count;

    switch (base)
    {
    case TIMER0:
        eclic_irq_enable(TIMER0_IRQn);
        break;
    case TIMER1:
        eclic_irq_enable(TIMER1_IRQn);
        break;
    case TIMER2:
        eclic_irq_enable(TIMER2_IRQn);
        break;
    case TIMER3:
        eclic_irq_enable(TIMER3_IRQn);
        break;
    }
    timer_init(base, init);
    timer_clear_irq(base);
    timer_start(base);
}

static void _timer_stop(os_clockevent_t *ce)
{
    struct ci_timer *timer;

    OS_ASSERT(ce != OS_NULL);

    timer = (struct ci_timer *)ce;
    switch (timer->handle)
    {
    case TIMER0:
        eclic_irq_disable(TIMER0_IRQn);
        break;
    case TIMER1:
        eclic_irq_disable(TIMER1_IRQn);
        break;
    case TIMER2:
        eclic_irq_disable(TIMER2_IRQn);
        break;
    case TIMER3:
        eclic_irq_disable(TIMER3_IRQn);
        break;
    }
    timer_stop(timer->handle);
}

static const struct os_clockevent_ops ci_tim_ops = {
    .start = _timer_start,
    .stop  = _timer_stop,
    .read  = _timer_read,
};
#endif

/**
 ***********************************************************************************************************************
 * @brief           tim_probe:probe timer device.
 *
 * @param[in]       none
 *
 * @return          Return timer probe status.
 * @retval          OS_SUCCESS         timer register success.
 * @retval          OS_FAILURE       timer register failed.
 ***********************************************************************************************************************
 */
static int ci_tim_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    struct ci_timer *timer;
    struct ci_timer *tim;
    timer_init_t     init;

    timer = os_calloc(1, sizeof(struct ci_timer));
    OS_ASSERT(timer);

    tim = (struct ci_timer *)(dev->info);

    timer->handle = tim->handle;

    timer->freq = _timer_get_freq(timer->handle);

    timer->mode = tim->mode;
    if (timer->mode == TIMER_MODE_TIM)
    {
#ifdef OS_USING_CLOCKSOURCE
        if (os_clocksource_best() == OS_NULL && _timer_is_32b(timer->handle))
        {
            init.mode  = timer_count_mode_auto;
            init.div   = timer_clk_div_0;
            init.width = timer_iqr_width_f;
            init.count = 0xfffffffful;
            Scu_SetDeviceGate((unsigned int)timer->handle, ENABLE);
            timer_init(timer->handle, init);
            timer_start(timer->handle);

            timer->clock.cs.rating = 320;
            timer->clock.cs.freq   = timer->freq;
            timer->clock.cs.mask   = 0xffffffffull;
            timer->clock.cs.read   = _timer_read;

            os_clocksource_register(dev->name, &timer->clock.cs);
        }
        else
#endif
        {
#ifdef OS_USING_CLOCKEVENT
            timer->clock.ce.rating = _timer_is_32b(timer->handle) ? 320 : 160;
            timer->clock.ce.freq   = timer->freq;
            timer->clock.ce.mask   = 0xffffffffull;

            timer->clock.ce.prescaler_mask = 0xff;
            timer->clock.ce.prescaler_bits = 8;

            timer->clock.ce.count_mask = _timer_is_32b(timer->handle) ? 0xfffffffful : 0xfffful;
            timer->clock.ce.count_bits = _timer_is_32b(timer->handle) ? 32 : 16;

            timer->clock.ce.feature = OS_CLOCKEVENT_FEATURE_PERIOD;

            timer->clock.ce.min_nsec = NSEC_PER_SEC / timer->clock.ce.freq;

            timer->clock.ce.ops = &ci_tim_ops;
            os_clockevent_register(dev->name, &timer->clock.ce);
#endif
        }
    }

    os_list_add(&ci_timer_list, &timer->list);

    return OS_SUCCESS;
}

OS_DRIVER_INFO ci_tim_driver = {
    .name  = "timer_base_t",
    .probe = ci_tim_probe,
};

OS_DRIVER_DEFINE(ci_tim_driver, OS_INIT_LEVEL_PRE_DEVICE, OS_INIT_SUBLEVEL_MIDDLE);

