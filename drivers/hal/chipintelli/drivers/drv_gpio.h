/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_gpio.h
 *
 * @brief       This file provides struct/macro declaration and functions declaration for CH32 gpio driver.
 *
 * @revision
 * Date         Author          Notes
 * 2022-01-10   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __DRV_GPIO_H__
#define __DRV_GPIO_H__

#include <ci112x_gpio.h>
#include <ci112x_scu.h>
#include <os_types.h>

#define GPIO_PIN_PER_PORT (8)
#define GPIO_PORT_MAX     (5)
#define GPIO_PIN_MAX      (29)

#define __PORT_INDEX(pin) (pin / GPIO_PIN_PER_PORT)

#define __PIN_INDEX(pin) (pin)

#define PIN_BASE(__pin)   get_pin_base(__pin)
#define PIN_OFFSET(__pin) (1 << ((__pin) % GPIO_PIN_PER_PORT))

#define __CI_PORT(port)     GPIO##port
#define GET_PIN(PORTx, PIN) (os_base_t)((8 * (((os_base_t)__CI_PORT(PORTx) - (os_base_t)GPIO0) / (0x1000UL))) + PIN)

enum GPIO_PORT_INDEX
{
    GPIO_INDEX_0 = 0,
    GPIO_INDEX_1,
    GPIO_INDEX_2,
    GPIO_INDEX_3,
    GPIO_INDEX_4,
    GPIO_INDEX_MAX,
};

struct pin_mode
{
    uint8_t pin_modes[GPIO_PIN_PER_PORT];
};

struct pin_pull_state
{
    int8_t pull_state;
};

#if 0
struct pin_index
{
    gpio_base_t gpio_port;
    struct pin_pull_state pin_pulls[GPIO_PIN_PER_PORT];
};
#endif

typedef struct gpio_pin_info
{
    PinPad_Name      pinpad;
    IOResue_FUNCTION io_function;
} gpio_pin_info_t;

struct pin_index
{
    gpio_base_t           gpio_port;
    struct pin_pull_state pin_pulls;
    PinPad_Name           pinpad;
    IOResue_FUNCTION      io_function;
};

// void GPIO_EXTI_IRQHandler(EINT_LINE_T line, uint16_t index);
int os_hw_pin_init(void);

#endif /* __DRV_GPIO_H__ */
