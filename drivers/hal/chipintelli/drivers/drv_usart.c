/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_usart.c
 *
 * @brief       This file implements usart driver for apm32
 *
 * @revision
 * Date         Author          Notes
 * 2022-01-10   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include <os_memory.h>
#include <bus/bus.h>
#include <dma/dma.h>
#include <board.h>
#include <string.h>
#include <os_list.h>

#define DRV_EXT_TAG "drv.usart"
#include <dlog.h>

#include "drv_usart.h"

static os_list_node_t ci_usart_list = OS_LIST_INIT(ci_usart_list);

static os_err_t ci_sdma_int_start(soft_dma_t *dma, void *buff, uint32_t size);

static os_err_t ci_sdma_dma_start(soft_dma_t *dma, void *buff, uint32_t size);

static void _usart_rxirq_callback(struct ci_usart *usart)
{
    usart->rx_buff[usart->rx_index] = (usart->info->husart->UARTRdDR & 0xFF);
    usart->rx_index++;

    if (usart->rx_index == (usart->rx_size / 2))
    {
        soft_dma_half_irq(&usart->sdma);
    }

    if (usart->rx_index == usart->rx_size)
    {
        soft_dma_full_irq(&usart->sdma);
    }
}

static void _usart_txirq_callback(struct ci_usart *usart)
{
    os_hw_serial_isr_txdone((struct os_serial_device *)usart);
}

void UART0_IRQHandler(void)
{
    struct ci_usart *usart = OS_NULL;

    os_list_for_each_entry(usart, &ci_usart_list, struct ci_usart, list)
    {
        if (usart->info->husart == UART0)
        {
            break;
        }
    }

    if (UART_MaskIntState(usart->info->husart, UART_OverrunErrorInt))
    {
        UART_IntClear(usart->info->husart, UART_OverrunErrorInt);
    }

    if (UART_MaskIntState(usart->info->husart, UART_TXInt))
    {
        _usart_txirq_callback(usart);
        UART_IntClear(usart->info->husart, UART_TXInt);
    }

    if (UART_MaskIntState(usart->info->husart, UART_RXInt))
    {
        _usart_rxirq_callback(usart);
        UART_IntClear(usart->info->husart, UART_RXInt);
    }

    UART_IntClear(usart->info->husart, UART_AllInt);
}

void UART1_IRQHandler(void)
{
    struct ci_usart *usart = OS_NULL;

    os_list_for_each_entry(usart, &ci_usart_list, struct ci_usart, list)
    {
        if (usart->info->husart == UART1)
        {
            break;
        }
    }
    if (UART_MaskIntState(usart->info->husart, UART_OverrunErrorInt))
    {
        UART_IntClear(usart->info->husart, UART_OverrunErrorInt);
    }

    if (UART_MaskIntState(usart->info->husart, UART_TXInt))
    {
        _usart_txirq_callback(usart);
        UART_IntClear(usart->info->husart, UART_TXInt);
    }

    if (UART_MaskIntState(usart->info->husart, UART_RXInt))
    {
        _usart_rxirq_callback(usart);
        UART_IntClear(usart->info->husart, UART_RXInt);
    }

    UART_IntClear(usart->info->husart, UART_AllInt);
}

void dma_cchannel0_callback_func(void)
{
    struct ci_usart *usart = OS_NULL;
    os_list_for_each_entry(usart, &ci_usart_list, struct ci_usart, list)
    {
        if ((usart->info->dma_enable != 0) && (usart->info->rx_channel == DMACChannel0))
        {
            break;
        }
    }
    if (usart != OS_NULL)
        _usart_rxirq_callback(usart);
}

void dma_cchannel1_callback_func(void)
{
    struct ci_usart *usart = OS_NULL;
    os_list_for_each_entry(usart, &ci_usart_list, struct ci_usart, list)
    {
        if ((usart->info->dma_enable != 0) && (usart->info->rx_channel == DMACChannel1))
        {
            break;
        }
    }
    if (usart != OS_NULL)
        _usart_rxirq_callback(usart);
}

static uint32_t ci_sdma_int_get_index(soft_dma_t *dma)
{
    struct ci_usart *usart = os_container_of(dma, struct ci_usart, sdma);

    return usart->rx_index;
}

static os_err_t ci_sdma_int_start(soft_dma_t *dma, void *buff, uint32_t size)
{
    struct ci_usart *usart = os_container_of(dma, struct ci_usart, sdma);
    UART_TypeDef    *huart = usart->info->husart;

    usart->rx_buff  = buff;
    usart->rx_index = 0;
    usart->rx_size  = size;

    UART_FIFOClear(huart);
    UART_IntMaskConfig(huart, UART_RXInt, DISABLE);
    UART_EN(huart, ENABLE);

    return OS_SUCCESS;
}

static uint32_t ci_sdma_int_stop(soft_dma_t *dma)
{
    struct ci_usart *usart = os_container_of(dma, struct ci_usart, sdma);
    UART_TypeDef    *huart = usart->info->husart;
    UART_IntMaskConfig(huart, UART_RXInt, ENABLE);
    UART_EN(huart, DISABLE);
    int count = usart->rx_index;

    return count;
}

/* dma rx mode */
static uint32_t ci_sdma_dma_get_index(soft_dma_t *dma)
{
    struct ci_usart *usart = os_container_of(dma, struct ci_usart, sdma);
    UART_TypeDef    *huart = usart->info->husart;

    return usart->rx_size - DMAC_ChannelCurrentTransferSize(usart->info->rx_channel);
}

static os_err_t ci_sdma_dma_init(soft_dma_t *dma)
{
    return OS_SUCCESS;
}

extern void set_dma_int_callback(DMACChannelx dmachannel, dma_callback_func_ptr_t func);

static os_err_t ci_sdma_dma_start(soft_dma_t *dma, void *buff, uint32_t size)
{
    struct ci_usart *usart = os_container_of(dma, struct ci_usart, sdma);

    usart->rx_buff = buff;
    usart->rx_size = size;

    set_dma_int_callback(usart->info->rx_channel, dma_cchannel0_callback_func);

    DMAC_M2P_P2M_advance_config(usart->info->rx_channel,
                                usart->info->rx_dma,
                                P2M_DMA,
                                usart->info->dma_uart_addr,
                                (unsigned int)usart->rx_buff,
                                usart->rx_size,
                                TRANSFERWIDTH_8b,
                                BURSTSIZE1);

    return OS_SUCCESS;
}

static uint32_t ci_sdma_dma_stop(soft_dma_t *dma)
{
    struct ci_usart *usart = os_container_of(dma, struct ci_usart, sdma);

    DMAC_ChannelHalt(usart->info->rx_channel, ENABLE);

    return ci_sdma_dma_get_index(dma);
}

static void ci_usart_sdma_callback(soft_dma_t *dma)
{
    struct ci_usart *usart = os_container_of(dma, struct ci_usart, sdma);

    os_hw_serial_isr_rxdone((struct os_serial_device *)usart);
}

static void ci_usart_sdma_init(struct ci_usart *usart, dma_ring_t *ring)
{
    soft_dma_t *dma = &usart->sdma;

    soft_dma_stop(dma);

    memset(&dma->hard_info, 0, sizeof(dma->hard_info));

    dma->hard_info.mode         = HARD_DMA_MODE_NORMAL;
    dma->hard_info.max_size     = 64 * 1024;
    dma->hard_info.data_timeout = uart_calc_byte_timeout_us(usart->serial.config.baud_rate);

    if (usart->info->dma_enable == 0)
    {
        dma->hard_info.flag = 0;

        dma->ops.get_index = ci_sdma_int_get_index;
        dma->ops.dma_init  = OS_NULL;
        dma->ops.dma_start = ci_sdma_int_start;
        dma->ops.dma_stop  = ci_sdma_int_stop;
    }
    else
    {
        dma->hard_info.flag |= HARD_DMA_FLAG_FULL_IRQ;

        dma->ops.get_index = ci_sdma_dma_get_index;
        dma->ops.dma_init  = ci_sdma_dma_init;
        dma->ops.dma_start = ci_sdma_dma_start;
        dma->ops.dma_stop  = ci_sdma_dma_stop;
    }

    dma->cbs.dma_half_callback    = ci_usart_sdma_callback;
    dma->cbs.dma_full_callback    = ci_usart_sdma_callback;
    dma->cbs.dma_timeout_callback = ci_usart_sdma_callback;

    soft_dma_init(dma);
    soft_dma_start(dma, ring);
    soft_dma_irq_enable(&usart->sdma, OS_TRUE);
}

static os_err_t ci_usart_init(struct os_serial_device *serial, struct serial_configure *cfg)
{
    struct ci_usart *usart;
    UART_TypeDef    *huart;
    UART_WordLength  wordlen;
    UART_StopBits    sbits;
    UART_Parity      party;

    OS_ASSERT(serial != OS_NULL);
    OS_ASSERT(cfg != OS_NULL);

    usart = os_container_of(serial, struct ci_usart, serial);
    huart = usart->info->husart;
    switch (cfg->data_bits)
    {
    case DATA_BITS_5:
        wordlen = UART_WordLength_5b;
        break;
    case DATA_BITS_6:
        wordlen = UART_WordLength_6b;
        break;
    case DATA_BITS_7:
        wordlen = UART_WordLength_7b;
        break;
    case DATA_BITS_8:
        wordlen = UART_WordLength_8b;
        break;
    default:
        wordlen = UART_WordLength_8b;
        break;
    }

    switch (cfg->stop_bits)
    {
    case STOP_BITS_2:
        sbits = UART_StopBits_2;
        break;
    default:
        sbits = UART_StopBits_1;
        break;
    }

    switch (cfg->parity)
    {
    case PARITY_ODD:
        party = UART_Parity_Odd;
        break;
    case PARITY_EVEN:
        party = UART_Parity_Even;
        break;
    default:
        party = UART_Parity_No;
        break;
    }

    if (usart->info->dma_enable == 0)
    {
        UartSetCLKBaseBaudrate(huart, cfg->baud_rate);
        Scu_SetIOReuse(usart->info->tx_pinpad, usart->info->tx_resue);
        Scu_SetIOReuse(usart->info->rx_pinpad, usart->info->rx_resue);
        UART_EN(huart, DISABLE);
        eclic_irq_enable(usart->info->usart_irqn);
        UART_BAUDRATEConfig(huart, cfg->baud_rate);
        UART_CRConfig(huart, UART_TXE, ENABLE);
        UART_CRConfig(huart, UART_RXE, ENABLE);
        UART_IntClear(huart, UART_AllInt);
        UART_IntMaskConfig(huart, UART_AllInt, ENABLE);
        UART_TXFIFOByteWordConfig(huart, UART_Byte);
        UART_LCRConfig(huart, wordlen, sbits, party);
        UART_RXFIFOConfig(huart, UART_FIFOLevel1);
        UART_TXFIFOConfig(huart, UART_FIFOLevel1_8);
        UART_FIFOClear(huart);
        UART_CRConfig(huart, UART_NCED, ENABLE);
        UART_IntMaskConfig(huart, UART_TXInt, ENABLE);
        UART_IntMaskConfig(huart, UART_RXInt, DISABLE);
    }
    else
    {
        eclic_irq_enable(GDMA_IRQn);
        Scu_SetDeviceGate((unsigned int)HAL_GDMA_BASE, ENABLE);
        Scu_SetIOReuse(usart->info->tx_pinpad, usart->info->tx_resue);
        Scu_SetIOReuse(usart->info->rx_pinpad, usart->info->rx_resue);
        Scu_Setdevice_Reset((unsigned int)HAL_GDMA_BASE);
        Scu_Setdevice_ResetRelease((unsigned int)HAL_GDMA_BASE);

        Scu_Setdevice_Reset((unsigned int)huart);
        Scu_Setdevice_ResetRelease((unsigned int)huart);

        UART_EN(huart, DISABLE);

        UART_CRConfig(huart, UART_TXE, ENABLE);
        UART_CRConfig(huart, UART_RXE, ENABLE);
        UART_BAUDRATEConfig(huart, cfg->baud_rate);
        UART_TXFIFOByteWordConfig(huart, UART_Byte);
        UART_LCRConfig(huart, wordlen, sbits, party);
        UART_RXFIFOConfig(huart, UART_FIFOLevel1);

        UART_TXFIFOConfig(huart, UART_FIFOLevel1_8);

        UART_DMAByteWordConfig(huart, ENABLE);

        UART_IntClear(huart, UART_AllInt);
        UART_IntMaskConfig(huart, UART_AllInt, ENABLE);
        UART_FIFOClear(huart);
        UART_CRConfig(huart, UART_NCED, ENABLE);
        UART_TXRXDMAConfig(huart, UART_RXDMA);
    }

    ci_usart_sdma_init(usart, &serial->rx_fifo->ring);

    return OS_SUCCESS;
}

static os_err_t ci_usart_deinit(struct os_serial_device *serial)
{
    struct ci_usart *usart;

    OS_ASSERT(serial != OS_NULL);

    usart = os_container_of(serial, struct ci_usart, serial);

    if (usart->info->dma_enable)
    {
        DMAC_ChannelTCInt(usart->info->rx_channel, DISABLE);
        DMAC_ChannelDisable(usart->info->rx_channel);
        DMAC_ChannelPowerDown(usart->info->rx_channel, DISABLE);
    }
    else
    {
        UART_IntMaskConfig(usart->info->husart, UART_RXInt, ENABLE);
        eclic_irq_disable(usart->info->usart_irqn);
        UART_EN(usart->info->husart, DISABLE);
    }

    soft_dma_stop(&usart->sdma);

    return 0;
}

static int ci_usart_poll_send(struct os_serial_device *serial, const uint8_t *buff, os_size_t size)
{
    struct ci_usart *usart;
    os_size_t        i;
    os_ubase_t        level;

    OS_ASSERT(serial != OS_NULL);

    usart = (struct ci_usart *)serial;

    for (i = 0; i < size; i++)
    {
        os_spin_lock_irqsave(&gs_device_lock, &level);
        UartPollingSenddata(usart->info->husart, buff[i]);
        UartPollingSenddone(usart->info->husart);
        os_spin_unlock_irqrestore(&gs_device_lock, level);
    }
    return size;
}

static const struct os_uart_ops _usart_ops = {
    .init   = ci_usart_init,
    .deinit = ci_usart_deinit,

    .start_send = OS_NULL,
    .poll_send  = ci_usart_poll_send,
};

static os_err_t _usart_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    os_ubase_t               level  = 0;
    struct serial_configure config = OS_SERIAL_CONFIG_DEFAULT;

    struct ci_usart *usart = os_calloc(1, sizeof(struct ci_usart));
    if (usart == OS_NULL)
    {
        return OS_SUCCESS;
    }

    usart->info          = (struct ci_usart_info *)dev->info;
    usart->serial.ops    = &_usart_ops;
    usart->serial.config = config;

    os_spin_lock_irqsave(&gs_device_lock, &level);
    os_list_add_tail(&ci_usart_list, &usart->list);
    os_spin_unlock_irqrestore(&gs_device_lock, level);

    if (os_hw_serial_register(&usart->serial, dev->name, NULL) != OS_SUCCESS)
    {
        LOG_E(DRV_EXT_TAG, "os_hw_serial_register error!");
    }
    return OS_SUCCESS;
}

OS_DRIVER_INFO _usart_driver = {
    .name  = "UART_TypeDef",
    .probe = _usart_probe,
};

OS_DRIVER_DEFINE(_usart_driver, OS_INIT_LEVEL_PRE_DEVICE, OS_INIT_SUBLEVEL_HIGH);

#ifdef OS_USING_CONSOLE
static UART_TypeDef *console_uart = 0;
void                 __os_hw_console_output(char *str)
{
    if (console_uart == 0)
        return;
    while (*str)
    {
        UartPollingSenddata(console_uart, *str++);
        UartPollingSenddone(console_uart);
    }
}

static void _usart_hard_init(ci_usart_info_t *info)
{
    UartSetCLKBaseBaudrate(info->husart, UART_BaudRate115200);
    Scu_SetIOReuse(info->tx_pinpad, info->tx_resue);
    Scu_SetIOReuse(info->rx_pinpad, info->rx_resue);
    UART_EN(info->husart, DISABLE);
    eclic_irq_enable(info->usart_irqn);

    UART_EN(info->husart, DISABLE);
    UART_BAUDRATEConfig(info->husart, UART_BaudRate115200);
    UART_CRConfig(info->husart, UART_TXE, ENABLE);
    UART_CRConfig(info->husart, UART_RXE, ENABLE);
    UART_IntClear(info->husart, UART_AllInt);
    UART_IntMaskConfig(info->husart, UART_AllInt, ENABLE);
    UART_TXFIFOByteWordConfig(info->husart, UART_Byte);
    UART_LCRConfig(info->husart, UART_WordLength_8b, UART_StopBits_1, UART_Parity_No);
    UART_RXFIFOConfig(info->husart, UART_FIFOLevel1);
    UART_TXFIFOConfig(info->husart, UART_FIFOLevel1_8);
    UART_FIFOClear(info->husart);
    UART_CRConfig(info->husart, UART_NCED, ENABLE);
    UART_IntMaskConfig(info->husart, UART_TXInt, ENABLE);
    UART_IntMaskConfig(info->husart, UART_RXInt, DISABLE);
    UART_EN(info->husart, ENABLE);
}

static os_err_t _usart_early_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    ci_usart_info_t *info = (ci_usart_info_t *)dev->info;
    _usart_hard_init(info);

    if (!strcmp(dev->name, OS_CONSOLE_DEVICE_NAME))
    {
        console_uart = (UART_TypeDef *)info->husart;
    }
    return OS_SUCCESS;
}

OS_DRIVER_INFO _usart_early_driver = {
    .name  = "UART_TypeDef",
    .probe = _usart_early_probe,
};

OS_DRIVER_DEFINE(_usart_early_driver, OS_INIT_LEVEL_PRE_KERNEL_1, OS_INIT_SUBLEVEL_LOW);
#endif

