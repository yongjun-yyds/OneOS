/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_common.c
 *
 * @brief       This file provides systick time init/IRQ and board init functions.
 *
 * @revision
 * Date         Author          Notes
 * 2022-01-10   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include <string.h>
#include <os_types.h>
#include <os_stddef.h>

#include <arch_interrupt.h>
#include <os_clock.h>
#include <os_memory.h>
#include <oneos_config.h>

#include <drv_common.h>
#include <board.h>
#include <drv_gpio.h>

#ifdef OS_USING_DMA
#include <dma.h>
#endif

#ifdef OS_USING_CLOCKSOURCE
#include <timer/clocksource.h>
#include <timer/clocksource_cortexm.h>
#endif

extern uint32_t SystemCoreClock;

void os_tick_handler(void)
{
    os_tick_increase();
#ifdef OS_USING_CLOCKSOURCE
    os_clocksource_update();
#endif
}

#if defined(SERIES_CH32V30X)
#ifdef OS_USING_SYSTICK_FOR_CLOCKSOURCE
static uint64_t riscv_systick_read(void *clock)
{
    return SysTick->CNT;
}

static os_clocksource_t riscv_systick_clocksource;

void riscv_systick_clocksource_init(void)
{
    SysTick->CTLR = 0;
    SysTick->SR   = 0;
    SysTick->CNT  = 0;
    SysTick->CMP  = 0xffffffffffffffffull;
    SysTick->CTLR = 0xD;

    riscv_systick_clocksource.rating = 640;
    riscv_systick_clocksource.freq   = SystemCoreClock;
    riscv_systick_clocksource.mask   = 0xffffffffffffffffull;
    riscv_systick_clocksource.read   = riscv_systick_read;

    os_clocksource_register("systick", &riscv_systick_clocksource);
}
#else
static uint32_t _SysTick_Config(uint32_t ticks)
{
    if ((ticks - 1) > 0xFFFFFFFF)
    {
        return 1;
    }
    NVIC_SetPriority(SysTicK_IRQn, 0xff);
    NVIC_EnableIRQ(SysTicK_IRQn);
    SysTick->CTLR = 0;
    SysTick->SR   = 0;
    SysTick->CNT  = 0;
    SysTick->CMP  = ticks - 1;
    SysTick->CTLR = 0xF;
    return 0;
}

void SysTick_Handler(void) __attribute__((interrupt()));
void SysTick_Handler(void)
{
    TOGGLE_SP();
    SysTick->SR = 0;
    os_tick_handler();
    TOGGLE_SP();
}
#endif /* OS_USING_SYSTICK_FOR_CLOCKSOURCE */

#elif defined(SERIES_CH32V10X)
void systick_set(uint32_t ticks)
{
    if (ticks > 0)
        ticks -= 1;
    SysTick->CTLR   = 0;
    SysTick->CNTL0  = 0;
    SysTick->CNTL1  = 0;
    SysTick->CNTL2  = 0;
    SysTick->CNTL3  = 0;
    SysTick->CNTH0  = 0;
    SysTick->CNTH1  = 0;
    SysTick->CNTH2  = 0;
    SysTick->CNTH3  = 0;
    SysTick->CMPLR0 = (uint8_t)(ticks);
    SysTick->CMPLR1 = (uint8_t)(ticks >> 8);
    SysTick->CMPLR2 = (uint8_t)(ticks >> 16);
    SysTick->CMPLR3 = (uint8_t)(ticks >> 24);
    SysTick->CMPHR0 = 0;
    SysTick->CMPHR1 = 0;
    SysTick->CMPHR2 = 0;
    SysTick->CMPHR3 = 0;
    SysTick->CTLR   = 1;
}

#ifdef OS_USING_SYSTICK_FOR_CLOCKSOURCE
#define sys_cntl ((uint32_t *)0xE000F004)
#define sys_cnth ((uint32_t *)0xE000F008)

static uint64_t riscv_systick_read(void *clock)
{
    uint64_t counter = (((uint64_t)(*sys_cntl)) | ((uint64_t)(*sys_cnth) << 32));

    return counter;
}

static os_clocksource_t riscv_systick_clocksource;

void riscv_systick_clocksource_init(void)
{
    systick_set(0);

    riscv_systick_clocksource.rating = 640;
    riscv_systick_clocksource.freq   = SystemCoreClock / 8;
    riscv_systick_clocksource.mask   = 0xffffffffffffffffull;
    riscv_systick_clocksource.read   = riscv_systick_read;

    os_clocksource_register("systick", &riscv_systick_clocksource);
}
#else
static uint32_t sys_tick = 0;
static uint32_t _SysTick_Config(uint32_t ticks)
{
    if ((ticks - 1) > 0xFFFFFFFF)
    {
        return 1;
    }
    sys_tick = ticks;

    NVIC_SetPriority(SysTicK_IRQn, 0xf0);
    systick_set(sys_tick);
    NVIC_EnableIRQ(SysTicK_IRQn);
    return 0;
}

void SysTick_Handler(void) __attribute__((interrupt()));
void SysTick_Handler(void)
{
    TOGGLE_SP();
    NVIC_ClearPendingIRQ(SysTicK_IRQn);
    os_tick_handler();
    systick_set(sys_tick);
    TOGGLE_SP();
}
#endif /* OS_USING_SYSTICK_FOR_CLOCKSOURCE */
#endif

void hardware_init(void)
{
#ifdef SERIES_CH32V10X
    PFIC->CFGR = NVIC_KEY1 | 0x3;
#endif
    NVIC_SetPriority(Software_IRQn, 0xf0);
    NVIC_EnableIRQ(Software_IRQn);
}

void os_hw_cpu_reset(void)
{
    NVIC_SystemReset();
}

/**
 ***********************************************************************************************************************
 * @brief           This function will initial ch32 board.
 *
 * @param[in]       none
 *
 * @return          none
 ***********************************************************************************************************************
 */
static os_err_t os_hw_board_init(void)
{
    hardware_init();

    /* Heap initialization */

#ifdef OS_USING_HEAP
    if ((os_size_t)HEAP_END > (os_size_t)HEAP_BEGIN)
    {
        os_default_heap_init();
        os_default_heap_add((void *)HEAP_BEGIN, (os_size_t)HEAP_END - (os_size_t)HEAP_BEGIN, OS_MEM_ALG_DEFAULT);
    }
#endif

#ifdef OS_USING_DMA_RAM
    os_dma_mem_init();
#endif

    return OS_SUCCESS;
}
OS_INIT_CALL(os_hw_board_init, OS_INIT_LEVEL_PRE_KERNEL_1, OS_INIT_SUBLEVEL_MIDDLE);

static os_err_t board_post_init(void)
{
#ifdef OS_USING_PIN
    os_hw_pin_init();
#endif
    SystemCoreClockUpdate();

#ifdef OS_USING_SYSTICK_FOR_CLOCKSOURCE
    riscv_systick_clocksource_init();
#elif defined(SERIES_CH32V30X)
    _SysTick_Config(SystemCoreClock / OS_TICK_PER_SECOND);
#else
    _SysTick_Config(SystemCoreClock / 8 / OS_TICK_PER_SECOND);

#endif

    return OS_SUCCESS;
}
OS_INIT_CALL(board_post_init, OS_INIT_LEVEL_POST_KERNEL, OS_INIT_SUBLEVEL_LOW);
