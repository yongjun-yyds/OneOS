/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_hwtimer.c
 *
 * @brief       This file implements timer driver for mm32.
 *
 * @revision
 * Date         Author          Notes
 * 2021-05-31   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include <drv_common.h>
#include <drv_hwtimer.h>
#include <os_memory.h>
#include <timer/timer.h>
#include <core_riscv.h>
#ifdef OS_USING_CLOCKSOURCE
#include <timer/clocksource.h>
#endif

#ifdef OS_USING_CLOCKEVENT
#include <timer/clockevent.h>
#endif

#define DRV_EXT_TAG "drv.hwtimer"
#include <drv_log.h>

static os_list_node_t ch32_timer_list = OS_LIST_INIT(ch32_timer_list);

#define PPRE1_Set_Mask ((uint32_t)0x00000700)
#define PPRE2_Set_Mask ((uint32_t)0x00003800)
static uint32_t ch32_timer_get_freq(struct ch32_timer *timer)
{
    uint32_t       clk = 0;
    uint32_t       tmp = 0;
    RCC_ClocksTypeDef RCC_Clocks;
    TIM_TypeDef      *htim;

    OS_ASSERT(timer->info->htim != OS_NULL);
    htim = timer->info->htim;

    RCC_GetClocksFreq(&RCC_Clocks);

    if (htim == TIM1 || htim == TIM8 || htim == TIM9 || htim == TIM10)
    {
        tmp = (RCC->CFGR0 & PPRE2_Set_Mask) >> 11;
        if (tmp < 0b100)
            clk = RCC_Clocks.PCLK2_Frequency;
        else
            clk = 2 * RCC_Clocks.PCLK2_Frequency;
    }
    else
    {
        tmp = (RCC->CFGR0 & PPRE1_Set_Mask) >> 8;
        if (tmp < 0b100)
            clk = RCC_Clocks.PCLK1_Frequency;
        else
            clk = 2 * RCC_Clocks.PCLK1_Frequency;
    }

    return clk;
}

static void ch32_timer_hard_init(struct ch32_timer *timer)
{
    if (timer->info->htim == TIM1 || timer->info->htim == TIM9 || timer->info->htim == TIM10)
        RCC_APB2PeriphClockCmd(timer->info->tim_clk, ENABLE);
    else
        RCC_APB1PeriphClockCmd(timer->info->tim_clk, ENABLE);
}

/* clang-format off */
void TIM1_UP_IRQHandler(void) __attribute__((interrupt()));
void TIM9_UP_IRQHandler(void) __attribute__((interrupt()));
void TIM10_UP_IRQHandler(void) __attribute__((interrupt()));
void TIM2_IRQHandler(void) __attribute__((interrupt()));
void TIM3_IRQHandler(void) __attribute__((interrupt()));
/* clang-format on */

void TIM_IRQ_Callback(TIM_TypeDef *htim)
{
    struct ch32_timer *timer;
    os_list_for_each_entry(timer, &ch32_timer_list, struct ch32_timer, list)
    {
        if (timer->info->htim == htim)
        {
            if (TIM_GetITStatus(timer->info->htim, TIM_IT_Update) != RESET)
            {
                TIM_ClearITPendingBit(timer->info->htim, TIM_IT_Update);
#ifdef OS_USING_CLOCKEVENT
                // if (timer->mode == TIMER_MODE_TIM)
                os_clockevent_isr((os_clockevent_t *)timer);
#endif
            }
            break;
        }
    }
}

#define TIM_IRQ_CALLBACK_FUNC(tim)                                                                                     \
    void TIM##tim##_UP_IRQHandler(void)                                                                                \
    {                                                                                                                  \
        TOGGLE_SP();                                                                                                   \
        TIM_IRQ_Callback(TIM##tim);                                                                                    \
        TOGGLE_SP();                                                                                                   \
    }

// gpio_irq_callback function
TIM_IRQ_CALLBACK_FUNC(1);
TIM_IRQ_CALLBACK_FUNC(9);
TIM_IRQ_CALLBACK_FUNC(10);

void TIM2_IRQHandler(void)
{
    TOGGLE_SP();
    TIM_IRQ_Callback(TIM2);
    TOGGLE_SP();
}

void TIM3_IRQHandler(void)
{
    TOGGLE_SP();
    TIM_IRQ_Callback(TIM3);
    TOGGLE_SP();
}

void TIM4_IRQHandler(void)
{
    TOGGLE_SP();
    TIM_IRQ_Callback(TIM4);
    TOGGLE_SP();
}

#if defined(OS_USING_CLOCKSOURCE) || defined(OS_USING_CLOCKEVENT)
static uint64_t ch32_timer_read(void *clock)
{
    struct ch32_timer *timer;

    timer = (struct ch32_timer *)clock;

    return TIM_GetCounter(timer->info->htim);
}
#endif

#ifdef OS_USING_CLOCKEVENT
static void ch32_timer_start(os_clockevent_t *ce, uint32_t prescaler, uint64_t count)
{
    struct ch32_timer *timer;

    OS_ASSERT(prescaler != 0);
    OS_ASSERT(count != 0);

    timer = (struct ch32_timer *)ce;

    TIM_TypeDef *htim = timer->info->htim;

    TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;
    TIM_TimeBaseInitStruct.TIM_Prescaler         = prescaler - 1;
    TIM_TimeBaseInitStruct.TIM_ClockDivision     = TIM_CKD_DIV1;
    TIM_TimeBaseInitStruct.TIM_CounterMode       = TIM_CounterMode_Up;
    TIM_TimeBaseInitStruct.TIM_Period            = count;
    TIM_TimeBaseInitStruct.TIM_RepetitionCounter = 0;
    TIM_TimeBaseInit(htim, &TIM_TimeBaseInitStruct);

    // TIM_ARRPreloadConfig(htim, ENABLE);//?? 在官方例程中测试有无该句代码的影响�??

    TIM_ClearITPendingBit(htim, TIM_IT_Update);
    TIM_ITConfig(htim, TIM_IT_Update, ENABLE);
    TIM_Cmd(htim, ENABLE);
}

static void ch32_timer_stop(os_clockevent_t *ce)
{
    struct ch32_timer *timer;

    OS_ASSERT(ce != OS_NULL);

    timer = (struct ch32_timer *)ce;

    TIM_TypeDef *htim = timer->info->htim;

    TIM_ITConfig(htim, TIM_IT_Update, DISABLE);
    TIM_Cmd(htim, DISABLE);
    TIM_ClearITPendingBit(htim, TIM_IT_Update);
}

static const struct os_clockevent_ops ch32_tim_ops = {
    .start = ch32_timer_start,
    .stop  = ch32_timer_stop,
    .read  = ch32_timer_read,
};
#endif

static int ch32_tim_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    struct ch32_timer *timer;

    timer = os_calloc(1, sizeof(struct ch32_timer));
    OS_ASSERT(timer);

    timer->info = (struct ch32_timer_info *)dev->info;

    ch32_timer_hard_init(timer);

    timer->freq = ch32_timer_get_freq(timer);

#ifdef OS_USING_CLOCKSOURCE
    if (os_clocksource_best() == OS_NULL)
    {
        TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;
        TIM_TimeBaseInitStruct.TIM_Prescaler         = 11;
        TIM_TimeBaseInitStruct.TIM_ClockDivision     = TIM_CKD_DIV1;
        TIM_TimeBaseInitStruct.TIM_CounterMode       = TIM_CounterMode_Up;
        TIM_TimeBaseInitStruct.TIM_Period            = 0xfffful;
        TIM_TimeBaseInitStruct.TIM_RepetitionCounter = 0x00;
        TIM_TimeBaseInit(timer->info->htim, &TIM_TimeBaseInitStruct);
        TIM_ARRPreloadConfig(timer->info->htim, ENABLE);

        TIM_Cmd(timer->info->htim, ENABLE);

        timer->clock.cs.rating = 160;
        timer->clock.cs.freq   = timer->freq / 12;
        timer->clock.cs.mask   = 0xfffful;
        timer->clock.cs.read   = ch32_timer_read;

        os_clocksource_register(dev->name, &timer->clock.cs);
    }
    else
#endif
    {
#ifdef OS_USING_CLOCKEVENT
        NVIC_Init(&timer->info->nvic_struct);

        timer->clock.ce.rating = 160;
        timer->clock.ce.freq   = timer->freq;
        timer->clock.ce.mask   = 0xffffull;

        timer->clock.ce.prescaler_mask = 0xfffful;
        timer->clock.ce.prescaler_bits = 16;

        timer->clock.ce.count_mask = 0xfffful;
        timer->clock.ce.count_bits = 16;

        timer->clock.ce.feature = OS_CLOCKEVENT_FEATURE_PERIOD;

        timer->clock.ce.min_nsec = NSEC_PER_SEC / timer->clock.ce.freq;

        timer->clock.ce.ops = &ch32_tim_ops;
        os_clockevent_register(dev->name, &timer->clock.ce);
#endif
    }

    os_list_add(&ch32_timer_list, &timer->list);

    return OS_SUCCESS;
}

OS_DRIVER_INFO ch32_tim_driver = {
    .name  = "TIM_TypeDef",
    .probe = ch32_tim_probe,
};

OS_DRIVER_DEFINE(ch32_tim_driver, OS_INIT_LEVEL_PRE_DEVICE, OS_INIT_SUBLEVEL_MIDDLE);

