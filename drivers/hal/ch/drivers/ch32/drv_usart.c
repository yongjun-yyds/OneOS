/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_usart.c
 *
 * @brief       This file implements usart driver for apm32
 *
 * @revision
 * Date         Author          Notes
 * 2022-01-10   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include <os_memory.h>
#include <bus/bus.h>
#include <dma/dma.h>
#include <board.h>
#include <string.h>
#include <os_list.h>

#define DRV_EXT_TAG "drv.usart"
#include <dlog.h>

#include "drv_usart.h"

static os_list_node_t ch32_usart_list = OS_LIST_INIT(ch32_usart_list);

static void _usart_hard_init(ch32_usart_info_t *info)
{
    info->gpio_PeriphClock(info->gpio_Periph, ENABLE);
    info->usart_PeriphClock(info->usart_Periph, ENABLE);

    GPIO_Init(info->tx_pin_port, &info->tx_pin_info);
    GPIO_Init(info->rx_pin_port, &info->rx_pin_info);

    USART_Init(info->husart, &info->usart_def_cfg);
    USART_Cmd(info->husart, ENABLE);
}

static void _usart_irq_callback(USART_TypeDef *husart)
{
    struct ch32_usart *usart      = OS_NULL;
    ITStatus           rx_state   = RESET;
    ITStatus           idle_state = RESET;

    os_list_for_each_entry(usart, &ch32_usart_list, struct ch32_usart, list)
    {
        if (usart->info->husart == husart)
        {
            rx_state   = USART_GetITStatus(usart->info->husart, USART_IT_RXNE);
            idle_state = USART_GetITStatus(usart->info->husart, USART_IT_IDLE);
            if (rx_state != RESET)
            {
                USART_ClearITPendingBit(usart->info->husart, USART_IT_RXNE);
                if (OS_NULL == usart->info->DMAy_Channelx)
                {
                    usart->rx_buff[usart->rx_index] = (uint8_t)USART_ReceiveData(usart->info->husart);
                    // USART_ClearFlag(usart->info->husart, USART_IT_RXNE);
                    usart->rx_index++;
                    if (idle_state != RESET)
                    {
                        /* soft dma idle irq */
                        soft_dma_timeout_irq(&usart->sdma);
                        break;
                    }

                    if (usart->rx_index == (usart->rx_size / 2))
                    {
                        soft_dma_half_irq(&usart->sdma);
                    }

                    if (usart->rx_index == usart->rx_size)
                    {
                        soft_dma_full_irq(&usart->sdma);
                    }
                }
            }
            /* usart idle irq */
            if (idle_state != RESET)
            {
                USART_ReceiveData(usart->info->husart);
                /* soft dma idle irq */
                soft_dma_timeout_irq(&usart->sdma);
            }
        }
    }
}

static void usart_dma_irqhandler(USART_TypeDef *husart, uint32_t int_flag)
{
    struct ch32_usart *usart;

    os_list_for_each_entry(usart, &ch32_usart_list, struct ch32_usart, list)
    {
        if (husart == usart->info->husart)
        {
            if (int_flag == DMA_IT_HT)
            {
                soft_dma_half_irq(&usart->sdma);
            }
            else
            {
                soft_dma_full_irq(&usart->sdma);
            }
            break;
        }
    }
}

void USART1_IRQHandler(void) __attribute__((interrupt()));
void USART2_IRQHandler(void) __attribute__((interrupt()));
void USART3_IRQHandler(void) __attribute__((interrupt()));
void DMA1_Channel3_IRQHandler(void) __attribute__((interrupt()));
void DMA1_Channel5_IRQHandler(void) __attribute__((interrupt()));
void DMA1_Channel6_IRQHandler(void) __attribute__((interrupt()));

void USART1_IRQHandler(void)
{
    TOGGLE_SP();
    _usart_irq_callback(USART1);
    TOGGLE_SP();
}

void USART2_IRQHandler(void)
{
    TOGGLE_SP();
    _usart_irq_callback(USART2);
    TOGGLE_SP();
}

void USART3_IRQHandler(void)
{
    TOGGLE_SP();
    _usart_irq_callback(USART3);
    TOGGLE_SP();
}

void DMA1_Channel5_IRQHandler(void)
{
    TOGGLE_SP();

    if (SET == DMA_GetITStatus(DMA1_IT_HT5))
    {
        DMA_ClearITPendingBit(DMA1_IT_HT5);
        usart_dma_irqhandler(USART1, DMA_IT_HT);
    }
    if (SET == DMA_GetITStatus(DMA1_IT_TC5))
    {
        DMA_ClearITPendingBit(DMA1_IT_TC5);
        usart_dma_irqhandler(USART1, DMA_IT_TC);
    }
    TOGGLE_SP();
}

void DMA1_Channel6_IRQHandler(void)
{
    TOGGLE_SP();

    if (SET == DMA_GetITStatus(DMA1_IT_HT6))
    {
        DMA_ClearITPendingBit(DMA1_IT_HT6);
        usart_dma_irqhandler(USART1, DMA_IT_HT);
    }
    if (SET == DMA_GetITStatus(DMA1_IT_TC6))
    {
        DMA_ClearITPendingBit(DMA1_IT_TC6);
        usart_dma_irqhandler(USART1, DMA_IT_TC);
    }
    TOGGLE_SP();
}

void DMA1_Channel3_IRQHandler(void)
{
    TOGGLE_SP();

    if (SET == DMA_GetITStatus(DMA1_IT_HT3))
    {
        DMA_ClearITPendingBit(DMA1_IT_HT3);
        usart_dma_irqhandler(USART1, DMA_IT_HT);
    }
    if (SET == DMA_GetITStatus(DMA1_IT_TC3))
    {
        DMA_ClearITPendingBit(DMA1_IT_TC3);
        usart_dma_irqhandler(USART1, DMA_IT_TC);
    }
    TOGGLE_SP();
}

static uint32_t _sdma_int_get_index(soft_dma_t *dma)
{
    ch32_usart_t *usart = os_container_of(dma, ch32_usart_t, sdma);

    return usart->rx_index;
}

static os_err_t _sdma_int_start(soft_dma_t *dma, void *buff, uint32_t size)
{
    ch32_usart_t *usart = os_container_of(dma, ch32_usart_t, sdma);

    usart->rx_buff  = buff;
    usart->rx_index = 0;
    usart->rx_size  = size;

    USART_ITConfig(usart->info->husart, USART_IT_RXNE, ENABLE);
    // USART_ITConfig(usart->info->husart, USART_IT_IDLE, ENABLE);

    return OS_SUCCESS;
}

static uint32_t _sdma_int_stop(soft_dma_t *dma)
{
    ch32_usart_t *usart = os_container_of(dma, ch32_usart_t, sdma);

    USART_ITConfig(usart->info->husart, USART_IT_RXNE, DISABLE);
    // USART_ITConfig(usart->info->husart, USART_IT_IDLE, DISABLE);

    return usart->rx_index;
}

static uint32_t ch32_dma_get_index(soft_dma_t *dma)
{
    uint32_t   count = 0;
    ch32_usart_t *usart = os_container_of(dma, ch32_usart_t, sdma);

    count = usart->sdma_hard_size - DMA_GetCurrDataCounter(usart->info->DMAy_Channelx);

    if (count == usart->sdma_hard_size)
    {
        count = 0;
    }

    return count;
}

static os_err_t ch32_dma_init(soft_dma_t *dma)
{
    return OS_SUCCESS;
}

static os_err_t ch32_dma_start(soft_dma_t *dma, void *buff, uint32_t size)
{
    ch32_usart_t *usart = os_container_of(dma, ch32_usart_t, sdma);

    usart->sdma_hard_size = size;

    /* enable DMA clock */
    RCC_AHBPeriphClockCmd(usart->info->DMA_RCCPeriph, ENABLE);

    /* DMA deinit */
    DMA_DeInit(usart->info->DMAy_Channelx);
    // DMA_StructInit(&usart->DMA_InitStruct);

    /* set dma init parameters */
    usart->DMA_InitStruct.DMA_PeripheralBaseAddr = (uint32_t)(&usart->info->husart->DATAR);
    usart->DMA_InitStruct.DMA_MemoryBaseAddr     = (uint32_t)buff;
    usart->DMA_InitStruct.DMA_DIR                = DMA_DIR_PeripheralSRC;
    usart->DMA_InitStruct.DMA_PeripheralInc      = DMA_PeripheralInc_Disable;
    usart->DMA_InitStruct.DMA_MemoryInc          = DMA_MemoryInc_Enable;
    usart->DMA_InitStruct.DMA_BufferSize         = size;
    usart->DMA_InitStruct.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
    usart->DMA_InitStruct.DMA_MemoryDataSize     = DMA_MemoryDataSize_Byte;
    usart->DMA_InitStruct.DMA_Mode               = DMA_Mode_Circular;
    usart->DMA_InitStruct.DMA_Priority           = DMA_Priority_VeryHigh;
    usart->DMA_InitStruct.DMA_M2M                = DMA_M2M_Disable;

    /* DMA init */
    DMA_Init(usart->info->DMAy_Channelx, &usart->DMA_InitStruct);

    /* enable USART idle interrupt */
    USART_ITConfig(usart->info->husart, USART_IT_IDLE, ENABLE);

    /* enable DMA transfer half interrupt */
    DMA_ITConfig(usart->info->DMAy_Channelx, DMA_IT_HT, ENABLE);
    /* enable DMA transfer complete interrupt */
    DMA_ITConfig(usart->info->DMAy_Channelx, DMA_IT_TC, ENABLE);

    /* enable DMA interrupt*/
    NVIC_Init(&usart->info->dma_nvic_cfg);

    /* enable DMA channel and dma_rx */
    DMA_Cmd(usart->info->DMAy_Channelx, ENABLE);
    USART_DMACmd(usart->info->husart, USART_DMAReq_Rx, ENABLE);

    return OS_SUCCESS;
}

static uint32_t ch32_dma_stop(soft_dma_t *dma)
{
    ch32_usart_t *usart = os_container_of(dma, ch32_usart_t, sdma);

    /* disable DMA transfer half interrupt */
    DMA_ITConfig(usart->info->DMAy_Channelx, DMA_IT_HT, DISABLE);
    /* disable DMA transfer complete interrupt */
    DMA_ITConfig(usart->info->DMAy_Channelx, DMA_IT_TC, DISABLE);
    /* disable DMA */
    DMA_Cmd(usart->info->DMAy_Channelx, DISABLE);

    return ch32_dma_get_index(dma);
}

/* sdma callback */
static void _usart_sdma_callback(soft_dma_t *dma)
{
    ch32_usart_t *usart = os_container_of(dma, ch32_usart_t, sdma);

    os_hw_serial_isr_rxdone((struct os_serial_device *)usart);
}

static void _usart_sdma_init(struct ch32_usart *usart, dma_ring_t *ring)
{
    soft_dma_t *dma = &usart->sdma;

    soft_dma_stop(dma);

    memset(&dma->hard_info, 0, sizeof(dma->hard_info));

    dma->hard_info.max_size = 64 * 1024;

    dma->hard_info.flag         = HARD_DMA_FLAG_TIMEOUT_IRQ | HARD_DMA_FLAG_HALF_IRQ | HARD_DMA_FLAG_FULL_IRQ;
    dma->hard_info.data_timeout = uart_calc_byte_timeout_us(usart->serial.config.baud_rate);

    if (usart->info->DMAy_Channelx == OS_NULL)
    {
        dma->hard_info.mode = HARD_DMA_MODE_NORMAL;
        dma->ops.get_index  = _sdma_int_get_index;
        dma->ops.dma_init   = OS_NULL;
        dma->ops.dma_start  = _sdma_int_start;
        dma->ops.dma_stop   = _sdma_int_stop;
    }
    else
    {
        dma->hard_info.mode = HARD_DMA_MODE_CIRCULAR;
        dma->ops.get_index  = ch32_dma_get_index;
        dma->ops.dma_init   = ch32_dma_init;
        dma->ops.dma_start  = ch32_dma_start;
        dma->ops.dma_stop   = ch32_dma_stop;
    }
    dma->cbs.dma_half_callback    = _usart_sdma_callback;
    dma->cbs.dma_full_callback    = _usart_sdma_callback;
    dma->cbs.dma_timeout_callback = _usart_sdma_callback;

    soft_dma_init(dma);
    soft_dma_start(dma, ring);
    soft_dma_irq_enable(dma, OS_TRUE);
}

static os_err_t _usart_init(struct os_serial_device *serial, struct serial_configure *cfg)
{
    USART_InitTypeDef usart_cfg = {0};

    struct ch32_usart *usart = (struct ch32_usart *)serial;

    usart_cfg.USART_Mode                = usart->info->usart_def_cfg.USART_Mode;
    usart_cfg.USART_HardwareFlowControl = usart->info->usart_def_cfg.USART_HardwareFlowControl;
    usart_cfg.USART_BaudRate            = cfg->baud_rate;

    switch (cfg->stop_bits)
    {
    case STOP_BITS_1:
        usart_cfg.USART_StopBits = USART_StopBits_1;
        break;
    case STOP_BITS_2:
        usart_cfg.USART_StopBits = USART_StopBits_2;
        break;
    default:
        usart_cfg.USART_StopBits = USART_StopBits_1;
        break;
    }
    switch (cfg->parity)
    {
    case PARITY_NONE:
        usart_cfg.USART_Parity = USART_Parity_No;
        break;
    case PARITY_ODD:
        usart_cfg.USART_Parity = USART_Parity_Odd;
        break;
    case PARITY_EVEN:
        usart_cfg.USART_Parity = USART_Parity_Even;
        break;
    }

    switch (cfg->data_bits)
    {
    case DATA_BITS_8:
        usart_cfg.USART_WordLength = USART_WordLength_8b;
        break;
    case DATA_BITS_9:
        usart_cfg.USART_WordLength = USART_WordLength_9b;
        break;
    default:
        LOG_E(DRV_EXT_TAG, "wordLength not support!");
        break;
    }

    usart->info->usart_PeriphClock(usart->info->usart_Periph, ENABLE);

    USART_Init(usart->info->husart, &usart_cfg);
    if (usart->info->DMAy_Channelx == OS_NULL)
        USART_ITConfig(usart->info->husart, usart->info->usart_it, ENABLE);
    USART_ITConfig(usart->info->husart, USART_IT_IDLE, ENABLE);
    NVIC_Init(&usart->info->usart_nvic_cfg);

    USART_Cmd(usart->info->husart, ENABLE);

    _usart_sdma_init(usart, &usart->serial.rx_fifo->ring);

    return OS_SUCCESS;
}

static os_err_t _usart_deinit(struct os_serial_device *serial)
{
    struct ch32_usart *usart = (struct ch32_usart *)serial;

    soft_dma_stop(&usart->sdma);
    USART_DeInit(usart->info->husart);

    return OS_SUCCESS;
}

/* clang-format off */
static int _usart_poll_send(struct os_serial_device *serial, const uint8_t *buff, os_size_t size)
{
    int i = 0;
    os_ubase_t level;

    struct ch32_usart *usart = (struct ch32_usart *)serial;

    for (i = 0; i < size; i++)
    {
        while (USART_GetFlagStatus(usart->info->husart, USART_FLAG_TXE) == RESET);
        os_spin_lock_irqsave(&gs_device_lock, &level);
        USART_SendData(usart->info->husart, *(buff + i));
        os_spin_unlock_irqrestore(&gs_device_lock, level);
    }

    return size;
}
/* clang-format on */

static const struct os_uart_ops _usart_ops = {
    .init   = _usart_init,
    .deinit = _usart_deinit,

    .start_send = OS_NULL,
    .poll_send  = _usart_poll_send,
};

static void _usart_parse_config(struct ch32_usart *usart)
{
    struct os_serial_device *serial = &usart->serial;

    serial->config.baud_rate = usart->info->usart_def_cfg.USART_BaudRate;

    switch (usart->info->usart_def_cfg.USART_StopBits)
    {
    case USART_StopBits_1:
        serial->config.stop_bits = STOP_BITS_1;
        break;
    case USART_StopBits_2:
        serial->config.stop_bits = STOP_BITS_2;
        break;
    default:
        LOG_E(DRV_EXT_TAG, "stop bit not support!");
        break;
    }
    switch (usart->info->usart_def_cfg.USART_Parity)
    {
    case USART_Parity_No:
        serial->config.parity = PARITY_NONE;
        break;
    case USART_Parity_Odd:
        serial->config.parity = PARITY_ODD;
        break;
    case USART_Parity_Even:
        serial->config.parity = PARITY_EVEN;
        break;
    }

    switch (usart->info->usart_def_cfg.USART_WordLength)
    {
    case USART_WordLength_8b:
        serial->config.data_bits = DATA_BITS_8;
        break;
    case USART_WordLength_9b:
        serial->config.data_bits = DATA_BITS_9;
        break;
    default:
        LOG_E(DRV_EXT_TAG, "wordLength not support!");
        break;
    }
}

static os_err_t _usart_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    os_ubase_t               level  = 0;
    struct serial_configure config = OS_SERIAL_CONFIG_DEFAULT;

    struct ch32_usart *usart = os_calloc(1, sizeof(struct ch32_usart));
    if (usart == OS_NULL)
    {
        return OS_SUCCESS;
    }

    usart->info          = (struct ch32_usart_info *)dev->info;
    usart->serial.ops    = &_usart_ops;
    usart->serial.config = config;

#ifndef OS_USING_CONSOLE
    _usart_hard_init(usart->info);
#endif

    _usart_parse_config(usart);

    os_spin_lock_irqsave(&gs_device_lock, &level);
    os_list_add_tail(&ch32_usart_list, &usart->list);
    os_spin_unlock_irqrestore(&gs_device_lock, level);

    if (os_hw_serial_register(&usart->serial, dev->name, NULL) != OS_SUCCESS)
    {
        LOG_E(DRV_EXT_TAG, "os_hw_serial_register error!");
    }

    return OS_SUCCESS;
}

OS_DRIVER_INFO _usart_driver = {
    .name  = "USART_TypeDef",
    .probe = _usart_probe,
};

OS_DRIVER_DEFINE(_usart_driver, OS_INIT_LEVEL_PRE_DEVICE, OS_INIT_SUBLEVEL_HIGH);

#ifdef OS_USING_CONSOLE
static USART_TypeDef *console_uart = 0;
/* clang-format off */
void __os_hw_console_output(char *str)
{
    if (console_uart == 0)
        return;
    while (*str)
    {
        while (USART_GetFlagStatus(console_uart, USART_FLAG_TC) == RESET);
        USART_SendData(console_uart, *str++);
    }
}
/* clang-format on */

static os_err_t _usart_early_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    ch32_usart_info_t *info = (ch32_usart_info_t *)dev->info;

    _usart_hard_init(info);

    if (!strcmp(dev->name, OS_CONSOLE_DEVICE_NAME))
    {
        console_uart = (USART_TypeDef *)info->husart;
    }
    return OS_SUCCESS;
}

OS_DRIVER_INFO _usart_early_driver = {
    .name  = "USART_TypeDef",
    .probe = _usart_early_probe,
};

OS_DRIVER_DEFINE(_usart_early_driver, OS_INIT_LEVEL_PRE_KERNEL_1, OS_INIT_SUBLEVEL_LOW);
#endif

