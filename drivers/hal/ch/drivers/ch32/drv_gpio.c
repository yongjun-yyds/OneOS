/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_gpio.c
 *
 * @brief       This file implements gpio driver for CH32.
 *
 * @revision
 * Date         Author          Notes
 * 2022-01-17   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <board.h>
#ifdef OS_USING_PIN

#define DRV_EXT_TAG "drv.gpio"
#include <dlog.h>

#include <driver.h>
#include "drv_gpio.h"
#include <os_types.h>
#define GPIO_ITEM_NUM(items) sizeof(items) / sizeof(items[0])

static struct pin_mode ch32_pin_mode[GPIO_PORT_MAX];

#define GPIO_PORT_INFO_MAP(port, num)                                                                                  \
    {                                                                                                                  \
        GPIO##port, RCC_APB2Periph_GPIO##port, GPIO_PortSourceGPIO##port, &ch32_pin_mode[num]                          \
    }

struct gpio_port_info
{
    GPIO_TypeDef    *gpio_port;
    uint32_t      Periph;
    uint8_t       PortSource;
    struct pin_mode *pin_mode;
};

struct pin_irq_map
{
    uint16_t  pinbit;
    uint32_t  exti_line;
    uint8_t   pinsource;
    IRQn_Type irqno;
};

struct gpio_info
{
    GPIO_TypeDef *gpio_port;
    uint16_t   port_index;
    uint16_t   pin;
    uint16_t   pin_index;
};

struct CH32_pin_info
{
    uint32_t irq_enable_mask;
    uint32_t irq_prigroup;
    uint8_t  irq_PrePri;
    uint8_t  irq_SubPri;

    uint16_t port_info_size;

    void (*PeriphClock)(uint32_t Periph, FunctionalState NewState);
};

static struct gpio_port_info port_info_table[] = {
#ifdef GPIOA
    GPIO_PORT_INFO_MAP(A, 0),
#endif
#ifdef GPIOB
    GPIO_PORT_INFO_MAP(B, 0),
#endif
#ifdef GPIOC
    GPIO_PORT_INFO_MAP(C, 0),
#endif
#ifdef GPIOD
    GPIO_PORT_INFO_MAP(D, 0),
#endif
#ifdef GPIOE
    GPIO_PORT_INFO_MAP(E, 0),
#endif
};

static const struct pin_irq_map pin_irq_map[] = {
    {GPIO_Pin_0, EXTI_Line0, GPIO_PinSource0, EXTI0_IRQn},
    {GPIO_Pin_1, EXTI_Line1, GPIO_PinSource1, EXTI1_IRQn},
    {GPIO_Pin_2, EXTI_Line2, GPIO_PinSource2, EXTI2_IRQn},
    {GPIO_Pin_3, EXTI_Line3, GPIO_PinSource3, EXTI3_IRQn},
    {GPIO_Pin_4, EXTI_Line4, GPIO_PinSource4, EXTI4_IRQn},
    {GPIO_Pin_5, EXTI_Line5, GPIO_PinSource5, EXTI9_5_IRQn},
    {GPIO_Pin_6, EXTI_Line6, GPIO_PinSource6, EXTI9_5_IRQn},
    {GPIO_Pin_7, EXTI_Line7, GPIO_PinSource7, EXTI9_5_IRQn},
    {GPIO_Pin_8, EXTI_Line8, GPIO_PinSource8, EXTI9_5_IRQn},
    {GPIO_Pin_9, EXTI_Line9, GPIO_PinSource9, EXTI9_5_IRQn},
    {GPIO_Pin_10, EXTI_Line10, GPIO_PinSource10, EXTI15_10_IRQn},
    {GPIO_Pin_11, EXTI_Line11, GPIO_PinSource11, EXTI15_10_IRQn},
    {GPIO_Pin_12, EXTI_Line12, GPIO_PinSource12, EXTI15_10_IRQn},
    {GPIO_Pin_13, EXTI_Line13, GPIO_PinSource13, EXTI15_10_IRQn},
    {GPIO_Pin_14, EXTI_Line14, GPIO_PinSource14, EXTI15_10_IRQn},
    {GPIO_Pin_15, EXTI_Line15, GPIO_PinSource15, EXTI15_10_IRQn},
};

static struct os_pin_irq_hdr pin_irq_hdr_tab[] = {
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
};

static struct CH32_pin_info pin_info = {
    .PeriphClock  = RCC_APB2PeriphClockCmd,
    .irq_prigroup = NVIC_PriorityGroup_2,
    .irq_PrePri   = 1,
    .irq_SubPri   = 2,
};

static os_err_t _get_gpio_info(struct gpio_info *pinfo, os_base_t pin)
{
    pinfo->port_index = (uint16_t)((pin >> 4) & 0x0F);
    pinfo->gpio_port  = port_info_table[pinfo->port_index].gpio_port;
    pinfo->pin        = (uint16_t)(1 << (pin & 0x0F));
    pinfo->pin_index  = (uint16_t)(pin & 0x0F);

    return OS_SUCCESS;
}

/* clk init */
static void _gpio_init(void)
{
    uint8_t i = 0;

    pin_info.port_info_size = GPIO_ITEM_NUM(port_info_table);

    for (i = 0; i < pin_info.port_info_size; i++)
    {
        pin_info.PeriphClock(port_info_table[i].Periph, ENABLE);
    }
}

static void _pin_write(os_device_t *dev, os_base_t pin, os_base_t value)
{
    struct gpio_info info = {0};

    if (_get_gpio_info(&info, pin) == OS_SUCCESS)
    {
        GPIO_WriteBit(info.gpio_port, info.pin, (uint8_t)value);
    }
}

static int _pin_read(os_device_t *dev, os_base_t pin)
{
    int value = PIN_LOW;

    struct gpio_info info = {0};

    if (_get_gpio_info(&info, pin) == OS_SUCCESS)
    {

        if (port_info_table[info.port_index].pin_mode->pin_modes[info.pin_index] & 0x10)    // output or af
        {
            value = (int)GPIO_ReadOutputDataBit(info.gpio_port, info.pin);
        }
        else
        {
            value = (int)GPIO_ReadInputDataBit(info.gpio_port, info.pin);
        }
    }

    return value;
}

static os_err_t _pin_detach_irq(struct os_device *device, int32_t pin);
static void     _pin_mode(os_device_t *dev, os_base_t pin, os_base_t mode)
{

    GPIO_InitTypeDef GPIO_InitStruct = {0};

    struct gpio_info info = {0};

    if (_get_gpio_info(&info, pin) != OS_SUCCESS)
    {
        return;
    }

    /* Configure GPIO_InitStructure */
    GPIO_InitStruct.GPIO_Pin   = info.pin;
    GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStruct.GPIO_Mode  = GPIO_Mode_Out_PP;

    if (mode == PIN_MODE_OUTPUT)
    {
        /* output setting */
        GPIO_InitStruct.GPIO_Mode = GPIO_Mode_Out_PP;
    }
    else if (mode == PIN_MODE_INPUT)
    {
        /* input setting: not pull. */
        GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    }
    else if (mode == PIN_MODE_INPUT_PULLUP)
    {
        /* input setting: pull up. */
        GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IPU;
    }
    else if (mode == PIN_MODE_INPUT_PULLDOWN)
    {
        /* input setting: pull down. */
        GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IPD;
    }
    else if (mode == PIN_MODE_OUTPUT_OD)
    {
        /* output setting: od. */
        GPIO_InitStruct.GPIO_Mode = GPIO_Mode_Out_OD;
    }
    else if (mode == PIN_MODE_DISABLE)
    {
        os_pin_irq_enable(pin, PIN_IRQ_DISABLE);
        if (OS_SUCCESS != _pin_detach_irq(dev, pin))
        {
            return;
        }
    }

    /* remeber the mode state. */
    GPIO_Init(info.gpio_port, &GPIO_InitStruct);
    port_info_table[info.port_index].pin_mode->pin_modes[info.pin_index] = GPIO_InitStruct.GPIO_Mode;
}

static os_err_t
_pin_attach_irq(struct os_device *device, int32_t pin, uint32_t mode, void (*hdr)(void *args), void *args)
{
    os_ubase_t level;

    struct gpio_info info = {0};

    if (_get_gpio_info(&info, pin) != OS_SUCCESS)
    {
        return OS_FAILURE;
    }

    os_spin_lock_irqsave(&gs_device_lock, &level);
    if (pin_irq_hdr_tab[info.pin_index].pin == pin && pin_irq_hdr_tab[info.pin_index].hdr == hdr &&
        pin_irq_hdr_tab[info.pin_index].mode == mode && pin_irq_hdr_tab[info.pin_index].args == args)
    {
        os_spin_unlock_irqrestore(&gs_device_lock, level);
        return OS_SUCCESS;
    }
    if (pin_irq_hdr_tab[info.pin_index].pin != -1)
    {
        os_spin_unlock_irqrestore(&gs_device_lock, level);
        return OS_BUSY;
    }
    pin_irq_hdr_tab[info.pin_index].pin  = pin;
    pin_irq_hdr_tab[info.pin_index].hdr  = hdr;
    pin_irq_hdr_tab[info.pin_index].mode = mode;
    pin_irq_hdr_tab[info.pin_index].args = args;
    os_spin_unlock_irqrestore(&gs_device_lock, level);

    return OS_SUCCESS;
}

static os_err_t _pin_detach_irq(struct os_device *device, int32_t pin)
{
    os_ubase_t level;

    struct gpio_info info = {0};

    if (_get_gpio_info(&info, pin) != OS_SUCCESS)
    {
        return OS_FAILURE;
    }

    os_spin_lock_irqsave(&gs_device_lock, &level);
    if (pin_irq_hdr_tab[info.pin_index].pin == -1)
    {
        os_spin_unlock_irqrestore(&gs_device_lock, level);
        return OS_SUCCESS;
    }
    pin_irq_hdr_tab[info.pin_index].pin  = -1;
    pin_irq_hdr_tab[info.pin_index].hdr  = OS_NULL;
    pin_irq_hdr_tab[info.pin_index].mode = 0;
    pin_irq_hdr_tab[info.pin_index].args = OS_NULL;
    os_spin_unlock_irqrestore(&gs_device_lock, level);

    return OS_SUCCESS;
}

static os_err_t _pin_irq_enable(struct os_device *device, os_base_t pin, uint32_t enabled)
{
    os_ubase_t                 level;
    const struct pin_irq_map *irqmap = OS_NULL;

    EXTI_InitTypeDef EXTI_InitStructure = {0};
    NVIC_InitTypeDef NVIC_InitStructure = {0};

    struct gpio_info info = {0};

    if (_get_gpio_info(&info, pin) != OS_SUCCESS)
    {
        return OS_FAILURE;
    }

    irqmap = &pin_irq_map[info.pin_index];
    os_spin_lock_irqsave(&gs_device_lock, &level);

    if (enabled == PIN_IRQ_ENABLE)
    {
        if (pin_irq_hdr_tab[info.pin_index].pin == -1)
        {
            os_spin_unlock_irqrestore(&gs_device_lock, level);
            return OS_NOSYS;
        }

        /* GPIOA ----> EXTI_Line0 */
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
        // EXTI_ClearFlag(irqmap->exti_line);
        GPIO_EXTILineConfig(port_info_table[info.port_index].PortSource, irqmap->pinsource);
        NVIC_PriorityGroupConfig(pin_info.irq_prigroup);

        switch (pin_irq_hdr_tab[info.pin_index].mode)
        {
        case PIN_IRQ_MODE_RISING:
            EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
            break;
        case PIN_IRQ_MODE_FALLING:
            EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
            break;
        case PIN_IRQ_MODE_RISING_FALLING:
            EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;
            break;
        default:
            LOG_E(DRV_EXT_TAG, "not support pin mode!");
            return OS_FAILURE;
        }

        EXTI_InitStructure.EXTI_Line    = irqmap->exti_line;
        EXTI_InitStructure.EXTI_Mode    = EXTI_Mode_Interrupt;
        EXTI_InitStructure.EXTI_LineCmd = ENABLE;
        EXTI_Init(&EXTI_InitStructure);

        NVIC_InitStructure.NVIC_IRQChannel                   = irqmap->irqno;
        NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = pin_info.irq_PrePri;
        NVIC_InitStructure.NVIC_IRQChannelSubPriority        = pin_info.irq_SubPri;
        NVIC_InitStructure.NVIC_IRQChannelCmd                = ENABLE;
        NVIC_Init(&NVIC_InitStructure);

        pin_info.irq_enable_mask |= irqmap->pinbit;
    }
    else if (enabled == PIN_IRQ_DISABLE)
    {
        pin_info.irq_enable_mask &= ~irqmap->pinbit;

        EXTI_InitStructure.EXTI_Line    = irqmap->exti_line;
        EXTI_InitStructure.EXTI_Mode    = EXTI_Mode_Interrupt;
        EXTI_InitStructure.EXTI_LineCmd = DISABLE;
        EXTI_Init(&EXTI_InitStructure);

        NVIC_InitStructure.NVIC_IRQChannel = irqmap->irqno;

        if ((irqmap->pinbit >= GPIO_Pin_5) && (irqmap->pinbit <= GPIO_Pin_9))
        {
            if (!(pin_info.irq_enable_mask & (GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9)))
            {
                NVIC_DisableIRQ(NVIC_InitStructure.NVIC_IRQChannel);
            }
        }
        else if ((irqmap->pinbit >= GPIO_Pin_10) && (irqmap->pinbit <= GPIO_Pin_15))
        {
            if (!(pin_info.irq_enable_mask &
                  (GPIO_Pin_10 | GPIO_Pin_11 | GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15)))
            {
                NVIC_DisableIRQ(NVIC_InitStructure.NVIC_IRQChannel);
            }
        }
        else
        {
            NVIC_DisableIRQ(NVIC_InitStructure.NVIC_IRQChannel);
        }
    }

    os_spin_unlock_irqrestore(&gs_device_lock, level);

    return OS_SUCCESS;
}

void EXTI0_IRQHandler(void) __attribute__((interrupt()));
void EXTI1_IRQHandler(void) __attribute__((interrupt()));
void EXTI2_IRQHandler(void) __attribute__((interrupt()));
void EXTI3_IRQHandler(void) __attribute__((interrupt()));
void EXTI4_IRQHandler(void) __attribute__((interrupt()));
void EXTI9_5_IRQHandler(void) __attribute__((interrupt()));
void EXTI15_10_IRQHandler(void) __attribute__((interrupt()));

void GPIO_EXTI_IRQHandler(uint32_t line, uint16_t index)
{
    if (EXTI_GetITStatus(line) != RESET)
    {
        EXTI_ClearITPendingBit(line);
    }

    if (pin_irq_hdr_tab[index].hdr)
    {
        pin_irq_hdr_tab[index].hdr(pin_irq_hdr_tab[index].args);
    }
}

#define EXTI_IRQ_HANDLER(index) GPIO_EXTI_IRQHandler(EXTI_Line##index, index)

void EXTI0_IRQHandler(void)
{
    EXTI_IRQ_HANDLER(0);
}
void EXTI1_IRQHandler(void)
{
    EXTI_IRQ_HANDLER(1);
}
void EXTI2_IRQHandler(void)
{
    EXTI_IRQ_HANDLER(2);
}
void EXTI3_IRQHandler(void)
{
    EXTI_IRQ_HANDLER(3);
}
void EXTI4_IRQHandler(void)
{
    EXTI_IRQ_HANDLER(4);
}
void EXTI9_5_IRQHandler(void)
{
    EXTI_IRQ_HANDLER(5);
    EXTI_IRQ_HANDLER(6);
    EXTI_IRQ_HANDLER(7);
    EXTI_IRQ_HANDLER(8);
    EXTI_IRQ_HANDLER(9);
}
void EXTI15_10_IRQHandler(void)
{
    EXTI_IRQ_HANDLER(10);
    EXTI_IRQ_HANDLER(11);
    EXTI_IRQ_HANDLER(12);
    EXTI_IRQ_HANDLER(13);
    EXTI_IRQ_HANDLER(14);
    EXTI_IRQ_HANDLER(15);
}

const static struct os_pin_ops CH32_pin_ops = {
    .pin_mode       = _pin_mode,
    .pin_write      = _pin_write,
    .pin_read       = _pin_read,
    .pin_attach_irq = _pin_attach_irq,
    .pin_detach_irq = _pin_detach_irq,
    .pin_irq_enable = _pin_irq_enable,
};

int os_hw_pin_init(void)
{
    _gpio_init();

    return os_device_pin_register(0, &CH32_pin_ops, OS_NULL);
}

#endif
