/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_gpio.h
 *
 * @brief       This file provides struct/macro declaration and functions declaration for CH32 gpio driver.
 *
 * @revision
 * Date         Author          Notes
 * 2022-01-10   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __DRV_GPIO_H__
#define __DRV_GPIO_H__
#include <os_types.h>
#ifdef SERIES_CH32V30X
#include "ch32v30x.h"
#else
#include "ch32v10x.h"
#endif

#define GPIO_PIN_PER_PORT (16)
#define GPIO_PORT_MAX     (7)
#define GPIO_PIN_MAX      (GPIO_PORT_MAX * GPIO_PIN_PER_PORT)

#define __PORT_INDEX(pin) (pin / GPIO_PIN_PER_PORT)
#define __PIN_INDEX(pin)  (pin % GPIO_PIN_PER_PORT)

#define PIN_BASE(__pin)   get_pin_base(__pin)
#define PIN_OFFSET(__pin) (1 << ((__pin) % GPIO_PIN_PER_PORT))

#define __CH32_PORT(port) GPIO##port##_BASE
#define GET_PIN(PORTx, PIN)                                                                                            \
    (os_base_t)((16 * (((os_base_t)__CH32_PORT(PORTx) - (os_base_t)GPIOA_BASE) / (0x0400UL))) + PIN)

struct pin_mode
{
    uint8_t pin_modes[GPIO_PIN_PER_PORT];
};

// void GPIO_EXTI_IRQHandler(EINT_LINE_T line, uint16_t index);
int os_hw_pin_init(void);

#endif /* __DRV_GPIO_H__ */
