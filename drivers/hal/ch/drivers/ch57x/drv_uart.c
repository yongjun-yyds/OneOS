/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_uart.c
 *
 * @brief       This file implements uart driver for apm32
 *
 * @revision
 * Date         Author          Notes
 * 2022-01-10   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include <os_memory.h>
#include <bus/bus.h>
#include <dma/dma.h>
#include <board.h>
#include <string.h>
#include <os_list.h>

#define DRV_EXT_TAG "drv.uart"
#include <dlog.h>

#include "drv_uart.h"

void UART_BaudRate_Cfg(UART_TypeDef *uart, uint32_t baudrate)
{
    uint32_t x;

    x         = 10 * GetSysClock() / 8 / baudrate;
    x         = (x + 5) / 10;
    uart->DL  = (uint16_t)x;
    uart->DIV = 1;
}

void UART_Init(UART_TypeDef *uart, UART_InitTypeDef *UART_InitStruct)
{
    uart->IER |= RB_IER_RESET;    // uart software reset
    if (uart == UART0 && UART_InitStruct->UART_HardwareFlowControl != 0)
    {
        uart->MCR |= RB_MCR_AU_FLOW_EN;
    }

    uart->LCR |= UART_InitStruct->UART_StopBits;

    uart->LCR |= UART_InitStruct->UART_WordLength;

    if (UART_InitStruct->UART_Parity != 0)
    {
        uart->LCR |= UART_InitStruct->UART_Parity;
    }

    UART_BaudRate_Cfg(uart, UART_InitStruct->UART_BaudRate);
    uart->FCR = RB_FCR_TX_FIFO_CLR | RB_FCR_RX_FIFO_CLR | RB_FCR_FIFO_EN;
    uart->IER = RB_IER_TXD_EN;    // enable uart_tx pin
}

void UART_irq_Init(UART_TypeDef *uart, UART_InitTypeDef *UART_InitStruct, FunctionalState s)
{
    if (s)
    {
        if (UART_InitStruct->UART_Rx_FIFO != 0)
        {
            uart->FCR = (uart->FCR & ~RB_FCR_FIFO_TRIG) | UART_InitStruct->UART_Rx_FIFO_IrqNum;
        }

        uart->IER |= UART_InitStruct->UART_irq_mode;
        uart->MCR |= RB_MCR_INT_OE;

        PFIC_EnableIRQ(UART_InitStruct->UART_irq);
    }
    else
    {
        uart->IER &= ~UART_InitStruct->UART_irq_mode;
        PFIC_DisableIRQ(UART_InitStruct->UART_irq);
    }
}

static os_list_node_t ch_uart_list = OS_LIST_INIT(ch_uart_list);

static void _uart_hard_init(ch_uart_info_t *info)
{
    GPIO_TypeDef *rx_port;
    GPIO_TypeDef *tx_port;
    rx_port = info->rx_port;
    tx_port = info->tx_port;

    tx_port->OUT |= info->tx_pin;    // pull up tx pin
    if (GPIOA == rx_port)
    {
        GPIOA_ModeCfg(info->rx_pin, GPIO_ModeIN_PU);
    }
    else
    {
        GPIOB_ModeCfg(info->rx_pin, GPIO_ModeIN_PU);
    }
    if (GPIOA == tx_port)
    {
        GPIOA_ModeCfg(info->tx_pin, GPIO_ModeOut_PP_5mA);
    }
    else
    {
        GPIOB_ModeCfg(info->tx_pin, GPIO_ModeOut_PP_5mA);
    }

    UART_Init(info->huart, &info->uart_def_cfg);
}

static void _uart_irq_callback(UART_TypeDef *huart)
{
    struct ch_uart *uart = OS_NULL;

    os_list_for_each_entry(uart, &ch_uart_list, struct ch_uart, list)
    {
        if (uart->info->huart == huart)
        {
            switch (huart->IIR & RB_IIR_INT_MASK)
            {
            case UART_II_LINE_STAT:
                os_kprintf("line error: IIR:0x%x,LSR:0x%x\r\n", huart->IIR, huart->LSR);
                break;
            case UART_II_RECV_RDY:    // read all data
                while (huart->RFC)
                {
                    uart->rx_buff[uart->rx_index] = (uint8_t)huart->R_TBR;
                    uart->rx_index++;
                }
                /*while(read_num)
                {
                    uart->rx_buff[uart->rx_index] = (uint8_t)huart->R_TBR;
                    uart->rx_index++;
                    read_num--;
                }*/
                if (uart->rx_index == (uart->rx_size / 2))
                {
                    soft_dma_half_irq(&uart->sdma);
                }

                if (uart->rx_index == uart->rx_size)
                {
                    soft_dma_full_irq(&uart->sdma);
                }
                break;
            case UART_II_RECV_TOUT:    // rx_timeout,read all data
                while (huart->RFC)
                {
                    uart->rx_buff[uart->rx_index] = (uint8_t)huart->R_TBR;
                    uart->rx_index++;
                }
                /* soft dma idle irq */
                // soft_dma_timeout_irq(&uart->sdma);
                break;
            }
        }
    }
}

void UART0_IRQHandler(void) __attribute__((interrupt()));
void UART1_IRQHandler(void) __attribute__((interrupt()));
void UART2_IRQHandler(void) __attribute__((interrupt()));
void UART3_IRQHandler(void) __attribute__((interrupt()));

void UART0_IRQHandler(void)
{
    TOGGLE_SP();
    _uart_irq_callback(UART0);
    TOGGLE_SP();
}

void UART1_IRQHandler(void)
{
    TOGGLE_SP();
    _uart_irq_callback(UART1);
    TOGGLE_SP();
}

void UART2_IRQHandler(void)
{
    TOGGLE_SP();
    _uart_irq_callback(UART2);
    TOGGLE_SP();
}

void UART3_IRQHandler(void)
{
    TOGGLE_SP();
    _uart_irq_callback(UART3);
    TOGGLE_SP();
}

static uint32_t _sdma_int_get_index(soft_dma_t *dma)
{
    ch_uart_t *uart = os_container_of(dma, ch_uart_t, sdma);

    return uart->rx_index;
}

static os_err_t _sdma_int_start(soft_dma_t *dma, void *buff, uint32_t size)
{
    ch_uart_t *uart = os_container_of(dma, ch_uart_t, sdma);

    uart->rx_buff  = buff;
    uart->rx_index = 0;
    uart->rx_size  = size;
    UART_irq_Init(uart->info->huart, &uart->info->uart_def_cfg, ENABLE);

    return OS_SUCCESS;
}

static uint32_t _sdma_int_stop(soft_dma_t *dma)
{
    ch_uart_t *uart = os_container_of(dma, ch_uart_t, sdma);

    UART_irq_Init(uart->info->huart, &uart->info->uart_def_cfg, DISABLE);

    return uart->rx_index;
}

/* sdma callback */
static void _uart_sdma_callback(soft_dma_t *dma)
{
    ch_uart_t *uart = os_container_of(dma, ch_uart_t, sdma);

    os_hw_serial_isr_rxdone((struct os_serial_device *)uart);
}

static void _uart_sdma_init(struct ch_uart *uart, dma_ring_t *ring)
{
    soft_dma_t *dma = &uart->sdma;

    soft_dma_stop(dma);

    memset(&dma->hard_info, 0, sizeof(dma->hard_info));

    dma->hard_info.max_size = 64 * 1024;

    dma->hard_info.flag         = HARD_DMA_FLAG_HALF_IRQ | HARD_DMA_FLAG_FULL_IRQ;    // | HARD_DMA_FLAG_TIMEOUT_IRQ;
    dma->hard_info.data_timeout = uart_calc_byte_timeout_us(uart->serial.config.baud_rate);

    dma->hard_info.mode = HARD_DMA_MODE_NORMAL;
    dma->ops.get_index  = _sdma_int_get_index;
    dma->ops.dma_init   = OS_NULL;
    dma->ops.dma_start  = _sdma_int_start;
    dma->ops.dma_stop   = _sdma_int_stop;

    dma->cbs.dma_half_callback    = _uart_sdma_callback;
    dma->cbs.dma_full_callback    = _uart_sdma_callback;
    dma->cbs.dma_timeout_callback = _uart_sdma_callback;

    soft_dma_init(dma);
    soft_dma_start(dma, ring);
    soft_dma_irq_enable(dma, OS_TRUE);
}

static os_err_t _uart_init(struct os_serial_device *serial, struct serial_configure *cfg)
{
    UART_InitTypeDef uart_cfg = {0};

    struct ch_uart *uart = (struct ch_uart *)serial;

    uart_cfg.UART_HardwareFlowControl = uart->info->uart_def_cfg.UART_HardwareFlowControl;
    uart_cfg.UART_BaudRate            = cfg->baud_rate;

    switch (cfg->stop_bits)
    {
    case STOP_BITS_1:
        uart_cfg.UART_StopBits = CH_STOP_BITS_1;
        break;
    case STOP_BITS_2:
        uart_cfg.UART_StopBits = CH_STOP_BITS_2;
        break;
    default:
        uart_cfg.UART_StopBits = CH_STOP_BITS_1;
        break;
    }
    switch (cfg->parity)
    {
    case PARITY_NONE:
        uart_cfg.UART_Parity = CH_PARITY_NONE;
        break;
    case PARITY_ODD:
        uart_cfg.UART_Parity = CH_PARITY_ODD;
        break;
    case PARITY_EVEN:
        uart_cfg.UART_Parity = CH_PARITY_EVEN;
        break;
    }

    switch (cfg->data_bits)
    {
    case DATA_BITS_8:
        uart_cfg.UART_WordLength = CH_DATA_BITS_8;
        break;
    case DATA_BITS_7:
        uart_cfg.UART_WordLength = CH_DATA_BITS_7;
        break;
    case DATA_BITS_6:
        uart_cfg.UART_WordLength = CH_DATA_BITS_6;
        break;
    case DATA_BITS_5:
        uart_cfg.UART_WordLength = CH_DATA_BITS_5;
        break;

    default:
        LOG_E(DRV_EXT_TAG, "wordLength not support!");
        break;
    }

    UART_Init(uart->info->huart, &uart_cfg);
    if (uart->info->uart_def_cfg.UART_irq_mode != 0)
        UART_irq_Init(uart->info->huart, &uart->info->uart_def_cfg, ENABLE);
    _uart_sdma_init(uart, &uart->serial.rx_fifo->ring);

    return OS_SUCCESS;
}

static os_err_t _uart_deinit(struct os_serial_device *serial)
{
    struct ch_uart *uart  = (struct ch_uart *)serial;
    UART_TypeDef   *huart = uart->info->huart;

    soft_dma_stop(&uart->sdma);

    huart->IER |= RB_IER_RESET;    // uart software reset
    return OS_SUCCESS;
}

static int _uart_poll_send(struct os_serial_device *serial, const uint8_t *buff, os_size_t size)
{
    int send_num = size;

    struct ch_uart *uart  = (struct ch_uart *)serial;
    UART_TypeDef   *huart = uart->info->huart;
    while (send_num)
    {
        if (huart->TFC != UART_FIFO_SIZE)
        {
            huart->R_TBR = *buff++;
            send_num--;
            //__nop();
        }
    }
    return size;
}

static const struct os_uart_ops _uart_ops = {
    .init       = _uart_init,
    .deinit     = _uart_deinit,
    .start_send = OS_NULL,
    .poll_send  = _uart_poll_send,
};

static void _uart_parse_config(struct ch_uart *uart)
{
    struct os_serial_device *serial = &uart->serial;

    serial->config.baud_rate = uart->info->uart_def_cfg.UART_BaudRate;

    switch (uart->info->uart_def_cfg.UART_StopBits)
    {
    case CH_STOP_BITS_1:
        serial->config.stop_bits = STOP_BITS_1;
        break;
    case CH_STOP_BITS_2:
        serial->config.stop_bits = STOP_BITS_2;
        break;
    default:
        LOG_E(DRV_EXT_TAG, "stop bit not support!");
        break;
    }
    switch (uart->info->uart_def_cfg.UART_Parity)
    {
    case CH_PARITY_NONE:
        serial->config.parity = PARITY_NONE;
        break;
    case CH_PARITY_ODD:
        serial->config.parity = PARITY_ODD;
        break;
    case CH_PARITY_EVEN:
        serial->config.parity = PARITY_EVEN;
        break;
    }

    switch (uart->info->uart_def_cfg.UART_WordLength)
    {
    case CH_DATA_BITS_8:
        serial->config.data_bits = DATA_BITS_8;
        break;
    case CH_DATA_BITS_7:
        serial->config.data_bits = DATA_BITS_7;
        break;
    case CH_DATA_BITS_6:
        serial->config.data_bits = DATA_BITS_6;
        break;
    case CH_DATA_BITS_5:
        serial->config.data_bits = DATA_BITS_5;
        break;

    default:
        LOG_E(DRV_EXT_TAG, "wordLength not support!");
        break;
    }
}

static os_err_t _uart_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    os_ubase_t               level  = 0;
    struct serial_configure config = OS_SERIAL_CONFIG_DEFAULT;

    struct ch_uart *uart = os_calloc(1, sizeof(struct ch_uart));
    if (uart == OS_NULL)
    {
        return OS_SUCCESS;
    }

    uart->info          = (struct ch_uart_info *)dev->info;
    uart->serial.ops    = &_uart_ops;
    uart->serial.config = config;

#ifndef OS_USING_CONSOLE
    _uart_hard_init(uart->info);
#endif

    _uart_parse_config(uart);

    os_spin_lock_irqsave(&gs_device_lock, &level);
    os_list_add_tail(&ch_uart_list, &uart->list);
    os_spin_unlock_irqrestore(&gs_device_lock, level);

    if (os_hw_serial_register(&uart->serial, dev->name, NULL) != OS_SUCCESS)
    {
        LOG_E(DRV_EXT_TAG, "os_hw_serial_register error!");
    }

    return OS_SUCCESS;
}

OS_DRIVER_INFO _uart_driver = {
    .name  = "UART_TypeDef",
    .probe = _uart_probe,
};

OS_DRIVER_DEFINE(_uart_driver, OS_INIT_LEVEL_PRE_DEVICE, OS_INIT_SUBLEVEL_HIGH);

#ifdef OS_USING_CONSOLE
static UART_TypeDef *console_uart = 0;

void __os_hw_console_output(char *str)
{
    if (console_uart == 0)
        return;

    while (*str)
    {
        if (console_uart->TFC != UART_FIFO_SIZE)
        {
            console_uart->R_TBR = *str++;
        }
    }
}

static os_err_t _uart_early_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    ch_uart_info_t *info = (ch_uart_info_t *)dev->info;

    _uart_hard_init(info);

    if (!strcmp(dev->name, OS_CONSOLE_DEVICE_NAME))
    {
        console_uart = (UART_TypeDef *)info->huart;
    }
    return OS_SUCCESS;
}

OS_DRIVER_INFO _uart_early_driver = {
    .name  = "UART_TypeDef",
    .probe = _uart_early_probe,
};

OS_DRIVER_DEFINE(_uart_early_driver, OS_INIT_LEVEL_PRE_KERNEL_1, OS_INIT_SUBLEVEL_LOW);
#endif

