/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_hwtimer.c
 *
 * @brief       This file implements timer driver for mm32.
 *
 * @revision
 * Date         Author          Notes
 * 2021-05-31   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include <drv_common.h>
#include <drv_hwtimer.h>
#include <os_memory.h>
#include <timer/timer.h>
#include <core_riscv.h>
#ifdef OS_USING_CLOCKSOURCE
#include <timer/clocksource.h>
#endif

#ifdef OS_USING_CLOCKEVENT
#include <timer/clockevent.h>
#endif

#define DRV_EXT_TAG "drv.hwtimer"
#include <drv_log.h>

static os_list_node_t ch_timer_list = OS_LIST_INIT(ch_timer_list);

uint32_t TIM_GetCounter(TIM_TypeDef *TIMx)
{
    return TIMx->TIM_CNT;
}

void TIM_ITConfig(TIM_TypeDef *TIMx, uint8_t TIM_IT, FunctionalState NewState)
{
    if (NewState != DISABLE)
    {
        TIMx->INT_EN |= TIM_IT;
    }
    else
    {
        TIMx->INT_EN &= (uint8_t)~TIM_IT;
    }
}

uint8_t TIM_GetITStatus(TIM_TypeDef *TIMx, uint8_t TIM_IT)
{
    return (TIMx->INT_FLAG & TIM_IT);
}

void TIM_ClearITPendingBit(TIM_TypeDef *TIMx, uint8_t TIM_IT)
{
    TIMx->INT_FLAG = TIM_IT;
}

static uint32_t ch_timer_get_freq(struct ch_timer *timer)
{
    uint32_t clk = 0;

    OS_ASSERT(timer->info->htim != OS_NULL);

    clk = GetSysClock();

    return clk;
}

/* clang-format off */
void TMR0_IRQHandler(void) __attribute__((interrupt()));
void TMR1_IRQHandler(void) __attribute__((interrupt()));
void TMR2_IRQHandler(void) __attribute__((interrupt()));
void TMR3_IRQHandler(void) __attribute__((interrupt()));
/* clang-format on */

void TIM_IRQ_Callback(TIM_TypeDef *htim)
{
    struct ch_timer *timer;
    os_list_for_each_entry(timer, &ch_timer_list, struct ch_timer, list)
    {
        if (timer->info->htim == htim)
        {
            if (TIM_GetITStatus(timer->info->htim, TMR0_3_IT_CYC_END))
            {
                TIM_ClearITPendingBit(timer->info->htim, TMR0_3_IT_CYC_END);
#ifdef OS_USING_CLOCKEVENT
                // if (timer->mode == TIMER_MODE_TIM)
                os_clockevent_isr((os_clockevent_t *)timer);
#endif
            }
            break;
        }
    }
}

#define TIM_IRQ_CALLBACK_FUNC(tim)                                                                                     \
    void TMR##tim##_IRQHandler(void)                                                                                   \
    {                                                                                                                  \
        TOGGLE_SP();                                                                                                   \
        TIM_IRQ_Callback(TIM##tim);                                                                                    \
        TOGGLE_SP();                                                                                                   \
    }

// gpio_irq_callback function
TIM_IRQ_CALLBACK_FUNC(0);
TIM_IRQ_CALLBACK_FUNC(1);
TIM_IRQ_CALLBACK_FUNC(2);
TIM_IRQ_CALLBACK_FUNC(3);

#if defined(OS_USING_CLOCKSOURCE) || defined(OS_USING_CLOCKEVENT)
static uint64_t ch_timer_read(void *clock)
{
    struct ch_timer *timer;

    timer = (struct ch_timer *)clock;

    return TIM_GetCounter(timer->info->htim);
}
#endif

#ifdef OS_USING_CLOCKEVENT
static void ch_timer_start(os_clockevent_t *ce, uint32_t prescaler, uint64_t count)
{
    struct ch_timer *timer;

    OS_ASSERT(prescaler != 0);
    OS_ASSERT(count != 0);

    timer = (struct ch_timer *)ce;

    TIM_TypeDef *htim = timer->info->htim;

    htim->CTR_MOD = RB_TMR_ALL_CLEAR;
    htim->CNT_END = (uint32_t)count;

    TIM_ITConfig(htim, TMR0_3_IT_CYC_END, ENABLE);
    htim->CTR_MOD = RB_TMR_COUNT_EN;
}

static void ch_timer_stop(os_clockevent_t *ce)
{
    struct ch_timer *timer;

    OS_ASSERT(ce != OS_NULL);

    timer = (struct ch_timer *)ce;

    TIM_TypeDef *htim = timer->info->htim;

    TIM_ITConfig(htim, TMR0_3_IT_CYC_END, DISABLE);
    htim->CTR_MOD &= ~RB_TMR_COUNT_EN;
    // TIM_ClearITPendingBit(htim, TMR0_3_IT_CYC_END);
}

static const struct os_clockevent_ops ch_tim_ops = {
    .start = ch_timer_start,
    .stop  = ch_timer_stop,
    .read  = ch_timer_read,
};
#endif

static int ch_tim_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    struct ch_timer *timer;
    TIM_TypeDef     *htim;

    timer = os_calloc(1, sizeof(struct ch_timer));
    OS_ASSERT(timer);

    timer->info = (struct ch_timer_info *)dev->info;

    timer->freq = ch_timer_get_freq(timer);

    htim = timer->info->htim;

#ifdef OS_USING_CLOCKSOURCE
    if (os_clocksource_best() == OS_NULL)
    {
        htim->CTR_MOD = RB_TMR_ALL_CLEAR;
        htim->CNT_END = 0x3fffffful;
        htim->CTR_MOD = RB_TMR_COUNT_EN;

        timer->clock.cs.rating = 260;
        timer->clock.cs.freq   = timer->freq;
        timer->clock.cs.mask   = 0x3fffffful;
        timer->clock.cs.read   = ch_timer_read;

        os_clocksource_register(dev->name, &timer->clock.cs);
    }
    else
#endif
    {
#ifdef OS_USING_CLOCKEVENT
        PFIC_EnableIRQ(timer->info->irq_num);

        timer->clock.ce.rating = 260;
        timer->clock.ce.freq   = timer->freq;
        timer->clock.ce.mask   = 0x3fffffful;

        timer->clock.ce.prescaler_mask = 1;
        timer->clock.ce.prescaler_bits = 0;

        timer->clock.ce.count_mask = 0x3fffffful;
        timer->clock.ce.count_bits = 26;

        timer->clock.ce.feature = OS_CLOCKEVENT_FEATURE_PERIOD;

        timer->clock.ce.min_nsec = NSEC_PER_SEC / timer->clock.ce.freq;

        timer->clock.ce.ops = &ch_tim_ops;
        os_clockevent_register(dev->name, &timer->clock.ce);
#endif
    }

    os_list_add(&ch_timer_list, &timer->list);

    return OS_SUCCESS;
}

OS_DRIVER_INFO ch_tim_driver = {
    .name  = "TIM_TypeDef",
    .probe = ch_tim_probe,
};

OS_DRIVER_DEFINE(ch_tim_driver, OS_INIT_LEVEL_PRE_DEVICE, OS_INIT_SUBLEVEL_MIDDLE);

