/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_common.c
 *
 * @brief       This file provides systick time init/IRQ and board init functions.
 *
 * @revision
 * Date         Author          Notes
 * 2022-01-10   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include <string.h>
#include <os_types.h>
#include <os_stddef.h>

#include <arch_interrupt.h>
#include <os_clock.h>
#include <os_memory.h>
#include <oneos_config.h>

#include <drv_common.h>
#include <board.h>
#include <drv_gpio.h>

#ifdef OS_USING_DMA
#include <dma.h>
#endif

#ifdef OS_USING_CLOCKSOURCE
#include <timer/clocksource.h>
#include <timer/clocksource_cortexm.h>
#endif

static uint32_t SystemCoreClock;

void os_tick_handler(void)
{
    os_tick_increase();
#ifdef OS_USING_CLOCKSOURCE
    os_clocksource_update();
#endif
}

#ifdef SERIES_CH57X
#ifdef OS_USING_SYSTICK_FOR_CLOCKSOURCE
static uint64_t riscv_systick_read(void *clock)
{
    return (0xffffffffffffffffull - SysTick->CNT);
}

static os_clocksource_t riscv_systick_clocksource;

void riscv_systick_clocksource_init(void)
{
    SysTick->CTLR  = 0;
    SysTick->CNTFG = 0;
    SysTick->CNT   = 0;
    SysTick->CMP   = 0xffffffffffffffffull;
    SysTick->CTLR  = 0x105;

    riscv_systick_clocksource.rating = 640;
    riscv_systick_clocksource.freq   = SystemCoreClock;
    riscv_systick_clocksource.mask   = 0xffffffffffffffffull;
    riscv_systick_clocksource.read   = riscv_systick_read;

    os_clocksource_register("systick", &riscv_systick_clocksource);
}
#else

static uint32_t _SysTick_Config(uint64_t ticks)
{
    if ((ticks - 1) > SysTick_LOAD_RELOAD_Msk)
        return 1; /* Reload value impossible */

    SysTick->CMP = ticks - 1; /* set reload register */
    PFIC_EnableIRQ(SysTick_IRQn);
    PFIC_SetPriority(SysTick_IRQn, 0xf0);
    /* Enable SysTick IRQ and SysTick Timer */
    SysTick->CTLR =
        SysTick_CTRL_RELOAD_Msk | SysTick_CTRL_CLKSOURCE_Msk | SysTick_CTRL_TICKINT_Msk | SysTick_CTRL_ENABLE_Msk;
    return (0);
}

/* clang-format off */
__attribute__((interrupt())) 
void SysTick_Handler(void)
{
    TOGGLE_SP();
    SysTick->CNTFG = 0;
    PFIC_ClearPendingIRQ(SysTick_IRQn);
    os_tick_handler();
    TOGGLE_SP();
}
/* clang-format on */
#endif /* OS_USING_SYSTICK_FOR_CLOCKSOURCE */

#endif

void hardware_init(void)
{
    PFIC->CFGR = PFIC_KEY1 | 0x3;

    SetSysClock(CLK_SOURCE_PLL_60MHz);
    PFIC_EnableIRQ(SWI_IRQn);
    PFIC_SetPriority(SWI_IRQn, 0xf0);
    SystemCoreClock = GetSysClock();

    // _SysTick_Config(SystemCoreClock/ 10);
}

void os_hw_cpu_reset(void)
{
    SYS_ResetExecute();
}

/**
 ***********************************************************************************************************************
 * @brief           This function will initial STM32 board.
 *
 * @param[in]       none
 *
 * @return          none
 ***********************************************************************************************************************
 */
static os_err_t os_hw_board_init(void)
{
    hardware_init();

    /* Heap initialization */

#ifdef OS_USING_HEAP
    if ((os_size_t)HEAP_END > (os_size_t)HEAP_BEGIN)
    {
        os_default_heap_init();
        os_default_heap_add((void *)HEAP_BEGIN, (os_size_t)HEAP_END - (os_size_t)HEAP_BEGIN, OS_MEM_ALG_DEFAULT);
    }
#endif

#ifdef OS_USING_DMA_RAM
    os_dma_mem_init();
#endif

    return OS_SUCCESS;
}
OS_INIT_CALL(os_hw_board_init, OS_INIT_LEVEL_PRE_KERNEL_1, OS_INIT_SUBLEVEL_MIDDLE);

static os_err_t board_post_init(void)
{
#ifdef OS_USING_PIN
    os_hw_pin_init();
#endif

#ifdef OS_USING_SYSTICK_FOR_CLOCKSOURCE
    riscv_systick_clocksource_init();
#else
    _SysTick_Config(SystemCoreClock / OS_TICK_PER_SECOND);
#endif

    return OS_SUCCESS;
}
OS_INIT_CALL(board_post_init, OS_INIT_LEVEL_POST_KERNEL, OS_INIT_SUBLEVEL_LOW);
