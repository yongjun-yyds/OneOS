/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_gpio.h
 *
 * @brief       This file provides struct/macro declaration and functions declaration for CH gpio driver.
 *
 * @revision
 * Date         Author          Notes
 * 2023-11-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __DRV_GPIO_H__
#define __DRV_GPIO_H__
#include <os_types.h>
#include "CH57x_common.h"

typedef enum
{
  GPIO_PIN_RESET = 0U,
  GPIO_PIN_SET
}GPIO_PinState;

/* General Purpose I/O */
typedef struct
{
    __IO uint32_t DIR;
    __IO uint32_t IN;
    __IO uint32_t OUT;
    __IO uint32_t CLR;
    __IO uint32_t PU;
    __IO uint32_t PD;
} GPIO_TypeDef;

#define PERIPH_BASE ((uint32_t)0x40000000)
#define GPIOA_BASE  (PERIPH_BASE + 0x10A0)
#define GPIOB_BASE  (PERIPH_BASE + 0x10C0)
#define GPIOA       ((GPIO_TypeDef *)GPIOA_BASE)
#define GPIOB       ((GPIO_TypeDef *)GPIOB_BASE)

#define GPIOA_PIN_OFFSET     0
#define GPIOB_PIN_OFFSET     16
#define GET_PIN(PORTx, PIN) (GPIO##PORTx##_PIN_OFFSET + PIN)

int os_hw_pin_init(void);

#endif /* __DRV_GPIO_H__ */

