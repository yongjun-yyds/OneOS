/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_common.c
 *
 * @brief       This file provides systick time init/IRQ and board init functions.
 *
 * @revision
 * Date         Author          Notes
 * 2023-11-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include <string.h>
#include <arch_interrupt.h>
#include <os_types.h>
#include <os_stddef.h>
#include <os_clock.h>
#include <os_memory.h>
#include <board.h>
#include <oneos_config.h>
#include <drv_common.h>
#include <drv_gpio.h>

#ifdef OS_USING_DMA
#include <dma.h>
#endif

#ifdef OS_USING_CLOCKSOURCE
#include <timer/clocksource.h>
#include <timer/clocksource_cortexm.h>
#endif

#if !defined(OS_USING_SYSTICK_FOR_KERNEL_TICK) && !defined(OS_USING_HRTIMER_FOR_KERNEL_TICK)
#error: OS_USING_SYSTICK_FOR_KERNEL_TICK or OS_USING_HRTIMER_FOR_KERNEL_TICK must define
#endif

static volatile os_bool_t hardware_init_done = OS_FALSE;

static uint32_t SystemCoreClock;

static uint32_t mult_systick2msec  = 1;
static uint32_t shift_systick2msec = 0;

__IO uint32_t uwTick;

uint32_t HAL_GetTick(void)
{
    if (hardware_init_done)
    {
        return (uint64_t)uwTick * mult_systick2msec >> shift_systick2msec;
    }
    else
    {
        return uwTick;
    }
}

void os_tick_handler(void)
{
    uwTick++;

    os_tick_increase();

#ifdef OS_USING_CLOCKSOURCE
    os_clocksource_update();
#endif
}

#ifdef OS_USING_SYSTICK_FOR_KERNEL_TICK
void HAL_IncTick(void)
{
    os_tick_handler();
}

static void cortexm_systick_kernel_tick_init(void)
{
    SystemCoreClock = GetSysClock();
    if (SysTick_Config(SystemCoreClock / OS_TICK_PER_SECOND))
    {
        while (1)
        {
        }
    };
}
#endif

#ifdef OS_USING_SYSTICK_FOR_CLOCKEVENT
void HAL_IncTick(void)
{
    uwTick++;

    if (hardware_init_done)
    {
        cortexm_systick_clockevent_isr();
    }
}
#endif

void cortexm_systick_init(void)
{
#ifdef OS_USING_SYSTICK_FOR_KERNEL_TICK
    cortexm_systick_kernel_tick_init();
#elif defined(OS_USING_SYSTICK_FOR_CLOCKSOURCE)
    cortexm_systick_clocksource_init();
#elif defined(OS_USING_SYSTICK_FOR_CLOCKEVENT)
    cortexm_systick_clockevent_init();
#endif
}

void SysTick_Handler(void)
{
    HAL_IncTick();
}

/**
 ***********************************************************************************************************************
 * @brief           This function will initial CH board.
 *
 * @param[in]       none
 *
 * @return          none
 ***********************************************************************************************************************
 */
static os_err_t os_hw_board_init(void)
{
    /* Heap initialization */
#ifdef OS_USING_HEAP
    if ((os_size_t)HEAP_END > (os_size_t)HEAP_BEGIN)
    {
        os_default_heap_init();
        os_default_heap_add((void *)HEAP_BEGIN, (os_size_t)HEAP_END - (os_size_t)HEAP_BEGIN, OS_MEM_ALG_DEFAULT);
    }
#endif

#ifdef OS_USING_DMA_RAM
    os_dma_mem_init();
#endif

    return OS_SUCCESS;
}
OS_INIT_CALL(os_hw_board_init, OS_INIT_LEVEL_PRE_KERNEL_1, OS_INIT_SUBLEVEL_MIDDLE);

static os_err_t board_post_init(void)
{
#ifdef OS_USING_PIN
    os_hw_pin_init();
#endif

#if defined(OS_USING_DWT_FOR_CLOCKSOURCE) && defined(DWT)
    cortexm_dwt_init();
#endif

    calc_mult_shift(&mult_systick2msec, &shift_systick2msec, OS_TICK_PER_SECOND, 1000, 1);

    cortexm_systick_init();

    hardware_init_done = OS_TRUE;

    return OS_SUCCESS;
}
OS_INIT_CALL(board_post_init, OS_INIT_LEVEL_POST_KERNEL, OS_INIT_SUBLEVEL_LOW);

void os_hw_cpu_reset(void)
{
    SYS_ResetExecute();
}

