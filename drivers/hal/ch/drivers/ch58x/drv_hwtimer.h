/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_hwtimer.h
 *
 * @brief       This file provides functions declaration for ch timer driver.
 *
 * @revision
 * Date         Author          Notes
 * 2023-11-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __DRV_HWTIMER_H__
#define __DRV_HWTIMER_H__

#include <timer/clocksource.h>
#include <timer/clockevent.h>

/* TIM */
typedef struct
{
    __IO uint8_t  CTRL_MOD;
    __IO uint8_t  CTRL_DMA;    /* Only TIM1 and TIM2 */
    __IO uint8_t  INT_EN;
         uint8_t  RESERVED0;
         uint16_t RESERVED1;
    __IO uint8_t  INT_FLAG;
    __IO uint8_t  FIFO_CNT;
    __IO uint32_t TIM_CNT;
    __IO uint32_t CNT_END;     /* Only the lower 26 bits are valid */
    __IO uint32_t FIFO;        /* Only the lower 26 bits are valid */
    
#if 0
    __IO uint16_t DMA_NOW;     /* Only TIM1 and TIM2 */
    __IO uint16_t RESERVED2;
    __IO uint16_t DMA_BEG;     /* Only TIM1 and TIM2: only the lower 15 bits are valid */
    __IO uint16_t RESERVED3;
    __IO uint16_t DMA_END;     /* Only TIM1 and TIM2: only the lower 15 bits are valid */
#endif    
} TIM_TypeDef;

#define PERIPH_BASE ((uint32_t)0x40000000)
#define TIM0_BASE   (PERIPH_BASE + 0x2000)    
#define TIM1_BASE   (PERIPH_BASE + 0x2400) 
#define TIM2_BASE   (PERIPH_BASE + 0x2800)
#define TIM3_BASE   (PERIPH_BASE + 0x2C00)
#define TIM0        ((TIM_TypeDef *)TIM0_BASE)
#define TIM1        ((TIM_TypeDef *)TIM1_BASE)
#define TIM2        ((TIM_TypeDef *)TIM2_BASE)
#define TIM3        ((TIM_TypeDef *)TIM3_BASE)

struct ch_timer_info
{
    TIM_TypeDef *htim;
    uint8_t      IT_mode;
    IRQn_Type    irq_num;
};

struct ch_timer
{
    union _clock
    {
        os_clocksource_t cs;
        os_clockevent_t  ce;
    } clock;

    struct ch_timer_info *info;

    uint32_t       freq;
    os_list_node_t list;
};

#endif

