/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_hwtimer.c
 *
 * @brief       This file implements timer driver for ch.
 *
 * @revision
 * Date         Author          Notes
 * 2023-11-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include <drv_common.h>
#include <drv_hwtimer.h>
#include <os_memory.h>
#include <timer/timer.h>

#ifdef OS_USING_CLOCKSOURCE
#include <timer/clocksource.h>
#endif

#ifdef OS_USING_CLOCKEVENT
#include <timer/clockevent.h>
#endif

#define DRV_EXT_TAG "drv.hwtimer"
#include <drv_log.h>

static os_list_node_t ch_timer_list = OS_LIST_INIT(ch_timer_list);

static void TIM_TimerInit(TIM_TypeDef *TIMx, uint32_t cnt)
{
    TIMx->CTRL_MOD = RB_TMR_ALL_CLEAR;
    TIMx->CNT_END  = cnt;
}

static void TIM_TimerEnable(TIM_TypeDef *TIMx, FunctionalState state)
{
    if (state != DISABLE)
    {
        TIMx->CTRL_MOD = RB_TMR_COUNT_EN;
    }
    else
    {
        TIMx->CTRL_MOD &= ~RB_TMR_COUNT_EN;
    }
}

static uint32_t TIM_GetCounter(TIM_TypeDef *TIMx)
{
    return TIMx->TIM_CNT;
}

static void TIM_ITConfig(TIM_TypeDef *TIMx, uint8_t TIM_IT, uint8_t FunctionalState)
{
    if (FunctionalState != DISABLE)
    {
        TIMx->INT_EN |= TIM_IT;
    }
    else
    {
        TIMx->INT_EN &= (uint8_t)~TIM_IT;
    }
}

static uint8_t TIM_GetITStatus(TIM_TypeDef *TIMx, uint8_t TIM_IT)
{
    return (TIMx->INT_FLAG & TIM_IT);
}

static void TIM_ClearITFlag(TIM_TypeDef *TIMx, uint8_t TIM_IT)
{
    TIMx->INT_FLAG = TIM_IT;
}

static uint32_t ch_timer_get_freq(struct ch_timer *timer)
{
    uint32_t clk = 0;

    OS_ASSERT(timer->info->htim != OS_NULL);

    clk = GetSysClock();

    return clk;
}

static void TIM_IRQ_Callback(TIM_TypeDef *htim)
{
    struct ch_timer *timer;

    if (TIM_GetITStatus(htim, TMR0_3_IT_CYC_END))
    {
        TIM_ClearITFlag(htim, TMR0_3_IT_CYC_END);
        os_list_for_each_entry(timer, &ch_timer_list, struct ch_timer, list)
        {
            if (timer->info->htim == htim)
            {
                #ifdef OS_USING_CLOCKEVENT
                os_clockevent_isr((os_clockevent_t *)timer);
                #endif
                
                break;
            }
        }
    }
}

/* clang-format off */
void TMR0_IRQHandler(void) __attribute__((interrupt()));
void TMR1_IRQHandler(void) __attribute__((interrupt()));
void TMR2_IRQHandler(void) __attribute__((interrupt()));
void TMR3_IRQHandler(void) __attribute__((interrupt()));
/* clang-format on */

#define TIM_IRQ_CALLBACK_FUNC(tim)                                                                                     \
    void TMR##tim##_IRQHandler(void)                                                                                   \
    {                                                                                                                  \
        TOGGLE_SP();                                                                                                   \
        TIM_IRQ_Callback(TIM##tim);                                                                                    \
        TOGGLE_SP();                                                                                                   \
    }

TIM_IRQ_CALLBACK_FUNC(0);
TIM_IRQ_CALLBACK_FUNC(1);
TIM_IRQ_CALLBACK_FUNC(2);
TIM_IRQ_CALLBACK_FUNC(3);

#if defined(OS_USING_CLOCKSOURCE) || defined(OS_USING_CLOCKEVENT)
static uint64_t ch_timer_read(void *clock)
{
    struct ch_timer *timer;

    timer = (struct ch_timer *)clock;

    return TIM_GetCounter(timer->info->htim);
}
#endif

#ifdef OS_USING_CLOCKEVENT
static void ch_timer_start(os_clockevent_t *ce, uint32_t prescaler, uint64_t count)
{
    struct ch_timer *timer;
    TIM_TypeDef     *htim;

    OS_ASSERT(prescaler != 0);
    OS_ASSERT(count != 0);

    timer = (struct ch_timer *)ce;
    htim  = timer->info->htim;
    
    TIM_ITConfig(htim, TMR0_3_IT_CYC_END, ENABLE);
    TIM_TimerInit(htim, count);
    TIM_TimerEnable(htim, ENABLE);
}

static void ch_timer_stop(os_clockevent_t *ce)
{
    struct ch_timer *timer;
    TIM_TypeDef     *htim;

    OS_ASSERT(ce != OS_NULL);

    timer = (struct ch_timer *)ce;
    htim  = timer->info->htim;

    TIM_ITConfig(htim, TMR0_3_IT_CYC_END, DISABLE);
    TIM_TimerEnable(htim, DISABLE);
}

static const struct os_clockevent_ops ch_tim_ops = {
    .start = ch_timer_start,
    .stop  = ch_timer_stop,
    .read  = ch_timer_read,
};
#endif

static int ch_tim_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    os_ubase_t       level;
    struct ch_timer *timer;
    TIM_TypeDef     *htim;

    timer = os_calloc(1, sizeof(struct ch_timer));
    OS_ASSERT(timer);

    timer->info = (struct ch_timer_info *)dev->info;
    timer->freq = ch_timer_get_freq(timer);

    htim = timer->info->htim;

#ifdef OS_USING_CLOCKSOURCE
    if (os_clocksource_best() == OS_NULL)
    {
        TIM_TimerInit(htim, 0x3fffffful);
        TIM_TimerEnable(htim, ENABLE);

        timer->clock.cs.rating = 260;
        timer->clock.cs.freq   = timer->freq;
        timer->clock.cs.mask   = 0x3fffffful;
        timer->clock.cs.read   = ch_timer_read;

        os_clocksource_register(dev->name, &timer->clock.cs);
    }
    else
#endif
    {
#ifdef OS_USING_CLOCKEVENT
        timer->clock.ce.rating = 260;
        timer->clock.ce.freq   = timer->freq;
        timer->clock.ce.mask   = 0x3fffffful;

        timer->clock.ce.prescaler_mask = 1;
        timer->clock.ce.prescaler_bits = 0; /* no prescaler */

        timer->clock.ce.count_mask = 0x3fffffful;
        timer->clock.ce.count_bits = 26;

        timer->clock.ce.feature = OS_CLOCKEVENT_FEATURE_PERIOD;

        timer->clock.ce.min_nsec = NSEC_PER_SEC / timer->clock.ce.freq;

        timer->clock.ce.ops = &ch_tim_ops;

        PFIC_EnableIRQ(timer->info->irq_num);
        os_clockevent_register(dev->name, &timer->clock.ce);
#endif
    }

    os_spin_lock_irqsave(&gs_device_lock, &level);
    os_list_add_tail(&ch_timer_list, &timer->list);
    os_spin_unlock_irqrestore(&gs_device_lock, level);

    return OS_SUCCESS;
}

OS_DRIVER_INFO ch_tim_driver = {
    .name  = "TIM_TypeDef",
    .probe = ch_tim_probe,
};

OS_DRIVER_DEFINE(ch_tim_driver, OS_INIT_LEVEL_PRE_DEVICE, OS_INIT_SUBLEVEL_MIDDLE);

