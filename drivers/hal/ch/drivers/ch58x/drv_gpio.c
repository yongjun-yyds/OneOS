/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_gpio.c
 *
 * @brief       This file implements gpio driver for CH.
 *
 * @revision
 * Date         Author          Notes
 * 2023-11-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include <board.h>

#ifdef OS_USING_PIN
#include <driver.h>
#include "drv_gpio.h"
#include <os_types.h>

#define DRV_EXT_TAG "drv.gpio"
#include <dlog.h>

struct gpio_info
{
    uint8_t       pin_index;
    GPIO_TypeDef *gpio_port;
    uint32_t      pin;
};

/* clang-format off */
#define __CH_PIN(index, port, pin)     {index, GPIO##port, GPIO_Pin_##pin}

static const struct gpio_info pins[] = {
#if defined(GPIOA)
    __CH_PIN(0, A, 0),   __CH_PIN(1, A, 1),   __CH_PIN(2, A, 2),   __CH_PIN(3, A, 3),
    __CH_PIN(4, A, 4),   __CH_PIN(5, A, 5),   __CH_PIN(6, A, 6),   __CH_PIN(7, A, 7),
    __CH_PIN(8, A, 8),   __CH_PIN(9, A, 9),   __CH_PIN(10, A, 10), __CH_PIN(11, A, 11),
    __CH_PIN(12, A, 12), __CH_PIN(13, A, 13), __CH_PIN(14, A, 14), __CH_PIN(15, A, 15),
#if defined(GPIOB)
    __CH_PIN(16, B, 0),  __CH_PIN(17, B, 1),  __CH_PIN(18, B, 2),  __CH_PIN(19, B, 3),
    __CH_PIN(20, B, 4),  __CH_PIN(21, B, 5),  __CH_PIN(22, B, 6),  __CH_PIN(23, B, 7),
    __CH_PIN(24, B, 8),  __CH_PIN(25, B, 9),  __CH_PIN(26, B, 10), __CH_PIN(27, B, 11),
    __CH_PIN(28, B, 12), __CH_PIN(29, B, 13), __CH_PIN(30, B, 14), __CH_PIN(31, B, 15),
    __CH_PIN(32, B, 16), __CH_PIN(33, B, 17), __CH_PIN(34, B, 18), __CH_PIN(35, B, 19),
    __CH_PIN(36, B, 20), __CH_PIN(37, B, 21), __CH_PIN(38, B, 22), __CH_PIN(39, B, 23),
#endif
#endif
};

static struct os_pin_irq_hdr pin_irq_hdr_tab[] = {
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
};
/* clang-format on */

#define ITEM_NUM(items) sizeof(items) / sizeof(items[0])

static const struct gpio_info *_get_gpio_info(os_base_t pin)
{
    const struct gpio_info *pin_info;
    if (pin < ITEM_NUM(pins))
    {
        pin_info = &pins[pin];
    }
    else
    {
        pin_info = OS_NULL;
    }
    return pin_info;
}

static void ch_pin_write(os_device_t *dev, os_base_t pin, os_base_t value)
{
    const struct gpio_info *info;

    info = _get_gpio_info(pin);
    if (OS_NULL == info)
    {
        return;
    }
    if ((GPIO_PinState)value != GPIO_PIN_RESET)
    {
        info->gpio_port->OUT |= info->pin;
    }
    else
    {
        info->gpio_port->CLR |= info->pin;
    }
}

static int ch_pin_read(os_device_t *dev, os_base_t pin)
{
    int                     value = PIN_LOW;
    const struct gpio_info *info;

    info = _get_gpio_info(pin);
    if (OS_NULL == info)
    {
        return OS_FAILURE;
    }

    if (info->gpio_port->DIR & pin)    /* output or af */
    {
        if ((info->gpio_port->OUT & info->pin) != (uint32_t)GPIO_PIN_RESET)
        {
            value = PIN_HIGH;
        }
        else
        {
            value = PIN_LOW;
        }
    }
    else
    {
        if ((info->gpio_port->IN & info->pin) != (uint32_t)GPIO_PIN_RESET)
        {
            value = PIN_HIGH;
        }
        else
        {
            value = PIN_LOW;
        }
    }

    return value;
}

static os_err_t ch_pin_detach_irq(struct os_device *device, int32_t pin);

static void ch_pin_mode(os_device_t *dev, os_base_t pin, os_base_t mode)
{
    GPIOModeTypeDef         ch_mode;
    const struct gpio_info *info;

    info = _get_gpio_info(pin);
    if (OS_NULL == info)
    {
        return;
    }

    if (mode == PIN_MODE_OUTPUT)
    {
        /* output setting */
        ch_mode = GPIO_ModeOut_PP_5mA;
        /* pin out_pp need to set IO to high level first */
        info->gpio_port->OUT |= info->pin;
    }
    else if (mode == PIN_MODE_INPUT)
    {
        /* input setting: not pull. */
        ch_mode = GPIO_ModeIN_Floating;
    }
    else if (mode == PIN_MODE_INPUT_PULLUP)
    {
        /* input setting: pull up. */
        ch_mode = GPIO_ModeIN_PU;
    }
    else if (mode == PIN_MODE_INPUT_PULLDOWN)
    {
        /* input setting: pull down. */
        ch_mode = GPIO_ModeIN_PD;
    }
    else if (mode == PIN_MODE_OUTPUT_OD)
    {
        /* output setting: od. */
        os_kprintf("CH GPIO does not support OUTPUT_OD mode!\r\n");
        return;
    }
    else if (mode == PIN_MODE_DISABLE)
    {
        if (info->pin_index < ITEM_NUM(pin_irq_hdr_tab))
        {
            os_pin_irq_enable(pin, PIN_IRQ_DISABLE);
            if (OS_SUCCESS != ch_pin_detach_irq(dev, pin))
            {
                return;
            }
        }

        return;
    }

    /* set the mode state. */
    if (info->gpio_port == GPIOA)
    {
        GPIOA_ModeCfg(info->pin, ch_mode);
    }
    else
    {
        GPIOB_ModeCfg(info->pin, ch_mode);
    }
}

static os_err_t
ch_pin_attach_irq(struct os_device *device, int32_t pin, uint32_t mode, void (*hdr)(void *args), void *args)
{
    os_ubase_t              level;
    const struct gpio_info *info;

    info = _get_gpio_info(pin);
    if (OS_NULL == info)
    {
        return OS_FAILURE;
    }

    if (info->pin_index >= ITEM_NUM(pin_irq_hdr_tab))
    {
        os_kprintf("The pin in use does not support the interrupt function! pin_attach_irq failed!\r\n");
        return OS_FAILURE;
    }

    os_spin_lock_irqsave(&gs_device_lock, &level);
    if (pin_irq_hdr_tab[info->pin_index].pin == pin && pin_irq_hdr_tab[info->pin_index].hdr == hdr &&
        pin_irq_hdr_tab[info->pin_index].mode == mode && pin_irq_hdr_tab[info->pin_index].args == args)
    {
        os_spin_unlock_irqrestore(&gs_device_lock, level);
        return OS_SUCCESS;
    }
    if (pin_irq_hdr_tab[info->pin_index].pin != -1)
    {
        os_spin_unlock_irqrestore(&gs_device_lock, level);
        return OS_BUSY;
    }
    pin_irq_hdr_tab[info->pin_index].pin  = pin;
    pin_irq_hdr_tab[info->pin_index].hdr  = hdr;
    pin_irq_hdr_tab[info->pin_index].mode = mode;
    pin_irq_hdr_tab[info->pin_index].args = args;
    os_spin_unlock_irqrestore(&gs_device_lock, level);

    return OS_SUCCESS;
}

static os_err_t ch_pin_detach_irq(struct os_device *device, int32_t pin)
{
    os_ubase_t              level;
    const struct gpio_info *info;

    info = _get_gpio_info(pin);
    if (OS_NULL == info)
    {
        return OS_FAILURE;
    }

    if (info->pin_index >= ITEM_NUM(pin_irq_hdr_tab))
    {
        os_kprintf("The pin in use does not support the interrupt function! pin_detach_irq failed!\r\n");
        return OS_FAILURE;
    }

    os_spin_lock_irqsave(&gs_device_lock, &level);
    if (pin_irq_hdr_tab[info->pin_index].pin == -1)
    {
        os_spin_unlock_irqrestore(&gs_device_lock, level);
        return OS_SUCCESS;
    }
    pin_irq_hdr_tab[info->pin_index].pin  = -1;
    pin_irq_hdr_tab[info->pin_index].hdr  = OS_NULL;
    pin_irq_hdr_tab[info->pin_index].mode = 0;
    pin_irq_hdr_tab[info->pin_index].args = OS_NULL;
    os_spin_unlock_irqrestore(&gs_device_lock, level);

    return OS_SUCCESS;
}

static os_err_t ch_pin_irq_enable(struct os_device *device, os_base_t pin, uint32_t enabled)
{
    os_ubase_t      level;
    GPIOITModeTpDef it_mode;
    uint32_t        temp_pin;

    const struct gpio_info *info;
    info = _get_gpio_info(pin);
    if (OS_NULL == info)
    {
        return OS_FAILURE;
    }

    if (info->pin_index >= ITEM_NUM(pin_irq_hdr_tab))
    {
        os_kprintf("The pin in use does not support the interrupt function! pin_irq_enable failed!\r\n");
        return OS_FAILURE;
    }

    os_spin_lock_irqsave(&gs_device_lock, &level);

    if (enabled == PIN_IRQ_ENABLE)
    {
        if (pin_irq_hdr_tab[info->pin_index].pin == -1)
        {
            os_spin_unlock_irqrestore(&gs_device_lock, level);
            return OS_NOSYS;
        }

        switch (pin_irq_hdr_tab[info->pin_index].mode)
        {
        case PIN_IRQ_MODE_RISING:
            it_mode = GPIO_ITMode_RiseEdge;
            break;
        case PIN_IRQ_MODE_FALLING:
            it_mode = GPIO_ITMode_FallEdge;
            break;
        case PIN_IRQ_MODE_HIGH_LEVEL:
            it_mode = GPIO_ITMode_HighLevel;
            break;
        case PIN_IRQ_MODE_LOW_LEVEL:
            it_mode = GPIO_ITMode_LowLevel;
            break;

        default:
            os_kprintf("not support this irq_mode:%d!\r\n", pin_irq_hdr_tab[info->pin_index].mode);
            os_spin_unlock_irqrestore(&gs_device_lock, level);
            return OS_FAILURE;
        }
        if (info->gpio_port == GPIOA)
        {
            GPIOA_ITModeCfg(info->pin, it_mode);
            PFIC_ClearPendingIRQ(GPIO_A_IRQn);
            PFIC_EnableIRQ(GPIO_A_IRQn);
        }
        else
        {
            GPIOB_ITModeCfg(info->pin, it_mode);
            PFIC_ClearPendingIRQ(GPIO_B_IRQn);
            PFIC_EnableIRQ(GPIO_B_IRQn);
        }
    }
    else if (enabled == PIN_IRQ_DISABLE)
    {
        if (info->gpio_port == GPIOA)
        {
            GPIOA_ClearITFlagBit(info->pin);
            R16_PA_INT_EN &= ~info->pin;
            if (!R16_PA_INT_EN)
            {
                PFIC_ClearPendingIRQ(GPIO_A_IRQn);
                PFIC_DisableIRQ(GPIO_A_IRQn);
            }
        }
        else
        {
            GPIOB_ClearITFlagBit(info->pin);
            temp_pin = info->pin | ((info->pin & (GPIO_Pin_22 | GPIO_Pin_23)) >> 14);
            R16_PB_INT_EN &= ~(uint16_t)temp_pin;
            if (!R16_PB_INT_EN)
            {
                PFIC_ClearPendingIRQ(GPIO_B_IRQn);
                PFIC_DisableIRQ(GPIO_B_IRQn);
            }
        }
    }

    os_spin_unlock_irqrestore(&gs_device_lock, level);

    return OS_SUCCESS;
}

OS_INLINE void pin_irq_hdr(uint8_t pin_index)
{
    if (pin_irq_hdr_tab[pin_index].hdr)
    {
        pin_irq_hdr_tab[pin_index].hdr(pin_irq_hdr_tab[pin_index].args);
    }
}

void GPIOA_IRQHandler(void) __attribute__((interrupt()));
void GPIOB_IRQHandler(void) __attribute__((interrupt()));

void GPIOA_IRQHandler(void)
{
    TOGGLE_SP();
    uint8_t pin_index = 0;

    for (pin_index = 0; pin_index < 16; pin_index++)
    {
        if (GPIOA_ReadITFlagBit(pins[pin_index].pin) != 0)
        {
           GPIOA_ClearITFlagBit(pins[pin_index].pin);
           pin_irq_hdr(pin_index); 
        } 
    } 
    TOGGLE_SP();
}

void GPIOB_IRQHandler(void)
{
    TOGGLE_SP();
    uint8_t pin_index = 0;

    for (pin_index = 16; pin_index < ITEM_NUM(pin_irq_hdr_tab); pin_index++)
    {
        if (GPIOB_ReadITFlagBit(pins[pin_index].pin) != 0)
        {
           GPIOB_ClearITFlagBit(pins[pin_index].pin);
           pin_irq_hdr(pin_index); 
        }  
    } 
    TOGGLE_SP();
}

const static struct os_pin_ops ch_pin_ops = {
    .pin_mode       = ch_pin_mode,
    .pin_write      = ch_pin_write,
    .pin_read       = ch_pin_read,
    .pin_attach_irq = ch_pin_attach_irq,
    .pin_detach_irq = ch_pin_detach_irq,
    .pin_irq_enable = ch_pin_irq_enable,
};

int os_hw_pin_init(void)
{
    return os_device_pin_register(0, &ch_pin_ops, OS_NULL);
}

#endif

