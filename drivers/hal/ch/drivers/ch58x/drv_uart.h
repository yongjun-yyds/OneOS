/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_uart.h
 *
 * @brief        This file provides functions declaration for uart driver.
 *
 * @revision
 * Date         Author          Notes
 * 2023-11-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#ifndef __DRV_UART_H__
#define __DRV_UART_H__

#include <os_types.h>
#include <oneos_config.h>
#include <drv_gpio.h>
#include "CH58x_common.h"

typedef struct
{
    __IO uint8_t  MCR;
    __IO uint8_t  IER;
    __IO uint8_t  FCR;
    __IO uint8_t  LCR;
    __IO uint8_t  IIR;
    __IO uint8_t  LSR;
    __IO uint8_t  MSR;      /* only for UART0 */
         uint8_t  RESERVED0;
    __IO uint8_t  R_TBR;    /* RBR_THR */
         uint8_t  RESERVED1;
    __IO uint8_t  RFC;
    __IO uint8_t  TFC;
    __IO uint16_t DL;
    __IO uint8_t  DIV;
    __IO uint8_t  ADR;      /* only for UART0 */
} UART_TypeDef;

#define PERIPH_BASE ((uint32_t)0x40000000)
#define UART0_BASE  (PERIPH_BASE + 0x3000)
#define UART1_BASE  (PERIPH_BASE + 0x3400)
#define UART2_BASE  (PERIPH_BASE + 0x3800)
#define UART3_BASE  (PERIPH_BASE + 0x3C00)
#define UART0       ((UART_TypeDef *)UART0_BASE)
#define UART1       ((UART_TypeDef *)UART1_BASE)
#define UART2       ((UART_TypeDef *)UART2_BASE)
#define UART3       ((UART_TypeDef *)UART3_BASE)

typedef struct
{
    uint32_t  UART_BaudRate;
    uint16_t  UART_WordLength;
    uint16_t  UART_StopBits;
    uint16_t  UART_Parity;
    uint16_t  UART_HardwareFlowControl;
    uint16_t  UART_RxFIFO;
    uint16_t  UART_RxFIFOTrigBits;
    IRQn_Type UART_Irq;
    uint32_t  UART_IrqMode;
} UART_InitTypeDef;

#define CH_DATA_BITS_5           0x00   /* RB_LCR_WORD_SZ */
#define CH_DATA_BITS_6           0x01   /* RB_LCR_WORD_SZ */
#define CH_DATA_BITS_7           0x02   /* RB_LCR_WORD_SZ */
#define CH_DATA_BITS_8           0x03   /* RB_LCR_WORD_SZ */
#define CH_STOP_BITS_1           0x00   /* RB_LCR_STOP_BIT */
#define CH_STOP_BITS_2           0x04   /* RB_LCR_STOP_BIT */
#define CH_PARITY_NONE           0x00   /* RB_LCR_PAR_EN, RB_LCR_PAR_MOD */
#define CH_PARITY_ODD            0x08   /* RB_LCR_PAR_EN, RB_LCR_PAR_MOD */
#define CH_PARITY_EVEN           0x18   /* RB_LCR_PAR_EN, RB_LCR_PAR_MOD */

#define CH_FIFO_TRIG_BITS_1      0x00   /* RB_FCR_FIFO_TRIG */
#define CH_FIFO_TRIG_BITS_2      0x40   /* RB_FCR_FIFO_TRIG */
#define CH_FIFO_TRIG_BITS_4      0x80   /* RB_FCR_FIFO_TRIG */
#define CH_FIFO_TRIG_BITS_7      0xC0   /* RB_FCR_FIFO_TRIG */

typedef struct ch_uart_info
{
    GPIO_TypeDef *tx_port;
    uint32_t      tx_pin;

    GPIO_TypeDef *rx_port;
    uint32_t      rx_pin;

    UART_TypeDef *huart;

    UART_InitTypeDef uart_def_cfg;

} ch_uart_info_t;

typedef struct ch_uart
{
    struct os_serial_device serial;
    ch_uart_info_t         *info;

    soft_dma_t  sdma;
    uint32_t    sdma_hard_size;

    uint8_t *rx_buff;
    uint32_t rx_index;
    uint32_t rx_size;

    const uint8_t *tx_buff;
    uint32_t       tx_count;
    uint32_t       tx_size;

    os_list_node_t list;
} ch_uart_t;

#endif /* __DRV_UART_H__ */

