/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with 
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_flash_ch58x.c
 *
 * @brief        This file provides flash read/write/erase functions for ch58x.
 *
 * @revision
 * Date         Author          Notes
 * 2023-11-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include "board.h"
#include "drv_flash.h"
#include "fal.h"
#include "flash_info.c"

/**
 ***********************************************************************************************************************
 * @brief           ch_flash_read:Read data from flash.
 *
 * @param[in]       addr            flash address.
 * @param[out]      buf             buffer to store read data.
 * @param[in]       size            read bytes size.
 *
 * @return          Return read size or status.
 * @retval          size            read bytes size.
 * @retval          Others          read failed.
 ***********************************************************************************************************************
 */
int ch_flash_read(uint32_t addr, uint8_t *buf, size_t size)
{
    size_t i;

    if ((addr + size) > CH_FLASH_END_ADDRESS)
    {
        os_kprintf("read outrange flash size! addr is (0x%p)\r\n", (void*)(addr + size));
        return OS_INVAL;
    }
    if (size < 1)
    {
        os_kprintf("read size < 1\r\n");
        return OS_INVAL;
    }

    for(i = 0; i < size; i++, buf++, addr++)
    {
        *buf = *(uint8_t *) addr;
    }

    return size;
}

static int flash_write_dword(uint32_t addr, uint8_t *buff, size_t size)
{
    uint8_t ret = 0;

    if(size % 4)
    {
        os_kprintf("Write size must be aligned with 4 bytes!\r\n");
        return OS_FAILURE;
    }
    
    ret = FLASH_ROM_WRITE(addr, buff, size);
    if (ret != OS_SUCCESS)
    {
        return OS_FAILURE;
    }
    ret = FLASH_ROM_VERIFY(addr, buff, size);
    if (ret != OS_SUCCESS)
    {
        os_kprintf("Flash write verify error!\r\n");
        return OS_FAILURE;
    }

    return OS_SUCCESS;
}

/**
 ***********************************************************************************************************************
 * @brief           Write data to flash.This operation's units is dword.
 *
 * @attention       This operation must after erase.
 *
 * @param[in]       addr            flash address.
 * @param[in]       buf             the write data buffer.
 * @param[in]       size            write bytes size.
 *
 * @return          Return write size or status.
 * @retval          size            write bytes size.
 * @retval          Others          write failed.
 ***********************************************************************************************************************
 */
int ch_flash_write(uint32_t addr, const uint8_t *buf, size_t size)
{
    uint8_t *data         = (uint8_t *)buf; 
    uint8_t  writedata[4] = {0};
    size_t   writesize    = 0;
    size_t   lastsize     = 0;
    uint32_t i            = 0;
    uint8_t  ret          = 0;
    
    if ((addr + size) > CH_FLASH_END_ADDRESS)
    {
        os_kprintf("ERROR: write outrange flash size! addr is (0x%p)\r\n", (void *)(addr + size));
        return OS_INVAL;
    }
    if (size < 1)
    {
        os_kprintf("write size < 1\r\n");
        return OS_INVAL;
    }

    if(size < 4)
    {
        writesize = 4;
        lastsize  = 0;
        
        for(i = 0; i < size; i++)
        {
            writedata[i] = *data;
            data++;
        }
        
        ret = flash_write_dword(addr, writedata, writesize);
        if (ret != OS_SUCCESS)
        {
            os_kprintf("Flash write failed!\r\n");
            return OS_FAILURE;
        }
    }
    else
    {
        lastsize  = size % 4;
        writesize = size - lastsize;

        ret = flash_write_dword(addr, data, writesize);
        if (ret != OS_SUCCESS)
        {
            os_kprintf("Flash write failed!\r\n");
            return OS_FAILURE;
        }

        if(lastsize > 0)
        {
            addr += writesize;
            data += writesize;

            writesize = 4;

            for(i = 0; i < lastsize; i++)
            {
                writedata[i] = *data;
                data++;
            }

            ret = flash_write_dword(addr, writedata, writesize);
            if (ret != OS_SUCCESS)
            {
                os_kprintf("Flash write failed!\r\n");
                return OS_FAILURE;
            }
        }
    }
  
    return size;
}

/**
 ***********************************************************************************************************************
 * @brief           Erase data on flash.This operation is irreversible and it's units is different which on many chips.
 *
 * @param[in]       addr            Flash address.
 * @param[in]       size            Erase bytes size.
 *
 * @return          Return erase result or status.
 * @retval          size            Erase bytes size.
 * @retval          Others          Erase failed.
 ***********************************************************************************************************************
 */
int ch_flash_erase(uint32_t addr, size_t size)
{
    uint8_t ret = 0;

    if ((addr + size) > CH_FLASH_END_ADDRESS)
    {
        os_kprintf("ERROR: write outrange flash size! addr is (0x%p)\r\n", (void *)(addr + size));
        return OS_INVAL;
    }
    if (size < 1)
    {
        os_kprintf("erase size < 1\r\n");
        return OS_INVAL;
    }

    ret = FLASH_ROM_ERASE(addr, size);
    if (ret != OS_SUCCESS)
    {
        os_kprintf("Flash erase failed!\r\n");
        return OS_FAILURE;
    }

    return size;
}

static int ch_flash_read_page(fal_flash_t *flash, uint32_t page_addr, uint8_t *buff, uint32_t page_nr)
{
    int count = 0;
    struct onchip_flash_info *flash_info = flash->priv;

    count = ch_flash_read(flash_info->start_addr + page_addr * flash->page_size, buff, page_nr * flash->page_size);
    return (count == page_nr * flash->page_size) ? 0 : -1;
}

static int ch_flash_write_page(fal_flash_t *flash, uint32_t page_addr, const uint8_t *buff, uint32_t page_nr)
{
    int count = 0;
    struct onchip_flash_info *flash_info = flash->priv;

    count = ch_flash_write(flash_info->start_addr + page_addr * flash->page_size, buff, page_nr * flash->page_size);
    return (count == page_nr * flash->page_size) ? 0 : -1;
}

static int ch_flash_erase_block(fal_flash_t *flash, uint32_t page_addr, uint32_t page_nr)
{
    int count = 0;
    struct onchip_flash_info *flash_info = flash->priv;

    count = ch_flash_erase(flash_info->start_addr + page_addr * flash->page_size, page_nr * flash->page_size);
    return (count == page_nr * flash->page_size) ? 0 : -1;
}

int ch_flash_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    fal_flash_t *fal_flash = os_calloc(1, sizeof(fal_flash_t));

    if (fal_flash == OS_NULL)
    {
        os_kprintf("fal flash mem leak %s.\r\n", dev->name);
        return -1;
    }

    struct onchip_flash_info *flash_info = (struct onchip_flash_info *)dev->info;

    memcpy(fal_flash->name, dev->name, min(FAL_DEV_NAME_MAX - 1, strlen(dev->name)));

    fal_flash->name[min(FAL_DEV_NAME_MAX - 1, strlen(dev->name))] = 0;

    fal_flash->capacity   = flash_info->capacity;
    fal_flash->block_size = flash_info->block_size;
    fal_flash->page_size  = flash_info->page_size;

    fal_flash->ops.read_page   = ch_flash_read_page;
    fal_flash->ops.write_page  = ch_flash_write_page;
    fal_flash->ops.erase_block = ch_flash_erase_block;

    fal_flash->priv = flash_info;

    return fal_flash_register(fal_flash);
}

OS_DRIVER_INFO ch_flash_driver = {
    .name  = "Onchip_Flash_Type",
    .probe = ch_flash_probe,
};

OS_DRIVER_DEFINE(ch_flash_driver, OS_INIT_LEVEL_DEVICE, OS_INIT_SUBLEVEL_HIGH);

