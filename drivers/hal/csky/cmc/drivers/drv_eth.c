/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_eth.c
 *
 * @brief       This file implements eth driver.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include "board.h"
#include <netif/ethernetif.h>
#include "lwipopts.h"
#include <drv_eth.h>
#include <os_memory.h>
#include <dma.h>
#include <string.h>
#include <bus/bus.h>

/*
 * Emac driver uses CubeMX tool to generate emac and phy's configuration,
 * the configuration files can be found in CubeMX_Config folder.
 */

#define DRV_EXT_LVL DBG_EXT_INFO
#define DRV_EXT_TAG "drv.eth"

#define DBG_TAG "drv.eth"
#include <dlog.h>

static struct os_ck_eth ck_eth_dev0;
static struct os_ck_eth ck_eth_dev1;

#if defined(ETH_RX_DUMP) || defined(ETH_TX_DUMP)
#define __is_print(ch) ((unsigned int)((ch) - ' ') < 127u - ' ')
static void dump_hex(const uint8_t *ptr, os_size_t buflen)
{
    unsigned char *buf = (unsigned char *)ptr;
    int            i, j;

    for (i = 0; i < buflen; i += 16)
    {
        os_kprintf("%08X: ", i);

        for (j = 0; j < 16; j++)
            if (i + j < buflen)
                os_kprintf("%02X ", buf[i + j]);
            else
                os_kprintf("   ");
        os_kprintf(" ");

        for (j = 0; j < 16; j++)
            if (i + j < buflen)
                os_kprintf("%c", __is_print(buf[i + j]) ? buf[i + j] : '.');
        os_kprintf("\r\n");
    }
}
#endif

static void ck_mac_gpio_init(void)
{
    static uint8_t init_state = 1;
    if (1 == init_state)
    {
        init_state = 0;
        GPIO_InitStructure GPIO_InitInfo;
        /* INIT MAC1 GPIO */
        GPIO_InitInfo.GPIOAddr      = GPIO0;
        GPIO_InitInfo.GPIOPort      = PORTB;
        GPIO_InitInfo.GPIODirection = GPIO_INPUT;
        GPIO_InitInfo.GPIOPin       = GPIO_P13 | GPIO_P14 | GPIO_P15;
        GPIO_InitInfo.bHardwareMode = TRUE;
        GPIO_InitInfo.GPIOIntr      = GPIO_NULL_INTR;
        GPIO_Config(&GPIO_InitInfo);

        GPIO_InitInfo.GPIOAddr = GPIO1;
        GPIO_InitInfo.GPIOPort = PORTA;
        GPIO_InitInfo.GPIOPin  = GPIO_P14 | GPIO_P15 | GPIO_P16 | GPIO_P17 | GPIO_P18 | GPIO_P19;
        GPIO_Config(&GPIO_InitInfo);

        /* INIT MAC0 GPIO */
        GPIO_InitInfo.GPIOAddr      = GPIO0;
        GPIO_InitInfo.GPIOPort      = PORTB;
        GPIO_InitInfo.GPIODirection = GPIO_INPUT;
        GPIO_InitInfo.GPIOPin = GPIO_P0 | GPIO_P1 | GPIO_P2 | GPIO_P3 | GPIO_P4 | GPIO_P5 | GPIO_P6 | GPIO_P7 | GPIO_P8;
        GPIO_InitInfo.bHardwareMode = TRUE;
        GPIO_InitInfo.GPIOIntr      = GPIO_NULL_INTR;
        GPIO_Config(&GPIO_InitInfo);
    }
}

/**
 ***********************************************************************************************************************
 * @brief           EMAC initialization function
 *
 * @param[in]       dev             pointer to os_device_t.
 *
 * @return          Return eth init status.
 * @retval          OS_SUCCESS       init success.
 * @retval          Others       init failed.
 ***********************************************************************************************************************
 */
static os_err_t os_ck_eth_init(os_device_t *dev)
{
    struct eth_device *eth_dev = os_container_of(dev, struct eth_device, parent);
    struct os_ck_eth  *ck_eth  = os_container_of(eth_dev, struct os_ck_eth, parent);
    uint32_t        level   = 0;
    struct xemac_s    *xemac;
    xemac                   = &ck_eth->xemac;
    pMacInformation_st pMac = NULL;
    os_spin_lock_irqsave(&gs_device_lock, &level);
    ck_mac_gpio_init();

    if (!(pMac = (pMacInformation_st)MAC_Config(ck_eth))) /* ���õ�ǰ��MACʵ�� */

    {
        return ERR_IF;
    }
    xemac->state = (void *)pMac;

    pMac->pSend_q = NULL;
    pMac->pRecv_q = pq_create_queue(); /* �������ն��� */
    if (NULL == pMac->pRecv_q)         /* ���ն��д���ʧ�� */
    {
        return ERR_MEM;
    }
    if (MAC_Init(pMac) != 0) /* ��ʼ��MAC */
    {
        os_spin_unlock_irqrestore(&gs_device_lock, level);
        return ERR_IF; /* MAC��ʼ��ʧ�� */
    }

    MAC_Enable(pMac); /* ʹ��MAC��DMA */
    os_spin_unlock_irqrestore(&gs_device_lock, level);

    return ERR_OK;
}

static os_err_t os_ck_eth_deinit(os_device_t *dev)
{
    LOG_D(DBG_TAG, "emac deinit");
    return OS_SUCCESS;
}

static os_size_t os_ck_eth_read(os_device_t *dev, os_off_t pos, void *buffer, os_size_t size)
{
    LOG_D(DBG_TAG, "emac read");
    os_set_errno(OS_NOSYS);
    return 0;
}

static os_size_t os_ck_eth_write(os_device_t *dev, os_off_t pos, const void *buffer, os_size_t size)
{
    LOG_D(DBG_TAG, "emac write");
    os_set_errno(OS_NOSYS);
    return 0;
}

static os_err_t os_ck_eth_control(os_device_t *dev, int cmd, void *args)
{
    struct eth_device *eth_dev = os_container_of(dev, struct eth_device, parent);
    struct os_ck_eth  *ck_eth  = os_container_of(eth_dev, struct os_ck_eth, parent);
    switch (cmd)
    {
    case NIOCTL_GADDR:
        /* get mac address */
        if (args)
            memcpy(args, &ck_eth->dev_addr, 6);
        else
            return OS_FAILURE;
        break;

    default:
        break;
    }

    return OS_SUCCESS;
}

/**
 ***********************************************************************************************************************
 * @brief           ethernet device interface,transmit data.
 *
 * @param[in]       dev             pointer to os_device_t.
 * @param[in]       p               pointer to struct pbuf.
 *
 * @return          Return eth tx result.
 * @retval          ERR_OK            tx success.
 * @retval          Others            tx failed.
 ***********************************************************************************************************************
 */
os_err_t os_ck_eth_tx(os_device_t *dev, struct pbuf *p)
{
    struct eth_device *eth_dev = os_container_of(dev, struct eth_device, parent);
    struct os_ck_eth  *ck_eth  = os_container_of(eth_dev, struct os_ck_eth, parent);
    pMacInformation_st pMac    = (pMacInformation_st)(ck_eth->xemac.state);
    uint32_t        level   = 0;
    os_spin_lock_irqsave(&gs_device_lock, &level);

#if ETH_PAD_SIZE
    pbuf_header(p, -ETH_PAD_SIZE); /* drop the padding word */
#endif

    if ((NULL == pMac) || (NULL == p))
    {
        os_spin_unlock_irqrestore(&gs_device_lock, level);
        return ERR_ARG;
    }

    if (MAC_SendFrame(pMac, p))
    {
        os_spin_unlock_irqrestore(&gs_device_lock, level);
        return ERR_IF;
    }

#if ETH_PAD_SIZE
    pbuf_header(p, ETH_PAD_SIZE); /* reclaim the padding word */
#endif

    os_spin_unlock_irqrestore(&gs_device_lock, level);
    return OS_SUCCESS;
}

/* receive data */
struct pbuf *os_ck_eth_rx(os_device_t *dev)
{
    struct eth_device *eth_dev = os_container_of(dev, struct eth_device, parent);
    struct os_ck_eth  *ck_eth  = os_container_of(eth_dev, struct os_ck_eth, parent);
    pMacInformation_st pMac    = (pMacInformation_st)(ck_eth->xemac.state);
    uint32_t        level   = 0;
    struct pbuf       *p       = NULL;

    MAC_RecvHandler(pMac);

    if (pq_qlength(pMac->pRecv_q) == 0)
    {
        return NULL;
    }
    p = (struct pbuf *)pq_dequeue(pMac->pRecv_q);

#ifdef ETH_RX_DUMP
    dump_hex(p->payload, p->tot_len);
#endif
    return p;
}

void HAL_ETH_RxCpltCallback(pMacInformation_st pMacInst)
{
    os_err_t result;
    if (xtopology[0].emac_baseaddr == (unsigned)pMacInst->pMacAddr)
    {
        result = eth_device_ready(&ck_eth_dev0.parent);
    }
    else if (xtopology[1].emac_baseaddr == (unsigned)pMacInst->pMacAddr)
    {
        result = eth_device_ready(&ck_eth_dev1.parent);
    }
    if (result != OS_SUCCESS)
        LOG_I(DBG_TAG, "RxCpltCallback err = %d", result);
}

static void phy_monitor_task_entry(void *parameter)
{
    os_bool_t link     = false;
    os_bool_t new_link = false;

    while (1)
    {

        new_link = PHY_GetLinkStatus((pMacInformation_st)ck_eth_dev0.xemac.state);
        if (link != new_link)
        {
            link = new_link;

            if (link)    // link up
            {
                LOG_D(DBG_TAG, "link up.\n");
                eth_device_linkchange(&ck_eth_dev0.parent, OS_TRUE);
            }
            else
            {
                LOG_D(DBG_TAG, "link down.\n");
                eth_device_linkchange(&ck_eth_dev0.parent, OS_FALSE);
            }
        }
        os_task_msleep(OS_TICK_PER_SECOND * 20);
    }
}

const static struct os_device_ops eth_ops = {
    .init    = os_ck_eth_init,
    .deinit  = os_ck_eth_deinit,
    .read    = os_ck_eth_read,
    .write   = os_ck_eth_write,
    .control = os_ck_eth_control,
};

/**
 ***********************************************************************************************************************
 * @brief           os_hw_ck_eth_init:init and register the EMAC device.
 *
 * @param[in]       none
 *
 * @return          Return eth init status.
 * @retval          ERR_OK            init success.
 * @retval          Others            init failed.
 ***********************************************************************************************************************
 */
static int ck_eth0_probe(void)
{
    os_err_t state;
    /* ck MAC */
    ck_eth_dev0.dev_addr[0]          = 0x00;
    ck_eth_dev0.dev_addr[1]          = 0x04;
    ck_eth_dev0.dev_addr[2]          = 0x9F;
    ck_eth_dev0.dev_addr[3]          = 0x05;
    ck_eth_dev0.dev_addr[4]          = 0x44;
    ck_eth_dev0.dev_addr[5]          = 0xE5;
    ck_eth_dev0.xemac.topology_index = xtopology_find_index(MAC0_BASE_ADDR);
    ck_eth_dev0.xemac.type           = xemac_type_gmac;

    ck_eth_dev0.parent.parent.ops       = &eth_ops;
    ck_eth_dev0.parent.parent.user_data = OS_NULL;
    ck_eth_dev0.parent.eth_rx           = os_ck_eth_rx;
    ck_eth_dev0.parent.eth_tx           = os_ck_eth_tx;

    LOG_D(DBG_TAG, "sem init: tx_wait\r\n");

    /* register eth device */
    LOG_D(DBG_TAG, "eth_device_init start\r\n");
    state = eth_device_init(&(ck_eth_dev0.parent), "e0");
    if (OS_SUCCESS == state)
    {
        LOG_D(DBG_TAG, "eth_device_init success\r\n");
    }
    else
    {
        LOG_D(DBG_TAG, "eth_device_init faild: %d\r\n", state);
    }

    eth_device_linkchange(&ck_eth_dev0.parent, OS_FALSE);

    /* start phy monitor */
    os_task_id tid;
    tid = os_task_create(OS_NULL, OS_NULL, 1024, "phy", phy_monitor_task_entry, OS_NULL, OS_TASK_PRIORITY_MAX - 2);
    if (tid != OS_NULL)
    {
        os_task_startup(tid);
    }
    else
    {
        state = OS_FAILURE;
    }

    return state;
}
OS_INIT_CALL(ck_eth0_probe, OS_INIT_LEVEL_COMPONENT, OS_INIT_SUBLEVEL_LOW);
