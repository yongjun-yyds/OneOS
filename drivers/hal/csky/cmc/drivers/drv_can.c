/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_can.c
 *
 * @brief       This file implements can driver for stm32.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <board.h>
#include <os_errno.h>
#include <os_assert.h>
#include <os_memory.h>
#include <string.h>

#define DRV_EXT_LVL DBG_EXT_INFO
#define DRV_EXT_TAG "drv.can"
#include <drv_log.h>
#include <drv_can.h>
#include "drv_gpio.h"
#include <lib_can.h>

#define BS1SHIFT    16
#define BS2SHIFT    20
#define RRESCLSHIFT 0
#define SJWSHIFT    24
#define BS1MASK     ((0x0F) << BS1SHIFT)
#define BS2MASK     ((0x07) << BS2SHIFT)
#define RRESCLMASK  (0x3FF << RRESCLSHIFT)
#define SJWMASK     (0x3 << SJWSHIFT)

typedef struct ck_can_baud
{
    uint32_t baud;

    uint16_t prescale;
    uint8_t  sjw;
    uint8_t  bs1;
    uint8_t  bs2;
} ck_can_baud_t;

struct ck_can
{
    struct os_can_device can;
    pStruct_CANInfo      hcan;
    ck_can_info_t       *info;
    ck_can_baud_t        baud;
    struct os_can_msg   *rx_msg;
    os_list_node_t       list;
};

static os_list_node_t gs_ck_can_list = OS_LIST_INIT(gs_ck_can_list);

static void ck_can_hard_init(ck_can_info_t *info)
{
    GPIO_InitStructure GPIO_InitInfo;
    uint32_t        level = 0;

    GPIO_InitInfo.GPIOAddr      = info->GPIOAddr;
    GPIO_InitInfo.GPIOPort      = info->GPIOPort;
    GPIO_InitInfo.GPIOPin       = info->tx_pin | info->rx_pin | info->can_busoff;
    GPIO_InitInfo.bHardwareMode = TRUE;
    GPIO_Config(&GPIO_InitInfo);
    os_spin_lock_irqsave(&gs_device_lock, &level);
    CAN_Config(&info->can_init);
    os_spin_unlock_irqrestore(&gs_device_lock, level);
}

static struct ck_can *find_ck_can(pStruct_CANInfo hcan)
{
    struct ck_can *ck_can;

    os_list_for_each_entry(ck_can, &gs_ck_can_list, struct ck_can, list)
    {
        if (ck_can->hcan == hcan)
        {
            return ck_can;
        }
    }
    return OS_NULL;
}

static os_err_t ck_can_config(struct os_can_device *can, struct can_configure *cfg)
{
    struct ck_can  *ck_can;
    pStruct_CANInit hcan;
    uint32_t     level = 0;

    OS_ASSERT(can);
    OS_ASSERT(cfg);
    ck_can = (struct ck_can *)can;
    hcan   = &(ck_can->info->can_init);

    switch (cfg->mode)
    {
    default:
        break;
    }
    ck_can->baud.baud = cfg->baud_rate;
    hcan->buadrate    = cfg->baud_rate;

    os_spin_lock_irqsave(&gs_device_lock, &level);
    CAN_Config(hcan);
    os_spin_unlock_irqrestore(&gs_device_lock, level);

    return OS_SUCCESS;
}

static os_err_t ck_can_control(struct os_can_device *can, int cmd, void *arg)
{
    uint32_t                  argval;
    struct ck_can               *ck_can;
    struct os_can_filter_config *filter_cfg;

    OS_ASSERT(can != OS_NULL);
    ck_can = (struct ck_can *)can;

    switch (cmd)
    {
    case OS_CAN_CMD_SET_FILTER:
        if (OS_NULL == arg)
        {
        }
        else
        {
            filter_cfg                    = (struct os_can_filter_config *)arg;
            ck_can->info->can_init.canacr = filter_cfg->items[0].id >> 3;
            ck_can->info->can_init.canamr = filter_cfg->items[0].mask;
            ck_can_config(can, &can->config);
        }
        break;
    case OS_CAN_CMD_SET_MODE:

        break;
    case OS_CAN_CMD_SET_BAUD:
        argval = (uint32_t)arg;
        if (argval != can->config.baud_rate)
        {
            can->config.baud_rate = argval;
            return ck_can_config(can, &can->config);
        }
        break;
    case OS_CAN_CMD_SET_PRIV:
        argval = (uint32_t)arg;
        if (argval != OS_CAN_MODE_PRIV && argval != OS_CAN_MODE_NOPRIV)
        {
            return OS_FAILURE;
        }
        if (argval != can->config.privmode)
        {
            can->config.privmode = argval;
            return ck_can_config(can, &can->config);
        }
        break;
    case OS_CAN_CMD_GET_STATUS:
        uint8_t errtype;
        errtype             = CAN_GetStatus(ck_can->hcan);
        can->status.errcode = errtype & 0x40;
        memcpy(arg, &can->status, sizeof(can->status));
        break;
    }

    return OS_SUCCESS;
}

static int ck_can_recvmsg(struct os_can_device *can, struct os_can_msg *pmsg, uint32_t fifo)
{
    struct ck_can  *ck_can;
    int8_t       status;
    pStruct_CANInfo hcan;
    CANFrameInfo_st rxheader = {0};

    OS_ASSERT(can);
    ck_can = (struct ck_can *)can;
    hcan   = ck_can->hcan;

    /* get data */
    status = CAN_ReceiveFrame(hcan, &rxheader);
    if (OS_SUCCESS != status)
        return OS_FAILURE;
    /* get id */
    pmsg->ide = rxheader.FF;
    pmsg->id  = rxheader.ID;
    /* get type */
    if (0 == rxheader.RTR)
    {
        pmsg->rtr = OS_CAN_DTR;
    }
    else
    {
        pmsg->rtr = OS_CAN_RTR;
    }
    /* get len */
    pmsg->len = rxheader.DLC;
    memcpy(pmsg->data, rxheader.Data, rxheader.DLC);
    /* get hdr */
    pmsg->hdr = 0;

    return OS_SUCCESS;
}

static int ck_can_start_send(struct os_can_device *can, const struct os_can_msg *pmsg)
{
    struct ck_can  *ck_can;
    pStruct_CANInfo hcan;

    CANFrameInfo_st txheader = {0};
    OS_ASSERT(can);
    ck_can = (struct ck_can *)can;

    hcan = ck_can->hcan;

    /* identification bit and CAN ID */
    txheader.FF = pmsg->ide;
    txheader.ID = pmsg->id;

    if (OS_CAN_DTR == pmsg->rtr)
    {
        txheader.RTR = 0;
    }
    else
    {
        txheader.RTR = 1;
    }

    txheader.DLC = pmsg->len;

    memcpy(txheader.Data, pmsg->data, txheader.DLC);

    hcan->CAN_Initstruct.intrVector |= CAN_IT_TIE;
    CAN_ITConfig(hcan, hcan->CAN_Initstruct.intrVector);

    uint8_t ret = CAN_Transmit(hcan, &txheader);
    if (ret != OS_SUCCESS)
    {
        os_kprintf("%s hal send failed.\r\n", device_name(&can->parent), ret);
        return -1;
    }

    return 0;
}

static int ck_can_stop_send(struct os_can_device *can)
{
    struct ck_can  *ck_can;
    pStruct_CANInfo hcan;

    OS_ASSERT(can);
    ck_can = (struct ck_can *)can;
    hcan   = ck_can->hcan;
    OS_ASSERT(hcan);

    /* �ж����� */
    hcan->CAN_Initstruct.intrVector &= (~CAN_IT_TIE);
    CAN_ITConfig(hcan, hcan->CAN_Initstruct.intrVector);

    return 0;
}

static int ck_can_start_recv(struct os_can_device *can, struct os_can_msg *msg)
{
    struct ck_can  *ck_can;
    pStruct_CANInfo hcan;
    OS_ASSERT(can);
    ck_can = (struct ck_can *)can;
    hcan   = ck_can->hcan;
    OS_ASSERT(hcan);

    ck_can->rx_msg = msg;
    hcan->CAN_Initstruct.intrVector |= CAN_IT_RIE;
    CAN_ITConfig(hcan, hcan->CAN_Initstruct.intrVector);

    return 0;
}

static int ck_can_stop_recv(struct os_can_device *can)
{
    struct ck_can  *ck_can;
    pStruct_CANInfo hcan;
    OS_ASSERT(can);
    ck_can = (struct ck_can *)can;
    hcan   = ck_can->hcan;
    OS_ASSERT(hcan);

    hcan->CAN_Initstruct.intrVector &= (~CAN_IT_RIE);
    CAN_ITConfig(hcan, hcan->CAN_Initstruct.intrVector);

    return 0;
}

static int ck_can_recv_state(struct os_can_device *can)
{
    return 0;
}

static const struct os_can_ops ck_can_ops = {
    .configure  = ck_can_config,
    .control    = ck_can_control,
    .start_send = ck_can_start_send,
    .stop_send  = ck_can_stop_send,
    .start_recv = ck_can_start_recv,
    .stop_recv  = ck_can_stop_recv,
    .recv_state = ck_can_recv_state,
};

void HAL_CAN_TxCallback(pStruct_CANInfo hcan)
{
    os_hw_can_isr_txdone((struct os_can_device *)find_ck_can(hcan), OS_CAN_EVENT_TX_DONE);
}

void HAL_CAN_RxCallback(pStruct_CANInfo hcan)
{
    struct ck_can *ck_can = find_ck_can(hcan);

    if (ck_can->rx_msg == OS_NULL)
        return;

    ck_can_recvmsg(&ck_can->can, ck_can->rx_msg, 0);
    ck_can->rx_msg = OS_NULL;

    os_hw_can_isr_rxdone((struct os_can_device *)ck_can);
}

void CAN0_IRQHandler(UINT32 dwIrqid)
{
    UINT8          ITVector        = 0;
    BOOL           PacketAvailable = FALSE;
    struct ck_can *ck_can;

    ck_can = find_ck_can(pCAN0);
    CAN_GetITVector(ck_can->hcan, &ITVector); /* ��ȡ�ж����� */

    if (ITVector & CAN_IT_TI) /* send irq */
    {
        HAL_CAN_TxCallback(ck_can->hcan);
    }
    if (ITVector & CAN_IT_RI) /* rev irq */
    {
        do
        {
            HAL_CAN_RxCallback(ck_can->hcan);

            /* �ж��Ƿ��������ڻ����� */
            if (ck_can->hcan->CANMode == CAN_MODE_STANDARD)
            {
                PacketAvailable = (ck_can->hcan->pCAN->SR & 0x01) ? TRUE : FALSE;
            }
            else
            {
                PacketAvailable = (ck_can->hcan->pCAN_Ex->RMC) ? TRUE : FALSE;
            }
        } while (PacketAvailable);
    }

    if (ITVector & CAN_IT_OI) /* ����ж� */
    {
        CAN_ReleaseFIFO(pCAN0);
    }
}

void CAN1_IRQHandler(UINT32 dwIrqid)
{
    UINT8          ITVector        = 0;
    BOOL           PacketAvailable = FALSE;
    struct ck_can *ck_can;

    ck_can = find_ck_can(pCAN1);
    CAN_GetITVector(ck_can->hcan, &ITVector); /* ��ȡ�ж����� */

    if (ITVector & CAN_IT_TI) /* �����ж� */
    {
        HAL_CAN_TxCallback(ck_can->hcan);
    }
    if (ITVector & CAN_IT_RI) /* �����ж� */
    {
        do
        {
            HAL_CAN_RxCallback(ck_can->hcan);

            /* �ж��Ƿ��������ڻ����� */
            if (ck_can->hcan->CANMode == CAN_MODE_STANDARD)
            {
                PacketAvailable = (ck_can->hcan->pCAN->SR & 0x01) ? TRUE : FALSE;
            }
            else
            {
                PacketAvailable = (ck_can->hcan->pCAN_Ex->RMC) ? TRUE : FALSE;
            }
        } while (PacketAvailable);
    }

    if (ITVector & CAN_IT_OI) /* ����ж� */
    {
        CAN_ReleaseFIFO(pCAN1);
    }
}

static int ck_can_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    struct can_configure config = CANDEFAULTCONFIG;

    os_ubase_t level;

    struct ck_can *ck_can = os_calloc(1, sizeof(struct ck_can));

    OS_ASSERT(ck_can);

    ck_can_hard_init((ck_can_info_t *)dev->info);
    ck_can->info = (ck_can_info_t *)dev->info;

    if (ck_can->info->can_init.pCAN == (pStruct_CAN)CAN0_BASE_ADDR)
        ck_can->hcan = pCAN0;
    else
        ck_can->hcan = pCAN1;

    config.privmode = OS_CAN_MODE_NOPRIV;
    config.ticks    = 50;
#ifdef OS_CAN_USING_HDR
    config.maxhdr = 14;
#ifdef CAN2
    config.maxhdr = 28;
#endif
#endif

    ck_can->can.config = config;

    os_spin_lock_irqsave(&gs_device_lock, &level);
    os_list_add_tail(&gs_ck_can_list, &ck_can->list);
    os_spin_unlock_irqrestore(&gs_device_lock, level);

    os_hw_can_register(&ck_can->can, dev->name, &ck_can_ops, ck_can);

    return OS_SUCCESS;
}

OS_DRIVER_INFO ck_can_driver = {
    .name  = "Struct_CANInit",
    .probe = ck_can_probe,
};

OS_DRIVER_DEFINE(ck_can_driver, OS_INIT_LEVEL_PRE_DEVICE, OS_INIT_SUBLEVEL_LOW);

