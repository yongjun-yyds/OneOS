/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_rtc.c
 *
 * @brief       This file implements RTC driver for gd32.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <board.h>
#include <os_memory.h>
#include <time.h>
#include <drv_log.h>
#include "drv_rtc.h"

#define DBG_TAG "drv.rtc"
#include <drv_log.h>
#include <dlog.h>

#define RTC_TIME_S (946686600) /* 2020-01-01 8:30:00 */

struct csky_rtc
{
    os_device_t     rtc;
    pStruct_RTCInfo ck_rtc;
};

uint8_t Hex_To_ByteDec(uint8_t PuB_Dat)
{
    uint8_t dat;
    dat = (PuB_Dat / 16) * 10 + PuB_Dat % 16;
    return dat;
}

uint8_t IntDec_To_Hex(int PuB_Dat)
{
    uint8_t dat;
    dat = (PuB_Dat / 10) * 16 + PuB_Dat % 10;
    return dat;
}

static time_t ck_rtc_get_timestamp(void)
{
    time_t      sec;
    uint32_t level = 0;
    os_spin_lock_irqsave(&gs_device_lock, &level);
    sec               = RTC_GetSeconds();
    os_spin_unlock_irqrestore(&gs_device_lock, level);
    return sec;
}

static os_err_t ck_rtc_set_time_stamp(time_t time_stamp)
{
    uint32_t level = 0;
    os_spin_lock_irqsave(&gs_device_lock, &level);
    Struct_TimeInfo Time_t;
    RTC_SecondToTime(time_stamp, &Time_t);
    RTC_SetRealTime(&Time_t);
    os_spin_unlock_irqrestore(&gs_device_lock, level);

    return OS_SUCCESS;
}

static void ck_rtc_init(pStruct_RTCInfo ck_rtc)
{
    uint32_t level = 0;
    os_spin_lock_irqsave(&gs_device_lock, &level);
    ck_rtc_set_time_stamp(RTC_TIME_S);
    os_spin_unlock_irqrestore(&gs_device_lock, level);
}

static os_err_t os_rtc_control(os_device_t *dev, int cmd, void *args)
{
    os_err_t result = OS_FAILURE;
    OS_ASSERT(dev != OS_NULL);

    switch (cmd)
    {
    case OS_DEVICE_CTRL_RTC_GET_TIME:
        *(uint32_t *)args = ck_rtc_get_timestamp();
        LOG_D(DBG_TAG, "RTC: get rtc_time %x\n", *(uint32_t *)args);
        result = OS_SUCCESS;
        break;

    case OS_DEVICE_CTRL_RTC_SET_TIME:
        if (ck_rtc_set_time_stamp(*(uint32_t *)args))
        {
            result = OS_FAILURE;
        }
        LOG_D(DBG_TAG, "RTC: set rtc_time %x\n", *(uint32_t *)args);
        result = OS_SUCCESS;
        break;
    }

    return result;
}

const static struct os_device_ops rtc_ops = {
    .control = os_rtc_control,
};

static int ck_rtc_probe(void)
{
    struct csky_rtc *csky_rtc;

    csky_rtc = os_calloc(1, sizeof(struct csky_rtc));

    OS_ASSERT(csky_rtc);

    csky_rtc->ck_rtc = RTC;

    ck_rtc_init(csky_rtc->ck_rtc);

    csky_rtc->rtc.type = OS_DEVICE_TYPE_RTC;
    csky_rtc->rtc.ops  = &rtc_ops;

    return os_device_register(&csky_rtc->rtc, "rtc");
}

OS_INIT_CALL(ck_rtc_probe, OS_INIT_LEVEL_DEVICE, OS_INIT_SUBLEVEL_LOW);
