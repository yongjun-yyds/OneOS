/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_gpio.c
 *
 * @brief       This file implements gpio driver for CK.
 *
 * @revision
 * Date         Author          Notes
 * 2020-07-08   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <drv_gpio.h>
#include <device.h>
#include <os_memory.h>
#include <os_errno.h>
#include <pin/pin.h>
#include <bsp.h>

#ifdef OS_USING_PIN

OS_INLINE void pin_irq_hdr(int irqno);

#define __CK_PIN(index, gpio, port, gpio_index)                                                                        \
    {                                                                                                                  \
        index, GPIO##gpio, PORT##port, GPIO_P##gpio_index                                                              \
    }

static const struct pin_index pins[] = {
#if defined(GPIO0)
    __CK_PIN(0, 0, A, 0),   __CK_PIN(1, 0, A, 1),   __CK_PIN(2, 0, A, 2),   __CK_PIN(3, 0, A, 3),
    __CK_PIN(4, 0, B, 0),   __CK_PIN(5, 0, B, 1),   __CK_PIN(6, 0, B, 2),   __CK_PIN(7, 0, B, 3),
    __CK_PIN(8, 0, B, 4),   __CK_PIN(9, 0, B, 5),   __CK_PIN(10, 0, B, 6),  __CK_PIN(11, 0, B, 7),
    __CK_PIN(12, 0, B, 8),  __CK_PIN(13, 0, B, 9),  __CK_PIN(14, 0, B, 10), __CK_PIN(15, 0, B, 11),
    __CK_PIN(16, 0, B, 12), __CK_PIN(17, 0, B, 13), __CK_PIN(18, 0, B, 14), __CK_PIN(19, 0, B, 15),
#if defined(GPIO1)
    __CK_PIN(20, 1, A, 0),  __CK_PIN(21, 1, A, 1),  __CK_PIN(22, 1, A, 2),  __CK_PIN(23, 1, A, 3),
    __CK_PIN(24, 1, A, 4),  __CK_PIN(25, 1, A, 5),  __CK_PIN(26, 1, A, 6),  __CK_PIN(27, 1, A, 7),
    __CK_PIN(28, 1, A, 8),  __CK_PIN(29, 1, A, 9),  __CK_PIN(30, 1, A, 10), __CK_PIN(31, 1, A, 11),
    __CK_PIN(32, 1, A, 12), __CK_PIN(33, 1, A, 13), __CK_PIN(34, 1, A, 14), __CK_PIN(35, 1, A, 15),
    __CK_PIN(36, 1, A, 16), __CK_PIN(37, 1, A, 17), __CK_PIN(38, 1, A, 18), __CK_PIN(39, 1, A, 19),
    __CK_PIN(40, 1, A, 20), __CK_PIN(41, 1, A, 21), __CK_PIN(42, 1, A, 22), __CK_PIN(43, 1, A, 23),
    __CK_PIN(44, 1, A, 24), __CK_PIN(45, 1, A, 25), __CK_PIN(46, 1, A, 26), __CK_PIN(47, 1, A, 27),
    __CK_PIN(48, 1, A, 28), __CK_PIN(49, 1, A, 29), __CK_PIN(50, 1, A, 30), __CK_PIN(51, 1, A, 31),

    __CK_PIN(52, 1, B, 0),  __CK_PIN(53, 1, B, 1),  __CK_PIN(54, 1, B, 2),  __CK_PIN(55, 1, B, 3),
    __CK_PIN(56, 1, B, 4),  __CK_PIN(57, 1, B, 5),  __CK_PIN(58, 1, B, 6),  __CK_PIN(59, 1, B, 7),
    __CK_PIN(60, 1, B, 8),  __CK_PIN(61, 1, B, 9),  __CK_PIN(62, 1, B, 10), __CK_PIN(63, 1, B, 11),
    __CK_PIN(64, 1, B, 12), __CK_PIN(65, 1, B, 13), __CK_PIN(66, 1, B, 14), __CK_PIN(67, 1, B, 15),
    __CK_PIN(68, 1, B, 16), __CK_PIN(69, 1, B, 17), __CK_PIN(70, 1, B, 18), __CK_PIN(71, 1, B, 19),
    __CK_PIN(72, 1, B, 20), __CK_PIN(73, 1, B, 21), __CK_PIN(74, 1, B, 22), __CK_PIN(75, 1, B, 23),
    __CK_PIN(76, 1, B, 24), __CK_PIN(77, 1, B, 25), __CK_PIN(78, 1, B, 26), __CK_PIN(79, 1, B, 27),
    __CK_PIN(80, 1, B, 28), __CK_PIN(81, 1, B, 29), __CK_PIN(82, 1, B, 30), __CK_PIN(83, 1, B, 31),
#endif /* defined(GPIO1) */
#endif /* defined(GPIO0) */
};

/*
 *     1.��ʹ�õ�ƽ�����ж�ʱ����Ҫ�ڴ˺����ڽ����жϣ��Ҳ����ٴδ��жϣ�����
 *     �ᷴ��������жϣ�ʵ��Ӧ��ʱ����Ҫ�ĵط�����ʹ���жϡ�
 *     GPIO_DisableInterrupt(GPIO0, GPIO_P0);         �����ж�
 *   GPIO_EnableInterrupt(GPIO0, GPIO_P0);            ʹ���ж�
 *     2.��ʹ�ñ��ش���ʱ��ֻ������жϱ�־���㲻�ᷴ�������жϡ�
 */
void GPIO0_A_Pin_IRQHandler(UINT32 irqid)
{
    if (GPIO0->INTSTATUS & GPIO_P0)
    {
        GPIO_ClearInterrupt(GPIO0, GPIO_P0); /* ����жϱ�־ */
        pin_irq_hdr(0);
    }
    if (GPIO0->INTSTATUS & GPIO_P1)
    {
        GPIO_ClearInterrupt(GPIO0, GPIO_P1); /* ����жϱ�־ */
        pin_irq_hdr(1);
    }
    if (GPIO0->INTSTATUS & GPIO_P2)
    {
        GPIO_ClearInterrupt(GPIO0, GPIO_P2); /* ����жϱ�־ */
        pin_irq_hdr(2);
    }
    if (GPIO0->INTSTATUS & GPIO_P3)
    {
        GPIO_ClearInterrupt(GPIO0, GPIO_P3); /* ����жϱ�־ */
        pin_irq_hdr(3);
    }
}

void GPIO1_A_Pin_IRQHandler(UINT32 irqid)
{
    if (GPIO1->INTSTATUS & GPIO_P0)
    {
        GPIO_ClearInterrupt(GPIO1, GPIO_P0); /* ����жϱ�־ */
        pin_irq_hdr(4);
    }
}

static const struct pin_irq_map ck_pin_irq_map[] = {
    {0, GPIO0_A_Pin_IRQHandler},
    {1, GPIO0_A_Pin_IRQHandler},
    {2, GPIO0_A_Pin_IRQHandler},
    {3, GPIO0_A_Pin_IRQHandler},
    {4, GPIO1_A_Pin_IRQHandler},
};

static struct os_pin_irq_hdr pin_irq_hdr_tab[] = {
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
};

#define ITEM_NUM(items) sizeof(items) / sizeof(items[0])

struct pin_index *get_pin_index(uint8_t pin)
{
    struct pin_index *index;

    if (pin < ITEM_NUM(pins))
    {
        index = &pins[pin];
        if (index->index == -1)
            index = OS_NULL;
    }
    else
    {
        index = OS_NULL;
    }

    return index;
}

OS_INLINE int32_t get_irqno(const struct pin_index *index)
{
    if (index->gpio_port == PORTA)
    {
        if (index->gpio_num == GPIO0)
        {
            return (index->index);
        }
        else if (index->gpio_num == GPIO1)
        {
            return 4;
        }
        else
            return -1;
    }
    else
        return -1;
}

OS_INLINE void pin_irq_hdr(int irqno)
{
    if (pin_irq_hdr_tab[irqno].hdr)
    {
        pin_irq_hdr_tab[irqno].hdr(pin_irq_hdr_tab[irqno].args);
    }
}

OS_INLINE const struct pin_irq_map *get_pin_index_irq_map(uint32_t pinbit)
{
    if (pinbit < 0 || pinbit >= ITEM_NUM(ck_pin_irq_map))
    {
        return OS_NULL;
    }
    return &ck_pin_irq_map[pinbit];
}

static void CK_pin_write(struct os_device *dev, os_base_t pin, os_base_t value)
{
    const struct pin_index *index;

    index = get_pin_index(pin);
    if (index == OS_NULL)
    {
        return;
    }

    GPIO_WritePins(index->gpio_num, index->gpio_port, index->pin, value);
}

static int CK_pin_read(struct os_device *dev, os_base_t pin)
{
    int                     value = PIN_LOW;
    const struct pin_index *index;
    index = get_pin_index(pin);
    if (index == OS_NULL)
    {
        return value;
    }
    if (index->gpio_port == PORTA)
    {
        if (index->gpio_num->SWPORTA_DDR & index->pin)
            value = GPIO_ReadOutputPinData(index->gpio_num, index->gpio_port, index->pin);
        else
            value = GPIO_ReadInputPinData(index->gpio_num, index->gpio_port, index->pin);
    }
    else
    {
        if (index->gpio_num->SWPORTB_DDR & index->pin)
            value = GPIO_ReadOutputPinData(index->gpio_num, index->gpio_port, index->pin);
        else
            value = GPIO_ReadInputPinData(index->gpio_num, index->gpio_port, index->pin);
    }
    return value;
}

static void CK_pin_mode(struct os_device *dev, os_base_t pin, os_base_t mode)
{
    const struct pin_index *index;
    GPIO_InitStructure      GPIO_InitInfo;

    index = get_pin_index(pin);
    if (index == OS_NULL)
    {
        return;
    }

    GPIO_InitInfo.GPIOAddr = index->gpio_num;
    GPIO_InitInfo.GPIOPort = index->gpio_port;
    GPIO_InitInfo.GPIOPin  = index->pin;

    if (mode == PIN_MODE_OUTPUT || mode == PIN_MODE_OUTPUT_OD)
    {
        /* output setting */
        GPIO_InitInfo.GPIODirection = GPIO_OUTPUT;
    }
    else if (mode == PIN_MODE_INPUT)
    {
        /* input setting: not pull. */
        GPIO_InitInfo.GPIODirection = GPIO_INPUT;
    }

    else if (mode == PIN_MODE_DISABLE)
    {
    }

    GPIO_InitInfo.bHardwareMode = FALSE;
    GPIO_InitInfo.GPIOIntr      = GPIO_NULL_INTR;

    GPIO_Config(&GPIO_InitInfo);
}

static os_err_t
CK_pin_attach_irq(struct os_device *device, int32_t pin, uint32_t mode, void (*hdr)(void *args), void *args)
{
    const struct pin_index *index;
    os_ubase_t               level;
    int32_t              irqindex = -1;

    index = get_pin_index(pin);
    if (index == OS_NULL)
    {
        return OS_NOSYS;
    }
    irqindex = get_irqno(index);
    if (irqindex < 0 || irqindex >= ITEM_NUM(pin_irq_hdr_tab))
    {
        return OS_NOSYS;
    }

    os_spin_lock_irqsave(&gs_device_lock, &level);
    if (pin_irq_hdr_tab[irqindex].pin == pin && pin_irq_hdr_tab[irqindex].hdr == hdr &&
        pin_irq_hdr_tab[irqindex].mode == mode && pin_irq_hdr_tab[irqindex].args == args)
    {
        os_spin_unlock_irqrestore(&gs_device_lock, level);
        return OS_SUCCESS;
    }
    if (pin_irq_hdr_tab[irqindex].pin != -1)
    {
        os_spin_unlock_irqrestore(&gs_device_lock, level);
        return OS_BUSY;
    }
    pin_irq_hdr_tab[irqindex].pin  = pin;
    pin_irq_hdr_tab[irqindex].hdr  = hdr;
    pin_irq_hdr_tab[irqindex].mode = mode;
    pin_irq_hdr_tab[irqindex].args = args;
    os_spin_unlock_irqrestore(&gs_device_lock, level);

    return OS_SUCCESS;
}

static os_err_t CK_pin_dettach_irq(struct os_device *device, int32_t pin)
{
    const struct pin_index *index;
    os_ubase_t               level;
    int32_t              irqindex = -1;

    index = get_pin_index(pin);
    if (index == OS_NULL)
    {
        return OS_NOSYS;
    }
    irqindex = get_irqno(index);
    if (irqindex < 0 || irqindex >= ITEM_NUM(pin_irq_hdr_tab))
    {
        return OS_NOSYS;
    }
    os_spin_lock_irqsave(&gs_device_lock, &level);
    if (pin_irq_hdr_tab[irqindex].pin == -1)
    {
        os_spin_unlock_irqrestore(&gs_device_lock, level);
        return OS_SUCCESS;
    }
    pin_irq_hdr_tab[irqindex].pin  = -1;
    pin_irq_hdr_tab[irqindex].hdr  = OS_NULL;
    pin_irq_hdr_tab[irqindex].mode = 0;
    pin_irq_hdr_tab[irqindex].args = OS_NULL;
    os_spin_unlock_irqrestore(&gs_device_lock, level);

    return OS_SUCCESS;
}

static os_err_t CK_pin_irq_enable(struct os_device *device, os_base_t pin, uint32_t enabled)
{
    const struct pin_index *index;
    os_ubase_t               level;
    int32_t              irqindex = -1;
    GPIO_InitStructure      GPIO_InitInfo;

    index = get_pin_index(pin);
    if (index == OS_NULL)
    {
        return OS_NOSYS;
    }

    if (enabled == PIN_IRQ_ENABLE)
    {
        os_spin_lock_irqsave(&gs_device_lock, &level);
        irqindex = get_irqno(index);
        if (irqindex < 0 || irqindex >= ITEM_NUM(pin_irq_hdr_tab))
        {
            os_spin_unlock_irqrestore(&gs_device_lock, level);
            return OS_NOSYS;
        }

        if (pin_irq_hdr_tab[irqindex].pin == -1)
        {
            os_spin_unlock_irqrestore(&gs_device_lock, level);
            return OS_NOSYS;
        }

        /* Configure pGPIOInfo */
        GPIO_InitInfo.GPIOAddr      = index->gpio_num;
        GPIO_InitInfo.GPIOPort      = index->gpio_port;
        GPIO_InitInfo.GPIOPin       = index->pin;
        GPIO_InitInfo.GPIODirection = GPIO_INPUT;
        GPIO_InitInfo.bHardwareMode = false;
        GPIO_InitInfo.GPIOIntr      = irqindex;
        if (irqindex == GPIO1A_INTR)
            GPIO_InitInfo.priority = GPIO1A_PRIO;
        else
            GPIO_InitInfo.priority = GPIO0A_0_PRIO;

        switch (pin_irq_hdr_tab[irqindex].mode)
        {
        case PIN_IRQ_MODE_RISING:
            GPIO_InitInfo.BurstMode = GPIO_Intact_High_Edge;
            break;
        case PIN_IRQ_MODE_FALLING:
            GPIO_InitInfo.BurstMode = GPIO_Intact_Low_Edge;
            break;
        case PIN_IRQ_MODE_HIGH_LEVEL:
            GPIO_InitInfo.BurstMode = GPIO_Intact_High_Level;
            break;
        case PIN_IRQ_MODE_LOW_LEVEL:
            GPIO_InitInfo.BurstMode = GPIO_Intact_Low_Level;
            break;
        default:
            os_spin_unlock_irqrestore(&gs_device_lock, level);
            return OS_FAILURE;
        }
        GPIO_InitInfo.GPIO_ISR = ck_pin_irq_map[irqindex].handler;

        GPIO_Config(&GPIO_InitInfo);
        os_spin_unlock_irqrestore(&gs_device_lock, level);
    }
    else if (enabled == PIN_IRQ_DISABLE)
    {
        os_spin_lock_irqsave(&gs_device_lock, &level);
        GPIO_DisableInterrupt(index->gpio_num, index->pin);
        os_spin_unlock_irqrestore(&gs_device_lock, level);
    }
    else
    {
        return OS_NOSYS;
    }

    return OS_SUCCESS;
}

const static struct os_pin_ops _CK_pin_ops = {
    .pin_mode       = CK_pin_mode,
    .pin_write      = CK_pin_write,
    .pin_read       = CK_pin_read,
    .pin_attach_irq = CK_pin_attach_irq,
    .pin_detach_irq = CK_pin_dettach_irq,
    .pin_irq_enable = CK_pin_irq_enable,
};

/**
 ***********************************************************************************************************************
 * @brief           os_hw_pin_init:enable gpio clk,register pin device.
 *
 * @param[in]       none
 *
 * @return          Return init result.
 * @retval          OS_SUCCESS       init success.
 * @retval          Others       init failed.
 ***********************************************************************************************************************
 */
int os_hw_pin_init(void)
{
    return os_device_pin_register(0, &_CK_pin_ops, OS_NULL);
}

#endif
