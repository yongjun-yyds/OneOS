/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_can.h
 *
 * @brief        This file provides functions declaration for can driver.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __DRV_CAN_H__
#define __DRV_CAN_H__

#include <lib_can.h>
#include <os_task.h>
#include <device.h>
#include <drv_cfg.h>
#include <ckgpio.h>

/* csky can driver */
typedef struct ck_can_info
{

    pStruct_GPIOInfo GPIOAddr;   /* Gpio����ַ */
    Enum_GPIO_Port   GPIOPort;   /* PORT */
    UINT32           tx_pin;     /* ���ź� */
    UINT32           rx_pin;     /* ���ź� */
    UINT32           can_busoff; /* ���ź� */
    Struct_CANInit   can_init;
} ck_can_info_t;

#endif /* __DRV_CAN_H__ */

/******************* end of file *******************/
