/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_hwtimer.c
 *
 * @brief       This file implements timer driver for csky.
 *
 * @revision
 * Date         Author          Notes
 * 2021-04-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */
//#pragma GCC push_options
//#pragma GCC optimize ("O0")

#include <os_memory.h>
#include <timer/timer.h>
#include <bus/bus.h>

#ifdef OS_USING_CLOCKSOURCE
#include <timer/clocksource.h>
#endif

#ifdef OS_USING_CLOCKEVENT
#include <timer/clockevent.h>
#endif

#include <drv_common.h>
#include <drv_hwtimer.h>

#define DRV_EXT_LVL DBG_EXT_INFO
#define DRV_EXT_TAG "drv.hwtimer"
#include <drv_log.h>

static os_list_node_t csky_timer_list = OS_LIST_INIT(csky_timer_list);

os_bool_t csky_timer_is_32b(Enum_Timer_Device Instance)
{
    return OS_TRUE;
}

static uint32_t csky_timer_get_freq(Enum_Timer_Device htim)
{
    //    OS_ASSERT(htim > TIMER3);

    return 1000000;
}

static void TIM_Callback(struct csky_timer *timer)
{
    Timer_ClearIrqFlag(timer->info->enTimerID);
    if (timer->info->enTimerID != 0)
    {
#ifdef OS_USING_CLOCKEVENT
        os_clockevent_isr((os_clockevent_t *)timer);
#endif
    }
}
// void TIMn_IRQHandler(UINT32 irqid) __attribute__((optimize("O0")));
void TIMn_IRQHandler(UINT32 irqid)
{

    struct csky_timer *timer;
    os_list_for_each_entry(timer, &csky_timer_list, struct csky_timer, list)
    {
        if (Timer_GetIntStatus(timer->info->enTimerID))
        {
            TIM_Callback(timer);
        }
    }
}

#if 0
extern void os_tick_handler(void);
void TIM2_IRQHandler(UINT32 irqid)
{
    Timer_ClearIrqFlag(TIMER2);
    os_tick_handler();
}
#endif

#if defined(OS_USING_CLOCKSOURCE) || defined(OS_USING_CLOCKEVENT)
static uint64_t csky_timer_read(void *clock)
{
    struct csky_timer *timer;
    uint64_t        timer_count = 0;
    timer                          = (struct csky_timer *)clock;
    os_ubase_t level               = 0;
    os_spin_lock_irqsave(&gs_device_lock, &level);
    timer_count                    = Timer_CurrentValue(timer->info->enTimerID) / (APB_FREQ / 1000000);
    timer_count                    = timer->info->dwTimeout - timer_count;
    os_spin_unlock_irqrestore(&gs_device_lock, level);
    return timer_count;
}
#endif

#ifdef OS_USING_CLOCKEVENT
static void csky_timer_start(os_clockevent_t *ce, uint32_t prescaler, uint64_t count)
{
    struct csky_timer *timer;
    os_ubase_t         level = 0;
    OS_ASSERT(prescaler != 0);
    OS_ASSERT(count != 0);
    timer                  = (struct csky_timer *)ce;
    os_spin_lock_irqsave(&gs_device_lock, &level);
    timer->info->dwTimeout = count;
    Timer_Config(timer->info);
    os_spin_unlock_irqrestore(&gs_device_lock, level);
}

static void csky_timer_stop(os_clockevent_t *ce)
{
    struct csky_timer *timer;
    OS_ASSERT(ce != OS_NULL);
    os_ubase_t level = 0;

    timer = (struct csky_timer *)ce;

    Enum_Timer_Device htim = timer->info->enTimerID;
    os_spin_lock_irqsave(&gs_device_lock, &level);
    Timer_Stop(htim);
    Timer_ClearIrqFlag(htim);
    os_spin_unlock_irqrestore(&gs_device_lock, level);
}

static const struct os_clockevent_ops csky_tim_ops = {
    .start = csky_timer_start,
    .stop  = csky_timer_stop,
    .read  = csky_timer_read,
};
#endif

static int csky_tim_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    struct csky_timer *timer;
    os_ubase_t         level;

    timer = os_calloc(1, sizeof(struct csky_timer));
    OS_ASSERT(timer);

    timer->info = (pTimer_InitStructure)dev->info;

    Enum_Timer_Device htim = timer->info->enTimerID;
    os_kprintf("htim:%d\r\n", htim);
    timer->freq = csky_timer_get_freq(htim);

#ifdef OS_USING_CLOCKSOURCE
    if (os_clocksource_best() == OS_NULL && csky_timer_is_32b(htim))
    {
        timer->info->dwTimeout = 0x3fffffful;
        os_spin_lock_irqsave(&gs_device_lock, &level);
        Timer_Config(timer->info);
        os_spin_unlock_irqrestore(&gs_device_lock, level);
        timer->clock.cs.rating = 260;
        timer->clock.cs.freq   = timer->freq;
        timer->clock.cs.mask   = 0x3fffffful;
        timer->clock.cs.read   = csky_timer_read;

        os_clocksource_register(dev->name, &timer->clock.cs);
    }

    else
#endif
    {
#ifdef OS_USING_CLOCKEVENT

        timer->clock.ce.rating = 260;
        timer->clock.ce.freq   = timer->freq;
        timer->clock.ce.mask   = 0x3fffffful;

        timer->clock.ce.prescaler_mask = 1;    // 0xfffful;
        timer->clock.ce.prescaler_bits = 0;

        timer->clock.ce.count_mask = 0x3fffffful;
        timer->clock.ce.count_bits = 26;

        timer->clock.ce.feature = OS_CLOCKEVENT_FEATURE_PERIOD;

        timer->clock.ce.min_nsec = NSEC_PER_SEC / timer->clock.ce.freq;

        timer->clock.ce.ops = &csky_tim_ops;
        os_clockevent_register(dev->name, &timer->clock.ce);
#endif
    }
    os_list_add(&csky_timer_list, &timer->list);

    return OS_SUCCESS;
}

OS_DRIVER_INFO csky_tim_driver = {
    .name  = "Timer_InitStructure",
    .probe = csky_tim_probe,
};

OS_DRIVER_DEFINE(csky_tim_driver, OS_INIT_LEVEL_PRE_DEVICE, OS_INIT_SUBLEVEL_MIDDLE);
//#pragma GCC pop_options

