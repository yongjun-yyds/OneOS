/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_spi.c
 *
 * @brief       This file implements SPI driver for gd32.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <board.h>
#include <os_task.h>
#include <device.h>
#include <os_memory.h>
#include <os_stddef.h>
#include <drv_gpio.h>
#include <drv_common.h>
#include <drv_spi.h>
#include <string.h>
#include <pin/pin.h>
#include <drv_log.h>
#include <spi/spi.h>
#include <drv_gpio.h>
#define DBG_TAG "drv.spi"
#include <dlog.h>

static os_list_node_t ck_spi_list = OS_LIST_INIT(ck_spi_list);

static void ck_spi_hard_init(ck_spi_info_t *info)
{
    GPIO_InitStructure GPIO_InitInfo;
    /* init mosi miso sck*/
    GPIO_InitInfo.GPIOAddr      = info->GPIOAddr;
    GPIO_InitInfo.GPIOPort      = info->GPIOPort;
    GPIO_InitInfo.GPIOPin       = info->spi_miso_pin | info->spi_mosi_pin | info->spi_sck_pin;
    GPIO_InitInfo.bHardwareMode = TRUE;
    GPIO_Config(&GPIO_InitInfo);
    /* init cs,use hard cs_pin for start spi_RXTX */
    if (info->SPI_InitStruct.cs == SPI_CS_Manual)
    {
    }
    else
    {
        GPIO_InitInfo.GPIOAddr      = info->spi_cs_pin.GPIOAddr;
        GPIO_InitInfo.GPIOPort      = info->spi_cs_pin.GPIOPort;
        GPIO_InitInfo.GPIOPin       = info->spi_cs_pin.pin;
        GPIO_InitInfo.bHardwareMode = TRUE;
        GPIO_Config(&GPIO_InitInfo);
    }
    SPI_Config(&info->SPI_InitStruct);
}

static os_err_t ck_spi_init(struct ck_spi *spi_drv, struct os_spi_configuration *cfg)
{
    SPI_InitStructure *spi_init_struct;
    OS_ASSERT(spi_drv != OS_NULL);
    OS_ASSERT(cfg != OS_NULL);

    spi_init_struct = &spi_drv->spi_info->SPI_InitStruct;

    /* datawidth */
    if (cfg->data_width >= SPI_DataWidth_8 && cfg->data_width <= SPI_DataWidth_16)
    {
        spi_init_struct->datawidth = cfg->data_width;
    }
    else
    {
        return OS_EIO;
    }

    /* baudrate */
    LOG_D(DBG_TAG, "max   freq: %d\n", cfg->max_hz);
    spi_init_struct->baudrate = cfg->max_hz;

    /* mode */
    switch (cfg->mode & OS_SPI_MODE_3)
    {
    case OS_SPI_MODE_0:
        spi_init_struct->polarity = SPI_CLOCK_POLARITY_LOW;
        spi_init_struct->phase    = SPI_CLOCK_PHASE_FIRST_EDGE;
        break;
    case OS_SPI_MODE_1:
        spi_init_struct->polarity = SPI_CLOCK_POLARITY_LOW;
        spi_init_struct->phase    = SPI_CLOCK_PHASE_SECOND_EDGE;

        break;
    case OS_SPI_MODE_2:
        spi_init_struct->polarity = SPI_CLOCK_POLARITY_HIGH;
        spi_init_struct->phase    = SPI_CLOCK_PHASE_FIRST_EDGE;

        break;
    case OS_SPI_MODE_3:
        spi_init_struct->polarity = SPI_CLOCK_POLARITY_HIGH;
        spi_init_struct->phase    = SPI_CLOCK_PHASE_SECOND_EDGE;

        break;
    }

    /* MSB or LSB */
    if (cfg->mode & OS_SPI_MSB)
    {
        /*not support*/
    }
    else
    {
        /*not support*/
    }

    // SPI_Open(spi_drv->spi_pInfo);
    SPI_Config(spi_init_struct);
    return OS_SUCCESS;
}

static os_err_t spi_configure(struct os_spi_device *device, struct os_spi_configuration *configuration)
{
    OS_ASSERT(device != OS_NULL);
    OS_ASSERT(configuration != OS_NULL);

    struct ck_spi *spi_drv = os_container_of(device->bus, struct ck_spi, spi_bus);
    spi_drv->cfg           = configuration;

    return ck_spi_init(spi_drv, configuration);
}

/*!
  \brief      send a byte through the SPI interface and return the byte received from the SPI bus
  \param[in]  byte: byte to send
  \param[out] none
  \retval     the value of the received byte
  */
static uint32_t spixfer(struct os_spi_device *device, struct os_spi_message *message)
{
    uint8_t        state;
    os_size_t         message_length, already_send_length;
    uint32_t       send_length;
    uint8_t       *recv_buf;
    const uint8_t *send_buf;

    OS_ASSERT(device != OS_NULL);
    OS_ASSERT(device->bus != OS_NULL);
    OS_ASSERT(message != OS_NULL);

    struct ck_spi  *spi_drv  = os_container_of(device->bus, struct ck_spi, spi_bus);
    pStruct_SPIInfo pSpiInfo = spi_drv->spi_pInfo;

    struct ck_hw_spi_cs *cs_pin = device->parent.user_data;

    if (message->cs_take)
    {
        GPIO_WritePins(cs_pin->GPIOAddr, cs_pin->GPIOPort, cs_pin->pin, 0);
        LOG_D(DBG_TAG, "spi take cs\n");
    }

    message_length = message->length;
    recv_buf       = message->recv_buf;
    send_buf       = message->send_buf;
    while (message_length)
    {
        if (message_length > (uint32_t)(~0))
        {
            send_length    = (uint32_t)(~0);
            message_length = message_length - (uint32_t)(~0);
        }
        else
        {
            send_length    = message_length;
            message_length = 0;
        }

        /* calculate the start address */
        already_send_length = message->length - send_length - message_length;
        send_buf            = (uint8_t *)message->send_buf + already_send_length;
        recv_buf            = (uint8_t *)message->recv_buf + already_send_length;

        if (message->send_buf && message->recv_buf)
        {
            memset((uint8_t *)recv_buf, 0xff, send_length);
            state = SPI_TxRx(pSpiInfo, pSpiInfo->SPI_InitStruct.cs, (void *)send_buf, (void *)recv_buf, send_length);
        }
        else if (message->send_buf)
        {
            state = SPI_TxRx(pSpiInfo, pSpiInfo->SPI_InitStruct.cs, (void *)send_buf, (void *)recv_buf, send_length);
        }
        else
        {
            state = SPI_TxRx(pSpiInfo, pSpiInfo->SPI_InitStruct.cs, (void *)send_buf, (void *)recv_buf, send_length);
        }

        if (state != OS_SUCCESS)
        {
            LOG_I(DBG_TAG, "spi transfer error : %d", state);
            message->length = 0;
        }
        else
        {
            LOG_D(DBG_TAG, "transfer done");
        }
    }

    if (message->cs_release)
    {
        GPIO_WritePins(cs_pin->GPIOAddr, cs_pin->GPIOPort, cs_pin->pin, 1);
        LOG_D(DBG_TAG, "spi release cs\n");
    }
    return message->length;
}

static const struct os_spi_ops ck_spi_ops = {
    .configure = spi_configure,
    .xfer      = spixfer,
};

os_err_t os_hw_spi_device_attach(const char *bus_name, const char *device_name, os_base_t cs_pin)
{
    OS_ASSERT(bus_name != OS_NULL);
    OS_ASSERT(device_name != OS_NULL);

    os_err_t              result;
    struct os_spi_device *spi_device;

    struct pin_index    *index;
    struct ck_hw_spi_cs *hw_cs_pin;
    hw_cs_pin = (struct ck_hw_spi_cs *)os_malloc(sizeof(struct ck_hw_spi_cs));
    OS_ASSERT(hw_cs_pin != OS_NULL);
    index = get_pin_index(cs_pin);
    if (index == OS_NULL)
    {
        return OS_FAILURE;
    }

    /* initialize the cs pin && select the slave*/
    hw_cs_pin->GPIOAddr = index->gpio_num;
    hw_cs_pin->GPIOPort = index->gpio_port;
    hw_cs_pin->pin      = index->pin;

    GPIO_InitStructure GPIO_InitInfo;
    GPIO_InitInfo.GPIOAddr      = hw_cs_pin->GPIOAddr;
    GPIO_InitInfo.GPIOPort      = hw_cs_pin->GPIOPort;
    GPIO_InitInfo.GPIODirection = GPIO_OUTPUT;
    GPIO_InitInfo.GPIOPin       = hw_cs_pin->pin;
    GPIO_InitInfo.bHardwareMode = FALSE;
    GPIO_InitInfo.GPIOIntr      = GPIO_NULL_INTR;
    GPIO_Config(&GPIO_InitInfo);
    GPIO_WritePins(hw_cs_pin->GPIOAddr, hw_cs_pin->GPIOPort, hw_cs_pin->pin, 1);

    /* attach the device to spi bus*/
    spi_device = (struct os_spi_device *)os_malloc(sizeof(struct os_spi_device));
    OS_ASSERT(spi_device != OS_NULL);

    result = os_spi_bus_attach_device(spi_device, device_name, bus_name, (void *)hw_cs_pin);

    if (result != OS_SUCCESS)
    {
        LOG_E(DBG_TAG, "%s attach to %s faild, %d\n", device_name, bus_name, result);
    }

    OS_ASSERT(result == OS_SUCCESS);

    LOG_D(DBG_TAG, "%s attach to %s done", device_name, bus_name);

    return result;
}
static int ck_spi_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    os_err_t  result = 0;
    os_ubase_t level;

    struct ck_spi *cmc_spi = os_calloc(1, sizeof(struct ck_spi));

    OS_ASSERT(cmc_spi);

    cmc_spi->spi_info                          = (ck_spi_info_t *)dev->info;
    cmc_spi->spi_info->SPI_InitStruct.baudrate = APB_FREQ / 100;

    ck_spi_hard_init(cmc_spi->spi_info);

    if (cmc_spi->spi_info->SPI_InitStruct.SPI_BaseAddr == (UINT32)SPI_BASE_ADDR)
        cmc_spi->spi_pInfo = pSPI;
    else
        cmc_spi->spi_pInfo = pSPI1;

    struct os_spi_bus *spi_bus = &cmc_spi->spi_bus;

    os_spin_lock_irqsave(&gs_device_lock, &level);
    os_list_add_tail(&ck_spi_list, &cmc_spi->list);
    os_spin_unlock_irqrestore(&gs_device_lock, level);

    result = os_spi_bus_register(spi_bus, dev->name, &ck_spi_ops);
    OS_ASSERT(result == OS_SUCCESS);

    LOG_D(DBG_TAG, "%s bus init done", dev->name);

    return result;
}

OS_DRIVER_INFO ck_spi_driver = {
    .name  = "SPI_InitStructure",
    .probe = ck_spi_probe,
};

OS_DRIVER_DEFINE(ck_spi_driver, OS_INIT_LEVEL_PRE_DEVICE, OS_INIT_SUBLEVEL_LOW);

