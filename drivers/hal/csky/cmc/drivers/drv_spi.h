/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_spi.h
 *
 * @brief       This file implements SPI driver for csky.
 *
 * @revision
 * Date         Author          Notes
 * 2020-07-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __DRV_SPI_H_
#define __DRV_SPI_H_
#include <ckgpio.h>
#include <lib_spi.h>
#include <spi/spi.h>
#define DUMMY_BYTE            0xA5
#define SPI_USING_RX_DMA_FLAG (1 << 0)
#define SPI_USING_TX_DMA_FLAG (1 << 1)

struct ck_hw_spi_cs
{
    pStruct_GPIOInfo GPIOAddr;
    Enum_GPIO_Port   GPIOPort;
    uint32_t      pin;
};

struct ck_spi_device
{
    uint32_t pin;
    char       *bus_name;
    char       *device_name;
};

typedef struct ck_spi_info
{
    pStruct_GPIOInfo    GPIOAddr;
    Enum_GPIO_Port      GPIOPort;
    uint32_t         spi_miso_pin;
    uint32_t         spi_mosi_pin;
    uint32_t         spi_sck_pin;
    struct ck_hw_spi_cs spi_cs_pin;
    SPI_InitStructure   SPI_InitStruct;
} ck_spi_info_t;

typedef struct ck_spi
{
    struct os_spi_bus            spi_bus;
    struct ck_spi_info          *spi_info;
    pStruct_SPIInfo              spi_pInfo;
    char                        *bus_name;
    struct os_spi_configuration *cfg;
    uint8_t                   spi_dma_flag;
    os_list_node_t               list;
} ck_spi_t;

#endif /*__DRV_SPI_H_ */
