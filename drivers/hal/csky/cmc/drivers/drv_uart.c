/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_uart.c
 *
 * @brief       This file implements uart driver for mm32
 *
 * @revision
 * Date         Author          Notes
 * 2021-05-31   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include "drv_uart.h"
#include "drv_gpio.h"

#include <os_memory.h>
#include <bus/bus.h>
#include <dma/dma.h>
#include <board.h>
#include <string.h>

#define DRV_EXT_TAG "drv.uart"
#include <drv_log.h>

static os_list_node_t ck_uart_list = OS_LIST_INIT(ck_uart_list);

static void ck_uart_hard_init(ck_uart_info_t *info)
{
    GPIO_InitStructure GPIO_InitInfo;
    os_ubase_t        level     = 0;
    GPIO_InitInfo.GPIOAddr      = info->GPIOAddr;
    GPIO_InitInfo.GPIOPort      = info->GPIOPort;
    GPIO_InitInfo.GPIOPin       = info->tx_pin | info->rx_pin;
    GPIO_InitInfo.bHardwareMode = TRUE;
    GPIO_Config(&GPIO_InitInfo);

    os_spin_lock_irqsave(&gs_device_lock, &level);
    Uart_CloseIntr(info->UART_Initstruct.UartId);
    Uart_Config(&(info->UART_Initstruct));
    os_spin_unlock_irqrestore(&gs_device_lock, level);
}

static void ck_uart_irq_rx_callback(ck_uart_t *uart)
{
    pStruct_UartInfo pInfo;
    pStruct_UartType pUart;

    if (uart->info->use_dma == 0)
    {
        OS_ASSERT(uart->rx_buff != OS_NULL);
        OS_ASSERT(uart->rx_index < uart->rx_size);

        pInfo = &(Uart_Table[uart->info->UART_Initstruct.UartId]);
        pUart = pInfo->addr;

        uart->rx_buff[uart->rx_index++] = pUart->RBR_THR_DLL;

        if (uart->rx_index == (uart->rx_size / 2))
        {
            soft_dma_half_irq(&uart->sdma);
        }

        if (uart->rx_index == uart->rx_size)
        {
            uart->rx_index = 0;
            soft_dma_full_irq(&uart->sdma);
        }
    }
}

static void ck_uart_irq_tx_callback(ck_uart_t *uart)
{
    pStruct_UartType pUart;
    UINT8            cState;

    uart->tx_count++;
    pUART_InitStructure uart_struct = &(uart->info->UartInfo->UART_Initstruct);
    if (uart->tx_count >= uart->tx_size)
    {
        uart_struct->TxMode = UART_Query;
        Uart_SetTXMode(uart_struct->UartId, uart_struct->TxMode);

        pUart = uart->info->UartInfo->addr;
        /* tx done */
        cState = pUart->LSR & LSR_TEMT;
        if (cState)
            os_hw_serial_isr_txdone((struct os_serial_device *)uart);
    }
    else
    {
        Uart_PutChar(uart_struct->UartId, uart->tx_buff[uart->tx_count]);
    }
}

static void clc_irq(uint8_t uart_id)
{
    pStruct_UartInfo pInfo;
    pStruct_UartType pUart;
    uint8_t       cState;
    uint8_t       cData;

    pInfo  = &(Uart_Table[uart_id]);
    pUart  = pInfo->addr;
    cState = pUart->IIR_FCR & 0xf;
    if ((cState == IIR_RECEIVED) && !(pInfo->brxquery))
    {
        cData = pUart->RBR_THR_DLL;
    }
    else if ((cState == IIR_BUSY) && !(pInfo->brxquery))
    {
        cData = pUart->USR & 0xf;    // read UART Status Register to reset busy state
    }
}

static void ck_uart_irq_callback(ck_uart_t *uart)
{
    pStruct_UartType pUart;
    pStruct_UartInfo pUartInfo;
    uint8_t       cState;
    uint8_t       uart_id;
    uart_id   = uart->info->UART_Initstruct.UartId;
    pUartInfo = &(Uart_Table[uart_id]);
    pUart     = pUartInfo->addr;

    cState = pUart->IIR_FCR & 0xf; /* ��ȡ�жϱ�� */
    if ((cState == IIR_EMPTY) && !(pUartInfo->btxquery))
    {
        ck_uart_irq_tx_callback(uart);
    }
    else if ((cState == IIR_RECEIVED) && !(pUartInfo->brxquery))
    {
        if (uart->rx_buff != OS_NULL)
        {
            ck_uart_irq_rx_callback(uart);
        }
        else
        {
            clc_irq(uart_id);
        }
    }
    else if ((cState == IIR_BUSY) && !(pUartInfo->brxquery))
    {
        cState = pUart->USR & 0xf;    // read UART Status Register to reset busy state
    }
}

void UART0_IRQHandler(UINT32 dwIrqid)
{
    struct ck_uart *uart;
    if (!os_list_empty(&ck_uart_list))
    {
        os_list_for_each_entry(uart, &ck_uart_list, struct ck_uart, list)
        {
            if (uart->info->UART_Initstruct.UartId == UART0)
            {
                ck_uart_irq_callback(uart);
            }
        }
    }
    else
    {
        clc_irq(UART0);
    }
}

void UART1_IRQHandler(UINT32 dwIrqid)
{
    struct ck_uart *uart;
    if (!os_list_empty(&ck_uart_list))
    {
        os_list_for_each_entry(uart, &ck_uart_list, struct ck_uart, list)
        {
            if (uart->info->UART_Initstruct.UartId == UART1)
            {
                ck_uart_irq_callback(uart);
            }
        }
    }
    else
    {
        clc_irq(UART1);
    }
}

/* interrupt rx mode */
static uint32_t ck_sdma_int_get_index(soft_dma_t *dma)
{
    ck_uart_t *uart = os_container_of(dma, ck_uart_t, sdma);

    return uart->rx_index;
}

static os_err_t ck_sdma_int_start(soft_dma_t *dma, void *buff, uint32_t size)
{
    ck_uart_t *uart = os_container_of(dma, ck_uart_t, sdma);

    uart->rx_buff  = buff;
    uart->rx_index = 0;
    uart->rx_size  = size;

    return OS_SUCCESS;
}

static uint32_t ck_sdma_int_stop(soft_dma_t *dma)
{
    ck_uart_t *uart = os_container_of(dma, ck_uart_t, sdma);

    return uart->rx_index;
}

/* sdma callback */
static void ck_uart_sdma_callback(soft_dma_t *dma)
{
    ck_uart_t *uart = os_container_of(dma, ck_uart_t, sdma);

    os_hw_serial_isr_rxdone((struct os_serial_device *)uart);
}

static void ck_uart_sdma_init(struct ck_uart *uart, dma_ring_t *ring)
{
    soft_dma_t *dma = &uart->sdma;

    soft_dma_stop(dma);

    dma->hard_info.mode         = HARD_DMA_MODE_NORMAL;
    dma->hard_info.max_size     = 64 * 1024;
    dma->hard_info.flag         = HARD_DMA_FLAG_HALF_IRQ | HARD_DMA_FLAG_FULL_IRQ;
    dma->hard_info.data_timeout = uart_calc_byte_timeout_us(uart->serial.config.baud_rate);

    dma->ops.get_index = ck_sdma_int_get_index;
    dma->ops.dma_init  = OS_NULL;
    dma->ops.dma_start = ck_sdma_int_start;
    dma->ops.dma_stop  = ck_sdma_int_stop;

    dma->cbs.dma_half_callback    = ck_uart_sdma_callback;
    dma->cbs.dma_full_callback    = ck_uart_sdma_callback;
    dma->cbs.dma_timeout_callback = ck_uart_sdma_callback;

    soft_dma_init(dma);
    soft_dma_start(dma, ring);
    soft_dma_irq_enable(dma, OS_TRUE);
}

static os_err_t ck_uart_init(struct os_serial_device *serial, struct serial_configure *cfg)
{
    struct ck_uart *uart;
    OS_ASSERT(serial != OS_NULL);
    OS_ASSERT(cfg != OS_NULL);
    os_ubase_t level = 0;

    uart                            = os_container_of(serial, struct ck_uart, serial);
    pUART_InitStructure uart_struct = &(uart->info->UART_Initstruct);
    switch (cfg->baud_rate)
    {
    case BAUD_RATE_2400:
    case BAUD_RATE_4800:
    case BAUD_RATE_9600:
    case BAUD_RATE_19200:
    case BAUD_RATE_38400:
    case BAUD_RATE_57600:
    case BAUD_RATE_115200:
        uart_struct->BaudRate = cfg->baud_rate;
        break;
    default:
        return OS_INVAL;
    }
    switch (cfg->stop_bits)
    {
    case STOP_BITS_1:
        uart_struct->StopBit = LCR_STOP_BIT_1;
        break;
    case STOP_BITS_2:
        uart_struct->StopBit = LCR_STOP_BIT_2;
        break;
    default:
        return OS_INVAL;
    }
    switch (cfg->parity)
    {
    case PARITY_NONE:
        uart_struct->Parity = NONE;
        break;
    case PARITY_ODD:
        uart_struct->Parity = ODD;
        break;
    case PARITY_EVEN:
        uart_struct->Parity = EVEN;
        break;
    default:
        return OS_INVAL;
    }

    switch (cfg->data_bits)
    {
    case DATA_BITS_8:
        uart_struct->WordSize = WORD_SIZE_8;
        break;
    default:
        return OS_INVAL;
    }
    os_spin_lock_irqsave(&gs_device_lock, &level);
    Uart_Config(uart_struct);
    os_spin_unlock_irqrestore(&gs_device_lock, level);
    ck_uart_sdma_init(uart, &uart->serial.rx_fifo->ring);
    return OS_SUCCESS;
}

static os_err_t ck_uart_deinit(struct os_serial_device *serial)
{
    struct ck_uart *uart;
    os_ubase_t      level = 0;
    OS_ASSERT(serial != OS_NULL);

    uart = os_container_of(serial, struct ck_uart, serial);
    /* close irq */
    soft_dma_stop(&uart->sdma);
    soft_dma_deinit(&uart->sdma);
    os_spin_lock_irqsave(&gs_device_lock, &level);
    Uart_CloseIntr(uart->info->UART_Initstruct.UartId);
    os_spin_unlock_irqrestore(&gs_device_lock, level);

    return OS_SUCCESS;
}

static int ck_uart_poll_send(struct os_serial_device *serial, const uint8_t *buff, os_size_t size)
{
    struct ck_uart *uart;
    int             i;
    os_ubase_t      level;

    OS_ASSERT(serial != OS_NULL);

    uart = os_container_of(serial, struct ck_uart, serial);

    for (i = 0; i < size; i++)
    {
        os_spin_lock_irqsave(&gs_device_lock, &level);
        Uart_PutChar(uart->info->UART_Initstruct.UartId, *(buff + i));
        os_spin_unlock_irqrestore(&gs_device_lock, level);
    }

    return size;
}

static const struct os_uart_ops ck_uart_ops = {
    .init   = ck_uart_init,
    .deinit = ck_uart_deinit,

    //    .start_send     = ck_uart_start_send,
    .poll_send = ck_uart_poll_send,
};

static void ck_uart_parse_configs(struct ck_uart *uart)
{
    pUART_InitStructure uart_struct = &(uart->info->UART_Initstruct);

    struct os_serial_device *serial = &uart->serial;

    switch (uart_struct->BaudRate)
    {
    case B2400:
    case B4800:
    case B9600:
    case B19200:
    case B38400:
    case B57600:
    case B115200:
        serial->config.baud_rate = uart_struct->BaudRate;
        break;
    default:
        break;
    }
    switch (uart_struct->StopBit)
    {
    case LCR_STOP_BIT_1:
        serial->config.stop_bits = STOP_BITS_1;
        break;
    case LCR_STOP_BIT_2:
        serial->config.stop_bits = STOP_BITS_2;
        break;
    default:
        break;
    }
    switch (uart_struct->Parity)
    {
    case NONE:
        serial->config.parity = PARITY_NONE;
        break;
    case ODD:
        serial->config.parity = PARITY_ODD;
        break;
    case EVEN:
        serial->config.parity = PARITY_EVEN;
        break;
    default:
        break;
    }
    switch (uart_struct->WordSize)
    {
    case WORD_SIZE_8:
        serial->config.data_bits = DATA_BITS_8;
        break;
    default:
        break;
    }
}

static int ck_uart_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    struct serial_configure config = OS_SERIAL_CONFIG_DEFAULT;

    os_err_t                 result = 0;
    os_ubase_t               level  = 0;
    struct os_serial_device *serial;
    struct ck_uart          *uart = os_calloc(1, sizeof(struct ck_uart));
    OS_ASSERT(uart);

    uart->info = (struct ck_uart_info *)dev->info;
#ifndef OS_USING_CONSOLE
    ck_uart_hard_init(uart->info);
#endif
    if (uart->info->UART_Initstruct.UartId == UART0)
    {
        uart->info->UartInfo                         = &Uart_Table[0];
        uart->info->UartInfo->UART_Initstruct.UartId = UART0;
    }
    else
    {
        uart->info->UartInfo                         = &Uart_Table[1];
        uart->info->UartInfo->UART_Initstruct.UartId = UART1;
    }
    uart->info->UartInfo->UART_Initstruct.UART_ISR = uart->info->UART_Initstruct.UART_ISR;
    uart->info->UartInfo->UART_Initstruct.Priority = uart->info->UART_Initstruct.Priority;

    serial         = &uart->serial;
    serial->ops    = &ck_uart_ops;
    serial->config = config;
    ck_uart_parse_configs(uart);

    os_spin_lock_irqsave(&gs_device_lock, &level);
    os_list_add_tail(&ck_uart_list, &uart->list);
    os_spin_unlock_irqrestore(&gs_device_lock, level);

    result = os_hw_serial_register(serial, dev->name, NULL);

    OS_ASSERT(result == OS_SUCCESS);

    return result;
}

OS_DRIVER_INFO ck_uart_driver = {
    .name  = "UART_InitStructure",
    .probe = ck_uart_probe,
};

OS_DRIVER_DEFINE(ck_uart_driver, OS_INIT_LEVEL_PRE_DEVICE, OS_INIT_SUBLEVEL_HIGH);


#ifdef OS_USING_CONSOLE
static Enum_Uart_Device console_uart = 0;
void                    __os_hw_console_output(char *str)
{
    os_ubase_t level = 0;
    os_spin_lock_irqsave(&gs_device_lock, &level);
    while (*str)
    {
        Uart_PutChar(console_uart, *str++);
    }
    os_spin_unlock_irqrestore(&gs_device_lock, level);
}

static int ck_uart_early_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    ck_uart_info_t *info = (ck_uart_info_t *)dev->info;
    ck_uart_hard_init(info);

    if (!strcmp(dev->name, OS_CONSOLE_DEVICE_NAME))
    {

        console_uart = info->UART_Initstruct.UartId;
    }
    return OS_SUCCESS;
}

OS_DRIVER_INFO ck_uart_early_driver = {
    .name  = "UART_InitStructure",
    .probe = ck_uart_early_probe,
};

OS_DRIVER_DEFINE(ck_uart_early_driver, OS_INIT_LEVEL_PRE_KERNEL_1, OS_INIT_SUBLEVEL_MIDDLE);
#endif

