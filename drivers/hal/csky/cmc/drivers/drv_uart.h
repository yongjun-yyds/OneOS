/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_usart.h
 *
 * @brief        This file provides functions declaration for usart driver.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __DRV_UART_H__
#define __DRV_UART_H__

#include <os_task.h>
#include <device.h>
#include <drv_cfg.h>
#include "uart.h"
#include <soft_dma.h>
#include <ckgpio.h>

/* csky uart driver */

typedef struct ck_uart_info
{
    pStruct_GPIOInfo   GPIOAddr; /* Gpio����ַ */
    Enum_GPIO_Port     GPIOPort; /* PORT */
    UINT32             tx_pin;   /* ���ź� */
    UINT32             rx_pin;   /* ���ź� */
    UART_InitStructure UART_Initstruct;
    pStruct_UartInfo   UartInfo;

    uint8_t  use_dma;
    uint32_t dma_periph;
    uint8_t  dma_channel;
    //   IRQn_Type           dma_irqn;
} ck_uart_info_t;

typedef struct ck_uart
{
    struct os_serial_device serial;

    struct ck_uart_info *info;

    soft_dma_t  sdma;
    uint32_t sdma_hard_size;

    uint8_t *rx_buff;
    uint32_t rx_index;
    uint32_t rx_size;

    os_size_t         tx_count;
    os_size_t         tx_size;
    const uint8_t *tx_buff;

    os_list_node_t list;
} ck_uart_t;

#endif /* __DRV_USART_H__ */

/******************* end of file *******************/
