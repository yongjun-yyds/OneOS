/*
 文件: cktimer.h
 版本: CMC693 V1.10
 版权: 浙江杰芯科技有限公司
 作者: zhulin(zhulin2@supcon.com)
 描述：
修改记录：
=========
日期                                      作者                       工作                            内容
*/

#ifndef __CKTIMER_H__
#define __CKTIMER_H__

#include "ckcore.h"
#include "datatype.h"

/* 定时器中断号宏定义 */
#define  TIMER_IRQ0   CK_INTC_TIMER0
#define  TIMER_IRQ1   CK_INTC_TIMER1
#define  TIMER_IRQ2   CK_INTC_TIMER2
#define  TIMER_IRQ3   CK_INTC_TIMER3

typedef struct {
    REG    LoadCount;          /* 定时器加载计数寄存器 */
    REG    CurrentValue;       /* 定时器当前计数寄存器 */
    REG    Control;            /* 定时器控制寄存器 */
    REG    EOI;	               /* 定时器中断清除寄存器 */
    REG    IntStatus;          /* 定时器中断状态寄存器 */
} Struct_TIMER,* pStruct_TIMER;

typedef struct S_TIMER_CON{
    REG    IntStatus;	       /* 全部定时器中断状态寄存器 */
    REG    EOI;		           /* 全部定时器中断清除寄存器 */
    REG    RawIntStatus;	   /* 全部定时器原始中断状态寄存器 */
} Struct_TIMER_CON,* pStruct_TIMER_CON;

/* 定时器控制寄存器位定义 */
#define TIMER_TXCONTROL_ENABLE      (1UL << 0)
#define TIMER_TXCONTROL_MODE        (1UL << 1)
#define TIMER_TXCONTROL_INTMASK     (1UL << 2)


#define TIMER0_BASSADDR   ((pStruct_TIMER)CK_TIMER0_BASSADDR)
#define TIMER1_BASSADDR   ((pStruct_TIMER)CK_TIMER1_BASSADDR)
#define TIMER2_BASSADDR   ((pStruct_TIMER)CK_TIMER2_BASSADDR)
#define TIMER3_BASSADDR   ((pStruct_TIMER)CK_TIMER3_BASSADDR)
#define  P_TIMER_CONTROL  ((pStruct_TIMER_CON)CK_TIMER_CONTROL_BASSADDR)

#endif
