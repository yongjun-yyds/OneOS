/*
 文件: ckspi.h
 版本: CMC693 V1.10
 版权: 浙江杰芯科技有限公司
 作者: zhulin(zhulin2@supcon.com)
 描述：
修改记录：
=========
日期                                      作者                       工作                            内容
*/

#ifndef __CKSPI_H_
#define __CKSPI_H_

#include "datatype.h"
#include "ckcore.h"

/*SPI 寄存器的位值宏 */
#define SPI_EN                  0x01    //Enable or Disable SPI  IN     SPIENR
#define SPI_TMOD_BIT8           0x0100  //SPI TxRx mode 0
#define SPI_TMOD_BIT9           0x0200  //SPI TxRx mode 1        IN     CTRLR0
#define SPI_POLARITY            0x80    //polarity bit           IN     CTRLR0
#define SPI_PHASE               0x40    //phase bit              IN     CTRLR0
#define SPI_BUSY                0x01    //BUSY bit               IN     SR
#define SPI_RFNE                0x08    //Receive FIFO Full Bit  IN     SR
#define SPI_RDMAE               0x01    //Receive DMA Enable     IN     DMACR
#define SPI_TDMAE               0x02    //Transmit  DMA  Enable  IN     DMACR
#define SPI_TXFIFO_EMPTY        0x04    //Transmit FIFO Empty    IN     SR
#define SPI_RXFIFO_NOT_EMPTY    0x08    //Receive FIFO Not Empty IN     SR
#define SPI_FIFO_MAX_LEVEL      0x0F    //Max LEVEL              IN     TXFTLR RXFTLR

//中断屏蔽选择
#define SPI_INTR_MASK_ALL       0x00    //mask all interrupt
#define SPI_INTR_MASK_NONE      0x3F    //Enable all interrupt
#define SPI_INTR_MASK_TXEIM     0x01    //Transmit FIFO Empty Interrupt NOT Mask
#define SPI_INTR_MASK_TXOIM     0x02    //Transmit FIFO Overflow Interrupt NOT Mask
#define SPI_INTR_MASK_RXUIM     0x04    //Receive FIFO Underflow Interrupt NOT Mask
#define SPI_INTR_MASK_RXOIM     0x08    //Receive FIFO Overflow Interrupt NOT Mask
#define SPI_INTR_MASK_TXFIM     0x10    //Receive FIFO Full Interrupt NOT Mask
#define SPI_INTR_MASK_MSTIM     0x20    //Multi-Master Contention Interrupt NOT Mask

//SPI中断状态 Interrupt Status Register
#define SPI_TxFIFO_EMPTY        0x01    //Transmit FIFO Empty Interrupt Status
#define SPI_TxFIFO_OVERFLOW     0x02    //Transmit FIFO Overflow Interrupt Status
#define SPI_RxFIFO_UNDERFLOW    0x04    //Receive FIFO Underflow Interrupt Status
#define SPI_RXFIFO_OVERFLOW     0x08    //Receive FIFO Overflow Interrupt Status
#define SPI_RXFIFO_FULL         0x10    //Receive FIFO Full Interrupt Status
#define SPI_MULTI_MASTER        0x20    //Multi-Master Contention Interrupt Status

//寄存器结构
typedef struct ST_SPI{
    SREG   CTRLR0;  //控制寄存器0                0x00
    SREG   rev0;    //未定义地址，无效
    SREG   CTRLR1;  //控制寄存器1                0x04
    SREG   rev1;    //未定义地址，无效
    CREG   SPIENR;  //SPI使能失能寄存器                               0x08
    CREG   rev2[7]; //未定义地址，无效
    REG    SER;     //配置SPI从机片选线，SPI为Master时有效  0x10
    SREG   BAUDR;   //波特率配置寄存器                                   0x14
    SREG   rev3;    //未定义地址，无效
    REG    TXFTLR;  //发送FIFO阙值配置寄存器， 配置触发中断的阙值 0x18
    REG    RXFTLR;  //接收FIFO阙值配置寄存器， 配置触发中断的阙值 0x1c
    REG    TXFLR;   //发送FIFO中数据量寄存器， 0x20
    REG    RXFLR;   //接收FIFO中数据量寄存器，0x24
    CREG   SR;      //状态寄存器， 0x28
    CREG   rev4[3]; //未定义地址，无效
    REG    IMR;     //中断屏蔽寄存器，0x2c
    REG    ISR;     //中断状态寄存器，0x30
    REG    RISR;    //原始中断状态寄存器，0x34
    CREG   TXOICR;  //读清除发送FIFO溢出（Overflow）中断，0x38
    CREG   rev5[3]; //未定义地址，无效
    CREG   RXOICR;  //读清除接收FIFO溢出中断，0x3c
    CREG   rev6[3]; //未定义地址，无效
    CREG   RXUICR;  //读清除接收FIFO下溢（Underflow）中断，0x40
    CREG   rev7[3]; //未定义地址，无效
    CREG   MSTICR;  //读清除多主机竞争中断，0x44
    CREG   rev8[3]; //未定义地址，无效
    CREG   ICR;     //读清除发送FIFO溢出中断、接收FIFO下溢中断、接收FIFO溢出中断、多主机竞争中断，0x48
    CREG   rev9[3]; //未定义地址，无效
    CREG   DMACR;   //DMA控制寄存器，0x4c
    CREG   rev10[3];//未定义地址，无效
    CREG   DMATDLR; //发送FIFO的DMA深度，0x50
    CREG   rev11[3];//未定义地址，无效
    CREG   DMARDLR; //接收FIFO的DMA深度，0x54
    CREG   rev12[3];//未定义地址，无效
    REG    IDR;     //ID寄存器，0x58
    REG    rev13;   //未定义地址，无效
    SREG   DR;      //数据寄存器，0x60-9c
    SREG   rev14[29];//未定义地址，无效
    CREG   rev15[34];//9D - BF
    CREG   WR;      //接收、发送模式选择寄存器c0
}Struct_SPI, *pStruct_SPI;

#define SPI_BASE_ADDR     CK_SPI_ADDRBASE0
#define SPI1_BASE_ADDR    CK_SPI1_ADDRBASE0

#endif /* CKSPI_H_ */
