/*
 文件: ckgpio.h
 版本: CMC693 V1.10
 版权: 浙江杰芯科技有限公司
 作者: zhulin(zhulin2@supcon.com)
 描述：
修改记录：
=========
日期                                      作者                       工作                            内容
*/
#ifndef __CK_GPIO_H__
#define __CK_GPIO_H__

#include "datatype.h"
#include "ckcore.h"

//#pragma pack(4)    /* zhulin 修改 */

/* GPIO寄存器结构体 */
typedef volatile struct GPIO_Info
{
    REG   SWPORTA_DR;         /*PortA输出寄存器*/
    REG   SWPORTA_DDR;        /*PortA方向控制寄存器*/
    REG   PORTA_CTL;          /*PortA复用，0不复用，1复用，默认全0*/
    REG   SWPORTB_DR;         /*PortB输出寄存器*/
    REG   SWPORTB_DDR;        /*PortB方向控制寄存器*/
    REG   PORTB_CTL;          /*PortB复用，0不复用，1复用，默认全0*/
    REG   revreg[6];          /*未定义地址，无效*/
    REG   INTEN;              /*PortA中断使能寄存器，0失能，1使能，默认全0*/
    REG   INTMASK;            /*PortA中断屏蔽寄存器，0未屏蔽，1屏蔽，默认0*/
    REG   INTTYPE_LEVEL;      /*PortA中断触发类型，0电平，1边缘，默认0*/
    REG   INT_POLARITY;       /*PortA中断极性，0低电平或下降沿，1高电平或上升沿*/
    REG   INTSTATUS;          /*PortA中断状态寄存器*/
    REG   RAWINTSTATUS;       /*PortA原始中断寄存器，无法被屏蔽*/
    REG   revreg0[1];         /*未定义地址，无效*/
    REG   PORTA_EOI;          /*PortA中断清除寄存器，用于清除边缘中断模式，写1清除中断*/
    REG   EXT_PORTA;          /*可读取PortA外部输入电平*/
    REG   EXT_PORTB;          /*可读取PortB外部输入电平*/
    REG   revreg1[2];         /*未定义地址，无效*/
    REG   LS_SYNC;            /*写1将使电平触发中断同步到pclk，0不同步，1同步，默认0*/
} Struct_GPIOInfo, *pStruct_GPIOInfo;

/* 端口类型 */
typedef enum
{
	PORTA = 1,
	PORTB = 2,

}Enum_GPIO_Port;

/* GPIO地址偏移，我们定义到最大的端口数量 */
#define GPIO_P0     ((unsigned int) 1 << 0)  /* Pin Controlled by P0 */
#define GPIO_P1     ((unsigned int) 1 << 1)  /* Pin Controlled by P1 */
#define GPIO_P2     ((unsigned int) 1 << 2)  /* Pin Controlled by P2 */
#define GPIO_P3     ((unsigned int) 1 << 3)  /* Pin Controlled by P3 */
#define GPIO_P4     ((unsigned int) 1 << 4)  /* Pin Controlled by P4 */
#define GPIO_P5     ((unsigned int) 1 << 5)  /* Pin Controlled by P5 */
#define GPIO_P6     ((unsigned int) 1 << 6)  /* Pin Controlled by P6 */
#define GPIO_P7     ((unsigned int) 1 << 7)  /* Pin Controlled by P7 */
#define GPIO_P8     ((unsigned int) 1 << 8)  /* Pin Controlled by P8 */
#define GPIO_P9     ((unsigned int) 1 << 9)  /* Pin Controlled by P9 */
#define GPIO_P10     ((unsigned int) 1 << 10)  /* Pin Controlled by P10 */
#define GPIO_P11     ((unsigned int) 1 << 11)  /* Pin Controlled by P11 */
#define GPIO_P12     ((unsigned int) 1 << 12)  /* Pin Controlled by P12 */
#define GPIO_P13     ((unsigned int) 1 << 13)  /* Pin Controlled by P13 */
#define GPIO_P14     ((unsigned int) 1 << 14)  /* Pin Controlled by P14 */
#define GPIO_P15     ((unsigned int) 1 << 15)  /* Pin Controlled by P15 */
#define GPIO_P16     ((unsigned int) 1 << 16)  /* Pin Controlled by P16 */
#define GPIO_P17     ((unsigned int) 1 << 17)  /* Pin Controlled by P17 */
#define GPIO_P18     ((unsigned int) 1 << 18)  /* Pin Controlled by P18 */
#define GPIO_P19     ((unsigned int) 1 << 19)  /* Pin Controlled by P19 */
#define GPIO_P20     ((unsigned int) 1 << 20)  /* Pin Controlled by P20 */
#define GPIO_P21     ((unsigned int) 1 << 21)  /* Pin Controlled by P21 */
#define GPIO_P22     ((unsigned int) 1 << 22)  /* Pin Controlled by P22 */
#define GPIO_P23     ((unsigned int) 1 << 23)  /* Pin Controlled by P23 */
#define GPIO_P24     ((unsigned int) 1 << 24)  /* Pin Controlled by P24 */
#define GPIO_P25     ((unsigned int) 1 << 25)  /* Pin Controlled by P25 */
#define GPIO_P26     ((unsigned int) 1 << 26)  /* Pin Controlled by P26 */
#define GPIO_P27     ((unsigned int) 1 << 27)  /* Pin Controlled by P27 */
#define GPIO_P28     ((unsigned int) 1 << 28)  /* Pin Controlled by P28 */
#define GPIO_P29     ((unsigned int) 1 << 29)  /* Pin Controlled by P29 */
#define GPIO_P30     ((unsigned int) 1 << 30)  /* Pin Controlled by P30 */
#define GPIO_P31     ((unsigned int) 1 << 31)  /* Pin Controlled by P31 */

/* 指向GPIO相应的寄存器地址 */
#define 	GPIO0 			((pStruct_GPIOInfo)(PCK_GPIO))
#define		GPIO1			((pStruct_GPIOInfo)(PCK_GPIO1))

#endif

