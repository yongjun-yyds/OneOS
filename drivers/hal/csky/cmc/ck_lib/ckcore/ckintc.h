/*
 文件: ckint.h
 版本: CMC693 V1.10
 版权: 浙江杰芯科技有限公司
 作者: zhulin(zhulin2@supcon.com)
 描述：
修改记录：
=========
日期                                      作者                       工作                            内容
*/

#ifndef _CKINTC_H_
#define _CKINTC_H_

#include "ckcore.h"
#include "datatype.h"

/* define the registers structure of the interrupt controller */
typedef struct S_INTC{
#ifdef __ckcoreBE__
  SREG   ICR;
  SREG   ISR;
#else
  SREG   ISR;
  SREG   ICR;
#endif
  REG    Rev0;
  REG    IFR;
  REG    IPR;
  REG    NIER;
  REG    NIPR;
  REG    FIER;
  REG    FIPR;
  REG    Rev[8];
  REG    PR[8];
}Struct_INTC, *pStruct_INTC;
 
#define P_INTC ((pStruct_INTC)CK_INTC_BASEADDRESS)

/*
 *  Bit Definition for the PIC Interrupt control register
 */
#define ICR_AVE   0x8000  /* Select vectored interrupt */
#define ICR_FVE   0x4000  /* Unique vector number for fast vectored*/
#define ICR_ME    0x2000  /* Interrupt masking enabled */
#define	ICR_MFI	  0x1000  /* Fast interrupt requests masked by MASK value */

#endif























