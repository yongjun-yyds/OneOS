/*
 文件: ckdma.h
 版本: CMC693 V1.10
 版权: 浙江杰芯科技有限公司
 作者: zhulin(zhulin2@supcon.com)
 描述：
修改记录：
=========
日期                                      作者                       工作                            内容
*/
#ifndef __CKDMA_H_
#define __CKDMA_H_

#include "datatype.h"
#include "ckcore.h"

//DMA通道特有寄存器组
typedef struct
{
    REG    SAR;
    REG    revreg0;
    REG    DAR;
    REG    revreg1[3];
    REG    CTRLa;
    REG    CTRLb;
    REG    revreg2[8];
    REG    CFGa;
    REG    CFGb;
} DMA_ChReg, *pDMA_ChReg;



//DMA两通道共用的寄存器组
typedef struct
{
	REG    RawTfr;
	REG    revreg0;
	REG    RawBlock;
	REG    revreg1;
	REG    RawSrcTran;
	REG    revreg2;
	REG    RawDstTran;
	REG    revreg3;
	REG    RawErr;
	REG    revreg4;
	REG    StatusTfr;
	REG    revreg5;
	REG    StatusBlock;
	REG    revreg6;
	REG    StatusSrcTran;
	REG    revreg7;
	REG    StatusDstTran;
	REG    revreg8;
	REG    StatusErr;
	REG    revreg9;
	REG    MaskTfr;
	REG    revreg10;
	REG    MaskBlock;
	REG    revreg11;
	REG    MaskSrcTran;
	REG    revreg12;
	REG    MaskDstTran;
	REG    revreg13;
	REG    MaskErr;
	REG    revreg14;
	REG    ClearTfr;
	REG    revreg15;
	REG    ClearBlock;
	REG    revreg16;
	REG    ClearSrcTran;
	REG    revreg17;
	REG    ClearDstTran;
	REG    revreg18;
	REG    ClearErr;
	REG    revreg19;
	REG    StatusInt;
	REG    revreg20;
	REG    ReqSrcReg;
	REG    revreg21;
	REG    ReqDstReg;
	REG    revreg22;
	REG    SglReqSrcReg;
// REG    revreg23;
// REG    SglReqDstReg;
	REG    revreg24[3];
	REG    LstSrcReg;
	REG    revreg25;
	REG    LstDstReg;
	REG    revreg26;
	REG    DmaCfgReg;
	REG    revreg27;
	REG    ChEnReg;
	REG    revreg28[3];
	REG    DmaTestReg;
} DMA_ComReg_st, *pDMA_ComReg_st;

#define		DMA_CH_NUM		2	//DMA通道数
#define     DMA_BASE_ADDR   CK_DMA_BASEADDR
#define     DMA_CH1_ADDR    CK_DMAC_CH1_BASEADDR
#define     DMA_CH2_ADDR    CK_DMAC_CH2_BASEADDR
#define     DMA_COMMON      ((pDMA_ComReg_st)CK_DMAC_CONTROL_BASEADDR)

#endif /* CKDMA_H_ */
