/*
 文件: cki2c.h
 版本: CMC693 V1.10
 版权: 浙江杰芯科技有限公司
 作者: zhulin(zhulin2@supcon.com)
 描述：
修改记录：
=========
日期                                      作者                       工作                            内容
*/

#ifndef CKI2C_H_
#define CKI2C_H_

#include "datatype.h"
#include "ckcore.h"

/*与相应寄存器相关，通过置位、复位，使能、失能功能*/
#define  IIC_DISABLE  			(0)
#define  IIC_ENABLE   			(1)
#define	 IIC_INT_NONE			(0)
#define	 IIC_INT_RXFIFO_UNDER 	(1 << 0)
#define  IIC_INT_RXFIFO_OVER   	(1 << 1)
#define  IIC_INT_RXFTL_ABOVE   	(1 << 2)
#define  IIC_INT_TXFIFO_OVER   	(1 << 3)
#define  IIC_INT_TXFTL_BELOW  	(1 << 4)
#define  IIC_INT_SRD_RES    	(1 << 5)
#define  IIC_INT_TX_ABRT   		(1 << 6)
#define  IIC_INT_STX_DONE   	(1 << 7)
#define  IIC_INT_ACTIVITY 		(1 << 8)
#define  IIC_INT_STOP_DET  		(1 << 9)
#define  IIC_INT_START_DET 		(1 << 10)
#define  IIC_INT_GEN_CALL  		(1 << 11)


typedef volatile struct
{
  REG  IC_CON;				/* 控制寄存器 */
  REG  IC_TAR;				/* 目的地址 */
  REG  IC_SAR;				/* 作为从机时的地址 */
  REG  rev1;				/* 未定义地址，无效 */
  REG  IC_DATA_CMD;			/* 数据和命令寄存器 */
  REG  IC_SS_SCL_HCNT;		/* 标准模式下时钟计数高位寄存器 */
  REG  IC_SS_SCL_LCNT;		/* 标准模式下时钟计数低位寄存器 */
  REG  IC_FS_SCL_HCNT;		/* 快速模式下时钟计数高位寄存器 */
  REG  IC_FS_SCL_LCNT;		/* 快速模式下时钟计数低位寄存器 */
  REG  rev2[2];				/* 未定义地址，无效 */
  REG  IC_INTR_STAT;		/* 中断状态寄存器 */
  REG  IC_INTR_MASK;		/* 中断屏蔽寄存器 */
  REG  IC_RAW_INTR_STAT;	/* 原始中断状态寄存器 */
  REG  IC_RX_TL;			/* 接收中断阈值寄存器，控制RX_FULL中断，0代表阈值为1,255代表阈值为256，硬件规定最大不超过缓冲区大小  */
  REG  IC_TX_TL;			/* 发送中断阈值寄存器，控制TX_EMPTY中断，0代表阈值为0,255代表阈值为255，硬件规定最大不超过缓冲区大小 */
  REG  IC_CLR_INTR;			/* 读寄存器清除组合和独立中断，IC_TX_ABRT_SOURCE将被清除  */
  REG  IC_CLR_RX_UNDER;		/* 读寄存器清除RX_UNDER中断 */
  REG  IC_CLR_RX_OVER;		/* 读寄存器清除RX_OVER中断 */
  REG  IC_CLR_TX_OVER;		/* 读寄存器清除TX_OVER中断 */
  REG  IC_CLR_RD_REQ;		/* 读寄存器清除RD_REQ中断 */
  REG  IC_CLR_TX_ABRT;		/* 读寄存器清除TX_ABRT中断，清除IC_TX_ABRT_SOURCE寄存器 */
  REG  IC_CLR_RX_DONE;		/* 读寄存器清除RX_DONE中断 */
  REG  IC_CLR_ACTIVITY;		/* 读寄存器获得ACTIVITY中断状态，此中断硬件自动清除 */
  REG  IC_CLR_STOP_DET;		/* 读寄存器清除STOP_DET中断 */
  REG  IC_CLR_START_DET;	/* 读寄存器清除START_DET中断 */
  REG  IC_CLR_GEN_CALL;		/* 读寄存器清除GEN_CALL中断 */
  REG  IC_ENABLE;		 	/* IIC使能寄存器，写1使能，写0失能 */
  REG  IC_STATUS;			/* 状态寄存器 */
  REG  IC_TXFLR;			/* 发送FIFO大小寄存器 */
  REG  IC_RXFLR;			/* 接收FIFO大小寄存器 */
  REG  rev26;				/* 未定义地址，无效 */
  REG  IC_TX_ABRT_SOURCE;	/* TX_ABRT中断中断源 */
  REG  rev27;				/* 未定义地址，无效 */
  REG  IC_DMA_CR;			/* DMA控制寄存器 */
  REG  IC_DMA_TDLR;			/* DMA发起传输的数据阈值 */
  REG  IC_DMA_RDLR;			/* DMA发起接的数据阈值，其值为寄存器值+1 */
}Struct_IIC_t, *pStruct_IIC_t;

/* IIC物理地址 */
#define 	IIC0_ADDR		((pStruct_IIC_t) CK_IIC_ADDRBASE0)

#endif /* CKI2C_H_ */
