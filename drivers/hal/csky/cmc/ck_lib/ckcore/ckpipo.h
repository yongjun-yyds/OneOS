/*
 * ckpipo.h
 *
 *  Created on: 2018-11-5
 *      Author: lenovo
 */

#ifndef _CKPIPO_H_
#define _CKPIPO_H_

#include "datatype.h"
#include "ckcore.h"

typedef struct PIPO_Info
{
	REG TIM_CR;             /*0x00 控制寄存器*/
	REG TIM_PSC;      		/*0x04 预分频器*/
	REG TIM_ARR;     		/*0x08 自动装载寄存器*/
	REG TIM_RCR;     		/*0x0C 重复次数寄存器*/
	REG TIM_CCRA;    		/*0x10 比较寄存器A*/
	REG TIM_CCRB;    		/*0x14 比较寄存器B*/
	REG TIM_DT_PRE;  		/*0x18 前死去时间寄存器*/
	REG TIM_DT_POST; 		/*0x1C 后死区时间寄存器*/
	REG RSD1;        		/*0x20 脉冲输出个数寄存器*/
	REG TIM_EGR;     		/*0x24 事件产生寄存器*/
	REG TIM_CNT;     		/*0x28 定时器内部计数器*/
	REG PW_H_A;      		/*0x2C 通道A高电平脉宽锁存器*/
	REG PW_L_A;      		/*0x30 通道A低电平脉宽锁存器*/
    REG PW_CNT_A;    		/*0x34 正交编码计数器*/
    REG PW_H_B;     		/*0x38 通道B高电平脉宽锁存器*/
    REG PW_L_B;      		/*0x3C 通道B低电平脉宽锁存器*/
    REG CW_MOTOR_DIR;		/*0x40正交编码器反馈方向寄存器*/
    REG PIPO_INT;    		/*0x44 中断类型寄存器*/
    REG PIPO_INT_DIS;		/*0x48 中断类型屏蔽寄存器*/
    REG CW_MAX_POS_ADDR;	/*0x4C 位置上限寄存器*/
    REG CW_MIN_POS_ADDR;	/*0x50 位置下限寄存器*/
} Struct_PIPOInfo, *pStruct_PIPOInfo;

/*指向PIPO相应的寄存器地址*/
#define 	PIPO0 	((pStruct_PIPOInfo)CK_PIPO0_ADDRBASE)
#define 	PIPO1 	((pStruct_PIPOInfo)CK_PIPO1_ADDRBASE)
#define 	PIPO2 	((pStruct_PIPOInfo)CK_PIPO2_ADDRBASE)
#define 	PIPO3 	((pStruct_PIPOInfo)CK_PIPO3_ADDRBASE)
#define 	PIPO4 	((pStruct_PIPOInfo)CK_PIPO4_ADDRBASE)
#define 	PIPO5 	((pStruct_PIPOInfo)CK_PIPO5_ADDRBASE)

#endif /* _CKPIPO_H_ */
