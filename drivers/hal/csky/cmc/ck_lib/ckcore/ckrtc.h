/*
 文件: ckrtc.h
 版本: CMC693 V1.10
 版权: 浙江杰芯科技有限公司
 作者: zhulin(zhulin2@supcon.com)
 描述：
修改记录：
=========
日期                                      作者                       工作                            内容
*/

#ifndef __CKRTC_H_
#define __CKRTC_H_

#include "ckcore.h"
#include "datatype.h"

typedef struct CKS_RTC
{
    REG  CCVR_L;
    REG  CMR_L;
    REG  CLR_L;
    REG  CCR;
    REG  STAT;
    REG  RSTAT;
    REG  EOI;
    REG  COMP_VERSION;
    REG  CCVR_H;
    REG  CMR_H;
    REG  CLR_H;
    REG  SYSTEM;
} Struct_RTCInfo, *pStruct_RTCInfo;

#define RTC           ((pStruct_RTCInfo)CK_RTC_ADDRBASE)

/* RTC_CCR寄存器位定义 */
#define RTC_CCR_WARP          (UINT32)(0x0008)                  /* rtc_wen使能位 */
#define RTC_CCR_EN            (UINT32)(0x0004)                  /* rtc_en计数器使能位 */
#define RTC_CCR_MASK          (UINT32)(0x0002)                  /* rtc_mask中断屏蔽控制位 */
#define RTC_CCR_IEN           (UINT32)(0x0001)                  /* rtc_ien中断使能位 */

/* RTC_STAT寄存器位定义 */
#define RTC_STAT_INTR         (UINT32)(0x0001)                  /* rtc_stat中断状态位 */

/* RTC_RSTAT寄存器位定义 */
#define RTC_RAWSTAT_INTR      (UINT32)(0x0001)                  /* rtc_rstat中断原始状态位 */

/* RTC_SYSTEM寄存器位定义 */
#define RTC_ISOLATION         (UINT32)(0x0001)                  /* rtc隔离模式控制位 */

#endif /* __CKRTC_H_ */
