/*
 文件: ckwdt.h
 版本: CMC693 V1.10
 版权: 浙江杰芯科技有限公司
 作者: zhulin(zhulin2@supcon.com)
 描述：
修改记录：
=========
日期                                      作者                       工作                            内容
*/

#ifndef __CKWDT_H_
#define __CKWDT_H_

#include "ckcore.h"
#include "datatype.h"

typedef struct
{
    REG  CR;
    REG  TORR;
    REG  CCVR;
    REG  CRR;
    REG  STAT;
    REG  EOI;
} Struct_WDTInfo,*pStruct_WDTInfo;

#define WDT     ((pStruct_WDTInfo)(CK_WDT_ADDRBASE))

#endif /* CKWDT_H_ */
