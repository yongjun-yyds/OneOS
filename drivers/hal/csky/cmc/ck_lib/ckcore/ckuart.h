/*
 文件: ckuart.h
 版本: CMC693 V1.10
 版权: 浙江杰芯科技有限公司
 作者: zhulin(zhulin2@supcon.com)
 描述：
修改记录：
=========
日期                                      作者                       工作                            内容
*/

#ifndef __CKUART_H__
#define __CKUART_H__

#include "ckcore.h"
#include "datatype.h"

typedef struct
{
    REG  RBR_THR_DLL;  /* RBR、THR、DLL地址偏移均为0x00，当LCR[7]=0时，读该地址为RBR寄存器的值，写该地址为THR的值，当LCR[7]=1时，该地址为DLL寄存器 */
    REG  IER_DLH;      /* IER和DLH的地址偏移均为0x04,当LCR[7]=1时，该地址代表DLH，当LCR[7]=0时，该地址代表IER */
    REG  IIR_FCR;      /* IIR和FCR的地址偏移均为0x08,读该地址为IIR的值，写该地址为FCR的值 */
    REG  LCR;
    REG  MCR;
    REG  LSR;
    REG  MSR;
    REG  rev[24];
    REG  USR;
} Struct_UartType,*pStruct_UartType;

#define UART0_BASEADDR   ((pStruct_UartType)(CK_UART0_ADDRBASE))
#define UART1_BASEADDR   ((pStruct_UartType)(CK_UART1_ADDRBASE))

//读取LSR寄存器所使用的
#define LSR_PFE              0x80
#define LSR_TEMT             0x40
#define LSR_TRANS_EMPTY      0x20
#define	LSR_BI               0x10
#define	LSR_FE               0x08
#define	LSR_PE               0x04
#define	LSR_OE               0x02
#define	LSR_DR               0x01


//配置LCR寄存器所使用的
#define LCR_SET_DLAB            0x80   /* enable r/w DLR to set the baud rate */
#define LCR_PARITY_ENABLE	    0x08   /* parity enabled */
#define LCR_PARITY_EVEN         0x10   /* Even parity enabled */
#define LCR_PARITY_ODD          0xef   /* Odd parity enabled */
#define LCR_WORD_SIZE_5         0xfc   /* the data length is 5 bits */
#define LCR_WORD_SIZE_6         0x01   /* the data length is 6 bits */
#define LCR_WORD_SIZE_7         0x02   /* the data length is 7 bits */
#define LCR_WORD_SIZE_8         0x03   /* the data length is 8 bits */
#define LCR_STOP_BIT1           0xfb   /* 1 stop bit */
#define LCR_STOP_BIT2           0x04   /* 1.5 stop bit */


//开关中断所使用的
#define IER_ERBFI				0x01   	/* Enable Received Data Available Interrupt. */
#define IER_ETBEI               0x02   	/* Enable Transmit Holding Register Empty Interrupt. */
#define IER_ELSI 				0x04	/* Enable Receiver Line Status Interrupt. */



//UART Status Register mask
#define USR_BUSY				0x01	/* UART Busy */
#define USR_TFNE				0x02	/* Transmit FIFO Not Full */
#define USR_TFE					0x04	/* Transmit FIFO Empty */
#define USR_RFNE				0x08	/* Receive FIFO Not Empty */
#define USR_RFF					0x10	/* Receive FIFO Full */


//
#define	LSR_DR					0x01 	/* Data Ready bit */
#define	LSR_OE					0x02	/* Overrun error bit */
#define	LSR_PE					0x04	/* Parity  Error  bit */
//....


//Interrupt ID
#define IIR_EMPTY		0x2
#define IIR_RECEIVED	0x4
#define IIR_RLS			0x6	// receiver line status,Overrun/parity/  framing  errors or break interrupt
#define IIR_BUSY		0x7 //busy
#define IIR_CH_TIMEOUT	0xC // character timeout

#endif
