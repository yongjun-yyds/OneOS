/*
 文件: ckcan.h
 版本: CMC693 V1.10
 版权: 浙江杰芯科技有限公司
 作者: zhulin(zhulin2@supcon.com)
 描述：
修改记录：
=========
日期                                      作者                       工作                            内容
*/

#ifndef CKCAN_H_
#define CKCAN_H_

#include "ckcore.h"
#include "datatype.h"

/* 标准模式下，CAN控制器寄存器结构体 */
typedef struct CAN_STANDARD
{
	REG	CR;		/* 0x00	控制器寄存器 */
    REG	CMR;	/* 0x04	命令寄存器 */
    REG	SR;		/* 0x08	状态寄存器 */
    REG	IR;		/* 0x0C	中断寄存器 */
    REG	ACR;	/* 0x10	验收代码寄存器，用于帧过滤设置 */
    REG	AMR;	/* 0x14	验收屏蔽寄存器，用于帧屏蔽设置 */
    REG	BTR0;	/* 0x18	总线定时寄存器0，定义波特率预设值BRP和同步跳转宽度SJW的值 */
    REG	BTR1;	/* 0x1C	总线定时寄存器1，定义每个位周期的长度采样点位置和采样数目 */
    REG  RSD0;	/* 0x20	保留 */
    REG  RSD1;	/* 0x24  保留 */
    REG	TXB0;	/* 0x28	发送缓冲器0 */
    REG	TXB1;	/* 0x2C	发送缓冲器1 */
    REG	TXB2;	/* 0x30	发送缓冲器2 */
    REG	TXB3;	/* 0x34	发送缓冲器3 */
    REG	TXB4;	/* 0x38	发送缓冲器4 */
    REG	TXB5;	/* 0x3C	发送缓冲器5 */
    REG	TXB6;	/* 0x40	发送缓冲器6 */
    REG	TXB7;	/* 0x44	发送缓冲器7 */
    REG	TXB8;	/* 0x48	发送缓冲器8 */
    REG	TXB9;	/* 0x4C	发送缓冲器9 */
    REG	RXB0;	/* 0x50	接收缓冲器0 */
    REG	RXB1;	/* 0x54	接收缓冲器1 */
    REG	RXB2;	/* 0x58	接收缓冲器2 */
    REG	RXB3;	/* 0x5C	接收缓冲器3 */
    REG	RXB4;	/* 0x60	接收缓冲器4 */
    REG	RXB5;	/* 0x64	接收缓冲器5 */
    REG	RXB6;	/* 0x68	接收缓冲器6 */
    REG	RXB7;	/* 0x6C	接收缓冲器7 */
    REG	RXB8;	/* 0x70	接收缓冲器8 */
    REG	RXB9;	/* 0x74	接收缓冲器9 */
    REG 	RSD2;	/* 0x78	保留 */
    REG	CDR;	/* 0x7C	时钟分频寄存器，用于使能CLKOUT引脚和控制输出频率 */
}Struct_CAN, *pStruct_CAN;

/* 扩展模式下，CAN控制器寄存器结构体 */
typedef struct CAN_EXTEND
{
	REG	MOD;	/* 0x00	模式寄存器 */
	REG	CMR;	/* 0x04	命令寄存器 */
	REG	SR;		/* 0x08	状态寄存器 */
	REG	IR;		/* 0x0C	中断寄存器 */
	REG	IER;	/* 0x10	中断使能寄存器 */
	REG	RSD0;	/* 0x14	保留 */
	REG	BTR0;	/* 0x18	总线定时寄存器0，定义波特率预设值BRP和同步跳转宽度SJW的值 */
	REG	BTR1;	/* 0x1C	总线定时寄存器1，定义每个位周期的长度采样点位置和采样数目 */
	REG  RSD1;	/* 0x20	保留 */
	REG  RSD2;	/* 0x24 	保留 */
	REG  RSD3;	/* 0x28 	保留 */
	REG  ALC;	/* 0x2C	仲裁丢失捕捉寄存器,存储仲裁丢失的位置信息 */
	REG	ECC;	/* 0x30	错误代码捕捉寄存器，包含总线错误的类型和位置信息 */
	REG	EWLR;	/* 0x34	错误报警限制寄存器，定义错误报警限制值，复位模式才允许写入 */
	REG	RXERR;	/* 0x38	RX错误计数寄存器，包含接收错误计数器当前值，复位模式才允许写入 */
	REG	TXERR;	/* 0x3C	TX错误计数寄存器，包含发送错误计数器的当前值，复位模式才允许写入 */

	union
	{
    	struct
    	{
    		REG	ACR0;	/* 0x40	验收代码寄存器0，用于帧过滤设置 */
    		REG	ACR1;	/* 0x44	验收代码寄存器1，用于帧过滤设置 */
    		REG	ACR2;	/* 0x48	验收代码寄存器2，用于帧过滤设置 */
    		REG	ACR3;	/* 0x4C	验收代码寄存器3，用于帧过滤设置 */
    		REG	AMR0;	/* 0x50	验收屏蔽寄存器0，用于帧屏蔽设置 */
    		REG	AMR1;	/* 0x54	验收屏蔽寄存器1，用于帧屏蔽设置 */
    		REG	AMR2;	/* 0x58	验收屏蔽寄存器2，用于帧屏蔽设置 */
    		REG	AMR3;	/* 0x5C	验收屏蔽寄存器3，用于帧屏蔽设置 */
    		REG	RSD4;	/* 0x60	保留 */
    		REG	RSD5;	/* 0x64	保留 */
    		REG	RSD6;	/* 0x68	保留 */
    		REG	RSD7;	/* 0x6C	保留 */
    		REG	RSD8;	/* 0x70	保留 */
    	};

    	struct
    	{
    		REG	RXB0;	/* 0x40	接收缓冲器0 */
    		REG	RXB1;	/* 0x44	接收缓冲器1 */
    		REG	RXB2;	/* 0x48	接收缓冲器2 */
    		REG	RXB3;	/* 0x4C	接收缓冲器3 */
    		REG	RXB4;	/* 0x50	接收缓冲器4 */
    		REG	RXB5;	/* 0x54	接收缓冲器5 */
    		REG	RXB6;	/* 0x58	接收缓冲器6 */
    		REG	RXB7;	/* 0x5C	接收缓冲器7 */
    		REG	RXB8;	/* 0x60	接收缓冲器8 */
    		REG	RXB9;	/* 0x64	接收缓冲器9 */
    		REG	RXB10;	/* 0x68	接收缓冲器10 */
    		REG	RXB11;	/* 0x6C	接收缓冲器11 */
    		REG	RXB12;	/* 0x70	接收缓冲器12 */
    	};

    	struct
    	{
    		REG	TXB0;	/* 0x40	发送缓冲器0 */
    		REG	TXB1;	/* 0x44	发送缓冲器1 */
    		REG	TXB2;	/* 0x48	发送缓冲器2 */
    		REG	TXB3;	/* 0x4C	发送缓冲器3 */
    		REG	TXB4;	/* 0x50	发送缓冲器4 */
    		REG	TXB5;	/* 0x54	发送缓冲器5 */
    		REG	TXB6;	/* 0x58	发送缓冲器6 */
    		REG	TXB7;	/* 0x5C	发送缓冲器7 */
    		REG	TXB8;	/* 0x60	发送缓冲器8 */
    		REG	TXB9;	/* 0x64	发送缓冲器9 */
    		REG	TXB10;	/* 0x68	发送缓冲器10 */
    		REG	TXB11;	/* 0x6C	发送缓冲器11 */
    		REG	TXB12;	/* 0x70	发送缓冲器12 */
    	};
	};

	REG	RMC;	/* 0x74	RX信息计数器，反映RXFIFO中可用的信息数目 */
	REG	RSD9;	/* 0x78	保留 */
	REG	CDR;	/* 0x7C	模式选择: 0 标准 ， 1 扩展. */
}Struct_CAN_Ex, *pStruct_CAN_Ex;

/* 指向can相应的寄存器地址 */
#define 	CAN0_BASE_ADDR 			(CK_CAN0_ADDRBASE)
#define		CAN1_BASE_ADDR			(CK_CAN1_ADDRBASE)

#endif /* CKCAN_H_ */
