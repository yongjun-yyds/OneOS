/*
 文件: ckpowm.h
 版本: CMC693 V1.10
 版权: 浙江杰芯科技有限公司
 作者: zhulin(zhulin2@supcon.com)
 描述：
 修改记录：
=========
日期                                      作者                       工作                            内容
*/

#ifndef __CK_POWM_H__
#define __CK_POWM_H__

#include "ckcore.h"

#define POWM           				((volatile pPOWM_st)CK_POWM_ADDRBASE)

#define 	IP_WDT					( 1 << 0)
#define 	IP_TIM					( 1 << 1)
#define 	IP_GPIO0				( 1 << 2)
#define 	IP_GPIO1				( 1 << 3)
#define 	IP_UART0				( 1 << 4)
#define 	IP_SPI					( 1 << 5)
#define 	IP_IIC					( 1 << 6)
#define 	IP_PIPO					( 1 << 7)
#define 	IP_CAN0					( 1 << 8)
#define 	IP_SPIS					( 1 << 9)
#define 	IP_RTC					( 1 << 10)
#define 	IP_UART1				( 1 << 11)
#define 	IP_CAN1					( 1 << 12)
#define 	IP_SPI1					( 1 << 13)
//#define 	IP_RESV					( 1 << 14)
//#define 	IP_RESV					( 1 << 15)
#define 	IP_DMAC					( 1 << 16)
#define 	IP_MAC0					( 1 << 17)
#define 	IP_MAC1					( 1 << 18)
#define 	IP_LCP					( 1 << 19)
#define 	IP_MCP					( 1 << 20)

/* POWM中断定义 */
#define POWM_INT_NOTMASK      		0x0
#define POWM_INT_MASK         		0x7
#define POWM_CPUTOLP_INT      		0x2
#define POWM_CLEARINT         		0x0

typedef struct{

    REG LPCR;		/*  Low power control register */
    REG SMCR;		/*  Stop mode control register */
    REG PCR;			/*  PLL control register */
    REG PLTR;		/*  PLL lock-time wait-time register */
    REG CRCR;		/*  Clock Ratio control register */
    REG CGCR;		/*  Clock gate control register */
    REG CGSR;		/*  Clock gate status register */
    REG INTM;		/*  Interrupt source mask R*/
    REG INTR;		/*  Interrupt source register */
    REG DCNT;		/*  CPU wake-up delay counter register */
    REG PRCR;
    REG PCR2;
    REG FICR;
    REG SCCR;
    REG WUSR;
    REG RSTEN;
    REG RSTCR;
    REG RSTCNT;
    REG DIV2;
    REG ECGER;
    REG EFSCR;
} POWM_st, *pPOWM_st;

#endif
