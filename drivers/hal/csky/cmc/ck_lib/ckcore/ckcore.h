/*
 * Description: ck5a6.h - Define the system configuration, memory & IO base
 * address, flash size & address, interrupt resource for ck5a6 soc.
 *
 * Copyright (C) : 2008 Hangzhou C-SKY Microsystems Co.,LTD.
 * Author(s): Liu Bing (bing_liu@c-sky.com)
 * Contributors: Liu Bing
 * Date:  2010-06-26
 * Modify by liu jirang  on 2012-09-11
 */

#ifndef __INCLUDE_CK5A6_H
#define __INCLUDE_CK5A6_H

#if CONFIG_CKCPU_MMU
#define PERI_BASE 0xa0000000
#else
#define PERI_BASE 0x0
#endif

/**************************************
 * MCU & Borads.
 *************************************/

#define CONFIG_CPU_CORE_CLK    200000000

/* PLL input ckock(crystal frequency) */
#define CONFIG_PLL_INPUT_CLK   10000000   /* HZ */
#define CONFIG_S2C_INPUT_CLK   10000000   /* HZ */

/* CPU frequence definition */
#define CPU_DEFAULT_FREQ       10000000   /* Hz */
/* AHB frequence definition */
#define AHB_DEFAULT_FREQ       10000000   /* Hz */
/* APB frequence definition */
#define APB_DEFAULT_FREQ       10000000   /* Hz */

/**********************************************
 * Config CPU cache
 *********************************************/
/* 0 - rw; 1 - rwc; 2 - rwc; 3 - rw */
#define CONFIG_CKCPU_MGU_BLOCKS         0xff01 //0xff02

/* 0 - baseaddr: 0x0; size: 4G */
#define CONFIG_CKCPU_MGU_REGION1        (0x100000 | 0x25) //0x3f
/* 1- baseaddr: 0x8000000; size: 8M */
#define CONFIG_CKCPU_MGU_REGION2         (0x160000 | 0x21) //0x800002d
/* 2- baseaddr: 0x8600000; size: 256K for MAC */
#undef CONFIG_CKCPU_MGU_REGION3        //0x8600023
/* 3- Disable */
#define CONFIG_CKCPU_MGU_REGION4         (0x10000000 | 0x23)

/*******************************
 * Config CPU cache
 ******************************/
#define CONFIG_CKCPU_ICACHE             1
#define CONFIG_CKCPU_DCACHE             0

/************************************************
 * perpheral module baseaddress and irq number
 ***********************************************/
/**** AHB BUS ****/
#define CK_AHBBUS_BASE			((volatile CK_UINT32*)(0x10000000+PERI_BASE))

/*
 * SDRAM Config Register set,because ck5a6 need to reset memory when init.
 * TODO: no used configuration
 */
#define MMC_SCONR				(0x10001000+PERI_BASE)

/***** Intc ******/
#define CK_INTC_BASEADDRESS		(0x10010000+PERI_BASE)
#define CK_INTC1_BASEADDRESS	(0x10011000+PERI_BASE)
#define CK_INTC2_BASEADDRESS	(0x10012000+PERI_BASE)

/*
 * define irq number of perpheral modules
 */
#define  CK_INTC_GPIO0A0     0
#define  CK_INTC_GPIO0A1     1
#define  CK_INTC_GPIO0A2     2
#define  CK_INTC_GPIO0A3     3
#define  CK_INTC_GPIO1A      4
#define  CK_INTC_TIMER0      5
#define  CK_INTC_TIMER1    	 6
#define  CK_INTC_TIMER2      7
#define  CK_INTC_TIMER3      8
#define  CK_INTC_PIPO0     	 9
#define  CK_INTC_PIPO1    	10
#define  CK_INTC_PIPO2      11
#define  CK_INTC_PIPO3      12
#define  CK_INTC_RTC        13
#define  CK_INTC_DMAC       14
#define  CK_INTC_UART0      15
#define  CK_INTC_UART1      16
#define  CK_INTC_SPI        17
#define  CK_INTC_I2C        18
#define  CK_INTC_WDT        19
#define  CK_INTC_MAC0       20
#define  CK_INTC_MAC1       21
#define  CK_INTC_POWM       22
#define  CK_INTC_MCP        23
#define  CK_INTC_LCP        24
#define  CK_INTC_CAN        25
#define  CK_INTC_CAN1       26
#define  CK_INTC_SPIS       27
#define  CK_INTC_PIPO4      28
#define  CK_INTC_PIPO5      29

/***** GPIO *****/
#define PCK_GPIO			((volatile CK_UINT32*)(0x10019000+PERI_BASE))
#define PCK_GPIO1			((volatile CK_UINT32*)(0x1001A000+PERI_BASE))

/**** MAC *******/
#define MAC0_BASE_ADD		(volatile CK_UINT32*)(0x10008000+PERI_BASE) /* mac base address */
#define MAC0_DMA_BASE_ADD	(volatile CK_UINT32*)(0x10009000+PERI_BASE) /* BD base addr*/
#define MAC1_BASE_ADD		(volatile CK_UINT32*)(0x1000E000+PERI_BASE) /* mac base address */
#define MAC1_DMA_BASE_ADD	(volatile CK_UINT32*)(0x1000F000+PERI_BASE) /* BD base addr*/

/***** Uart *******/
#define CK_UART0_ADDRBASE    (volatile CK_UINT32 *)(0x10015000+PERI_BASE)
#define CK_UART1_ADDRBASE    (volatile CK_UINT32 *)(0x10017000+PERI_BASE)

/***** CAN *******/
#define CK_CAN0_ADDRBASE    (volatile CK_UINT32 *)(0x1001D000+PERI_BASE)
#define CK_CAN1_ADDRBASE    (volatile CK_UINT32 *)(0x1001F000+PERI_BASE)

/**** Timer ****/
#define  CK_TIMER0_BASSADDR			(volatile CK_UINT32 *)(0x10013000+PERI_BASE)
#define  CK_TIMER1_BASSADDR			(volatile CK_UINT32 *)(0x10013014+PERI_BASE)
#define  CK_TIMER2_BASSADDR			(volatile CK_UINT32 *)(0x10013028+PERI_BASE)
#define  CK_TIMER3_BASSADDR			(volatile CK_UINT32 *)(0x1001303c+PERI_BASE)

#define  CK_TIMER_CONTROL_BASSADDR	(volatile CK_UINT32 *)(0x100130a0+PERI_BASE)

/****** DMA *****/
/*
 * Define DMA channel base address
 */
#define CK_DMA_BASEADDR  	  (volatile CK_UINT32 *)(0x10006000+PERI_BASE)
#define CK_DMAC_CH1_BASEADDR  (volatile CK_UINT32 *)(0x10006000+PERI_BASE)
#define CK_DMAC_CH2_BASEADDR  (volatile CK_UINT32 *)(0x10006058+PERI_BASE)
/*
 * Define DMA control base address
 */ 
#define CK_DMAC_CONTROL_BASEADDR  (volatile CK_UINT32 *)(0x100062c0+PERI_BASE)

/****** IIC *************/
/*
 * Define IIC base address
 */
#define CK_IIC_ADDRBASE0	(volatile CK_UINT32 *)(0x10018000+PERI_BASE)

/****** SPI *************/
/*
 * Define SPI base address
 */
#define CK_SPI_ADDRBASE0	(volatile CK_UINT32 *)(0x1001C000+PERI_BASE)
#define CK_SPIS_ADDRBASE0	(volatile CK_UINT32 *)(0x10016000+PERI_BASE)

#define CK_SPI1_ADDRBASE0	(volatile CK_UINT32 *)(0x10020000+PERI_BASE)

/****** WDT *************/
#define CK_WDT_ADDRBASE		(volatile CK_UINT32 *)(0x10014000+PERI_BASE)

/****** RTC *************/ 
#define CK_RTC_ADDRBASE		(volatile CK_UINT32 *)(0x1001E000+PERI_BASE)

/****** POWM  *************/
#define CK_POWM_ADDRBASE	(volatile CK_UINT32 *)(0x1000B000+PERI_BASE)

/****** LCP *************/
#define CK_LCP_ADDRBASE		(volatile CK_UINT32 *)(0x10004000+PERI_BASE)

/****** MCP  *************/
#define CK_MCP_ADDRBASE		(volatile CK_UINT32 *)(0x10002000+PERI_BASE)

/****** PIPO  *************/
/*#define CK_PIPO_ADDRBASE	(volatile CK_UINT32 *)(0x1001B000+PERI_BASE)*/
/* ��6��PIPOģ��,yuwenxing */
#define CK_PIPO0_ADDRBASE	(volatile CK_UINT32 *)(0x1001B000+PERI_BASE)
#define CK_PIPO1_ADDRBASE	(volatile CK_UINT32 *)(0x1001B100+PERI_BASE)
#define CK_PIPO2_ADDRBASE	(volatile CK_UINT32 *)(0x1001B200+PERI_BASE)
#define CK_PIPO3_ADDRBASE	(volatile CK_UINT32 *)(0x1001B300+PERI_BASE)
#define CK_PIPO4_ADDRBASE	(volatile CK_UINT32 *)(0x1001B400+PERI_BASE)
#define CK_PIPO5_ADDRBASE	(volatile CK_UINT32 *)(0x1001B500+PERI_BASE)

#endif /* __INCLUDE_CK5A6_H */
