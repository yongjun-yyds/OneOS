/*
 文件: datatype.h
 版本: CMC693 V1.10
 版权: 浙江杰芯科技有限公司
 作者: zhulin(zhulin2@supcon.com)
 描述：
修改记录：
=========
日期                                      作者                       工作                            内容
*/
#ifndef	__DATATYPE_H__
#define	__DATATYPE_H__

/*
#include   <limits.h>

#pragma pack(4)
*/

////////////////////////////////////////////////////////////////////////////////////////
//
//
#ifndef NULL
#define	NULL  0x00
#endif

#ifndef TRUE
#define TRUE  0x01
#endif
#ifndef FALSE
#define FALSE 0x00
#endif

#ifndef true
#define true  0x01
#endif
#ifndef false
#define false 0x00
#endif

#ifndef SUCCESS
#define SUCCESS  0
#endif
#ifndef FAILURE
#define FAILURE  -1 
#endif
#define TIMEOUT         0x1000

#define	STATUS_ERR	1
#define	STATUS_OK	0

typedef	unsigned char       CK_UINT8;
typedef unsigned short      CK_UINT16;
typedef unsigned int        CK_UINT32;
typedef	signed char         CK_INT8;
typedef signed short        CK_INT16;
typedef signed int          CK_INT32;
/* typedef signed long long    CK_INT64; */
typedef signed long         CK_INT64;
typedef unsigned int        BOOL;
#ifndef BYTE
typedef	unsigned char	    BYTE;
#endif
#ifndef WORD
typedef unsigned short	    WORD;
#endif

typedef volatile unsigned int 		CK_REG;
typedef volatile unsigned short 	CK_SREG;
typedef volatile unsigned char 		CK_CREG;

// FIXME:
typedef struct
{
	CK_UINT16 year;
	CK_UINT8  month;
	CK_UINT8  day;
	CK_UINT8  weekday;
	CK_UINT8  hour;
	CK_UINT8  min;
	CK_UINT8  sec;
}__attribute__((packed)) RTCTIME, *pRTCTIME;


#if defined(DEBUG)
#define Debug     printf
#else
#define Debug
#endif

#define  IN
#define  OUT
#define INOUT

typedef volatile unsigned int 		REG;
typedef volatile unsigned short 	SREG;
typedef volatile unsigned char 		CREG;

typedef REG * 						pREG;

//typedef unsigned char 				BOOL;

typedef signed char					INT8S;
typedef signed short	 			INT16S;
typedef signed  int					INT32S;
typedef signed long long 			INT64S;

typedef unsigned char 				INT8U;
typedef unsigned short  			INT16U;
typedef unsigned int 				INT32U;

typedef signed char         		INT8, *PINT8;
typedef signed short        		INT16, *PINT16;
typedef signed int          		INT32, *PINT32;
typedef signed long long    		INT64, *PINT64;

typedef unsigned char       		UINT8, *PUINT8;
typedef unsigned short     	 		UINT16, *PUINT16;
typedef unsigned int        		UINT32, *PUINT32;
typedef unsigned long long 	  		UINT64, *PUINT64;

typedef float 						Float32;
typedef double 						Float64;

typedef unsigned int 				CPU_Type;

///////////////////////////////////////////////////////////////////////////////////////
#endif  // __DATATYPE_H__
///////////////////////////////////////////////////////////////////////////////////////
