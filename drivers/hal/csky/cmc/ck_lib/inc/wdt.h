/*
 文件: wdt.h
 版本: CMC693 V1.10
 版权: 浙江杰芯科技有限公司
 作者: zhulin(zhulin2@supcon.com)
 描述：
修改记录：
=========
日期                                      作者                       工作                            内容
*/

#ifndef __WDT_H_
#define __WDT_H_

#include "ckwdt.h"

#define INTC_WDT                 CK_INTC_WDT

#define WDT_PclkCycles_Bit       ((UINT8)0x1c)            /* WDT系统复位的保持时间选择位 */
#define WDT_PclkCycles_2         ((UINT8)0x00)            /* 2个plck cycles */
#define WDT_PclkCycles_4         ((UINT8)0x04)            /* 4个plck cycles */
#define WDT_PclkCycles_8         ((UINT8)0x08)            /* 8个plck cycles */
#define WDT_PclkCycles_16        ((UINT8)0x0c)            /* 16个plck cycles */
#define WDT_PclkCycles_32        ((UINT8)0x10)            /* 32个plck cycles */
#define WDT_PclkCycles_64        ((UINT8)0x14)            /* 64个plck cycles */
#define WDT_PclkCycles_128       ((UINT8)0x18)            /* 128个plck cycles */
#define WDT_PclkCycles_256       ((UINT8)0x1c)            /* 256个plck cycles */

#define WDT_TopInit_Bit          ((UINT8)0xf0)            /* TOP_INIT超时周期初始值的配置位 */
#define WDT_Top_Bit              ((UINT8)0x0f)            /* TOP超时周期的配置位 */

#define WDT_CRR_RESTART          ((UINT8)0x76)            /* 重新启动WDT的计数器，只能写入0x76 */
#define WDT_CR_EN                ((UINT8)0x01)            /* WDT使能 */

#define WDT_CR_Mode_Bit          ((UINT8)0x02)
#define WDT_CR_Mode_SYSReset     ((UINT8)0x00)            /* 设置WDT工作模式：当计数器溢出后直接产生系统复位  */
#define WDT_CR_Mode_Intr         ((UINT8)0x02)            /* 设置WDT工作模式：当计数器第一次溢出时产生中断，第二次溢出且中断标志未清除则产生系统复位  */

typedef enum
{
    WDT_SysResetMode  = 0,
    WDT_InterruptMode
} CKEnum_WDT_Mode;

typedef struct{
	UINT8 cMode;                                /* 设置WDT的工作模式，可选值如下：
	                                               WDT_CR_Mode_SYSReset，当计数器溢出后直接产生系统复位
	                                               WDT_CR_Mode_Intr，当计数器第一次溢出时产生中断，第二次
	                                                                                                                     溢出且中断标志未清除则产生系统复位*/
	UINT32 dwMs;                                /* 超时时间-毫秒，范围1ms~42s */
	UINT32 dwPrio;                              /* 中断优先级，此参数在WDT设置为WDT_CR_Mode_Intr工作模式有效，
	                                               WDT设置为WDT_CR_Mode_SYSReset工作模式时无效，可为任一值*/
	void (*WDT_ISR)(UINT32);                    /* 中断处理回调函数，由用户自定义  */
}WDT_InitStructure,*pWDT_InitStructure;

/*
 * 设置WDT复位信号保持时间
 * 输入参数：cCycles: 用于控制WDT产生的系统复位信号的时间，一般为几个pclk cycles
 *         可设置如下倍数的pclk cycles
 *         WDT_PclkCycles_2,
 *         WDT_PclkCycles_4,
 *         WDT_PclkCycles_8,
 *         WDT_PclkCycles_16,
 *         WDT_PclkCycles_32,
 *         WDT_PclkCycles_64,
 *         WDT_PclkCycles_128,
 *         WDT_PclkCycles_256
 * 输出参数：void
 * 说明：
 */
void WDT_SetSystemResetCycles(UINT8 cCycles);

/*
 * 设置WDT计数器的超时周期初始值
 * 输入参数：cValue:需要配置的参数，范围：0~15
 * 输出参数：void
 * 说明：用于配置寄存器WDT_TORR中的TOP_INIT位，在第一次启动WDT计数器时，根据此寄存器的
 *      配置装载计数器值，必须在使能WDT计数器前配置.
 *      超时周期=2的（16+cValue）次方
 */
void WDT_SetInitPeriod(UINT8 cValue);

/*
 * 设置WDT计数器的超时周期
 * 输入参数：cValue:需要配置的参数，范围：0~15
 * 输出参数：void
 * 说明：用于配置寄存器WDT_TORR中的TOP位，在第二次启动WDT计数器时及以后，根据此寄存器的
 *      配置装载计数器值.可在WDT启动后任意时刻更改超时周期，但要在下一次启动计数器时生效。
 *      超时周期=2的（16+cValue）次方
 */
void WDT_SetPeriod(UINT8 cValue);

/*
 * 设置WDT的工作模式
 * 输入参数：cMode：工作模式
 *        WDT_CR_Mode_SYSReset，当计数器溢出后直接产生系统复位
 *        WDT_CR_Mode_Intr，当计数器第一次溢出时产生中断，第二次溢出且中断标志未清除
 *                         则产生系统复位
 * 输出参数：无
 * 说明：
 */
void WDT_SetMode(UINT8 cMode);

/*
 * 使能WDT
 * 输入参数：无
 * 输出参数：无
 * 说明：
 */
void WDT_Enable(void);

/*
 * 获取WDT当前计数器的值
 * 输入参数：无
 * 输出参数：WDT当前的计数器值
 * 说明：
 */
UINT32 WDT_GetCounter(void);

/*
 * 清除WDT中断标志
 * 输入参数：无
 * 输出参数：无
 * 说明：通过读EOI寄存器可清除中断标志，但不会改变计数器的值
 */
UINT32 WDT_ClearIntr(void);

/*
 * 配置WDT
 * 输入参数：pWDTInfo:为初始化参数的结构体指针，其具体参数如下：
 *        cMode:设置WDT的工作模式，可选值如下：
 *               WDT_CR_Mode_SYSReset，当计数器溢出后直接产生系统复位
 *               WDT_CR_Mode_Intr，当计数器第一次溢出时产生中断，第二次溢出且中断标志未清除
 *                               则产生系统复位
 *        dwMs：超时时间-毫秒，范围1ms~42s
 *        dwPrio:中断优先级，此参数在WDT设置为WDT_CR_Mode_Intr工作模式有效，
 *               WDT设置为WDT_CR_Mode_SYSReset工作模式时无效，可为任一值
 *        WDT_ISR：用户自定义的中断处理函数。
 * 输出参数：void
 * 说明：该函数内部固定选择WDT系统复位时间为2个pclk cycles，并启动WDT；若cMode为WDT_CR_Mode_Intr，则
 *      函数内会注册中断回调函数，中断回调函数接口在bsp.c中由用户实现具体功能。
 *      超时周期=2的（16+cTOP）次方，cTOP为TOP_INIT或者TOP寄存器的值,范围0~15
 *      函数内部根据dwMs计算出TOP和TOP_INIT的值，实际计算出的超时时间总是大于等于设置的
 *      超时时间
 */
void WDT_Config(pWDT_InitStructure pWDTInfo);

/*
 * 重启WDT计数器,向WDT_CRR寄存器写入0x76，使WDT重新装在计数器的值，并
 * 重新启动计数器
 * 输入参数：void
 * 输出参数：void
 * 说明：
 */
void WDT_Restart(void);

#endif /* WDT_H_ */
