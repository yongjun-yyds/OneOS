/*
 文件: i2c.h
 版本: CMC693 V1.10
 版权: 浙江杰芯科技有限公司
 作者: zhulin(zhulin2@supcon.com)
 描述：
修改记录：
=========
日期                                      作者                       工作                            内容
*/

#ifndef DRVI2C_H_
#define DRVI2C_H_

#include "cki2c.h"
#include "ckcore.h"

/*IIC工作模式*/
typedef enum{
  IIC_Character_Slave       = 0,
  IIC_Character_Master      = 1,
  IIC_Character_SlaveMaster = 2,
  IIC_Character_Unchange	= 3
}Enum_IIC_Character;

/*IIC地址位数*/
typedef enum{
  IIC_AddressMode_7bits  = 0,                        /* 7位地址模式 */
  IIC_AddressMode_10bits = 1                         /* 10位地址模式 */
}Enum_IIC_AddressMode;

/*IIC接收发送数据方式*/
typedef enum{
  IIC_OperationMode_Block 	  = 0,                   /* 查询方式通信 */
  IIC_OperationMode_Interupt  = 1                    /* 中断方式通信 */
}Enum_IIC_OperationMode;

/*IIC通信速度*/
typedef enum{
  IIC_SpeedMode_Standard = 0,	                     /* 100 kbit/s */
  IIC_SpeedMode_Fast     = 1	                     /* 400 kbit/s */
}Enum_IIC_SpeedMode;

typedef struct
{
	UINT8  *pBuffer;
	UINT16 size;			                          /* 当前大小 */
	UINT16 index;			                          /* 当前索引 */
}Struct_IICBuffer, *pStruct_IICBuffer;

/*IIC 的初始化结构体*/
typedef struct
{
	pStruct_IIC_t IIC_BaseAddr;                       /* IIC模块基地址 */
	UINT8 cIICIntr;                                   /* 是否开启IIC中断 */
	UINT32 dwPriority;                                /* IIC中断优先级 */
	void (*IIC_ISR)(UINT32);                          /* 由用户自定义的中断处理函数 */
	UINT16 IIC_Addr;							      /* 本机IIC地址 */
	UINT8 TxFTLevel;							      /* 发送缓冲区报警阈值。*/
	UINT8 RxFTLevel;							      /* 接收缓冲区报警阈值。 */
	Enum_IIC_Character IIC_Character;			      /* IIC的主从模式 */
	Enum_IIC_AddressMode IIC_AddrMode;   			  /* 地址模式 7/10bit */
	Enum_IIC_SpeedMode IIC_SpeedMode;				  /* IIC传输速率设置 */
	Struct_IICBuffer IIC_TxBuffer;					  /* 发送缓存????该缓存是在初始化时提供，还是在调用IIC_Transceive_Interrupt时提供？ */
	Struct_IICBuffer IIC_RxBuffer;					  /* 接收缓存 */
}IIC_InitStruct, *pIIC_InitStruct;

typedef struct
{
	IIC_InitStruct IIC_Initstruct;                    /* IIC初始化信息结构体 */
	pStruct_IIC_t IIC_BaseAddr;                       /* IIC模块基地址 */
	UINT16 wCASLock;                                  /* 自锁保护器 */
	UINT16 wErrFlag;                                  /* 错误标志 */
	void (*IIC_TxRx_CBFunc)(void);                    /* 收发完成回调函数,函数实体由用户自定义 */
}Struct_IICInfo, *pStruct_IICInfo;

extern Struct_IICInfo IIC0;

#define IIC_IS_TXFIFO_FULL(pIICInfo)	(0 == (pIICInfo->IIC_BaseAddr->IC_STATUS & 0x02))  //判断TXFIFO中是否为满
#define IIC_IS_TXFIFO_EMPTY(pIICInfo)	(0 != (pIICInfo->IIC_BaseAddr->IC_STATUS & 0x04))  //判断TXFIFO中是否为空
#define IIC_IS_RXFIFO_EMPTY(pIICInfo)	(0 == (pIICInfo->IIC_BaseAddr->IC_STATUS & 0x08))  //判断RXFIFO中是否为空
#define IIC_IS_RXFIFO_FULL(pIICInfo)	(0 != (pIICInfo->IIC_BaseAddr->IC_STATUS & 0x10))  //判断RXFIFO中是否为满
#define IIC_IS_MASTER_IDLE(pIICInfo)	(0 == (pIICInfo->IIC_BaseAddr->IC_STATUS & 0x20))  //判断主机是否为空闲
#define IIC_IS_SLAVE_IDLE(pIICInfo)		(0 == (pIICInfo->IIC_BaseAddr->IC_STATUS & 0x40))  //判断从机是否为空闲
#define IIC_IS_IDLE(pIICInfo)		    (0 == (pIICInfo->IIC_BaseAddr->IC_STATUS & 0x01))  //判断IIC是否为空闲
#define IIC_IS_RXFTL_ABOVE(pIICInfo)	(1 == (pIICInfo->IIC_BaseAddr->IC_RAW_INTR_STAT & 0x04) ) //判断IIC的RxFIFO到达阈值

/*
 * IIC 实例化
 * 输入参数：pIICInfo： IIC配置信息结构体；
 * 输出参数：
 */
void IIC_Config(pIIC_InitStruct pIICInfo);

/*
 * IIC 设置参数
 * 输入参数：pIICInfo      IIC信息结构体指针
 *        wIIC_Addr     本机IIC地址
 * 		  IIC_Character IIC设备角色
 *        IIC_AddrMode  地址模式，
 *	      IIC_SpeedMode 速度模式
 * 输出参数：返回 0， 成功; 其他，失败。
 * 说明：wIIC_Addr = 0 时不改变IIC地址。
 */
int IIC_SetParameters(pStruct_IICInfo pIICInfo,
		              UINT16 wIIC_Addr,
					  Enum_IIC_Character IIC_Character,
					  Enum_IIC_AddressMode IIC_AddrMode,
					  Enum_IIC_SpeedMode IIC_SpeedMode );

/*
 * 从接收缓存中读取数据
 * 输入参数：pIICInfo： IIC信息结构体指针
 *        cRxLevel 需要读取的缓存数目(0不指定数量，表示读空当前缓存)
 * 输出参数：返回  读取到数据长度
 */
int IIC_Read_RxFIFO(pStruct_IICInfo pIICInfo, UINT8 cRxLevel);

/*
 * IIC关闭
 * 输入参数：pIICInfo, IIC信息结构体指针
 * 输出参数：返回 -1关闭失败，0关闭成功
 */
int IIC_Close(pStruct_IICInfo pIICInfo);

/*
 * IIC 解锁
 * 输入参数：pIICInfo： IIC信息结构体指针
 * 输出参数：无
 * 说明：每次收发开始时上锁，获取权限，收发完成后释放权限。
 */
void IIC_UnLock(pStruct_IICInfo pIICInfo);

/*
 * 从发送缓存中发送数据
 * 输入参数：pIICInfo： IIC信息结构体指针
 *        cTxLevel 需要写入的缓存数目(0不指定数量，表示写满当前缓存)
 * 输出参数：返回  实际写入长度
 * 说明：16位长度，理论上数据长度不能超过65535/2，否则会导致函数中相加计算出错。
 */
int IIC_Write_TxFIFO(pStruct_IICInfo pIICInfo, UINT8 cTxLevel);

/*
 * IIC 虚写 不关心写入内容
 * 输入参数：pIICInfo, IIC信息结构体指针
 *        cTxLevel 写入长度
 * 输出参数：无
 * 说明：cTxLevel = 0 时表示写满Tx FIFO
 */
void IIC_DummyWrite(pStruct_IICInfo pIICInfo, UINT8 cTxLevel);

/*
 * 清除IIC中断
 * 输入参数：pIICInfo： IIC信息结构体指针
 *        wTntrVector:产生的中断
 * 输出参数：返回 读到的寄存器值
 * 说明：每次收发开始时上锁，获取权限，收发完成后释放权限。
 */
UINT16 IIC_ClearInterrupt(pStruct_IICInfo pIICInfo, UINT16 wTntrVector);

/*
 * IIC 【阻塞方式】进行数据收发
 * 输入参数：pIICInfo, 	IIC信息结构体指针
 * 		  wTargetAddr 	目的地址
 * 		  pcTxBuffer  	发送缓存指针 （如不使用，可指定为NULL ）
 * 		  wTxLength   	发送数据长度 （如不使用，可指定为0 ）
 * 		  pcRxBuffer		接收数据指针 （如不使用，可指定为NULL ）
 * 		  wRxLength   	接收数据长度 （如不使用，可指定为0 ）
 * 输出参数： > 0 实际接收到的数据长度 ; <= 0 出错.
 * 说明：TargetAddr 最高位置位时，显式指定10bit地址。如果当前IIC模块正处于活动状态，则放弃本次操作。
 */
int IIC_Transceive( pStruct_IICInfo pIICInfo,  UINT16 wTargetAddr,
					UINT8  *pcTxBuffer, UINT16 wTxLength,
					UINT8  *pcRxBuffer, UINT16 wRxLength);

/*
 * IIC【中断方式】 收发数据
 * 输入参数：pIICInfo,   IIC信息结构体指针
 * 		  wTargetAddr 目标地址
 * 		  pcTxBuffer   发送缓存指针
 * 		  TxLength    最大发送长度
 * 		  pcRxBuffer	     接收缓存指针
 * 		  RxLength 	     最大接收长度
 * 		  pvCallBackFunc	     发送完成回调函数
 * 输出参数： = 0，成功启动 ； < 0，执行出错
 * 说明：作为主机使用时，wTargetAddr > 0,作为从机使用时，wTargetAddr = 0. 每次传输完成后均会关闭IIC模块，等待下次执行该函数后开启。
 */
int IIC_Transceive_Interrupt( pStruct_IICInfo pIICInfo, UINT16 wTargetAddr,
					  	  	  UINT8 *pcTxBuffer, UINT16 wTxLength,
					  	  	  UINT8 *pcRxBuffer, UINT16 wRxLength,
					  	  	  void  *pvCallBackFunc);
#endif /* DRVI2C_H_ */
