/*
 文件: lib_spi.h
 版本: CMC693 V1.10
 版权: 浙江杰芯科技有限公司
 作者: zhulin(zhulin2@supcon.com)
 描述：
修改记录：
=========
日期                                      作者                       工作                            内容
*/

#ifndef __DRVSPI_H_
#define __DRVSPI_H_

#include "datatype.h"
#include "ckspi.h"
#include "ckcore.h"

/* 时钟极性选择 */
typedef enum{
    SPI_CLOCK_POLARITY_LOW  = 0,
    SPI_CLOCK_POLARITY_HIGH = 1
}Enum_SPI_Polarity;

/* 采样沿选择 */
typedef enum{
    SPI_CLOCK_PHASE_FIRST_EDGE  = 0,
    SPI_CLOCK_PHASE_SECOND_EDGE = 1
}Enum_SPI_Phase;

/* SPI 数据宽度 */
typedef enum{
    SPI_DataWidth_4 = 4,
    SPI_DataWidth_5,
    SPI_DataWidth_6,
    SPI_DataWidth_7,
    SPI_DataWidth_8,
    SPI_DataWidth_9,
    SPI_DataWidth_10,
    SPI_DataWidth_11,
    SPI_DataWidth_12,
    SPI_DataWidth_13,
    SPI_DataWidth_14,
    SPI_DataWidth_15,
    SPI_DataWidth_16
}Enum_SPI_DataWidth;

/* 数据存储宽度的类型 */
typedef enum{
    SPI_DataWidth_Type_8  = 1,
    SPI_DataWidth_Type_16 = 2
}Enum_SPI_DataWidth_Type;

/* SPI CS片选
 * SPI_CSx       片上使能
 * SPI_CS_Last   持续，保持上次的CS片选
 * SPI_CS_Manual GPIO指定 */
typedef enum{
    SPI_CS0 = 1,
    SPI_CS1 = 2,
    SPI_CS2 = 3,
    SPI_CS_Last = 4,
    SPI_CS_Manual
}Enum_SPI_CS;

/* SPI 的初始化结构体 */
typedef struct
{
	UINT32 SPI_BaseAddr;                            /* SPI基地址 */
    UINT32 baudrate;                                /* SPI 波特率 */
    Enum_SPI_DataWidth datawidth;                   /* SPI的数据宽度类型8/16 Bit */
    Enum_SPI_CS cs;                                 /* 片选选择 */
    Enum_SPI_Polarity polarity;                     /* 极性 */
    Enum_SPI_Phase phase;                           /* 相位 */
    UINT8 TxFTLevel;                                /* 发送缓冲区长度。 */
    UINT8 RxFTLevel;                                /* 接收缓冲区长度。 */
    UINT8 intrmask;                                 /* 中断屏蔽配置 */
}SPI_InitStructure, *pSPI_InitStructure;

/* SPI_DMA初始化结构体
 * 因为可能收发同时需要DMA，或许也可能存在只要收或发的情况的，所以函数分开写比较合理。
 */
typedef struct
{
    void *memADDR;                              /* SPI DMA接收地址 */
    UINT32 intrmask;                            /* 中断屏蔽标志位. 0 表示屏蔽， 1表示不屏蔽 */
    UINT16 length;                              /* DMA FIFO数据长度 */
    void (*cbfunc) (void *parg, UINT8 ch);      /* 回调函数 */
}SPI_DMA_InitStructure, *pSPI_DMA_InitStructure;

/* 如果申请成功DMA ID号应该大于0，而且每次申请ID的结果都不一样。故用此结构体保存便于查询、释放DMA。 */
typedef struct
{
    UINT8 DMA_P2M_ID;                           /* P2M DMA通道的ID */
    UINT8 DMA_M2P_ID;                           /* M2P DMA通道的ID */
}SPI_DMA_ID_Structure, *pSPI_DMA_ID_Structure;

/* SPI信息结构体定义 */
typedef struct
{
    SPI_InitStructure SPI_InitStruct;                     /* SPI的初始化配置结构体 */
    SPI_DMA_InitStructure SPI_DMA_M2P_Initstruct;         /* DMA发送相关的参数结构体 */
    SPI_DMA_InitStructure SPI_DMA_P2M_Initstruct;         /* DMA接收相关的参数结构体 */
    SPI_DMA_ID_Structure SPI_DMA_ID_struct;               /* DMA ID结构体 */
    pStruct_SPI SPI;                                      /* SPI模块寄存器起始地址 */
    void (*SPI_ISR) (UINT32 intr_state);                  /* SPI  底层回调函数 */
}Struct_SPIInfo, *pStruct_SPIInfo;

extern Struct_SPIInfo SPI;
extern pStruct_SPIInfo pSPI;
extern Struct_SPIInfo SPI1;
extern pStruct_SPIInfo pSPI1;

/* 外部实现的函数 */
extern void SPI_ReadFlash_asm(pStruct_SPIInfo pSpiInfo,
                              void *pcmd_buffer,
                              UINT32 cmdsize,
                              void *prx_buffer,
                              UINT32 buffersize,
                              UINT8 typewidth);

extern void SPI_WriteFlash_asm(pStruct_SPIInfo pSpiInfo,
                               void *pcmd_buffer,
                               UINT32 cmdsize,
                               void *ptx_buffer,
                               UINT32 buffersize,
                               UINT8 typewidth);

/*
 * 根据SPI初始化参数配置指定的SPI
 * 输入参数：pSpiInfo： 初始化参数结构体。
 * 输出参数：
 */
void SPI_Config(pSPI_InitStructure pSpiInfo);

/*
 * 关闭SPI
 * 输入参数：pSpiInfo, SPI信息结构体指针；
 * 输出参数：返回 0， 成功; 其他，失败。
 * 说明：这个过程会关闭SPI使能， 屏蔽所有中断源，清除中断标志。
 */
int SPI_Close(pStruct_SPIInfo pSpiInfo);

/*
 * 配置SPI并打开SPI
 * 输入参数：pSpiInfo, SPI信息结构体指针；
 * 输出参数：返回 0， 成功; 其他，失败。
 */
int SPI_Open(pStruct_SPIInfo pSpiInfo);

/*
 * SPI 设置所有参数
 * 输入参数：pSpiInfo： SPI信息结构体指针；
 *        Polarity    极性
 *        Phase       相位
 *        dwBaudrate    波特率    设置为0时可保持当前波特率
 *        cDataWidth   位宽
 *        cCS          片选
 * 输出参数：返回 0， 成功; 其他，失败
 */
int SPI_SetParameters(pStruct_SPIInfo pSpiInfo,
                      Enum_SPI_Polarity Polarity,
                      Enum_SPI_Phase Phase,
                      UINT32 dwBaudrate,
                      Enum_SPI_DataWidth cDataWidth,
                      Enum_SPI_CS CS);

/*
 * SPI 对外部设备写块数据(该函数仅用于CMC内部集成的flash批量写数据)
 * 输入参数：pSpiInfo：                      SPI信息结构体指针；
 *        CS                 片选值;
 *        pCmdBuffer:        命令数据的地址指针
 *        cCmdSize           数据的命令长度
 *        pTxBuffer          接收地址指针
 *        dwBufferSize       接收数据的长度
 * 输出参数：返回 0， 成功; 其他，失败。
 * 说明：  Flash-WRITE模式进行发送，发送函数汇编实现。
 */
int SPI_WriteFlash(pStruct_SPIInfo pSpiInfo,
                   Enum_SPI_CS CS,
                   void *pCmdBuffer,
                   UINT8 cCmdSize,
                   void *pTxBuffer,
                   UINT32 dwBufferSize );

/*
 * SPI 读取外部设备的块数据(仅适用于cmc内部flash的批量获取数据)
 * 输入参数：pSpiInfo：                   SPI信息结构体指针；
 *        CS 片选值;
 *        pCmdBuffer:       命令数据的地址指针
 *        cCmdSize            数据的命令长度
 *        pRxBuffer         接收地址指针
 *        dwBufferLength       接收数据的长度.
 * 输出参数：返回 0， 成功; 其他，失败。
 * 说明：  Flash-READ模式进行接收，接收函数汇编实现。
 */
int SPI_ReadFlash(pStruct_SPIInfo pSpiInfo,
                   Enum_SPI_CS CS,
                   void *pCmdBuffer,
                   UINT8 cCmdSize,
                   void *pRxBuffer,
                   UINT32 dwBufferSize );

/*
 * SPI 通过DMA发送发送和传输数据
 * 输入参数：pSpiInfo：          SPI信息结构体指针；
 *        pTxBuffer     要发送数据的地址指针
 *        pRx_buffer：        接收数据的地址指针
 *        wBufferLength:  数据长度
 *        cbfunc         回调函数
 * 输出参数：返回 0， 成功; 其他，失败。
 * 说明：  1 目前DMA的突发传输长度设置为1item, 用4item会出现问题，尚需定位。
 *      2 cbfunc为NULL时工作在阻塞状态。
 *      3 目前使用DMA传输方式传输数据时，若波特率过大过小，均会导致头部多读1Byte无效数据，请注意。
 */
#define ENABLE_INT_BLOCK		(1 << 0)	 /* Block Transfer Complete Interrupt */
int SPI_TxRx_DMA(pStruct_SPIInfo pSpiInfo,
                 Enum_SPI_CS CS,
                 void   *pTxBuffer,
                 void   *pRxBuffer,
                 UINT16 wBufferLength,
                 void  (*cbfunc)(void *parg, UINT8 ch));

/*
 * SPI 收发函数
 * 输入参数：pSpiInfo： SPI信息结构体指针；
 *        CS 片选值；
 *        pTxBuffer
 *        pRxBuffer：      发送接收地址
 *        wBufferLength: 传输的数据长度.
 * 输出参数：返回 0， 成功; 其他，失败。
 */
int SPI_TxRx(pStruct_SPIInfo pSpiInfo,
             Enum_SPI_CS CS,
             void *pTxBuffer,
             void *pRxBuffer,
             UINT32 wBufferLength);

/*
 * 获取SPI成员变量的结构体
 * 输入参数：pSpiInfo, SPI信息结构体指针；
 *        SPI_Initstruct SPI的初始化结构体
 * 输出参数：返回 0， 成功; 其他，失败。
 */
int SPI_GetInitStructure(pStruct_SPIInfo pSpiInfo, pSPI_InitStructure pInitstruct);

#endif /* DRVSPI_H_ */
