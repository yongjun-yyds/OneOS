/*
 文件: pipo.c
 版本: CMC693 V1.1
 版权: 浙江杰芯科技有限公司
 作者: 喻文星 (yuwenxing@supcon.com)
 描述：PIPO有以下硬件信息：
 	 1.PIPO共有6个分别是PIPO0~PIPO6,每个PIPO有A、B两个端口:
 	 PIPO0:端口A：GPIO1_B8   端口B：GPIO1_B9   PIPO1：端口A：GPIO1_B10   端口B：GPIO1_B11
 	 PIPO2:端口A：GPIO1_B12   端口B：GPIO1_B13 PIPO3：端口A：GPIO1_B14   端口B：GPIO1_B15
 	 PIPO4:端口A：GPIO1_A10   端口B：GPIO1_A11 PIPO5：端口A：GPIO1_A12   端口B：GPIO1_A13
 	 2.PIPO有7种工作模式：定时模式、PWM工作模式、脉冲宽度测量模式、脉冲个数测量模式单脉冲模式、保留（暂时未使用）、正交编码器接口模式
 	 3.A、B两端口都可以输出PWM波，两路PWM波的频率是相同的，占空比可以不同
 	 4.目前底层PWM停止后的电平状态时随机的，所以关闭PWM波要么波特率配置为0，要么关闭管脚复用
 	 5.PWM无法输出占空比为0的波形，当PWM输出占空比为0时，会在每个周期有1时钟周期的脉冲产生
 	 6.PWM波配置成死区PWM波，B端口的PWM波是由A端口PWM波翻转得到，并且死区PWM波的前、后死区时间不能为0，否则不输出死区PWM波
 	 7.脉冲宽度测量有7中模式：高电平宽度测量模式，高电平时间测量模式，低电平宽度测量模式，低电平时间测量模式，全电平测量模式，单个脉冲高电平宽度测量模式，
 	       单个脉冲低电平宽度测量模式；其中只有全电平测量模式，没有中断，此模式会将每次测到高低电平时间存放到相应的寄存器，需要停止工作才会结束测量

修改记录：
=========
日期                                      作者                       工作                            内容
-------------------------------------------------------
2018-11-15       喻文星                      创建                         PIPO驱动程序
2018-12-10       喻文星                      修改                PIPO的PWM模式输出的最高频率调整为1000000HZ
*/

#ifndef _PIPO_H_
#define _PIPO_H_

#include "datatype.h"
#include "ckpipo.h"
#include "ckcore.h"

/*中断向量*/
#define INTC_PIPO0   CK_INTC_PIPO0
#define INTC_PIPO1   CK_INTC_PIPO1
#define INTC_PIPO2   CK_INTC_PIPO2
#define INTC_PIPO3   CK_INTC_PIPO3
#define INTC_PIPO4   CK_INTC_PIPO4
#define INTC_PIPO5   CK_INTC_PIPO5

/*中断掩码偏移*/
#define PIPO_INTC_COP_DOWN_FLOW      	 ((UINT32)1<<0)		/*正交编码方式向下溢出*/
#define PIPO_INTC_COP_OVER_FLOW    		 ((UINT32)1<<1)		/*正交编码方式向上溢出*/
#define PIPO_INTC_B_PULSE_NUM     		 ((UINT32)1<<2)		/*B端口脉冲个数测量完成*/
#define PIPO_INTC_B_LOW_TIME      		 ((UINT32)1<<3)		/*B端口低电平时间测量完成*/
#define PIPO_INTC_B_HIGH_TIME    		 ((UINT32)1<<4)		/*B端口高电平时间测量完成*/
#define PIPO_INTC_B_LOW_SINGLE     	     ((UINT32)1<<5)		/*B端口低电平单次测量完成*/
#define PIPO_INTC_B_HIGH_SINGLE      	 ((UINT32)1<<6)		/*B端口高电平单次测量完成*/
#define PIPO_INTC_A_PULSE_NUM     	 	 ((UINT32)1<<7)		/*A端口脉冲个数测量完成*/
#define PIPO_INTC_A_LOW_TIME      	 	 ((UINT32)1<<8)		/*A端口低电平时间测量完成*/
#define PIPO_INTC_A_HIGH_TIME    		 ((UINT32)1<<9)		/*A端口高电平时间测量完成*/
#define PIPO_INTC_A_LOW_SINGLE     	 	 ((UINT32)1<<10)	/*A端口低电平单次测量完成*/
#define PIPO_INTC_A_HIGH_SINGLE      	 ((UINT32)1<<11)	/*A端口高电平单次测量完成*/
#define PIPO_INTC_CENTER_COUNT      	 ((UINT32)1<<12)	/*中央计数事件完成*/
#define PIPO_INTC_DOWN_COUNT    		 ((UINT32)1<<13)	/*向下计数事件完成*/
#define PIPO_INTC_UP_COUNT     	 	 	 ((UINT32)1<<14)	/*向上计数事件完成*/
#define PIPO_INTC_PULSE     	 		 ((UINT32)1<<15)	/*单脉冲输出完成*/

#define PIPO_INTC_CONTROL     	 		 ((UINT32)1<<21)    /*中断控制位*/
#define PIPO_INTC_CLEAN	    	 		 ((UINT32)1<<22)	/*中断标志清除位*/

#define PIPO_INTC_DTIME     	 		 ((UINT32)1<<9)     /*死区使能位*/

#define PIPO_TIMER_ENABLE     	 		 ((UINT32)1<<0)     /*定时器计数器使能位*/

#define PIPO_DUTY_RATIO_MAX     10000    /*PWM最大占空比*/
#define PIPO_FREQUENT_MAX    	1000000	 /*PWM最大输出频率*/

#define PIPO_DPWM_FREQUENT_MAX  1000000	 /*DPWM最大输出频率*/

#define PIPO_CLEAR_MODE   0XFFFFFF1F     /*用于清除当前PIPO工作模式*/
#define PIPO_GET_MODE     0X000000E0     /*用于获取当前PIPO工作模式*/

/*定义PIPO的工作模式*/
typedef enum
{
	PIPO_MODE_IDLE 				= 0,	/*模块空闲*/
	PIPO_MODE_TIMER				= 1,	/*定时器*/
	PIPO_MODE_PWM				= 2,	/*PWM工作模式*/
	PIPO_MODE_PulseWideMeasure	= 3,	/*脉冲宽度测量模式*/
	PIPO_MODE_PulseCount		= 4,	/*脉冲个数模式*/
	PIPO_MODE_OutputSingle		= 5,	/*单脉冲输出模式*/
	PIPO_MODE_NONE				= 6,	/*保留*/
	PIPO_MODE_MotorDir			= 7,	/*正交编码器接口模式*/
} Enum_PIPO_Mode;

/*定义PIPO的定时器计数模式*/
typedef enum{
	PIPO_TIMER_UP 		= 1,
	PIPO_TIMER_DOWN 	= 2,
	PIPO_TIMER_CENTER	= 3,
}Enum_PIPO_TimerDir;

/*脉冲计数模式检测边缘*/
typedef enum{
	PIPO_RISING_EDGE 	= 0,
	PIPO_TRAILING_EDGE 	= 1,
}Enum_PIPO_PulseCountTrigger;

/*定义PIPO的定时器计数器溢出模式*/
typedef enum{
	UNIT_S 	= 1,
	UNIT_MS	= 2,
	UNIT_US = 3,
	UNIT_HZ = 5,    /*PWM模式使用*/
	UNIT_TICK= 6,	/*时钟周期*/
}Enum_PIPO_TimerUnit;

/*定义PIPO的PWM对齐模式*/
typedef enum{
	PIPO_PWM_ALIGN_LEFT 	=1,
	PIPO_PWM_ALIGN_RIGHT	=2,
	PIPO_PWM_ALIGN_MIDDLE	=3,
}Enum_PIPO_PwmAlign;

/*定义PIPO的脉冲测量模式*/
typedef enum{
	PULSE_MEASURE_SigleHigh 	= 0,	/*高电平单次测量模式*/
	PULSE_MEASURE_MultHigh 		= 1,	/*高电平时间测量模式*/
	PULSE_MEASURE_SigleLow		= 2,	/*低电平单次测量模式*/
	PULSE_MEASURE_MultLow		= 3,	/*低电平时间测量模式*/
	PULSE_MEASURE_LowAndHigh	= 4,	/*全电平单次测量模式*/
}Enum_PIPO_PulseMeasureMode;

/*PIPI的定时器功能信息结构体*/
typedef struct
{
	UINT32 dwTimeout;           /*定时时间*/
	Enum_PIPO_TimerUnit Unit;   /*单位*/
	Enum_PIPO_TimerDir	Dir;	/*计数方向*/
	UINT8   InterruptEnable;	/*中断使能控制位*/
} PIPO_TimerStructure,*pPIPO_TimerStructure;

/*PIPO的PWM功能信息结构体*/
typedef struct
{
	UINT32	Freq_Hz;			/*频率  单位HZ*/
	UINT16	DutyRatioA;			/*占空比A,放大DUTY_RATIO_MAX倍*/
	UINT16	DutyRatioB;			/*占空比B，放大DUTY_RATIO_MAX倍*/
	UINT16	DtimePre;			/*前死区占空比*/
	UINT16 	DtimePost;			/*后死区占空比*/
	UINT8	DTimeEnable;		/*死区使能  0=禁用死区，1=使用死区*/
	UINT8	Polarity;			/*输出极性  0=正极性，1=负极性*/
	Enum_PIPO_PwmAlign Align;	/*对齐模式,即配置计数器计数模式（向上、向下、向上/向下）*/
} PIPO_PwmStructure,*pPIPO_PwmStructure;

/*PIPO的脉宽测量功能信息结构体*/
typedef struct{
	Enum_PIPO_PulseMeasureMode Mode;  	/*测量模式*/
	UINT32 Period;              		/*测量周期 */
	Enum_PIPO_TimerUnit Unit;   		/*单位*/
	UINT8 IntEnable_A;    				/*A端口中断使能控制位*/
	UINT8 IntEnable_B;					/*B端口中断使能控制位*/
}PIPO_PulseMeasureStructure,*pPIPO_PulseMeasureStructure;

/*PIPO的脉冲计数功能信息结构体*/
typedef struct{
	UINT32 Period;							/*测量周期*/
	Enum_PIPO_TimerUnit Unit;   			/*单位*/
	Enum_PIPO_PulseCountTrigger Trigger_A;	/*边沿检测方式 0=上升沿，1=下降沿*/
	Enum_PIPO_PulseCountTrigger Trigger_B;	/*边沿检测方式 0=上升沿，1=下降沿*/
	UINT8 IntEnable_A;						/*A端口中断使能控制位*/
	UINT8 IntEnable_B;						/*B端口中断使能控制位*/
}PIPO_PulseCountStructure,*pPIPO_PulseCountStructure;

/*PIPO的单脉冲功能信息结构体*/
typedef struct
{
	UINT32 TriggerDelay;			/*触发延时 单位 Tick*/
	UINT32 PulseWidth;				/*单脉冲脉宽*/
	Enum_PIPO_TimerUnit Unit;   	/*单位*/
	UINT8 IntEnable;				/*A端口中断使能控制位*/
}PIPO_SinglePulseStructure,*pPIPO_SinglePulseStructure;

/*PIPO的正交编码器功能信息结构体*/
typedef struct
{
	UINT32 MaxAddr;             /*计数上限*/
	UINT32 MinAddr;             /*计数下限*/
	UINT8 SignEnable;           /*是否采用有符号计数*/
	UINT8 IntEnableOverflow;	/*正交编码向上溢出*/
	UINT8 IntEnableDownflow;	/*正交编码向下溢出*/
}PIPO_COP_Structure,*pPIPO_COP_Structure;

/*PIPO信息化结构体*/
typedef struct
{
	pStruct_PIPOInfo pPIPOAddr; /*PIPO寄存器地址*/
	UINT8 PIPO_Index;           /*PIPO索引*/
	Enum_PIPO_Mode Mode;        /*PIPO工作模式*/
	UINT32 priority;            /*中断优先级 */
	UINT32 IntStatus;           /*上次中断的状态值*/
	void *Function_t;   		/*功能信息指针*/
	void (*PIPO_ISR)(UINT32);   /* 用户自定义PIPO的中断处理函数 */
}PIPO_InitStructure, *pPIPO_InitStructure;

/*
 * 清空PIPO所有寄存器，实现复位PIPO
 * 输入参数：pPipo：PIPO基地址，PIPO0~PIPO5
 * 输出参数：无
 */
void PIPO_Reset(pStruct_PIPOInfo pPipo);

/*
 * PIPO产生更新事件
 * 输入参数：pPipo：PIPO基地址，PIPO0~PIPO5
 * 输出参数：无
 * 说明：更新事件：重初始化寄存器，包括：TIM_PSC预分频器、TIM_ARR自动装载寄存器、
 *     TIM_RCR重复次数寄存器、TIM_CCRA比较寄存器A、TIM_CCRB比较寄存器B、
 *     TIM_DT_PRE前死去时间寄存器、TIM_DT_POST后死区时间寄存器
 *     清零寄存器，包括：PW_H_A、PW_H_B、 PW_L_A、 PW_L_B、PW_CNT_A、TIM_CNT(如果是单脉冲模式，则加载值ARR)
 */
void PIPO_Update(pStruct_PIPOInfo pPIPO);

/*
 * 产生更新事件并启动PIPO模式
 * 输入参数：pPipo：PIPO基地址，PIPO0~PIPO5
 * 输出参数：无
 */
void PIPO_UpdateAndStart(pStruct_PIPOInfo pPIPO);

/*
 * 启动定时器计数器
 * 输入参数：pPipo：PIPO基地址，PIPO0~PIPO5
 * 输出参数：无
 */
void PIPO_TimerCountStart(pStruct_PIPOInfo pPIPO);

/*
 * 停止定时器计数器
 * 输入参数：pPipo：PIPO基地址，PIPO0~PIPO5
 * 输出参数：无
 */
void PIPO_TimerCountStop(pStruct_PIPOInfo pPIPO);

/*
 * 清除中断标志位
 * 输入参数：pPipo：PIPO基地址，PIPO0~PIPO5
 * 输出参数：返回：dwIntr：返回中断状态寄存器值
 */
UINT32 PIPO_ClearInterrupt(pStruct_PIPOInfo pPIPO);

/*
 * 使能中断
 * 输入参数：pPipo：PIPO基地址，PIPO0~PIPO5
 * 输出参数：无
 */
void PIPO_EnableInterrupt(pStruct_PIPOInfo pPIPO);

/*
 * 失能中断
 * 输入参数：pPipo：PIPO基地址，PIPO0~PIPO5
 * 输出参数：无
 */
void PIPO_DisableInterrupt(pStruct_PIPOInfo pPIPO);

/*
 * 打开中断屏蔽功能
 * 输入参数：pPipo：PIPO基地址，PIPO0~PIPO5，dwInterrupt：所要屏蔽的中断
 * 输出参数：无
 * 说明：寄存器PIPO_INT_DIS相应位置1为屏蔽中断，置0为不屏蔽中断
 */
void PIPO_EnInterruptMask(pStruct_PIPOInfo pPIPO,UINT32 dwInterrupt);

/*
 * 关闭中断屏蔽功能
 * 输入参数：pPipo：PIPO基地址，PIPO0~PIPO5，dwInterrupt：不需屏蔽的中断
 * 输出参数：无
 * 说明：寄存器PIPO_INT_DIS相应位置1为屏蔽中断，置0为不屏蔽中断
 */
void PIPO_DisInterruptMask(pStruct_PIPOInfo pPIPO,UINT32 dwInterrupt);

/*
 * 获取定时器计数器计数值
 * 输入参数：pPipo：PIPO基地址，PIPO0~PIPO5
 * 输出参数：返回 TIM_CNT
 */
UINT32 PIPO_GetTimerCount(pStruct_PIPOInfo pPIPO);

/*
 * PIPO的定时器功能初始化
 * 输入参数：pPIPOInfo:PIPO初始化结构体
 * 输出参数：无
 */
void PIPO_TimerInit(pPIPO_InitStructure pPIPOInfo);

/*
 * 配置PIPO定时器计数器的计数时间，即配置分频系数和自动装载值
 * 输入参数：pPipo：PIPO基地址，PIPO0~PIPO5  dwPeriod：周期时间    Unit：单位
 * 输出参数：无
 * 说明：Unit：可选5种：
 *  UNIT_S 	= 1,
 *	UNIT_MS	= 2,
 *	UNIT_US = 3,
 *	UNIT_HZ = 5,   PWM模式使用
 *	UNIT_TICK= 6,  时钟滴答
 */
void PIPO_SetFreqTime(pStruct_PIPOInfo pPIPO,UINT32 dwPeriod,Enum_PIPO_TimerUnit Unit);

/*
 * PIPO的PWM模式初始化
 * 输入参数：pPIPOInfo:PIPO初始化结构体
 * 输出参数：无
 */
void PIPO_PwmInit(pPIPO_InitStructure pPIPOInfo);

/*
 * PIPO的PWM模式失能
 * 输入参数：pPipo：PIPO基地址，PIPO0~PIPO5
 * 输出参数：无
 * 说明：由于直接关闭定时器计数器会导致PWM直接输出随机，所以需要将工作中模式变成保留模式将输出电平变为低
 *     对于输出0HZ的PWM波，也使用此函数实现
 */
void PIPO_PwmStop(pStruct_PIPOInfo pPIPO);

/*
 * PIPO的PWM模式重新使能
 * 输入参数：pPipo：PIPO基地址，PIPO0~PIPO5
 * 输出参数：无
 * 说明：与函数PIPO_PwmStop对应，功能块输出PWM的频率为0HZ时通过关闭PWM实现，当输出PWM的频率不为0HZ时，使用此函数恢复PWM功能
 */
void PIPO_PwmRestart(pStruct_PIPOInfo pPIPO);

/*
 * PIPO的PWM模式死区功能打开
 * 输入参数：pPipo：PIPO基地址，PIPO0~PIPO5
 * 输出参数：无
 */
void PIPO_DtimeEnable(pStruct_PIPOInfo pPIPO);

/*
 * PIPO的PWM参数配置
 * 输入参数：pPIPOInfo:PIPO初始化结构体
 * 输出参数：无
 * 说明：配置PWM周期，A、B端口的占空比、死区时间
 */
void PIPO_SetPwm(pPIPO_InitStructure pPIPOInfo);

/*
 * PIPO配置PWM输出频率，并保持A、B通道占空比不变
 * 输入参数：pPipo：PIPO基地址，PIPO0~PIPO5，dwFrequent：PWM输出频率
 * 输出参数：无
 * 说明：此函数不作为首次配置使用，因为首次使用不存在占空比
 */
UINT8 PIPO_PwmSetFreq(pStruct_PIPOInfo pPIPO,UINT32 dwFrequent);

/*
 * PIPO配置PWMA、B通道占空比
 * 输入参数：pPipo：PIPO基地址，PIPO0~PIPO5，dwDutyRatioA：A通道占空比，dwDutyRatioB：B通道占空比
 * 输出参数：无
 * 说明：如果占空比配置值超过最大值即表示保持当前通道占空比不变
 */
void PIPO_PwmSetDutyRatio(pStruct_PIPOInfo pPIPO,UINT32 dwDutyRatioA,UINT32 dwDutyRatioB);

/*
 * PIPO配置PWMA、B通道占空比和频率
 * 输入参数：pPipo：PIPO基地址，PIPO0~PIPO5，dwFrequent：PWM输出频率，dwDutyRatioA：A通道占空比，dwDutyRatioB：B通道占空比
 * 输出参数：无
 * 说明：A、B两路同频率。如果占空比配置值超过最大值即表示保持当前通道占空比不变
 */
UINT8 PIPO_PwmSetFreqAndDutyRatio(pStruct_PIPOInfo pPIPO,UINT32 dwFrequent,UINT32 dwDutyRatioA,UINT32 dwDutyRatioB);

/*
 * PIPO配置PWM 死区时间
 * 输入参数：pPipo：PIPO基地址，PIPO0~PIPO5，DtimePre：前死区时间，DtimePost：后死区时间
 * 输出参数：无
 */
void PIPO_PwmSetDeadTime(pStruct_PIPOInfo pPIPO,UINT32 DtimePre,UINT32 DtimePost);

/*
 * PIPO脉宽测量初始化
 * 输入参数：pPIPOInfo:PIPO初始化结构体
 * 输出参数：返回值：0初始化成功，1初始化失败
 * 说明：脉宽测量总共有5种方式：
 * 	PULSE_MEASURE_SigleHigh 	= 0,	高电平单次测量模式
 *	PULSE_MEASURE_MultHigh 		= 1,	高电平时间测量模式
 *	PULSE_MEASURE_SigleLow		= 2,	低电平单次测量模式
 *	PULSE_MEASURE_MultLow		= 3,	低电平时间测量模式
 *	PULSE_MEASURE_LowAndHigh	= 4,	全电平单次测量模式
 *	这5种方式中时间测量在时间到后可触发中断，高/低电平单次测量模式测量完成触发中断
 *	全电平单次测量模式不会触发中断，只能通过查询方式
 */
UINT8 PIPO_PulseMeasureInit(pPIPO_InitStructure pPIPOInfo);

/*
 * 读取端口A的脉宽的相应寄存器
 * 输入参数：pPipo：PIPO基地址，PIPO0~PIPO5，
 * 输出参数：pHighA：端口A高脉宽时间  pLowA：端口A低脉宽时间
 */
void PIPO_PulseMeasureReadPortA(pStruct_PIPOInfo pPIPO,UINT32 * pHighA,UINT32 * pLowA);

/*
 * 读取端口B的脉宽的相应寄存器
 * 输入参数：pPipo：PIPO基地址，PIPO0~PIPO5，
 * 输出参数：pHighB：端口B高脉宽时间  pLowB：端口B低脉宽时间
 */
void PIPO_PulseMeasureReadPortB(pStruct_PIPOInfo pPIPO,UINT32 * pHighB,UINT32 * pLowB);

/*
 * PIPO脉冲个数测量初始化
 * 输入参数：pPIPOInfo:PIPO初始化结构体
 * 输出参数：返回值：0初始化成功，1初始化失败
 */
UINT8 PIPO_PulseCountInit(pPIPO_InitStructure pPIPOInfo);

/*
 * 读取A端口的脉冲个数
 * 输入参数：pPipo：PIPO基地址，PIPO0~PIPO5，
 * 输出参数：返回值：A端口脉冲个数
 */
UINT32 PIPO_PulseCountReadPortA(pStruct_PIPOInfo pPIPO);

/*
 * 读取B端口的脉冲个数
 * 输入参数：pPipo：PIPO基地址，PIPO0~PIPO5，
 * 输出参数：返回值：B端口脉冲个数
 */
UINT32 PIPO_PulseCountReadPortB(pStruct_PIPOInfo pPIPO);

/*
 * PIPO单脉冲输出功能初始化
 * 输入参数：pPIPOInfo:PIPO初始化结构体
 * 输出参数：返回值：0初始化成功，1初始化失败
 */
UINT8 PIPO_SinglePulseInit(pPIPO_InitStructure pPIPOInfo);

/*
 * PIPO正交编码器功能初始化
 * 输入参数：pPIPOInfo:PIPO初始化结构体
 * 输出参数：无
 */
void PIPO_COPInit(pPIPO_InitStructure pPIPOInfo);

/*
 * 返回运转方向
 * 输入参数：pPipo：PIPO基地址，PIPO0~PIPO5，
 * 输出参数：返回值：运转方向，最低位表示运转方向：0正转，1反转
 * 说明：当A相超前B相时，转动方向信号为1，向上计数（反转）；B相超前A相时，
 * 转动方向信号为0，往下计数（正传）
 */
UINT32 PIPO_COP_GetDir(pStruct_PIPOInfo pPIPO);

/*
 * 返回正交编码器计数器值
 * 输入参数：pPipo：PIPO基地址，PIPO0~PIPO5，
 * 输出参数：返回值：正交编码器计数器值
 */
UINT32 PIPO_COP_GetCount(pStruct_PIPOInfo pPIPO);

#endif /* _PIPO_H_ */
