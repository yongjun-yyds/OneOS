/*
 文件: timer.h
 版本: CMC693 V1.10
 版权: 浙江杰芯科技有限公司
 作者: zhulin(zhulin2@supcon.com)
 描述：
修改记录：
=========
日期                                      作者                       工作                            内容
*/
#ifndef __LIB_TIMER_H__
#define __LIB_TIMER_H__

#include "cktimer.h"
#include "intc.h"

#define TIMER_INTR_ACTIVE        (UINT8)(0x01)    /* 产生中断 */
#define TIMER_INTR_INACTIVE      (UINT8)(0x00)    /* 未产生中断 */

/* 定时器ID定义 */
typedef enum{
    TIMER0 = 0,
    TIMER1,
    TIMER2,
    TIMER3,
}Enum_Timer_Device;

/* 定时器的信息结构体 */
typedef struct {
	Enum_Timer_Device enID;         /* 定时器ID号 */
	pStruct_TIMER pAddr;            /* 定时器基地址 */
//	UINT32 dwIrq;                   /* 定时器中断号 */
	BOOL bopened;                   /* 定时器状态指示，1为已打开此定时器，0为未打开此定时器  */
	UINT32 dwTimeout;               /* 定时器定时时间us */
	Struct_IRQHandler IrqHandler;   /* 定时器中断处理函数 */
} Struct_TimerInfo, * pStruct_TIMERInfo;

/* timer初始化结构体 */
typedef struct
{
	Enum_Timer_Device enTimerID;    /* 定时器ID号 */
	UINT16 wPriority;               /* 定时器中断优先级 */
	UINT32 dwTimeout;               /* 定时器定时时间 */
	void (*TIMER_ISR)(UINT32);      /* 用户自定义的TIMER中断处理函数，为NULL时，使用驱动文件内默认的中断函数 */
}Timer_InitStructure,*pTimer_InitStructure;

/*
 * 根据初始化结构体信息，配置Timer参数
 * 输入参数：pTimerInfo timer初始化结构体
 * 输出参数：
 */
void Timer_Config(pTimer_InitStructure pTimerInfo);

/*
 * 获取指定的定时器中断状态
 * 输入参数：enTimerid：    TIMER0,
 *                     TIMER1,
 *                     TIMER2,
 *                     TIMER3
 * 输出参数：TIMER_INTR_ACTIVE,已触发中断
 *        TIMER_INTR_INACTIVE，未触发中断
 */
UINT8 Timer_GetIntStatus(Enum_Timer_Device enTimerid);

/*
 * 打开一个指定的定时器，配置中断模式，注册中断函数，取消中断屏蔽，但未使能定时器
 * 输入参数：enTimerid：    TIMER0,
 *                     TIMER1,
 *                     TIMER2,
 *                     TIMER3
 * 输出参数：SUCCESS或FAILURE
 */
INT32S Timer_Open(Enum_Timer_Device enTimerid);

/*
 * 关闭一个指定的定时器，清除中断配置，释放已注册的中断函数，屏蔽中断，禁能定时器
 * 输入参数：enTimerid：    TIMER0,
 *                     TIMER1,
 *                     TIMER2,
 *                     TIMER3
 * 输出参数：SUCCESS或FAILURE
 */
INT32S Timer_Close(Enum_Timer_Device enTimerid);

/*
 * 启动一个指定的定时器。设置定时器的工作模式，装载值。
 * 输入参数：enTimerid：    TIMER0,
 *                     TIMER1,
 *                     TIMER2,
 *                     TIMER3
 *        dwTimeout： 定时的时间，单位为us
 * 输出参数：SUCCESS或FAILURE
 */
INT32S Timer_Start(Enum_Timer_Device enTimerid,UINT32 dwTimeout);

/*
 * 停止一个指定的定时器，并返回停止时刻的计数值
 * 输入参数：enTimerid：    TIMER0,
 *                     TIMER1,
 *                     TIMER2,
 *                     TIMER3
 * 输出参数：停止时刻的计数器值
 */
UINT32 Timer_Stop(Enum_Timer_Device enTimerid);

/*
 * 清除一个指定的定时器中断状态，通过读EOI寄存器来清除中断状态
 * 输入参数：enTimerid：    TIMER0,
 *                     TIMER1,
 *                     TIMER2,
 *                     TIMER3
 * 输出参数：
 */
void Timer_ClearIrqFlag(Enum_Timer_Device enTimerid);

/*
 * 读取指定的定时器当前计数值
 * 输入参数：enTimerid：    TIMER0,
 *                     TIMER1,
 *                     TIMER2,
 *                     TIMER3
 * 输出参数：当前计数器值
 */
UINT32 Timer_CurrentValue(Enum_Timer_Device enTimerid);

/*
 * 读取指定的定时器装载值
 * 输入参数：enTimerid：    TIMER0,
 *                     TIMER1,
 *                     TIMER2,
 *                     TIMER3
 * 输出参数：计数器装载值
 */
UINT32 Timer_LoadValue(Enum_Timer_Device enTimerid);

#endif
