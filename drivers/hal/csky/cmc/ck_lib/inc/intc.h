/*
 文件: intc.h
 版本: CMC693 V1.10
 版权: 浙江杰芯科技有限公司
 作者: zhulin(zhulin2@supcon.com)
 描述：
修改记录：
=========
日期                                      作者                       工作                            内容
*/
#ifndef _INTR_H_
#define _INTR_H_

#include "datatype.h"

#define CKCORE_VECTOR_SYS  32
#define CKCORE_VECTOR_AUTOVEC 10
#define CKCORE_VECTOR_FASTAUTOVEC  11
#define CKCORE_VECTOR_TLBMISS 14

#define CKCORE_VECTOR_TRAP0 16

/* define the data structure of interrupt description */
typedef struct S_IRQ_Handler{
      char *devname;
      UINT32 irqid;
      UINT32 priority;
      void (*handler)(UINT32 irqid);
      BOOL bfast;
      struct S_IRQ_Handler *next;
}Struct_IRQHandler, *pStruct_IRQHandler;

// VSR table
extern  volatile unsigned int ckcpu_vsr_table[128];
/* Statement of those functions which are used in intc.c*/
/*
 * 中断向量表初始化
 * 输入参数：无
 * 输出参数：无
 * 说明：1.中断向量表的初始化必须在开启任何中断之前进行。
 *     2.若使用UCOSII，
 *       需要去掉以下两行程序的屏蔽
 *       ckcpu_vsr_table[CKCORE_VECTOR_AUTOVEC] = (UINT32)os_hw_vsr_autovec;
 *       ckcpu_vsr_table[CKCORE_VECTOR_TRAP0] = (UINT32)OSCtxSw;
 *       同时屏蔽以下程序
 *       ckcpu_vsr_table[CKCORE_VECTOR_AUTOVEC] = (UINT32)hw_vsr_autovec;
 */
void Exception_Init (void);

/*
 * 中断模块初始化
 * 输入参数：无
 * 输出参数：无
 * 说明：①使用AUTO_VECTOR中断模式，通过auto_vector中断号10响应全部普通中断，在
 *      CK_INTC_InterruptService中断处理函数中查找已注册的中断处理函数并进行
 *      处理。
 *     ②开启电源管理中断功能（由于使用PLL倍频时，模块会进入低功耗，通过电源管理中断
 *      唤醒）
 */
void INTC_Init(void);

/*
 * 中断模块初始化
 * 输入参数：无
 * 输出参数：无
 * 说明：①使用AUTO_VECTOR中断模式，通过auto_vector中断号10响应全部普通中断，在
 *      CK_INTC_InterruptService中断处理函数中查找已注册的中断处理函数并进行
 *      处理。
 *     ②开启电源管理中断功能（由于使用PLL倍频时，模块会进入低功耗，通过电源管理中断
 *      唤醒）
 */
void INTC_EnNormalIrq(IN UINT32 dwPriority);

/*
 * 禁能指定优先级的普通中断
 * 输入参数：dwPriority：优先级0~31
 * 输出参数：无
 * 说明：
 */
void INTC_DisNormalIrq(IN UINT32 dwPriority);

/*
 * 使能指定优先级的快速中断
 * 输入参数：dwPriority：优先级0~31
 * 输出参数：无
 * 说明：
 */
void INTC_EnFastIrq(IN UINT32 dwPriority);

/*
 * 禁能指定优先级的快速中断
 * 输入参数：dwPriority：优先级0~31
 * 输出参数：无
 * 说明：
 */
void INTC_DisFastIrq(IN UINT32 dwPriority);

/*
 * 屏蔽指定优先级的普通中断
 * 输入参数：dwPrimask：优先级0~31
 * 输出参数：无
 * 说明：
 */
void INTC_MaskNormalIrq(IN UINT32 dwPrimask);

/*
 * 禁能中断屏蔽
 * 输入参数：无
 * 输出参数：无
 * 说明：
 */
void INTC_UnMaskNormalIrq(void);

/*
 * 使能指定优先级的快速中断屏蔽
 * 输入参数：dwPrimask：优先级0~31
 * 输出参数：无
 * 说明：当开启快速中断屏蔽时，所有普通中断请求被屏蔽
 */
void INTC_MaskFastIrq(IN UINT32 dwPrimask);

/*
 * 禁能快速中断屏蔽
 * 输入参数：无
 * 输出参数：无
 * 说明：当禁能快速中断屏蔽时，普通中断请求由ICR寄存器中的MASK位的值屏蔽
 */
void INTC_UnMaskFastIrq(void);

/*
 * 配置中断参数、注册中断函数，使能中断
 * 输入参数：pirqhandler：待配置的中断的数据结构体指针
 * 输出参数：FAILURE：失败；SUCCESS，成功
 * 说明：
 */
INT32S INTC_RequestIrq(pStruct_IRQHandler pirqhandler);

/*
 * 释放已被注册的中断
 * 输入参数：pirqhandler：中断的数据结构体指针
 * 输出参数：FAILURE：失败；SUCCESS，成功
 * 说明：
 */
INT32S INTC_FreeIrq(INOUT pStruct_IRQHandler pirqhandler);

/*
 * 允许cpu产生中断，包括异常中断和普通中断
 * 输入参数：
 * 输出参数：无
 * 说明：
 */
void CPU_EnAllNormalIrq(void);

/*
 * 禁止cpu产生普通中断
 * 输入参数：
 * 输出参数：无
 * 说明：
 */
void CPU_DisAllNormalIrq(void);

/*
 * 允许cpu产生快速中断
 * 输入参数：
 * 输出参数：无
 * 说明：
 */
void CPU_EnAllFastIrq(void);

/*
 * 禁止cpu产生快速中断
 * 输入参数：
 * 输出参数：无
 * 说明：
 */
void CPU_DisAllFastIrq(void);

/*
 * 关闭cpu中断，并将关闭中断之前的psr寄存器值保存在psr中，待恢复cpu中断时，
 * 将保存的psr值重新写入到psr寄存器。
 * 输入参数：psr：用于存储psr寄存器的值
 * 输出参数：无
 * 说明：在执行一些不能被其他中断打断的关键代码时，需要调用此函数进行保护。
 */
void CPU_EnterCritical(UINT32 *psr);

/*
 * 恢复cpu中断，并将关闭中断之前的psr寄存器值重新写入到psr寄存器。恢复到关闭
 * 中断前的状态。
 * 输入参数：psr：保存的关闭cpu中断之前的psr寄存器的值
 * 输出参数：无
 * 说明：退出关键代码时，需要调用此函数来恢复中断。
 */
void CPU_ExitCritical(UINT32 psr);

#endif
