/*
 文件: gpio.h
 版本: CMC693 V1.10
 版权: 浙江杰芯科技有限公司
 作者: 喻文星 (yuwenxing@supcon.com)
 描述：GPIO有以下硬件信息：
	 1.总共84个GIPO：GPIO0有20个分别是4个PortA和16个PortB，GPIO1有64个分别是32个PortA和32个PortB
	 2.只有PortA可以产生中断，PortB不可产生中断
	 3.GPIO0的4个PortA有各自独立的中断源，GPIO1的32个PortA共用1个中断源
	 4.有普通中断和快速中断两种，普通中断无法打断普通中断和快速中断，快速中断可以打断普通中断，即最多中断嵌套一次
	 5.边缘触发中断可以通过写寄存器清除，电平触发中断无法通过写寄存器清除中断，只能通过轮询等待、中断屏蔽、中断失能进行清除，否则会反复进中断
	 6.GPIO0的PortB、GPIO1的PortA和PortB具有复用功能，GPIO0的PortA没有复用功能
	 7.GPIO0PortA的中断信号同时作为CPU、LCP、MCP三个的中断控制器的输入，对GPIO0PortA的配置会对LCP和MCP的中断有影响
修改记录：
=========
日期                                      作者                       工作                            内容
-------------------------------------------------------
2018-06-27    喻文星                              修改                           规范编程风格
*/


#ifndef __GPIO_H__
#define __GPIO_H__

#include "datatype.h"
#include "ckgpio.h"

#define INTC_GPIO0A0   CK_INTC_GPIO0A0
#define INTC_GPIO0A1   CK_INTC_GPIO0A1
#define INTC_GPIO0A2   CK_INTC_GPIO0A2
#define INTC_GPIO0A3   CK_INTC_GPIO0A3
#define INTC_GPIO1A   CK_INTC_GPIO1A
/* 定义GPIO中断触发方式 */
typedef enum
{
    GPIO_Intact_High_Level,
    GPIO_Intact_Low_Level,
    GPIO_Intact_High_Edge,
    GPIO_Intact_Low_Edge
} Enum_GPIO_IRQBurst;

/* 定义GPIO端口方向 */
typedef enum
{
    GPIO_INPUT,
    GPIO_OUTPUT
} Enum_GPIO_Direction;

/* 定义GPIO中断 */
typedef enum
{
	GPIO0A_0_INTR,
	GPIO0A_1_INTR,
	GPIO0A_2_INTR,
	GPIO0A_3_INTR,
	GPIO1A_INTR,
	GPIO_NULL_INTR
} Enum_GPIO_Intr;

/* GPIO初始化信息结构体 */
typedef struct
{
	pStruct_GPIOInfo     GPIOAddr;                       /* Gpio基地址 */
	Enum_GPIO_Port       GPIOPort;                       /* PORT */
	UINT32               GPIOPin;                        /* 引脚号 */
	Enum_GPIO_Direction  GPIODirection;                  /* 输入还是输出 */
	BOOL                 bHardwareMode;                  /* 是否打开硬件模式 */

	Enum_GPIO_Intr       GPIOIntr;                       /* 选择触发中断的GPIO，只有PORTA支持中断功能 */
	Enum_GPIO_IRQBurst   BurstMode;                      /* 中断触发方式 */
	UINT32               priority;                       /* 中断优先级 */
	void                 (*GPIO_ISR)(UINT32);            /* 由用户定义的GPIO中断处理函数 */
}GPIO_InitStructure, *pGPIO_InitStructure;

/*****************************功能函数********************************/
/*
 * 根据初始化结构体信息，配置GPIO参数
 * 输入参数：PGPIOInfo GPIO初始化结构体
 * 输出参数：
 */
void GPIO_Config(pGPIO_InitStructure pGPIOInfo);

/*
 * 读取GPIO端口所有引脚的输入电平值
 * 输入参数：pGpio： I/O，可选GPIO0、 GPIO1
 *         ePort： 端口，可选PORTA、 PORTB
 * 输出参数：返回 端口所有引脚的输入电平值
 */
UINT32 GPIO_ReadInputPortData(pStruct_GPIOInfo pGpio, Enum_GPIO_Port ePort);

/*
 * 读取指定管脚的输入电平值
 * 输入参数：pGpio： I/O，可选GPIO0、 GPIO1
 *         ePort： 端口，可选PORTA、 PORTB
 *         dwPin: GPIO_P0~GPIO_P31
 * 输出参数：读取到的管脚输入电平值
 */
UINT8 GPIO_ReadInputPinData(pStruct_GPIOInfo pGpio, Enum_GPIO_Port ePort, UINT32 dwPin);

/*
 * 读取GPIO端口所有引脚的输出电平值
 * 输入参数：pGpio： I/O，可选GPIO0、 GPIO1
 *         ePort： 端口，可选PORTA、 PORTB
 * 输出参数：返回 端口所有引脚的输出电平值
 */
UINT32 GPIO_ReadOutputPortData(pStruct_GPIOInfo pGpio, Enum_GPIO_Port ePort);

/*
 * 读取指定管脚的输出电平值
 * 输入参数：pGpio： I/O，可选GPIO0、 GPIO1
 *         ePort： 端口，可选PORTA、 PORTB
 *         dwPin: GPIO_P0~GPIO_P31
 * 输出参数：读取到的管脚输电平出值
 */
UINT8 GPIO_ReadOutputPinData(pStruct_GPIOInfo pGpio, Enum_GPIO_Port ePort, UINT32 dwPin);

/*
 * 用于写指定位（可以为多位）指定值（可0可1）
 * 输入参数：pGpio：I/O，可选GPIO0、 GPIO1
 *        ePort：端口，可选PORTA、 PORTB
 *        dwPins：GPIO_P0~GPIO_P31，可以多个引脚
 *        cPinVal：0或1
 * 输出参数：返回TRUE
 * 说明：
 */
BOOL GPIO_WritePins(pStruct_GPIOInfo pGpio, Enum_GPIO_Port ePort, UINT32 dwPins, UINT8 cPinVal);

/*
 * 设置管脚复用功能,即设置管脚的硬件模式
 * 输入参数：pGpio： I/O，可选GPIO0、 GPIO1
 *         ePort： 端口，可选PORTA、 PORTB
 *         dwPins: 所指定的管脚
 *         bHardwareMode：是否打开复用功能，TRUE打开复用功能，FALSE不打复用功能
 * 输出参数：返回 TRUE
 * 说明：GPIO0的PortA无复用功能
 */
BOOL GPIO_SetReuse(pStruct_GPIOInfo pGpio, Enum_GPIO_Port ePort, UINT32 dwPins, BOOL bHardwareMode);

/*
 * 设置管脚为输入或输出
 * 输入参数：pGpio： I/O，可选GPIO0、 GPIO1
 *         ePort： 端口，可选PORTA、 PORTB
 *         dwPins: 所指定的管脚
 *         eDirection：管脚方向，输入或输出
 * 输出参数：返回 TRUE
 */
BOOL GPIO_SetDirection(pStruct_GPIOInfo pGpio, Enum_GPIO_Port ePort, UINT32 dwPins, Enum_GPIO_Direction eDirection);

/*
 * 配置管脚中断信号触发方式
 * 输入参数：pGpio： I/O，可选GPIO0、 GPIO1
 *         dwPins: 所指定的管脚
 *         eBurstMode：触发方式，有电平（高电平和低电平）模式和边缘（上升沿和下降沿）模式
 * 输出参数：返回 TRUE
 * 说明：未指定Port是因为只有PortA有具有中断功能
 *      边缘触发中断可以通过写寄存器清除，电平触发中断无法通过写寄存器清除中断，只能通过轮询等待、中断屏蔽、中断失能进行清除，否则会反复进中断
 */
BOOL GPIO_SetInterruptLevelAndPolarity(pStruct_GPIOInfo pGpio, UINT32 dwPins, Enum_GPIO_IRQBurst eBurstMode);

/*
 * 清除指定引脚所产生的中断，仅适用于边沿触发方式的中断
 * 输入参数：pGpio： I/O，可选GPIO0、 GPIO1
 *         dwPins: 所指定的管脚
 * 输出参数：返回 TRUE
 * 说明：未指定Port是因为只有PortA有具有中断功能；
 *     边缘触发中断可以通过写寄存器清除，电平触发中断无法通过写寄存器清除中断，只能通过轮询等待、中断屏蔽、中断失能进行清除，否则会反复进中断
 */
BOOL GPIO_ClearInterrupt(pStruct_GPIOInfo pGpio, UINT32 dwPins);

/*
 * 使能指定引脚的中断
 * 输入参数：pGpio： I/O，可选GPIO0、 GPIO1
 *         dwPins: 所指定的管脚
 * 输出参数：返回 TRUE
 * 说明：未指定Port是因为只有PortA有具有中断功能；
 *     边缘触发中断可以通过写寄存器清除，电平触发中断无法通过写寄存器清除中断，只能通过轮询等待、中断屏蔽、中断失能进行清除，否则会反复进中断
 */
BOOL GPIO_EnableInterrupt(pStruct_GPIOInfo pGpio, UINT32 dwPins);

/*
 * 失能指定引脚的中断
 * 输入参数：pGpio： I/O，可选GPIO0、 GPIO1
 *         dwPins: 所指定的管脚
 * 输出参数：返回 TRUE
 * 说明：未指定Port是因为只有PortA有具有中断功能；
 */
BOOL GPIO_DisableInterrupt(pStruct_GPIOInfo pGpio, UINT32 dwPins);

/*
 * 屏蔽指定引脚的中断
 * 输入参数：pGpio： I/O，可选GPIO0、 GPIO1
 *         dwPins: 所指定的管脚
 * 输出参数：
 * 说明：未指定Port是因为只有PortA有具有中断功能；
 *     边缘触发中断可以通过写寄存器清除，电平触发中断无法通过写寄存器清除中断，只能通过轮询等待、中断屏蔽、中断失能进行清除，否则会反复进中断
 */
void GPIO_MaskInterrupt(pStruct_GPIOInfo pGpio, UINT32 dwPins);

/*
 * 解除指定引脚的屏蔽
 * 输入参数：pGpio： I/O，可选GPIO0、 GPIO1
 *         dwPins: 所指定的管脚
 * 输出参数：
 * 说明：未指定Port是因为只有PortA有具有中断功能；
 *     边缘触发中断可以通过写寄存器清除，电平触发中断无法通过写寄存器清除中断，只能通过轮询等待、中断屏蔽、中断失能进行清除，否则会反复进中断
 */
void GPIO_UnmaskInterrupt(pStruct_GPIOInfo pGpio, UINT32 dwPins);

#endif
