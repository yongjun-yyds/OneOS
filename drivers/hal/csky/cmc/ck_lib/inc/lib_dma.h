/*
 文件: dma.h
 版本: CMC693 V1.10
 版权: 浙江杰芯科技有限公司
 作者: zhulin(zhulin2@supcon.com)
 描述：
修改记录：
=========
日期                                      作者                       工作                            内容
*/

#ifndef __DRVDMA_H_
#define __DRVDMA_H_

#include "datatype.h"
#include "ckdma.h"

#define		DMA_CH_NUM		2	//DMA通道数

#define 	DMA_ID_INVALID	0	//通道不合法

#define ENABLE_INT_BLOCK		(1 << 0)	// Block Transfer Complete Interrupt
#define ENABLE_INT_DSTTRAN		(1 << 1)	// Destination Transaction Complete Interrupt
#define ENABLE_INT_ERR			(1 << 2)	// Error Interrupt
#define ENABLE_INT_SRCTRAN		(1 << 3)	// Source Transaction Complete Interrupt
#define ENABLE_INT_TFR			(1 << 4)	// DMA Transfer Complete Interrupt

#define 	DMA_CH_1		0		//DMA第1通道
#define 	DMA_CH_2		1		//DMA第2通道
#define 	DMA_CH_INVALID	0xFF	//不合法通道

typedef enum
{
    DMA_UART0_TX = 0,
    DMA_UART0_RX = 1,
    DMA_SPI_TX   = 2,
    DMA_SPI_RX   = 3,
    DMA_IIC_TX   = 4,
    DMA_IIC_RX   = 5,
    DMA_UART1_TX = 7,
    DMA_UART1_RX = 8,

    DMA_NOT_PERIPHERAL = 9 	//设备为Memory, 不是Peripheral
} Enum_DMA_Peripheral;

typedef enum
{
	DMA_Memory2Memory         = 0,
    DMA_Memory2Peripheral     = 1,
    DMA_Peripheral2Memory     = 2,
    DMA_Peripheral2Peripheral = 3
} Enum_DMA_DIR;



//地址生长方向
typedef enum
{
    DMA_Increment = 0,
    DMA_Decrement = 1,
    DMA_NoChange  = 2
} Enum_DMA_AddrGrowth;


//传输位宽
typedef enum
{
    DMA_Width_8bits  = 0,
    DMA_Width_16bits = 1,
    DMA_Width_32bits = 2
} Enum_DMA_DataWidth;


//突发传输（一次DMA）的长度
typedef enum
{
    DMA_Burst_1item = 0,
    DMA_Burst_4item = 1,
    DMA_Burst_8item = 2
} Enum_DMA_BurstLength;


//极性？
typedef enum
{
    DMA_High = 0,
    DMA_Low  = 1,
} Enum_DMA_Polarity;



//握手对象是软件，还是硬件
typedef enum
{
    DMA_Hardware = 0,
    DMA_Software = 1,
} Enum_DMA_Handshaking;



typedef enum
{
    DMA_PriLow = 0,
    DMA_PriHigh = 1,
} Enum_DMA_Priority;

/* 通道信息 */
typedef struct
{
	UINT32  dwChBaseAddr;		//相应通道的寄存器基地址
	Enum_DMA_Priority ChPrio;	//通道的优先级. NOTE: 通道0固定为高优先级，通道1固定为低优先级
	BOOL	beOpened;		//是否被占用
	UINT16  wID;				//用于唯一标识DMA配置
	UINT32	dwLastSrcAddr;	//上一次DMA传输的源地址
	UINT32 	dwLastDestAddr;	//上一次DMA传输的目的地址
	UINT32	dwLastItems;		//上一次传输长度
    void 	(*Callback)( void *parg, UINT8 ch);
    void 	*parg;			//回调函数参数
} ChannelInfo_st, *pDMA_ChannelInfo_st;

typedef struct
{
  Enum_DMA_DIR DMA_Direction;
  UINT32 DMA_EnableInterrupt;

  UINT32 DMA_SrcBaseAddr;
  Enum_DMA_AddrGrowth DMA_SrcGrowth;
  Enum_DMA_DataWidth DMA_SrcDataWidth;
  Enum_DMA_Peripheral DMA_SrcPeripheral;

  UINT32 DMA_DestBaseAddr;
  Enum_DMA_AddrGrowth DMA_DestGrowth;
  Enum_DMA_DataWidth DMA_DestDataWidth;
  Enum_DMA_Peripheral DMA_DestPeripheral;

  UINT32 DMA_BlockTransferSize;
  Enum_DMA_BurstLength DMA_BurstTransfer;

  void (*Callback)(void *parg, UINT8 ch);
  void 	*parg;			//回调函数参数

}DMA_Init_st, *pDMA_Init_st;

BOOL DMA_Restart(UINT16 id, UINT32 SrcAddr, UINT32 DestAddr, UINT32 items);
UINT8 DMA_Init(pDMA_Init_st pInit);
BOOL DMA_Free(UINT8 id);
void DMA_WaitBlockTfrIdle(void);
#endif /* DRVDMA_H_ */
