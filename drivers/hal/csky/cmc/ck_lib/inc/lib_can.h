/*
 文件: can.h
 版本: CMC693 V1.10
 版权: 浙江杰芯科技有限公司
 作者: zhulin(zhulin2@supcon.com)
 描述：
修改记录：
=========
日期                                      作者                       工作                            内容
*/

#ifndef LIB_CAN_H_
#define LIB_CAN_H_

#include "ckcan.h"
#include "queue.h"

#define CAN_BUADRATE_MAX	(1000000)    /* 波特率上限 */
#define CAN_BUADRATE_MIN	(2000)       /* 波特率下限 */

#define BTR_WATCH_EN		0    /* 使能BTR寄存器值的监视 */
#define CAN_CQ_EN			0    /* 使能循环队列存储CAN报文 */

/* CAN的工作模式 */
typedef enum
{
	CAN_MODE_STANDARD 	= 0,     /* 标准模式 */
	CAN_MODE_EXTEND		= 1,     /* 扩展模式 */
}CAN_MODE;

/* 四种具体的CAN工作模式 */
#define 	CAN_MODE_STANDARD_RESET		0
#define 	CAN_MODE_STANDARD_NORMAL	1
#define 	CAN_MODE_EXTEND_RESET		2
#define 	CAN_MODE_EXTEND_NORMAL		3


/* 用于给用户配置的中断向量（非对应实际中断寄存器相应位） */
#define CAN_IT_NONE			0x00	/* 关闭所有中断 */
#define CAN_IT_OIE			0x01	/* 溢出中断使能 */
#define CAN_IT_EIE			0x02	/* 错误中断使能 */
#define CAN_IT_TIE			0x04	/* 发送中断使能 */
#define CAN_IT_RIE			0x08	/* 接收中断使能 */
#define CAN_IT_EPIE			0x10	/* 错误消极中断使能 */
#define CAN_IT_ALIE			0x20	/* 仲裁丢失中断使能 */
#define CAN_IT_BEIE			0x40	/* 总线错误中断使能 */

/* 用于用户配置的中断类型（非对应实际中断寄存器相应位） */
#define CAN_IT_OI			0x01	/* 溢出中断使能 */
#define CAN_IT_EI			0x02	/* 错误中断使能 */
#define CAN_IT_TI			0x04	/* 发送中断使能 */
#define CAN_IT_RI			0x08	/* 接收中断使能 */
#define CAN_IT_BEI			0x10	/* 总线错误中断使能 */
#define CAN_IT_ALI			0x20	/* 仲裁丢失中断使能 */
#define CAN_IT_EPI			0x40	/* 错误消极中断使能 */


/* 中断使能向量(寄存器位，不应当被直接用于使用) */
#define CAN_STD_IT_OIE 		0x10	/* 溢出中断使能 */
#define CAN_STD_IT_EIE 	  	0x08  	/* 错误中断使能 */
#define CAN_STD_IT_TIE   	0x04  	/* 发送中断使能 */
#define CAN_STD_IT_RIE   	0x02  	/* 接收中断使能 */
#define CAN_EX_IT_BEIE  	0x80 	/* 总线错误中断使能 */
#define CAN_EX_IT_ALIE  	0x40  	/* 仲裁丢失中断使能 */
#define CAN_EX_IT_EPIE  	0x20 	/* 错误消极中断使能 */
#define CAN_EX_IT_DOIE  	0x08 	/* 溢出中断使能 */
#define CAN_EX_IT_EIE   	0x04 	/* 错误中断使能 */
#define CAN_EX_IT_TIE   	0x02 	/* 发送中断使能 */
#define CAN_EX_IT_RIE   	0x01 	/* 接收中断使能 */

/* 中断源向量(寄存器位，不应当被直接用于使用) */
#define CAN_STD_IT_DOI 	  	0x08	/* 数据溢出中断 */
#define CAN_STD_IT_EI   	0x04	/* 错误中断 */
#define CAN_STD_IT_TI   	0x02	/* 发送中断 */
#define CAN_STD_IT_RI 		0x01	/* 接收中断 */
#define CAN_EX_IT_BEI  		0x80	/* 总线错误中断 */
#define CAN_EX_IT_ALI  		0x40	/* 仲裁丢失中断 */
#define CAN_EX_IT_EPI  		0x20	/* 错误消极中断 */
#define CAN_EX_IT_DOI  		0x08	/* 数据溢出中断 */
#define CAN_EX_IT_EI  		0x04	/* 错误中断 */
#define CAN_EX_IT_TI   		0x02	/* 发送中断 */
#define CAN_EX_IT_RI   		0x01	/* 接收中断 */

/* 状态寄存器相关位定义 */
#define CAN_SR_BUSACTIVE	0x80	/* 总线状态  0 参与总线活动  1退出总线活动 */
#define CAN_SR_TSBUSY		0x20	/* 发送状态  1 发送忙，      0 发送空闲 */
#define CAN_SR_RSBUSY		0x10	/* 接收状态  1 正在接收， 0 接收空闲 */

/* 波特率寄存器值存储结构体 */
typedef struct S_BTR
{
	UINT16	BTR0;
	UINT16	BTR1;
	UINT32	BuadRate;
	INT32S	ClkError;		        /* 相对输入时钟，逆向计算得到的时钟频率差值。 */
}Struct_BTR, *pStruct_BTR;	        /* 总线定时寄存器参数结构体 */

/* can初始化参数结构体 */
typedef struct S_CANInit
{
	UINT32 buadrate;			    /* CAN的波特率参数 */
	UINT32 canacr;				    /* ACR.即本机ID */
	UINT32 canamr;				    /* CAN模块屏蔽值(AMR) */
	CAN_MODE canmode;			    /* CAN的工作模式选择 */
	UINT8 intrVector;			    /* 中断源中断向量设置 */
	UINT32 priority;                /* 中断优先级 */
	void (*CAN_ISR)(UINT32);        /* 由用户自定义的中断处理函数，若为NULL，则使用默认的中断处理函数 */
	pStruct_CAN pCAN;               /* CAN寄存器的基地址*/
}Struct_CANInit, *pStruct_CANInit;

/* CAN的帧结构信息 */
typedef struct S_CAN_Frame
{
	union
	{
		UINT8 Descriptor; 		/* 描述符 */
		struct
		{
			UINT8	DLC	: 4;	/* 报文数据长度 */
			UINT8 	RTR : 1; 	/* 远程帧识别位	1远程帧(无数据字节), 0数据帧 */
			UINT8	FF	: 1;	/* 扩展帧识别位	1扩展帧, 0标准帧格式 */
		};
	};

	UINT32	ID;					/* 识别码 11位或29位，放在描述符中太浪费，单独排列。 */
	UINT8 	Data[8];			/* 8个字节的数据 */
}CANFrameInfo_st, *pCANFrameInfo_st;

/* CAN信息结构体定义 */
typedef struct
{
	Struct_CANInit CAN_Initstruct;
	union
	{
		pStruct_CAN pCAN;           /* CAN寄存器的基地址*/
		pStruct_CAN_Ex pCAN_Ex;
	};
	CAN_MODE CANMode;              /* can的工作模式 */
	UINT32 BuadRate;               /* can的波特率参数 */
#if BTR_WATCH_EN
	Struct_BTR struct_BTR;         /* 总线定时寄存器参数 */
#endif
#if CAN_CQ_EN
	CycleQueue_t *pCANCQ;          /* CAN所指向的循环队列 */
#endif
}Struct_CANInfo, *pStruct_CANInfo;

extern pStruct_CANInfo pCAN0;
extern pStruct_CANInfo pCAN1;

extern CANFrameInfo_st CAN0FrameInfo_Rx;
extern CANFrameInfo_st CAN0FrameInfo_Tx;
extern CANFrameInfo_st CAN1FrameInfo_Rx;
extern CANFrameInfo_st CAN1FrameInfo_Tx;
/*
 * 根据初始化配置参数配置can
 * 输入参数：
 * 输出参数：
 */
void CAN_Config(pStruct_CANInit pCANInit);

/*
 * @brief  : CAN模块数据发送.如果一个发送请求尚未被处理则将被取消发
 * @param  : pCANInfo, can信息结构体指针
 *           pCanFrameInfo_st CAN帧数据结构
 * @note   : None.
 *
 * @retval : 0， 成功; 其他，失败。
 */
int CAN_Transmit(pStruct_CANInfo pCANInfo,
				 pCANFrameInfo_st pCanFrameInfo_st);

/*
 * @brief  : CAN模块入栈一个CAN报文
 * @param  : pCANInfo, can信息结构体指针
 *           pCanFrameInfo_st   CAN报文结构体指针
 * @note   : None.
 *
 * @retval : 实际读取的长度（按字节计数）
 */
UINT16 CAN_CQ_PopFrame(pStruct_CANInfo pCANInfo,
					   pCANFrameInfo_st pCanFrameInfo_st);

/*
 * @brief  : CAN模块入栈一个CAN报文
 * @param  : pCANInfo, can信息结构体指针
 *           pCanFrameInfo_st   CAN报文结构体指针
 * @note   : None.
 *
 * @retval : 实际写入的长度（按字节计数）
 */
UINT16 CAN_CQ_PushFrame(pStruct_CANInfo pCANInfo,
					    pCANFrameInfo_st pCanFrameInfo_st);

/*
 * @brief  : CAN模块获取中断向量
 * @param  : pCANInfo, can信息结构体指针
 * @note   : 返回的中断向量与具体的模式无关
 *
 * @retval : 中断向量值,如CAN_IT_RIE。
 */
int CAN_GetITVector(pStruct_CANInfo pCANInfo, UINT8 *Vector);

/*
 * @brief  : CAN控制器接收的数据
 * @param  : pCANInfo, can信息结构体指针
 *           pCanFrameInfo_st CAN帧数据结构
 * @note   : 只释放接收缓冲区
 *
 * @retval : 0， 成功; 其他，失败。
 */
int CAN_ReceiveFrame(pStruct_CANInfo pCANInfo,pCANFrameInfo_st pCanFrameInfo_st);

/*
 * @brief  : CAN控制器的缓冲区释放
 * @param  : pCANInfo, can信息结构体指针
 * @note   : 只释放接收缓冲区, 不能在数据接收操作前进行此步骤.
 *
 * @retval : 0， 成功; 其他，失败。
 */
int CAN_ReleaseFIFO(pStruct_CANInfo pCANInfo);

/*
 * @brief  : 得到当前CAN的状态
 * @param  : pCANInfo, can信息结构体指针
 * @note   : None.
 *
 * @retval : SR寄存器的值。
 */
UINT8 CAN_GetStatus(pStruct_CANInfo pCANInfo);

/*
 * 函数名称:CAN_GetInitStructure
 * 函数输入:pStruct_CANInit pInit_to 把旧初始化参数的赋值对象
 * 函数说明:把旧初始化参数赋值给传入结构体
 */
BOOL CAN_GetInitStructure(pStruct_CANInit pInit_to, UINT8 port);

int CAN_ITConfig(pStruct_CANInfo pCANInfo, UINT8 cIntr);


#endif /* CAN_H_ */
