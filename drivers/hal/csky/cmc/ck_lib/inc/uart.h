/*
 文件: uart.h
 版本: CMC693 V1.10
 版权: 浙江杰芯科技有限公司
 作者: zhulin(zhulin2@supcon.com)
 描述：
修改记录：
=========
日期                                      作者                       工作                            内容
*/

#ifndef __UART_H__
#define __UART_H__

#include "ckuart.h"
#include "intc.h"
#include "circlebuffer.h"
#include "datatype.h"

#define BAUDRATE   38400

#define  CONFIG_TERMINAL_UART UART0

#define UART0_IRQID    CK_INTC_UART0
#define UART1_IRQID    CK_INTC_UART1

#define UART_CIRCLEBUFFER_ENABLE    0    /* 为1时，UART使用环形buff。为0时，UART禁止使用环形buff，可以节省内存 */

#if UART_CIRCLEBUFFER_ENABLE
/*UART在中断模式下使用的环形buff大小 */
#define CK_UART_TXBUFFERSIZE 4096
#define CK_UART_RXBUFFERSIZE 4096
#endif

typedef enum{
	UART0,
	UART1
}Enum_Uart_Device;

typedef enum{
    B600 = 600,
    B1200 = 1200,
    B2400 = 2400,
    B4800 = 4800,
    B9600 = 9600,
    B14400 = 14400,
    B19200 = 19200,
    B56000 = 56000,
    B38400 = 38400,
    B57600 = 57600,
    B115200 = 115200,
    B125000 = 125000,
    B128000 = 128000,
    B256000 = 256000
}Enum_Uart_Baudrate;

typedef enum{
    WORD_SIZE_5=5,
    WORD_SIZE_6,
    WORD_SIZE_7,
    WORD_SIZE_8
}Enum_Uart_WordSize;

typedef enum{
    ODD=0,
    EVEN=1,
    NONE=2
}Enum_Uart_Parity;

typedef enum{
	LCR_STOP_BIT_1=1,
	LCR_STOP_BIT_2=2
}Enum_Uart_StopBit;

typedef enum{
    CK_Uart_CTRL_C = 0,
    CK_Uart_FrameError = 1,
    CK_Uart_ParityError = 2
}Enum_Uart_Error;

typedef enum{
	UART_Query,
	UART_Intr
}Enum_Uart_Mode;

typedef struct{
	Enum_Uart_Device UartId;                    /* uart ID号 */
	Enum_Uart_Baudrate BaudRate;                /* 波特率 */
	Enum_Uart_WordSize WordSize;                /* 字长，即几位有效数据位 */
	Enum_Uart_StopBit StopBit;                  /* 停止位个数 */
	Enum_Uart_Parity Parity;                    /* 奇偶校验位 */
	Enum_Uart_Mode RxMode;                      /* 接收模式，UART_Query:轮询模式, UART_Intr:中断模式 */
	Enum_Uart_Mode TxMode;                      /* 发送模式，UART_Query:轮询模式, UART_Intr:中断模式 */
	UINT8 Priority;                             /* 中断优先级，当设置为中断模式时需要设置此参数 */
	void (*UART_ISR)(UINT32);                   /* 中断处理回调函数，当用户需要自定义中断函数时，将自定义的函数地址
	                                                                                                                      添加到此处，若要使用默认的中断函数功能，将此参数配置为NULL */
}UART_InitStructure,*pUART_InitStructure;

typedef struct{
  UINT32 id;                                   /* UART ID号*/
  pStruct_UartType addr;                       /* UART基地址 */
  UINT32 priority;                             /* 中断优先级 */
  BOOL bopened;                                /* 串口打开状态 */
  Struct_IRQHandler irqhandler;                /* UART中断信息结构体 */
  void  (* handler)(INT8S error);               /* 用户自定义的回调函数接口 */
  UART_InitStructure UART_Initstruct;          /* 保存UART初始化信息 */
  BOOL btxquery;                               /* UART发送模式是否是轮询模式 */
  BOOL brxquery;                               /* UART接收模式是否是轮询模式 */
#if UART_CIRCLEBUFFER_ENABLE
  UINT8 txbuffer[CK_UART_TXBUFFERSIZE];        /* 中断模式下使用的数据发送缓冲区 */
  UINT8 rxbuffer[CK_UART_RXBUFFERSIZE];        /* 中断模式下使用的数据接收缓冲区 */
  CKStruct_CircleBuffer txcirclebuffer;        /* 中断模式下使用的发送环形buff结构 */
  CKStruct_CircleBuffer rxcirclebuffer;        /* 中断模式下使用的接收环形buff结构 */
#endif
} Struct_UartInfo, *pStruct_UartInfo;

extern Struct_UartInfo Uart_Table[];

#if 0
/*
 * 使所有UART处于空闲模式
 * 输入参数：
 * 输出参数：
 */
void Deactive_UartModule(void);
#endif

/*
 * 根据配置参数配置UART
 * 输入参数：pUartInfo： 串口配置信息结构体；
 * 输出参数：返回 0， 成功; 其他，失败
 */
INT32U Uart_Config(pUART_InitStructure pUartInfo);

/*
 * 设置UART的FIFO
 * 输入参数：enUartID：UART0
 *                  UART1；
 *         dwMode:待配置的FIFO模式
 *                bit[7:6]:接收触发设置，00=接收FIFO中有一个字节时触发
 *                                   01= 达到接收FIFO的1/4时触发
 *                                   10= 达到接收FIFO的1/2时触发
 *                                   11= 达到接收FIFO-2时触发
 *                bit[5:4]:发送为空触发，00=发送FIFO全空时触发
 *                                   01=FIFO中小于等于2个字节时触发
 *                                   10=小于等于FIFO的1/4时触发
 *                                   11=小于等于FIFO的1/2时触发
 *                bit3：DMA模式，0=MODE0，DMA仅支持单个数据的传送
 *                             1=MODE1
 *                bit2：发送FIFO复位
 *                bit1:接收FIFO复位
 *                bit0：使能FIFO
 * 输出参数：返回 0， 成功; 其他，失败
 */
void Uart_SetFIFO(Enum_Uart_Device enUartID,UINT32 dwMode);

/*
 * 设置UART的波特率
 * 输入参数：enUartID：  UART0,
 *                   UART1
 *        enBaudrate:
 *                B600,
 *                B1200,
 *                B2400,
 *                B4800,
 *                B9600,
 *                B14400,
 *                B19200,
 *                B56000,
 *                B38400,
 *                B57600,
 *                B76800,
 *                B115200,
 *                B125000,
 *                B128000,
 *                B256000
 * 输出参数：SUCCESS， 成功; FAILURE，失败
 */
INT8S Uart_ChangeBaudrate(Enum_Uart_Device enUartID,Enum_Uart_Baudrate enBaudrate);

/*
 * 设置UART的奇偶校验位
 * 输入参数：enUartID：UART0
 *                  UART1
 *        enParity： ODD   奇校验
 *                  EVEN  偶校验
 *                  NONE  无校验
 * 输出参数：SUCCESS， 成功; FAILURE，失败
 */
INT8S Uart_SetParity(Enum_Uart_Device enUartID, Enum_Uart_Parity enParity);

/*
 * 设置UART的停止位，可以设置1位停止位或者2位停止位。当设置2位停止位时，如果数据长度设置为5
 * ，即LCR[1:0]为0，则实际发送1.5个停止位。
 * 输入参数：enUartID:UART0
 *                  UART1
 *        enStopBit： LCR_STOP_BIT_1
 *                   LCR_STOP_BIT_2
 * 输出参数：SUCCESS， 成功; FAILURE，失败
 */
INT8S Uart_SetStopBit(Enum_Uart_Device enUartID, Enum_Uart_Error enStopBit);

/*
 * 设置UART的数据域长度
 * 输入参数：enUartID:UART0
 *                  UART1
 *        enWordSize： WORD_SIZE_5
 *                    WORD_SIZE_6
 *                    WORD_SIZE_7
 *                    WORD_SIZE_8
 * 输出参数：SUCCESS， 成功; FAILURE，失败
 */
INT8S Uart_SetWordSize(Enum_Uart_Device enUartID,Enum_Uart_WordSize enWordSize);

/*
 * 设置UART的发送模式，轮询模式还是中断模式
 * 输入参数：enUartID：UART0
 *                  UART1
 *        enMode： UART_Query    轮询模式
 *                UART_Intr     中断模式
 * 输出参数：SUCCESS， 成功; FAILURE，失败
 */
UINT8 Uart_SetTXMode(Enum_Uart_Device enUartID, Enum_Uart_Mode enMode);

/*
 * 设置UART的接收模式，轮询模式还是中断模式
 * 输入参数：enUartID：UART0
 *                  UART1
 *        enMode： UART_Query    轮询模式
 *                UART_Intr     中断模式
 * 输出参数：SUCCESS， 成功; FAILURE，失败
 */
INT8S Uart_SetRXMode(Enum_Uart_Device enUartID, Enum_Uart_Mode enMode);

/*
 * 从指定串口接收一个字符，轮询模式下会阻塞在此函数，建议使用中断模式
 * 输入参数：enUartID：UART0
 *                  UART1
 *         ch：存放待接收的字符
 * 输出参数：SUCCESS， 成功; FAILURE，失败
 */
INT8S Uart_GetChar(Enum_Uart_Device enUartID, UINT8 *ch);

/*
 * 从指定串口发送一个字符
 * 输入参数：enUartID：UART0
 *                  UART1
 *         ch：待发送的字符
 * 输出参数：SUCCESS， 成功; FAILURE，失败
 */
INT8S Uart_PutChar(Enum_Uart_Device enUartID, UINT8 ch);

#if UART_CIRCLEBUFFER_ENABLE
/*
 * 清除UART的环形buff
 * 输入参数：enUartID：UART0
 *                  UART1
 * 输出参数：
 */
void UART_ClearRxBuffer(Enum_Uart_Device enUartID);
#endif
/*
 * 打开串口，注册中断函数
 * 输入参数：enUartID：UART0
 *                  UART1
 *        dwPriority： 中断优先级
 *        handler：回调函数，可按照用户需求自行定义及使用
 * 输出参数：SUCCESS， 成功; FAILURE，失败
 */
INT8S Uart_OpenIntr(Enum_Uart_Device enUartID, UINT32 dwPriority, void (*handler)(CK_INT8 error));

/*
 * 关闭串口中断
 * 输入参数：enUartID：UART0
 *                  UART1
 * 输出参数：SUCCESS， 成功; FAILURE，失败
 */
INT8S Uart_CloseIntr(Enum_Uart_Device enUartID);
#endif
