/*
 文件: rtc.h
 版本: CMC693 V1.10
 版权: 浙江杰芯科技有限公司
 作者: zhulin(zhulin2@supcon.com)
 描述：
修改记录：
=========
日期                                      作者                       工作                            内容
*/

#ifndef __LIB_RTC_H_
#define __LIB_RTC_H_

#include "ckrtc.h"

#define RTC_INTR               CK_INTC_RTC

#define RTC_INTR_ACTIVE        (UINT8)(1)    /* 产生中断 */
#define RTC_INTR_INACTIVE      (UINT8)(0)    /* 未产生中断 */

/*
 * 获取RTC计数器的当前值
 * 输入参数：无
 * 输出参数：64位的RTC计数器当前值
 * 说明：
 */
UINT64 RTC_GetCounter(void);

/*
 * 设置RTC的计数器匹配寄存器
 * 输入参数：无
 * 输出参数：64位的RTC计数器当前值
 * 说明：当RTC的计数器值与计数器匹配寄存器相同时，且使能该终端时，可产生中断。
 */
void RTC_SetCounterMatch(UINT64 qwValue);

/*
 * 设置RTC的计数器装载值-RTC_CLR寄存器
 * 输入参数：无
 * 输出参数：64位的RTC计数器当前值
 * 说明：写入到计数器装载寄存器RTC_CLR的值，会被加载到当前计数器寄存器，更新当前时间
 */
void RTC_SetCounterLoad(UINT64 qwValue);

/*
 * 使能RTC计数器的warp功能
 * 输入参数：无
 * 输出参数：无
 * 说明：
 */
void RTC_EnableWarp(void);

/*
 * 禁能RTC计数器的warp功能
 * 输入参数：无
 * 输出参数：无
 * 说明：
 */
void RTC_DisableWarp(void);

/*
 * 使能RTC计数器
 * 输入参数：无
 * 输出参数：无
 * 说明：
 */
void RTC_EnableCounter(void);

/*
 * 禁能RTC计数器
 * 输入参数：无
 * 输出参数：无
 * 说明：
 */
void RTC_DisableCounter(void);

/*
 * 屏蔽RTC中断
 * 输入参数：无
 * 输出参数：无
 * 说明：
 */
void RTC_MaskIntr(void);

/*
 * 取消RTC中断的屏蔽
 * 输入参数：无
 * 输出参数：无
 * 说明：
 */
void RTC_UnmaskIntr(void);

/*
 * 使能RTC中断
 * 输入参数：无
 * 输出参数：无
 * 说明：
 */
void RTC_EnableIntr(void);

/*
 * 禁能RTC中断
 * 输入参数：无
 * 输出参数：无
 * 说明：
 */
void RTC_DisableIntr(void);

/*
 * 读取RTC中断状态
 * 输入参数：无
 * 输出参数：RTC_INTR_ACTIVE,产生了中断状态
 *         RTC_INTR_INACTIVE，未产生中断状态
 * 说明：该函数读取的是RTC_STAT中断状态寄存器，该状态与实际是否产生中断一致，若开启中断屏蔽，
 *      同时达到了中断触发条件，此寄存器不会置位
 */
UINT8 RTC_GetIntrStatus(void);

/*
 * 读取RTC中断原始状态
 * 输入参数：无
 * 输出参数：RTC_INTR_ACTIVE,产生了中断状态
 *         RTC_INTR_INACTIVE，未产生中断状态
 * 说明：该函数读取的是RTC_RSTAT中断原始状态寄存器，若开启了中断屏蔽功能，同时达到了
 *      中断条件时，该状态寄存器依然会置位，但RTC_STAT寄存器不会置位，且不产生中断
 *      信号。
 */
UINT8 RTC_GetIntrRawStatus(void);

/*
 * 清除RTC中断状态
 * 输入参数：无
 * 输出参数：无
 * 说明：
 */
void RTC_ClearIntr(void);

/*
 * 读取RTC组件软件版本
 * 输入参数：无
 * 输出参数：组件的软件版本
 * 说明：
 */
UINT32 RTC_GetCompVersion(void);

/*
 * 使能RTC隔离模式
 * 输入参数：无
 * 输出参数：无
 * 说明：设置为该模式后，当系统电源掉电后，RTC可使用自己的电源继续运行。当系统恢复上电后，
 *      用户应该通过POWM电源管理模块的AWAKEN寄存器唤醒RTC，使RTC返回正常模式。
 */
void RTC_EnableIsolate(void);

/*
 * 禁能RTC隔离模式
 * 输入参数：无
 * 输出参数：无
 * 说明：禁能该模式后，当系统电源掉电后，RTC不可使用自己的电源继续运行。
 */
void RTC_DisableIsolate(void);

/*
 * 注册RTC中断回调函数，及设置中断配置
 * 输入参数：dwPrio：RTC中断优先级
 *         RTC_ISR：中断处理回调函数，由用户自定义
 * 输出参数：无
 * 说明：只允许调用一次,若RTC_ISR未定义，则直接返回
 */
void RTC_ConfigIntr(UINT32 dwPrio, void (*RTC_ISR)(UINT32));

#endif /* __RTC_H_ */
