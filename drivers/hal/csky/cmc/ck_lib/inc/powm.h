/*****************************************************************************
 *  File: powm.h
 *
 *  Descirption: this file contains the functions support  power management
 *  operations.
 *
 *  Copyright (C) : 2008 Hangzhou C-Sky Microsystems Co.,Ltd.
 *
 *  Author: ShuLi Wu
 *  Mail:   shuli_wu@c-sky.com
 *  Date:   Oct 9 2008
 *
 *****************************************************************************/
#ifndef __POWM_H__
#define __POWM_H__

#include "datatype.h"

#define POWM_LP_WAIT      ((UINT8)0)         /* 低功耗-wait模式 */
#define POWM_LP_DOZE      ((UINT8)1)         /* 低功耗-doze模式 */
#define POWM_LP_STOP      ((UINT8)2)         /* 低功耗-stop模式 */

extern UINT32 SYS_FREQ;
extern UINT32 AHB_FREQ;
extern UINT32 APB_FREQ;

/*
 * 配置CPU、AHB、APB的频率
 * 输入参数：dwInputClk：外部输入的时钟频率（Hz）
 *                    若使用PLL倍频，则此参数应该是不小于1MHz的整数值，如1、2、3...(MHz)
 *                    若使用bypass功能，则将此参数设置为0,且dwCpuClk输入正确的值
 *        dwCpuClk：目标CPU频率
 *                  若使用PLL倍频，则此参数应不大于200000000Hz（200MHz）且不小于62500000Hz
 *                  若希望设置小于62500000Hz的频率，则只能采用bypass功能，直接使用外部输入时钟作为
 *                  系统时钟，则该值必须等于外部输入时钟的频率
 *        dwRatio:比率值，此值代表CPU、AHB和APB的比率，用来确定AHB和APB的频率
 *                具体为：
 *                0: CPU : AHB = 1 : 1;
 *                   AHB :APB  = 1 : 1;
 *                1: CPU : AHB = 2 : 1;
 *                   AHB :APB  = 1 : 1;
 *                2: CPU : AHB = 1 : 1;
 *                   AHB :APB  = 2 : 1;
 *                3: CPU : AHB = 4 : 1;
 *                   AHB :APB  = 1 : 1;
 *                4: CPU : AHB = 2 : 1;
 *                   AHB :APB  = 2 : 1;
 *                5: CPU : AHB = 8 : 1;
 *                   AHB :APB  = 1 : 1;
 *                6: CPU : AHB = 4 : 1;
 *                   AHB :APB  = 2 : 1;
 *                7: CPU : AHB = 8 : 1;
 *                   AHB :APB  = 2 : 1;
 * 输出参数：
 */
INT32S ConfigClk(UINT32 dwInputClk, UINT32 dwCpuClk, UINT32 dwRatio );

/*
 * 复位指定IP模块
 * 输入参数：dwIPs:IP模块的模块号
 *        如IP_WDT，在ckpowm.h中定义
 * 输出参数：
 */
INT32S POWM_IPSoftwareReset(UINT32 dwIPs);

/*
 * 电源管理模块的中断服务程序
 * 输入参数：dwIrqId：中断号
 * 输出参数：
 */
void PowerManage_Interrupt(UINT32 dwIrqId);

/*
 * 关闭指定IP模块
 * 输入参数：dwIPs:IP模块的模块号
 *        如IP_WDT，在ckpowm.h中定义
 * 输出参数：
 */
INT32S POWM_CutIPsClk(UINT32 dwIPs);

/*
 * 配置POWM进入指定的低功耗模式
 * 输入参数：cMode：待进入的低功耗模式
 *               POWM_LP_WAIT----wait模式
 *               POWM_LP_DOZE----doze模式
 *               POWM_LP_STOP----stop模式
 *        dwIP: 表示需要关闭时钟的功能IP模块，共32位,相应的位置1，则表示需要关闭该模块
 *              当进入STOP模式时，此参数不起作用
 *              bit0: WDT
 *              bit1: TIMER
 *              bit2: GPIO0
 *              bit3: GPIO1
 *              bit4: UART
 *              bit5: SPI
 *              bit6: I2C
 *              bit7: PIPO
 *              bit8: CAN0
 *              bit9: SPIS
 *              bit10:RTC
 *              bit11:UART1
 *              bit12:CAN1
 *              bit13:SPI1
 *              bit16:DMAC
 *              bit17:MAC0
 *              bit18:MAC1
 *              bit19:LCP
 *              bit20:MCP
 * 输出参数：
 * 说明：1.wait模式和doze模式的唤醒可通过外部中断实现，如RTC、GPIO或timer等中断,
 *       当使用外部中断唤醒时，不能关闭相应IP模块的时钟，如使用GPIO0中断唤醒，则dwIP
 *       参数的bit2应该为0，且支持边沿触发和电平触发中断；
 *     2.stop模式可通过外部中断（RTC或GPIO）或者stop模式计数器中断唤醒。
 *       由于CMC暂时不支持RTC隔离模式，所以仅能通过stop模式计数器和GPIO中断唤醒,
 *       而GPIO中断唤醒必须被配置为电平触发中断才能唤醒（最好将gpio_ls_sync配置为
 *       同步到pclk_intr）。
 */
void POWM_ToLowPower(UINT8 cMode, UINT32 dwIP);

/*
 * 使能stop模式计数器，用于唤醒STOP模式
 * 输入参数：dwCount:需要设置的计数值，当此值减为0时，唤醒CPU
 *                不能大于0X7FFFFFFF
 * 输出参数：
 */
void POWM_StopAwake(UINT32 dwCount);

#endif
