/*
 * xtopology.h
 *
 *  Created on: 2019-4-16
 *      Author: zhulin2
 */

#ifndef XTOPOLOGY_H_
#define XTOPOLOGY_H_

enum xemac_types { xemac_type_unknown = -1,
	               xemac_type_xps_emaclite,
	               xemac_type_xps_ll_temac,
	               xemac_type_axi_ethernet,
	               xemac_type_emacps,
	               xemac_type_gmac };

struct xtopology_t {
	unsigned emac_baseaddr;
	enum xemac_types emac_type;
	unsigned intc_baseaddr;
	unsigned intc_emac_intr;	/* valid only for xemac_type_xps_emaclite */
	unsigned scugic_baseaddr; /* valid only for Zynq */
	unsigned scugic_emac_intr; /* valid only for GEM */
};

extern int xtopology_n_emacs;
extern struct xtopology_t xtopology[];

extern int xtopology_find_index(unsigned base);

#endif /* XTOPOLOGY_H_ */
