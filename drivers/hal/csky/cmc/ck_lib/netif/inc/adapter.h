/*******************************************************************
*文件:     adapter.h
*作者:     朱林
*日期:     2018-9-13
*说明:     CMC mac适配层，用于配置mac信息的具体参数,其中信息pMacInformation_st结构体
*         中，无需配置pRecv_q和pSend_q,这两项在ethernetif_init中配置
*修改记录:
********************************************************************/

#ifndef __ADAPTER_H_
#define __ADAPTER_H_

#include "lwipopts.h"



#include "lwip/netif.h"
#include "netif/xtopology.h"
#include "dmac.h"

extern UINT32 SYS_FREQ;
extern UINT32 AHB_FREQ;
extern UINT32 APB_FREQ;

/* 定义TX描述符和RX描述符 */
volatile Desp_st mac0TxDesp[MAC_NUM_TX_DESP];
volatile Desp_st mac0RxDesp[MAC_NUM_RX_DESP];
volatile Desp_st mac1TxDesp[MAC_NUM_TX_DESP];
volatile Desp_st mac1RxDesp[MAC_NUM_RX_DESP];

extern volatile pDesp_st MACTxDesp[];
extern volatile pDesp_st MACRxDesp[];

struct xemac_s {
	enum xemac_types type;
	int  topology_index;
	void *state;
#if !NO_SYS && (NO_SYS == 2)
    sys_sem_t sem_rx_data_available;
#endif
};
struct os_ck_eth;
/*
 * 功能：根据MAC寄存器地址配置相应的MAC信息
 * 参数：wdMacAddr,MAC寄存器起始地址
 * 返回值：返回配置后的MAC信息
 */
//pMacInformation_st MAC_Config(struct netif *netif);



extern pMacInformation_st MAC_Config(struct os_ck_eth *ck_eth);

#endif /* ADAPTER_H_ */
