/*******************************************************************
*文件:     dmac.h
*作者:     朱林
*日期:     2018-9-13
*说明:     mac驱动
*修改记录:
********************************************************************/

#ifndef __DMAC_H_
#define __DMAC_H_

#include <string.h>
#include "datatype.h"
#include "netif/xpqueue.h"
#include "lwip/sys.h"
#include "lwip/pbuf.h"

/*******************************MAC寄存器起始地址定义********************/
#define MAC0_BASE_ADDR              0x10008000    /* MAC0控制器寄存器起始地址 */
#define MAC1_BASE_ADDR              0x1000E000    /* MAC1控制器寄存器起始地址 */
#define MAC0_DMA_BASE_ADDR          0x10009000    /* MAC0_DMA寄存器起始地址 */
#define MAC1_DMA_BASE_ADDR          0x1000F000    /* MAC1_DMA寄存器起始地址 */

#define MAC_DMA_ADDR_OFFSET         0x00001000    /* DMA寄存器的地址偏移 */
/*******************************end**********************************/

/*******************************MAC寄存器位定义*************************/
/* MCR寄存器位定义 */
#define MAC_MCR_TE                   0x00000008   /* MAC发送使能位 */
#define MAC_MCR_RE                   0x00000004   /* MAC接收使能位 */

/* GAR寄存器位定义 */
#define MAC_GAR_BUSY                 0x00000001   /* GMII Busy位，表示一个读或者写操作正在处理 */
#define MAC_GAR_WRITE                0x00000002   /* GMII Write位，置位表示写寄存器，复位表示读寄存器 */
#define MAC_GAR_PHY_ADDR             0x0000F800   /* PHY物理地址位 */
#define MAC_GAR_REG_ADDR             0x000007C0   /* PHY寄存器地址位 */
/*******************************end**********************************/

/*******************************DMA寄存器位定义 *************************/
/* BMR寄存器位定义 */
#define MAC_DMA_BMR_RST              0x00000001   /* 软件复位，置1开始复位，复位完成自动清零 */
#define MAC_DMA_BMR_TP               0x08000000   /* 传输优先级位 ，置1指示传输DMA比接收DMA优先级高 */

/* IER寄存器位定义 */
#define  MAC_DMA_IER_NIE             0x00010000   /* Normal Interrupt Summary Enable */
#define  MAC_DMA_IER_AIE             0x00008000   /* Abnormal Interrupt Summary Enable */
#define  MAC_DMA_IER_ERE             0x00004000   /* Early Receive Interrupt Enable */
#define  MAC_DMA_IER_FBE             0x00002000   /* Fatal Bus Error Enable */
#define  MAC_DMA_IER_EIE             0x00000400   /* Early Transmit Interrupt Enable */
#define  MAC_DMA_IER_RWE             0x00000200   /* Receive Watchdog Timeout Enable */
#define  MAC_DMA_IER_RSE             0x00000100   /* Receive Stopped Enable */
#define  MAC_DMA_IER_RU              0x00000080   /* Receive Buffer Unavailable Enable */
#define  MAC_DMA_IER_RIE             0x00000040   /* Receive Interrupt Enable */
#define  MAC_DMA_IER_UNE             0x00000020   /* Underflow Interrupt Enable */
#define  MAC_DMA_IER_OVE             0x00000010   /* Overflow Interrupt Enable */
#define  MAC_DMA_IER_TJE             0x00000008   /* Transmit Jabber Timeout Enable */
#define  MAC_DMA_IER_TUE             0x00000004   /* Transmit Buffer Unavailable Enable */
#define  MAC_DMA_IER_TSE             0x00000002   /* Transmit Stopped Enable */
#define  MAC_DMA_IER_TIE             0x00000001   /* Transmit Interrupt Enable */

/* OMR寄存器位定义 */
#define  MAC_DMA_OMR_SR              0x00000002   /* Start or Stop Receive */
#define  MAC_DMA_OMR_ST              0x00002000   /* Start or Stop Transmission Command */
#define  MAC_DMA_OMR_TTC             0x0000c000   /* transmit threshold control,此值代表256字节筏值 */
#define  MAC_DMA_OMR_TSF             0x00200000   /* transmit store and forward */
#define  MAC_DMA_OMR_RSF             0x02000000   /* receive store and forward */
/* SR寄存器位定义 */
#define  MAC_DMA_SR_NIS              0x00010000   /* Normal Interrupt Summary */
#define  MAC_DMA_SR_AIS              0x00008000   /* Abnormal Interrupt Summary */
#define  MAC_DMA_SR_ERI              0x00004000   /* Early Receive Interrupt */
#define  MAC_DMA_SR_FBI              0x00002000   /* fatal bus err */
#define  MAC_DMA_SR_ETI              0x00000400   /* Early Transmit Interrupt */
#define  MAC_DMA_SR_RWT              0x00000200   /* Receive Watchdog Timeout */
#define  MAC_DMA_SR_RPS              0x00000100   /* Receive Process Stopped */
#define  MAC_DMA_SR_RU               0x00000080   /* receive buffer unavailable */
#define  MAC_DMA_SR_RI               0x00000040   /* Receive Interrupt */
#define  MAC_DMA_SR_UNF              0x00000020   /* Transmit Underflow */
#define  MAC_DMA_SR_OVE              0x00000010   /* Receive Overflow */
#define  MAC_DMA_SR_TJT              0x00000008   /* Transmit Jabber Timeout */
#define  MAC_DMA_SR_TU               0x00000004   /* Transmit Buffer Unavailable */
#define  MAC_DMA_SR_TPS              0x00000002   /* Transmit Process Stopped */
#define  MAC_DMA_SR_TI               0x00000001   /* Transmit Interrupt */
/****************************end*************************************/

/****************************描述符位定义*******************************/
#define RX_RDES0_OWN                 0x80000000   /* 接收描述符拥有权标志位，1=由dma占有，0=由host占有 */
#define RX_RDES0_ES                  0x00008000   /* Error Summary */
#define RX_RDES0_FS                  0x00000200   /* first segment */
#define RX_RDES0_LS                  0x00000100   /* last segment */
#define RX_RDES0_LEN                 0x3FFF0000   /* 帧长度位，包括CRC */

#define RX_RDES1_RER                 0x02000000   /* RER位，指示接收描述符列表到达最后一个描述符 */
#define RX_RDES1_RCH                 0x01000000   /* RCH位，置位表示接收描述符第二个地址是下一个描述符的地址而不是第二个buffer的地址 */

#define TX_TDES0_OWN                 0x80000000   /* 发送描述符拥有权标志位，1=由dma占有，0=由host占有 */

#define TX_TDES1_IC                  0x80000000   /* 帧发送完成标志位 */
#define TX_TDES1_LS                  0x40000000   /* last segment */
#define TX_TDES1_FS                  0x20000000   /* first segment */
#define TX_TDES1_CIC                 0x18000000   /* 校验和插入控制，这里选择插入IPv4 header checksum */
#define TX_TDES1_TER                 0x02000000   /* TER位，指示接收描述符列表到达最后一个描述符 */
#define TX_TDES1_TCH                 0x01000000   /* TCH位，置位表示发送描述符第二个地址是下一个描述符的地址而不是第二个buffer的地址 */
#define TX_TDES1_DP                  0x00800000   /* DP位，置位表示禁能自动填充 */
/****************************end*************************************/

/* 定义描述符的数量 */
#define MAC_NUM_RX_DESP                  (4)
#define MAC_NUM_TX_DESP                  (4)

/* 定义RX的buffer大小，及buffer数量（与接收描述符的个数相同）*/
#define MAC_NUM_BUFFERS_RX               MAC_NUM_RX_DESP
#define MAC_BUFFER_SIZE                  ((UINT16)(1536))

/* 定义MAC的事件标志组的位号 */
#if !NO_SYS
#define MAC0_EVENT_FLAG    0x01       /* MAC0的事件标志组位号为bit0 */
#define MAC1_EVENT_FLAG    0x02       /* MAC0的事件标志组位号为bit1 */
extern UINT16 wMACEventFlags[];
#endif

/* MAC索引，用于确定当前MAC的信息 */
#define MAC0_INDEX         0
#define MAC1_INDEX         1


/* MAC寄存器结构体 */
/* 该结构各个成员的含义在文档《DesignWare Cores Ethernet MAC Universal Databook》 第6章 */
typedef struct
{
	REG	MAC_MCR;  	                  /* 寄存器0，相对地址0x0 */
	REG	MAC_MFF;  	                  /* 寄存器1，相对地址0x4 */
	REG	MAC_HTH;  	                  /* 寄存器2，相对地址0x8 */
	REG	MAC_HTL;  	                  /* 寄存器3，相对地址0xc */
	REG	MAC_GAR;  	                  /* 寄存器4，相对地址0x10 */
	REG	MAC_GDR;  	                  /* 寄存器5，相对地址0x14 */
	REG	MAC_FCR;  	                  /* 寄存器6，相对地址0x18 */
	REG	MAC_VTR;  	                  /* 寄存器7，相对地址0x1c */
	REG	Reserve_R1;     	          /* 保留 */
	REG	MAC_DBR;  	                  /* 寄存器9，相对地址0x24 */
    REG Reserve_R2[4];                /* 保留 */
	REG	MAC_ISR;  	                  /* 寄存器14，相对地址0x38 */
	REG	MAC_IMR;                      /* 寄存器15，相对地址0x3c */
	REG	MAC_MAH0;	                  /* 寄存器16，相对地址0x40 */
	REG	MAC_MAL0;	                  /* 寄存器17，相对地址0x44 */
	REG Reserve_R3[36];               /* 保留 */
	REG MAC_MIICSR;                   /* 寄存器54，相对地址0xd8 */
	REG WTR;                          /* 寄存器55，相对地址0xdc */
}Mac_st,*pMac_st;

/* GMAC DMA寄存器结构体 */
/* 该结构各个成员的含义在文档《DesignWare Cores Ethernet MAC Universal Databook》 第6章 */
typedef struct
{
	REG	MAC_DMA_BMR;                  /* BMR寄存器，相对地址0x1000 */
	REG	MAC_DMA_TPDR;                 /* TPDR寄存器，相对地址0x1004 */
	REG	MAC_DMA_RPDR;                 /* RPDR寄存器，相对地址0x1008 */
	REG	MAC_DMA_RDLA;                 /* RDLA寄存器，相对地址0x100c */
	REG	MAC_DMA_TDLA;                 /* TDLA寄存器，相对地址0x1010 */
	REG	MAC_DMA_SR;                   /* SR寄存器，相对地址0x1014 */
	REG	MAC_DMA_OMR;                  /* OMR寄存器，相对地址0x1018 */
	REG	MAC_DMA_IER;                  /* IER寄存器，相对地址0x101c */
	REG	MAC_DMA_MFR;                  /* MFR寄存器，相对地址0x1020 */
	REG	MAC_DMA_RIWT;                 /* RIWT寄存器，相对地址0x1024 */
	REG	Reserve_R1;                   /* 保留 */
	REG	MAC_DMA_ASR;                  /* ASR寄存器，相对地址0x102c */
	REG	Reserve_R2[6];                /* 保留 */
	REG	MAC_DMA_CHTD;                 /* CHTD寄存器，相对地址0x1048 */
	REG	MAC_DMA_CHRD;                 /* CHRD寄存器，相对地址0x104c */
	REG	MAC_DMA_CHTB;                 /* CHTB寄存器，相对地址0x1050 */
	REG	MAC_DMA_CHRB;                 /* CHRB寄存器，相对地址0x1054 */
} MAC_DMA_st, *pMAC_DMA_st;

/* 发送、接收描述符结构定义 */
/* 该结构各个成员的含义在文档《DesignWare Cores Ethernet MAC Universal Databook》 第8章 */
typedef struct
{
    REG DES0;                          /* DES0 */
    REG DES1;                          /* DES1 */
    REG BUFF1_ADDR;                    /* DES2 */
    REG BUFF2_ADDR;                    /* DES3 */
}Desp_st,*pDesp_st;

/* MAC实例数据结构体 */
typedef struct
{
    UINT16 wMacID;                     /* MAC唯一的ID */
    pMac_st pMacAddr;                  /* MAC的寄存器起始地址 */
    pMAC_DMA_st pMacDMAAddr;           /* MAC_DMA寄存器起始地址 */
    UINT8 cHardAddr[6];                /* MAC地址 */

    UINT8 dwPhyAddr;                   /* PHY物理地址 */
    UINT16 wPhySpeed;                  /* PHY连接速度，自协商时根据自协商的结果进行赋值  */

    UINT8 cTxDespNum;                  /* 发送描述符个数 */
    UINT8 cRxDespNum;                  /* 接收描述符个数 ，亦即接收buffer的个数 */
    UINT16 wRxBufSize;                 /* 单个接收buffer的大小 */
    void *pTxDespBase;                 /* 发送描述符结构起始地址 */
    void *pRxDespBase;                 /* 接收描述符结构起始地址 */
    pDesp_st pCurRxDesp;               /* 指向当前接收描述符位置 */
    pDesp_st pCurTxDesp;               /* 指向当前发送描述符位置 */

    pq_queue_t *pRecv_q;               /* 接收队列 */
    pq_queue_t *pSend_q;               /* 发送队列 */

    UINT8 cRxPbufSeq;                  /* 存放有效接收数据的pbuf的数组序号，根据此序号从数组中取得pbuf */
    UINT8 cRxPbufNum;                  /* 存放有效接收数据的pbuf的数组个数，此个数不能大于描述符个数 */
    UINT8 cTxPbufSeq;                  /* 存放发送数据的Pbuf的数组序号，根据此序号从数组中取得发送的Pbuf */
    UINT8 cTxPbufNum;                  /* 存放发送数据的Pbuf的数组个数，此个数不能大于描述符个数 */
    UINT8 cTxPbufHead;
    UINT32 dwRxPbufArr[MAC_NUM_RX_DESP];
    UINT32 dwTxPbufArr[MAC_NUM_TX_DESP];
    UINT32 dwTxDespArr[MAC_NUM_TX_DESP];

#if !NO_SYS
    //sys_sem_t pSemRxDataAvailable;     /* MAC接收到可用帧的信号量 */
    UINT16 wMacEventFlag;                   /* 当前MAC的事件标志组位号 */
#endif
}MacInformation_st,*pMacInformation_st;

MacInformation_st MAC0;                /* MAC0初始化配置信息全局变量 */
MacInformation_st MAC1;                /* MAC1初始化配置信息全局变量 */

/*
* 功能：MAC模块使能
* 参数：pMacInst,mac实例指针
* 返回值：
*/
UINT8 MAC_Enable(pMacInformation_st pMacInst);

/*
* 功能：MAC模块禁能
* 参数：pMacInst,mac实例指针
* 返回值：
*/
UINT8 MAC_Disable(pMacInformation_st pMacInst);

/*
* 功能：cmc MAC初始化
* 参数：pMacInst,mac信息对象
* 返回值：0,MAC初始化成功
*       1,MAC初始化失败
*/
UINT8 MAC_Init(pMacInformation_st pMacInst);

/*
* 功能：cmc 指定MAC通过SMI写指定PHY寄存器
* 参数：pMac,当前mac寄存器结构体指针
* 返回值：0,写寄存器成功
*/
UINT8 MAC_WritePhyReg(pMac_st pMac, UINT8 cPhyAddr, UINT8 cPhyReg, UINT16 wRegData);

/*
* 功能：cmc 指定MAC通过SMI读指定PHY寄存器
* 参数：pMac,当前mac寄存器结构体指针
* 返回值：0,写寄存器成功
*/
UINT8 MAC_ReadPhyReg(pMac_st pMac, UINT8 cPhyAddr, UINT8 cPhyReg, PUINT16 pRegData);

/*
* 功能：cmc 指定MAC通过SMI读指定PHY寄存器
* 参数：pMac,当前mac寄存器结构体指针
* 返回值：0,写寄存器成功
*/
UINT8 MAC_SendFrame(pMacInformation_st pMacInst, struct pbuf *pBuf);

/*
* 功能：cmc 清除并释放已保存的所有接收pbuf，目的是在重新初始化MAC时，需要重新分配接收
*      pbuf则之前已分配的pbuf需要释放掉，否则会造成内存泄漏，一般在重新初始化MAC前调用。
*      第一次初始化MAC时不需要调用此函数。
* 参数：pMacInst,mac信息对象
* 返回值：0,保存成功；1，保存失败
*/
UINT8 MAC_Clear_Rxpbuf(pMacInformation_st pMacInst);

/*
* 功能：cmc MAC0中断回调函数
* 参数：
* 返回值：
*/
void MAC0_ISRCallback(UINT32 dwIrqId);

/*
* 功能：cmc MAC1中断回调函数
* 参数：
* 返回值：
*/
void MAC1_ISRCallback(UINT32 dwIrqId);

void MAC_RecvHandler(pMacInformation_st pMacInst);


#endif /* DMAC_H_ */
