/*******************************************************************
*文件:     phy.h
*作者:     朱林
*日期:     2018-9-13
*说明:     phy芯片驱动程序
*修改记录:
********************************************************************/

#ifndef __PHY_H_
#define __PHY_H_

#include "datatype.h"
#include "dmac.h"

#define PHY0_DEVICE_ADDR    (0x00)            /* PHY0物理地址*/
#define PHY1_DEVICE_ADDR    (0x01)            /* PHY1物理地址 */

extern UINT8 cPhyAddr[];

/*
* 功能：根据phy identifer选择当前型号的phy进行初始化
* 参数：pMacInst,mac实例指针
* 返回值：0x00：phy初始化成功
*       0x01：phy复位超时
*       0x02：读phy id失败或无匹配id
*       0x04：自协商超时
*       0x08：link建立超时
*/
INT32S PHY_Init(pMacInformation_st pMacInst);
UINT8 PHY_GetLinkStatus(pMacInformation_st pMacInst);

#endif /* PHY_H_ */
