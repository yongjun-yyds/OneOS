/*******************************************************************
*文件:     adapter.c
*作者:     朱林
*日期:     2018-9-13
*说明:     CMC mac适配层，用于配置mac信息的具体参数,其中MAC信息
*         中，无需配置pRecv_q和pSend_q,这两项在ethernetif_init中配置
*修改记录:
********************************************************************/
#include "netif/dmac.h"
#include "intc.h"
#include "datatype.h"
#include "netif/phy.h"
#include "netif/adapter.h"
#include "ckcore.h"
#include "netif/xtopology.h"
//#include "netif/gmacif.h"
#include "netif/etharp.h"
#include "lwip/netif.h"
#include "lwip/tcpip.h"
//#include "xlwipconfig.h"
#include <drv_eth.h>
volatile pDesp_st MACTxDesp[] = {(pDesp_st)mac0TxDesp, (pDesp_st)mac1TxDesp};
volatile pDesp_st MACRxDesp[] = {(pDesp_st)mac0RxDesp, (pDesp_st)mac1RxDesp};

static Struct_IRQHandler MAC0IrqInfo;    /* MAC0普通中断信息结构体，用于注册到normal_irq_list列表中 ，add zhulin*/
static Struct_IRQHandler MAC1IrqInfo;    /* MAC1普通中断信息结构体，用于注册到normal_irq_list列表中 ，add zhulin*/
static pStruct_IRQHandler MACIrqInfo[] = {&MAC0IrqInfo, &MAC1IrqInfo};

extern void MAC0_ISRCallback(UINT32 dwIrqId);
extern void MAC1_ISRCallback(UINT32 dwIrqId);
void (* MAC_ISRCallback[])(UINT32) = {
		MAC0_ISRCallback,
		MAC1_ISRCallback
};

UINT32 INTC_MAC[] = {CK_INTC_MAC0, CK_INTC_MAC1};

static enum xemac_types find_mac_type(unsigned base)
{
	INT32S i;

	for (i = 0; i < xtopology_n_emacs; i++) {
		if (xtopology[i].emac_baseaddr == base)
			return xtopology[i].emac_type;
	}

	return xemac_type_unknown;
}

INT32S xtopology_find_index(unsigned base)
{
	INT32S i;

	for (i = 0; i < xtopology_n_emacs; i++) {
		if (xtopology[i].emac_baseaddr == base)
			return i;
	}

	return -1;
}

/*******************************************************************
* 功能：根据MAC寄存器地址配置相应的MAC信息
* 参数：wdMacAddr,MAC寄存器起始地址
* 返回值：返回配置后的MAC信息
********************************************************************/
pMacInformation_st MAC_Config(struct os_ck_eth *ck_eth)
{
    UINT8 i;
    UINT32 dwIndex;
    pMacInformation_st pMAC;

    if(MAC0_BASE_ADDR == xtopology[ck_eth->xemac.topology_index].emac_baseaddr)                                 /* MAC0控制器 */
    {
        dwIndex = MAC0_INDEX;
        pMAC = &MAC0;                                               /* MAC0是初始化后的全局变量 */
    }
    else if(MAC1_BASE_ADDR == xtopology[ck_eth->xemac.topology_index].emac_baseaddr)                            /* MAC1控制器 */
    {
        dwIndex = MAC1_INDEX;
        pMAC = &MAC1;                                               /* MAC1是初始化后的全局变量 */
    }
    else
    {
        return NULL;
    }

    pMAC->pMacAddr = (void *)xtopology[ck_eth->xemac.topology_index].emac_baseaddr;
    pMAC->pMacDMAAddr = (void *)(xtopology[ck_eth->xemac.topology_index].emac_baseaddr+ MAC_DMA_ADDR_OFFSET);
    for(i = 0; i < 6; i++)
    {
        pMAC->cHardAddr[i] =  ck_eth->dev_addr[i];
    }

#if !NO_SYS && (NO_SYS ==2)
    //pMAC->wMacEventFlag = wMACEventFlags[dwIndex];
#endif
    pMAC->dwPhyAddr = cPhyAddr[dwIndex];
    pMAC->cTxDespNum = MAC_NUM_TX_DESP;
    pMAC->cRxDespNum = MAC_NUM_RX_DESP;
    pMAC->wRxBufSize = MAC_BUFFER_SIZE;
    pMAC->cTxPbufHead = 0;
    pMAC->cTxPbufNum = 0;
    pMAC->cTxPbufSeq = 0;
    pMAC->cRxPbufNum = 0;
    pMAC->cRxPbufSeq = 0;
    pMAC->pTxDespBase = (void *)MACTxDesp[dwIndex];
    pMAC->pRxDespBase = (void *)MACRxDesp[dwIndex];
    pMAC->pCurRxDesp = (void *)MACRxDesp[dwIndex];
    pMAC->pCurTxDesp = (void *)MACTxDesp[dwIndex];

    /* 打开中断 */
    MACIrqInfo[dwIndex]->devname = "MAC";
    MACIrqInfo[dwIndex]->irqid = INTC_MAC[dwIndex];
    MACIrqInfo[dwIndex]->priority = INTC_MAC[dwIndex];
    MACIrqInfo[dwIndex]->handler = MAC_ISRCallback[dwIndex];
    MACIrqInfo[dwIndex]->bfast = FALSE;
    MACIrqInfo[dwIndex]->next = NULL;
    INTC_RequestIrq(MACIrqInfo[dwIndex]);

    return pMAC;
}


/* pMacInformation_st MAC_Config(struct netif *netif)
{
	UINT8 i;
	UINT32 dwIndex, dwMacAddr;
	pMacInformation_st pMAC;

	dwMacAddr = (UINT32)netif->state;

    if(MAC0_BASE_ADDR == dwMacAddr)                           
    {
    	dwIndex = MAC0_INDEX;
    	pMAC = &MAC0;                                          
    }
    else if(MAC1_BASE_ADDR == dwMacAddr)                       
    {
    	dwIndex = MAC1_INDEX;
    	pMAC = &MAC1;                                      
    }
    else
    {
    	return NULL;
    }

    pMAC->pMacAddr = (void *)dwMacAddr;
    pMAC->pMacDMAAddr = (void *)(dwMacAddr+ MAC_DMA_ADDR_OFFSET);
    for(i = 0; i < 6; i++)
    {
    	pMAC->cHardAddr[i] =  netif->hwaddr[i];
    }

#if !NO_SYS
    pMAC->wMacEventFlag = wMACEventFlags[dwIndex];
#endif
    pMAC->dwPhyAddr = cPhyAddr[dwIndex];
    pMAC->cTxDespNum = MAC_NUM_TX_DESP;
    pMAC->cRxDespNum = MAC_NUM_RX_DESP;
    pMAC->wRxBufSize = MAC_BUFFER_SIZE;
    pMAC->cTxPbufHead = 0;
    pMAC->cTxPbufNum = 0;
    pMAC->cTxPbufSeq = 0;
    pMAC->cRxPbufNum = 0;
    pMAC->cRxPbufSeq = 0;
    pMAC->pTxDespBase = (void *)MACTxDesp[dwIndex];
    pMAC->pRxDespBase = (void *)MACRxDesp[dwIndex];
    pMAC->pCurRxDesp = (void *)MACRxDesp[dwIndex];
    pMAC->pCurTxDesp = (void *)MACTxDesp[dwIndex];

    // open irq 
    MACIrqInfo[dwIndex]->devname = "MAC";
    MACIrqInfo[dwIndex]->irqid = INTC_MAC[dwIndex];
    MACIrqInfo[dwIndex]->priority = INTC_MAC[dwIndex];
    MACIrqInfo[dwIndex]->handler = MAC_ISRCallback[dwIndex];
    MACIrqInfo[dwIndex]->bfast = FALSE;
    MACIrqInfo[dwIndex]->next = NULL;
    INTC_RequestIrq(MACIrqInfo[dwIndex]);

	return pMAC;
} */

/*
 * 功能：netif_add函数的封装，用于根据给定的MAC地址调用相应的
 *     netif_add函数进行初始化。
 * 参数：netif，网络接口
 *     ipaddr，ip地址
 *     netmask，掩码
 *     gw，网关
 *     mac_ethernet_address，MAC物理地址
 *     mac_baseaddr，MAC寄存器地址
 * 返回值：返回配置后的MAC信息
 */
struct netif *
xemac_add(struct netif *netif,
	struct ip_addr *ipaddr, struct ip_addr *netmask, struct ip_addr *gw,
	unsigned char *mac_ethernet_address,
	unsigned mac_baseaddr)
{
	int i;

	/* set mac address */
	netif->hwaddr_len = 6;
	for (i = 0; i < 6; i++)
		netif->hwaddr[i] = mac_ethernet_address[i];

	/* initialize based on MAC type */
		switch (find_mac_type(mac_baseaddr)) {
			case xemac_type_xps_emaclite:
#ifdef XLWIP_CONFIG_INCLUDE_EMACLITE
				return netif_add(netif, ipaddr, netmask, gw,
					(void*)mac_baseaddr,
					xemacliteif_init,
#if NO_SYS && (NO_SYS ==2)
					ethernet_input
#else
					tcpip_input
#endif
					);
#else
				return NULL;
#endif
			case xemac_type_axi_ethernet:
#ifdef XLWIP_CONFIG_INCLUDE_AXI_ETHERNET
				return netif_add(netif, ipaddr, netmask, gw,
					(void*)mac_baseaddr,
					xaxiemacif_init,
#if NO_SYS && (NO_SYS ==2)
					ethernet_input
#else
					tcpip_input
#endif
					);
#else
				return NULL;
#endif
			case xemac_type_emacps:
#ifdef XLWIP_CONFIG_INCLUDE_GEM
				return netif_add(netif, ipaddr, netmask, gw,
						(void*)mac_baseaddr,
						xemacpsif_init,
#if NO_SYS && (NO_SYS ==2)
						ethernet_input
#else
						tcpip_input
#endif
						);
#else
				return NULL;
#endif
			case xemac_type_gmac:
/*
#ifdef XLWIP_CONFIG_INCLUDE_GMAC
				return netif_add(netif, ipaddr, netmask, gw,
						(void*)mac_baseaddr,
						gmacif_init,
#if NO_SYS
						ethernet_input
#else
						tcpip_input
#endif
						);
#else
				return NULL;
#endif
*/
			default:

				return NULL;
	}
}
