/*******************************************************************
*文件:     dmac.c
*作者:     朱林
*日期:     2018-9-13
*说明:     CMC mac及dma驱动程序，接收pbuf需要指定到非cache内存中，或者关闭Dcache
*修改记录:
********************************************************************/
#include "netif/dmac.h"
#include "netif/adapter.h"
#include "netif/phy.h"
#include "lwip/stats.h"

#if !NO_SYS
UINT16 wMACEventFlags[] = {MAC0_EVENT_FLAG, MAC1_EVENT_FLAG};
#endif

static UINT8 MAC_TxDesp_Init(pMacInformation_st pMacInst);
static UINT8 MAC_RxDesp_Init(pMacInformation_st pMacInst);
static UINT16 MAC_GetRxFrameLength(pMacInformation_st pMacInst);
static void MAC_SetupRxDesp(pMacInformation_st pMacInst);

/*******************************************************************
* 功能：cmc 指定MAC通过SMI写指定PHY寄存器
* 参数：pMac,当前mac寄存器结构体指针
* 返回值：0,写寄存器成功
********************************************************************/
UINT8 MAC_WritePhyReg(pMac_st pMac, UINT8 cPhyAddr, UINT8 cPhyReg, UINT16 wRegData)
{
	unsigned int temp;

    while((pMac->MAC_GAR & MAC_GAR_BUSY) != 0)                                    /* 等待PHY寄存器读写状态至空闲状态 */
    {
    	asm("nop");
    }

    temp = pMac->MAC_GAR;
    temp &= (~MAC_GAR_PHY_ADDR);                                                  /* 对PHY物理地址清零 */
    temp |= (cPhyAddr << 11);                                                     /* 写入PHY物理地址 */
    temp &= (~MAC_GAR_REG_ADDR);                                                  /* 对PHY寄存器地址清零 */
    temp |= cPhyReg << 6;                                                         /* 设置PHY寄存器地址 */
    temp |= MAC_GAR_WRITE;                                                        /* 写PHY寄存器操作使能 */

    pMac->MAC_GDR = wRegData;                                                     /* 对PHY寄存器的数据位赋值 */

    pMac->MAC_GAR = temp;

    pMac->MAC_GAR |= MAC_GAR_BUSY;                                                /* PHY寄存器处于忙状态 */

    while((pMac->MAC_GAR & MAC_GAR_BUSY) != 0)                                    /* 等待PHY寄存器写操作完成 */
    {
    	asm("nop");
    }

    return 0;
}

/*******************************************************************
* 功能：cmc 指定MAC通过SMI读指定PHY寄存器
* 参数：pMac,当前mac寄存器结构体指针
* 返回值：0,写寄存器成功
********************************************************************/
UINT8 MAC_ReadPhyReg(pMac_st pMac, UINT8 cPhyAddr, UINT8 cPhyReg, PUINT16 pRegData)
{
    while((pMac->MAC_GAR & MAC_GAR_BUSY) != 0)                                    /* 等待PHY寄存器读写状态至空闲状态 */
    {
    	asm("nop");
    }

    pMac->MAC_GAR &= (~MAC_GAR_PHY_ADDR);                                         /* 对PHY物理地址清零 */
    pMac->MAC_GAR |= cPhyAddr << 11;                                              /* 设置PHY物理地址 */
    pMac->MAC_GAR &= (~MAC_GAR_REG_ADDR);                                         /* 对PHY寄存器地址清零 */
    pMac->MAC_GAR |= cPhyReg << 6;                                                /* 设置PHY寄存器地址 */
    pMac->MAC_GAR &= (~MAC_GAR_WRITE);                                            /* 读PHY寄存器操作使能 */
    pMac->MAC_GAR |= MAC_GAR_BUSY;                                                /* PHY寄存器处于忙状态 */

    while((pMac->MAC_GAR & MAC_GAR_BUSY) != 0)                                    /* 等待PHY寄存器读操作完成 */
    {
    	asm("nop");
    }

    *pRegData = (UINT16)pMac->MAC_GDR;                                            /* 读取指定PHY寄存器的值 */

    return 0;
}

/*******************************************************************
* 功能：cmc 根据系统时钟范围，选择分频因子，确定MDC时钟
* 参数：pMac,当前mac寄存器结构体指针
* 返回值：0,配置时钟成功
*       1,配置时钟失败
********************************************************************/
static UINT8 MAC_ConfigCSRClk(pMac_st pMac)
{
	if ( AHB_FREQ < 35000000 )
	{
		pMac->MAC_GAR = 0x0008;                                               /* 对clk_csr_is时钟16分频 */
	}
	else if ((AHB_FREQ >= 35000000) && (AHB_FREQ < 60000000))
	{
		pMac->MAC_GAR = 0x000C;                                               /* 对clk_csr_is时钟26分频 */
	}
	else if ((AHB_FREQ >= 60000000) && (AHB_FREQ < 100000000))
	{
		pMac->MAC_GAR = 0x0000;                                               /* 对clk_csr_is时钟16分频 */
	}
	else
	{
		return 1;
	}

	return 0;
}

/*******************************************************************
* 功能：设置MAC地址到MAC寄存器中
* 参数：pMacInst,mac实例指针
* 返回值：0,配置成功
*       1,配置失败
********************************************************************/
static UINT8 MAC_SetHardAddr0(pMacInformation_st pMacInst)
{
    pMacInst->pMacAddr->MAC_MAH0 = (pMacInst->cHardAddr[5]<<8) | (pMacInst->cHardAddr[4]);
    pMacInst->pMacAddr->MAC_MAL0 = (pMacInst->cHardAddr[3]<<24) | (pMacInst->cHardAddr[2]<<16)
    		                       | (pMacInst->cHardAddr[1]<<8) | (pMacInst->cHardAddr[0]);

    return 0;
}

/*******************************************************************
* 功能：MAC模块使能
* 参数：pMacInst,mac实例指针
* 返回值：
********************************************************************/
UINT8 MAC_Enable(pMacInformation_st pMacInst)
{
	pMacInst->pMacDMAAddr->MAC_DMA_SR = 0xFFFFFFFF;

	pMacInst->pMacDMAAddr->MAC_DMA_IER = MAC_DMA_IER_FBE | MAC_DMA_IER_RU | MAC_DMA_IER_AIE
								 | MAC_DMA_IER_NIE | MAC_DMA_IER_OVE | MAC_DMA_IER_RIE
								 | MAC_DMA_IER_UNE | MAC_DMA_IER_TIE;

	pMacInst->pMacAddr->MAC_MCR |= (MAC_MCR_TE | MAC_MCR_RE);                /* 使能MII发送和接收  */

	pMacInst->pMacDMAAddr->MAC_DMA_OMR |= (MAC_DMA_OMR_SR | MAC_DMA_OMR_ST); /* 开始发送和接收  */

	return 0;
}

/*******************************************************************
* 功能：MAC模块禁能
* 参数：pMacInst,mac实例指针
* 返回值：
********************************************************************/
UINT8 MAC_Disable(pMacInformation_st pMacInst)
{
	pMacInst->pMacDMAAddr->MAC_DMA_SR = 0xFFFFFFFF;
	pMacInst->pMacDMAAddr->MAC_DMA_IER = 0x00;

    /* 禁能发送和接收 */
	pMacInst->pMacAddr->MAC_MCR &= ~(MAC_MCR_TE | MAC_MCR_RE);
	pMacInst->pMacDMAAddr->MAC_DMA_OMR &= ~(MAC_DMA_OMR_SR | MAC_DMA_OMR_ST);

    return 0;
}

/*******************************************************************
* 功能：cmc MAC初始化
* 参数：pMacInst,mac信息对象
* 返回值：0,MAC初始化成功
*       1,MAC初始化失败
********************************************************************/
UINT8 MAC_Init(pMacInformation_st pMacInst)
{
	INT32S nWait = AHB_FREQ/5;

	pMacInst->pMacDMAAddr->MAC_DMA_BMR = MAC_DMA_BMR_RST;                            /* 复位MAC DMA 控制器 */
	while(pMacInst->pMacDMAAddr->MAC_DMA_BMR & MAC_DMA_BMR_RST)
	{
		if(--nWait <= 0 )
		{
			return 1;                                                                /* 复位MAC DMA 超时 */
		}
	}

	if(0 != MAC_ConfigCSRClk(pMacInst->pMacAddr))                                    /* 设置MDC时钟 */
	{
		return 1;                                                                    /* MDC时钟配置失败 */
	}

    pMacInst->pMacDMAAddr->MAC_DMA_BMR |= MAC_DMA_BMR_TP;                            /* 设置DMA传输优先级比接收优先级高 */

    MAC_TxDesp_Init(pMacInst);
    MAC_RxDesp_Init(pMacInst);

	MAC_SetHardAddr0(pMacInst);                                                      /* 设置MAC地址到MAC地址寄存器0中 */

    pMacInst->pMacDMAAddr->MAC_DMA_SR = 0xffffffff;                                  /* 清中断标志 */

    pMacInst->pMacDMAAddr->MAC_DMA_IER = MAC_DMA_IER_FBE | MAC_DMA_IER_RU            /* 使能中断 */
    		                             | MAC_DMA_IER_AIE | MAC_DMA_IER_NIE
    		                             | MAC_DMA_IER_OVE | MAC_DMA_IER_RIE
    									 | MAC_DMA_IER_UNE | MAC_DMA_IER_TIE;

	//pMacInst->pMacAddr->MAC_MFF = 0x80000000;                                        /* 设置MAC接收所有帧 */

	if(PHY_Init(pMacInst) != 0)
	{
		return 1;                                                                    /* PHY初始化失败 */
	}

	if(pMacInst->wPhySpeed == 10)                                                    /* 根据PHY成功连接后的实际速度设置MAC */
	{
        pMacInst->pMacAddr->MAC_MCR = 0x00880;                                       /* 10M/全双工/接收帧删除pad和crc */
	}
	else if(pMacInst->wPhySpeed == 100)
	{
        pMacInst->pMacAddr->MAC_MCR = 0x04880;                                       /* 100M/全双工/接收帧删除pad和crc */
	}
	else if(pMacInst->wPhySpeed == 1000)                                             /* 1000M/全双工/接收帧删除pad和crc */
	{
		pMacInst->pMacAddr->MAC_MCR = 0x04880;                                       /* 1000M暂不配置 ，使用默认配置 */
	}

	return 0;
}

/*******************************************************************
* 功能：cmc 设置并保存接收pbuf
* 参数：pMacInst,mac信息对象
* 返回值：0,保存成功；1，保存失败
********************************************************************/
static UINT8 MAC_Save_Rxpbuf(pMacInformation_st pMacInst,struct pbuf *pBuf)
{
	UINT8 cSeq;

	if(pMacInst->cRxPbufNum >= MAC_NUM_RX_DESP)                     /* 超出可保存的最大数 */
	{
		pbuf_free(pBuf);
		return 1;
	}

	cSeq = (pMacInst->cRxPbufNum+pMacInst->cRxPbufSeq)%MAC_NUM_RX_DESP;

	pMacInst->dwRxPbufArr[cSeq] = (UINT32)pBuf;
	pMacInst->cRxPbufNum++;

	return 0;
}

/*******************************************************************
* 功能：cmc 设置并保存发送pbuf
* 参数：pMacInst,mac信息对象
* 返回值：0,保存成功；1，保存失败
********************************************************************/
static UINT8 MAC_Save_Txpbuf(pMacInformation_st pMacInst,struct pbuf *pBuf,pDesp_st pDesp)
{
	if(pMacInst->cTxPbufNum >= MAC_NUM_TX_DESP)                     /* 超出可保存的最大数 */
	{
		//pbuf_free(pBuf);
		return 1;
	}

	pMacInst->dwTxDespArr[pMacInst->cTxPbufSeq] = (UINT32)pDesp;
	pMacInst->dwTxPbufArr[pMacInst->cTxPbufSeq] = (UINT32)pBuf;
	pMacInst->cTxPbufNum++;
	if(++(pMacInst->cTxPbufSeq) >= MAC_NUM_TX_DESP)
	{
		pMacInst->cTxPbufSeq = 0;
	}
	return 0;
}

/*******************************************************************
* 功能：cmc 清除并释放已保存的所有发送完成的pbuf，该函数在发送完成中断内调用。目的是当一帧数据
*      发送完成后，需要释放发送pbuf。
* 参数：pMacInst,mac信息对象
* 返回值：0,保存成功；1，保存失败
* 说明：1.在发送数据申请发送pbuf时，pbuf的ref默认为1，在执行发送函数MAC_SendFrame时，
*      会将ref加1变成2，而应用程序在每次调用发送函数后，会进行一次pbuf的释放，此时的释放
*      有可能会导致DMA未将pbuf中的数据转移完成，将ref设置成2，在发送完成中断内，再次执行
*      pbuf的释放，保证DMA能够将pbuf中的数据转移完成后再彻底的完成释放pbuf。
*     2.此函数释放pbuf的原则是，查询所有发送描述符的状态，当发送描述符已被DMA释放，释放该pbuf。
********************************************************************/
static UINT8 MAC_Clear_Txpbuf(pMacInformation_st pMacInst)
{
	struct pbuf *pBuf;
	UINT8 cPbufNum;
	pDesp_st pCurDesp;

	cPbufNum = pMacInst->cTxPbufNum;
	while((cPbufNum--) != 0)
	{
		pBuf = (struct pbuf *)pMacInst->dwTxPbufArr[pMacInst->cTxPbufHead];       /* 正常情况下，pCurDesp中的pBuf肯定等于pBuf*/
		pCurDesp = (pDesp_st)pMacInst->dwTxDespArr[pMacInst->cTxPbufHead];

		if((pCurDesp->DES0 & TX_TDES0_OWN) == 0)                                  /* 此描述符已被DMA释放，pBuf中的数据已转移到MAC的FIFO */
		{
			pMacInst->cTxPbufNum--;
			pbuf_free(pBuf);
			pMacInst->dwTxDespArr[pMacInst->cTxPbufHead] = 0;
			if(++(pMacInst->cTxPbufHead) == MAC_NUM_TX_DESP)
			{
				pMacInst->cTxPbufHead = 0;
			}
		}
		else                                                                    /* 此描述符未被DMA释放，pBuf中数据未转移，正常情况下后续的描述符也为被DMA释放 */
		{
			break;
		}
	}

	return 0;
}

/*******************************************************************
* 功能：cmc 清除并释放已保存的所有接收pbuf，目的是在重新初始化MAC时，需要重新分配接收
*      pbuf则之前已分配的pbuf需要释放掉，否则会造成内存泄漏，一般在重新初始化MAC前调用。
*      第一次初始化MAC时不需要调用此函数。
* 参数：pMacInst,mac信息对象
* 返回值：0,保存成功；1，保存失败
********************************************************************/
UINT8 MAC_Clear_Rxpbuf(pMacInformation_st pMacInst)
{
	struct pbuf *pBuf;

	while(pMacInst->cRxPbufNum != 0)
	{
		pMacInst->cRxPbufNum--;
		pBuf = (struct pbuf *)pMacInst->dwRxPbufArr[pMacInst->cRxPbufSeq];
		pbuf_free(pBuf);
		pBuf = NULL;

		pMacInst->cRxPbufSeq++;
		if(pMacInst->cRxPbufSeq == MAC_NUM_RX_DESP)
		{
			pMacInst->cRxPbufSeq = 0;
		}
	}

	return 0;
}

/*******************************************************************
* 功能：cmc 获取接收pbuf
* 参数：pMacInst,mac信息对象
* 返回值：返回pbuf，为NULL时表示未获取到pbuf
********************************************************************/
struct pbuf * MAC_Get_Rxpbuf(pMacInformation_st pMacInst)
{
	UINT16 wLen;
	struct pbuf *pBuf = NULL;

	if(pMacInst->cRxPbufNum == 0)                                          /* 无有效数据的pbuf */
	{
		return NULL;
	}

	wLen = MAC_GetRxFrameLength(pMacInst);                                 /* 获取帧长度 */

	pBuf = (struct pbuf *)pMacInst->dwRxPbufArr[pMacInst->cRxPbufSeq];
	pbuf_realloc(pBuf,wLen);                                               /* 根据帧长度，调整pBuf长度 */

	pMacInst->cRxPbufSeq++;
	pMacInst->cRxPbufSeq = pMacInst->cRxPbufSeq%MAC_NUM_RX_DESP;
	pMacInst->cRxPbufNum--;

	return pBuf;
}

/*******************************************************************
* 功能：cmc 释放接收描述符中的所有权，使DMA拥有操作权限，并将此描述符中的pbuf释放
* 参数：pMacInst,mac信息对象
* 返回值：返回pbuf，为NULL时表示未获取到pbuf
********************************************************************/
void MAC_Release_RxDesp(pMacInformation_st pMacInst)
{
	struct pbuf *pBuf = NULL;

	if(pMacInst->cRxPbufNum == 0)                                          /* 无有效数据的pbuf */
	{
		return ;
	}

	pBuf = (struct pbuf *)pMacInst->dwRxPbufArr[pMacInst->cRxPbufSeq];
	pbuf_free(pBuf);
	pBuf = NULL;

	pMacInst->cRxPbufSeq++;
	pMacInst->cRxPbufSeq = pMacInst->cRxPbufSeq%MAC_NUM_RX_DESP;
	pMacInst->cRxPbufNum--;

	MAC_SetupRxDesp(pMacInst);
}

/*******************************************************************
* 功能：cmc 接收描述符初始化
* 参数：pMacInst,mac信息对象
* 返回值：0,初始化成功
********************************************************************/
static UINT8 MAC_RxDesp_Init(pMacInformation_st pMacInst)
{
	UINT8 i;
	pDesp_st pDesp;
	struct pbuf *pBuf;

	pMacInst->pMacDMAAddr->MAC_DMA_OMR &= (~MAC_DMA_OMR_SR);                          /* 停止DMA传送 */

    pMacInst->pMacDMAAddr->MAC_DMA_OMR |= MAC_DMA_OMR_RSF;                            /* 配置为接收到完整帧后DMA开始转发数据到内存 */
    memset(pMacInst->pRxDespBase,0,pMacInst->cRxDespNum * sizeof(Desp_st));

    pMacInst->pCurRxDesp = pMacInst->pRxDespBase;                                     /* 当前描述符指向第一个描述符 */

    for(i=0;i<pMacInst->cRxDespNum;i++)                                               /* 初始化全部的接收描述符 */
    {
    	pDesp = &((pDesp_st)(pMacInst->pRxDespBase))[i];                              /* 获取每一个接收描述符的首地址 */
    	pDesp->DES0 |= RX_RDES0_OWN;                                                  /* 描述符占有权由DMA控制 */
        pDesp->DES1 = RX_RDES1_RCH | pMacInst->wRxBufSize;                            /* 设置描述符的第二个地址是下一个描述符地址，同时设置buffer1大小 */

        pBuf = pbuf_alloc(PBUF_RAW, pMacInst->wRxBufSize, PBUF_POOL);                 /* 为每一个描述符分配一个接收数据存储地址 */
        if(!pBuf)                                                                     /* pbuf分配失败 */
        {
#if LINK_STATS
        	lwip_stats.link.memerr++;
        	lwip_stats.link.drop++;
#endif
        	return 1;
        }

        pDesp->BUFF1_ADDR = (UINT32)pBuf->payload;                                    /* 设置buffer1地址 */

        if(i == pMacInst->cRxDespNum - 1)                                             /* 最后一个描述符 */
        {
            pDesp->DES1 |= RX_RDES1_RER;                                              /* 指示此描述符位最后一个描述符 */
            pDesp->BUFF2_ADDR = (UINT32)pMacInst->pRxDespBase;                        /* 设置下一个描述符地址 */
        }
        else
        {
            pDesp->BUFF2_ADDR = (UINT32)(pDesp + 1);                                  /* 设置下一个描述符地址 */
        }

        MAC_Save_Rxpbuf(pMacInst,pBuf);
    }

    pMacInst->pMacDMAAddr->MAC_DMA_RDLA = (UINT32)pMacInst->pRxDespBase;              /* 设置MAC接收描述符寄存器 */
    pMacInst->pMacDMAAddr->MAC_DMA_OMR |= (MAC_DMA_OMR_SR);                           /* 启动接收 */
    return 0;
}

/*******************************************************************
* 功能：cmc 发送描述符初始化
* 参数：pMacInst,mac信息对象
* 返回值：0，初始化成功
* 说明：buffer1地址在发送数据时写入到描述符中，这里无需设置
********************************************************************/
static UINT8 MAC_TxDesp_Init(pMacInformation_st pMacInst)
{
	UINT8 i;
	pDesp_st pDesp;

	pMacInst->pMacDMAAddr->MAC_DMA_OMR &= (~MAC_DMA_OMR_ST);                          /* 停止DMA传送 */

    pMacInst->pMacDMAAddr->MAC_DMA_OMR |= MAC_DMA_OMR_TSF;                            /* 配置为FIFO中为完整帧时开始发送 */
    memset(pMacInst->pTxDespBase,0,pMacInst->cTxDespNum * sizeof(Desp_st));

    pMacInst->pCurTxDesp = pMacInst->pTxDespBase;                                     /* 当前描述符指向第一个描述符 */

    for(i=0;i<pMacInst->cTxDespNum;i++)                                               /* 初始化全部的接收描述符 */
    {
    	pDesp = &((pDesp_st)(pMacInst->pTxDespBase))[i];                              /* 获取每一个接收描述符的首地址 */
    	pDesp->DES0 &= (~TX_TDES0_OWN);                                               /* 描述符占有权由host控制 */
        pDesp->DES1 = TX_TDES1_LS | TX_TDES1_FS | TX_TDES1_TCH;                       /* 设置描述符的第二个地址是下一个描述符地址，同时设置LS及FS */

        if(i == pMacInst->cTxDespNum - 1)                                             /* 最后一个描述符 */
        {
            pDesp->DES1 |= TX_TDES1_TER;                                              /* 指示此描述符位最后一个描述符 */
            pDesp->BUFF2_ADDR = (UINT32)pMacInst->pTxDespBase;                        /* 设置下一个描述符地址 */
        }
        else
        {
            pDesp->BUFF2_ADDR = (UINT32)(pDesp + 1);                                  /* 设置下一个描述符地址 */
        }
    }

    pMacInst->pMacDMAAddr->MAC_DMA_TDLA = (UINT32)pMacInst->pTxDespBase;
    pMacInst->pMacDMAAddr->MAC_DMA_OMR |= MAC_DMA_OMR_ST;                             /* 启动发送 */

    return 0;
}

/*******************************************************************
* 功能：cmc 检查是否有足够的描述符用于发送pbuf
* 参数：pMacInst,mac信息对象
*     pBuf,待发送的数据
* 返回值：0，有足够的描述符用于发送数据
*       1，没有足够的描述符用于发送数据
********************************************************************/
static UINT8 MAC_CheckAvaiableDesp(pMacInformation_st pMacInst, struct pbuf *pBuf)
{
	struct pbuf *pTemp;
	UINT8 cPbufNum,cDespNum;
	pDesp_st pDespTemp = pMacInst->pCurTxDesp;

	for(pTemp = pBuf,cPbufNum = 0; pTemp != NULL; pTemp = pTemp->next)                /* 首先计算待发送的pbuf个数 */
	{
		cPbufNum++;
	}

	for(cDespNum = 0; cDespNum < MAC_NUM_TX_DESP; cDespNum++)                         /* 查询可用的描述个数 */
	{
		if(pDespTemp->DES0 & TX_TDES0_OWN)
		{
			break;
		}
		pDespTemp = (void *)(pMacInst->pCurTxDesp->BUFF2_ADDR);
	}

	return (cPbufNum <= cDespNum)?0:1;
}

/*******************************************************************
* 功能：cmc 发送一个MAC帧
* 参数：pMacInst,mac信息对象
*     pBuf,待发送的数据
* 返回值：0，发送成功
*       1，发送失败
********************************************************************/
UINT8 MAC_SendFrame(pMacInformation_st pMacInst, struct pbuf *pBuf)
{
	struct pbuf *pCurBuf;
	UINT32 dwDes1Temp;

	if(MAC_CheckAvaiableDesp(pMacInst, pBuf))                                           /* 检查是否有足够的描述符用于发送 */
	{
		return 1;                                                                       /* 没有足够的描述符用于发送数据 */
	}

    for(pCurBuf = pBuf; NULL != pCurBuf; pCurBuf = pCurBuf->next)
    {
    	dwDes1Temp = 0;
    	if(pCurBuf == pBuf)                                                             /* 此描述符为发送帧的第一段 */
    	{
    		dwDes1Temp |= TX_TDES1_FS;
    	}

    	if(NULL == pCurBuf->next)                                                       /* 此描述符为发送帧的最后一段 */
    	{
    		dwDes1Temp |= TX_TDES1_LS;
    	}

    	dwDes1Temp |= TX_TDES1_IC;                                                      /* 允许发送完成中断 */
    	dwDes1Temp |= TX_TDES1_TCH;                                                     /* 设置描述符的第二个地址是下一个描述符地址 */
    	dwDes1Temp |= pCurBuf->len;                                                     /* 设置此描述符的发送数据长度 */

    	pMacInst->pCurTxDesp->DES1 = dwDes1Temp;                                        /* 设置此描述符的DES1 */
    	pMacInst->pCurTxDesp->DES0 = TX_TDES0_OWN;                                      /* 设置此描述符的使用权由DMA控制 */

    	pMacInst->pCurTxDesp->BUFF1_ADDR = (UINT32)pCurBuf->payload;                    /* 添加数据buff到描述符buff1_addr */
    	pMacInst->pCurTxDesp = (void *)(pMacInst->pCurTxDesp->BUFF2_ADDR);              /* 更新描述符至下一个描述符 */

    	MAC_Save_Txpbuf(pMacInst, pCurBuf, pMacInst->pCurTxDesp);
    	pbuf_ref(pCurBuf);
    }

    pMacInst->pMacDMAAddr->MAC_DMA_SR = MAC_DMA_IER_TUE;                               /* 清除TU位*/
    pMacInst->pMacDMAAddr->MAC_DMA_TPDR = 0x00;                                         /* 允许DMA获取描述符 ,开始传输 */

    return 0;
}

/*******************************************************************
* 功能：cmc 获取接收帧长度
* 参数：pMacInst,mac信息对象
* 返回值：帧长度
********************************************************************/
static UINT16 MAC_GetRxFrameLength(pMacInformation_st pMacInst)
{
	UINT16 wLen;

	if(0 == (pMacInst->pCurRxDesp->DES0 & RX_RDES0_OWN))                                /* 检查描述符是否属于host */
	{
		wLen = ((pMacInst->pCurRxDesp->DES0 & RX_RDES0_LEN)>>16);                       /* 获取数据长度 */
	    if(wLen >= 64 && wLen <= 1518)                                                  /* 合法长度的帧 */
	    {
	    	wLen -= 4;                                                                  /* 减去CRC长度 */
	    }
	    else if(wLen < 64)                                                              /* 不标准的以太网帧 */
	    {
	    	MAC_Release_RxDesp(pMacInst);
	    	wLen = 0;
	    }
	    else
	    {
	    	MAC_Release_RxDesp(pMacInst);
	    	wLen = 0;
	    }

	    return wLen;
	}

	return 0;
}

/*******************************************************************
* 功能：cmc 获取接收帧数据
* 参数：pMacInst,mac信息对象
*     pBuf，接收到的数据拷贝到该指针所指向的区域
* 返回值：0，没有可接收的数据或者接收数据存在错误
*       非0，接收帧长度
********************************************************************/
static struct pbuf * MAC_GetRecvFrame(pMacInformation_st pMacInst)
{
	struct pbuf *pBuffer = NULL;

    if((pMacInst->pCurRxDesp->DES0 & RX_RDES0_OWN) == 0)
    {
    	pBuffer = MAC_Get_Rxpbuf(pMacInst);                                            /* 获取接收数据的pbuf */
    }

    return pBuffer;
}

/*******************************************************************
* 功能：cmc 设置接收描述符列表
* 参数：pMacInst,mac信息对象
* 返回值：
* 说明：当从接收描述符中接收到一帧数据后，调用此函数，用于从新设置描述符状态、重新分配数据
*     内存，以及调整当前描述符指针位置
********************************************************************/
static void MAC_SetupRxDesp(pMacInformation_st pMacInst)
{
	struct pbuf * pBuf;
	pDesp_st pBaseDesp = (pDesp_st)pMacInst->pRxDespBase;

	while(pMacInst->cRxPbufNum < MAC_NUM_RX_DESP)
	{
		pBuf = pbuf_alloc(PBUF_RAW, pMacInst->wRxBufSize, PBUF_POOL);                  /* 为该描述符重新分配buff */
		if(!pBuf)
		{
#if LINK_STATS
			lwip_stats.link.memerr++;
			lwip_stats.link.drop++;
#endif
			return ;
		}
		if(MAC_Save_Rxpbuf(pMacInst,pBuf))                                             /* 存放pbuf的数组已满，退出 */
		{
			return ;
		}

		pMacInst->pCurRxDesp->BUFF1_ADDR = (UINT32)pBuf->payload;                      /* 重新设置此接收描述符的数据buff地址 */

		pMacInst->pCurRxDesp->DES0 |= RX_RDES0_OWN;                                    /* 设置DMA拥有描述符控制权 */
		if(pMacInst->pCurRxDesp >= &pBaseDesp[pMacInst->cRxDespNum - 1])               /* 更新当前描述符 */
		{
			pMacInst->pCurRxDesp = pMacInst->pRxDespBase;
		}
		else
		{
			(pMacInst->pCurRxDesp)++;
		}
	}
}

/*******************************************************************
* 功能：cmc 接收中断处理函数
* 参数：pMacInst,mac信息对象
* 返回值：
********************************************************************/
#if !NO_SYS && (NO_SYS ==2)
extern OS_FLAG_GRP *pEthernetInputFlag;
#endif

void MAC_RecvHandler(pMacInformation_st pMacInst)
{
    UINT16 wLen;
#if !NO_SYS && (NO_SYS ==2)
    UINT8 err;
#endif

    struct pbuf *pPbuf;

    while((wLen = MAC_GetRxFrameLength(pMacInst)) != 0)                                  /* 获取当前描述符的数据长度 */
    {
    	pPbuf = MAC_GetRecvFrame(pMacInst);                                              /* 获取接收数据 */

    	if((pPbuf == NULL)||(pq_enqueue(pMacInst->pRecv_q, (void *)pPbuf) < 0))          /* 将数据存储在接收队列中 */
    	{
#if LINK_STATS
    		lwip_stats.link.memerr++;
    		lwip_stats.link.drop++;
#endif
    		pbuf_free(pPbuf);                                                            /* 存储失败，释放pbuf */
    	    break ;
    	}

    	MAC_SetupRxDesp(pMacInst);                                                       /* 更新此描述符状态 */
    }

#if !NO_SYS && (NO_SYS ==2)
//    sys_sem_signal(&pMacInst->pSemRxDataAvailable);                                      /* 发送信号量，通知上层接收数据可用 */

    OSFlagPost(pEthernetInputFlag,pMacInst->wMacEventFlag,OS_FLAG_SET,&err);

#endif
}

/*******************************************************************
* 功能：cmc MAC0中断回调函数
* 参数：
* 返回值：
********************************************************************/
extern void HAL_ETH_RxCpltCallback(pMacInformation_st pMacInst);
void MAC0_ISRCallback(UINT32 dwIrqId)
{
	if(MAC0.pMacDMAAddr->MAC_DMA_SR & MAC_DMA_SR_TI)
	{
		MAC0.pMacDMAAddr->MAC_DMA_SR = (MAC_DMA_SR_NIS | MAC_DMA_SR_TI);
		//MAC_Clear_Txpbuf(&MAC0);
	}

	if(MAC0.pMacDMAAddr->MAC_DMA_SR & MAC_DMA_SR_UNF)                                            /* DMA发送buff发生下溢 */
	{
		MAC0.pMacDMAAddr->MAC_DMA_SR = (MAC_DMA_SR_UNF | MAC_DMA_SR_AIS);
	}

	if(MAC0.pMacDMAAddr->MAC_DMA_SR & MAC_DMA_SR_OVE)                                            /* DMA接收buff发生上溢 */
	{
		MAC0.pMacDMAAddr->MAC_DMA_SR = (MAC_DMA_SR_OVE|MAC_DMA_SR_AIS);
	}

	if (MAC0.pMacDMAAddr->MAC_DMA_SR & MAC_DMA_SR_RI)                                            /* 帧接收完成，中断产生 */
	{
		MAC0.pMacDMAAddr->MAC_DMA_SR = (MAC_DMA_SR_RI | MAC_DMA_SR_NIS);
		HAL_ETH_RxCpltCallback(&MAC0);
	}

	if(MAC0.pMacDMAAddr->MAC_DMA_SR & MAC_DMA_SR_RU)                                             /* DMA不能获取接收描述符列表 */
	{
		MAC0.pMacDMAAddr->MAC_DMA_SR = (MAC_DMA_SR_AIS | MAC_DMA_SR_RU);
		//MAC_SetupRxDesp(&MAC0);
	}

	if(MAC0.pMacDMAAddr->MAC_DMA_SR & MAC_DMA_SR_FBI)                                            /* 总线错误，需要重新初始化MAC */
	{
		//MAC_Clear_Rxpbuf(&MAC0);                                                                    /* 先屏蔽总线错误时的恢复操作，因为需要重新初始化MAC，耗时时间长，*/
		//MAC_Init(&MAC0);                                                                            /* 不适合在中断中处理,如若有必要则打开 */
		//MAC_Enable(&MAC0);

		//MAC0.pMacDMAAddr->MAC_DMA_SR = MAC_DMA_SR_FBI;
	}
}

/*******************************************************************
* 功能：cmc MAC1中断回调函数
* 参数：
* 返回值：
********************************************************************/
void MAC1_ISRCallback(UINT32 dwIrqId)
{
	if(MAC1.pMacDMAAddr->MAC_DMA_SR & MAC_DMA_SR_TI)
	{
		MAC1.pMacDMAAddr->MAC_DMA_SR = (MAC_DMA_SR_NIS | MAC_DMA_SR_TI);
		//MAC_Clear_Txpbuf(&MAC1);
	}

	if(MAC1.pMacDMAAddr->MAC_DMA_SR & MAC_DMA_SR_UNF)                                            /* DMA发送buff发生下溢 */
	{
		MAC1.pMacDMAAddr->MAC_DMA_SR = (MAC_DMA_SR_UNF | MAC_DMA_SR_AIS);
	}

	if(MAC1.pMacDMAAddr->MAC_DMA_SR & MAC_DMA_SR_OVE)                                            /* DMA接收buff发生上溢 */
	{
		MAC1.pMacDMAAddr->MAC_DMA_SR = (MAC_DMA_SR_OVE|MAC_DMA_SR_AIS);
	}

	if (MAC1.pMacDMAAddr->MAC_DMA_SR & MAC_DMA_SR_RI)                                            /* 帧接收完成，中断产生 */
	{
		MAC1.pMacDMAAddr->MAC_DMA_SR = (MAC_DMA_SR_RI | MAC_DMA_SR_NIS);
		HAL_ETH_RxCpltCallback(&MAC1);
	}

	if(MAC1.pMacDMAAddr->MAC_DMA_SR & MAC_DMA_SR_RU)                                             /* DMA不能获取接收描述符列表 */
	{
		MAC1.pMacDMAAddr->MAC_DMA_SR = (MAC_DMA_SR_AIS | MAC_DMA_SR_RU);
		//MAC_SetupRxDesp(&MAC1);
	}

	if(MAC1.pMacDMAAddr->MAC_DMA_SR & MAC_DMA_SR_FBI)                                            /* 总线错误，需要重新初始化MAC */
	{
		//MAC_Clear_Rxpbuf(&MAC1);                                                                    /* 先屏蔽总线错误时的恢复操作，因为需要重新初始化MAC，耗时时间长，*/
		//MAC_Init(&MAC1);                                                                            /* 不适合在中断中处理,如若有必要则打开 */
		//MAC_Enable(&MAC1);

		//MAC1.pMacDMAAddr->MAC_DMA_SR |= MAC_DMA_SR_FBI;
	}
}
