/*******************************************************************
*文件:     phy.c
*作者:     朱林
*日期:     2018-9-13
*说明:     phy芯片驱动程序
*修改记录:
********************************************************************/
#include "netif/phy.h"
#include "netif/adapter.h"

/* phy芯片自动协商or强制100M */
#define PHY_AUTONEGO_EN    0                         /* 1,使能自动协商；0，禁能自动协商，强制100M*/
/* RMII/MII模式选择 */
#define MII_MODE_SELECT    0                         /* 1,RMII;0,MII*/

/* phy芯片Identifier */
#define PHY_ID_8208		(0xc882001c)
#define PHY_ID_83848	(0x5c902000)
#define PHY_ID_83849	(0x5ca22000)

/* PHY标准寄存器定义  */
#define IEEE_CONTROL_REG                        0    /* 控制寄存器 */
#define IEEE_STATUS_REG                         1    /* 状态寄存器 */
#define IEEE_IDENTIFIER_1_REG                   2    /* PHYIDR1寄存器 */
#define IEEE_IDENTIFIER_2_REG                   3    /* PHYIDR2寄存器 */
#define IEEE_AUTONEGO_ADVERTISE_REG             4    /* 自动协商通告寄存器 */
#define IEEE_AUTONEGO_PARTNER_ABILITY_1_REG     5    /* 自动协商连接伙伴能力寄存器(base page) */
#define IEEE_AUTONEGO_EXPANSION_REG             6    /* 自动协商扩展寄存器  */
#define IEEE_AUTONEGO_NEXT_PAGE_REG             7    /* 自动协商下一页寄存器 */

/* DP83849寄存器定义 */
#define DP83849_PHY_STATUS_REG                  16    /* PHY状态寄存器  */
#define DP83849_MII_INT_CTRL_REG                17    /* MII中断控制寄存器  */
#define DP83849_MII_INT_STATUS_REG              18    /* MII中断状态寄存器  */
#define DP83849_PAGE_SELECT_REG                 19    /* 页选择寄存器  */
#define DP83849_RMII_BYPASS_REG                 23    /* RMII/MII模式配置寄存器 */

/* DP83848寄存器定义 */
#define DP83848_PHY_STATUS_REG                  16    /* PHY状态寄存器  */
#define DP83848_MII_INT_CTRL_REG                17    /* MII中断控制寄存器  */
#define DP83848_MII_INT_STATUS_REG              18    /* MII中断状态寄存器  */
#define DP83848_PAGE_SELECT_REG                 19    /* 页选择寄存器  */
#define DP83848_RMII_BYPASS_REG                 23    /* RMII/MII模式配置寄存器 */

/* PHY标准寄存器位定义 */
#define IEEE_CTRL_RESET_BIT                     0x8000    /* phy芯片复位bit15*/
#define IEEE_CTRL_AUTONEGO_EN_BIT               0x1000    /* phy芯片自动协商使能位bit12*/
#define IEEE_CTRL_AUTONEGO_RESTART_BIT          0x0200    /* phy芯片自动协商重新启动位bit9*/
#define IEEE_CTRL_SPEED_SELECTION_BIT           0x2000    /* phy芯片速度选择，1=100Mb/s，0=10Mb/s */
#define IEEE_CTRL_DUPLEX_MODE_BIT               0x0100    /* phy芯片双工模式选择位，1=全双工，0=半双工 */
#define IEEE_STA_AUTONEG_COMPLETE_BIT           0x0020    /* phy芯片自动协商完成状态位，1=协商完成，0协商未完成 */
#define IEEE_STA_LINK_STATUS_BIT                0x0004    /* phy芯片连接状态位，1=linkup；0=linkdown */
#define IEEE_AUTONEGO_100M_BIT                  0x0180    /* phy芯片自动协商100M全双工能力 */
#define IEEE_AUTONEGO_10M_BIT                   0x0060    /* phy芯片自动协商10M全双工能力 */

/* DP83849寄存器位定义 */
#define DP83849_MII_MODE_BIT                    0x0020    /* RMII/MII工作模式选择位，0=MII；1=RMII */
#define DP83849_MII_REV1_0_BIT                  0x0010    /* RMIIV1.2/V1.0模式选择位，0=V1.2;1=V1.0 */

/* DP83848寄存器位定义 */
#define DP83848_MII_MODE_BIT                    0x0020    /* RMII/MII工作模式选择位，0=MII；1=RMII */
#define DP83848_MII_REV1_0_BIT                  0x0010    /* RMIIV1.2/V1.0模式选择位，0=V1.2;1=V1.0 */

UINT8 cPhyAddr[] = {PHY0_DEVICE_ADDR, PHY1_DEVICE_ADDR};

static UINT8 PHY_8208_Init(pMacInformation_st pMacInst);
static UINT8 PHY_83848_Init(pMacInformation_st pMacInst);
static UINT8 PHY_83849_Init(pMacInformation_st pMacInst);

/*******************************************************************
* 功能：根据phy identifer选择当前型号的phy进行初始化
* 参数：pMacInst,mac实例指针
* 返回值：0x00：phy初始化成功
*       0x01：phy复位超时
*       0x02：读phy id失败或无匹配id
*       0x04：自协商超时
*       0x08：link建立超时
* 修改记录:
* 修改日期               修改内容                                                                                修改人
* 20181228    为了适应CMC现有的核心板，只能用MAC1初始化              朱林
*             两路PHY，这里修改为无论pMacInst是哪个MAC，
*             初始化phy时，都只用MAC1
********************************************************************/
INT32S PHY_Init(pMacInformation_st pMacInst)
{
	UINT8 cRetCode = 0;
	UINT16 wPhyReg;
	UINT32 dwPhyID;
	INT32S nWait;
	pMac_st pMAC1Addr = (pMac_st)MAC1_BASE_ADDR;

	MAC_WritePhyReg(pMAC1Addr,pMacInst->dwPhyAddr,IEEE_CONTROL_REG,IEEE_CTRL_RESET_BIT);    /* 复位phy芯片 */
	nWait = SYS_FREQ/100000;
	MAC_ReadPhyReg(pMAC1Addr,pMacInst->dwPhyAddr,IEEE_CONTROL_REG,&wPhyReg);
    while(0 != (wPhyReg&IEEE_CTRL_RESET_BIT))                                                        /* 等待复位成功或超时 */
    {
    	if( --nWait <= 0)
    	{
    		cRetCode |= 0x01;
    		break;
    	}
    }

    MAC_ReadPhyReg(pMAC1Addr,pMacInst->dwPhyAddr,IEEE_IDENTIFIER_1_REG,&wPhyReg);           /* 检测phy id*/
    dwPhyID = wPhyReg;
    MAC_ReadPhyReg(pMAC1Addr,pMacInst->dwPhyAddr,IEEE_IDENTIFIER_2_REG,&wPhyReg);
    dwPhyID |= (wPhyReg << 16);

    switch(dwPhyID)
    {
        case PHY_ID_83849:
        	cRetCode |= PHY_83849_Init(pMacInst);
    	    break;
        case PHY_ID_83848:
        	cRetCode |= PHY_83848_Init(pMacInst);
        	break;
        case PHY_ID_8208:
        	cRetCode |= PHY_8208_Init(pMacInst);
        	break;
        default:
        	cRetCode |= 0x02;
            break;
    }

    return cRetCode;
}

/*******************************************************************
* 功能：RTL8208初始化
* 参数：pMacInst,mac实例指针
* 返回值：0x00：phy初始化成功
*       0x01：phy复位超时
*       0x02：读phy id失败或无匹配id
*       0x04：自协商超时
*       0x08：link建立超时
********************************************************************/
static UINT8 PHY_8208_Init(pMacInformation_st pMacInst)
{
	UINT8 cRetCode = 0;
	UINT16 wPhyReg;
	INT32S nWait;

#if PHY_AUTONEGO_EN == 1
	MAC_ReadPhyReg(pMacInst->pMacAddr,pMacInst->dwPhyAddr,IEEE_AUTONEGO_ADVERTISE_REG,&wPhyReg);     /* 配置自协商能力  */
	wPhyReg |= IEEE_AUTONEGO_100M_BIT;
	wPhyReg |= IEEE_AUTONEGO_10M_BIT;
	MAC_WritePhyReg(pMacInst->pMacAddr,pMacInst->dwPhyAddr,IEEE_AUTONEGO_ADVERTISE_REG,wPhyReg);

    MAC_ReadPhyReg(pMacInst->pMacAddr,pMacInst->dwPhyAddr,IEEE_CONTROL_REG,&wPhyReg);                /* 使能自协商  */
    wPhyReg |= IEEE_CTRL_AUTONEGO_EN_BIT;
    MAC_WritePhyReg(pMacInst->pMacAddr,pMacInst->dwPhyAddr,IEEE_CONTROL_REG,wPhyReg);
    MAC_ReadPhyReg(pMacInst->pMacAddr,pMacInst->dwPhyAddr,IEEE_CONTROL_REG,&wPhyReg);                /* 重新启动自协商  */
    wPhyReg |= IEEE_CTRL_AUTONEGO_RESTART_BIT;
    MAC_WritePhyReg(pMacInst->pMacAddr,pMacInst->dwPhyAddr,IEEE_CONTROL_REG,wPhyReg);

    nWait = SYS_FREQ / 100000;
    MAC_ReadPhyReg(pMacInst->pMacAddr,pMacInst->dwPhyAddr,IEEE_STATUS_REG,&wPhyReg);                 /* 等待自协商完成 */
	while(!(wPhyReg & IEEE_STA_AUTONEG_COMPLETE_BIT))
	{
		MAC_ReadPhyReg(pMacInst->pMacAddr,pMacInst->dwPhyAddr,IEEE_STATUS_REG,&wPhyReg);
		if( --nWait <= 0 )
		{
			cRetCode |= 0x04;	                                                                      /* 自协商超时 */
			break;
		}
	}
#else
    MAC_ReadPhyReg(pMacInst->pMacAddr,pMacInst->dwPhyAddr,IEEE_CONTROL_REG,&wPhyReg);                /* 强制100M全双工  */
    wPhyReg |= IEEE_CTRL_SPEED_SELECTION_BIT;
    wPhyReg |= IEEE_CTRL_DUPLEX_MODE_BIT;
    MAC_WritePhyReg(pMacInst->pMacAddr,pMacInst->dwPhyAddr,IEEE_CONTROL_REG,wPhyReg);
#endif

    nWait = SYS_FREQ / 100000;
    MAC_ReadPhyReg(pMacInst->pMacAddr,pMacInst->dwPhyAddr,IEEE_STATUS_REG,&wPhyReg);                 /* 等待连接建立成功 */
	while(!(wPhyReg & IEEE_STA_LINK_STATUS_BIT))
	{
		MAC_ReadPhyReg(pMacInst->pMacAddr,pMacInst->dwPhyAddr,IEEE_STATUS_REG,&wPhyReg);
		if( --nWait <= 0 )
		{
			cRetCode |= 0x08;	                                                                     /* 等待连接建立超时 */
			break;
		}
	}

	if(0 == cRetCode)                                                                                /* 连接建立成功，获取phy速度 */
	{
	    MAC_ReadPhyReg(pMacInst->pMacAddr,pMacInst->dwPhyAddr,IEEE_CONTROL_REG,&wPhyReg);
	    pMacInst->wPhySpeed = (wPhyReg&IEEE_CTRL_SPEED_SELECTION_BIT)?10:100;
	}
	else
	{
		pMacInst->wPhySpeed = 0;
	}

    return cRetCode;
}

/*******************************************************************
* 功能：dp83848初始化
* 参数：pMacInst,mac实例指针
* 返回值：0x00：phy初始化成功
*       0x01：phy复位超时
*       0x02：读phy id失败或无匹配id
*       0x04：自协商超时
*       0x08：link建立超时
********************************************************************/
static UINT8 PHY_83848_Init(pMacInformation_st pMacInst)
{
	UINT8 cRetCode = 0;
	UINT16 wPhyReg;
	INT32S nWait;

#if (MII_MODE_SELECT == 0)                                                                            /* 默认为MII*/

#else                                                                                                 /* 配置为RMII*/
	MAC_ReadPhyReg(pMacInst->pMacAddr,pMacInst->dwPhyAddr,DP83848_RMII_BYPASS_REG,&wPhyReg);
    wPhyReg |= DP83848_MII_MODE_BIT;
    wPhyReg |= DP83848_MII_REV1_0_BIT;
    MAC_WritePhyReg(pMacInst->pMacAddr,pMacInst->dwPhyAddr,DP83848_RMII_BYPASS_REG,wPhyReg);
#endif

#if PHY_AUTONEGO_EN == 1
	MAC_ReadPhyReg(pMacInst->pMacAddr,pMacInst->dwPhyAddr,IEEE_AUTONEGO_ADVERTISE_REG,&wPhyReg);     /* 配置自协商能力  */
	wPhyReg |= IEEE_AUTONEGO_100M_BIT;
	wPhyReg |= IEEE_AUTONEGO_10M_BIT;
	MAC_WritePhyReg(pMacInst->pMacAddr,pMacInst->dwPhyAddr,IEEE_AUTONEGO_ADVERTISE_REG,wPhyReg);

    MAC_ReadPhyReg(pMacInst->pMacAddr,pMacInst->dwPhyAddr,IEEE_CONTROL_REG,&wPhyReg);                /* 使能自协商  */
    wPhyReg |= IEEE_CTRL_AUTONEGO_EN_BIT;
    MAC_WritePhyReg(pMacInst->pMacAddr,pMacInst->dwPhyAddr,IEEE_CONTROL_REG,wPhyReg);
    MAC_ReadPhyReg(pMacInst->pMacAddr,pMacInst->dwPhyAddr,IEEE_CONTROL_REG,&wPhyReg);                /* 重新启动自协商  */
    wPhyReg |= IEEE_CTRL_AUTONEGO_RESTART_BIT;
    MAC_WritePhyReg(pMacInst->pMacAddr,pMacInst->dwPhyAddr,IEEE_CONTROL_REG,wPhyReg);

    nWait = SYS_FREQ / 100000;
    MAC_ReadPhyReg(pMacInst->pMacAddr,pMacInst->dwPhyAddr,IEEE_STATUS_REG,&wPhyReg);                 /* 等待自协商完成 */
	while(!(wPhyReg & IEEE_STA_AUTONEG_COMPLETE_BIT))
	{
		MAC_ReadPhyReg(pMacInst->pMacAddr,pMacInst->dwPhyAddr,IEEE_STATUS_REG,&wPhyReg);
		if( --nWait <= 0 )
		{
			cRetCode |= 0x04;	                                                                      /* 自协商超时 */
			break;
		}
	}
#else
    MAC_ReadPhyReg(pMacInst->pMacAddr,pMacInst->dwPhyAddr,IEEE_CONTROL_REG,&wPhyReg);                /* 强制100M全双工  */
    wPhyReg |= IEEE_CTRL_SPEED_SELECTION_BIT;
    wPhyReg |= IEEE_CTRL_DUPLEX_MODE_BIT;
    MAC_WritePhyReg(pMacInst->pMacAddr,pMacInst->dwPhyAddr,IEEE_CONTROL_REG,wPhyReg);
#endif

    nWait = SYS_FREQ / 100000;
    MAC_ReadPhyReg(pMacInst->pMacAddr,pMacInst->dwPhyAddr,IEEE_STATUS_REG,&wPhyReg);                 /* 等待连接建立成功 */
	while(!(wPhyReg & IEEE_STA_LINK_STATUS_BIT))
	{
		MAC_ReadPhyReg(pMacInst->pMacAddr,pMacInst->dwPhyAddr,IEEE_STATUS_REG,&wPhyReg);
		if( --nWait <= 0 )
		{
			cRetCode |= 0x08;	                                                                     /* 等待连接建立超时 */
			break;
		}
	}

	if(0 == cRetCode)                                                                                /* 连接建立成功，获取phy速度 */
	{
	    MAC_ReadPhyReg(pMacInst->pMacAddr,pMacInst->dwPhyAddr,DP83849_PHY_STATUS_REG,&wPhyReg);
	    pMacInst->wPhySpeed = (wPhyReg&0x02)?10:100;
	}
	else
	{
		pMacInst->wPhySpeed = 0;
	}

    return cRetCode;
}

/*******************************************************************
* 功能：dp83849初始化
* 参数：pMacInst,mac实例指针
* 返回值：0x00：phy初始化成功
*       0x01：phy复位超时
*       0x02：读phy id失败或无匹配id
*       0x04：自协商超时
*       0x08：link建立超时
* 修改记录:
* 修改日期               修改内容                                                                                修改人
* 20181228    为了适应CMC现有的核心板，只能用MAC1初始化              朱林
*             两路PHY，这里修改为无论pMacInst是哪个MAC，
*             初始化phy时，都只用MAC1
********************************************************************/
static UINT8 PHY_83849_Init(pMacInformation_st pMacInst)
{
	UINT8 cRetCode = 0;
	UINT16 wPhyReg;
	INT32S nWait;
	pMac_st pMAC1Addr = (pMac_st)MAC1_BASE_ADDR;

#if (MII_MODE_SELECT == 0)                                                                            /* 默认为MII*/

#else                                                                                                 /* 配置为RMII*/
	MAC_ReadPhyReg(pMAC1Addr,pMacInst->dwPhyAddr,DP83849_RMII_BYPASS_REG,&wPhyReg);
    wPhyReg |= DP83849_MII_MODE_BIT;
    wPhyReg |= DP83849_MII_REV1_0_BIT;
    MAC_WritePhyReg(MAC1_BASE_ADDR,pMacInst->dwPhyAddr,DP83849_RMII_BYPASS_REG,wPhyReg);
#endif

#if (PHY_AUTONEGO_EN == 1)
	MAC_ReadPhyReg(pMAC1Addr,pMacInst->dwPhyAddr,IEEE_AUTONEGO_ADVERTISE_REG,&wPhyReg);     /* 配置自协商能力  */
	wPhyReg |= IEEE_AUTONEGO_100M_BIT;
	wPhyReg |= IEEE_AUTONEGO_10M_BIT;
	MAC_WritePhyReg(pMAC1Addr,pMacInst->dwPhyAddr,IEEE_AUTONEGO_ADVERTISE_REG,wPhyReg);

	wPhyReg = 0;
    MAC_ReadPhyReg(pMAC1Addr,pMacInst->dwPhyAddr,IEEE_CONTROL_REG,&wPhyReg);                /* 使能自协商  */
    wPhyReg |= IEEE_CTRL_AUTONEGO_EN_BIT;
    MAC_WritePhyReg(pMAC1Addr,pMacInst->dwPhyAddr,IEEE_CONTROL_REG,wPhyReg);
    wPhyReg = 0;
    MAC_ReadPhyReg(pMAC1Addr,pMacInst->dwPhyAddr,IEEE_CONTROL_REG,&wPhyReg);                /* 重新启动自协商  */
    wPhyReg |= IEEE_CTRL_AUTONEGO_RESTART_BIT;
    MAC_WritePhyReg(pMAC1Addr,pMacInst->dwPhyAddr,IEEE_CONTROL_REG,wPhyReg);

    MAC_ReadPhyReg(pMAC1Addr,pMacInst->dwPhyAddr,IEEE_CONTROL_REG,&wPhyReg);

    wPhyReg = 0;
    MAC_ReadPhyReg(pMAC1Addr,pMacInst->dwPhyAddr,IEEE_STATUS_REG,&wPhyReg);                 /* 等待自协商完成 */
    nWait = SYS_FREQ / 1000;
	while(!(wPhyReg & IEEE_STA_AUTONEG_COMPLETE_BIT))
	{
		MAC_ReadPhyReg(pMAC1Addr,pMacInst->dwPhyAddr,IEEE_STATUS_REG,&wPhyReg);
		if( --nWait <= 0 )
		{
			cRetCode |= 0x04;	                                                                     /* 自协商超时 */
			break;
		}
	}
#else
    MAC_ReadPhyReg(pMAC1Addr,pMacInst->dwPhyAddr,IEEE_CONTROL_REG,&wPhyReg);                /* 强制100M全双工  */
    wPhyReg |= IEEE_CTRL_SPEED_SELECTION_BIT;
    wPhyReg |= IEEE_CTRL_DUPLEX_MODE_BIT;
    MAC_WritePhyReg(pMAC1Addr,pMacInst->dwPhyAddr,IEEE_CONTROL_REG,wPhyReg);
#endif

    nWait = SYS_FREQ / 1000;
    MAC_ReadPhyReg(pMAC1Addr,pMacInst->dwPhyAddr,IEEE_STATUS_REG,&wPhyReg);                 /* 等待连接建立成功 */
	while(!(wPhyReg & IEEE_STA_LINK_STATUS_BIT))
	{
		MAC_ReadPhyReg(pMAC1Addr,pMacInst->dwPhyAddr,IEEE_STATUS_REG,&wPhyReg);
		if( --nWait <= 0 )
		{
			cRetCode |= 0x08;	                                                                     /* 等待连接建立超时 */
			break;
		}
	}

	if(0 == cRetCode)                                                                                /* 连接建立成功，获取phy速度 */
	{
	    MAC_ReadPhyReg(pMAC1Addr,pMacInst->dwPhyAddr,DP83849_PHY_STATUS_REG,&wPhyReg);
	    pMacInst->wPhySpeed = (wPhyReg&0x02)?10:100;
	}
	else
	{
		pMacInst->wPhySpeed = 0;
	}

    return cRetCode;
}

UINT8 PHY_GetLinkStatus(pMacInformation_st pMacInst)
{
    UINT16 wPhyReg;
    UINT8 linkup_state = 0;
    pMac_st pMAC1Addr = (pMac_st)MAC1_BASE_ADDR;
    MAC_ReadPhyReg(pMAC1Addr,pMacInst->dwPhyAddr,IEEE_STATUS_REG,&wPhyReg); 
    if((wPhyReg & IEEE_STA_LINK_STATUS_BIT))
        linkup_state = 1;
    
    return linkup_state;
}



