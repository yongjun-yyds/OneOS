/*
 文件: uart.c
 版本: CMC693 V1.10
 版权: 浙江杰芯科技有限公司
 作者: zhulin(zhulin2@supcon.com)
 描述：
修改记录：
=========
日期                                      作者                       工作                            内容
2018-11-19		     朱林                       修改                           增加UART的中断模式配置
*/

#include "circlebuffer.h"
#include "uart.h"
#include "ckuart.h"
#include "intc.h"
#include "misc.h"

extern UINT32 APB_FREQ;

#define UARTID_MAX sizeof(Uart_Table)/sizeof(Struct_UartInfo)

Enum_Uart_Device consoleuart = CONFIG_TERMINAL_UART;

static void UART0_ISRHandler(UINT32 dwIrqid);
static void UART1_ISRHandler(UINT32 dwIrqid);

Struct_UartInfo Uart_Table[] = {
    {0,UART0_BASEADDR,FALSE,0,{"UART0",UART0_IRQID,0,UART0_ISRHandler,0,NULL}},
    {1,UART1_BASEADDR,FALSE,0,{"UART1",UART1_IRQID,0,UART1_ISRHandler,0,NULL}},
};

#if 0
/*
 * 使所有UART处于空闲模式
 * 输入参数：
 * 输出参数：
 */
void Deactive_UartModule(void)
{
    UINT8 i;
    pStruct_UartInfo pInfo;
    pStruct_UartType pUart;

    for(i = 0; i < UARTID_MAX; i++)
    {
    	pInfo = &(Uart_Table[i]);
        pUart = pInfo->addr;
    	pUart->LCR = 0x83;
    	pUart->RBR_THR_DLL = 0x1;
    	pUart->IER_DLH = 0x0;
    }
}
#endif

/*
 * 检测UART是否处于忙状态
 * 输入参数：pUart：UART0
 *               UART1
 * 输出参数：返回 1， 处于忙状态;
 *         返回0，处于空闲状态
 */
BOOL Uart_IsBusy(pStruct_UartType pUart)
{
	/* 本函数目前均是在uart初始化时调用，而UART模块在接收数据时也会将busy置位，所以
	 * 当在初始化时有接收数据，busy则置位，从而使得uart初始化死循环在该函数。这里暂且不
	 * 对busy位进行检测，直接返回0，使uart初始化顺利通过。若后续存在其他问题，或此问
	 * 题解决，则建议还是进行busy的检测。*/
	//return (pUart->USR & 0x01);
	return 0;
}

/*
 * 设置UART的FIFO
 * 输入参数：enUartID：UART0
 *                  UART1；
 *         dwMode:待配置的FIFO模式
 *                bit[7:6]:接收触发设置，00=接收FIFO中有一个字节时触发
 *                                   01= 达到接收FIFO的1/4时触发
 *                                   10= 达到接收FIFO的1/2时触发
 *                                   11= 达到接收FIFO-2时触发
 *                bit[5:4]:发送为空触发，00=发送FIFO全空时触发
 *                                   01=FIFO中小于等于2个字节时触发
 *                                   10=小于等于FIFO的1/4时触发
 *                                   11=小于等于FIFO的1/2时触发
 *                bit3：DMA模式，0=MODE0，DMA仅支持单个数据的传送
 *                             1=MODE1
 *                bit2：发送FIFO复位
 *                bit1:接收FIFO复位
 *                bit0：使能FIFO
 * 输出参数：返回 0， 成功; 其他，失败
 */
void Uart_SetFIFO(Enum_Uart_Device enUartID,UINT32 dwMode)
{
    pStruct_UartInfo pInfo;
    pStruct_UartType pUart;

    pInfo = &(Uart_Table[enUartID]);
    pUart = pInfo->addr;

    pUart->IIR_FCR = 0x06;                      /* 复位接收和发送FIFO */
    pUart->IIR_FCR = dwMode;
}

/*
 * 根据配置参数配置UART
 * 输入参数：pUartInfo： 串口配置信息结构体；
 * 输出参数：返回 0， 成功; 其他，失败
 * 说明：中断方式的接收及发送，是基于环形buff-circlebuffer的，若用户需要自定义中断
 *     处理函数，请参考本文件内默认的中断处理函数实现方法，方可正常使用本文件内的接收
 *     及发送函数接口。
 */
INT32U Uart_Config(pUART_InitStructure pUartInfo)
{
	Uart_Table[pUartInfo->UartId].UART_Initstruct.BaudRate = pUartInfo->BaudRate;
	Uart_Table[pUartInfo->UartId].UART_Initstruct.WordSize = pUartInfo->WordSize;
	Uart_Table[pUartInfo->UartId].UART_Initstruct.StopBit = pUartInfo->StopBit;
	Uart_Table[pUartInfo->UartId].UART_Initstruct.Parity = pUartInfo->Parity;
	Uart_Table[pUartInfo->UartId].UART_Initstruct.RxMode = pUartInfo->RxMode;
	Uart_Table[pUartInfo->UartId].UART_Initstruct.TxMode = pUartInfo->TxMode;

    Uart_ChangeBaudrate(pUartInfo->UartId,pUartInfo->BaudRate);
    Uart_SetWordSize(pUartInfo->UartId,pUartInfo->WordSize);
    Uart_SetStopBit(pUartInfo->UartId,pUartInfo->StopBit);
    Uart_SetParity(pUartInfo->UartId,pUartInfo->Parity);
    Uart_SetRXMode(pUartInfo->UartId,pUartInfo->RxMode);
    Uart_SetTXMode(pUartInfo->UartId,pUartInfo->TxMode);

    Uart_SetFIFO(pUartInfo->UartId,0x01);                                     /* 接收FIFO1个字节，发送FIFO空，DMA mode0，使能FIFO*/

    if((pUartInfo->RxMode == UART_Intr)||(pUartInfo->TxMode == UART_Intr))    /*中断模式*/
    {
    	if(pUartInfo->UART_ISR != NULL)                                       /* 若不为NULL，则将用户定义的中断函数注册到UART结构体信息变量中，否则使用默认的中断函数 */
    	{
    		Uart_Table[pUartInfo->UartId].irqhandler.handler = pUartInfo->UART_ISR;
    	}
        Uart_OpenIntr(pUartInfo->UartId,pUartInfo->Priority,NULL);
    }

    return SUCCESS;
}

/*
 * 打开串口，注册中断函数
 * 输入参数：enUartID：UART0
 *                  UART1
 *        dwPriority： 中断优先级
 *        handler：回调函数，可按照用户需求自行定义及使用
 * 输出参数：SUCCESS， 成功; FAILURE，失败
 */
INT8S Uart_OpenIntr(Enum_Uart_Device enUartID, UINT32 dwPriority, void (*handler)(INT8S error))
{
    pStruct_UartInfo pInfo;
    pStruct_UartType pUart;

    pInfo = &(Uart_Table[enUartID]);
    pUart = pInfo->addr;

    if((enUartID < 0) || (enUartID >= UARTID_MAX))
    {
	    return FAILURE;
    }

    if(pInfo->bopened)
    {
    	return FAILURE;
    }
#if UART_CIRCLEBUFFER_ENABLE
    /* intilize the sending and receiving buffers */
    CK_CircleBuffer_Init(&(pInfo->txcirclebuffer), pInfo->txbuffer, CK_UART_TXBUFFERSIZE);
    CK_CircleBuffer_Init(&(pInfo->rxcirclebuffer), pInfo->rxbuffer, CK_UART_RXBUFFERSIZE);
#endif
    pInfo->handler = handler;

    /* intilize irqhandler */
    pInfo->irqhandler.priority = dwPriority;

    INTC_FreeIrq(&(pInfo->irqhandler));

    /* register uart isr */
    INTC_RequestIrq(&(pInfo->irqhandler));
    pInfo->bopened = TRUE;
    return SUCCESS;
}

/*
 * 关闭串口中断
 * 输入参数：enUartID：UART0
 *                  UART1
 * 输出参数：SUCCESS， 成功; FAILURE，失败
 */
INT8S Uart_CloseIntr(Enum_Uart_Device enUartID)
{
    pStruct_UartInfo pInfo;
    pStruct_UartType pUart;

    pInfo = &(Uart_Table[enUartID]);
    pUart = pInfo->addr;
  
    if ((enUartID >= 0) && (enUartID < UARTID_MAX) && (pInfo->bopened))
    {
        /* Stop UART interrupt. */
        /* Clear TX & RX circle buffer. */
    	pUart->IER_DLH &= ~(IER_ERBFI | IER_ETBEI);
    	pInfo->handler = NULL;
        INTC_FreeIrq(&(pInfo->irqhandler));
        pInfo->bopened = 0;
        return SUCCESS;
    }
    return FAILURE;
}  

/*
 * 设置UART的波特率
 * 输入参数：enUartID：  UART0,
 *                   UART1
 *        enBaudrate:
 *                B600,
 *                B1200,
 *                B2400,
 *                B4800,
 *                B9600,
 *                B14400,
 *                B19200,
 *                B56000,
 *                B38400,
 *                B57600,
 *                B76800,
 *                B115200,
 *                B125000,
 *                B128000,
 *                B256000
 * 输出参数：SUCCESS， 成功; FAILURE，失败
 */
INT8S Uart_ChangeBaudrate(Enum_Uart_Device enUartID,Enum_Uart_Baudrate enBaudrate)
{
    UINT32 dwDiv;
    pStruct_UartInfo pInfo;
    pStruct_UartType pUart;

    pInfo = &(Uart_Table[enUartID]);
    pUart = pInfo->addr;

    while(Uart_IsBusy(pUart));

    if( (enBaudrate == B600) || (enBaudrate == B1200) || (enBaudrate == B2400)
        ||(enBaudrate == B4800) || (enBaudrate == B9600) || (enBaudrate == B14400)
    	|| (enBaudrate == B19200) || (enBaudrate == B38400) || (enBaudrate == B56000)
    	|| (enBaudrate == B57600) || (enBaudrate == B115200) || (enBaudrate == B125000)
    	|| (enBaudrate == B128000) || (enBaudrate == B256000))
    {
    	dwDiv = ((APB_FREQ / enBaudrate) >> 4);

    	pUart->LCR |= LCR_SET_DLAB;
    	pUart->RBR_THR_DLL = dwDiv & 0xFF;
    	pUart->IER_DLH = (dwDiv >> 8) & 0xFF;
    	pUart->LCR &= (~LCR_SET_DLAB);

		return SUCCESS;
    }

    return FAILURE;
}

/*
 * 设置UART的奇偶校验位
 * 输入参数：enUartID：UART0
 *                  UART1
 *        enParity： ODD   奇校验
 *                  EVEN  偶校验
 *                  NONE  无校验
 * 输出参数：SUCCESS， 成功; FAILURE，失败
 */
INT8S Uart_SetParity(Enum_Uart_Device enUartID, Enum_Uart_Parity enParity)
{
    pStruct_UartInfo pInfo;
    pStruct_UartType pUart;

    pInfo = &(Uart_Table[enUartID]);
    pUart = pInfo->addr;

	while(Uart_IsBusy(pUart));

	switch(enParity)
	{
		case NONE:
			/*CLear the PEN bit(LCR[3]) to disable parity.*/
			pUart->LCR &= (~LCR_PARITY_ENABLE);
		break;
		case ODD:
			/* Set PEN and clear EPS(LCR[4]) to set the ODD parity. */
			pUart->LCR |= LCR_PARITY_ENABLE;
			pUart->LCR &= LCR_PARITY_ODD;
		break;
		case EVEN:
			/* Set PEN and EPS(LCR[4]) to set the EVEN parity.*/
			pUart->LCR |= LCR_PARITY_ENABLE;
			pUart->LCR |= LCR_PARITY_EVEN;
		break;
		default:
			return FAILURE;
		break;
    }

	return SUCCESS;
}

/*
 * 设置UART的停止位，可以设置1位停止位或者2位停止位。当设置2位停止位时，如果数据长度设置为5
 * ，即LCR[1:0]为0，则实际发送1.5个停止位。
 * 输入参数：enUartID:UART0
 *                  UART1
 *        enStopBit： LCR_STOP_BIT_1
 *                   LCR_STOP_BIT_2
 * 输出参数：SUCCESS， 成功; FAILURE，失败
 */
INT8S Uart_SetStopBit(Enum_Uart_Device enUartID, Enum_Uart_Error enStopBit)
{
    pStruct_UartInfo pInfo;
    pStruct_UartType pUart;

    pInfo = &(Uart_Table[enUartID]);
    pUart = pInfo->addr;

	while(Uart_IsBusy(pUart));
	
	switch(enStopBit)
	{
		case LCR_STOP_BIT_1:
			/* Clear the STOP bit to set 1 stop bit*/
			pUart->LCR &= LCR_STOP_BIT1;
		break;
		case LCR_STOP_BIT_2:
			/*
			 * If the STOP bit is set "1",we'd gotten 1.5 stop
			 * bits when DLS(LCR[1:0]) is zero, else 2 stop bits.
			 */
			pUart->LCR |= LCR_STOP_BIT2;
		break;
		default:
			return FAILURE;
		break;
	}

    return SUCCESS;
}

/*
 * 设置UART的数据域长度
 * 输入参数：enUartID:UART0
 *                  UART1
 *        enWordSize： WORD_SIZE_5
 *                    WORD_SIZE_6
 *                    WORD_SIZE_7
 *                    WORD_SIZE_8
 * 输出参数：SUCCESS， 成功; FAILURE，失败
 */
INT8S Uart_SetWordSize(Enum_Uart_Device enUartID,Enum_Uart_WordSize enWordSize)
{
    pStruct_UartInfo pInfo;
    pStruct_UartType pUart;

    pInfo = &(Uart_Table[enUartID]);
    pUart = pInfo->addr;

    while(Uart_IsBusy(pUart)){};

	switch(enWordSize)
	{
		case WORD_SIZE_5:
			pUart->LCR &= LCR_WORD_SIZE_5;
		break;
		case WORD_SIZE_6:
			pUart->LCR &= 0xfd;
			pUart->LCR |= LCR_WORD_SIZE_6;
		break;
		case WORD_SIZE_7:
			pUart->LCR &= 0xfe;
			pUart->LCR |= LCR_WORD_SIZE_7;
		break;
		case WORD_SIZE_8:
			pUart->LCR |= LCR_WORD_SIZE_8;
		break;
		default:
			return FAILURE;
		break;
	}

	return SUCCESS;
}

/*
 * 设置UART的发送模式，轮询模式还是中断模式
 * 输入参数：enUartID：UART0
 *                  UART1
 *        enMode： UART_Query    轮询模式
 *                UART_Intr     中断模式
 * 输出参数：SUCCESS， 成功; FAILURE，失败
 */
UINT8 Uart_SetTXMode(Enum_Uart_Device enUartID, Enum_Uart_Mode enMode)
{
    pStruct_UartInfo pInfo;
    pStruct_UartType pUart;

    pInfo = &(Uart_Table[enUartID]);
    pUart = pInfo->addr;

    while(Uart_IsBusy(pUart));

    if(enMode == UART_Query)                                        /* 轮询模式 */
    {
    	pUart->IER_DLH &= (~IER_ETBEI);
        /* Refresh the uart info: transmit mode - query.*/
    	pInfo->btxquery = TRUE;
    }
    else if(enMode == UART_Intr)                                    /* 中断模式 */
    {
    	pUart->IER_DLH |= IER_ETBEI;
        /* Refresh the uart info: transmit mode - interrupt.*/
    	pInfo->btxquery = FALSE;
    }
    else
    {
    	return FAILURE;
    }
    return SUCCESS;
}

/*
 * 设置UART的接收模式，轮询模式还是中断模式
 * 输入参数：enUartID：UART0
 *                  UART1
 *        enMode： UART_Query    轮询模式
 *                UART_Intr     中断模式
 * 输出参数：SUCCESS， 成功; FAILURE，失败
 */
INT8S Uart_SetRXMode(Enum_Uart_Device enUartID, Enum_Uart_Mode enMode)
{
    pStruct_UartInfo pInfo;
    pStruct_UartType pUart;

    pInfo = &(Uart_Table[enUartID]);
    pUart = pInfo->addr;

    while(Uart_IsBusy(pUart));

    if(enMode == UART_Query)                                    /* 轮询模式 */
    {
        pUart->IER_DLH &= (~IER_ERBFI);
        /* Refresh the uart info: receive mode - query.*/
        pInfo->brxquery = TRUE;
        pInfo->bopened = TRUE;                                 /* add zhulin */
    }
    else if(enMode == UART_Intr)                               /* 中断模式 */
    {
    	pUart->IER_DLH |= IER_ERBFI;
        /* Refresh the uart info: receive mode - interrupt.*/
    	pInfo->brxquery = FALSE;
    }
    else
    {
    	return FAILURE;
    }
  return SUCCESS;
}
	
/*
 * 从指定串口接收一个字符，轮询模式下会阻塞在此函数，建议使用中断模式
 * 输入参数：enUartID：UART0
 *                  UART1
 *         ch：存放待接收的字符
 * 输出参数：SUCCESS， 成功; FAILURE，失败
 */
INT8S Uart_GetChar(Enum_Uart_Device enUartID, UINT8 *ch)
{
    pStruct_UartInfo pInfo;
    pStruct_UartType pUart;
  
    if((enUartID < 0) || (enUartID >= UARTID_MAX))
    {
        return FAILURE;
    }
    pInfo = &(Uart_Table[enUartID]);
    pUart = pInfo->addr;

    if(!(pInfo->bopened))
    {
    	return FAILURE;
    }

    /*query mode*/
    if(pInfo->brxquery)
    {
        while (!(pUart->LSR & LSR_DR));
        *ch = pUart->RBR_THR_DLL;
        return SUCCESS;
    }
    else
    {
#if UART_CIRCLEBUFFER_ENABLE
    	/*irq mode*/
        if(TRUE == CK_CircleBuffer_Read(&(pInfo->rxcirclebuffer), ch))
        return SUCCESS;
#endif
    }
    return FAILURE;
}

/*
 * 从指定串口发送一个字符
 * 输入参数：enUartID：UART0
 *                  UART1
 *         ch：待发送的字符
 * 输出参数：SUCCESS， 成功; FAILURE，失败
 */
INT8S Uart_PutChar(Enum_Uart_Device enUartID, UINT8 ch)
{
    pStruct_UartInfo pInfo;
    pStruct_UartType pUart;

    if((enUartID < 0) || (enUartID >= UARTID_MAX))
    {
        return FAILURE;
    }

    pInfo = &(Uart_Table[enUartID]);
    pUart = pInfo->addr;

    /*query mode*/
    if(pInfo->btxquery)
    {
        while (!(pUart->LSR & LSR_TRANS_EMPTY));
        pUart->RBR_THR_DLL = ch;

        return SUCCESS;
    }
    else  /*irq mode*/
    {
#if UART_CIRCLEBUFFER_ENABLE
    	UINT8 temp;
		if(!CK_CircleBuffer_Write(&(pInfo->txcirclebuffer), ch))
		{
			return FAILURE;
		}

        if(pUart->LSR & 0x20)
        {
            CK_CircleBuffer_Read(&(pInfo->txcirclebuffer), &temp);
            pUart->RBR_THR_DLL = temp;
        }
#endif
    }
    return SUCCESS;
}

/*
 * UART0中断处理函数
 * 输入参数：dwIrqid 中断号
 * 输出参数：
 */
static void UART0_ISRHandler(UINT32 dwIrqid)
{
    pStruct_UartInfo pInfo;
    pStruct_UartType pUart;
    UINT8 cState;
    UINT8 cData;

    pInfo = &(Uart_Table[UART0]);
    pUart = pInfo->addr;

    cState = pUart->IIR_FCR & 0xf;                               /* 获取中断标号 */
	if((cState == 0x2) && !(pInfo->btxquery))
	{
#if UART_CIRCLEBUFFER_ENABLE
	    if(!CK_CircleBuffer_IsEmpty(&(pInfo->txcirclebuffer)))
	    {
	        CK_CircleBuffer_Read(&(pInfo->txcirclebuffer), &cData);
	        pUart->RBR_THR_DLL = cData;
	    }
#endif
	}
	if((cState == 0x4) && !(pInfo->brxquery))
	{
		cData = pUart->RBR_THR_DLL;
#if UART_CIRCLEBUFFER_ENABLE
	    CK_CircleBuffer_Write(&(pInfo->rxcirclebuffer), cData);
#endif
	}
}

/*
 * UART1中断处理函数
 * 输入参数：dwIrqid 中断号
 * 输出参数：
 */
static void UART1_ISRHandler(UINT32 dwIrqid)
{
    pStruct_UartInfo pInfo;
    pStruct_UartType pUart;
    UINT8 cState;
    UINT8 cData;

    pInfo = &(Uart_Table[UART1]);
    pUart = pInfo->addr;

    cState = pUart->IIR_FCR & 0xf;                               /* 获取中断标号 */
	if((cState == 0x2) && !(pInfo->btxquery))
	{
#if UART_CIRCLEBUFFER_ENABLE
	    if(!CK_CircleBuffer_IsEmpty(&(pInfo->txcirclebuffer)))
	    {
	        CK_CircleBuffer_Read(&(pInfo->txcirclebuffer), &cData);
	        pUart->RBR_THR_DLL = cData;
	    }
#endif
	}
	if((cState == 0x4) && !(pInfo->brxquery))
	{
		cData = pUart->RBR_THR_DLL;
#if UART_CIRCLEBUFFER_ENABLE
	    CK_CircleBuffer_Write(&(pInfo->rxcirclebuffer), cData);
#endif
	}
}

#if UART_CIRCLEBUFFER_ENABLE
/*
 * 清除UART的环形buff
 * 输入参数：enUartID：UART0
 *                  UART1
 * 输出参数：
 */
void UART_ClearRxBuffer(Enum_Uart_Device enUartID)
{
    pStruct_UartInfo pInfo;

    pInfo = &(Uart_Table[UART0]);
    CK_CircleBuffer_Clear(&(pInfo->rxcirclebuffer));
}
#endif
