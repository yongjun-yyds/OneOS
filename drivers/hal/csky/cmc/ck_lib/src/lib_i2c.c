/*
 文件: i2c.c
 版本: CMC693 V1.10
 版权: 浙江杰芯科技有限公司
 作者: zhulin(zhulin2@supcon.com)
 描述：
修改记录：
=========
日期                                      作者                       工作                            内容
*/

#include "lib_i2c.h"
#include "powm.h"
#include "intc.h"

Struct_IICInfo IIC0;

void IIC_ISRHandler(UINT32 irqid);

Struct_IRQHandler IIC_Intr={"IIC",CK_INTC_I2C,0,IIC_ISRHandler,0,0};

/*
 * IIC 锁定当前线程
 * 输入参数：pIICInfo： IIC信息结构体指针
 * 输出参数：返回 TRUE， 上锁成功; FALSE，上锁失败。
 * 说明：每次收发开始时上锁，获取权限，收发完成后释放权限。
 */
static BOOL IIC_Lock(pStruct_IICInfo pIICInfo)
{
	UINT16 wLock = 0;

	if (pIICInfo->wCASLock != 0)
	{
		return FALSE;
	}

	wLock = pIICInfo->wCASLock;
	++pIICInfo->wCASLock;

	if (++wLock != pIICInfo->wCASLock)
	{
		return FALSE;
	}

	return TRUE;
}

/*
 * IIC 解锁
 * 输入参数：pIICInfo： IIC信息结构体指针
 * 输出参数：无
 * 说明：每次收发开始时上锁，获取权限，收发完成后释放权限。
 */
void IIC_UnLock(pStruct_IICInfo pIICInfo)
{
	while (pIICInfo->wCASLock)
	{
		pIICInfo->wCASLock = 0;
	}
}

/*
 * 清除IIC中断
 * 输入参数：pIICInfo： IIC信息结构体指针
 *        wTntrVector:产生的中断
 * 输出参数：返回 读到的寄存器值
 * 说明：每次收发开始时上锁，获取权限，收发完成后释放权限。
 */
UINT16 IIC_ClearInterrupt(pStruct_IICInfo pIICInfo, UINT16 wTntrVector)
{
	UINT16 wTemp = 0;

	if (wTntrVector & IIC_INT_RXFIFO_UNDER ) //读寄存器清除RX_UNDER中断
	{
		wTemp = pIICInfo->IIC_BaseAddr->IC_CLR_RX_UNDER;
	}

	if (wTntrVector & IIC_INT_RXFIFO_OVER ) //读寄存器清除RX_OVER中断
	{
		wTemp = pIICInfo->IIC_BaseAddr->IC_CLR_RX_OVER;
	}

	if (wTntrVector & IIC_INT_TXFIFO_OVER ) //读寄存器清除TX_OVER中断
	{
		wTemp = pIICInfo->IIC_BaseAddr->IC_CLR_TX_OVER;
	}

	if (wTntrVector & IIC_INT_SRD_RES ) //读寄存器清除RD_REQ中断
	{
		wTemp = pIICInfo->IIC_BaseAddr->IC_CLR_RD_REQ;
	}

	if (wTntrVector & IIC_INT_TX_ABRT ) //读寄存器清除TX_ABRT中断，清除IC_TX_ABRT_SOURCE寄存器
	{
		wTemp = pIICInfo->IIC_BaseAddr->IC_CLR_TX_ABRT;
	}

	if (wTntrVector & IIC_INT_STX_DONE ) //读寄存器清除RX_DONE中断
	{
		wTemp = pIICInfo->IIC_BaseAddr->IC_CLR_RX_DONE;
	}

	if (wTntrVector & IIC_INT_ACTIVITY ) //读寄存器获得ACTIVITY中断状态，此中断硬件自动清除
	{
		wTemp = pIICInfo->IIC_BaseAddr->IC_CLR_ACTIVITY;
	}

	if (wTntrVector & IIC_INT_STOP_DET ) //读寄存器清除STOP_DET中断
	{
		wTemp = pIICInfo->IIC_BaseAddr->IC_CLR_STOP_DET;
	}

	if (wTntrVector & IIC_INT_START_DET ) //读寄存器清除START_DET中断
	{
		wTemp = pIICInfo->IIC_BaseAddr->IC_CLR_START_DET;
	}

	if (wTntrVector & IIC_INT_GEN_CALL ) //读寄存器清除GEN_CALL中断
	{
		wTemp = pIICInfo->IIC_BaseAddr->IC_CLR_GEN_CALL;
	}

	return wTemp;
}

/*
 * 配置SCL的高低电平累计值
 * 输入参数：pIICInfo： IIC信息结构体指针
 *        dwApbClk APB时钟频率
 * 输出参数：是否配置成功-1失败，0成功
 * 说明：根据系统频率配置SCL的高低电平的累计值。
 */
static int IIC_ConfigSCL(pStruct_IICInfo pIICInfo, UINT32 dwApbClk)
{
	if (dwApbClk < 1000000 || dwApbClk > 100000000)
	{
		return -1;
	}

	//10M频率情况下，依次配置为：0x28, 0x2F, 0x06, 0x0D;
	//按此规律计算不同频率下相应的参数值 5000000 10000000为固定值
	pIICInfo->IIC_BaseAddr->IC_SS_SCL_HCNT = (0x28 * dwApbClk + 5000000) / 10000000;
	pIICInfo->IIC_BaseAddr->IC_SS_SCL_LCNT = (0x2F * dwApbClk + 5000000) / 10000000;

	pIICInfo->IIC_BaseAddr->IC_FS_SCL_HCNT = (0x06 * dwApbClk + 5000000) / 10000000;
	pIICInfo->IIC_BaseAddr->IC_FS_SCL_LCNT = (0x0D * dwApbClk + 5000000) / 10000000;

	return 0;
}

/*
 * IIC 重复起始信号使能
 * 输入参数：pIICInfo： IIC信息结构体指针
 * 输出参数：无
 */
static void IIC_EnableRestart(pStruct_IICInfo pIICInfo)
{
	pIICInfo->IIC_BaseAddr->IC_CON |= 1 << 5;
}

/*
 *IIC 屏蔽中断
 * 输入参数：pIICInfo： IIC信息结构体指针
 * 输出参数：无
 */
static void IIC_DisableInterrupt(pStruct_IICInfo pIICInfo)
{
	pIICInfo->IIC_BaseAddr->IC_INTR_MASK = IIC_INT_NONE;
}

/*
 * IIC 使能中断
 * 输入参数：pIICInfo：IIC信息结构体信息
 * 输出参数：无
 */
static void IIC_EnableInterrupt(pStruct_IICInfo pIICInfo, UINT16 wTntrVector)
{
	pIICInfo->IIC_BaseAddr->IC_INTR_MASK = wTntrVector;
}

/*
 * IIC 设置目标地址
 * 输入参数：pIICInfo： IIC信息结构体指针
 *        wTargetAddr目的地址，如果要指定10bit地址，可以将地址bit15置位.
 * 输出参数：无
 */
static void IIC_SetTargetAddr(pStruct_IICInfo pIICInfo, UINT16 wTargetAddr)
{
	if( wTargetAddr > 0x7F )
	{
		pIICInfo->IIC_BaseAddr->IC_CON |= (0x01 << 4);//10位地址模式
	}
	else
	{
		pIICInfo->IIC_BaseAddr->IC_CON &= ~(0x01 << 4);//7位地址模式
	}

	pIICInfo->IIC_BaseAddr->IC_TAR = wTargetAddr & 0x3FF; 	//配置寄存器
}

/*
 * 设置缓存数量的中断阈值
 * 输入参数：pIICInfo：IIC信息结构体指针
 * 输出参数：返回  设置结果 -1失败，0成功
 */
static int IIC_SetFTLevel(pStruct_IICInfo pIICInfo)
{
	if ((pIICInfo->IIC_Initstruct.RxFTLevel > 7) ||
		(pIICInfo->IIC_Initstruct.TxFTLevel > 7))
	{
		return -1;
	}

	pIICInfo->IIC_BaseAddr->IC_RX_TL = pIICInfo->IIC_Initstruct.RxFTLevel;
	pIICInfo->IIC_BaseAddr->IC_TX_TL = pIICInfo->IIC_Initstruct.TxFTLevel;

	return 0;
}

/*
 *  设置IIC地址
 * 输入参数：pIICInfo： IIC信息结构体指针
 * 输出参数：返回  设置结果 -1失败，0成功
 * 说明：根据7位和10位地址进行区分(为简化管理，主从都采用相同的地址方式)
 */
static int IIC_SetAddr(pStruct_IICInfo pIICInfo)
{
	if (pIICInfo->IIC_Initstruct.IIC_AddrMode == IIC_AddressMode_7bits)
	{
		pIICInfo->IIC_BaseAddr->IC_CON &= ~(0x3 << 3);
		pIICInfo->IIC_BaseAddr->IC_SAR = pIICInfo->IIC_Initstruct.IIC_Addr & 0x7F;
	}
	else if (pIICInfo->IIC_Initstruct.IIC_AddrMode == IIC_AddressMode_10bits)
	{
		pIICInfo->IIC_BaseAddr->IC_CON |= (0x3 << 3);
		pIICInfo->IIC_BaseAddr->IC_SAR = pIICInfo->IIC_Initstruct.IIC_Addr & 0x3FF;
	}
	else
	{
		return -1;
	}

	if( (pIICInfo->IIC_BaseAddr->IC_SAR & 0x3FF) == 0)
	{
		return -1;
	}

	return 0;
}

/*
 * 确定IIC设备角色
 * 输入参数：pIICInfo： IIC信息结构体指针
 * 输出参数：返回  设置结果 -1失败，0成功
 * 说明：三种角色： 主/从/主从
 */
static int IIC_SetCharacter(pStruct_IICInfo pIICInfo)
{
	if (pIICInfo->IIC_Initstruct.IIC_Character == IIC_Character_Slave)               /* 设置为从模式 */
	{
		pIICInfo->IIC_BaseAddr->IC_CON &= ~((0x0001 << 0) | (0x0001 << 6));
	}
	else if (pIICInfo->IIC_Initstruct.IIC_Character == IIC_Character_Master)         /* 设置为主模式 */
	{
		pIICInfo->IIC_BaseAddr->IC_CON |= ((0x0001 << 0) | (0x0001 << 6));
	}
	else if  (pIICInfo->IIC_Initstruct.IIC_Character == IIC_Character_SlaveMaster)   /* 设置为主从模式 */
	{
		pIICInfo->IIC_BaseAddr->IC_CON |= (0x0001 << 0);
		pIICInfo->IIC_BaseAddr->IC_CON &= ~(0x0001 << 6);
	}
	else
	{
		return -1;
	}

	return 0;
}

/*
 * 确定IIC速度模式
 * 输入参数：pIICInfo： IIC信息结构体指针
 * 输出参数：返回  设置结果 -1失败，0成功
 * 说明：目前支持： 标准模式 / 快速模式
 */
static int IIC_SetSpeedMode(pStruct_IICInfo pIICInfo)
{
	UINT16 wTemp;

	wTemp = pIICInfo->IIC_BaseAddr->IC_CON;
	wTemp &= ~(0x03 << 1);

	if(pIICInfo->IIC_Initstruct.IIC_SpeedMode == IIC_SpeedMode_Standard)
	{
		wTemp |= (0x01 << 1);
	}
	else if(pIICInfo->IIC_Initstruct.IIC_SpeedMode == IIC_SpeedMode_Fast)
	{
		wTemp |= (0x02 << 1);
	}
	else
	{
		return -1;
	}

	pIICInfo->IIC_BaseAddr->IC_CON = wTemp;
	return 0;
}

/*
 * 从接收缓存中读取数据
 * 输入参数：pIICInfo： IIC信息结构体指针
 *        cRxLevel 需要读取的缓存数目(0不指定数量，表示读空当前缓存)
 * 输出参数：返回  读取到数据长度
 */
int IIC_Read_RxFIFO(pStruct_IICInfo pIICInfo, UINT8 cRxLevel)
{
	UINT16 wRxIndex = pIICInfo->IIC_Initstruct.IIC_RxBuffer.index;
	UINT16 wBufferSize = pIICInfo->IIC_Initstruct.IIC_RxBuffer.size;
	UINT8 cReadCnt = 0;

	cReadCnt = wBufferSize - wRxIndex;
	if(cRxLevel)
	{
		cReadCnt = (cReadCnt >= cRxLevel) ? cRxLevel : cReadCnt;
	}

	while( ( !IIC_IS_RXFIFO_EMPTY(pIICInfo) ) &&
		   ( cReadCnt-- ) )
	{
		cRxLevel = pIICInfo->IIC_BaseAddr->IC_DATA_CMD;

		if(wRxIndex < wBufferSize)
		{
			*(pIICInfo->IIC_Initstruct.IIC_RxBuffer.pBuffer + wRxIndex) = cRxLevel;
			wRxIndex++;
		}
		else
		{
			break;
		}
	}

	cRxLevel = (wRxIndex - pIICInfo->IIC_Initstruct.IIC_RxBuffer.index);//计算读取长度
	pIICInfo->IIC_Initstruct.IIC_RxBuffer.index = wRxIndex;//更新索引

	return cRxLevel;
}

/*
 * 从发送缓存中发送数据
 * 输入参数：pIICInfo： IIC信息结构体指针
 *        cTxLevel 需要写入的缓存数目(0不指定数量，表示写满当前缓存)
 * 输出参数：返回  实际写入长度
 * 说明：16位长度，理论上数据长度不能超过65535/2，否则会导致函数中相加计算出错。
 */
int IIC_Write_TxFIFO(pStruct_IICInfo pIICInfo, UINT8 cTxLevel)
{
	UINT16 wTxIndex = pIICInfo->IIC_Initstruct.IIC_TxBuffer.index;
	UINT16 wTxSize  = pIICInfo->IIC_Initstruct.IIC_TxBuffer.size;
	UINT16 wRxSize  = pIICInfo->IIC_Initstruct.IIC_RxBuffer.size;

	if ( (!cTxLevel) ||
		 ( cTxLevel > (wTxSize + wRxSize - wTxIndex) ) )
	{
		cTxLevel = (wTxSize + wRxSize - wTxIndex);
	}

	//如果已经填充完，还要继续填入Tx FIFO，则填充0
	if(!cTxLevel)
	{
		return 0;
	}

	while ( !IIC_IS_TXFIFO_FULL(pIICInfo) &&
			(cTxLevel--) )
	{
		if (wTxIndex < wTxSize )
		{
			pIICInfo->IIC_BaseAddr->IC_DATA_CMD = *(pIICInfo->IIC_Initstruct.IIC_TxBuffer.pBuffer + wTxIndex);
		}
		else if (wTxIndex < (wTxSize + wRxSize) )
		{
			pIICInfo->IIC_BaseAddr->IC_DATA_CMD = 0x100;
		}
		else
		{
			break;
		}
		wTxIndex++;
	}

	if (wTxIndex >= (wTxSize + wRxSize) )
	{
		pIICInfo->IIC_BaseAddr->IC_INTR_MASK &= ~IIC_INT_TXFTL_BELOW;//禁止该中断源
	}

	cTxLevel = wTxIndex - pIICInfo->IIC_Initstruct.IIC_TxBuffer.index;
	pIICInfo->IIC_Initstruct.IIC_TxBuffer.index = wTxIndex;

	return cTxLevel;
}

/*
 * IIC 虚写 不关心写入内容
 * 输入参数：pIICInfo, IIC信息结构体指针
 *        cTxLevel 写入长度
 * 输出参数：无
 * 说明：cTxLevel = 0 时表示写满Tx FIFO
 */
void IIC_DummyWrite(pStruct_IICInfo pIICInfo, UINT8 cTxLevel)
{
	if(!cTxLevel)
	{
		cTxLevel = 0xFF;
	}

	while ( !IIC_IS_TXFIFO_FULL(pIICInfo) &&
			(cTxLevel--) )
	{
		pIICInfo->IIC_BaseAddr->IC_DATA_CMD = 0x00;
	}
}

/*
 * IIC关闭
 * 输入参数：pIICInfo, IIC信息结构体指针
 * 输出参数：返回 -1关闭失败，0关闭成功
 */
int IIC_Close(pStruct_IICInfo pIICInfo)
{
	UINT16 wTemp;

	if (pIICInfo == NULL)
	{
		return -1;
	}

	IIC_DisableInterrupt(pIICInfo);//关闭所有中断

	wTemp = pIICInfo->IIC_BaseAddr->IC_CLR_INTR & 0x10;         /* 清除所有中断 */

	pIICInfo->IIC_BaseAddr->IC_ENABLE = wTemp;                 /* 关闭IIC 此时自动清除Rx FIFO， Tx FIFO */

	return 0;
}

/*
 * IIC打开
 * 输入参数：pIICInfo, IIC信息结构体指针
 * 输出参数：返回 -1打开失败 0打开成功
 */
int IIC_Open(pStruct_IICInfo pIICInfo)
{
	UINT16 wTtemp;

	if (pIICInfo == NULL)
	{
		return -1;
	}

	if ( (pIICInfo->IIC_BaseAddr->IC_ENABLE & IIC_ENABLE) == IIC_ENABLE)
	{
		return 0;
	}

	pIICInfo->IIC_Initstruct.IIC_RxBuffer.index = 0;
	pIICInfo->IIC_Initstruct.IIC_TxBuffer.index = 0;

	wTtemp = pIICInfo->IIC_BaseAddr->IC_CLR_INTR;

	pIICInfo->IIC_BaseAddr->IC_ENABLE = wTtemp | IIC_ENABLE;

	return 0;
}

/*
 * IIC 初始化
 * 输入参数：pIICInfo, IIC信息结构体指针
 * 输出参数：返回 0， 成功; 其他，失败。
 */
int IIC_InitConfig(pStruct_IICInfo pIICInfo)
{

	if (pIICInfo == NULL)
	{
		return -1;
	}

	IIC_Close(pIICInfo);
	pIICInfo->wErrFlag = 0;

	if ( IIC_SetFTLevel(pIICInfo) )           /* FIFO阈值设定 */
	{
		pIICInfo->wErrFlag |= (1 << 2);
		return -2;
	}

	if ( IIC_SetCharacter(pIICInfo) )         /* IIC 设备角色 */
	{
		pIICInfo->wErrFlag |= (1 << 4);
		return -4;
	}

	if ( IIC_SetAddr(pIICInfo) )              /* IIC 设备地址 */
	{
		pIICInfo->wErrFlag |= (1 << 3);
		return -3;
	}

	if ( IIC_SetSpeedMode(pIICInfo) )         /* IIC 速度模式 */
	{
		pIICInfo->wErrFlag |= (1 << 5);
		return -5;
	}

	if ( IIC_ConfigSCL(pIICInfo, APB_FREQ) )  /* IIC SCL配置 */
	{
		pIICInfo->wErrFlag |= (1 << 6);
		return -6;
	}

	IIC_EnableRestart(pIICInfo);              /* 使能 重复起始信号 */

	IIC_UnLock(pIICInfo);

	return 0;
}

/*
 * IIC 设置参数
 * 输入参数：pIICInfo      IIC信息结构体指针
 *        wIIC_Addr     本机IIC地址
 * 		  IIC_Character IIC设备角色
 *        IIC_AddrMode  地址模式，
 *	      IIC_SpeedMode 速度模式
 * 输出参数：返回 0， 成功; 其他，失败。
 * 说明：wIIC_Addr = 0 时不改变IIC地址。
 */
int IIC_SetParameters(pStruct_IICInfo pIICInfo,
		              UINT16 wIIC_Addr,
					  Enum_IIC_Character IIC_Character,
					  Enum_IIC_AddressMode IIC_AddrMode,
					  Enum_IIC_SpeedMode IIC_SpeedMode )
{
	if( pIICInfo == NULL)
	{
		return -1;
	}

	//如果没有获取操作权限，退出
	if( !IIC_Lock(pIICInfo) )
	{
		return -2;
	}

	IIC_Close(pIICInfo);

	if ( (wIIC_Addr) &&
		 (wIIC_Addr < 0x3FF) &&
		 (pIICInfo->IIC_Initstruct.IIC_Addr != wIIC_Addr) )
	{
		pIICInfo->IIC_Initstruct.IIC_Addr = wIIC_Addr;

		if ( IIC_SetAddr(pIICInfo) )//重新配置 IIC设备地址
		{
			IIC_UnLock(pIICInfo);
			return -3;
		}
	}

	if ( pIICInfo->IIC_Initstruct.IIC_AddrMode != IIC_AddrMode )
	{
		pIICInfo->IIC_Initstruct.IIC_AddrMode = IIC_AddrMode;

		if ( IIC_SetAddr(pIICInfo) )//重新配置 IIC设备地址
		{
			IIC_UnLock(pIICInfo);
			return -4;
		}
	}

	if ( (UINT8)IIC_Character <= (UINT8)IIC_Character_SlaveMaster)
	{
		if (pIICInfo->IIC_Initstruct.IIC_Character != IIC_Character)
		{
			pIICInfo->IIC_Initstruct.IIC_Character = IIC_Character;
			if (IIC_SetCharacter(pIICInfo) )
			{
				IIC_UnLock(pIICInfo);
				return -5;
			}
		}
	}

	if (pIICInfo->IIC_Initstruct.IIC_SpeedMode != IIC_SpeedMode)
	{
		pIICInfo->IIC_Initstruct.IIC_SpeedMode = IIC_SpeedMode;
		if ( IIC_SetSpeedMode(pIICInfo) )//重新配置 IIC速度模式
		{
			IIC_UnLock(pIICInfo);
			return -6;
		}
	}

	IIC_UnLock(pIICInfo);
	return 0;
}

/*
 * IIC 【阻塞方式】进行数据收发
 * 输入参数：pIICInfo, 	IIC信息结构体指针
 * 		  wTargetAddr 	目的地址
 * 		  pcTxBuffer  	发送缓存指针 （如不使用，可指定为NULL ）
 * 		  wTxLength   	发送数据长度 （如不使用，可指定为0 ）
 * 		  pcRxBuffer		接收数据指针 （如不使用，可指定为NULL ）
 * 		  wRxLength   	接收数据长度 （如不使用，可指定为0 ）
 * 输出参数： > 0 实际接收到的数据长度 ; <= 0 出错.
 * 说明：TargetAddr 最高位置位时，显式指定10bit地址。如果当前IIC模块正处于活动状态，则放弃本次操作。
 */
int IIC_Transceive( pStruct_IICInfo pIICInfo,  UINT16 wTargetAddr,
					UINT8  *pcTxBuffer, UINT16 wTxLength,
					UINT8  *pcRxBuffer, UINT16 wRxLength)
{
	UINT16 wTxCnt = 0, wRxCnt = 0;
	BOOL bTxComplete_Flag = FALSE, bRxComplete_Flag = FALSE;

	if((pIICInfo == NULL) || (wTargetAddr == 0))
	{
		return -1;
	}

	if(pIICInfo->IIC_Initstruct.IIC_Character == IIC_Character_Slave)
	{
		return -2;
	}

	/* 如果没有获取操作权限，退出 */
	if( !IIC_Lock(pIICInfo) )
	{
		return -3;
	}

	/* 如果不需要发送数据，Tx直接表示完成。 */
	if( (pcTxBuffer == NULL) ||
		(wTxLength  == 0)     )
	{
		wTxLength = 0;
		bTxComplete_Flag = TRUE;
	}
	else
	{
		bTxComplete_Flag = FALSE;
	}

	/* 不需要接收数据 */
	if( (pcRxBuffer == NULL) ||
		(wRxLength  == 0)     )
	{
		wRxLength = 0;
		bRxComplete_Flag = TRUE;
	}
	else
	{
		bRxComplete_Flag = FALSE;
	}

	/* 更新TAR */
	if( (pIICInfo->IIC_BaseAddr->IC_TAR & 0x3FF) != (wTargetAddr & 0x3FF) )
	{
		IIC_Close(pIICInfo);						        /* 关闭IIC */
		IIC_SetTargetAddr(pIICInfo, wTargetAddr); 		    /* 配置目标地址 */
	}

	IIC_DisableInterrupt(pIICInfo);						    /* 关闭中断 */

	IIC_Open(pIICInfo);					            		/* 打开IIC */

	while ( (!bRxComplete_Flag) || (!bTxComplete_Flag) )
	{
		while ( !IIC_IS_TXFIFO_FULL(pIICInfo) )
		{
			if (!bTxComplete_Flag)
			{
				if ( wTxCnt++ >= wTxLength )
				{
					wTxCnt = 0;
					bTxComplete_Flag = TRUE;//发送完成标志
				}
				else
				{
					pIICInfo->IIC_BaseAddr->IC_DATA_CMD = *(pcTxBuffer++);
				}
			}
			else //发送完成，开始读取数据
			{
				if ( wTxCnt++ >= wRxLength )
				{
					bRxComplete_Flag = TRUE;
					break ;
				}
				else
				{
					pIICInfo->IIC_BaseAddr->IC_DATA_CMD = 0x100;
				}
			}

		}

		/* 读取 RxFIFO中剩余数据 */
		while ( !IIC_IS_RXFIFO_EMPTY(pIICInfo) )
		{
			*pcRxBuffer++ = pIICInfo->IIC_BaseAddr->IC_DATA_CMD;
			wRxCnt++;
		}

	}

	//等待传输完成
	while ( ( !IIC_IS_TXFIFO_EMPTY(pIICInfo) ) ||
			( !IIC_IS_IDLE(pIICInfo)         )  )
	{
		while ( !IIC_IS_RXFIFO_EMPTY(pIICInfo) )
		{
			*pcRxBuffer++ = pIICInfo->IIC_BaseAddr->IC_DATA_CMD;
			wRxCnt++;
		}
	}

	//读取 RxFIFO中剩余数据
	while( !IIC_IS_RXFIFO_EMPTY(pIICInfo) )
	{
		*pcRxBuffer++ = pIICInfo->IIC_BaseAddr->IC_DATA_CMD;
		wRxCnt++;
	}

	IIC_UnLock(pIICInfo);

	return wRxCnt;
}

/*
 * IIC【中断方式】 收发数据
 * 输入参数：pIICInfo,   IIC信息结构体指针
 * 		  wTargetAddr 目标地址
 * 		  pcTxBuffer   发送缓存指针
 * 		  TxLength    最大发送长度
 * 		  pcRxBuffer	     接收缓存指针
 * 		  RxLength 	     最大接收长度
 * 		  pvCallBackFunc	     发送完成回调函数
 * 输出参数： = 0，成功启动 ； < 0，执行出错
 * 说明：作为主机使用时，wTargetAddr > 0,作为从机使用时，wTargetAddr = 0. 每次传输完成后均会关闭IIC模块，等待下次执行该函数后开启。
 */
int IIC_Transceive_Interrupt( pStruct_IICInfo pIICInfo, UINT16 wTargetAddr,
					  	  	  UINT8 *pcTxBuffer, UINT16 wTxLength,
					  	  	  UINT8 *pcRxBuffer, UINT16 wRxLength,
					  	  	  void  *pvCallBackFunc)
{
	if ( ( pIICInfo == NULL) ||
		 ( (wTxLength + wRxLength) == 0) )
	{
		return -1;
	}

	//作为从站，不能主动发起数据
	if ( (wTargetAddr > 0) &&
		 (pIICInfo->IIC_Initstruct.IIC_Character == IIC_Character_Slave) )
	{
		return -2;
	}
	else if (wTargetAddr == 0)
	{
		if (pIICInfo->IIC_Initstruct.IIC_Character == IIC_Character_Master)
		{
			return -4;
		}

		//作为从站，只能有一种作用，即收或者发。
		if ( wRxLength )
		{
			wTxLength = 0;
		}
	}
	else
	{
		;
	}

	//尝试获取操作权限
	if ( !IIC_Lock(pIICInfo) )
	{
		return -3;
	}

	pIICInfo->IIC_TxRx_CBFunc = (void *)pvCallBackFunc;//发送完成回调函数

	if (pcTxBuffer == NULL)
	{
		wTxLength = 0;
	}

	if (pcRxBuffer == NULL)
	{
		wRxLength = 0;
	}

	//更新TAR
	if( (pIICInfo->IIC_BaseAddr->IC_TAR & 0x3FF) != (wTargetAddr & 0x3FF) )
	{
		IIC_Close(pIICInfo);			/* 关闭IIC 清除Tx FIFO， Rx FIFO */
		IIC_SetTargetAddr(pIICInfo, wTargetAddr & 0x3FF); 		/* 配置目标地址 */
	}

	pIICInfo->IIC_Initstruct.IIC_RxBuffer.pBuffer = pcRxBuffer;
	pIICInfo->IIC_Initstruct.IIC_RxBuffer.size = wRxLength;
	pIICInfo->IIC_Initstruct.IIC_RxBuffer.index = 0;

	pIICInfo->IIC_Initstruct.IIC_TxBuffer.pBuffer = pcTxBuffer;
	pIICInfo->IIC_Initstruct.IIC_TxBuffer.size =  wTxLength;
	pIICInfo->IIC_Initstruct.IIC_TxBuffer.index = 0;

	IIC_Open(pIICInfo);

	//主机模式
	if (wTargetAddr > 0)
	{
		IIC_EnableInterrupt(pIICInfo, IIC_INT_TX_ABRT | IIC_INT_STOP_DET
				          | IIC_INT_TXFTL_BELOW | IIC_INT_RXFTL_ABOVE);                 /* 使能中断 */
	}
	else if(wRxLength > 0)
	{
		IIC_EnableInterrupt(pIICInfo, IIC_INT_TX_ABRT | IIC_INT_STOP_DET
				            | IIC_INT_RXFTL_ABOVE );                                    /* 使能中断 */
	}
	else
	{
		IIC_EnableInterrupt(pIICInfo, IIC_INT_TX_ABRT | IIC_INT_STOP_DET
				            | IIC_INT_SRD_RES);                                         /* 使能中断 */
	}

	return 0;
}

/*
 * IIC 实例化
 * 输入参数：pIICInfo： IIC配置信息结构体；
 * 输出参数：
 */
void IIC_Config(pIIC_InitStruct pIICInfo)
{
	pStruct_IICInfo pInfo;

	if(pIICInfo->IIC_BaseAddr == (pStruct_IIC_t)IIC0_ADDR)
	{
		pInfo = &IIC0;
	}
	else
	{
		return ;
	}

	pInfo->IIC_BaseAddr = pIICInfo->IIC_BaseAddr;
	pInfo->IIC_Initstruct.IIC_Addr =  pIICInfo->IIC_Addr;
	pInfo->IIC_Initstruct.IIC_AddrMode = pIICInfo->IIC_AddrMode;
	pInfo->IIC_Initstruct.IIC_BaseAddr = pIICInfo->IIC_BaseAddr;
	pInfo->IIC_Initstruct.IIC_Character = pIICInfo->IIC_Character;
	pInfo->IIC_Initstruct.IIC_SpeedMode = pIICInfo->IIC_SpeedMode;
	pInfo->IIC_Initstruct.RxFTLevel = pIICInfo->RxFTLevel;
	pInfo->IIC_Initstruct.TxFTLevel = pIICInfo->TxFTLevel;
	pInfo->IIC_Initstruct.cIICIntr = pIICInfo->cIICIntr;
	pInfo->IIC_Initstruct.dwPriority = pIICInfo->dwPriority;

	if(pIICInfo->cIICIntr != 0)                                     /* 使用中断处理方式 */
	{
		/* 如果需要用中断方式处理，则还需要增加一块缓冲区用于存放数据 */
		pInfo->IIC_Initstruct.IIC_RxBuffer.pBuffer = pIICInfo->IIC_RxBuffer.pBuffer;
		pInfo->IIC_Initstruct.IIC_RxBuffer.size = pIICInfo->IIC_RxBuffer.size;
		pInfo->IIC_Initstruct.IIC_RxBuffer.index = pIICInfo->IIC_RxBuffer.index;

		pInfo->IIC_Initstruct.IIC_TxBuffer.pBuffer = pIICInfo->IIC_TxBuffer.pBuffer;
		pInfo->IIC_Initstruct.IIC_TxBuffer.size = pIICInfo->IIC_TxBuffer.size;
		pInfo->IIC_Initstruct.IIC_TxBuffer.index = pIICInfo->IIC_TxBuffer.index;
	}

	pInfo->IIC_TxRx_CBFunc = NULL;      /* 暂未用到 */

	IIC_InitConfig(pInfo);

	if(pIICInfo->cIICIntr != 0)
	{
		if(pIICInfo->IIC_ISR != NULL)                               /* 若用户自定义了中断处理函数，则使用用户自定义的，否则使用默认的中断处理函数 */
		{
			IIC_Intr.handler = pIICInfo->IIC_ISR;
		}
		IIC_Intr.priority = pIICInfo->dwPriority;
		INTC_RequestIrq(&IIC_Intr);
	}
}

/*
 * 中断服务函数
 * 输入参数：
 * 输出参数：无
 */
void IIC_ISRHandler(UINT32 irqid)
{
	UINT32 dwIntrSta = IIC0.IIC_BaseAddr->IC_INTR_STAT;
	UINT8 cTxLevel = 0;
	pStruct_IICInfo pIICInfo = &IIC0;

	// 清除中断
	IIC_ClearInterrupt(&IIC0, dwIntrSta);

	if (dwIntrSta & (IIC_INT_STOP_DET | IIC_INT_TX_ABRT) )  /* I2C收到结束信号 或 发送终止 */
	{
		IIC_Read_RxFIFO(&IIC0, 0);                          /* 读取 RxFIFO中剩余数据 */

		/* 关闭IIC是为了防止数据操作过程中又有新的中断到来，造成数据错误。 */
		IIC_Close(&IIC0);
		IIC_UnLock(&IIC0);

		if(IIC0.IIC_TxRx_CBFunc != NULL)
		{
			IIC0.IIC_TxRx_CBFunc();
		}
	}

	if (dwIntrSta & (IIC_INT_SRD_RES | IIC_INT_TXFTL_BELOW) )/* I2C从机回应读请求 */
	{
		if(IIC_IS_RXFTL_ABOVE(pIICInfo) )                       /* 如果RxFIFO已经超过阈值，应当控制TxFIFO的写入速度。 */
		{
			cTxLevel = 1;
		}
		else
		{
			cTxLevel = (8 - IIC0.IIC_Initstruct.TxFTLevel);
		}

		/* 向TxFIFO写入数据 如果实际写入了0长度字节的数据，表示需要额外填充，此时填充0. */
		if (!IIC_Write_TxFIFO(&IIC0, cTxLevel) )
		{
			IIC_DummyWrite(&IIC0, cTxLevel);
		}
	}

	if (dwIntrSta & IIC_INT_RXFTL_ABOVE)                    /* RX FIFO超过缓存阈值 */
	{
		IIC_Read_RxFIFO(&IIC0, 0);                          /* 读取 RxFIFO数据 */
	}
}
