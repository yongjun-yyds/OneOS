/*
 文件: dma.c
 版本: CMC693 V1.10
 版权: 浙江杰芯科技有限公司
 作者: zhulin(zhulin2@supcon.com)
 描述：
修改记录：
=========
日期                                      作者                       工作                            内容
*/
#include "lib_dma.h"
#include "intc.h"

static UINT8 DMA_IDVar = 0;		//用于生成唯一的ID值

ChannelInfo_st ChInfo[DMA_CH_NUM] =
{
	{(UINT32)DMA_CH1_ADDR, DMA_PriHigh, FALSE, DMA_ID_INVALID, 0xFFFFFFFF, 0xFFFFFFFF, NULL},
	{(UINT32)DMA_CH2_ADDR, DMA_PriLow, FALSE, DMA_ID_INVALID, 0xFFFFFFFF, 0xFFFFFFFF, NULL}
};

//开启DMA
static void DMA_StartCh(UINT8 ch)
{
	DMA_COMMON->DmaCfgReg = 0x01;
	DMA_COMMON->ChEnReg |= (1 << ch) | (1 << (ch + 8));
}



//停止DMA传输
//zhao修改了实现方式，应确保使其能正确清除两个通道的使能标志位。
//源代码在需要同时清除两个通道的情况时存在可能无法成功的问题。
static void DMA_StopCh(UINT8 ch)
{
	UINT32 temp = ( 0x0001 << (ch + 8) ) | ( (UINT8)(~(0x01 << ch) ) );
	UINT32 CH_EN = (1 << ch);

	//如果DMA的使能是关闭的，那么下面的通道使能会自动被关闭。
	if((DMA_COMMON->DmaCfgReg & 0x01 ) == 0x00)
	{
		return ;
	}

	do
	{
		DMA_COMMON->ChEnReg = temp;//对写入使能位0的通道使能位不影响。
	}while(0 != (DMA_COMMON->ChEnReg & CH_EN) );



//	//关闭DMA使能 如果要关闭所有通道，只需要关闭DMA使能即可。
//	do
//	{
//		DMA_COMMON->DmaCfgReg = 0x00;
//	}
//	while( (DMA_COMMON->DmaCfgReg & 0x01 ) != 0x00);


}



//使能指定通道中断总开关
static void DMA_EnableChannelGlobalInterrupt(UINT8 ch)
{
	pDMA_ChReg pChReg;
	pChReg = (pDMA_ChReg)ChInfo[ch].dwChBaseAddr;
	pChReg->CTRLa |= 0x01;
}



//失能指定通道中断总开关
//static void DMA_DisableChannelGlobalInterrupt(UINT8 ch)
//{
//	pDMA_ChReg pChReg;
//	pChReg = (pDMA_ChReg)ChInfo[ch].dwChBaseAddr;
//	pChReg->CTRLa &= ~0x01;
//}



//屏蔽指定端口的所有中断
static void DMA_MaskChannelAllInt(UINT8 ch)
{
	DMA_COMMON->MaskBlock &= (1 << (ch + 8)) | (~(1 << ch));
	DMA_COMMON->MaskDstTran &= (1 << (ch + 8)) | (~(1 << ch));
	DMA_COMMON->MaskErr &= (1 << (ch + 8)) | (~(1 << ch));
	DMA_COMMON->MaskSrcTran &= (1 << (ch + 8)) | (~(1 << ch));
	DMA_COMMON->MaskTfr &= (1 << (ch + 8)) | (~(1 << ch));
}


//清除指定端口的所有中断状态寄存器
static void DMA_ClearChannelAllIntStatus(UINT8 ch)
{
	DMA_COMMON->ClearBlock = 1 << ch;
	DMA_COMMON->ClearDstTran = 1 << ch;
	DMA_COMMON->ClearErr = 1 << ch;
	DMA_COMMON->ClearSrcTran = 1 << ch;
	DMA_COMMON->ClearSrcTran = 1 << ch;
}


//使能指定通道的Block中断（具体含义请参考User Guide）
static void DMA_EnableChannelIntBlock(UINT8 ch)
{
	DMA_COMMON->MaskBlock |= (1 << (ch + 8)) | (1 << ch);
}


//使能指定通道的SrcTran中断（具体含义请参考User Guide）
static void DMA_EnableChannelIntDstTran(UINT8 ch)
{
	DMA_COMMON->MaskDstTran |= (1 << (ch + 8)) | (1 << ch);
}


//使能指定通道的SrcTran中断（具体含义请参考User Guide）
static void DMA_EnableChannelIntErr(UINT8 ch)
{
	DMA_COMMON->MaskErr |= (1 << (ch + 8)) | (1 << ch);
}


//使能指定通道的SrcTran中断（具体含义请参考User Guide）
static void DMA_EnableChannelIntSrcTran(UINT8 ch)
{
	DMA_COMMON->MaskSrcTran |= (1 << (ch + 8)) | (1 << ch);
}



//使能指定通道的Tfr中断（具体含义请参考User Guide）
static void DMA_EnableChannelIntTfr(UINT8 ch)
{
	DMA_COMMON->MaskTfr |= (1 << (ch + 8)) | (1 << ch);
}


//设置优先级
static BOOL DMA_SetPriority(IN UINT8 ch, IN Enum_DMA_Priority Priority)
{
    UINT32 temp = 0;
    pDMA_ChReg pChReg;

	pChReg = (pDMA_ChReg)ChInfo[ch].dwChBaseAddr;

    temp = pChReg->CFGa;
    temp &= 0xFFFFFF1F;
    temp |= (Priority << 5);
    pChReg->CFGa = temp;

    return TRUE;
}


//配置传输
static BOOL DMA_SetTransferDirection(IN UINT8 ch, IN Enum_DMA_DIR dir)
{
	UINT32 temp = 0;
	pDMA_ChReg pChReg;

	pChReg = (pDMA_ChReg)ChInfo[ch].dwChBaseAddr;

    temp = pChReg->CTRLa;
    temp &= 0xFF8FFFFF;
    temp |= (dir << 20);
    pChReg->CTRLa = temp;

    return TRUE;
}


//设置目的地址、源地址增长方式
static BOOL DMA_SetAddrGrowth(IN UINT8 ch, IN Enum_DMA_AddrGrowth src_growth, IN Enum_DMA_AddrGrowth dest_growth)
{
	UINT32 temp = 0;
	pDMA_ChReg pChReg;

	pChReg = (pDMA_ChReg)ChInfo[ch].dwChBaseAddr;

    temp = pChReg->CTRLa;
    temp &= 0xFFFFF87F;
    temp |= (src_growth << 9);
    temp |= (dest_growth << 7);
    pChReg->CTRLa = temp;

    return TRUE;
}


//设置传输一个单元的宽度
static BOOL DMA_SetTransferWidth(IN UINT8 ch, IN Enum_DMA_DataWidth src_width, IN Enum_DMA_DataWidth dest_width)
{
	UINT32 temp = 0;
	pDMA_ChReg pChReg;

	pChReg = (pDMA_ChReg)ChInfo[ch].dwChBaseAddr;

    temp = pChReg->CTRLa;
    temp &= 0xFFFFFF81;
    temp |= (src_width << 4);
    temp |= (dest_width << 1);
    pChReg->CTRLa = temp;

    return TRUE;
}



//设置一次突发传输所传输的单元
static BOOL DMA_SetBurstLength(IN UINT8 ch, IN Enum_DMA_BurstLength src, IN Enum_DMA_BurstLength dest)
{
	UINT32 temp = 0;
	pDMA_ChReg pChReg;

	pChReg = (pDMA_ChReg)ChInfo[ch].dwChBaseAddr;

    temp = pChReg->CTRLa;
    temp &= 0xFFFE07FF;
    temp |= (src << 14);
    temp |= (dest << 11);
    pChReg->CTRLa = temp;

    return SUCCESS;
}


//设置极性
static BOOL DMA_SetPolarity(IN UINT8 ch, IN Enum_DMA_Polarity src, IN Enum_DMA_Polarity dest)
{
	UINT32 temp = 0;
	pDMA_ChReg pChReg;

	pChReg = (pDMA_ChReg)ChInfo[ch].dwChBaseAddr;

	temp = pChReg->CFGa; //TODO
    temp &= 0xFFF3FFFF;
    temp |= (src << 19);
    temp |= (dest << 18);
    pChReg->CFGa = temp;

    return TRUE;
}


static BOOL DMA_SetSrcDestDevice(IN UINT8 ch, IN Enum_DMA_Peripheral src, IN Enum_DMA_Peripheral dest)
{
    pDMA_ChReg pChReg;

    pChReg = (pDMA_ChReg)ChInfo[ch].dwChBaseAddr;


    pChReg->CFGb &= 0xFFFFF87F;
    if( DMA_NOT_PERIPHERAL != src )
    {
    	pChReg->CFGb |= (src << 7);
    }

    pChReg->CFGb &= 0xFFFF87FF;
    if( DMA_NOT_PERIPHERAL != dest )
    {
    	pChReg->CFGb |= (dest << 11);
    }

    return TRUE;
}



static BOOL DMA_SetBaseAddress(UINT8 ch, UINT32 src, UINT32 dest)
{
	pDMA_ChReg pChReg;

	pChReg = (pDMA_ChReg)ChInfo[ch].dwChBaseAddr;

	pChReg->SAR = src;
	pChReg->DAR = dest;

    return TRUE;
}



//期望传输多少个item，每个item即是传输宽度（8bits, 16bits, 32bits）
//每个item的宽度由 CTLx. SRC_TR_WIDTH决定。
static BOOL DMA_SetTransferItem(UINT8 ch, UINT32 item)
{
	pDMA_ChReg pChReg;

	pChReg = (pDMA_ChReg)ChInfo[ch].dwChBaseAddr;

	pChReg->CTRLb = item; //10位长度

    return TRUE;
}


//
static BOOL DMA_SetHandshaking(IN UINT8 ch, IN Enum_DMA_Handshaking src, IN Enum_DMA_Handshaking dest)
{
	UINT32 temp = 0;
	pDMA_ChReg pChReg;

	pChReg = (pDMA_ChReg)ChInfo[ch].dwChBaseAddr;

    temp = pChReg->CFGa;
    temp &= 0xFFFFF3FF;
    temp |= (src << 11);
    temp |= (dest << 10);
    pChReg->CFGa = temp;

    return TRUE;
}



//复位指定通道的状态值
static void DMA_ResetChInfo(UINT8 ch)
{
	ChInfo[ch].dwChBaseAddr = (UINT32)DMA_CH1_ADDR;
	ChInfo[ch].beOpened = FALSE;
	ChInfo[ch].dwLastSrcAddr = 0xFFFFFFFF;
	ChInfo[ch].dwLastDestAddr = 0xFFFFFFFF;
	ChInfo[ch].wID = DMA_ID_INVALID;
}



//分配一个ID，用于唯一标识DMA配置
static UINT8 DMA_AllocateID(void)
{
	do
	{
		++DMA_IDVar;

	}while ((DMA_IDVar == 0) || (DMA_IDVar == ChInfo[0].wID) || (DMA_IDVar == ChInfo[1].wID));

	return DMA_IDVar;
}



//寻找对应ID值的DMA通道
static UINT8 DMA_SearchCh(UINT16 id)
{
//	int i;
//
//	//ASSRT(0 != DMA_IDVar);
//
//	for (i = 0; i < DMA_CH_NUM; ++i)
//	{
//		if ((id == ChInfo[i].ID) && (ChInfo[i].beOpened))
//		{
//			return i;
//		}
//	}
	//zhao 改变了判断条件。主要是因为现有条件下beOpened 只能为FALSE，所以申请通道时只能申请到通道0.
#if 0
	if ((id == ChInfo[0].ID) && ( ! ChInfo[0].beOpened ))
	{
		return 0;
	}

	if ((id == ChInfo[1].ID) && ( ! ChInfo[1].beOpened ))
	{
		return 1;
	}

	return DMA_CH_INVALID;

#else

	if ((id == ChInfo[0].wID) && ( ChInfo[0].beOpened ))
	{
		return 0;
	}
	else if ((id == ChInfo[1].wID) && ( ChInfo[1].beOpened ))
	{
		return 1;
	}
	else
	{
		return DMA_CH_INVALID;
	}

#endif

}



/**
  * @brief  : 请求DMA
  * @param  : 无
  * @NOTE   :
  * @retval : 如果存在空闲的通道，返回所分配的ID值；如果所有通道已被占用，返回:DMA_ID_INVALID。
  */
static UINT16 DMA_Request(void)
{
	int i;
	UINT32 psr;

	CPU_EnterCritical(&psr);

	//查找一个空闲通道
	for ( i = 0; i < DMA_CH_NUM; ++i)
	{
		if ( ! ChInfo[i].beOpened )
		{
			ChInfo[i].wID = DMA_AllocateID();

			//zhao add @2015/04/28 18:21
			//这个必须放在这，而且DMA_SearCh()函数条件也要改变，不然出错。
			ChInfo[i].beOpened = TRUE;

			CPU_ExitCritical(psr);
			return (ChInfo[i].wID);
		}
	}

	CPU_ExitCritical(psr);
	return DMA_ID_INVALID;
}

/**
  * @brief  : 释放所有的通道
  * @param  : None
  * @NOTE   :
  * @retval : 如不是特殊情况，请慎用!
  */
void DMA_FreeAllChannels(void)
{
	UINT8 ch = 0;
	UINT32 psr;

	for (ch = 0; ch < 2; ch++)
	{
		CPU_EnterCritical(&psr);

		DMA_StopCh(ch);
		ChInfo[ch].beOpened = FALSE;
		ChInfo[ch].wID = DMA_ID_INVALID;

		CPU_ExitCritical(psr);

		//关闭所有的DMA中断
		DMA_MaskChannelAllInt( ch );
		DMA_ClearChannelAllIntStatus( ch );
	}
}

/**
  * @brief  : 释放已申请的通道
  * @param  : id, 需要释放的通道对应的id值
  * @NOTE   :
  * @retval : TRUE，   成功释放；
  	  	  	  FALSE，释放失败，可能是传递的参数值id不正确（没有与之对应的通道）
  */
BOOL DMA_Free(UINT8 id)
{
	UINT32 psr;

	UINT8 ch;

	ch = DMA_SearchCh(id);
	//zhao改变了条件，因为这里的条件会导致无法释放DMA通道。
	//if (ch != DMA_CH_INVALID)
	if (ch == DMA_CH_INVALID)
	{
		return FALSE;
	}


	DMA_StopCh(ch);

	CPU_EnterCritical(&psr);

	ChInfo[ch].beOpened = FALSE;
	ChInfo[ch].wID = DMA_ID_INVALID;

	CPU_ExitCritical(psr);

	//关闭所有的DMA中断
	DMA_MaskChannelAllInt( ch );
	DMA_ClearChannelAllIntStatus( ch );

	return TRUE;
}



//
/**
  * @brief  : DMA初始化
  * @param  : pInit, 初始化参数结构指针
  * @NOTE   :
  * @retval : 非0, 初始化成功，该值为分配的唯一ID值；
  	  	  	  0, 初始化失败；
  */
UINT8 DMA_Init(pDMA_Init_st pInit)
{
	UINT8 ch, id;
//	UINT8 ret_code = TRUE;

	//断言参数


	id = DMA_Request();
	if ( DMA_ID_INVALID == id )	//判断是否已成功申请DMA通道
	{
		return FALSE;
	}

	//此时一定可以获取到合法的通道值
	ch = DMA_SearchCh(id);
//	if ( DMA_CH_INVALID == ch )
//	{
//		ret_code = FALSE;
//	}

	//配置寄存器
	DMA_ClearChannelAllIntStatus( ch );
	DMA_MaskChannelAllInt( ch );


	DMA_SetPolarity(ch, DMA_High, DMA_High);

	//配置优先级
	DMA_SetPriority(ch, ChInfo[ch].ChPrio);

	//配置传输方向
	DMA_SetTransferDirection(ch, pInit->DMA_Direction);


	//源地址、目的地址
	DMA_SetBaseAddress(ch, pInit->DMA_SrcBaseAddr, pInit->DMA_DestBaseAddr);

	//源地址、目的地址增长方式
	DMA_SetAddrGrowth(ch, pInit->DMA_SrcGrowth, pInit->DMA_DestGrowth);

	//源数据、目的数据宽度
	DMA_SetTransferWidth(ch, pInit->DMA_SrcDataWidth, pInit->DMA_DestDataWidth);

	//配置传输长度（单元）
	DMA_SetTransferItem(ch, pInit->DMA_BlockTransferSize);


	//一次突发传输将传输多少个单元
	DMA_SetBurstLength(ch, pInit->DMA_BurstTransfer, pInit->DMA_BurstTransfer);


	//源、目的设备
	DMA_SetSrcDestDevice(ch, pInit->DMA_SrcPeripheral, pInit->DMA_DestPeripheral);

	//软硬握手配置
	DMA_SetHandshaking(ch,
			(DMA_NOT_PERIPHERAL == pInit->DMA_SrcPeripheral) ? DMA_Software : DMA_Hardware,
			(DMA_NOT_PERIPHERAL == pInit->DMA_DestPeripheral) ? DMA_Software : DMA_Hardware);

	//使能指定中断
	//CTRLax : INT_EN
	if ( 0 != pInit->DMA_EnableInterrupt )
	{
		ChInfo[ch].Callback = (void *)pInit->Callback;
		ChInfo[ch].parg = pInit->parg;

		DMA_EnableChannelGlobalInterrupt( ch );

		//
		if (0 != (pInit->DMA_EnableInterrupt & ENABLE_INT_BLOCK))
		{
			DMA_EnableChannelIntBlock(ch);
		}

		if (0 != (pInit->DMA_EnableInterrupt & ENABLE_INT_DSTTRAN))
		{
			DMA_EnableChannelIntDstTran(ch);
		}

		if (0 != (pInit->DMA_EnableInterrupt & ENABLE_INT_ERR))
		{
			DMA_EnableChannelIntErr(ch);
		}


		if (0 != (pInit->DMA_EnableInterrupt & ENABLE_INT_SRCTRAN))
		{
			DMA_EnableChannelIntSrcTran(ch);
		}

		if (0 != (pInit->DMA_EnableInterrupt & ENABLE_INT_TFR))
		{
			DMA_EnableChannelIntTfr(ch);
		}
	}
	else
	{
		ChInfo[ch].Callback = NULL;
		ChInfo[ch].parg = NULL;
	}

	return id;
}



//初始化，并开始一次传输
UINT8 DMA_Init_Start(pDMA_Init_st pInit)
{
	UINT8 id;

	//TODO  ASSERT( (.DMA_SrcBaseAddr != 0) && (.DMA_DestBaseAddr != 0) );

	id = DMA_Init( pInit );

	DMA_Restart( id, pInit->DMA_SrcBaseAddr, pInit->DMA_DestBaseAddr, pInit->DMA_BlockTransferSize );

	return id;

}


/**
  * @brief  : 使用已有初始化再次开启DMA传输
  * @param  : id,
  	  	  	  SrcAddr, 源地址，如果值为0，则使用上一次地址；
			  DestAddr, 目的地址，如果值为0，则使用上一次地址
  * @NOTE   :
  * @retval : TRUE,  再次传输成功；
  	  	  	  FALSE, 再次传输失败；
  */
//BOOL DMA_Restart(UINT16 id, UINT32 SrcAddr, UINT32 DestAddr)
BOOL DMA_Restart(UINT16 id, UINT32 SrcAddr, UINT32 DestAddr, UINT32 items)
{
	UINT8 ch;

	ch = DMA_SearchCh(id);
	if (DMA_CH_INVALID == ch)
	{
		return FALSE;
	}

	ChInfo[ch].dwLastSrcAddr = ( 0 == SrcAddr) ? ChInfo[ch].dwLastSrcAddr : SrcAddr;
	ChInfo[ch].dwLastDestAddr = ( 0 == DestAddr) ? ChInfo[ch].dwLastDestAddr : DestAddr;
	ChInfo[ch].dwLastItems = ( 0 == items) ? ChInfo[ch].dwLastItems : items;
	//TODO  ASSERT( (.DMA_SrcBaseAddr != 0) && (.DMA_DestBaseAddr != 0) );

	//配置地址寄存器
	DMA_SetBaseAddress(ch, ChInfo[ch].dwLastSrcAddr, ChInfo[ch].dwLastDestAddr);

	//重新配置长度寄存器
	DMA_SetTransferItem(ch, ChInfo[ch].dwLastItems);

	DMA_StartCh(ch);

	return TRUE;
}



//中断服务程序
/**
  * @brief  : 中断服务程序
  * @param  :
  * @NOTE   :
  * @retval :
  */

void DMA_ISRCallback(void)
{
	int i;

	//通过StatusInt寄存器判断产生的是何种中断（block， TFR， ERR，。。。）

	//读取相应的中断状态寄存器（StatusBlock， StatusTfr，  StatusErr，。。。）

	//判断是哪一通道参数中断，并调用回调函数

	for (i = 0; i < DMA_CH_NUM; ++i)
	{
		if ((DMA_COMMON->StatusBlock & (1 << i)) || (DMA_COMMON->StatusDstTran & (1 << i))
				|| (DMA_COMMON->StatusErr & (1 << i)) || (DMA_COMMON->StatusSrcTran & (1 << i))
				|| (DMA_COMMON->StatusTfr & (1 << i)))
		{
			ChInfo[i].Callback( ChInfo[i].parg, ChInfo[i].wID );

			DMA_ClearChannelAllIntStatus( i );
		}
	}
}



/**
  * @brief  : 停止指定的DMA传输
  * @param  :
  * @NOTE   :
  * @retval :
  */
void DMA_TerminateTransmit(UINT16 id)
{
	UINT8 ch = DMA_SearchCh(id);

	DMA_StopCh(ch);
}



/**
  * @brief  : 关闭所有DMA通道
  * @param  :
  * @NOTE   :
  * @retval :
  */
void DMA_TerminateAll(void)
{
	int i;

	for (i = 0; i < DMA_CH_NUM; ++i)
	{
		DMA_StopCh(i);
		DMA_ResetChInfo(i);
	}

	do
	{
		DMA_COMMON->DmaCfgReg = 0x0;
	}while(0 != (DMA_COMMON->DmaCfgReg & 0x01));

}

/**
  * @brief  : 等待DMA产生一个块传输完成信号
  * @param  :
  * @NOTE   :
  * @retval :
  */
void DMA_WaitBlockTfrIdle(void)
{
	//wait for DMA transmit complete
	while ( !(DMA_COMMON->RawBlock & 0x01) );
}

