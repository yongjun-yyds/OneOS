/*
 文件: spi.c
 版本: CMC693 V1.10
 版权: 浙江杰芯科技有限公司
 作者: zhulin(zhulin2@supcon.com)
 描述：
修改记录：
=========
日期                                      作者                       工作                            内容
*/
#include "lib_spi.h"
#include "powm.h"
#include "lib_dma.h"
#include "intc.h"

#define DMA_ID_INVALID 0

Struct_SPIInfo SPI;                     /* SPI信息定义 */
pStruct_SPIInfo pSPI = &SPI;
Struct_SPIInfo SPI1;
pStruct_SPIInfo pSPI1 = &SPI1;

static int SPI_DMA_Free(pStruct_SPIInfo pSpiInfo, UINT8 DMA_ID);

/*
 * 波特率设置
 * 输入参数：pSpiInfo, SPI信息结构体指针；
 * 输出参数：返回 0， 成功; 其他，失败。
 * 说明：SPI时钟输出频率：APB_FREQ/wClockDivider  不要配置太高频率，不建议频率超过2M
 */
static int SPI_SetBaudrate(pStruct_SPIInfo pSpiInfo)
{
    UINT32 dwBaudrate = pSpiInfo->SPI_InitStruct.baudrate;
    UINT32 dwBaudrateMax = APB_FREQ / 6;                         /* 最大波特率 */
    UINT16 wClockDivider;                                        /* SPI 时钟 分频系数 */

    if(pSpiInfo == NULL)
    {
        return -1;
    }

    SPI_Close(pSpiInfo);

    dwBaudrate = (dwBaudrate <= 1) ?  1 : dwBaudrate;
    dwBaudrate = (dwBaudrate >= dwBaudrateMax) ? dwBaudrateMax : dwBaudrate;

    wClockDivider = APB_FREQ / dwBaudrate;                       /* SPI时钟频率与APB时钟频率和分频系数SCKDV有关 */

    wClockDivider = (wClockDivider >= 65534) ?  65534 : wClockDivider;
    wClockDivider = (wClockDivider <= 4) ?  4 : wClockDivider;   /* 硬件上SCKDV可选范围为2~65534，这里做限制4 */
    pSpiInfo->SPI->BAUDR = wClockDivider;

    return 0;
}

/*
 * 设置数据宽度
 * 输入参数：pSpiInfo, SPI信息结构体指针；
 * 输出参数：返回 0， 成功; 其他，失败。
 * 说明：硬件支持4~16位宽的数据传输
 */
static int SPI_SetDataWidth(pStruct_SPIInfo pSpiInfo)
{
    UINT16 wRegisterValue = 0;
    UINT8 cDataWidth = (UINT8)(pSpiInfo->SPI_InitStruct.datawidth);

    SPI_Close(pSpiInfo);

    cDataWidth = (cDataWidth <= 4 ) ? 4 : cDataWidth;

    wRegisterValue = (pSpiInfo->SPI->CTRLR0) & 0xFFF0;
    wRegisterValue |= (cDataWidth - 1);
    pSpiInfo->SPI->CTRLR0 = wRegisterValue;

    return 0;
}

/*
 * 设置SPI片选
 * 输入参数：pSpiInfo, SPI信息结构体指针；
 * 输出参数：返回 0， 成功; 其他，失败。
 * 说明：只有主SPI具有片选线  SPI有3根片选线   SPI1有1根片选线
 */
static int SPI_SelectCS(pStruct_SPIInfo pSpiInfo)
{
    if(pSpiInfo == NULL)
    {
        return -1;
    }

    SPI_Close(pSpiInfo);

    if((pSpiInfo->SPI_InitStruct.cs == SPI_CS0) ||
       (pSpiInfo->SPI_InitStruct.cs == SPI_CS1) ||
       (pSpiInfo->SPI_InitStruct.cs == SPI_CS2))
    {
    	pSpiInfo->SPI->SER = 1 << (pSpiInfo->SPI_InitStruct.cs - 1);  /* cs1,cs2,cs3 -> 001,010,100 */
    }
    else if(pSpiInfo->SPI_InitStruct.cs == SPI_CS_Manual)
    {
        /* SPI_CS_Manual 用IO进行片选。 */
    	pSpiInfo->SPI->SER = 0;
    }
    else
    {
        ;
    }

    return 0;
}

/*
 * 设置Tx FIFO阈值
 * 输入参数：pSpiInfo, SPI信息结构体指针；
 * 输出参数：返回 0， 成功; 其他，失败。
 * 说明：此FIFO阈值是指触发Tx相关中断的阈值     FIFO深度是固定的为0x22
 *      当Tx FIFO的数据少于或等于这个值  Tx FIFO为空中断触发
 */
static int SPI_SetTxFTLevel(pStruct_SPIInfo pSpiInfo)
{
    UINT8 cLevel = (UINT8)(pSpiInfo->SPI_InitStruct.TxFTLevel);

    SPI_Close(pSpiInfo);

    cLevel = (cLevel >= SPI_FIFO_MAX_LEVEL) ? SPI_FIFO_MAX_LEVEL : cLevel;  /* 阈值不能超过最大值 */

    pSpiInfo->SPI->TXFTLR = cLevel;

    return 0;
}

/*
 * 设置Rx FIFO阈值
 * 输入参数：pSpiInfo, SPI信息结构体指针；
 * 输出参数：返回 0， 成功; 其他，失败。
 * 说明：此FIFO阈值是指触发Rx中断的阈值     FIFO深度是固定的为0x22
 *      当Rx FIFO的数据大于或等于这个值+1  Rx FIFO满中断触发
 */
static int SPI_SetRxFTLevel(pStruct_SPIInfo pSpiInfo)
{
    UINT8 cLevel = (UINT8)(pSpiInfo->SPI_InitStruct.RxFTLevel);

    SPI_Close(pSpiInfo);

    cLevel = (cLevel >= SPI_FIFO_MAX_LEVEL) ? SPI_FIFO_MAX_LEVEL : cLevel;//阈值不能超过最大值

    pSpiInfo->SPI->RXFTLR = cLevel;

    return 0;
}

/*
 * 设置中断屏蔽向量
 * 输入参数：pSpiInfo, SPI结构体信息指针；
 * 输出参数：返回 0， 成功; 其他，失败。
 * 说明：可选参数及组合如下：
 *     SPI_INTR_MASK_ALL
 *     SPI_INTR_MASK_NONE
 *     SPI_INTR_MASK_TXEIM
 *     SPI_INTR_MASK_TXOIM
 *     SPI_INTR_MASK_RXUIM
 *     SPI_INTR_MASK_TXOIM
 *     SPI_INTR_MASK_TXFIM
 *     SPI_INTR_MASK_MSTIM
 *
 *     0 表示屏蔽， 1表示不屏蔽
 */
static int SPI_SetIntrMask(pStruct_SPIInfo pSpiInfo)
{
    UINT8 cInterruptMask = pSpiInfo->SPI_InitStruct.intrmask;

    SPI_Close(pSpiInfo);

    cInterruptMask &= 0x3F;
    pSpiInfo->SPI->IMR = cInterruptMask;
    pSpiInfo->SPI_InitStruct.intrmask = cInterruptMask;

    return 0;
}

/*
 * 设置极性和相位
 * 输入参数：pSpiInfo, SPI信息结构体指针；
 *        Polarity 极性  0:时钟低电平为不活跃状态  1：时钟高电平为不活跃状态
 *        Phase    相位  0：第一个时钟进行采样        1：第二个时钟采样
 * 输出参数：返回 0， 成功; 其他，失败。
 */
int SPI_SetPolarityAndPhase(pStruct_SPIInfo pSpiInfo,
                            Enum_SPI_Polarity Polarity,
                            Enum_SPI_Phase Phase)
{
    if(pSpiInfo->SPI->SPIENR != 0x00)
    {
        SPI_Close(pSpiInfo);
    }
	/* 配置极性 */
    switch(Polarity)
    {
    case SPI_CLOCK_POLARITY_LOW:
    	pSpiInfo->SPI->CTRLR0 &= ~SPI_POLARITY;
        break;
    case SPI_CLOCK_POLARITY_HIGH:
    	pSpiInfo->SPI->CTRLR0 |= SPI_POLARITY;
        break;
    default:
        break;
    }
	/* 配置相位 */
    switch(Phase)
    {
    case SPI_CLOCK_PHASE_FIRST_EDGE:
    	pSpiInfo->SPI->CTRLR0 &= ~SPI_PHASE;
        break;
    case SPI_CLOCK_PHASE_SECOND_EDGE:
    	pSpiInfo->SPI->CTRLR0 |= SPI_PHASE;
        break;
    default:
        break;
    }

    pSpiInfo->SPI_InitStruct.polarity = Polarity;
    pSpiInfo->SPI_InitStruct.phase = Phase;
    return 0;
}

/*
 * 配置SPI并打开SPI
 * 输入参数：pSpiInfo, SPI信息结构体指针；
 * 输出参数：返回 0， 成功; 其他，失败。
 */
int SPI_Open(pStruct_SPIInfo pSpiInfo)
{
    while((pSpiInfo->SPI->SR & SPI_BUSY) || (pSpiInfo->SPI->TXFLR > 0));/* 等待空闲 如果有中断，会先处理完所有中断 */

    /* 如果当前系统是关闭的，就更新参数后开启。 */
    if(pSpiInfo->SPI->SPIENR == 0x00)
    {
        SPI_SetBaudrate(pSpiInfo);
        SPI_SelectCS(pSpiInfo);
        SPI_SetDataWidth(pSpiInfo);
        SPI_SetTxFTLevel(pSpiInfo);
        SPI_SetRxFTLevel(pSpiInfo);
        SPI_SetIntrMask(pSpiInfo);
        SPI_SetPolarityAndPhase(pSpiInfo, pSpiInfo->SPI_InitStruct.polarity, pSpiInfo->SPI_InitStruct.phase);

        pSpiInfo->SPI->SPIENR |= SPI_EN;                                /* 使能SPI */
    }

    return 0;
}

/*
 * 关闭SPI
 * 输入参数：pSpiInfo, SPI信息结构体指针；
 * 输出参数：返回 0， 成功; 其他，失败。
 * 说明：这个过程会关闭SPI使能， 屏蔽所有中断源，清除中断标志。
 */
int SPI_Close(pStruct_SPIInfo pSpiInfo)
{
    UINT8 cTemp;

    if(pSpiInfo->SPI->SPIENR != 0x00)                                         /* SPI当前状态为打开 */
    {
        while( (pSpiInfo->SPI->SR & SPI_BUSY) || (pSpiInfo->SPI->TXFLR > 0) );/* 等待空闲 如果有中断，会先处理完所有中断 */

        pSpiInfo->SPI->SPIENR &= ~SPI_EN;
        pSpiInfo->SPI->IMR = SPI_INTR_MASK_ALL;                               /* 屏蔽所有中断 */
        cTemp = pSpiInfo->SPI->ICR;                                           /* 清除所有中断标志 */
    }

    return 0;
}

/*
 * 获取SPI成员变量的结构体
 * 输入参数：pSpiInfo, SPI信息结构体指针；
 *        SPI_Initstruct SPI的初始化结构体
 * 输出参数：返回 0， 成功; 其他，失败。
 */
int SPI_GetInitStructure(pStruct_SPIInfo pSpiInfo, pSPI_InitStructure pInitstruct)
{
    if(pInitstruct == NULL)
    {
        return -1;
    }

    pInitstruct->baudrate = pSpiInfo->SPI_InitStruct.baudrate;
    pInitstruct->datawidth = pSpiInfo->SPI_InitStruct.datawidth;
    pInitstruct->polarity = pSpiInfo->SPI_InitStruct.polarity;
    pInitstruct->phase = pSpiInfo->SPI_InitStruct.phase;
    pInitstruct->RxFTLevel = pSpiInfo->SPI_InitStruct.RxFTLevel;
    pInitstruct->TxFTLevel = pSpiInfo->SPI_InitStruct.TxFTLevel;
    pInitstruct->intrmask = pSpiInfo->SPI_InitStruct.intrmask;
    pInitstruct->cs = pSpiInfo->SPI_InitStruct.cs;

    return 0;
}

/*
 * SPI 接收缓存的数据DMA传送到内存。
 * 输入参数：pSpiInfo： SPI信息结构体指针；
 *        pSPI_DMA_Initstruct, 指定的DMA结构体参数.pSPI_DMA_Initstruct 为NULL时使用上次已经保存在结构体中的参数。
 * 输出参数：返回 0， 成功; 其他，失败。
 * 说明：当遇到DMA_P2M 开始的位置和目的地址不在一个源文件中时，
 *     可以先将目的地址放入This->SPI_DMA_P2M_Initstruct 结构体中，使用时第二个参数设置为NULL即可.
 */
int SPI_DMA_P2M_Start(pStruct_SPIInfo pSpiInfo, pSPI_DMA_InitStructure pSPI_DMA_Initstruct)
{
    static DMA_Init_st DMA_P2M_Init_st;
    Enum_DMA_DataWidth  DataWidth;                                  /* DMA 的SPI数据宽度 */
    UINT8 cDMARDLR_Value = SPI_FIFO_MAX_LEVEL >> 1;

    /* 如果当前结构体指针为NULL且SPI结构体中没有保存相关参数，则出错。 */
    if( pSPI_DMA_Initstruct == NULL )
    {
        if(pSpiInfo->SPI_DMA_P2M_Initstruct.memADDR == NULL)
        {
            return -1;
        }
    }
    else
    {
        /* 如果总共只需要发送几个数据，则将阈值调成 0 + 1较合适。 */
        if (pSPI_DMA_Initstruct->length <= cDMARDLR_Value)
        {
            cDMARDLR_Value = 0;
        }

        pSpiInfo->SPI_DMA_P2M_Initstruct.length = pSPI_DMA_Initstruct->length - cDMARDLR_Value;
        pSpiInfo->SPI_DMA_P2M_Initstruct.memADDR = (void *)(pSPI_DMA_Initstruct->memADDR);      /* DST: MEMORY Address */
        pSpiInfo->SPI_DMA_P2M_Initstruct.intrmask = pSPI_DMA_Initstruct->intrmask;
    }

    pSpiInfo->SPI->DMACR |= SPI_RDMAE;                    /* DMA接收使能 */
    pSpiInfo->SPI->DMARDLR = cDMARDLR_Value;              /* 配置接收FIFO */
    pSpiInfo->SPI->SPIENR |= SPI_EN;                      /* 使能SPI */
    /* 判断是否已经初始化过，如果初始化过直接重新启动DMA */
    if( pSpiInfo->SPI_DMA_ID_struct.DMA_P2M_ID != DMA_ID_INVALID )
    {
        /* 更新传输长度，并开始传输 */
        DMA_P2M_Init_st.DMA_BlockTransferSize = pSpiInfo->SPI_DMA_P2M_Initstruct.length;

        DMA_Restart(pSpiInfo->SPI_DMA_ID_struct.DMA_P2M_ID,
                    DMA_P2M_Init_st.DMA_SrcBaseAddr,
                    DMA_P2M_Init_st.DMA_DestBaseAddr,
                    DMA_P2M_Init_st.DMA_BlockTransferSize);
        return 0;
    }

    DataWidth = (pSpiInfo->SPI_InitStruct.datawidth > (UINT8)(SPI_DataWidth_8) ) ? DMA_Width_16bits : DMA_Width_8bits;


    DMA_P2M_Init_st.Callback = pSPI_DMA_Initstruct->cbfunc;
    DMA_P2M_Init_st.parg = NULL;

    DMA_P2M_Init_st.DMA_Direction = DMA_Peripheral2Memory;                           /* DMA传输方向 */
    DMA_P2M_Init_st.DMA_BlockTransferSize = pSpiInfo->SPI_DMA_P2M_Initstruct.length;
    DMA_P2M_Init_st.DMA_BurstTransfer = DMA_Burst_1item;                             /* 突发传输的长度设置 */


    DMA_P2M_Init_st.DMA_SrcBaseAddr = (UINT32)( &(pSpiInfo->SPI->DR) );              /* SRC: Receive FIFO buffer */
    DMA_P2M_Init_st.DMA_SrcDataWidth = DataWidth;
    DMA_P2M_Init_st.DMA_SrcGrowth = DMA_NoChange;
    DMA_P2M_Init_st.DMA_SrcPeripheral = DMA_SPI_RX;

    DMA_P2M_Init_st.DMA_DestBaseAddr = (UINT32)(pSpiInfo->SPI_DMA_P2M_Initstruct.memADDR);/* DST: MEMORY Address */
    DMA_P2M_Init_st.DMA_DestDataWidth = DataWidth;
    DMA_P2M_Init_st.DMA_DestGrowth = DMA_Increment;
    DMA_P2M_Init_st.DMA_DestPeripheral = DMA_NOT_PERIPHERAL;

    DMA_P2M_Init_st.DMA_EnableInterrupt = pSpiInfo->SPI_DMA_P2M_Initstruct.intrmask;

	/* 根据配置配置DMA */
    pSpiInfo->SPI_DMA_ID_struct.DMA_P2M_ID = DMA_Init( &DMA_P2M_Init_st );

    /* DMA配置失败 */
    if(pSpiInfo->SPI_DMA_ID_struct.DMA_P2M_ID == DMA_ID_INVALID)
    {
        return -2;
    }
    else/* DMA配置成功，重新启动 */
    {
        DMA_Restart(pSpiInfo->SPI_DMA_ID_struct.DMA_P2M_ID,
                    DMA_P2M_Init_st.DMA_SrcBaseAddr,
                    DMA_P2M_Init_st.DMA_DestBaseAddr,
                    DMA_P2M_Init_st.DMA_BlockTransferSize );
        return 0;
    }
}

/*
 * 内存数据通过DMA传送到SPI TX FIFO。
 * 输入参数：pSpiInfo： SPI信息结构体指针；
 *        pSPI_DMA_Initstruct, 指定的DMA结构体参数.pSPI_DMA_Initstruct 为NULL时使用上次已经保存在结构体中的参数。
 * 输出参数：返回 0， 成功; 其他，失败。
 */
int SPI_DMA_M2P_Start(pStruct_SPIInfo pSpiInfo, pSPI_DMA_InitStructure pSPI_DMA_Initstruct)
{
    static DMA_Init_st  DMA_M2P_Init_st;
    Enum_DMA_DataWidth  DataWidth;      /* DMA 的SPI数据宽度 */

    /* 在目前只有一个通道可以注册的情况下，需要先释放一个通道，再打开另一个通道。 */
    //This->SPI_DMA_Free(This, This->SPI_DMA_ID_struct.DMA_P2M_ID);
    //This->SPI_DMA_ID_struct.DMA_P2M_ID = DMA_ID_INVALID;

    /* 如果当前结构体指针为NULL且SPI结构体中没有保存相关参数，则出错。 */
    if( pSPI_DMA_Initstruct == NULL )
    {
        if(pSpiInfo->SPI_DMA_M2P_Initstruct.memADDR == NULL)
        {
            return -1;
        }
    }
    else
    {
    	pSpiInfo->SPI_DMA_M2P_Initstruct.length = pSPI_DMA_Initstruct->length;
    	pSpiInfo->SPI_DMA_M2P_Initstruct.memADDR = (void *)(pSPI_DMA_Initstruct->memADDR); /* DST: MEMORY Address */
    	pSpiInfo->SPI_DMA_M2P_Initstruct.intrmask = pSPI_DMA_Initstruct->intrmask;
    }

    pSpiInfo->SPI->DMACR |= SPI_TDMAE;               /* DMA发送使能 */
    pSpiInfo->SPI->DMATDLR = SPI_FIFO_MAX_LEVEL >> 1;/* 配置发送FIFO */
    pSpiInfo->SPI->SPIENR |= SPI_EN;                 /* 使能SPI */
    /* 判断是否已经初始化过，如果初始化过直接重新启动DMA */
    if(pSpiInfo->SPI_DMA_ID_struct.DMA_M2P_ID != DMA_ID_INVALID)
    {
        /* 更新传输长度，并开始传输 */
        DMA_M2P_Init_st.DMA_BlockTransferSize = pSpiInfo->SPI_DMA_M2P_Initstruct.length;

        DMA_Restart(pSpiInfo->SPI_DMA_ID_struct.DMA_M2P_ID,
                    DMA_M2P_Init_st.DMA_SrcBaseAddr,
                    DMA_M2P_Init_st.DMA_DestBaseAddr,
                    DMA_M2P_Init_st.DMA_BlockTransferSize);
        return 0;
    }

    DataWidth = (pSpiInfo->SPI_InitStruct.datawidth > (UINT8)(SPI_DataWidth_8) ) ? DMA_Width_16bits : DMA_Width_8bits;

    DMA_M2P_Init_st.Callback = pSPI_DMA_Initstruct->cbfunc;
    DMA_M2P_Init_st.parg = NULL;

    DMA_M2P_Init_st.DMA_Direction = DMA_Memory2Peripheral;                               /* DMA传输方向 */
    DMA_M2P_Init_st.DMA_BlockTransferSize = pSpiInfo->SPI_DMA_M2P_Initstruct.length;
    DMA_M2P_Init_st.DMA_BurstTransfer = DMA_Burst_1item;                                 /* 突发传输的长度设置 */


    DMA_M2P_Init_st.DMA_SrcBaseAddr = (UINT32)(pSpiInfo->SPI_DMA_M2P_Initstruct.memADDR);/* SRC: MEMORY Address */
    DMA_M2P_Init_st.DMA_SrcDataWidth = DataWidth;
    DMA_M2P_Init_st.DMA_SrcGrowth = DMA_Increment;
    DMA_M2P_Init_st.DMA_SrcPeripheral = DMA_NOT_PERIPHERAL;

    DMA_M2P_Init_st.DMA_DestBaseAddr = (UINT32)( &(pSpiInfo->SPI->DR) );                 /* DST: Transmit FIFO buffer */
    DMA_M2P_Init_st.DMA_DestDataWidth = DataWidth;
    DMA_M2P_Init_st.DMA_DestGrowth = DMA_NoChange;
    DMA_M2P_Init_st.DMA_DestPeripheral = DMA_SPI_TX;

    DMA_M2P_Init_st.DMA_EnableInterrupt = pSpiInfo->SPI_DMA_M2P_Initstruct.intrmask;

    /* 不能使用这个函数，因为初始化函数在进行的过程中就可能产生中断，而此时DMA_P2M_ID尚未有返回值. */
    //This->SPI_DMA_ID_struct.DMA_M2P_ID = DMA_Init_Start(&DMA_M2P_Init_st);
    /* 根据配置配置DMA */
    pSpiInfo->SPI_DMA_ID_struct.DMA_M2P_ID = DMA_Init(&DMA_M2P_Init_st);

    if(pSpiInfo->SPI_DMA_ID_struct.DMA_M2P_ID == DMA_ID_INVALID)
    {
        return -2;
    }
    else
    {
        DMA_Restart(pSpiInfo->SPI_DMA_ID_struct.DMA_M2P_ID,
                    DMA_M2P_Init_st.DMA_SrcBaseAddr,
                    DMA_M2P_Init_st.DMA_DestBaseAddr,
                    DMA_M2P_Init_st.DMA_BlockTransferSize );
        return 0;
    }
}

/*
 * 释放已经申请的DMA通道。
 * 输入参数：pSpiInfo： SPI信息结构体指针；
 *        DMA_ID 需要释放的DMA通道.
 * 输出参数：返回 0， 成功; 其他，失败。
 */
static int SPI_DMA_Free(pStruct_SPIInfo pSpiInfo, UINT8 DMA_ID)
{
    if(pSpiInfo->SPI_DMA_ID_struct.DMA_M2P_ID == DMA_ID)
    {
    	pSpiInfo->SPI->DMACR &= ~SPI_TDMAE;
        DMA_Free(DMA_ID);
        pSpiInfo->SPI_DMA_ID_struct.DMA_M2P_ID = DMA_ID_INVALID;
        pSpiInfo->SPI_DMA_M2P_Initstruct.memADDR = NULL;
    }
    else if(pSpiInfo->SPI_DMA_ID_struct.DMA_P2M_ID == DMA_ID)
    {
    	pSpiInfo->SPI->DMACR &= ~SPI_RDMAE;
        DMA_Free(DMA_ID);
        pSpiInfo->SPI_DMA_ID_struct.DMA_P2M_ID = DMA_ID_INVALID;
        pSpiInfo->SPI_DMA_P2M_Initstruct.memADDR = NULL;
    }
    else
    {
        return -1;
    }

    return 0;
}

/*
 * SPI 收发函数
 * 输入参数：pSpiInfo： SPI信息结构体指针；
 *        CS 片选值；
 *        pTxBuffer
 *        pRxBuffer：      发送接收地址
 *        wBufferLength: 传输的数据长度.
 * 输出参数：返回 0， 成功; 其他，失败。
 */
int SPI_TxRx(pStruct_SPIInfo pSpiInfo,
             Enum_SPI_CS CS,
             void *pTxBuffer,
             void *pRxBuffer,
             UINT32 wBufferLength)
{
    Enum_SPI_DataWidth_Type dataType;                       /* SPI传输的数据宽度 */
    UINT8 cFTLR_Len = pSpiInfo->SPI_InitStruct.TxFTLevel;   /* 缓冲区长度 */
    UINT32 i = 0;
    UINT8 cTransmitCompleteFlag = 1;                        /* 发送完成标志 */
    unsigned int psr_bk;                                    /* 临界保护开启时寄存器保存的值 */

    /* 数据存储宽度类型 */
    dataType = (pSpiInfo->SPI_InitStruct.datawidth > 8) ? SPI_DataWidth_Type_16 : SPI_DataWidth_Type_8;

    if(wBufferLength < 1)                                   /* 无效长度 */
    {
        return -2;
    }

    if(cFTLR_Len <= 0)                                      /* 缓冲区长度为0 */
    {
        return -3;
    }

    /* 25M的APB时钟下，最大支持2.5M波特率，但不建议在波特率大于2M时使用该函数。此时可以使用DMA方式实现。 */
    if(pSpiInfo->SPI_InitStruct.baudrate >= ( APB_FREQ / 10))
    {
        return -4;
    }

    while((pSpiInfo->SPI->SR & SPI_BUSY) || (pSpiInfo->SPI->TXFLR > 0));/* 等待空闲 如果有中断，会先处理完所有中断 */


    /* 如果CS 发生改变，关闭SPI */
    if((CS != SPI_CS_Last) &&
       (pSpiInfo->SPI_InitStruct.cs != CS ))
    {
    	pSpiInfo->SPI_InitStruct.cs = CS;
        SPI_SelectCS(pSpiInfo);                                         /* 重新选择CS，会自动调用SPI_Close. */
    }

    /* 因为这里没有其他参数改变了，所以不用SPI_Open */
    pSpiInfo->SPI->SPIENR |= SPI_EN;                                    /* 使能SPI */

    CPU_EnterCritical(&psr_bk);                                         /* 进入临界保护 */
    if(dataType == SPI_DataWidth_Type_16)
    {
        UINT16 *bufferPtr_Tx = (UINT16 *)pTxBuffer;
        UINT16 *bufferPtr_Rx = (UINT16 *)pRxBuffer;
        UINT16 temp = 0x0000;

        while(cTransmitCompleteFlag)
        {
            while(pSpiInfo->SPI->TXFLR < cFTLR_Len)                     /* 直到发送缓冲区满 */
            {
            	/* 发送数据 */
                if(pTxBuffer != (void *)NULL)
                {
                	pSpiInfo->SPI->DR = *bufferPtr_Tx;
                    bufferPtr_Tx++;
                }
                else
                {
                	pSpiInfo->SPI->DR = 0xFFFF;
                }

                if(++i >= wBufferLength)
                {
                    cTransmitCompleteFlag = 0;
                    break;                                              /* 完成发送，退出 */
                }
            }

            /* 如果有数据就绪，读 */
            while(pSpiInfo->SPI->RXFLR > 0)
            {
                temp = (UINT16)(pSpiInfo->SPI->DR);
                if(pRxBuffer != (void *)NULL)
                {
                    *bufferPtr_Rx = temp;
                    bufferPtr_Rx++;
                }
            }

        }

        while((pSpiInfo->SPI->SR & SPI_BUSY) || (pSpiInfo->SPI->TXFLR > 0));/* 等待本次数据发送完成 */

        /* 读完所有接收到的FIFO */
        while(pSpiInfo->SPI->RXFLR > 0)
        {
            temp = (UINT16)(pSpiInfo->SPI->DR);
            if(pRxBuffer != (void *)NULL)
            {
                *bufferPtr_Rx = temp;
                bufferPtr_Rx++;
            }
        }

    }
    else /* SPI_DataWidth_Type_8 */
    {
        UINT8 *bufferPtr_Tx = (UINT8 *)pTxBuffer;
        UINT8 *bufferPtr_Rx = (UINT8 *)pRxBuffer;
        UINT8 temp = 0x00;

        while(cTransmitCompleteFlag)
        {
            while(pSpiInfo->SPI->TXFLR < cFTLR_Len)                        /* 直到发送缓冲区满 */
            {
            	/* 发送数据 */
                if(pTxBuffer != (void *)NULL)
                {
                	pSpiInfo->SPI->DR = *bufferPtr_Tx;
                    bufferPtr_Tx++;
                }
                else
                {
                	pSpiInfo->SPI->DR = 0xFF;
                }

                if(++i >= wBufferLength)
                {
                    cTransmitCompleteFlag = 0;
                    break;                                                 /* 完成发送，退出 */
                }
            }

            /* 如果有数据就绪，读 */
            while(pSpiInfo->SPI->RXFLR > 0)
            {
                temp = (UINT8)(pSpiInfo->SPI->DR);
                if(pRxBuffer != (void *)NULL)
                {
                    *bufferPtr_Rx = temp;
                    bufferPtr_Rx++;
                }
            }

        }

        while((pSpiInfo->SPI->SR & SPI_BUSY) || (pSpiInfo->SPI->TXFLR > 0));/* 等待本次数据发送完成 */

        /* 读完所有接收到的FIFO */
        temp = (UINT8)(pSpiInfo->SPI->RXFLR);
        while(pSpiInfo->SPI->RXFLR > 0)
        {
            temp = (UINT8)(pSpiInfo->SPI->DR);
            if( pRxBuffer != (void *)NULL )
            {
                *bufferPtr_Rx = temp;
                bufferPtr_Rx++;
            }
        }


    }
    CPU_ExitCritical(psr_bk);                                               /* 退出临界保护 */

    return 0;
}

/*
 * SPI 通过DMA发送发送和传输数据
 * 输入参数：pSpiInfo：          SPI信息结构体指针；
 *        pTxBuffer     要发送数据的地址指针
 *        pRx_buffer：        接收数据的地址指针
 *        wBufferLength:  数据长度
 *        cbfunc         回调函数
 * 输出参数：返回 0， 成功; 其他，失败。
 * 说明：  1 目前DMA的突发传输长度设置为1item, 用4item会出现问题，尚需定位。
 *      2 cbfunc为NULL时工作在阻塞状态。
 *      3 目前使用DMA传输方式传输数据时，若波特率过大过小，均会导致头部多读1Byte无效数据，请注意。
 */
#define ENABLE_INT_BLOCK		(1 << 0)	 /* Block Transfer Complete Interrupt */
int SPI_TxRx_DMA(pStruct_SPIInfo pSpiInfo,
                 Enum_SPI_CS CS,
                 void   *pTxBuffer,
                 void   *pRxBuffer,
                 UINT16 wBufferLength,
                 void  (*cbfunc)(void *parg, UINT8 ch))
{
    SPI_DMA_InitStructure SPI_Tx_DMA_Init, SPI_Rx_DMA_Init;

    wBufferLength &= 0x3FF;                                         /* 最多支持10位长度(即1023Byte) */

    if (pSpiInfo == NULL)
    {
        return -1;
    }

    if ((pTxBuffer == NULL) || (wBufferLength == 0))
    {
        return -2;
    }

    /* DMA模式下不能低于此波特率 */
//    if (pSpiInfo->SPI_InitStruct.baudrate < (APB_FREQ / 20) )
//    {
//        return -3;
//    }

    if (pRxBuffer != NULL)
    {
        SPI_Rx_DMA_Init.length = wBufferLength;
        SPI_Rx_DMA_Init.memADDR = (void *)pRxBuffer;
        SPI_Rx_DMA_Init.intrmask = 0;                              /* 不产生中断 */
        SPI_Rx_DMA_Init.cbfunc = NULL;

        if ( SPI_DMA_P2M_Start(pSpiInfo, &SPI_Rx_DMA_Init) < 0 )   /* Rx DMA Enable */
        {
            return -4;
        }
    }

    SPI_Tx_DMA_Init.length = wBufferLength;
    SPI_Tx_DMA_Init.memADDR = (void *)pTxBuffer;
    SPI_Tx_DMA_Init.cbfunc = cbfunc;
    SPI_Tx_DMA_Init.intrmask = (cbfunc != NULL) ? ENABLE_INT_BLOCK : 0; /* 有回调函数就产生中断 */

    if (SPI_DMA_M2P_Start(pSpiInfo, &SPI_Tx_DMA_Init) )                 /* Tx DMA Enable */
    {
        return -5;
    }

    if (cbfunc == NULL)
    {
        UINT8 *pBuff = (UINT8 *)pSpiInfo->SPI_DMA_P2M_Initstruct.memADDR + pSpiInfo->SPI_DMA_P2M_Initstruct.length;
        UINT8 i = 0;

        DMA_WaitBlockTfrIdle();
		while((pSpiInfo->SPI->SR & SPI_BUSY) || (pSpiInfo->SPI->TXFLR > 0));/* 等待本次数据发送完成 */

        while(pSpiInfo->SPI->RXFLR > 0)
        {
            *(pBuff + i) = pSpiInfo->SPI->DR;
            i++;
        }

        /* 每次操作完需要关闭DMA通道，防止出现其他程序无法使用的情况。 */
        SPI_DMA_Free(pSpiInfo, pSpiInfo->SPI_DMA_ID_struct.DMA_M2P_ID);
        SPI_DMA_Free(pSpiInfo, pSpiInfo->SPI_DMA_ID_struct.DMA_P2M_ID);
    }

    return wBufferLength;
}

/*
 * SPI 读取外部设备的块数据(仅适用于cmc内部flash的批量获取数据)
 * 输入参数：pSpiInfo：                   SPI信息结构体指针；
 *        CS 片选值;
 *        pCmdBuffer:       命令数据的地址指针
 *        cCmdSize            数据的命令长度
 *        pRxBuffer         接收地址指针
 *        dwBufferLength       接收数据的长度.
 * 输出参数：返回 0， 成功; 其他，失败。
 * 说明：  Flash-READ模式进行接收，接收函数汇编实现。
 */
int SPI_ReadFlash(pStruct_SPIInfo pSpiInfo,
                   Enum_SPI_CS CS,
                   void *pCmdBuffer,
                   UINT8 cCmdSize,
                   void *pRxBuffer,
                   UINT32 dwBufferSize )
{
    UINT32 psr_bk;                                                              /* 临界保护开启时寄存器保存的值 */
    UINT8 cDataWidth = (pSpiInfo->SPI_InitStruct.datawidth > 8 ) ? (16) : (8);  /* SPI传输的数据宽度 */

    if ( (cCmdSize >= 16) || (dwBufferSize < 1) )                               /* 无效长度 */
    {
        return -1;
    }

    while( (pSpiInfo->SPI->SR & SPI_BUSY) || (pSpiInfo->SPI->TXFLR > 0) );      /* 等待空闲 如果有中断，会先处理完所有中断 */

    /* 如果CS发生改变，关闭SPI */
    if( (CS != SPI_CS_Last) &&
        (pSpiInfo->SPI_InitStruct.cs != CS ) )
    {
    	pSpiInfo->SPI_InitStruct.cs = CS;
        SPI_SelectCS(pSpiInfo);                                                 /* 重新选择CS，会自动调用SPI_Close. */
    }

    /* 因为这里没有其他参数改变了，所以不用SPI_Open */
    pSpiInfo->SPI->SPIENR = (!SPI_EN);                                          /* 关闭SPI */
    pSpiInfo->SPI->CTRLR0 |= (0x03 << 8);                                       /* EEPROM READ Mode */
    pSpiInfo->SPI->CTRLR1 = (dwBufferSize - 1);                                 /* 接收的数据长度(只需要数据长度) */
    pSpiInfo->SPI->SPIENR = SPI_EN;                                             /* 使能SPI */

    CPU_EnterCritical(&psr_bk);                                                 /* 进入临界保护 */

    SPI_ReadFlash_asm(pSpiInfo, pCmdBuffer, cCmdSize, pRxBuffer, dwBufferSize, cDataWidth);

    CPU_ExitCritical(psr_bk);//退出临界保护

    return 0;
}

/*
 * SPI 对外部设备写块数据(该函数仅用于CMC内部集成的flash批量写数据)
 * 输入参数：pSpiInfo：                      SPI信息结构体指针；
 *        CS                 片选值;
 *        pCmdBuffer:        命令数据的地址指针
 *        cCmdSize           数据的命令长度
 *        pTxBuffer          接收地址指针
 *        dwBufferSize       接收数据的长度
 * 输出参数：返回 0， 成功; 其他，失败。
 * 说明：  Flash-WRITE模式进行发送，发送函数汇编实现。
 */
int SPI_WriteFlash(pStruct_SPIInfo pSpiInfo,
                   Enum_SPI_CS CS,
                   void *pCmdBuffer,
                   UINT8 cCmdSize,
                   void *pTxBuffer,
                   UINT32 dwBufferSize )
{
    UINT32 psr_bk;                                                              /* 临界保护开启时寄存器保存的值 */
    UINT8  cDataWidth = (pSpiInfo->SPI_InitStruct.datawidth > 8 ) ? (16) : (8); /* SPI传输的数据宽度 */
    UINT16 wTemp = 0;

    if (cCmdSize >= 16)
    {
        return -1;
    }

    if (pTxBuffer == NULL)
    {
        dwBufferSize = 0;
    }

    while( (pSpiInfo->SPI->SR & SPI_BUSY) || (pSpiInfo->SPI->TXFLR > 0) );     /* 等待空闲 如果有中断，会先处理完所有中断 */

    /* 因为这里没有其他参数改变了，所以不用SPI_Open */
    pSpiInfo->SPI->SPIENR = (!SPI_EN);//关闭SPI
    wTemp = pSpiInfo->SPI->CTRLR0;
    wTemp &= ~(0x03 << 8);
    wTemp |= (0x01 << 8);//Transmit Only Mode
    pSpiInfo->SPI->CTRLR0 = wTemp;
    pSpiInfo->SPI->SPIENR = SPI_EN;//使能SPI

    CPU_EnterCritical(&psr_bk);//进入临界保护

    SPI_WriteFlash_asm(pSpiInfo, pCmdBuffer, cCmdSize, pTxBuffer, dwBufferSize, cDataWidth);

    CPU_ExitCritical(psr_bk);//退出临界保护

    return 0;
}

/*
 * SPI 设置所有参数
 * 输入参数：pSpiInfo： SPI信息结构体指针；
 *        Polarity    极性
 *        Phase       相位
 *        dwBaudrate    波特率    设置为0时可保持当前波特率
 *        cDataWidth   位宽
 *        cCS          片选
 * 输出参数：返回 0， 成功; 其他，失败
 */
int SPI_SetParameters(pStruct_SPIInfo pSpiInfo,
                      Enum_SPI_Polarity Polarity,
                      Enum_SPI_Phase Phase,
                      UINT32 dwBaudrate,
                      Enum_SPI_DataWidth cDataWidth,
                      Enum_SPI_CS CS)
{
    if(pSpiInfo->SPI->SPIENR != 0x00)
    {
        SPI_Close(pSpiInfo);
    }

    /* 配置极性和相位 */
    if((Polarity != pSpiInfo->SPI_InitStruct.polarity) ||
       (Phase != pSpiInfo->SPI_InitStruct.phase))
    {
        SPI_SetPolarityAndPhase(pSpiInfo, Polarity, Phase);
    }

    /* 配置波特率 */
    if((dwBaudrate != 0) && (dwBaudrate != pSpiInfo->SPI_InitStruct.baudrate))
    {
    	pSpiInfo->SPI_InitStruct.baudrate = dwBaudrate;
        SPI_SetBaudrate(pSpiInfo);
    }

    /* 配置位宽 */
    if( cDataWidth != pSpiInfo->SPI_InitStruct.datawidth )
    {
    	pSpiInfo->SPI_InitStruct.datawidth = cDataWidth;
        SPI_SetDataWidth(pSpiInfo);
    }

    /* 配置CS */
    if( CS != pSpiInfo->SPI_InitStruct.cs )
    {
    	pSpiInfo->SPI_InitStruct.cs = CS;
        SPI_SelectCS(pSpiInfo);
    }

    return 0;
}

/*
 * SPI初始化配置
 * 输入参数：pSpiInfo： SPI信息结构体指针。
 * 输出参数：返回 0， 成功; 其他，失败
 */
int SPI_InitConfig(pStruct_SPIInfo pSpiInfo)
{
    if(pSpiInfo == NULL)
    {
        return -1;
    }

    SPI_Close(pSpiInfo);

    /* 关闭DMA通道先 */
    if(pSpiInfo->SPI_DMA_ID_struct.DMA_M2P_ID != DMA_ID_INVALID)
    {
        SPI_DMA_Free(pSpiInfo, pSpiInfo->SPI_DMA_ID_struct.DMA_M2P_ID);    /* 释放DMA */
    }

    if(pSpiInfo->SPI_DMA_ID_struct.DMA_P2M_ID != DMA_ID_INVALID)
    {
        SPI_DMA_Free(pSpiInfo, pSpiInfo->SPI_DMA_ID_struct.DMA_P2M_ID);    /* 释放DMA */
    }

    /* 设置SPI的TMODE固定为00，即发送接收模式 */
    pSpiInfo->SPI->CTRLR0 &= ~(SPI_TMOD_BIT8 | SPI_TMOD_BIT9);

    SPI_SetBaudrate(pSpiInfo);
    SPI_SelectCS(pSpiInfo);
    SPI_SetDataWidth(pSpiInfo);
    SPI_SetTxFTLevel(pSpiInfo);
    SPI_SetRxFTLevel(pSpiInfo);
    SPI_SetIntrMask(pSpiInfo);

    SPI_SetPolarityAndPhase(pSpiInfo, pSpiInfo->SPI_InitStruct.polarity, pSpiInfo->SPI_InitStruct.phase);

    return 0;
}

/*
 * 根据SPI初始化参数配置指定的SPI
 * 输入参数：pSpiInfo： 初始化参数结构体。
 * 输出参数：DMA功能暂未使用
 */
void SPI_Config(pSPI_InitStructure pSpiInfo)
{
	pStruct_SPIInfo pInfo;

	if(pSpiInfo->SPI_BaseAddr == (UINT32)SPI_BASE_ADDR)
	{
		pInfo = pSPI;
	}
	else if(pSpiInfo->SPI_BaseAddr == (UINT32)SPI1_BASE_ADDR)
	{
		pInfo = pSPI1;
	}
	else
	{
		return ;
	}

	pInfo->SPI = (pStruct_SPI)pSpiInfo->SPI_BaseAddr;

	pInfo->SPI_InitStruct.baudrate = pSpiInfo->baudrate;
	pInfo->SPI_InitStruct.datawidth = pSpiInfo->datawidth;
	pInfo->SPI_InitStruct.cs = pSpiInfo->cs;
	pInfo->SPI_InitStruct.polarity = pSpiInfo->polarity;
	pInfo->SPI_InitStruct.phase = pSpiInfo->phase;
	pInfo->SPI_InitStruct.TxFTLevel = pSpiInfo->TxFTLevel;
	pInfo->SPI_InitStruct.RxFTLevel = pSpiInfo->RxFTLevel;
	pInfo->SPI_InitStruct.intrmask = pSpiInfo->intrmask;
	pInfo->SPI_ISR = NULL;

	/* 暂未使用DMA功能 */
	pInfo->SPI_DMA_M2P_Initstruct.memADDR = NULL;
	pInfo->SPI_DMA_P2M_Initstruct.memADDR = NULL;
	pInfo->SPI_DMA_ID_struct.DMA_M2P_ID = DMA_ID_INVALID;
	pInfo->SPI_DMA_ID_struct.DMA_P2M_ID = DMA_ID_INVALID;

	SPI_InitConfig(pInfo);
}
