/*
 文件: timer.c
 版本: CMC693 V1.10
 版权: 浙江杰芯科技有限公司
 作者: zhulin(zhulin2@supcon.com)
 描述：
修改记录：
=========
日期                                      作者                       工作                            内容
*/

#include "datatype.h"
#include "lib_timer.h"
#include "intc.h"

void Timer0_ISRHandler(UINT32 irqid);
void Timer1_ISRHandler(UINT32 irqid);
void Timer2_ISRHandler(UINT32 irqid);
void Timer3_ISRHandler(UINT32 irqid);

extern UINT32 APB_FREQ;

static Struct_TimerInfo Timer_Table[] ={
    {0, TIMER0_BASSADDR,  FALSE, 0,{"TIMER0",TIMER_IRQ0,0,Timer0_ISRHandler,0,NULL}},
    {1, TIMER1_BASSADDR,  FALSE, 0,{"TIMER1",TIMER_IRQ1,0,Timer1_ISRHandler,0,NULL}},
    {2, TIMER2_BASSADDR,  FALSE, 0,{"TIMER2",TIMER_IRQ2,0,Timer2_ISRHandler,0,NULL}},
    {3, TIMER3_BASSADDR,  FALSE, 0,{"TIMER3",TIMER_IRQ3,0,Timer3_ISRHandler,0,NULL}}
};

#define TIMERID_MAX  (sizeof(Timer_Table) / sizeof(Struct_TimerInfo) - 1)

/*
 * 获取指定的定时器中断状态
 * 输入参数：enTimerid：    TIMER0,
 *                     TIMER1,
 *                     TIMER2,
 *                     TIMER3
 * 输出参数：TIMER_INTR_ACTIVE,已触发中断
 *        TIMER_INTR_INACTIVE，未触发中断
 */
UINT8 Timer_GetIntStatus(Enum_Timer_Device enTimerid)
{
    pStruct_TIMERInfo pInfo;
    pStruct_TIMER pTimer;

    pInfo = &(Timer_Table[enTimerid]);
    pTimer = pInfo->pAddr ;

    return (pTimer->IntStatus & 0x01)?TIMER_INTR_ACTIVE:TIMER_INTR_INACTIVE;
}

/*
 * 打开一个指定的定时器，配置中断模式，注册中断函数，取消中断屏蔽，但未使能定时器
 * 输入参数：enTimerid：    TIMER0,
 *                     TIMER1,
 *                     TIMER2,
 *                     TIMER3
 * 输出参数：SUCCESS或FAILURE
 */
INT32S Timer_Open(Enum_Timer_Device enTimerid)
{
    pStruct_TIMERInfo pInfo;
    pStruct_TIMER pTimer;
	
    if((enTimerid < 0) || (enTimerid > TIMERID_MAX))
    {
	    return FAILURE;
    }
    pInfo = &(Timer_Table[enTimerid]);
    pTimer = pInfo->pAddr ;
    if(pInfo->bopened){ return FAILURE;}

    if(NULL != pInfo->IrqHandler.handler)                                     /* 初始化中断 */
    {
    	INTC_FreeIrq(&(pInfo->IrqHandler));
        INTC_RequestIrq(&(pInfo->IrqHandler));
    }
    pInfo->bopened = TRUE;

    pTimer->Control &= ~(TIMER_TXCONTROL_INTMASK);                            /* 不屏蔽中断，使能定时器中断 */

    return SUCCESS;
}

/*
 * 关闭一个指定的定时器，清除中断配置，释放已注册的中断函数，屏蔽中断，禁能定时器
 * 输入参数：enTimerid：    TIMER0,
 *                     TIMER1,
 *                     TIMER2,
 *                     TIMER3
 * 输出参数：SUCCESS或FAILURE
 */
INT32S Timer_Close(Enum_Timer_Device enTimerid)
{
    pStruct_TIMERInfo pInfo;
	pStruct_TIMER pTimer;

    if ((enTimerid < 0) || (enTimerid > TIMERID_MAX)){ return FAILURE;}

    pInfo = &(Timer_Table[enTimerid]);
    pTimer = pInfo->pAddr;
    if(!(pInfo->bopened))
    {
    	return FAILURE;
    }

    pTimer->Control &= ~TIMER_TXCONTROL_ENABLE;                        /* 禁能定时器 */
    pTimer->Control |= TIMER_TXCONTROL_INTMASK;                        /* 屏蔽中断 */
    INTC_FreeIrq(&(pInfo->IrqHandler));                                /* 释放已被注册的中断处理函数 */
    pInfo->bopened = FALSE;

    return SUCCESS;
}

/*
 * 启动一个指定的定时器。设置定时器的工作模式，装载值。
 * 输入参数：enTimerid：    TIMER0,
 *                     TIMER1,
 *                     TIMER2,
 *                     TIMER3
 *        dwTimeout： 定时的时间，单位为us
 * 输出参数：SUCCESS或FAILURE
 */
INT32S Timer_Start(Enum_Timer_Device enTimerid,UINT32 dwTimeout)
{
    UINT32 dwLoad;
    pStruct_TIMERInfo pInfo;
    pStruct_TIMER pTimer;

    if((enTimerid < 0) || (enTimerid > TIMERID_MAX)){ return FAILURE;}
 
    pInfo = &(Timer_Table[enTimerid]);
    pTimer = pInfo->pAddr;

    if(!(pInfo->bopened))
    {
	    return FAILURE;
    }
	
    dwLoad = (UINT32)((APB_FREQ / 1000000) * dwTimeout);

    pTimer->Control &= ~TIMER_TXCONTROL_ENABLE;                        /* 先禁能定时器 */

    pTimer->LoadCount = dwLoad;
    pInfo->dwTimeout = dwTimeout;

    pTimer->Control |= TIMER_TXCONTROL_MODE;                           /* 选择用户自定义计数模式 */
    pTimer->Control |= TIMER_TXCONTROL_ENABLE;                         /* 使能定时器 */

    return SUCCESS;
}

/*
 * 停止一个指定的定时器，并返回停止时刻的计数值
 * 输入参数：enTimerid：    TIMER0,
 *                     TIMER1,
 *                     TIMER2,
 *                     TIMER3
 * 输出参数：停止时刻的计数器值
 */
UINT32 Timer_Stop(Enum_Timer_Device enTimerid)
{
    UINT32 dwStopVal;
    pStruct_TIMERInfo pInfo;
    pStruct_TIMER pTimer;

    pInfo = &(Timer_Table[enTimerid]);
    if(!(pInfo->bopened))
    {
	    return FAILURE;
    }

    pTimer = pInfo->pAddr;
    dwStopVal = pTimer->CurrentValue;
    pTimer->Control &= ~TIMER_TXCONTROL_ENABLE;              /* 禁能定时器 */

    return dwStopVal;
}

/*
 * 清除一个指定的定时器中断状态，通过读EOI寄存器来清除中断状态
 * 输入参数：enTimerid：    TIMER0,
 *                     TIMER1,
 *                     TIMER2,
 *                     TIMER3
 * 输出参数：
 */
void Timer_ClearIrqFlag(Enum_Timer_Device enTimerid)
{
    pStruct_TIMERInfo pInfo;
    pStruct_TIMER pTimer;

    pInfo = &(Timer_Table[enTimerid]);
    pTimer = pInfo->pAddr ;
    *((volatile UINT32 *)(&(pTimer->EOI)));
}

/*
 * 读取指定的定时器当前计数值
 * 输入参数：enTimerid：    TIMER0,
 *                     TIMER1,
 *                     TIMER2,
 *                     TIMER3
 * 输出参数：当前计数器值
 */
UINT32 Timer_CurrentValue(Enum_Timer_Device enTimerid)
{
    UINT32 dwCurrentVal;
    pStruct_TIMERInfo pInfo;
    pStruct_TIMER pTimer;

    pInfo = &(Timer_Table[enTimerid]);
    pTimer = pInfo->pAddr;
    dwCurrentVal = pTimer->CurrentValue;
    return dwCurrentVal ;
}

/*
 * 读取指定的定时器装载值
 * 输入参数：enTimerid：    TIMER0,
 *                     TIMER1,
 *                     TIMER2,
 *                     TIMER3
 * 输出参数：计数器装载值
 */
UINT32 Timer_LoadValue(Enum_Timer_Device enTimerid)
{
    pStruct_TIMERInfo pInfo;
    pStruct_TIMER pTimer;

    pInfo = &(Timer_Table[enTimerid]);
    pTimer = pInfo->pAddr;
    return pTimer->LoadCount;
}

/*
 * 根据初始化结构体信息，配置Timer参数
 * 输入参数：pTimerInfo timer初始化结构体
 * 输出参数：
 */
void Timer_Config(pTimer_InitStructure pTimerInitInfo)
{
	pStruct_TIMERInfo pTimerInfo;

	pTimerInfo = &Timer_Table[pTimerInitInfo->enTimerID];
	pTimerInfo->dwTimeout = pTimerInitInfo->dwTimeout;
	pTimerInfo->IrqHandler.priority = pTimerInitInfo->wPriority;

    if(pTimerInitInfo->TIMER_ISR != NULL)
    {
    	pTimerInfo->IrqHandler.handler = pTimerInitInfo->TIMER_ISR;
    }

	Timer_Open(pTimerInfo->enID);
	Timer_Start(pTimerInfo->enID,pTimerInfo->dwTimeout);
}

/* Timer0中断处理函数 */
void Timer0_ISRHandler(UINT32 irqid)
{
	Timer_ClearIrqFlag(TIMER0);
}

/* Timer1 中断处理函数 */
void Timer1_ISRHandler(UINT32 irqid)
{
	Timer_ClearIrqFlag(TIMER1);
}

/* Timer2 中断处理函数 */
void Timer2_ISRHandler(UINT32 irqid)
{
	Timer_ClearIrqFlag(TIMER2);
}

/* Timer3 中断处理函数 */
void Timer3_ISRHandler(UINT32 irqid)
{
	Timer_ClearIrqFlag(TIMER3);
}
