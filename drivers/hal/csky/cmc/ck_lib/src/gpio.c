/*
 文件: gpio.c
 版本: CMC693 V1.10
 版权: 浙江杰芯科技有限公司
 作者: 喻文星 (yuwenxing@supcon.com)
 描述：GPIO有以下硬件信息：
	 1.总共84个GIPO：GPIO0有20个分别是4个PortA和16个PortB，GPIO1有64个分别是32个PortA和32个PortB
	 2.只有PortA可以产生中断，PortB不可产生中断
	 3.GPIO0的4个PortA有各自独立的中断源，GPIO1的32个PortA共用1个中断源
	 4.有普通中断和快速中断两种，普通中断无法打断普通中断和快速中断，快速中断可以打断普通中断，即最多中断嵌套一次
	 5.边缘触发中断可以通过写寄存器清除，电平触发中断无法通过写寄存器清除中断，只能通过轮询等待、中断屏蔽、中断失能进行清除，否则会反复进中断
	 6.GPIO0的PortB、GPIO1的PortA和PortB具有复用功能，GPIO0的PortA没有复用功能
	 7.GPIO0PortA的中断信号同时作为CPU、LCP、MCP三个的中断控制器的输入，对GPIO0PortA的配置会对LCP和MCP的中断有影响
修改记录：
=========
日期                                      作者                       工作                            内容
-------------------------------------------------------
2018-06-27    喻文星                              修改                           规范编程风格
2019-01-12    朱林                                 修改                            规范编程风格，修改GPIO_Config内的中断使能bug
                                                                                                                删除无用的函数接口，增加新的函数接口
*/

#include 	"gpio.h"
#include    "intc.h"

Struct_IRQHandler GPIOIntr_Table[]={
		{"GPIO0A_0",INTC_GPIO0A0,0,NULL,0,NULL},
		{"GPIO0A_1",INTC_GPIO0A1,0,NULL,0,NULL},
		{"GPIO0A_2",INTC_GPIO0A2,0,NULL,0,NULL},
		{"GPIO0A_3",INTC_GPIO0A3,0,NULL,0,NULL},
		{"GPIO1A",INTC_GPIO1A,0,NULL,0,NULL}
};

/*
 * 读取GPIO端口所有引脚的输入电平值
 * 输入参数：pGpio： I/O，可选GPIO0、 GPIO1
 *         ePort： 端口，可选PORTA、 PORTB
 * 输出参数：返回 端口所有引脚的输入电平值
 */
UINT32 GPIO_ReadInputPortData(pStruct_GPIOInfo pGpio, Enum_GPIO_Port ePort)
{
//	ASSERT(((pGpio == GPIO0) || (pGpio == GPIO1)) && (( ePort == PORTA ) || ( ePort == PORTB )),
//				"  E2_2_x:  Arguments Error! ", 0);

	return (PORTA == ePort)?pGpio->EXT_PORTA:pGpio->EXT_PORTB;
}

/*
 * 读取指定管脚的输入电平值
 * 输入参数：pGpio： I/O，可选GPIO0、 GPIO1
 *         ePort： 端口，可选PORTA、 PORTB
 *         dwPin: GPIO_P0~GPIO_P31
 * 输出参数：读取到的管脚输入电平值
 */
UINT8 GPIO_ReadInputPinData(pStruct_GPIOInfo pGpio, Enum_GPIO_Port ePort, UINT32 dwPin)
{
	UINT8 cPinData = 0;

//	ASSERT(((pGpio == GPIO0) || (pGpio == GPIO1)) && (( ePort == PORTA ) || ( ePort == PORTB )),
//				"  E2_2_x:  Arguments Error! ", 0);

	if(ePort == PORTA)                                       /* 读取PORTA的GPIO引脚 */
	{
		cPinData = ((pGpio->EXT_PORTA & dwPin) != 0)?1:0;
	}
	else                                                     /* 读取PORTB的GPIO引脚 */
	{
		cPinData = ((pGpio->EXT_PORTB & dwPin) != 0)?1:0;
	}

	return cPinData;
}

/*
 * 读取GPIO端口所有引脚的输出电平值
 * 输入参数：pGpio： I/O，可选GPIO0、 GPIO1
 *         ePort： 端口，可选PORTA、 PORTB
 * 输出参数：返回 端口所有引脚的输出电平值
 */
UINT32 GPIO_ReadOutputPortData(pStruct_GPIOInfo pGpio, Enum_GPIO_Port ePort)
{
//	ASSERT(((pGpio == GPIO0) || (pGpio == GPIO1)) && (( ePort == PORTA ) || ( ePort == PORTB )),
//				"  E2_2_x:  Arguments Error! ", 0);

	return (PORTA == ePort)?pGpio->SWPORTA_DR:pGpio->SWPORTB_DR;
}

/*
 * 读取指定管脚的输出电平值
 * 输入参数：pGpio： I/O，可选GPIO0、 GPIO1
 *         ePort： 端口，可选PORTA、 PORTB
 *         dwPin: GPIO_P0~GPIO_P31
 * 输出参数：读取到的管脚输电平出值
 */
UINT8 GPIO_ReadOutputPinData(pStruct_GPIOInfo pGpio, Enum_GPIO_Port ePort, UINT32 dwPin)
{
	UINT8 cPinData = 0;

//	ASSERT(((pGpio == GPIO0) || (pGpio == GPIO1)) && (( ePort == PORTA ) || ( ePort == PORTB )),
//				"  E2_2_x:  Arguments Error! ", 0);

	if(ePort == PORTA)                                       /* 读取PORTA的GPIO引脚 */
	{
		cPinData = ((pGpio->SWPORTA_DR & dwPin) != 0)?1:0;
	}
	else                                                     /* 读取PORTB的GPIO引脚 */
	{
		cPinData = ((pGpio->SWPORTB_DR & dwPin) != 0)?1:0;
	}

	return cPinData;
}

/*
 * 用于写指定位（可以为多位）指定值（可0可1）
 * 输入参数：pGpio：I/O，可选GPIO0、 GPIO1
 *        ePort：端口，可选PORTA、 PORTB
 *        dwPins：GPIO_P0~GPIO_P31，可以多个引脚
 *        cPinVal：0或1
 * 输出参数：返回TRUE
 * 说明：
 */
BOOL GPIO_WritePins(pStruct_GPIOInfo pGpio, Enum_GPIO_Port ePort, UINT32 dwPins, UINT8 cPinVal)
{
//	ASSERT(((pGpio == GPIO0) || (pGpio == GPIO1)) && (( ePort == PORTA ) || ( ePort == PORTB )),
//				"  E2_2_x:  Arguments Error! ", 0);

	if(cPinVal == 0)
	{
		if(ePort == PORTA)
		{
			pGpio->SWPORTA_DR &= ~dwPins;
		}
		else
		{
			pGpio->SWPORTB_DR &= ~dwPins;
		}
	}
	else
	{
		if(ePort == PORTA)
		{
			pGpio->SWPORTA_DR |= dwPins;
		}
		else
		{
			pGpio->SWPORTB_DR |= dwPins;
		}
	}

	return TRUE;
}

/*
 * 设置管脚复用功能,即设置管脚的硬件模式
 * 输入参数：pGpio： I/O，可选GPIO0、 GPIO1
 *         ePort： 端口，可选PORTA、 PORTB
 *         dwPins: 所指定的管脚
 *         bHardwareMode：是否打开复用功能，TRUE打开复用功能，FALSE不打复用功能
 * 输出参数：返回 TRUE
 * 说明：GPIO0的PortA无复用功能
 */
BOOL GPIO_SetReuse(pStruct_GPIOInfo pGpio, Enum_GPIO_Port ePort, UINT32 dwPins, BOOL bHardwareMode)
{
//	ASSERT(((GPIO1 == Gpio) || ((GPIO0 == Gpio) && (PORTB == Port)))," E2_2_x:  Arguments Error! \n", 0);

	if(bHardwareMode == TRUE)
	{
		if(ePort == PORTA)
		{
			pGpio->PORTA_CTL |= dwPins;
		}
		else
		{
			pGpio->PORTB_CTL |= dwPins;
		}
	}
	else
	{
		if(ePort == PORTA)
		{
			pGpio->PORTA_CTL &= (~dwPins);
		}
		else
		{
			pGpio->PORTB_CTL &= (~dwPins);
		}
	}

	return TRUE;
}

/*
 * 设置管脚为输入或输出
 * 输入参数：pGpio： I/O，可选GPIO0、 GPIO1
 *         ePort： 端口，可选PORTA、 PORTB
 *         dwPins: 所指定的管脚
 *         eDirection：管脚方向，输入或输出
 * 输出参数：返回 TRUE
 */
BOOL GPIO_SetDirection(pStruct_GPIOInfo pGpio, Enum_GPIO_Port ePort, UINT32 dwPins, Enum_GPIO_Direction eDirection)
{
//	ASSERT(((Gpio == GPIO0) || (Gpio == GPIO1)) && (( Port == PORTA ) || ( Port == PORTB )),
//				"  E2_2_x:  Arguments Error! ", 0);

	if (eDirection == GPIO_INPUT)
	{
		if(ePort == PORTA)
		{
			pGpio->SWPORTA_DDR &= (~dwPins);
		}
		else
		{
			pGpio->SWPORTB_DDR &= (~dwPins);
		}
	}
	else
	{
		if(ePort == PORTA)
		{
			pGpio->SWPORTA_DDR |= dwPins;
		}
		else
		{
			pGpio->SWPORTB_DDR |= dwPins;
		}
	}

	return TRUE;
}

/*
 * 配置管脚中断信号触发方式
 * 输入参数：pGpio： I/O，可选GPIO0、 GPIO1
 *         dwPins: 所指定的管脚
 *         eBurstMode：触发方式，有电平（高电平和低电平）模式和边缘（上升沿和下降沿）模式
 * 输出参数：返回 TRUE
 * 说明：未指定Port是因为只有PortA有具有中断功能
 *      边缘触发中断可以通过写寄存器清除，电平触发中断无法通过写寄存器清除中断，只能通过轮询等待、中断屏蔽、中断失能进行清除，否则会反复进中断
 */
BOOL GPIO_SetInterruptLevelAndPolarity(pStruct_GPIOInfo pGpio, UINT32 dwPins, Enum_GPIO_IRQBurst eBurstMode)
{
//    ASSERT((GPIO0 == Gpio) || (GPIO1 == Gpio), "E2_2_: para1 must be GPIO0 or GPIO1", 0);

	switch(eBurstMode)
	{
		case GPIO_Intact_High_Level:
			pGpio->INTTYPE_LEVEL &= (~dwPins); 	               /* 设置指定引脚中断触发方式为0电平触发 */
			pGpio->INT_POLARITY |= dwPins;  		               /* 设置指定引脚中断触发极性为1高电平触发 */
			break;
		case GPIO_Intact_Low_Level:
			pGpio->INTTYPE_LEVEL &= (~dwPins);                  /* 设置指定引脚中断触发方式为0电平触发 */
			pGpio->INT_POLARITY &= (~dwPins);                   /* 设置指定引脚中断触发极性为0低电平触发 */
			break;
		case GPIO_Intact_High_Edge:
			pGpio->INTTYPE_LEVEL |= dwPins;                     /* 设置指定引脚中断触发方式为1边沿触发 */
			pGpio->INT_POLARITY |= dwPins;                      /* 设置指定引脚中断触发方式为1上升沿触发 */
			break;
		case GPIO_Intact_Low_Edge:
			pGpio->INTTYPE_LEVEL |= dwPins;                     /* 设置指定引脚中断触发方式为1边沿触发 */
			pGpio->INT_POLARITY &= (~dwPins);                   /* 设置指定引脚中断触发方式为0下降沿触发 */
			break;
		default:
			break;
	}

	return TRUE;
}

/*
 * 清除指定引脚所产生的中断，仅适用于边沿触发方式的中断
 * 输入参数：pGpio： I/O，可选GPIO0、 GPIO1
 *         dwPins: 所指定的管脚
 * 输出参数：返回 TRUE
 * 说明：未指定Port是因为只有PortA有具有中断功能；
 *     边缘触发中断可以通过写寄存器清除，电平触发中断无法通过写寄存器清除中断，只能通过轮询等待、中断屏蔽、中断失能进行清除，否则会反复进中断
 */
BOOL GPIO_ClearInterrupt(pStruct_GPIOInfo pGpio, UINT32 dwPins)
{
    UINT32 psrbk;

//    ASSERT((GPIO0 == Gpio) || (GPIO1 == Gpio), "E2_2_: para1 must be GPIO0 or GPIO1", 0);

    CPU_EnterCritical(&psrbk);
    pGpio->PORTA_EOI = (dwPins);
    CPU_ExitCritical(psrbk);

    return TRUE;
}

/*
 * 屏蔽指定引脚的中断
 * 输入参数：pGpio： I/O，可选GPIO0、 GPIO1
 *         dwPins: 所指定的管脚
 * 输出参数：
 * 说明：未指定Port是因为只有PortA有具有中断功能；
 *     边缘触发中断可以通过写寄存器清除，电平触发中断无法通过写寄存器清除中断，只能通过轮询等待、中断屏蔽、中断失能进行清除，否则会反复进中断
 */
void GPIO_MaskInterrupt(pStruct_GPIOInfo pGpio, UINT32 dwPins)
{
	pGpio->INTMASK |= dwPins;
}

/*
 * 解除指定引脚的屏蔽
 * 输入参数：pGpio： I/O，可选GPIO0、 GPIO1
 *         dwPins: 所指定的管脚
 * 输出参数：
 * 说明：未指定Port是因为只有PortA有具有中断功能；
 *     边缘触发中断可以通过写寄存器清除，电平触发中断无法通过写寄存器清除中断，只能通过轮询等待、中断屏蔽、中断失能进行清除，否则会反复进中断
 */
void GPIO_UnmaskInterrupt(pStruct_GPIOInfo pGpio, UINT32 dwPins)
{
	pGpio->INTMASK &= (~dwPins);
}

/*
 * 使能指定引脚的中断
 * 输入参数：pGpio： I/O，可选GPIO0、 GPIO1
 *         dwPins: 所指定的管脚
 * 输出参数：返回 TRUE
 * 说明：未指定Port是因为只有PortA有具有中断功能；
 *     边缘触发中断可以通过写寄存器清除，电平触发中断无法通过写寄存器清除中断，只能通过轮询等待、中断屏蔽、中断失能进行清除，否则会反复进中断
 */
BOOL GPIO_EnableInterrupt(pStruct_GPIOInfo pGpio, UINT32 dwPins)
{
	UINT32 psrbk;

//	ASSERT((GPIO0 == Gpio) || (GPIO1 == Gpio), "E2_2_2: para1 must be GPIO0 or GPIO1", 0);

    CPU_EnterCritical(&psrbk);
    pGpio->INTEN |= dwPins;
    CPU_ExitCritical(psrbk);

    return TRUE;
}

/*
 * 失能指定引脚的中断
 * 输入参数：pGpio： I/O，可选GPIO0、 GPIO1
 *         dwPins: 所指定的管脚
 * 输出参数：返回 TRUE
 * 说明：未指定Port是因为只有PortA有具有中断功能；
 */
BOOL GPIO_DisableInterrupt(pStruct_GPIOInfo pGpio, UINT32 dwPins)
{
    UINT32 psrbk;

//	ASSERT((GPIO0 == Gpio) || (GPIO1 == Gpio), "E2_2_3: para1 must be GPIO0 or GPIO1", FALSE);

    CPU_EnterCritical(&psrbk);
    pGpio->INTEN &= ~(dwPins);
    CPU_ExitCritical(psrbk);

    return TRUE;
}

/*
 * 根据初始化结构体信息，配置GPIO参数，若开启硬件模式，则其他参数无需配置,只需提供GPIOAddr、
 * GPIOPort、GPIOPin和bHardwareMode
 * 输入参数：pGPIOInfo GPIO初始化结构体
 * 输出参数：
 * 说明：由于gpio1 portA的所有pin共用一个中断，所以在对多个pin使能中断功能时，最后一次注册
 * 的中断处理函数有效。
 */
void GPIO_Config(pGPIO_InitStructure pGPIOInfo)
{
	pStruct_IRQHandler pGPIOIntrInfo;

	GPIO_SetReuse(pGPIOInfo->GPIOAddr, pGPIOInfo->GPIOPort,
	          pGPIOInfo->GPIOPin, pGPIOInfo->bHardwareMode);                              /* 设置gpio是否开启硬件模式 */

	if(pGPIOInfo->bHardwareMode == FALSE)                                                 /* 未开启硬件模式，则当做普通GPIO口 */
	{
		GPIO_SetDirection(pGPIOInfo->GPIOAddr, pGPIOInfo->GPIOPort,
						  pGPIOInfo->GPIOPin, pGPIOInfo->GPIODirection);                  /* 设置gpio输入输出模式 */

		if(pGPIOInfo->GPIOIntr != GPIO_NULL_INTR)                                         /* 是否需要配置中断 */
		{
			GPIO_ClearInterrupt(pGPIOInfo->GPIOAddr, pGPIOInfo->GPIOPin);                 /* 清除GPIO中断标志，只有GPIO0_A和GPIO1_A支持中断  */
			GPIO_SetInterruptLevelAndPolarity(pGPIOInfo->GPIOAddr,
									   pGPIOInfo->GPIOPin,pGPIOInfo->BurstMode);          /* 设置gpio中断触发方式 */

			pGPIOIntrInfo = &GPIOIntr_Table[pGPIOInfo->GPIOIntr];
			pGPIOIntrInfo->priority = pGPIOInfo->priority;
            if(pGPIOInfo->GPIO_ISR != NULL)                                               /* 若用户定义了中断处理函数，则使用该函数 */
            {
            	pGPIOIntrInfo->handler = pGPIOInfo->GPIO_ISR;
            }

            if(pGPIOIntrInfo->handler != NULL)                                            /* 若中断处理函数未定义，则不开启中断 */
            {
				INTC_FreeIrq(pGPIOIntrInfo);                                              /* 如果存在已注册中断函数，则清除 */
				INTC_RequestIrq(pGPIOIntrInfo);                                           /* 注册中断函数 */
				GPIO_EnableInterrupt(pGPIOInfo->GPIOAddr, pGPIOInfo->GPIOPin);            /* 使能中断 */
            }
		}
	}
}
