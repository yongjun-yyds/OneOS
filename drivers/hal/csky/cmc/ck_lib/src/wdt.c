/*
 文件: wdt.c
 版本: CMC693 V1.10
 版权: 浙江杰芯科技有限公司
 作者: zhulin(zhulin2@supcon.com)
 描述：
修改记录：
=========
日期                                      作者                       工作                            内容
*/
#include "wdt.h"
#include "datatype.h"
#include "intc.h"

extern UINT32 APB_FREQ;

Struct_IRQHandler WDTIntr_Table[]={
		{"WDT",INTC_WDT,0,NULL,0,NULL}
};

/*
 * 设置WDT复位信号保持时间
 * 输入参数：cCycles: 用于控制WDT产生的系统复位信号的时间，一般为几个pclk cycles
 *         可设置如下倍数的pclk cycles
 *         WDT_PclkCycles_2,
 *         WDT_PclkCycles_4,
 *         WDT_PclkCycles_8,
 *         WDT_PclkCycles_16,
 *         WDT_PclkCycles_32,
 *         WDT_PclkCycles_64,
 *         WDT_PclkCycles_128,
 *         WDT_PclkCycles_256
 * 输出参数：void
 * 说明：
 */
void WDT_SetSystemResetCycles(UINT8 cCycles)
{
    WDT->CR &= (~WDT_PclkCycles_Bit);       /* 清零分频因子RPL位 */
    WDT->CR |= cCycles;
}

/*
 * 设置WDT计数器的超时周期初始值
 * 输入参数：cValue:需要配置的参数，范围：0~15
 * 输出参数：void
 * 说明：用于配置寄存器WDT_TORR中的TOP_INIT位，在第一次启动WDT计数器时，根据此寄存器的
 *      配置装载计数器值，必须在使能WDT计数器前配置.
 *      超时周期=2的（16+cValue）次方
 */
void WDT_SetInitPeriod(UINT8 cValue)
{
    WDT->TORR &= (~WDT_TopInit_Bit);      /* 清零TOP_INIT位 */
    WDT->TORR |= (cValue<<4);             /* 设置TOP_INIT的值 */
}

/*
 * 设置WDT计数器的超时周期
 * 输入参数：cValue:需要配置的参数，范围：0~15
 * 输出参数：void
 * 说明：用于配置寄存器WDT_TORR中的TOP位，在第二次启动WDT计数器时及以后，根据此寄存器的
 *      配置装载计数器值.可在WDT启动后任意时刻更改超时周期，但要在下一次启动计数器时生效。
 *      超时周期=2的（16+cValue）次方
 */
void WDT_SetPeriod(UINT8 cValue)
{
    WDT->TORR &= (~WDT_Top_Bit);         /* 清零TOP位 */
    WDT->TORR |= cValue;                 /* 设置TOP的值 */
}

/*
 * 设置WDT的工作模式
 * 输入参数：cMode：工作模式
 *        WDT_CR_Mode_SYSReset，当计数器溢出后直接产生系统复位
 *        WDT_CR_Mode_Intr，当计数器第一次溢出时产生中断，第二次溢出且中断标志未清除
 *                         则产生系统复位
 * 输出参数：无
 * 说明：
 */
void WDT_SetMode(UINT8 cMode)
{
	WDT->CR &= (~WDT_CR_Mode_Bit);      /* 清零RMOD位 */
    WDT->CR |= cMode;
}

/*
 * 使能WDT
 * 输入参数：无
 * 输出参数：无
 * 说明：
 */
void WDT_Enable(void)
{
    WDT->CR |= WDT_CR_EN;
}

/*
 * 获取WDT当前计数器的值
 * 输入参数：无
 * 输出参数：WDT当前的计数器值
 * 说明：
 */
UINT32 WDT_GetCounter(void)
{
    return WDT->CCVR;
}

/*
 * 清除WDT中断标志
 * 输入参数：无
 * 输出参数：无
 * 说明：通过读EOI寄存器可清除中断标志，但不会改变计数器的值
 */
UINT32 WDT_ClearIntr(void)
{
    UINT32 psrbk;
    UINT32 dwV;

    CPU_EnterCritical(&psrbk);
    dwV = WDT->EOI;
    CPU_ExitCritical(psrbk);
    return dwV;
}

/*
 * 配置WDT
 * 输入参数：pWDTInfo:为初始化参数的结构体指针，其具体参数如下：
 *        cMode:设置WDT的工作模式，可选值如下：
 *               WDT_CR_Mode_SYSReset，当计数器溢出后直接产生系统复位
 *               WDT_CR_Mode_Intr，当计数器第一次溢出时产生中断，第二次溢出且中断标志未清除
 *                               则产生系统复位
 *        dwMs：超时时间-毫秒，范围1ms~42s
 *        dwPrio:中断优先级，此参数在WDT设置为WDT_CR_Mode_Intr工作模式有效，
 *               WDT设置为WDT_CR_Mode_SYSReset工作模式时无效，可为任一值
 *        WDT_ISR：用户自定义的中断处理函数。
 * 输出参数：void
 * 说明：该函数内部固定选择WDT系统复位时间为2个pclk cycles，并启动WDT；若cMode为WDT_CR_Mode_Intr，则
 *      函数内会注册中断回调函数，中断回调函数接口在bsp.c中由用户实现具体功能。
 *      超时周期=2的（16+cTOP）次方，cTOP为TOP_INIT或者TOP寄存器的值,范围0~15
 *      函数内部根据dwMs计算出TOP和TOP_INIT的值，实际计算出的超时时间总是大于等于设置的
 *      超时时间
 */
void WDT_Config(pWDT_InitStructure pWDTInfo)
{
	UINT32 dwCounter;
	UINT8 cTOP;
	pStruct_IRQHandler pWDTIntrInfo;

	WDT_SetMode(pWDTInfo->cMode);                                            /* 设置WDT的工作模式 */

	WDT_SetSystemResetCycles(WDT_PclkCycles_8);                    /* 设置WDT的系统复位保持时间为8个pclk cycles */

	dwCounter = (APB_FREQ/1000) * pWDTInfo->dwMs;                            /* 根据超时时间计算超时计数器的值 */
	for(cTOP = 0; cTOP <= 15; cTOP++)                              /* 根据超时计数器的值计算TOP和TOP_INIT的值 */
	{
		if(dwCounter < ((UINT32)65536 << cTOP))
		{
			break;
		}
	}

	WDT_SetInitPeriod(cTOP);                                       /* 设置TOP和TOP_INIT寄存器的值 */
	WDT_SetPeriod(cTOP);

	if(pWDTInfo->cMode == WDT_CR_Mode_Intr)                                  /* 若选择中断模式，则配置WDT的中断 */
	{
		pWDTIntrInfo = WDTIntr_Table;

		if(pWDTInfo->WDT_ISR != NULL)
		{
			pWDTIntrInfo->handler = pWDTInfo->WDT_ISR;
		}

		if(pWDTIntrInfo->handler != NULL)
		{
			WDT_ClearIntr();                                           /* 清除中断标志 */
			pWDTIntrInfo->priority = pWDTInfo->dwPrio;
			INTC_RequestIrq(pWDTIntrInfo);                             /* 注册中断函数 */
		}
	}

	WDT_Enable();                                                  /* 使能WDT */
}

/*
 * 喂狗，重启WDT计数器,向WDT_CRR寄存器写入0x76，使WDT重新装在计数器的值，并
 * 重新启动计数器
 * 输入参数：void
 * 输出参数：void
 * 说明：
 */
void WDT_Restart(void)
{
    WDT->CRR = WDT_CRR_RESTART;
}
