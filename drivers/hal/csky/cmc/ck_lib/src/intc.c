/*
 文件: intc.c
 版本: CMC693 V1.10
 版权: 浙江杰芯科技有限公司
 作者: zhulin(zhulin2@supcon.com)
 描述：
修改记录：
=========
日期                                      作者                       工作                            内容
*/


#include "datatype.h"
#include "intc.h"
#include "ckintc.h"
#include "powm.h"

#include "misc.h"
#include "cache.h"

/*
 * NR_IRQS: The number of member in normal_irq_list and fast_irq_list 
 *          respective.It is equal to the number of priority levels.
 */
#define NR_IRQS  32

/*
 * normal_irq_list[NR_IRQS]: All the normal interrupt service routines should
 *                           been registered in this list.
 *
 * fast_irq_list[NR_IRQS]: All the fast interrupt service routines should been
 *                         registered in this list.
 */
static pStruct_IRQHandler  normal_irq_list[NR_IRQS];
static pStruct_IRQHandler  fast_irq_list[NR_IRQS];

static Struct_IRQHandler powmIrqInfo;    /* POWM普通中断信息结构体，用于注册到normal_irq_list列表中 ，add zhulin */

/* P_INTC: The base address of interrupt controller registers */
volatile Struct_INTC *icrp = P_INTC;

static char *exception_names[] = {
    "重启异常",
    "未对齐访问异常",
    "访问错误异常",
    "除零异常",
    "非法指令异常",
    "特权违反异常",
    "跟踪异常",
    "断点异常",
    "不可恢复异常",
    "Idly4异常",
    "普通中断",
    "快速中断",
    "保留异常(HAI)",
    "保留异常(FP)",
    "TLB失配配异常",
    "TLB修改异常",
    "陷阱#0",
    "陷阱#1",
    "陷阱#2",
    "陷阱#3",
    "TLB读无效异常",
    "TLB写无效异常",
    "保留异常22",
    "保留异常23",
    "保留异常24",
    "保留异常25",
    "保留异常26",
    "保留异常27",
    "保留异常28",
    "保留异常29",
    "协处理器异常",
    "系统描述符指针",
};

/*
 * CKCORE_SAVED_CONTEXT -- Saved by a normal interrput or exception
 */
typedef struct{
    UINT32 pc;
    UINT32 psr;
    UINT32 r1;
    UINT32 r2;
    UINT32 r3;
    UINT32 r4;
    UINT32 r5;
    UINT32 r6;
    UINT32 r7;
    UINT32 r8;
    UINT32 r9;
    UINT32 r10;
    UINT32 r11;
    UINT32 r12;
    UINT32 r13;
    UINT32 r14;
    UINT32 r15;
} __attribute__ ((aligned, packed)) Core_SavedRegisters;

extern void hw_vsr_default_exception();
extern void hw_vsr_autovec();
extern void hw_vsr_fastautovec();
extern void hw_vsr_tlbmiss();

/*
 * 中断模块初始化
 * 输入参数：无
 * 输出参数：无
 * 说明：①使用AUTO_VECTOR中断模式，通过auto_vector中断号10响应全部普通中断，在
 *      CK_INTC_InterruptService中断处理函数中查找已注册的中断处理函数并进行
 *      处理。
 *     ②开启电源管理中断功能（由于使用PLL倍频时，模块会进入低功耗，通过电源管理中断
 *      唤醒）
 */
void INTC_Init(void)
{
  int i;

  for(i = 0; i < NR_IRQS; i++){normal_irq_list[i] = NULL;}
  for(i = 0; i < NR_IRQS; i++){fast_irq_list[i] = NULL;}
    
  /* initialize PR0-PR31, big endian */

  icrp->PR[0] = 0x00;
  icrp->PR[1] = 0x00;
  icrp->PR[2] = 0x00;
  icrp->PR[3] = 0x00;
  icrp->PR[4] = 0x00;
  icrp->PR[5] = 0x00;
  icrp->PR[6] = 0x00;
  icrp->PR[7] = 0x00;

  icrp->ICR |= ICR_AVE;

  powmIrqInfo.devname = "POWM";
  powmIrqInfo.irqid = CK_INTC_POWM;
  powmIrqInfo.priority = CK_INTC_POWM;
  powmIrqInfo.handler = (void *)PowerManage_Interrupt;
  powmIrqInfo.bfast = 0;
  powmIrqInfo.next = NULL;
  /* register timer isr */
  INTC_RequestIrq(&powmIrqInfo);
}

/*
 * 使能指定优先级的普通中断
 * 输入参数：dwPriority：优先级0~31
 * 输出参数：无
 * 说明：
 */
void INTC_EnNormalIrq(IN UINT32 dwPriority){
  UINT32 psrbk;

  CPU_EnterCritical(&psrbk);
  icrp->NIER |= (1 << dwPriority);
  CPU_ExitCritical(psrbk);
}
 
/*
 * 禁能指定优先级的普通中断
 * 输入参数：dwPriority：优先级0~31
 * 输出参数：无
 * 说明：
 */
void INTC_DisNormalIrq(IN UINT32 dwPriority){
  UINT32 psrbk;

  CPU_EnterCritical(&psrbk);
  icrp->NIER &= ~(1 << dwPriority);
  CPU_ExitCritical(psrbk);
}

/*
 * 使能指定优先级的快速中断
 * 输入参数：dwPriority：优先级0~31
 * 输出参数：无
 * 说明：
 */
void INTC_EnFastIrq(IN UINT32 dwPriority){
  UINT32 psrbk;

  CPU_EnterCritical(&psrbk);
  icrp->FIER |= (1 << dwPriority);
  CPU_ExitCritical(psrbk);
}

/*
 * 禁能指定优先级的快速中断
 * 输入参数：dwPriority：优先级0~31
 * 输出参数：无
 * 说明：
 */
void INTC_DisFastIrq(IN UINT32 dwPriority){
  UINT32 psrbk;

  CPU_EnterCritical(&psrbk);
  icrp->FIER &= ~(1 << dwPriority);
  CPU_ExitCritical(psrbk);
}

/*
 * 屏蔽指定优先级的普通中断
 * 输入参数：dwPrimask：优先级0~31
 * 输出参数：无
 * 说明：
 */
void INTC_MaskNormalIrq(IN UINT32 dwPrimask){
  UINT16 temp_icr;
  UINT32 psrbk;

  temp_icr = icrp->ICR;
  /*
   * This function will be implemented When fast interrupt masking is disabled,
   * or return at once.
   */
  if((temp_icr &= 0x1000) == 0x1000){
	  return;
  }else{
    CPU_EnterCritical(&psrbk);
    icrp->ICR &= 0xffe0;
    icrp->ICR |= (dwPrimask & 0x0000001f);
    icrp->ICR |= ICR_ME;
    CPU_ExitCritical(psrbk);
  }
}

/*
 * 禁能中断屏蔽
 * 输入参数：无
 * 输出参数：无
 * 说明：
 */
void INTC_UnMaskNormalIrq(void){
  UINT32 psrbk;

  CPU_EnterCritical(&psrbk);
  icrp->ICR &= ~ICR_ME;
  CPU_ExitCritical(psrbk);
}

/*
 * 使能指定优先级的快速中断屏蔽
 * 输入参数：dwPrimask：优先级0~31
 * 输出参数：无
 * 说明：当开启快速中断屏蔽时，所有普通中断请求被屏蔽
 */
void INTC_MaskFastIrq(IN UINT32 dwPrimask){
  UINT32 psrbk;

  CPU_EnterCritical(&psrbk);
  icrp->ICR &= 0xffe0;
  icrp->ICR |= (dwPrimask & 0x0000001f);
  icrp->ICR |= ICR_MFI;
  icrp->ICR |= ICR_ME;
  CPU_ExitCritical(psrbk);
}

/*
 * 禁能快速中断屏蔽
 * 输入参数：无
 * 输出参数：无
 * 说明：当禁能快速中断屏蔽时，普通中断请求由ICR寄存器中的MASK位的值屏蔽
 */
void INTC_UnMaskFastIrq(void){
  UINT32 psrbk;

  CPU_EnterCritical(&psrbk);
  icrp->ICR &= ~ICR_MFI;
  CPU_ExitCritical(psrbk);
}

/*
 * 配置中断参数、注册中断函数，使能中断
 * 输入参数：pirqhandler：待配置的中断的数据结构体指针
 * 输出参数：FAILURE：失败；SUCCESS，成功
 * 说明：
 */
INT32S INTC_RequestIrq(pStruct_IRQHandler pirqhandler){
  UINT32 pr_index;
  UINT32 shift;
  UINT32 psrbk;

  /* Judge the validity of pirqhandler */
  if((NULL == pirqhandler) ||
     (pirqhandler->handler == NULL) ||
     (pirqhandler->priority < 0 || pirqhandler->priority > 31) ||
     ((pirqhandler->irqid < 0) || pirqhandler->irqid > 31)){
	  return FAILURE;
  }
  
  /* Assigns pirqhandler->priority to corresponding interrupt source */
  pr_index = (pirqhandler->irqid) / 4;
  shift = (3-(pirqhandler->irqid) % 4) * 8;

  CPU_EnterCritical(&psrbk);
  icrp->PR[pr_index] &= ~(0x000000ff << shift);
  icrp->PR[pr_index] |= ((pirqhandler->priority) << shift);

  /* If is normal interrupt */
  if(!(pirqhandler->bfast)){

    /* If the list of this priority is empty */
    if(NULL == (normal_irq_list[pirqhandler->priority])){
      normal_irq_list[pirqhandler->priority] = pirqhandler;
      normal_irq_list[pirqhandler->priority]->next = NULL;
      INTC_EnNormalIrq( pirqhandler->priority );
    }
    /* If the list of this priority is not empty */
    else{
      pirqhandler->next = normal_irq_list[pirqhandler->priority];
      normal_irq_list[pirqhandler->priority] = pirqhandler;
    }
  }
  /* If is fast interrupt */
  else{
     /* If the list of this priority is empty */
     if(NULL == (fast_irq_list[pirqhandler->priority])){
       fast_irq_list[pirqhandler->priority] = pirqhandler;
       fast_irq_list[pirqhandler->priority]->next = NULL;
       INTC_EnFastIrq(pirqhandler->priority);
     }
     /* If the list of this priority is not empty */
     else{
       pirqhandler->next = fast_irq_list[pirqhandler->priority];
       fast_irq_list[pirqhandler->priority] = pirqhandler;
     }
  }
  CPU_ExitCritical(psrbk);

  return SUCCESS;
}

/*
 * 释放已被注册的中断
 * 输入参数：pirqhandler：中断的数据结构体指针
 * 输出参数：FAILURE：失败；SUCCESS，成功
 * 说明：
 */
INT32S INTC_FreeIrq(INOUT pStruct_IRQHandler pirqhandler){
  pStruct_IRQHandler  ptemp;
  pStruct_IRQHandler  pre_node = NULL;

  /* Judge the validity of pirqhandler */
  if((pirqhandler == NULL) ||
     (pirqhandler->handler == NULL) ||
     ((pirqhandler->irqid < 0) || (pirqhandler->irqid > 31))){
	  return FAILURE;
  }
  /* If is normal interrupt */
  if(!(pirqhandler->bfast)){
    if (NULL == normal_irq_list[pirqhandler->priority]){
      return FAILURE;
    }
    pre_node = ptemp = normal_irq_list[pirqhandler->priority];
    /* Find the location of pirqhandler in the list */
    while (ptemp != pirqhandler){
      pre_node = ptemp;
      ptemp = ptemp->next;
    }
    /* Delete pirqhandler from the list */
    if (ptemp == normal_irq_list[pirqhandler->priority]){
      normal_irq_list[pirqhandler->priority] = ptemp->next;
    }else if (NULL != ptemp){
      pre_node->next = ptemp->next;
    }else{
      return FAILURE;
    }

    /* If the list which has been deleted becomes empty */
    if(NULL == normal_irq_list[pirqhandler->priority]){
      INTC_DisNormalIrq(pirqhandler->priority);
    }
  }
  /* If is fast interrupt */
  else{
    if(NULL == fast_irq_list[pirqhandler->priority]){
      return FAILURE;
    }
    pre_node = ptemp = fast_irq_list[pirqhandler->priority];

    /* Find the location of pirqhandler in the list */
    while(ptemp != pirqhandler){
      pre_node = ptemp;
      ptemp = ptemp->next;
    }

    /* Delete pirqhandler from the list */
    if(ptemp == fast_irq_list[pirqhandler->priority]){
      fast_irq_list[pirqhandler->priority] = ptemp->next;
    }else if (NULL != ptemp){
      pre_node->next = ptemp->next;
    }else{
      return FAILURE;
    }

    /* If the list which has been deleted becomes empty */
    if(NULL == fast_irq_list[pirqhandler->priority]){
      INTC_DisFastIrq(pirqhandler->priority);
    }
  } /* if (!(pirqhandler->bfast)) */
  ptemp = NULL;

  return SUCCESS;
 }

/*
 * 默认的异常处理函数
 * 输入参数：vector：产生异常的向量号；
 *        regs：存储的寄存器
 * 输出参数：
 * 说明：该函数用entry.S文件中的hw_vsr_default_exception调用，用于处理发生的异常，
 *      这里仅打印异常信息。
 */
void CK_Default_Exception_Handler(int vector, Core_SavedRegisters *regs){
  printf("Exception: %s\n", exception_names[vector]);
}

/*
 * 中断向量表初始化
 * 输入参数：无
 * 输出参数：无
 * 说明：1.中断向量表的初始化必须在开启任何中断之前进行。
 *     2.若使用UCOSII，
 *       需要去掉以下两行程序的屏蔽
 *       ckcpu_vsr_table[CKCORE_VECTOR_AUTOVEC] = (UINT32)os_hw_vsr_autovec;
 *       ckcpu_vsr_table[CKCORE_VECTOR_TRAP0] = (UINT32)OSCtxSw;
 *       同时屏蔽以下程序
 *       ckcpu_vsr_table[CKCORE_VECTOR_AUTOVEC] = (UINT32)hw_vsr_autovec;
 */
void Exception_Init (void){
  int i;

  // set all exception vector table as hw_vsr_default_exception
  for(i = 0; i < CKCORE_VECTOR_SYS; i++){
	  ckcpu_vsr_table[i] =(UINT32)hw_vsr_default_exception;
  }

  // set the vector table for AUTOVEC as hw_vsr_autovec
  ckcpu_vsr_table[CKCORE_VECTOR_AUTOVEC] = (UINT32)hw_vsr_autovec;

  // set the vector table for AUTOVEC as os_hw_vsr_autovec, for the sake of ucosii
  //ckcpu_vsr_table[CKCORE_VECTOR_AUTOVEC] = (UINT32)os_hw_vsr_autovec;
  // set the vector table for FASTAUTOVEC as hw_vsr_fastautovec
  ckcpu_vsr_table[CKCORE_VECTOR_FASTAUTOVEC] =
                    (UINT32)hw_vsr_fastautovec | 0x1;
#if CONFIG_CKCPU_MMU
//  // set the vector table for TLBMISS as hw_vsr_tlbmiss
  ckcpu_vsr_table[CKCORE_VECTOR_TLBMISS] = (UINT32)hw_vsr_tlbmiss;
#endif
  // set the vector table for CKCORE_VECTOR_TRAP0 as OSCtxSw, for the sake of ucosii
  //ckcpu_vsr_table[CKCORE_VECTOR_TRAP0] = (UINT32)OSCtxSw;

  //CK_CPU_EnAllNormalIrq();
  //CK_CPU_EnAllFastIrq();

  __flush_icache();
  __clear_dcache();
}

/*
 * 执行普通中断服务
 * 输入参数：offset:需要执行的中断在normal_irq_list数组中的偏移量
 * 输出参数：无
 * 说明：该函数由entry.S文件中的hw_vsr_autovec调用
 */
void CK_INTC_InterruptService(int offset){
  pStruct_IRQHandler phandler;

  phandler = normal_irq_list[offset];
  while( phandler != NULL){
    /* phandler -> handler must not be NULL */
    phandler -> handler(phandler->irqid);
    phandler = phandler->next; 
  }
}

/*
 * 执行快速中断服务
 * 输入参数：offset:需要执行的中断在fast_irq_list数组中的偏移量
 * 输出参数：无
 * 说明：该函数由entry.S文件中的hw_vsr_fastautovec调用
 */
void CK_INTC_FastInterruptService (int offset){
  pStruct_IRQHandler phandler;

  phandler = fast_irq_list[offset];
  while( phandler != NULL){
    /* phandler -> handler must not be NULL */
    phandler -> handler(phandler->irqid);
    phandler = phandler->next;
  }
}

/*
 * 允许cpu产生中断，包括异常中断和普通中断
 * 输入参数：
 * 输出参数：无
 * 说明：
 */
void CPU_EnAllNormalIrq(void){asm("psrset ee,ie");}

/*
 * 禁止cpu产生普通中断
 * 输入参数：
 * 输出参数：无
 * 说明：
 */
void CPU_DisAllNormalIrq(void){asm("psrclr ie");}

/*
 * 允许cpu产生快速中断
 * 输入参数：
 * 输出参数：无
 * 说明：
 */
void CPU_EnAllFastIrq(void){asm("psrset fe");}

/*
 * 禁止cpu产生快速中断
 * 输入参数：
 * 输出参数：无
 * 说明：
 */
void CPU_DisAllFastIrq(void){asm("psrclr fe");}

/*
 * 关闭cpu中断，并将关闭中断之前的psr寄存器值保存在psr中，待恢复cpu中断时，
 * 将保存的psr值重新写入到psr寄存器。
 * 输入参数：psr：用于存储psr寄存器的值
 * 输出参数：无
 * 说明：在执行一些不能被其他中断打断的关键代码时，需要调用此函数进行保护。
 */
void CPU_EnterCritical(UINT32 *psr){
  asm volatile("mfcr    %0, psr\n\r"
               "psrclr  ie, fe"
                 : "=r" (*psr) );
}

/*
 * 恢复cpu中断，并将关闭中断之前的psr寄存器值重新写入到psr寄存器。恢复到关闭
 * 中断前的状态。
 * 输入参数：psr：保存的关闭cpu中断之前的psr寄存器的值
 * 输出参数：无
 * 说明：退出关键代码时，需要调用此函数来恢复中断。
 */
void CPU_ExitCritical(UINT32 psr){
  asm volatile("mtcr   %0, psr"
                 : 
                 :"r"(psr));
}
