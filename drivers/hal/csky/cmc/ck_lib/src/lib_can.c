/*
 文件: can.c
 版本: CMC693 V1.10
 版权: 浙江杰芯科技有限公司
 作者: zhulin(zhulin2@supcon.com)
 描述：
修改记录：
=========
日期                                      作者                       工作                            内容
*/
#include "lib_can.h"
#include "intc.h"
#include "string.h"

Struct_CANInfo CAN0;                     /* CAN信息定义 */
pStruct_CANInfo pCAN0 = &CAN0;
CANFrameInfo_st CAN0FrameInfo_Rx;
CANFrameInfo_st CAN0FrameInfo_Tx;

Struct_CANInfo CAN1;
pStruct_CANInfo pCAN1 = &CAN1;
CANFrameInfo_st CAN1FrameInfo_Rx;
CANFrameInfo_st CAN1FrameInfo_Tx;

#define PORT_CAN0	1
#define PORT_CAN1   2
#if CAN_CQ_EN
#define CQ_CAN0_BUFSIZE (4 * sizeof(CANFrameInfo_st))    /* CAN0队列 */
static UINT8 CQ_CAN0_Buf[CQ_CAN0_BUFSIZE];
static CycleQueue_t	CQ_CAN0;

#define CQ_CAN1_BUFSIZE (4 * sizeof(CANFrameInfo_st))    /* CAN1队列 */
static UINT8 CQ_CAN1_Buf[CQ_CAN1_BUFSIZE];
static CycleQueue_t	CQ_CAN1;
#endif

extern UINT32 APB_FREQ;

static void CAN0_ISRHandler(UINT32 dwIrqid);
static void CAN1_ISRHandler(UINT32 dwIrqid);
/* CAN中断信息定义 */
Struct_IRQHandler CAN_Intr[2]={
		{"CAN0",CK_INTC_CAN,0,CAN0_ISRHandler,0,0},
		{"CAN1",CK_INTC_CAN1,0,CAN1_ISRHandler,0,0}
};

/*
 * @brief  : CAN控制器的模式配置
 * @param  : pCANInfo, CAN信息结构体指针
 *           mode, CAN的工作模式,可选值为：
 *           CAN_MODE_STANDARD_RESET
 *           CAN_MODE_STANDARD_NORMAL
 *           CAN_MODE_EXTEND_RESET
 *           CAN_MODE_EXTEND_NORMAL
 * @note   : None.
 *
 * @retval : 0， 成功; 其他，失败。
 */
static int CAN_SetMode(pStruct_CANInfo pCANInfo, UINT8 mode)
{
	switch(mode)
	{
	case CAN_MODE_STANDARD_RESET:
		pCANInfo->pCAN->CR |= 0x01;		/* 硬件复位 */
		pCANInfo->pCAN->CDR &= ~0x80;	    /* 标准模式 */
		break;
	case CAN_MODE_STANDARD_NORMAL:
		pCANInfo->pCAN->CR |= 0x01;		/* 硬件复位 */
		pCANInfo->pCAN->CDR &= ~0x80;	    /* 标准模式 */
		pCANInfo->pCAN->CR &= ~0x01;	    /* 复位结束 */
		break;
	case CAN_MODE_EXTEND_RESET:
		pCANInfo->pCAN_Ex->MOD |= 0x09;	/* 硬件复位 */
		pCANInfo->pCAN_Ex->CDR |= 0x80;	/* 扩展模式 */
		break;
	case CAN_MODE_EXTEND_NORMAL:
		pCANInfo->pCAN_Ex->MOD |= 0x09;	/* 硬件复位 */
		pCANInfo->pCAN_Ex->CDR |= 0x80;	/* 扩展模式 */
		pCANInfo->pCAN_Ex->MOD &= ~0x01;   /* 复位结束 */
		break;
	default:
		return -2;
	}

	return 0;
}


/*
 * @brief  : CAN控制器的过滤设置，配置CAN控制器要接收的目的地址
 * @param  : pCANInfo, can信息结构体指针
 *           Acr, 验收代码寄存器。
 *           对于标准模式，最低3位丢弃；对于扩展模式，第二字节最低4位丢弃.
 *           Amr, 验收屏蔽寄存器
 * @note   : None.
 *
 * @retval : 0， 成功; 其他，失败。
 */
static int CAN_SetFilter(pStruct_CANInfo pCANInfo, INT32U Acr, INT32U Amr)
{
	/* 当前模式 */
	CAN_MODE curCANMode = pCANInfo->CANMode;

	/* 模式选择 */
	if( curCANMode == CAN_MODE_STANDARD )
	{
		/* 进入复位模式 */
		CAN_SetMode(pCANInfo, CAN_MODE_STANDARD_RESET);

		/* 写入 */
		pCANInfo->pCAN->ACR = (UINT8)(Acr >> 3);
		pCANInfo->pCAN->AMR = (UINT8)(Amr >> 3);


		/* 恢复模式 */
		CAN_SetMode(pCANInfo, CAN_MODE_STANDARD_NORMAL);
	}
	else
	{
		/* 进入复位模式 */
		CAN_SetMode(pCANInfo, CAN_MODE_EXTEND_RESET);

		/* 然后进行配置 */
		Acr <<= 3;
		Amr <<= 3;

		pCANInfo->pCAN_Ex->ACR0 = (Acr >> 24) & 0xFF;
		pCANInfo->pCAN_Ex->ACR1 = (Acr >> 16) & 0xFF;
		pCANInfo->pCAN_Ex->ACR2 = (Acr >> 8 ) & 0xFF;
		pCANInfo->pCAN_Ex->ACR3 = (Acr >> 0 ) & 0xFF;
		pCANInfo->pCAN_Ex->AMR0 = (Amr >> 24) & 0xFF;
		pCANInfo->pCAN_Ex->AMR1 = (Amr >> 16) & 0xFF;
		pCANInfo->pCAN_Ex->AMR2 = (Amr >> 8 ) & 0xFF;
		pCANInfo->pCAN_Ex->AMR3 = (Amr >> 0 ) & 0xFF;

		/* 恢复模式 */
		CAN_SetMode(pCANInfo, CAN_MODE_EXTEND_NORMAL);
	}

	return 0;
}


/*
 * @brief  : 适配波特率定时寄存器各参数
 * @param  : pCANInfo, can信息结构体指针
 *           APB_CLK APB时钟
 *           Dest_BaudRate，目标波特率
 * @note   : None.
 *
 * @retval : 0， 成功; 其他，失败。
 */
static int CAN_AdaptBTR( pStruct_CANInfo pCANInfo,
					 	 UINT32 APB_CLK,
					 	 UINT32 Dest_BaudRate)
{
	if(pCANInfo == NULL)
	{
		return -1001;
	}

	/* APB时钟频率不大于100MHz */
	if(APB_CLK > 100000000)
	{
		return -2;
	}

	/* 波特率不能大于上限值 */
	if( (Dest_BaudRate > CAN_BUADRATE_MAX) ||
		(Dest_BaudRate < CAN_BUADRATE_MIN)  )
	{
		return -3;
	}

    /* 先不考虑BTR0中的SJW和BTR1中的SAM，BTR1取8-25. */
    UINT16 BTR0 = 0, BTR1 = 8;
	UINT16 SJW = 0;

	APB_CLK /= 2;

	//波特率计算方法
	//baudrate = APBCLK / ( 2 * (BRP + 1) * (TSEG1 + 1 + TSEG2 + 1 + 1) );
	//将BRP和TESG重新组合后，可以简化为
	//baudrate = APBCLK / (2 * BRP * TSEG)

    UINT32 	Current_DiffCLK = 0;	    /* 当前组合的误差 */
    UINT32 	Current_CLK = 0;		    /* 按照当前组合计算出的APBCLK */
    UINT32 	MinDiffCLKValue = APB_CLK;	/* 记录最小差值 */
    UINT16  Optimization_BTR0 = 0;
    UINT16  Optimization_BTR1 = 0;	    /* 最优参数 */
    UINT8 	TSEG1 = 0, TSEG2 = 0;

    /* BTR0 和BTR1进行组合，得到最优值 */
    for( ; BTR1 <= 25; BTR1++)
    {
    	BTR0 = (UINT32)((APB_CLK + ((Dest_BaudRate * BTR1) >> 1)) / (Dest_BaudRate * BTR1));
        BTR0 = (BTR0 < 1) ? 1 : BTR0;
        BTR0 = (BTR0 > 256) ? 256 : BTR0;/* BTR0范围 1-256 */
    	Current_CLK = BTR0 * BTR1 * Dest_BaudRate;
    	Current_DiffCLK = (APB_CLK > Current_CLK) ?
    					  (APB_CLK - Current_CLK) :
    					  (Current_CLK - APB_CLK) ;/* 得到当前组合下CLK的差值 */
    	/* 取误差最小的值 */
    	if(Current_DiffCLK <= MinDiffCLKValue)
    	{
    		MinDiffCLKValue = Current_DiffCLK;
    		Optimization_BTR0 = BTR0;
    		Optimization_BTR1 = BTR1;
    	}

    	if(MinDiffCLKValue == 0)
    	{
    		break;
    	}

    }/* end for BTR1 */

    /* 此时Optimization_BTR1包含 TSEG1 + TSEG2 + 3 */
    Optimization_BTR1 -= 3;

    TSEG2 = Optimization_BTR1 / 2;
    TSEG2 = (TSEG2 > 7) ? 7 : TSEG2;
    TSEG1 = Optimization_BTR1 - TSEG2;

    SJW = Optimization_BTR1 / 6;/* SJW 0-3 */

    /* TSEG1 >= TSEG2 >= 2 * SJW */
    if(SJW > (TSEG2 >> 1) )
    {
    	SJW = (TSEG2 >> 1);
    }

    if(Optimization_BTR0 != 0)
    {
    	Optimization_BTR0 -= 1;
    }
    Optimization_BTR0 |= ( SJW << 8);/* SJW 1-3 */

    Optimization_BTR1 = 0;
    if(Dest_BaudRate <= 100000)      /* 低中速时使用 */
    {
    	Optimization_BTR1 = 0x80;    /* SAM = 1; */
    }
    Optimization_BTR1 |= (TSEG2 << 4) | (TSEG1);/* 最优组合 */

    CAN_MODE curCANMode = pCANInfo->CANMode;
    if(curCANMode == CAN_MODE_STANDARD)
    {
    	CAN_SetMode(pCANInfo, CAN_MODE_STANDARD_RESET);
    	pCANInfo->pCAN->BTR0 = Optimization_BTR0;
    	pCANInfo->pCAN->BTR1 = Optimization_BTR1;
    }
    else
    {
    	CAN_SetMode(pCANInfo, CAN_MODE_EXTEND_RESET);
    	pCANInfo->pCAN_Ex->BTR0 = Optimization_BTR0;
    	pCANInfo->pCAN_Ex->BTR1 = Optimization_BTR1;
    }

	#if BTR_WATCH_EN
    pCANInfo->struct_BTR.BTR0 = Optimization_BTR0;
    pCANInfo->struct_BTR.BTR1 = Optimization_BTR1;
    pCANInfo->struct_BTR.BuadRate = Dest_BaudRate;
    pCANInfo->struct_BTR.ClkError = MinDiffCLKValue;
	#endif

    return 0;
}


/*
 * @brief  : CAN控制器的波特率设置
 * @param  : pCANInfo, can信息结构体指针
 *           pBuadRate, 波特率相关参数结构体
 * @note   : None.
 *
 * @retval : 0， 成功; 其他，失败。
 */
static int CAN_SetBuadrate(pStruct_CANInfo pCANInfo, UINT32 buadrate)
{
	int ok = 0;
	/* 波特率需要更新 */
	if(pCANInfo->BuadRate != buadrate)
	{
		ok = CAN_AdaptBTR(pCANInfo, APB_FREQ, buadrate);//写入BTR0 BTR1的适配值
		if(ok < 0)
		{
			return -1;
		}

		pCANInfo->BuadRate = buadrate;//更新波特率
	}

	return 0;
}

/*
 * @brief  : 得到当前CAN的状态
 * @param  : pCANInfo, can信息结构体指针
 * @note   : None.
 *
 * @retval : SR寄存器的值。
 */
UINT8 CAN_GetStatus(pStruct_CANInfo pCANInfo)
{
	if(pCANInfo == NULL)
	{
		return 0xFF;//错误值
	}

	if(pCANInfo->CANMode == CAN_MODE_STANDARD)
	{
		return (pCANInfo->pCAN->SR);
	}
	else
	{
		return (pCANInfo->pCAN_Ex->SR);
	}
}


/*
 * @brief  : CAN控制器的缓冲区释放
 * @param  : pCANInfo, can信息结构体指针
 * @note   : 只释放接收缓冲区, 不能在数据接收操作前进行此步骤.
 *
 * @retval : 0， 成功; 其他，失败。
 */
int CAN_ReleaseFIFO(pStruct_CANInfo pCANInfo)
{
	CAN_MODE curCANMode = pCANInfo->CANMode;
	volatile UINT8 status = 0;

	if(pCANInfo == NULL)
	{
		return -1001;
	}

	/* 确保当前已经结束[正在接收报文]的状态 */
	do
	{
		status = CAN_GetStatus(pCANInfo) & 0x10;
	}
	while(status != 0x00);

	if( curCANMode == CAN_MODE_STANDARD)
	{
		CAN_SetMode(pCANInfo, CAN_MODE_STANDARD_RESET);
		pCANInfo->pCAN->CMR = 0x0C;             /* 释放接收缓冲器，并清除数据溢出 */
		CAN_SetMode(pCANInfo, CAN_MODE_STANDARD_NORMAL);
	}
	else
	{
		CAN_SetMode(pCANInfo, CAN_MODE_EXTEND_RESET);
		pCANInfo->pCAN_Ex->CMR = 0x0C;
		CAN_SetMode(pCANInfo, CAN_MODE_EXTEND_NORMAL);
	}

	return 0;
}


/*
 * 配置can中断
 * 输入参数：pCANInfo： can信息结构体；
 *        cIntr 中断源
 * 输出参数：返回 0， 成功; 其他，失败
 */
int CAN_ITConfig(pStruct_CANInfo pCANInfo, UINT8 cIntr)
{
	/* 标准模式下 */
	UINT8 temp;
	CAN_MODE curCANMode = pCANInfo->CANMode;

	if(curCANMode == CAN_MODE_STANDARD)
	{
		temp = pCANInfo->pCAN->IR;          /* 先清除已有中断 */
		if(cIntr == CAN_IT_NONE)                     /* 没有中断，直接退出 */
		{
			pCANInfo->pCAN->CR = 0x00;
			return 0;
		}

		temp = 0x00;
		if(cIntr & CAN_IT_RIE)                       /* 接收终端使能 */
		{
			temp |= CAN_STD_IT_RIE;	                /* RIE */
		}

		if(cIntr & CAN_IT_TIE)
		{
			temp |= CAN_STD_IT_TIE;      	        /* TIE */
		}

		if(cIntr & CAN_IT_EIE)
		{
			temp |= CAN_STD_IT_EIE; 	            /* EIE */
		}

		if(cIntr & CAN_IT_OIE)
		{
			temp |= CAN_STD_IT_OIE;	                /* OIE */
		}

		pCANInfo->pCAN->CR = temp;
	}
	else/* 扩展模式下 */
	{
		temp = pCANInfo->pCAN_Ex->IR;       /* 先清除已有中断 */

		if(cIntr == CAN_IT_NONE)                     /* 没有中断，直接退出 */
		{
			pCANInfo->pCAN_Ex->IER = 0x00;
			return 0;
		}

		temp = 0x00;
		if(cIntr & CAN_IT_RIE)                       /* 接收中断使能 */
		{
			temp |= CAN_EX_IT_RIE;	                /* RIE */
		}

		if(cIntr & CAN_IT_TIE)
		{
			temp |= CAN_EX_IT_TIE;	                /* TIE */
		}

		if(cIntr & CAN_IT_EIE)
		{
			temp |= CAN_EX_IT_EIE; 	                /* EIE */
		}

		if(cIntr & CAN_IT_OIE)
		{
			temp |= CAN_EX_IT_DOIE;	                /* OIE */
		}

		if(cIntr & CAN_IT_EPIE)
		{
			temp |= CAN_EX_IT_EPIE; 	            /* EPIE */
		}

		if(cIntr & CAN_IT_ALIE)
		{
			temp |= CAN_EX_IT_ALIE;	                /* ALIE */
		}

		if(cIntr & CAN_IT_BEIE)
		{
			temp |= CAN_EX_IT_BEIE;	                /* BEIE */
		}

		pCANInfo->pCAN_Ex->IER = temp;
	}

	return 0;
}


/*
 * @brief  : CAN模块获取中断向量
 * @param  : pCANInfo, can信息结构体指针
 * @note   : 返回的中断向量与具体的模式无关
 *
 * @retval : 中断向量值,如CAN_IT_RIE。
 */
int CAN_GetITVector(pStruct_CANInfo pCANInfo, UINT8 *Vector)
{
	UINT8 ITVector = 0x00, temp = 0x00;
	CAN_MODE curCANMode = pCANInfo->CANMode;

	if( curCANMode == CAN_MODE_STANDARD )
	{
		ITVector = pCANInfo->pCAN->IR;//读取中断状态

		if(ITVector & CAN_STD_IT_DOI)//
		{
			temp |= CAN_IT_OI;	//OI
		}

		if(ITVector & CAN_STD_IT_EI)
		{
			temp |= CAN_IT_EI;	//EI
		}

		if(ITVector & CAN_STD_IT_TI)
		{
			temp |= CAN_IT_TI; 	//TI
		}

		if(ITVector & CAN_STD_IT_RI)
		{
			temp |= CAN_IT_RI;	//RI
		}

	}
	else
	{
		ITVector = pCANInfo->pCAN_Ex->IR;//读取中断状态

		if(ITVector & CAN_EX_IT_BEI)
		{
			temp |= CAN_IT_BEI;	//BEI
		}

		if(ITVector & CAN_EX_IT_ALI)
		{
			temp |= CAN_IT_ALI;	//ALI
		}

		if(ITVector & CAN_EX_IT_EPI)
		{
			temp |= CAN_IT_EPI;	//ALI
		}

		if(ITVector & CAN_EX_IT_DOI)
		{
			temp |= CAN_IT_OI;	//OI
		}

		if(ITVector & CAN_EX_IT_EI)
		{
			temp |= CAN_IT_EI;	//EI
		}

		if(ITVector & CAN_EX_IT_TI)
		{
			temp |= CAN_IT_TI;	//TI
		}

		if(ITVector & CAN_EX_IT_RI)
		{
			temp |= CAN_IT_RI;	//RI
		}

	}

	*Vector = temp;
	return 0;
}

/*
 * @brief  : 判断CAN控制器当前是否空闲
 * @param  : pCANInfo, can信息结构体指针
 * @note   :
 *
 * @retval : TRUE 空闲， FALSE 忙
 */
int CAN_IsIdle(pStruct_CANInfo pCANInfo, BOOL *flag)
{
	UINT8 Status;
	if( (pCANInfo == NULL) || (flag == NULL) )
	{
		return -1001;
	}

	Status = CAN_GetStatus(pCANInfo);

	if( (Status & CAN_SR_TSBUSY) || (Status & CAN_SR_RSBUSY) )
	{
		*flag = FALSE;
	}
	else
	{
		*flag = TRUE;
	}

	return 0;
}

/*
 * @brief  : CAN控制器接收的数据
 * @param  : pCANInfo, can信息结构体指针
 *           pCanFrameInfo_st CAN帧数据结构
 * @note   : 只释放接收缓冲区
 *
 * @retval : 0， 成功; 其他，失败。
 */
int CAN_ReceiveFrame(pStruct_CANInfo pCANInfo,
				pCANFrameInfo_st pCanFrameInfo_st)
{
	UINT32 *RXBDataPtr = NULL;                             /* 指向数据寄存器起始地址 */
	UINT8 i = 0;

	if((pCANInfo == NULL ) ||
	   (pCanFrameInfo_st == NULL))
	{
		return -1001;                                      /* 参数错误 */
	}

	/* 只有在工作模式下才能正确读取RXB */
	if(pCANInfo->CANMode == CAN_MODE_STANDARD)             /* 标准模式 */
	{
		UINT8 rxb0 = 0, rxb1 = 0;

		RXBDataPtr = (UINT32 *)(&(pCANInfo->pCAN->RXB2));
		pCanFrameInfo_st->FF = 0;                          /* 无效 */
		rxb0 = pCANInfo->pCAN->RXB0;
		rxb1 = pCANInfo->pCAN->RXB1;

		pCanFrameInfo_st->DLC = rxb1 & 0x0F; 		       /* 数据长度 */
		pCanFrameInfo_st->RTR = (rxb1 & 0x10) >> 4;	       /* 远程帧标志 */
		pCanFrameInfo_st->ID  = (UINT32)(rxb0 << 3) | (rxb1 >> 5);

	}
	else/* 扩展模式 */
	{
		UINT8 rxb = 0;

		rxb = pCANInfo->pCAN_Ex->RXB0;

		pCanFrameInfo_st->DLC = rxb & 0x0F; 		                       /* 数据长度 */
		pCanFrameInfo_st->RTR = (rxb & 0x40) >> 6;	                       /* 远程帧标志 */
		pCanFrameInfo_st->FF =  (rxb & 0x80) >> 7;	                       /* 扩展帧格式识别位 */

		if(pCanFrameInfo_st->FF == 1)                                      /* 当前帧为扩展帧格式*/
		{
			rxb = pCANInfo->pCAN_Ex->RXB1;                        /* 高8位ID */
			pCanFrameInfo_st->ID = (UINT32)rxb << 8;

			rxb = pCANInfo->pCAN_Ex->RXB2;                        /* ID 20-13 */
			pCanFrameInfo_st->ID |= rxb;
			pCanFrameInfo_st->ID <<= 8;

			rxb = pCANInfo->pCAN_Ex->RXB3;                        /* ID 12-5 */
			pCanFrameInfo_st->ID |= rxb;
			pCanFrameInfo_st->ID <<= 5;

			rxb = pCANInfo->pCAN_Ex->RXB4;                        /* ID 5-0 */
			rxb >>= 3;
			pCanFrameInfo_st->ID |= rxb;

			RXBDataPtr = (UINT32 *)(&(pCANInfo->pCAN_Ex->RXB5));  /* RXB起始地址 */
		}
		else /* 标准格式 */
		{
			rxb = pCANInfo->pCAN_Ex->RXB1;                        /* 高8位ID */
			pCanFrameInfo_st->ID = rxb;
			pCanFrameInfo_st->ID <<= 8;

			rxb = pCANInfo->pCAN_Ex->RXB2 & 0xE0;
			pCanFrameInfo_st->ID |= rxb;
			pCanFrameInfo_st->ID <<= 13;                                   /* ID28-ID18 得到高11位的ID */

			RXBDataPtr = (UINT32 *)(&(pCANInfo->pCAN_Ex->RXB3));  /* RXB起始地址 */
		}
	}

	/*如果数据长度 超过8，则出错. */
	if(pCanFrameInfo_st->DLC > 8)
	{
		pCanFrameInfo_st->DLC = 0;
	}

	/* 非远程帧，读取数据 */
	if(pCanFrameInfo_st->RTR != 1)
	{
		for(i = 0; i < pCanFrameInfo_st->DLC; i++)
		{
			pCanFrameInfo_st->Data[i] = (UINT8)(*RXBDataPtr);
			RXBDataPtr++;
		}
	}

	/* 清除当前FIFO,由于pCAN和pCAN_Ex为同一联合体内的成员，且pCAN->CMR和pCAN_Ex->CMR
	    地址偏移相同，这里代码仅对pCAN->CMR操作，其结果也相当于对pCAN_Ex->CMR*/
	pCANInfo->pCAN->CMR = 0x04;                                   /* 【工作模式下】读取当前FIFO后，报文数目自动更新 */

	return 0;
}

/*
 * @brief  : CAN模块数据发送.如果一个发送请求尚未被处理则将被取消发
 * @param  : pCANInfo, can信息结构体指针
 *           pCanFrameInfo_st CAN帧数据结构
 * @note   : None.
 *
 * @retval : 0， 成功; 其他，失败。
 */
int CAN_Transmit(pStruct_CANInfo pCANInfo,
				 pCANFrameInfo_st pCanFrameInfo_st)
{
	UINT32 *TXBDataPtr = NULL;                                    /* 指向数据寄存器起始地址 */
	UINT8 i = 0;
	UINT8 txb0 = 0, txb1 = 0;
	UINT32 ID = 0;

	if((pCANInfo == NULL) ||
	   (pCanFrameInfo_st == NULL) ||
	   (pCanFrameInfo_st->DLC > 8))
	{
		return -1001;                                             /* 参数错误 */
	}

	do
	{
		i = CAN_GetStatus(pCANInfo);
		/* 如果错误已经在报警限以上，直接退出。 */
		if(i & 0x40)
		{
			return -1002;
		}

	}
	while((i & 0x24) != 0x04);                                    /* 等待写入完成，发送就绪. */

	/* 只能在工作模式下完成发送操作 */
	if(pCANInfo->CANMode == CAN_MODE_STANDARD)                    /* 标准模式 */
	{
		TXBDataPtr = (UINT32 *)(&(pCANInfo->pCAN->TXB2));/* 发送起始地址 */

		txb0 = pCanFrameInfo_st->ID >> 3;                         /* 高八位ID */
		txb1 = pCanFrameInfo_st->ID & 0x07;                       /* 低三位ID */

		txb1 <<= 1;
		txb1 |= pCanFrameInfo_st->RTR;                            /* 远程帧标志 */

		txb1 <<= 4;
		txb1 |= pCanFrameInfo_st->DLC;                            /* 数据长度 */

		/*写入前两个参数 */
		pCANInfo->pCAN->TXB0 = txb0;
		pCANInfo->pCAN->TXB1 = txb1;
	}
	else/* 扩展模式 */
	{
		txb0 = pCanFrameInfo_st->FF;                              /* 扩展帧格式识别位 */

		txb0 <<= 1;
		txb0 |= pCanFrameInfo_st->RTR;                            /* 远程 */

		txb0 <<= 6;
		txb0 |= pCanFrameInfo_st->DLC;                            /* 数据长度 */


		/* 写入前两个参数 */
		pCANInfo->pCAN_Ex->TXB0 = txb0;

		ID = pCanFrameInfo_st->ID << 3;                           /* 29+3 == 32b */
		if(pCanFrameInfo_st->FF == 1)                             /* 扩展帧格式 */
		{
			pCANInfo->pCAN_Ex->TXB1 = (ID >> 24) & 0xFF;
			pCANInfo->pCAN_Ex->TXB2 = (ID >> 16) & 0xFF;
			pCANInfo->pCAN_Ex->TXB3 = (ID >>  8) & 0xFF;
			pCANInfo->pCAN_Ex->TXB4 = (ID >>  0) & 0xFF;
			TXBDataPtr = (UINT32 *)(&(pCANInfo->pCAN_Ex->TXB5));/* 发送起始地址 */

		}
		else/* 标准帧格式 */
		{
			pCANInfo->pCAN_Ex->TXB1 = (ID >> 24) & 0xFF;
			pCANInfo->pCAN_Ex->TXB2 = (ID >> 16) & 0xFF;
			TXBDataPtr = (UINT32 *)(&(pCANInfo->pCAN_Ex->TXB3));/* 发送起始地址 */
		}
	}

	/*非远程帧，写入数据 */
	if(pCanFrameInfo_st->RTR != 1)
	{
		for(i = 0; i < pCanFrameInfo_st->DLC; i++)
		{
			*TXBDataPtr = (UINT32)pCanFrameInfo_st->Data[i];
			TXBDataPtr++;
		}
	}

	pCANInfo->pCAN->CMR = 0x01;                               /* 发送数据 */

	return 0;
}

/* 使能CAN的报文存储 */
#if CAN_CQ_EN

/*
 * @brief  : CAN模块用于存储报文的循环队列复位
 * @param  : pCANInfo, can信息结构体指针
 * @note   : None.
 *
 * @retval : 0， 成功; 其他，失败。
 */
int CAN_CQ_Reset(pStruct_CANInfo pCANInfo)
{
	if((pCANInfo == NULL) ||
	   (pCANInfo->pCANCQ == NULL) ||
	   (pCANInfo->pCANCQ->This != pCANInfo->pCANCQ))
	{
		return -1001;
	}

	CQ_Reset(pCANInfo->pCANCQ);
	return 0;
}

/*
 * @brief  : CAN模块用于存储报文的循环队列初始化
 * @param  : pCANInfo, can信息结构体指针
 *           pCQ   循环队列指针
 *           pBuf  循环队列的数据部分
 *           CQSize循环队列长度
 * @note   : None.
 *
 * @retval : 0， 成功; 其他，失败。
 */
int CAN_CQ_Init(pStruct_CANInfo pCANInfo,
				 CycleQueue_t *pCQ,
				 UINT8 *pBuf,
				 UINT16 CQSize)
{
	if((pCANInfo == NULL) || (pCQ == NULL))//其余参数在初始化函数中判断
	{
		return -1001;
	}

	CQ_Init(pCQ, pBuf, CQSize);

	pCANInfo->pCANCQ = pCQ;

	return 0;
}

/*
 * @brief  : CAN模块入栈一个CAN报文
 * @param  : pCANInfo, can信息结构体指针
 *           pCanFrameInfo_st   CAN报文结构体指针
 * @note   : None.
 *
 * @retval : 实际写入的长度（按字节计数）
 */
UINT16 CAN_CQ_PushFrame(pStruct_CANInfo pCANInfo,
					    pCANFrameInfo_st pCanFrameInfo_st)
{
	UINT16 Len = 0;

	if(pCANInfo == NULL)//其余参数在初始化函数中判断
	{
		return -1001;
	}

	//如果本次已经不足以存放下一帧，退出。
	Len = CQ_GetEmptySize(pCANInfo->pCANCQ);

	if(Len < sizeof(CANFrameInfo_st) )
	{
		return 0;
	}

	return CQ_Write(pCANInfo->pCANCQ, (UINT8 *)pCanFrameInfo_st, sizeof(CANFrameInfo_st) );

}

/*
 * @brief  : CAN模块入栈一个CAN报文
 * @param  : pCANInfo, can信息结构体指针
 *           pCanFrameInfo_st   CAN报文结构体指针
 * @note   : None.
 *
 * @retval : 实际读取的长度（按字节计数）
 */
UINT16 CAN_CQ_PopFrame(pStruct_CANInfo pCANInfo,
					   pCANFrameInfo_st pCanFrameInfo_st)
{
	UINT16 Len = 0;
	if((pCANInfo == NULL) || (pCanFrameInfo_st == NULL))//其余参数在初始化函数中判断
	{
		return -1001;
	}

	Len = CQ_Read(pCANInfo->pCANCQ, (UINT8 *)pCanFrameInfo_st, sizeof(CANFrameInfo_st) );

	//如果实际读取长度小于报文长度，表明帧被破坏.报文结构体指针指向的数据长度设置为0。
	if(Len < sizeof(CANFrameInfo_st) )
	{
		pCanFrameInfo_st->DLC = 0;
	}

	return Len;
}
#endif
//使能CAN的报文存储

/*
 * 根据初始化配置参数配置can
 * 输入参数：
 * 输出参数：
 */
void CAN_Config(pStruct_CANInit pCANInit)
{
	INT32S ok = 0;
	pStruct_CANInfo pInfo;
	pStruct_IRQHandler pCANIntr;
#if CAN_CQ_EN
	CycleQueue_t *pCQCan;
	UINT8 *pCQ_CAN_Buf;
	UINT16 wCQ_CAN0_BUFSIZE;

#endif
	if(pCANInit->pCAN == (pStruct_CAN)CAN0_BASE_ADDR)             /* CAN0 */
	{
		pInfo = pCAN0;
		pCANIntr = &CAN_Intr[0];
#if CAN_CQ_EN
		pCQCan = &CQ_CAN0;
		pCQ_CAN_Buf = CQ_CAN0_Buf;
		wCQ_CAN0_BUFSIZE = CQ_CAN0_BUFSIZE;
#endif
	}
	else if(pCANInit->pCAN == (pStruct_CAN)CAN1_BASE_ADDR)       /* CAN1 */
	{
		pInfo = pCAN1;
		pCANIntr = &CAN_Intr[1];
#if CAN_CQ_EN
		pCQCan = &CQ_CAN1;
		pCQ_CAN_Buf = CQ_CAN1_Buf;
		wCQ_CAN0_BUFSIZE = CQ_CAN1_BUFSIZE;
#endif
	}
	else
	{
		return ;
	}

	pInfo->CAN_Initstruct.buadrate = pCANInit->buadrate;          /* 保存初始化信息 */
    pInfo->CAN_Initstruct.canacr = pCANInit->canacr;
    pInfo->CAN_Initstruct.canamr = pCANInit->canamr;
    pInfo->CAN_Initstruct.canmode = pCANInit->canmode;
    pInfo->CAN_Initstruct.intrVector = pCANInit->intrVector;
    pInfo->CAN_Initstruct.priority = pCANInit->priority;
    pInfo->CAN_Initstruct.CAN_ISR = pCANInit->CAN_ISR;
    pInfo->CAN_Initstruct.pCAN = pCANInit->pCAN;

	pInfo->CANMode = pCANInit->canmode;                            /* can工作模式 */
	pInfo->pCAN = pCANInit->pCAN;

	CAN_ITConfig(pInfo, CAN_IT_NONE);                              /* 当前模式下关闭所有中断使能 */

	/* 设定模式 */
	if(pCANInit->canmode == CAN_MODE_STANDARD)                     /* 标准模式 */
	{
		ok = CAN_SetMode(pInfo, CAN_MODE_STANDARD_RESET);
	}
	else                                                           /* 扩展模式 */
	{
		ok = CAN_SetMode(pInfo, CAN_MODE_EXTEND_RESET);
	}

	if(ok < 0)                                                     /* 针对参数错误的情况 */
	{
		return ;
	}

	/* 波特率设置 */
	ok = CAN_SetBuadrate(pInfo, pCANInit->buadrate);

	if(ok < 0)                                                     /* 针对参数错误的情况 */
	{
		return ;
	}

	/* 验收滤波设置 */
	CAN_SetFilter(pInfo, pCANInit->canacr, pCANInit->canamr);

	/* 中断配置 */
	ok = CAN_ITConfig(pInfo, pCANInit->intrVector);

	if(ok < 0)                                                     /* 针对参数错误的情况 */
	{
		return ;
	}

	CAN_ReleaseFIFO(pInfo);                                        /* 先清除FIFO */

	/* 如果使能了CAN的循环队列存储，则初始化时复位循环队列 */
#if CAN_CQ_EN
	CAN_CQ_Reset(pInfo);                                           /* 复位循环队列 */
	CAN_CQ_Init(pInfo, pCQCan, pCQ_CAN_Buf, wCQ_CAN0_BUFSIZE);     /* 初始化循环队列 */
#endif
	if(pCANInit->CAN_ISR != NULL)                                  /* 若用户自定义了中断处理函数，则使用用户定义的中断函数，否则使用默认的 */
	{
		pCANIntr->handler = pCANInit->CAN_ISR;
	}
	pCANIntr->priority = pCANInit->priority;
	INTC_FreeIrq(pCANIntr);
	INTC_RequestIrq(pCANIntr);
}

/* CAN0中断服务程序 */
static void CAN0_ISRHandler(UINT32 dwIrqid)
{
	UINT8 ITVector = 0;
	BOOL  PacketAvailable = FALSE;

	CAN_GetITVector(pCAN0, &ITVector);                        /* 获取中断向量 */

	if(ITVector & CAN_IT_TI)                                  /* 发送中断 */
	{

	}

	if(ITVector & CAN_IT_RI)                                  /* 接收中断 */
	{
		do
		{
		    CAN_ReceiveFrame(pCAN0, &CAN0FrameInfo_Rx);

			/* 如果使能了循环队列保存报文，就入栈到循环队列 */
			#if CAN_CQ_EN

			CAN_CQ_PushFrame(pCAN0, &CAN0FrameInfo_Rx);

			#endif

			/* 判断是否还有数据在缓存中 */
			if(pCAN0->CANMode == CAN_MODE_STANDARD)
			{
				PacketAvailable = (pCAN0->pCAN->SR & 0x01) ? TRUE : FALSE;
			}
			else
			{
				PacketAvailable = (pCAN0->pCAN_Ex->RMC) ? TRUE : FALSE;
			}
		}
		while(PacketAvailable);

	}

	if(ITVector & CAN_IT_OI)                                    /* 溢出中断 */
	{
		CAN_ReleaseFIFO(pCAN0);
	}
}

/* CAN1中断服务程序 */
static void CAN1_ISRHandler(UINT32 dwIrqid)
{
	UINT8 ITVector = 0;
	BOOL  PacketAvailable = FALSE;

	CAN_GetITVector(pCAN1, &ITVector);                        /* 获取中断向量 */

	if(ITVector & CAN_IT_TI)                                  /* 发送中断 */
	{

	}

	if(ITVector & CAN_IT_RI)                                  /* 接收中断 */
	{
		do
		{
		    CAN_ReceiveFrame(pCAN1, &CAN1FrameInfo_Rx);

			/* 如果使能了循环队列保存报文，就入栈到循环队列 */
			#if CAN_CQ_EN

			CAN_CQ_PushFrame(pCAN1, &CAN1FrameInfo_Rx);

			#endif

			/* 判断是否还有数据在缓存中 */
			if(pCAN1->CANMode == CAN_MODE_STANDARD)
			{
				PacketAvailable = (pCAN1->pCAN->SR & 0x01) ? TRUE : FALSE;
			}
			else
			{
				PacketAvailable = (pCAN1->pCAN_Ex->RMC) ? TRUE : FALSE;
			}
		}
		while(PacketAvailable);

	}

	if(ITVector & CAN_IT_OI)                                    /* 溢出中断 */
	{
		CAN_ReleaseFIFO(pCAN1);
	}
}

/*
 * 函数名称:CAN_GetInitStructure
 * 函数输入:pStruct_CANInit pInit_to 把旧初始化参数的赋值对象, port can对应口
 * 函数说明:把旧初始化参数赋值给传入结构体
 */
BOOL CAN_GetInitStructure(pStruct_CANInit pInit_to, UINT8 port)
{
	pStruct_CANInit pInit_from;

	if (port == PORT_CAN0)
	{
		pInit_from =  &pCAN0->CAN_Initstruct;
	}
	else if(port == PORT_CAN1)
	{
		pInit_from =  &pCAN1->CAN_Initstruct;
	}
	else
	{
		return FALSE;
	}
	memcpy(pInit_to, pInit_from, sizeof(Struct_CANInit));
	return TRUE;
}
