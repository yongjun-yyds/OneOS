/*
 文件: pipo.c
 版本: CMC693 V1.1
 版权: 浙江杰芯科技有限公司
 作者: 喻文星 (yuwenxing@supcon.com)
 描述：PIPO有以下硬件信息：
 	 1.PIPO共有6个分别是PIPO0~PIPO6,每个PIPO有A、B两个端口:
 	 PIPO0:端口A：GPIO1_B8   端口B：GPIO1_B9   PIPO1：端口A：GPIO1_B10   端口B：GPIO1_B11
 	 PIPO2:端口A：GPIO1_B12   端口B：GPIO1_B13 PIPO3：端口A：GPIO1_B14   端口B：GPIO1_B15
 	 PIPO4:端口A：GPIO1_A10   端口B：GPIO1_A11 PIPO5：端口A：GPIO1_A12   端口B：GPIO1_A13
 	 2.PIPO有7种工作模式：定时模式、PWM工作模式、脉冲宽度测量模式、脉冲个数测量模式单脉冲模式、保留（暂时未使用）、正交编码器接口模式
 	 3.A、B两端口都可以输出PWM波，两路PWM波的频率是相同的，占空比可以不同
 	 4.目前底层PWM停止后的电平状态时随机的，所以关闭PWM波要么波特率配置为0，要么关闭管脚复用
 	 5.PWM无法输出占空比为0的波形，当PWM输出占空比为0时，会在每个周期有1时钟周期的脉冲产生
 	 6.PWM波配置成死区PWM波，B端口的PWM波是由A端口PWM波翻转得到，并且死区PWM波的前、后死区时间不能为0，否则不输出死区PWM波
 	 7.脉冲宽度测量有7中模式：高电平宽度测量模式，高电平时间测量模式，低电平宽度测量模式，低电平时间测量模式，全电平测量模式，单个脉冲高电平宽度测量模式，
 	       单个脉冲低电平宽度测量模式；其中只有全电平测量模式，没有中断，此模式会将每次测到高低电平时间存放到相应的寄存器，需要停止工作才会结束测量

修改记录：
=========
日期                                      作者                       工作                            内容
-------------------------------------------------------
2018-11-15       喻文星                      创建                         PIPO驱动程序
2018-12-10       喻文星                      修改                PIPO的PWM模式输出的最高频率调整为1000000HZ
*/
#include "pipo.h"
#include "intc.h"

extern UINT32 APB_FREQ;

void PIPO0_ISRHandler(UINT32 irqid);
void PIPO1_ISRHandler(UINT32 irqid);
void PIPO2_ISRHandler(UINT32 irqid);
void PIPO3_ISRHandler(UINT32 irqid);
void PIPO4_ISRHandler(UINT32 irqid);
void PIPO5_ISRHandler(UINT32 irqid);

Struct_IRQHandler PIPOIntr_Table[]={
		{"PIPO0",INTC_PIPO0,0,PIPO0_ISRHandler,0,NULL},
		{"PIPO1",INTC_PIPO1,0,PIPO1_ISRHandler,0,NULL},
		{"PIPO2",INTC_PIPO2,0,PIPO2_ISRHandler,0,NULL},
		{"PIPO3",INTC_PIPO3,0,PIPO3_ISRHandler,0,NULL},
		{"PIPO4",INTC_PIPO4,0,PIPO4_ISRHandler,0,NULL},
		{"PIPO5",INTC_PIPO5,0,PIPO5_ISRHandler,0,NULL},
};

/*
 * 清空PIPO所有寄存器，实现复位PIPO
 * 输入参数：pPipo：PIPO基地址，PIPO0~PIPO5
 * 输出参数：无
 * 说明：目前PIPO寄存器复位值都为0，所以可以通过此方式实现复位
 */
void PIPO_Reset(pStruct_PIPOInfo pPipo)
{
	UINT32 i,siz = sizeof(Struct_PIPOInfo);
	UINT8 * p = (UINT8 *)pPipo;

	for(i=0;i<siz;i++) /*将寄存器全部清零，PIPO所有寄存器复位值都为零*/
		p[i] = 0;
}

/*
 * PIPO产生更新事件
 * 输入参数：pPipo：PIPO基地址，PIPO0~PIPO5
 * 输出参数：无
 * 说明：更新事件：重初始化寄存器，包括：TIM_PSC预分频器、TIM_ARR自动装载寄存器、
 *     TIM_RCR重复次数寄存器、TIM_CCRA比较寄存器A、TIM_CCRB比较寄存器B、
 *     TIM_DT_PRE前死去时间寄存器、TIM_DT_POST后死区时间寄存器
 *     清零寄存器，包括：PW_H_A、PW_H_B、 PW_L_A、 PW_L_B、PW_CNT_A、TIM_CNT(如果是单脉冲模式，则加载值ARR)
 */
void PIPO_Update(pStruct_PIPOInfo pPIPO)
{
	UINT8  i;
	pPIPO->TIM_EGR = 1;  /*产生更新事件*/
	for(i = 0; i < 100; i++)
	{
		asm("NOP");
	}
	pPIPO->TIM_EGR = 0;
}

/*
 * 产生更新事件并启动PIPO模式
 * 输入参数：pPipo：PIPO基地址，PIPO0~PIPO5
 * 输出参数：无
 */
void PIPO_UpdateAndStart(pStruct_PIPOInfo pPIPO)
{
	UINT8  i;

	pPIPO->TIM_EGR = 1;
	for(i = 0; i < 100; i++)
	{
		asm("NOP");
	}
	pPIPO->TIM_EGR = 0;/*产生更新事件*/
	pPIPO->TIM_CR |= 1;/*使能定时器计数器*/
}

/*
 * 启动定时器计数器
 * 输入参数：pPipo：PIPO基地址，PIPO0~PIPO5
 * 输出参数：无
 */
void PIPO_TimerCountStart(pStruct_PIPOInfo pPIPO)
{
	pPIPO->TIM_CR |= 1;
}

/*
 * 停止定时器计数器
 * 输入参数：pPipo：PIPO基地址，PIPO0~PIPO5
 * 输出参数：无
 */
void PIPO_TimerCountStop(pStruct_PIPOInfo pPIPO)
{
	pPIPO->TIM_CR &= ~1;
}

/*
 * 清除中断标志位
 * 输入参数：pPipo：PIPO基地址，PIPO0~PIPO5
 * 输出参数：返回：dwIntr：返回中断状态寄存器值
 */
UINT32 PIPO_ClearInterrupt(pStruct_PIPOInfo pPIPO)
{
	UINT32 dwIntr = pPIPO->PIPO_INT;
	pPIPO->TIM_CR |= PIPO_INTC_CLEAN; /*写入“1”之后不能再立即写入“0”，否则可能会导致中断清除失败*/

	return dwIntr;
}

/*
 * 使能中断
 * 输入参数：pPipo：PIPO基地址，PIPO0~PIPO5
 * 输出参数：无
 */
void PIPO_EnableInterrupt(pStruct_PIPOInfo pPIPO)
{
	pPIPO->TIM_CR &= ~PIPO_INTC_CONTROL;
}

/*
 * 失能中断
 * 输入参数：pPipo：PIPO基地址，PIPO0~PIPO5
 * 输出参数：无
 */
void PIPO_DisableInterrupt(pStruct_PIPOInfo pPIPO)
{
	pPIPO->TIM_CR |= PIPO_INTC_CONTROL;
}

/*
 * 打开中断屏蔽功能
 * 输入参数：pPipo：PIPO基地址，PIPO0~PIPO5，dwInterrupt：所要屏蔽的中断
 * 输出参数：无
 * 说明：寄存器PIPO_INT_DIS相应位置1为屏蔽中断，置0为不屏蔽中断
 */
void PIPO_EnInterruptMask(pStruct_PIPOInfo pPIPO,UINT32 dwInterrupt)
{
	pPIPO->PIPO_INT_DIS |= dwInterrupt;/*打开定时器中断*/
}

/*
 * 关闭中断屏蔽功能
 * 输入参数：pPipo：PIPO基地址，PIPO0~PIPO5，dwInterrupt：不需屏蔽的中断
 * 输出参数：无
 * 说明：寄存器PIPO_INT_DIS相应位置1为屏蔽中断，置0为不屏蔽中断
 */
void PIPO_DisInterruptMask(pStruct_PIPOInfo pPIPO,UINT32 dwInterrupt)
{
	pPIPO->PIPO_INT_DIS &= ~dwInterrupt;/*打开定时器中断*/
}

/*
 * 获取定时器计数器计数值
 * 输入参数：pPipo：PIPO基地址，PIPO0~PIPO5
 * 输出参数：返回 TIM_CNT
 */
UINT32 PIPO_GetTimerCount(pStruct_PIPOInfo pPIPO)
{
	return (pPIPO->TIM_CNT);
}

/*
 * PIPO的定时器功能初始化
 * 输入参数：pPIPOInfo:PIPO初始化结构体
 * 输出参数：无
 * 修改：1、在注册中断函数之前，先进行释放，避免重复调用时多次注册。（zhulin 20190716）
 */
void PIPO_TimerInit(pPIPO_InitStructure pPIPOInfo)
{
	pStruct_IRQHandler pPIPOIntrInfo;
	pPIPO_TimerStructure pTimer = (pPIPO_TimerStructure)(pPIPOInfo->Function_t);

	pPIPOInfo->pPIPOAddr->TIM_CR = ( PIPO_MODE_TIMER<< 5)|( pTimer->Dir<< 2);	/*配置控制寄存器*/

	if(pTimer->InterruptEnable==0)											/*配置中断*/
	{
		/*屏蔽定时器的向上、向下、中央计数完成中断*/
		PIPO_EnInterruptMask(pPIPOInfo->pPIPOAddr,(PIPO_INTC_CENTER_COUNT|PIPO_INTC_DOWN_COUNT|PIPO_INTC_UP_COUNT));
		PIPO_DisableInterrupt(pPIPOInfo->pPIPOAddr);
	}
	else
	{
		PIPO_ClearInterrupt(pPIPOInfo->pPIPOAddr);             				/* 清除中断标志 */
		PIPO_DisInterruptMask(pPIPOInfo->pPIPOAddr,(PIPO_INTC_CENTER_COUNT|PIPO_INTC_DOWN_COUNT|PIPO_INTC_UP_COUNT));
		pPIPOIntrInfo = &PIPOIntr_Table[pPIPOInfo->PIPO_Index];
		if(pPIPOInfo->PIPO_ISR != NULL)
		{
			pPIPOIntrInfo->handler = pPIPOInfo->PIPO_ISR;
		}
		pPIPOIntrInfo->priority = pPIPOInfo->priority;
		INTC_FreeIrq(pPIPOIntrInfo);
		INTC_RequestIrq(pPIPOIntrInfo);                                     /* 注册中断函数 */
		PIPO_EnableInterrupt(pPIPOInfo->pPIPOAddr);
	}

	PIPO_SetFreqTime(pPIPOInfo->pPIPOAddr,pTimer->dwTimeout,pTimer->Unit);		/*配置时定时时间*/
}

/*
 * 配置PIPO定时器计数器的计数时间，即配置分频系数和自动装载值
 * 输入参数：pPipo：PIPO基地址，PIPO0~PIPO5  dwPeriod：周期时间    Unit：单位
 * 输出参数：无
 * 说明：Unit：可选5种：
 *  UNIT_S 	= 1,
 *	UNIT_MS	= 2,
 *	UNIT_US = 3,
 *	UNIT_HZ = 5,   PWM模式使用
 *	UNIT_TICK= 6,  时钟滴答
 */
void PIPO_SetFreqTime(pStruct_PIPOInfo pPIPO,UINT32 dwPeriod,Enum_PIPO_TimerUnit Unit)
{
	UINT32 dwSysClock = APB_FREQ;			/*需要计数值，系统频率*/
	UINT64 ddwCnt=0;
	UINT32 dwLoadARR=0,dwLoadPSC=0;

	switch(Unit)	/*根据单位计算技术值，为防止溢出ddwCnt为64位变量*/
	{
	case UNIT_S:
			ddwCnt = (UINT64)dwSysClock * dwPeriod;
		break;
	case UNIT_MS:
			ddwCnt = ((UINT64)dwSysClock * dwPeriod)/1000 ;
		break;
	case UNIT_US:
			ddwCnt = ((UINT64)dwSysClock * dwPeriod)/1000000;
		break;
	case UNIT_HZ:
			ddwCnt = dwSysClock / dwPeriod ;
		break;
	case UNIT_TICK:
			ddwCnt = dwPeriod;
		break;
	default:
		return ;
		break;
	}

	/*计算预分频值、装载值*/
	if(ddwCnt <= 0xffffffff)	/*无需分频*/
	{
		dwLoadARR = (UINT32)ddwCnt;
		dwLoadPSC = 0;
	}
	else 						/*需要分频，此种计算方式忽略分频误差*/
	{
		dwLoadPSC = (ddwCnt+0xfffffffe)/0xffffffff;
		dwLoadARR = ddwCnt/dwLoadPSC;
	}

	pPIPO->TIM_PSC = dwLoadPSC;	/*配置分频器*/
	pPIPO->TIM_ARR = dwLoadARR; /*配置预装载值*/
}

/*
 * PIPO的PWM模式初始化
 * 输入参数：pPIPOInfo:PIPO初始化结构体
 * 输出参数：无
 */
void PIPO_PwmInit(pPIPO_InitStructure pPIPOInfo)
{
	pPIPO_PwmStructure pPWM = (pPIPO_PwmStructure)(pPIPOInfo->Function_t);

	/*配置控制寄存器*/
	pPIPOInfo->pPIPOAddr->TIM_CR = (PIPO_MODE_PWM << 5 )|(pPWM->Polarity<< 8)|(pPWM->DTimeEnable<< 9 );

	/*根据模式和对齐方式决定计数器 计数方式*/
	if(pPWM->Polarity==0)
	{
		pPIPOInfo->pPIPOAddr->TIM_CR |=((pPWM->Align)<<2);
	}
	else if(pPWM->Polarity==1)
	{
		switch(pPWM->Align)
		{
		case PIPO_PWM_ALIGN_LEFT:
			pPIPOInfo->pPIPOAddr->TIM_CR |=(2<<2);
			break;
		case PIPO_PWM_ALIGN_RIGHT:
			pPIPOInfo->pPIPOAddr->TIM_CR |=(1<<2);
			break;
		case PIPO_PWM_ALIGN_MIDDLE:
			pPIPOInfo->pPIPOAddr->TIM_CR |=(3<<2);
			break;
		default:
			break;
		}

	}

	PIPO_SetPwm(pPIPOInfo); /*对计数器进行配置，但不启动更新事件*/
}

/*
 * PIPO的PWM模式失能
 * 输入参数：pPipo：PIPO基地址，PIPO0~PIPO5
 * 输出参数：无
 * 说明：由于直接关闭定时器计数器会导致PWM直接输出随机，配置为保留模式实现输出低
 */
void PIPO_PwmStop(pStruct_PIPOInfo pPIPO)
{
	pPIPO->TIM_CR &= ~(1 <<0);
	pPIPO->TIM_CR &= PIPO_CLEAR_MODE;/*清除当前模式*/
	pPIPO->TIM_CR |= (PIPO_MODE_NONE << 5 );/*配置为保留模式*/
}

/*
 * PIPO的PWM模式重新使能
 * 输入参数：pPipo：PIPO基地址，PIPO0~PIPO5
 * 输出参数：无
 * 说明：与函数PIPO_PwmStop对应，功能块输出PWM的频率为0HZ时通过关闭PWM实现，当输出PWM的频率不为0HZ时，使用此函数恢复PWM功能
 */
void PIPO_PwmRestart(pStruct_PIPOInfo pPIPO)
{
	if((pPIPO->TIM_CR & PIPO_TIMER_ENABLE)==0)
	{
		pPIPO->TIM_CR &= PIPO_CLEAR_MODE;
		pPIPO->TIM_CR |= (PIPO_MODE_PWM<<5);
		pPIPO->TIM_CR |= 1;
	}
}

/*
 * PIPO的PWM模式死区功能打开
 * 输入参数：pPipo：PIPO基地址，PIPO0~PIPO5
 * 输出参数：无
 */
void PIPO_DtimeEnable(pStruct_PIPOInfo pPIPO)
{
	if((pPIPO->TIM_CR & PIPO_INTC_DTIME)==0)
		pPIPO->TIM_CR |= PIPO_INTC_DTIME;
}

/*
 * PIPO的PWM参数配置
 * 输入参数：pPIPOInfo:PIPO初始化结构体
 * 输出参数：无
 * 说明：配置PWM周期，A、B端口的占空比、死区时间    频率不能为0，不能超过最大值PIPO_FREQUENT_MAX
 */
void PIPO_SetPwm(pPIPO_InitStructure pPIPOInfo)
{
	UINT64 ddwValue,ddwTIM_ARR;
	pPIPO_PwmStructure pPWM = (pPIPO_PwmStructure)(pPIPOInfo->Function_t);

	if((pPWM->Freq_Hz==0)||(pPWM->Freq_Hz>PIPO_FREQUENT_MAX))	/*频率参数不能为0,不能超过最大值*/
		return ;

	PIPO_SetFreqTime(pPIPOInfo->pPIPOAddr,pPWM->Freq_Hz,UNIT_HZ); /*设置PWM周期*/

	ddwTIM_ARR = (UINT64)pPIPOInfo->pPIPOAddr->TIM_ARR ;

	ddwValue = ddwTIM_ARR * pPWM->DutyRatioA / PIPO_DUTY_RATIO_MAX;
	pPIPOInfo->pPIPOAddr->TIM_CCRA = (UINT32) ddwValue;           /*配置A端口占空比*/

	ddwValue =	ddwTIM_ARR * pPWM->DutyRatioB / PIPO_DUTY_RATIO_MAX;
	pPIPOInfo->pPIPOAddr->TIM_CCRB = (UINT32)ddwValue;            /*配置B端口占空比*/

	if(pPIPOInfo->pPIPOAddr->TIM_CR &PIPO_INTC_DTIME)			  /*配置死区*/
	{
		ddwValue =	ddwTIM_ARR*pPWM->DtimePre/PIPO_DUTY_RATIO_MAX;
		pPIPOInfo->pPIPOAddr->TIM_DT_PRE = (UINT32)ddwValue;
		ddwValue =	ddwTIM_ARR*pPWM->DtimePost/PIPO_DUTY_RATIO_MAX;
		pPIPOInfo->pPIPOAddr->TIM_DT_POST= (UINT32)ddwValue;
	}

}

/*
 * PIPO配置PWM输出频率，并保持A、B通道占空比不变
 * 输入参数：pPipo：PIPO基地址，PIPO0~PIPO5，dwFrequent：PWM输出频率   不能为0，不能超过最大值PIPO_FREQUENT_MAX
 * 输出参数：无
 * 说明：此函数不作为首次配置使用，因为首次使用不存在占空比
 */
UINT8 PIPO_PwmSetFreq(pStruct_PIPOInfo pPIPO,UINT32 dwFrequent)
{
	UINT32 dwLastTIM_ARR;
	UINT64 ddwTIM_ARR;

	if((pPIPO==NULL)||(dwFrequent==0)||(dwFrequent>PIPO_FREQUENT_MAX))/*参数错误*/
		return 1;

	dwLastTIM_ARR = pPIPO->TIM_ARR;
	PIPO_SetFreqTime(pPIPO,dwFrequent,UNIT_HZ);
	ddwTIM_ARR = (UINT64)pPIPO->TIM_ARR ;

	if(dwLastTIM_ARR)/*防止除零错误*/
	{
		pPIPO->TIM_CCRA = ddwTIM_ARR * pPIPO->TIM_CCRA / dwLastTIM_ARR;
		pPIPO->TIM_CCRB = ddwTIM_ARR * pPIPO->TIM_CCRB / dwLastTIM_ARR;
	}

	return 0;
}

/*
 * PIPO配置PWMA、B通道占空比
 * 输入参数：pPipo：PIPO基地址，PIPO0~PIPO5，dwDutyRatioA：A通道占空比，dwDutyRatioB：B通道占空比
 * 输出参数：无
 * 说明：如果占空比配置值超过最大值即表示保持当前通道占空比不变
 */
void PIPO_PwmSetDutyRatio(pStruct_PIPOInfo pPIPO,UINT32 dwDutyRatioA,UINT32 dwDutyRatioB)
{
	if(dwDutyRatioA <= PIPO_DUTY_RATIO_MAX)
		pPIPO->TIM_CCRA = (UINT64)dwDutyRatioA * pPIPO->TIM_ARR / PIPO_DUTY_RATIO_MAX;
	if(dwDutyRatioB <= PIPO_DUTY_RATIO_MAX)
		pPIPO->TIM_CCRB = (UINT64)dwDutyRatioB * pPIPO->TIM_ARR / PIPO_DUTY_RATIO_MAX;
}

/*
 * PIPO配置PWMA、B通道占空比和频率
 * 输入参数：pPipo：PIPO基地址，PIPO0~PIPO5，dwFrequent：PWM输出频率，dwDutyRatioA：A通道占空比，dwDutyRatioB：B通道占空比
 * 输出参数：无
 * 说明：A、B两路同频率。如果占空比配置值超过最大值即表示保持当前通道占空比不变
 */
UINT8 PIPO_PwmSetFreqAndDutyRatio(pStruct_PIPOInfo pPIPO,UINT32 dwFrequent,UINT32 dwDutyRatioA,UINT32 dwDutyRatioB)
{
	UINT32 dwLastTIM_ARR;
	UINT64 ddwTIM_ARR;
	UINT32 dwPrescaler = pPIPO->TIM_PSC;

	if((pPIPO==NULL)||(dwFrequent==0)||(dwFrequent>PIPO_FREQUENT_MAX))/*参数错误*/
		return 1;

	dwLastTIM_ARR = pPIPO->TIM_ARR;
	if(dwFrequent <= PIPO_FREQUENT_MAX)
		pPIPO->TIM_ARR = APB_FREQ / dwFrequent / (dwPrescaler+1);
	ddwTIM_ARR = (UINT64)pPIPO->TIM_ARR;  /*64位防止计算溢出*/

	if(dwDutyRatioA <= PIPO_DUTY_RATIO_MAX)
		pPIPO->TIM_CCRA = ddwTIM_ARR * dwDutyRatioA / PIPO_DUTY_RATIO_MAX;
	else if(dwLastTIM_ARR!=0)
		pPIPO->TIM_CCRA = ddwTIM_ARR * pPIPO->TIM_CCRA / dwLastTIM_ARR;

	if(dwDutyRatioB <= PIPO_DUTY_RATIO_MAX)
		pPIPO->TIM_CCRB = ddwTIM_ARR * dwDutyRatioB / PIPO_DUTY_RATIO_MAX;
	else if(dwLastTIM_ARR!=0)
		pPIPO->TIM_CCRB = ddwTIM_ARR * pPIPO->TIM_CCRB / dwLastTIM_ARR;

	return 0;
}

/*
 * PIPO配置PWM 死区时间
 * 输入参数：pPipo：PIPO基地址，PIPO0~PIPO5，DtimePre：前死区时间，DtimePost：后死区时间
 * 输出参数：无
 */
void PIPO_PwmSetDeadTime(pStruct_PIPOInfo pPIPO,UINT32 DtimePre,UINT32 DtimePost)
{
	pPIPO->TIM_DT_PRE = (UINT64)DtimePre*pPIPO->TIM_ARR/PIPO_DUTY_RATIO_MAX;
	pPIPO->TIM_DT_POST= (UINT64)DtimePost*pPIPO->TIM_ARR/PIPO_DUTY_RATIO_MAX;
}

/*
 * PIPO脉宽测量初始化
 * 输入参数：pPIPOInfo:PIPO初始化结构体
 * 输出参数：返回值：0初始化成功，1初始化失败
 * 说明：脉宽测量总共有5种方式：
 * 	PULSE_MEASURE_SigleHigh 	= 0,	高电平单次测量模式
 *	PULSE_MEASURE_MultHigh 		= 1,	高电平时间测量模式
 *	PULSE_MEASURE_SigleLow		= 2,	低电平单次测量模式
 *	PULSE_MEASURE_MultLow		= 3,	低电平时间测量模式
 *	PULSE_MEASURE_LowAndHigh	= 4,	全电平单次测量模式
 *	这5种方式中时间测量在时间到后可触发中断，高/低电平单次测量模式测量完成触发中断
 *	全电平单次测量模式不会触发中断，只能通过查询方式
 *
 * 修改：1、在注册中断函数之前，先进行释放，避免重复调用时多次注册。（zhulin 20190716）
 */
UINT8 PIPO_PulseMeasureInit(pPIPO_InitStructure pPIPOInfo)
{
	pStruct_IRQHandler pPIPOIntrInfo;
	pPIPO_PulseMeasureStructure pPulseMeasure = (pPIPO_PulseMeasureStructure)(pPIPOInfo->Function_t);

	pPIPOInfo->pPIPOAddr->TIM_CR = ((PIPO_MODE_PulseWideMeasure << 5)|(pPulseMeasure->Mode << 10)  \
			|(PIPO_TIMER_UP<<2));	/*配置控制寄存器,脉宽测量模式，计数器向上计数，测量类型*/

	/*配置中断*/
	if((pPulseMeasure->IntEnable_A ==1)||(pPulseMeasure->IntEnable_B ==1))
	{
		PIPO_ClearInterrupt(pPIPOInfo->pPIPOAddr);             /* 清除中断标志 */

		if(pPulseMeasure->IntEnable_A ==1)
		{
			pPIPOInfo->pPIPOAddr->PIPO_INT_DIS &= ~(PIPO_INTC_A_LOW_TIME|PIPO_INTC_A_HIGH_TIME|   \
					PIPO_INTC_A_LOW_SINGLE|PIPO_INTC_A_HIGH_SINGLE);/*打开A端口脉冲测量的四类中断*/
		}

		if(pPulseMeasure->IntEnable_B ==1)
		{
			pPIPOInfo->pPIPOAddr->PIPO_INT_DIS &= ~(PIPO_INTC_B_LOW_TIME|PIPO_INTC_B_HIGH_TIME|   \
					PIPO_INTC_B_LOW_SINGLE|PIPO_INTC_B_HIGH_SINGLE);/*打开B端口脉冲测量的四类中断*/
		}
		pPIPOIntrInfo = &PIPOIntr_Table[pPIPOInfo->PIPO_Index];
		if(pPIPOInfo->PIPO_ISR != NULL)
		{
			pPIPOIntrInfo->handler = pPIPOInfo->PIPO_ISR;
		}
		pPIPOIntrInfo->priority = pPIPOInfo->priority;
		INTC_FreeIrq(pPIPOIntrInfo);
		INTC_RequestIrq(pPIPOIntrInfo);                         /* 注册中断函数 */
		PIPO_EnableInterrupt(pPIPOInfo->pPIPOAddr);
	}
	else
	{
		pPIPOInfo->pPIPOAddr->PIPO_INT_DIS |= (PIPO_INTC_A_LOW_TIME|PIPO_INTC_A_HIGH_TIME|   \
				PIPO_INTC_A_LOW_SINGLE|PIPO_INTC_A_HIGH_SINGLE);/*关闭A端口脉冲测量的四类中断*/
		pPIPOInfo->pPIPOAddr->PIPO_INT_DIS |= (PIPO_INTC_B_LOW_TIME|PIPO_INTC_B_HIGH_TIME|   \
				PIPO_INTC_B_LOW_SINGLE|PIPO_INTC_B_HIGH_SINGLE);/*关闭B端口脉冲测量的四类中断*/
		PIPO_DisableInterrupt(pPIPOInfo->pPIPOAddr);

	}

	/*配置高电平时间测量模式、低电平时间测量模式的测量时间*/
	if((pPulseMeasure->Mode == PULSE_MEASURE_MultHigh) ||(pPulseMeasure->Mode == PULSE_MEASURE_MultLow ))
	{
		if(pPulseMeasure->Period ==0)
			return 1;/*脉宽测量模式，却未指定周期*/
		else
			PIPO_SetFreqTime(pPIPOInfo->pPIPOAddr,pPulseMeasure->Period,pPulseMeasure->Unit);/*配置测量周期*/
	}

	return 0;
}

/*
 * 读取端口A的脉宽的相应寄存器
 * 输入参数：pPipo：PIPO基地址，PIPO0~PIPO5，
 * 输出参数：pHighA：端口A高脉宽时间  pLowA：端口A低脉宽时间
 */
void PIPO_PulseMeasureReadPortA(pStruct_PIPOInfo pPIPO,UINT32 * pHighA,UINT32 * pLowA)
{
	UINT32 dwMode;

	dwMode = (pPIPO->TIM_CR >> 10) & 0x7 ;
	if(pHighA != NULL)
	{
		if((dwMode == PULSE_MEASURE_SigleHigh)||(dwMode == PULSE_MEASURE_MultHigh)  \
				||(dwMode == PULSE_MEASURE_LowAndHigh))  /*读取高脉宽时间计数值*/
		{
			*pHighA = pPIPO->PW_H_A;
		}
		else
		{
			*pHighA = 0;
		}
	}
	if(pLowA != NULL)
	{
		if((dwMode == PULSE_MEASURE_SigleLow)||(dwMode == PULSE_MEASURE_MultLow)   \
				||(dwMode == PULSE_MEASURE_LowAndHigh))  /*读取低脉宽时间计数值*/
		{
			*pLowA	= pPIPO->PW_L_A;
		}
		else
		{
			*pLowA = 0;
		}
	}

}

/*
 * 读取端口B的脉宽的相应寄存器
 * 输入参数：pPipo：PIPO基地址，PIPO0~PIPO5，
 * 输出参数：pHighB：端口B高脉宽时间  pLowB：端口B低脉宽时间
 */
void PIPO_PulseMeasureReadPortB(pStruct_PIPOInfo pPIPO,UINT32 * pHighB,UINT32 * pLowB)
{
	UINT32 dwMode;

	dwMode = (pPIPO->TIM_CR >> 10) & 0x7 ;
	if(pHighB != NULL)
	{
		if((dwMode == PULSE_MEASURE_SigleHigh)||(dwMode == PULSE_MEASURE_MultHigh)  \
				||(dwMode == PULSE_MEASURE_LowAndHigh))
		{
			*pHighB = pPIPO->PW_H_B;
		}
		else
		{
			*pHighB = 0;
		}
	}
	if(pLowB != NULL)
	{
		if((dwMode == PULSE_MEASURE_SigleLow)||(dwMode == PULSE_MEASURE_MultLow)  \
				||(dwMode == PULSE_MEASURE_LowAndHigh))
		{
			*pLowB	= pPIPO->PW_L_B;
		}
		else
		{
			*pLowB = 0;
		}
	}

}

/*
 * PIPO脉冲个数测量初始化
 * 输入参数：pPIPOInfo:PIPO初始化结构体
 * 输出参数：返回值：0初始化成功，1初始化失败
 *
 * 修改：1、在注册中断函数之前，先进行释放，避免重复调用时多次注册。（zhulin 20190716）
 */
UINT8 PIPO_PulseCountInit(pPIPO_InitStructure pPIPOInfo)
{
	pStruct_IRQHandler pPIPOIntrInfo;
	pPIPO_PulseCountStructure pPulseCount = (pPIPO_PulseCountStructure)(pPIPOInfo->Function_t);

	pPIPOInfo->pPIPOAddr->TIM_CR = (PIPO_MODE_PulseCount << 5)|(pPulseCount->Trigger_A << 18)   \
			|(pPulseCount->Trigger_B << 19)|(PIPO_TIMER_UP<<2);/*配置PIPO工作模式、边缘检测方式、计数器计数模式*/

	/*配置中断*/
	if((pPulseCount->IntEnable_A ==1)||(pPulseCount->IntEnable_B ==1))
	{
		PIPO_ClearInterrupt(pPIPOInfo->pPIPOAddr);             /* 清除中断标志 */

		if(pPulseCount->IntEnable_A ==1)
		{
			pPIPOInfo->pPIPOAddr->PIPO_INT_DIS &= ~PIPO_INTC_A_PULSE_NUM;
		}

		if(pPulseCount->IntEnable_B ==1)
		{
			pPIPOInfo->pPIPOAddr->PIPO_INT_DIS &= ~PIPO_INTC_B_PULSE_NUM;
		}
		pPIPOIntrInfo = &PIPOIntr_Table[pPIPOInfo->PIPO_Index];
		if(pPIPOInfo->PIPO_ISR != NULL)
		{
			pPIPOIntrInfo->handler = pPIPOInfo->PIPO_ISR;
		}
		pPIPOIntrInfo->priority = pPIPOInfo->priority;
		INTC_FreeIrq(pPIPOIntrInfo);
		INTC_RequestIrq(pPIPOIntrInfo);                          /* 注册中断函数 */
		PIPO_EnableInterrupt(pPIPOInfo->pPIPOAddr);
	}
	else
	{
		pPIPOInfo->pPIPOAddr->PIPO_INT_DIS |= PIPO_INTC_A_PULSE_NUM;
		pPIPOInfo->pPIPOAddr->PIPO_INT_DIS |= PIPO_INTC_B_PULSE_NUM;
		PIPO_DisableInterrupt(pPIPOInfo->pPIPOAddr);
	}

	/*配置测量周期*/
	if(pPulseCount->Period == 0)
		return 1;
	else
		PIPO_SetFreqTime(pPIPOInfo->pPIPOAddr,pPulseCount->Period,pPulseCount->Unit);

	return 0;
}

/*
 * 读取A端口的脉冲个数
 * 输入参数：pPipo：PIPO基地址，PIPO0~PIPO5，
 * 输出参数：返回值：A端口脉冲个数
 */
UINT32 PIPO_PulseCountReadPortA(pStruct_PIPOInfo pPIPO)
{
	return (pPIPO->PW_H_A);
}

/*
 * 读取B端口的脉冲个数
 * 输入参数：pPipo：PIPO基地址，PIPO0~PIPO5，
 * 输出参数：返回值：B端口脉冲个数
 */
UINT32 PIPO_PulseCountReadPortB(pStruct_PIPOInfo pPIPO)
{
	return (pPIPO->PW_H_B);
}

/*
 * PIPO单脉冲输出功能初始化
 * 输入参数：pPIPOInfo:PIPO初始化结构体
 * 输出参数：返回值：0初始化成功，1初始化失败
 *
 * 修改：1、在注册中断函数之前，先进行释放，避免重复调用时多次注册。（zhulin 20190716）
 */
UINT8 PIPO_SinglePulseInit(pPIPO_InitStructure pPIPOInfo)
{
	UINT64 ddwValue=0,ddwTIM_ARR=0;
	pStruct_IRQHandler pPIPOIntrInfo;
	pPIPO_SinglePulseStructure SinglePulse = (pPIPO_SinglePulseStructure)(pPIPOInfo->Function_t);

	pPIPOInfo->pPIPOAddr->TIM_CR = ((PIPO_MODE_OutputSingle) << 5 )|(PIPO_TIMER_DOWN<<2);/*配置单脉冲模式，计数器向下计数*/

	/*配置中断*/
	if(SinglePulse->IntEnable ==1)
	{
		PIPO_ClearInterrupt(pPIPOInfo->pPIPOAddr);             /* 清除中断标志 */
		pPIPOInfo->pPIPOAddr->PIPO_INT_DIS &= ~PIPO_INTC_PULSE;
		pPIPOIntrInfo = &PIPOIntr_Table[pPIPOInfo->PIPO_Index];
		if(pPIPOInfo->PIPO_ISR != NULL)
		{
			pPIPOIntrInfo->handler = pPIPOInfo->PIPO_ISR;
		}
		pPIPOIntrInfo->priority = pPIPOInfo->priority;
		INTC_FreeIrq(pPIPOIntrInfo);
		INTC_RequestIrq(pPIPOIntrInfo);                         /* 注册中断函数 */
		PIPO_EnableInterrupt(pPIPOInfo->pPIPOAddr);
	}
	else
	{
		pPIPOInfo->pPIPOAddr->PIPO_INT_DIS |= PIPO_INTC_PULSE;
		PIPO_DisableInterrupt(pPIPOInfo->pPIPOAddr);
	}

	/*配置脉冲时间*/
	if(SinglePulse->PulseWidth == 0)
		return 1;
	else
	{
		PIPO_SetFreqTime(pPIPOInfo->pPIPOAddr,(SinglePulse->TriggerDelay+SinglePulse->PulseWidth),  \
				SinglePulse->Unit); /*配置单脉冲时间，包括延时及脉冲高电平时间*/
		ddwTIM_ARR = (UINT64)pPIPOInfo->pPIPOAddr->TIM_ARR ;
		ddwValue =	ddwTIM_ARR*SinglePulse->PulseWidth/(SinglePulse->TriggerDelay+SinglePulse->PulseWidth);
		pPIPOInfo->pPIPOAddr->TIM_CCRB = (UINT32)ddwValue;/*配置脉冲高电平时间*/
	}

	return 0;
}

/*
 * 配置PIPO单脉冲输出模式的延迟及高电平时间
 * 输入参数：pPipo：PIPO基地址，PIPO0~PIPO5，Delay：脉冲延时时间，Htime：脉冲高电平时间，Unit：时间单位
 * 输出参数：返回值：0配置成功，1配置失败
 */
UINT8 PIPO_SetSinglePulse(pStruct_PIPOInfo pPIPO,UINT32 Delay,UINT32 Htime,Enum_PIPO_TimerUnit Unit)
{
	UINT64 ddwValue=0,ddwTIM_ARR=0;
	/*配置脉冲时间*/
	if(Htime == 0)
		return 1;
	else
	{
		PIPO_SetFreqTime(pPIPO,(Delay+Htime),Unit); /*配置单脉冲时间，包括延时及脉冲高电平时间*/
		ddwTIM_ARR = (UINT64)pPIPO->TIM_ARR ;
		ddwValue =	ddwTIM_ARR*Htime/(Delay+Htime);
		pPIPO->TIM_CCRB = (UINT32)ddwValue;/*配置脉冲高电平时间*/
	}
	return 0;
}

/*
 * PIPO正交编码器功能初始化
 * 输入参数：pPIPOInfo:PIPO初始化结构体
 * 输出参数：无
 *
 * 修改：1、在注册中断函数之前，先进行释放，避免重复调用时多次注册。（zhulin 20190716）
 */
void PIPO_COPInit(pPIPO_InitStructure pPIPOInfo)
{
	pStruct_IRQHandler pPIPOIntrInfo;
	pPIPO_COP_Structure pCOP = (pPIPO_COP_Structure)(pPIPOInfo->Function_t);

	pPIPOInfo->pPIPOAddr->TIM_CR = (PIPO_MODE_MotorDir << 5);/*配置正交编码模式*/

	if(pCOP->SignEnable==0)
	{
		pPIPOInfo->pPIPOAddr->TIM_CR &= ~(0x1 << 13);/*无符号计数*/
	}
	else
	{
		pPIPOInfo->pPIPOAddr->TIM_CR |= 0x1 << 13;/*有符号计数*/
	}

	/*配置计数上限、下限*/
	pPIPOInfo->pPIPOAddr->CW_MAX_POS_ADDR = pCOP->MaxAddr;/*正向最大值*/
	pPIPOInfo->pPIPOAddr->CW_MIN_POS_ADDR = pCOP->MinAddr;/*反向最小值*/

	/*配置中断*/
	if((pCOP->IntEnableDownflow ==1)||(pCOP->IntEnableOverflow ==1))
	{
		PIPO_ClearInterrupt(pPIPOInfo->pPIPOAddr);             /* 清除中断标志 */

		if(pCOP->IntEnableDownflow ==1)
		{
			pPIPOInfo->pPIPOAddr->PIPO_INT_DIS &= ~PIPO_INTC_COP_DOWN_FLOW;
		}

		if(pCOP->IntEnableOverflow ==1)
		{
			pPIPOInfo->pPIPOAddr->PIPO_INT_DIS &= ~PIPO_INTC_COP_OVER_FLOW;
		}
		pPIPOIntrInfo = &PIPOIntr_Table[pPIPOInfo->PIPO_Index];
		if(pPIPOInfo->PIPO_ISR != NULL)
		{
			pPIPOIntrInfo->handler = pPIPOInfo->PIPO_ISR;
		}
		pPIPOIntrInfo->priority = pPIPOInfo->priority;
		INTC_FreeIrq(pPIPOIntrInfo);
		INTC_RequestIrq(pPIPOIntrInfo);                       /* 注册中断函数 */
		PIPO_EnableInterrupt(pPIPOInfo->pPIPOAddr);
	}
	else
	{
		pPIPOInfo->pPIPOAddr->PIPO_INT_DIS |= PIPO_INTC_COP_DOWN_FLOW;
		pPIPOInfo->pPIPOAddr->PIPO_INT_DIS |= PIPO_INTC_COP_OVER_FLOW;
		PIPO_DisableInterrupt(pPIPOInfo->pPIPOAddr);
	}
}

/*
 * 返回运转方向
 * 输入参数：pPipo：PIPO基地址，PIPO0~PIPO5，
 * 输出参数：返回值：运转方向，最低位表示运转方向：0正转，1反转
 * 说明：当A相超前B相时，转动方向信号为1，向上计数（反转）；B相超前A相时，
 * 转动方向信号为0，往下计数（正传）
 */
UINT32 PIPO_COP_GetDir(pStruct_PIPOInfo pPIPO)
{
	return (pPIPO->CW_MOTOR_DIR);
}

/*
 * 返回正交编码器计数器值
 * 输入参数：pPipo：PIPO基地址，PIPO0~PIPO5，
 * 输出参数：返回值：正交编码器计数器值
 */
UINT32 PIPO_COP_GetCount(pStruct_PIPOInfo pPIPO)
{
	return (pPIPO->PW_CNT_A);
}

/*
 * PIPO0默认中断处理函数
 * 输入参数：
 * 输出参数：
 */
void PIPO0_ISRHandler(UINT32 irqid)
{
	PIPO_ClearInterrupt(PIPO0);
}

/*
 * PIPO1默认中断处理函数
 * 输入参数：
 * 输出参数：
 */
void PIPO1_ISRHandler(UINT32 irqid)
{
	PIPO_ClearInterrupt(PIPO1);
}

/*
 * PIPO2默认中断处理函数
 * 输入参数：
 * 输出参数：
 */
void PIPO2_ISRHandler(UINT32 irqid)
{
	PIPO_ClearInterrupt(PIPO2);
}

/*
 * PIPO3默认中断处理函数
 * 输入参数：
 * 输出参数：
 */
void PIPO3_ISRHandler(UINT32 irqid)
{
	PIPO_ClearInterrupt(PIPO3);
}

/*
 * PIPO4默认中断处理函数
 * 输入参数：
 * 输出参数：
 */
void PIPO4_ISRHandler(UINT32 irqid)
{
	PIPO_ClearInterrupt(PIPO4);
}

/*
 * PIPO5默认中断处理函数
 * 输入参数：
 * 输出参数：
 */
void PIPO5_ISRHandler(UINT32 irqid)
{
	PIPO_ClearInterrupt(PIPO5);
}
