/************************************************************************
 *  文件名: powm.c
 *
 *  描述: 该文件包含电源管理相关操作
 *
 *  版权 (C) : 2008 Hangzhou C-Sky Microsystems Co.,Ltd.
 *  作者: ShuLi Wu
 *  邮箱:   shuli_wu@c-sky.com
 *  日期:   Oct 9 2008
 *  修改记录：
 *  时间                                 修改人                                       修改能容
 *  2018-11-13      朱林                                           修改POWM_ConfigClk函数中当PLL_M<128
 *                                       时的时钟分频处理
 ************************************************************************/
#include "datatype.h"
#include "ckpowm.h"
#include "powm.h"

UINT32 SYS_FREQ;
UINT32 AHB_FREQ;
UINT32 APB_FREQ;

/*
 * 等待cpu进入低功耗模式完成
 * 输入参数：
 * 输出参数：
 */
void Wait_CPUtoLP_INT(void)
{
    while(1)
    {
        if(*((volatile UINT32 *)(&(POWM->INTR))) & POWM_CPUTOLP_INT)
        {
            break;
        }
    }
}

/*
 * 电源管理模块的中断服务程序
 * 输入参数：dwIrqId：中断号
 * 输出参数：
 */
void PowerManage_Interrupt(UINT32 dwIrqId)
{
    POWM->INTR = POWM_CLEARINT;
    POWM->INTM = POWM_INT_NOTMASK;
}

/*
 * @Brief    : 	配置系统频率
 * @Param_I  : 	dwSrcClk, 	外部输入的时钟频率（Hz），应该不小于1MHz的整数值，如1、2、3...(MHz)
 				dwDestSysClk, 目标系统频率（Hz） ,应不大于200000000Hz（200MHz）且不小于62500000Hz
				dwRatioSYSAHB, 	0: CPU : AHB = 1 : 1;
								1: CPU : AHB = 2 : 1;
								2: CPU : AHB = 4 : 1;
								3: CPU : AHB = 8 : 1;
				dwRatioAHBAPB,  0: AHB :APB  = 1 : 1;
								1: AHB :APB  = 2 : 1;
 * @param_O  :
 * @param_IO : 无
 * @NOTE     : 1,如果dwSrcClk、dwDestSysClk不是整MHz的（如:10002000、25500000），那么系统变频将存在误差!
 	 	 	   2,为使系统时钟变化精确：如果dwSrcClk的值小于16M,则dwSrcClk % 1M应为0
 	 	 	   	   	   	   	         如果dwSrcClk的值大于32M,则dwSrcClk % 3M应为0
 	 	 	   	   	   	   	         其它,则dwSrcClk % 2M应为0
 */
void POWM_ConfigClk(UINT32 dwSrcClk, UINT32 dwDestSysClk, UINT32 dwRatioSYSAHB, UINT32 dwRatioAHBAPB)
{
	UINT8 PLL_M = 0;
	UINT8 PLL_M_MIN = 0;
	UINT8 PLL_N = 15;
	UINT8 PLL_N_MIN = 0;
	UINT8 PLL_OD = 0;
	UINT8 PLL_NO = 1; //{1, 2, 4, 8};
	float ratio_MN = 0.0f;//temp
	UINT32 diff_min = dwDestSysClk;
	UINT32 temp = 0;
	UINT32 dwRealCpuClk = 0,dwRealCpuClkTemp = 0;

//		ASSERT(dwSrcClk % 1000000 == 0, NULL, 0);
//		ASSERT(dwDestSysClk <= 100000000, NULL, 0);
//		ASSERT((dwRatioSYSAHB >= 0) && (dwRatioSYSAHB <= 3), NULL, 0);
//		ASSERT((dwRatioAHBAPB >= 0) && (dwRatioAHBAPB <= 1), NULL, 0);

	POWM->INTR = 0;
	POWM->INTM = 0x6;

	POWM->LPCR = 0x1c;
	POWM->FICR = 0xFFFFFF;
	POWM->CGCR = 0x00;

	POWM->FICR = 0xFFFFFF;
	POWM->DCNT = 10000;
	POWM->PLTR = 10000;

	//计算出合适的OD值
    if ((dwDestSysClk >= 125000000) && (dwDestSysClk <= 200000000) )
	{
		PLL_NO = 4;
		PLL_OD = 2;
	}
	else if ((dwDestSysClk >= 62500000) && (dwDestSysClk < 187500000) )
	{
		PLL_NO = 8;
		PLL_OD = 3;
	}
	else
	{
//        ASSERT(0, NULL, 0);//出错
	}

	ratio_MN = ( (float)dwDestSysClk / (float)dwSrcClk * PLL_NO );//计算出MN比例
	ratio_MN /= 2;//由于PLL_M的约束（必须为偶数），还要除以2.

	for (PLL_N = 2; PLL_N <= 15; PLL_N++)
	{
		PLL_M = (UINT8)((float)PLL_N * ratio_MN + 0.5);//计算出M值

		temp = dwSrcClk * 2 * PLL_M / PLL_NO / PLL_N;
		dwRealCpuClkTemp = temp;
		temp = (temp >= dwDestSysClk) ?
			   (temp - dwDestSysClk)  :
			   (dwDestSysClk - temp);

		if ((temp <= diff_min)&&(PLL_M<128))//更新组合
		{
			dwRealCpuClk = dwRealCpuClkTemp;
			diff_min = temp;
			PLL_M_MIN = PLL_M;
			PLL_N_MIN = PLL_N;
			if (diff_min == 0)
			{
				break;
			}
		}
	}//end for (PLL_N = 2; PLL_N <= 15; PLL_N++)

	dwDestSysClk = dwRealCpuClk;
	temp = (PLL_M_MIN);
	temp |= ((UINT32)PLL_N_MIN) << 8;
	temp |= ((UINT32)PLL_OD) << 12;
	POWM->PCR = temp;

	POWM->CRCR = (dwRatioSYSAHB << 1) | (dwRatioAHBAPB);

	POWM->SMCR = 0x01;
	POWM->LPCR |= 0x1C;

	Wait_CPUtoLP_INT();
	POWM->INTR = 0x00;  /* clear the interrupt */

	__asm__("stop");

	POWM->FICR = 0xFFFFFF;

    SYS_FREQ = dwDestSysClk;
    AHB_FREQ = SYS_FREQ >> dwRatioSYSAHB;
    APB_FREQ = AHB_FREQ >> dwRatioAHBAPB;
}

/*
 * 设置POWM为bypass模式，系统时钟直接采用外部输入时钟，不经过PLL模块
 * 输入参数：外部输入时钟频率
 * 输出参数：
 */
void POWM_PLLBypass(UINT32 dwSysFreq)
{
	POWM->PCR  |= (1<<16);
	SYS_FREQ = dwSysFreq;
	AHB_FREQ = dwSysFreq;
	APB_FREQ = dwSysFreq;

	POWM->FICR = 0xFFFFFF;
}

/*
 * 复位指定IP模块
 * 输入参数：dwIPs:IP模块的模块号
 *        如IP_WDT，在ckpowm.h中定义
 * 输出参数：
 */
INT32S POWM_IPSoftwareReset(UINT32 dwIPs)
{
	POWM->RSTEN = 0x01;          /* 允许通过电源管理模块复位IP */
	POWM->RSTCR |= dwIPs;	     /* 复位指定IPs */

	asm("nop"); asm("nop"); asm("nop"); asm("nop"); asm("nop"); asm("nop"); asm("nop");

    POWM->RSTCR &= ~dwIPs;       /* 结束复位 */

	POWM->RSTEN = 0x00;	         /* 不允许通过电源管理模块复位IP */

	return 0;
}

/*
 * 关闭指定IP模块
 * 输入参数：dwIPs:IP模块的模块号
 *        如IP_WDT，在ckpowm.h中定义
 * 输出参数：
 */
INT32S POWM_CutIPsClk(UINT32 dwIPs)
{
	POWM->FICR |= dwIPs;
	POWM->CGCR &= ~dwIPs;
    return 0;
}

/*
 * 配置POWM进入指定的低功耗模式
 * 输入参数：cMode：待进入的低功耗模式
 *               POWM_LP_WAIT----wait模式
 *               POWM_LP_DOZE----doze模式
 *               POWM_LP_STOP----stop模式
 *        dwIP: 表示需要关闭时钟的功能IP模块，共32位,相应的位置1，则表示需要关闭该模块
 *              当进入STOP模式时，此参数不起作用,需要关闭全部的IP模块时钟
 *              bit0: WDT
 *              bit1: TIMER
 *              bit2: GPIO0
 *              bit3: GPIO1
 *              bit4: UART
 *              bit5: SPI
 *              bit6: I2C
 *              bit7: PIPO
 *              bit8: CAN0
 *              bit9: SPIS
 *              bit10:RTC
 *              bit11:UART1
 *              bit12:CAN1
 *              bit13:SPI1
 *              bit16:DMAC
 *              bit17:MAC0
 *              bit18:MAC1
 *              bit19:LCP
 *              bit20:MCP
 * 输出参数：
 * 说明：1.wait模式和doze模式的唤醒可通过外部中断实现，如RTC、GPIO或timer等中断,
 *       当使用外部中断唤醒时，不能关闭相应IP模块的时钟，如使用GPIO0中断唤醒，则dwIP
 *       参数的bit2应该为0，且支持边沿触发和电平触发中断；
 *     2.stop模式可通过外部中断（RTC或GPIO）或者stop模式计数器中断唤醒。
 *       由于CMC暂时不支持RTC隔离模式，所以仅能通过stop模式计数器和GPIO中断唤醒,
 *       而GPIO中断唤醒必须被配置为电平触发中断才能唤醒（最好将gpio_ls_sync配置为
 *       同步到pclk_intr）。
 */
void POWM_ToLowPower(UINT8 cMode, UINT32 dwIP)
{
    switch(cMode)
    {
        case POWM_LP_WAIT:
        	POWM->INTR = 0;                        /* 清中断状态 */
        	POWM->INTM = 0x06;                     /* 屏蔽错误低功耗中断和CPUtoLP中断 */
        	POWM->LPCR = 0x10;                     /* 使能电源管理 */
            POWM->FICR |= dwIP;                    /* 设置此寄存器，在禁用功能IP时钟时不关心相应的模块是否处于空闲状态 */
            POWM->CGCR &= (~dwIP);                 /* 关闭相应的功能IP模块的时钟*/
            POWM->DCNT = 10000;                    /* 设置CPU唤醒延时计数器的值 */
            POWM->LPCR = 0x19;                     /* 使能低功耗，并设置为wait模式 */

        	Wait_CPUtoLP_INT();                    /* 等待无用的外设时钟已被关闭 */
        	POWM->INTR = 0x00;                     /* clear the interrupt */

        	__asm__("wait");

        	POWM->CGCR |= dwIP;                    /* 当退出低功耗后，打开关闭的IP模块的时钟 */
    	    break;
        case POWM_LP_DOZE:
        	POWM->INTR = 0;                        /* 清中断状态 */
        	POWM->INTM = 0x06;                     /* 屏蔽错误低功耗中断和CPUtoLP中断 */
        	POWM->LPCR = 0x10;                     /* 使能电源管理 */
            POWM->FICR |= dwIP;                    /* 设置此寄存器，在禁用功能IP时钟时不关心相应的模块是否处于空闲状态 */
            POWM->CGCR &= (~dwIP);                 /* 关闭相应的功能IP模块的时钟*/
            POWM->DCNT = 10000;                    /* 设置CPU唤醒延时计数器的值 */
            POWM->LPCR = 0x1a;                     /* 使能低功耗，并设置为doze模式 */

        	Wait_CPUtoLP_INT();                    /* 等待无用的外设时钟已被关闭 */
        	POWM->INTR = 0x00;                     /* clear the interrupt */

        	__asm__("doze");

        	POWM->CGCR |= dwIP;                    /* 当退出低功耗后，打开关闭的IP模块的时钟 */
        	break;
        case POWM_LP_STOP:
        	POWM->INTR = 0;                        /* 清中断状态 */
        	POWM->INTM = 0x06;                     /* 屏蔽错误低功耗中断和CPUtoLP中断 */
        	POWM->LPCR = 0x10;                     /* 使能电源管理 */
        	POWM->FICR |= 0xffffff;                /* 设置此寄存器，在禁用功能IP时钟时不关心所有的模块是否处于空闲状态 */
        	POWM->CGCR = 0;                        /* 关闭所有的功能IP模块的时钟*/
        	POWM->WUSR = 0x03;                     /* 清除唤醒源寄存器 */
            POWM->DCNT = 10000;                    /* 设置CPU唤醒延时计数器的值 */
            POWM->PLTR = 10000;                    /* 配置PLL工作稳定的延时时间，此计数值根据晶振确定，10M晶振可用此值 */
            POWM->SMCR = 0x00;                     /* 设置POWM进入STOP，而不是频率转换模式 */
            POWM->LPCR = 0x1c;                     /* 使能低功耗，并设置为stop模式 */

        	Wait_CPUtoLP_INT();                    /* 等待无用的外设时钟已被关闭 */
        	POWM->INTR = 0x00;                     /* clear the interrupt */

        	__asm__("stop");

        	break;
        default:
        	break;
    }
}

/*
 * 使能stop模式计数器，用于唤醒STOP模式
 * 输入参数：dwCount:需要设置的计数值，当此值减为0时，唤醒CPU
 *                不能大于0X7FFFFFFF
 * 输出参数：
 */
void POWM_StopAwake(UINT32 dwCount)
{
    POWM->SCCR = 0x80000000 | dwCount;              /* 使能stop计数器，并设置计数器载入值 */
    POWM->WUSR = 0x01;
}

/*
 * 配置CPU、AHB、APB的频率
 * 输入参数：dwInputClk：外部输入的时钟频率（Hz）
 *                    若使用PLL倍频，则此参数应该是不小于1MHz的整数值，如1、2、3...(MHz)
 *                    若使用bypass功能，则将此参数设置为0,且dwCpuClk输入正确的值
 *        dwCpuClk：目标CPU频率
 *                  若使用PLL倍频，则此参数应不大于100000000Hz（100MHz）且不小于62500000Hz
 *                  若希望设置小于62500000Hz的频率，则只能采用bypass功能，直接使用外部输入时钟作为
 *                  系统时钟，则该值必须等于外部输入时钟的频率
 *        dwRatio:比率值，此值代表CPU、AHB和APB的比率，用来确定AHB和APB的频率
 *                具体为：
 *                0: CPU : AHB = 1 : 1;
 *                   AHB :APB  = 1 : 1;
 *                1: CPU : AHB = 2 : 1;
 *                   AHB :APB  = 1 : 1;
 *                2: CPU : AHB = 1 : 1;
 *                   AHB :APB  = 2 : 1;
 *                3: CPU : AHB = 4 : 1;
 *                   AHB :APB  = 1 : 1;
 *                4: CPU : AHB = 2 : 1;
 *                   AHB :APB  = 2 : 1;
 *                5: CPU : AHB = 8 : 1;
 *                   AHB :APB  = 1 : 1;
 *                6: CPU : AHB = 4 : 1;
 *                   AHB :APB  = 2 : 1;
 *                7: CPU : AHB = 8 : 1;
 *                   AHB :APB  = 2 : 1;
 * 输出参数：
 */
INT32S ConfigClk(UINT32 dwInputClk, UINT32 dwCpuClk, UINT32 dwRatio )
{
	UINT8 cRatioCpuAhb, cRationAhbApb;

	if ( 0 == dwInputClk )
	{
		POWM_PLLBypass( dwCpuClk );
	}
	else
	{
		switch ( dwRatio )
		{
		case 0:
			cRatioCpuAhb = 0;
			cRationAhbApb = 0;
			break;
		case 1:
			cRatioCpuAhb = 1;
			cRationAhbApb = 0;
			break;
		case 2:
			cRatioCpuAhb = 0;
			cRationAhbApb = 1;
			break;
		case 3:
			cRatioCpuAhb = 2;
			cRationAhbApb = 0;
			break;
		case 4:
			cRatioCpuAhb = 1;
			cRationAhbApb = 1;
			break;
		case 5:
			cRatioCpuAhb = 3;
			cRationAhbApb = 0;
			break;
		case 6:
			cRatioCpuAhb = 2;
			cRationAhbApb = 1;
			break;
		case 7:
			cRatioCpuAhb = 3;
			cRationAhbApb = 1;
			break;
		default :
			return -1;
		}

		POWM_ConfigClk(dwInputClk, dwCpuClk, cRatioCpuAhb, cRationAhbApb);
	}

	return 0;
}

/* End of File */
