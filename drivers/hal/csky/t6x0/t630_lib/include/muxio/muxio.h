#ifndef __MUXIO_H__
#define __MUXIO_H__

#include <sysdep.h>

/* muxio IRQ num */
#define MUXIO_IRQ_NUM                   8

/* muxio control */
#define MUXIO_PREFETCH_ON               1
#define MUXIO_PREFETCH_OFF              0
#define MUXIO_DATA_WIDTH_8              0 // 8 bits (only valid in SRAM master mode)
#define MUXIO_DATA_WIDTH_16             1 // 16 bits (valid in all transfer mode)
#define MUXIO_DATA_WIDTH_32             2 // 32 bits (unvalid in ADMUX master mode)

/* muxio fifo slave control*/
#define MUXIO_FS_SYNC_MODE              0
#define MUXIO_FS_ASYNC_MODE             1

/* muxio sram master control */
#define MUXIO_FS_ALE_LATCH_HIGH         0
#define MUXIO_FS_ALE_LATCH_LOW          1

/* muxio INTR status */
#define MUXIO_INTR_FS_CRF_AFULL         (0x1 << 13)
#define MUXIO_INTR_FS_CRF_FULL          (0x1 << 12)
#define MUXIO_INTR_FS_CWF_AEMP          (0x1 << 11)
#define MUXIO_INTR_FS_CWF_EMP           (0x1 << 10)
#define MUXIO_INTR_FS_RF_AFULL          (0x1 << 9)
#define MUXIO_INTR_FS_RF_FULL           (0x1 << 8)
#define MUXIO_INTR_FS_WF_AEMP           (0x1 << 7)
#define MUXIO_INTR_FS_WF_EMP            (0x1 << 6)
#define MUXIO_INTR_FS_READ_TOUT         (0x1 << 5)
#define MUXIO_INTR_FS_WRITE_TOUT        (0x1 << 4)
#define MUXIO_INTR_FM_READ_TOUT         (0x1 << 3)
#define MUXIO_INTR_FM_WRITE_TOUT        (0x1 << 2)
#define MUXIO_INTR_FS_DATA_PKGEND       (0x1 << 1)
#define MUXIO_INTR_FS_CMD_PKGEND        1
#define MUXIO_EXCEPT_INTR               (MUXIO_INTR_FS_CRF_AFULL |\
                                         MUXIO_INTR_FS_CRF_FULL |\
                                         MUXIO_INTR_FS_CWF_AEMP |\
                                         MUXIO_INTR_FS_CWF_EMP |\
                                         MUXIO_INTR_FS_RF_AFULL |\
                                         MUXIO_INTR_FS_RF_FULL |\
                                         MUXIO_INTR_FS_WF_AEMP |\
                                         MUXIO_INTR_FS_WF_EMP |\
                                         MUXIO_INTR_FS_READ_TOUT |\
                                         MUXIO_INTR_FS_WRITE_TOUT |\
                                         MUXIO_INTR_FM_READ_TOUT |\
                                         MUXIO_INTR_FM_WRITE_TOUT |\
                                         MUXIO_INTR_FS_DATA_PKGEND |\
                                         MUXIO_INTR_FS_CMD_PKGEND )

/* muxio fifo slave status */
#define MUXIO_FS_STATUS_CRF_AEMP        (0x1 << 31)
#define MUXIO_FS_STATUS_CRF_EMP         (0x1 << 30)
#define MUXIO_FS_STATUS_CRF_AFULL       (0x1 << 29)
#define MUXIO_FS_STATUS_CRF_FULL        (0x1 << 28)

#define MUXIO_FS_STATUS_CWF_AEMP        (0x1 << 27)
#define MUXIO_FS_STATUS_CWF_EMP         (0x1 << 26)
#define MUXIO_FS_STATUS_CWF_AFULL       (0x1 << 25)
#define MUXIO_FS_STATUS_CWF_FULL        (0x1 << 24)

#define MUXIO_FS_STATUS_RF_AEMP         (0x1 << 23)
#define MUXIO_FS_STATUS_RF_EMP          (0x1 << 22)
#define MUXIO_FS_STATUS_RF_AFULL        (0x1 << 21)
#define MUXIO_FS_STATUS_RF_FULL         (0x1 << 20)

#define MUXIO_FS_STATUS_WF_AEMP         (0x1 << 19)
#define MUXIO_FS_STATUS_WF_EMP          (0x1 << 18)
#define MUXIO_FS_STATUS_WF_AFULL        (0x1 << 17)
#define MUXIO_FS_STATUS_WF_FULL         (0x1 << 16)

/* muxio fifo slave data and cmd fifo depth */
#define MUXIO_FS_FIFO_SLAVE_CMD_DEPTH   0x20 // unit by word
#define MUXIO_FS_FIFO_SLAVE_DATA_DEPTH  0x800 // unit by word

/* muxio admux master control */
#define MUXIO_ADMUX_DATA_MODE           0
#define MUXIO_ADMUX_CMD_MODE            1
#define MUXIO_ADMUX_TIME_SYNC_MODE      0
#define MUXIO_ADMUX_TIME_ASYNC_MODE     1

typedef struct muxio_fifo_slave_config {
    uint32 ahb_pref;
    uint32 data_width;
    uint32 timing_mode;
    uint32 timing_twsample;
    uint32 timing_trdhold;
    uint32 tout_counter;
} muxio_fs_conf_t;

typedef struct muxio_fifo_mster_config {
    uint32 ahb_pref;
    uint32 data_width;
    uint32 tout_counter;
} muxio_fm_conf_t;

typedef struct muxio_sram_config {
    uint32 ahb_pref;
    uint32 data_width;
    uint32 ale_latch;
    uint32 tale;
    uint32 tsamplec;
    uint32 trl;
    uint32 trs;
    uint32 twl;
    uint32 tws;
    uint32 tcs;
} muxio_sram_conf_t;

typedef struct muxio_admux_config {
    uint32 ahb_pref;
    uint32 data_width;
    uint32 op_mode;
    uint32 tim_mode;
} muxio_admux_conf_t;

/* muxio function mode */
typedef enum muxio_mode {
    MUXIO_FIFO_SLAVE_MODE   = 1,
    MUXIO_FIFO_MASTER_MODE  = 2,
    MUXIO_SRAM_MASTER_MODE  = 3,
    MUXIO_ADMUX_MASTER_MODE = 4
} muxio_func_mode_t;

/* muxio tx data mode */
typedef enum muxio_tx_data_mode {
    MUXIO_DATA_ORDER    = 1,
    MUXIO_DATA_RANDOM   = 2
} muxio_tx_data_mode_t;

/* muxio bus mode */
typedef enum muxio_bus_mode {
    MUXIO_CPU_MODE      = 1,
    MUXIO_DMA_MODE      = 2,
    MUXIO_CRYPTO_MODE   = 3,
    MUXIO_USB_MODE      = 4
} muxio_bus_mode_t;

/* muxio intr */
uint32 muxio_intr_status (void);
void muxio_intr_clear (uint32 intr_src);
void muxio_intr_enable (uint32 intr_src);
void muxio_intr_disable (uint32 intr_src);

/* muxio fifo slave function */
uint32 muxio_fs_wfifo_free_entries (void);
uint32 muxio_fs_rfifo_valid_entries (void);
uint32 muxio_fs_cwfifo_free_entries (void);
uint32 muxio_fs_crfifo_valid_entries (void);
uint32 muxio_fs_status_get (void);
void muxio_fs_hw_init (muxio_fs_conf_t config);
uint32 muxio_fs_data_send (uint32 *buf, uint32 nwords);
uint32 muxio_fs_data_recv (uint32 *buf, uint32 nwords);
uint32 muxio_fs_cmd_send (uint32 *buf, uint32 nwords);
uint32 muxio_fs_cmd_recv (uint32 *buf, uint32 nwords);

/* muxio fifo master function*/
bool muxio_fm_exfifo_is_empty (void);
bool muxio_fm_exfifo_is_full (void);
void muxio_fm_hw_init (muxio_fm_conf_t config);
uint32 muxio_fm_send (uint32 *buf, uint32 nwords);
uint32 muxio_fm_recv (uint32 *buf, uint32 nwords);

/* muxio sram master function */
void muxio_sram_hw_init (muxio_sram_conf_t config);

/* muxio admux master function */
void muxio_admux_op_mode_set (uint32 mode);
void muxio_admux_tim_mode_set (uint32 mode);
void muxio_admux_hw_init (muxio_admux_conf_t config);

/* muxio data port */
uint32 muxio_data_port_addr (muxio_func_mode_t fnc_mode, muxio_bus_mode_t bus_mode);
/* muxio cmd port */
uint32 muxio_cmd_port_addr (muxio_bus_mode_t bus_mode);

void set_cmd_muxio_send(uint32 cmd);
void get_cmd_muxio_send(uint32 *cmd);
void set_cmd_muxio_recv(uint32 cmd);
void get_cmd_muxio_recv(uint32 *cmd);
uint8 get_state_muxio_send(void);
void set_state_muxio_send(uint32 state);

#endif
