#ifndef __I2C_H__
#define __I2C_H__
void i2c_init(void);
int32 i2c_send_data(uint8 addr, uint8 type, uint8 nbytes , uint8 *buf);
#endif