#ifndef __DEBUG__H__
#define __DEBUG__H__
#include <sysdep.h>
#include <sram.h>

#define DEBUG_ADDR (RAM_ADDR_BASE + 0x1000)
#define ADDR0 	DEBUG_ADDR	//store received cmd,2048 BYTES for 2048/4=512 cmd
#define ADDR1	(ADDR0 + 0x800)	//store stop count
#define ADDR2 	(ADDR1 + 4)		//start count in muxio_isr
#define ADDR3	(ADDR2 + 4)		//start count in ck803_isr
#define ADDR4	(ADDR3 + 4)		//muxio status0 after start has been sent
#define ADDR5	(ADDR4 + 4)		//muxio status1 after start has been sent
#define ADDR6	(ADDR5 + 4)		//start count in fpga_start
#define ADDR7 	(ADDR6 + 4)		//begin count 
#define ADDR8 	(ADDR7 + 4)		//end count 
#define ADDR9 	(ADDR8 + 4)		//fpga_pause_flag
#endif