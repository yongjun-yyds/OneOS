#ifndef __SRAM_H__
#define __SRAM_H__

#define SRAM_EXT_BASE       0x20000000  // equals AXI_BASE
#define SRAM_EXT_SIZE       0           // External SRAM: max 32MB

#define SRAM_TAB_BASE       0x11100000
#define SRAM_TAB_SIZE       0x2000      // SRAM for table: 8KB

#define SRAM_BUF_BASE   0x800000     // TODO
#define SRAM_BUF_SIZE   0x40000
#define OFFSET          0

/*SRAM*/
#define SRAM_BASE_ADDR                  (0x81fc00)
#define USB_PRD_ADDR					(SRAM_BASE_ADDR + 0x20200)
#define USB_PRD_INDEX_ADDR				(USB_PRD_ADDR + 30*4)
#define DROP_DATA_ADDR                    (USB_PRD_INDEX_ADDR + 4) 

/*DTCM*/
#define RAM_ADDR_BASE                   (0x00406000)	//DTCM address

/* the following macros are used for AHB/AXI addr converting */
#define AXI_TAB_ADDR        0x22200000
#define AXI_BUF_ADDR        0x22300000

#define AXI_ADDR(addr)     (((addr) >= SRAM_BUF_BASE && (addr) < (SRAM_BUF_BASE + SRAM_BUF_SIZE)) ? \
        ((addr) + AXI_BUF_ADDR - (SRAM_BUF_BASE & 0xFFF00000)) : \
        ((addr) >= SRAM_TAB_BASE && (addr) < (SRAM_TAB_BASE + SRAM_TAB_SIZE)) ? \
        ((addr) + AXI_TAB_ADDR - (SRAM_TAB_BASE & 0xFFF00000)) : \
        (addr))
#define AHB_ADDR(addr)     (((addr) >= AXI_BUF_ADDR && (addr) < (AXI_BUF_ADDR + SRAM_BUF_SIZE + OFFSET)) ? \
        ((addr) - AXI_BUF_ADDR + (SRAM_BUF_BASE & 0xFFF00000)) : \
        ((addr) >= AXI_TAB_ADDR && (addr) < (AXI_TAB_ADDR + SRAM_TAB_SIZE)) ? \
        ((addr) - AXI_TAB_ADDR + (SRAM_TAB_BASE & 0xFFF00000)) : \
        (addr))

#endif /* __SRAM_H__ */

