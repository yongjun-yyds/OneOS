#ifndef __SPI_SLAVE_H__
#define __SPI_SLAVE_H__

#include <sysdep.h>

/* SPI IRQ num */
#define SPI_SLAVE_IRQ_NUM            11

/* interrupt event list */
#define SPI_SLAVE_INTR_TFTH          (1 << 0)
#define SPI_SLAVE_INTR_RFTH          (1 << 1)
#define SPI_SLAVE_INTR_TFUR          (1 << 2)
#define SPI_SLAVE_INTR_RFOR          (1 << 3)

#define SPI_SLAVE_TFTH_STS           (1 << 0)
#define SPI_SLAVE_RFTH_STS           (1 << 1)
#define SPI_SLAVE_TFUR_STS           (1 << 2)
#define SPI_SLAVE_RFOR_STS           (1 << 3)

/* spi_config_t params */
/* word_size */
#define SPI_SLAVE_WORD_SIZE_8BIT      0
#define SPI_SLAVE_WORD_SIZE_16BIT     1

/* endian */
#define SPI_SLAVE_ENDIAN_MSB          0
#define SPI_SLAVE_ENDIAN_LSB          1

/* cpol */
#define SPI_SLAVE_CPOL_LOW            0
#define SPI_SLAVE_CPOL_HIGH           1

/* cpha */
#define SPI_SLAVE_CPHA_1EDGE          0
#define SPI_SLAVE_CPHA_2EDGE          1

typedef struct spi_slave_config {
    uint32 word_size;
    uint32 endian;
    uint32 cpol;
    uint32 cpha;
} spi_slave_config_t;

void spi_slave_intr_enable(uint32 intr_src);
void spi_slave_intr_disable(uint32 intr_src);
uint32 spi_slave_intr_status(void);
void spi_slave_intr_clear(uint32 intr_src);
void spi_slave_threshold_set(uint32 rx_threshold, uint32 tx_threshold);
void spi_slave_txd_enable(void);// only use In the multi-slave system, when tx finish,should exe spi_slave_txd_disable()
void spi_slave_txd_disable(void);// only use In the multi-slave system
uint32 spi_slave_rxfifo_valid_entries(void);// the number of entries in the receive FIFO waiting for host processor to read them.
uint32 spi_slave_txfifo_valid_entries(void);// the number of entries in the transmit FIFO waiting to be transmitted
uint32 spi_slave_recv(uint8 *buf, uint32 nbytes);
uint32 spi_slave_send(uint8 *buf, uint32 nbytes);
int32 spi_slave_hw_init(spi_slave_config_t  *config);
#endif

