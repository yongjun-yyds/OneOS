#ifndef __I2C_SLAVE_H__
#define __I2C_SLAVE_H__

#include <sysdep.h>


/* i2c IRQ num */
#define I2C_SLAVE_IRQ_NUM             16

/* interrupt event list */
#define I2C_SLAVE_INTR_STOPI          (1 << 0)
#define I2C_SLAVE_INTR_STARTI         (1 << 1)
#define I2C_SLAVE_INTR_NACKI          (1 << 2)
#define I2C_SLAVE_INTR_TDIEN          (1 << 3)

#define I2C_SLAVE_STOP_STS            (1 << 0)
#define I2C_SLAVE_START_STS           (1 << 1)
#define I2C_SLAVE_NACK_STS            (1 << 2)
#define I2C_SLAVE_TDI_STS             (1 << 3)

#define I2C_SLAVE_READ          0
#define I2C_SLAVE_WRITE         1

/* value can not change */
#define I2C_INT_TD                    (0x1 << 5)
#define I2C_INT_STOP                  (0x1 << 7)
#define I2C_INT_NACK                  (0x1 << 6)
#define I2C_INT_START                 (0x1 << 11)

typedef struct i2c_slave_config {
    uint8 addr;
} i2c_slave_config_t;

void i2c_slave_intr_enable(uint32 intr_src);
void i2c_slave_intr_disable(uint32 intr_src);
uint32 i2c_slave_status_get(void);
void i2c_slave_intr_clear_raw(uint32 intr_src);
uint32 i2c_slave_intr_status(void);
void i2c_slave_intr_clear(uint32 intr_src);

uint32 i2c_slave_rw_get(void);
uint8 i2c_slave_data_read(void);
void i2c_slave_data_write(uint8 data);
int32 i2c_slave_hw_init(i2c_slave_config_t *config);

#endif /* end of __I2C_SLAVE_H__ */

