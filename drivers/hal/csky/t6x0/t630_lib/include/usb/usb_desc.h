#ifndef __USB_DESC_H__
#define __USB_DESC_H__

#include <sysdep.h>

#define MAX_CONFIG_NUM                  1
#define MAX_INTF_NUM                    4

/* descriptor length */
#define DEVICE_LENGTH                   0x12
#define DEVICE_QUALIFIER_LENGTH         0xA
#define BOS_LENGTH                      0x5
#define USB20_EXT_LENGTH                0x7
#define SS_USB_LENGTH                   0xA
#define CONTAINERID_LENGTH              0x14
#define CONFIG_LENGTH                   0x9
#define INTERFACE_LENGTH                0x9
#define INTERFACE_ASSOCIATION_LENGTH    0x8
#define EP_LENGTH                       0x7
#define STANDARD_EP_AUDIO_LENGTH        0x9
#define SS_EP_COMPANION_LENGTH          0x6
#define PIPE_USAGE_LENGTH               0x4

/* Table 9-5. Descriptor Types */
#define DT_DEVICE                       1
#define DT_CONFIGURATION                2
#define DT_STRING                       3
#define DT_INTERFACE                    4
#define DT_ENDPOINT                     5
#define DT_DEVICE_QUALIFIER             6
#define DT_OTHER_SPEED_CONFIGURATION    7
#define DT_INTERFACE_POWER              8
#define DT_INTERFACE_ASSOCIATION        11
#define DT_BOS                          15
#define DT_DEVICE_CAPABILITY            16
#define DT_SS_USB_ENDPOINT_COMPANION    48
#define DT_PIPE_USAGE                   36

/* UVC descriptor types */
#define DT_INTRFC_ASSN_DESCR                   (11)            /* Interface association descriptor type. */
#define DT_CS_INTRFC_DESCR                     (0x24)          /* Class Specific Interface Descriptor type: CS_INTERFACE */


/* Table 9-11 Device Capability Type Codes */
#define DC_WIRELESS_USB                 1
#define DC_USB20_EXT                    2
#define DC_SUPERSPEED_USB               3
#define DC_CONTAINER_ID                 4

/* Endpoint transfer type */
#define TF_TYPE_CONTROL                 0
#define TF_TYPE_ISOCHRONOUS             1
#define TF_TYPE_BULK                    2
#define TF_TYPE_INTERRUPT               3

/* Endpoint or FIFO direction define */
#define USB_DIR_IN                      0x80    // to host
#define USB_DIR_OUT                     0       // to device

/* EP0 define */
#define EP0MAXPACKETSIZE_USB20          0x40
#define EP0MAXPACKETSIZE_USB30          0x9  // 2^9 for ss mode

#define DEV_BCDUSB_3                    0x320 //for cv test ,change from 0x300 to 0x320
#define DEV_BCDUSB_2                    0x210 // 0x201 //0x200

#define DEV_BCDDEVICE                   0x0001
#define DEV_IMANUFACTURER               0x10
#define DEV_IPRODUCT                    0x20
#define DEV_ISERIALNUMBER               0x00
#define DEV_BNUMCONFIGURATIONS          0x01

/* Endpoint number definition */
#define EP0                             0x00
#define EP1                             0x01
#define EP2                             0x02
#define EP3                             0x03
#define EP4                             0x04
#define EP5                             0x05
#define EP6                             0x06
#define EP7                             0x07
#define EP8                             0x08
#define EP9                             0x09
#define EP10                            0x10
#define EP11                            0x11
#define EP12                            0x12
#define EP13                            0x13
#define EP14                            0x14
#define EP15                            0x15

/* bmAttributes definition */
#define D5_REMOTE_WAKEUP                (1 << 5)
#define D6_SELF_POWERED                 (1 << 6)
#define D7_RESERVED                     (1 << 7)

/**
 * Device and/or Interface Class codes
 * as found in bDeviceClass or bInterfaceClass
 * and defined by www.usb.org documents
 */
#define USB_CLASS_PER_INTERFACE         0    // for DeviceClass
#define USB_CLASS_AUDIO                 1
#define USB_CLASS_COMM                  2
#define USB_CLASS_HID                   3
#define USB_CLASS_PHYSICAL              5
#define USB_CLASS_STILL_IMAGE           6
#define USB_CLASS_PRINTER               7
#define USB_CLASS_MASS_STORAGE          8
#define USB_CLASS_HUB                   9
#define USB_CLASS_CDC_DATA              0x0a
#define USB_CLASS_CSCID                 0x0b    // chip+ smart card
#define USB_CLASS_CONTENT_SEC           0x0d    // content security
#define USB_CLASS_VIDEO                 0x0e
#define USB_CLASS_WIRELESS_CONTROLLER   0xe0
#define USB_CLASS_MISC                  0xef
#define USB_CLASS_APP_SPEC              0xfe
#define USB_CLASS_VENDOR_SPEC           0xff

/**
 * for API usb_desc_set()/usb_desc_get()
 */
#define HS_DEV_DESC                     0
#define SS_DEV_DESC                     1
#define HS_CONF_DESC                    2
#define SS_CONF_DESC                    3
#define DEVQUAL_DESC                    4
#define BOS_DESC                        5
#define STR_DESC                        6
#define OTHER_CONF_DESC                 7
#define SS_STR_DESC                     8

/* All standard descriptors have these 2 fields at the beginning */
typedef struct usb_descriptor_header {
    uint8 bLength;
    uint8 bDescriptorType;
} __packed usb_desc_header_t;

typedef struct device_descriptor {
    uint8  bLength;
    uint8  bDescriptorType;
    uint16 bcdUSB;
    uint8  bDeviceClass;
    uint8  bDeviceSubClass;
    uint8  bDeviceProtocol;
    uint8  bMaxPacketSize0;
    uint16 idVendorID;
    uint16 idProductID;
    uint16 bcdDevice;
    uint8  iManufacturer;
    uint8  iProduct;
    uint8  iSerialNum;
    uint8  bNumConfigurations;
} __packed dev_desc_t;

typedef struct configuration_descriptor {
    uint8  bLength;
    uint8  bDescriptorType;
    uint16 wTotalLength;
    uint8  bNumInterfaces;
    uint8  bConfigurationValue;
    uint8  iConfiguration;
    uint8  bmAttributes;
    uint8  bMaxPower;
} __packed conf_desc_t;

typedef struct interface_descriptor {
    uint8  bLength;
    uint8  bDescriptorType;
    uint8  bInterfaceNumber;
    uint8  bAlterSetting;
    uint8  bNumEndpoints;
    uint8  bInterfaceClass;
    uint8  bInterfaceSubClass;
    uint8  bInterfaceProtocol;
    uint8  iInterface;
} __packed intf_desc_t;

typedef struct endpoint_descriptor {
    uint8  bLength;
    uint8  bDescriptorType;
    uint8  bEndpointAddress;
    uint8  bmAttributes;
    uint16 wMaxPacketSize;
    uint8  bInterval;
} __packed ep_desc_t;

typedef struct ss_endpoint_companion {
    uint8  bLength;
    uint8  bDescriptorType;
    uint8  bMaxBurst;
    uint8  bmAttributes;
    uint16 wBytesPerInterval;
} __packed ss_ep_desc_t;

typedef struct interface_association {
    uint8  bLength;
    uint8  bDescriptorType;
    uint8  bFirstInterface;
    uint8  bInterfaceCount;
    uint8  bFunctionClass;
    uint8  bFunctionSubClass;
    uint8  bFunctionProtocol;
    uint8  iFunction;
} __packed intf_asc_desc_t;

typedef struct pipe_usage_descriptor {
    uint8  bLength;
    uint8  bDescriptorType;
    uint8  bPipeID;
    uint8  bReserved;
} __packed pu_desc_t;

typedef struct bos_descriptor {
    uint8  bLength;
    uint8  bDescriptorType;
    uint16 wTotalLength;
    uint8  bNumDeviceCaps;
} __packed bos_desc_t;

typedef struct usb20_extension {
    uint8  bLength;
    uint8  bDescriptorType;
    uint8  bDevCapabilityType;
    uint32 bmAttributes;
} __packed u2_ext_desc_t;

typedef struct ssusb_device_capability {
    uint8  bLength;
    uint8  bDescriptorType;
    uint8  bDevCapabilityType;
    uint8  bmAttributes;
    uint16 wSpeedsSupported;
    uint8  bFunctioalitySupport;
    uint8  bU1DevExitLat;
    uint16 bU2DevExitLat;
} __packed ss_devcap_desc_t;

typedef struct container_id {
    uint8  bLength;
    uint8  bDescriptorType;
    uint8  bDevCapabilityType;
    uint8  bReserved;
    uint8  ContainerID[16];
} __packed container_id_desc_t;

typedef struct dev_qual_desc {
    uint8  bLength;
    uint8  bDescriptorType;
    uint16 bcdUSB;
    uint8  bDeviceClass;
    uint8  bDeviceSubClass;
    uint8  bDeviceProtocol;
    uint8  bMaxPacketSize0;
    uint8  bNumConfigurations;
    uint8  bReserved;
} __packed dev_qual_desc_t;

int32 usb_desc_set (uint32 type, uint32 id, uint8 *desc);
int32 usb_desc_get (uint32 type, uint32 id, uint8 **desc);

#endif
