#ifndef __USB_REQUEST_H__
#define __USB_REQUEST_H__

#include <sysdep.h>

/**
 * USB types, the second of three bRequestType fields
 */
#define USB_TYPE_STANDARD           0x00
#define USB_TYPE_CLASS              0x01
#define USB_TYPE_VENDOR             0x02
#define USB_TYPE_RESERVED           0x03

/**
 * USB recipients, the third of three bRequestType fields
 */
#define USB_RECIP_MASK              0x1f
#define USB_RECIP_DEVICE            0x00
#define USB_RECIP_INTERFACE         0x01
#define USB_RECIP_ENDPOINT          0x02
#define USB_RECIP_OTHER             0x03

typedef enum {
    GET_STATUS = 0,
    CLEAR_FEATURE,
    SET_FEATURE = 3,
    SET_ADDRESS = 5,
    GET_DESCRIPTOR,
    SET_DESCRIPTOR,
    GET_CONFIGURATION,
    SET_CONFIGURATION,
    GET_INTERFACE,
    SET_INTERFACE,
    SYNCH_FRAME,
    SET_SEL = 48,
    SET_ISOCH_DELAY = 49
} std_req_code;

typedef enum {
    GET_REPORT = 0x1,
    GET_IDLE = 0x2,
    GET_PROTOCOL = 0x3,
    SET_REPORT = 0x9,
    SET_IDLE = 0xA,
    SET_PROTOCOL = 0xB,
    MS_RESET = 0xFF,
    GET_MAX_LUN = 0xFE
} class_req_code;

typedef enum {
    ENDPOINT_HALT = 0,
    FUNCTION_SUSPEND = 0,
    DEVICE_REMOTE_WAKEUP,
    TEST_MODE,
    U1_ENABLE = 48,
    U2_ENABLE,
    LTM_ENABLE
} std_feature_selectors;

typedef enum {
    TEST_J = 1,
    TEST_K,
    TEST_SE0_NAK,
    TEST_PACKET,
    TEST_FORCE_ENABLE,
} tst_mode_selectors;

typedef enum {
    NORMAL_OPERATION_STATE = 0,
    FUNCTION_REMOTE_WAKE_DISABLE = 0,
    LOW_POWER_SUSPEND_STATE = 1,
    FUNCTION_REMOTE_WAKE_ENABLE = 2
} suspend_option;

typedef struct setup_packet {
    uint8  bmRequestType;    // D[7]: direction (IN/OUT); D[6:5]: Type; D[4:0]: Recipient
    uint8  bRequest;         // Refer to Table 9-3
    uint16 wValue;
    uint16 wIndex;
    uint16 wLength;
} __packed setup_pkt_t;

typedef struct udev_status {
    uint8 u1en;
    uint8 u2en;
    uint8 ltmen;
    uint8 frwen;
    uint8 rmwkup;
} __packed udev_status_t;

extern udev_status_t status;
extern uint32 set_uac_flag;
void  udev_reset (void);
int32 get_status (setup_pkt_t *setup_pkt);
int32 clear_feature (setup_pkt_t *setup_pkt);
int32 set_feature (setup_pkt_t *setup_pkt);
int32 set_address (setup_pkt_t *setup_pkt);
int32 get_descriptor (setup_pkt_t *setup_pkt);
int32 set_descriptor (setup_pkt_t *setup_pkt);
int32 get_configuration (setup_pkt_t *setup_pkt);
int32 set_configuration (setup_pkt_t *setup_pkt);
int32 get_interface (setup_pkt_t *setup_pkt);
int32 set_interface (setup_pkt_t *setup_pkt);
int32 sync_frame (setup_pkt_t *setup_pkt);
int32 set_sel (setup_pkt_t *setup_pkt);
int32 standard_request (setup_pkt_t *setup_pkt);

#endif
