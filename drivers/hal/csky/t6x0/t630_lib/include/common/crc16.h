/**
 *  crc16.h
 *
 * Implements the standard CRC ITU-T V.41:
 *   Width 16
 *   Poly  0x1021 (x^16 + x^12 + x^15 + 1)
 *   Init  0
 *
 * This source code is licensed under the GNU General Public License,
 * Version 2. See the file COPYING for more details.
 */

#ifndef __CRC16_H__
#define __CRC16_H__

#include <sysdep.h>
/**
 * For CRC16, need table size using RO data section about 256*2 = 512B
 *
 * CRC-itu-t/CRC16 is a standard used by XMODEM/ZMODEM communication
 *
 * For checking, we can generate CRC16 like XMODEM software mode
 *
 * We suggest that:
 *     seed = 0;
 *     crc16 = crc16_itu_t(seed, buffer, length);
 *     return crc32;
 */
uint16 crc16_itu_t (uint16 seed, const uint8 *buffer, uint32 len);
#define crc16(seed, pbuf, length) crc16_itu_t(seed, (const uint8 *)pbuf, length)

#endif /* __CRC16_H__ */

