#ifndef __LOG_H__
#define __LOG_H__

#include <sysdep.h>
#include <stdlib.h>
#include <string.h>


int32 log_init(volatile uint32 *t_stamp);
int32 log_st_init(uint32 *st_word, uint32 cnt, uint8 *init_buf); 
int32 log_write(uint8 *buf, uint32 nbytes);
int32 log_read(uint8 *buf, int nbytes);
int32 log_error_id(uint32 err_id);
int32 log_st_data(void);
int32 log_nonvolatile(const char *fmt, ...);
uint32 log_flag_addr();
#endif
