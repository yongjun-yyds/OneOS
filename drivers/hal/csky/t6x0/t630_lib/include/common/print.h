#ifndef __PRINT_H__
#define __PRINT_H__

#include <stdio.h>
#include <config.h>
#include <log.h>


#define HIGH_LEVEL   1

/* print */
#define print(level, ...) \
    if (level >= HIGH_LEVEL) { \
        printf(__VA_ARGS__);   \
    }     

#define nvlog(...) log_nonvolatile(__VA_ARGS__)
/* assert */
#ifndef assert
#ifdef CONFIG_ASSERT 
#define assert(expr) \
    if (!(expr)) { \
        printf("[ASSERT]: File:%s, Function:%s, at Line%d. \n", __FILE__, __func__, __LINE__); \
        while (1); \
    } 
#else
#define assert(expr)   ((void)0)
#endif /* end CONFIG_DEBUG */
#endif /* end assert */

/* error number */
#define ERROR(err_id)   log_error_id(err_id)
    
#endif /* __PRINT_H__ */
