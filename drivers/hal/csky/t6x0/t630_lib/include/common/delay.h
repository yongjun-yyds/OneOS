#ifndef __DELAY_H__
#define __DELAY_H__

#include <sysdep.h>

/* functions */
int32 delay_init (void);
void delay (uint32 s);
void mdelay (uint32 ms);
void udelay (uint32 us);

#endif /* __DELAY_H__ */
