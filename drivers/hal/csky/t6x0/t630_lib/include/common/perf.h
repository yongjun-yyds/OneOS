#ifndef __PERF_H__
#define __PERF_H__

#include <sysdep.h>

enum {
    UNIT_BYTE = 0,
    UNIT_WORD,
    UNIT_SECTOR,
};

void perf_start (uint32 unit, uint32 s);
bool perf_is_done (void);
void perf_statistic (uint32 submit, uint32 cmplt);
void perf_result (float s);

#endif

