#ifndef __MP_H__
#define __MP_H__

#include <sysdep.h>

#define MP_FW_VER       1
#define MP_USB_INFO     2
#define MP_SATA_INFO    3
#define MP_EMMC_INFO    4
#define MP_CHIP_ID      5
#define MP_EMMC_INFO_EN 0x55AA

/* informations written in Mass Production */
typedef struct mp_fw_ver {
    uint8  version[8];              // Firmware version
} mp_fw_ver_t;

typedef struct mp_usb_info {
    uint32 vid;
    uint32 pid;
    uint8  p_desc[34];              // Product ID string descriptor
    uint8  v_desc[34];              // Vendor ID string descriptor
    uint8  sn_desc[50];             // SN string descriptor
} mp_usb_info_t;

typedef struct mp_sata_info {
    uint8  model_number[40];        // model number (device name)
    uint8  serial_number[20];       // serial number
} mp_sata_info_t;

typedef struct emmc_bus{
    uint8 type;
    uint8 clk_div;
    uint8 clk_270_dll[2];
    uint8 ds_dll[2];
    uint8 iodrv_clk[2]; 
    uint8 iodrv_cmd[2]; 
    uint8 iodrv_dat[2];
} emmc_bus_t;
typedef struct mp_emmc_info {
    uint16 en;
    int16  switch_temp;
    emmc_bus_t condition[2];
} mp_emmc_info_t;
typedef struct mp_chipid {
    uint64 chipid;                  // chipid
} mp_chipid_t;
const void *mp_info_get (uint32 what);

#endif

