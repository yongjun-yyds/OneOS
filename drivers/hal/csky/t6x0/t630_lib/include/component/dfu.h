#ifndef __DFU_H__
#define __DFU_H__

#include <sysdep.h>

/* Device Firmware Upgrade */
int32 dfu_init (uint32 fw_addr, uint32 fw_nbytes);
int32 dfu_update (uint8 *buf, uint32 nbytes);
int32 dfu_final (void);

#endif

