#ifndef __SETUP_H__
#define __SETUP_H__

#include <sysdep.h>

#define FW_UPD_TYPE_CK        0x01
#define FW_UPD_TYPE_MUXIO     0x02
/* struct/enum */
/* setup.bin define */
typedef struct fw_linfo {    //bootrom load image info
    uint32 fw_header;
    uint32 fw_ver;
    uint32 fw_lbase;
    uint32 fw_lsize;
} fw_linfo_t;

typedef struct reg_group {   //config use like reg_addr = reg_value
    uint32 reg_addr;
    uint32 reg_value;
} reg_group_t;

typedef struct usb_info {
    uint32 vid;
    uint32 pid;
    /* format: unicode */
    uint8  p_desc[34];          //Product ID string descriptor 
    uint8  v_desc[34];          //Vendor ID string descriptor 
    uint8  sn_desc[50];         //SN string descriptor
} usb_info_t;

typedef struct sata_info {
    /* format: ASCII */
    //uint8  fw_version[8];     //Firmware version: mv to tsetup_t
    uint8  model_number[40] ;   //model number (device name)
    uint8  serial_number[20] ;  //serial number 
} sata_info_t;

typedef union dev_info {
    usb_info_t  usb;
    sata_info_t sata;
} dev_info_t;

typedef struct emmc_bus_param{
    uint8 type;
    uint8 clk_div;
    uint8 clk_270_dll[2];
    uint8 ds_dll[2];
    uint8 iodrv_clk[2]; 
    uint8 iodrv_cmd[2]; 
    uint8 iodrv_dat[2];
} emmc_bus_param_t;

typedef struct emmc_info {
    uint16 en;
    int16  switch_temp;
    emmc_bus_param_t condition[2];
} emmc_info_t;
typedef struct tih_setup {      //bootrom setup.bin file
#define BOOT_FW_HEADER       0x00484954  //TIH, also see tih610.inc
    /* fields for bootrom */
    uint32 setup_header;        //'TIH'
    uint32 setup_size;          //variable size   
    uint32 setup_crc32;
    uint32 image_size;
    uint32 image_crc32;
    uint32 update_addr;
    uint32 update_online;       //1: enable    0: disable
    uint32 biboot_size;         //align by 4KB
    uint32 biboot_crc32;
    uint32 biboot_sup;          //1: support   0: non-support
    uint32 fuse;                //1: enable    1: disable
    reg_group_t regs[20];       //20 sets

    /* fields for firmware */
#define DEV_TYPE_USB         0
#define DEV_TYPE_SATA        1              
    uint8  fw_version[8];       //Firmware version
    uint32 alien_act;           //0: default   1: alien act
    uint32 dev_type;            //USB or SATA                       
    dev_info_t dev_info;
    emmc_info_t emmc_info;
    //other, such as crypto    
    uint64 chip_id;
    uint32 image2_size;         // for dual-core second fw
    uint32 image2_crc32;        // for dual-core second fw
    uint32 update_image2_addr;  // for dual-core second fw
	uint32 update_image2_online; // for dual-core second fw
} tsetup_t;

#endif /* __SETUP_H__ */
