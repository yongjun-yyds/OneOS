#include <sysdep.h>
#include <print.h>
#include <i2c.h>
#include <delay.h>
#define ADDR_SLAVE 0x01
#define SET_SPEED 2000000//3400000
#define PRINT_I2C_TEST 0
void i2c_init(void)
{
	i2c_config_t config;	
	config.speed = SET_SPEED;
	i2c_hw_init(&config);	
}
int32 i2c_send_data(uint8 addr, uint8 type, uint8 nbytes , uint8 *buf)
{
    uint32 ret;
	uint8 i;
	
    assert(nbytes > 0);
	print(PRINT_I2C_TEST, "0.send addr addr: %d t:%d l:%d b:%02x \n",addr,type,nbytes,buf[0]);
    if (i2c_is_busy()) {
        print(PRINT_I2C_TEST, "I2C bus busy");
        return -1;
    }

    /* 1. write address */
    addr = (addr << 1) & 0xFE;//write
    ret = i2c_send((char)addr, 1, 0); // set start signal and address data + write
	print(PRINT_I2C_TEST, "1.send addr ret: %d \n",ret);
    if (ret) {
        return -1;
    }
	
    /* 2. write type  and lenth */
	
	ret = i2c_send(type, 0, 0);
	print(PRINT_I2C_TEST, "2.send type ret: %d \n",ret);
    if (ret) {
        return -1;
    }
	
	ret = i2c_send(nbytes, 0, 0);
	print(PRINT_I2C_TEST, "3.send nbytes ret: %d \n",ret);
    if (ret) {
        return -1;
    }

    /* 3. write the first (nbytes-1) bytes */
    for (i=0; i<(nbytes - 1); i++) {
        ret = i2c_send((char)buf[i], 0, 0);
		print(PRINT_I2C_TEST, "4.send data [%d] ret: %d \n", i, ret);
        if (ret) {
            return -1;
        }
    }
    ret = i2c_send((char)buf[i], 0, 1);
		print(PRINT_I2C_TEST, "4.send data + stop [%d] ret: %d \n", i, ret);
    if (ret) {
        return -1;
    }
    /* wait for slave write complete 
    mdelay((nbytes / 3) + 1);*/// 3 bytes need 1ms  to complet write

    return nbytes;
}