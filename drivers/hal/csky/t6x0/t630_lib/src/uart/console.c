#include <sysdep.h>
#include <sysdep.h>
#include <uart.h>
#include <vendor_cmd.h>
#include <stdio.h>
#include <wdt.h>
#include <interrupt.h>
#include <debug.h>
extern void print_log(uint32 flag);
void __fast __isr console_isr(void)
{
    uint8 ch;
    uint32 status;
    status = uart_intr_status(UART0);
    uart_intr_clear(UART0, status);
    intr_clear(UART0_IRQ_NUM);
	
    switch (status & 0xf) {
        case UART_INTR_RX_ERROR:
            if (status & UART_ERR_OVERRUN) {
                // do TODO
            }

            if (status & UART_ERR_FRAMING) {
                // do TODO
            }

            if (status & UART_ERR_PARITY) {
                // do TODO
            }
            break;
		case UART_INTR_RX_READY:
			while (uart_rx_ready(UART0)) {
				ch = uart_ndelay_getc(UART0);
				if(ch == 0x55){
					printf("chip_reset(TARGET_BOOTROM)\n");
					chip_reset(TARGET_BOOTROM);
					while(1);
				} else if(ch == 0x56) {						
					print_log(1);
				} else if(ch == 0x57){
					print_log(2);
				}
			}
            break;
        case UART_INTR_TX_EMPTY:
            /* TX new data into THR */

            break;
        default:
            break;
    }
}
void console_init(void)
{
    uart_config_t config;
    config.baudrate  = UART_BAUDRATE_115200;
    config.parity    = UART_PARITY_NO;
    config.stop_bits = UART_STOP_BIT_EQ_1;
    config.word_size = UART_WORD_SIZE_8BIT;

    uart_hw_init(UART_CONSOLE, &config);
	uart_intr_enable(UART0, UART_INTR_RX_READY);
    //uart_intr_enable(UART0, UART_INTR_TX_EMPTY);
    uart_intr_enable(UART0, UART_INTR_RX_ERROR);
    intr_connect(UART0_IRQ_NUM, UART0_IRQ_NUM, console_isr);
    intr_enable(UART0_IRQ_NUM);		
}