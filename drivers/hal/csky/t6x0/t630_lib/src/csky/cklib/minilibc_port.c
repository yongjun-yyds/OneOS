/*
 * Copyright (C) 2017 C-SKY Microsystems Co., Ltd. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/******************************************************************************
 * @file     minilibc_port.c
 * @brief    minilibc port
 * @version  V1.0
 * @date     26. Dec 2017
 ******************************************************************************/
#include <sysdep.h>
#include <stdio.h>
#include <uart.h>

#define BACKSP_KEY       0x08
#define RETURN_KEY       0x0D
#define DELETE_KEY       0x7F
#define CONSOLE_BUF_SIZE 128

struct __console_fifo {
    uint32 in;
    uint32 out;
    uint8  buf[CONSOLE_BUF_SIZE];
};

struct __console_fifo cfifo = {0};

int fputc (int ch, FILE *stream)
{
    if (ch == '\n') {
        console_putc(UART_CONSOLE, '\r');
    }

    console_putc(UART_CONSOLE, (uint8)ch);

    return 0;
}

int fgetc (FILE *stream)
{
    uint8 data;

    if (cfifo.in == cfifo.out) { // empty
        cfifo.in = 0;
        cfifo.out = 0;

        do {
            data = console_getc(UART_CONSOLE);

            switch (data) {
                case RETURN_KEY:
                    if (cfifo.in < CONSOLE_BUF_SIZE) {
                        cfifo.buf[cfifo.in++] = '\n'; // \n or \0
                        console_putc(UART_CONSOLE, '\n');
                    }

                    break;

                case BACKSP_KEY:
                case DELETE_KEY:
                    if (cfifo.in) {
                        cfifo.in--;
                        cfifo.buf[cfifo.in] = '\0';
                        console_putc(UART_CONSOLE, '\b');
                        console_putc(UART_CONSOLE, ' ');  // for echo backspace
                        console_putc(UART_CONSOLE, '\b');
                    }

                    break;

                default:
                    if (data > 0x1F && data < 0x7F && cfifo.in < CONSOLE_BUF_SIZE) {
                        cfifo.buf[cfifo.in++] = data;
                        console_putc(UART_CONSOLE, data);
                    }

                    break;
            }
        } while (data != RETURN_KEY);
    }

    data = cfifo.buf[cfifo.out++];
    return data;
}

int __fast os_critical_enter (unsigned int *lock)
{
    return 0;
}

int __fast os_critical_exit (unsigned int *lock)
{
    return 0;
}
