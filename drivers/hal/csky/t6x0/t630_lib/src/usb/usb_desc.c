#include <sysdep.h>
#include <print.h>
#include <usb_desc.h>

#define PRINT_USB_DESC      0

static dev_desc_t *dev_desc;
static dev_desc_t *ss_dev_desc;
static conf_desc_t *conf_desc;
static conf_desc_t *ss_conf_desc;
static bos_desc_t *bos_desc;
static uint8 *str_desc[12];
static dev_qual_desc_t *dev_qual_desc;
static conf_desc_t *other_conf_desc;

/* register the user-defined descriptors to usb system */
int32 usb_desc_set (uint32 type, uint32 id, uint8 *desc)
{
    int32 ret = 0;

    switch (type) {
        case HS_DEV_DESC:
            dev_desc = (dev_desc_t *)desc;
            break;

        case SS_DEV_DESC:
            ss_dev_desc = (dev_desc_t *)desc;
            break;

        case HS_CONF_DESC:
            conf_desc = (conf_desc_t *)desc;
            break;

        case SS_CONF_DESC:
            ss_conf_desc = (conf_desc_t *)desc;
            break;

        case DEVQUAL_DESC:
            dev_qual_desc = (dev_qual_desc_t *)desc;
            break;

        case BOS_DESC:
            bos_desc = (bos_desc_t *)desc;
            break;

        case STR_DESC:
            if (id > 12) 
			{
                print (PRINT_USB_DESC, "out of range\n");
                ret = -1;
            }

            str_desc[id] = desc;
            break;

        case OTHER_CONF_DESC:
            other_conf_desc = (conf_desc_t *)desc;
            break;

        default:
            ret = -1;
    }

    return ret;
}

/* get the registered descriptors */
int32 usb_desc_get (uint32 type, uint32 id, uint8 **desc)
{
    int32 ret = 0;

    switch (type) {
        case HS_DEV_DESC:
            *desc = (uint8 *)dev_desc;
            break;

        case SS_DEV_DESC:
            *desc = (uint8 *)ss_dev_desc;
            break;

        case HS_CONF_DESC:
            *desc = (uint8 *)conf_desc;
            break;

        case SS_CONF_DESC:
            *desc = (uint8 *)ss_conf_desc;
            break;

        case DEVQUAL_DESC:
            *desc = (uint8 *)dev_qual_desc;
            break;

        case BOS_DESC:
            *desc = (uint8 *)bos_desc;
            break;

        case STR_DESC:
            if (id >12) {
                print (PRINT_USB_DESC, "out of range\n");
                ret = -1;
            }

            *desc = str_desc[id];
            break;

        case OTHER_CONF_DESC:
            *desc = (uint8 *)other_conf_desc;
            break;

        default:
            ret = -1;
    }

    return ret;
}
