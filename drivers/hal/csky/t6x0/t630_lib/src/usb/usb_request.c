#include <sysdep.h>
#include <config.h>
#include <print.h>
#include <usb.h>
#include <wdt.h>
#include <usb_desc.h>
#include <usb_request.h>
#define PRINT_USB_REQ      0

static uint8 altsetting[MAX_INTF_NUM];
static uint8 bUsbEP0HaltSt = 0;
static uint32 usb_config_flag = 0;
#if ((defined CONFIG_USB_SATA) || (defined CONFIG_USB_EMMC) || (defined CONFIG_USB_CRYPTO))
extern uint32 log_link_err[4];
#endif

udev_status_t status = {
    .u1en = 0,
    .u2en = 0,
    .ltmen = 0, // ltm = 2 while receiving clear_feature(LTM_ENABLE) and not yet to disable LTM_EN
    .frwen = 0, // function remote wakeup (for SS)
    .rmwkup = 0 // remote wake-up (for HS/FS)
};

uint32 set_uac_flag = 0;
uint32 uac_restart_flag = 0;
uint8 usb_clear_feature_flag;

void udev_reset(void)
{
    status.u1en = status.u2en = status.ltmen = 0;
    status.frwen = 0;
    memset(altsetting, 0, MAX_INTF_NUM);
}

static int32 udev_intf_ep_config(conf_desc_t *cfg, uint32 infnum, uint32 infalt)
{
    uint32 ep_num, cfg_ep_num = 0;
    uint32 i = 0, n = 0;
    uint8 ep, ep_type;
    uint8 direction;
    uint8 fifo_entry_num;
    uint8 ep_fifo_entry;
    uint8 start_entry = 0;
    uint8 bw_num;
    uint16 mps;
    uint8 total_entry;
    uint32 offset = 0;
    uint32 flag = 0;
    uint32 intf_ep_max[MAX_INTF_NUM] = {0};
    uint32 interval;
    uint32 max_interval=0;

    int32 other_inf_mum = -1;
    uint32 other_inf_alt_flag = 0;
    uint32 inf_entry_num[MAX_INTF_NUM] = {0};
    uint32 inf_alt_entry_num[MAX_INTF_NUM] = {0};

    uint8 *buffer = NULL;
    usb_desc_header_t *header;
    intf_desc_t *inf_desc = NULL;
    ep_desc_t *ep_desc = NULL;
    ss_ep_desc_t *ss_ep_cmp_desc = NULL;

    uint32 spd_mode = usb_spd_get();

    if (cfg->bLength != CONFIG_LENGTH ||
        cfg->bDescriptorType != DT_CONFIGURATION) {
        return -1;
    }

    offset += cfg->bLength;
    buffer = (uint8 *)cfg;
    buffer += cfg->bLength;
	print(PRINT_USB_REQ, "udev_intf_ep_config intf=%d ,alt=%d\n",infnum,infalt);
	uint32 k = 0;
	if(infnum == 3 && infalt == 1){
		print(PRINT_USB_REQ,"start uac.\n");
		if(set_uac_flag) {
			uac_restart_flag = 1;
		}
		set_uac_flag = 1;
	}else if(infnum == 3 && infalt == 0){
		if(set_uac_flag) {
			uac_restart_flag = 1;
		}
		set_uac_flag = 0;
		print(PRINT_USB_REQ,"stop uac.\n");
	}
    while (offset < cfg->wTotalLength) {
        header = (struct usb_descriptor_header *)buffer;
        if (header->bLength != INTERFACE_LENGTH ||
            header->bDescriptorType != DT_INTERFACE) {
            offset += header->bLength;
            buffer += header->bLength;
            continue;
        }

        inf_desc = (struct interface_descriptor *)buffer;
        buffer += inf_desc->bLength;
        offset += inf_desc->bLength;

        if (intf_ep_max[inf_desc->bInterfaceNumber] < inf_desc->bNumEndpoints) {
            cfg_ep_num += (inf_desc->bNumEndpoints - intf_ep_max[inf_desc->bInterfaceNumber]);
            intf_ep_max[inf_desc->bInterfaceNumber] = inf_desc->bNumEndpoints;
        }
    }

    total_entry = usb_total_entry_get();
    fifo_entry_num = total_entry / cfg_ep_num;

    offset = 0;
    offset += cfg->bLength;
    buffer = (uint8 *)cfg;
    buffer += cfg->bLength;

    while (offset < cfg->wTotalLength) {
        header = (struct usb_descriptor_header *)buffer;
        if (header->bLength != INTERFACE_LENGTH ||
            header->bDescriptorType != DT_INTERFACE) {
            offset += header->bLength;
            buffer += header->bLength;

            /* SS mode: get bMaxBurst */
            if (spd_mode == SS_MODE && other_inf_mum > -1) {
                if (header->bLength == SS_EP_COMPANION_LENGTH) {
                    if (other_inf_alt_flag == 0) {
                        inf_entry_num[other_inf_mum] = inf_entry_num[other_inf_mum] + ((uint8 *)header)[2] + 1;
                    } else {
                        inf_alt_entry_num[other_inf_mum] = inf_alt_entry_num[other_inf_mum] + ((uint8 *)header)[2] + 1;
                    }
                }
            }
            continue;
        }
        inf_desc = (struct interface_descriptor *)buffer;
        buffer += inf_desc->bLength;
        offset += inf_desc->bLength;

        other_inf_mum = -1;
        other_inf_alt_flag = 0;

        if (inf_desc->bInterfaceNumber != infnum ||
            inf_desc->bAlterSetting != infalt) {
            /* SS mode: get bInterfaceNumber / bAlterSetting */
            if (spd_mode == SS_MODE && inf_desc->bInterfaceNumber != infnum) {
                other_inf_mum = inf_desc->bInterfaceNumber;
                if (inf_desc->bAlterSetting == 1) {
                    other_inf_alt_flag = 1;
                }
            }
            continue;
        }

        ep_num = inf_desc->bNumEndpoints;
        if (ep_num > MAX_EP_NUM) {
            return -1;
        }

        if (ep_num == 0) {
            return 0;
        }

        max_interval=0;
        for (i = 1; i <= ep_num; i++) {
            header = (struct usb_descriptor_header *)buffer;
			//if (header->bLength != EP_LENGTH ||
            if ((header->bLength != EP_LENGTH && header->bLength != STANDARD_EP_AUDIO_LENGTH) ||
                header->bDescriptorType != DT_ENDPOINT) //find endpoint descriptor
			{
				print(PRINT_USB_REQ, "bLength1 = %d,bDescriptorType=%d\n",header->bLength,header->bDescriptorType);
                buffer += header->bLength;
                offset += header->bLength;
                i--;
                continue;
            }
			if(header->bLength == EP_LENGTH)
            ep_desc = (struct endpoint_descriptor *)buffer;
			else
				ep_desc = (struct stand_as_iso_ep_desc *)buffer;
            buffer += ep_desc->bLength;
            offset += ep_desc->bLength;

            if (spd_mode == SS_MODE) {
                header = (struct usb_descriptor_header *)buffer;
                if (header->bLength != SS_EP_COMPANION_LENGTH ||
                    header->bDescriptorType != DT_SS_USB_ENDPOINT_COMPANION) {
                    buffer += header->bLength;
                    offset += header->bLength;
                    i--;
                    continue;
                }
                ss_ep_cmp_desc = (struct ss_endpoint_companion *)buffer;
                buffer += ss_ep_cmp_desc->bLength;
                offset += ss_ep_cmp_desc->bLength;
            }

            /* ep num */
            ep = ep_desc->bEndpointAddress & 0xF;

            bw_num = ((ep_desc->wMaxPacketSize & 0x1800) >> 11) + 1;

            /* reset ep */
            usb_ep_reset(ep);

             /* transfer type */
            ep_type = ep_desc->bmAttributes & 0x3;
            usb_ep_type_set(ep, ep_type);

            /* direction */
            direction = ep_desc->bEndpointAddress >> 7 & 0x1;
            usb_ep_dir_set(ep, direction);

            /* service interval */
            if (spd_mode == SS_MODE) {
                if ((ep_type == TF_TYPE_INTERRUPT) || (ep_type == TF_TYPE_ISOCHRONOUS)) {
                    interval = ep_desc->bInterval;
                    if (max_interval < interval)
                        max_interval = interval;

                    if (ep_type == TF_TYPE_ISOCHRONOUS) {
                        bw_num = (ss_ep_cmp_desc->bMaxBurst + 1) * (ss_ep_cmp_desc->bmAttributes + 1);

                        if (direction == 1){
                            
                            if(bw_num > 48){
                                bw_num = 48;
                            }                   
                            usb_iso_in_pktnum_set(ep, bw_num);
                        }
                    }
                }
            } else {
                bw_num = ((ep_desc->wMaxPacketSize & 0x1800) >> 11) + 1;
                if (ep_type == TF_TYPE_ISOCHRONOUS) {
                    usb_bwnum_set(ep, bw_num);
                }
            }

            /* max packet size */
            mps = ep_desc->wMaxPacketSize & 0x7FF;
            usb_ep_mps_set(ep, mps);

            /* ep fifo entry */
            ep_fifo_entry = (fifo_entry_num * intf_ep_max[inf_desc->bInterfaceNumber]) / ep_num;
            if (spd_mode == SS_MODE) {
                ep_fifo_entry = ss_ep_cmp_desc->bMaxBurst + 1;
                if (ep_type == TF_TYPE_ISOCHRONOUS) {
                    ep_fifo_entry = bw_num;
                }
            } else {
                switch(ep_type) {
                    case TF_TYPE_INTERRUPT:
                        ep_fifo_entry = bw_num * 1;
                        break;
                    case TF_TYPE_ISOCHRONOUS:
                        if (spd_mode == HS_MODE)
                            ep_fifo_entry = bw_num * 8; // 1000us/125us = 8;
                        else
                            ep_fifo_entry = bw_num * 3;
                        break;
                }
            }
			if (ep_type == TF_TYPE_ISOCHRONOUS){
	            if(direction  == 1){
					uint8 ep_fifo_entry_tmp;
					(ep_fifo_entry>15)? (ep_fifo_entry_tmp = 15):(ep_fifo_entry_tmp = ep_fifo_entry);
	                usb_ep_entry_set (ep, ep_fifo_entry_tmp);
	            }else{
	                usb_ep_entry_set (ep, ep_fifo_entry);
	            }
            }else{
				usb_ep_entry_set (ep, ep_fifo_entry);
			}

            if (i == 1) {
                for (n = 0; n < inf_desc->bInterfaceNumber; n++) {
                    if (spd_mode == SS_MODE) {
                        if (inf_entry_num[n] >= inf_alt_entry_num[n]) {
                            start_entry += inf_entry_num[n];
                        } else {
                            start_entry += inf_alt_entry_num[n];
                        }
                    } else {
                        start_entry += fifo_entry_num * intf_ep_max[n];
                    }
                }
            }

            usb_ep_start_entry_set(ep, start_entry);
            start_entry += ep_fifo_entry;

            /* active */
            usb_ep_active(ep);
            usb_ep_seqnum_reset(ep);
            flag = 1;
        }
        if (spd_mode == SS_MODE)
            usb_max_interval_set(max_interval);
        if (flag == 1)
            break;
    }
    if (flag != 1)
        return -1;

    return 0;
}

static int32 udev_conf_desc(uint8 **desc)
{
    int32 ret = 0;
    uint32 spd_mode = usb_spd_get();
    ep_desc_t *fs_ep;
	print(PRINT_USB_REQ,"speed_mode:%d.\n",spd_mode);
    switch (spd_mode) {
        case SS_MODE:
            ret = usb_desc_get(SS_CONF_DESC, 0, desc);
            break;

        case HS_MODE:
            ret = usb_desc_get(HS_CONF_DESC, 0, desc);
            break;

        case FS_MODE:
            ret = usb_desc_get(HS_CONF_DESC, 0, desc);
            fs_ep = (ep_desc_t *)(*desc + CONFIG_LENGTH + INTERFACE_LENGTH);
            fs_ep->wMaxPacketSize = FS_BULK_MAX_PACKET_SIZE;
            fs_ep = (ep_desc_t *)(*desc + CONFIG_LENGTH + INTERFACE_LENGTH + EP_LENGTH);
            fs_ep->wMaxPacketSize = FS_BULK_MAX_PACKET_SIZE;
            break;

        default:
            ret = -1;
    }

    return ret;
}

static int32 udev_dev_desc(uint8 **desc)
{
    int32 ret = 0;
    uint32 spd_mode = usb_spd_get();

    switch (spd_mode) {
        case SS_MODE:
            ret = usb_desc_get(SS_DEV_DESC, 0, desc);
            break;

        case HS_MODE:
            ret = usb_desc_get(HS_DEV_DESC, 0, desc);
            break;

        case FS_MODE:
            ret = usb_desc_get(HS_DEV_DESC, 0, desc);
            break;

        default:
            ret = -1;
    }
    return ret;
}

static int32 udev_bos_desc(uint8 **desc)
{
    return usb_desc_get(BOS_DESC, 0, desc);
}

static int32 udev_string_desc(uint8 **desc, uint32 id)
{
    return usb_desc_get(STR_DESC, id, desc);
}

static int32 udev_dev_qual_desc(uint8 **desc)
{
    return usb_desc_get(DEVQUAL_DESC, 0, desc);
}

static int32 udev_other_conf_desc(uint8 **desc)
{
    int32 ret = 0;
    ret = usb_desc_get(OTHER_CONF_DESC, 0, desc);
    ep_desc_t *hs_ep;

    if (usb_spd_get() == FS_MODE) {
        hs_ep = (ep_desc_t *)(*desc + CONFIG_LENGTH + INTERFACE_LENGTH);
        hs_ep->wMaxPacketSize = HS_BULK_MAX_PACKET_SIZE;
        hs_ep = (ep_desc_t *)(*desc + CONFIG_LENGTH + INTERFACE_LENGTH + EP_LENGTH);
        hs_ep->wMaxPacketSize = HS_BULK_MAX_PACKET_SIZE;
    }

    return ret;
}

void test_pkt_gen(uint8 *tst_packet)
{
    uint8 *tp = tst_packet;
    int32 i;

    for (i = 0; i < 9; i++)/*JKJKJKJK x 9*/
        *tp++ = 0x00;

    for (i = 0; i < 8; i++) /* 8*AA */
        *tp++ = 0xAA;

    for (i = 0; i < 8; i++) /* 8*EE */
        *tp++ = 0xEE;

    *tp++ = 0xFE;

    for (i = 0; i < 11; i++) /* 11*FF */
        *tp++ = 0xFF;

    *tp++ = 0x7F;
    *tp++ = 0xBF;
    *tp++ = 0xDF;
    *tp++ = 0xEF;
    *tp++ = 0xF7;
    *tp++ = 0xFB;
    *tp++ = 0xFD;
    *tp++ = 0xFC;
    *tp++ = 0x7E;
    *tp++ = 0xBF;
    *tp++ = 0xDF;
    *tp++ = 0xEF;
    *tp++ = 0xF7;
    *tp++ = 0xFB;
    *tp++ = 0xFD;
    *tp++ = 0x7E;
}

int32 get_status(setup_pkt_t *setup_pkt)
{
    uint8 rtdata[2];
    uint8 rtdata_Low = 0;
    uint8 rtdata_High = 0;
    uint8 ep;
    conf_desc_t *conf_desc = NULL;
    uint32 spd_mode = usb_spd_get();
    uint8 cap_rmwkup;

    if (-1 == udev_conf_desc((uint8 **)&conf_desc)) {
        return -1;
    }

    switch (setup_pkt->bmRequestType & USB_RECIP_MASK) {

        case USB_RECIP_DEVICE:
            if (spd_mode == SS_MODE) {
                rtdata_Low = status.ltmen << 4 | status.u2en << 3 |
                     status.u1en << 2 | (conf_desc->bmAttributes >> 6 & 0x1);
            } else {
                cap_rmwkup = conf_desc->bmAttributes >> 5 & 0x1;
                if (cap_rmwkup) {
                    rtdata_Low = (status.rmwkup << 1) | (conf_desc->bmAttributes >> 6 & 0x1);
                } else {
                    rtdata_Low = conf_desc->bmAttributes >> 6 & 0x1;
                }
            }
            break;

        case USB_RECIP_INTERFACE:
            if (spd_mode == SS_MODE) {
                rtdata_Low = status.frwen << 1;  // support function remote wake
            }
            // Return 2-byte ZEROs Interface status to Host when USB2.0
            break;

        case USB_RECIP_ENDPOINT:
            ep = setup_pkt->wIndex & 0xF;
            if (ep) {
                /* active */
                if ((ep > MAX_EP_NUM) || !(usb_ep_is_active(ep))) {
                    return -1;
                }
                if (usb_ep_is_stalled(ep)) {
                    rtdata_Low = 1;
                }
            } else {
                rtdata_Low = bUsbEP0HaltSt;
            }
            break;

        default:
            return -1;
    }

    rtdata[0] = rtdata_Low;
    rtdata[1] = rtdata_High;
    usb_ep0_send(rtdata, 2);
    return 0;
}

int32 clear_feature(setup_pkt_t *setup_pkt)
{
    uint16 value = setup_pkt->wValue;
    uint16 index = setup_pkt->wIndex;
    uint8 ep = index & 0xF;
    uint32 spd_mode = usb_spd_get();

    switch (setup_pkt->bmRequestType & USB_RECIP_MASK) {

        case USB_RECIP_DEVICE:
            if (spd_mode == SS_MODE) {
                switch (value) {
                    case U1_ENABLE:
                        usb_u1_disable();
                        status.u1en = 0;
                        break;

                    case U2_ENABLE:
                        usb_u2_disable();
                        status.u2en = 0;
                        break;

                    case LTM_ENABLE:
                        status.ltmen = 2;
                        break;

                    default:
                        return -1;
                }
            } else {
                switch (value) {
                    case DEVICE_REMOTE_WAKEUP:
                        usb_remote_wakeup_clear();
                        status.rmwkup = 0;
                        break;

                    default:
                        return -1;
                }
            }
            break;

        case USB_RECIP_INTERFACE:
            if (value == FUNCTION_SUSPEND) {
                status.frwen = 0;
            } else {
                return -1;
            }
            break;

        case USB_RECIP_ENDPOINT:
            if (value == ENDPOINT_HALT) {
                /* if ep=0, cx_stl will be cleared automatically */
                if (ep) {
                    /* active */
                    if ((ep > MAX_EP_NUM) || !(usb_ep_is_active(ep))) {
                        return -1;
                    }
					usb_clear_feature_flag = 1;
                    usb_ep_seqnum_reset(ep);
                    usb_ep_stall_clear(ep);
                } else {
                    bUsbEP0HaltSt = 0;
                }
            } else {
                return -1;
            }
            break;

        default:
            return -1;
    }

    if (status.ltmen == 2) {
        status.ltmen = 0;
        usb_ltm_disable();
    }
    return 0;
}

int32 set_feature(setup_pkt_t *setup_pkt)
{
    uint16 value = setup_pkt->wValue;
    uint16 index = setup_pkt->wIndex;
    uint8 suspend_op;
    uint8 ep = index & 0xF;
    uint8 tst_packet[55];
    uint32 spd_mode = usb_spd_get();
    uint32 test_mode;
    uint8 cap_rmwkup;
    conf_desc_t *conf_desc = NULL;
    uint32 timeout = 0;
#ifdef CONFIG_PM
    timeout = 0xFE;
#endif

    if ((spd_mode == SS_MODE) && (!usb_config_is_set())) {
        return -1;
    }

    if (-1 == udev_conf_desc((uint8 **)&conf_desc)) {
        return -1;
    }

    switch (setup_pkt->bmRequestType & USB_RECIP_MASK) {
        case USB_RECIP_DEVICE:
            if (spd_mode == SS_MODE) {
                switch (value) {
                    case U1_ENABLE:
                        usb_u1_timeout_set(0);//for cv test,change from 0 to 0xFE
                        usb_u1_enable();
                        status.u1en = 1;
                        break;

                    case U2_ENABLE:
                        usb_u2_timeout_set(timeout);//for cv test,change form 0 to 0xFE
                        usb_u2_enable();
                        status.u2en = 1;
                        break;

                    case LTM_ENABLE:
                        status.ltmen = 1;
                        break;

                    default:
                        return -1;
                }
            } else {
                switch (value) {
                    case DEVICE_REMOTE_WAKEUP:
                        cap_rmwkup = conf_desc->bmAttributes >> 5 & 0x1;
                        if (cap_rmwkup) {
                            usb_remote_wakeup_set();
                            status.rmwkup = 1;
                        }
                        break;

                    case TEST_MODE:
                        test_mode = index >> 8;
                        switch (test_mode) {
                            case TEST_J:
                            case TEST_K:
                            case TEST_SE0_NAK:
                                usb_test_mode_set(test_mode);
                                break;

                            case TEST_PACKET:
                                usb_test_mode_set(test_mode);
                                usb_ep0_ack();
                                test_pkt_gen(tst_packet);
                                usb_ep0_send(tst_packet, 53);
                                return 0;

                            case TEST_FORCE_ENABLE:
                                break;

                            default:
                                return -1;
                        }
                        break;

                    default:
                        return -1;
                }
            }
            break;

        case USB_RECIP_INTERFACE:
            if (spd_mode == SS_MODE) {
                if (value == FUNCTION_SUSPEND) {
                    suspend_op = (index >> 0x8) & 0xFF;
                    if (suspend_op & FUNCTION_REMOTE_WAKE_ENABLE) {
                        status.frwen = 1;
                    } else {
                        status.frwen = 0;
                    }
					if(suspend_op & LOW_POWER_SUSPEND_STATE){//low power suspend state
						//stop uvc trans
					} else {
						//start uvc trans
					}
                } else
                    return -1;
            } else
                return -1;
            break;

        case USB_RECIP_ENDPOINT:
            if (value == ENDPOINT_HALT) {
                if (ep) {
                    /* active */
                    if ((ep > MAX_EP_NUM) || !(usb_ep_is_active(ep))) {
                        return -1;
                    }
                    usb_ep_stall(ep);
                    if (spd_mode == SS_MODE) {
                        /* change stream state to Disabled */
                        usb_stream_disable(ep);
                    }
                } else {
                    usb_ep0_stall();
                    bUsbEP0HaltSt = 1;
                }
            } else
                return -1;
            break;

        default:
            return -1;
    }

    if (status.ltmen) {
        usb_set_belt(125, 1, 1000, 1);
        usb_ltm_enable();
    }

    return 0;
}

int32 set_address(setup_pkt_t *setup_pkt)
{
    if (setup_pkt->wValue > 0x7F) {
        return -1;
    }
    usb_addr_set(setup_pkt->wValue);
    return 0;
}

int32 get_descriptor(setup_pkt_t *setup_pkt)
{
    uint8  descriptor_type = setup_pkt->wValue >> 8 & 0xFF;
    uint16 return_length = setup_pkt->wLength;
    uint16 tfr_length = 0; // if descriptor is longer than wlength, only wlength of descriptor are returned
    uint8  *desc = NULL;
    usb_desc_header_t *str_header;

    switch (descriptor_type) {
        case DT_DEVICE:
            print(PRINT_USB_REQ, "DEVICE\n");
            if (-1 == udev_dev_desc(&desc)) {
                return -1;
            }
            tfr_length = DEVICE_LENGTH <= return_length ?
                DEVICE_LENGTH : return_length;
            usb_ep0_send(desc, tfr_length);
            break;

        case DT_BOS:
            print(PRINT_USB_REQ, "BOS\n");
            if (-1 == udev_bos_desc(&desc)) {
                return -1;
            }
            tfr_length = ((bos_desc_t *)desc)->wTotalLength <= return_length ?
                ((bos_desc_t *)desc)->wTotalLength : return_length;
            usb_ep0_send(desc, tfr_length);
            break;

        case DT_CONFIGURATION:
            print(PRINT_USB_REQ, "CONFIGURATION\n");
            if (-1 == udev_conf_desc(&desc)) {
                return -1;
            }
            tfr_length = ((conf_desc_t *)desc)->wTotalLength <= return_length ?
                ((conf_desc_t *)desc)->wTotalLength : return_length;
            usb_ep0_send((uint8 *)desc, tfr_length);
            break;

        case DT_STRING:
            print(PRINT_USB_REQ, "STRING\n");
            if ((setup_pkt->wValue & 0xFF) > 12) {
                return -1;
            }
            if (-1 == udev_string_desc(&desc, setup_pkt->wValue & 0xFF)) {
                return -1;
            }
            str_header = (usb_desc_header_t *)desc;
            tfr_length = str_header->bLength <= return_length ?
                str_header->bLength : return_length;
            usb_ep0_send(desc, tfr_length);
            break;

        case DT_DEVICE_QUALIFIER:
            print(PRINT_USB_REQ, "DEVICE_QUALIFIER\n");
            if (usb_spd_get() == SS_MODE) {
                return -1;
            } else {
                if (-1 == udev_dev_qual_desc(&desc)) {
                    return -1;
                }
                tfr_length = DEVICE_QUALIFIER_LENGTH <= return_length ?
                    DEVICE_QUALIFIER_LENGTH : return_length;
                usb_ep0_send(desc, tfr_length);
            }
            break;

        case DT_OTHER_SPEED_CONFIGURATION:
            print(PRINT_USB_REQ, "OTHER_SPEED_CONFIGURATION\n");
            if (usb_spd_get() == SS_MODE) {
                return -1;
            } else {
                if(-1 == udev_other_conf_desc(&desc)) {
                    return -1;
                }
                tfr_length = ((conf_desc_t *)desc)->wTotalLength <= return_length ?
                    ((conf_desc_t *)desc)->wTotalLength : return_length;
                usb_ep0_send(desc, tfr_length);
            }
            break;

        default:
            print(PRINT_USB_REQ, "OTHER DESCRIPTOR\n");
            return -1;
    }
    return 0;
}

int32 set_descriptor(setup_pkt_t *setup_pkt)
{
    return -1;
}

int32 get_configuration(setup_pkt_t *setup_pkt)
{
    uint8 config = usb_config_is_set() ? 1 : 0;
    usb_ep0_send(&config, 1);
    return 0;
}

int32 set_configuration(setup_pkt_t *setup_pkt)
{
    int32 i;
    conf_desc_t *conf_desc = NULL;
    if (!(setup_pkt->wValue & 0xFF)) {
        /* if configuration value is 0, device enters the Address state */
        usb_config_clear();
        udev_reset();
        return 0;
    } else if (setup_pkt->wValue == 1) { //we support only one configuration
        /* if already configured */
        if (usb_config_is_set()) {
            return 0;
        }

        /* if can not find the config descriptor */
        if (-1 == udev_conf_desc((uint8 **)&conf_desc)) {
            return -1;
        }

        /* config all the relevent eps by config descriptor */
        for (i = 0; i < conf_desc->bNumInterfaces; i++) {
            if (-1 == udev_intf_ep_config(conf_desc, i, 0)) {
                return -1;
            } else
                continue;
        }

        /* set the config register bit */
        usb_config_set();

        if (usb_spd_get() == SS_MODE) {
            print(1, "USB 3.0 connected.\n");
            //nvlog("U3C\n");
        } else {
            print(1, "USB 2.0 connected.\n");
            //nvlog("U2C\n");
        }
        usb_config_flag = 1;
        return 0;
    } else {
        return -1;
    }
}

int32 get_interface(setup_pkt_t *setup_pkt)
{
    conf_desc_t *conf_desc = NULL;
    uint32 intf = setup_pkt->wIndex & 0xFF;

    if (!usb_config_is_set()) {
        return -1;
    }

    if (-1 == udev_conf_desc((uint8 **)&conf_desc)) {
        return -1;
    }

    if (intf >= conf_desc->bNumInterfaces) {
        return -1;
    }

    usb_ep0_send(&altsetting[intf], 1);
    return 0;
}

int32 set_interface(setup_pkt_t *setup_pkt)
{
    conf_desc_t *conf_desc = NULL;
    uint8 intf = setup_pkt->wIndex & 0xFF;
    uint8 alt = setup_pkt->wValue & 0xFF;

    if (!usb_config_is_set()) {
        return -1;
    }

    if (-1 == udev_conf_desc((uint8 **)&conf_desc)) {
        return -1;
    }

    if (intf >= conf_desc->bNumInterfaces) {
        return -1;
    }

    if (-1 == udev_intf_ep_config(conf_desc, intf, alt)) {
        return -1;
    } else {
        altsetting[intf] = alt;
		return 0;
    }
}

int32 sync_frame(setup_pkt_t *setup_pkt)
{
    // TODO
    return -1;
}

int32 set_sel(setup_pkt_t *setup_pkt)
{
    uint8 cx_data[6];

    usb_ep0_recv(cx_data, 6);
    // TODO

    return 0;
}

static int32 set_iso_delay(setup_pkt_t *setup_pkt)
{
    uint16 value = setup_pkt->wValue;

    if (value > 65535) {
        return -1;
    } else {
        // TODO
        return 0;
    }
}
int32 audio_class_request(setup_pkt_t *setup_okt)
{
	int32 ret = 0;
	return ret;
}
int32 standard_request(setup_pkt_t *setup_pkt)
{
    int32 ret = 0;
    print(PRINT_USB_REQ, "STANDARD USB REQ:  ");
    switch (setup_pkt->bRequest) {
        case GET_STATUS:
            print(PRINT_USB_REQ, "GET_STATUS\n");
            ret = get_status(setup_pkt);
            break;

        case CLEAR_FEATURE:
            print(PRINT_USB_REQ, "CLEAR_FEATURE\n");
            ret = clear_feature(setup_pkt);
            break;

        case SET_FEATURE:
            print(PRINT_USB_REQ, "SET_FEATURE\n");
            ret = set_feature(setup_pkt);
            break;

        case SET_ADDRESS:
            print(PRINT_USB_REQ, "SET_ADDRESS\n");
/*	for the cv test,must comment out the line of usb_config_flag:start */
			if(1 == usb_config_flag) {
				chip_reset(RESET_TARGET_FIRMWARE);	
				while(1);
			} else {
				usb_config_flag = 1;	
			}
/*	for the cv test,must comment out the line of usb_config_flag:end */
            if (!bUsbEP0HaltSt)
                ret = set_address(setup_pkt);
            break;

        case GET_DESCRIPTOR:
            print(PRINT_USB_REQ, "GET_DESCRIPTOR: ");
            if (!bUsbEP0HaltSt)
                ret = get_descriptor(setup_pkt);
			else
				printf("bUsbEP0HaltSt\n");
            break;

        case SET_DESCRIPTOR:
            print(PRINT_USB_REQ, "SET_DESCRIPTOR\n");
            if (!bUsbEP0HaltSt)
                ret = set_descriptor(setup_pkt);
            break;

        case GET_CONFIGURATION:
            print(PRINT_USB_REQ, "GET_CONFIGURATION\n");
            if (!bUsbEP0HaltSt)
                ret = get_configuration(setup_pkt);
            break;

        case SET_CONFIGURATION:
            print(PRINT_USB_REQ, "SET_CONFIGURATION\n");
            if (!bUsbEP0HaltSt)
                ret = set_configuration(setup_pkt);
            break;

        case GET_INTERFACE:
            print(PRINT_USB_REQ, "GET_INTERFACE\n");
            if (!bUsbEP0HaltSt)
                ret = get_interface(setup_pkt);
            break;

        case SET_INTERFACE:
            print(PRINT_USB_REQ, "SET_INTERFACE\n");
            if (!bUsbEP0HaltSt)
                ret = set_interface(setup_pkt);
            break;

        case SYNCH_FRAME:
            print(PRINT_USB_REQ, "SYNCH_FRAME\n");
            ret = sync_frame(setup_pkt);
            break;

        case SET_SEL:
            print(PRINT_USB_REQ, "SET_SEL\n");
            ret = set_sel(setup_pkt);
            break;

        case SET_ISOCH_DELAY:
            print(PRINT_USB_REQ, "SET_ISOCH_DELAY\n");
            ret = set_iso_delay(setup_pkt);
            break;

        default:
            print(PRINT_USB_REQ, "UNSUPPORTED: %d\n", setup_pkt->bRequest);
            ret = -1;
            break;
    }

    return ret;
}
