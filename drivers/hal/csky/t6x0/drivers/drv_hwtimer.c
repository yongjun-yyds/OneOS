/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_hwtimer.c
 *
 * @brief       This file implements timer driver for csky.
 *
 * @revision
 * Date         Author          Notes
 * 2021-04-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <os_memory.h>
#include <timer/timer.h>
#include <bus/bus.h>

#ifdef BSP_USING_TIMER

#include <drv_hwtimer.h>
#include <drv_intr.h>
#include <tih/scu.h>
#define DRV_EXT_LVL DBG_EXT_INFO
#define DRV_EXT_TAG "drv.hwtimer"
#include <drv_log.h>

static os_list_node_t csky_timer_list = OS_LIST_INIT(csky_timer_list);

static void TIM_Callback(struct csky_timer *timer)
{
    if (timer->info->tim_num != 0)
    {
#ifdef OS_USING_CLOCKEVENT
        os_clockevent_isr((os_clockevent_t *)timer);
#endif
    }
}

void TIMn_IRQHandler(void)
{
    struct csky_timer *timer;
    uint32_t        intr_status;
    uint8_t         tim_num;
    intr_status = timer_intr_status();
    timer_intr_clear(intr_status);
    os_hw_interrupt_clear(TIMER_IRQ_NUM);
    os_list_for_each_entry(timer, &csky_timer_list, struct csky_timer, list)
    {
        tim_num = timer->info->tim_num;
        if ((0x01 << tim_num) & intr_status)
        {
            TIM_Callback(timer);
        }
    }
}

void timer_init(struct csky_timer *timer)
{
    timer_config_t *cfg;
    uint32_t     htim;
    cfg  = &(timer->info->tim_cfg);
    htim = timer->info->tim_num;

    timer_hw_init(htim, cfg);
    timer_intr_enable(htim);
    os_hw_interrupt_mask(TIMER_IRQ_NUM);
    os_hw_interrupt_install(TIMER_IRQ_NUM, (os_isr_handler_t)TIMn_IRQHandler, (void *)htim, "timer");
    os_hw_interrupt_umask(TIMER_IRQ_NUM);
}

#if defined(OS_USING_CLOCKSOURCE) || defined(OS_USING_CLOCKEVENT)
static uint64_t csky_timer_read(void *clock)
{
    struct csky_timer *timer;
    uint64_t        timer_count = 0;
    timer = (struct csky_timer *)clock;
    os_ubase_t level = 0;
    uint8_t    tim_num;
    uint64_t   start_count;

    tim_num     = timer->info->tim_num;
    start_count = timer->info->tim_cfg.us * (timer->clock.ce.freq / USEC_PER_SEC);

    os_spin_lock_irqsave(&gs_device_lock, &level);
    timer_count = start_count - timer_count_get(tim_num);
    os_spin_unlock_irqrestore(&gs_device_lock, level);
    return timer_count;
}
#endif

#ifdef OS_USING_CLOCKEVENT
static void csky_timer_start(os_clockevent_t *ce, uint32_t prescaler, uint64_t count)
{
    struct csky_timer *timer;
    os_ubase_t         level = 0;
    timer_config_t    *cfg;
    uint8_t            htim;
    OS_ASSERT(prescaler != 0);
    OS_ASSERT(count != 0);

    timer = (struct csky_timer *)ce;
    cfg   = &(timer->info->tim_cfg);
    htim  = timer->info->tim_num;
    // cfg->reload_mode = TIMER_LOAD_AUTO;
    cfg->us = count * USEC_PER_SEC / timer->clock.ce.freq;
    if (cfg->us < 1)
        cfg->us = 1;

    os_spin_lock_irqsave(&gs_device_lock, &level);
    timer_init(timer);
    timer_start(htim);
    os_spin_unlock_irqrestore(&gs_device_lock, level);
}

static void csky_timer_stop(os_clockevent_t *ce)
{
    struct csky_timer *timer;
    OS_ASSERT(ce != OS_NULL);

    timer           = (struct csky_timer *)ce;
    uint8_t htim = timer->info->tim_num;

    timer_stop(htim);
    timer_intr_disable(htim);
}

static const struct os_clockevent_ops csky_tim_ops = {
    .start = csky_timer_start,
    .stop  = csky_timer_stop,
    .read  = csky_timer_read,
};
#endif

static int csky_tim_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    struct csky_timer *timer;
    uint32_t           max_us;
    os_ubase_t         level;

    timer = os_calloc(1, sizeof(struct csky_timer));
    OS_ASSERT(timer);

    timer->info = (csky_timer_info_t *)dev->info;
    timer->freq = clock_freq_get(MODULE_APB);
    max_us      = 0xFFFFFFFF / (timer->freq / USEC_PER_SEC);

#ifdef OS_USING_CLOCKSOURCE
    if (os_clocksource_best() == OS_NULL)
    {
        timer->info->tim_cfg.us          = max_us;    // max_us
        timer->info->tim_cfg.reload_mode = TIMER_LOAD_AUTO;
        os_spin_lock_irqsave(&gs_device_lock, &level);
        timer_hw_init(timer->info->tim_num, &(timer->info->tim_cfg));
        timer_start(timer->info->tim_num);
        os_spin_unlock_irqrestore(&gs_device_lock, level);
        timer->clock.cs.rating = 320;    // digit * 10;
        timer->clock.cs.freq   = timer->freq;
        timer->clock.cs.mask   = 0xffffffff;    // set_mas_us;
        timer->clock.cs.read   = csky_timer_read;
        os_clocksource_register(dev->name, &timer->clock.cs);
    }
    else
#endif
    {
#ifdef OS_USING_CLOCKEVENT

        timer->clock.ce.rating = 320;    // digit * 10;
        timer->clock.ce.freq   = timer->freq;
        timer->clock.ce.mask   = 0xffffffff;    // set_mas_us;

        timer->clock.ce.prescaler_mask = 1;
        timer->clock.ce.prescaler_bits = 0;

        timer->clock.ce.count_mask = 0xffffffff;    // set_mas_us;
        timer->clock.ce.count_bits = 32;            // digit;

        timer->clock.ce.feature = OS_CLOCKEVENT_FEATURE_PERIOD;

        timer->clock.ce.min_nsec = 1000;    // timer min clock is 1us

        timer->clock.ce.ops = &csky_tim_ops;
        os_clockevent_register(dev->name, &timer->clock.ce);

#endif
    }
    os_list_add(&csky_timer_list, &timer->list);

    return OS_SUCCESS;
}

OS_DRIVER_INFO csky_tim_driver = {
    .name  = "Timer_InitStructure",
    .probe = csky_tim_probe,
};

OS_DRIVER_DEFINE(csky_tim_driver, OS_INIT_LEVEL_PRE_DEVICE, OS_INIT_SUBLEVEL_MIDDLE);
#endif

