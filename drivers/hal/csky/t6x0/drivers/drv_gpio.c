/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_gpio.c
 *
 * @brief       This file implements gpio driver for CK.
 *
 * @revision
 * Date         Author          Notes
 * 2020-07-08   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <drv_gpio.h>
#include <device.h>
#include <os_memory.h>
#include <os_errno.h>
#include <pin/pin.h>
#include <gpio.h>
#include <drv_intr.h>
#include <tih/scu.h>
#ifdef OS_USING_PIN

struct os_pin_irq_hdr ck_pin_irq_hdr_tab[GPIO_NUMS] = {
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
};

static void ck_pin_write(struct os_device *dev, os_base_t pin, os_base_t value)
{
    if (pin < GPIO_NUMS)
    {
        gpio_set(pin, value);
    }
    else
        return;
}

static int ck_pin_read(struct os_device *dev, os_base_t pin)
{
    int value = PIN_LOW;
    if (pin < GPIO_NUMS)
    {
        if (gpio_get(pin) != 0)
        {
            value = PIN_HIGH;
        }
        else
        {
            value = PIN_LOW;
        }
    }
    return value;
}

static void ck_pin_mode(struct os_device *dev, os_base_t pin, os_base_t mode)
{
    gpio_config_t config;

    if (pin < GPIO_NUMS)
    {
        if (mode == PIN_MODE_OUTPUT)
        {
            config.io_mode = GPIO_IOMODE_OUT;
        }
        else if (mode == PIN_MODE_INPUT)
        {
            config.io_mode = GPIO_IOMODE_INFT;
        }
        else if (mode == PIN_MODE_INPUT_PULLUP)
        {
            config.io_mode = GPIO_IOMODE_INPU;
        }
        else if (mode == PIN_MODE_INPUT_PULLDOWN)
        {
            config.io_mode = GPIO_IOMODE_INPD;
        }
        else
        {
            return;
        }
        config.pin       = pin;
        config.trig_mode = GPIO_TRIGMODE_NONE;
        gpio_hw_init(&config);
        // scu_pin_mux_set(SCU_PIN_MUX_MODE1_1, SCU_PIN_MUX_MODE2_GPIO1);
    }
    else
        return;
}

static void __ck_hal_gpio_int_service(uint32_t irq_Status)
{
    uint32_t ui32Clz;
    uint32_t ui32Status;
    ui32Status = irq_Status;
    while (ui32Status)
    {
        /* Pick one of any remaining active interrupt bits */
        ui32Clz = __builtin_clz(ui32Status);

        /* Turn off the bit we picked in the working copy */
        ui32Status &= ~(0x80000000 >> ui32Clz);

        if (ck_pin_irq_hdr_tab[31 - ui32Clz].hdr)
        {
            /* If we found an interrupt handler routine, call it now. */
            ck_pin_irq_hdr_tab[31 - ui32Clz].hdr(ck_pin_irq_hdr_tab[31 - ui32Clz].args);
        }
    }
}

void ck_gpio_isr(void)
{
    uint32_t irq_Status;
    /* Read and clear the GPIO interrupt status. */
    irq_Status = gpio_intr_status();
    gpio_intr_clear(irq_Status);
    __ck_hal_gpio_int_service(irq_Status);
}

static os_err_t
ck_pin_attach_irq(struct os_device *device, int32_t pin, uint32_t mode, void (*hdr)(void *args), void *args)
{
    os_ubase_t     level;
    gpio_config_t config;
    int32_t    irqindex = -1;

    if (pin < GPIO_NUMS)
    {
        irqindex = pin;
        os_spin_lock_irqsave(&gs_device_lock, &level);
        if (ck_pin_irq_hdr_tab[irqindex].pin == pin && ck_pin_irq_hdr_tab[irqindex].hdr == hdr &&
            ck_pin_irq_hdr_tab[irqindex].mode == mode && ck_pin_irq_hdr_tab[irqindex].args == args)
        {
            os_spin_unlock_irqrestore(&gs_device_lock, level);
            return OS_SUCCESS;
        }
        if (ck_pin_irq_hdr_tab[irqindex].pin != -1)
        {
            os_spin_unlock_irqrestore(&gs_device_lock, level);
            return OS_BUSY;
        }
        ck_pin_irq_hdr_tab[irqindex].pin  = pin;
        ck_pin_irq_hdr_tab[irqindex].hdr  = hdr;
        ck_pin_irq_hdr_tab[irqindex].mode = mode;
        ck_pin_irq_hdr_tab[irqindex].args = args;

        config.pin     = pin;
        config.io_mode = GPIO_IOMODE_INPU;
        if (mode == PIN_IRQ_MODE_RISING)
        {
            config.trig_mode = GPIO_TRIGMODE_EDGE_RISING;
        }
        else if (mode == PIN_IRQ_MODE_FALLING)
        {
            config.trig_mode = GPIO_TRIGMODE_EDGE_FALLING;
        }
        else if (mode == PIN_IRQ_MODE_RISING_FALLING)
        {
            config.trig_mode = GPIO_TRIGMODE_EDGE_BOTH;
        }
        else if (mode == PIN_IRQ_MODE_HIGH_LEVEL)
        {
            config.trig_mode = GPIO_TRIGMODE_LEVEL_HIGH;
        }
        else if (mode == PIN_IRQ_MODE_LOW_LEVEL)
        {
            config.trig_mode = GPIO_TRIGMODE_LEVEL_LOW;
        }
        else
        {
            return OS_NOSYS;
        }

        gpio_hw_init(&config);
        // scu_pin_mux_set(SCU_PIN_MUX_MODE1_1, SCU_PIN_MUX_MODE2_GPIO1);
        os_spin_unlock_irqrestore(&gs_device_lock, level);
        return OS_SUCCESS;
    }
    else
        return OS_FAILURE;
}

static os_err_t ck_pin_dettach_irq(struct os_device *device, int32_t pin)
{
    os_ubase_t  level;
    int32_t irqindex = -1;

    if (pin < GPIO_NUMS)
    {
        irqindex = pin;
        os_spin_lock_irqsave(&gs_device_lock, &level);
        if (ck_pin_irq_hdr_tab[irqindex].pin == -1)
        {
            os_spin_unlock_irqrestore(&gs_device_lock, level);
            return OS_SUCCESS;
        }
        ck_pin_irq_hdr_tab[irqindex].pin  = -1;
        ck_pin_irq_hdr_tab[irqindex].hdr  = OS_NULL;
        ck_pin_irq_hdr_tab[irqindex].mode = 0;
        ck_pin_irq_hdr_tab[irqindex].args = OS_NULL;
        os_spin_unlock_irqrestore(&gs_device_lock, level);
        return OS_SUCCESS;
    }
    else
        return OS_NOSYS;
}

static os_err_t ck_pin_irq_enable(struct os_device *device, os_base_t pin, uint32_t enabled)
{
    os_ubase_t         level;
    static int32_t irq_pin = 0;

    if (pin < GPIO_NUMS)
    {
        if (enabled == PIN_IRQ_ENABLE)
        {
            os_spin_lock_irqsave(&gs_device_lock, &level);
            if (!irq_pin)
            {
                os_hw_interrupt_install(GPIO_IRQ_NUM, (os_isr_handler_t)ck_gpio_isr, NULL, "GPIO");
                os_hw_interrupt_umask(GPIO_IRQ_NUM);
            }
            irq_pin |= (1 << pin);
            gpio_intr_enable(pin);
            os_spin_unlock_irqrestore(&gs_device_lock, level);
        }
        else if (enabled == PIN_IRQ_DISABLE)
        {
            os_spin_lock_irqsave(&gs_device_lock, &level);
            gpio_intr_disable(pin);
            irq_pin &= ~(1 << pin);
            if (!irq_pin)
            {
                os_hw_interrupt_mask(GPIO_IRQ_NUM);
            }
            os_spin_unlock_irqrestore(&gs_device_lock, level);
        }
        else
        {
            return OS_NOSYS;
        }
    }
    else
    {
        return OS_NOSYS;
    }
    return OS_SUCCESS;
}

const static struct os_pin_ops _CK_pin_ops = {
    .pin_mode       = ck_pin_mode,
    .pin_write      = ck_pin_write,
    .pin_read       = ck_pin_read,
    .pin_attach_irq = ck_pin_attach_irq,
    .pin_detach_irq = ck_pin_dettach_irq,
    .pin_irq_enable = ck_pin_irq_enable,
};

/**
 ***********************************************************************************************************************
 * @brief           os_hw_pin_init:enable gpio clk,register pin device.
 *
 * @param[in]       none
 *
 * @return          Return init result.
 * @retval          OS_SUCCESS       init success.
 * @retval          Others       init failed.
 ***********************************************************************************************************************
 */
int os_hw_pin_init(void)
{
    /* gpio default mode: 0 1 0,see t680 datasheet table2.2 */
    scu_pin_mux_set(SCU_PIN_MUX_MODE1_0, SCU_PIN_MUX_MODE2_S_U1);
    return os_device_pin_register(0, &_CK_pin_ops, OS_NULL);
}

#endif
