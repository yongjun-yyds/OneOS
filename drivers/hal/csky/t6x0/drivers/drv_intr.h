#ifndef __DRV_INTR_H__
#define __DRV_INTR_H__

typedef void (*os_isr_handler_t)(int vector, void *param);

struct os_irq_desc
{
    os_isr_handler_t handler;
    void            *param;
};
void                    os_hw_interrupt_init(void);
extern os_isr_handler_t os_hw_interrupt_install(int vector, os_isr_handler_t handler, void *param, char *name);

void os_hw_interrupt_mask(int vector);
void os_hw_interrupt_umask(int vector);
void os_hw_interrupt_clear(int vevtor);

#endif
