/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_eth.c
 *
 * @brief       This file implements eth driver.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include "board.h"
#include <string.h>
#include <bus/bus.h>
#ifdef BSP_USING_ETH
#include <netif/ethernetif.h>
#include "lwipopts.h"
#include <drv_eth.h>
#include <os_memory.h>
#include <dma.h>
#include <drv_intr.h>
#include <stdio.h>
/*
 * Emac driver uses CubeMX tool to generate emac and phy's configuration,
 * the configuration files can be found in CubeMX_Config folder.
 */

#define DRV_EXT_LVL DBG_EXT_INFO
#define DRV_EXT_TAG "drv.eth"

#define DBG_TAG "drv.eth"
#include <dlog.h>

#define GMAC_INT_DEFAULT (GMAC_INT_RPKT2B)
/* ===================================================================== */
#ifdef OS_USING_ETH0
#define ETH0_TX_QUEUE_ENTRIES 4
#define ETH0_RX_QUEUE_ENTRIES 4

uint32 eth0_tx_pbuf_record[ETH0_TX_QUEUE_ENTRIES]; /* TX pbuf recorder */
uint32 eth0_rx_pbuf_record[ETH0_RX_QUEUE_ENTRIES]; /* RX pbuf recorder */

static struct os_ck_net ck_eth_dev0;
static os_ck_net_t     *eth0_dev = &ck_eth_dev0;
#endif
/* ===================================================================== */
#ifdef OS_USING_ETH1
#define ETH1_TX_QUEUE_ENTRIES 4
#define ETH1_RX_QUEUE_ENTRIES 4

uint32_t eth1_tx_pbuf_record[ETH1_TX_QUEUE_ENTRIES]; /* TX pbuf recorder */
uint32_t eth1_rx_pbuf_record[ETH1_RX_QUEUE_ENTRIES]; /* RX pbuf recorder */

static struct os_ck_net ck_eth_dev1;
static os_ck_net_t     *eth1_dev = &ck_eth_dev1;
#endif
/* ===================================================================== */
extern const phy_ops_t m88e1512_phy_ops;
extern const phy_ops_t rtl8211_phy_ops;

#if defined(ETH_RX_DUMP) || defined(ETH_TX_DUMP)
#define __is_print(ch) ((unsigned int)((ch) - ' ') < 127u - ' ')
static void dump_hex(const uint8_t *ptr, os_size_t buflen)
{
    unsigned char *buf = (unsigned char *)ptr;
    int            i, j;

    for (i = 0; i < buflen; i += 16)
    {
        os_kprintf("%08X: ", i);

        for (j = 0; j < 16; j++)
            if (i + j < buflen)
                os_kprintf("%02X ", buf[i + j]);
            else
                os_kprintf("   ");
        os_kprintf(" ");

        for (j = 0; j < 16; j++)
            if (i + j < buflen)
                os_kprintf("%c", __is_print(buf[i + j]) ? buf[i + j] : '.');
        os_kprintf("\r\n");
    }
}
#endif

/**
 ***********************************************************************************************************************
 * @brief           EMAC initialization function
 *
 * @param[in]       dev             pointer to os_device_t.
 *
 * @return          Return eth init status.
 * @retval          OS_SUCCESS       init success.
 * @retval          Others       init failed.
 ***********************************************************************************************************************
 */
static os_err_t os_ck_eth_init(os_device_t *dev)
{
    LOG_D(DBG_TAG, "emac init");
    return ERR_OK;
}

static os_err_t os_ck_eth_deinit(os_device_t *dev)
{
    LOG_D(DBG_TAG, "emac deinit");
    return OS_SUCCESS;
}

static os_base_t os_ck_eth_read(os_device_t *dev, os_off_t pos, void *buffer, os_size_t size)
{
    LOG_D(DBG_TAG, "emac read");
    os_set_errno(OS_NOSYS);
    return 0;
}

static os_base_t os_ck_eth_write(os_device_t *dev, os_off_t pos, const void *buffer, os_size_t size)
{
    LOG_D(DBG_TAG, "emac write");
    os_set_errno(OS_NOSYS);
    return 0;
}

static os_err_t os_ck_eth_control(os_device_t *dev, int cmd, void *args)
{
    os_ck_net_t *net_dev = (os_ck_net_t *)dev;
    switch (cmd)
    {
    case NIOCTL_GADDR:
        /* get mac address */
        if (args)
            gmac_mac_addr_get(net_dev->gmac_id, args);
        else
            return OS_FAILURE;

    default:
        break;
    }

    return OS_SUCCESS;
}

/**
 ***********************************************************************************************************************
 * @brief           ethernet device interface,transmit data.
 *
 * @param[in]       dev             pointer to os_device_t.
 * @param[in]       p               pointer to struct pbuf.
 *
 * @return          Return eth tx result.
 * @retval          ERR_OK            tx success.
 * @retval          Others            tx failed.
 ***********************************************************************************************************************
 */
static os_err_t os_ck_eth_tx(os_device_t *dev, struct pbuf *p)
{
    uint32       i;
    uint8       *buf;
    struct pbuf *q;
    struct pbuf *pbuf_final;
    os_ck_net_t *net_dev = (os_ck_net_t *)dev;

    /**
     * This entire function must run within a "critical section" to preserve
     * the integrity of the transmit pbuf queue.
     */

    /* check if there is transmitted buffer */
    buf = gmac_frame_transmitted_buf_poll(net_dev->gmac_id);
    if (buf != OS_NULL)
    {
        /* find transmitted buffer accorded pbuf */
        for (i = 0; i < net_dev->tx_desc_num; i++)
        {
            q = (struct pbuf *)(net_dev->tx_pbuf_record[i]);
            if ((q != OS_NULL) && ((uint8 *)q->payload == buf))
            {
                pbuf_free(q);
                net_dev->tx_pbuf_record[i] = 0;
                break;
            }
        }
    }

    /* Make sure we still have a valid buffer (it may have been copied) */
    if (!p)
    {
        LINK_STATS_INC(link.memerr);
        // SYS_ARCH_UNPROTECT(lev);
        return (-OS_NOMEM);
    }
#if 1
    LOG_D(DBG_TAG, "save p frame\r\n");
    pbuf_final = pbuf_alloc(PBUF_RAW,
                            p->tot_len > 60 ? p->tot_len : 60,
                            PBUF_RAM); /* do padding for frame whose len less than 60 */
    if (pbuf_final == NULL)
    {
        LOG_D(DBG_TAG, "Alloc padded frame fail\n");
        LINK_STATS_INC(link.memerr);
        // SYS_ARCH_UNPROTECT(lev);
        return (-OS_NOMEM);
    }

    memset(pbuf_final->payload, 0, pbuf_final->tot_len);
    pbuf_copy(pbuf_final, p);
#endif
    // pbuf_final = p;

    SYS_ARCH_DECL_PROTECT(lev);
    SYS_ARCH_PROTECT(lev);

    OS_ASSERT(((uint32)pbuf_final->payload > 0x20000) && ((uint32)pbuf_final->payload < 0x40000));
    for (i = 0; i < net_dev->tx_desc_num; i++)
    {
        /* Does this descriptor have a buffer attached to it? */
        if (net_dev->tx_pbuf_record[i] == 0)
        {
            /* record this pbuf address */
            pbuf_ref(pbuf_final);
            net_dev->tx_pbuf_record[i] = (uint32)pbuf_final;
            break;
        }
    }

    if (i == net_dev->tx_desc_num)
    {
        /* free new alloced padded pbuf */
        SYS_ARCH_UNPROTECT(lev);
        pbuf_free(pbuf_final);
        LINK_STATS_INC(link.memerr);
        return (-OS_NOMEM);
    }

    if (gmac_frame_transmit(net_dev->gmac_id, pbuf_final->payload, pbuf_final->tot_len) != OS_SUCCESS)
    {
        /* free for pbuf_ref() */
        SYS_ARCH_UNPROTECT(lev);
        pbuf_free(pbuf_final);
        net_dev->tx_pbuf_record[i] = 0;
        LINK_STATS_INC(link.memerr);
        return (-OS_NOMEM);
    }

    pbuf_free(pbuf_final);
    /* Update lwIP statistics */
    SYS_ARCH_UNPROTECT(lev);
    LINK_STATS_INC(link.xmit);

    return OS_SUCCESS;
}

/* receive data */
struct pbuf *os_ck_eth_rx(os_device_t *dev)
{
    uint32       i;
    uint32       frame_len;
    uint8       *buf;
    os_ck_net_t *net_dev = (os_ck_net_t *)dev;
    struct pbuf *new_p   = NULL;
    struct pbuf *p       = NULL;

    frame_len = gmac_frame_recv_poll(net_dev->gmac_id);
    if (frame_len == 0)
    {
        gmac_intr_enable(net_dev->gmac_id, GMAC_INT_DEFAULT);
        return OS_NULL;
    }

    buf = gmac_rx_valid_buf(net_dev->gmac_id);
    if (buf == NULL)
    {
        gmac_intr_enable(net_dev->gmac_id, GMAC_INT_DEFAULT);
        return OS_NULL;
    }

    /* asssign new pbuf->payload to ring buffer */
    new_p = pbuf_alloc(PBUF_RAW, PBUF_POOL_BUFSIZE, PBUF_POOL);
    if (new_p == NULL)
    {
        /** has no new pbuf and we reuse previous pbuf then drop packet, so the data here
         *  would not be delivered to upper layer.
         */
        LINK_STATS_INC(link.memerr);
        LINK_STATS_INC(link.drop);

        LOG_D(DBG_TAG, "Rx alloc pbuf fial\n");
        gmac_rx_buf_processed(net_dev->gmac_id);
        gmac_intr_enable(net_dev->gmac_id, GMAC_INT_DEFAULT);
        return OS_NULL;
    }
    else
    {
        LINK_STATS_INC(link.recv);

        gmac_rx_buf_attach(net_dev->gmac_id, (uint8 *)new_p->payload);
        gmac_rx_buf_processed(net_dev->gmac_id);

        for (i = 0; i < net_dev->rx_desc_num; i++)
        {
            p = (struct pbuf *)(net_dev->rx_pbuf_record[i]);
            if ((p != NULL) && ((uint32)p->payload == (uint32)buf))
            {
                net_dev->rx_pbuf_record[i] = 0;
                p->tot_len = p->len = frame_len;
                break;
            }
        }

        for (i = 0; i < net_dev->rx_desc_num; i++)
        {
            if (net_dev->rx_pbuf_record[i] == 0)
            {
                net_dev->rx_pbuf_record[i] = (uint32)new_p;
                break;
            }
        }
#ifdef ETH_RX_DUMP
        dump_hex(p->payload, p->tot_len);
#endif
        return p;
    }
}

static void phy_monitor_task_entry(void *paraeter)
{
    char           state[50];
    link_status_t  new_state;
    link_status_t *pre_state;
    os_ck_net_t   *net_dev = (os_ck_net_t *)paraeter;
    struct netif  *Netif   = net_dev->parent.netif;
    while (1)
    {
        /* 1.get PHY current status by reading related registers */
        new_state = net_dev->phy->phy_link_status_get(net_dev->gmac_id);
        pre_state = &net_dev->link_status;

        /* 2.if link state changed */
        if (new_state.linked != pre_state->linked)
        {
            *pre_state = new_state;

            /* link established */
            if (new_state.linked)
            {
                /* set gmac speed and duplex mode according to this new state of phy */
                gmac_speed_duplex_set(net_dev->gmac_id, new_state.speed, new_state.duplex);

                /* notify lwip this change: dhcp re-work */
                eth_device_linkchange(&net_dev->parent, OS_TRUE);

                sprintf(state, "Linked");

                switch (new_state.speed)
                {
                case SPEED_1000:
                    strcat(state, "-1000Mbps");
                    break;
                case SPEED_100:
                    strcat(state, "-100Mbps");
                    break;
                case SPEED_10:
                    strcat(state, "-10Mbps");
                    break;
                default:
                    break;
                }

                /* show this new state on console*/
                if (new_state.duplex == DUPLEX_FULL)
                {
                    strcat(state, "-Full\n");
                }
                else
                {
                    strcat(state, "-Half\n");
                }
            }
            else
            {
                sprintf(state, "Un-linked");
                eth_device_linkchange(&net_dev->parent, OS_FALSE);
            }

            os_kprintf("netif[%c%c] state changed: %s\r\n", Netif->name[0], Netif->name[1], state);
        }
        os_task_msleep(OS_TICK_PER_SECOND * 10);
    }
}

const static struct os_device_ops eth_ops = {
    .init    = os_ck_eth_init,
    .deinit  = os_ck_eth_deinit,
    .read    = os_ck_eth_read,
    .write   = os_ck_eth_write,
    .control = os_ck_eth_control,
};

static os_err_t os_ck_eth_hard_init(os_ck_net_t *net_dev)
{
    uint32_t  i;
    int32_t   ret;
    uint32_t  phy_mmd_clk;
    struct pbuf *p;

    ret = gmac_hw_init(net_dev->gmac_id);
    if (ret != 0)
    {
        LOG_D(DBG_TAG, "gmac_hw_init error\r\n");
        return OS_FAILURE;
    }

    ret = gmac_tx_queue_init(net_dev->gmac_id, net_dev->tx_desc_num);
    if (ret != 0)
    {
        LOG_D(DBG_TAG, "gmac_tx_queue_init error\r\n");
        return OS_FAILURE;
    }

    ret = gmac_rx_queue_init(net_dev->gmac_id, net_dev->rx_desc_num, PBUF_POOL_BUFSIZE);
    if (ret != 0)
    {
        return OS_FAILURE;
    }

    for (i = 0; i < net_dev->rx_desc_num; i++)
    {
        p = pbuf_alloc(PBUF_RAW, PBUF_POOL_BUFSIZE, PBUF_POOL);
        if (p != NULL)
        {
            net_dev->rx_pbuf_record[i] = (uint32)p;

            gmac_rx_buf_attach(net_dev->gmac_id, (uint8 *)p->payload);
        }
        else
        {
            return OS_FAILURE;
        }
    }
    /* clang-format off */
#if OS_LWIP_HW_OFFLOAD
   
    gmac_chksum_offload_set(net_dev->gmac_id, CHKSUM_OFFLOAD_UDP_TX | CHKSUM_OFFLOAD_UDP_RX |
                                              CHKSUM_OFFLOAD_TCP_TX | CHKSUM_OFFLOAD_TCP_RX |
                                              CHKSUM_OFFLOAD_IP4_TX | CHKSUM_OFFLOAD_IP4_RX |
                                              CHKSUM_OFFLOAD_IP6_TX | CHKSUM_OFFLOAD_IP6_RX);
#endif
    /* clang-format on */

#if PBUF_POOL_BUFSIZE > 1518    //(MTU+header+CRC)
    gmac_jumbo_frame_enable(net_dev->gmac_id);
#endif

    gmac_flow_ctrl_enable(net_dev->gmac_id, 0x1000, 10);
    gmac_mac_addr_set(net_dev->gmac_id, net_dev->dev_addr);
    gmac_intr_enable(net_dev->gmac_id, GMAC_INT_DEFAULT);
    gmac_addr_filter_set(net_dev->gmac_id, FILTER_ADDR_MCAST | FILTER_ADDR_BCAST);
    gmac_tx_enable(net_dev->gmac_id);
    gmac_rx_enable(net_dev->gmac_id);

    /* init phy */
    phy_mmd_clk = net_dev->phy->phy_mmd_max_fre_get(net_dev->gmac_id);
    gmac_phy_addr_set(net_dev->gmac_id, net_dev->phy_addr);
    gmac_phy_mmd_clk_set(net_dev->gmac_id, phy_mmd_clk);
    ret = net_dev->phy->phy_init(net_dev->gmac_id);
    if (ret != 0)
    {
        LOG_E(DBG_TAG, "ERR##: phy_init fail!\n");
    }
    return OS_SUCCESS;
}

#ifdef OS_USING_ETH0
static void __fast eth0_isr()
{
    uint32   status;
    os_err_t result;

    os_hw_interrupt_clear(GMAC0_IRQ_NUM);
    status = gmac_intr_status(eth0_dev->gmac_id);
    gmac_intr_clear(eth0_dev->gmac_id, status);

    if (status & GMAC_INT_AHB_ERR)
    {
        LOG_D(DBG_TAG, "GMAC_INT_AHB_ERR in isr0\n");
    }

    if (status & GMAC_INT_TPKT_LOST)
    {
        LOG_D(DBG_TAG, "GMAC_INT_TPKT_LOST in isr0\n");
    }

    if (status & GMAC_INT_TXBUF_UNAVA)
    {
        LOG_D(DBG_TAG, "GMAC_INT_TXBUF_UNAVA in isr0\n");
    }

    if (status & GMAC_INT_TPKT2E)
    {
        LOG_D(DBG_TAG, "GMAC_INT_TPKT2E in isr0\n");
    }

    /* do receiving routin */
    if (status & (GMAC_INT_RPKT2B | GMAC_INT_RXBUF_UNAVA | GMAC_INT_RPKT_LOST))
    {
        result = eth_device_ready(&eth0_dev->parent);
        if (result != OS_SUCCESS)
            LOG_I(DBG_TAG, "t6x0_rx: notify rx error result = %d", result);
        else
            gmac_intr_disable(eth0_dev->gmac_id, GMAC_INT_DEFAULT);
    }
}

/**
 ***********************************************************************************************************************
 * @brief           ck_eth0_probe:init and register the EMAC device.
 *
 * @param[in]       none
 *
 * @return          Return eth init status.
 * @retval          ERR_OK            init success.
 * @retval          Others            init failed.
 ***********************************************************************************************************************
 */
static int ck_eth0_probe(void)
{
    os_err_t state;
    memset(eth0_dev, 0, sizeof(os_ck_net_t));

    eth0_dev->gmac_id            = GMAC0;
    eth0_dev->tx_desc_num        = ETH0_TX_QUEUE_ENTRIES;
    eth0_dev->rx_desc_num        = ETH0_RX_QUEUE_ENTRIES;
    eth0_dev->link_status.linked = FALSE;
    eth0_dev->tx_pbuf_record     = eth0_tx_pbuf_record;
    eth0_dev->rx_pbuf_record     = eth0_rx_pbuf_record;
    eth0_dev->phy                = &rtl8211_phy_ops;
    eth0_dev->phy_addr           = 0;
    eth0_dev->dev_addr[0]        = 0x18;
    eth0_dev->dev_addr[1]        = 0x60;
    eth0_dev->dev_addr[2]        = 0x24;
    eth0_dev->dev_addr[3]        = 0x91;
    eth0_dev->dev_addr[4]        = 0x06;
    eth0_dev->dev_addr[5]        = 0x54;    // 0x5a

    state = os_ck_eth_hard_init(eth0_dev);
    if (OS_SUCCESS != state)
    {
        LOG_D(DBG_TAG, "os_ck_eth_hard_init failed\r\n");
        return state;
    }
    /* set MAC interrupt number and accorded interrupting handler */
    if (os_hw_interrupt_install(GMAC0_IRQ_NUM, eth0_isr, NULL, "ETH0") != NULL)
    {
        os_hw_interrupt_umask(GMAC0_IRQ_NUM);
    }

    /* init device interface */
    eth0_dev->parent.parent.ops       = &eth_ops;
    eth0_dev->parent.eth_rx           = os_ck_eth_rx;
    eth0_dev->parent.eth_tx           = os_ck_eth_tx;
    eth0_dev->parent.parent.user_data = OS_NULL;
    LOG_D(DBG_TAG, "eth_device_init start\r\n");
    state = eth_device_init(&(eth0_dev->parent), "e0");
    if (OS_SUCCESS == state)
    {
        LOG_D(DBG_TAG, "eth_device_init success\r\n");
    }
    else
    {
        return OS_FAILURE;
    }

    /* start phy monitor */
    eth_device_linkchange(&ck_eth_dev0.parent, OS_FALSE);
    os_task_id tid;
    tid = os_task_create(OS_NULL, OS_NULL, 1024, "eth0", phy_monitor_task_entry, (void *)eth0_dev, OS_TASK_PRIORITY_MAX - 2);
    if (tid != OS_NULL)
    {
        os_task_startup(tid);
    }
    else
    {
        state = OS_FAILURE;
    }
    return state;
}
OS_INIT_CALL(ck_eth0_probe, OS_INIT_LEVEL_COMPONENT, OS_INIT_SUBLEVEL_LOW);

#endif

#ifdef OS_USING_ETH1
static void __fast eth1_isr()
{
    uint32   status;
    os_err_t result;

    os_hw_interrupt_clear(GMAC1_IRQ_NUM);
    status = gmac_intr_status(eth1_dev->gmac_id);
    gmac_intr_clear(eth1_dev->gmac_id, status);

    if (status & GMAC_INT_AHB_ERR)
    {
        LOG_D(DBG_TAG, "GMAC_INT_AHB_ERR in isr0\n");
    }

    if (status & GMAC_INT_TPKT_LOST)
    {
        LOG_D(DBG_TAG, "GMAC_INT_TPKT_LOST in isr0\n");
    }

    if (status & GMAC_INT_TXBUF_UNAVA)
    {
        LOG_D(DBG_TAG, "GMAC_INT_TXBUF_UNAVA in isr0\n");
    }

    if (status & GMAC_INT_TPKT2E)
    {
        LOG_D(DBG_TAG, "GMAC_INT_TPKT2E in isr0\n");
    }

    /* do receiving routin */
    if (status & (GMAC_INT_RPKT2B | GMAC_INT_RXBUF_UNAVA | GMAC_INT_RPKT_LOST))
    {
        result = eth_device_ready(&eth1_dev->parent);
        if (result != OS_SUCCESS)
            LOG_I(DBG_TAG, "t6x0_rx: notify rx error result = %d", result);
        else
            gmac_intr_disable(eth1_dev->gmac_id, GMAC_INT_DEFAULT);
    }
}

/**
 ***********************************************************************************************************************
 * @brief           ck_eth0_probe:init and register the EMAC device.
 *
 * @param[in]       none
 *
 * @return          Return eth init status.
 * @retval          ERR_OK            init success.
 * @retval          Others            init failed.
 ***********************************************************************************************************************
 */
static int ck_eth1_probe(void)
{
    os_err_t state;
    memset(eth1_dev, 0, sizeof(os_ck_net_t));

    eth1_dev->gmac_id            = GMAC1;
    eth1_dev->tx_desc_num        = ETH1_TX_QUEUE_ENTRIES;
    eth1_dev->rx_desc_num        = ETH1_RX_QUEUE_ENTRIES;
    eth1_dev->link_status.linked = FALSE;
    eth1_dev->tx_pbuf_record     = eth1_tx_pbuf_record;
    eth1_dev->rx_pbuf_record     = eth1_rx_pbuf_record;
    eth1_dev->phy                = &rtl8211_phy_ops;
    eth1_dev->phy_addr           = 0;
    eth1_dev->dev_addr[0]        = 0x00;
    eth1_dev->dev_addr[1]        = 0x84;    // T
    eth1_dev->dev_addr[2]        = 0x73;    // I
    eth1_dev->dev_addr[3]        = 0x72;    // H
    eth1_dev->dev_addr[4]        = 0x02;
    eth1_dev->dev_addr[5]        = 0x02;

    state = os_ck_eth_hard_init(eth1_dev);
    if (OS_SUCCESS != state)
    {
        LOG_D(DBG_TAG, "os_ck_eth_hard_init failed\r\n");
        return state;
    }
    /* set MAC interrupt number and accorded interrupting handler */
    if (os_hw_interrupt_install(GMAC1_IRQ_NUM, eth1_isr, NULL, "ETH1") != NULL)
    {
        os_hw_interrupt_umask(GMAC1_IRQ_NUM);
    }

    /* init device interface */
    eth1_dev->parent.parent.ops       = &eth_ops;
    eth1_dev->parent.eth_rx           = os_ck_eth_rx;
    eth1_dev->parent.eth_tx           = os_ck_eth_tx;
    eth1_dev->parent.parent.user_data = OS_NULL;
    LOG_D(DBG_TAG, "eth_device_init start\r\n");
    state = eth_device_init(&(eth1_dev->parent), "e1");
    if (OS_SUCCESS == state)
    {
        LOG_D(DBG_TAG, "eth_device_init success\r\n");
    }
    else
    {
        return OS_FAILURE;
    }

    /* start phy monitor */
    eth_device_linkchange(&ck_eth_dev1.parent, OS_FALSE);
    os_task_id tid;
    tid = os_task_create(OS_NULL, OS_NULL, 1024, "eth1", phy_monitor_task_entry, (void *)eth1_dev, OS_TASK_PRIORITY_MAX - 2);
    if (tid != OS_NULL)
    {
        os_task_startup(tid);
    }
    else
    {
        state = OS_FAILURE;
    }
    return state;
}
OS_INIT_CALL(ck_eth1_probe, OS_INIT_LEVEL_COMPONENT, OS_INIT_SUBLEVEL_LOW);

#endif
#endif
