/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_i2c.c
 *
 * @brief       This file implements i2c driver for hc32.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <board.h>
#include <drv_cfg.h>
#include <device.h>
#include <os_memory.h>

#ifdef OS_USING_I2C
#include "drv_i2c.h"

#define DBG_TAG "drv.i2c"
#include <dlog.h>

os_size_t ck_i2c_master_xfer(struct os_i2c_bus_device *bus, struct os_i2c_msg *msgs, uint32_t num)
{
    uint32_t        msgs_count = 0;
    uint32_t        buf_count  = 0;
    struct os_i2c_msg *i2c_msg;
    uint8_t         dev_addr   = 0;
    uint8_t         i2c_flag   = 0;
    os_err_t           xfer_state = OS_SUCCESS;

    for (msgs_count = 0; msgs_count < num; msgs_count++)
    {
        i2c_msg  = &msgs[msgs_count];
        i2c_flag = msgs[msgs_count].flags;
        dev_addr = msgs[msgs_count].addr;

        if (i2c_is_busy())
        {
            return 0;
        }
        if (i2c_flag == OS_I2C_WR)
        {
            dev_addr = dev_addr << 1 & 0xfe;

            if ((i2c_flag & OS_I2C_NO_START) != 0)
                xfer_state = i2c_send(dev_addr, 0, 0);
            else
                xfer_state = i2c_send(dev_addr, 1, 0);
            if (xfer_state != OS_SUCCESS)
                return 0;

            for (buf_count = 0; buf_count < (i2c_msg->len - 1); buf_count++)
            {
                xfer_state = i2c_send(i2c_msg->buf[buf_count], 0, 0);
                if (xfer_state)
                {
                    return 0;
                }
            }
            xfer_state = i2c_send(i2c_msg->buf[buf_count], 0, 1);
            if (xfer_state)
            {
                return 0;
            }
        }
        else
        {
            dev_addr = dev_addr << 1 | OS_I2C_RD;

            if ((i2c_flag & OS_I2C_NO_START) != 0)
                xfer_state = i2c_send(dev_addr, 0, 0);
            else
                xfer_state = i2c_send(dev_addr, 1, 0);
            if (xfer_state != OS_SUCCESS)
                return 0;

            for (buf_count = 0; buf_count < (i2c_msg->len - 1); buf_count++)
            {
                xfer_state = i2c_recv(&i2c_msg->buf[buf_count], 0);
                if (xfer_state)
                {
                    return 0;
                }
            }
            xfer_state = i2c_recv(&i2c_msg->buf[buf_count], 1);
            if (xfer_state)
            {
                return 0;
            }
        }
    }
    return msgs_count;
}

os_err_t ck_i2c_bus_control(struct os_i2c_bus_device *bus, void *arg)
{
    return OS_SUCCESS;
}

static const struct os_i2c_bus_device_ops ck_i2c_ops = {
    .i2c_transfer       = ck_i2c_master_xfer,
    .i2c_slave_transfer = OS_NULL,
    .i2c_bus_control    = ck_i2c_bus_control,
};

static int ck_i2c_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    os_err_t result = 0;

    i2c_config_t  *info   = (i2c_config_t *)dev->info;
    struct ck_i2c *ck_i2c = os_calloc(1, sizeof(struct ck_i2c));

    OS_ASSERT(ck_i2c);

    ck_i2c->info = info;
    if (I2C_MAX_SPEED > info->speed)
        i2c_hw_init(info);

    struct os_i2c_bus_device *dev_i2c = &ck_i2c->i2c;

    dev_i2c->ops = &ck_i2c_ops;

    result = os_i2c_bus_device_register(dev_i2c, dev->name, dev_i2c);
    OS_ASSERT(result == OS_SUCCESS);

    return result;
}

OS_DRIVER_INFO hc32_i2c_driver = {
    .name  = "I2C_Type",
    .probe = ck_i2c_probe,
};

OS_DRIVER_DEFINE(hc32_i2c_driver, OS_INIT_LEVEL_DEVICE, OS_INIT_SUBLEVEL_HIGH);
#endif