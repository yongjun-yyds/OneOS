/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_uart.c
 *
 * @brief       This file implements uart driver for mm32
 *
 * @revision
 * Date         Author          Notes
 * 2021-05-31   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <drv_uart.h>
#include <os_memory.h>
#include <bus/bus.h>
#include <dma/dma.h>
#include <board.h>
#include <string.h>
#include <drv_intr.h>
#include <oneos_config.h>

#define DRV_EXT_TAG "drv.uart"
#include <drv_log.h>
#include <tih/scu.h>
#include <tih/uart.h>

static os_list_node_t ck_uart_list = OS_LIST_INIT(ck_uart_list);

static void ck_uart_hard_init(ck_uart_info_t *info)
{
    os_ubase_t level = 0;
    os_spin_lock_irqsave(&gs_device_lock, &level);
    ck_uart_t *uart   = os_container_of(info, ck_uart_t, info);

    uart_hw_init(info->uart_num, &info->config);
    uart_fifo_init(info->uart_num);
    uart_intr_enable(info->uart_num, UART_INTR_RX_READY);
    os_hw_interrupt_install(info->irq_num, (os_isr_handler_t)info->irq_handler, (void *)uart, (char *)info->uart_name);
    os_hw_interrupt_umask(info->irq_num);
    os_spin_unlock_irqrestore(&gs_device_lock, level);
}

static void __fast ck_uart_irq_rx_callback(ck_uart_t *uart)
{
    OS_ASSERT(uart->rx_buff != OS_NULL);
    OS_ASSERT(uart->rx_index < uart->rx_size);

    while (uart_rx_ready(uart->info->uart_num))
    {
        uart->rx_buff[uart->rx_index++] = uart_ndelay_getc(uart->info->uart_num);
        if (uart->rx_index == uart->rx_size / 2)
        {
            soft_dma_half_irq(&uart->sdma);
        }
    }
}

void __fast UART_IRQHandler(int vector, void *param)
{
    struct ck_uart *uart = (struct ck_uart *)param;
    uint8_t      intr_src;

    intr_src = uart_intr_status(uart->info->uart_num);
    uart_intr_clear(uart->info->uart_num, intr_src);
    os_hw_interrupt_clear(uart->info->irq_num);
    if (intr_src == UART_INTR_RX_READY)
    {
        ck_uart_irq_rx_callback(uart);
    }
}

/* interrupt rx mode */
static uint32_t __fast ck_sdma_int_get_index(soft_dma_t *dma)
{
    ck_uart_t *uart = os_container_of(dma, ck_uart_t, sdma);
    return uart->rx_index;
}

static os_err_t ck_sdma_int_start(soft_dma_t *dma, void *buff, uint32_t size)
{
    ck_uart_t *uart = os_container_of(dma, ck_uart_t, sdma);

    uart->rx_buff  = buff;
    uart->rx_index = 0;
    uart->rx_size  = size;
    return OS_SUCCESS;
}

static uint32_t ck_sdma_int_stop(soft_dma_t *dma)
{
    ck_uart_t *uart = os_container_of(dma, ck_uart_t, sdma);
    // uart_intr_disable(uart->info->uart_num, UART_INTR_RX_READY);
    return uart->rx_index;
}

/* sdma callback */
static void ck_uart_sdma_callback(soft_dma_t *dma)
{
    ck_uart_t *uart = os_container_of(dma, ck_uart_t, sdma);
    os_hw_serial_isr_rxdone((struct os_serial_device *)uart);
}

static void ck_uart_sdma_init(struct ck_uart *uart, dma_ring_t *ring)
{
    soft_dma_t *dma = &uart->sdma;

    soft_dma_stop(dma);

    dma->hard_info.mode         = HARD_DMA_MODE_NORMAL;
    dma->hard_info.max_size     = 64 * 1024;
    dma->hard_info.flag         = HARD_DMA_FLAG_HALF_IRQ;
    dma->hard_info.data_timeout = uart_calc_byte_timeout_us(uart->serial.config.baud_rate);

    dma->ops.get_index = ck_sdma_int_get_index;
    dma->ops.dma_init  = OS_NULL;
    dma->ops.dma_start = ck_sdma_int_start;
    dma->ops.dma_stop  = ck_sdma_int_stop;

    dma->cbs.dma_half_callback    = ck_uart_sdma_callback;
    dma->cbs.dma_full_callback    = ck_uart_sdma_callback;
    dma->cbs.dma_timeout_callback = ck_uart_sdma_callback;

    soft_dma_init(dma);
    soft_dma_start(dma, ring);
    soft_dma_irq_enable(dma, OS_TRUE);
}

static os_err_t ck_uart_init(struct os_serial_device *serial, struct serial_configure *cfg)
{
    struct ck_uart *uart;
    OS_ASSERT(serial != OS_NULL);
    OS_ASSERT(cfg != OS_NULL);
    os_ubase_t level = 0;

    uart                       = os_container_of(serial, struct ck_uart, serial);
    uart_config_t *uart_config = &(uart->info->config);
    switch (cfg->baud_rate)
    {
    case BAUD_RATE_9600:
    case BAUD_RATE_19200:
    case BAUD_RATE_38400:
    case BAUD_RATE_57600:
    case BAUD_RATE_115200:
        uart_config->baudrate = cfg->baud_rate;
        break;
    default:
        return OS_INVAL;
    }

    switch (cfg->stop_bits)
    {
    case STOP_BITS_1:
        uart_config->stop_bits = UART_STOP_BIT_EQ_1;
        break;
    case STOP_BITS_2:
        uart_config->stop_bits = UART_STOP_BIT_GT_1;
        break;
    default:
        return OS_INVAL;
    }

    switch (cfg->parity)
    {
    case PARITY_NONE:
        uart_config->parity = UART_PARITY_NO;
        break;
    case PARITY_ODD:
        uart_config->parity = UART_PARITY_ODD;
        break;
    case PARITY_EVEN:
        uart_config->parity = UART_PARITY_EVEN;
        break;
    default:
        return OS_INVAL;
    }

    switch (cfg->data_bits)
    {
    case DATA_BITS_5:
        uart_config->word_size = UART_WORD_SIZE_5BIT;
    case DATA_BITS_6:
        uart_config->word_size = UART_WORD_SIZE_6BIT;
    case DATA_BITS_7:
        uart_config->word_size = UART_WORD_SIZE_7BIT;
    case DATA_BITS_8:
        uart_config->word_size = UART_WORD_SIZE_8BIT;
        break;
    default:
        return OS_INVAL;
    }
    uart_hw_init(uart->info->uart_num, uart_config);
    uart_fifo_init(uart->info->uart_num);
    ck_uart_sdma_init(uart, &uart->serial.rx_fifo->ring);

    os_spin_lock_irqsave(&gs_device_lock, &level);
    os_hw_interrupt_mask(uart->info->irq_num);
    uart_intr_enable(uart->info->uart_num, UART_INTR_RX_READY);
    os_hw_interrupt_install(uart->info->irq_num,
                            (os_isr_handler_t)uart->info->irq_handler,
                            (void *)uart,
                            (char *)uart->info->uart_name);
    os_hw_interrupt_umask(uart->info->irq_num);
    os_spin_unlock_irqrestore(&gs_device_lock, level);

    return OS_SUCCESS;
}

static os_err_t ck_uart_deinit(struct os_serial_device *serial)
{
    struct ck_uart *uart;
    os_ubase_t      level = 0;
    OS_ASSERT(serial != OS_NULL);
    uart = os_container_of(serial, struct ck_uart, serial);
    /* close irq */
    soft_dma_stop(&uart->sdma);
    soft_dma_deinit(&uart->sdma);
    os_spin_lock_irqsave(&gs_device_lock, &level);
    uart_intr_disable(uart->info->uart_num, UART_INTR_RX_READY);
    os_hw_interrupt_umask(uart->info->irq_num);
    os_spin_unlock_irqrestore(&gs_device_lock, level);

    return OS_SUCCESS;
}

static int ck_uart_poll_send(struct os_serial_device *serial, const uint8_t *buff, os_size_t size)
{
    struct ck_uart *uart;
    OS_ASSERT(serial != OS_NULL);

    uart = os_container_of(serial, struct ck_uart, serial);

    for (int i = 0; i < size; i++)
    {
        uart_putc(uart->info->uart_num, *buff++);
    }
    return size;
}

static const struct os_uart_ops ck_uart_ops = {
    .init      = ck_uart_init,
    .deinit    = ck_uart_deinit,
    .poll_send = ck_uart_poll_send,
};

static void ck_uart_parse_configs(struct ck_uart *uart)
{
    uart_config_t *config = &(uart->info->config);

    struct os_serial_device *serial = &uart->serial;

    switch (config->baudrate)
    {
    case UART_BAUDRATE_9600:
    case UART_BAUDRATE_19200:
    case UART_BAUDRATE_38400:
    case UART_BAUDRATE_57600:
    case UART_BAUDRATE_115200:
        serial->config.baud_rate = config->baudrate;
        break;
    default:
        break;
    }
    switch (config->stop_bits)
    {
    case UART_STOP_BIT_EQ_1:
        serial->config.stop_bits = STOP_BITS_1;
        break;
    case UART_STOP_BIT_GT_1:
        serial->config.stop_bits = STOP_BITS_2;
        break;
    default:
        break;
    }
    switch (config->parity)
    {
    case UART_PARITY_NO:
        serial->config.parity = PARITY_NONE;
        break;
    case UART_PARITY_ODD:
        serial->config.parity = PARITY_ODD;
        break;
    case UART_PARITY_EVEN:
        serial->config.parity = PARITY_EVEN;
        break;
    default:
        break;
    }
    switch (config->word_size)
    {
    case UART_WORD_SIZE_5BIT:
        serial->config.data_bits = DATA_BITS_5;
        break;
    case UART_WORD_SIZE_6BIT:
        serial->config.data_bits = DATA_BITS_6;
        break;
    case UART_WORD_SIZE_7BIT:
        serial->config.data_bits = DATA_BITS_7;
        break;
    case UART_WORD_SIZE_8BIT:
        serial->config.data_bits = DATA_BITS_8;
        break;
    default:
        break;
    }
}

static int ck_uart_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    struct serial_configure config = OS_SERIAL_CONFIG_DEFAULT;

    os_err_t                 result = 0;
    os_ubase_t                level  = 0;
    struct os_serial_device *serial;
    struct ck_uart          *uart = os_calloc(1, sizeof(struct ck_uart));
    OS_ASSERT(uart);

    uart->info = (struct ck_uart_info *)dev->info;
#ifndef OS_USING_CONSOLE
    ck_uart_hard_init(uart->info);
#endif

    serial         = &uart->serial;
    serial->ops    = &ck_uart_ops;
    serial->config = config;
    ck_uart_parse_configs(uart);

    os_spin_lock_irqsave(&gs_device_lock, &level);
    os_list_add_tail(&ck_uart_list, &uart->list);
    os_spin_unlock_irqrestore(&gs_device_lock, level);

    result = os_hw_serial_register(serial, dev->name, NULL);

    OS_ASSERT(result == OS_SUCCESS);

    return result;
}

OS_DRIVER_INFO ck_uart_driver = {
    .name  = "UART_InitStructure",
    .probe = ck_uart_probe,
};

OS_DRIVER_DEFINE(ck_uart_driver, OS_INIT_LEVEL_PRE_DEVICE, OS_INIT_SUBLEVEL_HIGH);


#ifdef OS_USING_CONSOLE
static uint8_t console_uart = 0;
void              __os_hw_console_output(char *str)
{
    os_ubase_t level = 0;
    os_spin_lock_irqsave(&gs_device_lock, &level);
    uart_send(console_uart, (uint8 *)str, strlen(str));
    os_spin_unlock_irqrestore(&gs_device_lock, level);
}

static int ck_uart_early_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    ck_uart_info_t *info = (ck_uart_info_t *)dev->info;
    ck_uart_hard_init(info);

    if (!strcmp(dev->name, OS_CONSOLE_DEVICE_NAME))
    {
        console_uart = info->uart_num;
    }
    return OS_SUCCESS;
}

OS_DRIVER_INFO ck_uart_early_driver = {
    .name  = "UART_InitStructure",
    .probe = ck_uart_early_probe,
};

OS_DRIVER_DEFINE(ck_uart_early_driver, OS_INIT_LEVEL_PRE_KERNEL_1, OS_INIT_SUBLEVEL_MIDDLE);
#endif

