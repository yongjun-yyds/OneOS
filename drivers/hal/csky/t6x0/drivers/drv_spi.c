/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_spi.c
 *
 * @brief       This file implements SPI driver for gd32.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <board.h>
#include <os_task.h>
#include <device.h>
#include <os_memory.h>
#include <os_stddef.h>
#include <drv_gpio.h>
#include <drv_intr.h>
#include <drv_spi.h>
#include <string.h>
#include <drv_log.h>
#include <spi/spi.h>
#define DBG_TAG "drv.spi"
#include <dlog.h>
#include <tih/spi.h>
#include <tih/scu.h>

static os_err_t ck_spi_init(struct ck_spi *spi_drv)
{
    uint32_t SPI_CLOCK;

    struct os_spi_configuration *cfg;
    ck_spi_info_t               *spi_cfg;
    OS_ASSERT(spi_drv != OS_NULL);
    cfg     = spi_drv->cfg;
    spi_cfg = spi_drv->spi_info;
    OS_ASSERT(cfg != OS_NULL);

    /* datawidth */
    if (cfg->data_width <= 8)
    {
        spi_cfg->t680_spi.word_size = SPI_WORD_SIZE_8BIT;
    }
    else if (cfg->data_width <= 16)
    {
        spi_cfg->t680_spi.word_size = SPI_WORD_SIZE_16BIT;
    }
    else
    {
        return OS_EIO;
    }

    /* baudrate */
    LOG_D(DBG_TAG, "max   freq: %d\n", cfg->max_hz);
    SPI_CLOCK = clock_freq_get(MODULE_SPI_IO);
    if (cfg->max_hz >= SPI_CLOCK / 2)
    {
        spi_cfg->t680_spi.sck_div = 0;
    }
    else if (cfg->max_hz >= SPI_CLOCK / 4)
    {
        spi_cfg->t680_spi.sck_div = SPI_SCK_DIV_2;
    }
    else if (cfg->max_hz >= SPI_CLOCK / 8)
    {
        spi_cfg->t680_spi.sck_div = SPI_SCK_DIV_4;
    }
    else
    {
        spi_cfg->t680_spi.sck_div = 99;
    }

    /* mode */
    switch (cfg->mode & OS_SPI_MODE_3)
    {
    case OS_SPI_MODE_0:
        spi_cfg->t680_spi.cpol = SPI_CPOL_LOW;
        spi_cfg->t680_spi.cpha = SPI_CPHA_1EDGE;
        break;
    case OS_SPI_MODE_1:
        spi_cfg->t680_spi.cpol = SPI_CPOL_LOW;
        spi_cfg->t680_spi.cpha = SPI_CPHA_2EDGE;
        break;
    case OS_SPI_MODE_2:
        spi_cfg->t680_spi.cpol = SPI_CPOL_HIGH;
        spi_cfg->t680_spi.cpha = SPI_CPHA_1EDGE;
        break;
    case OS_SPI_MODE_3:
        spi_cfg->t680_spi.cpol = SPI_CPOL_HIGH;
        spi_cfg->t680_spi.cpha = SPI_CPHA_2EDGE;
        break;
    }

    /* MSB or LSB */
    if (cfg->mode & OS_SPI_MSB)
    {
        spi_cfg->t680_spi.endian = SPI_ENDIAN_MSB;
    }
    else
    {
        spi_cfg->t680_spi.endian = SPI_ENDIAN_LSB;
    }

    spi_hw_init(&spi_cfg->t680_spi);
    return OS_SUCCESS;
}

static os_err_t spi_configure(struct os_spi_device *device, struct os_spi_configuration *configuration)
{
    OS_ASSERT(device != OS_NULL);
    OS_ASSERT(configuration != OS_NULL);

    struct ck_spi *spi_drv = os_container_of(device->bus, struct ck_spi, spi_bus);
    spi_drv->cfg           = configuration;
    return ck_spi_init(spi_drv);
}

/*!
  \brief      send a byte through the SPI interface and return the byte received from the SPI bus
  \param[in]  byte: byte to send
  \param[out] none
  \retval     the value of the received byte
  */
static uint32_t __fast spixfer(struct os_spi_device *device, struct os_spi_message *message)
{
    uint8_t  state = OS_SUCCESS;
    os_size_t   message_length, already_send_length;
    uint32_t send_length;
    uint8_t *recv_buf;
    uint8_t *send_buf;

    OS_ASSERT(device != OS_NULL);
    OS_ASSERT(device->bus != OS_NULL);
    OS_ASSERT(message != OS_NULL);

    // struct ck_spi *spi_drv = os_container_of(device->bus, struct ck_spi, spi_bus);

    if (message->cs_take)
    {
        spi_frame_output_low();    // CS LOW
        LOG_D(DBG_TAG, "spi take cs\n");
    }

    message_length = message->length;
    recv_buf       = message->recv_buf;
    send_buf       = (uint8_t *)message->send_buf;
    while (message_length)
    {
        if (message_length > (uint32_t)(~0))
        {
            send_length    = (uint32_t)(~0);
            message_length = message_length - (uint32_t)(~0);
        }
        else
        {
            send_length    = message_length;
            message_length = 0;
        }

        /* calculate the start address */
        already_send_length = message->length - send_length - message_length;
        send_buf            = (uint8_t *)message->send_buf + already_send_length;
        recv_buf            = (uint8_t *)message->recv_buf + already_send_length;

        if (message->send_buf && message->recv_buf)
        {
            // memset((uint8_t *)recv_buf, 0xff, send_length);
            spi_send_recv(send_buf, recv_buf, send_length);
        }
        else if (message->send_buf)
        {
            state = spi_send(send_buf, send_length);
        }
        else
        {
            state = spi_recv(recv_buf, send_length);
        }

        if (state != OS_SUCCESS)
        {
            LOG_I(DBG_TAG, "spi transfer error : %d", state);
            message->length = 0;
        }
        else
        {
            LOG_D(DBG_TAG, "transfer done");
        }
    }

    if (message->cs_release)
    {
        spi_frame_output_high();
        LOG_D(DBG_TAG, "spi release cs\n");
    }
    return message->length;
}

static const struct os_spi_ops ck_spi_ops = {
    .configure = spi_configure,
    .xfer      = spixfer,
};

static int ck_spi_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    os_err_t result = 0;

    struct ck_spi *t6x0_spi = os_calloc(1, sizeof(struct ck_spi));

    OS_ASSERT(t6x0_spi);

    t6x0_spi->spi_info       = (struct ck_spi_info *)dev->info;
    spi_config_t *spi_config = &(t6x0_spi->spi_info->t680_spi);

    clock_freq_set(MODULE_SPI_IO, 50000000);    // 50M
    clock_enable(MODULE_SPI_IO);
    spi_hw_init(spi_config);

    struct os_spi_bus *spi_bus = &t6x0_spi->spi_bus;
    result                     = os_spi_bus_register(spi_bus, dev->name, &ck_spi_ops);
    OS_ASSERT(result == OS_SUCCESS);

    LOG_D(DBG_TAG, "%s bus init done", dev->name);

    return result;
}

OS_DRIVER_INFO ck_spi_driver = {
    .name  = "SPI_Type",
    .probe = ck_spi_probe,
};

OS_DRIVER_DEFINE(ck_spi_driver, OS_INIT_LEVEL_PRE_DEVICE, OS_INIT_SUBLEVEL_LOW);

