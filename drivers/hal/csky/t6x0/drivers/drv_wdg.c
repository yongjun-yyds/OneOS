/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_wdt.c
 *
 * @brief       This file implements watchdog driver for gd32
 *
 * @revision
 * Date         Author          Notes
 * 2020-01-10   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include <oneos_config.h>
#ifdef OS_USING_WDG

#include <board.h>
#include <drv_log.h>
#include <bus/bus.h>
#include <tih/wdt.h>

#define DBG_TAG "drv.wdt"

struct ck_wdg
{
    os_watchdog_t wdg;
    wdt_config_t *config;
};

static os_err_t ck_wdt_init(os_watchdog_t *wdt)
{
    return OS_SUCCESS;
}

static os_err_t ck_wdt_control(os_watchdog_t *wdt, int cmd, void *arg)
{

    struct ck_wdg *wdg = (struct ck_wdg *)wdt;
    switch (cmd)
    {
    case OS_DEVICE_CTRL_WDT_KEEPALIVE:
        wdt_feed();
        return OS_SUCCESS;

    case OS_DEVICE_CTRL_WDT_SET_TIMEOUT:
        if (wdg->config->seconds != *(uint32_t *)arg)
        {
            /*Limit timeout_time does not exceed 21s*/
            if (*(uint32_t *)arg > 21)
                return OS_NOSYS;
            else
            {
                wdg->config->seconds = *(uint32_t *)arg;
                wdt_hw_init(wdg->config);
            }
        }
        break;

    case OS_DEVICE_CTRL_WDT_GET_TIMEOUT:
        arg = (void *)wdg->config->seconds;
        break;

    case OS_DEVICE_CTRL_WDT_START:
        wdt_start();
        LOG_D(DBG_TAG, "wdt start.");
        break;

    case OS_DEVICE_CTRL_WDT_STOP:
        wdt_stop();
        LOG_D(DBG_TAG, "wdt stop.");
        break;

    default:
        return OS_NOSYS;
    }
    return OS_SUCCESS;
}

const static struct os_watchdog_ops ops = {
    .init    = &ck_wdt_init,
    .control = &ck_wdt_control,
};

static int ck_wdg_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    struct ck_wdg *wdg;

    wdg = os_calloc(1, sizeof(struct ck_wdg));

    OS_ASSERT(wdg);

    wdg->config  = (wdt_config_t *)dev->info;
    wdg->wdg.ops = &ops;

    wdt_hw_init(wdg->config);

    if (os_hw_watchdog_register(&wdg->wdg, dev->name, OS_NULL) != OS_SUCCESS)
    {
        LOG_E(DBG_TAG, "wdt device register failed.");
        return OS_FAILURE;
    }
    LOG_D(DBG_TAG, "wdt device register success.");
    return OS_SUCCESS;
}

OS_DRIVER_INFO ck_wdt_driver = {
    .name  = "WDT_InitStructure",
    .probe = ck_wdg_probe,
};

OS_DRIVER_DEFINE(ck_wdt_driver, OS_INIT_LEVEL_PRE_DEVICE, OS_INIT_SUBLEVEL_LOW);
#endif

