/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_hwtimer.h
 *
 * @brief       This file provides functions declaration for csky timer driver.
 *
 * @revision
 * Date         Author          Notes
 * 2021-04-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __DRV_HWTIMER_H__
#define __DRV_HWTIMER_H__
#include <oneos_config.h>

#ifdef BSP_USING_TIMER
#include <timer/clocksource.h>
#include <timer/clockevent.h>
#include <tih/timer.h>

typedef struct csky_timer_info
{
    uint8_t     tim_num;
    timer_config_t tim_cfg;
} csky_timer_info_t;

typedef struct csky_timer
{
    union _clock
    {
        os_clocksource_t cs;
        os_clockevent_t  ce;
    } clock;

    csky_timer_info_t *info;
    uint32_t        freq;
    os_list_node_t     list;
} csky_timer_t;

#endif
#endif
