/*
 * Copyright (C) 2017 C-SKY Microsystems Co., Ltd. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/******************************************************************************
 * @file     retarget.c
 * @brief    retarget interface
 * @version  V1.0
 * @date     26. Dec 2017
 ******************************************************************************/
#include <os_task.h>
#include <oneos_config.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <minilibc_os.h>
#include <tih/uart.h>
#include <tih/scu.h>

#include <os_util.h>
extern int16_t g_os_sched_lock_cnt;
#if 0
void __fast os_enter_critical(void)
{
    OS_KERNEL_ENTER();
    g_os_sched_lock_cnt++;
    OS_KERNEL_EXIT();
}

/**
 * This function will unlock the thread scheduler.
 */
void __fast os_exit_critical(void)
{
    OS_KERNEL_ENTER();

    g_os_sched_lock_cnt --;

    if (g_os_sched_lock_cnt <= 0)
    {
        g_os_sched_lock_cnt = 0;
        /* enable interrupt */
        OS_KERNEL_EXIT_SCHED();

    }
    else
    {
        /* enable interrupt */
        OS_KERNEL_EXIT();

    }
}

int __fast os_critical_enter(unsigned int *lock)
{
    os_enter_critical();
    return 0;
}

int __fast os_critical_exit(unsigned int *lock)
{
    os_exit_critical();
    return 0;
}
#endif
int fputc(int ch, FILE *stream)
{
#if 0
    os_device_t dev;
    dev = os_console_get_device();
    if (dev == NULL) {
        console_putc(ch);
        return 0;
    }

    uint16_t old_flag = dev->ref_count;
    dev->ref_count |= OS_DEVICE_FLAG_STREAM;
    if (os_device_write(dev, 0, &ch, 1) != 1) {
        dev->ref_count = old_flag;
        return -1;
    }
    dev->ref_count = old_flag;
    return 0;
#else
    console_putc(ch);
    return 0;
#endif
}

int fgetc(FILE *stream)
{
#if 0
    uint8_t ch;
    os_device_t dev;
    dev = os_console_get_device();
    if (dev == NULL) {
        return console_getc();
    }

    uint16_t old_flag = dev->ref_count;
    dev->ref_count |= OS_DEVICE_FLAG_STREAM;
    if (os_device_read(dev, 0, &ch, 1) != 1) {
        dev->ref_count = old_flag;
        return -1;
    }
    dev->ref_count = old_flag;
    return ch;
#else
    return console_getc();
#endif
}

void __stack_chk_fail(void)
{
    os_task_id task;

    task = os_get_current_task();
    os_kprintf("[stack_fail] current task = %s \n", task->name_dummy);

    os_irq_disable();
    while (1)
        ;
}

uint32 __stack_chk_guard(void)
{
    return rand() + scu_sw_cnt_get();
}

#ifdef TIH_TRACE_STUB

static uint32_t      trace_count = 0;
struct tih_trace_point *tih_trace_record;

struct tih_trace_point
{
    os_bool_t   flag;
    uint32_t this_func;
    uint32_t call_site;
    uint32_t hard_clock;
};

void tih_trace_stub_init(void)
{
    tih_trace_record = (struct tih_trace_point *)os_malloc(sizeof(struct tih_trace_point) * 1024);
}
INIT_APP_EXPORT(tih_trace_stub_init);

void __attribute__((__no_instrument_function__)) __fast __cyg_profile_func_enter(void *this_func, void *call_site)
{
    tih_trace_record[trace_count].flag       = 0;
    tih_trace_record[trace_count].this_func  = this_func;
    tih_trace_record[trace_count].call_site  = call_site;
    tih_trace_record[trace_count].hard_clock = scu_sw_cnt_get();
    trace_count++;
}
void __attribute__((__no_instrument_function__)) __fast __cyg_profile_func_exit(void *this_func, void *call_site)
{
    os_base_t trace_int;

    tih_trace_record[trace_count].flag       = 1;
    tih_trace_record[trace_count].this_func  = this_func;
    tih_trace_record[trace_count].call_site  = call_site;
    tih_trace_record[trace_count].hard_clock = scu_sw_cnt_get();
    trace_count++;

    if (trace_count > 1000)
    {
        trace_int = os_hw_interrupt_disable();
        os_kprintf("\nDUMP TRACE RECORD\n");
        for (int i = 0; i < trace_count; i++)
        {
            if (tih_trace_record[i].flag == 0)
            {
                os_kprintf("\nENTER\n%x\n%x\n%u\n",
                           tih_trace_record[i].this_func,
                           tih_trace_record[i].call_site,
                           tih_trace_record[i].hard_clock);
            }
            else
            {
                os_kprintf("\nEXIT\n%x\n%x\n%u\n",
                           tih_trace_record[i].this_func,
                           tih_trace_record[i].call_site,
                           tih_trace_record[i].hard_clock);
            }
        }
        trace_count = 0;
        os_hw_interrupt_enable(trace_int);
    }
}

void tih_trace_flush(void)
{
    os_kprintf("DUMP TRACE RECORD\n");

    for (int i = 0; i < trace_count; i++)
    {
        if (tih_trace_record[i].flag == 0)
        {
            os_kprintf("\nENTER\n%x\n%x\n%u\n",
                       tih_trace_record[i].this_func,
                       tih_trace_record[i].call_site,
                       tih_trace_record[i].hard_clock);
        }
        else
        {
            os_kprintf("\nEXIT\n%x\n%x\n%u\n",
                       tih_trace_record[i].this_func,
                       tih_trace_record[i].call_site,
                       tih_trace_record[i].hard_clock);
        }
    }

    trace_count = 0;
}
// MSH_CMD_EXPORT(tih_trace_flush, flush the trace record);
#endif
