/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_common.c
 *
 * @brief       This file provides systick time init/IRQ and board init functions.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <string.h>
#include <oneos_config.h>
#include "drv_common.h"
#include "board.h"
#include "os_stddef.h"
#include <os_errno.h>
#include <driver.h>
#include <os_memory.h>
#include <os_clock.h>
#include <drv_gpio.h>
#include <tih/wdt.h>
#ifdef OS_USING_CLOCKSOURCE
#include <timer/clocksource.h>
#include <timer/clocksource_cortexm.h>
#endif
extern void hardware_init(void);

static volatile os_bool_t hardware_init_done = OS_FALSE;
volatile uint32_t      os_tick            = 0;

void os_tick_handler(void)
{
    os_tick_increase();
    os_tick++;
#ifdef OS_USING_CLOCKSOURCE
    os_clocksource_update();
#endif
}

/* clang-format off */
void _Error_Handler(char *s, int num)
{
    volatile int loop = 1;
    while (loop);
}
/* clang-format on */

/**
 ***********************************************************************************************************************
 * @brief           This function will initial STM32 board.
 *
 * @param[in]       none
 *
 * @return          none
 ***********************************************************************************************************************
 */
static os_err_t os_hw_board_init(void)
{

#if defined(__CC_ARM) || defined(__CLANG_ARM)
    extern int __Vectors;
    interrupt_stack_addr = *(os_ubase_t *)&__Vectors;
#elif __ICCARM__
#error :not support iccarm
#else
    extern os_ubase_t irq_stack_offset;
    interrupt_stack_addr = &irq_stack_offset;
#endif

    hardware_init_done = OS_FALSE;

    hardware_init();

    /* Heap initialization */
#if defined(OS_USING_HEAP)
    if ((os_size_t)HEAP_END > (os_size_t)HEAP_BEGIN)
    {
        os_default_heap_init();
        os_default_heap_add((void *)HEAP_BEGIN, (os_size_t)HEAP_END - (os_size_t)HEAP_BEGIN, OS_MEM_ALG_DEFAULT);
    }
#endif

#ifdef OS_USING_DMA_RAM
    os_dma_mem_init();
#endif
    return OS_SUCCESS;
}
OS_INIT_CALL(os_hw_board_init, OS_INIT_LEVEL_PRE_KERNEL_1, OS_INIT_SUBLEVEL_HIGH);

static os_err_t board_post_init(void)
{
#ifdef OS_USING_PIN
    os_hw_pin_init();
#endif

    hardware_init_done = OS_TRUE;
    return OS_SUCCESS;
}

OS_INIT_CALL(board_post_init, OS_INIT_LEVEL_POST_KERNEL, OS_INIT_SUBLEVEL_LOW);

void os_hw_cpu_reset(void)
{
    chip_reset(RESET_TARGET_FIRMWARE);
}

#ifdef SOC_T630
#ifdef OS_USING_SHELL
#include <shell.h>
static void download(int argc, char *argv[])
{
    chip_reset(RESET_TARGET_XBOOTROM);
}
SH_CMD_EXPORT(download, download, "download");
#endif
#endif