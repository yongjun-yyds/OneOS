/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_eth.h
 *
 * @brief       This file provides macro declaration for eth driver.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __DRV_ETH_H__
#define __DRV_ETH_H__

#include <os_task.h>
#include <arch_interrupt.h>
#include <device.h>
#include <board.h>
#include <netif/ethernetif.h>
#include <tih/phy.h>
#include <tih/gmac.h>
#include <phy_rtl8211_hw.h>

#define MAX_ADDR_LEN 6

struct os_ck_net
{
    struct eth_device parent;                 /* inherit from ethernet device */
    uint32_t       gmac_id;                /* GMAC0 or GMAC1 */
    uint8_t        dev_addr[MAX_ADDR_LEN]; /* interface address info, hw address */

    uint8_t   tx_desc_num;
    uint32_t *tx_pbuf_record; /* TX pbuf recorder */
    uint8_t   rx_desc_num;
    uint32_t *rx_pbuf_record; /* RX pbuf recorder */

    uint32_t      phy_addr; /* phy addr */
    const phy_ops_t *phy;      /* phy control functions */
    link_status_t    link_status;
};
typedef struct os_ck_net os_ck_net_t;

#endif /* __DRV_ETH_H__ */
