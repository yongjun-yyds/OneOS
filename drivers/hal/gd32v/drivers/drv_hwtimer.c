/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_hwtimer.c
 *
 * @brief       This file implements hwtimer driver for gd32.
 *
 * @revision
 * Date         Author          Notes
 * 2020-09-01   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <os_task.h>

#define LOG_TAG "drv.hwtimer"
#include <drv_log.h>

#include <device.h>
#include <os_memory.h>
#include "drv_hwtimer.h"


static os_list_node_t gd32_timer_list = OS_LIST_INIT(gd32_timer_list);

os_bool_t gd32_timer_is_32b(uint32_t timer)
{
    return OS_FALSE;
}

static void timer_irq_callback(struct gd32_timer *timer)
{
    uint32_t timer_periph = timer->info->timer_periph;

    if (timer_interrupt_flag_get(timer_periph, TIMER_INT_FLAG_UP) == SET)
    {
        timer_interrupt_flag_clear(timer_periph, TIMER_INT_FLAG_UP);
#ifdef OS_USING_CLOCKEVENT
        os_clockevent_isr((os_clockevent_t *)timer);
#endif
    }
}

#define TIMER_IRQHandler_DEFINE(__uart_index)                                                                          \
    void TIMER##__uart_index##_IRQHandler(void)                                                                        \
    {                                                                                                                  \
        struct gd32_timer *timer;                                                                                      \
                                                                                                                       \
        os_list_for_each_entry(timer, &gd32_timer_list, struct gd32_timer, list)                                       \
        {                                                                                                              \
            if (timer->info->timer_periph == TIMER##__uart_index)                                                      \
            {                                                                                                          \
                timer_irq_callback(timer);                                                                             \
                return;                                                                                                \
            }                                                                                                          \
        }                                                                                                              \
    }


TIMER_IRQHandler_DEFINE(1);
TIMER_IRQHandler_DEFINE(2);
TIMER_IRQHandler_DEFINE(3);
TIMER_IRQHandler_DEFINE(4);
TIMER_IRQHandler_DEFINE(5);
TIMER_IRQHandler_DEFINE(6);


static uint64_t gd32_timer_read(void *clock)
{
    struct gd32_timer *timer;

    timer = (struct gd32_timer *)clock;

    return timer_counter_read(timer->info->timer_periph);
}

static void gd32_timer_start(os_clockevent_t *ce, uint32_t prescaler, uint64_t count)
{
    struct gd32_timer *timer;
    uint32_t           timer_periph;
    timer_parameter_struct *timer_initpara;

    OS_ASSERT(ce != OS_NULL);
    OS_ASSERT(prescaler != 0);
    OS_ASSERT(count != 0);

    timer = (struct gd32_timer *)ce;

    timer_periph   = timer->info->timer_periph;
    timer_initpara = &timer->info->timer_initpara;

    timer_initpara->prescaler = prescaler - 1;
    timer_initpara->period    = count;

    timer_deinit(timer_periph);
    timer_init(timer_periph, timer_initpara);
    
    timer_interrupt_flag_clear(timer_periph, TIMER_INT_FLAG_UP);

    timer_enable(timer_periph);
    timer_update_event_enable(timer_periph);
    timer_interrupt_enable(timer_periph, TIMER_INT_UP);
}

static void gd32_timer_stop(os_clockevent_t *ce)
{
    struct gd32_timer *timer;
    uint32_t           timer_periph;

    OS_ASSERT(ce != OS_NULL);

    timer = (struct gd32_timer *)ce;

    timer_periph = timer->info->timer_periph;

    timer_interrupt_disable(timer_periph, TIMER_INT_UP);
    timer_disable(timer_periph);
}

static const struct os_clockevent_ops gd32_tim_ops = {
    .start = gd32_timer_start,
    .stop  = gd32_timer_stop,
    .read  = gd32_timer_read,
};

static uint32_t gd32_clock_freq(struct gd32_timer_info *info)
{
    return SystemCoreClock;
}

static int gd32_timer_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    os_ubase_t level;
    os_err_t  result = 0;

    struct gd32_timer *gd32_timer = os_calloc(1, sizeof(struct gd32_timer));
    OS_ASSERT(gd32_timer);

    struct gd32_timer_info *tim = (struct gd32_timer_info *)dev->info;
    gd32_timer->info            = tim;
    gd32_timer->freq            = gd32_clock_freq(gd32_timer->info);

    rcu_periph_clock_enable(gd32_timer->info->periph);

    if (tim->mode == TIMER_MODE_TIM)
    {
#ifdef OS_USING_CLOCKSOURCE
        if (os_clocksource_best() == OS_NULL)
        {
            tim->timer_initpara.prescaler = 23;
            tim->timer_initpara.period    = 0xfffful;

            timer_deinit(tim->timer_periph);
            timer_init(tim->timer_periph, &tim->timer_initpara);
            timer_enable(tim->timer_periph);

            gd32_timer->clock.cs.rating = 160;
            gd32_timer->clock.cs.freq   = gd32_timer->freq/24;
            gd32_timer->clock.cs.mask   = 0xfffful;
            gd32_timer->clock.cs.read   = gd32_timer_read;

            os_clocksource_register(dev->name, &gd32_timer->clock.cs);
        }
        else
#endif
        {        
#ifdef OS_USING_CLOCKEVENT
            gd32_timer->clock.ce.rating = 160;
            gd32_timer->clock.ce.freq   = gd32_timer->freq;
            gd32_timer->clock.ce.mask   = 0xfffful;

            gd32_timer->clock.ce.prescaler_mask = 0xfffful;
            gd32_timer->clock.ce.prescaler_bits = 16;

            gd32_timer->clock.ce.count_mask = 0xfffful;
            gd32_timer->clock.ce.count_bits = 16;

            gd32_timer->clock.ce.feature  = OS_CLOCKEVENT_FEATURE_PERIOD;
            gd32_timer->clock.ce.min_nsec = NSEC_PER_SEC / gd32_timer->clock.ce.freq;

            gd32_timer->clock.ce.ops = &gd32_tim_ops;

            eclic_irq_enable(gd32_timer->info->nvic_irq, 0, 2);

            os_clockevent_register(dev->name, &gd32_timer->clock.ce);
#endif            
        }
    }

    os_spin_lock_irqsave(&gs_device_lock, &level);
    os_list_add_tail(&gd32_timer_list, &gd32_timer->list);
    os_spin_unlock_irqrestore(&gs_device_lock, level);

    return result;
}


OS_DRIVER_INFO gd32_timer_driver = {
    .name  = "TIMER_Type",
    .probe = gd32_timer_probe,
};

OS_DRIVER_DEFINE(gd32_timer_driver, OS_INIT_LEVEL_PRE_DEVICE, OS_INIT_SUBLEVEL_MIDDLE);

