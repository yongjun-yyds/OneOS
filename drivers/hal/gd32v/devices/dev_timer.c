/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        dev_timer.c
 *
 * @brief       This file implements timer driver for stm32
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#ifdef BSP_USING_TIMER

#include "drv_hwtimer.h"

#ifdef BSP_USING_TIM4

static struct gd32_timer_info timer4_info = {
    .timer_periph = TIMER4,
    .periph       = RCU_TIMER4,
    .nvic_irq     = TIMER4_IRQn,

    .timer_initpara =
        {
            .prescaler         = 0U,
            .alignedmode       = TIMER_COUNTER_EDGE,
            .counterdirection  = TIMER_COUNTER_UP,
            .period            = 65535U,
            .clockdivision     = TIMER_CKDIV_DIV1,
            .repetitioncounter = 0U,


        },
    .mode = TIMER_MODE_TIM,
};
OS_HAL_DEVICE_DEFINE("TIMER_Type", "timer4", timer4_info);
#endif

#ifdef BSP_USING_TIM2
static struct gd32_timer_info timer2_info = {
    .timer_periph = TIMER2,
    .periph       = RCU_TIMER2,
    .nvic_irq     = TIMER2_IRQn,

    .timer_initpara =
        {
            .prescaler         = 0U,
            .alignedmode       = TIMER_COUNTER_EDGE,
            .counterdirection  = TIMER_COUNTER_UP,
            .period            = 65535U,
            .clockdivision     = TIMER_CKDIV_DIV1,
            .repetitioncounter = 0U,
        },
    .mode = TIMER_MODE_TIM,

};
OS_HAL_DEVICE_DEFINE("TIMER_Type", "timer2", timer2_info);
#endif

#ifdef BSP_USING_TIM3
static struct gd32_timer_info timer3_info = {
    .timer_periph = TIMER3,
    .periph       = RCU_TIMER3,
    .nvic_irq     = TIMER3_IRQn,

    .timer_initpara =
        {
            .prescaler         = 0U,
            .alignedmode       = TIMER_COUNTER_EDGE,
            .counterdirection  = TIMER_COUNTER_UP,
            .period            = 65535U,
            .clockdivision     = TIMER_CKDIV_DIV1,
            .repetitioncounter = 0U,

        },
    .mode = TIMER_MODE_TIM,
};
OS_HAL_DEVICE_DEFINE("TIMER_Type", "timer3", timer3_info);
#endif

#ifdef BSP_USING_TIM1
static struct gd32_timer_info timer1_info = {
    .timer_periph = TIMER1,
    .periph       = RCU_TIMER1,
    .nvic_irq     = TIMER1_IRQn,

    .timer_initpara =
        {
            .prescaler         = 0U,
            .alignedmode       = TIMER_COUNTER_EDGE,
            .counterdirection  = TIMER_COUNTER_UP,
            .period            = 65535U,
            .clockdivision     = TIMER_CKDIV_DIV1,
            .repetitioncounter = 0U,

        },
    .mode = TIMER_MODE_TIM,

};
OS_HAL_DEVICE_DEFINE("TIMER_Type", "timer1", timer1_info);
#endif

#ifdef BSP_USING_TIM5
static struct gd32_timer_info timer5_info = {
    .timer_periph = TIMER5,
    .periph       = RCU_TIMER5,
    .nvic_irq     = TIMER5_IRQn,

    .timer_initpara =
        {
            .prescaler         = 0U,
            .alignedmode       = TIMER_COUNTER_EDGE,
            .counterdirection  = TIMER_COUNTER_UP,
            .period            = 65535U,
            .clockdivision     = TIMER_CKDIV_DIV1,
            .repetitioncounter = 0U,

        },
    .mode = TIMER_MODE_TIM,

};
OS_HAL_DEVICE_DEFINE("TIMER_Type", "timer5", timer5_info);
#endif

#ifdef BSP_USING_TIM6
static struct gd32_timer_info timer6_info = {
    .timer_periph = TIMER6,
    .periph       = RCU_TIMER6,
    .nvic_irq     = TIMER6_IRQn,

    .timer_initpara =
        {
            .prescaler         = 0U,
            .alignedmode       = TIMER_COUNTER_EDGE,
            .counterdirection  = TIMER_COUNTER_UP,
            .period            = 65535U,
            .clockdivision     = TIMER_CKDIV_DIV1,
            .repetitioncounter = 0U,

        },
    .mode = TIMER_MODE_TIM,

};
OS_HAL_DEVICE_DEFINE("TIMER_Type", "timer6", timer6_info);
#endif

#endif
