/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_gpio.c
 *
 * @brief       This file implements gpio driver for semidrive.
 *
 * @revision
 * Date         Author          Notes
 * 2023-10-25   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include "drv_gpio.h"
#include <pin/pin.h>
#include <driver.h>
#include <os_memory.h>

#define DRV_EXT_LVL DBG_EXT_INFO
#define DRV_EXT_TAG "drv.gpio"
#include <drv_log.h>

#ifdef OS_USING_PIN

static struct os_pin_irq_hdr pin_irq_hdr_tab[TAISHAN_PIN_NUM] = {0};

/* clk and other init */
static void semidrive_gpio_init(void)
{
    int pin_num = 0;
    for(; pin_num < TAISHAN_PIN_NUM; pin_num++)
    {
        pin_irq_hdr_tab[pin_num].pin  = -1;
        pin_irq_hdr_tab[pin_num].hdr  = OS_NULL;
        pin_irq_hdr_tab[pin_num].mode = 0;
        pin_irq_hdr_tab[pin_num].args = OS_NULL;
    }
}

static void semidrive_pin_write(struct os_device *dev, os_base_t pin, os_base_t value)
{
    sdrv_gpio_set_pin_output_level(pin, value);
}

#define REG_OEN_OFFSET     (0x580u)
#define PIN_OFFSET(pin)    ((pin) / 32 * 0x10)
#define PIN_BIT(pin)       ((pin) % 32)
#define PIN_MASK(pin)      (1ul << PIN_BIT(pin))

static bool gpio_read(paddr_t base, uint32_t reg_offset, uint32_t pin)
{
    uint32_t val = readl(base + reg_offset + PIN_OFFSET(pin));
    return ((val & PIN_MASK(pin)) == 0) ? false : true;
}

static int semidrive_pin_read(struct os_device *dev, os_base_t pin_num)
{
    int value = PIN_LOW;

    uint32_t ctrl_base;
    uint32_t ctrl_index;

    OS_ASSERT(pin_num < TAISHAN_PIN_NUM);

    ctrl_base = TAISHAN_GPIO_GET_BASE(pin_num);
    ctrl_index = TAISHAN_GPIO_GET_INDEX(pin_num);

    if (gpio_read(ctrl_base, REG_OEN_OFFSET, ctrl_index))
    {
        value = sdrv_gpio_get_pin_output_level(pin_num);
    }
    else
    {
        value = sdrv_gpio_read_pin_input_level(pin_num);
    }
    
    return value;
}

static void semidrive_pin_mode(struct os_device *dev, os_base_t pin, os_base_t mode)
{
    pin_settings_config_t pin_init_config = {
   .pin_index         =  0,
   .mux               =  PIN_MUX_ALT0,
   .open_drain        =  PIN_PUSH_PULL,
   .pull_config       =  PIN_NOPULL,
   .drive_strength    =  PIN_DS_8MA,
   .slew_rate         =  PIN_SR_FAST,
   .input_select      =  PIN_IS_CMOS_SCHMITT,
   .force_input       =  PIN_FORCE_INPUT_NORMAL,
   .mode_select       =  PIN_MODE_DIGITAL,

   .initial_value     =  PIN_LEVEL_LOW,
   .data_direction    =  PIN_INPUT_DIRECTION,
   .interrupt_config  =  PIN_INTERRUPT_DISABLED,
   };

    OS_ASSERT(pin < TAISHAN_PIN_NUM);
    
    pin_init_config.pin_index = pin;

    if (mode == PIN_MODE_OUTPUT)
    {
        pin_init_config.data_direction = PIN_OUTPUT_DIRECTION;
        pin_init_config.input_select   = PIN_IS_CMOS;
    }
    else if (mode == PIN_MODE_INPUT)
    {
        pin_init_config.data_direction = PIN_INPUT_DIRECTION;
    }

    else if (mode == PIN_MODE_INPUT_PULLUP)
    {
        pin_init_config.data_direction = PIN_INPUT_DIRECTION;
        pin_init_config.pull_config    = PIN_PULL_UP;
        pin_init_config.initial_value  =  PIN_LEVEL_HIGH;
    }
    else if (mode == PIN_MODE_INPUT_PULLDOWN)
    {
        pin_init_config.data_direction = PIN_INPUT_DIRECTION;
         pin_init_config.pull_config    = PIN_PULL_DOWN;
    }
    else 
    {   
        LOG_I(DRV_EXT_TAG, "not support gpio mode!set as input mode");
        pin_init_config.data_direction = PIN_INPUT_DIRECTION;
    }
    sdrv_pinctrl_init(1, &pin_init_config);
}


void pin_irq_hdr(uint32_t irq, uint32_t pin, void *arg)
{
    if (pin_irq_hdr_tab[pin].hdr)
    {
        pin_irq_hdr_tab[pin].hdr(pin_irq_hdr_tab[pin].args);
    }
}

static os_err_t
semidrive_pin_attach_irq(struct os_device *dev, int32_t pin_num, uint32_t mode, void (*hdr)(void *args), void *args)
{
    os_ubase_t              level;
    gpio_interrupt_e type = GPIO_INTR_DISABLE;

    OS_ASSERT(pin_num < TAISHAN_PIN_NUM);

    os_spin_lock_irqsave(&gs_device_lock, &level);
    if (pin_num < 0)
    {
        os_spin_unlock_irqrestore(&gs_device_lock, level);
        return OS_NOSYS;
    }
    if (pin_irq_hdr_tab[pin_num].pin == pin_num && pin_irq_hdr_tab[pin_num].hdr == hdr &&
        pin_irq_hdr_tab[pin_num].mode == mode && pin_irq_hdr_tab[pin_num].args == args)
    {
        os_spin_unlock_irqrestore(&gs_device_lock, level);
        return OS_SUCCESS;
    }
    if (pin_irq_hdr_tab[pin_num].pin != -1)
    {
        os_spin_unlock_irqrestore(&gs_device_lock, level);
        return OS_BUSY;
    }

    switch (mode)
    {
    case PIN_IRQ_MODE_RISING:
        type = GPIO_INTR_RISING_EDGE;
        break;
    case PIN_IRQ_MODE_FALLING:
        type = GPIO_INTR_FALLING_EDGE;
        break;
    case PIN_IRQ_MODE_RISING_FALLING:
        type = GPIO_INTR_BOTH_EDGE;
        break;
    case PIN_IRQ_MODE_HIGH_LEVEL:
        type = GPIO_INTR_HIGH_LEVEL;
    case PIN_IRQ_MODE_LOW_LEVEL:
        type = GPIO_INTR_LOW_LEVEL;
    default:
        os_spin_unlock_irqrestore(&gs_device_lock, level);
        return OS_FAILURE;
    }

    pin_irq_hdr_tab[pin_num].pin  = pin_num;
    pin_irq_hdr_tab[pin_num].hdr  = hdr;
    pin_irq_hdr_tab[pin_num].mode = mode;
    pin_irq_hdr_tab[pin_num].args = args;

    sdrv_gpio_set_pin_interrupt(pin_num, type);
    sdrv_gpio_register_interrupt_handler(pin_num, pin_irq_hdr, NULL);
    
    os_spin_unlock_irqrestore(&gs_device_lock, level);

    return OS_SUCCESS;
}

static os_err_t semidrive_pin_dettach_irq(struct os_device *device, int32_t pin_num)
{
    os_base_t         level;
    OS_ASSERT(pin_num < TAISHAN_PIN_NUM);

    os_spin_lock_irqsave(&gs_device_lock, &level);
    if (pin_irq_hdr_tab[pin_num].pin == -1)
    {
        os_spin_unlock_irqrestore(&gs_device_lock, level);
        return OS_SUCCESS;
    }
    pin_irq_hdr_tab[pin_num].pin  = -1;
    pin_irq_hdr_tab[pin_num].hdr  = OS_NULL;
    pin_irq_hdr_tab[pin_num].mode = 0;
    pin_irq_hdr_tab[pin_num].args = OS_NULL;
    os_spin_unlock_irqrestore(&gs_device_lock, level);

    return OS_SUCCESS;
}

static os_err_t semidrive_pin_irq_enable(struct os_device *device, os_base_t pin_num, uint32_t enabled)
{
    os_base_t level = 0;
    OS_ASSERT(pin_num < TAISHAN_PIN_NUM);

    if (pin_irq_hdr_tab[pin_num].pin == -1)
    {
        return OS_ENOSYS;
    }

    if (enabled == PIN_IRQ_ENABLE)
    {
        sdrv_gpio_pin_interrupt_enable(pin_num);
    }
    else if (enabled == PIN_IRQ_DISABLE)
    {
        sdrv_gpio_unregister_interrupt_handler(pin_num);
    }
    else
    {
        return OS_ENOSYS;
    }
    return OS_SUCCESS;
}

const static struct os_pin_ops _semidrive_pin_ops = {
    .pin_mode       = semidrive_pin_mode,
    .pin_write      = semidrive_pin_write,
    .pin_read       = semidrive_pin_read,
    .pin_attach_irq = semidrive_pin_attach_irq,
    .pin_detach_irq = semidrive_pin_dettach_irq,
    .pin_irq_enable = semidrive_pin_irq_enable,
};

/**
 ***********************************************************************************************************************
 * @brief           os_hw_pin_init:enable gpio clk,register pin device.
 *
 * @param[in]       none
 *
 * @return          Return init result.
 * @retval          OS_EOK       init success.
 * @retval          Others       init failed.
 ***********************************************************************************************************************
 */

int os_hw_pin_init(void)
{
    semidrive_gpio_init();

    return os_device_pin_register(0, &_semidrive_pin_ops, OS_NULL);
}
#endif
