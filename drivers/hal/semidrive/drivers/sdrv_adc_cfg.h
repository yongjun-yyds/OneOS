/**
 * @file sdrv_adc_cfg.h
 * @brief Semidrive ADC driver config header file.
 *
 * @copyright Copyright (c) 2021  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef SDRV_ADC_CFG_H
#define SDRV_ADC_CFG_H

#define SDRV_ADC_IP_1V0
#define SDRV_ADC_INIT_TIME_VALUE    0

#endif  /* SDRV_ADC_CFG_H */
