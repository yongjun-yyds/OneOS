/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_uart.h
 *
 * @brief        This file provides functions declaration for uart driver.
 *
 * @revision
 * Date         Author          Notes
 * 2021-04-12   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#ifndef __DRV_UART_H__
#define __DRV_UART_H__

#include <os_types.h>
#include <board.h>
#include "sdrv_uart.h"

typedef struct semidrive_uart_info
{
    paddr_t      base; /**< uart module base */
    int          irq;      /**< uart module irq num */
    const pin_settings_config_t  *pin_config;
    uint8_t                       pin_num;
    uint32_t                      irq_priority;
    sdrv_ckgen_node_t            *ckgen;
}semidrive_uart_info_t;



#endif /* __DRV_UART_H__ */
