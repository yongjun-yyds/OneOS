/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_common.c
 *
 * @brief       This file provides systick time init/IRQ and board init functions.
 *
 * @revision
 * Date         Author          Notes
 * 2022-11-21   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include <string.h>
#include <oneos_config.h>
#include <arch_interrupt.h>
#include <os_types.h>
#include <os_stddef.h>
#include <os_memory.h>

#include "board.h"
#include "drv_common.h"
#include <os_clock.h>

#ifdef OS_USING_DMA
#include <dma.h>
#endif

extern sdrv_btm_cfg_t  g_btm_systick_config;

static sdrv_btm_t gs_btm_systick;
int tick_main=0;

static void sdrv_systick_callback(void *arg)
{
    tick_main++;

    os_tick_increase();
}

static void semidrive_systick_start(void)
{
    sdrv_btm_start(&gs_btm_systick, BTM_TYPE_PERIOD, BTM_TIME_MS, 1000 / OS_TICK_PER_SECOND);
}

static void semidrive_systick_stop(void)
{
    sdrv_btm_stop(&gs_btm_systick);
}

static void semidrive_systick_init(void)
{
    sdrv_btm_init(&gs_btm_systick, &g_btm_systick_config);
    sdrv_btm_set_callback(&gs_btm_systick, sdrv_systick_callback, &gs_btm_systick);

    semidrive_systick_start();
}

/**
 ***********************************************************************************************************************
 * @brief           This function will initial board.
 *
 * @param[in]       none
 *
 * @return          none
 ***********************************************************************************************************************
 */
static os_err_t os_hw_board_init(void)
{

#ifdef SCB_EnableICache
    /* Enable I-Cache---------------------------------------------------------*/
    SCB_EnableICache();
#endif

#ifdef SCB_EnableDCache
    /* Enable D-Cache---------------------------------------------------------*/
    SCB_EnableDCache();
#endif

    semidrive_chip_init();
//    os_irq_enable();

#ifdef HAL_SDRAM_MODULE_ENABLED
//    SDRAM_Init();
#endif

    /* Heap initialization */
#if defined(OS_USING_HEAP)
    if ((os_size_t)HEAP_END > (os_size_t)HEAP_BEGIN)
    {
        os_default_heap_init();
        os_default_heap_add((void *)HEAP_BEGIN, (os_size_t)HEAP_END - (os_size_t)HEAP_BEGIN,OS_MEM_ALG_DEFAULT);
    }
#endif

#ifdef OS_USING_DMA_RAM
//    os_dma_mem_init();
#endif

    return OS_SUCCESS;
}
OS_INIT_CALL(os_hw_board_init, OS_INIT_LEVEL_PRE_KERNEL_1, OS_INIT_SUBLEVEL_MIDDLE);

static os_err_t board_post_init(void)
{

#ifdef OS_USING_PIN
     os_hw_pin_init();
#endif

    semidrive_systick_init();

    return OS_SUCCESS;
}
OS_INIT_CALL(board_post_init, OS_INIT_LEVEL_POST_KERNEL, OS_INIT_SUBLEVEL_HIGH);

void os_hw_cpu_reset(void)
{
}


