/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_uart.c
 *
 * @brief       This file implements uart driver for SemiDrive
 *
 * @revision
 * Date         Author          Notes
 * 2021-05-31   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include "drv_uart.h"
#include "drv_gpio.h"

#include <os_memory.h>
#include <bus/bus.h>
#include <dma/dma.h>
#include <board.h>
#include <string.h>

#define DRV_EXT_LVL DBG_EXT_INFO
#define DRV_EXT_TAG "drv.uart"
#include <drv_log.h>

typedef struct semidrive_uart
{
    struct os_serial_device     serial;
    struct semidrive_uart_info *info;
    sdrv_uart_t                 uart;
    sdrv_uart_config_t          uart_config;

    soft_dma_t                  sdma;
    uint32_t                    sdma_hard_size;

    uint8_t                 *rx_buff;
    uint32_t                 rx_index;
    uint32_t                 rx_size;

    const uint8_t           *tx_buff;
    uint32_t                 tx_count;
    uint32_t                 tx_size;

    os_list_node_t              list;
}semidrive_uart_t;

static os_list_node_t semidrive_uart_list = OS_LIST_INIT(semidrive_uart_list);

static void rx_callback(sdrv_uart_t *ctrl, sdrv_uart_callback_status_e status, void *userData)
{
    uint16_t size = 0;
    struct semidrive_uart *suart = (struct semidrive_uart *)userData;

    if (userData == OS_NULL)
        return;

    if (status == SDRV_UART_RxFWF || status == SDRV_UART_RxFifoOverFlow)
    {
       // while(sdrv_uart_get_rxfifolen(suart->uart))
        size = sdrv_uart_get_rxfifodata(&suart->uart, &suart->rx_buff[suart->rx_index], suart->rx_size-suart->rx_index);
        suart->rx_index += size;

        if ((suart->rx_index >= suart->rx_size / 2) && ((suart->rx_index - size) < suart->rx_size / 2))
        {
            soft_dma_half_irq(&suart->sdma);
        }
        
        if (suart->rx_size == suart->rx_index)
        {
            suart->rx_index = 0;
            soft_dma_full_irq(&suart->sdma);
        }
    }
    if(status != SDRV_UART_RxFWF)
    {
        LOG_E(DRV_EXT_TAG, "err in rx_irq,status:%d",status);
    }
}

static void async_tx_callback(sdrv_uart_t *ctrl, sdrv_uart_callback_status_e status, void *userData)
{
    struct semidrive_uart *uart = (struct semidrive_uart *)userData;
    
    if (userData == OS_NULL)
        return;
    
    if (SDRV_UART_TxCompleted == status)
    {
        os_hw_serial_isr_txdone((struct os_serial_device *)uart);
    }
    else
    {
        LOG_E(DRV_EXT_TAG, "err in tx_irq");
    }
}

static void semidrive_uart_hard_init(semidrive_uart_t *suart)
{
    struct semidrive_uart_info *info = suart->info;
    /* config default uart_config */
    suart->uart_config.base = info->base;
    suart->uart_config.irq  = info->irq;
    suart->uart_config.baud       = 115200;
    suart->uart_config.data_bits  = SDRV_UART_CHAR_8_BITS;
    suart->uart_config.stop_bits  = SDRV_UART_STOP_1_BIT;
    suart->uart_config.parity     = SDRV_UART_NO_PARITY;
#if CONFIG_UART_ENABLE_DMA
    suart->uart_config = SDRV_IDLE_DETECT_1024CYCLE;
#endif

    sdrv_pinctrl_init(info->pin_num, info->pin_config);
    suart->uart_config.clk_freq = sdrv_ckgen_get_rate(info->ckgen);
    sdrv_uart_controller_init(&suart->uart, &suart->uart_config, rx_callback, OS_NULL);
}

/* interrupt rx mode */
static uint32_t semidrive_sdma_int_get_index(soft_dma_t *dma)
{
    semidrive_uart_t *suart = os_container_of(dma, semidrive_uart_t, sdma);

    return suart->rx_index;
}

static os_err_t semidrive_sdma_int_start(soft_dma_t *dma, void *buff, uint32_t size)
{
    semidrive_uart_t *suart = os_container_of(dma, semidrive_uart_t, sdma);

    suart->rx_buff  = buff;
    suart->rx_index = 0;
    suart->rx_size  = size;

    sdrv_uart_stop_realtime_receive(&suart->uart);
    sdrv_uart_start_realtime_receive(&suart->uart);

    return OS_SUCCESS;
}

static uint32_t semidrive_sdma_int_stop(soft_dma_t *dma)
{
    semidrive_uart_t *suart = os_container_of(dma, semidrive_uart_t, sdma);

    //sdrv_uart_get_rxfifodata();

    sdrv_uart_stop_realtime_receive(&suart->uart);

    return suart->rx_index;
}

/* sdma callback */
static void semidrive_uart_sdma_callback(soft_dma_t *dma)
{
    semidrive_uart_t *suart = os_container_of(dma, semidrive_uart_t, sdma);

    os_hw_serial_isr_rxdone((struct os_serial_device *)&suart->serial);
}

static void semidrive_uart_sdma_init(struct semidrive_uart *suart, dma_ring_t *ring)
{
    soft_dma_t *dma = &suart->sdma;

    soft_dma_stop(dma);
    
    memset(&dma->hard_info, 0, sizeof(dma->hard_info));

    dma->hard_info.max_size     = 64 * 1024;
    dma->hard_info.flag         = HARD_DMA_FLAG_HALF_IRQ | HARD_DMA_FLAG_FULL_IRQ;// | HARD_DMA_FLAG_TIMEOUT_IRQ;
    dma->hard_info.data_timeout = uart_calc_byte_timeout_us(suart->serial.config.baud_rate);

    if (suart->uart_config.dma_en == 0)
    {
        dma->hard_info.mode = HARD_DMA_MODE_NORMAL;
        dma->ops.get_index  = semidrive_sdma_int_get_index;
        dma->ops.dma_init   = OS_NULL;
        dma->ops.dma_start  = semidrive_sdma_int_start;
        dma->ops.dma_stop   = semidrive_sdma_int_stop;
    }
    else
    {
    #if 0
        dma->hard_info.mode = HARD_DMA_MODE_NORMAL;
        dma->ops.get_index = semidrive_sdma_dma_get_index;
        dma->ops.dma_init  = OS_NULL;
        dma->ops.dma_start = semidrive_sdma_dma_start;
        dma->ops.dma_stop  = semidrive_sdma_dma_stop;
    #endif
    }

    dma->cbs.dma_half_callback    = semidrive_uart_sdma_callback;
    dma->cbs.dma_full_callback    = semidrive_uart_sdma_callback;
    dma->cbs.dma_timeout_callback = semidrive_uart_sdma_callback;

    soft_dma_init(dma);
    soft_dma_start(dma, ring);
    soft_dma_irq_enable(dma, OS_TRUE);
}

static os_err_t semidrive_uart_init(struct os_serial_device *serial, struct serial_configure *cfg)
{
    OS_ASSERT(serial != OS_NULL);
    OS_ASSERT(cfg != OS_NULL);

    struct semidrive_uart *suart = (struct semidrive_uart *)serial;

    sdrv_uart_config_t *uconfig = &suart->uart_config;

    uconfig->baud = cfg->baud_rate;

    switch (cfg->stop_bits)
    {
    case STOP_BITS_1:
        uconfig->stop_bits = SDRV_UART_STOP_1_BIT;
        break;
    case STOP_BITS_2:
        uconfig->stop_bits = SDRV_UART_STOP_2_BIT;
        break;
    default:
        return OS_INVAL;
    }
    switch (cfg->parity)
    {
    case PARITY_NONE:
        uconfig->parity = SDRV_UART_NO_PARITY;
        break;
    case PARITY_EVEN:
        uconfig->parity = SDRV_UART_EVEN_PARITY;
        break;
    case PARITY_ODD:
        uconfig->parity = SDRV_UART_ODD_PARITY;
        break;
    default:
        return OS_INVAL;
    }

    switch (cfg->data_bits)
    {
    case DATA_BITS_5:
        uconfig->data_bits = SDRV_UART_CHAR_5_BITS;
        break;
    case DATA_BITS_6:
        uconfig->data_bits = SDRV_UART_CHAR_6_BITS;
        break;
    case DATA_BITS_7:
        uconfig->data_bits = SDRV_UART_CHAR_7_BITS;
        break;
    case DATA_BITS_8:
        uconfig->data_bits = SDRV_UART_CHAR_8_BITS;
        break;
    default:
        return OS_INVAL;
    }

    sdrv_uart_controller_init(&suart->uart, uconfig, rx_callback, suart);
    irq_set_priority(suart->info->irq, suart->info->irq_priority);

    semidrive_uart_sdma_init(suart, &suart->serial.rx_fifo->ring);

    return OS_SUCCESS;
}

static os_err_t semidrive_uart_deinit(struct os_serial_device *serial)
{
    OS_ASSERT(serial != OS_NULL);

    struct semidrive_uart *suart = (struct semidrive_uart *)serial;

    soft_dma_stop(&suart->sdma);
    sdrv_uart_deinit(&suart->uart);
    return OS_SUCCESS;
}

static int semidrive_uart_start_send(struct os_serial_device *serial, const uint8_t *buff, os_size_t size)
{
    OS_ASSERT(serial != OS_NULL);

    struct semidrive_uart *suart = (struct semidrive_uart *)serial;

    suart->tx_count = 0;
    suart->tx_buff  = (uint8_t *)buff;
    suart->tx_size  = size;

    sdrv_uart_async_transmit(&suart->uart, buff, size, async_tx_callback, suart);

    return size;
}

static int semidrive_uart_poll_send(struct os_serial_device *serial, const uint8_t *buff, os_size_t size)
{
    os_ubase_t level;
    os_size_t send_size = 0;
    OS_ASSERT(serial != OS_NULL);

    struct semidrive_uart *suart = (struct semidrive_uart *)serial;

    level = os_irq_lock();
    sdrv_uart_sync_transmit(&suart->uart, buff, size, &send_size, (uint32_t)WaitForever);
    os_irq_unlock(level);

    return size;
}
/* clang-format on */

static const struct os_uart_ops semidrive_uart_ops = {
    .init   = semidrive_uart_init,
    .deinit = semidrive_uart_deinit,

    .start_send = semidrive_uart_start_send,
    .poll_send  = semidrive_uart_poll_send,
};

static void semidrive_uart_parse_configs(struct semidrive_uart *suart)
{
    struct os_serial_device *serial = &suart->serial;

    serial->config.baud_rate = suart->uart_config.baud;

    switch (suart->uart_config.stop_bits)
    {
    case SDRV_UART_STOP_1_BIT:
        serial->config.stop_bits = STOP_BITS_1;
        break;
    case SDRV_UART_STOP_2_BIT:
        serial->config.stop_bits = STOP_BITS_2;
        break;
    }
    switch (suart->uart_config.parity)
    {
    case SDRV_UART_NO_PARITY:
        serial->config.parity = PARITY_NONE;
        break;
    case SDRV_UART_ODD_PARITY:
        serial->config.parity = PARITY_ODD;
        break;
    case SDRV_UART_EVEN_PARITY:
        serial->config.parity = PARITY_EVEN;
        break;
    }

    switch (suart->uart_config.data_bits)
    {
    case SDRV_UART_CHAR_5_BITS:
        serial->config.data_bits = DATA_BITS_5;
        break;
    case SDRV_UART_CHAR_6_BITS:
        serial->config.data_bits = DATA_BITS_6;
        break;
    case SDRV_UART_CHAR_7_BITS:
        serial->config.data_bits = DATA_BITS_7;
        break;
    case SDRV_UART_CHAR_8_BITS:
        serial->config.data_bits = DATA_BITS_8;
        break;
    }
}

static int semidrive_uart_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    os_ubase_t level;
    struct serial_configure config = OS_SERIAL_CONFIG_DEFAULT;

    struct os_serial_device *serial = OS_NULL;
    //extern void os_uart_print(int num);
    //os_uart_print(0x24);

    struct semidrive_uart   *suart = os_calloc(1, sizeof(struct semidrive_uart));
    if (suart == NULL)
    {
        LOG_E(DRV_EXT_TAG, "uart mem call failed");
        return OS_FAILURE;
    }
    
    suart->info = (struct semidrive_uart_info *)dev->info;
    semidrive_uart_hard_init(suart);

    serial         = &suart->serial;
    serial->ops    = &semidrive_uart_ops;
    serial->config = config;
    semidrive_uart_parse_configs(suart);

    level = os_irq_lock();
    os_list_add_tail(&semidrive_uart_list, &suart->list);
    os_irq_unlock(level);

    if(os_hw_serial_register(serial, dev->name, NULL) != OS_SUCCESS)
        return OS_FAILURE;

    return OS_SUCCESS;
}

OS_DRIVER_INFO semidrive_uart_driver = {
    .name  = "semidrive_uart",
    .probe = semidrive_uart_probe,
};
OS_DRIVER_DEFINE(semidrive_uart_driver, OS_INIT_LEVEL_PRE_DEVICE, OS_INIT_SUBLEVEL_HIGH);

#ifdef OS_USING_CONSOLE
static semidrive_uart_t console_uart = {0};
/* clang-format off */
void __os_hw_console_output(char *str)
{
    if (console_uart.info->base == 0)
        return;

    uint8_t length = strlen(str);

    if (length)
        sdrv_uart_sync_transmit(&console_uart.uart, str, length, 0, (uint32_t)WaitForever);
}
/* clang-format on */

static int semidrive_uart_early_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    if (!strcmp(dev->name, OS_CONSOLE_DEVICE_NAME))
    {
        console_uart.info = (semidrive_uart_info_t *)dev->info;

        semidrive_uart_hard_init(&console_uart);
    }
    return OS_SUCCESS;
}

OS_DRIVER_INFO semidrive_uart_early_driver = {
    .name  = "semidrive_uart",
    .probe = semidrive_uart_early_probe,
};
OS_DRIVER_DEFINE(semidrive_uart_early_driver, OS_INIT_LEVEL_PRE_KERNEL_1, OS_INIT_SUBLEVEL_LOW);
#endif





