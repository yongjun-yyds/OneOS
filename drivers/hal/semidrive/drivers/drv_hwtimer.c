/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_hwtimer.c
 *
 * @brief       This file implements timer driver for stm32.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <bus/bus.h>
#include <os_memory.h>
#include <timer/timer.h>

#ifdef OS_USING_CLOCKSOURCE
#include <timer/clocksource.h>
#endif

#ifdef OS_USING_CLOCKEVENT
#include <timer/clockevent.h>
#endif

#include <drv_hwtimer.h>

static os_list_node_t semidrive_timer_list = OS_LIST_INIT(semidrive_timer_list);

void timer_handle(void *args)
{
    struct semidrive_timer *timer = (struct semidrive_timer *)args;

#ifdef OS_USING_CLOCKEVENT
    os_clockevent_isr((os_clockevent_t *)timer);
#endif
}

#if defined(OS_USING_CLOCKSOURCE) || defined(OS_USING_CLOCKEVENT)
static uint64_t semidrive_timer_read(void *clock)
{
    struct semidrive_timer *timer;
    sdrv_btm_t             *tim_ctrl;

    timer = (struct semidrive_timer *)clock;
    tim_ctrl = &(timer->btm_handle);

    return sdrv_btm_get_counter(tim_ctrl);
}

#endif

#ifdef OS_USING_CLOCKEVENT
static void semidrive_timer_start(os_clockevent_t *ce, uint32_t prescaler, uint64_t count)
{
    struct semidrive_timer *timer;
    sdrv_btm_t             *tim_ctrl;

    OS_ASSERT(prescaler != 0);
    OS_ASSERT(count != 0);

    timer    = (struct semidrive_timer *)ce;
    tim_ctrl = &timer->btm_handle;

    timer->config.tmr_cfg.si_val = prescaler - 1;

    sdrv_btm_init(tim_ctrl, &timer->config);
    sdrv_btm_set_callback(tim_ctrl, timer_handle, (void *)timer);
    sdrv_btm_hw_timer_start(tim_ctrl->config->base, tim_ctrl->config->tmr_id, count - 1);
    sdrv_btm_hw_compare_int_en(tim_ctrl->config->base, tim_ctrl->config->tmr_id, false);
    //sdrv_btm_lld_timer_start(tim_ctrl->config->base, tim_ctrl->config->tmr_id, count - 1);
}

static void semidrive_timer_stop(os_clockevent_t *ce)
{
    struct semidrive_timer *timer;
    sdrv_btm_t             *tim_ctrl;

    OS_ASSERT(ce != OS_NULL);

    timer = (struct semidrive_timer *)ce;
    tim_ctrl = &(timer->btm_handle);

    sdrv_btm_stop(tim_ctrl);
}

static const struct os_clockevent_ops semidrive_tim_ops = {
    .start = semidrive_timer_start,
    .stop  = semidrive_timer_stop,
    .read  = semidrive_timer_read,
};
#endif

/**
 ***********************************************************************************************************************
 * @brief           semidrive_tim_probe:probe timer device.
 *
 * @param[in]       none
 *
 * @return          Return timer probe status.
 * @retval          OS_SUCCESS         timer register success.
 * @retval          OS_FAILURE       timer register failed.
 ***********************************************************************************************************************
 */
static int semidrive_tim_probe(const os_driver_info_t *drv,const os_device_info_t *dev)
{
    struct semidrive_timer *timer;
    semidrive_tim_info_t   *time_info;
    sdrv_btm_cfg_t         *config;

    timer = os_calloc(1, sizeof(struct semidrive_timer));
    OS_ASSERT(timer);

    time_info = (semidrive_tim_info_t *)dev->info;

    config = &timer->config;
    
    config->base   = time_info->base;
    config->irq    = time_info->irq;
    config->tmr_id = time_info->tmr_id;
    
    config->tmr_cfg.si_val             = 0;
    config->tmr_cfg.inc_val            = 1;
    config->tmr_cfg.frc_rld_rst_cnt_en = true;
    config->tmr_cfg.term_use_mode      = SDRV_BTM_DIRECT;
    config->tmr_cfg.cmp_use_mode       = SDRV_BTM_DIRECT;
    config->tmr_cfg.cnt_dir            = SDRV_BTM_CNT_UP;
    config->tmr_cfg.cnt_mode           = SDRV_BTM_CONTINOUS_MODE;

    timer->btm_handle.config = &timer->config;

    timer->freq = sdrv_ckgen_bus_get_rate(CLK_NODE(g_ckgen_bus_cr5_sf), CKGEN_BUS_CLK_OUT_P);

#ifdef OS_USING_CLOCKSOURCE
    if (os_clocksource_best() == OS_NULL)
    {
        config->tmr_cfg.si_val = 0;
        config->irq            = 0;

        config->tmr_cfg.cnt_dir = SDRV_BTM_CNT_UP;

        sdrv_btm_init(&timer->btm_handle, &timer->config);
        if(timer->btm_handle.status != BTM_STATUS_BUSY)
        {
            sdrv_btm_hw_timer_start(config->base, config->tmr_id, 0xfffffffful);
            sdrv_btm_hw_compare_int_en(config->base, config->tmr_id, false);    
            //sdrv_btm_lld_timer_start(config->base, config->tmr_id, 0xfffffffful);
            
            timer->btm_handle.status = BTM_STATUS_BUSY;
        }

        timer->clock.cs.rating = 320;
        timer->clock.cs.freq   = timer->freq;
        timer->clock.cs.mask   = 0xffffffffull;
        timer->clock.cs.read   = semidrive_timer_read;

        os_clocksource_register(dev->name, &timer->clock.cs);
    }
    else
#endif
    {
#ifdef OS_USING_CLOCKEVENT
        timer->clock.ce.rating = 320;
        timer->clock.ce.freq   = timer->freq;
        timer->clock.ce.mask   = 0xffffffffull;
        
        timer->clock.ce.prescaler_mask = 0xff;
        timer->clock.ce.prescaler_bits = 8;

        timer->clock.ce.count_mask = 0xffffffffull;
        timer->clock.ce.count_bits = 32;
        timer->clock.ce.feature = OS_CLOCKEVENT_FEATURE_PERIOD;
        timer->clock.ce.min_nsec = NSEC_PER_SEC / timer->clock.ce.freq;

        timer->clock.ce.ops = &semidrive_tim_ops;
        os_clockevent_register(dev->name, &timer->clock.ce);
#endif
    }

    os_list_add(&semidrive_timer_list, &timer->list);

    return OS_SUCCESS;
}

OS_DRIVER_INFO semidrive_tim_driver = {
    .name  = "btm_timer",
    .probe = semidrive_tim_probe,
};

OS_DRIVER_DEFINE(semidrive_tim_driver, OS_INIT_LEVEL_PRE_DEVICE, OS_INIT_SUBLEVEL_MIDDLE);

