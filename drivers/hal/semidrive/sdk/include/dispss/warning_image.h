/**
 * @file warning_image.h
 * @brief SemiDrive Warning Image header file.
 *
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef WARNING_IMAGE_H__
#define WARNING_IMAGE_H__

static const struct {
    unsigned int width;
    unsigned int height;
    char format[10];
    unsigned char y[2400];
    unsigned char u[1];
    unsigned char v[1];
} warning_image = {
    40,
    30,
    "RGB565",
    {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0x08, 0xA2, 0x00, 0x80, 0x00, 0x81, 0x18, 0xC6, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x80, 0x10, 0xE4,
     0x08, 0xE3, 0x10, 0xE5, 0x00, 0x80, 0x18, 0xC6, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0x08, 0xA2, 0x08, 0xC3, 0x08, 0xE2, 0x00, 0xC1, 0x00, 0xC1,
     0x10, 0xE5, 0x00, 0x80, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xDE, 0xFF, 0x00, 0x80,
     0x10, 0xE4, 0x00, 0xC1, 0x00, 0xF8, 0x00, 0xC1, 0x08, 0xE2, 0x08, 0xC3,
     0x08, 0xA2, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x80, 0x10, 0xE5, 0x00, 0xC1, 0x00, 0xF8,
     0x00, 0x00, 0x00, 0xC1, 0x00, 0xC1, 0x10, 0xE4, 0x00, 0x80, 0xF8, 0xC6,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x10, 0xA4,
     0x08, 0xC2, 0x08, 0xE2, 0x00, 0xF8, 0x00, 0x41, 0x14, 0xA5, 0x00, 0x00,
     0x00, 0xF8, 0x00, 0xC1, 0x10, 0xE4, 0x00, 0x80, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xDE, 0xFF, 0x00, 0x80, 0x10, 0xE5, 0x00, 0xC1,
     0x00, 0xC1, 0x00, 0x00, 0xFF, 0xFF, 0x14, 0xA5, 0x00, 0x41, 0x00, 0xE1,
     0x08, 0xE2, 0x08, 0xC2, 0x08, 0xA3, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0x00, 0x80, 0x10, 0xE4, 0x00, 0xC1, 0x00, 0xF8, 0x00, 0x00, 0xDE, 0xFF,
     0xF8, 0xC6, 0xFF, 0xFF, 0x08, 0x01, 0x00, 0xC1, 0x00, 0xC1, 0x10, 0xE4,
     0x00, 0x80, 0xDE, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x18, 0xC5, 0x00, 0xA1, 0x10, 0xE3,
     0x00, 0xC1, 0x00, 0x80, 0x10, 0x84, 0xDE, 0xFF, 0xF8, 0xC6, 0xF8, 0xC6,
     0xFF, 0xFF, 0x00, 0x00, 0x00, 0xF8, 0x00, 0xC1, 0x10, 0xE3, 0x00, 0x80,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0x00, 0x80, 0x10, 0xE5, 0x00, 0xC1, 0x00, 0xF8, 0x00, 0x00,
     0xFF, 0xFF, 0xF8, 0xC6, 0xF8, 0xC6, 0xF8, 0xC6, 0xF8, 0xC6, 0x10, 0x84,
     0x00, 0x61, 0x00, 0xE1, 0x08, 0xE3, 0x00, 0xA1, 0x10, 0xA4, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x08, 0x81, 0x10, 0xE4,
     0x00, 0xC2, 0x00, 0xF8, 0x00, 0x00, 0xF8, 0xC6, 0xF8, 0xC6, 0xDE, 0xFF,
     0xDE, 0xFF, 0xDE, 0xFF, 0xDE, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0xE1,
     0x00, 0xC1, 0x10, 0xE4, 0x00, 0x80, 0xDE, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0x18, 0xC6, 0x00, 0x80, 0x10, 0xE4, 0x00, 0xC1, 0x00, 0xA1,
     0x08, 0x42, 0xDE, 0xFF, 0x18, 0xC6, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
     0x08, 0x42, 0xDE, 0xFF, 0xDE, 0xFF, 0x00, 0x00, 0x00, 0xF8, 0x00, 0xC1,
     0x10, 0xE3, 0x00, 0x80, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x80,
     0x10, 0xE5, 0x00, 0xC1, 0x00, 0xF8, 0x00, 0x00, 0xFF, 0xFF, 0xF8, 0xC6,
     0xF8, 0xC6, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x10, 0x84, 0xFF, 0xFF,
     0xDE, 0xFF, 0x08, 0x42, 0x00, 0x80, 0x00, 0xC1, 0x08, 0xE2, 0x00, 0xA1,
     0x18, 0xC5, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0x08, 0xA2, 0x08, 0xC3, 0x08, 0xE2, 0x00, 0xE1,
     0x00, 0x00, 0x18, 0xC6, 0xF8, 0xC6, 0xF8, 0xC6, 0xDE, 0xFF, 0x00, 0x00,
     0x00, 0x00, 0x00, 0x00, 0x10, 0x84, 0xFF, 0xFF, 0xDE, 0xFF, 0xFF, 0xFF,
     0x00, 0x00, 0x00, 0xF8, 0x00, 0xC1, 0x10, 0xE4, 0x00, 0x80, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xF8, 0xC6,
     0x00, 0x80, 0x10, 0xE4, 0x00, 0xC1, 0x00, 0xC1, 0x08, 0x21, 0xFF, 0xFF,
     0xF8, 0xC6, 0xDE, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
     0x14, 0xA5, 0xFF, 0xFF, 0xDE, 0xFF, 0xFF, 0xFF, 0x18, 0xC6, 0x00, 0x00,
     0x00, 0xF8, 0x00, 0xC1, 0x08, 0xE2, 0x00, 0xA1, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x80, 0x10, 0xE4, 0x00, 0xC1,
     0x00, 0xE1, 0x00, 0x00, 0xDE, 0xFF, 0xF8, 0xC6, 0xDE, 0xFF, 0xF8, 0xC6,
     0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x14, 0xA5, 0xFF, 0xFF,
     0xDE, 0xFF, 0xDE, 0xFF, 0xFF, 0xFF, 0x08, 0x22, 0x00, 0x80, 0x00, 0xC1,
     0x08, 0xE2, 0x00, 0x80, 0x18, 0xC6, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0x10, 0xC3, 0x08, 0xC2, 0x08, 0xE2, 0x00, 0xE1, 0x00, 0x41, 0x14, 0xA5,
     0xF8, 0xC6, 0xDE, 0xFF, 0xF8, 0xC6, 0xDE, 0xFF, 0xFF, 0xFF, 0x00, 0x00,
     0x00, 0x00, 0x00, 0x00, 0x18, 0xC6, 0xDE, 0xFF, 0xDE, 0xFF, 0xDE, 0xFF,
     0xDE, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0xF8, 0x00, 0xC1, 0x08, 0xE3,
     0x00, 0x80, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xDE, 0xFF, 0x00, 0x80, 0x10, 0xE4,
     0x00, 0xC1, 0x00, 0xE1, 0x00, 0x00, 0xFF, 0xFF, 0xDE, 0xFF, 0xF8, 0xC6,
     0xF8, 0xC6, 0xDE, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
     0xF8, 0xC6, 0xDE, 0xFF, 0xDE, 0xFF, 0xDE, 0xFF, 0xDE, 0xFF, 0xFF, 0xFF,
     0x18, 0xA6, 0x00, 0x41, 0x00, 0xF8, 0x00, 0xC1, 0x08, 0xE2, 0x08, 0xA2,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0x00, 0x80, 0x10, 0xE5, 0x00, 0xE1, 0x00, 0xE1, 0x00, 0x00,
     0xDE, 0xFF, 0xF8, 0xC6, 0xF8, 0xC6, 0xDE, 0xFF, 0xDE, 0xFF, 0xDE, 0xFF,
     0xFF, 0xFF, 0x08, 0x63, 0x08, 0x42, 0x00, 0x00, 0xDE, 0xFF, 0xDE, 0xFF,
     0xDE, 0xFF, 0xDE, 0xFF, 0xDE, 0xFF, 0xDE, 0xFF, 0xFF, 0xFF, 0x08, 0x01,
     0x00, 0xA1, 0x00, 0xE1, 0x08, 0xE2, 0x00, 0x80, 0x18, 0xC6, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x18, 0xE5, 0x00, 0xA2,
     0x08, 0xE2, 0x00, 0xC1, 0x00, 0x80, 0x10, 0x43, 0xDE, 0xFF, 0xDE, 0xFF,
     0xDE, 0xFF, 0xDE, 0xFF, 0xDE, 0xFF, 0xDE, 0xFF, 0xFF, 0xFF, 0x10, 0x63,
     0x00, 0x41, 0x08, 0x42, 0xDE, 0xFF, 0xDE, 0xFF, 0xDE, 0xFF, 0xDE, 0xFF,
     0xDE, 0xFF, 0xDE, 0xFF, 0xDE, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0xE1,
     0x00, 0xC1, 0x08, 0xE3, 0x00, 0x80, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x80, 0x10, 0xE4, 0x00, 0xC1, 0x00, 0xF8,
     0x00, 0x00, 0xFF, 0xFF, 0xDE, 0xFF, 0xDE, 0xFF, 0xDE, 0xFF, 0xDE, 0xFF,
     0xDE, 0xFF, 0xFF, 0xFF, 0x10, 0x63, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
     0x00, 0x00, 0xFF, 0xFF, 0xDE, 0xFF, 0xDE, 0xFF, 0xDE, 0xFF, 0xDE, 0xFF,
     0xDE, 0xFF, 0xFF, 0xFF, 0x10, 0x85, 0x00, 0x41, 0x00, 0xE1, 0x08, 0xE1,
     0x08, 0xC1, 0x08, 0xA2, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x81,
     0x10, 0xE4, 0x00, 0xC1, 0x00, 0xF8, 0x00, 0x00, 0x18, 0xC6, 0xDE, 0xFF,
     0xDE, 0xFF, 0xDE, 0xFF, 0xDE, 0xFF, 0xDE, 0xFF, 0xDE, 0xFF, 0xFF, 0xFF,
     0x14, 0xA5, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x08, 0x42, 0xFF, 0xFF,
     0xDE, 0xFF, 0xDE, 0xFF, 0xDE, 0xFF, 0xDE, 0xFF, 0xDE, 0xFF, 0xDE, 0xFF,
     0xFF, 0xFF, 0x00, 0x00, 0x00, 0xE1, 0x00, 0xC1, 0x08, 0xE2, 0x00, 0x80,
     0xDE, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0x18, 0xE5, 0x00, 0x80, 0x10, 0xE3, 0x00, 0xC1,
     0x00, 0x80, 0x08, 0x22, 0xDE, 0xFF, 0xDE, 0xFF, 0xDE, 0xFF, 0xDE, 0xFF,
     0xDE, 0xFF, 0xDE, 0xFF, 0xDE, 0xFF, 0xDE, 0xFF, 0xFF, 0xFF, 0x14, 0xA5,
     0x08, 0x42, 0x08, 0x63, 0xFF, 0xFF, 0xDE, 0xFF, 0xDE, 0xFF, 0xDE, 0xFF,
     0xDE, 0xFF, 0xDE, 0xFF, 0xDE, 0xFF, 0xDE, 0xFF, 0xDE, 0xFF, 0xDE, 0xFF,
     0x00, 0x00, 0x00, 0xF8, 0x00, 0xC1, 0x08, 0xE2, 0x00, 0x80, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0x00, 0x80, 0x10, 0xE4, 0x00, 0xC1, 0x00, 0xF8, 0x00, 0x00, 0xFF, 0xFF,
     0xFF, 0xFF, 0xDE, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x10, 0x84, 0x00, 0x80,
     0x00, 0xC1, 0x08, 0xE1, 0x00, 0xC1, 0x10, 0xA3, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xDE, 0xFF, 0xFF, 0xFF, 0x08, 0xA2, 0x08, 0xC3, 0x00, 0xE2,
     0x00, 0xC1, 0x00, 0x61, 0x08, 0x02, 0x10, 0x43, 0x08, 0x43, 0x08, 0x43,
     0x08, 0x43, 0x08, 0x43, 0x08, 0x43, 0x08, 0x43, 0x08, 0x43, 0x08, 0x43,
     0x08, 0x43, 0x08, 0x43, 0x08, 0x43, 0x08, 0x43, 0x08, 0x43, 0x08, 0x43,
     0x08, 0x43, 0x08, 0x43, 0x08, 0x43, 0x08, 0x43, 0x08, 0x43, 0x08, 0x43,
     0x08, 0x43, 0x08, 0x43, 0x10, 0x64, 0x00, 0x00, 0x00, 0xE1, 0x00, 0xC1,
     0x08, 0xE2, 0x00, 0x80, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xDE, 0xFF,
     0xFF, 0xFF, 0x00, 0x80, 0x10, 0xE5, 0x00, 0xC1, 0x00, 0xC1, 0x00, 0xE1,
     0x00, 0xE1, 0x00, 0xE1, 0x00, 0xE1, 0x00, 0xE1, 0x00, 0xE1, 0x00, 0xE1,
     0x00, 0xE1, 0x00, 0xE1, 0x00, 0xE1, 0x00, 0xE1, 0x00, 0xE1, 0x00, 0xE1,
     0x00, 0xE1, 0x00, 0xE1, 0x00, 0xE1, 0x00, 0xE1, 0x00, 0xE1, 0x00, 0xE1,
     0x00, 0xE1, 0x00, 0xE1, 0x00, 0xE1, 0x00, 0xE1, 0x00, 0xE1, 0x00, 0xE1,
     0x00, 0xC1, 0x00, 0xF8, 0x00, 0xC1, 0x00, 0xC1, 0x08, 0xE1, 0x00, 0xA1,
     0x10, 0xE4, 0xFF, 0xFF, 0xFF, 0xFF, 0xDE, 0xFF, 0xF8, 0xC6, 0x08, 0xA2,
     0x08, 0xA1, 0x08, 0xE3, 0x10, 0xE3, 0x10, 0xE3, 0x10, 0xE3, 0x10, 0xE3,
     0x10, 0xE3, 0x10, 0xE3, 0x10, 0xE3, 0x10, 0xE3, 0x10, 0xE3, 0x10, 0xE2,
     0x10, 0xE2, 0x10, 0xE2, 0x10, 0xE2, 0x08, 0xE2, 0x08, 0xE2, 0x08, 0xE2,
     0x08, 0xE2, 0x08, 0xE2, 0x08, 0xE2, 0x08, 0xE2, 0x08, 0xE2, 0x08, 0xE1,
     0x08, 0xE2, 0x08, 0xE2, 0x08, 0xE2, 0x00, 0xE2, 0x08, 0xE2, 0x08, 0xE2,
     0x08, 0xE2, 0x00, 0xE2, 0x08, 0xC1, 0x00, 0x80, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xDE, 0xFF, 0xF8, 0xC6, 0xF8, 0xC6, 0x10, 0xC4, 0x08, 0x81,
     0x08, 0xA1, 0x08, 0xA1, 0x08, 0xA1, 0x08, 0xA1, 0x08, 0xA1, 0x08, 0xA1,
     0x08, 0xA1, 0x08, 0xA1, 0x08, 0xA1, 0x08, 0xA1, 0x08, 0xA1, 0x08, 0xA1,
     0x08, 0xA1, 0x08, 0xA1, 0x08, 0xA1, 0x08, 0xA1, 0x08, 0xA1, 0x08, 0xA1,
     0x08, 0xA1, 0x08, 0xA1, 0x08, 0xA1, 0x08, 0xA1, 0x08, 0xA1, 0x08, 0xA1,
     0x08, 0xA1, 0x08, 0xA1, 0x08, 0xA1, 0x08, 0xA1, 0x08, 0xA1, 0x08, 0x81,
     0x10, 0xA3, 0xFF, 0xFF, 0xDE, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xDE, 0xFF, 0xDE, 0xFF, 0xDE, 0xFF, 0xFF, 0xFF, 0xDE, 0xFF, 0xDE, 0xFF,
     0xDE, 0xFF, 0xDE, 0xFF, 0xDE, 0xFF, 0xDE, 0xFF, 0xDE, 0xFF, 0xDE, 0xFF,
     0xDE, 0xFF, 0xDE, 0xFF, 0xDE, 0xFF, 0xDE, 0xFF, 0xDE, 0xFF, 0xDE, 0xFF,
     0xDE, 0xFF, 0xDE, 0xFF, 0xDE, 0xFF, 0xDE, 0xFF, 0xDE, 0xFF, 0xDE, 0xFF,
     0xDE, 0xFF, 0xDE, 0xFF, 0xDE, 0xFF, 0xDE, 0xFF, 0xDE, 0xFF, 0xDE, 0xFF,
     0xDE, 0xFF, 0xDE, 0xFF, 0xDE, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xDE, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xDE, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF},
    {0x00},
    {0x00}};

#endif // WARNING_IMAGE_H__ */
