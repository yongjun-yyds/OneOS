/**
 * @file disp.h
 * @brief SemiDrive Display header file.
 *
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef DISP_CTRL_H__
#define DISP_CTRL_H__
#include <asw/asw_g_use.h>
#include <sdrv_common.h>
#include <types.h>

#define PLL_LVDS_MAX 1200000000 /* PLL_LVDS_MAX 1.2GHz*/

typedef enum { PIPE_TYPE_SPIPE, PIPE_TYPE_GPIPE, PIPE_TYPE_NUM } pipe_type_t;

typedef enum { MLC_LAYER_SP, MLC_LAYER_GP, MLC_LAYER_NUM } mlc_layer_index;

typedef enum {
    MLC_PATH_BG,
    MLC_PATH_SP,
    MLC_PATH_GP,
    MLC_PATH_NUM
} mlc_path_index;

enum { CLEAR_SPIPE_LAYER = 1, CLEAR_GPIPE_LAYER, CLEAR_ALL_LAYERS };

enum {
    IF_TYPE_INVALID = -1,
    IF_TYPE_NONE,
    IF_TYPE_LVDS,
    IF_TYPE_INTERNAL = IF_TYPE_LVDS,
};

enum { LVDS_MAP_FORMAT_NC = -1, LVDS_MAP_FORMAT_JEIDA, LVDS_MAP_FORMAT_SWPG };

enum { CSI_DC_1_1 = 0, CSI_DC_1_2, CSI_DC_1_3, CSI_DC_1_4 };

typedef enum { LCM_POLARITY_RISING = 0, LCM_POLARITY_FALLING = 1 } LCM_POLARITY;

enum { CRC32_MODE_INTERNAL = 0, CRC32_MODE_EXTERNAL = 1 };

/**
 * @brief Dispss status return code
 */
enum {
    SDRV_DISPSS_STATUS_OK = SDRV_STATUS_OK,
    SDRV_DISPSS_STATUS_FAIL = SDRV_STATUS_FAIL,
    SDRV_DISPSS_STATUS_INVALID_PARAM =
        SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_DISPSS, 1),
    SDRV_DISPSS_STATUS_TIMEOUT = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_DISPSS, 2),

};

typedef struct {
    int x;
    int y;
    int w;
    int h;
} rect_t;

struct rle_info {
    int en;
    int data_size;
    int y_len;
    int y_checksum;
};

/**@brief sdrv display surface struct*/
struct surface {
    int id;    /**< layer identifier(0 or 1)*/
    int dirty; /**< Is this layer changed*/
    int en; /**< the layer enables identification, input layer is processed only
               if this value is true*/
    int fmt;               /**< input image format*/
    rect_t src;            /**< the rect_t struct src includes the source image
                              information(x,y,w,h)*/
    unsigned long addr[4]; /**< input image memory address, corresponding to
                              YUVA four-component address respectively*/
    int src_stride[4];     /**< image component pitch value*/
    rect_t start;
    rect_t dst;   /**< positon information in display*/
    int ckey_en;  /**< color key enable identifier*/
    int ckey;     /**< color key alpha value*/
    int alpha_en; /**< global alpha enable identifier*/
    char alpha;   /**< global layer alpha value*/
    int z_order;  /**< layer order(higher values above)*/
    int security;

    struct rle_info rle;
};

struct crc32block {
    unsigned int block_use;
    unsigned int cfg_start_x;
    unsigned int cfg_start_y;
    unsigned int cfg_end_x;
    unsigned int cfg_end_y;
    unsigned int expect_crc;
};

struct crc_info {
    int block_num;
    int mode; /**< 0:internal 1:external*/
    struct crc32block *blocks;
};

/**@brief sdrv display post config struct*/
struct post_cfg {
    char num_layers;        /**< the numbers of layers*/
    struct surface *layers; /**< layers config information*/
};

struct tcon_timing {
    unsigned int hact;
    unsigned int htol;
    unsigned int hsbp;
    unsigned int hsync;

    unsigned int vact;
    unsigned int vtol;
    unsigned int vsbp;
    unsigned int vsync;
};

/**@brief sdrv display timing*/
struct display_timing {
    unsigned int tcon_clk;     /**< DC TCON CLK*/
    unsigned int hactive;      /**< horizontal active size*/
    unsigned int hfront_porch; /**< horizontal front porch size*/
    unsigned int hback_porch;  /**< horizontal back porch size*/
    unsigned int hsync_len;    /**< horizontal sync len*/
    unsigned int vactive;      /**< vertical active size */
    unsigned int vfront_porch; /**< vertical front porch size*/
    unsigned int vback_porch;  /**< vertical back porch size*/
    unsigned int vsync_len;    /**< vertical sync len*/
    float fps;                 /**< refresh rate*/

    LCM_POLARITY dsp_clk_pol; /**< CLK polarity*/
    LCM_POLARITY de_pol;      /**< DE polarity*/
    LCM_POLARITY vsync_pol;   /**< vsync polarity*/
    LCM_POLARITY hsync_pol;   /**< hsync polarity*/

    unsigned int map_format;
};

struct dc_config {
    paddr_t base;
    int irq;
    struct crc_info *crc32;
};

/**@brief sdrv DC device struct*/
struct dc_dev {
    paddr_t base;                    /**< DC register base address*/
    int irq;                         /**< DC irq number*/
    const struct dc_operations *ops; /**< DC function operations*/
    struct sdrv_panel *panel;        /**< sdrv panel struct*/
    int last_zorder[2];              /**< DC layers*/
    struct asw_app_dc app;
    struct crc_info *crc32;       /**< DC CRC*/
    void (*vsync_callback)(void); /**< DC vsync callback function*/
};

struct dc_operations {
    int (*init)(struct dc_dev *dev);
    int (*reset)(struct dc_dev *dev);
    int (*update)(struct dc_dev *dev, struct post_cfg *post);
    int (*update_layer)(struct dc_dev *dev, struct surface *layer);
    void (*triggle)(struct dc_dev *dev);
    unsigned int (*check_triggle_status)(struct dc_dev *dev);
    void (*vsync_enable)(struct dc_dev *dev, bool enable);
    void (*enable)(struct dc_dev *dev);
    int (*irq_handler)(uint32_t irq, void *arg);
    void (*hsdk_enable)(struct dc_dev *dev, int mode, bool enable);
    int (*csi_mask_ratio_set)(struct dc_dev *dev, int mask_count, int ratio);
    int (*csi_timing_dectet_enable)(struct dc_dev *dev, bool enable);
    void (*set_tcon_after_detect)(struct dc_dev *dev, int ratio_num);
    void (*tcon_auto_adj_enable)(struct dc_dev *dev, bool enable);
    void (*tcon_csi_delay_set)(struct dc_dev *dev);
    void (*update_csi_detect)(struct dc_dev *dev, struct surface *surface,
                              int mask_count, int ratio, bool enable,
                              bool pll_mode);
    void (*set_bg_color)(struct dc_dev *dev, int color);
    void (*clear_layers)(struct dc_dev *dev, uint8_t id_mask, uint8_t z_order);
};

/**@brief sdrv panel struct*/
struct sdrv_panel {
    const char *panel_name; /**< the name of panel*/
    int if_type;            /**< interface type of panel*/
    int cmd_intf;  /**< the interface type which send command to panel IC*/
    int pixel_bpp; /**< bit per pixel*/
    struct display_timing *timing; /**< panel timing*/
};

extern struct dc_dev g_dc_dev;
extern struct dc_operations dc_ops;

status_t sdrv_paral_to_csi_en(bool enable);

status_t sdrv_disp_mux_init(struct sdrv_panel *panel);

status_t sdrv_crc_mode_set(bool is_internal);

status_t sdrv_lvds_init(struct sdrv_panel *panel);

/**
 * @brief sdrv display clk set.
 *
 * This function set display pixel clk.
 *
 * @param [in] clk lvds pixel clk.
 * @return SDRV_DISPSS_STATUS_OK display clk set successfully,
 * SDRV_DISPSS_STATUS_INVALID_PARAM clk is invalid param.
 */
status_t sdrv_display_set_pixelclk(int clk);

/**
 * @brief sdrv disp interface init.
 *
 * This function init display interface.
 *
 * @param [in] panel display panel param.
 * @return SDRV_DISPSS_STATUS_OK panel probe successfully,
 * SDRV_DISPSS_STATUS_INVALID_PARAM panel is invalid param.
 */
status_t sdrv_panel_probe(struct sdrv_panel *panel);

/**
 * @brief sdrv display init.
 *
 * This function init display hardware.
 *
 * @param [in] dev DC device struct.
 * @return SDRV_DISPSS_STATUS_OK display init successfully.
 */
status_t sdrv_display_init(struct dc_dev *dev);

/**
 * @brief lvds channel enable.
 *
 * This function control the lvds channel output.
 *
 * @param [in] enable false->disable; true->enable.
 */
void sdrv_lvds_channel_enable(bool enable);

/**
 * @brief sdrv display rgb565 logo.
 *
 * This function display a static logo.
 *
 * @param [in] dev DC device struct.
 * @param [in] width logo width.
 * @param [in] height logo height.
 * @param [in] addr the memory addr of logo data.
 *
 */
void sdrv_display_rgb565_logo(struct dc_dev *dev, unsigned int width,
                              unsigned int height, unsigned char const *addr);

/**
 * @brief sdrv display blank.
 *
 * This function display a blank frame.
 *
 * @param [in] dev DC device struct.
 */
void sdrv_display_blank(struct dc_dev *dev);

/**
 * @brief get display width.
 *
 * This function obtains display width.
 *
 * @param [in] dev DC device struct.
 * @return the hactive of tcon timing.
 */
int sdrv_display_get_width(struct dc_dev *dev);

/**
 * @brief get display height.
 *
 * This function obtains display height.
 *
 * @param [in] dev DC device struct.
 * @return the vactive of tcon timing.
 */
int sdrv_display_get_height(struct dc_dev *dev);

/**
 * @brief set display background color.
 *
 * This function set the background color of display.
 *
 * @param [in] dev DC device struct.
 * @param [in] color background color value.
 */
void sdrv_display_set_bg_color(struct dc_dev *dev, int color);

/**
 * @brief refresh display single surface.
 *
 * This function implements display configuration and refresh display.
 *
 * @param [in] dev DC device struct.
 * @param [in] layer display surface param.
 *
 * @return SDRV_DISPSS_STATUS_OK display surface successfully,
 * SDRV_DISPSS_STATUS_TIMEOUT display surface timeout.
 */
status_t sdrv_display_surface(struct dc_dev *dev, struct surface *layer);

/**
 * @brief noblock refresh display surface.
 *
 * This function implements display configuration and nonblock refresh display
 * surface.
 *
 * @param [in] dev DC device struct.
 * @param [in] layer surface param.
 *
 * @return SDRV_DISPSS_STATUS_OK display surface successfully,
 * SDRV_DISPSS_STATUS_FAIL display surface fail.
 */
status_t sdrv_display_surface_noblock(struct dc_dev *dev,
                                      struct surface *layer);

/**
 * @brief refresh display.
 *
 * This function implements display configuration and refresh display.
 *
 * @param [in] dev DC device struct.
 * @param [in] post display config param.
 *
 * @return SDRV_DISPSS_STATUS_OK display successfully,
 * SDRV_DISPSS_STATUS_TIMEOUT display timeout.
 */
status_t sdrv_display_post(struct dc_dev *dev, struct post_cfg *post);

/**
 * @brief noblock refresh display.
 *
 * This function implements display configuration and nonblock refresh display.
 *
 * @param [in] dev DC device struct.
 * @param [in] post display config param.
 *
 * @return SDRV_DISPSS_STATUS_OK display successfully,
 * SDRV_DISPSS_STATUS_FAIL display fail.
 */
status_t sdrv_display_post_noblock(struct dc_dev *dev, struct post_cfg *post);

/**
 * @brief dc probe.
 *
 * @param [in] dev DC device struct.
 * @param [in] dc_cfg dc config param.
 *
 */
void sdrv_dc_probe(struct dc_dev *dev, const struct dc_config *dc_cfg);

/**
 * @brief asw-dc handshake mode enable with update layer.
 *
 * This function implements display configuration and enable asw-dc handshake
 * mode.
 *
 * @param [in] dev DC device struct.
 * @param [in] mode HSDK MODE:
 *  0:BUF_SIZE_8LINES; 1: BUF_SIZE_16LINES; 2:BUF_SIZE_32LINES;
 * 3:BUF_SIZE_64LINES
 * @param [in] layer surface param.
 */
void sdrv_dc_handshake_enable(struct dc_dev *dev, int mode,
                              struct surface *layer);

/**
 * @brief asw-dc handshake mode disable with g-pipe disable.
 *
 * This function enable asw-dc handshake mode.
 *
 * @param [in] dev DC device struct.
 * @param [in] mode HSDK MODE:
 *  0:BUF_SIZE_8LINES; 1: BUF_SIZE_16LINES; 2:BUF_SIZE_32LINES;
 * 3:BUF_SIZE_64LINES
 * @param [in] zorder g-pipe layer zorder
 */
void sdrv_dc_handshake_disable(struct dc_dev *dev, int mode, uint8_t zorder);

/**
 * @brief update dc detect.
 *
 * @param [in] dev DC device struct.
 * @param [in] dc_cfg dc config param.
 *
 */
void sdrv_update_csi_detect(struct dc_dev *dev, struct surface *layer,
                            int mask_count, int ratio, bool enable,
                            bool pll_mode);

/**
 * @brief tcon auto adj enable.
 *
 * @param [in] dev DC device struct.
 * @param [in] enable enable or disable.
 *
 */
void sdrv_tcon_auto_adj_enable(struct dc_dev *dev, bool enable);
#endif /* DISP_CTRL_H__ */
