/**
 * @file disp_reg_rw.h
 * @brief SemiDrive Display REG RW header file.
 *
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef DISP_REG_RW__
#define DISP_REG_RW__

#include <reg.h>

static inline unsigned int reg_value(unsigned int val, unsigned int src,
                                     unsigned int shift, unsigned int mask)
{
    return (src & ~mask) | ((val << shift) & mask);
}

static inline unsigned int disp_read(unsigned long base, unsigned int reg)
{
    return readl(base + reg);
}

static inline void disp_write(unsigned long base, unsigned int reg,
                              unsigned int value)
{
    writel(value, base + reg);
}

#endif /* DISP_REG_RW__ */
