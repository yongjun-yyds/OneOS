/**
 * @file dispss_log.h
 * @brief SemiDrive Display Log header file.
 *
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef DISPSS_LOG_H__
#define DISPSS_LOG_H__
#include <debug.h>

#define DISPSS_LOG_ENABLE 1

#define DISPSS_LOG_LEVEL_EMERG SSDK_EMERG  /*System is unusable*/
#define DISPSS_LOG_LEVEL_ALERT SSDK_ALERT  /*Action must be taken immediately*/
#define DISPSS_LOG_LEVEL_CRIT SSDK_CRIT    /*Critical conditions*/
#define DISPSS_LOG_LEVEL_ERR SSDK_ERR      /*Error conditions*/
#define DISPSS_LOG_LEVEL_WARN SSDK_WARNING /*Warning conditions*/
#define DISPSS_LOG_LEVEL_NOTICE                                                \
    SSDK_NOTICE                         /*Normal, but significant, conditions*/
#define DISPSS_LOG_LEVEL_INFO SSDK_INFO /*Informational message*/
#define DISPSS_LOG_LEVEL_DEBUG SSDK_DEBUG /*Debug-level message*/

/*log level default*/
#define DISPSS_LOG_LEVEL DISPSS_LOG_LEVEL_INFO

#define DISPSS_LOG_EMERG(string, args...)                                      \
    if (DISPSS_LOG_ENABLE && (DISPSS_LOG_LEVEL >= DISPSS_LOG_LEVEL_EMERG)) {   \
        ssdk_printf(DISPSS_LOG_LEVEL_EMERG,                                    \
                    "[DISPSS]EMERG|%s| " string "\r\n", __func__, ##args);     \
    }

#define DISPSS_LOG_ALERT(string, args...)                                      \
    if (DISPSS_LOG_ENABLE && (DISPSS_LOG_LEVEL >= DISPSS_LOG_LEVEL_ALERT)) {   \
        ssdk_printf(DISPSS_LOG_LEVEL_ALERT,                                    \
                    "[DISPSS]ALERT|%s| " string "\r\n", __func__, ##args);     \
    }

#define DISPSS_LOG_CRIT(string, args...)                                       \
    if (DISPSS_LOG_ENABLE && (DISPSS_LOG_LEVEL >= DISPSS_LOG_LEVEL_CRIT)) {    \
        ssdk_printf(DISPSS_LOG_LEVEL_CRIT, "[DISPSS]CRIT|%s| " string "\r\n",  \
                    __func__, ##args);                                         \
    }

#define DISPSS_LOG_ERR(string, args...)                                        \
    if (DISPSS_LOG_ENABLE && (DISPSS_LOG_LEVEL >= DISPSS_LOG_LEVEL_ERR)) {     \
        ssdk_printf(DISPSS_LOG_LEVEL_ERR, "[DISPSS]ERR|%s| " string "\r\n",    \
                    __func__, ##args);                                         \
    }

#define DISPSS_LOG_WARN(string, args...)                                       \
    if (DISPSS_LOG_ENABLE && (DISPSS_LOG_LEVEL >= DISPSS_LOG_LEVEL_WARN)) {    \
        ssdk_printf(DISPSS_LOG_LEVEL_WARN, "[DISPSS]WARN|%s| " string "\r\n",  \
                    __func__, ##args);                                         \
    }

#define DISPSS_LOG_NOTICE(string, args...)                                     \
    if (DISPSS_LOG_ENABLE && (DISPSS_LOG_LEVEL >= DISPSS_LOG_LEVEL_NOTICE)) {  \
        ssdk_printf(DISPSS_LOG_LEVEL_NOTICE,                                   \
                    "[DISPSS]NOTICE|%s| " string "\r\n", __func__, ##args);    \
    }

#define DISPSS_LOG_INFO(string, args...)                                       \
    if (DISPSS_LOG_ENABLE && (DISPSS_LOG_LEVEL >= DISPSS_LOG_LEVEL_INFO)) {    \
        ssdk_printf(DISPSS_LOG_LEVEL_INFO, "[DISPSS]INFO|%s| " string "\r\n",  \
                    __func__, ##args);                                         \
    }

#define DISPSS_LOG_DEBUG(string, args...)                                      \
    if (DISPSS_LOG_ENABLE && (DISPSS_LOG_LEVEL >= DISPSS_LOG_LEVEL_DEBUG)) {   \
        ssdk_printf(DISPSS_LOG_LEVEL_DEBUG,                                    \
                    "[DISPSS]DEBUG|%s| " string "\r\n", __func__, ##args);     \
    }

#define DISPSS_LOG_FUNC()                                                      \
    if (DISPSS_LOG_ENABLE && (DISPSS_LOG_LEVEL >= DISPSS_LOG_LEVEL_INFO)) {    \
        ssdk_printf(DISPSS_LOG_LEVEL_INFO, "[DISPSS]FUNC|%s\r\n", __func__);   \
    }

#endif /*_DISPSS_LOG_H__ */
