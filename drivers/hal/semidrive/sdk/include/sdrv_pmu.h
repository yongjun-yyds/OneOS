/**
 * @file sdrv_pmu.h
 * @brief SemiDrive PMU header file.
 *
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef SDRV_PMU_DRV_H_
#define SDRV_PMU_DRV_H_

#include <sdrv_common.h>
#include <types.h>
#include <part.h>

#define SDRV_PMU_WAKEUP_PIN0_MASK  (0X1U)
#define SDRV_PMU_WAKEUP_PIN1_MASK  (0X2U)
#define SDRV_PMU_WAKEUP_RTC_MASK   (0X4U)
#define SDRV_PMU_WAKEUP_BC_MASK    (0X8U)

/**
 * @brief PMU status error code.
 */
enum sdrv_pmu_error {
    SDRV_PMU_CONFIG_TYPE_WRONG = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_PMU, 0),
    SDRV_PMU_CONFIG_NOT_SUPPORT = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_PMU, 1),
};

/**
 * @brief Description for SRCS_BITS each bit.
 */
#if CONFIG_E3 || CONFIG_D3
enum srcs_bit {
    SRCS_RSTGEN_SF_I_IST_EN_RTC_OVRD = 0U,
    SRCS_RSTGEN_SF_I_IST_EN_RTC_OVVAL = 1U,
    SRCS_ISTC_RTC_SS_POST_EN_OVVAL = 2U,
    SRCS_ISTC_RTC_SS_POST_EN_OVRD = 3U,
    SRCS_ISTC_PLL_CFG_POST_EN_OVVAL = 4U,
    SRCS_ISTC_PLL_CFG_POST_EN_OVRD = 8U,
    SRCS_ISTC_POWER_CFG_POST_EN_OVVAL = 9U,
    SRCS_ISTC_POWER_CFG_POST_EN_OVRD = 13U,
    SRCS_PT_SNS_SF_PT_SNS_AP_CVEN33_FORCE_OFF = 14U,
    SRCS_ANA_SF_ANALOG_IP_CVEN33_FORCE_OFF = 15U,
    SRCS_DCDC_ISO_EN_B = 16U,
    SRCS_LDO_ISO_EN_B = 17U,
    SRCS_BGR_LDO_AUTO_DISABLE_BY_PMU_EN = 18U,
    SRCS_BGR_SF_AUTO_DISABLE_BY_SMC_EN = 19U,
    SRCS_WAKEUP0_FALL_EDGE_TO_PULSE_EN = 20U,
    SRCS_POR_SF_PU33_B = 21U,
    SRCS_RC24M_PU33_B = 22U,
    SRCS_VD_SF_PU33_B = 23U,
    SRCS_BGR_ANA_PU33_B = 24U,
    SRCS_BGR_SF_PU33_B = 25U,
    SRCS_BGR_LDO_PU33_B = 26U,
    SRCS_POR_AP_PU33_B = 27U,
    SRCS_VD_AP_PU33_B = 28U,
    SRCS_BGR_AP_PU33_B = 29U,
    SRCS_BGR_DISP_PU33_B = 30U,
    SRCS_WAKEUP0_RISE_EDGE_TO_PULSE_EN = 31U,
};
#endif

#if CONFIG_E3L
enum srcs_bit {
    SRCS_RSTGEN_SF_I_IST_EN_RTC_OVRD = 0U,
    SRCS_RSTGEN_SF_I_IST_EN_RTC_OVVAL = 1U,
    SRCS_ISTC_RTC_SS_POST_EN_OVVAL = 2U,
    SRCS_ISTC_RTC_SS_POST_EN_OVRD = 3U,
    SRCS_ISTC_PLL_CFG_POST_EN_OVVAL = 4U,
    SRCS_ISTC_PLL_CFG_POST_EN_OVRD = 8U,
    SRCS_ISTC_POWER_CFG_POST_EN_OVVAL = 9U,
    SRCS_ISTC_POWER_CFG_POST_EN_OVRD = 13U,
    SRCS_DCDC_ISO_EN_B = 16U,
    SRCS_SCR_CFG_LATCH_EN = 17U,
    SRCS_BGR_SF_AUTO_DISABLE_BY_SMC_EN = 19U,
    SRCS_WAKEUP0_FALL_EDGE_TO_PULSE_EN = 20U,
    SRCS_POR_SF_PU33_B = 21U,
    SRCS_RC24M_PU33_B = 22U,
    SRCS_VD_SF_PU33_B = 23U,
    SRCS_BGR_ANA_PU33_B = 24U,
    SRCS_BGR_SF_PU33_B = 25U,
    SRCS_SADC1_CVEN33 = 26U,
    SRCS_SACMP1_CVEN33 = 27U,
    SRCS_APAD_CVEN33_B0_B1 = 28U,
    SRCS_APAD_CVEN33_B2_B8 = 29U,
    SRCS_OTHER_CVEN33 = 30U,
    SRCS_WAKEUP0_RISE_EDGE_TO_PULSE_EN = 31U,
};
#endif

/**
 * @brief Definition for pmu wakeup pin and button control active level or pulse.
 */
typedef enum sdrv_pmu_polarity {
    SDRV_PMU_HIGH_LEVEL = 0,
    SDRV_PMU_LOW_LEVEL,
    SDRV_PMU_RISING_EDGE,
    SDRV_PMU_FALLING_EDGE,
} sdrv_pmu_polarity_e;

/**
 * @brief Definition for pmu select which edge needs to apply the filter.
 */
typedef enum sdrv_pmu_edge_filter_enum {
    SDRV_PMU_FILTER_RISING = 0,
    SDRV_PMU_FILTER_FALLING,
    SDRV_PMU_FILTER_BOTH_EDGE,
} sdrv_pmu_edge_filter_e;

/**
 * @brief Definition for pmu wakeup pin.
 */
typedef enum sdrv_pmu_wakeup_pin {
    SDRV_PMU_WAKEUP_PIN_0 = 0,
    SDRV_PMU_WAKEUP_PIN_1,
} sdrv_pmu_wakeup_pin_e;

/**
 * @brief Definition for wakeup pin glitch, glitch is enable if wakeup edge < 32K cycle.
 */
typedef enum sdrv_pmu_wakeup_glitch {
    SDRV_PMU_GLITCH_NONE    = 0,
    SDRV_PMU_GLITCH_RISING  = 1,
    SDRV_PMU_GLITCH_FALLING = 2,
    SDRV_PMU_GLITCH_BOTH    = 3,
} sdrv_pmu_wakeup_glitch_e;

/**
 * @brief Definition for pmu wakeup pin logic.
 */
typedef enum sdrv_pmu_wakeup_pin_logic {
    SDRV_PMU_WAKEUP_PIN_OR = 0,  /**< generate wakeup event either pin0 or pin1 active. */
    SDRV_PMU_WAKEUP_PIN_AND,  /**< generate wakeup event both pin0/1 active. */
} sdrv_pmu_wakeup_pin_logic_e;

/**
 * @brief Definition for pmu action when receive wakeup during rtc state.
 *
 * PMU has RTC and OFF state. During RTC state, pmu doesn't respond to wakeup
 * event. It only responds to wakeup event in OFF state.
 */
typedef enum sdrv_pmu_wakeup_in_rtc {
    SDRV_PMU_WAKEUP_IN_RTC_IGNORE = 0,  /**< ignore wakeup event in rtc state */
    SDRV_PMU_WAKEUP_IN_RTC_LATCH,  /**< latch wakeup event in rtc state */
} sdrv_pmu_wakeup_in_rtc_e;

/**
 * @brief Definition for PMU work mode.
 */
typedef enum sdrv_pmu_mode {
    SDRV_PMU_RUN = 0,
    SDRV_PMU_SLEEP,
    SDRV_PMU_HIBERNATE,
    SDRV_PMU_RTC,
} sdrv_pmu_mode_e;

/**
 * @brief Definition for config pwrctrl and pwron delay setting in different period.
 */
typedef enum sdrv_pmu_delay_period {
    SDRV_PMU_PWRUP_DELAY = 0,
    SDRV_PMU_RUN_DELAY,
    SDRV_PMU_SLEEP_DELAY,
    SDRV_PMU_HIB_DELAY,
    SDRV_PMU_RTC_DELAY,
} sdrv_pmu_delay_period_e;

/**
 * @brief Definition for config minimal delay for state transition.
 */
typedef enum sdrv_pmu_state {
    SDRV_PMU_STATE_PWR_ON = 0,
    SDRV_PMU_STATE_PWRUP_RUN,
    SDRV_PMU_STATE_SLEEP_RUN,
    SDRV_PMU_STATE_HIBERNATE_RUN,
} sdrv_pmu_state_e;

/**
 * @brief Definition for pmu pwr_ctrl and pwr_on enable or disable.
 */
typedef enum sdrv_pmu_ctrl {
    SDRV_PMU_CTRL_OFF = 0,
    SDRV_PMU_CTRL_ON,
} sdrv_pmu_ctrl_e;

/**
 * @brief Definition for pmu pwr_ctrl and pwr_on operate by hardware or
 * software override.
 */
typedef enum sdrv_pmu_operate {
    SDRV_PMU_MANUAL = 0,
    SDRV_PMU_AUTO,
} sdrv_pmu_operate_e;

/**
 * @brief Definition for PWRCTRL active level.
 */
typedef enum sdrv_pmu_pwr_ctrl_polarity {
    SDRV_PMU_PWR_CTRL_HIGH_ACTIVE = 0,
    SDRV_PMU_PWR_CTRL_LOW_ACTIVE,
} sdrv_pmu_pwr_ctrl_polarity_e;

/**
 * @brief Definition for pmu pwr_ctrl.
 */
typedef enum sdrv_pmu_pwr_ctrl {
    SDRV_PMU_PWR_CTRL_0 = 0,
    SDRV_PMU_PWR_CTRL_1,
    SDRV_PMU_PWR_CTRL_2,
    SDRV_PMU_PWR_CTRL_3,
} sdrv_pmu_pwr_ctrl_e;

/**
 * @brief Definition for pmu pwr_on, now pwr_on_0 use for internal
 * DCDC, pwr_on_2 use for internal LDO.
 */
typedef enum sdrv_pmu_pwr_on {
    SDRV_PMU_PWR_ON_0 = 0,
    SDRV_PMU_PWR_ON_1,
    SDRV_PMU_PWR_ON_2,
    SDRV_PMU_PWR_ON_3,
} sdrv_pmu_pwr_on_e;

/**
 * @brief Definition for pmu pwr_on control target. It use for hardware power on
 * sequence.
 */
typedef enum sdrv_pmu_target {
    SDRV_PMU_SAF = 0,
    SDRV_PMU_AP,
    SDRV_PMU_OTHER,
} sdrv_pmu_target_e;

/**
 * @brief Definition for pmu power good config.
 */
typedef enum sdrv_pmu_pg {
    SDRV_PMU_POWER_GOOD_ENABLE = 0,
    SDRV_PMU_POWER_GOOD_DISABLE,
} sdrv_pmu_pg_e;

/**
 * @brief Definition for wakeup source.
 */
typedef enum sdrv_pmu_wakeup_src {
    SDRV_PMU_WAKEUP_BY_BC = 0x1U,
    SDRV_PMU_WAKEUP_BY_RTC = 0x2U,
    SDRV_PMU_WAKEUP_BY_PIN = 0x4U,
    SDRV_PMU_WAKEUP_BY_HW_RESET = 0x8U,
} sdrv_pmu_wakeup_src_e;

/**
 * @brief Definition for hardware power down request.
 */
typedef enum sdrv_pmu_hw_power_down_req {
    SDRV_PMU_SEM_INT_POWER_DOWN_REQ = 0x1U,
    SDRV_PMU_RESERVED_POWER_DOWN_REQ = 0x2U,
    SDRV_PMU_FS32K_LVPG_POWER_DOWN_REQ = 0x4U,
    SDRV_PMU_VD_SF_INT_POWER_DOWN_REQ = 0x8U,
    SDRV_PMU_VD_AP_INT_POWER_DOWN_REQ = 0x10U,
    SDRV_PMU_SF_PT_SENSOR_INT_POWER_DOWN_REQ = 0x20U,
    SDRV_PMU_AP_PT_SENSOR_INT_POWER_DOWN_REQ = 0x40U,
    SDRV_PMU_EFUSE_VIO_POWER_DOWN_REQ = 0x80U,
} sdrv_pmu_hw_power_down_req_e;

/**
 * @brief Definition for power down reason.
 */
typedef enum sdrv_pmu_power_down_reason {
    SDRV_PMU_POWER_DOWN_SMC_CHECK_FAIL = 0U,
    SDRV_PMU_POWER_DOWN_RESERVED,
    SDRV_PMU_POWER_DOWN_SEM_INT,
    SDRV_PMU_POWER_DOWN_RESERVED_1,
    SDRV_PMU_POWER_DOWN_FS32K_LVPG,
    SDRV_PMU_POWER_DOWN_VD_SF_INT,
    SDRV_PMU_POWER_DOWN_VD_AP_INT,
    SDRV_PMU_POWER_DOWN_SF_PT_SENSOR_INT,
    SDRV_PMU_POWER_DOWN_AP_PT_SENSOR_INT,
    SDRV_PMU_POWER_DOWN_EFUSE_VIO,
    SDRV_PMU_POWER_DOWN_SOFTWARE,
    SDRV_PMU_POWER_DOWN_BC,
} sdrv_pmu_power_down_reason_e;

/**
 * @brief Definition for PMU interrupt type.
 */
typedef enum sdrv_pmu_int_type {
    SDRV_PMU_INT_PWRON0_RDY_TIMEOUT   = 1U << 0U,
    SDRV_PMU_INT_PWRON2_RDY_TIMEOUT   = 1U << 2U,
    SDRV_PMU_INT_PWRUP_TO_RUN_TIMEOUT = 1U << 4U,
    SDRV_PMU_INT_SLEEP_TO_RUN_TIMEOUT = 1U << 5U,
    SDRV_PMU_INT_HIBERNATE_TO_RUN_TIMEOUT = 1U << 6U,
    SDRV_PMU_INT_LOCKSTEP_ERR             = 1U << 7U,
    SDRV_PMU_INT_SWM_WARN                 = 1U << 10U,
    SDRV_PMU_INT_SWM_FATAL                = 1U << 11U,
    SDRV_PMU_INT_POWER_DOWN_REQ           = 1U << 12U,
} sdrv_pmu_int_type_e;

/**
 * @brief Definition for PMU Wakeup pin and BC edge filter config.
 * filter time = 32K cycle * (2 ^ sample interval) * (counter + 2)
 */
typedef struct sdrv_pmu_edge_filter {
    bool enable_filter;          /**< enable filter for wakeup pin */
    uint8_t filter_sample_interval; /**< sample interval setting for wakeup pin */
    uint8_t filter_count;        /**< filter counter for wakeup pin */
    sdrv_pmu_edge_filter_e filter_type; /**< filter type */
} sdrv_pmu_edge_filter_t;

/**
 * @brief Definition for PMU BC level filter config.
 */
typedef struct sdrv_pmu_level_filter {
    uint8_t min_window_for_on;
    uint8_t max_window_for_on;
    uint8_t min_window_for_off;
    uint8_t max_window_for_off;
} sdrv_pmu_level_filter_t;

/**
 * @brief Definition for PMU BC level filter enable config.
 */
typedef struct sdrv_pmu_level_filter_enable {
    uint8_t bc_cnt_unit;  /**< real window setting is min/max window * 2^bc_cnt_unit
                           bc_cnt_unit must be smaller than 12 */
    bool max_window_for_on_en;
    bool max_window_for_off_en;
} sdrv_pmu_level_filter_enable_t;

/**
 * @brief Definition for button config active level or pulse to enter/exit rtc mode.
 */
typedef struct sdrv_pmu_bc_config {
    sdrv_pmu_polarity_e bc_off_pol;     /**< enter RTC mode */
    sdrv_pmu_polarity_e bc_on_pol;      /**< exit RTC mode */
} sdrv_pmu_bc_config_t;

/**
 * @brief Definition for wakeup pin active level or pulse to wakeup.
 */
typedef struct sdrv_pmu_wakeup_pin_config {
    sdrv_pmu_polarity_e pin_pol; /**< wakeup signal active type */
} sdrv_pmu_wakeup_pin_config_t;

/**
 * @brief Definition for wakeup pins config parameters.
 */
typedef struct sdrv_pmu_wakeuppins_config {
    sdrv_pmu_wakeup_pin_logic_e logic;
    sdrv_pmu_wakeup_in_rtc_e latch;

    bool wakepin0_en;
    sdrv_pmu_wakeup_glitch_e wakepin0_glitch;
    sdrv_pmu_polarity_e wakepin0_pol;
    sdrv_pmu_edge_filter_t wakepin0_filter;

    bool wakepin1_en;
    sdrv_pmu_polarity_e wakepin1_pol;
    sdrv_pmu_edge_filter_t wakepin1_filter;
} sdrv_pmu_wakeuppins_config_t;

/**
 * @brief Definition for button control config parameters.
 */
typedef struct sdrv_pmu_buttonctrl_config {
    sdrv_pmu_bc_config_t polarity;
    sdrv_pmu_edge_filter_t edge_filter;
    sdrv_pmu_level_filter_enable_t level_filter_en;
    sdrv_pmu_level_filter_t level_filter_para;
    bool fsm_enable;
} sdrv_pmu_buttonctrl_config_t;

/**
 * @brief Definition for power good watchdog on pwron.
 */
typedef struct sdrv_pmu_pwron_wdt_config {
    uint8_t por_tw; /**< power good time window. */
    uint8_t por_tw_adj; /**< power good time window adjustment */
    uint8_t wdt_div; /**< counter increase interval */
} sdrv_pmu_pwron_wdt_config_t;

/**
 * @brief Definition for WDT counter and enable config.
 */
typedef struct sdrv_pmu_pwron_wdt_enable {
    bool pwron_wdt_en; /**< enable pwron watchdog counter */
    uint8_t wdt_counter; /**< timeout setting */
} sdrv_pmu_pwron_wdt_enable_t;

/**
 * @brief Definition for pmu pwr ctrl config.
 */
typedef struct sdrv_pmu_pwrctrl_config {
    sdrv_pmu_ctrl_e ctrl_status;  /**< on or off */
    sdrv_pmu_operate_e ctrl_operate;  /**< auto or manual */
} sdrv_pmu_pwrctrl_config_t;

/**
 * @brief Definition for setting for pwrctrl0-3 minimal delay enable config.
 */
typedef struct sdrv_pmu_pwrctrl_min_delay_config {
    bool pwrup_run_min_delay_en;
    bool sleep_run_min_delay_en;
    bool hibernate_run_min_delay_en;
} sdrv_pmu_pwrctrl_min_delay_config_t;

/**
 * @brief Definition for pmu pwr on config.
 */
typedef struct sdrv_pmu_pwron_config {
    sdrv_pmu_ctrl_e ctrl_status;  /**< on or off */
    sdrv_pmu_operate_e ctrl_operate;  /**< auto or manual */
} sdrv_pmu_pwron_config_t;

/**
 * @brief Enable wakeup source when in RTC mode.
 *
 * This function enable which source can use to wakeup from RTC mode.
 * Source are: SDRV_PMU_WAKEUP_PIN0_MASK, SDRV_PMU_WAKEUP_PIN1_MASK
 * SDRV_PMU_WAKEUP_RTC_MASK, SDRV_PMU_WAKEUP_BC_MASK
 *
 * @param [in] enable_bitmap OR'ed value of the source.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_wakeup_enable(uint32_t enable_bitmap);

/**
 * @brief Disable wakeup source when in RTC mode.
 *
 * This function disable which source can use to wakeup from RTC mode.
 * Source are: SDRV_PMU_WAKEUP_PIN0_MASK, SDRV_PMU_WAKEUP_PIN1_MASK
 * SDRV_PMU_WAKEUP_RTC_MASK, SDRV_PMU_WAKEUP_BC_MASK
 *
 * @param [in] disable_bitmap OR'ed value of the source.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_wakeup_disable(uint32_t disable_bitmap);

/**
 * @brief Config button control register.
 *
 * This function config button control enter/exit rtc mode active level or
 * pulse.
 *
 * @param [in] bc_config button control config.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_button_control_init(sdrv_pmu_bc_config_t *bc_config);

/**
 * @brief Config edge filter for button control.
 *
 * @param [in] filter_config edge filter parameters.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_button_control_edge_filter_config(sdrv_pmu_edge_filter_t *filter_config);

/**
 * @brief Config level filter parameters for button control.
 *
 * @param [in] filter_config level filter parameters.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_button_control_level_filter_para(sdrv_pmu_level_filter_t *filter_config);

/**
 * @brief Enable level filter for button control.
 *
 * @param [in] filter_config level filter config.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_button_control_level_filter_config(sdrv_pmu_level_filter_enable_t *filter_config);

/**
 * @brief Config hardware power down request enable.
 *
 * @param [in] enable_bitmap OR' value defined in sdrv_pmu_hw_power_down_req_e.
 * @param [in] enable enable or disable.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_hardware_power_down_request_enable(uint32_t enable_bitmap, bool enable);

/**
 * @brief Enable or Disable button control function.
 *
 * This function config whether use button control or not.
 *
 * @param [in] enable true represents enable, false represents disable.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_button_control_enable(bool enable);

/**
 * @brief Enable status monitor for button control.
 *
 * This function enable satus monitor for button control.
 * When enabled, only responds the valid power on request in BC_OFF state,
 * and only respond the valid power off request in BC_ON state.
 *
 * @param [in] enable true represents enable fsm check, false represents disable fsm check.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_button_control_fsm_enable(bool enable);

/**
 * @brief Initialize button control parameter config.
 *
 * @param [in] config bc config parameter.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_button_control_config(sdrv_pmu_buttonctrl_config_t *config);

/**
 * @brief Config specific wakeup pin.
 *
 * This function config wakeup pin active level or pulse.
 *
 * @param [in] wakeup_pin wakeup pin defined in sdrv_pmu_wakeup_pin_e.
 * @param [in] pin_config wakeup pin config parameters.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_wakeup_pin_init(sdrv_pmu_wakeup_pin_e wakeup_pin, sdrv_pmu_wakeup_pin_config_t *pin_config);

/**
 * @brief Config edge filter for wakeup pin.
 *
 * This function config edge filter parameters for specific wakeup pin.
 *
 * @param [in] wakeup_pin wakeup pin defined in sdrv_pmu_wakeup_pin_e.
 * @param [in] filter_config wakeup pin filter parameters.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_wakeup_pin_filter_config(sdrv_pmu_wakeup_pin_e wakeup_pin, sdrv_pmu_edge_filter_t *filter_config);

/**
 * @brief Enable or Disable specific system wakeup pin.
 *
 * This function config whether specific wakeup pin use or not.
 *
 * @param [in] wakeup_pin wakeup pin defined in sdrv_pmu_wakeup_pin_e.
 * @param [in] enable true represents enable, false represents disable.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_wakeup_pin_enable(sdrv_pmu_wakeup_pin_e wakeup_pin, bool enable);

/**
 * @brief Config wakeup pin 0/1 generate wakeup event logic.
 *
 * This function config whether generate wakeup event needs both wakeup pins active or not.
 *
 * @param [in] logic defined in sdrv_pmu_wakeup_pin_logic_e.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_wakeup_pin_logic_set(sdrv_pmu_wakeup_pin_logic_e logic);

/**
 * @brief Config pmu action when receive wakeup event during rtc state.
 *
 * PMU has RTC and OFF state. During RTC state, pmu doesn't respond to wakeup
 * event. It only responds to wakeup event in OFF state. This function config whether pmu ignore
 * rtc state wakeup event or latch wakeup when enter off state.
 *
 * @param [in] latch defined in sdrv_pmu_wakeup_in_rtc_e.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_wakeup_latch_set(sdrv_pmu_wakeup_in_rtc_e latch);

/**
 * @brief Config Wake pin 0 and 1 enable and active level.
 *
 * @param [in] config config parameters defined in sdrv_pmu_wakeuppins_config_t.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_wakeup_pins_config(sdrv_pmu_wakeuppins_config_t *config);

/**
 * @brief Software trigger power down.
 *
 * This function write register to trigger power down request. After call this function, system will enter
 * RTC mode at once.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_power_down(void);

/**
 * @brief Config power good watchdog parameters for pwron.
 *
 * @param [in] wdt_config pwron watchdog config parameters.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_pwron_watchdog_config(sdrv_pmu_pwron_wdt_config_t *wdt_config);

/**
 * @brief Enable power good watchdog counter for specific pwron.
 *
 * @param [in] pwron pwron defined in sdrv_pmu_pwr_on_e.
 * @param [in] enable_config enable config.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_pwron_watchdog_enable(sdrv_pmu_pwr_on_e pwron, sdrv_pmu_pwron_wdt_enable_t *enable_config);

/**
 * @brief Config pwr ctrl output level for different mode.
 *
 * This function config pwr ctrl output level under run/sleep/hibernate/rtc mode.
 *
 * @param [in] ctrl pwr ctrl pin defined in sdrv_pmu_pwr_ctrl_e.
 * @param [in] mode pmu work mode defined in sdrv_pmu_mode_e.
 * @param [in] config config parameters.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_pwrctrl_config(sdrv_pmu_pwr_ctrl_e ctrl, sdrv_pmu_mode_e mode, sdrv_pmu_pwrctrl_config_t *config);

/**
 * @brief Config pwrctrl0-3 minimal delay enable config during pwrup/sleep/hibernate to run mode.
 *
 * @param [in] config minimal delay enable config.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_pwrctrl_min_delay_config(sdrv_pmu_pwrctrl_min_delay_config_t *config);

/**
 * @brief Config pwrctrl0-3 active level.
 *
 * @param [in] ctrl pwr ctrl pin defined in sdrv_pmu_pwr_ctrl_e.
 * @param [in] pol high or low level represents pwrctrl on.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_pwrctrl_polarity_config(sdrv_pmu_pwr_ctrl_e ctrl, sdrv_pmu_pwr_ctrl_polarity_e pol);

/**
 * @brief Enable software override pwrctrl.
 *
 * @param [in] ctrl pwr ctrl pin defined in sdrv_pmu_pwr_ctrl_e.
 * @param [in] enable true or false.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_pwrctrl_software_override_enable(sdrv_pmu_pwr_ctrl_e ctrl, bool enable);

/**
 * @brief Software override pwrctrl value.
 *
 * @param [in] ctrl pwr ctrl pin defined in sdrv_pmu_pwr_ctrl_e.
 * @param [in] value true or false.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_pwrctrl_software_override_value(sdrv_pmu_pwr_ctrl_e ctrl, bool value);

/**
 * @brief Config pwrctrl delay setting in different period.
 *
 * Delay time = 32Kcycle * (2^delay) * (delay_adj * 2 + 1)
 *
 * @param [in] period period defined in sdrv_pmu_delay_period_e.
 * @param [in] ctrl pwr ctrl pin defined in sdrv_pmu_pwr_ctrl_e.
 * @param [in] delay delay value.
 * @param [in] delay_adj delay adjust value.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_pwrctrl_delay_config(sdrv_pmu_delay_period_e period, sdrv_pmu_pwr_ctrl_e ctrl, uint8_t delay, uint8_t delay_adj);

/**
 * @brief Config pwr on whether work for different mode.
 *
 * This function config pwr on whether work under run/sleep/hibernate/rtc mode.
 *
 * @param [in] ctrl pwr on defined in sdrv_pmu_pwr_on_e.
 * @param [in] mode pmu work mode defined in sdrv_pmu_mode_e.
 * @param [in] config config parameters.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_pwron_config(sdrv_pmu_pwr_on_e ctrl, sdrv_pmu_mode_e mode, sdrv_pmu_pwron_config_t *config);

/**
 * @brief Config pwron power supply target under different mode.
 *
 * @param [in] ctrl pwr on defined in sdrv_pmu_pwr_on_e.
 * @param [in] mode pmu work mode defined in sdrv_pmu_mode_e.
 * @param [in] target target defined in sdrv_pmu_target_e.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_pwron_target_config_with_mode(sdrv_pmu_pwr_on_e ctrl, sdrv_pmu_mode_e mode, sdrv_pmu_target_e target);

/**
 * @brief Config internal DCDC/LDO power supply.
 *
 * This function config internal DCDC/LDO power supply to SAF/AP/Others.
 * It will effect hardware power on sequence.
 *
 * @param [in] ctrl pwr on defined in sdrv_pmu_pwr_on_e.
 * @param [in] target target defined in sdrv_pmu_target_e.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_pwron_target_config(sdrv_pmu_pwr_on_e ctrl, sdrv_pmu_target_e target);

/**
 * @brief Config SAF/AP domain whether powered under different mode.
 *
 * This function config SAF/AP domain whether powered in run/sleep/hibernate/rtc mode.
 * For example, if AP domain config powered in run mode and not powered in hibernate mode,
 * after wakeup from hibernate to run, hardware will auto reset ap domain.
 *
 * @param [in] target target defined in sdrv_pmu_target_e.
 * @param [in] mode mode defined in sdrv_pmu_mode_e.
 * @param [in] powered true represents powered, false represents not powered.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_target_power_on_config(sdrv_pmu_target_e target, sdrv_pmu_mode_e mode, bool powered);

/**
 * @brief Config pwron delay setting in different period.
 *
 * Delay time = 32Kcycle * (2^delay) * (delay_adj * 2 + 1)
 *
 * @param [in] period period defined in sdrv_pmu_delay_period_e.
 * @param [in] ctrl pwron defined in sdrv_pmu_pwr_on_e.
 * @param [in] delay delay value.
 * @param [in] delay_adj delay adjust value.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_pwron_delay_config(sdrv_pmu_delay_period_e period, sdrv_pmu_pwr_on_e ctrl, uint8_t delay, uint8_t delay_adj);

/**
 * @brief Enable software override pwron.
 *
 * @param [in] ctrl pwron defined in sdrv_pmu_pwr_on_e.
 * @param [in] enable true or false.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_pwron_software_override_enable(sdrv_pmu_pwr_on_e ctrl, bool enable);

/**
 * @brief Software override pwron value.
 *
 * @param [in] ctrl pwron defined in sdrv_pmu_pwr_on_e.
 * @param [in] value true or false.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_pwron_software_override_value(sdrv_pmu_pwr_on_e ctrl, bool value);

/**
 * @brief Config minimal delay for pmu state transition.
 *
 * Delay time = 32Kcycle * (2^delay) * (delay_adj * 2 + 1)
 *
 * @param [in] state state defined in sdrv_pmu_state_e.
 * @param [in] delay delay value.
 * @param [in] delay_adj delay adjust.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_minimal_delay_for_state_transition(sdrv_pmu_state_e state, uint8_t delay, uint8_t delay_adj);

/**
 * @brief Enable PMU internal interrupt.
 *
 * @param [in] bitmap OR'Value defined in sdrv_pmu_int_type_e.
 * @param [in] enable true or false.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_interrupt_enable(uint32_t bitmap, bool enable);

/**
 * @brief Get PMU internal interrupt status.
 *
 * @return OR'Value defined in sdrv_pmu_int_type_e.
 */
uint32_t sdrv_pmu_get_interrupt_status(void);

/**
 * @brief Clear PMU internal interrupt status.
 *
 * @param [in] bitmap OR'Value defined in sdrv_pmu_int_type_e.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_clear_interrupt_status(uint32_t bitmap);

/**
 * @brief Set corresponding bit to 1 for 32bit state retention bits register.
 *
 * @param [in] bitmap each bit description in enum srcs_bit
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_srcs_set(uint32_t bitmap);

/**
 * @brief Clear corresponding bit to 0 for 32bit state retention bits register.
 *
 * @param [in] bitmap each bit description in enum srcs_bit
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_srcs_clear(uint32_t bitmap);

/**
 * @brief Enable or disable analog IP power.
 *
 * This function config PMU to control SF voltage detect analog power.
 * Default is power on.
 *
 * @param [in] enable true represents power on, false represents power down.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_vd_sf_ana_enable(bool enable);

/**
 * @brief Enable or disable analog IP power.
 *
 * This function config PMU to control SF POR analog power.
 * Default is power on.
 *
 * @param [in] enable true represents power on, false represents power down.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_por_sf_enable(bool enable);

/**
 * @brief Enable or disable analog IP power.
 *
 * This function config PMU to control rc24m analog power.
 * Default is power on.
 *
 * @param [in] enable true represents power on, false represents power down.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_rc24m_enable(bool enable);

/**
 * @brief Enable or disable analog IP power.
 *
 * This function config PMU to control bgr_sf power.
 * Default is power on.
 *
 * @param [in] enable true represents power on, false represents power down.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_bgr_sf_enable(bool enable);

/**
 * @brief Enable or disable analog IP power.
 *
 * This function config PMU to control bgr ldo power.
 * Default is power on.
 *
 * @param [in] enable true represents power on, false represents power down.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_bgr_ldo_enable(bool enable);

/**
 * @brief Enable or disable analog IP power.
 *
 * This function config PMU to control bgr ana sf1 sf2 power.
 * Default is power on.
 *
 * @param [in] enable true represents power on, false represents power down.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_bgr_ana_sf1_sf2_enable(bool enable);

/**
 * @brief Enable or disable analog IP power.
 *
 * This function config PMU to control AP voltage detect analog power.
 * Default is power on.
 *
 * @param [in] enable true represents power on, false represents power down.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_vd_ap_enable(bool enable);

/**
 * @brief Enable or disable analog IP power.
 *
 * This function config PMU to control AP POR analog power.
 * Default is power on.
 *
 * @param [in] enable true represents power on, false represents power down.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_por_ap_enable(bool enable);

/**
 * @brief Enable or disable analog IP power.
 *
 * This function config PMU to control bgr ap power.
 * Default is power on.
 *
 * @param [in] enable true represents power on, false represents power down.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_bgr_ap_enable(bool enable);

/**
 * @brief Enable or disable analog IP power.
 *
 * This function config PMU to control bgr display power.
 * Default is power on.
 *
 * @param [in] enable true represents power on, false represents power down.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_bgr_disp_enable(bool enable);

/**
 * @brief Enable or disable analog IP power.
 *
 * This function config PMU to control pt_sns_sf/pt_sns_ap, analog ip, such as adc, acmp cven33 power.
 * Default is power on.
 *
 * @param [in] enable true represents power on, false represents power down.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_analog_cven33_enable(bool enable);

/**
 * @brief Enable or disable analog IP power.
 *
 * This function config PMU to control pt_sns_sf/pt_sns_ap cven33 power.
 * Default is power on.
 *
 * @param [in] enable true represents power on, false represents power down.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_pt_sns_enable(bool enable);

/**
 * @brief Enable or disable analog IP power.
 *
 * This function config PMU to control ana_sf analog IP cven33 power.
 * Default is power on.
 *
 * @param [in] enable true represents power on, false represents power down.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_ana_sf_analog_enable(bool enable);

/**
 * @brief Config bgr_ldo auto-disable.
 *
 * This function config whether bgr_ldo auto-disable with rc24m/xtal24m when enter low power mode.
 *
 * @param [in] enable true represents auto-disable with rc24m/xtal24m, false represents not.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_bgr_ldo_auto_disable(bool enable);

/**
 * @brief Config bgr_sf auto-disable.
 *
 * This function config whether bgr_sf auto-disable with rc24m/xtal24m when enter low power mode.
 *
 * @param [in] enable true represents auto-disable with rc24m/xtal24m, false represents not.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_bgr_sf_auto_disable(bool enable);

/**
 * @brief Enable glitch circle for wakeup pin 0.
 *
 * This function enable glitch circle for wakeup pin 0, in this case, it can glitch < 32khz pulse.
 * When enable this feature, filter for wakeup 0 will be disabled.
 *
 * @param [in] mask OR'ed value, bit0: rising edge, bit1: falling edge.
 *                  for example: (1<<0) | (1<<1) means both edge.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_wakeup0_glitch_enable(uint32_t mask);

/**
 * @brief Disable glitch circle for wakeup pin 0.
 *
 * This function disable glitch circle for wakeup pin 0, in this case, it can't glitch < 32khz pulse.
 * When both edge disabled, filter for wakeup 0 will be re-enabled.
 *
 * @param [in] mask OR'ed value, bit0: rising edge, bit1: falling edge.
 *                  for example: (1<<0) | (1<<1) means both edge.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_wakeup0_glitch_disable(uint32_t mask);

/**
 * @brief Config saf domain 3.3v and 0.8v power good monitor.
 *
 * This function config saf domain power monitor , when enabled, once 3.3v or 0.8v is
 * not good, saf isolate and sf_por_b is assert. After power is good, saf isolate and
 * sf_por_b deassert.
 *
 * @param [in] cfg SDRV_PMU_POWER_GOOD_ENABLE/SDRV_PMU_POWER_GOOD_DISABLE
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_saf_domain_power_good_monitor(sdrv_pmu_pg_e cfg);

/**
 * @brief Config ap domain 3.3v and 0.8v power good monitor.
 *
 * This function config ap domain power monitor , when enabled, once 3.3v or 0.8v is
 * not good, ap isolate and sf_por_b is assert. After power is good, ap isolate and
 * sf_por_b deassert.
 *
 * @param [in] cfg SDRV_PMU_POWER_GOOD_ENABLE/SDRV_PMU_POWER_GOOD_DISABLE
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_ap_domain_power_good_monitor(sdrv_pmu_pg_e cfg);

/**
 * @brief Config saf domain power good consider or ignore when switching from RTC state to OFF state.
 *
 * This function config saf domain power good whether consider when enter RTC mode. If enabled, switching
 * from RTC state to OFF state only when the power down counter is done and saf domain power are not good.
 * If disabled, switching from RTC state to OFF state only when the power down counter is done, but ignore
 * saf domain power good.
 *
 * @param [in] cfg SDRV_PMU_POWER_GOOD_ENABLE/SDRV_PMU_POWER_GOOD_DISABLE
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_rtc_to_off_saf_domain_power_good(sdrv_pmu_pg_e cfg);

/**
 * @brief Config ap domain power good consider or ignore when switching from RTC state to OFF state.
 *
 * This function config ap domain power good whether consider when enter RTC mode. If enabled, switching
 * from RTC state to OFF state only when the power down counter is done and ap domain power are not good.
 * If disabled, switching from RTC state to OFF state only when the power down counter is done, but ignore
 * ap domain power good.
 *
 * @param [in] cfg SDRV_PMU_POWER_GOOD_ENABLE/SDRV_PMU_POWER_GOOD_DISABLE
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_rtc_to_off_ap_domain_power_good(sdrv_pmu_pg_e cfg);

/**
 * @brief Get system wakeup source.
 *
 * This function read pmu wakeup source, return value is OR'ed value of sdrv_pmu_wakeup_src_e
 *
 * @return uint32_t OR'ed value of sdrv_pmu_wakeup_src_e
 */
uint32_t sdrv_pmu_get_wakeup_source(void);

/**
 * @brief Clear system wakeup source.
 *
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_clear_wakeup_source(void);

/**
 * @brief Get system power down reason.
 *
 * This function return last power down reason, each bit represent reason see sdrv_pmu_power_down_reason_e.
 *
 * @return uint32_t power down reason.
 */
uint32_t sdrv_pmu_get_power_down_reason(void);

/**
 * @brief Clear system power down reason.
 *
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pmu_clear_power_down_reason(void);

/**
 * @brief Get PMU internal state.
 *
 * @return PMU_STATE register value.
 */
uint32_t sdrv_pmu_get_state(void);

#endif /* SDRV_PMU_DRV_H_ */
