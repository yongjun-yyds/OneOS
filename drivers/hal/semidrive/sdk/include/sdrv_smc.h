/**
 * @file sdrv_smc.h
 * @brief SemiDrive System work Mode Controller (SMC) driver header file.
 *
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef SDRV_SMC_H_
#define SDRV_SMC_H_

#include <sdrv_common.h>
#include <types.h>
#include <part.h>

/**
 * @brief SMC additional wakeup irq num
 */
#if CONFIG_E3 || CONFIG_D3
#define SDRV_SMC_ASYNC_INT_USB (236 + 16)
#define SDRV_SMC_ASYNC_INT_IOC (237 + 16)
#define SDRV_SMC_ASYNC_INT_VIC1_SF_GPIO (238 + 16)
#define SDRV_SMC_ASYNC_INT_VIC1_AP_GPIO (239 + 16)
#define SDRV_SMC_ASYNC_INT_VIC2A_SF_GPIO (240 + 16)
#define SDRV_SMC_ASYNC_INT_VIC2A_AP_GPIO (241 + 16)
#define SDRV_SMC_ASYNC_INT_VIC2B_SF_GPIO (242 + 16)
#define SDRV_SMC_ASYNC_INT_VIC2B_AP_GPIO (243 + 16)
#define SDRV_SMC_ASYNC_INT_VIC3A_SF_GPIO (244 + 16)
#define SDRV_SMC_ASYNC_INT_VIC3A_AP_GPIO (245 + 16)
#define SDRV_SMC_ASYNC_INT_VIC3B_SF_GPIO (246 + 16)
#define SDRV_SMC_ASYNC_INT_VIC3B_AP_GPIO (247 + 16)

#define SDRV_SF_CKGEN_PCG_NUM  334
#define SDRV_SF_CKGEN_BCG_NUM  15
#define SDRV_SF_CKGEN_CCG_NUM  5

#endif

#if CONFIG_E3L || CONFIG_D3L
#define SDRV_SMC_ASYNC_INT_USB (163 + 16)
#define SDRV_SMC_ASYNC_INT_IOC (164 + 16)
#define SDRV_SMC_ASYNC_INT_VIC1_SF_GPIO (165 + 16)

#define SDRV_SF_CKGEN_PCG_NUM  207
#define SDRV_SF_CKGEN_BCG_NUM  8
#define SDRV_SF_CKGEN_CCG_NUM  1

#endif

#define RAM_LP_PG_EN 0x4u
#define RAM_LP_RET1N 0x2u
#define RAM_LP_RET2N 0x1u

#define SDRV_SMC_CHIP_ENABLE (RAM_LP_RET1N | RAM_LP_RET2N)
#define SDRV_SMC_SELECTIVE_PRECHARGE (RAM_LP_RET2N)
#define SDRV_SMC_RETENTION_1 (RAM_LP_PG_EN | RAM_LP_RET2N)
#define SDRV_SMC_RETENTION_2 (RAM_LP_PG_EN | RAM_LP_RET1N)
#define SDRV_SMC_POWER_DOWN (RAM_LP_PG_EN | RAM_LP_RET1N | RAM_LP_RET2N)

/**
 * @brief AP power state impacts to SMC state machine.
 */
/**< AP status is ignored. Only Safety SMC state used for SWM transtion. */
#define SDRV_SMC_IGNORE_AP (0x1u)
/**< AP domain enter low power mode with SAF domain */
#define SDRV_SMC_AP_LP_ALIGN_SAF (0x2u)
/**< AP domain wakeup with SAF domain */
#define SDRV_SMC_AP_WK_ALIGN_SAF (0x4u)

#define SDRV_SMC_SWM_RUN (0x0u)
#define SDRV_SMC_SWM_SLP (0x1u)
#define SDRV_SMC_SWM_HIB (0x2u)
#define SDRV_SMC_SWM_RTC (0x3u)

/**
 * @brief SMC status error code.
 */
enum sdrv_smc_error {
    SDRV_SMC_CONFIG_TYPE_WRONG = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_SMC, 0),
    SDRV_SMC_CONFIG_NOT_SUPPORT = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_SMC, 1),
};

/**
 * @brief SMC domains.
 *
 * The chip is composed of 3 power domains, Safety, AP and RTC. SMC manages
 * power states of Safety and AP domains, while RTC is the always on domain.
 */
typedef enum sdrv_smc_domain {
    SDRV_SMC_SAF, /**< Safety power domain */
    SDRV_SMC_AP,  /**< AP power domain */
} sdrv_smc_domain_e;

/**
 * @brief SMC power modes.
 */
typedef enum sdrv_smc_mode {
    /**< Sleep mode
     *
     * - CPUs inactive. CPU clocks are gated.
     * - PLL power down.
     * - 24M XTAL power on or off.
     * - Periplerals are clock gated, except clocks requried by wake up sources.
     * - RAM in retention mode.
     */
    SDRV_SMC_SLEEP,

    /**< Hibernate mode
     *
     * - CPU power down
     * - AP domain power down
     * - PLL and 24M XTAL power down
     * - Safety domain periplerals clock gated
     * - RAM in retention mode
     */
    SDRV_SMC_HIBERNATE,
} sdrv_smc_mode_e;

#if CONFIG_E3 || CONFIG_D3
/**
 * @brief Power switches.
 *
 * Power switches are controlled by the SMC to turn on or turn off
 * module power in Sleep or Hibernate modes.
 */
typedef enum sdrv_smc_power_switch {
    SDRV_SMC_POWER_SWITCH_SF = 0,    /**< CR5_SF_MIX power switch */
    SDRV_SMC_POWER_SWITCH_SP,        /**< CR5_SP_MIX power switch */
    SDRV_SMC_POWER_SWITCH_SX,        /**< CR5_SX_MIX power switch */
    SDRV_SMC_POWER_SWITCH_GAMA1,     /**< SP_SS gama1 power switch */
    SDRV_SMC_POWER_SWITCH_AP_DISP,   /**< AP_SS disp power switch */
} sdrv_smc_power_switch_e;

/**
 * @brief On chip RAMs that have low power modes controlled by SMC.
 */
typedef enum sdrv_smc_ram {
    SDRV_SMC_RAM_SF = 0,    /**< SF core RAM */
    SDRV_SMC_RAM_SP,        /**< SP core RAM */
    SDRV_SMC_RAM_SX,        /**< SX core RAM */
    SDRV_SMC_RAM_GAMA1,     /**< GAMA internal RAM */
    SDRV_SMC_RAM_SF_MISC,   /**< Other RAMs in Safety subsystem */
    SDRV_SMC_RAM_AP,        /**< RAMs in AP subsystem */
    SDRV_SMC_RAM_DISP,      /**< RAMs in display domain */
} sdrv_smc_ram_e;
#endif /* CONFIG_E3 || CONFIG_D3 */

#if CONFIG_E3L || CONFIG_D3L
/**
 * @brief Power switches.
 *
 * Power switches are controlled by the SMC to turn on or turn off
 * module power in Sleep or Hibernate modes.
 */
typedef enum sdrv_smc_power_switch {
    SDRV_SMC_POWER_SWITCH_SF      = 0,        /**< CR5_SF_MIX power switch */
    SDRV_SMC_POWER_SWITCH_SF_BOOT = 2,        /**< SF BOOT power switch */
    SDRV_SMC_POWER_SWITCH_AP_MIX  = 3,        /**< AP MIX power switch */
} sdrv_smc_power_switch_e;

/**
 * @brief On chip RAMs that have low power modes controlled by SMC.
 */
typedef enum sdrv_smc_ram {
    SDRV_SMC_RAM_SF      = 0,    /**< SF core RAM */
    SDRV_SMC_RAM_SF_BOOT = 2,    /**< SF BOOT RAM */
    SDRV_SMC_RAM_AP_MIX  = 3,    /**< AP MIX RAM */
} sdrv_smc_ram_e;
#endif /* CONFIG_E3L || CONFIG_D3L */

/**
 * @brief SMC handshake signals with other on chip modules.
 */
typedef enum sdrv_smc_handshake {
    SDRV_SMC_HK_AP_CKGEN = 0,   /**< handshake with AP clock generator */
    SDRV_SMC_HK_AP_RSTGEN,      /**< handshake with AP reset generator */
    SDRV_SMC_HK_SAF_CKGEN,      /**< handshake with Safety clock generator */
    SDRV_SMC_HK_SAF_RSTGEN,     /**< handshake with Safety reset generator */
    SDRV_SMC_HK_PMU,            /**< handshake with PMU */
} sdrv_smc_handshake_e;

/**
 * @brief CPU cores managed by SMC.
 */
typedef enum sdrv_smc_core {
    SDRV_SMC_CORE_SF = 0,   /**< cluster 0 */
    SDRV_SMC_CORE_SP0,      /**< cluster 2, core 0 */
    SDRV_SMC_CORE_SP1,      /**< cluster 2, core 1 */
    SDRV_SMC_CORE_SX0,      /**< cluster 1, core 0 */
    SDRV_SMC_CORE_SX1,      /**< cluster 1, core 1 */
} sdrv_smc_core_e;

/**
 * @brief SMC debug mux sel.
 */
typedef enum sdrv_smc_debug_mux {
    SDRV_SMC_DBG_HK_SAF = 1,
    SDRV_SMC_DBG_HK_AP,
    SDRV_SMC_DBG_HK_PMU
} sdrv_smc_debug_mux_e;

/**
 * @brief Definition for domain timeout setting.
 */
typedef struct sdrv_smc_domain_timeout {
    bool wkup_en; /**< timeout wakeup during lp process enable */
    bool wdt_en; /**< watchdog enable for handshake time monitor */
    uint8_t ckgen_hk; /**< timeout value for ckgen swm handshake */
    uint8_t rstgen_hk; /**< timeout value for rstgen swm handshake */
    uint8_t mode_req_trans; /**< timeout value for mode trans request */
} sdrv_smc_domain_timeout_t;

/**
 * @brief Definition for SOC SWM timeout setting.
 */
typedef struct sdrv_smc_soc_timeout {
    bool wkup_en; /**< timeout wakeup during lp process enable */
    bool wdt_en; /**< watchdog enable for handshake time monitor */
    uint8_t pmu_hk; /**< timeout value for pmu swm handshake */
} sdrv_smc_soc_timeout_t;

/**
 * @brief Definition for soc wakeup timeout control setting.
 */
typedef struct sdrv_smc_soc_wakeup_ctrl {
    bool lp2wkup_tout_wkup_en; /**< timeout between lp to timeout wakeup enable */
    bool lp2wkup_wdt_en; /**< wdt enable between lp to wakeup event receive */
    uint16_t lp2wkup_val; /**< time between lp to wakeup event receive */

    bool wkup_wdt_en; /**< wakeup watchdog enable */
    uint8_t wkup_done_src_sel; /**< wakeup done event source sel.
        0: SOC to RUN state, 1: all active CPU acknowledge done */
    uint16_t wkup_ack_val; /**< timeout value for wakeup event receive to all active cpu acknowledge done */
    uint16_t soc_run_val; /**< timeout value for wakeup event receive to soc goes into RUN state */
} sdrv_smc_soc_wakeup_ctrl_t;

/**
 * @brief Definition for SMC global control setting.
 */
typedef struct sdrv_smc_global_control {
    sdrv_smc_core_e saf_pri_core; /**< safety domain primary core */
    sdrv_smc_core_e ap_pri_core; /**< ap domain primary core */
    bool ap_off_en; /**< ap domain power off, smc will only handshake with saf domain */
    bool lp_align_saf_en; /**< ap domain lp align with saf enable */
    bool wkup_align_saf_en; /**< ap domain wakeup align with saf enable */
} sdrv_smc_global_control_t;

/**
 * @brief System work mode controller instance.
 */
typedef struct sdrv_smc {
    paddr_t base;

    /* recored for wakeup irq info */
    uint32_t core_irq_status[5];
    uint32_t group_irq_status[8];
} sdrv_smc_t;

/**
 * @brief Config Safety/AP domain global control setting.
 *
 * @param [in] smc_ctrl SMC controller instance.
 * @param [in] config global control setting.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_ctrl_global_control_config(sdrv_smc_t *smc_ctrl, sdrv_smc_global_control_t *config);

/**
 * @brief Set primary core id for a power domain.
 *
 * This function sets primary core id for a domain. Only the primary CPU of
 * one domain can configure SMC registers of this domain.
 *
 * @param [in] smc_ctrl SMC controller instance
 * @param [in] domain Safety or AP domain
 * @param [in] id core id
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_ctrl_set_primary_core(sdrv_smc_t *smc_ctrl, sdrv_smc_domain_e domain, sdrv_smc_core_e id);

/**
 * @brief Set low power mode for specific domain.
 *
 * This function sets the low power mode of specific domain. The domain will
 * enter required low power mode after its primary CPU and all CPUs belongs
 * to this domain enters WFI.
 *
 * @param [in] smc_ctrl SMC controller instance
 * @param [in] domain Safety or AP domain
 * @param [in] mode Sleep or hibernate after primary CPU WFI
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_ctrl_set_lowpower_mode(sdrv_smc_t *smc_ctrl, sdrv_smc_domain_e domain, sdrv_smc_mode_e mode);

/**
 * @brief Enable or disable low power for specific domain
 *
 * This function enable or disable low power mode for a domain. If enabled, when all
 * CPUs enter wfi, SMC will start low power procedure.
 *
 * @param [in] smc_ctrl SMC controller instance
 * @param [in] domain safety or ap domain
 * @param [in] enable true/false
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_ctrl_lowpower_enable(sdrv_smc_t *smc_ctrl, sdrv_smc_domain_e domain, bool enable);

/**
 * @brief Control power switchs in low power mode.
 *
 * This function configures power switch states (on or off) in domain
 * low power mode.
 *
 * @param [in] smc_ctrl SMC controller instance
 * @param [in] power_switch The power switch to configure.
 * @param [in] mode The SMC low power mode to configure the switch for.
 * @param [in] power_down_enable True to turn off the switch in low power
 *  mode. False to leave the power switch on in lower power mode.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_ctrl_power_switch_config(sdrv_smc_t *smc_ctrl, sdrv_smc_power_switch_e power_switch,
                                           sdrv_smc_mode_e mode, bool power_down_enable);

/**
 * @brief Config Power switch in low power delay control.
 *
 * @param [in] smc_ctrl SMC controller instance.
 * @param [in] power_switch The power switch to configure.
 * @param [in] iso_en isolation enable delay between LP process start to X_iso_en pose.
 * @param [in] pg power gate delay between X_iso_en pose to X_pwr_gate pose.
 * @param [in] po power on delay between RUN process start to X_pwr_gate nege.
 * @param [in] iso_dis isolation disable delay between X_pwr_gate nege to X_iso_en_nege.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_ctrl_power_switch_delay_config(sdrv_smc_t *smc_ctrl, sdrv_smc_power_switch_e power_switch,
                                                 uint32_t iso_en, uint32_t pg, uint32_t po, uint32_t iso_dis);

/**
 * @brief Configure low power modes for core RAM
 *
 * This function configures low power modes for core RAMs, i.e, module interal
 * RAM, CPU cache and TCM, in SMC sleep or hibernate modes.
 *
 * @param [in] smc_ctrl SMC controller instance
 * @param [in] ram RAM type.
 * @param [in] mode The SMC low power mode to configure RAM mode for.
 * @param [in] ram_mode RAM mode in SMC low power mode. Possible values are:
 *        SDRV_SMC_CHIP_ENABLE
 *        SDRV_SMC_SELECTIVE_PRECHARGE
 *        SDRV_SMC_RETENTION_1
 *        SDRV_SMC_RETENTION_2
 *        SDRV_SMC_POWER_DOWN
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_ctrl_ram_lowpower_config(sdrv_smc_t *smc_ctrl, sdrv_smc_ram_e ram, sdrv_smc_mode_e mode,
                                           uint32_t ram_mode);

/**
 * @brief Config SMC Saf or AP domain tiemout setting.
 *
 * @param [in] smc_ctrl SMC controller instance.
 * @param [in] domain SDRV_SMC_SAF or SDRV_SMC_AP.
 * @param [in] config domain timeout setting.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_ctrl_domain_timeout_config(sdrv_smc_t *smc_ctrl, sdrv_smc_domain_e domain,
                                             sdrv_smc_domain_timeout_t *config);

/**
 * @brief Config SMC Saf or AP domain misc.
 *
 * @param [in] smc_ctrl SMC controller instance.
 * @param [in] domain SDRV_SMC_SAF or SDRV_SMC_AP.
 * @param [in] lp_trans_req LP mode transition request.
 * @param [in] irq_mask_dly Interrupt mask delay.
 * @param [in] ill_trans_wkup_en Swm illegal transfer wakeup enable.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_ctrl_domain_misc_config(sdrv_smc_t *smc_ctrl, sdrv_smc_domain_e domain,
                                          bool lp_trans_req, uint32_t irq_mask_dly, bool ill_trans_wkup_en);

/**
 * @brief AP domain smc config
 *
 * This function configures whether AP power state has impacts to the
 * SMC state machine.
 *
 * @param [in] smc_ctrl SMC controller instance
 * @param [in] ap_bitmap OR'ed value of the following macros:
 *                      SDRV_SMC_IGNORE_AP
 *                      SDRV_SMC_AP_LP_ALIGN_SAF
 *                      SDRV_SMC_AP_WK_ALIGN_SAF
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_ctrl_ap_domain_config(sdrv_smc_t *smc_ctrl, uint32_t ap_bitmap);

/**
 * @brief Configure RC oscillator state in hibernate mode.
 *
 * This function configures on chip 24M RC oscillator state in SMC
 * hibernate mode.
 *
 * @param [in] smc_ctrl SMC controller instance
 * @param [in] enable True to enable the RC oscillator in hibernate mode.
 *  False to turn off the oscillator in hibernate mode.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_ctrl_rc24m_hibernate_enable(sdrv_smc_t *smc_ctrl, bool enable);

/**
 * @brief Set pre-divider number for 24M and 32K clock.
 *
 * @param [in] smc_ctrl SMC controller instance.
 * @param [in] div_32k pre div number for clk32k.
 * @param [in] div_24m pre div number for clk24m.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_ctrl_pre_divider_config(sdrv_smc_t *smc_ctrl, uint16_t div_32k, uint8_t div_24m);

/**
 * @brief Set soc swm timeout setting.
 *
 * @param [in] smc_ctrl SMC controller instance.
 * @param [in] config soc timeout settings.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_ctrl_soc_swm_timeout_config(sdrv_smc_t *smc_ctrl, sdrv_smc_soc_timeout_t *config);

/**
 * @brief Set soc wakeup control setting.
 *
 * @param [in] smc_ctrl SMC controller instance.
 * @param [in] config soc wakeup control settings.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_ctrl_soc_wakeup_control_config(sdrv_smc_t *smc_ctrl, sdrv_smc_soc_wakeup_ctrl_t *config);

/**
 * @brief Clear all core wakeup acknowledge.
 *
 * @param [in] smc_ctrl SMC controller instance.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_ctrl_clear_core_wakeup_ack(sdrv_smc_t *smc_ctrl);

/**
 * @brief Set core wakeup acknowledge.
 *
 * @param [in] smc_ctrl SMC controller instance.
 * @param [in] ack bit0:cr5_saf, bit1:cr5_sp0, bit2:cr5_sp1, bit3:cr5_sx0, bit4:cr5_sx1
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_ctrl_set_core_wakeup_ack(sdrv_smc_t *smc_ctrl, uint32_t ack);

/**
 * @brief Set wakeup core irq enable or disable.
 *
 * @param [in] smc_ctrl SMC controller instance.
 * @param [in] err_wkup smc error wakeup core irq enable
 *                      bit0:cr5_saf, bit1:cr5_sp0, bit2:cr5_sp1, bit3:cr5_sx0, bit4:cr5_sx1
 * @param [in] bk_wkup smc wakeup core irq enable
 *                      bit0:cr5_saf, bit1:cr5_sp0, bit2:cr5_sp1, bit3:cr5_sx0, bit4:cr5_sx1
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_ctrl_core_wakeup_irq_config(sdrv_smc_t *smc_ctrl, uint32_t err_wkup, uint32_t bk_wkup);

/**
 * @brief Get wakeup core irq status.
 *
 * @param [in] smc_ctrl SMC controller instance.
 * @return uint32_t bit0:cr5_saf, bit1:cr5_sp0, bit2:cr5_sp1, bit3:cr5_sx0, bit4:cr5_sx1
 */
uint32_t sdrv_smc_ctrl_get_core_wakeup_irq_status(sdrv_smc_t *smc_ctrl);

/**
 * @brief Clear wakeup core irq status.
 *
 * @param [in] smc_ctrl SMC controller instance.
 * @param [in] clr clear status bit.
 *                 bit0:cr5_saf, bit1:cr5_sp0, bit2:cr5_sp1, bit3:cr5_sx0, bit4:cr5_sx1
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_ctrl_clear_core_wakeup_irq_status(sdrv_smc_t *smc_ctrl, uint32_t clr);

/**
 * @brief Config SOC misc.
 *
 * @param [in] smc_ctrl SMC controller instance.
 * @param [in] ill_trans_wkup_en Swm illegal transfer wakeup enable.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_ctrl_soc_misc_config(sdrv_smc_t *smc_ctrl, bool ill_trans_wkup_en);

/**
 * @brief Config SMC misc.
 *
 * @param [in] smc_ctrl SMC controller instance.
 * @param [in] lpbk_force_check_en lpbk force check enable, loopback wdt div number check without req/ack active.
 * @param [in] permission_err_en enable for permission error as apbslverr.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_ctrl_smc_misc_config(sdrv_smc_t *smc_ctrl, bool lpbk_force_check_en, bool permission_err_en);

/**
 * @brief Software trigger SMC handshake with other module.
 *
 * This function trigger SMC handshake with other module, after handshake
 * successful, that module will in low power mode, it will use configuration
 * in low power mode.
 *
 * @param [in] smc_ctrl SMC controller instance
 * @param [in] handshake module to handshake
 * @param [in] smc_swm system work mode, can be following macro:
 *          SDRV_SMC_SWM_RUN
 *          SDRV_SMC_SWM_SLP
 *          SDRV_SMC_SWM_HIB
 *          SDRV_SMC_SWM_RTC
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_ctrl_trigger_software_handshake(sdrv_smc_t *smc_ctrl, sdrv_smc_handshake_e handshake,
                                                  uint32_t smc_swm);

/**
 * @brief Software trigger SMC control power switch in run mode.
 *
 * This function can software override power switch value.
 *
 * @param [in] smc_ctrl SMC controller instance
 * @param [in] power_switch The power switch to configure.
 * @param [in] power_down_enable True to turn off the switch,
 *  False to turn on the switch.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_ctrl_trigger_software_power_switch(sdrv_smc_t *smc_ctrl,
            sdrv_smc_power_switch_e power_switch, bool power_down_enable);

/**
 * @brief Get power switch software power gate status.
 *
 * @param [in] smc_ctrl SMC controller instance
 * @param [in] power_switch The power switch to read.
 * @return Power gate status.
 */
bool sdrv_smc_ctrl_software_power_switch_gate_status(sdrv_smc_t *smc_ctrl, sdrv_smc_power_switch_e power_switch);

/**
 * @brief Software trigger SMC control core ram power down in run mode.
 *
 * This function can software override core ram power down value.
 *
 * @param [in] smc_ctrl SMC controller instance
 * @param [in] ram RAM type.
 * @param [in] ram_mode RAM mode override by software. Possible values are:
 *        SDRV_SMC_CHIP_ENABLE
 *        SDRV_SMC_SELECTIVE_PRECHARGE
 *        SDRV_SMC_RETENTION_1
 *        SDRV_SMC_RETENTION_2
 *        SDRV_SMC_POWER_DOWN
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_ctrl_trigger_software_ram_lowpower(sdrv_smc_t *smc_ctrl, sdrv_smc_ram_e ram,
                                                     uint32_t ram_mode);

/**
 * @brief Get SWM status monitor register.
 *
 * This function get SOC/SAF/AP system work mode, futhermode,
 * get SAF/AP internal system work mode.
 *
 * @param [in] smc_ctrl SMC controller instance
 * @return uint32_t
 *          0x1 Run, 0x2 Sleep, 0x4 Hibernate
 *          bit[3:0]-SAF_SWM, bit[7:4]-AP_SWM, bit[11:8]-SOC_SWM
 *          0x1 Run, 0x2 LP Proc, 0x4 Run Proc, 0x8 LP
 *          bit[20:16]-SAF_INTER_SWM, bit[25:21]-AP_INTER_SWM
 */
uint32_t sdrv_smc_ctrl_status_monitor(sdrv_smc_t *smc_ctrl);

/**
 * @brief Clear SMC timeout and illegal transition error status.
 *
 * @param [in] smc_ctrl SMC controller instance
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_ctrl_clear_timeout_illegal_status(sdrv_smc_t *smc_ctrl);

/**
 * @brief Get SMC timeout event monitor status.
 *
 * @param [in] smc_ctrl SMC controller instance
 * @return uint32_t
 */
uint32_t sdrv_smc_ctrl_timeout_status_monitor(sdrv_smc_t *smc_ctrl);

/**
 * @brief Get SMC illegal transition event monitor status.
 *
 * @param [in] smc_ctrl SMC controller instance
 * @return uint32_t
 */
uint32_t sdrv_smc_ctrl_illegal_status_monitor(sdrv_smc_t *smc_ctrl);

/**
 * @brief Mask all interrupt for SMC.
 *
 * This function mask all interrupt source in low power mode.
 *
 * @param [in] smc_ctrl SMC controller instance
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_ctrl_all_wakeup_interrupt_disable(sdrv_smc_t *smc_ctrl);

/**
 * @brief Enable wakeup interrupt for specific core.
 *
 * This function enable interrupt to wakeup specific core in low power mode.
 *
 * @param [in] smc_ctrl SMC controller instance
 * @param [in] core core id
 * @param [in] irq_num irq number
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_ctrl_wakeup_interrupt_enable(sdrv_smc_t *smc_ctrl, sdrv_smc_core_e core, uint32_t irq_num);

/**
 * @brief Disable wakeup interrupt for specific core.
 *
 * This function disable interrupt to wakeup specific core in low power mode.
 *
 * @param [in] smc_ctrl SMC controller instance
 * @param [in] core core id
 * @param [in] irq_num irq number
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_ctrl_wakeup_interrupt_disable(sdrv_smc_t *smc_ctrl, sdrv_smc_core_e core, uint32_t irq_num);

/**
 * @brief Clear all interrupt monitor register status.
 *
 * This function will clear all interrupt status in interrupt monitor register.
 *
 * @param [in] smc_ctrl SMC controller instance
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_ctrl_all_interrupt_monitor_clear(sdrv_smc_t *smc_ctrl);

/**
 * @brief Get specific core interrupt status in interrupt monitor register.
 *
 * This function get the interrupt status for specific core.
 *
 * @param [in] smc_ctrl SMC controller instance
 * @param [in] core core id
 * @param [in] irq_num irq number
 * @return true represents irq occurs, false represents no interrupt occurs.
 */
bool sdrv_smc_ctrl_interrupt_monitor_status(sdrv_smc_t *smc_ctrl, sdrv_smc_core_e core, uint32_t irq_num);

/**
 * @brief Clear specific core interrupt status in interrupt monitor register.
 *
 * This function clear the interrupt status for specific core.
 *
 * @param [in] smc_ctrl SMC controller instance
 * @param [in] core core id
 * @param [in] irq_num irq number
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_ctrl_clear_interrupt_monitor_status(sdrv_smc_t *smc_ctrl, sdrv_smc_core_e core,
                                                      uint32_t irq_num);

/**
 * @brief This function record monitor interrupt status when exit WFI.
 *
 * @param [in] smc_ctrl SMC controller instance
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_ctrl_record_interrupt_monitor_status(sdrv_smc_t *smc_ctrl);

/**
 * @brief Config SMC whether handshake with AP domain.
 *
 * This function config SMC ignore AP domain.
 *
 * @param [in] smc_ctrl SMC controller instance
 * @param [in] enable true represents SMC will handshake with AP,
 *  false represents SMC will not handshake with AP.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_ctrl_ap_domain_handshake(sdrv_smc_t *smc_ctrl, bool enable);

/**
 * @brief Debug for SMC status.
 *
 * This function choose a debug output type, and return the debug value.
 *
 * @param [in] smc_ctrl SMC controller instance.
 * @param [in] dbg_mux mux defined in sdrv_smc_debug_mux_e.
 * @return uint32_t
 */
uint32_t sdrv_smc_ctrl_debug_monitor(sdrv_smc_t *smc_ctrl, sdrv_smc_debug_mux_e dbg_mux);

/**
 * @brief Config Safety/AP domain global control setting.
 *
 * @param [in] config global control setting.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_global_control_config(sdrv_smc_global_control_t *config);

/**
 * @brief Set primary core id for a power domain.
 *
 * This function sets primary core id for a domain. Only the primary CPU of
 * one domain can configure SMC registers of this domain.
 *
 * @param [in] domain Safety or AP domain
 * @param [in] id core id
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_set_primary_core(sdrv_smc_domain_e domain, sdrv_smc_core_e id);

/**
 * @brief Set low power mode for specific domain.
 *
 * This function sets the low power mode of specific domain. The domain will
 * enter required low power mode after its primary CPU and all CPUs belongs
 * to this domain enters WFI.
 *
 * @param [in] domain Safety or AP domain
 * @param [in] mode Sleep or hibernate after primary CPU WFI
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_set_lowpower_mode(sdrv_smc_domain_e domain, sdrv_smc_mode_e mode);

/**
 * @brief Enable or disable low power for specific domain
 *
 * This function enable or disable low power mode for a domain. If enabled, when all
 * CPUs enter wfi, SMC will start low power procedure.
 *
 * @param [in] domain safety or ap domain
 * @param [in] enable true/false
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_lowpower_enable(sdrv_smc_domain_e domain, bool enable);

/**
 * @brief Control power switchs in low power mode.
 *
 * This function configures power switch states (on or off) in domain
 * low power mode.
 *
 * @param [in] power_switch The power switch to configure.
 * @param [in] mode The SMC low power mode to configure the switch for.
 * @param [in] power_down_enable True to turn off the switch in low power
 *  mode. False to leave the power switch on in lower power mode.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_power_switch_config(sdrv_smc_power_switch_e power_switch,
                                      sdrv_smc_mode_e mode, bool power_down_enable);

/**
 * @brief Config Power switch in low power delay control.
 *
 * @param [in] power_switch The power switch to configure.
 * @param [in] iso_en isolation enable delay between LP process start to X_iso_en pose.
 * @param [in] pg power gate delay between X_iso_en pose to X_pwr_gate pose.
 * @param [in] po power on delay between RUN process start to X_pwr_gate nege.
 * @param [in] iso_dis isolation disable delay between X_pwr_gate nege to X_iso_en_nege.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_power_switch_delay_config(sdrv_smc_power_switch_e power_switch,
                                            uint32_t iso_en, uint32_t pg, uint32_t po, uint32_t iso_dis);

/**
 * @brief Configure low power modes for core RAM
 *
 * This function configures low power modes for core RAMs, i.e, module interal
 * RAM, CPU cache and TCM, in SMC sleep or hibernate modes.
 *
 * @param [in] ram RAM type.
 * @param [in] mode The SMC low power mode to configure RAM mode for.
 * @param [in] ram_mode RAM mode in SMC low power mode. Possible values are:
 *        SDRV_SMC_CHIP_ENABLE
 *        SDRV_SMC_SELECTIVE_PRECHARGE
 *        SDRV_SMC_RETENTION_1
 *        SDRV_SMC_RETENTION_2
 *        SDRV_SMC_POWER_DOWN
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_ram_lowpower_config(sdrv_smc_ram_e ram, sdrv_smc_mode_e mode,
                                      uint32_t ram_mode);

/**
 * @brief Config SMC Saf or AP domain timeout setting.
 *
 * @param [in] domain SDRV_SMC_SAF or SDRV_SMC_AP.
 * @param [in] config domain timeout setting config.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_domain_timeout_config(sdrv_smc_domain_e domain, sdrv_smc_domain_timeout_t *config);

/**
 * @brief Config SMC Saf or AP domain misc.
 *
 * @param [in] domain SDRV_SMC_SAF or SDRV_SMC_AP.
 * @param [in] lp_trans_req LP mode transition request.
 * @param [in] irq_mask_dly Interrupt mask delay.
 * @param [in] ill_trans_wkup_en Swm illegal transfer wakeup enable.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_domain_misc_config(sdrv_smc_domain_e domain,
                                     bool lp_trans_req, uint32_t irq_mask_dly, bool ill_trans_wkup_en);

/**
 * @brief AP domain smc config
 *
 * This function configures whether AP power state has impacts to the
 * SMC state machine.
 *
 * @param [in] ap_bitmap OR'ed value of the following macros:
 *                      SDRV_SMC_IGNORE_AP
 *                      SDRV_SMC_AP_LP_ALIGN_SAF
 *                      SDRV_SMC_AP_WK_ALIGN_SAF
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_ap_domain_config(uint32_t ap_bitmap);

/**
 * @brief Configure RC oscillator state in hibernate mode.
 *
 * This function configures on chip 24M RC oscillator state in SMC
 * hibernate mode.
 *
 * @param [in] enable True to enable the RC oscillator in hibernate mode.
 *  False to turn off the oscillator in hibernate mode.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_rc24m_hibernate_enable(bool enable);

/**
 * @brief Set pre-divider number for 24M and 32K clock.
 *
 * @param [in] div_32k pre div number for clk32k.
 * @param [in] div_24m pre div number for clk24m.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_pre_divider_config(uint16_t div_32k, uint8_t div_24m);

/**
 * @brief Set soc swm timeout setting.
 *
 * @param [in] config soc timeout settings.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_soc_swm_timeout_config(sdrv_smc_soc_timeout_t *config);

/**
 * @brief Set soc wakeup control setting.
 *
 * @param [in] config soc wakeup control settings.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_soc_wakeup_control_config(sdrv_smc_soc_wakeup_ctrl_t *config);

/**
 * @brief Clear all core wakeup acknowledge.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_clear_core_wakeup_ack(void);

/**
 * @brief Set core wakeup acknowledge.
 *
 * @param [in] ack bit0:cr5_saf, bit1:cr5_sp0, bit2:cr5_sp1, bit3:cr5_sx0, bit4:cr5_sx1
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_set_core_wakeup_ack(uint32_t ack);

/**
 * @brief Set wakeup core irq enable or disable.
 *
 * @param [in] err_wkup smc error wakeup core irq enable
 *                      bit0:cr5_saf, bit1:cr5_sp0, bit2:cr5_sp1, bit3:cr5_sx0, bit4:cr5_sx1
 * @param [in] bk_wkup smc wakeup core irq enable
 *                      bit0:cr5_saf, bit1:cr5_sp0, bit2:cr5_sp1, bit3:cr5_sx0, bit4:cr5_sx1
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_core_wakeup_irq_config(uint32_t err_wkup, uint32_t bk_wkup);

/**
 * @brief Get wakeup core irq status.
 *
 * @return uint32_t bit0:cr5_saf, bit1:cr5_sp0, bit2:cr5_sp1, bit3:cr5_sx0, bit4:cr5_sx1
 */
uint32_t sdrv_smc_get_core_wakeup_irq_status(void);

/**
 * @brief Clear wakeup core irq status.
 *
 * @param [in] clr clear status bit.
 *                 bit0:cr5_saf, bit1:cr5_sp0, bit2:cr5_sp1, bit3:cr5_sx0, bit4:cr5_sx1
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_clear_core_wakeup_irq_status(uint32_t clr);

/**
 * @brief Config SOC misc.
 *
 * @param [in] ill_trans_wkup_en Swm illegal transfer wakeup enable.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_soc_misc_config(bool ill_trans_wkup_en);

/**
 * @brief Config SMC misc.
 *
 * @param [in] lpbk_force_check_en lpbk force check enable, loopback wdt div number check without req/ack active.
 * @param [in] permission_err_en enable for permission error as apbslverr.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_smc_misc_config(bool lpbk_force_check_en, bool permission_err_en);

/**
 * @brief Software trigger SMC handshake with other module.
 *
 * This function trigger SMC handshake with other module, after handshake
 * successful, that module will in low power mode, it will use configuration
 * in low power mode.
 *
 * @param [in] handshake module to handshake
 * @param [in] smc_swm system work mode, can be following macro:
 *          SDRV_SMC_SWM_RUN
 *          SDRV_SMC_SWM_SLP
 *          SDRV_SMC_SWM_HIB
 *          SDRV_SMC_SWM_RTC
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_trigger_software_handshake(sdrv_smc_handshake_e handshake,
                                             uint32_t smc_swm);

/**
 * @brief Software trigger SMC control power switch in run mode.
 *
 * This function can software override power switch value.
 *
 * @param [in] power_switch The power switch to configure.
 * @param [in] power_down_enable True to turn off the switch,
 *  False to turn on the switch.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_trigger_software_power_switch(
            sdrv_smc_power_switch_e power_switch, bool power_down_enable);

/**
 * @brief Get power switch software power gate status.
 *
 * @param [in] power_switch The power switch to read.
 * @return Power gate status.
 */
bool sdrv_smc_software_power_switch_gate_status(sdrv_smc_power_switch_e power_switch);

/**
 * @brief Software trigger SMC control core ram power down in run mode.
 *
 * This function can software override core ram power down value.
 *
 * @param [in] ram RAM type.
 * @param [in] ram_mode RAM mode override by software. Possible values are:
 *        SDRV_SMC_CHIP_ENABLE
 *        SDRV_SMC_SELECTIVE_PRECHARGE
 *        SDRV_SMC_RETENTION_1
 *        SDRV_SMC_RETENTION_2
 *        SDRV_SMC_POWER_DOWN
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_trigger_software_ram_lowpower(sdrv_smc_ram_e ram,
                                                uint32_t ram_mode);

/**
 * @brief Get SWM status monitor register.
 *
 * This function get SOC/SAF/AP system work mode, futhermode,
 * get SAF/AP internal system work mode.
 *
 * @return uint32_t
 *          0x1 Run, 0x2 Sleep, 0x4 Hibernate
 *          bit[3:0]-SAF_SWM, bit[7:4]-AP_SWM, bit[11:8]-SOC_SWM
 *          0x1 Run, 0x2 LP Proc, 0x4 Run Proc, 0x8 LP
 *          bit[20:16]-SAF_INTER_SWM, bit[25:21]-AP_INTER_SWM
 */
uint32_t sdrv_smc_status_monitor(void);

/**
 * @brief Clear SMC timeout and illegal transition error status.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_clear_timeout_illegal_status(void);

/**
 * @brief Get SMC timeout event monitor status.
 *
 * @return uint32_t
 */
uint32_t sdrv_smc_timeout_status_monitor(void);

/**
 * @brief Get SMC illegal transition event monitor status.
 *
 * @return uint32_t
 */
uint32_t sdrv_smc_illegal_status_monitor(void);

/**
 * @brief Mask all interrupt for SMC.
 *
 * This function mask all interrupt source in low power mode.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_all_wakeup_interrupt_disable(void);

/**
 * @brief Enable wakeup interrupt for specific core.
 *
 * This function enable interrupt to wakeup specific core in low power mode.
 *
 * @param [in] core core id
 * @param [in] irq_num irq number
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_wakeup_interrupt_enable(sdrv_smc_core_e core, uint32_t irq_num);

/**
 * @brief Disable wakeup interrupt for specific core.
 *
 * This function disable interrupt to wakeup specific core in low power mode.
 *
 * @param [in] core core id
 * @param [in] irq_num irq number
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_wakeup_interrupt_disable(sdrv_smc_core_e core, uint32_t irq_num);

/**
 * @brief Clear all interrupt monitor register status.
 *
 * This function will clear all interrupt status in interrupt monitor register.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_all_interrupt_monitor_clear(void);

/**
 * @brief Get specific core interrupt status in interrupt monitor register.
 *
 * This function get the interrupt status for specific core.
 *
 * @param [in] core core id
 * @param [in] irq_num irq number
 * @return true represents irq occurs, false represents no interrupt occurs.
 */
bool sdrv_smc_interrupt_monitor_status(sdrv_smc_core_e core, uint32_t irq_num);

/**
 * @brief Clear specific core interrupt status in interrupt monitor register.
 *
 * This function clear the interrupt status for specific core.
 *
 * @param [in] core core id
 * @param [in] irq_num irq number
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_clear_interrupt_monitor_status(sdrv_smc_core_e core,
                                                 uint32_t irq_num);

/**
 * @brief This function record monitor interrupt status when exit WFI.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_record_interrupt_monitor_status(void);

/**
 * @brief Config SMC whether handshake with AP domain.
 *
 * This function config SMC ignore AP domain.
 *
 * @param [in] enable true represents SMC will handshake with AP,
 *  false represents SMC will not handshake with AP.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_smc_ap_domain_handshake(bool enable);

/**
 * @brief Debug for SMC status.
 *
 * This function choose a debug output type, and return the debug value.
 *
 * @param [in] dbg_mux mux defined in sdrv_smc_debug_mux_e.
 * @return uint32_t
 */
uint32_t sdrv_smc_debug_monitor(sdrv_smc_debug_mux_e dbg_mux);

#endif /* SDRV_SMC_H_ */
