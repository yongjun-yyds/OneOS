/**
 * @file sdrv_mmc_sdhci.h
 * @brief SemiDrive MMC SDHCI driver header file.
 *
 * @copyright Copyright (c) 2020  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef SDRV_MMC_SDHCI_H_
#define SDRV_MMC_SDHCI_H_

#include "sdrv_sdhci.h"


#ifndef CONFIG_ARCH_CACHE_LINE
#define CONFIG_ARCH_CACHE_LINE 32
#endif

/** @name MMC clk
 *  MMC clk macros definitions
 */
#define MMC_CLK_400KHZ 400000
#define MMC_CLK_25MHZ 25000000
#define MMC_CLK_50MHZ 50000000
#define MMC_CLK_100MHZ 100000000
#define MMC_CLK_200MHZ 200000000
#define MMC_CLK_400MHZ 400000000

/** @name Emmc Card bus commands
 *  Emmc Card bus commands macros definitions
 */
#define CMD0_GO_IDLE_STATE 0
#define CMD1_SEND_OP_COND 1
#define CMD2_ALL_SEND_CID 2
#define CMD3_SEND_RELATIVE_ADDR 3
#define CMD4_SET_DSR 4
#define CMD5_SLEEP_AWAKE 5
#define CMD6_SWITCH_FUNC 6
#define CMD7_SELECT_DESELECT_CARD 7
#define CMD8_SEND_EXT_CSD 8
#define CMD9_SEND_CSD 9
#define CMD10_SEND_CID 10
#define CMD12_STOP_TRANSMISSION 12
#define CMD13_SEND_STATUS 13
#define CMD15_GO_INACTIVE_STATUS 15
#define CMD16_SET_BLOCKLEN 16
#define CMD17_READ_SINGLE_BLOCK 17
#define CMD18_READ_MULTIPLE_BLOCK 18
#define CMD21_SEND_TUNING_BLOCK 21
#define CMD23_SET_BLOCK_COUNT 23
#define CMD24_WRITE_SINGLE_BLOCK 24
#define CMD25_WRITE_MULTIPLE_BLOCK 25
#define CMD28_SET_WRITE_PROTECT 28
#define CMD29_CLEAR_WRITE_PROTECT 29
#define CMD31_SEND_WRITE_PROT_TYPE 31
#define CMD32_ERASE_WR_BLK_START 32
#define CMD33_ERASE_WR_BLK_END 33
#define CMD35_ERASE_GROUP_START 35
#define CMD36_ERASE_GROUP_END 36
#define CMD38_ERASE 38
#define CMD52_SDIO_STOP_TRANSMISSION 52

/** @name Card type
 *  Card type macros definitions
 */
#define MMC_TYPE_STD_SD 0
#define MMC_TYPE_SDHC 1
#define MMC_TYPE_SDIO 2
#define MMC_TYPE_MMCHC 3
#define MMC_TYPE_STD_MMC 4

/** @name OCR Register
 *  OCR Register macros definitions
 */
#define MMC_OCR_17_19 (1U << 7)
#define MMC_OCR_27_36 (0x1FF << 15)
#define MMC_OCR_SEC_MODE (2U << 29)
#define MMC_OCR_BUSY (1U << 31)

/** @name Card status
 *  Card status macros definitions
 */
#define MMC_CARD_STATUS(x) ((x >> 9) & 0x0F)
#define MMC_TRAN_STATE 4
#define MMC_PROG_STATE 7
#define MMC_SWITCH_FUNC_ERR_FLAG (1 << 7)
#define MMC_STATUS_INACTIVE 0
#define MMC_STATUS_ACTIVE 1
#define MMC_READY_FOR_DATA (1 << 8)

/** @name EXT_CSD Offsets in the ext csd
 *  EXT_CSD Offsets in the ext csd macros definitions
 */
#define MMC_EXT_CSD_RST_N_FUNC 162
#define MMC_EXT_MMC_BUS_WIDTH 183
#define MMC_EXT_MMC_HS_TIMING 185
#define MMC_DEVICE_TYPE 196
#define MMC_EXT_MMC_DRV_STRENGTH 197
#define MMC_EXT_HC_WP_GRP_SIZE 221
#define MMC_SEC_COUNT4 215
#define MMC_SEC_COUNT3 214
#define MMC_SEC_COUNT2 213
#define MMC_SEC_COUNT1 212
#define MMC_PART_CONFIG 179
#define MMC_ERASE_GRP_DEF 175
#define MMC_USR_WP 171
#define MMC_ERASE_TIMEOUT_MULT 223
#define MMC_HC_ERASE_GRP_SIZE 224
#define MMC_PARTITION_CONFIG 179

/** @name Values for ext csd fields
 *  Values for ext csd fields macros definitions
 */
#define MMC_HS_TIMING 0x1
#define MMC_HS200_TIMING 0x2
#define MMC_HS400_TIMING 0x3
#define MMC_ACCESS_WRITE 0x3
#define MMC_SET_BIT 0x1
#define MMC_HS_DDR_MODE (BIT(2) | BIT(3))
#define MMC_HS_HS200_MODE (BIT(4) | BIT(5))
#define MMC_HS_HS400_MODE (BIT(6) | BIT(7))
#define MMC_SEC_COUNT4_SHIFT 24
#define MMC_SEC_COUNT3_SHIFT 16
#define MMC_SEC_COUNT2_SHIFT 8
#define MMC_HC_ERASE_MULT (512 * 1024)
#define RST_N_FUNC_ENABLE BIT(0)

/** @name BOOT Related
 *  BOOT Related macros definitions
 */
#define BOOT_PART_MIN_SIZE (128u * 1024u)
#define BOOT_SIZE_MULT 226

/** @name RPMB Related
 *  RPMB Related macros definitions
 */
#define RPMB_PART_MIN_SIZE (128u * 1024u)
#define RPMB_SIZE_MULT 168
#define REL_WR_SEC_C 222
#define PARTITION_ACCESS_MASK 0x7
#define MAX_RPMB_CMDS 0x3

/** @name Command related
 *  Command related macros definitions
 */
#define MMC_MAX_COMMAND_RETRY 1000
#define MMC_MAX_CARD_STAT_RETRY 10000
#define MMC_RD_BLOCK_LEN 512
#define MMC_WR_BLOCK_LEN 512
#define MMC_R1_WP_ERASE_SKIP BIT(15)
#define MMC_US_PERM_WP_DIS BIT(4)
#define MMC_US_PWR_WP_DIS BIT(3)
#define MMC_US_PERM_WP_EN BIT(2)
#define MMC_US_PWR_WP_EN BIT(0)
#define MMC_BLOCK_COUNT_MASK 0x0000ffff

/** @name MMC errors
 *  MMC errors macros definitions
 */
#define MMC_R1_BLOCK_LEN_ERR (1U << 29)
#define MMC_R1_ADDR_ERR (1U << 30)
#define MMC_R1_GENERIC_ERR (1U << 19)
#define MMC_R1_CC_ERROR (1U << 20)
#define MMC_R1_WP_VIOLATION (1U << 26)
#define MMC_R1_ADDR_OUT_OF_RANGE (1U << 31)

/** @name RCA of the card
 *  RCA of the card macros definitions
 */
#define MMC_RCA 2
#define MMC_CARD_RCA_BIT 16

/** @name Misc card
 *  Misc card macros definitions
 */
#define MMC_BLK_SZ 512
#define MMC_CARD_SLEEP (1 << 15)

/** @name check addr is out of range
 *  check addr is out of range macros definitions
 */
#define MMC_ADDR_OUT_OF_RANGE(resp) ((resp >> 31) & 0x01)

/** @name SD card related Macros, Arguments for commands
 *  SD card related Macros, Arguments for commands macros definitions
 */
#define MMC_SD_HC_VOLT_SUPPLIED 0x000001AA
#define MMC_SD_OCR 0x00FF8000
#define MMC_SD_S18R 0x1000000
#define MMC_SD_HC_HCS 0x40000000
#define MMC_SD_UHS_HCS 0x40000000
#define MMC_SD_DEV_READY 0x80000000
#define MMC_CARD_TYPE_UHS 0x2
#define MMC_CARD_TYPE_SDHC 0x1
#define MMC_CARD_TYPE_STD_SD 0x0
#define SD_CARD_RCA 0x0
#define MMC_SD_SWITCH_HS 0x80FFFFF1

#define SD_CMD8_MAX_RETRY 0x3
#define SD_ACMD41_MAX_RETRY 100

/** @name SCR(SD Card Register) related
 *  SCR(SD Card Register) related macros definitions
 */
#define SD_SCR_BUS_WIDTH 16
#define SD_SCR_SD_SPEC 24
#define SD_SCR_SD_SPEC3 15
#define SD_SCR_SD_STAT_AFTER_ERASE 23
#define SD_SCR_BUS_WIDTH_MASK 0xf0000
#define SD_SCR_SD_SPEC_MASK 0x0f000000
#define SD_SCR_SD_SPEC3_MASK 0x8000
#define SD_SCR_STAT_AFTER_ERASE_MASK 0x800000
#define SD_SCR_CMD23_SUPPORT BIT(1)
#define SD_SCR_WIDTH_4BIT BIT(2)


/** @name SSR related
 *  SSR related macros definitions
 */
#define MMC_SD_AU_SIZE_BIT 428
#define MMC_SD_AU_SIZE_LEN 4
#define MMC_SD_ERASE_SIZE_BIT 408
#define MMC_SD_ERASE_SIZE_LEN 16
#define MMC_SD_ERASE_TOUT_BIT 402
#define MMC_SD_ERASE_TOUT_LEN 6
#define MMC_SD_ERASE_OFFSET_BIT 400
#define MMC_SD_ERASE_OFFSET_LEN 2

/*! @name Commands for SD card
 * Commands for SD card macros definitions
 */
#define CMD8_SEND_IF_COND 8
#define CMD11_SWITCH_VOLTAGE 11
#define ACMD6_SET_BUS_WIDTH 6
#define ACMD13_SEND_SD_STATUS 13
#define ACMD41_SEND_OP_COND 41
#define ACMD51_READ_CARD_SCR 51
#define CMD55_APP_CMD 55

#define EXT_CSD_BUF_SIZE ROUNDUP(512, CONFIG_ARCH_CACHE_LINE)

/*!
 * @}
 */ /* end of group Commands for SD card */

#define MMC_SAVE_TIMING(host, TIMING) host->timing = TIMING

/** @brief Can be used to unpack array of upto 32 bits data */
#define UNPACK_BITS(array, start, len, size_of)                                \
    ({                                                                         \
        uint32_t indx = (start) / (size_of);                                   \
        uint32_t offset = (start) % (size_of);                                 \
        uint32_t mask = (((len) < (size_of)) ? 1ULL << (len) : 0) - 1;         \
        uint32_t unpck = array[indx] >> offset;                                \
        uint32_t indx2 = ((start) + (len)-1) / (size_of);                      \
        if (indx2 > indx)                                                      \
            unpck |= array[indx2] << ((size_of)-offset);                       \
        unpck &mask;                                                           \
    })

#define swap_endian32(x)                                                       \
    ((uint32_t)((((uint32_t)(x) & (uint32_t)0x000000ffUL) << 24) |             \
                (((uint32_t)(x) & (uint32_t)0x0000ff00UL) << 8) |              \
                (((uint32_t)(x) & (uint32_t)0x00ff0000UL) >> 8) |              \
                (((uint32_t)(x) & (uint32_t)0xff000000UL) >> 24)))

#define MMC_CARD_SD(card)                                                      \
    ((card->type == MMC_CARD_TYPE_SDHC) || (card->type == MMC_CARD_TYPE_STD_SD)|| (card->type == MMC_CARD_TYPE_UHS))

#define MMC_CARD_MMC(card)                                                     \
    ((card->type == MMC_TYPE_STD_MMC) || (card->type == MMC_TYPE_MMCHC))

/** @brief CSD Register, not all the fields have been defined here */
struct mmc_csd {
    uint32_t cmmc_structure;   /**< the version of the CSD structure. */
    uint32_t spec_vers;        /**< System Specification version supported by the Device. */
    uint32_t card_cmd_class;   /**< card support cmd class */
    uint32_t write_blk_len;    /**< block length for write operations */
    uint32_t read_blk_len;     /**< block length for read operations */
    uint32_t r2w_factor;       /**< read access time */
    uint32_t sector_size;      /**< device sector size */
    uint32_t c_size_mult;      /**< computing the total device size */
    uint32_t c_size;           /**< used capacity for devices up to 2 GB of density */
    uint32_t nsac_clk_cycle;   /**< the clock dependent factor of the data access time */
    uint32_t taac_ns;          /**< the asynchronous part of the data access time. */
    uint32_t tran_speed;       /**< Maximum bus clock frequency definition */
    uint32_t erase_grp_size;   /**< used to calculate the size of the erasable unit of the Device. */
    uint32_t erase_grp_mult;   /**< calculating the size of the erasable unit of the Device. */
    uint32_t wp_grp_size;      /**< the minimum size of a write protected region */
    uint32_t wp_grp_enable : 1; /**< whether write protection of regions is possible */
    uint32_t perm_wp : 1;       /**< protects the whole device */
    uint32_t temp_wp : 1;       /**< Temporarily protects the whole Device content from being overwritten or erased */
    uint32_t erase_blk_len : 1; /**< supported maximum erase data block length */
    uint32_t read_blk_misalign : 1;  /**< read over more than one physical block */
    uint32_t write_blk_misalign : 1; /**< write over more than one physical block */
    uint32_t read_blk_partial : 1;   /**< whether partial block sizes can be used in block read commands. */
    uint32_t write_blk_partial : 1;  /**< whether partial block sizes can be used in block write commands. */
};

/** @brief CID Register */
struct mmc_cid {
    uint32_t mid;   /**< 8 bit manufacturer id */
    uint32_t oid;   /**< 16 bits 2 character ASCII - OEM ID */
    uint8_t pnm[7]; /**< 6 character ASCII -  product name */
    uint32_t prv;   /**< 8 bits - product revision */
    uint32_t psn;   /**< 32 bits - product serial number */
    uint32_t month; /**< 4 bits manufacturing month */
    uint32_t year;  /**< 4 bits manufacturing year */
};

/** @brief SCR register for SD card */
struct mmc_sd_scr {
    uint32_t bus_widths;    /**< Bus width support, 8 or 1 bit */
    uint32_t sd_spec;       /**< sd spec version */
    uint32_t sd3_spec;      /**< sd spec 3 version */
    uint32_t cmd23_support; /**< cmd23 supported or not */
    uint32_t data_stat_after_erase; /**< data value after erase */
};

/** @brief SD Status Register */
struct mmc_sd_ssr {
    uint32_t au_size;       /**< Allocation unit (AU) size */
    uint32_t num_aus;       /**< Number of AUs */
    uint32_t erase_timeout; /**< Erase timeout of one AU */
    uint32_t erase_offset;  /**< Erase time offset */
};

/** @brief mmc card register */
struct mmc_card {
    uint32_t rca;          /**< Relative addres of the card*/
    uint32_t ocr;          /**< Operating range of the card*/
    uint32_t block_size;   /**< Block size for the card */
    uint32_t wp_grp_size;  /**< WP group size for the card */
    uint64_t capacity;     /**< card capacity */
    uint32_t type;         /**< Type of the card */
    uint32_t status;       /**< Card status */
#if WITH_KERNEL_VM
    uint8_t *ext_csd;      /**< Ext CSD for the card info */
#else
    uint8_t ext_csd[EXT_CSD_BUF_SIZE] __ALIGNED(CONFIG_ARCH_CACHE_LINE);
#endif
    uint32_t raw_csd[4];   /**< Raw CSD for the card */
    uint32_t raw_scr[2];   /**< SCR for SD card */
    uint32_t boot_size;    /**< Size of boot partition */
    uint32_t rpmb_size;    /**< Size of rpmb partition */
    uint32_t rel_wr_count; /**< Reliable write count */
    struct mmc_cid cid;    /**< CID structure */
    struct mmc_csd csd;    /**< CSD structure */
    struct mmc_sd_scr scr; /**< SCR structure */
    struct mmc_sd_ssr ssr; /**< SSR Register */
};

/** @brief mmc device config data */
struct mmc_config_data {
    uint8_t slot;          /**< Sdcc slot used */
    addr_t sdhc_base;      /**< Base address for the sdhc */
    uint32_t irq;          /**< irq num */
    uint16_t bus_width;    /**< Bus width used */
    uint32_t max_clk_rate; /**< Max clock rate supported */
    uint32_t voltage;      /**< bus voltage */
    uint8_t ddr50_support; /**< SDHC ddr50 mode supported or not */
    uint8_t hs200_support; /**< SDHC HS200 mode supported or not */
    uint8_t hs400_support; /**< SDHC HS400 mode supported or not */
    uint8_t use_io_switch; /**< IO pad switch flag for shared sdc controller */
    uint8_t hw_reset_support; /**< SDHC hardware reset supported or not */
    uint8_t uhs_support; /**< SDHC hardware supported uhs mode */
    void *clk;             /**< Clock config data */
};

/** @brief mmc read write erase operation */
enum mmc_opt {
    MMC_OPT_READ = 1,
    MMC_OPT_WRITE,
    MMC_OPT_ERASE,
    MMC_OPT_MAX,
};

/** @brief mmc operation return result */
enum mmc_opt_result {
    MMC_OPT_COMPLETE = 0,
    MMC_OPT_FAILED,
    MMC_OPT_PENDING,
    MMC_OPT_INCONSISTENT,
    MMC_OPT_INVALID,
    MMC_OPT_RESULET_MAX,
};

/** @brief mmc event handle callback function. */
typedef void (*mmc_notification)(enum mmc_opt type, enum mmc_opt_result result);


/** @brief mmc device structure */
struct mmc_device {
    struct sdhci_host host;        /**< Handle to host controller */
    struct mmc_card card;          /**< Handle to mmc card */
    struct mmc_config_data config; /**< Handle for the mmc config data */
    uint32_t erase_grp_size;

    /**< if the async_mode is ture,
     * the erase/read/write api is asynchronous functions,
     * the user need register the notification event handle,
     * ctrl the mmc complete or error event.
     */
    bool async_mode;
    enum mmc_opt opt_type;
    enum mmc_opt_result opt_result;
    mmc_notification event_handle;

    //osMutexId_t mmc_mutex;
    uint32_t part_curr; /**< current Part partition */
};

/** @brief mmc partition */
enum part_access_type {
    PART_ACCESS_DEFAULT = 0x0,
    PART_ACCESS_BOOT1,
    PART_ACCESS_BOOT2,
    PART_ACCESS_RPMB,
    PART_ACCESS_GP1,
    PART_ACCESS_GP2,
    PART_ACCESS_GP3,
    PART_ACCESS_GP4
};

/**
 * @brief MMC status return code
 */
enum
{
    SDRV_STATUS_MMC_CMD_ERR = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_MMC, 0),             /* MMC status for cmd error. */
    SDRV_STATUS_MMC_DAT_ERR = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_MMC, 1),             /* MMC status for data error. */
    SDRV_STATUS_MMC_MISC_ERR = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_MMC, 2),             /* MMC status for misc error. */
    SDRV_STATUS_MMC_CMD_ABORT = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_MMC, 3),             /* MMC status for cmd abort. */
    SDRV_STATUS_MMC_DAT_ABORT = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_MMC, 4),             /* MMC status for data abort. */
    SDRV_STATUS_MMC_TUNING_FAIL = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_MMC, 5),             /* MMC status for tuning fail. */
    SDRV_STATUS_MMC_CARD_STATUS_ERR = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_MMC, 6),             /* MMC status for card status error. */
    SDRV_STATUS_MMC_SWITCH_FUNC_ERR = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_MMC, 7),             /* MMC status for switch function error. */
    SDRV_STATUS_MMC_CMD_RSP_ERR = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_MMC, 8),             /* MMC status for cmd response error. */
    SDRV_STATUS_MMC_DEV_TYPE_ERR = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_MMC, 9),             /* MMC status for device type error. */
};

/**
 * @brief alloc the mmc device memory.
 * @return pointer to mmc device
 * @retval alloc mmc device memory ptr
 * @retval null: failed
 */
struct mmc_device *sdrv_mmc_alloc_dev(void);

/**
 * @brief initialize the mmc card
 * @param[in] dev mmc device structure
 * @details entry point to mmc boot process initialize the sd host
 * controller initialize the mmc card set the clock & high speed mode
 * @return int
 * @retval 0: success
 * @retval other: failed
 */
status_t sdrv_mmc_sdrv_sdhci_init(struct mmc_device *dev);


/**
 * @brief Initialize the mmc platform driver
 * @param[in] dev mmc device structure
 * @priv_data[in] specific host info
 * @return int
 * @retval 0: success
 * @retval other: failed
 */
status_t sdrv_mmc_platform_init(struct mmc_device *dev, void *priv_data);


/**
 * @brief mmc sdhci read
 * @details Read required number of blocks from card into destination
 * @param[in] dev mmc device structure
 * @param[in] dest destination address
 * @param[in] blk_addr mmc device block address
 * @param[in] num_blocks number of blocks
 * @return uint32_t
 * @retval 0: success
 * @retval other: failed
 */
status_t sdrv_mmc_sdhci_read(struct mmc_device *dev, void *dest,
                             uint32_t blk_addr,
                             uint32_t num_blocks);

/**
 * @brief mmc sdhci write
 * @details write requried number of blocks from source to card
 * @param[in] dev mmc device structure
 * @param[in] src source address
 * @param[in] blk_addr mmc device block address
 * @param[in] num_blocks number of blocks
 * @return uint32_t
 * @retval 0: success
 * @retval other: failed
 */
status_t sdrv_mmc_sdhci_write(struct mmc_device *dev, void *src,
                              uint32_t blk_addr,
                              uint32_t num_blocks);

/**
 * @brief mmc sdhci erase
 * @details write requried number of blocks from source to card
 * @param[in] dev mmc device structure
 * @param[in] blk_addr mmc device block address
 * @param[in] len erase length
 * @return uint32_t
 * @retval 0: success
 * @retval other: failed
 */
status_t sdrv_mmc_sdhci_erase(struct mmc_device *dev, uint32_t blk_addr,
                              uint64_t len);

/**
 * @brief mmc sdhci read part
 * @details Read required number of blocks from card part into destination
 * @param[in] dev mmc device structure
 * @param[in] dest destination address
 * @param[in] blk_addr mmc device block address
 * @param[in] num_blocks number of blocks
 * @param[in] type type of the card
 * @return uint32_t
 * @retval 0: success
 * @retval other: failed
 */
status_t sdrv_mmc_sdhci_read_part(struct mmc_device *dev, void *dest,
                                   uint32_t blk_addr,
                                   uint32_t num_blocks, uint32_t type);
/**
 * @brief mmc sdhci Write part
 * @details Write requried number of blocks from source to card part
 * @param[in] dev mmc device structure
 * @param[in] src source address
 * @param[in] blk_addr mmc device block address
 * @param[in] num_blocks number of blocks
 * @param[in] type type of the card
 * @return uint32_t
 * @retval 0: success
 * @retval other: failed
 */
status_t sdrv_mmc_sdhci_write_part(struct mmc_device *dev, void *src,
                                    uint32_t blk_addr,
                                    uint32_t num_blocks, uint32_t type);
/**
 * @brief mmc sdhci Erase part
 * @details Erase len bytes (after converting to number of erase groups), from specified address
 * @param[in] dev mmc device structure
 * @param[in] blk_addr mmc device block address
 * @param[in] len erase length
 * @param[in] type type of the card
 * @return uint32_t
 * @retval 0: success
 * @retval other: failed
 */
status_t sdrv_mmc_sdhci_erase_part(struct mmc_device *dev,
                                        uint32_t blk_addr,
                                        uint32_t num_blocks, uint32_t type);

/**
 * @brief cancel the previous operation read/write/erase
 * @param[in] dev mmc device structure
 * @return uint32_t
 * @retval 0: success
 * @retval other: failed
 */
status_t sdrv_mmc_sdhci_cancel(struct mmc_device *dev);

/**
 * @brief mmc set/clear wp on user area
 * @details write protect or release len bytes (after converting to number of write
 * protect groups) from specified start address
 * @param[in] dev mmc device structure
 * @param[in] addr mmc device block address
 * @param[in] len length
 * @param[in] set_clr write protect flag
 * @return uint32_t
 * @retval 0: success
 * @retval other: failed
 */
status_t sdrv_mmc_set_clr_power_on_wp_user(struct mmc_device *dev,
        uint32_t addr,
        uint64_t len, uint8_t set_clr);


/**
 * @brief mmc get wp status
 * @details get the wp status of write protect groups starting at addr
 * @param[in] dev mmc device structure
 * @param[in] addr mmc device block address
 * @param[in] wp_status wp group status
 * @return uint32_t
 * @retval 0: success
 * @retval other: failed
 */
status_t sdrv_mmc_get_wp_status(struct mmc_device *dev, uint32_t addr,
                                uint8_t *wp_status);
/**
 * @brief put the mmc card to sleep
 * @param[in] dev mmc device structure
 */
void sdrv_mmc_put_card_to_sleep(struct mmc_device *dev);


/**
 * @brief change the driver type of the card
 * @details get the wp status of write protect groups starting at addr
 * @param[in] host sdhci host structure
 * @param[in] card mmc card register
 * @param[in] drv_type mmc switch type
 * @return bool
 * @retval true: success
 * @retval false: failed
 */
bool sdrv_mmc_set_drv_type(struct sdhci_host *host, struct mmc_card *card,
                           uint8_t drv_type);

/**
 * @brief switch the partition
 * @details switch emmc partition: boot1/2, rpmb, gp1/2/3/4
 * @param[in] dev mmc device structure
 * @param[in] type mmc partition type
 * @return uint32_t
 * @retval 0: success
 * @retval other: failed
 */
status_t sdrv_mmc_sdhci_switch_part(struct mmc_device *dev, uint32_t type);

/**
 * @brief sdhci rpmb send
 * @details Send the read & write command sequence to rpmb
 * @param[in] dev mmc device structure
 * @param[in] cmd mmc command structure
 * @return uint32_t
 * @retval 0: success
 * @retval other: failed
 */
status_t sdrv_mmc_sdhci_rpmb_send(struct mmc_device *dev,
                                  struct mmc_command *cmd);

/**
 * @brief sdhci rpmb send
 * @details Send the read & write command sequence to rpmb
 * @param[in] dev mmc device structure
 * @param[in] type type of partition
 * @return uint64_t
 * @retval 0: failed
 * @retval other: size of partition
 */
uint64_t sdrv_mmc_get_size(struct mmc_device *dev, uint32_t type);

/**
 * @brief mmc get erase group size
 * @param[in] dev mmc device structure
 * @return uint32_t
 * @retval erase group size;
 */
uint32_t sdrv_mmc_get_erase_group_size(struct mmc_device *dev);

/**
 * @brief mmc Get card block size
 * @param[in] dev mmc device structure
 * @return uint32_t
 * @retval card block size;
 */
uint32_t sdrv_mmc_get_block_size(struct mmc_device *dev);

/**
 * @brief mmc send tuning
 * @param[in] dev mmc device structure
 * @param[in] opcode mmc send cmd
 * @param[in] bus_width data bus width
 * @retval 0: success
 * @retval other: failed
 */
status_t sdrv_mmc_send_tuning(struct mmc_device *dev, uint32_t opcode,
                         uint32_t bus_width);

/**
 * @brief mmc Get card data after erase
 * @param[in] dev mmc device structure
 * @return uint32_t
 * @retval get card data after erase;
 */
uint32_t sdrv_mmc_get_data_after_erase(struct mmc_device *dev);
#endif /* SDRV_MMC_SDHCI_H_ */
