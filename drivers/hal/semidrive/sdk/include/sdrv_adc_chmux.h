/**
 * @file sdrv_adc_chmux.h
 * @brief SemiDrive ADC channel mux definition file.
 *
 * @copyright Copyright (c) 2021  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef SDRV_ADC_CHMUX_H
#define SDRV_ADC_CHMUX_H

enum adc1_ch5n_mux {
    ADC1_CH5N_MUX_A0 = 0u,
    ADC1_CH5N_MUX_A2,
    ADC1_CH5N_MUX_A4,
    ADC1_CH5N_MUX_A6,
    ADC1_CH5N_MUX_A8,
    ADC1_CH5N_MUX_A10,
    ADC1_CH5N_MUX_A12,
    ADC1_CH5N_MUX_A14
};

enum adc1_ch5p_mux {
    ADC1_CH5P_MUX_A1 = 0u,
    ADC1_CH5P_MUX_A3,
    ADC1_CH5P_MUX_A5,
    ADC1_CH5P_MUX_A7,
    ADC1_CH5P_MUX_A9,
    ADC1_CH5P_MUX_A11,
    ADC1_CH5P_MUX_A13,
    ADC1_CH5P_MUX_A15
};

enum adc2_ch4n_mux {
    ADC2_CH4N_MUX_A0 = 0u,
    ADC2_CH4N_MUX_A2,
    ADC2_CH4N_MUX_A4,
    ADC2_CH4N_MUX_A6,
    ADC2_CH4N_MUX_A8,
    ADC2_CH4N_MUX_A10,
    ADC2_CH4N_MUX_A12,
    ADC2_CH4N_MUX_A14
};

enum adc2_ch4p_mux {
    ADC2_CH4P_MUX_A1 = 0u,
    ADC2_CH4P_MUX_A3,
    ADC2_CH4P_MUX_A5,
    ADC2_CH4P_MUX_A7,
    ADC2_CH4P_MUX_A9,
    ADC2_CH4P_MUX_A11,
    ADC2_CH4P_MUX_A13,
    ADC2_CH4P_MUX_A15
};

enum adc2_ch5n_mux {
    ADC2_CH5N_MUX_C0 = 0u,
    ADC2_CH5N_MUX_C2,
    ADC2_CH5N_MUX_C4,
    ADC2_CH5N_MUX_C6,
    ADC2_CH5N_MUX_C8,
    ADC2_CH5N_MUX_C10,
    ADC2_CH5N_MUX_C12,
    ADC2_CH5N_MUX_C14
};

enum adc2_ch5p_mux {
    ADC2_CH5P_MUX_C1 = 0u,
    ADC2_CH5P_MUX_C3,
    ADC2_CH5P_MUX_C5,
    ADC2_CH5P_MUX_C7,
    ADC2_CH5P_MUX_C9,
    ADC2_CH5P_MUX_C11,
    ADC2_CH5P_MUX_C13,
    ADC2_CH5P_MUX_C15
};

enum adc2_ch6n_mux {
    ADC2_CH6N_MUX_B8 = 0u,
    ADC2_CH6N_MUX_B9,
    ADC2_CH6N_MUX_B10,
    ADC2_CH6N_MUX_B11,
    ADC2_CH6N_MUX_B12,
    ADC2_CH6N_MUX_B13,
    ADC2_CH6N_MUX_B14,
    ADC2_CH6N_MUX_B15
};

enum adc3_ch5n_mux {
    ADC3_CH5N_MUX_C0 = 0u,
    ADC3_CH5N_MUX_C2,
    ADC3_CH5N_MUX_C4,
    ADC3_CH5N_MUX_C6,
    ADC3_CH5N_MUX_C8,
    ADC3_CH5N_MUX_C10,
    ADC3_CH5N_MUX_C12,
    ADC3_CH5N_MUX_C14
};

enum adc3_ch5p_mux {
    ADC3_CH5P_MUX_C1 = 0u,
    ADC3_CH5P_MUX_C3,
    ADC3_CH5P_MUX_C5,
    ADC3_CH5P_MUX_C7,
    ADC3_CH5P_MUX_C9,
    ADC3_CH5P_MUX_C11,
    ADC3_CH5P_MUX_C13,
    ADC3_CH5P_MUX_C15
};


#endif  /* SDRV_ADC_CHMUX_H */
