﻿/**
 * @file sdrv_multicapture
 * @brief SemiDrive etimer capture driver header file.
 *
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */
#ifndef SDRV_MULTI_CPT_H
#define SDRV_MULTI_CPT_H


#define ETIMER_IP_MAX_MODULE            (8U)    /*etimer的模块的最大数量*/
#define MULTI_CPT_MODULE_NUM            (2U)    /*支持multi capture模块的数量。当前只有etimer1 和 etimer2支持multi capture的功能*/
#define MULTI_CPT_MAX_IO_NUM            (32U)   /*支持multi capture模块,最大输入捕获的通道数量*/
#define MULTI_CPT_BURST_LEN             (1)     /*缓冲区数据buff的大小*/
#define MULTI_CPT_DMA_BURST_LEN         (1)     /*DMA每次搬运一个数据*/


typedef enum {
    MULTI_CPT_RISING_EDGE = 0u,                 /*上升沿*/
    MULTI_CPT_FALLING_EDGE                      /*下降沿*/
} sdrv_multicpt_edge_change_type;


typedef enum {
    MULTI_CPT_ACTIVE = 0u,                      /*通道Active*/
    MULTI_CPT_IDLE                              /*通道Idle*/
} sdrv_multicpt_input_state_type;


typedef enum {
    MULTI_CPT_ETIMER1 = 0U,
    MULTI_CPT_ETIMER2 = 1U,
    MULTI_CPT_ETIMER3 = 2U,
    MULTI_CPT_ETIMER4 = 3U,                     /*etimer的模块*/
} sdrv_multicpt_hw_module;

typedef enum {
    MULTI_CPT_SEL_HF_CLK = 0,                   /*默认值*/
    MULTI_CPT_SEL_AHF_CLK,
    MULTI_CPT_SEL_EXT_CLK,
    MULTI_CPT_SEL_LP_CLK,
} sdrv_multicpt_clock_ref_type;         /*时钟源*/


typedef struct {
    uint8_t  u8TimerModule;                      /*etimer模块ID*/
    uint8_t  u8MultiCptChannum;                  /*multi capture的数量*/
    uint16_t u16Prescaler;                       /*时钟分频系数*/
    sdrv_multicpt_clock_ref_type eClockRef;      /*时钟源*/
    uint32_t irq;                                /*中断号*/
    bool int_en;                                 /*是否使能中断还是DMA*/
} sdrv_multicpt_config;




extern uint8_t sdrv_get_chip_version(void);

/**
 * @brief sdrv_etimer_multicpt_init
 * @details multi cpt init.
 * @implements sdrv_etimer_multicpt_init
 * @return none
 */
extern void sdrv_etimer_multicpt_init(void);

/**
 * @brief sdrv_etimer_multicpt_module_init
 * @details multi cpt channel init.
 * @param[in] pmulticpt_config multi cpt channel config .
 * @implements sdrv_etimer_multicpt_module_init
 * @return none
 */
extern void sdrv_etimer_multicpt_module_init(sdrv_multicpt_config *pmulticpt_config);

/**
 * @brief sdrv_etimer_multicpt_module_deinit
 * @details multi cpt channel deinit.
 * @param[in] timer_module selected etimer module.
 * @implements sdrv_etimer_multicpt_module_deinit
 * @return none
 */
extern void sdrv_etimer_multicpt_module_deinit(uint8_t timer_module);

/**
 * @brief sdrv_etimer_multicpt_channel_start_capture
 * @details multi cpt channel start capture.
 * @param[in] timer_module selected etimer module.
 * @param[in] channel selected etimer channel.
 * @implements sdrv_etimer_multicpt_channel_start_capture
 * @return none
 */
extern void sdrv_etimer_multicpt_channel_start_capture(uint8_t timer_module, uint8_t channel);

/**
 * @brief sdrv_etimer_multicpt_channel_stop_capture
 * @details multi cpt channel stop capture.
 * @param[in] timer_module selected etimer module.
 * @param[in] channel selected etimer channel.
 * @implements sdrv_etimer_multicpt_channel_stop_capture
 * @return none
 */
extern void sdrv_etimer_multicpt_channel_stop_capture(uint8_t timer_module, uint8_t channel);

#endif