/**
 * @file sdrv_semag.h
 * @brief SemiDrive Semaphore Gate driver header file.
 *
 * User should initialize mbox module before using semag driver.
 *
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef SDRV_SEMAG_H_
#define SDRV_SEMAG_H_

#include <sdrv_common.h>
#include <types.h>

#define SDRV_SEMAG_NUM (32UL)
#define SDRV_SEMAG_PROCESSOR_NUM (8UL)

/**
 * @brief Tries to lock the SEMAG gate.
 *
 * This function tries to lock the specific SEMAG gate. If the gate has been
 * locked by another processor, this function returns an error code.
 *
 * @param [in] gate_idx  Gate number to lock.
 * @return SDRV_STATUS_INVALID_PARAM  The gate id is unvalid.
 *         SDRV_STATUS_FAIL  Semag gate has been locked by another processor.
 *         SDRV_STATUS_OK  Lock the semag gate successfully.
 */
status_t sdrv_semag_trylock(uint32_t gate_idx);

/**
 * @brief Locks the SEMAG gate.
 *
 * This function locks the specific SEMAG gate. If the gate has been
 * locked by other processors, this function waits until it is unlocked and then
 * lock it.
 *
 * @param [in] gate_idx  Gate number to lock.
 * @return SDRV_STATUS_INVALID_PARAM  The gate id is unvalid.
 *         SDRV_STATUS_OK  Lock the semag gate successfully.
 */
status_t sdrv_semag_lock(uint32_t gate_idx);

/**
 * @brief Unlocks the SEMAG gate.
 *
 * This function unlocks the specific SEMAG gate. It only writes unlock value
 * to the SEMAG gate register. However, it does not check whether the SEMAG gate
 * is locked by the current processor or not. As a result, if the SEMAG gate is
 * not locked by the current processor, this function has no effect.
 *
 * @param [in] gate_idx  Gate number to unlock.
 * @return SDRV_STATUS_OK Unlock the semag gate successfully.
 */
status_t sdrv_semag_unlock(uint32_t gate_idx);

/**
 * @brief Gets the status of the SEMAG gate.
 *
 * This function checks the lock status of a specific SEMAG gate.
 *
 * @param [in] gate_idx  Gate number to check lock status.
 * @return Return -1 if the gate is unlocked, otherwise return the
 * processor number which has locked the gate.
 */
int32_t sdrv_semag_get_lock_proc(uint32_t gate_idx);

/**
 * @brief Resets all SEMAG gates to an unlocked status.
 *
 * This function resets all SEMAG gate to an unlocked status,
 * it will only unlock gates which locked by itself, can't reset
 * gates which locked by other processors.
 * @return SDRV_STATUS_OK Reset all semag gates successfully.
 */
status_t sdrv_semag_reset_all_gate(void);

/**
 * @brief Enable the gate notification interrupt.
 *
 * Gate notification provides such feature, when interrupt is enabled, then core
 * tried to lock the gate and failed, it could get notification when the gate is
 * idle.
 *
 * @param [in] mask Mask of the gate index, for example: (1<<0U) | (1<<1U)
 * means gate 0 and gate 1.
 * @return SDRV_STATUS_OK Enable the notify interrupt of the semag gate
 * successfully.
 */
status_t sdrv_semag_enable_gate_notify_interrupt(uint32_t mask);

/**
 * @brief Disable the gate notification interrupt.
 *
 * Gate notification provides such feature, when interrupt is enabled, then core
 * tried to lock the gate and failed, it could get notification when the gate is
 * idle.
 *
 * @param [in] mask Mask of the gate index, for example: (1<<0U) | (1<<1U)
 * means gate 0 and gate 1.
 * @return SDRV_STATUS_OK Disable the notify interrupt of the semag gate
 * successfully.
 */
status_t sdrv_semag_disable_gate_notify_interrupt(uint32_t mask);

/**
 * @brief Get the gate notification flags.
 *
 * Gate notification provides such feature, when interrupt is enabled, then core
 * tried to lock the gate and failed, it could get notification when the gate is
 * idle.
 *
 * @return Mask of the gate index, for example: (1<<0U) | (1<<1U) means
 * gate 0 and gate 1 flags are pending.
 */
uint32_t sdrv_semag_get_notify_status(void);

/**
 * @brief Clear the gate notification interrupt status.
 *
 * This function clear SEMAG gate IRQ notifications.
 *
 * @param [in] mask Mask of the gate index, for example: (1<<0U) | (1<<1U)
 * means gate 0 and gate 1.
 * @return SDRV_STATUS_OK Clear the notify interrupt status of the semag gate
 * successfully.
 */
status_t sdrv_semag_clear_notify_status(uint32_t mask);

#endif /* SDRV_SEMAG_H_ */
