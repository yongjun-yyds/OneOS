/**
 * @file sdrv_xspi_slv.h
 * @brief SemiDrive common XSPI slave driver header file.
 *
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef _SDRV_XSPI_SLV_H_
#define _SDRV_XSPI_SLV_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <debug.h>
#include <stdlib.h>
#include <types.h>

#include <sdrv_ckgen.h>
#include <sdrv_common.h>
/**
 * @brief xspi slaver status error code.
 */
enum sdrv_xspi_master_error {
    SSDRV_STATUS_GROUP_XSPI_SLAVER_SET_FLAG_ATTR_ERR = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_XSPI_MST_SLV, 1), /**< xspi slaver set flag error */
    SSDRV_STATUS_GROUP_XSPI_SLAVER_GET_FLAG_ATTR_ERR = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_XSPI_MST_SLV, 2), /**< xspi slaver get flag error */
    SSDRV_STATUS_GROUP_XSPI_SLAVER_GET_STATUS_ERR = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_XSPI_MST_SLV, 3), /**< xspi slaver get status error */
};

enum sdrv_xspi_slv_bit_endian {
    XSPI_SLV_BIG_ENDIAN,    /**< xspi bus transfer big endian */
    XSPI_SLV_LITTLE_ENDIAN  /**< xspi bus transfer little endian */
};

enum sdrv_xspi_slv_tx_mode {
    XSPI_SLV_DQS_DIV2_ACLK, /**< QDS is DIV2 based on aclk */
    XSPI_SLV_DQS_SCLK       /**< QDS is based on sclk */
};

enum sdrv_xspi_slv_line {
    XSPI_SLV_1_LINE, /**< xspi slave line 00: 1-line */
    XSPI_SLV_4_LINE, /**< xspi slave line 01: 4-line */
    XSPI_SLV_8_LINE, /**< xspi slave line 10: 8-line */
    XSPI_SLV_16_LINE /**< xspi slave line 11: 16-line */
};

enum sdrv_xspi_slv_mode {
    XSPI_SLV_INDIRECT_MODE, /**< xspi slave indirect mode */
    XSPI_SLV_DIRECT_MODE    /**< xspi slave direct mode */
};

struct sdrv_xspi_slv_sclk_idle_time_conf {
    bool enable;
    uint8_t idle_time; /**< sclk idle time */
};

struct sdrv_xspi_slv_debounce_ctrl_conf {
    bool enable;
    uint8_t time; /**< debounce time (based on aclk) */
};

struct sdrv_xspi_slv_dummy_cycle_conf {
    uint8_t dummy_val; /**< slave dummy cycle configurations */
};

struct sdrv_xspi_slv_timeout_mon_ctrl_conf {
    bool enable;
    uint16_t time; /**< timeout monitor control */
};

struct sdrv_xspi_slv_aid_conf {
    bool enable;
    uint8_t aid; /**< read arid override value */
};

struct sdrv_xspi_slv_acache_conf {
    bool enable;
    uint8_t acache; /**< read arcache override value */
};

struct sdrv_xspi_slv_aprot_conf {
    bool enable;
    uint8_t aprot; /**< read arprot override value */
};

struct sdrv_xspi_slv_region_map_conf {
    bool enable;
    uint32_t offset_addr; /**< region map offset address */
    uint32_t begin_addr;  /**< region map begin address */
    uint32_t end_addr;    /**< region map end address */
    uint32_t id;          /**< region map select, have four regions */
};

struct sdrv_xspi_slv_mon_conf {
    bool addr_en;
    bool addr_lock; /**< monitor address lock */
    uint32_t begin_addr; /**< monitor begin address */
    uint32_t end_addr; /**< monitor end address */
    uint32_t id;          /**< region map select, have four regions */
};

/**
 * @brief xspi slave set attr.
 */
enum sdrv_xspi_slv_ioctl_flag {
    XSPI_SLV_FLAG_EN,             /**< xspi slave mode control */
    XSPI_SLV_FLAG_CRC_EN,         /**< CRC32 enable */
    XSPI_SLV_FLAG_BIT_ENDIAN,     /**< xspi bus transfer big endian enable */
    XSPI_SLV_FLAG_PAD_RESET_MASK, /**< soft reset enable */
    XSPI_SLV_FLAG_WR_CHK_EN,      /**< write check enable */
    XSPI_SLV_FLAG_TX_MODE,        /**< tx mode configurations */
    XSPI_SLV_FLAG_AXI_WR_EN,      /**< axi write operation enable*/
    XSPI_SLV_FLAG_AXI_RD_EN,      /**< axi read operation enable*/
    XSPI_SLV_FLAG_MODE,           /**< driect mode or indirect mode*/
    XSPI_SLV_FLAG_SCLK_IDLE_TIME, /**< sclk idle time */
    XSPI_SLV_FLAG_RATE,           /**< xspi slave rate */
    XSPI_SLV_FLAG_LINE,           /**< xspi slave line */
    XSPI_SLV_FLAG_DEBOUNCE_CTRL,  /**< xspi debounce time */
    XSPI_SLV_FLAG_TIMEOUT_CTRL,   /**< xspi timeout monitor time */
    XSPI_SLV_FLAG_ST_ACCESS,      /**< xspi slave status access permission by xspi master */
    XSPI_SLV_FLAG_ID0,            /**< ID0 value */
    XSPI_SLV_FLAG_ID1,            /**< ID1 value */
    XSPI_SLV_FLAG_PAD_INTB_MASK,  /**< pad intb mask value base on xspi slave status */
    XSPI_SLV_FLAG_PAD_CRCB_MASK,  /**< pad crcb mask value base on xspi slave status */
    XSPI_SLV_FLAG_INT_ST,         /**< irq interrupt status */
    XSPI_SLV_FLAG_INT_MASK,       /**< irq interrupt status mask */
    XSPI_SLV_FLAG_ST,             /**< xspi slave status */
    XSPI_SLV_FLAG_ST_UPDATE,      /**< xspi slave status update */
    XSPI_SLV_FLAG_CMD_WR_TRG,     /**< write trigger command (only valid in indirect mode) */
    XSPI_SLV_FLAG_CMD_RD_TRG,     /**< read trigger command (only valid in indirect mode) */
    XSPI_SLV_FLAG_CMD_WR_PACKET0, /**< write packet0 command (only valid in indirect mode) */
    XSPI_SLV_FLAG_CMD_WR_PACKET1, /**< write packet1 command (only valid in indirect mode) */
    XSPI_SLV_FLAG_CMD_WR_PAYLOAD, /**< write payload command (only valid in indirect mode) */
    XSPI_SLV_FLAG_CMD_WR_WDATA,   /**< write wdata command (only valid in indirect mode) */
    XSPI_SLV_FLAG_CMD_WR_ST,      /**< write status command (only valid in indirect mode) */
    XSPI_SLV_FLAG_CMD_RD_ID,      /**< read id command (only valid in indirect mode) */
    XSPI_SLV_FLAG_CMD_RD_ST,      /**< read status command (only valid in indirect mode) */
    XSPI_SLV_FLAG_CMD_RD_RDATA,   /**< read rdata command (only valid in indirect mode) */
    XSPI_SLV_FLAG_CMD_RD_RDATA_CRC, /**< read rdata with CRC command (only valid in indirect mode) */
    XSPI_SLV_FLAG_ARID_OVERRIDE,    /**< arid override value */
    XSPI_SLV_FLAG_ARCACHE_OVERRIDE, /**< arcache override value */
    XSPI_SLV_FLAG_ARPROT_OVERRIDE,  /**< arprot override value */
    XSPI_SLV_FLAG_AWID_OVERRIDE,    /**< awid override value */
    XSPI_SLV_FLAG_AWCACHE_OVERRIDE, /**< awcache override value */
    XSPI_SLV_FLAG_AWPROT_OVERRIDE,  /**< awprot override value */
    XSPI_SLV_FLAG_REGION_MAP,       /**< region map control */
    XSPI_SLV_FLAG_RD_MON,           /**< read monitor permission control */
    XSPI_SLV_FLAG_WR_MON,           /**< write monitor permission control */
    XSPI_SLV_FLAG_WR_DUMMY_CYCLE,   /**< set xspi slv dummy cycle */
    XSPI_SLV_FLAG_MAX               /**< end */
};

/**
 * @brief sdrv_xspi_slv slave configurations.
 */
struct sdrv_xspi_slv_conf {
    paddr_t base; /**< xspi slave register base address */
    uint16_t irq; /**< xspi slave irq number */

    bool crc_dis; /**< crc32 disable or not */
    enum sdrv_xspi_slv_bit_endian bit_endian; /**< big endian or little endian */
    enum sdrv_xspi_slv_tx_mode tx_mode;       /**< QDS is DIV2 based on aclk or  sclk */
    enum sdrv_xspi_slv_mode mode;             /**< indirect mode or direct mode */
    bool is_ddr;                              /**< ddr or sdr mode */
    enum sdrv_xspi_slv_line line;             /**< slave line select */

    /**< indirect configurations */
    uint8_t cmd_wr_trg;     /**< write trigger command */
    uint8_t cmd_rd_trg;     /**< read trigger command */
    uint8_t cmd_wr_packet0; /**< write package0 command */
    uint8_t cmd_wr_packet1; /**< write package1 command */
    uint8_t cmd_wr_payload; /**< write payload command */
    uint8_t cmd_wr_wdata;   /**< write rdata command */
    uint8_t cmd_wr_st;      /**< write status command */
    uint8_t cmd_rd_id;      /**< write status command */
    uint8_t cmd_rd_st;      /**< read trigger command */
    uint8_t cmd_rd_rdata;   /**< read trigger command */
    uint8_t cmd_rd_rdata_crc; /**< read rdata with CRC command */
};

/**
 * @brief slv_device
 */
struct sdrv_xspi_slv {
    paddr_t base; /**< xspi slave register base address */
    uint16_t irq; /**< xspi slave irq number */
};

/**
 * @brief xspi slave slave init.
 *
 * @param[in] xspi slave dev
 * @param[in] xspi slave config
 * @return int
 * @retval  0: success
 * @retval other: fail
 */
status_t sdrv_xspi_slv_init(struct sdrv_xspi_slv *slave,
                  const struct sdrv_xspi_slv_conf *conf);

/**
 * @brief xspi slave set attr.
 * @details This function used for xspi slave set attribute, and detailed flag reference structure definitions.
 * @param[in] xspi slave dev
 * @param[in] xspi slave ioctl flag
 * @param[in] xspi slave buf
 * @return int
 * @retval  0: success
 * @retval -1: fail
 */
status_t sdrv_xspi_slv_set_attr(struct sdrv_xspi_slv *slave,
                      enum sdrv_xspi_slv_ioctl_flag flag, const void *buf);

/**
 * @brief xspi slave get attr.
 * @details This function used for xspi slave get attribute, and detailed flag reference structure definitions.
 * @param[in] xspi slave dev
 * @param[in] xspi slave ioctl flag
 * @param[out] xspi slave buf
 * @return int
 * @retval  0: success
 * @retval other: fail
 */
status_t sdrv_xspi_slv_get_attr(struct sdrv_xspi_slv *slave,
                      enum sdrv_xspi_slv_ioctl_flag flag, void *buf);

/**
 * @brief xspi slave enable.
 * @details This function used for xspi enable receive data.
 * @param[in] xspi slave dev
 * @return int
 * @retval  0: success
 * @retval other: fail
 */
status_t sdrv_xspi_slv_enable(struct sdrv_xspi_slv *slave);

/**
 * @brief xspi slave disable.
 * @details This function used for xspi disable receive data.
 * @param[in] xspi slave dev
 * @return int
 * @retval  0: success
 * @retval other: fail
 */
status_t sdrv_xspi_slv_disable(struct sdrv_xspi_slv *slave);

/**
 * @brief xspi slave deinit.
 *
 * @param[in] xspi slave dev
 * @return int
 * @retval  0: success
 * @retval other: fail
 */
status_t sdrv_xspi_slv_deinit(struct sdrv_xspi_slv *slave);

/**
 * @brief xspi slave status.
 *
 * @param[in] xspi get slaver status
 * @return int
 * @retval  0: success
 * @retval other: fail
 */
status_t sdrv_xspi_slv_clr_status(struct sdrv_xspi_slv *slave);
#ifdef __cplusplus
}
#endif

#endif

