/**
 * @file sdrv_flexray.h
 * @brief SemiDrive Flexray driver header file.
 *
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef SDRV_FLEXRAY_H_
#define SDRV_FLEXRAY_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "sdrv_common.h"
#include "sdrv_flexray_driver.h"
#include "sdrv_flexray_general_types.h"

typedef enum {
    FR_UNINIT = 0U,  /* Flexray un-init */
    FR_INIT          /* Flexray init */
} Fr_SoftwareStatesType;

typedef Fr_ConfigType                   sdrv_flexray_config_type;
typedef Fr_CtrlCfgType                  sdrv_flexray_ctrl_cfg_type;
typedef Fr_ChannelType                  sdrv_flexray_channel_type;
typedef Fr_POCStatusType                sdrv_flexray_poc_status_type;
typedef Fr_ConfigurationType            sdrv_flexray_configuration_type;
typedef Fr_SoftwareStatesType           sdrv_flexray_software_states_type;
typedef Fr_SlotAssignmentType           sdrv_flexray_slot_assignment_type;
typedef Fr_CCTxBufferConfigType         sdrv_flexray_cc_tx_buffer_config_type;
typedef Fr_RxLPduStatusType             sdrv_flexray_rx_lpdu_status_type;
typedef Fr_TxLPduStatusType             sdrv_flexray_tx_lpdu_status_type;
typedef GlobalInterruptType             sdrv_flexray_global_interrupt_type;
typedef Fr_CCHardwareConfigType         sdrv_flexray_cc_hardware_config_type;
typedef Fr_CCLowLevelConfigSetType
sdrv_flexray_cc_low_level_config_set_type;
typedef Fr_CCRxBufferConfigType         sdrv_flexray_cc_rx_buffer_config_type;
typedef Fr_CCFifoConfigType             sdrv_flexray_cc_fifo_config_type;
typedef Fr_CCBufferConfigSetType        sdrv_flexray_cc_buffer_config_set_type;
typedef Fr_CCLpduInfoType               sdrv_flexray_cc_lpdu_info_type;
typedef Fr_CCBufferAddressType          sdrv_flexray_cc_buffer_address_type;
typedef Fr_CCBufferOffset16Type         sdrv_flexray_cc_buffer_offset16_type;

/**
 * @brief flexray status error code.
 */
typedef struct sdrv_flexray {
    uint32_t irq;                                               /**< flexray module irq num */
    uint32_t abs_timers_num;                                    /**< flexray module ads timer num */
    sdrv_flexray_software_states_type     status;               /**< flexray module init state */
    const sdrv_flexray_configuration_type *internal_cfg;        /**< flexray module configure */
} sdrv_flexray_t;

/**
 * @brief flexray status error code.
 */
enum sdrv_flexray_error {
    /* Error reported when parameter timer index exceeds number of available timers */
    SDRV_FLEXRAY_INV_TIMER_IDX      = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_FLEXRAY, 1),
    /* Error reported when invalid pointer in parameter list */
    SDRV_FLEXRAY_PARAM_POINTER      = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_FLEXRAY, 2),
    /* Error reported when parameter offset exceeds bounds */
    SDRV_FLEXRAY_INV_OFFSET         = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_FLEXRAY, 3),
    /* Error reported when invalid controller base */
    SDRV_FLEXRAY_INV_BASE_ADDR      = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_FLEXRAY, 4),
    /* Error reported when invalid channel index */
    SDRV_FLEXRAY_INV_CHNL_IDX       = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_FLEXRAY, 5),
    /* Error reported when parameter cycle exceeds 63 */
    SDRV_FLEXRAY_INV_CYCLE          = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_FLEXRAY, 6),
    /* Error reported when Fr module was not initialized */
    SDRV_FLEXRAY_INIT_FAILED        = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_FLEXRAY, 7),
    /* Error reported when Fr CC is not in the expected POC state */
    SDRV_FLEXRAY_INV_POCSTATE       = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_FLEXRAY, 8),
    /* Error reported when Payload length parameter has an invalid value */
    SDRV_FLEXRAY_INV_LENGTH         = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_FLEXRAY, 9),
    /* Error reported when invalid LPdu index */
    SDRV_FLEXRAY_INV_LPDU_IDX       = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_FLEXRAY, 10),
    /* Error reported when invalid FlexRay header CRC */
    SDRV_FLEXRAY_INV_HEADERCRC      = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_FLEXRAY, 11),
    /* Error reported when invalid value passed as parameter Fr_ConfigParamIdx. */
    SDRV_FLEXRAY_INV_CONFIG_IDX     = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_FLEXRAY, 12),
    /* Error reported when invalid framelist size value. */
    SDRV_FLEXRAY_INV_FRAMELIST_SIZE = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_FLEXRAY, 13),
    /* Error reported when requested resource is not configured to be available
      on the current core */
    SDRV_FLEXRAY_PARAM_CONFIG       = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_FLEXRAY, 14),
};

/**
 * @function_name    sdrv_flexray_init
 *
 * @brief            Controller initialization function
 * @param[in]        sdrv flexray handle
 * @return           SDRV_STATUS_OK or error code.
 */
extern status_t sdrv_flexray_init(sdrv_flexray_t *handle);

/**
 * @function_name    sdrv_flexray_controller_init
 *
 * @brief            CC configuration
 * @param[in]        sdrv flexray handle
 * @return           SDRV_STATUS_OK or error code.
 */
extern status_t sdrv_flexray_controller_init(sdrv_flexray_t *handle);

/**
 * @function_name    sdrv_flexray_start_communication
 *
 * @brief            Starts the communication
 * @param[in]        sdrv flexray handle
 * @return           SDRV_STATUS_OK or error code.
 */
extern status_t sdrv_flexray_start_communication(sdrv_flexray_t *handle);

/**
 * @function_name    sdrv_flexray_allow_coldstart
 *
 * @brief            Invokes the CC CHI command ALLOW_COLDSTART
 * @param[in]        sdrv flexray handle
 *
 * @return           SDRV_STATUS_OK or error code.
 */
extern status_t sdrv_flexray_allow_coldstart(sdrv_flexray_t *handle);

/**
 * @function_name    sdrv_flexray_all_slots
 *
 * @brief            Invokes the CC CHI command ALL_SLOTS
 * @param[in]        sdrv flexray handle
 * @return           SDRV_STATUS_OK or error code.
 */
extern status_t sdrv_flexray_all_slots(sdrv_flexray_t *handle);

/**
 * @function_name    sdrv_flexray_halt_communication
 *
 * @brief            This API call stops communication.
 * @param[in]        sdrv flexray handle
 * @return           SDRV_STATUS_OK or error code.
 */
extern status_t sdrv_flexray_halt_communication(sdrv_flexray_t *handle);

/**
 * @function_name    sdrv_flexray_abort_communication
 *
 * @brief            Abort the communication
 * @param[in]        sdrv flexray handle
 * @return           SDRV_STATUS_OK or error code.
 */
extern status_t sdrv_flexray_abort_communication(sdrv_flexray_t *handle);

/**
 * @function_name    sdrv_flexray_send_wakeup
 *
 * @brief            Initiates transition to POC:Wakeup
 * @param[in]        ctrl_idx - Index of flexray CC within the context of the flexray Driver
 * @return           SDRV_STATUS_OK or error code.
 */
extern status_t sdrv_flexray_send_wakeup(sdrv_flexray_t *handle);

/**
 * @function_name    sdrv_flexray_set_wakeup_channel
 *
 * @brief            Selects which channel sends WUPs
 * @param[in]        sdrv flexray handle
 * @param[in]        channel_idx Index of flexray channel within the context of the flexray CC
 *                   ctrl_idx
 * @return           SDRV_STATUS_OK or error code.
 */
extern status_t sdrv_flexray_set_wakeup_channel(sdrv_flexray_t *handle,
        sdrv_flexray_channel_type channel_idx);

/**
 * @function_name    sdrv_flexray_get_poc_status
 *
 * @brief            Query for the controller status
 * @param[in]        sdrv flexray handle
 * @param[out]       poc_status_ptr Address the output value is stored to
 * @return           SDRV_STATUS_OK or error code.
 */
extern status_t sdrv_flexray_get_poc_status(sdrv_flexray_t *handle,
        sdrv_flexray_poc_status_type *poc_status_ptr);

/**
 * @function_name    sdrv_flexray_set_absolute_timer
 *
 * @brief            Sets the absolute flexray timer
 * @param[in]        sdrv flexray handle
 * @param[in]        abs_timer_idx Index of absolute timer within the context of the flexray CC
 * @param[in]        cycle Absolute cycle the timer shall elapse in
 * @param[in]        offset Offset within cycle cycle in units of macrotick the timer shall
 *                   elapse at
 * @return           SDRV_STATUS_OK or error code.
 */
extern status_t sdrv_flexray_set_absolute_timer(sdrv_flexray_t *handle,
        uint8_t abs_timer_idx,
        uint8_t cycle, uint16_t offset);

/**
 * @function_name    sdrv_flexray_transmit_tx_lpdu
 *
 * @brief            Update selected message buffer with new data
 * @param[in]        sdrv flexray handle
 * @param[in]        lpdu_idx The index is used to uniquely identify a flexray frame
 * @param[in]        lsdu_ptr Pointer to a buffer where the assembled LSdu to be transmitted
 *                   within this LPdu is stored at
 * @param[in]        lsdu_length Determines the length of the data (in Bytes) to be transmitted
 * @param[out]       slot_assignment_ptr This reference points to the memory location where the
                    actual cycle, slot ID, and channel of the frame identified by
                    lpdu_idx shall be stored. A NULL indicates that the
                    information is not required by the caller.
 * @return           SDRV_STATUS_OK or error code.
 */
extern status_t sdrv_flexray_transmit_tx_lpdu(sdrv_flexray_t *handle,
        uint16_t lpdu_idx,
        const uint8_t *lsdu_ptr,
        uint8_t lsdu_length, sdrv_flexray_slot_assignment_type *slot_assignment_ptr);

/**
 * @function_name    sdrv_flexray_cancel_tx_lpdu
 *
 * @brief            Reconfigures physical resource
 * @param[in]        sdrv flexray handle
 * @param[in]        lpdu_idx This index is used to uniquely identify a flexray frame
 * @return           SDRV_STATUS_OK or error code.
 */
extern status_t sdrv_flexray_cancel_tx_lpdu(sdrv_flexray_t *handle,
        uint16_t lpdu_idx);

/**
 * @function_name    sdrv_flexray_receive_RxLPdu
 *
 * @brief            Receives data
 * @param[in]        sdrv flexray handle
 * @param[in]        lpdu_idx This index is used to uniquely identify a flexray frame
 * @param[out]       lsdu_ptr Pointer to a buffer where the LSdu
 * @param[out]       lpdu_status_ptr Pointer to the memory location where the status of the LPdu
 *                   shall be stored
 * @param[out]       lsdu_length_ptr Pointer to the memory location where the length of the LSdu
 *                   (in bytes) shall be stored
 * @param[out]       slot_assignment_ptr  This reference points to the memory location where the
                    actual cycle, slot ID, and channel of the frame identified by
                    lpdu_idx shall be stored. A NULL indicates that the
                    information is not required by the caller.
 * @return           SDRV_STATUS_OK or error code.
 */
extern status_t sdrv_flexray_receive_rx_lpdu(sdrv_flexray_t *handle,
        uint16_t lpdu_idx,
        uint8_t *lsdu_ptr,
        sdrv_flexray_rx_lpdu_status_type *lpdu_status_ptr, uint8_t *lsdu_length_ptr,
        sdrv_flexray_slot_assignment_type *slot_assignment_ptr);

/**
 * @function_name    sdrv_flexray_check_tx_lpdu_status
 *
 * @brief            Checks if data have been transmitted.
 * @param[in]        sdrv flexray handle
 * @param[in]        lpdu_idx This index is used to uniquely identify a flexray frame
 * @param[out]       tx_lpdu_status_ptr Pointer used to store the transmit status of the LSdu
 * @param[out]       slot_assignment_ptr Pointer used to store cycle, slot ID, and channel of the frame identified by lpdu_idx
 * @return           SDRV_STATUS_OK or error code.
 */
extern status_t sdrv_flexray_check_tx_lpdu_status(sdrv_flexray_t *handle,
        uint16_t lpdu_idx,
        sdrv_flexray_tx_lpdu_status_type *tx_lpdu_status_ptr,
        sdrv_flexray_slot_assignment_type *slot_assignment_ptr);

/**
 * @function_name    sdrv_flexray_get_global_time
 *
 * @brief            Function gets flexray cluster global time
 * @param[in]        sdrv flexray handle
 * @param[out]       cycle_ptr Address where the current flexray communication cycle value
 *                   shall be stored
 * @param[out]       macro_tick_ptr Address where the current macrotick value shall be stored
 * @return           SDRV_STATUS_OK or error code.
 */
extern status_t sdrv_flexray_get_global_time(sdrv_flexray_t *handle,
        uint8_t *cycle_ptr,
        uint16_t *macro_tick_ptr);

/**
 * @function_name    sdrv_flexray_get_network_vector
 *
 * @brief            Read network management vector
 * @param[in]        ctrl_idx - Index of flexray CC within the context of the flexray Driver
 * @param[out]       network_vector - Address where the NmVector of the last communication cycle
 *                   shall be stored
 * @return           SDRV_STATUS_OK or error code.
 */
extern status_t sdrv_flexray_get_network_vector(sdrv_flexray_t *handle,
        uint8_t *network_vector);

/**
 * @function_name    sdrv_flexray_get_number_startup_frames
 *
 * @brief            Gets the current number of startup frames seen on the cluster.
 * @param[in]        sdrv flexray handle
 * @param[in]        number_startup_frame_ptr Address where the number of startup frames seen
 *                                            within the last even or odd cycle pair shall be stored.
 * @return           SDRV_STATUS_OK or error code.
 */
extern status_t sdrv_flexray_get_number_startup_frames(sdrv_flexray_t *handle,
        uint8_t *number_startup_frame_ptr);

/**
 * @function_name    sdrv_flexray_get_channel_status
 *
 * @brief            Read channel status
 * @param[in]        ctrl_idx - Index of flexray CC within the context of the flexray Driver
 * @param[out]       channel_a_status_ptr - Address where the bitcoded channel A status information
 *                                          shall be stored.
 * @param[out]       channel_b_status_ptr - Address where the bitcoded channel B status information
 *                                          shall be stored.
 * @return           SDRV_STATUS_OK or error code.
 */
extern status_t sdrv_flexray_get_channel_status(sdrv_flexray_t *handle,
        uint16_t *channel_a_status_ptr, uint16_t *channel_b_status_ptr);

/**
 * @function_name    sdrv_flexray_get_clock_correction
 *
 * @brief            Gets clock correction values
 * @param[in]        ctrl_idx - Index of flexray CC within the context of the flexray Driver
 * @param[out]       rate_correction_ptr - Address where the current rate correction value shall
 *                                          be stored.
 * @param[out]       offset_correction_ptr - Address where the current offset correction value
 *                                            shall be stored.
 * @return           SDRV_STATUS_OK or error code.
 */
extern status_t sdrv_flexray_get_clock_correction(sdrv_flexray_t *handle,
        sint16 *rate_correction_ptr, sint32 *offset_correction_ptr);

/**
 * @function_name    sdrv_flexray_get_sync_frame_list
 *
 * @brief            Gets clock correction values
 * @param[in]        handle - sdrv flexray handler
 * @param[in]        list_size - Size of the arrays passed via parameters:
 *                                channel_a_even_list_ptr, channel_b_even_list_ptr,
 *                                channel_a_odd_list_ptr channel_b_odd_list_ptr.
 * @param[out]       channel_a_even_list_ptr - Address the list of syncframes on channel A within the
 *                                            even communication cycle is written to.
 * @param[out]       channel_b_even_list_ptr - Address the list of syncframes on channel B within the
 *                                            even communication cycle is written to.
 * @param[out]       channel_a_odd_list_ptr - Address the list of syncframes on channel A within the
 *                                            odd communication cycle is written to.
 * @param[out]       channel_b_odd_list_ptr - Address the list of syncframes on channel B within the
 *                                           odd communication cycle is written to.
 * @return           SDRV_STATUS_OK or error code.
 */
extern status_t sdrv_flexray_get_sync_frame_list(sdrv_flexray_t *handle,
        uint8_t list_size,
        uint16_t *channel_a_even_list_ptr,
        uint16_t *channel_b_even_list_ptr, uint16_t *channel_a_odd_list_ptr,
        uint16_t *channel_b_odd_list_ptr);

/**
 * @function_name    sdrv_flexray_get_wakeup_rx_status
 *
 * @brief            Gets clock correction values
 * @param[in]        handle - sdrv flexray handler
 *
 * @param[out]       wakeup_rx_status_ptr - Address where bitcoded wakeup reception status shall be
 *                                          stored.
 * @return           SDRV_STATUS_OK or error code.
 */
extern status_t sdrv_flexray_get_wakeup_rx_status(sdrv_flexray_t *handle,
        uint8_t *wakeup_rx_status_ptr);

/**
 * @function_name    sdrv_flexray_cancel_absolute_timer
 *
 * @brief            Stops an absolute timer
 * @param[in]        sdrv flexray handle
 * @param[in]        abs_timer_idx Index of absolute timer within the context of the flexray CC
 * @return           SDRV_STATUS_OK or error code.
 */
extern status_t sdrv_flexray_cancel_absolute_timer(sdrv_flexray_t *handle,
        uint8_t abs_timer_idx);

/**
 * @function_name    sdrv_flexray_read_cc_config
 *
 * @brief            Reconfigures physical resource
 * @param[in]        sdrv flexray handle
 * @param[in]        config_paramter_idx Index that identifies the configuration parameter
 *                                          to read. See macros FR_CIDX_<config_parameter_name>.
 * @param[out]       config_para_value_ptr Address the output value is stored to.
 * @return           SDRV_STATUS_OK or error code.
 */
extern status_t sdrv_flexray_read_cc_config(sdrv_flexray_t *handle,
        uint8_t config_paramter_idx, uint32_t *config_para_value_ptr);

/**
 * @function_name    sdrv_flexray_enable_absolute_timer_irq
 *
 * @brief            Enables absolute timer interrupts
 * @param[in]        ctrl_idx      Index of flexray CC within the context of the flexray Driver
 * @param[in]        abs_timer_idx  Index of absolute timer within the context of the flexray C
 * @return           SDRV_STATUS_OK or error code.
 */
extern status_t sdrv_flexray_enable_absolute_timer_irq(sdrv_flexray_t *handle,
        uint8_t abs_timer_idx);

/**
 * @function_name    sdrv_flexray_ack_absolute_timer_irq
 *
 * @brief            Clears absolute timer interrupt flag
 * @param[in]        sdrv flexray handle
 * @param[in]        abs_timer_idx Index of absolute timer within the context of the flexray CC
 * @return           SDRV_STATUS_OK or error code.
 */
extern status_t sdrv_flexray_ack_absolute_timer_irq(sdrv_flexray_t *handle,
        uint8_t abs_timer_idx);

/**
 * @function_name    sdrv_flexray_disable_absolute_timer_irq
 *
 * @brief            Disables absolute timer interrupt generation
 * @param[in]        ctrl_idx - Index of flexray CC within the context of the flexray Driver
 * @param[in]        abs_timer_idx - Index of absolute timer within the context of the flexray CC
 * @return           SDRV_STATUS_OK or error code.
 */
extern status_t sdrv_flexray_disable_absolute_timer_irq(sdrv_flexray_t *handle,
        uint8_t abs_timer_idx);

/**
 * @function_name    sdrv_flexray_get_absolute_timer_irq_status
 *
 * @brief            Checks if the absolute timer flag is set
 * @param[in]        ctrl_idx - Index of flexray CC within the context of the flexray Driver
 * @param[in]        abs_timer_idx - Index of absolute timer within the context of the flexray CC
 * @param[out]       irq_status_ptr - Address the output value is stored to
 * @return           SDRV_STATUS_OK or error code.
 */
extern status_t sdrv_flexray_get_absolute_timer_irq_status(
    sdrv_flexray_t *handle,
    uint8_t abs_timer_idx,
    boolean *irq_status_ptr);

/**
 * @function_name    sdrv_flexray_interrupt_enable
 *
 * @brief            open flexray interrupt
 * @param[in]        ctrl_idx - Index of FlexRay CC within the context of the FlexRay Driver
 * @param[in]        interrupt_source - interrupt source
 * @return           SDRV_STATUS_OK or error code.
 */
extern status_t sdrv_flexray_interrupt_enable(sdrv_flexray_t *handle,
        sdrv_flexray_global_interrupt_type interrupt_source);

/**
 * @function_name    sdrv_flexray_interrupt_disable
 *
 * @brief            disable flexray interrupt
 * @param[in]        ctrl_idx - Index of FlexRay CC within the context of the FlexRay Driver
 * @param[in]        interrupt_source - interrupt source
 * @return           SDRV_STATUS_OK or error code.
 */
extern status_t sdrv_flexray_interrupt_disable(sdrv_flexray_t *handle,
        sdrv_flexray_global_interrupt_type interrupt_source);

#ifdef __cplusplus
}
#endif

#endif /* SDRV_FLEXRAY_H_ */
