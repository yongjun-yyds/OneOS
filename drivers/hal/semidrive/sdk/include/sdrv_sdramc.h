/**
 * @file sdrv_sdramc.h
 * @brief SemiDrive SDRAM driver header file.
 *
 * @copyright Copyright (c) 2021  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef SDRV_SDRAMC_H_
#define SDRV_SDRAMC_H_

#include <types.h>
#include <debug.h>
#include <reg.h>
#include <sdrv_sdramc.h>
#include <sdrv_common.h>

#ifdef __cplusplus
extern "C" {
#endif

//#define ENABLE_SDRAMC_CTL_DATA_DEBUG
#ifdef ENABLE_SDRAMC_CTL_DATA_DEBUG
#define SDRAMC_CTL_DATA_DEBUG(format, ...) printf(format, ##__VA_ARGS__)
#else
#define SDRAMC_CTL_DATA_DEBUG(format, ...)
#endif

//#define WITH_DDR_DEBUG
#ifdef WITH_DDR_DEBUG
#define ddr_readl(reg) \
        readl(reg);         \
        ssdk_printf(SSDK_EMERG, "r(0x%x, r(0x%08x)\r\n", reg, readl(reg));
#define ddr_writel(val, reg)                                          \
        writel(val, reg);                                                  \
        ssdk_printf(SSDK_EMERG, "w(0x%x, 0x%08x), r(0x%08x)\r\n", reg, val, \
                    readl(reg));
#else
#define ddr_writel(val, reg) writel(val, reg)
#define ddr_readl(reg)       readl(reg)
#endif

#undef BIT
#define BIT(nr) ((uint32_t)1u << (nr))

#define SDRAMC_NS_2_TICKS(hz, ns) (((hz) / 10000u * (ns) + 100000u) / 100000u)

#define SDRAMC_INIT (0x0u)
#define SDRAMC_INIT_DONE BIT(31)
#define SDRAMC_INIT_SOFT_RESET BIT(8)
#define SDRAMC_INIT_CLK_OUT_EN BIT(4)
#define SDRAMC_INIT_SDR_SYNC_FLUSH BIT(3)
#define SDRAMC_INIT_DDR_ASYNC_FLUSH_B BIT(2)
#define SDRAMC_INIT_CFG_MRS BIT(1)
#define SDRAMC_INIT_START BIT(0)

#define SDRAMC_AXI_RDY_CNT (0x4u)
#define SDRAMC_AXI_RDY_CNT_R_CH_LSB (16u)
#define SDRAMC_AXI_RDY_CNT_B_CH_LSB (0u)

#define SDRAMC_AXI_FIFO_GAP (0x8u)
#define SDRAMC_AXI_FIFO_GAP_B_LSB (10u)
#define SDRAMC_AXI_FIFO_GAP_AR_LSB (8u)
#define SDRAMC_AXI_FIFO_GAP_RD_LSB (6u)
#define SDRAMC_AXI_FIFO_GAP_AW_LSB (4u)
#define SDRAMC_AXI_FIFO_GAP_WR_LSB (0u)

#define SDRAMC_BA (0x10u)
#define SDRAMC_BA_BASE_ADDR_LSB (0u)

#define RD_CLK (0x14u)
#define RD_CLK_DUMMY_LSB (5u)
#define RD_CLK_ASYNC_FIFO_RD_SEL_LSB (2u)
#define RD_CLK_SEL_LSB (0u)

#define SDRAM_DDR_MODE (0x18u)
#define SDRAM_DDR_MODE_CFG BIT(0)

#define SDRAM_BURST_MODE (0x1Cu)
#define SDRAM_BURST_MODE_TYPE BIT(3)
#define SDRAM_BURST_MODE_LSB (0u)

#define SDRAM_WIDTH_MODE (0x20u)
#define SDRAM_WIDTH_MODE_LSB (0u)

#define SDRAMC_FLOW_CTRL (0x24u)
#define SDRAMC_FLOW_CTRL_RR_SCH_STATUS BIT(25)
#define SDRAMC_FLOW_CTRL_RR_SCH_MASK BIT(24)
#define SDRAMC_FLOW_CTRL_RR_SCH_SET BIT(23)
#define SDRAMC_FLOW_CTRL_RETURN_STATUS BIT(22)
#define SDRAMC_FLOW_CTRL_SCH_STATUS BIT(21)
#define SDRAMC_FLOW_CTRL_RD_STATUS BIT(20)
#define SDRAMC_FLOW_CTRL_B_STATUS BIT(19)
#define SDRAMC_FLOW_CTRL_WR_STATUS BIT(18)
#define SDRAMC_FLOW_CTRL_AW_STATUS BIT(17)
#define SDRAMC_FLOW_CTRL_AR_STATUS BIT(16)
#define SDRAMC_FLOW_CTRL_RETURN_MASK BIT(14)
#define SDRAMC_FLOW_CTRL_SCH_MASK BIT(13)
#define SDRAMC_FLOW_CTRL_RD_MASK BIT(12)
#define SDRAMC_FLOW_CTRL_B_MASK BIT(11)
#define SDRAMC_FLOW_CTRL_WR_MASK BIT(10)
#define SDRAMC_FLOW_CTRL_AW_MASK BIT(9)
#define SDRAMC_FLOW_CTRL_AR_MASK BIT(8)
#define SDRAMC_FLOW_CTRL_RETURN_SET BIT(6)
#define SDRAMC_FLOW_CTRL_SCH_SET BIT(5)
#define SDRAMC_FLOW_CTRL_RD_SET BIT(4)
#define SDRAMC_FLOW_CTRL_B_SET BIT(3)
#define SDRAMC_FLOW_CTRL_WR_SET BIT(2)
#define SDRAMC_FLOW_CTRL_AW_SET BIT(1)
#define SDRAMC_FLOW_CTRL_AR_SET BIT(0)

#define SDRAMC_INTERRUPT (0x28u)

#define SDRAMC_INTERRUPT_EN (0x2Cu)

// #define SDRAMC_SHARE_BUFFER_GAP (0x28u)
// #define SDRAMC_SHARE_BUFFER_GAP_LSB (0u)

// #define SDRAMC_QUEUE_ALFUL_GAP (0x2Cu)
// #define SDRAMC_QUEUE_ALFUL_GAP_LSB (0u)

#define SDRAMC_SCH_WEIGHT (0x30u)
#define SDRAMC_SCH_WEIGHT_RW_W_LSB (24u)
#define SDRAMC_SCH_WEIGHT_PAGE_W_LSB (16u)
#define SDRAMC_SCH_WEIGHT_BANKIDLE_W_LSB (8u)
#define SDRAMC_SCH_WEIGHT_QOS_W_LSB (0u)

#define SDRAMC_AGING_WEIGHT (0x34u)
#define SDRAMC_AGING_WEIGHT_LSB (0u)

#define SDRAMC_CAS_MODE (0x38u)
#define SDRAMC_CAS_MODE_RD_NONE_DLY (8u)
#define SDRAMC_CAS_MODE_LSB (0u)

#define SDRAMC_OPERATING_MODE (0x3Cu)
#define SDRAMC_OPERATING_MODE_A13_A9_LSB (16u)
#define SDRAMC_OPERATING_MODE_LSB (0u)

#define SDRAMC_EXTEND_CONTROL (0x40u)
#define SDRAMC_EXTEND_CONTROL_A13_A2_LSB (16u)
#define SDRAMC_EXTEND_CONTROL_DRIVE_STRENGTH (1u)
#define SDRAMC_EXTEND_CONTROL_DLL_EN (0u)

#define SDRAMC_TIMING_REGS_PART1 (0x44u)
#define SDRAMC_TIMING_TRCD_LSB (24u)
#define SDRAMC_TIMING_TRFC_LSB (16u)
#define SDRAMC_TIMING_TRC_LSB (8u)
#define SDRAMC_TIMING_TRAS_MIN_LSB (0u)

#define SDRAMC_TIMING_REGS_PART2 (0x48u)
#define SDRAMC_TIMING_TWTR_LSB (24u)
#define SDRAMC_TIMING_TWR_LSB (16u)
#define SDRAMC_TIMING_TRRD_LSB (8u)
#define SDRAMC_TIMING_TRP_LSB (0u)

#define SDRAMC_TIMING_REGS_PART3 (0x4Cu)
#define SDRAMC_TIMING_CNT_200CYCLE_LSB (16u)
#define SDRAMC_TIMING_TREFI_LSB (0u)

#define SDRAMC_INIT_WAIT_CONT (0x50u)
#define SDRAMC_INIT_WAIT_CONT_200US (0u)

#define SDRAMC_ADDR_DIVED_MODE (0x54u)
#define SDRAMC_ADDR_DIVED_MODE_CS_START_LSB (24u)
#define SDRAMC_ADDR_DIVED_MODE_COLOW_START_LSB (16u)
#define SDRAMC_ADDR_DIVED_MODE_BANK_START_LSB (8u)
#define SDRAMC_ADDR_DIVED_MODE_ROW_MODE_LSB (5u)
#define SDRAMC_ADDR_DIVED_MODE_ROW_START_LSB (0u)

#define SDRAMC_CHIP_SELECT_MODE (0x58u)
#define SDRAMC_CHIP_SELECT_COLOW_MODE_LSB (2u)
#define SDRAMC_CHIP_SELECT_MODE_LSB (0u)

#define SDRAMC_RETURN_FC_GAP (0x5Cu)
#define SDRAMC_RETURN_FC_GAP_LSB (0u)

#define SDRAMC_BANK_MODE (0x60u)
#define SDRAMC_BANK_MODE_LSB (0u)

#define SDRAMC_TIMING_REGS_PART4 (0x64u)
#define SDRAMC_TIMING_TXSRD_LSB (16u)
#define SDRAMC_TIMING_TRAS_MAX_LSB (0u)

#define SDRAMC_MRD (0x68u)
#define SDRAMC_MRD_TMR_LSB (0u)

#define SDRAMC_CFG_CMD (0x6Cu)
#define SDRAMC_CFG_CMD_SELF_REFRESH BIT(0)

#define SDRAMC_CFG_PWR_DWN (0x70u)
#define SDRAMC_CFG_PWR_DWN_EN BIT(0)

#define WCMD_ERR_INT_SET (0x74u)
#define WCMD_ERR_INT_SET_CLR BIT(0)

#define SDRAMC_AXI_AWCNT (0x78u)
#define SDRAMC_AXI_AWCNT_LSB (0u)

#define SDRAMC_AXI_WCNT (0x7Cu)
#define SDRAMC_AXI_WCNT_LSB (0u)

#define SDRAMC_AXI_ARCNT (0x80u)
#define SDRAMC_AXI_ARCNT_LSB (0u)

#define SDRAMC_AXI_BRESP_CNT (0x84u)
#define SDRAMC_AXI_BRESP_CNT_LSB (0u)

#define SDRAMC_AXI_RCNT (0x88u)
#define SDRAMC_AXI_RCNT_LSB (0u)

#define SDRAMC_DRAM_RCNT (0x8Cu)
#define SDRAMC_DRAM_RCNT_LSB (0u)

#define SDRAMC_DRAM_WCNT (0x90u)
#define SDRAMC_DRAM_WCNT_LSB (0u)

#define SDRAMC_DRAM_DCNT (0x94u)
#define SDRAMC_DRAM_DCNT_LSB (0u)

#define SDRAMC_DLL0 (0x98u)
#define SDRAMC_DLL0_SLVUPDGATE BIT(31)
#define SDRAMC_DLL0_REFUPDINT1_LSB (28u)
#define SDRAMC_DLL0_REFUPDINT0_LSB (25u)
#define SDRAMC_DLL0_REF0RSTV_LSB (18u)
#define SDRAMC_DLL0_REFSEL1_LSB (16u)
#define SDRAMC_DLL0_OVRDVAL_LSB (9u)
#define SDRAMC_DLL0_OVRDEN BIT(8)
#define SDRAMC_DLL0_SLVTGRT_LSB (1u)
#define SDRAMC_DLL0_DLLEN BIT(0)

#define SDRAMC_DLL1 (0x9Cu)
#define SDRAMC_DLL1_SLVUPDGATE BIT(31)
#define SDRAMC_DLL1_REFUPDINT1_LSB (28u)
#define SDRAMC_DLL1_REFUPDINT0_LSB (25u)
#define SDRAMC_DLL1_REF0RSTV_LSB (18u)
#define SDRAMC_DLL1_REFSEL1_LSB (16u)
#define SDRAMC_DLL1_OVRDVAL_LSB (9u)
#define SDRAMC_DLL1_OVRDEN BIT(8)
#define SDRAMC_DLL1_SLVTGRT_LSB (1u)
#define SDRAMC_DLL1_DLLEN BIT(0)

#define SDRAMC_DLL2 (0xA0u)
#define SDRAMC_DLL2_SLVUPDGATE BIT(31)
#define SDRAMC_DLL2_REFUPDINT1_LSB (28u)
#define SDRAMC_DLL2_REFUPDINT0_LSB (25u)
#define SDRAMC_DLL2_REF0RSTV_LSB (18u)
#define SDRAMC_DLL2_REFSEL1_LSB (16u)
#define SDRAMC_DLL2_OVRDVAL_LSB (9u)
#define SDRAMC_DLL2_OVRDEN BIT(8)
#define SDRAMC_DLL2_SLVTGRT_LSB (1u)
#define SDRAMC_DLL2_DLLEN BIT(0)

#define SDRAMC_DLL3 (0xA4u)
#define SDRAMC_DLL3_SLVUPDGATE BIT(31)
#define SDRAMC_DLL3_REFUPDINT1_LSB (28u)
#define SDRAMC_DLL3_REFUPDINT0_LSB (25u)
#define SDRAMC_DLL3_REF0RSTV_LSB (18u)
#define SDRAMC_DLL3_REFSEL1_LSB (16u)
#define SDRAMC_DLL3_OVRDVAL_LSB (9u)
#define SDRAMC_DLL3_OVRDEN BIT(8)
#define SDRAMC_DLL3_SLVTGRT_LSB (1u)
#define SDRAMC_DLL3_DLLEN BIT(0)

#define SDRAMC_DLL_STATUS0 (0xA8u)
#define SDRAMC_DLL_STATUS0_SLVSEL_LSB (9u)
#define SDRAMC_DLL_STATUS0_SLVLOCK BIT(8)
#define SDRAMC_DLL_STATUS0_REFSEL_LSB (1u)
#define SDRAMC_DLL_STATUS0_REFLOCK BIT(0)

#define SDRAMC_DLL_STATUS1 (0xACu)
#define SDRAMC_DLL_STATUS1_SLVSEL_LSB (9u)
#define SDRAMC_DLL_STATUS1_SLVLOCK BIT(8)
#define SDRAMC_DLL_STATUS1_REFSEL_LSB (1u)
#define SDRAMC_DLL_STATUS1_REFLOCK BIT(0)

#define SDRAMC_DLL_STATUS2 (0xB0u)
#define SDRAMC_DLL_STATUS2_SLVSEL_LSB (9u)
#define SDRAMC_DLL_STATUS2_SLVLOCK BIT(8)
#define SDRAMC_DLL_STATUS2_REFSEL_LSB (1u)
#define SDRAMC_DLL_STATUS2_REFLOCK BIT(0)

#define SDRAMC_DLL_STATUS3 (0xB4u)
#define SDRAMC_DLL_STATUS3_SLVSEL_LSB (9u)
#define SDRAMC_DLL_STATUS3_SLVLOCK BIT(8)
#define SDRAMC_DLL_STATUS3_REFSEL_LSB (1u)
#define SDRAMC_DLL_STATUS3_REFLOCK BIT(0)

#define SDRAMC_DQS_QE_SET (0xB8u)
#define SDRAMC_DQS_QE_SET_VAL BIT(1)
#define SDRAMC_DQS_QE_SET_EN BIT(0)

#define SDRAMC_DDR_DQS_SHIFT (0xC0u)
#define SDRAMC_DDR_DQS_SHIFT_GATE_FORCE_TIE1 BIT(31)
#define SDRAMC_DDR_DQS_SHIFT_NUM3NEG_LSB (28u)
#define SDRAMC_DDR_DQS_SHIFT_NUM2NEG_LSB (24u)
#define SDRAMC_DDR_DQS_SHIFT_NUM1NEG_LSB (20u)
#define SDRAMC_DDR_DQS_SHIFT_NUM0NEG_LSB (16u)
#define SDRAMC_DDR_DQS_SHIFT_NUM3_LSB (12u)
#define SDRAMC_DDR_DQS_SHIFT_NUM2_LSB (8u)
#define SDRAMC_DDR_DQS_SHIFT_NUM1_LSB (4u)
#define SDRAMC_DDR_DQS_SHIFT_NUM0_LSB (0u)

#define SDRAMC_SDR_DQS_SHIFT (0xC4u)
#define SDRAMC_SDR_DQS_SHIFT_NUM_LSB (0u)


/**
 * @brief sdramc status error code.
 */
enum sdrv_sdramc_error {
    SDRV_STATUS_SDRAMC_DLL_LOCK_ERR = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_SDRAMC, 1), /* sdramc dll lock error */
    SDRV_STATUS_SDRAMC_DLL_WAYS_ERR = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_SDRAMC, 2), /* sdramc dll ways error */
    SDRV_STATUS_SDRAMC_SET_DDR_CLK_ERR = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_SDRAMC, 3), /* sdramc set clk error */
};

/** @brief sdramc commom configuration */
typedef struct sdram_comm_config {
    uint32_t mrd_cycle;    /**< mode register set command cycle time */
    uint32_t t_wtr_cycle;  /**< internal write to read command delay */
    uint32_t t_rcd_ns;     /**< active to read or write delay */
    uint32_t t_rfc_ns;     /**< auto refresh to active/auto */
    uint32_t t_rc_ns;      /**< active to active/auto refresh command period */
    uint32_t t_ras_ns;     /**< active to precharge command */
    uint32_t t_wr_ns;      /**< write recovery time */
    uint32_t t_rrd_ns;     /**< active bank a to active bank b command */
    uint32_t t_rp_ns;      /**< precharge command period */
} sdram_comm_config_t;

/** @brief sdramc private configuration */
typedef struct sdram_config {
    const char *name;
    bool ddr_mode;           /**< support ddr or sdr */
    uint32_t row_num;        /**< device row num */
    uint32_t column_num;     /**< device column num */
    uint32_t bank_num;       /**< device bank num */
    uint32_t cs_num;         /**< device cs num according to the hardware design */
    uint32_t dq_width;       /**< device width  according to the hardware design */
    uint32_t clock;          /**< device support clock rate*/
    uint32_t cas_latency_x2; /**< read command and the availability of the first piece of output data */
    uint32_t burst_length;   /**< read or write burst length */
    sdram_comm_config_t  *comm_config; /**< sdr/ddr commom config */
} sdram_config_t;

/** @brief sdramc row column cs bank configuration */
typedef struct sd_para {
    uint32_t column_start; /**< sdramc column start */
    uint32_t column_bits;  /**< sdramc bit width */
    uint32_t cs_start;     /**< sdramc cs start */
    uint32_t cs_bits;      /**< sdramc cs width */
    uint32_t bank_start;   /**< sdramc bank start */
    uint32_t bank_bits;    /**< sdramc bank width */
    uint32_t row_start;    /**< sdramc row start */
    uint32_t row_bits;     /**< sdramc row width */
} sd_para_s;

/** @brief gate value record */
typedef struct gate_record {
    uint32_t  gate_val;   /**< set gate val depending on the frequency */
    uint32_t  gate_index; /**< gate index num */
} gate_record_s;

extern struct sd_para cs_p;

/**
 * @brief get sdram para
 * @return[out] sdram para
 */
struct sd_para *sdrv_get_sd_para(void);

/**
 * @brief save sdram para
 * @param[in] cs sdram info
 */
void sdrv_set_sd_para(struct sd_para *cs);

/**
 * @brief sdramc init main function
 *
 * 1.config sdr/ddr register according to different mode
 * 2.training and config dll value for sdr/ddr mode
 *
 * @param [in] base sdramc base register address
 * @param [in] conf sdramc config parameters
 * @return int32_t
 * @retval 0: success
 * @retval other: failed
 */
int32_t sdrv_sdramc_init(addr_t base, sdram_config_t *conf);

#ifdef __cplusplus
}
#endif
#endif /* SDRV_SDRAMC_H_ */
