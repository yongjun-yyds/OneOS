/**
 * @file g2dlite_core.h
 * @brief SemiDrive G2DLite core header file.
 *
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef G2DLITE_CORE_H__
#define G2DLITE_CORE_H__
#include <dispss/disp_data_type.h>
#include <reg.h>
#include <string.h>

enum {
    G2DLITE_FUN_BLEND = 0x1,
    G2DLITE_FUN_ROTATION = 0x2,
};

typedef enum {
    G2DLITE_CPU_WRITE,
    G2DLITE_CMD_WRITE,
} g2dlite_write_mode;

typedef enum {
    G2DLITE_MLC_GP,
    G2DLITE_MLC_SP,
    G2DLITE_MLC_MAX
} g2dlite_mlc_layer;

typedef enum {
    MLC_PATH_BG,
    MLC_PATH_GP,
    MLC_PATH_SP,
    MLC_PATH_MAX
} g2dlite_mlc_path;

typedef struct {
    unsigned long base_addr;
    unsigned int irq;
    unsigned int layer_n;
} g2dlite_profile_t;

static inline unsigned int reg_value(unsigned int val, unsigned int src,
                                     unsigned int shift, unsigned int mask)
{
    return (src & ~mask) | ((val << shift) & mask);
}

static inline unsigned int g2dlite_read(unsigned long base, unsigned int reg)
{
    return readl(base + reg);
}

static inline void g2dlite_write(unsigned long base, unsigned int reg,
                                 unsigned int value)
{
    writel(value, base + reg);
}
static inline void g2dlite_cwrite(struct g2dlite_device *dev, int cmd_id,
                                  unsigned int reg, unsigned int value)
{
    if (dev->write_mode == G2DLITE_CMD_WRITE) {
        cmdfile_info *cmd = &dev->cmd_info[cmd_id];
        cmd->arg[cmd->len++] = reg;
        cmd->arg[cmd->len++] = value;
        if (!cmd->dirty)
            cmd->dirty = 1;
    } else {
        writel(value, dev->reg_base + reg);
    }
}

#endif /* G2DLITE_CORE_H__ */
