/**
 * @file g2dlite_log.h
 * @brief SemiDrive G2DLite log header file.
 *
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef G2DLITE_LOG_H__
#define G2DLITE_LOG_H__
#include <debug.h>

#define G2DLITE_LOG_ENABLE 1

#define G2DLITE_LOG_LEVEL_EMERG SSDK_EMERG /*System is unusable*/
#define G2DLITE_LOG_LEVEL_ALERT                                                \
    SSDK_ALERT                           /*Action must be taken             \  \
                                            immediately*/
#define G2DLITE_LOG_LEVEL_CRIT SSDK_CRIT /*Critical conditions*/
#define G2DLITE_LOG_LEVEL_ERR SSDK_ERR   /*Error conditions*/
#define G2DLITE_LOG_LEVEL_WARN SSDK_WARNING /*Warning conditions*/
#define G2DLITE_LOG_LEVEL_NOTICE                                               \
    SSDK_NOTICE                          /*Normal, but significant, conditions*/
#define G2DLITE_LOG_LEVEL_INFO SSDK_INFO /*Informational message*/
#define G2DLITE_LOG_LEVEL_DEBUG SSDK_DEBUG /*Debug-level message*/

/*log level default*/
#define G2DLITE_LOG_LEVEL G2DLITE_LOG_LEVEL_ERR

#define G2DLITE_LOG_EMERG(string, args...)                                     \
    if (G2DLITE_LOG_ENABLE &&                                                  \
        (G2DLITE_LOG_LEVEL >= G2DLITE_LOG_LEVEL_EMERG)) {                      \
        ssdk_printf(G2DLITE_LOG_LEVEL_EMERG,                                   \
                    "[G2DLITE]EMERG|%s|" string "\r\n", __func__, ##args);     \
    }

#define G2DLITE_LOG_ALERT(string, args...)                                     \
    if (G2DLITE_LOG_ENABLE &&                                                  \
        (G2DLITE_LOG_LEVEL >= G2DLITE_LOG_LEVEL_ALERT)) {                      \
        ssdk_printf(G2DLITE_LOG_LEVEL_ALERT,                                   \
                    "[G2DLITE]ALERT|%s|" string "\r\n", __func__, ##args);     \
    }

#define G2DLITE_LOG_CRIT(string, args...)                                      \
    if (G2DLITE_LOG_ENABLE && (G2DLITE_LOG_LEVEL >= G2DLITE_LOG_LEVEL_CRIT)) { \
        ssdk_printf(G2DLITE_LOG_LEVEL_CRIT, "[G2DLITE]CRIT|%s|" string "\r\n", \
                    __func__, ##args);                                         \
    }

#define G2DLITE_LOG_ERR(string, args...)                                       \
    if (G2DLITE_LOG_ENABLE && (G2DLITE_LOG_LEVEL >= G2DLITE_LOG_LEVEL_ERR)) {  \
        ssdk_printf(G2DLITE_LOG_LEVEL_ERR, "[G2DLITE]ERR|%s|" string "\r\n",   \
                    __func__, ##args);                                         \
    }

#define G2DLITE_LOG_WARN(string, args...)                                      \
    if (G2DLITE_LOG_ENABLE && (G2DLITE_LOG_LEVEL >= G2DLITE_LOG_LEVEL_WARN)) { \
        ssdk_printf(G2DLITE_LOG_LEVEL_WARN, "[G2DLITE]WARN|%s|" string "\r\n", \
                    __func__, ##args);                                         \
    }

#define G2DLITE_LOG_NOTICE(string, args...)                                    \
    if (G2DLITE_LOG_ENABLE &&                                                  \
        (G2DLITE_LOG_LEVEL >= G2DLITE_LOG_LEVEL_NOTICE)) {                     \
        ssdk_printf(G2DLITE_LOG_LEVEL_NOTICE,                                  \
                    "[G2DLITE]NOTICE|%s|" string "\r\n", __func__, ##args);    \
    }

#define G2DLITE_LOG_INFO(string, args...)                                      \
    if (G2DLITE_LOG_ENABLE && (G2DLITE_LOG_LEVEL >= G2DLITE_LOG_LEVEL_INFO)) { \
        ssdk_printf(G2DLITE_LOG_LEVEL_INFO, "[G2DLITE]INFO|%s|" string "\r\n", \
                    __func__, ##args);                                         \
    }

#define G2DLITE_LOG_DEBUG(string, args...)                                     \
    if (G2DLITE_LOG_ENABLE &&                                                  \
        (G2DLITE_LOG_LEVEL >= G2DLITE_LOG_LEVEL_DEBUG)) {                      \
        ssdk_printf(G2DLITE_LOG_LEVEL_DEBUG,                                   \
                    "[G2DLITE]DEBUG|%s|" string "\r\n", __func__, ##args);     \
    }

#define G2DLITE_LOG_FUNC()                                                     \
    if (G2DLITE_LOG_ENABLE && (G2DLITE_LOG_LEVEL >= G2DLITE_LOG_LEVEL_INFO)) { \
        ssdk_printf(G2DLITE_LOG_LEVEL_INFO, "[G2DLITE]FUNC|%s\r\n", __func__); \
    }

#endif /*_G2DLITE_DRV_LOG_H__ */
