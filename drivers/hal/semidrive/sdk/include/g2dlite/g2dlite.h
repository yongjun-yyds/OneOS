/**
 * @file g2dlite.h
 * @brief SemiDrive G2DLite header file.
 *
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef G2DLITE_H__
#define G2DLITE_H__

#include <sdrv_common.h>

typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;
typedef unsigned long long u64;

#define G2DLITE_LAYER_MAX_NUM 2
/*LEN:"1"means one pair of 32-bit register addr + 32-bit register value*/
#define G2DLITE_CMDFILE_MAX_LEN 512
#define G2DLITE_CMDFILE_MAX_NUM 10

#define G2DLITE_CMDFILE_MAX_MEM                                                \
    (G2DLITE_CMDFILE_MAX_LEN * G2DLITE_CMDFILE_MAX_NUM * 2)

/**
 * @brief G2dlite status return code
 */
enum {
    SDRV_G2DLITE_STATUS_OK = SDRV_STATUS_OK,
    SDRV_G2DLITE_STATUS_FAIL = SDRV_STATUS_FAIL,
    SDRV_G2DLITE_STATUS_INVALID_PARAM =
        SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_G2DLITE, 1),
    SDRV_G2DLITE_STATUS_TIMEOUT =
        SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_G2DLITE, 2),

};

/**
 * @brief G2dlite Work status
 */
enum {
    G2DLITE_IDLE = 0, /**< g2dlite idle*/
    G2DLITE_WORKING,  /**< g2dlite in working*/
    G2DLITE_DONE,     /**< g2dlite done*/
};

enum { BLEND_PIXEL_NONE = 0, BLEND_PIXEL_PREMULTI, BLEND_PIXEL_COVERAGE };

typedef enum {
    ROTATION_TYPE_NONE = 0x0,
    ROTATION_TYPE_ROT_90 = 0x1,
    ROTATION_TYPE_HFLIP = 0x2,
    ROTATION_TYPE_VFLIP = 0x4,
    ROTATION_TYPE_ROT_180 = ROTATION_TYPE_VFLIP | ROTATION_TYPE_HFLIP,
    ROTATION_TYPE_ROT_270 =
        ROTATION_TYPE_ROT_90 | ROTATION_TYPE_VFLIP | ROTATION_TYPE_HFLIP,
    ROTATION_TYPE_VF_90 = ROTATION_TYPE_VFLIP | ROTATION_TYPE_ROT_90,
    ROTATION_TYPE_HF_90 = ROTATION_TYPE_HFLIP | ROTATION_TYPE_ROT_90,
} rotation_type;

typedef enum { PD_NONE = 0, PD_SRC = 0x1, PD_DST = 0x2 } PD_LAYER_TYPE;

/**@brief sdrv g2dlite Porter-DUFF mode type*/
typedef enum {
    CLEAR = 0,
    SRC,
    DST,
    SRC_OVER,
    DST_OVER,
    SRC_IN,
    DST_IN,
    SRC_OUT,
    DST_OUT,
    SRC_ATOP,
    DST_ATOP,
    XOR,
    DARKEN,
    LIGHTEN,
    MULTIPLY,
    SCREEN,
    ADD,
    OVERLAY,
    SRC_SUB,
    DES_SUB
} pd_mode_t;

struct rect_t {
    u32 x;
    u32 y;
    u32 w;
    u32 h;
};

struct ckey_range_t {
    u32 r_dn;
    u32 r_up;
    u32 g_dn;
    u32 g_up;
    u32 b_dn;
    u32 b_up;
};

/**@brief g2dlite ckey struct*/
struct ckey_t {
    u8 en;                     /**< color key enable identifier*/
    u32 alpha;                 /**< color key alpha value, range: 0~0xFF*/
    struct ckey_range_t range; /**< color key range settings*/
};

/**@brief g2dlite RLE struct*/
struct rle_t {
    u32 en;             /**< RLE enable identifier*/
    u32 rle_y_len;      /**< RLE RGB/Y data length*/
    u32 rle_y_checksum; /**< RLE channel checksum value*/
    u32 rle_data_size;  /**< 0: 1byte, 1: 2byte, 2: 3byte, 3: 4byte*/
};

/**@brief g2dlite clut struct*/
struct clut_t {
    u32 en;   /**< CLUT enable identifier*/
    u32 addr; /**< color table src data address*/
};

/**@brief g2dlite Porter-DUFF struct*/
struct pd_t {
    u32 en;     /**< the Porder-DUFF funtion enables identification, and the
                   function is processed only if this value is true*/
    u32 zorder; /**< layer stacking order, layer with higher values above*/
    pd_mode_t
        mode; /**< Porter-DUFF mode type, the value must in enum pd_mode_t*/
    u8 alpha_need; /**< if need output alpha channel*/
};

struct fcopy_t {
    u32 addr;
    u32 width;
    u32 height;
    u32 stride;
};

/**@brief sdrv g2dlite backgound config struct*/
struct g2dlite_bg_cfg {
    u32 en;     /**< the backgound layer enables identification and the layer
                   information is processed only if this value is true*/
    u32 color;  /**< color value to be filled in*/
    u8 g_alpha; /**< layer global alpha value,range is 0~0xFF*/
    u8 zorder;  /**< layer stacking order(layers with higher values above)*/

    u32 aaddr; /**< alpha buffer address, valid only enable alpha buffer data
                  pattern*/
    u8 bpa; /**< bits per alpha, valid only enable alpha buffer data pattern*/
    u32 astride; /**< alpha buffer pitch, valid only enable alpha buffer data*/
    PD_LAYER_TYPE pd_type; /**< porter-duff layer type, only can set value in
                              enum PD_LAYER_TYPE*/
};

/**@brief sdrv g2dlite input config struct*/
struct g2dlite_input_cfg {
    u8 layer; /**< layer identifier used to select the hardware layer(0: layer,
                 1: layer1)*/
    u8 layer_en; /**< the layer enables identifiction, and the input layer
                    information is processed only if this value is true*/
    u32 fmt;     /**< input image format*/
    u32 zorder;  /**< layer stacking order(layer with higher values above)*/
    struct rect_t src;  /**< the starting point coordinates of the processed
                           image in the original image*/
    u64 addr[4];        /**< input image memory address, correspinding to YUVA
                           four-component address respectively*/
    u32 src_stride[4];  /**< image component pitch value*/
    struct rect_t dst;  /**< the dst point coordinates of the processed image in
                           the canvas*/
    u32 blend;          /**< image component mode selection*/
    u8 alpha;           /**< layer global alpha value, range:0~0xff*/
    struct ckey_t ckey; /**< color kay related configuration parameters*/
    struct rle_t rle;   /**< RLE related configuration parameters*/
    struct clut_t
        clut; /**< Color LookUp Table related configuration parameters*/
    PD_LAYER_TYPE pd_type; /**< Porter-duff layer type*/
};

/**@brief sdrv g2dlite output config struct*/
struct g2dlite_output_cfg {
    u32 width;     /**< the width of the output image*/
    u32 height;    /**< the height of the output image*/
    u32 fmt;       /**< output image format*/
    u64 o_x;       /**< output image start x position in output buffer*/
    u64 o_y;       /**< output image start y position in output buffer*/
    u64 addr[4];   /**< output image memory address, correspinding to YUVA
                      four-component address respectively*/
    u32 stride[4]; /**< output image component pitch value*/
    u32 rotation;  /**< output image rotation mode*/
};

/**@brief sdrv g2dlite input struct*/
struct g2dlite_input {
    u8 layer_num;        /**< input layer number*/
    struct pd_t pd_info; /**< g2dlite Porter-DUFF configuration*/
    struct g2dlite_bg_cfg
        bg_layer; /**< g2dlite background layer configuration*/
    struct g2dlite_input_cfg
        layer[G2DLITE_LAYER_MAX_NUM]; /**< g2dlite input layers configuration*/
    struct g2dlite_output_cfg output; /**< g2dlite output configuration*/
};

struct g2dlite_config {
    u32 base;
    u32 irq;
};

enum {
    G2DLITE_TYPE_GPIPE,
    G2DLITE_TYPE_SPIPE,
    G2DLITE_TYPE_APIPE,
    G2DLITE_TYPE_NUM
};

/**@brief cmdfile_info - command mode cmdfile info struct*/
typedef struct {
    unsigned char dirty;
    unsigned int len; /**< command file length(real data number-1), 1 means one
                         pair of 32bit register addr + 32bit register value*/
    unsigned int
        *arg; /**< buffer for store 32bit register and 32bit register value*/
} cmdfile_info;

struct g2dlite_device {
    unsigned long reg_base;
    cmdfile_info cmd_info[G2DLITE_CMDFILE_MAX_NUM];
    unsigned char write_mode;
    unsigned int irq;
    int (*irq_handler)(u32 irq, void *arg);
    const struct g2dlite_ops *ops;
    unsigned int inited;
    int status;
    void (*done_callback)(void);
};

struct g2dlite_ops {
    int (*init)(struct g2dlite_device *dev);
    int (*reset)(struct g2dlite_device *dev);
    int (*clut_index_set)(struct g2dlite_device *dev, char *clut_src);
    int (*update)(struct g2dlite_device *dev, struct g2dlite_input *ins);
    int (*rotation)(struct g2dlite_device *dev, struct g2dlite_input *ins);
    int (*fill_rect)(struct g2dlite_device *dev, struct g2dlite_bg_cfg *bgcfg,
                     struct g2dlite_output_cfg *output);
    int (*fastcopy)(struct g2dlite_device *dev, struct fcopy_t *in,
                    struct fcopy_t *out);
    int (*enable)(struct g2dlite_device *dev);
    int (*irq_handler)(u32 irq, void *arg);
    int (*check_status)(struct g2dlite_device *dev);
};

extern const struct g2dlite_ops ops;
status_t sdrv_g2dlite_probe(struct g2dlite_device *dev,
                            const struct g2dlite_config *g2dlite_cfg);
status_t sdrv_g2dlite_update(struct g2dlite_device *dev,
                             struct g2dlite_input *input);
status_t sdrv_g2dlite_rotaion(struct g2dlite_device *dev,
                              struct g2dlite_input *input);
status_t sdrv_g2dlite_fill_rect(struct g2dlite_device *dev,
                                struct g2dlite_bg_cfg *bgcfg,
                                struct g2dlite_output_cfg *output);
status_t sdrv_g2dlite_fastcopy(struct g2dlite_device *dev, struct fcopy_t *in,
                               struct fcopy_t *out);
status_t sdrv_g2dlite_clut_setting(struct g2dlite_device *dev,
                                   char *clut_table);
#endif /* G2DLITE_H__ */
