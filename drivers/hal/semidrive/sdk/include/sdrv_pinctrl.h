/**
 * @file sdrv_pinctrl.h
 * @brief SemiDrive pinctrl device driver header.
 *
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef SDRV_PINCTRL_H_
#define SDRV_PINCTRL_H_

#include <types.h>
#include <sdrv_common.h>

/*
 * Use pinctrl.h for different parts, located in
 * SSDK_PATH/devices/E3xxx/include
 *
 * note: use macro defined in pinctrl.h when call PINCTRL/GPIO api.
 */
#include <pinctrl.h>

struct sdrv_gpio;

/**
 * @brief PINCTRL status error code.
 */
enum sdrv_pinctrl_error
{
    SDRV_PINCTRL_BAD_PIN = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_PINCTRL, 1),   /* PINCTRL Pin Index Wrong. */
    SDRV_PINCTRL_CTRL_NULL = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_PINCTRL, 2), /* PINCTRL Controller is NULL. */
};

/**
 * @brief Definition for INPUT SELECT REGISTER config.
 *
 */
typedef struct sdrv_input_select_t {
    uint32_t  pin    : 16;
    uint32_t  mux    : 4;
    uint32_t  offset : 16;
    uint32_t  value  : 4;
    paddr_t   base;
} sdrv_input_select_t;

/**
 * @brief Definition for Pinctrl instance.
 *
 */
typedef struct sdrv_pinctrl {
    paddr_t base;
    uint32_t pad_offset;
    uint32_t mux_offset;
    uint32_t pin_start;
    uint32_t pin_num;
    uint32_t disp_mux_base;

    uint32_t input_select_size;
    const sdrv_input_select_t *input_select;
} sdrv_pinctrl_t;

/**
 * @brief Definition for pin mux function, each pin has different function,
 * detail see datasheet.
 */
typedef enum pin_mux {
    PIN_MUX_ALT0  = 0U,     /**< MUX0 */
    PIN_MUX_ALT1  = 1U,     /**< MUX1 */
    PIN_MUX_ALT2  = 2U,     /**< MUX2 */
    PIN_MUX_ALT3  = 3U,     /**< MUX3 */
    PIN_MUX_ALT4  = 4U,     /**< MUX4 */
    PIN_MUX_ALT5  = 5U,     /**< MUX5 */
    PIN_MUX_ALT6  = 6U,     /**< MUX6 */
    PIN_MUX_ALT7  = 7U,     /**< MUX7 */
    PIN_MUX_ALT8  = 8U,     /**< MUX8 */
    PIN_MUX_ALT9  = 9U,     /**< MUX9 */
} pin_mux_e;

/**
 * @brief Definition for pin output type.
 */
typedef enum pin_open_drain {
    PIN_OPEN_DRAIN  = 0U,   /**< open drain output */
    PIN_PUSH_PULL   = 2U,   /**< push pull output */
} pin_open_drain_e;

/**
 * @brief Definition for pin internal pull type.
 */
typedef enum pin_pull_config {
    PIN_NOPULL     = 0U,    /**< no pull */
    PIN_PULL_DOWN  = 1U,    /**< pull down */
    PIN_PULL_UP    = 2U,    /**< pull up */
} pin_pull_config_e;

/**
 * @brief Definition for pin current drive strength.
 */
typedef enum pin_drive_strength {
    PIN_DS_2MA  = 0U,       /**< drive strength 2mA */
    PIN_DS_8MA  = 1U,       /**< drive strength 8mA */
    PIN_DS_4MA  = 2U,       /**< drive strength 4mA */
    PIN_DS_12MA = 3U,       /**< drive strength 12mA */
} pin_drive_strength_e;

/**
 * @brief Definition for pin slew rate type.
 */
typedef enum pin_slew_rate {
    PIN_SR_FAST  = 0U,      /**< slew rate fast */
    PIN_SR_SLOW  = 1U,      /**< slew rate slow */
} pin_slew_rate_e;

/**
 * @brief Definition for pin input select.
 */
typedef enum pin_input_select {
    PIN_IS_CMOS          = 0U,  /**< input select cmos */
    PIN_IS_CMOS_SCHMITT  = 1U,  /**< input select cmos schmitt */
} pin_input_select_e;

/**
 * @brief Definition for pin data direction, only valid for pin mux as gpio.
 */
typedef enum pin_data_direction {
    PIN_INPUT_DIRECTION  = 0U,  /**< gpio direction input */
    PIN_OUTPUT_DIRECTION = 1U,  /**< gpio direction output */
} pin_data_direction_e;

/**
 * @brief Definition for pin interrupt type, only valid for pin mux as gpio.
 */
typedef enum pin_interrupt_config {
    PIN_INTERRUPT_DISABLED     = 0U,    /**< interrupt disable */
    PIN_INTERRUPT_HIGH_LEVEL   = 1U,    /**< high level interrupt active */
    PIN_INTERRUPT_LOW_LEVEL    = 2U,    /**< low level interrupt active */
    PIN_INTERRUPT_RISING_EDGE  = 3U,    /**< rising edge interrupt active */
    PIN_INTERRUPT_FALLING_EDGE = 4U,    /**< falling edge interrupt active */
    PIN_INTERRUPT_BOTH_EDGE    = 5U,    /**< both edge interrupt active */
} pin_interrupt_config_e;

/**
 * @brief Definition for pin initial output level, only valid for pin mux as gpio output.
 */
typedef enum pin_level_type {
    PIN_LEVEL_LOW  = 0U,    /**< gpio initial output low */
    PIN_LEVEL_HIGH = 1U,    /**< gpio initial output high */
} pin_level_type_e;

/**
 * @brief Definition for pin force input config.
 *
 * PIN_FORCE_INPUT_NORMAL means external level has no effect to pin origin ouput.
 * PIN_FORCE_INPUT_ENABLE means if pin output high, external circle pull down pin, pad will read low.
 * PIN_FORCE_INPUT_HIGH means internal force pin tie to high level.
 * PIN_FORCE_INPUT_LOW means internal force pin tie to low level.
 */
typedef enum pin_force_input {
    PIN_FORCE_INPUT_NORMAL  = 0U,   /**< pin force input normal */
    PIN_FORCE_INPUT_ENABLE  = 1U,   /**< pin force input enable */
    PIN_FORCE_INPUT_HIGH    = 2U,   /**< pin force input high */
    PIN_FORCE_INPUT_LOW     = 3U,   /**< pin force input low */
} pin_force_input_e;

/**
 * @brief Definition for pin mode select config.
 *
 * PIN_MODE_DIGITAL use for digital pin.
 * PIN_MODE_SELECT_NORMAL use for analog pin and reference voltage is 3.3V.
 * PIN_MODE_DOWN_CONVERSION use for analog pin and reference voltage is 5V.
 */
typedef enum pin_mode_select {
    PIN_MODE_DIGITAL            = 0U,   /**< digital pin */
    PIN_MODE_SELECT_NORMAL      = 1U,   /**< pass pad signal as is, only for Analog-digital combo IO */
    PIN_MODE_DOWN_CONVERSION    = 2U    /**< down coversion of pad signal before passing to core, only for Analog-digital combo IO */
} pin_mode_select_e;

/**
 * @brief Definiction for pin initialize config.
 */
typedef struct pin_settings_config {
    uint32_t                  pin_index;        /**< pin index */
    pin_mux_e                 mux;              /**< mux config */
    pin_open_drain_e          open_drain;       /**< open drain config */
    pin_pull_config_e         pull_config;      /**< pull config */
    pin_drive_strength_e      drive_strength;   /**< drive strength config */
    pin_slew_rate_e           slew_rate;        /**< slew rate config */
    pin_input_select_e        input_select;     /**< input select config */
    pin_data_direction_e      data_direction;   /**< gpio direction config */
    pin_interrupt_config_e    interrupt_config; /**< gpio interrupt config */
    pin_level_type_e          initial_value;    /**< gpio initial value */
    pin_force_input_e         force_input;      /**< pin force input config */
    pin_mode_select_e         mode_select;      /**< mode select config */
} pin_settings_config_t;


/**
 * @brief Initialize pins as pre-defined.
 *
 * @param [in] count pin numbers to be initilized.
 * @param [in] configs pre-defined pin config array list.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pinctrl_init(uint32_t count, const pin_settings_config_t *configs);

/**
 * @brief Config mux function for specific pin.
 *
 * This function select a mux function for specific pin.
 *
 * @param [in] pin_index defined pin macro in pinctrl.h.
 * @param [in] mux mux function
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pinctrl_set_mux(uint32_t pin_index, pin_mux_e mux);

/**
 * @brief Config output type for specific pin.
 *
 * This function config pin output use open drain or push pull.
 *
 * @param [in] pin_index defined pin macro in pinctrl.h.
 * @param [in] open_drain output type defined in pin_open_drain_e.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pinctrl_set_opendrain(uint32_t pin_index, pin_open_drain_e open_drain);

/**
 * @brief Config pull type for specific pin.
 *
 * This function config pin internal pull config.
 *
 * @param [in] pin_index defined pin macro in pinctrl.h.
 * @param [in] pull_config pull type defined in pin_pull_config_e.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pinctrl_set_pull(uint32_t pin_index, pin_pull_config_e pull_config);

/**
 * @brief Config drive strength for specific pin.
 *
 * This function config pin current drive strength.
 *
 * @param [in] pin_index defined pin macro in pinctrl.h.
 * @param [in] drive_strength current drive strength defined in pin_drive_strength_e.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pinctrl_set_drive_strength(uint32_t pin_index, pin_drive_strength_e drive_strength);

/**
 * @brief Config slew rate for specific pin.
 *
 * This function config pin slew rate.
 *
 * @param [in] pin_index defined pin macro in pinctrl.h.
 * @param [in] slew_rate slew rate type defined in pin_slew_rate_e.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pinctrl_set_slew_rate(uint32_t pin_index, pin_slew_rate_e slew_rate);

/**
 * @brief Config input select for specific pin.
 *
 * This function config pin input select cmos or cmos schmitt.
 *
 * @param [in] pin_index defined pin macro in pinctrl.h.
 * @param [in] input_select input select defined in pin_input_select_e.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pinctrl_set_input_select(uint32_t pin_index, pin_input_select_e input_select);

/**
 * @brief Config force input for specific pin.
 *
 * This function config force input type for a specific pin. For Multiply slave device in bus, cs pin should
 * config to force input enable, such as I2C clock etc.
 *
 * @param [in] pin_index defined pin macro in pinctrl.h.
 * @param [in] force_input force input defined in pin_force_input_e.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pinctrl_set_force_input(uint32_t pin_index, pin_force_input_e force_input);

/**
 * @brief Config mode select for specific pin.
 *
 * This function config mode select register for one pin.
 * normal/down coversion only valid for Analog-digital combo IO (GPIOA/B/C)
 * normal: passes pad signal as is
 * down coversion: enable down coversion of pad signal before passing to core
 *
 * @param [in] pin_index defined pin macro in pinctrl.h.
 * @param [in] mode_select mode select defined in pin_mode_select_e.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pinctrl_set_mode_select(uint32_t pin_index, pin_mode_select_e mode_select);

/**
 * @brief Clear display mux config.
 *
 * This function used to clear display mux config. If GPIO_L0 - GPIO_L9 not use for LVDS,
 * you must call this function.
 *
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pinctrl_clear_dispmux_config(void);

/**
 * @brief Config pin to high resistance.
 *
 * This function use to config pin as high resistance.
 *
 * @param [in] pin_index defined pin macro in pinctrl.h.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pinctrl_set_high_resistance(uint32_t pin_index);

/**
 * @brief Initialize one pin as pre-defined.
 *
 * @param [in] pinctrl pin belongs PINCTRL contoller instance.
 * @param [in] gpio pin belongs GPIO contoller instance.
 * @param [in] pin_cfg pre-defined pin config.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pinctrl_ctrl_init_pin(sdrv_pinctrl_t *pinctrl, struct sdrv_gpio *gpio, const pin_settings_config_t *pin_cfg);

/**
 * @brief Config mux function for specific pin.
 *
 * This function select a mux function for specific pin.
 *
 * @param [in] pinctrl PINCTRL contoller instance.
 * @param [in] pin_index defined pin macro in pinctrl.h.
 * @param [in] mux mux function
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pinctrl_ctrl_set_mux(sdrv_pinctrl_t *pinctrl, uint32_t pin_index, pin_mux_e mux);

/**
 * @brief Config output type for specific pin.
 *
 * This function config pin output use open drain or push pull.
 *
 * @param [in] pinctrl PINCTRL contoller instance.
 * @param [in] pin_index defined pin macro in pinctrl.h.
 * @param [in] open_drain output type defined in pin_open_drain_e.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pinctrl_ctrl_set_opendrain(sdrv_pinctrl_t *pinctrl, uint32_t pin_index, pin_open_drain_e open_drain);

/**
 * @brief Config pull type for specific pin.
 *
 * This function config pin internal pull config.
 *
 * @param [in] pinctrl PINCTRL contoller instance.
 * @param [in] pin_index defined pin macro in pinctrl.h.
 * @param [in] pull_config pull type defined in pin_pull_config_e.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pinctrl_ctrl_set_pull(sdrv_pinctrl_t *pinctrl, uint32_t pin_index, pin_pull_config_e pull_config);

/**
 * @brief Config drive strength for specific pin.
 *
 * This function config pin current drive strength.
 *
 * @param [in] pinctrl PINCTRL contoller instance.
 * @param [in] pin_index defined pin macro in pinctrl.h.
 * @param [in] drive_strength current drive strength defined in pin_drive_strength_e.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pinctrl_ctrl_set_drive_strength(sdrv_pinctrl_t *pinctrl, uint32_t pin_index, pin_drive_strength_e drive_strength);

/**
 * @brief Config slew rate for specific pin.
 *
 * This function config pin slew rate.
 *
 * @param [in] pinctrl PINCTRL contoller instance.
 * @param [in] pin_index defined pin macro in pinctrl.h.
 * @param [in] slew_rate slew rate type defined in pin_slew_rate_e.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pinctrl_ctrl_set_slew_rate(sdrv_pinctrl_t *pinctrl, uint32_t pin_index, pin_slew_rate_e slew_rate);

/**
 * @brief Config input select for specific pin.
 *
 * This function config pin input select cmos or cmos schmitt.
 *
 * @param [in] pinctrl PINCTRL contoller instance.
 * @param [in] pin_index defined pin macro in pinctrl.h.
 * @param [in] input_select input select defined in pin_input_select_e.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pinctrl_ctrl_set_input_select(sdrv_pinctrl_t *pinctrl, uint32_t pin_index, pin_input_select_e input_select);

/**
 * @brief Config force input for specific pin.
 *
 * This function config force input type for a specific pin. For Multiply slave device in bus, cs pin should
 * config to force input enable, such as I2C clock etc.
 *
 * @param [in] pinctrl PINCTRL contoller instance.
 * @param [in] pin_index defined pin macro in pinctrl.h.
 * @param [in] force_input force input defined in pin_force_input_e.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pinctrl_ctrl_set_force_input(sdrv_pinctrl_t *pinctrl, uint32_t pin_index, pin_force_input_e force_input);

/**
 * @brief Config mode select for specific pin.
 *
 * This function config mode select register for one pin.
 * normal/down coversion only valid for Analog-digital combo IO (GPIOA/B/C)
 * normal: passes pad signal as is
 * down coversion: enable down coversion of pad signal before passing to core
 *
 * @param [in] pinctrl PINCTRL contoller instance.
 * @param [in] pin_index defined pin macro in pinctrl.h.
 * @param [in] mode_select mode select defined in pin_mode_select_e.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pinctrl_ctrl_set_mode_select(sdrv_pinctrl_t *pinctrl, uint32_t pin_index, pin_mode_select_e mode_select);

/**
 * @brief Clear display mux config.
 *
 * This function used to clear display mux config. If GPIO_L0 - GPIO_L9 not use for LVDS,
 * you must call this function.
 *
 * @param [in] pinctrl PINCTRL contoller instance.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pinctrl_ctrl_clear_dispmux_config(sdrv_pinctrl_t *pinctrl);

/**
 * @brief Config pin to high resistance.
 *
 * This function use to config pin as high resistance.
 *
 * @param [in] pinctrl PINCTRL contoller instance.
 * @param [in] gpio GPIO contoller instance.
 * @param [in] pin_index defined pin macro in pinctrl.h.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_pinctrl_ctrl_set_high_resistance(sdrv_pinctrl_t *pinctrl, struct sdrv_gpio *gpio, uint32_t pin_index);

#endif /* SDRV_PINCTRL_H_ */
