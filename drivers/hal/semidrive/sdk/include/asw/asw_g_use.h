/**
 * @file asw_g_use.h
 * @brief SemiDrive asw header file.
 *
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef __ASW_G_USE_H__
#define __ASW_G_USE_H__

/*application id*/
enum ASW_APP_ID {
    ASW_APP_NONE = 0,
    ASW_APP_EMIRROR,
};

enum {
    WORK_MODE_M_TO_M = 0,
    WORK_MODE_CSI_HS,
    WORK_MODE_DC_HS,
    WORK_MODE_CSI_DC_HS
};

struct asw_app_dc {
    int id;

    int work_mode; /**< M_TO_M, CSI_HS, DC_HS, CSI_DC_HS*/
    int mode;      /**< 0:bilinear, 1: cubic spline*/
    int buf_size_num_lines;
    int t_delta;
    int out_buf[2];
    int buf_index;
};

struct asw_app_asw {
    int id;

    int work_mode;
    int out_buf[2]; /**< only for csi-asw handshake*/
    int out_index;
    int ext_lut_x[2];
    int ext_lut_y[2];
    int swap;
};

#endif
