/**
 * @file asw.h
 * @brief SemiDrive asw header file.
 *
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef __ASW_H__
#define __ASW_H__
#include <types.h>

#include "asw_g_use.h"
#include "asw_lut_hvk.h"

/*fmt*/
/*DES FMT support: */
/*   FMT_MONOTONIC_8BIT   */
/*   FMT_RGB565           */
/*   FMT_RGBA5551         */
/*   FMT_ARGB1555         */
/*   FMT_RGB_YUV888       */
/*   FMT_YUV422_UYVY      */
/*   FMT_YUV422_VYUY      */
/*   FMT_RGBA8888         */
/*   FMT_ARGB8888         */
/*   FMT_YUV422_YUYV      */
/*   FMT_YUV422_YVYU      */

/*SRC FMT support: */
/*   FMT_MONOTONIC_8BIT   */
/*   FMT_RGB_YUV888_PLANAR*/
/*   FMT_YUV422_PLANAR    */
/*   FMT_YUV420_PLANAR    */
/*   FMT_RGB565           */
/*   FMT_RGBA5551         */
/*   FMT_ARGB1555         */
/*   FMT_RGB_YUV888       */
/*   FMT_YUV422_SEMI_U0V0 */
/*   FMT_YUV422_SEMI_V0U0 */
/*   FMT_RGBA8888         */
/*   FMT_ARGB8888         */

enum {
    FMT_MONOTONIC_8BIT = 0,
    FMT_RGB_YUV888_PLANAR = 1,
    FMT_YUV422_PLANAR = 2,
    FMT_YUV420_PLANAR = 3,
    FMT_RGB565 = 4,
    FMT_RGBA5551 = 5,
    FMT_ARGB1555 = 6,
    FMT_RGB_YUV888 = 8,
    FMT_YUV422_SEMI_U0V0 = 9,
    FMT_YUV422_SEMI_V0U0 = 10,
    FMT_RGBA8888 = 12,
    FMT_ARGB8888 = 13,
    FMT_YUV422_YUYV = 14,
    FMT_YUV422_YVYU = 15
};

#define FMT_YUV422_UYVY 9
#define FMT_YUV422_VYUY 10

enum { LB_LAYOUT_A = 0, LB_LAYOUT_B };

enum { LUT_MODE_BILINEAR = 0, LUT_MODE_CUBIC_SPLINE };

enum { EXT_LUT_FORMAT_16BIT = 0, EXT_LUT_FORMAT_32BIT };

enum { CUBIC_MODE_INT_ASM = 0, CUBIC_MODE_EXT_ASM };

/*cache entry type (hsize x vsize)*/
enum {
    C_ENTRY_TYPE_8X4 = 0,
    C_ENTRY_TYPE_8X8,
    C_ENTRY_TYPE_16X4,
    C_ENTRY_TYPE_16X8,
    C_ENTRY_TYPE_32X4
};

enum {
    BUF_SIZE_8LINES = 0,
    BUF_SIZE_16LINES,
    BUF_SIZE_32LINES,
    BUF_SIZE_64LINES
};

enum {
    FLIP_TYPE_NONE = 0,
    FLIP_TYPE_HFLIP = 1,
    FLIP_TYPE_VFLIP = 2,
    FLIP_TYPE_HVFLIP = 3
};

enum TRIAG_YTPE {
    TRIAG_LEFT_TOP = 0,
    TRIAG_LEFT_BOTTOM,
    TRIAG_TYPE_RIGHT_TOP,
    TRIAG_TYPE_RIGHT_BOTTOM
};

struct asw_config {
    paddr_t base;
    int irq;
};

/**@brief This structure include ASW output image configuration information*/
struct dst_info {
    int dst_fmt;      /**< Output image format*/
    int dst_hsize;    /**< Output image width (unit: pixel)*/
    int dst_vsize;    /**< Output image height (unit:line)*/
    int dst_ba;       /**< Output image memory base address*/
    int dst_stride;   /**< Output image pitch*/
    int dst_bg_color; /**< Output image background color*/
    int dst_flip;     /**< Output image flip mode select*/
};

/**@brief This structure include ASW input image information*/
struct src_info {
    int src_fmt;       /**< Input image format*/
    int src_hsize;     /**< Input image width (unit: pixel)*/
    int src_vsize;     /**< Input image height (unit: line)*/
    int src_ry_ba;     /**< Input image first plane memory base address*/
    int src_gu_ba;     /**< Input image second plane memory base address*/
    int src_bv_ba;     /**< Input image third plane memory base address*/
    int src_ry_stride; /**< Input image first plane pitch*/
    int src_gu_stride; /**< Input image second plane pitch*/
    int src_bv_stride; /**< Input image third plane pitch*/
    int src_flip;      /**< Input image flip mode select*/
};

/**@brief This structure include lookup table size and step information*/
struct lut_info {
    int asw_lut_hsize; /**< Lookup table horizontal size*/
    int asw_lut_vsize; /**< Lookup table vertical size*/
    int asw_lut_hstep; /**< Lookup table horizontal step size*/
    int asw_lut_vstep; /**< Lookup table vertical step size*/
};

struct cubic_info {
    int lut_src_ba;
    int alpha_src_ba;
};

/**@brief This structure include bilinear spline lookup table information*/
struct ext_info {
    int ext_mlut_rx_ba; /**< First plane x position lookup table external memory
                           base address*/
    int ext_mlut_ry_ba; /**< First plane y position lookup table external memory
                           base address*/
    int ext_mlut_gx_ba; /**< Second plane x position lookup table external
                           memory base address*/
    int ext_mlut_gy_ba; /**< Second plane y position lookup table external
                           memory base address*/
    int ext_mlut_bx_ba; /**< Third plane x position lookup table external memory
                           base address*/
    int ext_mlut_by_ba; /**< Third plane y position lookup table external
                           memory base address*/
    int ext_mlut_rx_stride; /**< First plane x position lookup table external
                               memory pitch*/
    int ext_mlut_ry_stride; /**< First plane y position lookup table external
                               memory pitch*/
    int ext_mlut_gx_stride; /**< Second plane x position lookup table external
                               memory pitch*/
    int ext_mlut_gy_stride; /**< Second plane y position lookup table external
                               memory pitch*/
    int ext_mlut_bx_stride; /**< Third plane x position lookup table external
                               memory pitch*/
    int ext_mlut_by_stride; /**< Third plane y position lookup table external
                               memory pitch*/
};

/**@brief adb info struct*/
struct adb_info {
    int dirty;  /**< 0:using system default offset&&slop;1:using this
                   offset&&slop*/
    int offset; /**< range[0-0x3f]*/
    int slope;  /**< range[0-0xf]*/
};

struct circular_buf {
    int enable;
    int range;
    int low_limit_addr;
    int high_limit_addr;
};

/**@brief This structure includes all the configuration information of ASW for
 * controlling the ASW’s work*/
struct asw_input {
    int work_mode; /**< ASW work mode select*/
    int hdsk_mode; /**< ASW output buffer size (only "Display coupled mode" and
                      "CSI and display tightly coupled ultra low latency mode"
                      using)*/
    int lut_mode;  /**< LUT mode select*/
    int ext_lut_fmt; /**< Bilinear external LUT format select*/

    struct dst_info dst; /**< ASW output image information*/
    struct src_info src; /**< ASW input image information*/
    struct lut_info lut; /**< LUT information*/

    struct adb_info adb; /**< adb information*/

    int cache_entry_type; /**< cache entry type select (hsize x vsize)*/
    int cache_way_n;      /**< cache way number (unit: cache entry vsize)*/

    struct cubic_info
        cubic; /**< spline LUT, alpha source (only cubic spline using)*/

    struct ext_info ext; /**< bilinear spline external LUT base address*/

    int csi_start_ln; /**< When using "CSI coupled mode" or "CSI and display
                         tightly coupled ultra low latency mode", this line is
                         written out from CSI to RAM to start ASW*/
    struct circular_buf cbuf;
};

struct asw_ba_info {
    int asw_pos_calc_pfile_ba;
    int asw_pos_lut_ba;
    int asw_m_v_ba;
    int asw_alpha_ba;
    int asw_y_stepy_ba;
    int asw_lut_h_ba;
    int asw_m_h_ba;
    int asw_frb_ba;
    int asw_cache_ry_ba;
    int asw_cache_gu_ba;
    int asw_cache_bv_ba;
    int asw_cache_a_ba;
};

/**@brief This structure include rotation informations*/
struct asw_rot {
    int hsize;    /**< Image width (unit: pixel)*/
    int vsize;    /**< Image height (unit: line)*/
    double angle; /**< Rotation angle*/

    int dst_fmt;    /**< Output image format*/
    int dst_ba;     /**< Output image memory base address*/
    int dst_stride; /**< Output image pitch*/

    int src_fmt;       /**< Input image format*/
    int src_ba[4];     /**< Input image memory base address*/
    int src_stride[4]; /**< Input image pitch*/

    int t_buf;
    int t_stride;

    int bg_color; /**< Output image background color*/
};

/**@brief This struct include asw draw triangle input infomations*/
struct asw_ratria {
    int type;  /**< triangle type, value in enum TRIAG_TYPE*/
    int fmt;   /**< buffer format, only support FMT_RGB565, FMT_RGBA5551,
                  FMT_ARGB1555, FMT_RGB_YUV888, FMT_RGBA8888, FMT_ARGB8888*/
    int color; /**< B:bit[31:24], G:bit[23:16], R:bit[15:8], A:bit[7:0]*/
    int hsize;
    int vsize;
    int pos_x;  /**< the position x in buffer*/
    int pos_y;  /**< the position y in buffer*/
    int addr;   /**< buffer base address*/
    int stride; /**< buffer pitch*/
};

struct asw_operations;

/**@brief This structure describe the ASW information and function opeartaions*/
struct asw_dev {
    paddr_t base;               /**< ASW register base address*/
    int irq;                    /**< ASW irq number*/
    struct asw_ba_info bas;     /**< Parts base address in ASW local buffer*/
    struct asw_operations *ops; /**< ASW function operations*/
    struct asw_input *input;    /**< Configure information of ASW*/
    struct asw_app_asw app;     /**< Application information*/
    int asw_done;               /**< flags for asw done*/
};

struct asw_operations {
    void (*soft_reset)(struct asw_dev *dev);
    void (*cubic_cfg)(struct asw_dev *dev);
    void (*cubic_cfg_with_hvkt)(struct asw_dev *dev, struct hvkt_param *hvkt);
    void (*cubic_rotation)(struct asw_dev *dev, double rotate_angle,
                           struct hvkt_param *hvkt);
    void (*bilinear_cfg)(struct asw_dev *dev);
    void (*bilinear_cfg_with_hvkt)(struct asw_dev *dev,
                                   struct hvkt_param *hvkt);
    void (*bilinear_rotation)(struct asw_dev *dev, double rotate_angle,
                              struct hvkt_param *hvkt);
    int (*irq_handler)(unsigned int irq, void *arg);
};

extern struct asw_dev aswdev;

extern int is_asw_done;

void sdrv_asw_probe(struct asw_dev *dev, const struct asw_config *asw_cfg);

/**
 * @brief sdrv asw cubic.
 *
 * This function performs warp task with cubic spline lookup table.
 *
 * @param [in] input All the configuration information of ASW for controlling
 * the ASW’s work.
 */
void sdrv_asw_cubic(struct asw_input *input);

/**
 * @brief sdrv asw cubic with hvkt.
 *
 * This function performs warp task with cubic spline h/v keystone infomation.
 *
 * @param [in] input All the configuration information of ASW for controlling
 * the ASW’s work.
 * @param [in] hvkt h/v keystone information.
 *
 */
void sdrv_asw_cubic_with_hvkt(struct asw_input *input, struct hvkt_param *hvkt);

/**
 * @brief sdrv asw bilinear.
 *
 * This function performs warp task with bilinear spline lookup tables.

 * @param [in] input All the configuration information of ASW for controlling.
 */
void sdrv_asw_bilinear(struct asw_input *input);

/**
 * @brief sdrv asw bilinear noblock.
 *
 * This function performs warp task with bilinear spline lookup tables.
 *
 * @param [in] input All the configuration information of ASW for controlling.
 **/
void sdrv_asw_bilinear_noblock(struct asw_input *input);

/**
 * @brief sdrv asw bilinear with hvkt.
 *
 * This function performs warp task with bilinear spline h/v keystone
 * infomation.
 *
 * @param [in] input All the configuration information of ASW for controlling
 * the ASW’s work.
 * @param [in] hvkt: h/v keystone information.
 */
void sdrv_asw_bilinear_with_hvkt(struct asw_input *input,
                                 struct hvkt_param *hvkt);
/**
 * @brief sdrv asw bilinear ftob32.
 *
 * Covert data from float type to int type.
 *
 * @param [in] faddr source data array point with float data type.
 * @param [in] b32 dst data array point with int data type.
 * @param [in] len convert data length.
 */
void sdrv_asw_bilinear_ftob32(const float *faddr, int *b32, int len);
/**
 * @brief sdrv asw bilinear ftob16.
 *
 * Covert data from float type to short type.
 *
 * @param [in] faddr source data array point with float data type.
 * @param [in] b16 dst data array point with short data type.
 * @param [in] len convert data length.
 */
void sdrv_asw_bilinear_ftob16(const float *faddr, short *b16, int len);
/**
 * @brief sdrv asw bilinear rotation.
 *
 * Image rotation with bilinear algorithm.
 *
 * @param [in] rot rotation infomation.
 */
void sdrv_asw_bilinear_rotation(struct asw_rot *rot);
/**
 * @brief sdrv asw ra triangle.
 *
 * This function can draw right-angled triangle.
 *
 * @param [in] info right-angled triangle infomation.
 */
void sdrv_asw_ra_triangle(struct asw_ratria *info);
/**
 * @brief sdrv asw fill rect.
 *
 * This function can fill rectangle with single color.
 *
 * @param [in] fmt buffer format.
 * @param [in] color B:bit[31:24], G:bit[23:16], R:bit[15:8], A:bit[7:0].
 * @param [in] hsize the active size of width.
 * @param [in] vsize the active size of height.
 * @param [in] pos_x the position of x in buffer.
 * @param [in] pos_y the position of y in buffer.
 * @param [in] addr buffer base address.
 * @param [in] stride buffer pitch.
 */
void sdrv_asw_fill_rect(int fmt, int color, int hsize, int vsize, int pos_x,
                        int pos_y, int addr, int stride);

#endif //__GAMA_H__
