/**
 * @file asw_log.h
 * @brief SemiDrive asw log header file.
 *
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef ASW_LOG_H__
#define ASW_LOG_H__
#include <debug.h>

#define ASW_LOG_ENABLE 1

#define ASW_LOG_LEVEL_EMERG SSDK_EMERG  /*System is unusable*/
#define ASW_LOG_LEVEL_ALERT SSDK_ALERT  /*Action must be taken immediately*/
#define ASW_LOG_LEVEL_CRIT SSDK_CRIT    /*Critical conditions*/
#define ASW_LOG_LEVEL_ERR SSDK_ERR      /*Error conditions*/
#define ASW_LOG_LEVEL_WARN SSDK_WARNING /*Warning conditions*/
#define ASW_LOG_LEVEL_NOTICE                                                   \
    SSDK_NOTICE                        /*Normal, but significant,              \
                                          conditions*/
#define ASW_LOG_LEVEL_INFO SSDK_INFO   /*Informational message*/
#define ASW_LOG_LEVEL_DEBUG SSDK_DEBUG /*Debug-level message*/

/*log level default*/
#define ASW_LOG_LEVEL ASW_LOG_LEVEL_ERR

#define ASW_LOG_EMERG(string, args...)                                         \
    if (ASW_LOG_ENABLE && (ASW_LOG_LEVEL >= ASW_LOG_LEVEL_EMERG)) {            \
        ssdk_printf(ASW_LOG_LEVEL_EMERG, "[ASW]EMERG|%s| " string "\r\n",      \
                    __func__, ##args);                                         \
    }

#define ASW_LOG_ALERT(string, args...)                                         \
    if (ASW_LOG_ENABLE && (ASW_LOG_LEVEL >= ASW_LOG_LEVEL_ALERT)) {            \
        ssdk_printf(ASW_LOG_LEVEL_ALERT, "[ASW]ALERT|%s| " string "\r\n",      \
                    __func__, ##args);                                         \
    }

#define ASW_LOG_CRIT(string, args...)                                          \
    if (ASW_LOG_ENABLE && (ASW_LOG_LEVEL >= ASW_LOG_LEVEL_CRIT)) {             \
        ssdk_printf(ASW_LOG_LEVEL_CRIT, "[ASW]CRIT|%s| " string "\r\n",        \
                    __func__, ##args);                                         \
    }

#define ASW_LOG_ERR(string, args...)                                           \
    if (ASW_LOG_ENABLE && (ASW_LOG_LEVEL >= ASW_LOG_LEVEL_ERR)) {              \
        ssdk_printf(ASW_LOG_LEVEL_ERR, "[ASW]ERR|%s| " string "\r\n",          \
                    __func__, ##args);                                         \
    }

#define ASW_LOG_WARN(string, args...)                                          \
    if (ASW_LOG_ENABLE && (ASW_LOG_LEVEL >= ASW_LOG_LEVEL_WARN)) {             \
        ssdk_printf(ASW_LOG_LEVEL_WARN, "[ASW]WARN|%s| " string "\r\n",        \
                    __func__, ##args);                                         \
    }

#define ASW_LOG_NOTICE(string, args...)                                        \
    if (ASW_LOG_ENABLE && (ASW_LOG_LEVEL >= ASW_LOG_LEVEL_NOTICE)) {           \
        ssdk_printf(ASW_LOG_LEVEL_NOTICE, "[ASW]NOTICE|%s| " string "\r\n",    \
                    __func__, ##args);                                         \
    }

#define ASW_LOG_INFO(string, args...)                                          \
    if (ASW_LOG_ENABLE && (ASW_LOG_LEVEL >= ASW_LOG_LEVEL_INFO)) {             \
        ssdk_printf(ASW_LOG_LEVEL_INFO, "[ASW]INFO|%s| " string "\r\n",        \
                    __func__, ##args);                                         \
    }

#define ASW_LOG_DEBUG(string, args...)                                         \
    if (ASW_LOG_ENABLE && (ASW_LOG_LEVEL >= ASW_LOG_LEVEL_DEBUG)) {            \
        ssdk_printf(ASW_LOG_LEVEL_DEBUG, "[ASW]DEBUG|%s| " string "\r\n",      \
                    __func__, ##args);                                         \
    }

#define ASW_LOG_FUNC()                                                         \
    if (ASW_LOG_ENABLE && (ASW_LOG_LEVEL >= ASW_LOG_LEVEL_INFO)) {             \
        ssdk_printf(ASW_LOG_LEVEL_INFO, "[ASW]FUNC|%s\r\n", __func__);         \
    }

#endif //_ASW_DRV_LOG_H__
