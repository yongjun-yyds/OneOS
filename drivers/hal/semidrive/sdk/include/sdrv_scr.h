/**
 * @file sdrv_scr.h
 * @brief SemiDrive Status and Controller Register driver header file.
 *
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */
#ifndef _SDRV_SCR_H_
#define _SDRV_SCR_H_

#include <sdrv_common.h>
#include <types.h>

/* SCR signal types. */
#define TYPE_RW 0u
#define TYPE_RO 1u
#define TYPE_L16 2u
#define TYPE_L31 3u
#define TYPE_R16W16 4u

/**
 * @brief SCR signal structure.
 *
 * @note Do NOT define SCR signals by yourself. Use signals defined in
 * "scr_hw.h", which is automatically generated.
 */
typedef struct scr_signal {
    /**< Signal type. */
    uint16_t type;
    /**< Start bit from register offset of current SCR type. */
    uint16_t start_bit;
    /**< Signal width in bits. */
    uint16_t width;
} scr_signal_t;

/**
 * @brief SCR device structure.
 */
typedef struct sdrv_scr {
    paddr_t base; /**< scr module base */
} sdrv_scr_t;

/**
 * @brief SCR driver error codes.
 */
enum {
    /* The signal is locked, so it cannot be updated. */
    SDRV_SCR_LOCKED = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_SCR, 0),
    /* The signal is locked, or not L16/L31, so it cannot be locked. */
    SDRV_SCR_CANNOT_LOCK = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_SCR, 1),
    /* The signal isn't writeable. */
    SDRV_SCR_CANNOT_WRITE = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_SCR, 2),
};

/**
 * @brief Get value of signal.
 *
 * @param scr SCR controller.
 * @param signal Signal defined in "scr_hw.h".
 * @return Signal value.
 */
uint32_t scr_get(const sdrv_scr_t *scr, const scr_signal_t *signal);

/**
 * @brief Set value of signal.
 *
 * @param scr SCR controller.
 * @param signal Signal defined in "scr_hw.h".
 * @param value  Signal value.
 * @return SDRV_STATUS_OK if successfully set. SDRV_SCR_LOCKED if signal is
 *  locked, or SDRV_SCR_CANNOT_WRITE if signal is read only.
 */
status_t scr_set(const sdrv_scr_t *scr, const scr_signal_t *signal, uint32_t value);

/**
 * @brief Lock the given signal.
 *
 * @param scr SCR controller.
 * @param signal Signal defined in "scr_hw.h", which should be of L16/L31 type.
 * @return SDRV_STATUS_OK if successfully locked signal.
 */
status_t scr_lock(const sdrv_scr_t *scr, const scr_signal_t *signal);

/**
 * @brief Check the lock status of signal
 *
 * @param scr SCR controller.
 * @param signal Signal defined in "scr_hw.h", which should be of L16/L31 type.
 * @return True if signal is locked.
 */
bool scr_is_locked(const sdrv_scr_t *scr, const scr_signal_t *signal);

#endif /*_SDRV_SCR_H_*/