/**
 * @file cam-def.h
 *
 * @brief Semidrive camera header file
 *
 * @copyright Copyright (c) 2021 Semidrive Semiconductor.
 * All rights reserved.
 *
 * for rtos and baremetal both
 * Revision History:
 * -----------------
 */
#ifndef CAM_DEF_H
#define CAM_DEF_H

#include "armv7-r/atomic.h"
#include <debug.h>


#ifndef atomic_t
typedef volatile int atomic_t;
#endif

static inline int atomic_add(atomic_t *ptr, int val)
{
    return arch_atomic_add((int *)ptr, val);
}

#endif // CAM_DEF_H
