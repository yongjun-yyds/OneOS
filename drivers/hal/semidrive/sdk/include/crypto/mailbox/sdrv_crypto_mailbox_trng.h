/**
 * @file sdrv_crypto_mailbox_trng.h
 * @brief crypto mailbox trng cmd interface
 *
 * @copyright Copyright (c) 2021  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef SDRV_CRYPTO_MAILBOX_TRNG_H
#define SDRV_CRYPTO_MAILBOX_TRNG_H

#include "sdrv_crypto_mailbox_common.h"
#include "sdrv_crypto_mailbox_soc_otp_key.h"

/**
 * @brief CRYPTO cmd of trng.
 */
typedef struct {
    uint8_t cmd_id;
    uint8_t rev1[3];
    uint8_t random_ptr[4];
    uint8_t random_len[4];
} cmd_tng_t;

/**
 * @brief get trng
 *
 * This function get trng
 *
 * @param[out] rand_output rand buff
 * @param[in] rand_bytes rand len
 */
sdrv_crypto_error_status_e cmd_trng_get_rand(uint8_t *rand_output,
                                             uint32_t rand_bytes);

#endif
