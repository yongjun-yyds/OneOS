/**
 * @file sdrv_crypto_mailbox_eccp.h
 * @brief SemiDrive CRYPTO mailbox eccp api header file.
 *
 * @copyright Copyright (c) 2021  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef SDRV_CRYPTO_MAILBOX_ECCP_H
#define SDRV_CRYPTO_MAILBOX_ECCP_H

#include "sdrv_crypto_mailbox_common.h"
#include "sdrv_crypto_mailbox_trng.h"

#define ECDSA_RAND_TEST
#define ECDSA_RAND_TEST_COUNT (2)

/**
 * @brief CRYPTO cmd of eccp basic vectors.
 */
typedef struct {
    uint8_t *G_x_y;
    uint8_t *k;
    uint8_t *kG_x_y;
    uint8_t *dblG_x_y;
    uint8_t *kG_plus_dblG_x_y;
} cmd_eccp_basic_vectors_t;

/**
 * @brief ECC curve type.
 */
typedef enum {
    SECP192R1 = 0,
    SECP224R1,
    SECP256R1,
    SECP384R1,
    SECP521R1,
} crypto_ECCcurvetype;

/**
 * @brief CRYPTO cmd of eccp point doubling.
 */
typedef struct {
    uint8_t cmd_id;
    uint8_t rev1[2];
    uint8_t curve_id;
    uint8_t inputPoint_ptr[4];
    uint8_t outputPoint_ptr[4];
} cmd_eccp_pointdoubling_t;

/**
 * @brief CRYPTO cmd of eccp point addition.
 */
typedef struct {
    uint8_t cmd_id;
    uint8_t rev1[2];
    uint8_t curve_id;
    uint8_t inputPoint1_ptr[4];
    uint8_t inputPoint2_ptr[4];
    uint8_t outputPoint_ptr[4];
} cmd_eccp_pointaddition_t;

/**
 * @brief CRYPTO cmd of eccp point multiplication.
 */
typedef struct {
    uint8_t cmd_id;
    uint8_t rev1[2];
    uint8_t curve_id;
    uint8_t inputScalar_ptr[4];
    uint8_t inputPoint_ptr[4];
    uint8_t outputPoint_ptr[4];
} cmd_eccp_pointmultiplication_t;

/**
 * @brief CRYPTO cmd of eccp point verifying.
 */
typedef struct {
    uint8_t cmd_id;
    uint8_t rev1[2];
    uint8_t curve_id;
    uint8_t inputPoint_ptr[4];
} cmd_eccp_pointverifying_t;

/**
 * @brief CRYPTO cmd of eccp generate key.
 */
typedef struct {
    uint8_t cmd_id;
    uint8_t curve_id;
    uint8_t key_id[2];
    uint8_t pubKey_ptr[4];
    uint8_t priKey_ptr[4];
    uint8_t aut_info_ptr[4];
} cmd_eccp_generatekey_t;

/**
 * @brief CRYPTO cmd of ecdh exchange key.
 */
typedef struct {
    uint8_t cmd_id;
    uint8_t rev1[1];
    uint8_t curve_id;
    uint8_t key_len;
    uint8_t key_id0[2];
    uint8_t key_id1[2];
    uint8_t selfPrikey_ptr[4];
    uint8_t PeerPubKey_ptr[4];
    uint8_t rev2[2];
    uint8_t key_id2[2];
    uint8_t key_ptr[4];
    uint8_t aut_info_ptr[4];
} cmd_ecdh_exchangekey_t;

/**
 * @brief CRYPTO cmd of ecdsa generate signature.
 */
typedef struct {
    uint8_t cmd_id;
    uint8_t rev1[1];
    uint8_t curve_id;
    uint8_t e_len;
    uint8_t e_ptr[4];
    uint8_t key_id0[2];
    uint8_t key_id1[2];
    uint8_t priKey_ptr[4];
    uint8_t signature_ptr[4];
    uint8_t aut_info_ptr[4];
} cmd_ecdsa_generatesignature_t;

/**
 * @brief CRYPTO cmd of ecdsa verify signature.
 */
typedef struct {
    uint8_t cmd_id;
    uint8_t rev1[1];
    uint8_t curve_id;
    uint8_t e_len;
    uint8_t e_ptr[4];
    uint8_t pubKey_ptr[4];
    uint8_t signature_ptr[4];
} cmd_ecdsa_verifysignature_t;

/**
 * @brief eccp point doubling
 *
 * This function get eccp point doubling.
 *
 * @param[in] curve_id curve_id
 * @param[in] G_x_y curve xy buff
 * @param[out] output_x_y out buff
 * @return CMD_RETURN_SUCCESS or error code.
 */
sdrv_crypto_error_status_e cmd_eccp_point_doubling(uint8_t curve_id,
                                                   uint8_t *G_x_y,
                                                   uint8_t *output_x_y);

/**
 * @brief eccp point addition
 *
 * This function get eccp point addition.
 *
 * @param[in] curve_id curve_id
 * @param[in] kG_x_y kG_x_y buff
 * @param[in] dblG_x_y dblG_x_y buff
 * @param[out] output_x_y out buff
 * @return CMD_RETURN_SUCCESS or error code.
 */
sdrv_crypto_error_status_e cmd_eccp_point_addition(uint8_t curve_id,
                                                   uint8_t *kG_x_y,
                                                   uint8_t *dblG_x_y,
                                                   uint8_t *output_x_y);

/**
 * @brief eccp point multplication
 *
 * This function get eccp point multplication.
 *
 * @param[in] curve_id curved id
 * @param[in] G_x_y point value
 * @param[in] k scalar value
 * @param[out]  output_x_y out buff
 * @return CMD_RETURN_SUCCESS or error code.
 */
sdrv_crypto_error_status_e cmd_eccp_point_multiplication(uint8_t curve_id,
                                                         uint8_t *G_x_y,
                                                         uint8_t *k,
                                                         uint8_t *output_x_y);

/**
 * @brief eccp point verify
 *
 * This function verify eccp point.
 *
 * @param[in] curve_id curved id
 * @param[in] G_x_y point value
 * @return CMD_RETURN_SUCCESS or error code.
 */
sdrv_crypto_error_status_e cmd_eccp_point_verify(uint8_t curve_id,
                                                 uint8_t *G_x_y);

/**
 * @brief eccp generate ecdh keypair
 *
 * This function generate ecdh keypair.
 *
 * @param[in] curve_id curved id
 * @param[out] prikey prikey buff
 * @param[out] pubkey pubkey buff
 * @param[in] generate_key_type pri and pub key output type
 * @param[in] generate_key_id for enc key
 * @param[in] mac_buf key_id mac buf
 * @return CMD_RETURN_SUCCESS or error code.
 */
sdrv_crypto_error_status_e cmd_eccp_ecdh_generate_keypair(
    uint8_t curve_id, uint8_t *prikey, uint8_t *pubkey,
    cmd_key_type_e generate_key_type, uint16_t generate_key_id,
    uint8_t *mac_buf);

/**
 * @brief ecdh exchange key
 *
 * This function for ecdh exchange key.
 *
 * @param[in] curve_id curved id
 * @param[in] key_len key len
 * @param[out] KA key buff
 * @param[in] pubkey_B PeerPubKey buff
 * @param[in] prikey_A selfPrikey buff
 * @param[in] ecdh_key_type selfPrikey type
 * @param[in] ecdh_key_id for enc key
 * @param[in] output_key_type output key type
 * @param[in] output_key_id for enc key
 * @param[in] mac_buf key_id mac buf
 * @return CMD_RETURN_SUCCESS or error code.
 */
/*mac_buf len is CMD_BASIC_TWO_KEY_MAC_BUFF_LEN 64*/
sdrv_crypto_error_status_e cmd_eccp_ecdh_exchange_key(
    uint8_t curve_id, uint8_t key_len, uint8_t *KA, uint8_t *pubkey_B,
    uint8_t *prikey_A, cmd_key_type_e ecdh_key_type, uint16_t ecdh_key_id,
    cmd_key_type_e output_key_type, uint16_t output_key_id, uint8_t *mac_buf);

/**
 * @brief eccp ecdsa gen signature
 *
 * This function for ecdsa gen signature.
 *
 * @param[in] curve_id curved id
 * @param[in] e e value
 * @param[out] e_len e len
 * @param[in] sign_key_type sign key type
 * @param[in] sign_key_id sign key id
 * @param[in] prikey prikey value
 * @param[out] signature signature value
 * @param[in] mac_buf key_id mac buf
 * @return CMD_RETURN_SUCCESS or error code.
 */
sdrv_crypto_error_status_e cmd_eccp_ecdsa_gen_signature(
    uint8_t curve_id, uint8_t *e, uint32_t e_len,
    cmd_key_type_e sign_key_type, uint16_t sign_key_id,
    uint8_t *prikey, uint8_t *signature, uint8_t *mac_buf);

/**
 * @brief eccp point verify
 *
 * This function verify eccp point.
 *
 * @param[in] curve_id curved id
 * @param[in] e e value
 * @param[out] e_len e len
 * @param[in] pubkey pubkey value
 * @param[in] signature signature value
 * @return CMD_RETURN_SUCCESS or error code.
 */
sdrv_crypto_error_status_e cmd_eccp_ecdsa_verify_signature(uint8_t curve_id,
                                                           uint8_t *e,
                                                           uint32_t e_len,
                                                           uint8_t *pubkey,
                                                           uint8_t *signature);

#endif