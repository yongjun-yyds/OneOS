/**
 * @file sdrv_crypto_mailbox_sm2.h
 * @brief crypto sm2 cmd interface
 *
 * @copyright Copyright (c) 2021  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef SDRV_CRYPTO_MAILBOX_SM2_H
#define SDRV_CRYPTO_MAILBOX_SM2_H

#include "sdrv_crypto_mailbox_common.h"
#include "sdrv_crypto_mailbox_trng.h"

/**
 * @brief CRYPTO cmd of sm2 get z.
 */
typedef struct {
    uint8_t cmd_id;
    uint8_t rev1[1];
    uint8_t id_len[2];
    uint8_t id_ptr[4];
    uint8_t pubKey_ptr[4];
    uint8_t z[4];
} cmd_sm2_getZ_t;

/**
 * @brief CRYPTO cmd of sm2 get E.
 */
typedef struct {
    uint8_t cmd_id;
    uint8_t rev1[3];
    uint8_t data_ptr[4];
    uint8_t data_len[4];
    uint8_t z[4];
    uint8_t e[4];
} cmd_sm2_getE_t;

/**
 * @brief CRYPTO cmd of sm2 generate key.
 */
typedef struct {
    uint8_t cmd_id;
    uint8_t rev1[1];
    uint8_t key_id[2];
    uint8_t pubKey_ptr[4];
    uint8_t priKey_ptr[4];
    uint8_t aut_info_ptr[4];
} cmd_sm2_generatekey_t;

/**
 * @brief CRYPTO cmd of sm2 get pubkey from prikey.
 */
typedef struct {
    uint8_t cmd_id;
    uint8_t rev1[3];
    uint8_t key_id0[2];
    uint8_t key_id1[2];
    uint8_t priKey_ptr[4];
    uint8_t pubKey_ptr[4];
    uint8_t aut_info_ptr[4];
} cmd_sm2_get_pubkey_from_prikey_t;

/**
 * @brief CRYPTO cmd of sm2 genrate signature.
 */
typedef struct {
    uint8_t cmd_id;
    uint8_t need_high_sec_ver;
    uint8_t rev1[2];
    uint8_t e_ptr[4];
    uint8_t key_id0[2];
    uint8_t key_id1[2];
    uint8_t priKey_ptr[4];
    uint8_t signature_ptr[4];
    uint8_t aut_info_ptr[4];
} cmd_sm2_generatesignature_t;

/**
 * @brief CRYPTO cmd of sm2 verify signature.
 */
typedef struct {
    uint8_t cmd_id;
    uint8_t need_high_sec_ver;
    uint8_t rev1[2];
    uint8_t e_ptr[4];
    uint8_t pubKey_ptr[4];
    uint8_t signature_ptr[4];
} cmd_sm2_verifysignature_t;

/**
 * @brief CRYPTO cmd of sm2 encrypt.
 */
typedef struct {
    uint8_t cmd_id;
    uint8_t need_high_sec_ver;
    uint8_t rev1[2];
    uint8_t data_ptr[4];
    uint8_t data_len[4];
    uint8_t pubKey_ptr[4];
    uint8_t cipher_ptr[4];
} cmd_sm2_encrypt_t;

/**
 * @brief CRYPTO cmd of sm2 decrypt.
 */
typedef struct {
    uint8_t cmd_id;
    uint8_t need_high_sec_ver;
    uint8_t rev1[2];
    uint8_t cipher_ptr[4];
    uint8_t cipher_len[4];
    uint8_t key_id0[2];
    uint8_t key_id1[2];
    uint8_t priKey_ptr[4];
    uint8_t data_ptr[4];
    uint8_t aut_info_ptr[4];
} cmd_sm2_decrypt_t;

/**
 * @brief input pointer of sm2 exchange key.
 */
typedef struct {
    uint8_t dA_ptr[4];
    uint8_t RA_ptr[4];
    uint8_t rA_ptr[4];
    uint8_t PB_ptr[4];
    uint8_t RB_ptr[4];
    uint8_t ZA_ptr[4];
    uint8_t ZB_ptr[4];
} sm2_exc_key_input_ptr_t;

/**
 * @brief CRYPTO cmd of sm2 exchange keyinfo.
 */
typedef struct {
    cmd_key_type_e dA_key_type;
    uint16_t dA_key_id;
    cmd_key_type_e rA_key_type;
    uint16_t rA_key_id;
    uint16_t enc_key_id;
    cmd_key_type_e output_key_type;
    uint16_t output_key_id;
} cmd_sm2_exchangekey_keyinfo_t;

/**
 * @brief CRYPTO cmd of sm2 exchange key.
 */
typedef struct {
    uint8_t cmd_id;
    uint8_t need_high_sec_ver;
    uint8_t role;
    uint8_t key_len;
    uint8_t key_id0[2];
    uint8_t key_id1[2];
    uint8_t key_id2[2];
    uint8_t key_id3[2];
    uint8_t input_ptr[4];
    uint8_t outputKey_ptr[4];
    uint8_t s1_s2_ptr[4];
    uint8_t sa_sb_ptr[4];
    uint8_t aut_info_ptr[4];
} cmd_sm2_exchangekey_t;

/**
 * @brief CRYPTO role of sm2 exchange.
 */
typedef enum {
    CMD_SM2_ROLE_SPONSOR = 0,
    CMD_SM2_ROLE_RESPONSOR,
    CMD_SM2_ROLE_MAX
} cmd_sm2_exchange_role_e;

/**
 * @brief sm2 get z value
 *
 * This function get z.
 *
 * @param[in] id id buff
 * @param[in] id_len id len
 * @param[in] pubkey pubkey buff
 * @param[out] z_buff out buff
 * @return CMD_RETURN_SUCCESS or error code
 */
sdrv_crypto_error_status_e cmd_sm2_get_z(uint8_t *id, uint32_t id_len,
                                         uint8_t *pubkey, uint8_t *z_buf);

/**
 * @brief get e value
 *
 * This function sm2 get e value
 *
 * @param[in] msg msg buff
 * @param[in] msg_len msg len
 * @param[in] z_buff z buff
 * @param[out] e_buff out buff
 * @return CMD_RETURN_SUCCESS or error code
 */
sdrv_crypto_error_status_e cmd_sm2_get_e(uint8_t *msg, uint32_t msg_len,
                                         uint8_t *z_buf, uint8_t *e_buf);

/**
 * @brief sm2 get pubkey
 *
 * This function get pubkey from prikey.
 *
 * @param[in] prikey prikey buff
 * @param[out] pubkey out buff
 * @return CMD_RETURN_SUCCESS or error code
 */
sdrv_crypto_error_status_e cmd_sm2_get_pubkey_from_prikey(uint8_t *prikey,
                                                          uint8_t *pubkey);

/**
 * @brief sm2 generatekey
 *
 * This function sm2 generatekey.
 *
 * @param[in] generate_key_type out put key type
 * @param[in] generate_key_id enc key id
 * @param[out] prikey prikey buff
 * @param[out] pubkey pubkey buff
 * @param[in] mac_buf mac buff
 * @return CMD_RETURN_SUCCESS or error code
 */
sdrv_crypto_error_status_e cmd_sm2_generatekey(cmd_key_type_e generate_key_type,
                                               uint16_t generate_key_id,
                                               uint8_t *prikey, uint8_t *pubkey,
                                               uint8_t *mac_buf);

/**
 * @brief sm2 sign
 *
 * This function sm2 sign
 *
 * @param[in] e e buff
 * @param[in] prikey prikey buff
 * @param[in] generate_sign_key_type sign key type
 * @param[in] generate_sign_key_id sign key id
 * @param[out] signature signature buff
 * @param[in] mac_buf mac buff
 * @return CMD_RETURN_SUCCESS or error code
 */
sdrv_crypto_error_status_e cmd_sm2_sign(uint8_t *e, uint8_t *prikey,
                                        cmd_key_type_e generate_sign_key_type,
                                        uint16_t generate_sign_key_id,
                                        uint8_t *signature, uint8_t *mac_buf);

/**
 * @brief sm2 verify
 *
 * This function sm2 verify
 *
 * @param[in] e e buff
 * @param[in] pubkey pubkey buff
 * @param[out] signature signature buff
 * @return CMD_RETURN_SUCCESS or error code
 */
sdrv_crypto_error_status_e cmd_sm2_verify(uint8_t *e, uint8_t *pubkey,
                                          uint8_t *signature);

/**
 * @brief sm2 enc
 *
 * This function sm2 enc
 *
 * @param[in] msg msg buff
 * @param[in] msg_len msg_len
 * @param[in] pubkey pubkey buff
 * @param[out] cipher cipher buff
 * @return CMD_RETURN_SUCCESS or error code
 */
sdrv_crypto_error_status_e cmd_sm2_enc(uint8_t *msg, uint32_t msg_len,
                                       uint8_t *pubkey, uint8_t *cipher);

/**
 * @brief sm2 dec
 *
 * This function sm2 dec
 *
 * @param[in] prikey prikey buff
 * @param[out] msg msg buff
 * @param[in] cipher cipher buff
 * @param[in] cipher_len cipher_len
 * @param[in] decrypt_key_type key type
 * @param[in] decrypt_key_id key id
 * @param[in] mac_buff mac buff
 * @return CMD_RETURN_SUCCESS or error code
 */
sdrv_crypto_error_status_e cmd_sm2_dec(uint8_t *prikey, uint8_t *msg,
                                       uint8_t *cipher, uint32_t cipher_len,
                                       cmd_key_type_e decrypt_key_type,
                                       uint16_t decrypt_key_id,
                                       uint8_t *mac_buf);

/**
 * @brief sm2 set exckey value
 *
 * This function set exckey value
 *
 * @param[in] dA dA buff
 * @param[in] RA RA buff
 * @param[in] rA rA buff
 * @param[in] RB RB buff
 * @param[in] PB PB buff
 * @param[in] ZA ZA buff
 * @param[in] ZB ZB buff
 * @param[out] cmd_sm2_exc_key_input exc_key_input buff
 * @return CMD_RETURN_SUCCESS or error code
 */
sdrv_crypto_error_status_e cmd_sm2_set_exckey_input_ptr(
    uint8_t *dA, uint8_t *RA, uint8_t *rA, uint8_t *RB,
    uint8_t *PB, uint8_t *ZA, uint8_t *ZB,
    sm2_exc_key_input_ptr_t *cmd_sm2_exc_key_input);

/**
 * @brief sm2 exchange key
 *
 * This function exchange key, mac_buf len is CMD_BASIC_TWO_KEY_MAC_BUFF_LEN 64
 *
 * @param[in] role exchange key role
 * @param[in] cmd_sm2_exckey_keyinfo exckey keyinfo
 * @param[in] cmd_sm2_exc_key_input exckey key input
 * @param[in] kbytelen kbytelen
 * @param[in] S1 S1 buff
 * @param[in] SA SA buff
 * @param[out] KA KA buff
 * @param[in] mac_buff mac buff
 * @return CMD_RETURN_SUCCESS or error code
 */
sdrv_crypto_error_status_e cmd_sm2_exchange_key(
    cmd_sm2_exchange_role_e role,
    cmd_sm2_exchangekey_keyinfo_t *cmd_sm2_exckey_keyinfo,
    sm2_exc_key_input_ptr_t *cmd_sm2_exc_key_input,
    uint32_t kbytelen, uint8_t *S1, uint8_t *SA, uint8_t *KA,
    uint8_t *mac_buf);
#endif