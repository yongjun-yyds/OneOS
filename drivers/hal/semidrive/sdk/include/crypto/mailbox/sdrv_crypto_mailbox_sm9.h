/**
 * @file sdrv_crypto_mailbox_sm9.h
 * @brief crypto sm9 cmd interface
 *
 * @copyright Copyright (c) 2021  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef SDRV_CRYPTO_MAILBOX_SM9_H
#define SDRV_CRYPTO_MAILBOX_SM9_H

#include "sdrv_crypto_mailbox_common.h"
#include "sdrv_crypto_mailbox_trng.h"

/**
 * @brief CRYPTO cmd of sm9 sign generate master key.
 */
typedef struct {
    uint8_t cmd_id;
    uint8_t rev1[3];
    uint8_t mastPubKey_ptr[4];
    uint8_t mastPriKey_ptr[4];
} cmd_sm9_sign_generate_master_key_t;

/**
 * @brief CRYPTO cmd of sm9 sign generate user prikey.
 */
typedef struct {
    uint8_t cmd_id;
    uint8_t hid;
    uint8_t id_len[2];
    uint8_t id_ptr[4];
    uint8_t mastPriKey_ptr[4];
    uint8_t userPriKey_ptr[4];
} cmd_sm9_sign_generate_user_prikey_t;

/**
 * @brief CRYPTO cmd of sm9 generate signature.
 */
typedef struct {
    uint8_t cmd_id;
    uint8_t rev1[3];
    uint8_t data_ptr[4];
    uint8_t data_len[4];
    uint8_t mastPubKey_ptr[4];
    uint8_t userPriKey_ptr[4];
    uint8_t signature_ptr[4];
} cmd_sm9_generatesignature_t;

/**
 * @brief CRYPTO cmd of sm9 verify signature.
 */
typedef struct {
    uint8_t cmd_id;
    uint8_t hid;
    uint8_t id_len[2];
    uint8_t id_ptr[4];
    uint8_t data_ptr[4];
    uint8_t data_len[4];
    uint8_t mastPubKey_ptr[4];
    uint8_t signature_ptr[4];
} cmd_sm9_verifysignature_t;

/**
 * @brief CRYPTO cmd of sm9 enc generate master key.
 */
typedef struct {
    uint8_t cmd_id;
    uint8_t rev1[3];
    uint8_t mastPubKey_ptr[4];
    uint8_t mastPriKey_ptr[4];
} cmd_sm9_enc_generate_master_key_t;

/**
 * @brief CRYPTO cmd of sm9 enc generate user prikey.
 */
typedef struct {
    uint8_t cmd_id;
    uint8_t hid;
    uint8_t id_len[2];
    uint8_t id_ptr[4];
    uint8_t mastPriKey_ptr[4];
    uint8_t userPriKey_ptr[4];
} cmd_sm9_enc_generate_user_prikey_t;

/**
 * @brief CRYPTO cmd of sm9 wrapkey.
 */
typedef struct {
    uint8_t cmd_id;
    uint8_t rev1[1];
    uint8_t hid;
    uint8_t key_len;
    uint8_t id_ptr[4];
    uint8_t id_len[2];
    uint8_t rev2[2];
    uint8_t mastPubKey_ptr[4];
    uint8_t key_ptr[4];
    uint8_t cipher_ptr[4];
} cmd_sm9_wrapkey_t;

/**
 * @brief CRYPTO cmd of sm9 unwrapkey.
 */
typedef struct {
    uint8_t cmd_id;
    uint8_t key_len;
    uint8_t id_len[2];
    uint8_t id_ptr[4];
    uint8_t userPriKey_ptr[4];
    uint8_t cipher_ptr[4];
    uint8_t key_ptr[4];
} cmd_sm9_unwrapkey_t;

/**
 * @brief CRYPTO cmd of sm9 encrypt.
 */
typedef struct {
    uint8_t cmd_id;
    uint8_t enc_type;
    uint8_t padding_type;
    uint8_t key2_len;
    uint8_t data_ptr[4];
    uint8_t data_len[4];
    uint8_t id_ptr[4];
    uint8_t id_len[2];
    uint8_t rev1[1];
    uint8_t hid;
    uint8_t mastPubKey_ptr[4];
    uint8_t cipher_ptr[4];
    uint8_t cipherLen_ptr[4];
} cmd_sm9_encrypt_t;

/**
 * @brief CRYPTO cmd of sm9 decrypt.
 */
typedef struct {
    uint8_t cmd_id;
    uint8_t enc_type;
    uint8_t padding_type;
    uint8_t key2_len;
    uint8_t cipher_ptr[4];
    uint8_t cipher_len[4];
    uint8_t id_ptr[4];
    uint8_t rev1[2];
    uint8_t id_len[2];
    uint8_t userPriKey_ptr[4];
    uint8_t data_ptr[4];
    uint8_t dataLen_ptr[4];
} cmd_sm9_decrypt_t;

typedef cmd_sm9_enc_generate_master_key_t cmd_sm9_exckey_generate_master_key_t;

typedef cmd_sm9_enc_generate_user_prikey_t
    cmd_sm9_exckey_generate_user_prikey_t;

/**
 * @brief CRYPTO cmd of sm9 exchange key generate user tmpkey.
 */
typedef struct {
    uint8_t cmd_id;
    uint8_t hid;
    uint8_t peer_id_len[2];
    uint8_t peer_id_ptr[4];
    uint8_t mastPubKey_ptr[4];
    uint8_t userTmpPriKey_ptr[4];
    uint8_t userTmpPubKey_ptr[4];
} cmd_sm9_exckey_generate_user_tmpkey_t;

/**
 * @brief CRYPTO cmd of sm9 exchange key generate user tmpkey.
 */
typedef struct {
    uint8_t Ppub_e_ptr[4];
    uint8_t deA_ptr[4];
    uint8_t RA_ptr[4];
    uint8_t rA_ptr[4];
    uint8_t RB_ptr[4];
    uint8_t IDA_ptr[4];
    uint8_t IDB_ptr[4];
} cmd_sm9_exc_key_input_ptr_t;

/**
 * @brief CRYPTO cmd of sm9 exchange key.
 */
typedef struct {
    uint8_t cmd_id;
    uint8_t rev1[1];
    uint8_t key_len;
    uint8_t role;
    uint8_t input_ptr[4];
    uint8_t ida_len[2];
    uint8_t idb_len[2];
    uint8_t outputKey_ptr[4];
    uint8_t s1_s2_ptr[4];
    uint8_t sa_sb_ptr[4];
} cmd_sm9_exchangekey_t;

/**
 * @brief type of sm9 encrypt.
 */
typedef enum {
    CMD_SM9_ENC_KDF_STREAM_CIPHER = 0,
    CMD_SM9_ENC_KDF_BLOCK_CIPHER,
    CMD_SM9_ENC_KDF_MAX
} cmd_sm9_enc_type_e;

/**
 * @brief padding of sm9 encrypt.
 *
 * available when choose SM9_ENC_KDF_BLOCK_CIPHER
 */
typedef enum {
    CMD_SM9_ENC_NO_PADDING = 0,
    CMD_SM9_ENC_PKCS7_PADDING,
    CMD_SM9_ENC_MAX_PADDING
} cmd_sm9_enc_padding_e;

/**
 * @brief SM9 key exchange role.
 */
typedef enum {
    CMD_SM9_ROLE_SPONSOR = 0,
    CMD_SM9_ROLE_RESPONSOR,
    CMD_SM9_ROLE_MAX
} cmd_sm9_exchangekey_role_e;

/**
 * @brief sm9 get master sign key
 *
 * This function get master sign key
 *
 * @param[out] ks prikey buff
 * @param[out] pub_s pubkey buff
 * @return CMD_RETURN_SUCCESS or error code
 */
sdrv_crypto_error_status_e cmd_sm9_sign_gen_master_key(uint8_t *ks,
                                                       uint8_t *pub_s);

/**
 * @brief sm9 get user sign prikey
 *
 * This function get user sign prikey
 *
 * @param[in] ida ida buff
 * @param[in] ida_len ida len
 * @param[out] ks master prikey buff
 * @param[out] dsa user prikey buff
 * @return CMD_RETURN_SUCCESS or error code
 */
sdrv_crypto_error_status_e cmd_sm9_sign_gen_user_prikey(uint8_t *ida,
                                                        uint32_t ida_len,
                                                        uint8_t *ks,
                                                        uint8_t *dsa);

/**
 * @brief sm9 gen signature
 *
 * This function gen signature
 *
 * @param[in] msg msg buff
 * @param[in] msg_len msg len
 * @param[in] pub_s master pubkey buff
 * @param[in] dsa user prikey buff
 * @param[out] signature out buff
 * @return CMD_RETURN_SUCCESS or error code
 */
sdrv_crypto_error_status_e cmd_sm9_gen_signature(uint8_t *msg, uint32_t msg_len,
                                                 uint8_t *pub_s, uint8_t *dsa,
                                                 uint8_t *signature);

/**
 * @brief sm9 verify signature
 *
 * This function verify signature
 *
 * @param[in] msg msg buff
 * @param[in] msg_len msg len
 * @param[in] ida ida buff
 * @param[in] ida_len ida len
 * @param[in] pub_s master pubkey buff
 * @param[out] signature out buff
 * @return CMD_RETURN_SUCCESS or error code
 */
sdrv_crypto_error_status_e cmd_sm9_verify_signature(uint8_t *msg,
                                                    uint32_t msg_len,
                                                    uint8_t *ida,
                                                    uint32_t ida_len,
                                                    uint8_t *pub_s,
                                                    uint8_t *signature);

/**
 * @brief sm9 gen enc master key
 *
 * This function get enc master key
 *
 * @param[out] ke prikey buff
 * @param[out] pub_e pubkey buff
 * @return CMD_RETURN_SUCCESS or error code
 */
sdrv_crypto_error_status_e cmd_sm9_enc_gen_master_key(uint8_t *ke,
                                                      uint8_t *pub_e);

/**
 * @brief sm9 gen user enc key
 *
 * This function get user enc key
 *
 * @param[in] idb idb buff
 * @param[in] idb_len idb len
 * @param[out] ke master prikey buff
 * @param[out] deb user prikey buff
 * @return CMD_RETURN_SUCCESS or error code
 */
sdrv_crypto_error_status_e cmd_sm9_enc_gen_user_prikey(uint8_t *idb,
                                                       uint32_t idb_len,
                                                       uint8_t *ke,
                                                       uint8_t *deb);

/**
 * @brief sm9 enc wrap key
 *
 * This function enc wrap key
 *
 * @param[in] idb idb buff
 * @param[in] idb_len idb len
 * @param[in] pub_e pubkey buff
 * @param[in] K key buff
 * @param[in] kbytelen key len
 * @param[out] C cipher key buff
 * @return CMD_RETURN_SUCCESS or error code
 */
sdrv_crypto_error_status_e cmd_sm9_enc_wrap_key(uint8_t *idb, uint32_t idb_len,
                                                uint8_t *pub_e, uint8_t *K,
                                                uint32_t kbytelen, uint8_t *C);

/**
 * @brief sm9 enc unwrap key
 *
 * This function enc unwrap key
 *
 * @param[in] idb idb buff
 * @param[in] idb_len idb len
 * @param[in] deb user prikey buff
 * @param[out] K_unwrap key buff
 * @param[in] kbytelen key len
 * @param[in] C cipher key buff
 * @return CMD_RETURN_SUCCESS or error code
 */
sdrv_crypto_error_status_e cmd_sm9_enc_unwrap_key(uint8_t *idb,
                                                  uint32_t idb_len,
                                                  uint8_t *deb,
                                                  uint8_t *K_unwrap,
                                                  uint32_t kbytelen,
                                                  uint8_t *C);

/**
 * @brief enc kdf stream
 *
 * This function enc kdf stream cipher
 *
 * @param[in] idb idb buff
 * @param[in] idb_len idb len
 * @param[in] key2_len key2 len
 * @param[in] pub_e pubkey buff
 * @param[in] msg msg buff
 * @param[in] msg_len msg len
 * @param[out] cipher out buff
 * @param[out] cipher_len out len
 * @return CMD_RETURN_SUCCESS or error code
 */
sdrv_crypto_error_status_e cmd_sm9_enc_kdf_stream_cipher(uint8_t *idb,
                                                         uint32_t idb_len,
                                                         uint32_t key2_len,
                                                         uint8_t *pub_e,
                                                         uint8_t *msg,
                                                         uint32_t msg_len,
                                                         uint8_t *cipher,
                                                         uint32_t *cipher_len);

/**
 * @brief dec kdf stream
 *
 * This function dec kdf stream
 *
 * @param[in] idb idb buff
 * @param[in] idb_len idb len
 * @param[in] key2_len key2 len
 * @param[in] deb prikey buff
 * @param[out] msg msg buff
 * @param[out] msg_len msg len
 * @param[in] cipher out buff
 * @param[in] cipher_len out len
 * @return CMD_RETURN_SUCCESS or error code
 */
sdrv_crypto_error_status_e cmd_sm9_dec_kdf_stream_cipher(uint8_t *idb,
                                                         uint32_t idb_len,
                                                         uint32_t key2_len,
                                                         uint8_t *deb,
                                                         uint8_t *msg,
                                                         uint32_t *msg_len,
                                                         uint8_t *cipher,
                                                         uint32_t cipher_len);

/**
 * @brief sm9 enc kdf block
 *
 * This function enc kdf block
 *
 * @param[in] idb idb buff
 * @param[in] idb_len idb len
 * @param[in] key2_len key2 len
 * @param[in] pub_e pubkey buff
 * @param[in] msg msg buff
 * @param[in] msg_len msg len
 * @param[out] cipher out buff
 * @param[out] cipher_len out len
 * @param[in] padding_type padding type
 * @return CMD_RETURN_SUCCESS or error code
 */
sdrv_crypto_error_status_e cmd_sm9_enc_kdf_block_cipher(uint8_t *idb,
                                                        uint32_t idb_len,
                                                        uint32_t key2_len,
                                                        uint8_t *pub_e,
                                                        uint8_t *msg,
                                                        uint32_t msg_len,
                                                        uint8_t *cipher,
                                                        uint32_t *cipher_len,
                                                        uint8_t padding_type);

/**
 * @brief sm9 dec kdf block
 *
 * This function dec kdf block
 *
 * @param[in] idb idb buff
 * @param[in] idb_len idb len
 * @param[in] key2_len key2 len
 * @param[in] deb prikey buff
 * @param[out] msg msg buff
 * @param[out] msg_len msg len
 * @param[in] cipher out buff
 * @param[in] cipher_len out len
 * @param[in] padding_type padding type
 * @return CMD_RETURN_SUCCESS or error code
 */
sdrv_crypto_error_status_e cmd_sm9_dec_kdf_block_cipher(uint8_t *id,
                                                        uint32_t id_len,
                                                        uint32_t key2_len,
                                                        uint8_t *deb,
                                                        uint8_t *msg,
                                                        uint32_t *msg_len,
                                                        uint8_t *cipher,
                                                        uint32_t cipher_len,
                                                        uint8_t padding_type);

/**
 * @brief sm9 gen master exckey key
 *
 * This function get master exckey key
 *
 * @param[out] ke prikey buff
 * @param[out] pub_e pubkey buff
 * @return CMD_RETURN_SUCCESS or error code
 */
sdrv_crypto_error_status_e cmd_sm9_exckey_gen_master_key(uint8_t *ke,
                                                         uint8_t *pub_e);

/**
 * @brief sm9 gen user exckey key
 *
 * This function get user exckey key
 *
 * @param[in] id id buff
 * @param[in] id_len id len
 * @param[out] ke master prikey buff
 * @param[out] de user prikey buff
 * @return CMD_RETURN_SUCCESS or error code
 */
sdrv_crypto_error_status_e cmd_sm9_exckey_gen_user_prikey(uint8_t *id,
                                                          uint32_t id_len,
                                                          uint8_t *ke,
                                                          uint8_t *de);

/**
 * @brief sm9 gen user exckey tmpkey
 *
 * This function get user exckey tmpkey
 *
 * @param[in] id id buff
 * @param[in] id_len id len
 * @param[out] pub_e master pubkey buff
 * @param[out] r user prikey buff
 * @param[out] R user Pubkey buff
 * @return CMD_RETURN_SUCCESS or error code
 */
sdrv_crypto_error_status_e cmd_sm9_exckey_gen_user_tmpkey(uint8_t *id,
                                                          uint32_t id_len,
                                                          uint8_t *pub_e,
                                                          uint8_t *r,
                                                          uint8_t *R);

/**
 * @brief sm9 set exckey input msg
 *
 * This function set exckey input msg
 *
 * @param[in] pub_e pub_e buff
 * @param[in] dA dA buff
 * @param[in] RA RA buff
 * @param[in] rA rA buff
 * @param[in] RB RB buff
 * @param[in] ida ida buff
 * @param[in] idb idb buff
 * @param[out] exckey_input out buff
 * @return CMD_RETURN_SUCCESS or error code
 */
sdrv_crypto_error_status_e cmd_sm9_set_exckey_input_ptr(
    uint8_t *pub_e, uint8_t *dA, uint8_t *RA, uint8_t *rA,
    uint8_t *RB, uint8_t *ida, uint8_t *idb,
    cmd_sm9_exc_key_input_ptr_t *exckey_input);

/**
 * @brief sm9 exhange key
 *
 * This function exchange key
 *
 * @param[in] role exchange key role
 * @param[in] ida_len ida len
 * @param[in] idb_len idb len
 * @param[in] kByteLen kbytelen
 * @param[in] exckey_input exckey_input buff
 * @param[out] KA KA buff
 * @param[in] S1 S1 buff
 * @param[in] SA SA buff
 * @return CMD_RETURN_SUCCESS or error code
 */
sdrv_crypto_error_status_e cmd_sm9_exchange_key(
    cmd_sm9_exchangekey_role_e role, uint32_t ida_len,
    uint32_t idb_len, uint32_t kByteLen,
    cmd_sm9_exc_key_input_ptr_t *exckey_input,
    uint8_t *KA, uint8_t *S1, uint8_t *SA);
#endif