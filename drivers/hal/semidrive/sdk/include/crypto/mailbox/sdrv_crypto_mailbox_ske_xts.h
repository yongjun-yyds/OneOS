/**
 * @file sdrv_crypto_mailbox_ske_xts.h
 * @brief crypto ske xts cmd interface
 *        XTS mode is only available for AES and SM4(block length is 128bits)
 *        plain must padding block len, bytelen is msg len before padding
 *
 * @copyright Copyright (c) 2021  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef SDRV_CRYPTO_MAILBOX_SKE_XTS_H
#define SDRV_CRYPTO_MAILBOX_SKE_XTS_H

#include "sdrv_crypto_mailbox_common.h"
#include "sdrv_crypto_mailbox_ske_basic.h"
#include "sdrv_crypto_mailbox_trng.h"

/**
 * @brief ske xts enc
 *
 * This function ske xts enc
 *
 * @param[in] mode ske mode
 * @param[in] plain plain buff
 * @param[in] bytelen plain bytelen
 * @param[in] key_type enc key type
 * @param[in] key enc key buff
 * @param[in] key_id enc key id
 * @param[in] iv iv buff
 * @param[out] cipher out buff
 * @param[in] mac_buf mac buff
 * @return CMD_RETURN_SUCCESS or error code.
 */
sdrv_crypto_error_status_e cmd_ske_xts_enc(uint8_t mode, uint8_t *plain,
                                           uint32_t bytelen,
                                           cmd_key_type_e key_type,
                                           uint8_t *key, uint16_t key_id,
                                           uint8_t *iv, uint8_t *cipher,
                                           uint8_t *mac_buf);

/**
 * @brief get device info
 *
 * This function get ehsm device info.
 *
 * @param[in] mode ske mode
 * @param[out] replain replain buff
 * @param[in] key_type enc key type
 * @param[in] key enc key buff
 * @param[in] key_id enc key id
 * @param[in] iv iv buff
 * @param[in] cipher out buff
 * @param[in] cipherbytelen cipher bytelen
 * @param[in] mac_buf mac buff
 * @return CMD_RETURN_SUCCESS or error code.
 */
sdrv_crypto_error_status_e cmd_ske_xts_dec(uint8_t mode, uint8_t *replain,
                                           cmd_key_type_e key_type,
                                           uint8_t *key, uint16_t key_id,
                                           uint8_t *iv, uint8_t *cipher,
                                           uint32_t cipherbytelen,
                                           uint8_t *mac_buf);
#endif