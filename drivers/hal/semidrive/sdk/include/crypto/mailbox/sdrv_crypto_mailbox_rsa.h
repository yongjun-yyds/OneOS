/**
 * @file sdrv_crypto_mailbox_rsa.h
 * @brief SemiDrive CRYPTO rsa interface header file.
 *
 * @copyright Copyright (c) 2021  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef SDRV_CRYPTO_MAILBOX_RSA_H
#define SDRV_CRYPTO_MAILBOX_RSA_H

#include "sdrv_crypto_mailbox_common.h"
#include "sdrv_crypto_mailbox_trng.h"

/**
 * @brief CRYPTO cmd of rsa get prime.
 */
typedef struct {
    uint8_t cmd_id;
    uint8_t rev1[1];
    uint8_t prime_len[2];
    uint8_t prime_ptr[4];
} cmd_rsa_getprime_t;

/**
 * @brief CRYPTO cmd of rsa generate key.
 */
typedef struct {
    uint8_t cmd_id;
    uint8_t rev1[3];
    uint8_t e_len[2];
    uint8_t n_len[2];
    uint8_t e_ptr[4];
    uint8_t rev2[2];
    uint8_t key_id[2];
    uint8_t d_ptr[4];
    uint8_t n_ptr[4];
    uint8_t aut_info_ptr[4];
} cmd_rsa_generatekey_t;

/**
 * @brief rsa crt prikey pointer.
 */
typedef struct {
    uint8_t p_ptr[4];
    uint8_t q_ptr[4];
    uint8_t dp_ptr[4];
    uint8_t dq_ptr[4];
    uint8_t u_ptr[4];
} rsa_crt_pri_key_ptr_t;

/**
 * @brief CRYPTO cmd of rsa generate CRTkey.
 */
typedef struct {
    uint8_t cmd_id;
    uint8_t rev1[3];
    uint8_t e_len[2];
    uint8_t n_len[2];
    uint8_t e_ptr[4];
    uint8_t rev2[2];
    uint8_t key_id[2];
    uint8_t priKey_ptr[4];
    uint8_t n_ptr[4];
    uint8_t aut_info_ptr[4];
} cmd_rsa_generateCRTkey_t;

/**
 * @brief CRYPTO cmd of rsa encrypt.
 */
typedef struct {
    uint8_t cmd_id;
    uint8_t rev1[1];
    uint8_t n_len[2];
    uint8_t data_ptr[4];
    uint8_t e_ptr[4];
    uint8_t n_ptr[4];
    uint8_t cipher_ptr[4];
} cmd_rsa_encrypt_t;

/**
 * @brief CRYPTO cmd of rsa decrypt.
 */
typedef struct {
    uint8_t cmd_id;
    uint8_t need_high_sec_ver;
    uint8_t n_len[2];
    uint8_t cipher_ptr[4];
    uint8_t rev2[2];
    uint8_t key_id[2];
    uint8_t e_ptr[4];
    uint8_t d_ptr[4];
    uint8_t n_ptr[4];
    uint8_t data_ptr[4];
    uint8_t aut_info_ptr[4];
} cmd_rsa_decrypt_t;

/**
 * @brief CRYPTO cmd of rsa CRT decrypt.
 */
typedef struct {
    uint8_t cmd_id;
    uint8_t need_high_sec_ver;
    uint8_t n_len[2];
    uint8_t cipher_ptr[4];
    uint8_t rev2[2];
    uint8_t key_id[2];
    uint8_t e_ptr[4];
    uint8_t priKey_ptr[4];
    uint8_t data_ptr[4];
    uint8_t aut_info_ptr[4];
} cmd_rsa_CRT_decrypt_t;

/**
 * @brief CRYPTO cmd of rsa generate signature.
 */
typedef struct {
    uint8_t cmd_id;
    uint8_t need_high_sec_ver;
    uint8_t n_len[2];
    uint8_t data_ptr[4];
    uint8_t rev2[2];
    uint8_t key_id[2];
    uint8_t e_ptr[4];
    uint8_t d_ptr[4];
    uint8_t n_ptr[4];
    uint8_t signature_ptr[4];
    uint8_t aut_info_ptr[4];
} cmd_rsa_generate_signature_t;

/**
 * @brief CRYPTO cmd of rsa CRT generate signature.
 */
typedef struct {
    uint8_t cmd_id;
    uint8_t need_high_sec_ver;
    uint8_t n_len[2];
    uint8_t data_ptr[4];
    uint8_t rev2[2];
    uint8_t key_id[2];
    uint8_t e_ptr[4];
    uint8_t priKey_ptr[4];
    uint8_t signature_ptr[4];
    uint8_t aut_info_ptr[4];
} cmd_rsa_CRT_generate_signature_t;

/**
 * @brief CRYPTO cmd of rsa verify.
 */
typedef struct {
    uint8_t cmd_id;
    uint8_t rev1[1];
    uint8_t n_len[2];
    uint8_t data_ptr[4];
    uint8_t e_ptr[4];
    uint8_t n_ptr[4];
    uint8_t signature_ptr[4];
} cmd_rsa_verify_t;

/**
 * @brief get prime
 *
 * This function get prime
 *
 * @param[in] primebitlen prime len
 * @param[out] prime out buff
 * @return CMD_RETURN_SUCCESS or error code.
 */
sdrv_crypto_error_status_e cmd_rsa_get_prime(uint16_t primebitlen,
                                             uint8_t *prime);

/**
 * @brief generate rsa key
 *
 * This function generate ras key.
 *
 * @param[in] ebitlen e bit len
 * @param[in] nbitlen n bit len
 * @param[in] generate_key_type out put key type
 * @param[in] generate_key_id enc key id
 * @param[out] n n buff
 * @param[out] e e buff
 * @param[out] d d buff
 * @param[in] mac_buf mac buff
 * @return CMD_RETURN_SUCCESS or error code.
 */
sdrv_crypto_error_status_e cmd_rsa_generate_key(
    uint16_t ebitlen, uint16_t nbitlen,
    cmd_key_type_e generate_key_type,
    uint16_t generate_key_id, uint8_t *n,
    uint8_t *e, uint8_t *d, uint8_t *mac_buf);

/**
 * @brief rsa encrypt
 *
 * This function encrypt , msg_len is same with n bitlen
 *
 * @param[in] msg msg buff
 * @param[in] e e buff
 * @param[in] n n buff
 * @param[in] nbitlen n bit len
 * @param[out] out out buff
 * @return CMD_RETURN_SUCCESS or error code.
 */
sdrv_crypto_error_status_e cmd_rsa_encrypt(uint8_t *msg, uint8_t *e, uint8_t *n,
                                           uint32_t nbitlen, uint8_t *out);

/**
 * @brief rsa decrypt
 *
 * This function decrypt.
 *
 * @param[out] plain plain buff
 * @param[in] d d buff
 * @param[in] e e buff
 * @param[in] n n buff
 * @param[in] nbitlen n bit len
 * @param[in] cipher cipher buff
 * @param[in] decrypt_key_type dec key type
 * @param[in] decrypt_key_id dec key id
 * @param[in] mac_buf out buff
 * @return CMD_RETURN_SUCCESS or error code.
 */
sdrv_crypto_error_status_e cmd_rsa_decrypt(uint8_t *plain, uint8_t *d,
                                           uint8_t *e, uint8_t *n,
                                           uint32_t nbitlen, uint8_t *cipher,
                                           cmd_key_type_e decrypt_key_type,
                                           uint16_t decrypt_key_id,
                                           uint8_t *mac_buf);

/**
 * @brief rsa generate sign
 *
 * This function generate signature.
 *
 * @param[in] msg msg buff
 * @param[in] d d buff
 * @param[in] e e buff
 * @param[in] n n buff
 * @param[in] nbitlen n bit len
 * @param[out] out out buff
 * @param[in] sign_key_type sign key type
 * @param[in] sign_key_id sign key id
 * @param[in] mac_buf out buff
 * @return CMD_RETURN_SUCCESS or error code.
 */
sdrv_crypto_error_status_e cmd_rsa_generate_signature(
    uint8_t *msg, uint8_t *d, uint8_t *e, uint8_t *n,
    uint32_t nbitlen, uint8_t *out,
    cmd_key_type_e sign_key_type,
    uint16_t sign_key_id, uint8_t *mac_buf);

/**
 * @brief rsa verify signature
 *
 * This function verify sign
 *
 * @param[in] msg msg buff
 * @param[in] e e buff
 * @param[in] n n buff
 * @param[in] nbitlen n bit len
 * @param[in] sign sign buff
 * @return CMD_RETURN_SUCCESS or error code.
 */
sdrv_crypto_error_status_e cmd_rsa_verify_signature(uint8_t *msg,
                                                    uint8_t *e,
                                                    uint8_t *n,
                                                    uint32_t nbitlen,
                                                    uint8_t *sign);

/**
 * @brief set prikey ptr
 *
 * This function set crt prikey
 *
 * @param[in] p p buff
 * @param[in] q q buff
 * @param[in] dp dp buff
 * @param[in] dq dq buff
 * @param[in] u u buff
 * @param[in] rsa_crt_pri_key rsa_crt_pri_key buff
 * @return CMD_RETURN_SUCCESS or error code.
 */
sdrv_crypto_error_status_e cmd_rsa_set_crt_prikey_ptr(
    uint8_t *p, uint8_t *q, uint8_t *dp, uint8_t *dq,
    uint8_t *u, rsa_crt_pri_key_ptr_t *rsa_crt_pri_key);

/**
 * @brief rsa generate crt key
 *
 * This function get crt key.
 *
 * @param[in] ebitlen e bit len
 * @param[in] nbitlen n bit len
 * @param[in] generate_key_type out put key type
 * @param[in] generate_key_id enc key id
 * @param[in] n n buff
 * @param[in] e e buff
 * @param[in] rsa_crt_pri_key rsa_crt_pri_key buff
 * @param[in] mac_buf out buff
 * @return CMD_RETURN_SUCCESS or error code.
 */
sdrv_crypto_error_status_e cmd_rsa_generate_crt_key(
    uint16_t ebitlen, uint16_t nbitlen, cmd_key_type_e generate_key_type,
    uint16_t generate_key_id, uint8_t *n, uint8_t *e,
    rsa_crt_pri_key_ptr_t *rsa_crt_pri_key, uint8_t *mac_buf);

/**
 * @brief rsa crt decrypt
 *
 * This function rsa crt decrypt.
 *
 * @param[in] msg msg buff
 * @param[in] e e buff
 * @param[in] n n buff
 * @param[in] nbitlen n bit len
 * @param[out] out out buff
 * @param[in] decrypt_key_type dec key type
 * @param[in] decrypt_key_id dec key id
 * @param[in] rsa_crt_pri_key rsa_crt_pri_key buff
 * @param[in] mac_buf out buff
 * @return CMD_RETURN_SUCCESS or error code.
 */
sdrv_crypto_error_status_e cmd_rsa_crt_decrypt(
    uint8_t *msg, uint8_t *e, uint8_t *n, uint32_t nbitlen,
    uint8_t *out, cmd_key_type_e decrypt_key_type,
    uint16_t decrypt_key_id, rsa_crt_pri_key_ptr_t *rsa_crt_pri_key,
    uint8_t *mac_buf);

/**
 * @brief rsa crt generate sign
 *
 * This function rsa crt generate sign
 *
 * @param[in] msg msg buff
 * @param[in] e e buff
 * @param[in] n n buff
 * @param[in] nbitlen n bit len
 * @param[out] out out buff
 * @param[in] sign_key_type sign key type
 * @param[in] sign_key_id sign key id
 * @param[in] rsa_crt_pri_key rsa_crt_pri_key buff
 * @param[in] mac_buf out buff
 * @return CMD_RETURN_SUCCESS or error code.
 */
sdrv_crypto_error_status_e cmd_rsa_crt_generate_signature(
    uint8_t *msg, uint8_t *e, uint8_t *n, uint32_t nbitlen,
    uint8_t *out, cmd_key_type_e sign_key_type,
    uint16_t sign_key_id, rsa_crt_pri_key_ptr_t *rsa_crt_pri_key,
    uint8_t *mac_buf);

#endif