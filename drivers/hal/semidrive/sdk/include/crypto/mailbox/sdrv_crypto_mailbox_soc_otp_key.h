/**
 * @file sdrv_crypto_mailbox_soc_otp_key.h
 * @brief soc otp key interface
 *
 * @copyright Copyright (c) 2021  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef SDRV_CRYPTO_MAILBOX_SOC_OTP_KEY_H
#define SDRV_CRYPTO_MAILBOX_SOC_OTP_KEY_H

#include "sdrv_crypto_mailbox_common.h"

/**
 * @brief get key from key_id
 *
 * @param[in] key_id id of key
 * @return address of key
 */
uint8_t *get_key_from_key_id(uint16_t key_id);

/**
 * @brief sm4 encrypt key
 *
 * This function is used to enc key
 *
 * @param[in] key_plain plain key
 * @param[in] key_plain_bytes key plain bytes
 * @param[in] kek_keyid key id
 * @param[out] key_cipher cipher key
 * @param[in] mac_buf for mac
 * @return CMD_RETURN_SUCCESS or error code
 */
sdrv_crypto_error_status_e KEK_encrypt(uint8_t *key_plain,
                                       uint32_t key_plain_bytes,
                                       uint16_t kek_keyid,
                                       uint8_t *key_cipher,
                                       uint8_t *mac_buf);

/**
 * @brief sm4 decrypt key
 *
 * This function is used to dec key
 *
 * @param[in] key_cipher cipher key
 * @param[in] key_plain_bytes key plain bytes
 * @param[in] kek_keyid key id
 * @param[out] key_plain replain key
 * @param[in] mac_buf for mac
 * @return CMD_RETURN_SUCCESS or error code
 */
sdrv_crypto_error_status_e KEK_decrypt(uint8_t *key_cipher,
                                       uint32_t key_plain_bytes,
                                       uint16_t kek_keyid,
                                       uint8_t *key_plain,
                                       uint8_t *mac_buf);

/**
 * @brief cacc read key
 *
 * This function is used to read key in cacc mode
 */
void otp_key_buf_test();

/**
 * @brief get otp key attribute
 *
 * This function is used to get attribute of otp key
 *
 * @param[in] key_id key id
 * @return the attribute of key
 */
uint8_t get_otp_key_attribute(uint16_t key_id);

/**
 * @brief sm2 get pubkey
 *
 * This function get pubkey from prikey.
 *
 * @param[in] prikey prikey buff
 * @param[out] pubkey out buff
 * @return CMD_SEIP_SUCCESS or error code
 */
sdrv_crypto_error_status_e check_key_id(uint16_t key_id, uint16_t is_aut_key);

/**
 * @brief get auth key id
 *
 * This function get auth key id from key_id.
 *
 * @param[in] key_id key id
 * @return auth key id
 */
uint16_t get_otp_key_mac_aut_key_id(uint16_t key_id);

/**
 * @brief get cmac
 *
 * This function get cmac.
 *
 * @param[in] mode cmac mode
 * @param[in] key_type key type
 * @param[in] key_id key id
 * @param[in] std_msg msg buff
 * @param[in] msg_bytes msg bytes
 * @param[out] output_mac out mac buff
 * @param[in] mac_bytes out buff
 * @return CMD_RETURN_SUCCESS or error code
 */
sdrv_crypto_error_status_e cmd_get_cmac(uint8_t mode, uint32_t key_type,
                                        uint16_t key_id, uint8_t *std_msg,
                                        uint32_t msg_bytes, uint8_t *output_mac,
                                        uint8_t mac_bytes);

/**
 * @brief get auth cmac
 *
 * This function get otp auth mac.
 *
 * @param[in] key_id key id buff
 * @param[in] key_count key count
 * @param[in] cmd cmd buff
 * @param[out] authentication_info out buff
 * @return CMD_SEIP_SUCCESS or error code
 */
sdrv_crypto_error_status_e get_otp_key_aut_mac(uint16_t *key_id,
                                               uint32_t key_count,
                                               uint8_t cmd[32],
                                               uint8_t *authentication_info);

#endif
