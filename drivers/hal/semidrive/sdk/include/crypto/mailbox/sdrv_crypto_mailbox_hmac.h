/**
 * @file sdrv_crypto_mailbox_hmac.h
 * @brief SemiDrive CRYPTO hmac interface header file.
 *
 * @copyright Copyright (c) 2021  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef SDRV_CRYPTO_MAILBOX_HMAC_H
#define SDRV_CRYPTO_MAILBOX_HMAC_H

#include "sdrv_crypto_mailbox_common.h"
#include "sdrv_crypto_mailbox_trng.h"

/**
 * @brief CRYPTO cmd of generate MAC.
 */
typedef struct {
    uint8_t cmd_id;
    uint8_t need_seed;
    uint8_t mode;
    uint8_t mac_len;
    uint8_t data_ptr[4];
    uint8_t data_len[4];
    uint8_t key_id0[2];
    uint8_t key_id1[2];
    uint8_t key_ptr[4];
    uint8_t rev2[2];
    uint8_t key_len[2];
    uint8_t mac_ptr[4];
    uint8_t aut_info_ptr[4];
} cmd_generateMAC_t;

/**
 * @brief CRYPTO cmd of verify MAC.
 */
typedef struct {
    uint8_t cmd_id;
    uint8_t need_seed;
    uint8_t mode;
    uint8_t mac_len;
    uint8_t data_ptr[4];
    uint8_t data_len[4];
    uint8_t key_id0[2];
    uint8_t key_id1[2];
    uint8_t key_ptr[4];
    uint8_t rev2[2];
    uint8_t key_len[2];
    uint8_t mac_ptr[4];
    uint8_t aut_info_ptr[4];
} cmd_verifyMAC_t;

/**
 * @brief get hmac value
 *
 * This function get hmac
 *
 * @param[in] mode hmac mode
 * @param[in] msg msg buff
 * @param[in] msg_bytes msg len
 * @param[in] key_type key type
 * @param[in] key key buff
 * @param[in] key_id key id
 * @param[in] key_bytes key len
 * @param[in] mac_bytes mac len
 * @param[out] out mac buff
 * @param[in] mac_buf key_id mac buf
 * @return CMD_RETURN_SUCCESS or error code.
 */
sdrv_crypto_error_status_e cmd_hmac_generate_mac(uint8_t mode, uint8_t *msg,
                                                 uint32_t msg_bytes,
                                                 cmd_key_type_e key_type,
                                                 const uint8_t *key,
                                                 uint16_t key_id,
                                                 uint32_t key_bytes,
                                                 uint8_t mac_bytes,
                                                 uint8_t *out,
                                                 uint8_t *mac_buf);

/**
 * @brief verify hmac
 *
 * This function verify hmac.
 *
 * @param[in] mode hmac mode
 * @param[in] msg msg buff
 * @param[in] msg_bytes msg len
 * @param[in] key_type key type
 * @param[in] key key buff
 * @param[in] key_id key id
 * @param[in] key_bytes key len
 * @param[in] hmac_bytes mac len
 * @param[in] hmac mac buff
 * @param[in] mac_buf key_id mac buf
 * @return CMD_RETURN_SUCCESS or error code.
 */
sdrv_crypto_error_status_e cmd_hmac_verify_mac(uint8_t mode, uint8_t *msg,
                                               uint32_t msg_bytes,
                                               cmd_key_type_e key_type,
                                               const uint8_t *key,
                                               uint16_t key_id,
                                               uint32_t key_bytes,
                                               uint8_t hmac_bytes,
                                               uint8_t *hmac, uint8_t *mac_buf);
#endif