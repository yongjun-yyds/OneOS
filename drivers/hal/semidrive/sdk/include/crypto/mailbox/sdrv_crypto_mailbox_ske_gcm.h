/**
 * @file sdrv_crypto_mailbox_ske_gcm.h
 * @brief SemiDrive CRYPTO mailbox ske gcm interface header file.
 *
 * @copyright Copyright (c) 2021  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef SDRV_CRYPTO_MAILBOX_SKE_GCM_H
#define SDRV_CRYPTO_MAILBOX_SKE_GCM_H

#include "sdrv_crypto_mailbox_common.h"
#include "sdrv_crypto_mailbox_ske_basic.h"
#include "sdrv_crypto_mailbox_trng.h"

/**
 * @brief ske gcm msg
 *
 * This function get ske gcm msg(aad + plaintext)
 *
 * @param[in] plain plain buff
 * @param[in] plainlen plain bytelen
 * @param[in] aad aad buff
 * @param[in] aadbytelen aad bytelen
 * @param[out] msg msg buff
 * @param[out] msgbytelen msg len
 * @return CMD_RETURN_SUCCESS or error code.
 */
sdrv_crypto_error_status_e cmd_ske_gcm_get_msg(uint8_t *plain,
                                               uint32_t plainlen,
                                               uint8_t *aad,
                                               uint8_t aadbytelen,
                                               uint8_t *msg,
                                               uint32_t *msgbytelen);

/**
 * @brief ske gcm enc
 *
 * This function ske gcm enc.
 *
 * @param[in] mode ske mode
 * @param[in] msg msg buff
 * @param[in] msgbytelen msg bytelen
 * @param[in] key_type enc key type
 * @param[in] key enc key buff
 * @param[in] key_id enc key id
 * @param[in] iv iv buff
 * @param[in] ivbytelen iv bytelen
 * @param[in] aadbytelen aadbytelen
 * @param[out] cipher out buff
 * @param[in] tagbytelen tag bytelen
 * @param[in] mac_buf mac buff
 * @param[in] cmd_gcm_enc_input gcm_enc_input buff
 * @return CMD_RETURN_SUCCESS or error code.
 */
sdrv_crypto_error_status_e cmd_ske_gcm_enc(
    uint8_t mode, uint8_t *msg, uint32_t msgbytelen,
    cmd_key_type_e key_type, uint8_t *key, uint16_t key_id,
    uint8_t *iv, uint8_t ivbytelen,uint8_t aadbytelen,
    uint8_t *cipher, uint32_t tagbytelen, uint8_t *mac_buf,
    gcm_enc_input_ptr_t *cmd_gcm_enc_input);

/**
 * @brief ske gcm dec
 *
 * This function ske gcm dec.
 *
 * @param[in] mode ske mode
 * @param[in] cipher cipher buff
 * @param[in] cipherbytelen cipher bytelen
 * @param[in] key_type enc key type
 * @param[in] key enc key buff
 * @param[in] key_id enc key id
 * @param[in] iv iv buff
 * @param[in] ivbytelen iv bytelen
 * @param[in] aadbytelen aadbytelen
 * @param[out] msg out buff
 * @param[in] tag tag buff
 * @param[in] tagbytelen tag bytelen
 * @param[in] mac_buf mac buff
 * @param[in] cmd_gcm_enc_input gcm_enc_input buff
 * @return CMD_RETURN_SUCCESS or error code.
 */
sdrv_crypto_error_status_e cmd_ske_gcm_dec(
    uint8_t mode, uint8_t *cipher, uint32_t cipherbytelen,
    cmd_key_type_e key_type, uint8_t *key, uint16_t key_id,
    uint8_t *iv, uint8_t ivbytelen, uint8_t aadbytelen,
    uint8_t *msg, uint8_t *tag, uint32_t tagbytelen,
    uint8_t *mac_buf, gcm_dec_input_ptr_t *cmd_gcm_dec_input);
#endif