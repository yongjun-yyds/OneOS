/**
 * @file sdrv_crypto_mailbox_basic_mgmt.h
 * @brief SemiDrive CRYPTO mailbox basic header file.
 *
 * @copyright Copyright (c) 2021  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef SDRV_CRYPTO_MAILBOX_BASIC_MGMT_H
#define SDRV_CRYPTO_MAILBOX_BASIC_MGMT_H

#include "sdrv_crypto_mailbox_common.h"

/**
 * @brief CRYPTO cmd of get device information.
 */
typedef struct {
    uint8_t cmd_id;
    uint8_t info_len;
    uint8_t key_id[2];
    uint8_t device_info_ptr[4];
    uint8_t aut_info_ptr[4];
} cmd_get_device_info_t;

/**
 * @brief CRYPTO cmd of get device status.
 */
typedef struct {
    uint8_t cmd_id;
    uint8_t status_len;
    uint8_t key_id[2];
    uint8_t device_status_ptr[4];
    uint8_t aut_info_ptr[4];
} cmd_get_device_status_t;

/**
 * @brief CRYPTO cmd of get trng.
 */
typedef struct {
    uint8_t cmd_id;
    uint8_t rev1[3];
    uint8_t random_ptr[4];
    uint8_t random_len[4];
} cmd_rng_t;

/**
 * @brief CRYPTO cmd of get Symmetric key.
 */
typedef struct {
    uint8_t cmd_id;
    uint8_t key_bytes;
    uint8_t key_id[2];
    uint8_t key_ptr[4];
    uint8_t aut_info_ptr[4];
} cmd_generate_key_t;

/**
 * @brief CRYPTO cmd of failure analysis.
 */
typedef struct {
    uint8_t cmd_id;
    uint8_t rev1[3];
    uint8_t aut_info_ptr[4];
} cmd_failure_analysis_t;

/**
 * @brief CRYPTO cmd of self destroy.
 */
typedef struct {
    uint8_t cmd_id;
    uint8_t rev1[3];
    uint8_t aut_info_ptr[4];
} cmd_self_destroy_t;

/**
 * @brief CRYPTO cmd of get trng.
 */
typedef struct {
    uint8_t cmd_id;
    uint8_t rev1[1];
    uint8_t key_id[2];
    uint8_t addr_ptr[4];
    uint8_t aut_info_ptr[4];
} cmd_seip_jump_t;

/**
 * @brief CRYPTO cmd of seip disable.
 */
typedef struct {
    uint8_t cmd_id;
    uint8_t rev1[15];
    uint8_t pin_code[16];
} cmd_seip_disable_t;

/**
 * @brief CRYPTO cmd of sensor clock configurations.
 */
typedef struct {
    uint8_t cmd_id;
    uint8_t rev1[3];
    uint8_t reg_addr[4];
    uint8_t reg_value[4];
    uint8_t reg_max_value[4];
    uint8_t reg_min_value[4];
} cmd_sensor_clk_cfg_t;

/**
 * @brief get device info.
 *
 * This function get ehsm device info.
 *
 * @param[in] key_id internel key id, use for encrypt device info.
 * @param[in] mac_buf for auth.
 * @param[out] device_info_buff out buff.
 * @return CMD_RETURN_SUCCESS or error code.
 */
sdrv_crypto_error_status_e cmd_get_device_info(uint16_t key_id,
                                               uint32_t *device_info_buf,
                                               uint8_t *mac_buf);

/**
 * @brief get device status.
 *
 * This function get ehsm device status.
 *
 * @param[in] key_id internel key id, use for encrypt device status.
 * @param[in] mac_buf for auth.
 * @param[out] device_status_buff out buff.
 * @return CMD_RETURN_SUCCESS or error code.
 */
sdrv_crypto_error_status_e cmd_get_device_status(uint16_t key_id,
                                                 uint32_t *device_status_buf,
                                                 uint8_t *mac_buf);

/**
 * @brief get 256bit random key.
 *
 * This function get 256bit random key use for ske or hmac, key_id must != 0.
 *
 * @param[in] key_id internel key id, use for encrypt random key.
 * @param[in] mac_buf for auth.
 * @param[out] key out buff.
 * @return CMD_RETURN_SUCCESS or error code.
 */
sdrv_crypto_error_status_e cmd_get_ske_hmac_256bit_key(uint16_t key_id,
                                                       uint32_t *key,
                                                       uint8_t *mac_buf);

/**
 * @brief self destroy.
 *
 * This function can destroy SSRK, this cmd only work when seip on high secure
 * status.
 *
 * @param[in] mac_buf for auth.
 * @return CMD_RETURN_SUCCESS or error code.
 */
sdrv_crypto_error_status_e cmd_key_self_destroy(uint8_t *mac_buf);

/**
 * @brief jump to addr run
 *
 * This function can set seip run addr, after set seip will jump to run
 *
 * @param[in] addr addr for seip run start
 * @param[in] mac_buf for auth
 * @return CMD_RETURN_SUCCESS or error code.
 */
sdrv_crypto_error_status_e cmd_jump(uint32_t addr, uint8_t *mac_buf);

/**
 * @brief disable seip
 *
 * This function disable seip, pincode used for auth
 *
 * @return CMD_RETURN_SUCCESS or error code.
 */
sdrv_crypto_error_status_e cmd_disable_seip(void);

/**
 * @brief set sensor clk
 *
 * This function config sensor clk, for protect seip
 *
 * @param[in] reg_addr sensor clk reg addr
 * @param[in] reg_value value
 * @param[in] reg_max_value max value
 * @param[in] reg_min_value min value
 * @return CMD_RETURN_SUCCESS or error code.
 */
sdrv_crypto_error_status_e cmd_sensor_clk_cfg(uint32_t reg_addr,
                                              uint32_t reg_value,
                                              uint32_t reg_max_value,
                                              uint32_t reg_min_value);

#endif
