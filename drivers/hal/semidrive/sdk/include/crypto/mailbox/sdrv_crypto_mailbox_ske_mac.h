/**
 * @file sdrv_crypto_mailbox_ske_mac.h
 * @brief SemiDrive CRYPTO ske mac interface header file.
 *
 * @copyright Copyright (c) 2021  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef SDRV_CRYPTO_MAILBOX_SKE_MAC_H
#define SDRV_CRYPTO_MAILBOX_SKE_MAC_H

#include "sdrv_crypto_mailbox_common.h"
#include "sdrv_crypto_mailbox_hmac.h"
#include "sdrv_crypto_mailbox_ske_basic.h"
#include "sdrv_crypto_mailbox_trng.h"

/**
 * @brief ske gen mac
 *
 * This function gen mac.
 *
 * @param[in] mode ske mode
 * @param[in] msg msg buff
 * @param[in] msg_bytes msg bytelen
 * @param[in] key_type enc key type
 * @param[in] key enc key buff
 * @param[in] key_id enc key id
 * @param[out] mac mac buff
 * @param[in] mac_bytes mac bytelen
 * @param[in] mac_buf mac buff
 * @return CMD_RETURN_SUCCESS or error code.
 */
sdrv_crypto_error_status_e cmd_ske_gen_mac(uint8_t mode, uint8_t *msg,
                                           uint32_t msg_bytes,
                                           cmd_key_type_e key_type,
                                           uint8_t *key, uint16_t key_id,
                                           uint8_t *mac, uint8_t mac_bytes,
                                           uint8_t *mac_buf);

/**
 * @brief ske verify mac
 *
 * This function verify mac
 *
 * @param[in] mode ske mode
 * @param[in] msg msg buff
 * @param[in] msg_bytes msg bytelen
 * @param[in] key_type enc key type
 * @param[in] key enc key buff
 * @param[in] key_id enc key id
 * @param[in] mac mac buff
 * @param[in] mac_bytes mac bytelen
 * @param[in] mac_buf mac buff
 * @return CMD_RETURN_SUCCESS or error code.
 */
sdrv_crypto_error_status_e cmd_ske_verify_mac(uint8_t mode, uint8_t *msg,
                                              uint32_t msg_bytes,
                                              cmd_key_type_e key_type,
                                              uint8_t *key, uint16_t key_id,
                                              uint8_t *mac, uint8_t mac_bytes,
                                              uint8_t *mac_buf);
#endif