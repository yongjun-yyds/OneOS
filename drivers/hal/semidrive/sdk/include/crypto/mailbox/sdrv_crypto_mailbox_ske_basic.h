/**
 * @file sdrv_crypto_mailbox_ske_basic.h
 * @brief SemiDrive CRYPTO ske basic interface header file.
 *
 * @copyright Copyright (c) 2021  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef SDRV_CRYPTO_MAILBOX_SKE_BASIC_H
#define SDRV_CRYPTO_MAILBOX_SKE_BASIC_H

#include "sdrv_crypto_mailbox_common.h"
#include "sdrv_crypto_mailbox_trng.h"

/**
 * @brief CRYPTO cmd of ske encrypt.
 */
typedef struct {
    uint8_t cmd_id;
    uint8_t need_seed;
    uint8_t rev1[1];
    uint8_t mode;
    uint8_t iv_ptr[4];
    uint8_t data_ptr[4];
    uint8_t data_len[4];
    uint8_t key_id0[2];
    uint8_t key_id1[2];
    uint8_t key_ptr[4];
    uint8_t cipher_ptr[4];
    uint8_t aut_info_ptr[4];
} cmd_ske_encrypt_t;

/**
 * @brief CRYPTO cmd of ske decrypt.
 */
typedef struct {
    uint8_t cmd_id;
    uint8_t need_seed;
    uint8_t rev1[1];
    uint8_t mode;
    uint8_t iv_ptr[4];
    uint8_t cipher_ptr[4];
    uint8_t cipher_len[4];
    uint8_t key_id0[2];
    uint8_t key_id1[2];
    uint8_t key_ptr[4];
    uint8_t data_ptr[4];
    uint8_t aut_info_ptr[4];
} cmd_ske_decrypt_t;

/**
 * @brief gcm encrypt input pointer.
 */
typedef struct {
    uint8_t iv_ptr[4];
    uint8_t aad_data_ptr[4];
} gcm_enc_input_ptr_t;

/**
 * @brief CRYPTO cmd of gcm encrypt.
 */
typedef struct {
    uint8_t cmd_id;
    uint8_t need_seed;
    uint8_t mode;
    uint8_t tag_len;
    uint8_t gcm_enc_input_ptr[4];
    uint8_t iv_len[2];
    uint8_t aad_len[2];
    uint8_t data_len[4];
    uint8_t key_id0[2];
    uint8_t key_id1[2];
    uint8_t key_ptr[4];
    uint8_t cipher_tag_ptr[4];
    uint8_t aut_info_ptr[4];
} cmd_aead_encrypt_gcm_t;

/**
 * @brief gcm decrypt input pointer.
 */
typedef struct {
    uint8_t iv_ptr[4];
    uint8_t aad_cipher_ptr[4];
    uint8_t tag_ptr[4];
} gcm_dec_input_ptr_t;

/**
 * @brief CRYPTO cmd of gcm decrypt.
 */
typedef struct {
    uint8_t cmd_id;
    uint8_t need_seed;
    uint8_t mode;
    uint8_t tag_len;
    uint8_t gcm_dec_input_ptr[4];
    uint8_t iv_len[2];
    uint8_t aad_len[2];
    uint8_t cipher_len[4];
    uint8_t key_id0[2];
    uint8_t key_id1[2];
    uint8_t key_ptr[4];
    uint8_t data_tag_ptr[4];
    uint8_t aut_info_ptr[4];
} cmd_aead_decrypt_gcm_t;

/**
 * @brief ske basic encrypt
 *
 * This function ske basic encrypt
 *
 * @param[in] mode ske mode
 * @param[in] plain plain buff
 * @param[in] bytelen plain bytelen
 * @param[in] key_type enc key type
 * @param[in] key_buf enc key buff
 * @param[in] key_id enc key id
 * @param[in] iv iv buff
 * @param[out] cipher out buff
 * @param[in] mac_buf mac buff
 * @return CMD_RETURN_SUCCESS or error code.
 */
sdrv_crypto_error_status_e cmd_ske_basic_enc(
    uint8_t mode, uint8_t *plain, uint32_t bytelen,
    cmd_key_type_e key_type, uint8_t *key_buf, uint16_t key_id,
    uint8_t *iv, uint8_t *cipher, uint8_t *mac_buf);

/**
 * @brief ske basic decrypt
 *
 * This function ske basic dec
 *
 * @param[in] mode ske mode
 * @param[out] replain replain buff
 * @param[in] bytelen plain bytelen
 * @param[in] key_type enc key type
 * @param[in] key_buf enc key buff
 * @param[in] key_id enc key id
 * @param[in] iv iv buff
 * @param[in] cipher out buff
 * @param[in] mac_buf mac buff
 * @return CMD_RETURN_SUCCESS or error code.
 */
sdrv_crypto_error_status_e cmd_ske_basic_dec(
    uint8_t mode, uint8_t *replain, uint32_t bytelen,
    cmd_key_type_e key_type, uint8_t *key_buf, uint16_t key_id,
    uint8_t *iv, uint8_t *cipher, uint8_t *mac_buf);
#endif