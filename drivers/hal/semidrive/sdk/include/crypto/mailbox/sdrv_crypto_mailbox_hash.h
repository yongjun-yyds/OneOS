/**
 * @file sdrv_crypto_mailbox_hash.h
 * @brief SemiDrive CRYPTO hash interface header file.
 *
 * @copyright Copyright (c) 2021  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef SDRV_CRYPTO_MAILBOX_HASH_H
#define SDRV_CRYPTO_MAILBOX_HASH_H

#include "sdrv_crypto_mailbox_common.h"
#include "sdrv_crypto_mailbox_trng.h"

#define HASH_PROMODE_START_TO_END       (0)
#define HASH_PROMODE_START_TO_MIDDLE    (1)
#define HASH_PROMODE_MIDDLE_TO_MIDDLE   (2)
#define HASH_PROMODE_MIDDLE_TO_END      (3)

/**
 * @brief CRYPTO cmd of HASH.
 */
typedef struct {
    uint8_t cmd_id;
    uint8_t process_mode;
    uint8_t rev1[1];
    uint8_t mode;
    uint8_t data_ptr[4];
    uint8_t data_len[4];
    uint8_t digest_ptr[4];
    uint8_t predata_len[8];
} cmd_hash_t;

/**
 * @brief get hash value.
 *
 * This function get hash.
 *
 * @param[in] mode hash mode.
 * @param[in] msg msg buff.
 * @param[in] msg_bytes msg len.
 * @param[out] digest hash value.
 * @return CMD_RETURN_SUCCESS or error code.
 */
sdrv_crypto_error_status_e cmd_hash_calc(uint8_t mode, uint8_t *msg,
                                         uint32_t msg_bytes, uint8_t *digest);

/**
 * @brief get hash value.
 *
 * This function get segment-hash.
 *
 * @param[in] mode hash mode.
 * @param[in] process_mode segment-hash mode.
 * @param[in] msg msg buff.
 * @param[in] msg_bytes msg len.
 * @param[out] digest hash value.
 * @param[in] byte length of previously processed messages.
 * @return CMD_RETURN_SUCCESS or error code.
 */
sdrv_crypto_error_status_e cmd_hash_update_calc(uint8_t mode,
                                                uint8_t process_mode,
                                                uint8_t *msg,
                                                uint32_t msg_bytes,
                                                uint8_t *digest,
                                                uint64_t predata_len);

#endif
