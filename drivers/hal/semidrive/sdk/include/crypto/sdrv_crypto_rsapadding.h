/**
 * @file sdrv_crypto_rsapadding.h
 * @brief crypto rsapadding interface
 *
 * @copyright Copyright (c) 2021  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef CRYPTO_RSAPADDING_H
#define CRYPTO_RSAPADDING_H

#include <sdrv_crypto_mailbox_common.h>
#include <sdrv_crypto_mailbox_trng.h>
#include <sdrv_crypto_mailbox_hash.h>
#include <sdrv_crypto_sharemem.h>
#include <armv7-r/cache.h>

/**
 * @brief CRYPTO mode of rsa padding.
 */
typedef enum {
    /* RSA Optimal Asymmetric Encryption Padding */
    CRYPTO_ALGOMODE_RSAES_OAEP,
    /* RSA encryption/decryption with PKCS#1 v1.5 padding */
    CRYPTO_ALGOMODE_RSAES_PKCS1_v1_5,
    /* RSA Probabilistic Signature */
    CRYPTO_ALGOMODE_RSASSA_PSS,
    /* RSA signature with PKCS#1 v1.5 */
    CRYPTO_ALGOMODE_RSASSA_PKCS1_v1_5,
    /* No padding */
    CRYPTO_ALGOMODE_RSA_NO_PAD,
} rsa_padding_mode_e;

/* Size of MD5 data block in bytes */
#define MD5_BLOCKSIZE           64
/* Size of MD5 initialization value in bytes */
#define MD5_INITSIZE            16
/* Size of MD5 digest in bytes */
#define MD5_DIGESTSIZE          16
/* Size of SHA1 data block in bytes */
#define SHA1_BLOCKSIZE          64
/* Size of SHA1 initialization value in bytes */
#define SHA1_INITSIZE           20
/* Size of SHA1 digest in bytes */
#define SHA1_DIGESTSIZE         20
/* Size of SHA224 data block in bytes */
#define SHA224_BLOCKSIZE        64
/* Size of SHA224 initialization value in bytes */
#define SHA224_INITSIZE         32
/* Size of SHA224 digest in bytes */
#define SHA224_DIGESTSIZE       28
/* Size of SHA256 data block in bytes */
#define SHA256_BLOCKSIZE        64
/* Size of SHA256 initialization value in bytes */
#define SHA256_INITSIZE         32
/* Size of SHA256 digest in bytes */
#define SHA256_DIGESTSIZE       32
/* Size of SHA384 data block in bytes */
#define SHA384_BLOCKSIZE        128
/* Size of SHA384 initialization value in bytes */
#define SHA384_INITSIZE         64
/* Size of SHA384 digest in bytes */
#define SHA384_DIGESTSIZE       48
/* Size of SHA512 data block in bytes */
#define SHA512_BLOCKSIZE        128
/* Size of SHA512 initialization value in bytes */
#define SHA512_INITSIZE         64
/* Size of SHA512 digest in bytes */
#define SHA512_DIGESTSIZE       64
/* Size of SHA512 data block in bytes */
#define SM3_BLOCKSIZE           64
/* Size of SHA512 initialization value in bytes */
#define SM3_INITSIZE            32
/* Size of SHA512 digest in bytes */
#define SM3_DIGESTSIZE          32
/* Maximum block size to be supported */
#define MAX_BLOCKSIZE           SHA512_BLOCKSIZE
/* Maximum digest size to be supported */
#define MAX_DIGESTSIZE          SHA512_DIGESTSIZE

#define RSA_MAX_SIZE            (4096/8)

#define MAC_BUFF_MAX_SIZE       (4096/8)

#define SEIP_CACHE_ALIGN_32BYTE_ADDR_MASK (0x1F)

/**
 * Encode a message using OAEP
 * @param  k        RSA parameter length in bytes
 * @param  hashType The hash function to use
 * @param  EM       Pointer to the encoded message (EM) buffer
 * @param  message  Block containing address of the message to encode
 * @param  mLen     Length of the message to be encoded (bytes)
 * @param  rng      Random number generator to use
 * @return          E_OK when no error occurs
 */
sdrv_crypto_error_status_e rsa_pad_eme_oaep_encode(uint32_t k,
                                                   uint32_t hash_alg,
                                                   uint8_t *EM,
                                                   uint8_t *message,
                                                   uint32_t mLen);

/**
 * Decode a message using OEAP
 * @param  k        RSA parameter length in bytes
 * @param  hashType the hash function to use
 * @param  EM       Pointer to the encoded message (EM) buffer
 * @param  message  Output: Pointer to the decoded message. The decoding is
 *                  done in place within the limits of the EM buffer.
 * @param  mLen     Output: Length of the decoded message
 * @return          E_OK when no error occurs
 */
sdrv_crypto_error_status_e rsa_pad_eme_oaep_decode(uint32_t k,
                                                   uint32_t hash_alg,
                                                   uint8_t *EM,
                                                   uint8_t **message,
                                                   uint32_t *mLen);

/**
 * Encode message using PKCS
 * @param  k        RSA parameter length in bytes
 * @param  EM       Pointer to the encoded message (EM) buffer
 * @param  message  Block containing address of the message to encode
 * @param  mLen     Len of the message to be encoded (bytes)
 * @param  rng      random number generator to use
 * @return          E_OK when no error occurs
 */
sdrv_crypto_error_status_e rsa_pad_eme_pkcs_encode(uint32_t k,
                                                   uint8_t *EM,
                                                   uint8_t *message,
                                                   uint32_t mLen);

/**
 * Decodes encoded message using PKCS. message will point to the decoded message in EM
 * @param  k        RSA param length in bytes
 * @param  EM       Pointer to the encoded message (EM) buffer
 * @param  message  Output: Pointer to the decoded message in EM. The decoding
 *                  is done in place within the limits of the EM buffer.
 * @param  mLen     Output: Length of the decoded message (bytes)
 * @return          E_OK when no error occurs
 */
sdrv_crypto_error_status_e rsa_pad_eme_pkcs_decode(uint32_t k,
                                                   uint8_t *EM,
                                                   uint8_t **message,
                                                   uint32_t *mLen);

/**
 * Encode hash using PKCS
 * @param  emLen     Length of encoded message (EM) buffer (RSA parameter length
 *                   in bytes)
 * @param  hashype 	 Hash function used for hashing hash (also used for the
 *                   PKCS algorithm)
 * @param  EM        Output: Pointer to the encoded message buffer
 * @param  hash      Input: Hash to encode
 * @return           E_OK if no error occurs
 */
sdrv_crypto_error_status_e rsa_pad_emsa_pkcs_encode(uint32_t emLen,
                                                    uint32_t hash_type,
                                                    uint8_t *EM,
                                                    uint8_t *hash);
/**
 * Encode hash using PSS. This function uses a salt length equal to the hash
 * digest length.
 * @param  emLen     Length of encoded message (EM) buffer (RSA parameter length
 *                   in bytes)
 * @param  hashType  Hash function used for hashing hash (also used for PKCS
 *                   algorithm)
 * @param  EM        Output: Pointer to the encoded message buffer
 * @param  hash      Input: Hash to encode
 * @param  n0        MSB of the modulus (for masking in order to match the
 *                   modulus size)
 * @param  sLen      Intended length of the salt
 * @param  rng       Random number generator to use
 * @return           E_OK if no error occurs
 */
sdrv_crypto_error_status_e rsa_pad_emsa_pss_encode(uint32_t emLen,
                                                   uint32_t hash_alg,
                                                   uint8_t *EM,
                                                   uint8_t *hash,
                                                   uint32_t n0,
                                                   uint32_t sLen);
/**
 * Decode encoded message using PSS and compares hash to the decoded hash
 * @param  emLen     Length of encoded message (EM) buffer (RSA parameter length
 *                   in bytes)
 * @param  hashType  Hash function used for hasing hash
 * @param  EM        Input: Pointer to the encoded message buffer
 * @param  hash      Hash to compare with
 * @param  sLen      Intended length of the salt
 * @param  n0        MSB of the modulus (for masking in order to match the
 *                   modulus size)
 * @return           E_OK if no error occurs and hash is valid
 */
sdrv_crypto_error_status_e rsa_pad_emsa_pss_decode(uint32_t emLen,
                                                   uint32_t hash_alg,
                                                   uint8_t *EM,
                                                   uint8_t *hash,
                                                   uint32_t sLen,
                                                   uint32_t n0);
/**
 * Pads the hash of hashLen to EM of emLen. MSBs are set to 0.
 *
 * \warning There should not be overlapping between EM and hash !
 * @param EM      Destination buffer (pointer)
 * @param emLen   Length of the destination buffer (bytes)
 * @param hash    Input to pad
 * @param hashLen Length of the input
 */
void rsa_pad_zeros(uint8_t *EM,
                   uint32_t emLen,
                   uint8_t *hash,
                   uint32_t hashLen);

#endif /* SDRV_CRYPTO_RSAPADDING_H */
