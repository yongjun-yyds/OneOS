/**
 * @file sdrv_crypto_init.h
 * @brief SemiDrive CRYPTO init header file.
 *
 * @copyright Copyright (c) 2021  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef SDRV_CRYPTO_INIT_H
#define SDRV_CRYPTO_INIT_H
#include <stdint.h>

/**
 * @brief CRYPTO type.
 */
typedef enum {
    SDRV_CRYPTO_TYPE_CACC = 0,
    SDRV_CRYPTO_TYPE_MAILBOX,
    SDRV_CRYPTO_TYPE_MAX
} sdrv_crypto_type_e;

/**
 * @brief Get the type of CRYPTO type.
 *
 * This function return crypto type
 *
 * @return The result of CRYPTO type.
 */
uint32_t sdrv_crypto_get_type(void);

/**
 * @brief Sdrv CRYPTO initialization.
 *
 * This function init hsm
 */
void sdrv_crypto_init(void);

#endif