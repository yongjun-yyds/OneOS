/**
 * @file sdrv_crypto_sharemem.h
 * @brief crypto sharemem interface
 *
 * @copyright Copyright (c) 2021  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef SDRV_CRYPTO_SHARE_MEM_H
#define SDRV_CRYPTO_SHARE_MEM_H

#include <sdrv_crypto_mailbox_common.h>

#define CRYPTO_SHARE_MEM_RSA_MAX                   512
#define CRYPTO_SHARE_MEM_RSA_MAX_EXT               516
#define CRYPTO_SHARE_MEM_DIG_MAX                   64
#define CRYPTO_SHARE_MEM_HASH_PAD_MAX              256
#define CRYPTO_SHARE_MEM_COMM_SMALL_BUFF_MAX       64
#define CRYPTO_SHARE_MEM_COMM_BUFF_MAX             516

/**
 * @brief get idle block
 *
 * This function get share memory block
 *
 * @param[in] size size of block.
 * @return block address.
 */
uint8_t *ShareMem_GetBlock(uint32_t size);

/**
 * @brief release block
 *
 * This function release block
 *
 * @param[in] ptr block pointer.
 * @return E_OK or error code.
 */
sdrv_crypto_error_status_e ShareMem_ReleaseBlock(uint8_t *ptr);

#endif /* SDRV_CRYPTO_SHARE_MEM_H */
