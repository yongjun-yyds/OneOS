/**
 * @file sdrv_power.h
 * @brief SemiDrive power manager header file.
 *
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef SDRV_POWER_H_
#define SDRV_POWER_H_

#include <sdrv_common.h>
#include <types.h>
#include <regs_base.h>
#include <part.h>
#include "sdrv_smc.h"
#include "sdrv_pmu.h"

#if CONFIG_E3 || CONFIG_D3
#define SDRV_SF_MODULE_CANFD1_MASK        (0x1UL)
#define SDRV_SF_MODULE_CANFD2_MASK        (0x2UL)
#define SDRV_SF_MODULE_CANFD3_4_MASK      (0x4UL)
#define SDRV_SF_MODULE_CANFD5_8_MASK      (0x8UL)
#define SDRV_SF_MODULE_CANFD9_16_MASK     (0x10UL)
#define SDRV_SF_MODULE_CANFD17_24_MASK    (0x20UL)
#define SDRV_SF_MODULE_XSPI1A_MASK        (0x40UL)
#define SDRV_SF_MODULE_XSPI1B_MASK        (0x80UL)
#define SDRV_SF_MODULE_XSPI2A_MASK        (0x100UL)
#define SDRV_SF_MODULE_XSPI2B_MASK        (0x200UL)
#define SDRV_SF_MODULE_DMA_MASK           (0x400UL)
#define SDRV_SF_MODULE_GAMA1_MASK         (0x800UL)
#define SDRV_SF_MODULE_GAMA2_MASK         (0x1000UL)
#define SDRV_SF_MODULE_ENET1_MASK         (0x2000UL)
#define SDRV_SF_MODULE_ENET2_MASK         (0x4000UL)
#define SDRV_SF_MODULE_VIC1_MASK          (0x8000UL)
#define SDRV_SF_MODULE_VIC2A_MASK         (0x10000UL)
#define SDRV_SF_MODULE_VIC2B_MASK         (0x20000UL)
#define SDRV_SF_MODULE_VIC3A_MASK         (0x40000UL)
#define SDRV_SF_MODULE_VIC3B_MASK         (0x80000UL)
#define SDRV_SF_MODULE_XSPI_SLV_MASK      (0x100000UL)
#define SDRV_SF_MODULE_MB_MASK            (0x200000UL)
#define SDRV_SF_MODULE_XTRG_MASK          (0x400000UL)

#define SDRV_SF_PD_MASK                   (0x1UL)
#define SDRV_SP_PD_MASK                   (0x2UL)
#define SDRV_SX_PD_MASK                   (0x4UL)
#define SDRV_GAMA1_PD_MASK                (0x8UL)
#define SDRV_AP_DISP_PD_MASK              (0x10UL)
#endif /* #if CONFIG_E3 || CONFIG_D3 */

#if CONFIG_E3L || CONFIG_D3L
/* If SF_BOOT set power down in hib, Module reset below can't be mask:
    CANFD16, CANFD21, CANFD3, CANFD4, CANFD5, CANFD6, CANFD7, CANFD23
    XSPI1A, XSPI1B, DMA_RST0, DMA_RST1, ENET1, VIC1, XSPI_SLV, XTRG

    If AP_MIX set power down in hib, Module reset below can't be mask:
     SACI2, SEHC1, USB, SEIP
*/
#define SDRV_SF_MODULE_CANFD16_MASK       (0x1UL)
#define SDRV_SF_MODULE_CANFD21_MASK       (0x2UL)
#define SDRV_SF_MODULE_CANFD3_MASK        (0x4UL)
#define SDRV_SF_MODULE_CANFD4_MASK        (0x8UL)
#define SDRV_SF_MODULE_CANFD5_MASK        (0x10UL)
#define SDRV_SF_MODULE_CANFD6_MASK        (0x20UL)
#define SDRV_SF_MODULE_CANFD7_MASK        (0x40UL)
#define SDRV_SF_MODULE_CANFD23_MASK       (0x80UL)
#define SDRV_SF_MODULE_XSPI1A_MASK        (0x100UL)
#define SDRV_SF_MODULE_XSPI1B_MASK        (0x200UL)
#define SDRV_SF_MODULE_DMA_RST0_MASK      (0x400UL)
#define SDRV_SF_MODULE_DMA_RST1_MASK      (0x800UL)
#define SDRV_SF_MODULE_ENET1_MASK         (0x1000UL)
#define SDRV_SF_MODULE_VIC1_MASK          (0x2000UL)
#define SDRV_SF_MODULE_XSPI_SLV_MASK      (0x4000UL)
#define SDRV_SF_MODULE_XTRG_MASK          (0x8000UL)
#define SDRV_SF_MODULE_SACI2_MASK         (0x10000UL)
#define SDRV_SF_MODULE_SEHC1_MASK         (0x20000UL)
#define SDRV_SF_MODULE_USB_MASK           (0x40000UL)
#define SDRV_SF_MODULE_SEIP_MASK          (0x80000UL)
#define SDRV_SF_MODULE_CSLITE_MASK        (0x100000UL)

#define SDRV_SF_PD_MASK                   (0x1UL)
#define SDRV_SF_BOOT_PD_MASK              (0x2UL)
#define SDRV_AP_MIX_PD_MASK               (0x4UL)
#endif /* #if CONFIG_E3L || CONFIG_D3L */

#define SDRV_AP_MODULE_CSI_MASK           (0x1UL)
#define SDRV_AP_MODULE_DC_MASK            (0x2UL)
#define SDRV_AP_MODULE_G2D_MASK           (0x4UL)
#define SDRV_AP_MODULE_SDRAMC_MASK        (0x8UL)
#define SDRV_AP_MODULE_SACI1_MASK         (0x10UL)
#define SDRV_AP_MODULE_SACI2_MASK         (0x20UL)
#define SDRV_AP_MODULE_DMA_MASK           (0x40UL)
#define SDRV_AP_MODULE_SEHC1_MASK         (0x80UL)
#define SDRV_AP_MODULE_SEHC2_MASK         (0x100UL)
#define SDRV_AP_MODULE_USB_MASK           (0x200UL)
#define SDRV_AP_MODULE_SEIP_MASK          (0x400UL)
#define SDRV_AP_MODULE_LVDS_MASK          (0x800UL)

#define SDRV_PLL1_PD_MASK                 (0x1UL)
#define SDRV_PLL2_PD_MASK                 (0x2UL)
#define SDRV_PLL3_PD_MASK                 (0x4UL)
#define SDRV_PLL4_PD_MASK                 (0x8UL)
#define SDRV_PLL5_PD_MASK                 (0x10UL)
#define SDRV_PLL_LVDS_PD_MASK             (0x20UL)

#define SDRV_PWR_CTRL0_PD_MASK            (0x1UL)
#define SDRV_PWR_CTRL1_PD_MASK            (0x2UL)
#define SDRV_PWR_CTRL2_PD_MASK            (0x4UL)
#define SDRV_PWR_CTRL3_PD_MASK            (0x8UL)
#define SDRV_PWR_ON0_PD_MASK              (0x10UL)
#define SDRV_PWR_ON1_PD_MASK              (0x20UL)
#define SDRV_PWR_ON2_PD_MASK              (0x40UL)
#define SDRV_PWR_ON3_PD_MASK              (0x80UL)

#define SDRV_IRAM1_PD_MASK                (0x1UL)
#define SDRV_IRAM2_PD_MASK                (0x2UL)
#define SDRV_IRAM3_PD_MASK                (0x4UL)
#define SDRV_IRAM4_PD_MASK                (0x8UL)

/**
 * @brief Power status error code.
 */
enum sdrv_power_error {
    SDRV_POWER_CONFIG_PARAMETER_WRONG = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_POWER, 0),
    SDRV_POWER_MODE_SET_WRONG = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_POWER, 1),
    SDRV_POWER_WAKEUP_STATUS_ERROR = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_POWER, 2),
    SDRV_POWER_IRAM_REINIT_TIMEOUT = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_POWER, 3),
    SDRV_POWER_CLOCK_SET_FAILED = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_POWER, 4),
    SDRV_POWER_AP_STATUS_ERROR = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_POWER, 5),
    SDRV_POWER_PMU_STATUS_ERROR = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_POWER, 6),
    SDRV_POWER_SMC_STATUS_ERROR = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_POWER, 7),
};

/**
 * @brief Definition for MCU power switch and IRAMC power.
 */
typedef enum mcu_power {
    MCU_POWER_SF = 0,
    MCU_POWER_SP,
    MCU_POWER_SX,
    MCU_POWER_GAMA,
    MCU_POWER_IRAM1,
    MCU_POWER_IRAM2,
    MCU_POWER_IRAM3,
    MCU_POWER_IRAM4,
    MCU_POWER_ACMP1,
    MCU_POWER_ACMP2,
    MCU_POWER_ACMP3,
    MCU_POWER_ACMP4,
    MCU_POWER_END = 0xFFFFFFFF,
} mcu_power_e;

/**
 * @brief Definition for wakeup source.
 */
typedef enum sdrv_wakeup_src {
    SDRV_WAKEUP_GPIO_SF = 0UL,
    SDRV_WAKEUP_GPIO_AP,

    SDRV_WAKEUP_RTC1,
    SDRV_WAKEUP_RTC2,

    SDRV_WAKEUP_UART1,
    SDRV_WAKEUP_UART2,
    SDRV_WAKEUP_UART3,
    SDRV_WAKEUP_UART4,
    SDRV_WAKEUP_UART5,
    SDRV_WAKEUP_UART6,
    SDRV_WAKEUP_UART7,
    SDRV_WAKEUP_UART8,

#ifdef APB_UART9_BASE
    SDRV_WAKEUP_UART9,
    SDRV_WAKEUP_UART10,
    SDRV_WAKEUP_UART11,
    SDRV_WAKEUP_UART12,
#endif

#ifdef APB_UART13_BASE
    SDRV_WAKEUP_UART13,
    SDRV_WAKEUP_UART14,
    SDRV_WAKEUP_UART15,
    SDRV_WAKEUP_UART16,
#endif

#ifdef APB_CANFD1_BASE
    SDRV_WAKEUP_CANFD1,
    SDRV_WAKEUP_CANFD2,
#endif

#ifdef APB_CANFD3_BASE
    SDRV_WAKEUP_CANFD3,
    SDRV_WAKEUP_CANFD4,
#endif

#ifdef APB_CANFD5_BASE
    SDRV_WAKEUP_CANFD5,
    SDRV_WAKEUP_CANFD6,
#endif

#ifdef APB_CANFD7_BASE
    SDRV_WAKEUP_CANFD7,
#endif

#ifdef APB_CANFD8_BASE
    SDRV_WAKEUP_CANFD8,
    SDRV_WAKEUP_CANFD9,
    SDRV_WAKEUP_CANFD10,
    SDRV_WAKEUP_CANFD11,
    SDRV_WAKEUP_CANFD12,
    SDRV_WAKEUP_CANFD13,
    SDRV_WAKEUP_CANFD14,
    SDRV_WAKEUP_CANFD15,
#endif

#ifdef APB_CANFD16_BASE
    SDRV_WAKEUP_CANFD16,
#endif

#ifdef APB_CANFD17_BASE
    SDRV_WAKEUP_CANFD17,
    SDRV_WAKEUP_CANFD18,
    SDRV_WAKEUP_CANFD19,
    SDRV_WAKEUP_CANFD20,
#endif

#ifdef APB_CANFD21_BASE
    SDRV_WAKEUP_CANFD21,
#endif

#ifdef APB_CANFD22_BASE
    SDRV_WAKEUP_CANFD22,
#endif

#ifdef APB_CANFD23_BASE
    SDRV_WAKEUP_CANFD23,
#endif

#ifdef APB_CANFD24_BASE
    SDRV_WAKEUP_CANFD24,
#endif

    SDRV_WAKEUP_END = 0x7FFFFFFFUL,
} sdrv_wakeup_src_e;

/**
 * @brief Definition for system work mode.
 */
typedef enum sdrv_power_mode {
    SDRV_RUN_MODE,
    SDRV_SLEEP_MODE,
    SDRV_HIBERNATE_MODE,
    SDRV_RTC_MODE,
} sdrv_power_mode_e;

/**
 * @brief Definition for DCDC work mode.
 */
typedef enum sdrv_power_dcdc_mode {
    DCDC_HP_MODE,  /**< High performance mode */
    DCDC_LP_MODE,  /**< Low performance mode */
} sdrv_power_dcdc_mode_e;

/**
 * @brief DCDC reg config, each reg has 8bit. REG0,7,8 are common config.
 * REG1~6 are separate config for HP_MODE and LP_MODE.
 */
typedef enum sdrv_power_dcdc_reg {
    DCDC_REG_0,
    DCDC_REG_1,
    DCDC_REG_2,
    DCDC_REG_3,
    DCDC_REG_4,
    DCDC_REG_5,
    DCDC_REG_6,
    DCDC_REG_7,
    DCDC_REG_8,
} sdrv_power_dcdc_reg_e;

/**
 * @brief Definition for iram.
 */
typedef enum sdrv_iram {
    SDRV_IRAM1 = 0,
    SDRV_IRAM2,
    SDRV_IRAM3,
    SDRV_IRAM4,
} sdrv_iram_e;

/**
 * @brief Definition for core clock mode.
 */
typedef enum sdrv_clock_mode {
    SDRV_CLOCK_NORMAL,
    SDRV_CLOCK_MEDIUM,
    SDRV_CLOCK_LOW,
} sdrv_clock_mode_e;

/**
 * @brief Definition for soc internal analog part power control.
 */
typedef struct sdrv_power_analog {
    bool vd_sf_pd;  /**< Saf domain Voltage Detect IP analog */
    bool por_sf_pd;  /**< Saf domain POR IP analog */
    bool rc24m_pd;  /**< RC24M analog */
    bool bgr_sf_pd; /**< BGR33_SF */
    bool bgr_ana_pd;  /**< BGR33_ANA */
    bool bgr_ldo_pd;  /**< BGR33_LDO */
    bool analog_cven33_pd;  /**< Analog IP CVEN33 power, include pt-sns, adc, acmp */
    bool vd_ap_pd;  /**< AP domain Voltage Detect IP analog */
    bool por_ap_pd;  /**< AP domain POR IP analog */
    bool bgr_ap_pd; /**< BGR33_AP */
    bool bgr_disp_pd;  /**< BGR33_DISP */

    bool bgr_sf_auto_en;  /**< Enable BGR33_SF auto power down in hibernate mode with rc_24m_disable config */
    bool bgr_ldo_auto_en;  /**< Enable BGR33_LDO auto power down in hibernate mode with PMU pwron2 config */
} sdrv_power_analog_t;

/**
 * @brief Definition for sleep mode config.
 */
typedef struct sdrv_sleep_mode_config {
    sdrv_wakeup_src_e *wakeup_srcs;  /**< wakeup ip list */
    uint32_t sf_module_reset_mask;   /**< saf domain module not reset bitmap
                                          OR'ed value in macro defined SDRV_SF_MODULE_xxx
                                          config this if you want specific module not be reset after wakeup */
    uint32_t ap_module_reset_mask;   /**< ap domain module not reset bitmap
                                          OR'ed value in macro defined SDRV_AP_MODULE_xxx
                                          config this if you want specific module not be reset after wakeup */
    uint32_t pll_pd_mask;            /**< pll not power down bitmap
                                          OR'ed value in macro defined SDRV_PLLxxx
                                          config this if you want specific PLL work in sleep mode */
    bool xtal_24m_disable;           /**< whether xtal 24m disable in sleep mode */
} sdrv_sleep_mode_config_t;

/**
 * @brief Definition for hibernate mode config.
 */
typedef struct sdrv_hibernate_mode_config {
    sdrv_wakeup_src_e *wakeup_srcs;  /**< wakeup ip list */
    uint32_t sf_module_reset_mask;   /**< saf domain module not reset bitmap
                                          OR'ed value in macro defined SDRV_SF_MODULE_xxx
                                          config this if you want specific module not be reset after wakeup */
    uint32_t ap_module_reset_mask;   /**< ap domain module not reset bitmap
                                          OR'ed value in macro defined SDRV_AP_MODULE_xxx
                                          config this if you want specific module not be reset after wakeup */
    uint32_t pll_pd_mask;            /**< pll not power down bitmap
                                          OR'ed value in macro defined SDRV_PLLxxx
                                          config this if you want specific PLL work in sleep mode */
    uint32_t core_pd_mask;           /**< core not power down bitmap
                                          OR'ed value in macro pre-defined, for example, if sf keep power, you should set bit SDRV_SF_PD_MASK */
    uint32_t iram_pd_mask;           /**< iram not power down bitmap
                                          OR'ed value in macro defined SDRV_IRAMxxx
                                          if iram is power down, you must reinit it after wakeup */
    bool xtal_24m_disable;           /**< whether xtal 24m disable in hibernate mode */
    bool rc_24m_disable;             /**< whether rc 24m disable in hibernate mode */
    bool ap_domain_powered;          /**< whether AP domain keep power supply in hibernate mode */
    uint32_t regulator_pd_mask;      /**< external/internal regulator not power down bitmap
                                          OR'ed value in macro defined SDRV_PWR_xxx */

    sdrv_power_analog_t analog_power;  /**< define analog power on or off in hibernate */
} sdrv_hibernate_mode_config_t;

/**
 * @brief Initialize clock/reset/power config under sleep and hibernate mode.
 *
 * This function will mask all interrupts and gate all xcg clock expect for wakeup modules. It will reset all
 * modules expect that need wakeup and user defined not reset in low power mode. According to user configration,
 * config smc sleep/hibernate mode core power switch and pmu pwr ctrl/pwr on status.
 *
 * @param [in] sleep_cfg config for sleep mode
 * @param [in] hib_cfg config for hibernate mode
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_power_init(sdrv_sleep_mode_config_t *sleep_cfg,
                         sdrv_hibernate_mode_config_t *hib_cfg);

/**
 * @brief Set soc to specific power mode.
 *
 * This function config SMC enable wfi detect and excute wfi instrcut. After all core enter wfi, it will start hardware
 * operation. Before enter sleep, software config all iram to retention mode. Before enter hibernate, software power down
 * iram as pre-defined, and power down analog part. Then use SVC interrupt to save all core registers. After wakeup from
 * sleep/hibernate, it will return from this call. Iram and analog will be powered again. If call to enter RTC mode, it
 * will power down at once.
 *
 * @param [in] mode mode defined in sdrv_power_mode_e.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_power_set_mode(sdrv_power_mode_e mode);

/**
 * @brief Set clock mode for core and bus.
 *
 * This function config clock mode for core and bus. Normal mode use default clock settings, medium mode reduce core and bus
 * frequency to half, low mode set core and bus frequency to 24MHz.
 *
 * @param [in] mode clock mode defined in sdrv_clock_mode_e.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_power_clock_set_mode(sdrv_clock_mode_e mode);

/**
 * @brief Reinit specific iram.
 *
 * This function reinit specific iram. If iram is power down in hibernate, after wakeup, you need call this function before
 * use it.
 *
 * @param [in] iram iram defined in sdrv_iram_e.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_power_iram_reinit(sdrv_iram_e iram);

/**
 * @brief Set rom disable.
 *
 * When set rom disable, ROM code read and register access are disable until POR reset.
 *
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_power_rom_disable(void);

/**
 * @brief USB phy power down.
 *
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_power_usb_power_down(void);

/**
 * @brief Config DCDC REG0~8.
 *
 * @param [in] dcdc_base DCDC controller base.
 * @param [in] mode high or low performance mode.
 * @param [in] dcdc_reg DCDC_REG_0 - DCDC_REG_8.
 * @param [in] cfg_val config value.
 * @return SDRV_STATUS_OK or error code.
 */
status_t sdrv_power_dcdc_config(paddr_t dcdc_base, sdrv_power_dcdc_mode_e mode,
                                sdrv_power_dcdc_reg_e dcdc_reg, uint8_t cfg_val);

#endif /* SDRV_POWER_H_ */
