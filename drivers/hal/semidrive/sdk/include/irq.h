/**
 * @file irq.h
 * @brief Interrupt interface hearder file.
 *
 * @copyright (c) 2022 Semidrive Semiconductor.
 * All rights reserved.
 *
 * Revision History:
 * -----------------
 */

#ifndef INCLUDE_DRV_IRQ_H
#define INCLUDE_DRV_IRQ_H

#include <types.h>
#include <compiler.h>

#include "armv7-r/irq.h"

/* trigger mode */
#define TRIGGER_MODE_LEVEL         0
#define TRIGGER_MODE_EDGE          1

#ifndef ASSEMBLY

__BEGIN_CDECLS

/* irq callback type */
typedef int (*irq_handler)(uint32_t irq, void *arg);

/*
 * irq initialize.
 *
 * @base   irq base
 * @intr_num  irq number
 */
#if CONFIG_IRQ
void irq_initialize(uint32_t base, uint32_t intr_num);
#else
static inline void irq_initialize(uint32_t base, uint32_t intr_num)
{

}
#endif

/*
 * irq attach.
 *
 * @irq     irq id.
 * @handler irq callback handler.
 * @arg     irq callback arg.
 * @return  result.
 */
#if CONFIG_IRQ
int irq_attach(uint32_t irq, irq_handler handler, void *arg);
#else
static inline int irq_attach(uint32_t irq, irq_handler handler, void *arg)
{
    return -1;
}
#endif

/*
 * irq detach.
 *
 * @irq     irq id.
 * @return  result.
 */
#if CONFIG_IRQ
int irq_detach(uint32_t irq);
#else
static inline int irq_detach(uint32_t irq)
{
    return -1;
}
#endif

/*
 * interrupt dispatch.
 *
 * @return  irq dispatch result.
 */
#if CONFIG_IRQ
int irq_dispatch(void);
#else
static inline int irq_dispatch(void)
{
    return -1;
}
#endif

/*
 * irq enable.
 *
 * @irq   irq number.
 */
#if CONFIG_IRQ
void irq_enable(uint32_t irq);
#else
static inline void irq_enable(uint32_t irq)
{

}
#endif

/*
 * irq disable.
 *
 * @irq   irq number.
 */
#if CONFIG_IRQ
void irq_disable(uint32_t irq);
#else
static inline void irq_disable(uint32_t irq)
{

}
#endif

/*
 * interrupt set priority mask.
 *
 * @mask     int priority mask.
 * @return   old int priority mask.
 */
#if CONFIG_IRQ
uint32_t irq_mask(uint32_t mask);
#else
static inline uint32_t irq_mask(uint32_t mask)
{
    return ~0;
}
#endif

/*
 * interrupt unmask all int.
 */
#if CONFIG_IRQ
void irq_unmask(void);
#else
static inline void irq_unmask(void)
{

}
#endif

/*
 * set irq priority.
 *
 * @irq         irq number.
 * @priority    irq priority.
 */
#if CONFIG_IRQ
void irq_set_priority(uint32_t irq, uint32_t priority);
#else
static inline void irq_set_priority(uint32_t irq, uint32_t priority)
{

}
#endif

/*
 * get irq priority.
 *
 * @irq         irq number.
 * @return      irq priority.
 */
#if CONFIG_IRQ
int irq_get_priority(uint32_t irq);
#else
static inline int irq_get_priority(uint32_t irq)
{
    return 0xFF;
}
#endif

/*
 * interrupt get current int priority.
 *
 * @return      int priority.
 */
#if CONFIG_IRQ
uint32_t irq_get_current_priority(void);
#else
static inline uint32_t irq_get_current_priority(void)
{
    return 0xFF;
}
#endif

/*
 * irq save.
 *
 * @return  old irq state.
 */
#if CONFIG_IRQ
#define IRQ_SAVE                \
    irq_state_t irq_flags;      \
    irq_flags = arch_irq_save();
#else
#define IRQ_SAVE
#endif

/*
 * irq restore.
 *
 * @flags   old irq state.
 */
#if CONFIG_IRQ
#define IRQ_RESTORE             \
    arch_irq_restore(irq_flags);
#else
#define IRQ_RESTORE
#endif

__END_CDECLS

#endif

#endif
