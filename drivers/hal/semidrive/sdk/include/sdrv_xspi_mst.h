/**
 * @file sdrv_xspi_mst.h
 * @brief SemiDrive common XSPI master driver header file.
 *
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef _SDRV_XSPI_MST_H_
#define _SDRV_XSPI_MST_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <debug.h>
#include <lib/list.h>
#include <stdlib.h>
#include <types.h>

#include <sdrv_ckgen.h>
#include <sdrv_common.h>
/**
 * @brief xspi master status error code.
 */
enum sdrv_xspi_master_error {
    SSDRV_STATUS_GROUP_XSPI_MASTER_LOCKED_ERR = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_XSPI, 1), /**< xspi master lock error */
    SSDRV_STATUS_GROUP_XSPI_MASTER_CHECK_BIT_ERR = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_XSPI, 2), /**< xspi master check bit error */
    SSDRV_STATUS_GROUP_XSPI_MASTER_MODE_CTRL_BUSY_ERR = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_XSPI, 3), /**< xspi master ctrl mode busy */
    SSDRV_STATUS_GROUP_XSPI_MASTER_CTRL_BUSY_ERR = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_XSPI, 4), /**< xspi master ctrl busy */
    SSDRV_STATUS_GROUP_XSPI_MASTER_SET_FLAG_ATTR_ERR = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_XSPI, 5), /**< xspi master set flag error */
    SSDRV_STATUS_GROUP_XSPI_MASTER_GET_FLAG_ATTR_ERR = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_XSPI, 6), /**< xspi master get flag error */
};

/**
 * @brief xspi master set attr.
 */
enum sdrv_xspi_mst_ioctl_flag {
    XSPI_MST_FLAG_ADDR_OFFSET,  /**< xspi global address offset configuration */
    XSPI_MST_FLAG_SLV_ERR_RESP, /**< xspi slv error respond enable */
    XSPI_MST_FLAG_CRC_EN,       /**< xspi master mode crc enable */
    XSPI_MST_FLAG_DUMMY,        /**< xspi master dummy cycle */
    XSPI_MST_FLAG_RATE,         /**< xspi master data rate */
    XSPI_MST_FLAG_LINE,         /**< xspi master line number */
    XSPI_MST_FLAG_WAIT_TIME,    /**< xspi master wait time */
    XSPI_MST_FLAG_TIMEOUT,      /**< xspi master mode timeout config register */
    XSPI_MST_FLAG_INT_ST,       /**< xspi master mode interrupt status */
    XSPI_MST_FLAG_INT_EN,       /**< xspi master mode interrupt enable */
    XSPI_MST_MS_INT_ST,         /**< xspi master MS mode interrupt status */
    XSPI_MST_FLAG_MAX
};

enum sdrv_xspi_mst_line {
    XSPI_MST_1_LINE, /**< xspi master line 00: 1-line */
    XSPI_MST_2_LINE, /**< xspi master line 01: 2-line */
    XSPI_MST_4_LINE, /**< xspi master line 10: 4-line */
    XSPI_MST_8_LINE  /**< xspi master line 11: 8-line */
};

struct sdrv_xspi_mst_addr_offset_conf {
    bool enable;
    uint32_t addr_offset; /**< xspi global address offset */
};

/**
 * @brief sdrv_xspi_mst master configurations.
 */
struct sdrv_xspi_mst_conf {
    paddr_t base; /**< xspi master register base address */
    uint16_t irq; /**< xspi master irq number */

    uint8_t cs;   /**< chip select */
    struct sdrv_xspi_mst_addr_offset_conf offset;
    bool crc_en;   /**< xspi master crc enable */
    bool slv_err_resp_en;   /**< xspi master slv_err_resp enable */
    uint8_t dummy; /**< xspi master dummy cycle */
    bool is_ddr;   /**< xspi master use ddr or sdr mode */
    enum sdrv_xspi_mst_line line;   /**< xspi master use line num */
    bool is_training;   /**< xspi master support training */
    paddr_t port_base;  /**< xspi master port base address*/
};

/**
 * @brief master master
 */
struct sdrv_xspi_mst {
    paddr_t base; /**< xspi master register base address */
    uint16_t irq; /**< xspi master irq number */
};

/**
 * @brief xspi master master init.
 *
 * @param[in] xspi master dev
 * @param[in] xspi master config
 * @return int
 * @retval  0: success
 * @retval -1: fail
 */
status_t sdrv_xspi_mst_init(struct sdrv_xspi_mst *master,
                  const struct sdrv_xspi_mst_conf *conf);

/**
 * @brief xspi master set attr.
 * @details This function used for xspi master set attribute, and detailed flag reference structure definitions.
 * @param[in] xspi master dev
 * @param[in] xspi master ioctl flag
 * @param[in] xspi master buf
 * @return int
 * @retval  0: success
 * @retval -1: fail
 */
status_t sdrv_xspi_mst_set_attr(struct sdrv_xspi_mst *master,
                      enum sdrv_xspi_mst_ioctl_flag flag, const void *buf);

/**
 * @brief xspi master get attr.
 * @details This function used for xspi master get attribute, and detailed flag reference structure definitions.
 * @param[in] xspi master dev
 * @param[in] xspi master ioctl flag
 * @param[out] xspi master buf
 * @return int
 * @retval  0: success
 * @retval -1: fail
 */
status_t sdrv_xspi_mst_get_attr(struct sdrv_xspi_mst *master,
                      enum sdrv_xspi_mst_ioctl_flag flag, void *buf);

/**
 * @brief xspi master deinit.
 *
 * @param[in] xspi master dev
 * @return int
 * @retval  0: success
 * @retval -1: fail
 */
status_t sdrv_xspi_mst_deinit(struct sdrv_xspi_mst *master);

#ifdef __cplusplus
}
#endif

#endif

