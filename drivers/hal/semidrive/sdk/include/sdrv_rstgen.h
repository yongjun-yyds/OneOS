/**
 * @file sdrv_rstgen.h
 * @brief SemiDrive Reset driver header file.
 *
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */
#ifndef SDRV_RSTGEN_H_
#define SDRV_RSTGEN_H_

#include <types.h>
#include <sdrv_common.h>

/* Define the type of rstgen */
#define SDRV_RSTGEN_CORE     1
#define SDRV_RSTGEN_LATENT   2
#define SDRV_RSTGEN_MISSION  3
#define SDRV_RSTGEN_MODULE   4
#define SDRV_RSTGEN_IST      5
#define SDRV_RSTGEN_DEBUG    6

#define SDRV_RSTGEN_TYPE_SHIFT  24

#define SDRV_RSTGEN_SIG_ID(type, idx) \
    ((uint32_t)((((uint32_t)(type)) << ((uint32_t)SDRV_RSTGEN_TYPE_SHIFT)) | ((uint32_t)(idx))))

#define SDRV_RSTGEN_TYPE(id)   ((uint32_t)(((uint32_t)(id)) >> ((uint32_t)SDRV_RSTGEN_TYPE_SHIFT)))
#define SDRV_RSTGEN_INDEX(id)  ((uint32_t)(((uint32_t)(id)) & BIT_MASK(SDRV_RSTGEN_TYPE_SHIFT)))

#define SDRV_RSTGEN_GENERAL_REG_NUM  8

/**
 * @brief RESET status error code.
 */
enum sdrv_reset_error
{
    SDRV_RESET_STATUS_SIGNAL_ASSERT = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_RESET, 1),          /* RESET singal assert. */
    SDRV_RESET_STATUS_LOCK = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_RESET, 2),            /* RESET lock error. */
    SDRV_RESET_STATUS_TIMEOUT = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_RESET, 3),         /* RESET timeout error. */
    SDRV_RESET_STATUS_WRONG_SIGNAL = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_RESET, 4),       /* RESET wrong signal. */
};

/* Define lowpower mode */
enum reset_lowpower_mode {
    RESET_LP_HIB   = 0,
    RESET_LP_SLEEP = 1,
};

typedef enum reset_wdt_id {
    RESET_WDT1,  /* Trigger global reset or SF core reset */
    RESET_WDT2,  /* Trigger global reset or SF core reset */
    RESET_WDT3,  /* Trigger global reset or SP0/SP1 core reset */
    RESET_WDT4,  /* Trigger global reset or SP0/SP1 core reset */
    RESET_WDT5,  /* Trigger global reset or SX0/SX1 core reset */
    RESET_WDT6,  /* Trigger global reset or SX0/SX1 core reset */
} reset_wdt_id_e;

typedef enum reset_core_id {
    RESET_CORE_SF = 1,
    RESET_CORE_SP0,
    RESET_CORE_SP1,
    RESET_CORE_SX0,
    RESET_CORE_SX1,
} reset_core_id_e;

/**
 * @brief SDRV rstgen controller.
*/
typedef struct sdrv_rstgen {
    paddr_t        base;
} sdrv_rstgen_t;

/**
 * @brief SDRV rstgen signal.
*/
typedef struct sdrv_rstgen_sig {
    sdrv_rstgen_t    *rst_ctl;
    uint32_t          id;
    bool              need_clr_rst;
} sdrv_rstgen_sig_t;

/**
 * @brief SDRV rstgen general register.
*/
typedef struct sdrv_rstgen_general_reg {
    sdrv_rstgen_t    *rst_ctl;
    uint32_t          id;  /* id 1 ~ 7 */
} sdrv_rstgen_general_reg_t;

/**
 * @brief SDRV rstgen global reset controller.
*/
typedef struct sdrv_rstgen_glb_ctl {
    sdrv_rstgen_t    *rst_sf_ctl;
    sdrv_rstgen_t    *rst_ap_ctl;
} sdrv_rstgen_glb_t;

/**
 * @brief semidrive recovery device.
*/
typedef struct sdrv_recovery_btm {
    uint32_t btm_num;  /* btm numbers */
    uint32_t btm_base[];  /* btm base list */
} sdrv_recovery_btm_t;

typedef struct sdrv_recovery_etimer {
    uint32_t etimer_num;  /* etimer numbers */
    uint32_t etimer_base[];  /* etimer base list */
} sdrv_recovery_etimer_t;

typedef struct sdrv_recovery_epwm {
    uint32_t epwm_num;  /* epwm numbers */
    uint32_t epwm_base[];  /* epwm base list */
} sdrv_recovery_epwm_t;

/* semidrive recovery latent module */
typedef struct sdrv_recovery_module {
    sdrv_recovery_btm_t const *btm_list;
    sdrv_recovery_etimer_t const *etimer_list;
    sdrv_recovery_epwm_t const *epwm_list;
} sdrv_recovery_module_t;

/**
 * @brief Assert a reset signal.
 *
 * The reset singal will stay asserted until reset_deassert() is called.
 *
 * @param[in] rst_sig Reset signal.
 * @return 0 if OK, or a negative error code.
 */
status_t sdrv_rstgen_assert(sdrv_rstgen_sig_t *rst_sig);

/**
 * @brief Deassert a reset signal.
 *
 * Release a reset signal.
 *
 * @param[in] rst_sig Reset signal.
 * @return 0 if OK, or a negative error code.
 */
status_t sdrv_rstgen_deassert(sdrv_rstgen_sig_t *rst_sig);

/**
 * @brief Reset a reset signal.
 *
 * Assert a reset signal, and then deassert it automatically.
 *
 * @param[in] rst_sig Reset signal.
 * @return 0 if OK, or a negative error code.
 */
status_t sdrv_rstgen_reset(sdrv_rstgen_sig_t *rst_sig);

/**
 * @brief Global reset.
 *
 * Global reset SF/AP domain
 *
 * @param[in] rst_glb_ctl Global reset controller.
 * @return 0 if OK, or a negative error code.
 */
status_t sdrv_rstgen_global_reset(sdrv_rstgen_glb_t *rst_glb_ctl);

/**
 * @brief Check reset signal status.
 *
 * @param[in] rst_sig Reset signal.
 * @return 0 if deasserted, or a negative error code.
 */
status_t sdrv_rstgen_status(sdrv_rstgen_sig_t *rst_sig);

/**
 * @brief Check global reset status.
 *
 * Global reset status for all the time,
 * including each global reset status before clear or power off.
 * If multiple reset source comes neatly the same time,
 * only the first one will be recorded.
 *
 * @param[in] rst_glb_ctl Global reset controller.
 * @return SF and AP domain global reset status.
 * bit[31:25] for rstgen AP:
 *         bit[31] rstgen ap software global reset
 *         bit[30] reserved
 *         bit[29] efusec security violation
 *         bit[28] system panic(vdc function irq)
 *         bit[27] sem1/sem2 error
 *         bit[26] reserved
 *         bit[25] PT sensor interrupt
 * bit[24:0] for rstgen SF:
 *         bit[24] wdt6 int reset request
 *         bit[23] wdt6 int reset request
 *         bit[22] wdt4 int reset request
 *         bit[21] wdt4 int reset request
 *         bit[20] wdt2 int reset request
 *         bit[19] reserved
 *         bit[18] wdt5 int reset request
 *         bit[17] wdt5 int reset request
 *         bit[16] wdt3 int reset request
 *         bit[15] wdt3 int reset request
 *         bit[14] wdt1 int reset request
 *         bit[13] reserved
 *         bit[12] ist done fail
 *         bit[11] rstgen sf software global reset
 *         bit[10] reserved
 *         bit[9] wdt2 int reset request (Only for E3104/E3106/E3205/E3206)
 *         bit[8] wdt1 int reset request (Only for E3104/E3106/E3205/E3206)
 *         bit[7] ist done fail (Only for E3104/E3106/E3205/E3206)
 *         bit[6] rstgen sf software global reset (Only for E3104/E3106/E3205/E3206)
 *         bit[5] rstgen ap cold reset request
 *         bit[4] efusec security violation
 *         bit[3] system panic(vdc function irq)
 *         bit[2] sem1/sem2 error
 *         bit[1] reserved
 *         bit[0] PT sensor interrupt
 * Especially, bit[31:10] are invalid for E3104/E3106/E3205/E3206.
 */
uint32_t sdrv_rstgen_global_status(sdrv_rstgen_glb_t *rst_glb_ctl);

/**
 * @brief clear global reset status.
 *
 * Clear global reset status, otherwise the status will keep until power off.
 *
 * @param[in] rst_glb_ctl Global reset controller.
 */
status_t sdrv_rstgen_global_status_clear(sdrv_rstgen_glb_t *rst_glb_ctl);

/**
 * @brief Check current global reset status.
 *
 * Global reset status only for the last time.
 * If multiple reset source comes neatly the same time,
 * only the first one will be recorded.
 *
 * @param[in] rst_glb_ctl Global reset controller.
 * @return SF and AP domain current global reset status.
 * bit[31:25] for rstgen AP:
 *         bit[31] rstgen ap software global reset
 *         bit[30] reserved
 *         bit[29] efusec security violation
 *         bit[28] system panic(vdc function irq)
 *         bit[27] sem1/sem2 error
 *         bit[26] reserved
 *         bit[25] PT sensor interrupt
 * bit[24:0] for rstgen SF:
 *         bit[24] wdt6 int reset request
 *         bit[23] wdt6 int reset request
 *         bit[22] wdt4 int reset request
 *         bit[21] wdt4 int reset request
 *         bit[20] wdt2 int reset request
 *         bit[19] reserved
 *         bit[18] wdt5 int reset request
 *         bit[17] wdt5 int reset request
 *         bit[16] wdt3 int reset request
 *         bit[15] wdt3 int reset request
 *         bit[14] wdt1 int reset request
 *         bit[13] reserved
 *         bit[12] ist done fail
 *         bit[11] rstgen sf software global reset
 *         bit[10] reserved
 *         bit[9] wdt2 int reset request (Only for E3104/E3106/E3205/E3206)
 *         bit[8] wdt1 int reset request (Only for E3104/E3106/E3205/E3206)
 *         bit[7] ist done fail (Only for E3104/E3106/E3205/E3206)
 *         bit[6] rstgen sf software global reset (Only for E3104/E3106/E3205/E3206)
 *         bit[5] rstgen ap cold reset request
 *         bit[4] efusec security violation
 *         bit[3] system panic(vdc function irq)
 *         bit[2] sem1/sem2 error
 *         bit[1] reserved
 *         bit[0] PT sensor interrupt
 * Especially, bit[31:10] are invalid for E3104/E3106/E3205/E3206.
 */
uint32_t sdrv_rstgen_current_global_status(sdrv_rstgen_glb_t *rst_glb_ctl);

/**
 * @brief Read reset general reg.
 *
 * The data saved in general regs will be not lost until SOC power down.
 *
 * @param[in] rst_gen_reg reset general reg.
 * @return general reg value.
 */
uint32_t sdrv_rstgen_read_general(sdrv_rstgen_general_reg_t *rst_gen_reg);

/**
 * @brief Write reset general reg.
 *
 * @param[in] rst_gen_reg reset general reg.
 * @param[in] val write value.
 * @return 0 if success,
 *         or a negative error code.
 */
status_t sdrv_rstgen_write_general(sdrv_rstgen_general_reg_t *rst_gen_reg,
                                  uint32_t val);

/**
 * @brief Write reset general reg bit.
 *
 * @param[in] rst_gen_reg reset general reg.
 * @param[in] start write start bit.
 * @param[in] width write bit width.
 * @param[in] val write value.
 * @return 0 if success,
 *         or a negative error code.
 */
status_t sdrv_rstgen_write_general_bit(sdrv_rstgen_general_reg_t *rst_gen_reg,
                                      uint8_t start, uint8_t width, uint32_t val);

/**
 * @brief Config reset signal assert/deassert in lowpower mode.
 *
 * @param[in] rst_sig Reset signal.
 * @param[in] mode lowpower mode
 * @param[in] val 0:assert, 1:deassert
 *
 * @return 0 if OK, or a negative error code.
 */
status_t sdrv_rstgen_lowpower_set(sdrv_rstgen_sig_t *rst_sig,
                                 enum reset_lowpower_mode mode, uint32_t val);

/**
 * @brief Config wdt cause global reset enable
 *
 * @param [in] wdt wdt id
 * @param [in] enable true/false
 * @return 0 if success, or a negative error code.
 */
status_t sdrv_rstgen_wdt_reset_enable(reset_wdt_id_e wdt, bool enable);

/**
 * @brief Config wdt cause single core reset enable
 *
 * Config watchdog timeout trigger core reset. Each core has two watchdog, WDT1 and WDT2 belongs to SF,
 * WDT3 and WDT4 belongs to SP0/SP1, WDT5 and WDT6 belongs to SX0/SX1. SP1 and SX1 can be reset alone, if
 * SP0 or SX0 be reset, SP1 or SX1 will reset followed.
 *
 * @param [in] wdt wdt id
 * @param [in] core core id
 * @param [in] enable true/false
 * @return 0 if success, or a negative error code.
 */
status_t sdrv_rstgen_wdt_core_reset_enable(reset_wdt_id_e wdt, reset_core_id_e core, bool enable);

/**
 * @brief sdrv recovery latent module.
 *
 * Some latent modules (btm/etimer/epwm etc.) will be not reset when core is reset,
 * so these modules should be recovery when core restart.
 *
 * @param[in] recovery_module recovery latent module list
 * @return 0 if success, or a negative error code.
 */
status_t sdrv_recovery_module(sdrv_recovery_module_t* recovery_module);

#endif /* SDRV_RSTGEN_RSTGEN_H_ */
