/**
 * @file sdrv_fuse.h
 * @brief SemiDrive eFuse driver header file.
 *
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 * All rights reserved.
 */

#ifndef SDRV_FUSE_H_
#define SDRV_FUSE_H_

#include <compiler.h>
#include <types.h>
#include "sdrv_common.h"

/**
 * @brief Fuse lock types.
 */
typedef enum {
    /**< Prohibit programming fuse bank */
    PLOCK = 1,
    /**< Prohibit reading from the fuse bank, the corresponding shadow
     * registers of the bank, and its output to SEIP.
     */
    RLOCK = 2,
    /**< Prohibit overriding shadow register for the bank. */
    OLOCK = 4,
    /**< Same as RLock, but allow output to SEIP */
    HLOCK = 8,
} fuse_lock_bits_e;

/**
 * @brief Fuse lock bank.
 */
typedef enum {
    /* fuse lock bank from bank1 to bank32 */
    FUSE_LOCK_BANK1    = 0,
    FUSE_LOCK_BANK2    = 1,
    FUSE_LOCK_BANK3    = 2,
    FUSE_LOCK_BANK4    = 3,
    FUSE_LOCK_BANK5    = 4,
    FUSE_LOCK_BANK6    = 5,
    FUSE_LOCK_BANK7    = 6,
    FUSE_LOCK_BANK8    = 7,
    FUSE_LOCK_BANK9    = 8,
    FUSE_LOCK_BANK10   = 9,
    FUSE_LOCK_BANK11   = 10,
    FUSE_LOCK_BANK12   = 11,
    FUSE_LOCK_BANK13   = 12,
    FUSE_LOCK_BANK14   = 13,
    FUSE_LOCK_BANK15   = 14,
    FUSE_LOCK_BANK16   = 15,
    FUSE_LOCK_BANK17   = 16,
    FUSE_LOCK_BANK18   = 17,
    FUSE_LOCK_BANK19   = 18,
    FUSE_LOCK_BANK20   = 19,
    FUSE_LOCK_BANK21   = 20,
    FUSE_LOCK_BANK22   = 21,
    FUSE_LOCK_BANK23   = 22,
    FUSE_LOCK_BANK24   = 23,
    FUSE_LOCK_BANK25   = 24,
    FUSE_LOCK_BANK26   = 25,
    FUSE_LOCK_BANK27   = 26,
    FUSE_LOCK_BANK28   = 27,
    FUSE_LOCK_BANK29   = 28,
    FUSE_LOCK_BANK30   = 29,
    FUSE_LOCK_BANK31   = 30,
    FUSE_LOCK_BANK32   = 31,
} fuse_lock_bank_e;

/**
 * @brief Fuse status return code
 */
enum
{
    SDRV_FUSE_CTRL_ERROR_OR_BUSY = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_FUSE, 0),           /* Fuse status for fuse_ctrl register is error or busy. */
    SDRV_FUSE_CTRL_ERROR = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_FUSE, 1),                   /* Fuse status for fuse_ctrl register is error. */
};

/**
 * @brief Read fuse value from shadow register.
 *
 * This function load fuse value from shadow registers, which are filled
 * automatically by hardware after chip reset.
 *
 * @param index Fuse index
 * @return Fuse value
 */
uint32_t sdrv_fuse_read(uint32_t index);

/**
 * @brief Load fuse value by software.
 *
 * This function manually triggers fuse reading operation to specified
 * index, and store fuse value in user buffer.
 *
 * @param index Fuse index
 * @param data Buffer to place fuse data
 * @return Return SDRV_STATUS_OK or error code
 */
status_t sdrv_fuse_sense(uint32_t index, uint32_t *data);

/**
 * @brief Program fuse.
 *
 * This function program specified value into fuse area. Redundant bits
 * are programmed as well, for fuse indices protected by redundancy.
 *
 * @param index Fuse index
 * @param v Fuse value
 * @return Return SDRV_STATUS_OK or error code
 */
status_t sdrv_fuse_program(uint32_t index, uint32_t v);

/**
 * @brief Program raw fuse value.
 *
 * This function program specified value into fuse area, without redundancy
 * protection bits.
 *
 * @param index Fuse index
 * @param val Fuse value
 * @param ecc True to enable ECC protection, only for non-redundancy bits
 * @return Return SDRV_STATUS_OK or error code
 */
status_t sdrv_fuse_program_raw(uint32_t index, uint32_t val, bool ecc);

/**
 * @brief Reload fuse into shadow registers.
 *
 * @return Return SDRV_STATUS_OK or error code
 */
status_t sdrv_fuse_reload(void);

/**
 * @brief Set stick bit for specified fuse Id.
 *
 * @param id fuse id
 * @return SDRV_STATUS_OK represents success
 */
status_t sdrv_fuse_set_sticky_bit(uint32_t id);

/**
 * @brief Lock fuse banks.
 *
 * @param bank The bank to lock.
 * @param lock_bits Lock type bitmask.
 * @return SDRV_STATUS_OK represents success
 */
status_t sdrv_fuse_lock_bank(fuse_lock_bank_e bank, fuse_lock_bits_e lock_bits);

/**
 * @brief Get fuse bank lock type.
 *
 * @param bank The bank to get lock type.
 * @return Lock type bitmask.
 */
uint32_t sdrv_fuse_get_bank_lock(fuse_lock_bank_e bank);

/**
 * @brief Update fuse control timing.
 *
 * @param freq_mhz APB clock frequency. Default hardware setting is
 *        based on 150MHz APB clock.
 * @return SDRV_STATUS_OK represents success
 */
status_t sdrv_fuse_ctl_cfg_timing(uint32_t freq_mhz);

/**
 * @brief Trigger software violation.
 *
 * @param msk Lock type bitmask.
 * @return SDRV_STATUS_OK represents success
 */
status_t sdrv_fuse_ctl_trigger_sw_vio(fuse_lock_bits_e msk);

#endif /* SDRV_FUSE_H_ */
