/**
 * @file sdrv_sent.h
 * @brief SemiDrive Sent driver header file.
 *        Sent driver supports the following functions:
 *         - Initialization and Configuration
 *         - Data receive
 *         - Fast channel mode
 *         - Interrupts and flags management
 *
 *        Note:
 *         - Fast channel read mode have 3 different format:
 *           - Standard format(driver use)
 *           - Single secure sensor message format
 *           - High Speed Message Format
 *           User should parse the data field further according to the format,
 *           if the frame is not a standard format.
 *         - Fifo water mark level
 *           User should process the remaining data in the fifo, if the total
 *           amount of data received does not align with the fifo water mark
 *           level.
 *
 * @copyright Copyright (c) 2020  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef SDRV_SENT_DRV_H_
#define SDRV_SENT_DRV_H_

#include <compiler.h>
#include <sdrv_common.h>
#include <types.h>

/* Single Secure Sensor Message Format include 6 data nibbles(24-bit). */
#define SENT_SECURE_SENSOR_DATA_NIBBLE 6U
/* High Speed Message Format include 4 data nibbles(12-bit). */
#define SENT_HIGH_SPEED_DATA_NIBBLE 4U

/** @brief Sdrv sent channel id enum. */
enum sdrv_sent_chanid {
    SDRV_SENT_HW_CPT_A = 0U, /**< Sdrv sent hardware channel A. */
    SDRV_SENT_HW_CPT_B = 1U, /**< Sdrv sent hardware channel B. */
    SDRV_SENT_HW_CPT_C = 2U, /**< Sdrv sent hardware channel C. */
    SDRV_SENT_HW_CPT_D = 3U, /**< Sdrv sent hardware channel D. */
    SDRV_SENT_CHAN_NUM = 4U, /**< Sdrv sent hardware channel number. */
};

/* Sdrv sent struct. */
typedef struct sdrv_sent sdrv_sent_t;
typedef enum sdrv_sent_chanid sdrv_sent_chanid_e;
/* Recv frame over callback function. */
typedef void (*sdrv_sent_recv_cb_t)(sdrv_sent_t *ctrl,
                                    sdrv_sent_chanid_e sent_chan_id,
                                    void *userdata);

/** @brief sdrv sent clock source enum. */
typedef enum {
    SDRV_SENT_HF_CLK = 0U, /**< High frequency(HF) clock, up to 400MHz. */
    SDRV_SENT_AHF_CLK =
        1U, /**< Alternative High frequency(AHF) clock, up to 400MHz. */
    SDRV_SENT_EXT_CLK = 2U, /**< External clock, up to 24MHz. */
    SDRV_SENT_LP_CLK =
        3U, /**< Low power(LP) clock, typically from low speed om chip RCOSC. */
} sdrv_sent_clk_src_e;

/** @brief Sdrv sent data width enum. */
typedef enum {
    SENT_DATA_WIDTH_4_BIT = 1U,  /**< Sent frame data width is 4bits. */
    SENT_DATA_WIDTH_8_BIT = 2U,  /**< Sent frame data width is 8bits. */
    SENT_DATA_WIDTH_12_BIT = 3U, //**< Sent frame data width is 12bits. */
    SENT_DATA_WIDTH_16_BIT = 4U, /**< Sent frame data width is 16bits. */
    SENT_DATA_WIDTH_20_BIT = 5U, /**< Sent frame data width is 20bits. */
    SENT_DATA_WIDTH_24_BIT = 6U, /**< Sent frame data width is 24bits. */
} sdrv_sent_bitnum_e;

/** @brief Sdrv sent status enum. */
typedef enum {
    SENT_STATUS_UNINIT = 0U, /**< Sdrv sent module is uninitialized. */
    SENT_STATUS_INITED = 1U, /**< Sdrv sent module is initialized. */
} sdrv_sent_sta_e;

/** @brief Sdrv sent channel transmit format enum. */
typedef enum {
    SENT_STANDARD_FORMAT = 0U, /**< Sent use standard format. */
    SENT_SINGLE_SECURE_FORMAT =
        1U, /**< Sent use single secure sensor message format. */
    SENT_HIGH_SPEED_FORMAT = 2U, /**< Sent use high speed message format. */
} sdrv_sent_fastchan_e;

/** @brief Sdrv sent frame error code enum. */
typedef enum sdrv_frame_err {
    SENT_OK = 0U,                /**< Frame no error.*/
    SENT_SYNC_ERR = (1U << 0),   /**< Sync/Calibration pulse is not 56 ticks. */
    SENT_STATUS_ERR = (1U << 1), /**< Status signal error. */
    SENT_DATA_ERR = (1U << 2),   /**< Nibble data is bigger than 15. */
    SENT_CRC_ERR = (1U << 3),    /**< Nibble data crc is mismatched. */
    SENT_LOW_PULSE_ERR = (1U << 4),    /**< Nibble low pulse width error. */
    SENT_MSB_NOT_ZERO = (1U << 5),     /**< SENT_HIGH_SPEED_FORMAT Only. */
    SENT_INV_MSN_ERR = (1U << 6),      /**< SENT_SINGLE_SECURE_FORMAT Only. */
    SENT_CONUNTER_VAL_ERR = (1U << 7), /**< SENT_SINGLE_SECURE_FORMAT Only. */
    SENT_TICK_FREQ_ERR = (1U << 8),    /**< SENT_SINGLE_SECURE_FORMAT Only. */
    SENT_NOT_OK = (1U << 31),          /**< SENT_SINGLE_SECURE_FORMAT Only. */
} sdrv_frame_err_e;

/** @brief Sdrv sent frame status enum. */
typedef enum sent_frame_state {
    SENT_IDLE = 0U, /**< The sent is idle, waiting to receive the sync pulse. */
    SENT_SYNC = 1U, /**< The frame successfully receives the sync pulse and
                       waits to receive the status pulse.*/
    SENT_STATUS = 2U, /**< The frame successfully receives the status pulse and
                       waits to receive the data pulse. */
    SENT_DATA = 3U,   /**< The frame is receiving the data pulse. */
    SENT_CRC = 4U,    /**< The frame successfully receives all data pulse and
                       waits to receive the crc pulse.*/
    SENT_PAUSE = 5U,  /**< The frame successfully receives the crc pulse and
                       waits to receive the pause pulse. */
} sdrv_sent_frame_state_e;

/** @brief Sdrv sent frame status enum. */
typedef enum sent_chan_state {
    CHAN_STATUS_UNINIT = 0U,  /**< Sdrv sent channel is uninitialized. */
    CHAN_STATUS_INITED = 1U,  /**< Sdrv sent channel is initialized. */
    CHAN_STATUS_RUNNING = 2U, /**< Sdrv sent channel is running. */
    CHAN_STATUS_STOPPED = 3U, /**< Sdrv sent channel is stopped. */
} sent_chan_state_e;

/* Sent frame information. */
typedef struct sent_chan_frame {
    sdrv_sent_fastchan_e frame_format;          /**< The format of the frame. */
    volatile sdrv_sent_frame_state_e cur_state; /**< The state of frame. */
    volatile uint8_t rxframe[6];                /**< The receive buffer.*/
    uint8_t frame_dl;            /**< The length of data in frame. */
    volatile uint8_t rx_index;   /**< The current index of data.*/
    volatile uint8_t frame_sta;  /**< The value of the status field in frame. */
    volatile uint8_t frame_crc;  /**< The value of the crc field in frame. */
    volatile uint32_t frame_err; /**< The error type of frame. */
    volatile bool frame_valid;   /**< The frame is valid or not. */
} sent_chan_frame_t;

/** @brief Sdrv sent channel struct. */
typedef struct sdrv_sent_chan {
    sdrv_sent_chanid_e chan_id;          /**< The channel id of sent module. */
    volatile sent_chan_state_e chan_sta; /**< The state of channel. */

    sdrv_sent_recv_cb_t frameover_cb; /**< The callback function. */
    void *userdata; /**< The user's param of callback functrion.*/

    uint8_t lowpulse_width; /**< The width of low pulse. */
    bool pause_pulse;       /**< The frame has pause pulse or not. */
    uint32_t tickfreq;      /**< The frequency of tick.*/
    uint8_t fifowml;        /**< The dma fifo water level.
                             * User should process the remaining data in the
                             * fifo, if the total amount of data received does
                             * not align with the fifo water mark level.
                             */
    bool filter_en;         /**< Enable/Disable capture filter.*/
    uint8_t
        filter_bandwidth; /**< Filter bandwidth for negedge capture channel. */
    uint8_t
        sample_interval; /**< Sample interval for timer 1 ~ 256 clock cycles. */

    uint32_t expect_scaler;          /**< The value of expect scaler.*/
    volatile uint32_t actual_scaler; /**< The value of actual scaler. */

    sent_chan_frame_t frame_info; /**< The information of the frame. */
} sdrv_sent_chan_t;

/** @brief Sdrv sent module struct. */
struct sdrv_sent {
    uint32_t base; /**< The register base of sent module. */
    int32_t irq;   /**< The irq number of sent channel. */

    sdrv_sent_clk_src_e clk_sel; /**< The clock source. */
    uint16_t clk_div;            /**< The clock divider. */
    uint32_t clk_freq;           /**< The frequency of the clock. */

    volatile sdrv_sent_sta_e sent_sta; /**< The state of sent module. */
    uint8_t chan_num; /**< The number of channels in sent module. */
    sdrv_sent_chan_t
        chan[SDRV_SENT_CHAN_NUM]; /**< The entities of channel struct. */
};

/** @brief Sdrv sent channel config struct. */
typedef struct sdrv_sent_chan_config {
    sdrv_sent_chanid_e chan_id; /**< The channel id of sent module. */

    uint32_t tickfreq; /**< The frequency of tick, range : 11112 ~ 333334, match
                          tick 3us ~ 90us. */
    sdrv_sent_fastchan_e
        fast_chan; /**< The format of fast channel mode, default value is 0. */
    sdrv_sent_bitnum_e
        data_bitnum;        /**< The width of data in frame, range : 1 ~ 6U. */
    uint8_t lowpulse_width; /**< The width of low pulse, the unit is tick, range
                               : 5 ~ 12. */
    uint8_t fifowml;        /**< The dma fifo water level, range : 1 ~ 15U.
                             * User should process the remaining data in the
                             * fifo, if the total amount of data received does
                             * not align with the fifo water mark level.
                             */
    bool pause_pulse; /**< The frame  has pause pulse or not, default value
                                 is false. */
    bool filter_en;   /**< Enable/Disable capture filter.*/
    uint8_t
        filter_bandwidth; /**< Filter bandwidth for negedge capture channel. */
    uint8_t
        sample_interval; /**< Sample interval for timer 1 ~ 256 clock cycles,
                            the value of sample_interval is from 0 to 255.*/
} sdrv_sent_chan_config_t;

/** @brief Sdrv sent module config struct. */
typedef struct sdrv_sent_config {
    uint32_t base; /**< The register base of sent module. */
    uint32_t irq;  /**< The irq number of sent channel. */

    sdrv_sent_clk_src_e clk_sel; /**< The clock source. */
    uint16_t clk_div;            /**< The clock divider, range 0 ~ 65534. */
    uint32_t clk_freq;           /**< The frequency of the clock. */

    uint8_t
        chan_cfg_num; /**< The number of channels those will be initialized. */
    sdrv_sent_chan_config_t
        chan_cfg[SDRV_SENT_CHAN_NUM]; /**< The configurations of channel. */
} sdrv_sent_config_t;

/**
 * @brief Sdrv sent controller initialization.
 *
 * This fuction initializes sent(etimer) module and channel and controller
 * entity status.
 *
 * @param [in] ctrl Sdrv sent controller.
 * @param [in] cfg  Sdrv sent configurations.
 * @return  The result of initializing sent.
 * - SDRV_STATUS_INVALID_PARAM : The input parameter contains a null
 * pointer.
 * - SDRV_STATUS_OK : Sent is initialized success.
 */
status_t sdrv_sent_init(sdrv_sent_t *ctrl, sdrv_sent_config_t *cfg);

/**
 * @brief Sdrv sent controller deinitialization.
 *
 * This fuction deinitializes sent module.
 *
 * @param [in] ctrl Sdrv sent controller.
 * @return  The result of deinitializing sent.
 * - SDRV_STATUS_INVALID_PARAM : The input parameter contains a null pointer.
 * - SDRV_STATUS_OK : Sent is deinitialized success.
 */
status_t sdrv_sent_deinit(sdrv_sent_t *ctrl);

/**
 * @brief Sent set callback function
 *
 * @param[in] ctrl Sdrv sent controller.
 * @param[in] sent_chan_id Sent channel id
 * @param[in] callback Callback function
 * @param[in] userdata Callback user param
 * @return  The result of initializing sent.
 * - SDRV_STATUS_INVALID_PARAM : The input parameter contains a null pointer.
 * - SDRV_STATUS_OK : Set callback function success.
 */
status_t sdrv_sent_set_callback(sdrv_sent_t *ctrl,
                                sdrv_sent_chanid_e sdrv_sent_chanid,
                                sdrv_sent_recv_cb_t callback, void *userdata);

/**
 * @brief Sent starts to receive frames.
 *
 * Sent channel will receive frames after calling this function.
 * User could stop the sent channel by calling sdrv_sent_recv_stop().
 *
 * @param[in] ctrl Sent controller
 * @param[in] sent_chan_id Sent channel id
 * @return  The result of initializing sent.
 * - SDRV_STATUS_INVALID_PARAM : The input parameter contains a null pointer.
 * - SDRV_STATUS_OK : Start sent channel success.
 */
status_t sdrv_sent_recv_start(sdrv_sent_t *ctrl,
                              sdrv_sent_chanid_e sent_chan_id);

/**
 * @brief Sent stop recv frame
 *
 * Sent channel will receive frames after calling sdrv_sent_recv_start().
 * User could stop the sent channel by calling this function.
 *
 * @param[in] ctrl Sent controller
 * @param[in] sent_chan_id Sent channel id
 * @return  The result of initializing sent.
 * - SDRV_STATUS_INVALID_PARAM : The input parameter contains a null pointer.
 * - SDRV_STATUS_OK : Stop sent channel success.
 */
status_t sdrv_sent_recv_stop(sdrv_sent_t *ctrl,
                             sdrv_sent_chanid_e sent_chan_id);

/**
 * @brief Parse sent frame
 *
 * This function is used in the following two situations:
 * 1.Enter sent interrupt,
 * 2.The user actively calls function to parse the remaining data in fifo after
 * stopping channal.
 *
 * @param[in] ctrl Sent controller
 * @param[in] sent_chan_id Sent channel id
 * @return  The result of initializing sent.
 * - SDRV_STATUS_INVALID_PARAM : The input parameter contains a null pointer.
 * - SDRV_STATUS_OK : Parse sent frame success.
 */
status_t sdrv_sent_parse_msg(sdrv_sent_t *ctrl,
                             sdrv_sent_chanid_e sent_chan_id);

/**
 * @brief Get sent frame info
 *
 * @param[in] ctrl Sent controller
 * @param[in] sent_chan_id Sent channel id
 * @param[out] frame_err Frame error code
 * @param[out] frame Frame information
 * @return  The result of initializing sent.
 * - SDRV_STATUS_INVALID_PARAM : The input parameter contains a null pointer.
 * - SDRV_STATUS_FAIL : The state of channel is error or the frame is invalid.
 * - SDRV_STATUS_OK : Get sent frame success.
 */
status_t sdrv_sent_get_frame(sdrv_sent_t *ctrl, sdrv_sent_chanid_e sent_chan_id,
                             uint32_t *frame_err, sent_chan_frame_t *frame);
/**
 * @brief Sdrv sent interrupt handler.
 *
 * @param [in] irq The irq number of uartx
 * @param [in] ctrl Sdrv sent controller
 */
int sdrv_sent_irq_handle(uint32_t irq, void *ctrl);
#endif /* SDRV_SENT_H_ */
