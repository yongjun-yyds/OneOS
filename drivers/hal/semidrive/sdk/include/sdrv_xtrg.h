/**
 * @file sdrv_xtrg.h
 * @brief SemiDrive XTRG driver header file.
 *
 * @copyright Copyright (c) 2021  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef SDRV_XTRG_H
#define SDRV_XTRG_H

#include <compiler.h>
#include <reg.h>
#include <types.h>

#include "part.h"

#define FUNC_INT_STA (0x0)
#define FUNC_INT_STA_EN (0x4)
#define FUNC_INT_SIG_EN (0x8)
#define FUNC_INT_STA_SHIFT(int_id) (int_id)
#define FUNC_INT_STA_MASK(int_id) (0x1 << FUNC_INT_STA_SHIFT(int_id))

#define FUNC_ERR_INT_STA (0x10)
#define FUNC_ERR_INT_STA_EN (0x14)

#define FUNC_COR_ERR_INT_SIG_EN (0x18)
#define FUNC_UNC_ERR_INT_SIG_EN (0x1C)

#define FUNC_IO_ERR0_INT_STA (0x20)
#define FUNC_IO_ERR0_INT_STA_EN (0x24)

#define FUNC_IO_COR_ERR0_INT_SIG_EN (0x28)
#define FUNC_IO_UNC_ERR0_INT_SIG_EN (0x2C)

#define FUNC_IO_ERR1_INT_STA (0x30)
#define FUNC_IO_ERR1_INT_STA_EN (0x34)

#define FUNC_IO_COR_ERR1_INT_SIG_EN (0x38)
#define FUNC_IO_UNC_ERR1_INT_SIG_EN (0x3C)

#define FUNC_SMON_CMP_ERR_INT_STA (0x40)
#define FUNC_SMON_CMP_ERR_INT_STA_EN (0x44)

#define FUNC_SMON_CMP_COR_ERR_INT_SIG_EN (0x48)
#define FUNC_SMON_CMP_UNC_ERR_INT_SIG_EN (0x4C)

#define SWDT_ERR_INT_STA (0x50)
#define SWDT_ERR_INT_STA_EN (0x54)

#define SWDT_COR_ERR_INT_SIG_EN (0x58)
#define SWDT_UNC_ERR_INT_SIG_EN (0x5C)

#define FUNC_SEQ_ERR_INT_STA (0x60)
#define FUNC_SEQ_ERR_INT_STA_EN (0x64)

#define FUNC_SEQ_COR_ERR_INT_SIG_EN (0x68)
#define FUNC_SEQ_UNC_ERR_INT_SIG_EN (0x6C)

#define FAULT_SOURCE_EN0 (0xD00)
#define FAULT_SOURCE_EN1 (0xD04)
#define FAULT_SOURCE_EN2 (0xD08)
#define FAULT_SOURCE_EN3 (0xD0C)
#define FAULT_SOURCE_EN4 (0xD10)
#define FAULT_EVENT_CLR (0xD14)

#define TRG_SYNC_DIS (0x100)
#define SIG_SYNC_DIS (0x104)
#define SYNC_SYNC_DIS (0x110)
#define IO_SYNC_DIS (0x200)
#define IO_FLAT(io) (0x210 + (io)*4)

#define SW_TRG_CTRL (0xc00)
#define SW_TRG_STATUS (0xc04)
#define SW_TRG_PULSE_WIDTH(swt) (0xc10 + (swt) / 2 * 4)

#define SSE_IN_0_3_SEL(sse) (0x310 + 0x10 * (sse))
#define SSE_IN_4_SEL(sse) (SSE_IN_0_3_SEL(sse) + 0x4)
#define SSE_REG(sse) (SSE_IN_0_3_SEL(sse) + 0x8)
#define SSE_CTRL(sse) (SSE_IN_0_3_SEL(sse) + 0xc)

#define SIG_O_SEL(sig_o) (0x410 + (sig_o)*4)
#define IO_O_SEL(io_o) (0x540 + (io_o)*4)

#define IO_OUT_EN(io_o) (0x670 + (io_o) / 32 * 4)
#define SYNC_O_SEL(sync_o) (0x680 + (sync_o)*4)

#define TMUX_DEC_EN (0x8D0)
#define TMUX0_DRT_CTRL(drt_id) (0x6d0 + (drt_id)*0x18)
#define TMUX0_DRT_DECODE_SEL(drt_id, sel) (TMUX0_DRT_CTRL(drt_id) + (sel)*4)

#define TMUX0_INDRT_CTRL(drt_id) (0x790 + (drt_id)*0x28)
#define TMUX0_INDRT_DECODE_SEL(drt_id, sel) (TMUX0_INDRT_CTRL(drt_id) + (sel)*4)
#define TMUX0_INDRT_TID_POOL(drt_id, sel)                                      \
    (TMUX0_INDRT_CTRL(drt_id) + (sel)*4 + 0x8)

#define TUMX_DONE_MONITOR_CNT (0x8E0)

#define SMON_EN (0x8FC)
#define SMON_FIRST_MUX_SELECT(smon_id) (0x900 + smon_id * 8)
#define SMON_SECOND_MUX_SELECT(smon_id) (0x904 + smon_id * 8)

#define SWDT_IN_SEL(swdt_id) (0xA00 + swdt_id * 0x10)
#define SWDT_EVENT_CHECK(swdt_id) (SWDT_IN_SEL(swdt_id) + 4)
#define SWDT_PULSE_CHECK(swdt_id) (SWDT_IN_SEL(swdt_id) + 8)
#define SWDT_PULSE_CHECK_WINDOW(swdt_id) (SWDT_IN_SEL(swdt_id) + 0xC)

#define DMAC0_CTRL (0xB00)
#define DMAC1_CTRL (0xB04)

#define FAULT_SOURCE_EN(fault_id) (0xD00 + (fault_id) / 32 * 4)

#define TMUX_ADC_RSLT(adc_id, tmux_id) (0xE00 + (adc_id)*4 + (tmux_id)*0xC)
#define TMUX_ACMP_RSLT(acmp_id, tmux_id) (0xE80 + (acmp_id)*4 + (tmux_id)*0x10)

#define FUSA_COR_ERR_INT_STA (0x70)
#define FUSA_COR_ERR_INT_STA_EN (0x74)
#define FUSA_COR_ERR_INT_SIG_EN (0x78)
#define FUSA_UNC_ERR_INT_STA (0x7C)
#define FUSA_UNC_ERR_INT_STA_EN (0x80)
#define FUSA_UNC_ERR_INT_SIG_EN (0x84)
#define REG_PARITY_ERR_INT_STAT (0x88)
#define REG_PARITY_ERR_INT_STAT_EN (0x8C)
#define REG_PARITY_ERR_INT_SIG_EN (0x90)
#define TRG_SYNC_ERR_INJ (0xF00)

#define SIG_SYNC_ERR_INJ(num) (0xF10 + num * 4)

#define SYNC_SYNC_ERR_INJ (0xF20)
#define IO_SYNC_ERR_INJ0 (0xF24)
#define IO_FLT_ERR_INJ (0xF2C)
#define IO_ERR_INJ (0xF30)
#define SMON_ERR_INJ (0xF34)
#define TMUX_TRG_SYNC_ERR_INJ (0xF38)

#define TMUX_DEC_CMP_ERR_INJ(n) (0xF50 + 4 * (n))

#define TMUX_ARB_LSP_ERR_INJ (0xF90)
#define FAULT_EVENT_RED_ERR_INJ (0xFA4)
#define DMA_CODE_INJ (0xFE4)
#define INT_ERR_INJ (0xFF0)
#define SELFTEST_MODE (0xFFC)
/**
 * @brief xTRG signal groups, including input (i) and output (o) signals.
 */
typedef enum sdrv_xtrg_i_group {
    /* Input signal groups. */
    SDRV_XTRG_SWT_i_GROUP,
    SDRV_XTRG_TRG_i_GROUP,
    SDRV_XTRG_SIG_i_GROUP,
    SDRV_XTRG_SYNC_i_GROUP,
    SDRV_XTRG_IO_i_GROUP,
    SDRV_XTRG_TID_i_GROUP,

    /* Output signal groups. */
    SDRV_XTRG_SIG_o_GROUP,
    SDRV_XTRG_IO_o_GROUP,
    SDRV_XTRG_SYNC_o_GROUP,

    /* Logical groups for convenience. */
    SDRV_XTRG_SSIG_GROUP,

} sdrv_xtrg_i_group_e;

/**
 * @brief xTRG signals.
 */

#if (CONFIG_E3L)
#define XTRG_i_GROUP_SHIFT 28
#define XTRG_i_GROUP(SIG) (sdrv_xtrg_i_group_e)((SIG) >> XTRG_i_GROUP_SHIFT)
#define XTRG_i_SIG(SIG) ((SIG)&0x0FFFFFFFul)

#define SWT_i_START (SDRV_XTRG_SWT_i_GROUP << XTRG_i_GROUP_SHIFT)
#define SWT_i_NR 8

#define TRG_i_START (SDRV_XTRG_TRG_i_GROUP << XTRG_i_GROUP_SHIFT)
#define TRG_i_NR 4

#define SIG_i_START (SDRV_XTRG_SIG_i_GROUP << XTRG_i_GROUP_SHIFT)
#define SIG_i_NR 38

#define SYNC_i_START (SDRV_XTRG_SYNC_i_GROUP << XTRG_i_GROUP_SHIFT)
#define SYNC_i_NR 10

#define IO_i_START (SDRV_XTRG_IO_i_GROUP << XTRG_i_GROUP_SHIFT)
#define IO_i_NR 64

#define TID_i_START (SDRV_XTRG_TID_i_GROUP << XTRG_i_GROUP_SHIFT)
#define TID_i_NR 32

#define SIG_o_START (SDRV_XTRG_SIG_o_GROUP << XTRG_i_GROUP_SHIFT)
#define SIG_o_NR 40

#define IO_o_START (SDRV_XTRG_IO_o_GROUP << XTRG_i_GROUP_SHIFT)
#define IO_o_NR 64

#define SYNC_o_START (SDRV_XTRG_SYNC_o_GROUP << XTRG_i_GROUP_SHIFT)
#define SYNC_o_NR 10

#define SSIG_START (SDRV_XTRG_SSIG_GROUP << XTRG_i_GROUP_SHIFT)
#define SSIG_NR 16

/* SWT_i group. */
#define XTRG_SWT(swt) (SWT_i_START + (swt))

/* SIG_i group. */
#define ETIMER_CMP_A1(ETMR_ID) (SIG_i_START + 7 * (ETMR_ID) + 0)
#define ETIMER_CMP_B0(ETMR_ID) (SIG_i_START + 7 * (ETMR_ID) + 1)
#define ETIMER_CMP_B1(ETMR_ID) (SIG_i_START + 7 * (ETMR_ID) + 2)
#define ETIMER_CMP_C0(ETMR_ID) (SIG_i_START + 7 * (ETMR_ID) + 3)
#define ETIMER_CMP_C1(ETMR_ID) (SIG_i_START + 7 * (ETMR_ID) + 4)
#define ETIMER_CMP_D0(ETMR_ID) (SIG_i_START + 7 * (ETMR_ID) + 5)
#define ETIMER_CMP_D1(ETMR_ID) (SIG_i_START + 7 * (ETMR_ID) + 6)
#define ETIMER_DIR(ETMR_ID) (SIG_i_START + 14 + (ETMR_ID))
#define EPWM_CMP_A1(EPWM_ID) (SIG_i_START + 16 + 7 * (EPWM_ID) + 0)
#define EPWM_CMP_B0(EPWM_ID) (SIG_i_START + 16 + 7 * (EPWM_ID) + 1)
#define EPWM_CMP_B1(EPWM_ID) (SIG_i_START + 16 + 7 * (EPWM_ID) + 2)
#define EPWM_CMP_C0(EPWM_ID) (SIG_i_START + 16 + 7 * (EPWM_ID) + 3)
#define EPWM_CMP_C1(EPWM_ID) (SIG_i_START + 16 + 7 * (EPWM_ID) + 4)
#define EPWM_CMP_D0(EPWM_ID) (SIG_i_START + 16 + 7 * (EPWM_ID) + 5)
#define EPWM_CMP_D1(EPWM_ID) (SIG_i_START + 16 + 7 * (EPWM_ID) + 6)
#define ADC_DONE(ADC_ID) (SIG_i_START + 30 + (ADC_ID))
#define ACMPO(ACMP_ID) (SIG_i_START + 33 + (ACMP_ID))
#define SEM_FAULT (SIG_i_START + 37)

/* SYNC_i group. */
#define ETIMER_SNAP_SHOT_I(ETMR_ID) (SYNC_i_START + (ETMR_ID))
#define ETIMER_SET_I(ETMR_ID) (SYNC_i_START + 2 + (ETMR_ID))
#define ETIMER_CLR_I(ETMR_ID) (SYNC_i_START + 4 + (ETMR_ID))
#define EPWM_SET_I(EPWM_ID) (SYNC_i_START + 6 + (EPWM_ID))
#define EPWM_CLR_I(EPWM_ID) (SYNC_i_START + 8 + (EPWM_ID))

/* TRG_i group. */
#define ETIMER_CMP0(ETMR_ID) (TRG_i_START + (ETMR_ID))
#define EPWM_CMP0(EPWM_ID) (TRG_i_START + 2 + (EPWM_ID))

/* IO_i group. */
#define IO_i(IO) (IO_i_START + (IO))

/* TID_i group. */
#define ETIMER_EID(ETMR) (TID_i_START + (ETMR)*8)
#define EPWM_EID(EPWM) (TID_i_START + 16 + (EPWM)*8)

/* SIG_o group */
#define ETIMER_CPT0(ETMR, CHNL) (SIG_o_START + ETMR * 8 + (CHNL))
#define ETIMER_FAULT(ETMR, CHNL) (SIG_o_START + 16 + (ETMR)*4 + (CHNL))
#define ETIMER_CLK(ETMR) (SIG_o_START + 24 + (ETMR))
#define EPWM_FAULT(EPWM, CHNL) (SIG_o_START + 26 + (EPWM)*4 + (CHNL))
#define EPWM_CLK(EPWM) (SIG_o_START + 34 + (EPWM))
#define ACMP_SAMPLE(ACMP) (SIG_o_START + 36 + (ACMP))

/* IO_o group. */
#define IO_o(IO) (IO_o_START + (IO))

/* SYNC_o group. */
#define ETIMER_SNAP_SHOT_O(ETMR) (SYNC_o_START + (ETMR))
#define ETIMER_SET_O(ETMR) (SYNC_o_START + 2 + (ETMR))
#define ETIMER_CLR_O(ETMR) (SYNC_o_START + 4 + (ETMR))
#define EPWM_SET_O(EPWM) (SYNC_o_START + 6 + (EPWM))
#define EPWM_CLR_O(EPWM) (SYNC_o_START + 8 + (EPWM))

/* SSIG group. */
#define SSIG(SSIG) (SSIG_START + (SSIG))

#else

#define XTRG_i_GROUP_SHIFT 28
#define XTRG_i_GROUP(SIG) (sdrv_xtrg_i_group_e)((SIG) >> XTRG_i_GROUP_SHIFT)
#define XTRG_i_SIG(SIG) ((SIG)&0x0FFFFFFFul)

#define SWT_i_START (SDRV_XTRG_SWT_i_GROUP << XTRG_i_GROUP_SHIFT)
#define SWT_i_NR 8

#define TRG_i_START (SDRV_XTRG_TRG_i_GROUP << XTRG_i_GROUP_SHIFT)
#define TRG_i_NR 8

#define SIG_i_START (SDRV_XTRG_SIG_i_GROUP << XTRG_i_GROUP_SHIFT)
#define SIG_i_NR 68

#define SYNC_i_START (SDRV_XTRG_SYNC_i_GROUP << XTRG_i_GROUP_SHIFT)
#define SYNC_i_NR 20

#define IO_i_START (SDRV_XTRG_IO_i_GROUP << XTRG_i_GROUP_SHIFT)
#define IO_i_NR 64

#define TID_i_START (SDRV_XTRG_TID_i_GROUP << XTRG_i_GROUP_SHIFT)
#define TID_i_NR 64

#define SIG_o_START (SDRV_XTRG_SIG_o_GROUP << XTRG_i_GROUP_SHIFT)
#define SIG_o_NR 76

#define IO_o_START (SDRV_XTRG_IO_o_GROUP << XTRG_i_GROUP_SHIFT)
#define IO_o_NR 64

#define SYNC_o_START (SDRV_XTRG_SYNC_o_GROUP << XTRG_i_GROUP_SHIFT)
#define SYNC_o_NR 20

#define SSIG_START (SDRV_XTRG_SSIG_GROUP << XTRG_i_GROUP_SHIFT)
#define SSIG_NR 16

/* SWT_i group. */
#define XTRG_SWT(swt) (SWT_i_START + (swt))

/* SIG_i group. */
#define ETIMER_CMP_A1(ETMR_ID) (SIG_i_START + 7 * (ETMR_ID) + 0)
#define ETIMER_CMP_B0(ETMR_ID) (SIG_i_START + 7 * (ETMR_ID) + 1)
#define ETIMER_CMP_B1(ETMR_ID) (SIG_i_START + 7 * (ETMR_ID) + 2)
#define ETIMER_CMP_C0(ETMR_ID) (SIG_i_START + 7 * (ETMR_ID) + 3)
#define ETIMER_CMP_C1(ETMR_ID) (SIG_i_START + 7 * (ETMR_ID) + 4)
#define ETIMER_CMP_D0(ETMR_ID) (SIG_i_START + 7 * (ETMR_ID) + 5)
#define ETIMER_CMP_D1(ETMR_ID) (SIG_i_START + 7 * (ETMR_ID) + 6)
#define ETIMER_DIR(ETMR_ID) (SIG_i_START + 28 + (ETMR_ID))

#define EPWM_CMP_A1(EPWM_ID) (SIG_i_START + 32 + 7 * (EPWM_ID) + 0)
#define EPWM_CMP_B0(EPWM_ID) (SIG_i_START + 32 + 7 * (EPWM_ID) + 1)
#define EPWM_CMP_B1(EPWM_ID) (SIG_i_START + 32 + 7 * (EPWM_ID) + 2)
#define EPWM_CMP_C0(EPWM_ID) (SIG_i_START + 32 + 7 * (EPWM_ID) + 3)
#define EPWM_CMP_C1(EPWM_ID) (SIG_i_START + 32 + 7 * (EPWM_ID) + 4)
#define EPWM_CMP_D0(EPWM_ID) (SIG_i_START + 32 + 7 * (EPWM_ID) + 5)
#define EPWM_CMP_D1(EPWM_ID) (SIG_i_START + 32 + 7 * (EPWM_ID) + 6)
#define ADC_DONE(ADC_ID) (SIG_i_START + 60 + (ADC_ID))
#define ACMPO(ACMP_ID) (SIG_i_START + 63 + (ACMP_ID))
#define SEM_FAULT (SIG_i_START + 67)

/* SYNC_i group. */
#define ETIMER_SNAP_SHOT_I(ETMR_ID) (SYNC_i_START + (ETMR_ID))
#define ETIMER_SET_I(ETMR_ID) (SYNC_i_START + 4 + (ETMR_ID))
#define ETIMER_CLR_I(ETMR_ID) (SYNC_i_START + 8 + (ETMR_ID))
#define EPWM_SET_I(EPWM_ID) (SYNC_i_START + 12 + (EPWM_ID))
#define EPWM_CLR_I(EPWM_ID) (SYNC_i_START + 16 + (EPWM_ID))

/* TRG_i group. */
#define ETIMER_CMP0(ETMR_ID) (TRG_i_START + (ETMR_ID))
#define EPWM_CMP0(EPWM_ID) (TRG_i_START + 4 + (EPWM_ID))

/* IO_i group. */
#define IO_i(IO) (IO_i_START + (IO))

/* TID_i group. */
#define ETIMER_EID(ETMR) (TID_i_START + (ETMR)*8)
#define EPWM_EID(EPWM) (TID_i_START + 32 + (EPWM)*8)

/* SIG_o group */
#define ETIMER_CPT0(ETMR, CHNL) (SIG_o_START + ETMR * 8 + (CHNL))
#define ETIMER_FAULT(ETMR, CHNL) (SIG_o_START + 32 + (ETMR)*4 + (CHNL))
#define ETIMER_CLK(ETMR) (SIG_o_START + 48 + (ETMR))
#define EPWM_FAULT(EPWM, CHNL) (SIG_o_START + 52 + (EPWM)*4 + (CHNL))
#define EPWM_CLK(EPWM) (SIG_o_START + 68 + (EPWM))
#define ACMP_SAMPLE(ACMP) (SIG_o_START + 72 + (ACMP))

/* IO_o group. */
#define IO_o(IO) (IO_o_START + (IO))

/* SYNC_o group. */
#define ETIMER_SNAP_SHOT_O(ETMR) (SYNC_o_START + (ETMR))
#define ETIMER_SET_O(ETMR) (SYNC_o_START + 4 + (ETMR))
#define ETIMER_CLR_O(ETMR) (SYNC_o_START + 8 + (ETMR))
#define EPWM_SET_O(EPWM) (SYNC_o_START + 12 + (EPWM))
#define EPWM_CLR_O(EPWM) (SYNC_o_START + 16 + (EPWM))

/* SSIG group. */
#define SSIG(SSIG) (SSIG_START + (SSIG))

#endif

/**
 * @brief xTRG interrupt event.
 */
typedef enum sdrv_xtrg_irq_status {
    SDRV_FAULT_EVENT = 0U,
    SDRV_TMUX0_DONE_EVENT = 16U,
    SDRV_TMUX1_DONE_EVENT,
    SDRV_TMUX2_DONE_EVENT,
    SDRV_TMUX3_DONE_EVENT,
    SDRV_TMUX4_DONE_EVENT,
    SDRV_TMUX5_DONE_EVENT,
    SDRV_TMUX6_DONE_EVENT,
    SDRV_TMUX7_DONE_EVENT,
} sdrv_xtrg_irq_status_e;

typedef struct sdrv_xtrg sdrv_xtrg_t;

/*! @brief XTRG callback function. */
typedef void (*xtrg_callback_t)(sdrv_xtrg_t *ctrl,
                                sdrv_xtrg_irq_status_e status, void *para);

typedef struct sdrv_xtrg_sig_group {
    /* Signal number. */
    uint32_t sig_nr;

    /* Start index of SSE5 input select mux. */
    uint32_t sse_idx;

    /* Start index of smux input. */
    uint32_t smux_idx;

    /* Start index of syncmux input. */
    uint32_t syncmux_idx;

    /* Start index of smon input. */
    uint32_t smon_idx;

    /* Synchronization disable register offset. */
    uint32_t sync_dis_off;

} sdrv_xtrg_sig_group_t;

/**
 * @brief Edge detect mode for I/O filter and SSE5.
 */
typedef enum sdrv_xtrg_edge {
    SDRV_XTRG_POSEDGE = 0U,
    SDRV_XTRG_NEGEDGE,
    SDRV_XTRG_BOTHEDGE
} sdrv_xtrg_edge_e;

/**
 * @brief XTRG configurations.
 */
typedef struct sdrv_xtrg_config {
    uint32_t sig;
    uint32_t sig_i;
    uint32_t sig_o;
} sdrv_xtrg_config_t;

/**
 * @brief XTRG low level device.
 */
struct sdrv_xtrg {
    paddr_t base;
    int irq;
    xtrg_callback_t cb;
    void *para;
};

/**
 * @brief Software Triggers sel.
 */
typedef enum sdrv_xtrg_swt_sel {
    SDRV_XTRG_SWT0 = 0U,
    SDRV_XTRG_SWT1,
    SDRV_XTRG_SWT2,
    SDRV_XTRG_SWT3,
    SDRV_XTRG_SWT4,
    SDRV_XTRG_SWT5,
    SDRV_XTRG_SWT6,
    SDRV_XTRG_SWT7,
} sdrv_xtrg_swt_sel_e;

/**
 * @brief Software Triggers events.
 */
typedef enum sdrv_xtrg_swt_event {
    SDRV_XTRG_SWT_EVENT_SET = 0U,
    SDRV_XTRG_SWT_EVENT_CLEAR,
    SDRV_XTRG_SWT_EVENT_TRIGGER,
    SDRV_XTRG_SWT_EVENT_PULSE,
} sdrv_xtrg_swt_event_e;

/**
 * @brief Software Triggers config.
 */
typedef struct sdrv_xtrg_swt_config {
    sdrv_xtrg_swt_sel_e swt;
    sdrv_xtrg_swt_event_e event;
    uint32_t width;
} sdrv_xtrg_swt_config_t;

/**
 * @brief ssm sel.
 */
typedef enum sdrv_xtrg_ssm_sel {
    SDRV_XTRG_SSM0 = 0U,
    SDRV_XTRG_SSM1,
    SDRV_XTRG_SSM2,
    SDRV_XTRG_SSM3,
    SDRV_XTRG_SSM4,
    SDRV_XTRG_SSM5,
    SDRV_XTRG_SSM6,
    SDRV_XTRG_SSM7,
    SDRV_XTRG_SSM8,
    SDRV_XTRG_SSM9,
    SDRV_XTRG_SSM10,
    SDRV_XTRG_SSM11,
    SDRV_XTRG_SSM12,
    SDRV_XTRG_SSM13,
    SDRV_XTRG_SSM14,
    SDRV_XTRG_SSM15,
} sdrv_xtrg_ssm_sel_e;

/**
 * @brief SSE5 configuration structure.
 */
typedef struct sdrv_xtrg_sse5_config {
    /* 5 input signals. */
    struct {
        uint32_t sig;
        /* Detect mode. False for level detect. True for Edge detect. */
        bool edge_detect;
        /* Edge select, when edge_detect is True. */
        sdrv_xtrg_edge_e edge;
    } in[5];

    /*
     * Synthesis Engine register (sse_reg) value. The output SSIG
     * is ssig=sse_reg[in], where "in" is the 5 bit input signal value.
     */
    uint32_t synthesis_val;

    sdrv_xtrg_ssm_sel_e ssm_sel;

} sdrv_xtrg_sse5_config_t;

/**
 * @brief SMON sel.
 */
typedef enum sdrv_xtrg_smon_sel {
    SDRV_XTRG_SMON0 = 0U,
    SDRV_XTRG_SMON1,
    SDRV_XTRG_SMON2,
    SDRV_XTRG_SMON3,
    SDRV_XTRG_SMON4,
    SDRV_XTRG_SMON5,
    SDRV_XTRG_SMON6,
    SDRV_XTRG_SMON7,
    SDRV_XTRG_SMON8,
    SDRV_XTRG_SMON9,
    SDRV_XTRG_SMON10,
    SDRV_XTRG_SMON11,
    SDRV_XTRG_SMON12,
    SDRV_XTRG_SMON13,
    SDRV_XTRG_SMON14,
    SDRV_XTRG_SMON15,
    SDRV_XTRG_SMON16,
    SDRV_XTRG_SMON17,
    SDRV_XTRG_SMON18,
    SDRV_XTRG_SMON19,
    SDRV_XTRG_SMON20,
    SDRV_XTRG_SMON21,
    SDRV_XTRG_SMON22,
    SDRV_XTRG_SMON23,
    SDRV_XTRG_SMON24,
    SDRV_XTRG_SMON25,
    SDRV_XTRG_SMON26,
    SDRV_XTRG_SMON27,
    SDRV_XTRG_SMON28,
    SDRV_XTRG_SMON29,
    SDRV_XTRG_SMON30,
    SDRV_XTRG_SMON31,
} sdrv_xtrg_smon_sel_e;

/**
 * @brief SMON configuration structure.
 */
typedef struct sdrv_xtrg_smon_config {
    /* First level mux select
        sig_out_sel from SYNC_o, SIG_o and IO_o*/
    uint32_t sig_out_sel;

    /* sig_in_sel  from IO_i, TRG_i, SIG_i, SYNC_i, SSIG and SWT */
    uint32_t sig_in_sel;

    /* Second level mux select Compare input0/1 mux select from first level mux
     * output */
    uint32_t sig_input1_sel;
    uint32_t sig_input0_sel;

    /* Change polarity of input0 of comparator */
    bool change_pol_in0;

    sdrv_xtrg_smon_sel_e smon_sel;
} sdrv_xtrg_smon_config_t;

/**
 * @brief event trigger mode.
 */
typedef enum sdrv_xtrg_event_trigger_mode {
    SDRV_XTRG_EVENT_TRIGGER_MODE_POSEDGE = 0U,
    SDRV_XTRG_EVENT_TRIGGER_MODE_NEGEDGE,
    SDRV_XTRG_EVENT_TRIGGER_MODE_TOGGLE,
    SDRV_XTRG_EVENT_TRIGGER_MODE_HIGH,
    SDRV_XTRG_EVENT_TRIGGER_MODE_LOW,
} sdrv_xtrg_event_trigger_mode_e;

/**
 * @brief swdt sel.
 */
typedef enum sdrv_xtrg_swdt_sel {
    SDRV_XTRG_SWDT0 = 0U,
    SDRV_XTRG_SWDT1,
    SDRV_XTRG_SWDT2,
    SDRV_XTRG_SWDT3,
} sdrv_xtrg_swdt_sel_e;

/**
 * @brief SWDT event check configuration structure.
 */
typedef struct sdrv_xtrg_swdt_event_check_config {
    /* SWDT input select from SMON first level mux output */
    uint32_t swdt_sig_sel;

    /* Preconfigured time window for check event happen */
    uint32_t swdt_event_check_window;

    /* event trigger mode */
    sdrv_xtrg_event_trigger_mode_e event_trg_mode;

    sdrv_xtrg_swdt_sel_e swdt_sel;
} sdrv_xtrg_swdt_event_check_config_t;

/**
 * @brief Pulse signal width check.
 */
typedef enum sdrv_xtrg_pulse_check_mode {
    SDRV_XTRG_PULSE_CHECK_MODE_BETWEEN_WINDOW = 0U,
    SDRV_XTRG_PULSE_CHECK_MODE_MORE_OR_LESS_WINDOW,
} sdrv_xtrg_pulse_check_mode_e;

/**
 * @brief Pulse signal width edge.
 */
typedef enum sdrv_xtrg_pulse_check_edge {
    SDRV_XTRG_PULSE_CHECK_POSITIVE_PULSE = 0,
    SDRV_XTRG_PULSE_CHECK_NEGATIVE_PULSE,
} sdrv_xtrg_pulse_check_edge_e;

/**
 * @brief SWDT pulse check configuration structure.
 */
typedef struct sdrv_xtrg_swdt_pulse_check_config {
    /* SWDT input select from SMON first level mux output */
    uint32_t swdt_sig_sel;

    /* Pulse check mode */
    sdrv_xtrg_pulse_check_mode_e swdt_pulse_check_mode;

    /* Pulse check edge */
    sdrv_xtrg_pulse_check_edge_e swdt_pulse_check_edge;

    /* Max value of check window */
    uint32_t swdt_max_check_window;

    /* Min value of check window */
    uint32_t swdt_min_check_window;

    /* SWDT select */
    sdrv_xtrg_swdt_sel_e swdt_sel;
} sdrv_xtrg_swdt_pulse_check_config_t;

/**
 * @brief swdt check mode.
 */
typedef enum sdrv_xtrg_swdt_check_mode {
    SDRV_XTRG_SWDT_PULSE_CHECK = 0U,
    SDRV_XTRG_SWDT_EVENT_CHECK,
} sdrv_xtrg_swdt_check_mode_e;

/**
 * @brief swdt conifg.
 */
typedef struct sdrv_xtrg_swdt_config {
    sdrv_xtrg_swdt_pulse_check_config_t xtrg_swdt_pulse_check_config;
    sdrv_xtrg_swdt_event_check_config_t xtrg_swdt_event_check_config;
    sdrv_xtrg_swdt_check_mode_e xtrg_swdt_check_mode;
} sdrv_xtrg_swdt_config_t;

/**
 * @brief I/O Filter configuration.
 */
typedef struct sdrv_xtrg_io_filter_config {
    /* Filter input signal select.*/
    uint32_t sig;

    /* Debouncing filter edge. */
    sdrv_xtrg_edge_e edge;

    /*
     * Filter intervals for positive and negative edges, in units of
     * sampling intervals. Range: 2 ~ 17.
     */
    uint32_t pos_edge_intvl;
    uint32_t neg_edge_intvl;

    /* Sampling interval, in timer clock cycles. Range 1~256. */
    uint32_t sampling_intvl;

} sdrv_xtrg_io_filter_config_t;

/**
 * @brief tmux0 drt sel.
 */
typedef enum sdrv_xtrg_tmux0_drt_sel {
    SDRV_XTRG_TMUX0_DRT0 = 0U,
    SDRV_XTRG_TMUX0_DRT1,
    SDRV_XTRG_TMUX0_DRT2,
    SDRV_XTRG_TMUX0_DRT3,
    SDRV_XTRG_TMUX0_DRT4,
    SDRV_XTRG_TMUX0_DRT5,
    SDRV_XTRG_TMUX0_DRT6,
    SDRV_XTRG_TMUX0_DRT7,
} sdrv_xtrg_tmux0_drt_sel_e;

/**
 * @brief tmux0 indrt sel.
 */
typedef enum sdrv_xtrg_tmux0_indrt_sel {
    SDRV_XTRG_TMUX0_INDRT0 = 0U,
    SDRV_XTRG_TMUX0_INDRT1,
    SDRV_XTRG_TMUX0_INDRT2,
    SDRV_XTRG_TMUX0_INDRT3,
    SDRV_XTRG_TMUX0_INDRT4,
    SDRV_XTRG_TMUX0_INDRT5,
    SDRV_XTRG_TMUX0_INDRT6,
    SDRV_XTRG_TMUX0_INDRT7,
} sdrv_xtrg_tmux0_indrt_sel_e;

/**
 * @brief Trigger Mux (tmux) configuration.
 */
typedef struct sdrv_xtrg_tmux_drt_config {
    /* Trigger signal from TRG_i or SSIG groups. */
    uint32_t trigger_sig;

    /* TID_i signal, one of etimer_eid, or epwm_eid. */
    uint32_t tid;

    /* The LUT is used to select which ADC or ACMP should be triggered
     * according to tid[7:5].
     */
    uint8_t target_lut[8];

    /* TMUX drt select */
    sdrv_xtrg_tmux0_drt_sel_e tmux0_drt_sel;
} sdrv_xtrg_tmux_drt_config_t;

/**
 * @brief Trigger Mux (tmux) configuration.
 */
typedef struct sdrv_xtrg_tmux_indrt_config {
    /* Trigger signal from TRG_i or SSIG groups. */
    uint32_t trigger_sig;

    /* TERM_VAL, when read address of ID pool reaches TERM_VAL, it goes back to
     * 0. */
    uint32_t term_val;

    /* TERM_TID, when current read TID of ID pools matches TERM_TID, read
     * address of ID pool can be reset to 0. */
    uint32_t term_tid;

    /* The LUT is used to select which ADC or ACMP should be triggered
     * according to tid[7:5].
     */
    uint8_t target_lut[8];

    /* TMUX indrt input mux TID pool
    pool0 pool1 8(bit) * 8(entries) */
    uint32_t tid_pool[2];

    /*TMUX indrt select*/
    sdrv_xtrg_tmux0_indrt_sel_e tmux0_indrt_sel;

} sdrv_xtrg_tmux_indrt_config_t;

/**
 * @brief Edge detect mode for dma.
 */
typedef enum sdrv_xtrg_dma_ssig_mode {
    SDRV_XTRG_SSIG_POSEDGE = 0U,
    SDRV_XTRG_SSIG_NEGEDGE
} sdrv_xtrg_dma_ssig_mode_e;

/**
 * @brief DMA sel.
 */
typedef enum sdrv_xtrg_dma_sel {
    SDRV_XTRG_DMA0 = 0U,
    SDRV_XTRG_DMA1
} sdrv_xtrg_dma_sel_e;

/**
 * @brief DMA configuration.
 */
typedef struct sdrv_xtrg_dma_config {
    /* Select one of SSIG signals as DMA request trigger */
    sdrv_xtrg_ssm_sel_e ssm_sel;

    /* Generate DMA request on pos/neg edge of selcted SSIG */
    sdrv_xtrg_dma_ssig_mode_e ssig_mode;

    /* DMA select */
    sdrv_xtrg_dma_sel_e dma_sel;
} sdrv_xtrg_dma_config_t;

/**
 * @brief smon cmp err signal select.
 */
typedef enum sdrv_xtrg_smon_cmp_err {
    SDRV_XTRG_SMON_CMP_ERR0 = 0U,
    SDRV_XTRG_SMON_CMP_ERR1,
    SDRV_XTRG_SMON_CMP_ERR2,
    SDRV_XTRG_SMON_CMP_ERR3,
    SDRV_XTRG_SMON_CMP_ERR4,
    SDRV_XTRG_SMON_CMP_ERR5,
    SDRV_XTRG_SMON_CMP_ERR6,
    SDRV_XTRG_SMON_CMP_ERR7,
    SDRV_XTRG_SMON_CMP_ERR8,
    SDRV_XTRG_SMON_CMP_ERR9,
    SDRV_XTRG_SMON_CMP_ERR10,
    SDRV_XTRG_SMON_CMP_ERR11,
    SDRV_XTRG_SMON_CMP_ERR12,
    SDRV_XTRG_SMON_CMP_ERR13,
    SDRV_XTRG_SMON_CMP_ERR14,
    SDRV_XTRG_SMON_CMP_ERR15,
    SDRV_XTRG_SMON_CMP_ERR16,
    SDRV_XTRG_SMON_CMP_ERR17,
    SDRV_XTRG_SMON_CMP_ERR18,
    SDRV_XTRG_SMON_CMP_ERR19,
    SDRV_XTRG_SMON_CMP_ERR20,
    SDRV_XTRG_SMON_CMP_ERR21,
    SDRV_XTRG_SMON_CMP_ERR22,
    SDRV_XTRG_SMON_CMP_ERR23,
    SDRV_XTRG_SMON_CMP_ERR24,
    SDRV_XTRG_SMON_CMP_ERR25,
    SDRV_XTRG_SMON_CMP_ERR26,
    SDRV_XTRG_SMON_CMP_ERR27,
    SDRV_XTRG_SMON_CMP_ERR28,
    SDRV_XTRG_SMON_CMP_ERR29,
    SDRV_XTRG_SMON_CMP_ERR30,
    SDRV_XTRG_SMON_CMP_ERR31,
} sdrv_xtrg_smon_cmp_err_e;

/**
 * @brief SMON cmp err sem signal mode select.
 */
typedef enum sdrv_xtrg_err_sel {
    SDRV_SMON_COR_ERROR = 0U,
    SDRV_SMON_UNC_ERROR,
    SDRV_SMON_NULL,
} sdrv_xtrg_err_sel_e;

/**
 * @brief SMON cmp err config struct.
 */
typedef struct sdrv_xtrg_smon_cmp_err_config {
    /* Select one of SMON cmp err signal.*/
    sdrv_xtrg_smon_cmp_err_e smon_cmp_err;
    /* Select SMON cmp err signal as cor/uncor err signal.*/
    sdrv_xtrg_err_sel_e err_sel;
} sdrv_xtrg_smon_cmp_err_config_t;

/**
 * @brief tmux index sel.
 */
typedef enum sdrv_xtrg_tmux_sel {
    SDRV_XTRG_TMUX0 = 0U,
    SDRV_XTRG_TMUX1,
    SDRV_XTRG_TMUX2,
    SDRV_XTRG_TMUX3,
    SDRV_XTRG_TMUX4,
    SDRV_XTRG_TMUX5,
    SDRV_XTRG_TMUX6,
    SDRV_XTRG_TMUX7,
} sdrv_xtrg_tmux_sel_e;

/**
 * @brief tmux adc sel.
 */
typedef enum sdrv_xtrg_adc_sel {
    SDRV_XTRG_ADC0 = 0U,
    SDRV_XTRG_ADC1,
    SDRV_XTRG_ADC2,
} sdrv_xtrg_adc_sel_e;

/**
 * @brief tmux acmp sel.
 */
typedef enum sdrv_xtrg_acmp_sel {
    SDRV_XTRG_ACMP0 = 0U,
    SDRV_XTRG_ACMP1,
    SDRV_XTRG_ACMP2,
    SDRV_XTRG_ACMP3,
} sdrv_xtrg_acmp_sel_e;

/**
 * @brief  XTRG irp handler.
 *
 * @param[in] irq  xTRG irq.
 * @param[in] ctrl xTRG controller
 */
int sdrv_xtrg_irq_handler(uint32_t irq, void *ctrl);

/**
 * @brief Init XTRG.
 *
 * @param[in] ctrl   xTRG controller.
 */
void sdrv_xtrg_init(sdrv_xtrg_t *ctrl, xtrg_callback_t callback, void *para);

/**
 * @brief Deinit XTRG.
 *
 * @param[in] ctrl   xTRG controller.
 */
void sdrv_xtrg_deinit(sdrv_xtrg_t *ctrl);

/**
 * @brief sdrv xtrg int enable.
 *
 * @param[in] ctrl   xTRG controller.
 * @param[in] int_id int id
 */
void sdrv_xtrg_int_enable(sdrv_xtrg_t *ctrl, sdrv_xtrg_irq_status_e int_id);

/**
 * @brief sdrv xtrg int disable.
 *
 * @param[in] ctrl   xTRG controller
 * @param[in] int_id int id
 */
void sdrv_xtrg_int_disable(sdrv_xtrg_t *ctrl, sdrv_xtrg_irq_status_e int_id);

/**
 * @brief Enable or disable input signal synchronization.
 *
 * SIG_i, TRG_i, SYNC_i and IO_i signals are synchronized to timer clock domain
 * by default. This function enable or disable signal synchronizations.
 *
 * @param[in] ctrl     xTRG controller.
 * @param[in] cfg      xTRG configuration.
 * @param[in] enable   Enable or disable synchronization.
 */
void sdrv_xtrg_sync_config(sdrv_xtrg_t *ctrl, const sdrv_xtrg_config_t *cfg,
                           bool enable);

/**
 * @brief Configure I/O filters.
 *
 * @param[in] ctrl          xTRG controller.
 * @param[in] cfg           The input signal to enable or disable debouncing
 * @param[in] io_filter_cfg I/O filter configuration.
 */
void sdrv_xtrg_io_filter_config(
    sdrv_xtrg_t *ctrl, const sdrv_xtrg_io_filter_config_t *io_filter_cfg,
    bool enable);

/**
 * @brief Config SWT (software trigger).
 *
 * @param[in] ctrl       xTRG controller.
 * @param[in] swt_cfg    swt configuration.
 */
int sdrv_xtrg_swt_config(sdrv_xtrg_t *ctrl,
                         const sdrv_xtrg_swt_config_t *swt_cfg);

/**
 * @brief Enable or disable SSE5.
 *ctrl
 * @param[in] ctrl          xTRG controller.
 * @param[in] sse5_cfg      xTRG sse configuration.
 * @param[in] enable        Enable or disable sse5.
 */
void sdrv_xtrg_sse5_config(sdrv_xtrg_t *ctrl,
                           const sdrv_xtrg_sse5_config_t *sse5_cfg,
                           bool enable);

/**
 * @brief Configure signal mux, muxing input signals to SIG_o or IO_o.
 *
 * The smux module muxes SSIG, TRG_i, SIG_i, and I/O inputs to SIG_o
 * and I/O outputs.
 *
 * @param[in] ctrl      xTRG controller.
 * @param[in] cfg       xTRG configuration.
 */
void sdrv_xtrg_smux_config(sdrv_xtrg_t *ctrl, const sdrv_xtrg_config_t *cfg);

/**
 * @brief Configure IO output, muxing SSIG_i,TRG_i,SIG_i,IO_i to IO_o.
 *
 * @param[in] ctrl      xTRG controler.
 * @param[in] cfg       xTRG configuration.
 * @param[in] enable    Enable or disable io out.
 */
void sdrv_xtrg_io_out_config(sdrv_xtrg_t *ctrl, const sdrv_xtrg_config_t *cfg,
                             bool enable);

/**
 * @brief Configure syncmux, muxing input sync signals to SYNC_o.
 *
 * The syncmux module muxes SWT_i, SSIG and SYNC_i inputs to SYNC_o outputs.
 *
 * @param[in] ctrl      xTRG controller.
 * @param[in] cfg       xTRG configuration.
 */
void sdrv_xtrg_syncmux_config(sdrv_xtrg_t *ctrl, const sdrv_xtrg_config_t *cfg);

/**
 * @brief Configure syncmux, muxing input sync signals to SYNC_o.
 *
 * The syncmux module muxes SWT_i, SSIG and SYNC_i inputs to SYNC_o outputs.
 *
 * @param[in] ctrl           xTRG controller.
 * @param[in] tmux_drt_cfg   xTRG tmux direct configuration.
 */
void sdrv_xtrg_tmux0_drt_config(
    sdrv_xtrg_t *ctrl, const sdrv_xtrg_tmux_drt_config_t *tmux_drt_cfg);
/**
 * @brief Configure the Trigger Mux (tmux) indrt modules..
 *
 * The tmux muxes TRG_i, SSIG signals to ADC and ACMP modules, with specified
 * TID. There are 8 "drt" decoders, whose TID comes from TID_i, and 8
 * "indrt" decoders, whose TID comes from xTRG internel TID pool.
 *
 * @param[in] ctrl        xTRG controller.
 * @param[in] tmux_indrt  xTRG tmux indirect configuration.
 */
void sdrv_xtrg_tmux0_indrt_config(
    sdrv_xtrg_t *ctrl, const sdrv_xtrg_tmux_indrt_config_t *tmux_indrt_cfg);

/**
 * @brief Configure SMON (Signal Monitor).
 * The SMON monitors the input and output signal groups
 * First level select 32 input signals and 32 output signals.
 * Second level select inputs from output of 1st level
 *
 * @param[in] ctrl       xTRG controller.
 * @param[in] smon_cfg   smon configuration.
 * @param[in] enable     Enable or disable smon.
 */
void sdrv_xtrg_smon_config(sdrv_xtrg_t *ctrl,
                           const sdrv_xtrg_smon_config_t *smon_cfg,
                           bool enable);

/**
 * @brief Configure SWDT Pulse Check.(Signal Watch Dog Timer).
 *
 * @param[in] ctrl       xTRG controller.
 * @param[in] swdt_cfg   swdt configuration.
 * @param[in] enable     Enable or disable swdt.
 */
void sdrv_xtrg_swdt_config(sdrv_xtrg_t *ctrl,
                           const sdrv_xtrg_swdt_config_t *swdt_cfg,
                           bool enable);

/**
 * @brief Enable or disable DMA0/1.
 *
 * @param[in] ctrl     xTRG controller.
 * @param[in] config   xTRG dma configuration.
 * @param[in] enable   Enable or disable dma.
 */
void sdrv_xtrg_dma_config(sdrv_xtrg_t *ctrl,
                          const sdrv_xtrg_dma_config_t *config, bool enable);

/**
 * @brief Read tmux adc result.
 *
 * @param[in] ctrl     xTRG controller.
 * @param[in] tmux_id  xTRG tmux index.
 * @param[in] adc_id   xTRG adc index.
 *
 * @return adc result
 */
uint32_t sdrv_xtrg_adc_rslt(sdrv_xtrg_t *ctrl, sdrv_xtrg_tmux_sel_e tmux_id,
                            sdrv_xtrg_adc_sel_e adc_id);

/**
 * @brief Read tmux acmp result.
 *
 * @param[in] ctrl     xTRG controller.
 * @param[in] tmux_id  xTRG tmux index.
 * @param[in] acmp_id  xTRG acmp index.
 *
 * @return acmp result
 */
uint32_t sdrv_xtrg_acmp_rslt(sdrv_xtrg_t *ctrl, sdrv_xtrg_tmux_sel_e tmux_id,
                             sdrv_xtrg_acmp_sel_e acmp_id);

/**
 * @brief sdrv xtrg fault source enable.
 *
 * @param[in] ctrl      xTRG controller.
 * @param[in] fault_id  xTRG fault source id.
 */
void sdrv_xtrg_fault_source_config(sdrv_xtrg_t *ctrl, uint32_t fault_id,
                                   bool enable);

/**
 * @brief Smon cmp err config.
 *
 * @param[in] ctrl                      xTRG controller.
 * @param[in] xtrg_smon_cmp_err_config  Smon cmp err config.
 * @param[in] enable                    Enable or disable err signal.
 */
void sdrv_xtrg_smon_cmp_err_config(
    sdrv_xtrg_t *ctrl,
    sdrv_xtrg_smon_cmp_err_config_t *xtrg_smon_cmp_err_config, bool enable);

/**
 * @brief sdrv xtrg read fault status.
 *
 * @param[in] ctrl xTRG controller.
 */
uint32_t sdrv_xtrg_read_smon_cmp_err_status(sdrv_xtrg_t *ctrl);

/**
 * @brief sdrv set up timeout value from ADC
 *
 * @param[in] xtrg dev base.
 */
void sdrv_xtrg_tmux_done_monitor_cnt_config(sdrv_xtrg_t *ctrl, uint8_t timeOutValue);

#endif /* SDRV_XTRG_H */