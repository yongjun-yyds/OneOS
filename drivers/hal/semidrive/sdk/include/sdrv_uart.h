/**
 * @file sdrv_uart.h
 * @brief SemiDrive UART driver header file.
 *
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef INCLUDE_DRV_SDRV_UART_H
#define INCLUDE_DRV_SDRV_UART_H

#include <compiler.h>
#include <sdrv_common.h>
#if CONFIG_UART_ENABLE_DMA
#include <sdrv_dma.h>
#endif
#include <types.h>

#ifndef ASSEMBLY

__BEGIN_CDECLS

/* The default count of timeout */
#define WaitForever 0xFFFFFFFFU

/* The operation's timeout period of register. */
#define SDRV_UART_OPERATION_TIMEOUT 0x500000U

/* Auto baud detect param */
#define UART_ABR_PATTERN 0U
#define SDRV_UART_ABR_PATTERN_CHAR 0x55U
#define SDRV_UART_MAX_ABR_NUM 8U

#if CONFIG_UART_ENABLE_DMA
/* Linklist config. */
#ifdef CONFIG_NUM_LINKLIST_ITEMS
/* User linklist length. */
#define NUM_LINKLIST_ITEMS CONFIG_NUM_LINKLIST_ITEMS
#else
/* Default linklist length. */
#define NUM_LINKLIST_ITEMS 8
#endif
/* Default mad length. */
#define DMA_BURST16_MAD_SIZE_MAX (4000U * 16U)
#define DMA_BURST1_MAD_SIZE_MAX (4000U * 1U)
#endif

/** @brief Uart interrupt bitmask */
#define SDRV_UART_INTR_TX_FWE 1U /**< TX fifo waterlevel interrupt bitmask. */
#define SDRV_UART_INTR_RX_FWF                                                  \
    (1U << 1) /**< RX fifo waterlevel interrupt bitmask. */
#define SDRV_UART_INTR_RX_FUDF                                                 \
    (1U << 3) /**< RX fifo underflow interrupt bitmask. */
#define SDRV_UART_INTR_TX_FOVF                                                 \
    (1U << 4) /**< TX fifo overflow interrupt bitmask. */
#define SDRV_UART_INTR_RX_FOVF                                                 \
    (1U << 5) /**< RX fifo overflow interrupt bitmask. */
#define SDRV_UART_INTR_PARITYERR                                               \
    (1U << 8) /**< Parity error interrupt bitmask. */
#define SDRV_UART_INTR_FRAMEERR                                                \
    (1U << 9) /**< Frame error interrupt                                       \
                 bitmask.*/
#define SDRV_UART_INTR_BAUDRATEERR                                             \
    (1U << 10) /**< Baudrate error interrupt bitmask. */
#define SDRV_UART_INTR_NOISERERR                                               \
    (1U << 11) /**< Noise error interrupt bitmask. */
#define SDRV_UART_INTR_RXBREAK                                                 \
    (1U << 13) /**< RX break detect interrupt bitmask. */
#define SDRV_UART_INTR_RXIDLE                                                  \
    (1U << 14) /**< RX idle detect interrupt bitmask. */
#define SDRV_UART_INTR_TX_COMPLETED                                            \
    (1U << 17) /**< TX completed interrupt bitmask. */
#define SDRV_UART_INTR_ABRPASS                                                 \
    (1U << 18) /**< Auto baudrate detect pass interrupt bitmask. */
#define SDRV_UART_INTR_ABRFAIL                                                 \
    (1U << 19) /**< Auto baudrate detect error interrupt bitmask. */

/* Uart receive error interrupt bitmask. */
#define SDRV_UART_INTR_RX_ERROR                                                \
    (SDRV_UART_INTR_RX_FOVF | SDRV_UART_INTR_RX_FUDF |                         \
     SDRV_UART_INTR_PARITYERR | SDRV_UART_INTR_FRAMEERR |                      \
     SDRV_UART_INTR_BAUDRATEERR | SDRV_UART_INTR_NOISERERR)

/* Uart transmit error interrupt bitmask. */
#define SDRV_UART_INTR_TX_ERROR (SDRV_UART_INTR_TX_FOVF)

/* Uart abr interrupt bitmask. */
#define SDRV_UART_INTR_ABR (SDRV_UART_INTR_ABRPASS | SDRV_UART_INTR_ABRFAIL)

/* Uart tx/rx fifo base offset. */
#define SDRV_UART_TX_FIFO_OFFSET 0x200U
#define SDRV_UART_RX_FIFO_OFFSET 0x300U

typedef union {
    struct {
        uint32_t MODEN : 1;
        uint32_t MODRST : 1;
        uint32_t CGC : 2;
        uint32_t OPMOD : 4;
        uint32_t QCHCTL : 1;
        uint32_t SELFTESTMODEEN : 1;
        uint32_t RESERVED : 22;
    };
    uint32_t v;
} MCR0_Type;

typedef union {
    struct {
        uint32_t PRESALE : 5;
        uint32_t TXEN : 1;
        uint32_t RXEN : 1;
        uint32_t BITORDER : 1;
        uint32_t DATABIT : 3;
        uint32_t STOPBIT : 2;
        uint32_t PARITYBIT : 3;
        uint32_t ADDRBIT : 1;
        uint32_t ABREN : 1;
        uint32_t ABRCTL0 : 4;
        uint32_t ABRCTL1 : 3;
        uint32_t FCM : 3;
        uint32_t TRANSFERMODE : 1;
        uint32_t RXMUTECTL : 3;
    };
    uint32_t v;
} PCR0_Type;

typedef union {
    struct {
        uint32_t BAUDRATE : 24;
        uint32_t SAMPLERATE : 2;
        uint32_t RXBREAKCTL : 3;
        uint32_t RXIDLECTL : 3;
    };
    uint32_t v;
} PCR1_Type;

typedef union {
    struct {
        uint32_t CHAR1 : 8;
        uint32_t CHAR2 : 8;
        uint32_t ADDRCHAR : 8;
        uint32_t ADDRMSK : 8;
    };
    uint32_t v;
} PCR2_Type;

typedef union {
    struct {
        uint32_t DE2RE : 8;
        uint32_t RE2DE : 8;
        uint32_t DEAT : 8;
        uint32_t DEDT : 8;
    };
    uint32_t v;
} PCR3_Type;

typedef union {
    struct {
        uint32_t TXADDR : 1;
        uint32_t TXBREAK : 1;
        uint32_t TXIDLE : 1;
        uint32_t RESERVED1 : 5;
        uint32_t TXBREAKCTL : 4;
        uint32_t TXIDLECTL : 4;
        uint32_t CHARMATCHCTL : 3;
        uint32_t BCLKOE : 1;
        uint32_t RXTOCTL : 3;
        uint32_t RESERVED2 : 9;
    };
    uint32_t v;
} PCR4_Type;

typedef union {
    struct {
        uint32_t TXCHARINT : 8;
        uint32_t RESERVED1 : 8;
        uint32_t SIRTXPUL : 8;
        uint32_t SIRRXPUL : 8;
    };
    uint32_t v;
} PCR5_Type;

typedef union {
    struct {
        uint32_t LINBREAKDEL : 3;
        uint32_t CHKSUMTYP : 1;
        uint32_t RESERVED : 28;
    };
    uint32_t v;
} PCR6_Type;

typedef union {
    struct {
        uint32_t PCMDTO : 5;
        uint32_t RESERVED : 27;
    };
    uint32_t v;
} PCR7_Type;

typedef union {
    struct {
        uint32_t TXSEL : 4;
        uint32_t TXMODE : 3;
        uint32_t TXPOL : 1;
        uint32_t RXSEL : 4;
        uint32_t RXMODE : 2;
        uint32_t RXEDGESEL : 2;
        uint32_t RXPOL : 1;
        uint32_t RXSYNCEN : 1;
        uint32_t RXFILTCTL : 3;
        uint32_t RESERVED : 11;
    };
    uint32_t v;
} PCR8_Type;

typedef union {
    struct {
        uint32_t TXSEL : 4;
        uint32_t TXMODE : 3;
        uint32_t TXPOL : 1;
        uint32_t RXSEL : 4;
        uint32_t RXMODE : 2;
        uint32_t RXEDGESEL : 2;
        uint32_t RXPOL : 1;
        uint32_t RXSYNCEN : 1;
        uint32_t RXFILTCTL : 3;
        uint32_t RESERVED : 11;
    };
    uint32_t v;
} PCR11_Type;

typedef union {
    struct {
        uint32_t WMLVL : 8;
        uint32_t RESERVED1 : 8;
        uint32_t CLR : 1;
        uint32_t RESERVED2 : 15;
    };
    uint32_t v;
} FCR_Type;

typedef union {
    struct {
        uint32_t FILLLVL : 8;
        uint32_t EMPTYLVL : 8;
        uint32_t FIFOSZ : 8;
        uint32_t EMPTY : 1;
        uint32_t FULL : 1;
        uint32_t RESERVED : 6;
    };
    uint32_t v;
} FSR_Type;

typedef union {
    struct {
        uint32_t TXFWE : 1;
        uint32_t RXFWF : 1;
        uint32_t TXFUDF : 1;
        uint32_t RXFUDF : 1;
        uint32_t TXFOVF : 1;
        uint32_t RXFOVF : 1;
        uint32_t TXFABT : 1;
        uint32_t RXFABT : 1;
        uint32_t PARITYERR : 1;
        uint32_t FRAMEERR : 1;
        uint32_t BAUDRATEERR : 1;
        uint32_t NOISEERR : 1;
        uint32_t RXTO : 1;
        uint32_t RXBREAK : 1;
        uint32_t RXIDLE : 1;
        uint32_t RXADDR : 1;
        uint32_t CHARMATCH : 1;
        uint32_t TC : 1;
        uint32_t ABRPASS : 1;
        uint32_t ABRFAIL : 1;
        uint32_t STARTERR : 1;
        uint32_t APBCMDDONE : 1;
        uint32_t APBCMDABORT : 1;
        uint32_t RXPIDPASS : 1;
        uint32_t RXPIDERR : 1;
        uint32_t RXCHKSUMPASS : 1;
        uint32_t RXCHKSUMERR : 1;
        uint32_t RESERVED : 1;
        uint32_t IO0EDGE : 1;
        uint32_t IO1EDGE : 1;
        uint32_t IO2EDGE : 1;
        uint32_t IO3EDGE : 1;
    };
    uint32_t v;
} INTR0_Type;

typedef union {
    struct {
        uint32_t FC_STATE : 4;
        uint32_t LIN_STATE : 4;
        uint32_t IORX_STATE : 4;
        uint32_t IOTX_STATE : 4;
        uint32_t RXBREAK_STATE : 2;
        uint32_t ABR_STATE : 3;
        uint32_t MUTEMODE : 1;
        uint32_t PCMD_STATE : 2;
        uint32_t LIN_CHECKSUM : 8;
    };
    uint32_t v;
} PSR0_Type;

typedef union {
    struct {
        uint32_t AUTOBAUDRATE : 24;
        uint32_t ABRCHAR : 4;
    };
    uint32_t v;
} PSR1_Type;

typedef union {
    struct {
        uint32_t DOORBELL : 16;
        uint32_t RESERVED : 16;
    };
    uint32_t v;
} CMDCSR0_Type;

typedef union {
    struct {
        uint32_t TXBYTENUM : 8;
        uint32_t RXBYTENUM : 8;
        uint32_t PID : 8;
        uint32_t RESERVED : 8;
    };
    uint32_t v;
} CMDCSR1_Type;

typedef union {
    struct {
        uint32_t TXDMAE : 1;
        uint32_t RXDMAE : 1;
        uint32_t TXDMARST : 1;
        uint32_t RXDMARST : 1;
        uint32_t RESERVED : 28;
    };
    uint32_t v;
} DMACR_Type;

typedef union {
    struct {
        uint32_t TXDMA_STATE : 2;
        uint32_t RXDMA_STATE : 2;
        uint32_t TXDMAREQ : 1;
        uint32_t RXDMAREQ : 1;
        uint32_t RESERVED : 26;
    };
    uint32_t v;
} DMASR_Type;

typedef struct sdrv_uart_regs {
    volatile MCR0_Type MCR0;          /* 0x0 */
    volatile uint32_t PRDATAINJ;      /* 0x4 */
    volatile uint32_t MCR2;           /* 0x8 */
    volatile uint32_t MCR3;           /* 0xC */
    volatile uint32_t MCR4;           /* 0x10 */
    volatile uint32_t RESERVED1;      /* 0x14 */
    volatile uint32_t MSR0;           /* 0x18 */
    volatile uint32_t MSR1;           /* 0x1C */
    volatile INTR0_Type INTR0;        /* 0x20 */
    volatile uint32_t RESERVED2[2];   /* 0x24 ~ 0x28 */
    volatile uint32_t INTR3;          /* 0x2C */
    volatile INTR0_Type INTEN0;       /* 0x30 */
    volatile uint32_t RESERVED3[2];   /* 0x34 ~ 0x38 */
    volatile uint32_t INTEN3;         /* 0x3C */
    volatile CMDCSR0_Type CMDCSR0;    /* 0x40 */
    volatile CMDCSR1_Type CMDCSR1;    /* 0x44 */
    volatile uint32_t RESERVED4[2];   /* 0x48 ~ 0x4C */
    volatile FCR_Type FCR0;           /* 0x50 */
    volatile FCR_Type FCR1;           /* 0x54 */
    volatile uint32_t RESERVED5[2];   /* 0x58 ~ 0x5C */
    volatile FSR_Type FSR0;           /* 0x60 */
    volatile FSR_Type FSR1;           /* 0x64 */
    volatile uint32_t RESERVED6[2];   /* 0x68 ~ 0x6C */
    volatile DMACR_Type DMACR;        /* 0x70 */
    volatile DMASR_Type DMASR;        /* 0x74 */
    volatile uint32_t RESERVED7[2];   /* 0x78 ~ 0x7C */
    volatile PCR0_Type PCR0;          /* 0x80 */
    volatile PCR1_Type PCR1;          /* 0x84 */
    volatile PCR2_Type PCR2;          /* 0x88 */
    volatile PCR3_Type PCR3;          /* 0x8C */
    volatile PCR4_Type PCR4;          /* 0x90 */
    volatile PCR5_Type PCR5;          /* 0x94 */
    volatile PCR6_Type PCR6;          /* 0x98 */
    volatile PCR7_Type PCR7;          /* 0x9C */
    volatile PCR8_Type PCR8;          /* 0xA0 */
    volatile uint32_t PCR9;           /* 0xA4 */
    volatile uint32_t PCR10;          /* 0xA8 */
    volatile PCR11_Type PCR11;        /* 0xAC */
    volatile uint32_t PCR12;          /* 0xB0 */
    volatile uint32_t PCR13;          /* 0xB4 */
    volatile uint32_t PCR14;          /* 0xB8 */
    volatile uint32_t PCR15;          /* 0xBC */
    volatile uint32_t RESERVED8[8];   /* 0xC0 ~ 0xDC */
    volatile uint32_t INT_STA;        /* 0xE0 */
    volatile uint32_t INT_STA_EN;     /* 0xE4 */
    volatile uint32_t INT_SIG_EN;     /* 0xE8 */
    volatile uint32_t RESERVED9[5];   /* 0xEC ~ 0xFC */
    volatile PSR0_Type PSR0;          /* 0x100 */
    volatile PSR1_Type PSR1;          /* 0x104 */
    volatile uint32_t PSR2;           /* 0x108 */
    volatile uint32_t PSR3;           /* 0x10C */
    volatile uint32_t PSR4;           /* 0x110 */
    volatile uint32_t PSR5;           /* 0x114 */
    volatile uint32_t PSR6;           /* 0x118 */
    volatile uint32_t PSR7;           /* 0x11C */
    volatile uint32_t RESERVED10[56]; /* 0x120 ~ 0x1FC */
    volatile uint32_t TXDR;           /* 0x200 */
    volatile uint32_t RESERVED11[63]; /* 0x204 ~ 0x2FC */
    volatile uint32_t RXDR;           /* 0x300 */
} sdrv_uart_regs_t;

/**
 * @brief UART interrupt handle callback status for user.
 */
enum sdrv_uart_callback_status {
    SDRV_UART_NoEvent = 0U,         /**< Uart no event. */
    SDRV_UART_TxCompleted = 1U,     /**< Uart transmit is completed. */
    SDRV_UART_RxFWF = 2U,           /**< Uart rx fifo waterlevel full. */
    SDRV_UART_RxCompleted = 3U,     /**< Uart receive is completed. */
    SDRV_UART_TxFifoOverFlow = 4U,  /**< Uart tx fifo over flow error. */
    SDRV_UART_RxFifoOverFlow = 5U,  /**< Uart rx fifo over flow error. */
    SDRV_UART_RxFifoUnderFlow = 6U, /**< Uart rx fifo under flow error. */
    SDRV_UART_FramingError = 7U,    /**< Uart framing error. */
    SDRV_UART_ParityError = 8U,     /**< Uart parity error. */
    SDRV_UART_NoiseError = 9U,      /**< Uart noise error. */
    SDRV_UART_BaudrateError = 10U,  /**< Uart baud rate error. */
    SDRV_UART_AbrPass = 11U,        /**< Uart auto baudrate detect pass. */
    SDRV_UART_AbrFail = 12U,        /**< Uart auto baudrate detect fail. */
    SDRV_UART_TxDmaDone = 13U,      /**< Uart transmit with dma completed. */
    SDRV_UART_TxDmaError = 14U,     /**< Uart transmit with dma occurs error. */
    SDRV_UART_RxDmaDone = 15U,      /**< Uart receive with dma completed. */
    SDRV_UART_RxDmaError = 16U,     /**< Uart receive with dma occurs error. */
    SDRV_UART_RxIdle = 17U,         /**< Uart rx line idle detected. */
    SDRV_UART_RxBufferFull = 18U,   /**< User's rx buffer is full. */
};

typedef struct sdrv_uart sdrv_uart_t;
typedef enum sdrv_uart_callback_status sdrv_uart_callback_status_e;

/*! @brief UART transfer callback function. */
typedef void (*uart_callback_t)(sdrv_uart_t *ctrl,
                                sdrv_uart_callback_status_e status,
                                void *userData);

/**
 * @brief UART operation mode.
 */
typedef enum sdrv_uart_op_mode {
    SDRV_UART_SERIAL_MODE = 0U,
} sdrv_uart_op_mode_e;

/**
 * @brief UART transfer mode.
 */
typedef enum sdrv_uart_transfer_mode {
    SDRV_UART_FULL_DUPLEX = 0U,
} sdrv_uart_transfer_mode_e;

/**
 * @brief Number of data bits in one character.
 */
typedef enum sdrv_uart_dls {
    SDRV_UART_CHAR_8_BITS = 0U, /**< 8 bits data in one character*/
    SDRV_UART_CHAR_7_BITS = 1U, /**< 7 bits data in one character*/
    SDRV_UART_CHAR_6_BITS = 2U, /**< 6 bits data in one character*/
    SDRV_UART_CHAR_5_BITS = 3U, /**< 5 bits data in one character*/
} sdrv_uart_dls_e;

/**
 * @brief Number of stop bits.
 */
typedef enum sdrv_uart_stop {
    SDRV_UART_STOP_1_BIT = 0U,   /**< 1 bit in one character*/
    SDRV_UART_STOP_1_5_BIT = 1U, /**< 1.5 bit in one character*/
    SDRV_UART_STOP_2_BIT = 2U,   /**< 2 bit in one character*/
    SDRV_UART_STOP_0_5_BIT = 3U, /**< 0.5 bit in one character*/
} sdrv_uart_stop_e;

/**
 * @brief UART parity type.
 */
typedef enum sdrv_uart_parity {
    SDRV_UART_NO_PARITY = 0U,   /**< No parity bit*/
    SDRV_UART_EVEN_PARITY = 1U, /**< Even parity*/
    SDRV_UART_ODD_PARITY = 2U,  /**< Odd parity*/
} sdrv_uart_parity_e;

/**
 * @brief Sdrv UART flow control mode.
 */
typedef enum sdrv_uart_fcm {
    SDRV_UART_NO_FC = 0U, /**< Non flow control mode*/
} sdrv_uart_fcm_e;

/**
 * @brief Sdrv UART RS485 flow control configurations.
 */
typedef struct sdrv_uart_rs485_fcm {
    uint8_t deat_cycle;  /**< RS485 DE deassert time*/
    uint8_t dedt_cycle;  /**< RS485 DE assert time*/
    uint8_t de2re_cycle; /**< RS485 RE to DE turn around time*/
    uint8_t re2de_cycle; /**< RS485 DE to RE turn around time*/
} sdrv_uart_rs485_fcm_t;

/**
 * @brief Sdrv UART flow control mode configurations.
 */
typedef struct sdrv_uart_fcm_config {
    sdrv_uart_fcm_e fcm_type; /**< Sdrv flow control mode*/
    sdrv_uart_rs485_fcm_t
        rs485_fcm; /**< RS485 flow control mode configurations*/
} sdrv_uart_fcm_config_t;

/**
 * @brief Sdrv UART the character count that abr expect to recv and match.
 */
typedef enum sdrv_abr_max {
    SDRV_ABR_CHAR1 = 0U,  /**< The count that abr expect to match is 1 byte.*/
    SDRV_ABR_CHAR2 = 1U,  /**< The count that abr expect to match is 2 bytes.*/
    SDRV_ABR_CHAR4 = 2U,  /**< The count that abr expect to match is 4 bytes.*/
    SDRV_ABR_CHAR8 = 3U,  /**< The count that abr expect to match is 8 bytes.*/
    SDRV_ABR_CHAR16 = 4U, /**< The count that abr expect to match is 16 bytes.*/
    SDRV_ABR_CHAR32 = 5U, /**< The count that abr expect to match is 32 bytes.*/
    SDRV_ABR_CHAR64 = 6U, /**< The count that abr expect to match is 64 bytes.*/
    SDRV_ABR_CHAR128 = 7U, /**< The count that abr expect to match is 128bytes*/
} sdrv_abr_max_e;

/**
 * @brief Sdrv UART the time of high level on rx line to detect idle.
 */
typedef enum sdrv_rxidle_ctl {
    SDRV_IDLE_DETECT_DISABLE = 0U, /**< Disable rx idle detect.*/
    SDRV_IDLE_DETECT_16CYCLE =
        1U, /**< The cycle of high level on rx line to detect idle is 16.*/
    SDRV_IDLE_DETECT_32CYCLE =
        2U, /**< The cycle of high level on rx line to detect idle is 32.*/
    SDRV_IDLE_DETECT_64CYCLE =
        3U, /**< The cycle of high level on rx line to detect idle is 64.*/
    SDRV_IDLE_DETECT_128CYCLE =
        4U, /**< The cycle of high level on rx line to detect idle is 128.*/
    SDRV_IDLE_DETECT_256CYCLE =
        5U, /**< The cycle of high level on rx line to detect idle is 256.*/
    SDRV_IDLE_DETECT_512CYCLE =
        6U, /**< The cycle of high level on rx line to detect idle is 512.*/
    SDRV_IDLE_DETECT_1024CYCLE =
        7U, /**< The cycle of high level on rx line to detect idle is 1024.*/
} sdrv_rxidle_ctl_e;

/**
 * @brief Sdrv UART transfer mode.
 */
typedef enum sdrv_uart_transfer_type {
    SDRV_TRANSFER_NO_MODE = 0U,        /**< Uart do not in any tranfer mode.*/
    SDRV_TRANSFER_SYNC_MODE = 1U,      /**< Uart is in sync transfer mode.*/
    SDRV_TRANSFER_ASYNC_MODE = 2U,     /**< Uart is in async transfer mode.*/
    SDRV_TRANSFER_REALTIME_MODE = 3U,  /**< Uart is in realtime receive mode.*/
    SDRV_TRANSFER_DMA_ASYNC_MODE = 4U, /**< Uart is in dma transfer mode, This
               mode can receive the fixed length data.*/
    SDRV_TRANSFER_DMA_REALTIME_MODE =
        5U, /**< Uart is in dma transfer mode, This mode can receive the
               indefinite length data.*/
} sdrv_uart_transfer_type_e;

/**
 * @brief UART transfer status.
 */
typedef enum sdrv_uart_transfer_status {
    Transfer_TxIdle = 0U,     /**< TX idle. */
    Transfer_TxBusy = 1U,     /**< TX busy. */
    Transfer_RxIdle = 2U,     /**< RX idle. */
    Transfer_RxBusy = 3U,     /**< RX busy. */
    Transfer_RxFail = 4U,     /**< Rx error */
    Transfer_TxFail = 5U,     /**< Tx error */
    Transfer_RxAbrBusy = 6U,  /**< Auto baudrate detect busy*/
    Transfer_RxAbrReady = 7U, /**< Auto baudrate detect ready*/
    Transfer_RxAbrFail = 8U,  /**< The character number that abr expect to recv
                            and  match.*/
} sdrv_uart_transfer_status_e;

/**
 * @brief UART error status return codes.
 */
typedef enum sdrv_uart_error_status {
    Status_Success = SDRV_STATUS_OK,      /**< status for success*/
    Status_Timeout = SDRV_STATUS_TIMEOUT, /**< status for timeout*/
    Status_Busy = SDRV_STATUS_BUSY,       /**< status for busy*/
    Status_Fail = SDRV_STATUS_FAIL,       /**< status for common error*/
    Status_Invalid_Param =
        SDRV_STATUS_INVALID_PARAM, /**< status for invalid param error*/
    Status_Abr_Not_Ready = SDRV_ERROR_STATUS(
        SDRV_STATUS_GROUP_UART, 0U), /**< Status for abr not ready error. */
    Status_Rxfovf = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_UART,
                                      1U), /**< Status for rx fifo overflow. */
    Status_Parity_Err = SDRV_ERROR_STATUS(
        SDRV_STATUS_GROUP_UART, 2U), /**< Status for frame parity error. */
    Status_Frame_Err = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_UART,
                                         3U), /**<Status for frame error. */
    Status_Baudrate_Err = SDRV_ERROR_STATUS(
        SDRV_STATUS_GROUP_UART, 4U), /**< Status for baudrate error. */
    Status_Noise_Err = SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_UART,
                                         5U), /**< Status for noise error. */
    Status_dma_channel_config_Err =
        SDRV_ERROR_STATUS(SDRV_STATUS_GROUP_UART,
                          6U), /**< Status for dma channel config error. */
} sdrv_uart_error_status_e;

/**
 * @brief Sdrv UART controller.
 */
struct sdrv_uart {
    uint32_t base; /**< Uart module base */
    uint32_t irq;  /**< Uart module irq num */

    uint32_t baud;     /**< Uart dev baud rate */
    uint32_t clk_freq; /**< Uart module clk freq */

    volatile sdrv_uart_transfer_type_e
        rxtransfer_type; /**< Uart transfer mode */
    volatile sdrv_uart_transfer_type_e
        txtransfer_type; /**< Uart transfer mode */

    const uint8_t *tx_ptr;       /**< Pointer to current Tx character */
    volatile uint32_t tx_remain; /**< Number of remaining Tx characters in
                                      SDRV_TRANSFER_ASYNC_MODE;
                                      The size of the data to be transferred in
                                      SDRV_TRANSFER_DMA_MODE. */
    uart_callback_t tx_async_cb; /**< Uart transmit callback function */
    void *tx_userparam; /**< Uart transmit callback function user param */

    uint8_t *rx_ptr;             /**< Pointer to current Rx character */
    volatile uint32_t rx_remain; /**< Number of remaining Rx characters in
                                      SDRV_TRANSFER_ASYNC_MODE;
                                      The size of the data to be transferred in
                                      SDRV_TRANSFER_DMA_MODE. */
    uart_callback_t rx_async_cb; /**< Uart transmit callback function */
    void *rx_userparam; /**< Uart transmit callback function user param */

    uart_callback_t callback; /**< Uart transmit callback function */
    void *userData;           /**< Uart transmit callback function user param */

    volatile sdrv_uart_transfer_status_e tx_state; /**< Receive status */
    volatile sdrv_uart_transfer_status_e rx_state; /**< Transmit status */

    volatile uint8_t abr_count; /**< Auto baudrate detect count */
    volatile sdrv_uart_transfer_status_e
        abr_state; /**< Auto baudrate detect status */
#if CONFIG_UART_ENABLE_DMA
    sdrv_dma_t *dma_instance;
    sdrv_dma_channel_t *rx_dmachannel; /**< The pointer of rx dma channel. */
    sdrv_dma_channel_t *tx_dmachannel; /**< The pointer of tx dma channel. */
    sdrv_rxidle_ctl_e
        rx_idle; /**< The time of high level on rx line to detect idle. */
    uint32_t bytes_rxcarried; /* The number of carried bytes by dma, only for
                               dma mode. */
#endif
};

/**
 * @brief Sdrv UART configurations.
 */
typedef struct sdrv_uart_config {
    uint32_t base; /**< Uart module base*/
    uint32_t irq;  /**< Uart module irq num*/
    uint32_t
        baud;          /**< Driver will set the result of clk_freq/baud to the
                     baudrate register.
                     Note :
                     User need to note the baudrate register is an integer, there is an
                     error between the excepted value and the actual value set in the
                     register.The baudrate error interrupt state is generated when the
                     error exceeds 1.5%. When the value of clk_freq/baud is greater than
                     34, user do not need to consder the above error. */
    uint32_t clk_freq; /**< Uart clk freq*/
    sdrv_uart_dls_e data_bits;  /**< Number of data bits in one character*/
    sdrv_uart_stop_e stop_bits; /**< Number of stop bits*/
    sdrv_uart_parity_e parity;  /**< Parity type*/
    sdrv_uart_fcm_config_t fcm; /**< Flow control mode*/
    uint8_t abr_en;             /**< Auto baudrate detect enable/disable*/
    sdrv_abr_max_e
        match_num; /**< The character number that abr expect to recv and match*/

    uint8_t txwmlvl; /**< Transmit water level*/
    /* SDRV_UART_INTR_RX_FWF interrupt will be setted when the number of data in
     * rx fifo more than rxwmlvl + 1.
     */
    uint8_t rxwmlvl; /**< Receive water level*/

    bool dma_en; /**< Uart dma enable status*/
#if CONFIG_UART_ENABLE_DMA
    sdrv_rxidle_ctl_e
        rx_idle; /**< The time of high level on rx line to detect idle. */
#endif
} sdrv_uart_config_t;

/**
 * @brief Sdrv UART controller initialization.
 *
 * This function initializes:
 * 1) The uart module;
 * 2) The uart controller entity.
 *
 * This function attach irq handler and transfer callback function.
 *
 * @param [in] ctrl sdrv uart controller.
 * @param [in] cfg  sdrv uart configurations.
 * @param [in] callback sdrv uart transfer callback function.
 * @param [in] userData sdrv uart transfer callback function param.
 * @return The result of initilalizing uart module.
 */
sdrv_uart_error_status_e
sdrv_uart_controller_init(sdrv_uart_t *ctrl, const sdrv_uart_config_t *cfg,
                          uart_callback_t callback, void *userData);

/**
 * @brief Srdv UART Deinitializes.
 *
 * This function deinitializes:
 * 1) The uart controller entity
 * 2) The uart module;
 *
 * This function dettach irq handler and transfer callback function.
 *
 * @param [in] ctrl  sdrv uart ctrl
 * @return The result of deinitilalizing uart module.
 */
sdrv_uart_error_status_e sdrv_uart_deinit(sdrv_uart_t *ctrl);

/**
 * @brief Configure uart baud rate.
 *
 *This function configures the uart module baud rate.
 *This function is used to update the uart module baud rate
 *after the uart module is initialized by the sdrv_uart_controller_init.
 *
 * @param [in] ctrl sdrv uart controller
 * @param [in] baud baud
 * @return The result of setting baudrate.
 */
sdrv_uart_error_status_e sdrv_uart_set_baudrate(sdrv_uart_t *ctrl,
                                                uint32_t baud);

/**
 * @brief Start auto baudrate detect.
 *
 *This function start auto baudrate detect.
 *
 * @param [in] ctrl sdrv uart controller
 * @param [in] num The character number that abr expect to recv and match
 * @return The result of starting abr.
 */
sdrv_uart_error_status_e sdrv_uart_abr_start(sdrv_uart_t *ctrl,
                                             sdrv_abr_max_e num);

/**
 * @brief Get the data in rx fifo.
 *
 * This function is used to get data in rx fifo.
 * If user use sdrv_uart_start_realtime_receive function, this function is used
 * in user's callback function to get rx fifo data.
 * Users could call sdrv_uart_get_rxfifodata() to clear fifo.
 *
 * @param [in] ctrl sdrv uart controller
 * @param [in] data the first address of user data buffer
 * @param [in] maxlen the max length of user's data buffer
 * @return the count of received byte
 */
uint32_t sdrv_uart_get_rxfifodata(sdrv_uart_t *ctrl, uint8_t *data,
                                  uint32_t maxlen);

/**
 * @brief Start UART realtime receive.
 *
 * This function start the feature of continuous receive RX data with a
 * interrupt method.
 *
 * @param [in]  ctrl sdrv uart controller
 * @return the result of starting recv
 */
sdrv_uart_error_status_e sdrv_uart_start_realtime_receive(sdrv_uart_t *ctrl);

/**
 * @brief Stop UART realtime receive.
 *
 * This function stop the feature of continuous receive RX data with a interrupt
 * method.
 *
 * @param [in]  ctrl sdrv uart controller
 * @return The result of sopping realtime receive.
 */
sdrv_uart_error_status_e sdrv_uart_stop_realtime_receive(sdrv_uart_t *ctrl);

/**
 * @brief Synchronous UART receive.
 *
 * This function receive RX data with a blocking method.
 *
 * @param [in]  ctrl sdrv uart controller
 * @param [out] data receive data
 * @param [in]  size the size of receive data
 * @param [in]  received_bytes the size of data has been received
 * @param [in]  times the max times of query fifo empty register, recommend to
 * set DefaultTimes.
 * @return the result of transfer */
sdrv_uart_error_status_e sdrv_uart_sync_receive(sdrv_uart_t *ctrl,
                                                uint8_t *data, uint32_t size,
                                                uint32_t *received_bytes,
                                                uint32_t times);

/**
 * @brief Get empty status of tx fifo.
 *
 * This function is used to get empty status of tx fifo.
 *
 * @param [in] ctrl sdrv uart controller
 * @return the empty status of tx fifo
 */
bool sdrv_uart_txfifo_empty(sdrv_uart_t *ctrl);

/**
 * @brief Synchronous UART transmit.
 *
 * This function transmit TX data with a blocking method.
 *
 * @param [in]  ctrl sdrv uart controller
 * @param [in]  data transmit data
 * @param [in]  size the size of transmit data
 * @param [in]  transmitted_bytes the size of data has been tansmitted
 * @param [in]  times the max times of query fifo empty register, recommend
 * to set 0xFFFFFFFF
 * @return the result of transfer
 */
sdrv_uart_error_status_e
sdrv_uart_sync_transmit(sdrv_uart_t *ctrl, const uint8_t *data, uint32_t size,
                        uint32_t *transmitted_bytes, uint32_t times);

/**
 * @brief Asynchronous UART receive.
 *
 * This function receive RX data with an interrupt method.
 *
 * @param [in]  ctrl sdrv uart controller
 * @param [out] data receive data
 * @param [in]  size the size of receive data
 * @param [in]  rx_cb the callback function of receive
 * @param [in]  rx_userparam the param of callback function
 * @return the result of uart async receive function
 */
sdrv_uart_error_status_e sdrv_uart_async_receive(sdrv_uart_t *ctrl,
                                                 uint8_t *data, uint32_t size,
                                                 uart_callback_t rx_cb,
                                                 void *rx_userparam);

/**
 * @brief Asynchronous UART receive abort.
 *
 * This function abort the transfer of receive RX data with an interrupt method.
 *
 * @param [in]  ctrl sdrv uart controller
 * @return The result of aborting transmit.
 */
sdrv_uart_error_status_e sdrv_uart_async_receive_abort(sdrv_uart_t *ctrl);

/**
 * @brief Asynchronous UART transmit.
 *
 * This function transmit TX data with an interrupt method.
 *
 * @param [in] ctrl sdrv uart controller
 * @param [in] data transmit data
 * @param [in] size the size of transmit data
 * @param [in] tx_cb the callback function of transmit
 * @param [in] tx_userparam the param of callback function
 * @return the result of uart async transmit function
 */
sdrv_uart_error_status_e
sdrv_uart_async_transmit(sdrv_uart_t *ctrl, const uint8_t *data, uint32_t size,
                         uart_callback_t tx_cb, void *tx_userparam);

/**
 * @brief Asynchronous UART transmit abort.
 *
 * This function abort the transfer of transmit TX data with an interrupt
 * method.
 *
 * @param [in]  ctrl sdrv uart controller
 * @return The result of aborting transmit.
 */
sdrv_uart_error_status_e sdrv_uart_async_transmit_abort(sdrv_uart_t *ctrl);

#if CONFIG_UART_ENABLE_DMA
/**
 * @brief UART receive data with dma linklist mode.
 *
 * This function receive data with dma module.
 * User should initializes dma module before use this function.
 *
 * @param [in]  ctrl sdrv uart controller
 * @param [out] data receive data
 * @param [in]  size the size of receive data
 * @param [in]  rx_cb the callback function of receive
 * @param [in]  rx_userparam the param of callback function
 * @param [in]  dma_ctrl sdrv dma controller entity
 * @param [in]  dma_channel sdrv dma channel controller entity
 * @param [in]  channel_id The number of dma channel
 * @return The result of uart dma receive function
 *         Status_Busy : The uart receiver is busy.
 *         Status_dma_channel_config_Err : The dma channel config error.
 *         Status_Timeout : Reset uart rx dma interface timeout.
 *         Status_Success : Starting the uart to receive with dma success.
 */
sdrv_uart_error_status_e
sdrv_uart_dma_receive(sdrv_uart_t *ctrl, uint8_t *data, uint32_t size,
                      uart_callback_t rx_cb, void *rx_userparam,
                      sdrv_dma_t *dma_ctrl, sdrv_dma_channel_t *dma_channel,
                      sdrv_dma_channel_id_e channel_id);

/**
 * @brief Abort the process of uart to receive data with dma.
 *
 * This function stop the dma channel and disable uart rx dma interface and
 * clear tranfer status.
 *
 * @param [in]  ctrl sdrv uart controller
 * @return The result of aborting receive.
 *         Status_Success : Abort the transfer success.
 */
sdrv_uart_error_status_e sdrv_uart_dma_receive_abort(sdrv_uart_t *ctrl);

/**
 * @brief UART transmit data with dma linklist mode.
 *
 * This function transmit data with dma module.
 * User should initializes dma module before use this
 * function.
 *
 * @param [in]  ctrl sdrv uart controller
 * @param [out] data transmit data
 * @param [in]  size the size of transmit data
 * @param [in]  tx_cb the callback function of transmit
 * @param [in]  tx_userparam the param of callback function
 * @param [in]  dma_ctrl sdrv dma controller entity
 * @param [in]  dma_channel sdrv dma channel controller
 * entity
 * @param [in]  channel_id The number of dma channel
 * @return The result of uart dma transmit function
 *         Status_Busy : The uart transmitter is busy.
 *         Status_dma_channel_config_Err : The dma channel
 * config error. Status_Timeout : Reset uart tx dma
 * interface timeout. Status_Success : Starting the uart to
 * transmit with dma success.
 */
sdrv_uart_error_status_e
sdrv_uart_dma_transmit(sdrv_uart_t *ctrl, const uint8_t *data, uint32_t size,
                       uart_callback_t tx_cb, void *tx_userparam,
                       sdrv_dma_t *dma_ctrl, sdrv_dma_channel_t *dma_channel,
                       sdrv_dma_channel_id_e channel_id);

/**
 * @brief Abort the process of uart to transmit data with dma.
 *
 * This function stop the dma channel and disable uart tx dma interface and
 * clear transfer status.
 *
 * @param [in]  ctrl sdrv uart controller
 * @return The result of aborting transmit.
 *         Status_Timeout : Reset uart tx dma interface or clear tx fifo timeout
 *         Status_Success : Abort the transfer success.
 */
sdrv_uart_error_status_e sdrv_uart_dma_transmit_abort(sdrv_uart_t *ctrl);

/**
 * @brief UART receive data with dma.
 *
 * This function receive data with dma module.
 * User should initializes dma module before use this function.
 * User data buffer must be __aligned(CONFIG_ARCH_CACHE_LINE).
 * The size of the user's buffer should be larger than the size of the data that
 * excepted received.
 *
 * @param [in]  ctrl sdrv uart controller
 * @param [out] data receive data
 * @param [in]  size the max size of data buffer
 * @param [in]  rx_cb the callback function of receive
 * @param [in]  rx_userparam the param of callback function
 * @param [in]  dma_ctrl sdrv dma controller entity
 * @param [in]  dma_channel sdrv dma channel controller entity
 * @param [in]  channel_id The number of dma channel
 * @return The result of uart dma receive function
 *         Status_Busy : The uart receiver is busy.
 *         Status_dma_channel_config_Err : The dma channel config error.
 *         Status_Timeout : Reset uart rx dma interface timeout.
 *         Status_Success : Starting the uart to receive with dma success.
 */
sdrv_uart_error_status_e sdrv_uart_dma_receive_start(
    sdrv_uart_t *ctrl, uint8_t *data, uint32_t size, uart_callback_t rx_cb,
    void *rx_userparam, sdrv_dma_t *dma_ctrl, sdrv_dma_channel_t *dma_channel,
    sdrv_dma_channel_id_e channel_id);

/**
 * @brief Get the count of data carried by dma.
 *
 * This function only for the dma mode.
 *
 * @param [in] ctrl sdrv uart controller
 * @return the count of received byte
 */
uint32_t sdrv_uart_dma_bytes_received(sdrv_uart_t *ctrl);

#endif

/**
 * @brief Enable/Disable Sdrv UART Interrupt.
 *
 * This function enable/disable sdrv uart interrupt with the provided mask.
 *
 * @param [in] ctrl sdrv uart controller
 * @param [in] mask the interrupts to enable
 * @param [in] value enable or disable
 * @return The result of setting interrupt register.
 */
sdrv_uart_error_status_e sdrv_uart_intr_ctrl(sdrv_uart_t *ctrl, uint32_t mask,
                                             bool value);

/**
 * @brief Get Sdrv UART Interrupt enable/disable status.
 *
 * This function get sdrv uart interrupt enable/disable status with the provided
 * mask.
 *
 * @param [in] ctrl sdrv uart controller
 * @return Sdrv UART Interrupt enable/disable status
 */
uint32_t sdrv_uart_intr_get(sdrv_uart_t *ctrl);

/**
 * @brief Get Sdrv UART Interrupt flag status.
 *
 * This function get Sdrv UART Interrupt flag status.
 *
 * @param [in] ctrl sdrv uart controller
 * @return all sdrv UART Interrupt status
 */
uint32_t sdrv_uart_intr_status_get(sdrv_uart_t *ctrl);

/**
 * @brief Clear Sdrv UART Interrupt flag status.
 *
 * This function clear Sdrv UART Interrupt flag status with the provided mask.
 *
 * @param [in] ctrl sdrv uart controller
 * @param [in] mask the interrupts status to clear
 * @return The result of clearing interrupt status.
 */
sdrv_uart_error_status_e sdrv_uart_intr_status_clear(sdrv_uart_t *ctrl,
                                                     uint32_t mask);

/**
 * @brief Sdrv UART default IRQ handle function.
 *
 * This function handles the sdrv uart transmit and receive
 * and error IRQ request.
 *
 * @param [in] irq the irq number of uartx
 * @param [in] ctrl sdrv uart controller
 */
int sdrv_uart_irq_handler(uint32_t irq, void *ctrl);

__END_CDECLS

#endif

#endif
