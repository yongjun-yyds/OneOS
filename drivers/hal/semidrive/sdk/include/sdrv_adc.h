/**
 * @file sdrv_adc.h
 * @brief SemiDrive ADC driver header file.
 *
 * @copyright Copyright (c) 2021  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef SDRV_ADC_H
#define SDRV_ADC_H

#include <stdint.h>
#include <stdbool.h>
#include <regs_base.h>

#define sdrv_adc1                       ((sdrv_adc_t *)APB_ADC1_BASE)
#define sdrv_adc2                       ((sdrv_adc_t *)APB_ADC2_BASE)
#define sdrv_adc3                       ((sdrv_adc_t *)APB_ADC3_BASE)


#define SDRV_ADC_RC_CNT                 (4u)
#define SDRV_ADC_RC_ENTRY_CNT           (16u)
#define SDRV_ADC_RCHT_ENTRY_CNT         (32u)
#define SDRV_ADC_CID_PART_CNT           (8u)
#define SDRV_ADC_ANA_PARAM_CNT          (16u)
#define SDRV_ADC_MNT_SINGLE_CNT         (8u)
#define SDRV_ADC_SUB_FIFO_CNT           (4u)

#define ADC_CH_POLAR_POS                1u
#define ADC_CH_POLAR_NEG                0u

/* Construct Channel for Single Mode */
#define ADC_CH_SEL_TAISHAN(ch, polar, mux)                              \
            ((((mux) & 0x7u) << 6) | (((polar) & 0x1u) << 3) | ((ch) & 0x7u))
/* Construct Channel for Differential Mode */
#define ADC_DCH_SEL_TAISHAN(ch_p, ch_n, mux)                            \
            ((((mux) & 0x7u) << 6) | (((ch_p) & 0x7u) << 3) | ((ch_n) & 0x7u))


#define SADC_SOFT_RST_DIG               (1u << 0)
#define SADC_SOFT_RST_ANA               (1u << 1)
#define SADC_SOFT_RST_RC_POS            (2)
#define SADC_SOFT_RST_RC0TMR            (1u << 2)
#define SADC_SOFT_RST_RC1TMR            (1u << 3)
#define SADC_SOFT_RST_RC2TMR            (1u << 4)
#define SADC_SOFT_RST_RC3TMR            (1u << 5)

#define SADC_INIT_VALUE_POS             (0)
#define SADC_INIT_VALUE_MSK             (0xFFFFFu << SADC_INIT_VALUE_POS)
#define SADC_INIT_START                 (1u << 20)
#define SADC_INIT_DONE                  (1u << 24)

#define SADC_HTC_DONE_LEN_POS           (0)
#define SADC_HTC_DONE_LEN_MSK           (0xFu << SADC_HTC_DONE_LEN_POS)
#define SADC_HTC_READY_LEN_POS          (8)
#define SADC_HTC_READY_LEN_MSK          (0xFFu << SADC_HTC_READY_LEN_POS)
#define SADC_HTC_READY                  (1u << 31)

#define SADC_RCHT_ENTRY_AMSEL_POS       (0)
#define SADC_RCHT_ENTRY_AMSEL_MSK       (0x1FFu << SADC_RCHT_ENTRY_AMSEL_POS)
#define SADC_RCHT_ENTRY_CSEL_POS        (12)
#define SADC_RCHT_ENTRY_CSEL_MSK        (0xFu << SADC_RCHT_ENTRY_CSEL_POS)
#define SADC_RCHT_ENTRY_RPT_MODE        (1u << 16)
#define SADC_RCHT_ENTRY_RPT_CNT_POS     (24)
#define SADC_RCHT_ENTRY_RPT_CNT_MSK     (0x7u << SADC_RCHT_ENTRY_RPT_CNT_POS)

#define SADC_RC_TIMER_COMPARE_POS       (0)
#define SADC_RC_TIMER_COMPARE_MSK       (0xFFFFu << SADC_RC_TIMER_COMPARE_POS)
#define SADC_RC_TIMER_TERMINAL_POS      (16)
#define SADC_RC_TIMER_TERMINAL_MSK      (0xFFFFu << SADC_RC_TIMER_TERMINAL_POS)

#define SADC_RC_Q_CUR_POS               (0)
#define SADC_RC_Q_CUR_MSK               (0xFu << SADC_RC_Q_CUR_POS)
#define SADC_RC_Q_START_POS             (4)
#define SADC_RC_Q_START_MSK             (0xFu << SADC_RC_Q_START_POS)
#define SADC_RC_Q_END_POS               (8)
#define SADC_RC_Q_END_MSK               (0xFu << SADC_RC_Q_END_POS)
#define SADC_RC_TRG_START               (1u << 12)
#define SADC_RC_TMR_SLAVE               (1u << 13)
#define SADC_RC_TRG_MODE_SW             (1u << 14)
#define SADC_RC_SOFT_TRG                (1u << 15)
#define SADC_RC_OVWR_CUR_EN             (1u << 16)
#define SADC_RC_TRG_EN                  (1u << 17)

#define SADC_INT_STAT_END_COV_RC3       (1u << 0)
#define SADC_INT_STAT_END_COV_RC2       (1u << 1)
#define SADC_INT_STAT_END_COV_RC1       (1u << 2)
#define SADC_INT_STAT_END_COV_RC0       (1u << 3)
#define SADC_INT_STAT_END_COV_RCHT      (1u << 4)
#define SADC_INT_STAT_SUB_FIFO0         (1u << 8)
#define SADC_INT_STAT_SUB_FIFO1         (1u << 9)
#define SADC_INT_STAT_SUB_FIFO2         (1u << 10)
#define SADC_INT_STAT_SUB_FIFO3         (1u << 11)
#define SADC_INT_STAT_TS_OVF            (1u << 12)
#define SADC_INT_STAT_MNT_SINGLE0       (1u << 16)
#define SADC_INT_STAT_MNT_SINGLE1       (1u << 17)
#define SADC_INT_STAT_MNT_SINGLE2       (1u << 18)
#define SADC_INT_STAT_MNT_SINGLE3       (1u << 19)
#define SADC_INT_STAT_MNT_SINGLE4       (1u << 20)
#define SADC_INT_STAT_MNT_SINGLE5       (1u << 21)
#define SADC_INT_STAT_MNT_SINGLE6       (1u << 22)
#define SADC_INT_STAT_MNT_SINGLE7       (1u << 23)
#define SADC_INT_STAT_MNT_CONT          (1u << 24)

#define SADC_RC_ENTRY_AMSEL_POS         (0)
#define SADC_RC_ENTRY_AMSEL_MSK         (0x1FFu << SADC_RC_ENTRY_AMSEL_POS)
#define SADC_RC_ENTRY_CSEL_POS          (12)
#define SADC_RC_ENTRY_CSEL_MSK          (0xFu << SADC_RC_ENTRY_CSEL_POS)
#define SADC_RC_ENTRY_RPT_MODE          (1u << 16)
#define SADC_RC_ENTRY_RPT_CNT_POS       (24)
#define SADC_RC_ENTRY_RPT_CNT_MSK       (0xFu << SADC_RC_ENTRY_RPT_CNT_POS)

#define SADC_SCH_CID_MSK                (0x7u)
#define SADC_SCH_CID_WIDTH_BIT          (4)
#define SADC_SCH_CID_CNT_PER_REG        (32 / SADC_SCH_CID_WIDTH_BIT)
#define SADC_SCH_CID_CNT                (SDRV_ADC_CID_PART_CNT * SADC_SCH_CID_CNT_PER_REG)
#define SADC_SCH_CID_MV_NEXT            (7u)


#define SADC_SCH_CFG_SLOT_ITVL_POS      (0)
#define SADC_SCH_CFG_SLOT_ITVL_MSK      (0xFFu << SADC_SCH_CFG_SLOT_ITVL_POS)
#define SADC_SCH_CFG_SLOT_CONST         (1u << 8)
#define SADC_SCH_CFG_MODE_MASTER        (1u << 9)
#define SADC_SCH_CFG_MODE_SYNC          (1u << 10)
#define SADC_SCH_CFG_ROT_EN             (1u << 11)
#define SADC_SCH_CFG_ASYNC_STALL        (1u << 12)
#define SADC_SCH_CFG_SLV_DLY_POS        (16)
#define SADC_SCH_CFG_SLV_DLY_MSK        (0xFFu << SADC_SCH_CFG_SLV_DLY_POS)
#define SADC_SCH_CFG_SLOT_HALT          (1u << 24)
#define SADC_SCH_CFG_SLOT_RST           (1u << 25)
#define SADC_SCH_CFG_RST_DONE           (1u << 28)
#define SADC_SCH_CFG_TS_VLD             (1u << 30)
#define SADC_SCH_CFG_TS_RST             (1u << 31)

#define SADC_CLK_CTRL_DIV_BYPASS        (1u << 0)
#define SADC_CLK_CTRL_REFH_POS          (8)
#define SADC_CLK_CTRL_REFH_MSK          (0xFu << SADC_CLK_CTRL_REFH_POS)
#define SADC_CLK_CTRL_REFL_POS          (12)
#define SADC_CLK_CTRL_REFL_MSK          (0xFu << SADC_CLK_CTRL_REFL_POS)
#define SDRV_ADC_CLK_DIV_MAX            32      /* max clock division ratio */
#define SDRV_ADC_CLK_DIV_MIN            5       /* recommended min clock division ratio */

#define SADC_ANA_PARAM_SAMCTRL_POS      (0)
#define SADC_ANA_PARAM_SAMCTRL_MSK      (0x7u << SADC_ANA_PARAM_SAMCTRL_POS)
#define SADC_ANA_PARAM_REF_SEL          (1u << 4)
#define SADC_ANA_PARAM_DIFF_SEL         (1u << 5)
#define SADC_ANA_PARAM_CCP_POS          (8)
#define SADC_ANA_PARAM_CCP_MSK          (0xFu << SADC_ANA_PARAM_CCP_POS)
#define SADC_ANA_PARAM_CCN_POS          (12)
#define SADC_ANA_PARAM_CCN_MSK          (0xFu << SADC_ANA_PARAM_CCN_POS)
#define SADC_ANA_PARAM_CCT_POS          (16)
#define SADC_ANA_PARAM_CCT_MSK          (0x1Fu << SADC_ANA_PARAM_CCT_POS)

#define SADC_FIFO_CFG_PACK_M_POS        (0)
#define SADC_FIFO_CFG_PACK_M_MSK        (0x3u << SADC_FIFO_CFG_PACK_M_POS)
#define SADC_FIFO_CFG_PACK_M_16         (0u)
#define SADC_FIFO_CFG_PACK_M_32         (1u)
#define SADC_FIFO_CFG_PACK_M_64         (2u)
#define SADC_FIFO_CFG_BYPASS            (1u << 4)
#define SADC_FIFO_CFG_PACK16_AMSEL      (1u << 8)

#define SADC_SUB_FIFO_SADDR_POS         (0)
#define SADC_SUB_FIFO_SADDR_MSK         (0x7Fu << SADC_SUB_FIFO_SADDR_POS)
#define SADC_SUB_FIFO_THRES_POS         (8)
#define SADC_SUB_FIFO_THRES_MSK         (0x7Fu << SADC_SUB_FIFO_THRES_POS)
#define SADC_SUB_FIFO_RC_EN_POS         (16)
#define SADC_SUB_FIFO_RC_EN_MSK         (0x1Fu << SADC_SUB_FIFO_RC_EN_POS)
#define SADC_SUB_FIFO_EMPTY             (1u << 24)
#define SADC_SUB_FIFO_FULL              (1u << 25)

#define SADC_DMA_MODE_FIFO3             (1u << 0)
#define SADC_DMA_MODE_FIFO2             (1u << 1)
#define SADC_DMA_MODE_FIFO1             (1u << 2)
#define SADC_DMA_MODE_FIFO0             (1u << 3)
#define SADC_DMA_MODE_RCHT              (1u << 4)
#define SADC_DMA_CH0EN_F3RC3            (1u << 8)
#define SADC_DMA_CH0EN_F2RC2            (1u << 9)
#define SADC_DMA_CH0EN_F1RC1            (1u << 10)
#define SADC_DMA_CH0EN_F0RC0            (1u << 11)
#define SADC_DMA_CH0EN_RCHT             (1u << 12)
#define SADC_DMA_CH1EN_F3RC3            (1u << 16)
#define SADC_DMA_CH1EN_F2RC2            (1u << 17)
#define SADC_DMA_CH1EN_F1RC1            (1u << 18)
#define SADC_DMA_CH1EN_F0RC0            (1u << 19)
#define SADC_DMA_CH1EN_RCHT             (1u << 20)

#define SADC_ANA_REF_PART1_PD           (1U << 0)
#define SADC_ANA_REF_PART1_PDBISA       (1U << 1)


/**
 * @brief Registers of ADC Controller.
 */
typedef volatile struct sdrv_adc {
    uint32_t    soft_rst;
    uint32_t    init;
    uint32_t    dcoc;
    uint32_t    htc;
    uint32_t    rcht_entry[SDRV_ADC_RCHT_ENTRY_CNT];
    uint32_t    rc_timer[SDRV_ADC_RC_CNT];
    uint32_t    rc[SDRV_ADC_RC_CNT];
    uint32_t    int_stat;
    uint32_t    int_stat_en;
    uint32_t    int_sig_en;
    uint32_t    reserved1;
    uint32_t    cor_err_int_stat;
    uint32_t    cor_err_int_stat_en;
    uint32_t    cor_err_int_sig_en;
    uint32_t    reserved2;
    uint32_t    unc_err_int_stat;
    uint32_t    unc_err_int_stat_en;
    uint32_t    unc_err_int_sig_en;

    uint8_t     reserved3[0x100 - 0xDC];
    uint32_t    rc_entry[SDRV_ADC_RC_CNT][SDRV_ADC_RC_ENTRY_CNT];
    uint32_t    sch_cid_part[SDRV_ADC_CID_PART_CNT];
    uint32_t    ts_value;
    uint32_t    sch_tmo;
    uint32_t    reserved4[2];
    uint32_t    sch_cfg;
    uint32_t    sch_prio;
    uint32_t    clk_ctrl;
    uint32_t    reserved5;
    uint32_t    ana_ref_cfg1;
    uint32_t    ana_ref_cfg2;
    uint32_t    cont_mode;
    uint32_t    cont_mode1;
    uint32_t    ana_para[SDRV_ADC_ANA_PARAM_CNT];
    uint32_t    mnt_single[SDRV_ADC_MNT_SINGLE_CNT];
    uint32_t    mnt_cont;
    uint32_t    mnt_thrd_single[SDRV_ADC_MNT_SINGLE_CNT];
    uint32_t    mnt_thrd_cont;
    uint32_t    reserved6[2];
    uint32_t    mnt_cont_cfg;
    uint32_t    fifo_cfg;
    uint32_t    sub_fifo[SDRV_ADC_SUB_FIFO_CNT];
    uint32_t    dma_cfg;
    uint32_t    reserved7;
    uint32_t    fifo[SDRV_ADC_SUB_FIFO_CNT][16];

    uint32_t    reg_prty_err_int_stat;
    uint32_t    reg_prty_err_int_stat_en;
    uint32_t    reg_prty_err_int_sig_en;
    uint32_t    reserved8;
    uint32_t    fusa_cor_err_int_stat;
    uint32_t    fusa_cor_err_int_stat_en;
    uint32_t    fusa_cor_err_int_sig_en;
    uint32_t    reserved9;
    uint32_t    fusa_unc_err_int_stat;
    uint32_t    fusa_unc_err_int_stat_en;
    uint32_t    fusa_unc_err_int_sig_en;
    uint8_t     reserved10[0x460 - 0x42C];
    uint64_t    conv_rcht;
    uint64_t    conv_rc[SDRV_ADC_RC_CNT];
    uint32_t    ate_test;
    uint32_t    ate_test_cfg;
    uint32_t    dbg_reg_ctrl;
    uint32_t    dbg_reg[10];
} sdrv_adc_t;

#define SDRV_ADC_RC_TMR_MODE_SLAVE      1
#define SDRV_ADC_RC_TMR_MODE_MASTER     0
#define SDRV_ADC_RC_TRG_MODE_SW         1
#define SDRV_ADC_RC_TRG_MODE_HW         0
/**
 * @brief RC(Request Channel) configuration info.
 */
typedef struct sdrv_adc_rc_cfg {
    uint16_t    terminal;               /**< rc timer terminal value */
    uint16_t    compare;                /**< rc timer compare value */
    uint32_t    q_cur       : 4;        /**< current position of rc entry queue */
    uint32_t    q_start     : 4;        /**< start position of rc entry queue */
    uint32_t    q_end       : 4;        /**< end position of rc entry queue */
    uint32_t                : 1;
    uint32_t    tmr_mode    : 1;        /**< timer mode, SDRV_ADC_RC_TMR_MODE_SLAVE or SDRV_ADC_RC_TMR_MODE_MASTER */
    uint32_t    trg_mode    : 1;        /**< trigger mode, SDRV_ADC_RC_TRG_MODE_SW or SDRV_ADC_RC_TRG_MODE_HW */
    uint32_t                : 2;
    uint32_t    trg_en      : 1;        /**< trigger enable */
} sdrv_adc_rc_cfg_t;

#define SDRV_ADC_RCHT_REPEAT_1      0
#define SDRV_ADC_RCHT_REPEAT_2      1
#define SDRV_ADC_RCHT_REPEAT_4      2
#define SDRV_ADC_RCHT_REPEAT_8      3
#define SDRV_ADC_RCHT_REPEAT_16     4
/**
 * @brief RCHT(Request Channel Hardware Trigger) entry configuration info.
 */
typedef struct sdrv_adc_rcht_entry_cfg {
    uint32_t    channel     : 9;        /**< ADC channel constructed by ADC_[D]CH_SEL_TAISHAN macro */
    uint32_t                : 3;
    uint32_t    cfg_sel     : 4;        /**< selected analog param of the entry */
    uint32_t                : 8;
    uint32_t    repeat_cnt  : 3;        /**< repeat count, must be SDRV_ADC_RCHT_REPEAT_* */
} sdrv_adc_rcht_entry_cfg_t;

#define SDRV_ADC_RC_REPM_SW_TRG     1
#define SDRV_ADC_RC_REPM_HW_TRG     0
/**
 * @brief RC(Request Channel) entry configuration info.
 */
typedef struct sdrv_adc_rc_entry_cfg {
    uint32_t    channel     : 9;        /**< ADC channel constructed by ADC_[D]CH_SEL_TAISHAN macro */
    uint32_t                : 3;
    uint32_t    cfg_sel     : 4;        /**< selected analog param of the entry */
    uint32_t    repeat_mode : 1;        /**< repeat mode, SDRV_ADC_RC_REPM_SW_TRG or SDRV_ADC_RC_REPM_HW_TRG according to trigger method */
    uint32_t                : 7;
    uint32_t    repeat_cnt  : 5;        /**< repeat count, must be in [1, 16] */
} sdrv_adc_rc_entry_cfg_t;

#define SDRV_ADC_SAMPLE_TIME_2D5        0
#define SDRV_ADC_SAMPLE_TIME_4D5        1
#define SDRV_ADC_SAMPLE_TIME_6D5        2
#define SDRV_ADC_SAMPLE_TIME_10D5       3
#define SDRV_ADC_SAMPLE_TIME_18D5       4
#define SDRV_ADC_SAMPLE_TIME_34D5       5
#define SDRV_ADC_SAMPLE_TIME_66D5       6
#define SDRV_ADC_SAMPLE_TIME_130D5      7
#define SDRV_ADC_REF_VREFP2             1
#define SDRV_ADC_REF_VREFP1             0
#define SDRV_ADC_INPUT_DIFF             1
#define SDRV_ADC_INPUT_SINGLE           0
/**
 * @brief Analog Param configuration info.
 */
typedef struct sdrv_adc_ana_param_cfg {
    uint32_t    sample_time : 3;        /**< sample time, must be SDRV_ADC_SAMPLE_TIME_* */
    uint32_t                : 1;
    uint32_t    ref_sel     : 1;        /**< reference voltage selection, must be SDRV_ADC_REF_VREFP* */
    uint32_t    input_mode  : 1;        /**< channel input mode, SDRV_ADC_INPUT_SINGLE or SDRV_ADC_INPUT_DIFF */
} sdrv_adc_ana_param_cfg_t;

#define SDRV_ADC_FIFO_MODE_PACK16       (0u)
#define SDRV_ADC_FIFO_MODE_PACK32       (1u)
#define SDRV_ADC_FIFO_MODE_PACK64       (2u)
/**
 * @brief FIFO configuration info.
 */
typedef struct sdrv_adc_fifo_cfg {
    uint32_t    pack_mode   : 2;        /**< fifo data pack mode, must be SDRV_ADC_FIFO_MODE_PACK* */
    uint32_t                : 2;
    uint32_t    bypass      : 1;        /**< bypass fifo or not */
    uint32_t                : 3;
    uint32_t    pack16_chnl : 1;        /**< fifo data in pack16 mode has channel info or not */
    uint32_t    threshold   : 7;        /**< fifo water mark to trigger interrupt or DMA request */
} sdrv_adc_fifo_cfg_t;

/**
 * @brief Descriptor for configuring FIFO data source.
 */
#define SDRV_ADC_FIFO_INPUT_RC3         (1u << 0)
#define SDRV_ADC_FIFO_INPUT_RC2         (1u << 1)
#define SDRV_ADC_FIFO_INPUT_RC1         (1u << 2)
#define SDRV_ADC_FIFO_INPUT_RC0         (1u << 3)
#define SDRV_ADC_FIFO_INPUT_RCHT        (1u << 4)
#define SDRV_ADC_FIFO_INPUT_ALL         (0x1Fu)
typedef unsigned int                    sdrv_adc_fifo_rc_cfg_t;

/**
 * @brief Descriptor for starting multiple RC timer at the same time.
 */
#define SDRV_ADC_START_RC0      (1u << 0)
#define SDRV_ADC_START_RC1      (1u << 1)
#define SDRV_ADC_START_RC2      (1u << 2)
#define SDRV_ADC_START_RC3      (1u << 3)
#define SDRV_ADC_START_ALL      ((1u << SDRV_ADC_RC_CNT) - 1)
typedef unsigned int            sdrv_adc_rc_start_flag_t;

/**
 * @brief Describe which source to trigger DMA request.
 */
typedef enum sdrv_adc_dma_src_cfg {
    SDRV_ADC_DMA_SRC_FIFO   = 0,        /**< fifo threshold */
    SDRV_ADC_DMA_SRC_RC0,               /**< RC0 conversion done */
    SDRV_ADC_DMA_SRC_RC1,               /**< RC1 conversion done */
    SDRV_ADC_DMA_SRC_RC2,               /**< RC2 conversion done */
    SDRV_ADC_DMA_SRC_RC3,               /**< RC3 conversion done */
    SDRV_ADC_DMA_SRC_RCHT               /**< RCHT conversion done */
} sdrv_adc_dma_src_cfg_t;

/**
 * @brief Descriptor for configuring slot CID in sync mode.
 */
typedef enum sdrv_adc_sync_cid {
    SDRV_ADC_SYNC_CID_RC0 = 0,
    SDRV_ADC_SYNC_CID_RC1,
    SDRV_ADC_SYNC_CID_RC2,
    SDRV_ADC_SYNC_CID_RC3,
    SDRV_ADC_SYNC_CID_RCHT,
    SDRV_ADC_SYNC_CID_MERGE,
    SDRV_ADC_SYNC_CID_IDLE
} sdrv_adc_sync_cid_t;

/**
 * @brief Sync mode configuration info.
 */
typedef struct sdrv_adc_sync_cfg {
    uint32_t    slot_itvl   : 8;        /**< slot interval or timeout */
    uint32_t    itvl_const  : 1;        /**< slot interval is const or not */
} sdrv_adc_sync_cfg_t;


/**
 * @brief ADC controller init.
 *
 * This function initializes the ADC controller device. Excluding clock divison ratio,
 * the controller is initialized for common usage. If user need special configuration,
 * call other functions after initialization.
 *
 * Note: Call this function when switching between sync mode and Async mode.
 *
 * @param[in] sdrv_adcX ADC controller
 * @param[in] clk_div   clock division ratio for analog part
 */
void sdrv_adc_init(sdrv_adc_t *sdrv_adcX, unsigned int clk_div);

/**
 * @brief Set ADC controller to Slave mode.
 *
 * After initialization, ADC controller is in Master mode. Call this if user needs Slave mode.
 *
 * @param[in] sdrv_adcX ADC controller
 */
void sdrv_adc_set_slave_mode(sdrv_adc_t *sdrv_adcX);

/**
 * @brief Set ADC controller to Master mode.
 *
 * Call this if it's set ot Slave mode and Slave mode isn't needed any more.
 *
 * @param[in] sdrv_adcX ADC controller
 */
void sdrv_adc_clr_slave_mode(sdrv_adc_t *sdrv_adcX);

/**
 * @brief Read ADC normal interrupt status.
 *
 * @param[in] sdrv_adcX ADC controller
 * @param[out]          normal interrupt status
 */
uint32_t sdrv_adc_read_int_status(sdrv_adc_t *sdrv_adcX);

/**
 * @brief Clear ADC normal interrupt status.
 *
 * @param[in] sdrv_adcX ADC controller
 * @param[in] int_bits  interrupt bits to be cleared
 */
void sdrv_adc_clear_int_status(sdrv_adc_t *sdrv_adcX, uint32_t int_bits);

/**
 * @brief Enable or disable ADC controller to record normal interrupt status.
 *
 * @param[in] sdrv_adcX ADC controller
 * @param[in] int_bits  interrupt bits
 * @param[in] enable    set int_bits or clear int_bits
 */
void sdrv_adc_int_status_en_cfg(sdrv_adc_t *sdrv_adcX, uint32_t int_bits, bool enable);

/**
 * @brief Enable or disable ADC controller to trigger normal interrupt.
 *
 * @param[in] sdrv_adcX ADC controller
 * @param[in] int_bits  interrupt bits
 * @param[in] enable    set int_bits or clear int_bits
 */
void sdrv_adc_int_status_sig_en_cfg(sdrv_adc_t *sdrv_adcX, uint32_t int_bits, bool enable);

/**
 * @brief RC(Request Channel) configuration.
 *
 * @param[in] sdrv_adcX ADC controller
 * @param[in] int_bits  rc number [0, 3]
 * @param[in] rc_cfg    RC configuration parameter
 */
void sdrv_adc_rc_cfg(sdrv_adc_t *sdrv_adcX, unsigned int rc_nbr, sdrv_adc_rc_cfg_t *rc_cfg);

/**
 * @brief Start RC timer to trigger ADC conversion.
 *
 * @param[in] sdrv_adcX ADC controller
 * @param[in] int_bits  rc number [0, 3]
 */
void sdrv_adc_rc_start_tmr(sdrv_adc_t *sdrv_adcX, unsigned int rc_nbr);

/**
 * @brief Start multiple RC timer at the same time to trigger ADC conversion.
 *
 * @param[in] sdrv_adcX ADC controller
 * @param[in] rc_flags  flag of one or multiple rc number
 */
void sdrv_adc_rc_start_timers(sdrv_adc_t *sdrv_adcX, sdrv_adc_rc_start_flag_t rc_flags);

/**
 * @brief Stop RC timer.
 *
 * @param[in] sdrv_adcX ADC controller
 * @param[in] int_bits  rc number [0, 3]
 */
void sdrv_adc_rc_stop_tmr(sdrv_adc_t *sdrv_adcX, unsigned int rc_nbr);

/**
 * @brief Trigger ADC conversion once if the RC is in software trigger mode.
 *
 * @param[in] sdrv_adcX ADC controller
 * @param[in] int_bits  rc number [0, 3]
 */
void sdrv_adc_rc_soft_trg(sdrv_adc_t *sdrv_adcX, unsigned int rc_nbr);

/**
 * @brief Set RCHT(Request Channel Hardware Trigger) ready.
 *
 * If RCHT is ready, RCHT receives signals from cross trigger.
 *
 * @param[in] sdrv_adcX ADC controller
 * @param[in] ready_len htc ready length in sync mode. The recommended value is
 *                      (sync slot_interval - 5). It's invalid in Async mode.
 */
void sdrv_adc_rcht_ready(sdrv_adc_t *sdrv_adcX, uint8_t ready_len);

/**
 * @brief Clear RCHT(Request Channel Hardware Trigger) ready.
 *
 * @param[in] sdrv_adcX ADC controller
 */
void sdrv_adc_rcht_clr_ready(sdrv_adc_t *sdrv_adcX);

/**
 * @brief Configure RCHT(Request Channel Hardware Trigger) entry.
 *
 * @param[in] sdrv_adcX ADC controller
 * @param[in] entry_nbr entry number [0, 31]
 * @param[in] entry_cfg entry configuration parameter, including channel info
 */
void sdrv_adc_rcht_entry_cfg(sdrv_adc_t *sdrv_adcX,
                             unsigned int entry_nbr,
                             sdrv_adc_rcht_entry_cfg_t entry_cfg);

/**
 * @brief Configure RC(Request Channel) entry.
 *
 * @param[in] sdrv_adcX ADC controller
 * @param[in] rc_nbr    RC number [0, 3]
 * @param[in] entry_nbr entry number [0, 15]
 * @param[in] entry_cfg entry configuration parameter, including channel info
 */
void sdrv_adc_rc_entry_cfg( sdrv_adc_t *sdrv_adcX,
                            unsigned int rc_nbr,
                            unsigned int entry_nbr,
                            sdrv_adc_rc_entry_cfg_t entry_cfg);

/**
 * @brief Configure one of the analog parameter array.
 *
 * @param[in] sdrv_adcX ADC controller
 * @param[in] param_nbr parameter number [0, 15]
 * @param[in] aparam_cfg analog parameter configuration info
 */
void sdrv_adc_ana_param_cfg(sdrv_adc_t *sdrv_adcX,
                            unsigned int param_nbr,
                            sdrv_adc_ana_param_cfg_t aparam_cfg);

/**
 * @brief Configure FIFO working mode.
 *
 * @param[in] sdrv_adcX ADC controller
 * @param[in] fifo_cfg  fifo configuration parameter
 */
void sdrv_adc_fifo_cfg(sdrv_adc_t *sdrv_adcX, sdrv_adc_fifo_cfg_t *fifo_cfg);

/**
 * @brief Configure which RC or RCHT could insert its data into FIFO.
 *
 * After initialization, FIFO receives data from all RC and RCHT. Call this only if
 *  data from some RC or RCHT isn't needed.
 *
 * @param[in] sdrv_adcX ADC controller
 * @param[in] rc_cfg    could be any or combine of SDRV_ADC_FIFO_INPUT_RC*
 */
void sdrv_adc_fifo_rc_cfg(sdrv_adc_t *sdrv_adcX, sdrv_adc_fifo_rc_cfg_t rc_cfg);

/**
 * @brief Get ADC FIFO status.
 *
 * @param[in] sdrv_adcX ADC controller
 */
uint32_t sdrv_adc_fifo_status(sdrv_adc_t *sdrv_adcX);

/**
 * @brief Read ADC FIFO data.
 *
 * In pack16 mode, reading once returns 2 conversion data.
 * In pack64 mode, must call this twice to get 1 conversion data.
 *
 * @param[in] sdrv_adcX ADC controller
 */
uint32_t sdrv_adc_read_fifo(sdrv_adc_t *sdrv_adcX);

/**
 * @brief Return FIFO address of the ADC controller.
 *
 * @param[in] sdrv_adcX ADC controller
 */
uintptr_t sdrv_adc_fifo_addr(sdrv_adc_t *sdrv_adcX);

/**
 * @brief Configure the source to trigger DMA request and enable it.
 *
 * @param[in] sdrv_adcX ADC controller
 * @param[in] req_src   DMA request source
 */
void sdrv_adc_dma_enable(sdrv_adc_t *sdrv_adcX, sdrv_adc_dma_src_cfg_t req_src);

/**
 * @brief Disable DMA request of the ADC controller.
 *
 * @param[in] sdrv_adcX ADC controller
 */
void sdrv_adc_dma_disable(sdrv_adc_t *sdrv_adcX);

/**
 * @brief Configure slot CIDs and interval if it's in sync mode.
 *
 * @param[in] sdrv_adcX ADC controller
 * @param[in] sync_cfg  slot interval and interval mode parameter
 * @param[in] slot_cid  slot CID buffer, could be SDRV_ADC_SYNC_CID_*
 * @param[in] len       slot CID buffer length [1, 64]
 */
void sdrv_adc_sync_cfg( sdrv_adc_t *sdrv_adcX,
                        sdrv_adc_sync_cfg_t sync_cfg,
                        uint8_t *slot_cid,
                        unsigned int len);

/**
 * @brief Put ADC in sync mode and start working.
 *
 * Call sdrv_adc_sync_cfg before this.
 *
 * @param[in] sdrv_adcX ADC controller
 */
void sdrv_adc_sync_start(sdrv_adc_t *sdrv_adcX);

/**
 * @brief Put ADC in Async mode and start working.
 *
 * @param[in] sdrv_adcX ADC controller
 */
void sdrv_adc_Async_start(sdrv_adc_t *sdrv_adcX);

/**
 * @brief Reset slot CID pointer to 0 in sync mode.
 *
 * @param[in] sdrv_adcX ADC controller
 */
void sdrv_adc_sync_slot_reset(sdrv_adc_t *sdrv_adcX);

/**
 * @brief Stop ADC from working.
 *
 * @param[in] sdrv_adcX     ADC controller
 * @param[in] rc_tmr_stop   stop RC timer from running or not
 */
void sdrv_adc_stop(sdrv_adc_t *sdrv_adcX, bool rc_tmr_stop);

/**
 * @brief ADC analog reference configuration.
 *
 * This function control BIAS and SAR analog part power down or power on.
 *
 * @param [in] sdrv_adcX ADC controller
 * @param [in] down true for power down, false for power on
 */
void sdrv_adc_power_ctrl(sdrv_adc_t *sdrv_adcX, bool down);


#endif  /* SDRV_ADC_H */
