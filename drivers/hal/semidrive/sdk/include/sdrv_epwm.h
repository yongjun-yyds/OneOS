/**
 * @file sdrv_epwm.h
 * @brief SemiDrive Energy Pulse-Width Modulation (EPWM) driver header file.
 *
 * @copyright Copyright (c) 2022 Semidrive Semiconductor.
 * @All rights reserved.
 *
 */

#ifndef SDRV_EPWM_DRV_H_
#define SDRV_EPWM_DRV_H_

#include "../source/epwm/sdrv_epwm_reg.h"

#if CONFIG_E3L
#if ((CONFIG_E3106) || (CONFIG_E3104))
#define EPWM_SOURCE_CLK 150
#else
#define EPWM_SOURCE_CLK 198
#endif
#else
#define EPWM_SOURCE_CLK 250
#endif

#define EPWM_FIFO_A_OFFSET (0xc0U)
#define EPWM_FIFO_B_OFFSET (0xc4U)
#define EPWM_FIFO_C_OFFSET (0xc8U)
#define EPWM_FIFO_D_OFFSET (0xccU)
#define EPWM_CNT_G0_OVF_OFFSET (0x104U)
#define EPWM_CNT_G1_OVF_OFFSET (0x124U)
#define EPWM_INT_STA_EN (0x3f << 0 | 0xf << 20)
#define EPWM_COR_ERR_INT_STA_EN (0xf << 0 | 0xffff << 8)
#define EPWM_UNC_ERR_INT_STA_EN (0xf << 0 | 0xffff << 8)

#define EPWM_BM_INT_EN_CHN_D_DMA_REQ_SHIFT (23U)
#define EPWM_BM_INT_EN_CHN_C_DMA_REQ_SHIFT (22U)
#define EPWM_BM_INT_EN_CHN_B_DMA_REQ_SHIFT (21U)
#define EPWM_BM_INT_EN_CHN_A_DMA_REQ_SHIFT (20U)
#define EPWM_BM_INT_EN_CNT_G1_OVF_SHIFT (5U)
#define EPWM_BM_INT_EN_CNT_G0_OVF_SHIFT (4U)
#define EPWM_BM_INT_EN_CMP_D_SHIFT (3U)
#define EPWM_BM_INT_EN_CMP_C_SHIFT (2U)
#define EPWM_BM_INT_EN_CMP_B_SHIFT (1U)
#define EPWM_BM_INT_EN_CMP_A_SHIFT (0U)

#define EPWM_BM_ERR_INT_STA_CMP_D_REG_UPD_TWICE_SHIFT (23U)
#define EPWM_BM_ERR_INT_STA_CMP_C_REG_UPD_TWICE_SHIFT (22U)
#define EPWM_BM_ERR_INT_STA_CMP_B_REG_UPD_TWICE_SHIFT (21U)
#define EPWM_BM_ERR_INT_STA_CMP_A_REG_UPD_TWICE_SHIFT (20U)
#define EPWM_BM_ERR_INT_STA_CMP_D_REG_NO_UPD_SHIFT (19U)
#define EPWM_BM_ERR_INT_STA_CMP_C_REG_NO_UPD_SHIFT (18U)
#define EPWM_BM_ERR_INT_STA_CMP_B_REG_NO_UPD_SHIFT (17U)
#define EPWM_BM_ERR_INT_STA_CMP_A_REG_NO_UPD_SHIFT (16U)
#define EPWM_BM_ERR_INT_STA_CMP_D_FAULT_EVENT_SHIFT (15U)
#define EPWM_BM_ERR_INT_STA_CMP_C_FAULT_EVENT_SHIFT (14U)
#define EPWM_BM_ERR_INT_STA_CMP_B_FAULT_EVENT_SHIFT (13U)
#define EPWM_BM_ERR_INT_STA_CMP_A_FAULT_EVENT_SHIFT (12U)
#define EPWM_BM_ERR_INT_STA_CMP_D0_FAULT_SHIFT (11U)
#define EPWM_BM_ERR_INT_STA_CMP_C0_FAULT_SHIFT (10U)
#define EPWM_BM_ERR_INT_STA_CMP_B0_FAULT_SHIFT (9U)
#define EPWM_BM_ERR_INT_STA_CMP_A0_FAULT_SHIFT (8U)
#define EPWM_BM_ERR_INT_STA_FIFO_D_UNDERRUN_SHIFT (3U)
#define EPWM_BM_ERR_INT_STA_FIFO_C_UNDERRUN_SHIFT (2U)
#define EPWM_BM_ERR_INT_STA_FIFO_B_UNDERRUN_SHIFT (1U)
#define EPWM_BM_ERR_INT_STA_FIFO_A_UNDERRUN_SHIFT (0U)

#define EPWM_BM_CHN_D_DMA_REQ (1 << EPWM_BM_INT_EN_CHN_D_DMA_REQ_SHIFT)
#define EPWM_BM_CHN_C_DMA_REQ (1 << EPWM_BM_INT_EN_CHN_C_DMA_REQ_SHIFT)
#define EPWM_BM_CHN_B_DMA_REQ (1 << EPWM_BM_INT_EN_CHN_B_DMA_REQ_SHIFT)
#define EPWM_BM_CHN_A_DMA_REQ (1 << EPWM_BM_INT_EN_CHN_A_DMA_REQ_SHIFT)
#define EPWM_BM_CNT_G0_OVF (1 << EPWM_BM_INT_EN_CNT_G0_OVF_SHIFT)
#define EPWM_BM_CNT_G1_OVF (1 << EPWM_BM_INT_EN_CNT_G1_OVF_SHIFT)
#define EPWM_BM_CMP_D (1 << EPWM_BM_INT_EN_CMP_D_SHIFT)
#define EPWM_BM_CMP_C (1 << EPWM_BM_INT_EN_CMP_C_SHIFT)
#define EPWM_BM_CMP_B (1 << EPWM_BM_INT_EN_CMP_B_SHIFT)
#define EPWM_BM_CMP_A (1 << EPWM_BM_INT_EN_CMP_A_SHIFT)

#define EPWM_BM_CMP_D_REG_UPD_TWICE                                            \
    (1 << EPWM_BM_ERR_INT_STA_CMP_D_REG_UPD_TWICE_SHIFT)
#define EPWM_BM_CMP_C_REG_UPD_TWICE                                            \
    (1 << EPWM_BM_ERR_INT_STA_CMP_C_REG_UPD_TWICE_SHIFT)
#define EPWM_BM_CMP_B_REG_UPD_TWICE                                            \
    (1 << EPWM_BM_ERR_INT_STA_CMP_B_REG_UPD_TWICE_SHIFT)
#define EPWM_BM_CMP_A_REG_UPD_TWICE                                            \
    (1 << EPWM_BM_ERR_INT_STA_CMP_A_REG_UPD_TWICE_SHIFT)
#define EPWM_BM_CMP_D_REG_NO_UPD                                               \
    (1 << EPWM_BM_ERR_INT_STA_CMP_D_REG_NO_UPD_SHIFT)
#define EPWM_BM_CMP_C_REG_NO_UPD                                               \
    (1 << EPWM_BM_ERR_INT_STA_CMP_C_REG_NO_UPD_SHIFT)
#define EPWM_BM_CMP_B_REG_NO_UPD                                               \
    (1 << EPWM_BM_ERR_INT_STA_CMP_B_REG_NO_UPD_SHIFT)
#define EPWM_BM_CMP_A_REG_NO_UPD                                               \
    (1 << EPWM_BM_ERR_INT_STA_CMP_A_REG_NO_UPD_SHIFT)
#define EPWM_BM_CMP_D_FAULT_EVENT                                              \
    (1 << EPWM_BM_ERR_INT_STA_CMP_D_FAULT_EVENT_SHIFT)
#define EPWM_BM_CMP_C_FAULT_EVENT                                              \
    (1 << EPWM_BM_ERR_INT_STA_CMP_C_FAULT_EVENT_SHIFT)
#define EPWM_BM_CMP_B_FAULT_EVENT                                              \
    (1 << EPWM_BM_ERR_INT_STA_CMP_B_FAULT_EVENT_SHIFT)
#define EPWM_BM_CMP_A_FAULT_EVENT                                              \
    (1 << EPWM_BM_ERR_INT_STA_CMP_A_FAULT_EVENT_SHIFT)
#define EPWM_BM_CMP_D0_FAULT (1 << EPWM_BM_ERR_INT_STA_CMP_D0_FAULT_SHIFT)
#define EPWM_BM_CMP_C0_FAULT (1 << EPWM_BM_ERR_INT_STA_CMP_C0_FAULT_SHIFT)
#define EPWM_BM_CMP_B0_FAULT (1 << EPWM_BM_ERR_INT_STA_CMP_B0_FAULT_SHIFT)
#define EPWM_BM_CMP_A0_FAULT (1 << EPWM_BM_ERR_INT_STA_CMP_A0_FAULT_SHIFT)
#define EPWM_BM_FIFO_D_UNDERRUN (1 << EPWM_BM_ERR_INT_STA_FIFO_D_UNDERRUN_SHIFT)
#define EPWM_BM_FIFO_C_UNDERRUN (1 << EPWM_BM_ERR_INT_STA_FIFO_C_UNDERRUN_SHIFT)
#define EPWM_BM_FIFO_B_UNDERRUN (1 << EPWM_BM_ERR_INT_STA_FIFO_B_UNDERRUN_SHIFT)
#define EPWM_BM_FIFO_A_UNDERRUN (1 << EPWM_BM_ERR_INT_STA_FIFO_A_UNDERRUN_SHIFT)

#if CONFIG_E3L
#define EPWM_BM_CENTER_ALIGN_CMP_INT_STA_CMP_D_CMP11 (0x01U << 15U)
#define EPWM_BM_CENTER_ALIGN_CMP_INT_STA_CMP_D_CMP10 (0x01U << 14U)
#define EPWM_BM_CENTER_ALIGN_CMP_INT_STA_CMP_D_CMP01 (0x01U << 13U)
#define EPWM_BM_CENTER_ALIGN_CMP_INT_STA_CMP_D_CMP00 (0x01U << 12U)
#define EPWM_BM_CENTER_ALIGN_CMP_INT_STA_CMP_C_CMP11 (0x01U << 11U)
#define EPWM_BM_CENTER_ALIGN_CMP_INT_STA_CMP_C_CMP10 (0x01U << 10U)
#define EPWM_BM_CENTER_ALIGN_CMP_INT_STA_CMP_C_CMP01 (0x01U << 9U)
#define EPWM_BM_CENTER_ALIGN_CMP_INT_STA_CMP_C_CMP00 (0x01U << 8U)
#define EPWM_BM_CENTER_ALIGN_CMP_INT_STA_CMP_B_CMP11 (0x01U << 7U)
#define EPWM_BM_CENTER_ALIGN_CMP_INT_STA_CMP_B_CMP10 (0x01U << 6U)
#define EPWM_BM_CENTER_ALIGN_CMP_INT_STA_CMP_B_CMP01 (0x01U << 5U)
#define EPWM_BM_CENTER_ALIGN_CMP_INT_STA_CMP_B_CMP00 (0x01U << 4U)
#define EPWM_BM_CENTER_ALIGN_CMP_INT_STA_CMP_A_CMP11 (0x01U << 3U)
#define EPWM_BM_CENTER_ALIGN_CMP_INT_STA_CMP_A_CMP10 (0x01U << 2U)
#define EPWM_BM_CENTER_ALIGN_CMP_INT_STA_CMP_A_CMP01 (0x01U << 1U)
#define EPWM_BM_CENTER_ALIGN_CMP_INT_STA_CMP_A_CMP00 (0x01U << 0U)
#endif

/**
 * @brief epwm_cmp_cfg_con_mode.
 */
typedef enum epwm_cmp_cfg_con_mode {
    EPWM_CONSECUTIVE_MODE,
    EPWM_ONLY_EXECUTE_ONE_TIME,
} epwm_cmp_cfg_con_mode_e;

/**
 * @brief epwm_cmp_out_mode.
 */
typedef enum epwm_cmp_out_mode {
    EPWM_ONLY_COMP0_OUTPUT,
    EPWM_BOTH_COMP0_AND_COMP1_OUTPUT,
} epwm_cmp_out_mode_e;

/**
 * @brief epwm_cfg_ctrl_dma_trig_sel.
 */
typedef enum epwm_cfg_ctrl_dma_trig_sel {
    EPWM_CMP_A_CE = 1 << 0,
    EPWM_CMP_B_CE = 1 << 1,
    EPWM_CMP_C_CE = 1 << 2,
    EPWM_CMP_D_CE = 1 << 3,
    EPWM_CNT_G0_CE = 1 << 4,
    EPWM_CNT_G1_CE = 1 << 5,
} epwm_cfg_ctrl_dma_trig_sel_e;

/**
 * @brief epwm_cmpxx_val NS.
 */
typedef struct epwm_multi_cmp_val {
    uint32_t cmp00_val; /**< multi compare00 value*/
    uint32_t cmp01_val; /**< multi compare01 value*/
    uint32_t cmp10_val; /**< multi compare10 value*/
    uint32_t cmp11_val; /**< multi compare11 value*/
} epwm_multi_cmp_val_t;

/**
 * @brief epwm_eid_val.
 */
typedef struct epwm_eid_val {
    uint8_t eid00; /**< compare00 event ID*/
    uint8_t eid01; /**< compare01 event ID*/
    uint8_t eid10; /**< compare10 event ID*/
    uint8_t eid11; /**< compare11 event ID*/
} epwm_eid_val_t;

/**
 * @brief epwm_dual_duty_state.
 */
typedef struct epwm_dual_duty_state {
    uint32_t left_point;  /**< dual compare mode left point value*/
    uint32_t right_point; /**< dual compare mode right point value*/
} epwm_dual_duty_state_t;

/**
 * @brief epwm_fs_sta.
 */
typedef struct epwm_fs_state {
    bool cmp0_fs_state; /**< cmp0 failsafe state*/
    bool cmp1_fs_state; /**< cmp1 failsafe state*/
} epwm_fs_state_t;

/**
 * @brief epwm_dma_cfg.
 */
typedef struct epwm_dma_cfg {
    bool dma_enable;  /**< compare channel dma enable*/
    uint8_t fifo_wml; /**< compare channel fifo water mark level*/
} epwm_dma_cfg_t;

/**
 * @brief pwm_status.
 *
 */
typedef struct pwm_status {
    uint32_t period; /**< PWM period in nano seconds. */
    uint32_t duty1;  /**< PWM cmp0 dutycycle in nano seconds. */
    uint32_t duty2;  /**< PWM cmp1 dutycycle in nano seconds. */
} pwm_state_t;

typedef struct sdrv_epwm_controller sdrv_epwm_controller_t;

typedef void (*pwm_callback_t)(void *arg, uint32_t irq_status);

/**
 * @brief epwm_cmp_cfg.
 */
typedef struct epwm_cmp_cfg {
    /**< pwm cnt sellect  */
    sdrv_epwm_cnt_gx_e cmp_cnt_sel;
    /**< pwm [x]0 or [x]1 output */
    epwm_cmp_out_mode_e chnl_out_mode;
    /**< pwm consecutive mode */
    epwm_cmp_cfg_con_mode_e con_mode;
    /**< pwm cmp_mode */
    sdrv_epwm_cmp_mode_e cmp_mode;
    /**< pwm cmp event mode */
    sdrv_epwm_cmp_event_out_t out_mode;
    /**< pwm cmp00/01/10/11 pulse width */
    sdrv_epwm_cmp_pulse_wid0_t cmp_pulse_wid0;
    /**< pwm ovf00/01 pulse width */
    sdrv_epwm_cmp_pulse_wid1_t cmp_pulse_wid1;
    /**< pwm refresh time */
    uint8_t refresh_intval;
    /**< pwm enable audio */
    bool audio_enable;
    /**< pwm dma enable */
    epwm_dma_cfg_t dma_cfg;
    /**< pwm software reload */
    bool sw_rld_mode;
#if CONFIG_E3L
    /**< pwm center align mode */
    bool center_align_mode;
#endif
} epwm_cmp_cfg_t;

/**
 * @brief ePWM controller.
 *
 */
typedef struct sdrv_epwm {
    /**< module base addr */
    paddr_t base;
    /**< callback function */
    pwm_callback_t cb;
    /**< pwm cmp cfg */
    epwm_cmp_cfg_t cmp_cfg;
    /**< pwm cmp channel id */
    sdrv_epwm_channel_e chnl;
    /**< pwm dual cmp value aux */
    epwm_dual_duty_state_t pre_duty;
    /**< pwm module controller */
    sdrv_epwm_controller_t *controller;
} sdrv_epwm_t;

/* sdrv epwm controller module */
struct sdrv_epwm_controller {
    /**< module base addr */
    uint32_t base;
    /**< module irq id */
    uint8_t irq;
    /**< irq cntg0/g1 */
    /**< when using cntg0 ovf int, must set channel a callback function */
    /**< when using cntg1 ovf int, must set channel b callback function */
    uint32_t irq_id;
    /**< clk src */
    sdrv_epwm_clk_config_src_clk_sel_e clk_src;
    /**< clk_div */
    uint16_t clk_div;
    /**< channel ctrl */
    sdrv_epwm_t *epwm_bank[SDRV_EPWM_CHANNEL_NR];
};

/**
 * @brief sdrv epwm ns transfer to val.
 *
 * @param[in] clk src clk
 * @param[in] ns time val
 * @param[in] div src clk divider
 * @return ns_to_val
 */
uint32_t sdrv_epwm_ns_to_val(uint32_t clk, uint32_t ns, uint32_t div);

/**
 * @brief sdrv epwm ns transfer to val and minus 1.
 *
 * @param[in] clk src clk
 * @param[in] ns time val
 * @param[in] div src clk divider
 * @return ns_to_val_minus_1
 */
uint32_t sdrv_epwm_ns_to_val_1(uint32_t clk, uint32_t ns, uint32_t div);

/**
 * @brief sdrv epwm val transfer to ns.
 *
 * @param[in] clk src clk
 * @param[in] cnt time val
 * @param[in] div src clk divider
 * @return val_to_ns
 */
uint32_t sdrv_epwm_val_to_ns(uint32_t clk, uint32_t val, uint32_t div);

/**
 * @brief compare channel enable.
 *
 * This function enable compare channel
 *
 * @param[in] dev pwm common instance
 *
 */
void sdrv_epwm_cmp_en(sdrv_epwm_t *dev, bool en);

/**
 * @brief counter enable.
 *
 * This function enable CNTG0 or CNT_G1
 *
 * @param[in] dev pwm common instance
 *
 */
void sdrv_epwm_cnt_en(sdrv_epwm_t *dev, bool en);

/**
 * @brief Start ePWM channel output.
 *
 * This function start pwm output
 *
 * @param[in] dev pwm common instance
 *
 */
void sdrv_epwm_start(sdrv_epwm_t *dev);

/**
 * @brief Stop ePWM channel output.
 *
 * This function stop pwm output
 *
 * @param[in] dev pwm common instance
 *
 */
void sdrv_epwm_stop(sdrv_epwm_t *dev);

/**
 * @brief cnt overflow value upload (unit is ns)
 *
 * This function upload counter overflow value
 *
 * @param[in] dev pwm common instance
 * @param[in] period cnt overflow value
 */
void sdrv_epwm_cnt_ovf_upd(sdrv_epwm_t *dev, uint32_t period);

/**
 * @brief compare value upload
 *
 * This function upload compare value
 *
 * @param[in] dev pwm common instance
 * @param[in] state pwm duty and period
 */
status_t sdrv_epwm_cmp_val_upd(sdrv_epwm_t *dev, const pwm_state_t *state);

/**
 * @brief ePWM channel config.
 *
 * This function configure epwm
 *
 * @param[in] dev pwm common instance
 * @param[in] state pwm duty and period
 *
 */
status_t sdrv_epwm_config(sdrv_epwm_t *dev, const pwm_state_t *state);

/**
 * @brief epwm setup callback function
 *
 * @param[in] dev: epwm ctrl instance
 * @param[in] callback: user callback function
 * @return    succeed:0  fail:other
 */
uint8_t sdrv_epwm_set_callback(sdrv_epwm_t *dev, pwm_callback_t callback);

/**
 * @brief configure multi compare mode
 *
 * This function configure multi compare value
 *
 * @param[in] dev pwm common instance
 * @param[in] state pwm duty and period
 * @param[in] multi cmp00/01/10/11 val
 *
 */
status_t sdrv_epwm_multi_cmp_mode(sdrv_epwm_t *dev, pwm_state_t *state,
                                  epwm_multi_cmp_val_t *multi_cmp);

/**
 * @brief configure epwm counter clear
 *
 * This function configure counter clear
 *
 * @param[in] dev pwm common instance
 * @param[in] cnt_clr_trig_sel sellect cnt_clr_trig_polarity
 * @param[in] clr_sel input_clr_sel_trig
 *
 */
void sdrv_epwm_cnt_clr_cfg(sdrv_epwm_t *dev,
                           sdrv_epwm_event_trigger_mode_e cnt_clr_trig_sel,
                           sdrv_epwm_input_sel_trig_e clr_sel);

/**
 * @brief configure epwm counter overflow upload cfg
 *
 * This function configure counter overflow upload
 *
 * @param[in] dev pwm common instance
 * @param[in] set_mode_sel sellect set_mode
 * @param[in] cnt_set_trig_sel sellect cnt_set_trig_polarity
 * @param[in] set_sel input_set_sel_trig
 *
 */
void sdrv_epwm_cnt_ovf_upd_cfg(sdrv_epwm_t *dev,
                               sdrv_epwm_cnt_cfg_set_upd_sel_e set_mode_sel,
                               sdrv_epwm_event_trigger_mode_e cnt_set_trig_sel,
                               sdrv_epwm_input_sel_trig_e set_sel);

/**
 * @brief software trigger
 *
 * This function configure software trigger, such as pulse.
 *
 * @param[in] dev pwm common instance
 * @param[in] tirg sellect trigger mode
 *
 */
void sdrv_epwm_sw_trig(sdrv_epwm_t *dev, sdrv_epwm_sw_trig_ctrl_e trig);

/**
 * @brief configure epwm death time
 *
 * This function configure compare death time
 *
 * @param[in] dev pwm common instance
 * @param[in] prefin_pol prefinial polarity
 * @param[in] dti_val death time value
 *
 */
void sdrv_epwm_dti(sdrv_epwm_t *dev, sdrv_epwm_cmp_prefin_pol_t *prefin_pol,
                   const uint32_t dti_val);

/**
 * @brief configure sse epwm output
 *
 * This function configure compare sse mode
 *
 * @param[in] dev pwm common instance
 * @param[in] sse_cfg sse_mode and edge sellect
 * @param[in] sse_reg_val sse register value
 *
 */
void sdrv_epwm_sse(sdrv_epwm_t *dev, sdrv_epwm_cmp_sse_ctrl_t *sse_cfg,
                   uint32_t sse_reg_val);

/**
 * @brief configure modulation frequency control
 *
 * This function configure compare mfc mode
 *
 * @param[in] dev pwm common instance
 * @param[in] val mfc up times to 2^val times
 *
 */
void sdrv_epwm_mfc(sdrv_epwm_t *dev, uint8_t val);

/**
 * @brief configure compare data format
 *
 * This function configure compare data format, such as 8bit or 16bit.
 *
 * @param[in] dev pwm common instance
 * @param[in] dat_format cmp data formate
 */
void sdrv_epwm_cmp_data_format(
    sdrv_epwm_t *dev, sdrv_epwm_chn_dma_ctrl_cmp_x_dat_format_e dat_format);

/**
 * @brief configure compare dither mode
 *
 * This function configure compare dither mode
 *
 * @param[in] dev pwm common instance
 * @param[in] clip_rslt dither clip_rslt val
 * @param[in] init_offset dither init_offset val
 */
void sdrv_epwm_cmp_dither(sdrv_epwm_t *dev, uint8_t clip_rslt,
                          uint16_t init_offset);

/**
 * @brief configure compare multi channels
 *
 * This function configure compare value of multi channels
 *
 * @param[in] dev pwm common instance
 * @param[in] full_en full channels enable
 */
void sdrv_epwm_cmp_multi_chnl(sdrv_epwm_t *dev, bool full_en);

/**
 * @brief configure compare event/cnt overflow event
 *
 * This function configure compare event or counter event
 *
 * @param[in] dev pwm common instance
 * @param[in] src_sel choose cmp or cnt trigger source
 */
void sdrv_epwm_ce_ctrl(sdrv_epwm_t *dev, bool src_sel);

/**
 * @brief configure dma request except fifo wml trig
 *
 * This function configure pwm trig dma tranferrs with ce
 *
 * @param[in] dev pwm common instance
 * @param[in] sel_trig sellect trigger source
 */
void sdrv_epwm_ce_dma_trig(sdrv_epwm_t *dev, uint8_t sel_trig);

/**
 * @brief filter input fault source signals
 *
 * This function configure fault event source
 *
 * @param[in] dev pwm common instance
 * @param[in] flt_cfg config filter
 */
void sdrv_epwm_fault_flt(sdrv_epwm_t *dev, sdrv_epwm_fault_flt_t *flt_cfg);

/**
 * @brief configure input fault event
 *
 * This function configure compare fault event
 *
 * @param[in] dev pwm common instance
 * @param[in] fault_src config which fault source to trigger
 * @param[in] fault_cfg config fault event
 * @param[in] fs_state cmp failsafe status
 */
void sdrv_epwm_fault_event(sdrv_epwm_t *dev, uint8_t fault_src,
                           sdrv_epwm_cmp_fault_event_ctrl_t *fault_cfg,
                           epwm_fs_state_t *fs_state);

/**
 * @brief input fault event clr
 *
 * This function clear fault status in sticky mode
 *
 * @param[in] dev pwm common instance
 * @param[in] fault_src config which fault source to trigger
 */
void sdrv_epwm_fault_event_clr(sdrv_epwm_t *dev, uint8_t fault_src);

/**
 * @brief cnt overflow value upload directly (unit is cnt val)
 *
 * This function configure counter overflow upload
 *
 * @param[in] dev pwm common instance
 * @param[in] period_cnt counter overflow value
 */
void sdrv_epwm_cnt_ovf_dir_upd(sdrv_epwm_t *dev, uint32_t period_cnt);

/**
 * @brief upload chnl_b/c/d compare value with dual mode 100% duty check(unit is
 * cnt val)
 *
 * This function upload channel_b/c/d value with 100% over two period
 *
 * @param[in] dev pwm common instance
 * @param[in] left_point dual mode left cmp_val
 * @param[in] right_point dual mode right cmp_val
 */
void sdrv_epwm_cmp_bcd_update(sdrv_epwm_t *dev, uint32_t left_point,
                              uint32_t right_point);

/**
 * @brief compare value chnl_b/c/d upload (unit is cnt val)
 *
 * This function upload compare value in dual mode
 *
 * @param[in] dev pwm common instance
 * @param[in] left_point dual mode left cmp_val
 * @param[in] right_point dual mode right cmp_val
 */
void sdrv_epwm_cmp_val_upd_bcd(sdrv_epwm_t *dev, uint32_t left_point,
                               uint32_t right_point);

/**
 * @brief compare value chnl_a upload (unit is cnt val)
 *
 * This function upload channel a value
 *
 * @param[in] dev pwm common instance
 * @param[in] trig_point set trig point to tmux
 */
void sdrv_epwm_cmp_val_upd_a(sdrv_epwm_t *dev, uint32_t trig_point);

/**
 * @brief compare software reload
 *
 * This function set compare value valid immediately
 *
 * @param[in] dev pwm common instance
 */
void sdrv_epwm_cmp_sw_rld(sdrv_epwm_t *dev);

/**
 * @brief compare clr/set configure
 *
 * This function configure compare clear/set source
 *
 * @param[in] dev pwm common instance
 * @param[in] input_sel input sellect
 * @param[in] set_trig cmp set trigger sel
 * @param[in] clr_trig cmp clr trigger sel
 */
void sdrv_epwm_cmp_input(sdrv_epwm_t *dev, sdrv_epwm_input_sel_t *input_sel,
                         uint8_t set_trig, uint8_t clr_trig);

/**
 * @brief configure compare a event id
 *
 * This function configure cmpa0 event id
 *
 * @param[in] dev pwm common instance
 * @param[in] eid_val cmp00/01/10/11 eid
 */
void sdrv_epwm_cmp_a_eid(sdrv_epwm_t *dev, epwm_eid_val_t *eid_val);

/**
 * @brief epwm initial status.
 *
 * This function configure compare initial value
 *
 * @param[in] dev pwm common instance
 * @param[in] cmp0_init cmp0 channel init status
 * @param[in] cmp1_init cmp1 channel init status
 *
 */
void sdrv_epwm_init_sta(sdrv_epwm_t *dev, bool cmp0_init, bool cmp1_init);

/**
 * @brief epwm upload chnl_b/c/d [X]0/1 compare value
 *
 * @param[in] dev pwm common instance
 * @param[in] subchnl choose cmp0 or cmp1
 * @param[in] left_point dual mode left cmp_val
 * @param[in] right_point dual mode right cmp_val
 *
 */
void sdrv_epwm_val_chnl_bcd_upd_split(sdrv_epwm_t *dev,
                                      sdrv_epwm_output_e subchnl,
                                      uint32_t left_point,
                                      uint32_t right_point);

/**
 * @brief epwm upload chnl_b/c/d [X]0/1 compare value
 *
 * @param[in] dev pwm common instance
 * @param[in] cmp0_left_point cmp0 dual mode left cmp_val
 * @param[in] cmp0_right_point cmp0 dual mode right cmp_val
 * @param[in] cmp1_left_point cmp1 dual mode left cmp_val
 * @param[in] cmp1_right_point cmp1 dual mode right cmp_val
 *
 */
void sdrv_epwm_val_chnl_bcd_upd_split_both(sdrv_epwm_t *dev,
                                           uint32_t cmp0_left_point,
                                           uint32_t cmp0_right_point,
                                           uint32_t cmp1_left_point,
                                           uint32_t cmp1_right_point);

#if CONFIG_E3L
/**
 * @brief center align mode enable.
 *
 * This function configure align mode enable
 *
 * @param[in] dev pwm common instance
 * @param[in] en enable/diable center align mode
 *
 */
void sdrv_epwm_center_align_mode_en(sdrv_epwm_t *dev, bool en);

/**
 * @brief compare value chnl_b/c/d upload in center align mode (unit is cnt val)
 *
 * This function configure channel_b/c/d value upload in center align mode
 *
 * @param[in] dev pwm common instance
 * @param[in] cmp_point center align mode cmp_val
 */
void sdrv_epwm_cmp_center_align_val_upd_bcd(sdrv_epwm_t *dev,
                                            uint32_t cmp_point);

#endif

#endif
