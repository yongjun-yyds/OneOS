/**
 * @file partition_disk_io.c
 *
 * Copyright (c) 2021 Semidrive Semiconductor.
 * All rights reserved.
 *
 * Description:
 *
 * Revision History:
 * -----------------
 */
#include <ab_partition_parser.h>
#include <assert.h>
#include <compiler.h>
#include <debug.h>
#include <disk.h>
#include <param.h>
#include <partition_parser.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <types.h>

/**
 * @brief pt_disk_read
 *
 * @param dev disk device
 * @param addr read addr
 * @param dst data destination
 * @param size read size
 * @return int 0 success other if failed
 */
int pt_disk_read(struct disk_dev *dev, disk_addr_t addr, uint8_t *dst,
                 disk_size_t size)
{
    struct disk_info *info = dev->info;

#if CONFIG_PARTITION_BLOCK_IO_MODE

    if (NULL == dev || NULL == dev->info) {
        PT_ERROR("no disk device node\n");
        return -1;
    }

    if (!dev->block_size || !IS_ALIGNED(addr, dev->block_size) ||
        !IS_ALIGNED(size, dev->block_size)) {
        PT_ERROR("addr 0x%llx or size 0x%llx not aligned to block_size = %d\n",
                 addr, size, dev->block_size);
        return -1;
    }

    if (!IS_ALIGNED(dst, dev->info->mem_align_size)) {
        PT_ERROR("mem addresses 0x%p not aligned to 0x%x\n", dst,
                 dev->info->mem_align_size);
        return -1;
    }

    if (info->disk_ops->disk_read_block)
        return info->disk_ops->disk_read_block(
            info, dst, addr / (dev->block_size), size / (dev->block_size),
            dev->block_size);
    else
        return -DISK_ERROR_NO_FUN;

#else

    if (info->disk_ops->disk_read)
        return info->disk_ops->disk_read(info, dst, addr, size);
    else
        return -DISK_ERROR_NO_FUN;

#endif
}

/**
 * @brief dl_disk_write
 *
 * @param dev disk device
 * @param addr write addr
 * @param src data source
 * @param size write size
 * @return int 0 success other if failed
 */
int pt_disk_write(struct disk_dev *dev, disk_addr_t addr, uint8_t *src,
                  disk_size_t size)
{
    struct disk_info *info = dev->info;

#if CONFIG_PARTITION_BLOCK_IO_MODE

    if (NULL == dev || NULL == dev->info) {
        PT_ERROR("no disk device node\n");
        return -1;
    }

    if (!dev->block_size || !IS_ALIGNED(addr, dev->block_size) ||
        !IS_ALIGNED(size, dev->block_size)) {
        PT_ERROR("addr 0x%llx or size 0x%llx not aligned to block_size = %d\n",
                 addr, size, dev->block_size);
        return -1;
    }

    if (!IS_ALIGNED(src, dev->info->mem_align_size)) {
        PT_ERROR("mem addresses 0x%p not aligned to 0x%x\n", src,
                 dev->info->mem_align_size);
        return -1;
    }

    if (info->disk_ops->disk_write_block)
        return info->disk_ops->disk_write_block(
            info, src, addr / (dev->block_size), size / (dev->block_size),
            dev->block_size);
    else
        return -DISK_ERROR_NO_FUN;

#else

    if (info->disk_ops->disk_write)
        return info->disk_ops->disk_write(info, src, addr, size);
    else
        return -DISK_ERROR_NO_FUN;

#endif
}

/**
 * @brief dloader disk erase
 *
 * @param dev disk device
 * @param addr erase addr
 * @param size erase size
 * @return int 0 success
 */
int pt_disk_erase(struct disk_dev *dev, disk_addr_t addr, disk_size_t size)
{
    struct disk_info *info = dev->info;

#if CONFIG_PARTITION_BLOCK_IO_MODE

    if (NULL == dev || NULL == dev->info) {
        PT_ERROR("no disk device node\n");
        return -1;
    }

    if (strstr(dev->info->disk_name, "flash")) {
        if (!dev->info->erase_size ||
            !IS_ALIGNED(addr, dev->info->erase_size) ||
            !IS_ALIGNED(size, dev->info->erase_size)) {
            PT_ERROR(
                "addr 0x%llx or size 0x%llx not aligned to erase_size = %d\n",
                addr, size, dev->info->erase_size);
            return -1;
        }
        if (info->disk_ops->disk_erase_group)
            return info->disk_ops->disk_erase_group(
                info, addr / (dev->info->erase_size),
                size / (dev->info->erase_size), info->erase_size,
                DISK_ERASE_DEFAULT);
        else
            return -DISK_ERROR_NO_FUN;
    } else {

        if (info->disk_ops->disk_erase)
            return info->disk_ops->disk_erase(info, addr, size,
                                              DISK_ERASE_DEFAULT);
        else
            return -DISK_ERROR_NO_FUN;
    }

#else

    if (info->disk_ops->disk_erase)
        return info->disk_ops->disk_erase(info, addr, size, DISK_ERASE_DEFAULT);
    else
        return -DISK_ERROR_NO_FUN;

#endif
}

/**
 * @brief pt_disk_get_blocksize
 *
 * @param dev disk device
 * @return uint32_t block size
 */
uint32_t pt_disk_get_blocksize(struct disk_dev *dev) { return dev->block_size; }

/**
 * @brief pt_disk_get_erasesize
 *
 * @param dev disk device
 * @return uint32_t ersase size
 */
uint32_t pt_disk_get_erasesize(struct disk_dev *dev)
{
    return dev->info->erase_size;
}

/**
 * @brief pt_disk_get_capacity
 *
 * @param dev disk device
 * @return uint32_t disk capacity size
 */
uint64_t pt_disk_get_capacity(struct disk_dev *dev)
{
    return dev->info->disk_size;
}
