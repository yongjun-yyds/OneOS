/**
 * @file fls.h
 *
 * Copyright (c) 2022 Semidrive Semiconductor.
 * All rights reserved.
 *
 * Description:
 *
 * Revision History:
 * -----------------
 */
#ifndef FLASH_FLS_H_
#define FLASH_FLS_H_

#include <sdrv_spi_nor.h>
#include <sdrv_xspi.h>


typedef enum {
    FLS_XSPI1_PORTA = 0,
    FLS_XSPI1_PORTB,
    FLS_XSPI2_PORTA,
    FLS_XSPI2_PORTB,
    FLS_CONTROLLER_ID_MAX,
} Fls_Controller_ID_t;

typedef struct {
    bool octal_dtr_en;
    uint8_t cinst_type;   /* 0:Single | 1:Dual | 2:Quad | 3:Octal */
} Fls_Flash_Info_t;

/******************************************************************************
** Service name        : sdrv_norflash_init                                   **
** Parameters (in)     : ConfigPtr  Pointer to flash driver configuration set **
** Parameters (in)     : Number of flash bound to the controller              **
** Parameters (in)     : Enable or disable xip mode                           **
** Return value        : struct spi_nor_host*, failed is NULL                 **
** Description        : Initializes the Flash Driver                          **
******************************************************************************/
struct spi_nor_host* sdrv_norflash_init(struct spi_nor_config *config,
        uint8_t cs_num, bool xip_en);

/******************************************************************************
** Service name        : sdrv_norflash_deinit                                 **
** Parameters (in)     : ConfigPtr  Pointer to xspi host                      **
** Return value        : NONE                                                 **
** Description        : DeInitializes the Host and the Flash attached         **
******************************************************************************/
void sdrv_norflash_deinit(struct spi_nor_host* host);

/******************************************************************************
** Service name        : sdrv_norflash_deinit_all                             **
** Return value        : NONE                                                 **
** Description        : DeInitializes all Host and the Flash                  **
******************************************************************************/
void sdrv_norflash_deinit_all(void);

/******************************************************************************
** Service name        : get_flashhandler_by_id                               **
** Parameters (in)     : The index of host(Fls_Controller_ID_t)               **
** Parameters (in)     : The index of flash bound to the controller           **
** Return value        : struct spi_nor*, failed is NULL                      **
** Description        : Get the Flash handler                                 **
******************************************************************************/
struct spi_nor* get_flashhandler_by_id(Fls_Controller_ID_t port_id, uint8_t cs_id);

/******************************************************************************
** Service name        : get_flashhandler_by_id                               **
** Parameters (in)     : The index of host(Fls_Controller_ID_t)               **
** Parameters (in)     : The mask of rfd region                               **
** Return value        : 0, failed is !0                                      **
** Description        : Get the Flash handler                                 **
******************************************************************************/
int sdrv_norflash_enable_rfd(struct spi_nor_host* host, uint8_t mask);

/******************************************************************************
** Service name        : get_norflash_direct_base                             **
** Parameters (in)     : The index of host(Fls_Controller_ID_t)               **
** Parameters (out)    : The direct base of flash                             **
** Return value        : 0, failed is !0                                      **
** Description        : Get the Flash handler                                 **
******************************************************************************/
int get_norflash_direct_base(struct spi_nor_host* host, addr_t *direct_base);

/******************************************************************************
** Service name        : sdrv_norflash_swap_cs                                **
** Parameters (in)     : The index of host(Fls_Controller_ID_t)               **
** Parameters (in)     : The order of cs (Positive:0, Reverse:1)              **
** Return value        : 0, failed is !0                                      **
** Description        : Swap Flash cs sequence                                **
******************************************************************************/
int sdrv_norflash_swap_cs(struct spi_nor_host* host, bool is_reverse);

/******************************************************************************
** Service name        : sdrv_norflash_swap_cs                                **
** Parameters (in)     : The index of host(Fls_Controller_ID_t)               **
** Parameters (in)     : The index of cs                                      **
** Parameters (out)    : The infomation of flash                              **
** Return value        : 0, failed is !0                                      **
** Description        : Swap Flash cs sequence                                **
******************************************************************************/
int sdrv_norflash_get_flash_info(struct spi_nor_host* host, uint8_t cs_id,
    Fls_Flash_Info_t* info);

#endif