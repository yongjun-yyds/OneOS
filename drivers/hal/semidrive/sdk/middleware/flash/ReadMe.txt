1. The path to the required header file:
    \middleware\flash\include
2. Dependent source:
    \drivers\source\dma\sdrv_dma.c
    drivers\source\vic\irq.c
    \devices\$PART_ID$\reset_ip.c
    \drivers\source\reset\sdrv_rstgen.c
    \drivers\source\reset\sdrv_rstgen_hw.h
    \drivers\source\clk\sdrv_ckgen.c
    \drivers\source\spi_nor\sdrv_hyperbus.c
    \drivers\source\spi_nor\sdrv_spi_nor.c
    \drivers\source\spi_nor\sdrv_xspi.c
3. -DCONFIG_IRQ for irq mode
4. -DCONFIG_XSPI_ENABLE_DMA & -DCONFIG_IRQ & -DXSPI_USE_DIRECT_MODE=0 for dma mode