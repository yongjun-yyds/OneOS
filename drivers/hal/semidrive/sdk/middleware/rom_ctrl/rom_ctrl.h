/**
 * @file rom_ctrl.h
 * @brief rom control common api
 *
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef ROM_CTRL_H_
#define ROM_CTRL_H_

#include <types.h>
#include <part.h>

typedef enum sdrv_rom_ctrl_boot_mode {
    SDRV_ROM_CTRL_BOOT_MODE_BOOT_PIN,
    SDRV_ROM_CTRL_BOOT_MODE_FLASH,
    SDRV_ROM_CTRL_BOOT_MODE_EMMC,
#if !CONFIG_E3L
    SDRV_ROM_CTRL_BOOT_MODE_SD2,
#endif
    SDRV_ROM_CTRL_BOOT_MODE_USB,
} sdrv_rom_ctrl_boot_mode_e;

/**
 * @brief Set rom control boot pin mode flag.
 *
 * This function is used to set rom control boot pin mode flag, call this
 * function and processor software reset, processor will boot from the
 * set mode.
 *
 * @param [in] boot_mode boot mode
 */
void sdrv_rom_ctrl_set_boot_pin_flag(sdrv_rom_ctrl_boot_mode_e boot_mode);

/**
 * @brief Update rom control data.
 */
void sdrv_rom_ctrl_data_update(void);

#endif /* ROM_CTRL_H_ */
