/*
* hal_sys.c
*@brief hal sys function file.
*
* Copyright (c) 2012 Semidrive Semiconductor.
* All rights reserved.
*
* Description:
*
* Revision History:
* -----------------
* 2022/09/19 create this file
*/
#include "types.h"
#include "armv7-r/cache.h"
#include "hal_comm_inner.h"
#include "hal_sys.h"


int HAL_SYS_CleanCacheRange(const HAL_SYS_CacheInfo *info)
{
    CHECK_NULLPTR_RETURN(HAL_MOD_SYS, info, HAL_SYS_NULL_PTR);
    CHECK_NULLPTR_RETURN(HAL_MOD_SYS, info->buf, HAL_SYS_NULL_PTR);

    if (info->rect.w == 0 || info->rect.h == 0 || info->stride == 0) {
        HAL_LOG_E(HAL_MOD_SYS, "rect w[%u] or h[%u] stride[%u] is invalid!\n",
            info->rect.w, info->rect.h, info->buf, info->stride);
        return HAL_SYS_ERR_PARAM;
    }

    int bpp = COMM_getFmtBpp(info->fmt);
    if (bpp < 0) {
        return HAL_SYS_ERR_PARAM;
    }

    HAL_LOG_D(HAL_MOD_SYS, "cache :buf[%#x] stride[%u] format[%d], rect[%u, %u, %u, %u] ",
        info->buf, info->stride, info->fmt, info->rect.x, info->rect.y, info->rect.w, info->rect.h);

    // This function should be called when the CPU is accessing the drawing device.
    for(int i = 0; i < info->rect.h; i++) {
        addr_t buf = (addr_t)(info->buf + info->rect.y * info->stride + info->rect.x * bpp + i * info->stride);
        uint32_t len = info->rect.w * bpp;
        arch_clean_cache_range(buf, len);
    }

    return HAL_SUCCESS;
}

int  HAL_SYS_InvalidateCacheRange(const HAL_SYS_CacheInfo *info)
{
    CHECK_NULLPTR_RETURN(HAL_MOD_SYS, info, HAL_SYS_NULL_PTR);
    CHECK_NULLPTR_RETURN(HAL_MOD_SYS, info->buf, HAL_SYS_NULL_PTR);

    if (info->rect.w == 0 || info->rect.h == 0 || info->stride == 0) {
        HAL_LOG_E(HAL_MOD_SYS, "rect w[%u] or h[%u] or stride[%u] is invalid!\n",
            info->rect.w, info->rect.h, info->buf, info->stride);
        return HAL_SYS_ERR_PARAM;
    }

    int bpp = COMM_getFmtBpp(info->fmt);
    if (bpp < 0) {
        return HAL_SYS_ERR_PARAM;
    }
    HAL_LOG_D(HAL_MOD_SYS, "cache :buf[%#x] stride[%u] format[%d], rect[%u, %u, %u, %u] ",
        info->buf, info->stride, info->fmt, info->rect.x, info->rect.y, info->rect.w, info->rect.h);

    // This function should be called when the CPU is accessing the drawing device.
    for(int i = 0; i < info->rect.h; i++) {
        addr_t buf = (addr_t)(info->buf + info->rect.y * info->stride + info->rect.x * bpp + i * info->stride);
        uint32_t len = info->rect.w * bpp;
        arch_invalidate_cache_range(buf, len);
    }

    return HAL_SUCCESS;
}

int HAL_SYS_CleanInvalidateCacheRange(const HAL_SYS_CacheInfo *info)
{
    CHECK_NULLPTR_RETURN(HAL_MOD_SYS, info, HAL_SYS_NULL_PTR);
    CHECK_NULLPTR_RETURN(HAL_MOD_SYS, info->buf, HAL_SYS_NULL_PTR);

    if (info->rect.w == 0 || info->rect.h == 0 || info->stride == 0) {
        HAL_LOG_E(HAL_MOD_SYS, "rect w[%u] or h[%u] or stride[%u] is invalid!\n",
            info->rect.w, info->rect.h, info->buf, info->stride);
        return HAL_SYS_ERR_PARAM;
    }
    HAL_LOG_D(HAL_MOD_SYS, "cache :buf[%#x] stride[%u] format[%d], rect[%u, %u, %u, %u] ",
        info->buf, info->stride, info->fmt, info->rect.x, info->rect.y, info->rect.w, info->rect.h);

    int bpp = COMM_getFmtBpp(info->fmt);
    if (bpp < 0) {
        return HAL_SYS_ERR_PARAM;
    }

    // This function should be called when the CPU is accessing the drawing device.
    for(int i = 0; i < info->rect.h; i++) {
        addr_t buf = (addr_t)(info->buf + info->rect.y * info->stride + info->rect.x * bpp + i * info->stride);
        uint32_t len = info->rect.w * bpp;
        arch_clean_invalidate_cache_range(buf, len);
    }

    return HAL_SUCCESS;
}

void HAL_SYS_CleanInvalidateCacheAll(void)
{
    arch_clean_invalidate_dcache_all();
}
