/*
* hal_asw.c
*@brief hal asw function file.
*
* Copyright (c) 2012 Semidrive Semiconductor.
* All rights reserved.
*
* Description:
*
* Revision History:
* -----------------
* 2022/09/19 create this file
*/
#include <stdlib.h>
#include <math.h>
#include "types.h"
#include "hal_comm_define.h"
#include "hal_comm_inner.h"
#include "hal_errno.h"
#ifdef CONFIG_ASW
#include "asw/asw.h"
#endif
#include "hal_asw.h"

#define ASW_MAX_ANGLE 180 /* 180 ~360 filp */
#define ASW_ROTATE_MAX_ANGLE 5
#define ASW_ROTATE_IMG_CNT  (ASW_MAX_ANGLE / (ASW_ROTATE_MAX_ANGLE * 2) ) /* 10: 0~20 */
static COMM_RotateContext g_aswContext = {0};
static bool g_ratateChanged = true;
static COMM_RotateBuf g_rotateBuf[ASW_ROTATE_IMG_CNT + 1] = { NULL, NULL };  /* add tmp buf */

static int ASW_TransformFmtType(HAL_Format fmtType, int *fmt)
{
 #ifdef CONFIG_ASW
    switch (fmtType) {
        case HAL_FMT_A8:
            *fmt = FMT_MONOTONIC_8BIT;
            break;
        case HAL_FMT_RGB565:
            *fmt = FMT_RGB565;
            break;
        case HAL_FMT_ARGB1555:
            *fmt = FMT_RGBA5551;
            break;
        case HAL_FMT_ARGB8888:
            *fmt = FMT_ARGB8888;
            break;
        default:
            HAL_LOG_E(HAL_MOD_ASW, "can't support this fmt:%d!\n", fmtType);
            return HAL_ASW_ERR_PARAM;
    }
    return HAL_SUCCESS;
#else
    HAL_LOG_E(HAL_MOD_ASW, "CONFIG_ASW not open, don't support asw!\n");
    return HAL_ASW_NOT_SUPPORT;
#endif
}

/* The color order of asw and dc is different and needs to be converted
    @ dc color  : A:bit[31:24], R:bit[23:16], G:bit[15:8], B:bit[7:0] ARGB
    @ asw input bg color : A:bit[31:24], B:bit[23:16], G:bit[15:8], R:bit[7:0] ABGR
    @ color : input color, A:bit[31:24], R:bit[23:16], G:bit[15:8], B:bit[7:0]
    @ halFmt : HAL output format
    @ fmt: ASW putput format
 */
static uint32_t ASW_TransformBgColor(HAL_Format halFmt, uint32_t color)
{
 #ifdef CONFIG_ASW
    uint32_t out_color = color;
    switch (halFmt) {
        case HAL_FMT_RGB565:
            /* ASW output BGR: B:bit[23:16], G:bit[15:8], R:bit[7:0]
               The alpha value is ignored andConsistent position does not need to be moved,
               and the RGB is in its original order */
            return color;
        case HAL_FMT_ARGB1555:
            /* ASW input ABGR, output ABGR: A:bit[31:24], B:bit[23:16], G:bit[15:8], R:bit[7:0]
               To get ARGB data, A:bit[31:24], B:bit[23:16], G:bit[15:8], B:bit[7:0]
               Consistent position does not need to be moved.
             */
            return color;
        case HAL_FMT_ARGB8888:
            /* asw bg color RGBA, DC color ARGB
                @ input in bg color  : A:bit[31:24], R:bit[23:16], G:bit[15:8], B:bit[7:0] ARGB
                @ asw input bg color : A:bit[31:24], B:bit[23:16], G:bit[15:8], R:bit[7:0] ABGR
                @ asw out bg color   : B:bit[31:24], G:bit[23:16], R:bit[15:8], A:bit[7:0] BGRA
                @ dc out color       : A:bit[31:24], R:bit[23:16], G:bit[15:8], B:bit[7:0] ARGB
                input color       -> asw input color:   A->A R->B G->G B->R
                asw input color   -> asw out bg color:  B->B G->G R->R A->A
                input color       -> asw out bg color:  R->B G->G B->R A->A
                asw out bg color  -> dc out color:      B->A G->R R->G A->B
                input color       -> dc out color:      R->A G->R B->G A->B
                You need to convert ARGB to BARG */
            uint32_t out_color = (((color >> 24) & 0xff) << 16) | (((color >> 16) & 0xff) << 8) |
                                 ((color >> 8) & 0xff) | ((color & 0xff) << 24);
            return out_color;
        default :
            return color;
    }
#else
    HAL_LOG_E(HAL_MOD_ASW, "CONFIG_ASW not open, don't support asw!\n");
    return HAL_ASW_NOT_SUPPORT;
#endif
}

/* The color order of asw and dc is different and needs to be converted
   ASW in ARGB: B:bit[31:24], G:bit[23:16], R:bit[15:8], A:bit[7:0]
   DC ARGB : A:bit[31:24], R:bit[23:16], G:bit[15:8], B:bit[7:0]
   @ color : input color, A:bit[31:24], R:bit[23:16], G:bit[15:8], B:bit[7:0]
   @ halFmt : HAL output format
   @ fmt: ASW putput format
*/
static uint32_t ASW_TransformColor(HAL_Format halFmt, uint32_t color)
{
 #ifdef CONFIG_ASW
    uint32_t outColor = color;
    switch (halFmt) {
        case HAL_FMT_RGB565:
            /* ASW output RGB: B:bit[23:16], G:bit[15:8], R:bit[7:0]
               The alpha value is ignored and needs to be moved to [7:0], and the RGB is in its original order */
            uint32_t outColor = ((color >> 24) & 0xff) | (((color >> 16) & 0xff) << 24) |
                                (((color >> 8) & 0xff) << 16) | ((color & 0xff) << 8);
            return outColor;
        case HAL_FMT_ARGB1555:
            /* ASW output RGBA: A:bit[31:24], B:bit[23:16], G:bit[15:8], R:bit[7:0]
               To get ARGB data, A:bit[31:24], B:bit[23:16], G:bit[15:8], B:bit[7:0]
               press RGBA input can be. R:bit[31:24], G:bit[23:16], B:bit[15:8], A:bit[7:0]
             */
            outColor = ((color >> 24) & 0xff) | (((color >> 16) & 0xff) << 24) |
                                (((color >> 8) & 0xff) << 16) | ((color & 0xff) << 8);
            return outColor;
        case HAL_FMT_ARGB8888:
            /* There is no format conversion within ASWs, and no conversion is required from ASWs to DC */
            return color;
        default:
            return color;
    }
#else
    HAL_LOG_E(HAL_MOD_ASW, "CONFIG_ASW not open, don't support asw!\n");
    return HAL_ASW_NOT_SUPPORT;
#endif
}

static void ASW_DeleteRotateBuf(uint32_t rotateImgCnt)
{
    for (int i = 0; i < rotateImgCnt; i++) {
        if (g_rotateBuf[i].buf != NULL) {
            free((void *)g_rotateBuf[i].buf);
            g_rotateBuf[i].bufLen = 0;
            g_rotateBuf[i].buf = NULL;
        }
    }

    /* free tmp buf */
    if (g_rotateBuf[ASW_ROTATE_IMG_CNT].buf != NULL) {
        free((void *)g_rotateBuf[ASW_ROTATE_IMG_CNT].buf);
        g_rotateBuf[ASW_ROTATE_IMG_CNT].bufLen = 0;
        g_rotateBuf[ASW_ROTATE_IMG_CNT].buf = NULL;
    }
}

static int ASW_MallocRotateBuf(const HAL_RotateInfo *info, uint32_t rotateImgCnt, uint32_t sideLen, int srcBpp, int dstBpp)
{
    /* malloc tmp buf */
    if ((g_rotateBuf[ASW_ROTATE_IMG_CNT].buf == NULL) || (sideLen * sideLen * dstBpp > g_rotateBuf[ASW_ROTATE_IMG_CNT].bufLen)) {
        if (g_rotateBuf[ASW_ROTATE_IMG_CNT].buf != NULL) {
            free((void *)g_rotateBuf[ASW_ROTATE_IMG_CNT].buf);
        }
        g_rotateBuf[ASW_ROTATE_IMG_CNT].bufLen = sideLen * sideLen * dstBpp;
        g_rotateBuf[ASW_ROTATE_IMG_CNT].buf = (uint8_t*)pvPortMallocAligned(g_rotateBuf[ASW_ROTATE_IMG_CNT].bufLen, 0x1000);
        if (g_rotateBuf[ASW_ROTATE_IMG_CNT].buf == NULL) {
            HAL_LOG_E(HAL_MOD_ASW, "pvPortMallocAligned rotate buf, len:%d error!\n",g_rotateBuf[ASW_ROTATE_IMG_CNT].bufLen);
            g_rotateBuf[ASW_ROTATE_IMG_CNT].bufLen = 0;
            g_rotateBuf[ASW_ROTATE_IMG_CNT].buf = NULL;
            ASW_DeleteRotateBuf(rotateImgCnt);
            return HAL_ASW_NO_MEM;
        }
    }

    /* Save the image rotated degrees */
    for (int i = 0; i < rotateImgCnt; i++) {
        int bpp = dstBpp;
        if (i == 0) {
            bpp = srcBpp;
        }
        if ((g_rotateBuf[i].buf == NULL) || (sideLen * sideLen * bpp > g_rotateBuf[i].bufLen)) {
            if (g_rotateBuf[i].buf != NULL) {
                free((void *)g_rotateBuf[i].buf);
            }

            g_rotateBuf[i].bufLen = sideLen * sideLen * bpp;
            g_rotateBuf[i].buf = (uint8_t*)pvPortMallocAligned(g_rotateBuf[i].bufLen, 0x1000);
            if (g_rotateBuf[i].buf == NULL) {
                HAL_LOG_E(HAL_MOD_ASW, "pvPortMallocAligned rotate buf, len:%d error!\n",g_rotateBuf[i].bufLen);
                g_rotateBuf[i].bufLen = 0;
                g_rotateBuf[i].buf = NULL;
                ASW_DeleteRotateBuf(rotateImgCnt);
                return HAL_ASW_NO_MEM;
            }
            g_ratateChanged = true;
        }
    }

    return HAL_SUCCESS;
}

static int ASW_SaveRotateImg(const HAL_RotateInfo *info, uint32_t maxRotateAngle, uint32_t sideLen, int srcBpp, int dstBpp)
{
    /*  need to re-save the rotated image  */
    if (!g_ratateChanged && (info->srcBuf == g_aswContext.rotateInfo.srcBuf) && (info->srcRect.x == g_aswContext.rotateInfo.srcRect.x) && (info->srcRect.y == g_aswContext.rotateInfo.srcRect.y) &&
        (info->srcRect.w == g_aswContext.rotateInfo.srcRect.w) && (info->srcRect.h == g_aswContext.rotateInfo.srcRect.h) &&
        (info->srcFmt == g_aswContext.rotateInfo.srcFmt) && (info->dstFmt == g_aswContext.rotateInfo.dstFmt) && (info->bgColor == g_aswContext.rotateInfo.bgColor)) {
        HAL_LOG_I(HAL_MOD_ASW, "the rotate img is not change!");
        return HAL_SUCCESS;
    }
    uint32_t rotateImgCnt = ASW_MAX_ANGLE / (maxRotateAngle * 2);

    int srcFmt = 0;
    int ret = ASW_TransformFmtType(info->srcFmt, &srcFmt);
    CHECK_RETURN(HAL_MOD_ASW, ret, "transform fmt type");
    int dstFmt = 0;
    ret = ASW_TransformFmtType(info->dstFmt, &dstFmt);
    CHECK_RETURN(HAL_MOD_ASW, ret, "transform fmt type");

    /* clean buf, save the image rotated degrees */
    sdrv_asw_fill_rect(srcFmt, 0, sideLen, sideLen, 0, 0, (int)g_rotateBuf[0].buf, sideLen * srcBpp);
    sdrv_asw_fill_rect(dstFmt, 0, sideLen, sideLen, 0, 0, (int)g_rotateBuf[ASW_ROTATE_IMG_CNT].buf, sideLen * dstBpp);
    for (int i = 1; i < rotateImgCnt; i++) {
       sdrv_asw_fill_rect(dstFmt, 0, sideLen, sideLen, 0, 0, (int)g_rotateBuf[i].buf, sideLen * dstBpp);
    }

    int copy_x = (sideLen - info->srcRect.w) / 2;
    int copy_y = (sideLen - info->srcRect.h) / 2;
    for (int i = 0; i < info->srcRect.h; i++) {
        uint8_t *dstBuf = (g_rotateBuf[0].buf + copy_y * sideLen * srcBpp + copy_x * srcBpp) + i * sideLen * srcBpp;
        uint8_t *srcBuf = (info->srcBuf + info->srcRect.y * info->srcStride + info->srcRect.x * srcBpp) + i * info->srcStride;
        memcpy(dstBuf, srcBuf, info->srcRect.w * srcBpp);
    }
    arch_clean_cache_range(g_rotateBuf[0].buf, sideLen * srcBpp * sideLen);

    ASW_RotateInfo rotateInfo = {0};
    rotateInfo.rotate.srcRect.x = 0;
    rotateInfo.rotate.srcRect.y = 0;
    rotateInfo.rotate.srcRect.w = sideLen;
    rotateInfo.rotate.srcRect.h = sideLen;
    rotateInfo.rotate.srcFmt = info->srcFmt;
    rotateInfo.rotate.srcStride = sideLen * srcBpp;
    rotateInfo.rotate.srcBuf = g_rotateBuf[0].buf;

    rotateInfo.rotate.dstWidth = sideLen;
    rotateInfo.rotate.dstHeight = sideLen;
    rotateInfo.rotate.dstFmt = info->dstFmt;
    rotateInfo.rotate.dstStride = sideLen * dstBpp;

    rotateInfo.rotate.bgColor = info->bgColor;
    rotateInfo.tmpBuf = g_rotateBuf[ASW_ROTATE_IMG_CNT].buf;
    rotateInfo.tmpStride = sideLen * dstBpp;

    /* save the image rotated degrees */
    for (int i = 1; i < rotateImgCnt; i++) {
        rotateInfo.rotate.angle = (maxRotateAngle * 2) * i;
        rotateInfo.rotate.dstBuf = g_rotateBuf[i].buf;
        ret = ASW_Rotate(&rotateInfo);
        CHECK_RETURN(HAL_MOD_ASW, ret, "ASW_Rotate");
    }

    g_ratateChanged = false;
    g_aswContext.rotateInfo = *info;

    return HAL_SUCCESS;
}

static int ASW_RotateAndCache(const HAL_RotateInfo *info, uint32_t maxRotateAngle)
{
#ifdef CONFIG_ASW
    int ret = HAL_SUCCESS;

    HAL_LOG_D(HAL_MOD_ASW, "src:rect[%u, %u, %u, %u] buf[%#x] stride[%u] format[%d]",
        info->srcRect.x, info->srcRect.y, info->srcRect.w, info->srcRect.h,
        info->srcBuf, info->srcStride, info->srcFmt);
    HAL_LOG_D(HAL_MOD_ASW, "dst:width[%u] height[%u] buf[%#x] stride[%u] format[%d] angle[%lf] bgColor[%#x]",
        info->dstWidth, info->dstHeight, info->dstBuf, info->dstStride, info->dstFmt, info->angle, info->bgColor);

    /* Apply buf with the diagonal of the picture as the side length */
    uint32_t rotateWidth = (uint32_t)sqrt(info->srcRect.w * info->srcRect.w + info->srcRect.h * info->srcRect.h) + 1;
    /* align 8 */
    rotateWidth = rotateWidth % 8 ? ((rotateWidth / 8 + 1) * 8) : rotateWidth;

    /* The target area should not be smaller than the diagonal of the picture */
    if (rotateWidth > info->dstWidth || rotateWidth > info->dstHeight) {
        HAL_LOG_E(HAL_MOD_ASW, "dst: width[%u] and height[%u] Must be greater than %u!\n",info->dstWidth, info->dstHeight, rotateWidth);
        return HAL_ASW_ERR_PARAM;
    }

    int srcBpp = COMM_getFmtBpp(info->srcFmt);
    if (srcBpp < 0){
        return HAL_ASW_ERR_PARAM;
    }

    int dstBpp = COMM_getFmtBpp(info->dstFmt);
    if (dstBpp < 0){
        return HAL_ASW_ERR_PARAM;
    }

    HAL_LOG_I(HAL_MOD_ASW, "rotate:width[%u] height[%u] srcBpp[%d] dstBpp[%d]", rotateWidth, rotateWidth, srcBpp, dstBpp);

    if ((maxRotateAngle < ASW_ROTATE_MAX_ANGLE) || (ASW_MAX_ANGLE % maxRotateAngle != 0)) {
        HAL_LOG_E(HAL_MOD_ASW, "maxRotateAngle[%d] Must be greater than %u and It has to be divisible by 180!\n",maxRotateAngle, ASW_ROTATE_MAX_ANGLE);
        return HAL_ASW_ERR_PARAM;
    }

    uint32_t rotateImgCnt = ASW_MAX_ANGLE / (maxRotateAngle * 2);

    /* Save the image rotated degrees */
    ret = ASW_MallocRotateBuf(info, rotateImgCnt, rotateWidth, srcBpp, dstBpp);
    CHECK_RETURN(HAL_MOD_ASW, ret, "transform fmt type");

    if (!g_ratateChanged && g_aswContext.maxRotateAngle != maxRotateAngle) {
        g_ratateChanged = true;
        g_aswContext.maxRotateAngle = maxRotateAngle;
    }

    /* clean buf, save the image rotated degrees */
    ret = ASW_SaveRotateImg(info, maxRotateAngle, rotateWidth, srcBpp, dstBpp);
    CHECK_GOTO(HAL_MOD_ASW, ret, "ASW_SaveRotateImg", GOTO);

    int posX = 0;
    int posY = 0;
    if ((rotateWidth != info->dstWidth) || (rotateWidth != info->dstHeight)) {
        posX = (info->dstWidth - rotateWidth) / 2;
        posY = (info->dstHeight - rotateWidth) / 2;
    }

    HAL_LOG_I(HAL_MOD_ASW, "out rotate rect:posX[%d] posY[%d]", posX, posY);

    ASW_RotateInfo rotateInfo = {0};
    rotateInfo.rotate.srcRect.x = 0;
    rotateInfo.rotate.srcRect.y = 0;
    rotateInfo.rotate.srcRect.w = rotateWidth;
    rotateInfo.rotate.srcRect.h = rotateWidth;
    rotateInfo.rotate.srcFmt = info->srcFmt;
    rotateInfo.rotate.srcStride = rotateWidth * srcBpp;

    rotateInfo.rotate.dstWidth = rotateWidth;
    rotateInfo.rotate.dstHeight = rotateWidth;
    rotateInfo.rotate.dstFmt = info->dstFmt;
    rotateInfo.rotate.dstBuf = (info->dstBuf + posY * info->dstStride + posX * dstBpp);
    rotateInfo.rotate.dstStride = info->dstStride;

    rotateInfo.rotate.bgColor = info->bgColor;
    rotateInfo.tmpBuf = g_rotateBuf[ASW_ROTATE_IMG_CNT].buf;
    rotateInfo.tmpStride = rotateWidth * dstBpp;


    rotateInfo.rotate.angle = info->angle;

    while (rotateInfo.rotate.angle < 0) {
        rotateInfo.rotate.angle += 360;
    }

    while (rotateInfo.rotate.angle > 360) {
        rotateInfo.rotate.angle -= 360;
    }

    if ((rotateInfo.rotate.angle <= maxRotateAngle || rotateInfo.rotate.angle >= 360 - maxRotateAngle) ||
       ((ASW_MAX_ANGLE - maxRotateAngle <= rotateInfo.rotate.angle) && (rotateInfo.rotate.angle <= ASW_MAX_ANGLE + maxRotateAngle))) {
        rotateInfo.rotate.srcBuf = g_rotateBuf[0].buf;
        ret = ASW_Rotate(&rotateInfo);
        CHECK_GOTO(HAL_MOD_ASW, ret, "ASW_Rotate", GOTO);
        return HAL_SUCCESS;
    }

    for (int i = 1; i < rotateImgCnt; i++) {
        if (((maxRotateAngle + (maxRotateAngle * 2 * (i - 1)) <= rotateInfo.rotate.angle)  &&
            (rotateInfo.rotate.angle <= maxRotateAngle + (maxRotateAngle * 2 * i))) ||
            ((rotateInfo.rotate.angle >= ASW_MAX_ANGLE + (maxRotateAngle + maxRotateAngle * 2 * (i - 1))) &&
                (rotateInfo.rotate.angle <= ASW_MAX_ANGLE + (maxRotateAngle + maxRotateAngle * 2 * i)))) {
            rotateInfo.rotate.srcStride = rotateWidth * dstBpp;
            rotateInfo.rotate.srcBuf = g_rotateBuf[i].buf;
            rotateInfo.rotate.srcFmt = info->dstFmt;
            rotateInfo.rotate.angle -= (maxRotateAngle * 2 * i);
            ret = ASW_Rotate(&rotateInfo);
            CHECK_GOTO(HAL_MOD_ASW, ret, "ASW_Rotate", GOTO);
            break;
        }
    }

    return HAL_SUCCESS;

GOTO:
    ASW_DeleteRotateBuf(rotateImgCnt);

    return ret;
#else
    HAL_LOG_E(HAL_MOD_ASW, "CONFIG_ASW not open, don't support asw!\n");
    return HAL_ASW_NOT_SUPPORT;
#endif
}

int HAL_ASW_FillTriangle(const HAL_FillRatriaInfo *info)
{
#ifdef CONFIG_ASW
    CHECK_NULLPTR_RETURN(HAL_MOD_ASW, info, HAL_ASW_NULL_PTR);
    CHECK_NULLPTR_RETURN(HAL_MOD_ASW, info->buf, HAL_ASW_NULL_PTR);
    if ((info->stride == 0) || (info->stride % 8 != 0)) {
        HAL_LOG_E(HAL_MOD_ASW, "ratria stride[%d] is invalid,It has to be a multiple of 8!\n", info->stride);
        return HAL_ASW_ERR_PARAM;
    }
    if (info->type >= HAL_TRIANGLE_BUTT) {
        HAL_LOG_E(HAL_MOD_ASW, "triangle_info->type:%d is invaild!\n", info->type);
        return HAL_ASW_ERR_PARAM;
    }

    int fmt = 0;
    int ret = ASW_TransformFmtType(info->fmt, &fmt);
    CHECK_RETURN(HAL_MOD_ASW, ret, "transform fmt type");

    HAL_LOG_D(HAL_MOD_ASW, "buf[%#x] type[%#x] stride[%d] format[%d], color[0x%x] rect(%d, %d, %d, %d)",
        info->buf, info->type, info->stride, info->fmt, info->color,
        info->dstRect.x, info->dstRect.y, info->dstRect.w, info->dstRect.h);

    struct asw_ratria ratria_info;
    ratria_info.type = info->type;
    ratria_info.fmt = fmt;
    ratria_info.color = ASW_TransformColor(info->fmt, info->color);
    ratria_info.hsize = info->dstRect.w;
    ratria_info.vsize = info->dstRect.h;
    ratria_info.pos_x = info->dstRect.x;
    ratria_info.pos_y = info->dstRect.y;
    ratria_info.addr = (int)info->buf;
    ratria_info.stride = info->stride;
    sdrv_asw_ra_triangle(&ratria_info);
    return HAL_SUCCESS;
#else
    HAL_LOG_E(HAL_MOD_ASW, "CONFIG_ASW not open, don't support asw!\n");
    return HAL_ASW_NOT_SUPPORT;
#endif
}

int HAL_ASW_FillRect(const HAL_FillRectInfo *info)
{
#ifdef CONFIG_ASW
    CHECK_NULLPTR_RETURN(HAL_MOD_ASW, info, HAL_ASW_NULL_PTR);
    CHECK_NULLPTR_RETURN(HAL_MOD_ASW, info->buf, HAL_ASW_NULL_PTR);
    if (info->dstRect.w == 0 || info->dstRect.h == 0) {
        HAL_LOG_E(HAL_MOD_ASW, "rect w[%d] or h[%d] is invalid!\n",
                  info->dstRect.w, info->dstRect.h);
        return HAL_ASW_ERR_PARAM;
    }

    if ((info->stride == 0) || (info->stride % 8 != 0)) {
        HAL_LOG_E(HAL_MOD_ASW, "rect stride[%d] is invalid,It has to be a multiple of 8!\n", info->stride);
        return HAL_ASW_ERR_PARAM;
    }

    int fmt = 0;
    int ret = ASW_TransformFmtType(info->fmt, &fmt);
    CHECK_RETURN(HAL_MOD_ASW, ret, "transform fmt type");
    HAL_LOG_D(HAL_MOD_ASW, "out :buf[%#x] stride[%d] format[%d], color [0x%x] rect(%d, %d, %d, %d)",
        info->buf, info->stride, info->fmt, info->color,
        info->dstRect.x, info->dstRect.y, info->dstRect.w, info->dstRect.h);
     uint32_t color = ASW_TransformColor(info->fmt, info->color);
    sdrv_asw_fill_rect(fmt, color, info->dstRect.w, info->dstRect.h,
        info->dstRect.x, info->dstRect.y, (int)info->buf, info->stride);

    return HAL_SUCCESS;
#else
    HAL_LOG_E(HAL_MOD_ASW, "CONFIG_ASW not open, don't support asw!\n");
    return HAL_ASW_NOT_SUPPORT;
#endif
}

int HAL_ASW_Rotate(const HAL_RotateInfo *info)
{
    CHECK_NULLPTR_RETURN(HAL_MOD_ASW, info, HAL_ASW_NULL_PTR);
    CHECK_NULLPTR_RETURN(HAL_MOD_ASW, info->srcBuf, HAL_ASW_NULL_PTR);
    CHECK_NULLPTR_RETURN(HAL_MOD_ASW, info->dstBuf, HAL_ASW_NULL_PTR);
    if ((info->dstStride == 0) || (info->dstStride % 8 != 0)) {
        HAL_LOG_E(HAL_MOD_ASW, "dst stride[%d] is invalid,It has to be a multiple of 8!\n", info->dstStride);
        return HAL_ASW_ERR_PARAM;
    }
    return ASW_RotateAndCache(info, ASW_MAX_ANGLE / 2);
}

int HAL_ASW_RotateEx(const HAL_RotateInfo *info)
{
    CHECK_NULLPTR_RETURN(HAL_MOD_ASW, info, HAL_ASW_NULL_PTR);
    CHECK_NULLPTR_RETURN(HAL_MOD_ASW, info->srcBuf, HAL_ASW_NULL_PTR);
    CHECK_NULLPTR_RETURN(HAL_MOD_ASW, info->dstBuf, HAL_ASW_NULL_PTR);
    if ((info->dstStride == 0) || (info->dstStride % 8 != 0)) {
        HAL_LOG_E(HAL_MOD_ASW, "dst stride[%d] is invalid,It has to be a multiple of 8!\n", info->dstStride);
        return HAL_ASW_ERR_PARAM;
    }
    return ASW_RotateAndCache(info, ASW_ROTATE_MAX_ANGLE);
}

int ASW_Rotate(const ASW_RotateInfo *info)
{
#ifdef CONFIG_ASW
    CHECK_NULLPTR_RETURN(HAL_MOD_ASW, info, HAL_ASW_NULL_PTR);

    HAL_LOG_D(HAL_MOD_ASW, "src:rect[%u, %u, %u, %u] buf[%#x] stride[%u] format[%d]",
        info->rotate.srcRect.x, info->rotate.srcRect.y, info->rotate.srcRect.w, info->rotate.srcRect.h,
        info->rotate.srcBuf, info->rotate.srcStride, info->rotate.srcFmt);
    HAL_LOG_D(HAL_MOD_ASW, "dst:width[%u] height[%u] buf[%#x] stride[%u] format[%d] angle[%lf] bgColor[%#x]",
        info->rotate.dstWidth, info->rotate.dstHeight, info->rotate.dstBuf, info->rotate.dstStride,
        info->rotate.dstFmt, info->rotate.angle, info->rotate.bgColor);

    struct asw_rot rot = {0};
    rot.hsize = info->rotate.srcRect.w;
    rot.vsize = info->rotate.srcRect.h;
    rot.angle = info->rotate.angle;
    int ret = ASW_TransformFmtType(info->rotate.dstFmt, &rot.dst_fmt);
    CHECK_RETURN(HAL_MOD_ASW, ret, "transform fmt type");
    rot.dst_ba = (int)(info->rotate.dstBuf);
    rot.dst_stride = info->rotate.dstStride;

    ret = ASW_TransformFmtType(info->rotate.srcFmt, &rot.src_fmt);
    CHECK_RETURN(HAL_MOD_ASW, ret, "transform fmt type");
    rot.bg_color = ASW_TransformBgColor(info->rotate.dstFmt, info->rotate.bgColor);
    rot.src_stride[0] = info->rotate.srcStride;
    rot.src_ba[0] = (int)info->rotate.srcBuf;
    rot.t_buf = (int)info->tmpBuf;
    rot.t_stride = (int)info->tmpStride;
    sdrv_asw_bilinear_rotation(&rot);
    return HAL_SUCCESS;
#else
    HAL_LOG_E(HAL_MOD_ASW, "CONFIG_ASW not open, don't support asw!\n");
    return HAL_ASW_NOT_SUPPORT;
#endif
}
