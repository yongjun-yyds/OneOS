/*
* hal_g2dlite.c
*@brief hal g2dlite function file.
*
* Copyright (c) 2012 Semidrive Semiconductor.
* All rights reserved.
*
* Description:
*
* Revision History:
* -----------------
* 2022/06/01 create this file
*/
#include <string.h>
#include "types.h"
#include "g2dlite/g2dlite.h"
#include "dispss/disp_data_type.h"
#include "FreeRTOS.h"
#include "hal_comm_inner.h"
#include "hal_comm_define.h"
#include "hal_g2dlite.h"

extern struct g2dlite_device g2dlite_dev;

#define G2DLITE_RLE_USER_DATA_LEN 258

static int G2DLITE_GetUintWordSize(HAL_Format fmt, uint32_t width)
{
    switch (fmt) {
        case HAL_FMT_A8:
            if (width % 4 != 0) {
                HAL_LOG_E(HAL_MOD_G2DLITE, "fmt[%d], width[%u] must allgn to 4!\n", fmt, width);
                return -1;
            }
            return width / 4;
        case HAL_FMT_ARGB4444:
        case HAL_FMT_ARGB1555:
        case HAL_FMT_RGB565:
            if (width % 2 != 0) {
                HAL_LOG_E(HAL_MOD_G2DLITE, "fmt[%d], width[%u] must allgn to 2!\n", fmt, width);
                return -1;
            }
            return width / 2;
        case HAL_FMT_RGB888:
                if ((width * 3) % 4 != 0) {
                    HAL_LOG_E(HAL_MOD_G2DLITE, "fmt[%d], width[%u] must allgn to 4/3!\n", fmt, width);
                    return -1;
                }
            return (width * 3) / 4;
        case HAL_FMT_ARGB8888:
            return width;
        default:
            HAL_LOG_E(HAL_MOD_G2DLITE, "can't support this fmt:%d!\n", fmt);
            return HAL_G2DLITE_ERR_PARAM;
    }
}

static int G2DLITE_TransformBlendMode(HAL_G2DLITE_BlendMode blendMode, u32 *mode)
{
    switch (blendMode) {
        case HAL_G2DLITE_BLEND_PIXEL_NONE:
            *mode = BLEND_PIXEL_NONE;
            break;
        case HAL_G2DLITE_BLEND_PIXEL_PREMULTI:
            *mode = BLEND_PIXEL_PREMULTI;
            break;
        case HAL_G2DLITE_BLEND_PIXEL_COVERAGE:
            *mode = BLEND_PIXEL_COVERAGE;
            break;
        default:
            HAL_LOG_E(HAL_MOD_G2DLITE, "can't support this mode:%d!\n", blendMode);
            return HAL_G2DLITE_ERR_PARAM;
    }

    return HAL_SUCCESS;
}

static int G2DLITE_TransformFmtType(HAL_Format fmtType, uint32_t *fmt)
{
    switch (fmtType) {
        case HAL_FMT_RGB565:
            *fmt = COLOR_RGB565;
            break;
        case HAL_FMT_ARGB4444:
            *fmt = COLOR_ARGB4444;
            break;
        case HAL_FMT_ARGB1555:
            *fmt = COLOR_ARGB1555;
            break;
        case HAL_FMT_RGB888:
            *fmt = COLOR_RGB888;
            break;
        case HAL_FMT_ARGB8888:
            *fmt = COLOR_ARGB8888;
            break;
        default:
            HAL_LOG_E(HAL_MOD_G2DLITE, "can't support this fmt:%d!\n", fmtType);
            return HAL_G2DLITE_ERR_PARAM;
    }

    return HAL_SUCCESS;
}

/* In order to ensure the accuracy of the color, the R G B Fill in the top of RGB101010 respectively */
static int G2DLITE_TransformColor(uint32_t inColor, HAL_Format fmt, uint32_t *outColor, uint8_t *outAlpha)
{
    switch (fmt) {
        case HAL_FMT_ARGB8888:
            *outColor = (((inColor >> 16 & 0x000000ff) << 22) | ((inColor >> 8 & 0x000000ff) << 12) | ((inColor & 0x000000ff) << 2));
            *outAlpha = (inColor >> 24) & 0x000000ff;
            break;
        default:
            HAL_LOG_E(HAL_MOD_G2DLITE, "can't support this fmt:%d!\n", fmt);
            return HAL_G2DLITE_ERR_PARAM;
    }

    return HAL_SUCCESS;
}

static int G2DLITE_TransformPdType(HAL_G2DLITE_PdSurfaceType pdType, PD_LAYER_TYPE *bgPdType, PD_LAYER_TYPE *fgPdType)
{
    if (pdType == HAL_G2DLITE_PD_TYPE_SRC_IN_BG) {
        *bgPdType = PD_SRC;
        *fgPdType = PD_DST;
    } else if (pdType == HAL_G2DLITE_PD_TYPE_SRC_IN_FG) {
        *bgPdType = PD_DST;
        *fgPdType = PD_SRC;
    } else {
        HAL_LOG_E(HAL_MOD_G2DLITE, "porterduff mode[%d] is invalid!",  pdType);
        return HAL_G2DLITE_ERR_PARAM;
    }

    HAL_LOG_I(HAL_MOD_G2DLITE, "bgPdType[%d],  fgPdType[%d", *bgPdType, *fgPdType);

    return HAL_SUCCESS;
}

int HAL_G2DLITE_Init(void)
{
    return HAL_SUCCESS;
}

int HAL_G2DLITE_Deinit(void)
{
    return HAL_SUCCESS;
}

int HAL_G2DLITE_BlendRleImg(const HAL_G2DLITE_BlendImgInfo *info, HAL_G2DLITE_RleInfo *rleInfo)
{
    CHECK_NULLPTR_RETURN(HAL_MOD_G2DLITE, info, HAL_G2DLITE_NULL_PTR);
    CHECK_NULLPTR_RETURN(HAL_MOD_G2DLITE, rleInfo, HAL_G2DLITE_NULL_PTR);
    CHECK_NULLPTR_RETURN(HAL_MOD_G2DLITE, info->bgSurface.buf, HAL_G2DLITE_NULL_PTR);
    CHECK_NULLPTR_RETURN(HAL_MOD_G2DLITE, info->fgSurface.buf, HAL_G2DLITE_NULL_PTR);
    CHECK_NULLPTR_RETURN(HAL_MOD_G2DLITE, info->output.buf, HAL_G2DLITE_NULL_PTR);

    HAL_LOG_D(HAL_MOD_G2DLITE, "fg :srcRect[%u, %u, %u, %u] dstRect[%u, %u, %u, %u] ",
        info->fgSurface.srcRect.x, info->fgSurface.srcRect.y, info->fgSurface.srcRect.w, info->fgSurface.srcRect.h,
        info->fgSurface.dstRect.x, info->fgSurface.dstRect.y, info->fgSurface.dstRect.w, info->fgSurface.dstRect.h);
    HAL_LOG_D(HAL_MOD_G2DLITE, "fg :buf[%#x] stride[%u] format[%d], alpha[%d] ",
         info->fgSurface.buf, info->fgSurface.stride, info->fgSurface.fmt, info->fgSurface.alpha);

    HAL_LOG_D(HAL_MOD_G2DLITE, "bg :srcRect[%u, %u, %u, %u] dstRect[%u, %u, %u, %u] ",
        info->bgSurface.srcRect.x, info->bgSurface.srcRect.y, info->bgSurface.srcRect.w, info->bgSurface.srcRect.h,
        info->bgSurface.dstRect.x, info->bgSurface.dstRect.y, info->bgSurface.dstRect.w, info->bgSurface.dstRect.h);
    HAL_LOG_D(HAL_MOD_G2DLITE, "bg :buf[%#x] stride[%u] format[%d], alpha[%d] ",
         info->bgSurface.buf,  info->bgSurface.stride, info->fgSurface.fmt,  info->bgSurface.alpha);

    HAL_LOG_D(HAL_MOD_G2DLITE, "out :buf[%#x] stride[%u] format[%d], rect[%u, %u, %u, %u] ",
        info->output.buf, info->output.stride, info->output.fmt,
        info->output.dstRect.x, info->output.dstRect.y, info->output.dstRect.w, info->output.dstRect.h);

    HAL_LOG_D(HAL_MOD_G2DLITE, "rleInfo :type[%d]", rleInfo->rleType);

    struct g2dlite_input input;
    memset(&input, 0, sizeof(struct g2dlite_input));
    input.layer_num = 2;
    int rleIndex = 0;

    /* only layer 1 support RLE, fgsurface save layer[0], bgsurface save layer[1] */
    const HAL_G2DLITE_SurfaceInfo *surface[G2DLITE_LAYER_MAX_NUM] = { &info->fgSurface, &info->bgSurface };
    switch (rleInfo->rleType) {
        case HAL_G2DLITE_RLE_IN_BG_SURFACE:
            rleIndex = 1;
        break;
        case HAL_G2DLITE_RLE_IN_FG_SURFACE:
            rleIndex = 0;
        break;
        default:
            HAL_LOG_E(HAL_MOD_G2DLITE, "rleType[%d] is invaild", rleInfo->rleType);
            return HAL_G2DLITE_ERR_PARAM;
    }

    if ((surface[rleIndex]->srcRect.w != surface[rleIndex]->dstRect.w) || (surface[rleIndex]->srcRect.h != surface[rleIndex]->dstRect.h)) {
        HAL_LOG_E(HAL_MOD_G2DLITE, "rle surface not support scale!");
        return HAL_G2DLITE_NOT_SUPPORT;
    }

    int bpp = COMM_getFmtBpp(surface[rleIndex]->fmt);
    if (bpp < 0){
        return HAL_G2DLITE_ERR_PARAM;
    }
    input.layer[rleIndex].rle.en = 1;
    /* datasize: 0: Bpp = 1, 1: Bpp =2, 2: Bpp = 3, 3: Bpp = 4. (Bpp: Byte per pixel). */
    input.layer[rleIndex].rle.rle_data_size = bpp - 1;
    input.layer[rleIndex].rle.rle_y_len = ((uint32_t *)(surface[rleIndex]->buf))[G2DLITE_RLE_USER_DATA_LEN + 2] / (input.layer[rleIndex].rle.rle_data_size + 1) - 1;
    input.layer[rleIndex].rle.rle_y_checksum = ((uint32_t *)(surface[rleIndex]->buf))[G2DLITE_RLE_USER_DATA_LEN + 3];

    HAL_LOG_I(HAL_MOD_G2DLITE, "rle_data_size:%d, rle_y_len[%d]", input.layer[rleIndex].rle.rle_y_len, bpp);

    for (int i = 0; i < input.layer_num; i++) {
        struct g2dlite_input_cfg *l = &input.layer[i];
        const HAL_G2DLITE_SurfaceInfo *sur = surface[i];
        l->layer_en = 1;
        l->zorder = 1 - i;
        l->ckey.en = 0;

        /* only layer 1 support RLE, fgsurface save layer[0], bgsurface save layer[1] */
        if (rleInfo->rleType == HAL_G2DLITE_RLE_IN_BG_SURFACE) {
            l->layer = i;
        } else {
            l->layer = 1 - i;
        }

        int ret = G2DLITE_TransformBlendMode(sur->blendMode, &l->blend);
        CHECK_RETURN(HAL_MOD_G2DLITE, ret, "transform blend mode");
        ret = G2DLITE_TransformFmtType(sur->fmt, &l->fmt);
        CHECK_RETURN(HAL_MOD_G2DLITE, ret, "transform fmt type");
        l->alpha = sur->alpha;
        l->addr[0] = (u64)sur->buf;

        /* RLE Location of net load data */
        if (l->layer == 1) {
            l->addr[0] = (u64)(&((uint32_t *)sur->buf)[G2DLITE_RLE_USER_DATA_LEN + 16]);
        }
        l->src.x = sur->srcRect.x;
        l->src.y = sur->srcRect.y;
        l->src.w = sur->srcRect.w;
        l->src.h = sur->srcRect.h;
        l->src_stride[0] = sur->stride;

        l->dst.x = sur->dstRect.x;// canvas :output x y
        l->dst.y = sur->dstRect.y;
        l->dst.w = sur->dstRect.w;
        l->dst.h = sur->dstRect.h;
    }

    int out_bpp = COMM_getFmtBpp(info->output.fmt);
    if (out_bpp < 0){
        return HAL_G2DLITE_ERR_PARAM;
    }

    input.output.width = info->output.dstRect.w;
    input.output.height = info->output.dstRect.h;
    int ret = G2DLITE_TransformFmtType(info->output.fmt, &input.output.fmt);
    CHECK_RETURN(HAL_MOD_G2DLITE, ret, "transform fmt type");
    input.output.addr[0] = (u64)(info->output.buf +
        (info->output.dstRect.y * info->output.stride + info->output.dstRect.x * out_bpp));
    input.output.stride[0] = info->output.stride;
    input.output.rotation = 0;

    sdrv_g2dlite_update(&g2dlite_dev, &input);
    return HAL_SUCCESS;
}

int HAL_G2DLITE_BlendRleImgWithPd(const HAL_G2DLITE_BlendImgInfo *info, HAL_G2DLITE_RleInfo *rleInfo, HAL_G2DLITE_PorterDuffInfo *pdInfo)
{
    CHECK_NULLPTR_RETURN(HAL_MOD_G2DLITE, info, HAL_G2DLITE_NULL_PTR);
    CHECK_NULLPTR_RETURN(HAL_MOD_G2DLITE, rleInfo, HAL_G2DLITE_NULL_PTR);
    CHECK_NULLPTR_RETURN(HAL_MOD_G2DLITE, pdInfo, HAL_G2DLITE_NULL_PTR);
    CHECK_NULLPTR_RETURN(HAL_MOD_G2DLITE, info->bgSurface.buf, HAL_G2DLITE_NULL_PTR);
    CHECK_NULLPTR_RETURN(HAL_MOD_G2DLITE, info->fgSurface.buf, HAL_G2DLITE_NULL_PTR);
    CHECK_NULLPTR_RETURN(HAL_MOD_G2DLITE, info->output.buf, HAL_G2DLITE_NULL_PTR);
    if (pdInfo->pdMode < 0 || pdInfo->pdMode >= HAL_G2DLITE_PD_MODE_BUTT) {
        HAL_LOG_E(HAL_MOD_G2DLITE, "porterduff mode[%d] is invalid!", pdInfo->pdMode);
        return HAL_G2DLITE_ERR_PARAM;
    }
    PD_LAYER_TYPE bgPdType, fgPdType;
    int ret = G2DLITE_TransformPdType(pdInfo->pdType, &bgPdType, &fgPdType);
    CHECK_RETURN(HAL_MOD_G2DLITE, ret, "transform pd type");

    HAL_LOG_D(HAL_MOD_G2DLITE, "fg :srcRect[%u, %u, %u, %u] dstRect[%u, %u, %u, %u] ",
        info->fgSurface.srcRect.x, info->fgSurface.srcRect.y, info->fgSurface.srcRect.w, info->fgSurface.srcRect.h,
        info->fgSurface.dstRect.x, info->fgSurface.dstRect.y, info->fgSurface.dstRect.w, info->fgSurface.dstRect.h);
    HAL_LOG_D(HAL_MOD_G2DLITE, "fg :buf[%#x] stride[%u] format[%d], alpha[%d] ",
         info->fgSurface.buf, info->fgSurface.stride, info->fgSurface.fmt, info->fgSurface.alpha);

    HAL_LOG_D(HAL_MOD_G2DLITE, "bg :srcRect[%u, %u, %u, %u] dstRect[%u, %u, %u, %u] ",
        info->bgSurface.srcRect.x, info->bgSurface.srcRect.y, info->bgSurface.srcRect.w, info->bgSurface.srcRect.h,
        info->bgSurface.dstRect.x, info->bgSurface.dstRect.y, info->bgSurface.dstRect.w, info->bgSurface.dstRect.h);
    HAL_LOG_D(HAL_MOD_G2DLITE, "bg :buf[%#x] stride[%u] format[%d], alpha[%d] ",
         info->bgSurface.buf,  info->bgSurface.stride, info->fgSurface.fmt,  info->bgSurface.alpha);

    HAL_LOG_D(HAL_MOD_G2DLITE, "out :buf[%#x] stride[%u] format[%d], rect[%u, %u, %u, %u] ",
        info->output.buf, info->output.stride, info->output.fmt,
        info->output.dstRect.x, info->output.dstRect.y, info->output.dstRect.w, info->output.dstRect.h);

    HAL_LOG_D(HAL_MOD_G2DLITE, "rleInfo :rleType[%d], pdInfo:mode[%d]", rleInfo->rleType, pdInfo->pdMode);

    struct g2dlite_input input;
    memset(&input, 0, sizeof(struct g2dlite_input));
    input.layer_num = 2;
    int rleIndex = 0;
    input.pd_info.alpha_need = 1;
    input.pd_info.en = 1;
    input.pd_info.mode = (pd_mode_t)pdInfo->pdMode;
    input.pd_info.zorder = 0;

    /* only layer 1 support RLE, fgsurface save layer[0], bgsurface save layer[1] */
    const HAL_G2DLITE_SurfaceInfo *surface[G2DLITE_LAYER_MAX_NUM] = { &info->fgSurface, &info->bgSurface };
    switch (rleInfo->rleType) {
        case HAL_G2DLITE_RLE_IN_BG_SURFACE:
            rleIndex = 1;
        break;
        case HAL_G2DLITE_RLE_IN_FG_SURFACE:
            rleIndex = 0;
        break;
        default:
            HAL_LOG_E(HAL_MOD_G2DLITE, "rleType[%d] is invaild", rleInfo->rleType);
            return HAL_G2DLITE_ERR_PARAM;
    }

    if ((surface[rleIndex]->srcRect.w != surface[rleIndex]->dstRect.w) || (surface[rleIndex]->srcRect.h != surface[rleIndex]->dstRect.h)) {
        HAL_LOG_E(HAL_MOD_G2DLITE, "rle surface not support scale!");
        return HAL_G2DLITE_NOT_SUPPORT;
    }

    int bpp = COMM_getFmtBpp(surface[rleIndex]->fmt);
    if (bpp < 0){
        return HAL_G2DLITE_ERR_PARAM;
    }
    input.layer[rleIndex].rle.en = 1;
    /* datasize: 0: Bpp = 1, 1: Bpp =2, 2: Bpp = 3, 3: Bpp = 4. (Bpp: Byte per pixel). */
    input.layer[rleIndex].rle.rle_data_size = bpp - 1;
    input.layer[rleIndex].rle.rle_y_len = ((uint32_t *)(surface[rleIndex]->buf))[G2DLITE_RLE_USER_DATA_LEN + 2] / (input.layer[rleIndex].rle.rle_data_size + 1) - 1;
    input.layer[rleIndex].rle.rle_y_checksum = ((uint32_t *)(surface[rleIndex]->buf))[G2DLITE_RLE_USER_DATA_LEN + 3];

    HAL_LOG_I(HAL_MOD_G2DLITE, "rle_data_size:%d, rle_y_len[%d]", input.layer[rleIndex].rle.rle_y_len, bpp);

    for (int i = 0; i < input.layer_num; i++) {
        struct g2dlite_input_cfg *l = &input.layer[i];
        const HAL_G2DLITE_SurfaceInfo *sur = surface[i];
        l->layer_en = 1;
        l->zorder = 1 - i;
        l->ckey.en = 0;
        l->pd_type = fgPdType;
        /* only layer 1 support RLE, fgsurface save layer[0], bgsurface save layer[1] */
        if (rleInfo->rleType == HAL_G2DLITE_RLE_IN_BG_SURFACE) {
            l->layer = i;
        } else {
            l->layer = 1 - i;
        }

        int ret = G2DLITE_TransformBlendMode(sur->blendMode, &l->blend);
        CHECK_RETURN(HAL_MOD_G2DLITE, ret, "transform blend mode");
        ret = G2DLITE_TransformFmtType(sur->fmt, &l->fmt);
        CHECK_RETURN(HAL_MOD_G2DLITE, ret, "transform fmt type");
        l->alpha = sur->alpha;
        l->addr[0] = (u64)sur->buf;

        /* RLE Location of net load data */
        if (l->layer == 1) {
            l->pd_type = bgPdType;
            l->addr[0] = (u64)(&((uint32_t *)sur->buf)[G2DLITE_RLE_USER_DATA_LEN + 16]);
        }
        l->src.x = sur->srcRect.x;
        l->src.y = sur->srcRect.y;
        l->src.w = sur->srcRect.w;
        l->src.h = sur->srcRect.h;
        l->src_stride[0] = sur->stride;

        l->dst.x = sur->dstRect.x;// canvas :output x y
        l->dst.y = sur->dstRect.y;
        l->dst.w = sur->dstRect.w;
        l->dst.h = sur->dstRect.h;
    }

    int out_bpp = COMM_getFmtBpp(info->output.fmt);
    if (out_bpp < 0){
        return HAL_G2DLITE_ERR_PARAM;
    }

    input.output.width = info->output.dstRect.w;
    input.output.height = info->output.dstRect.h;
    ret = G2DLITE_TransformFmtType(info->output.fmt, &input.output.fmt);
    CHECK_RETURN(HAL_MOD_G2DLITE, ret, "transform fmt type");
    input.output.addr[0] = (u64)(info->output.buf +
        (info->output.dstRect.y * info->output.stride + info->output.dstRect.x * out_bpp));
    input.output.stride[0] = info->output.stride;
    input.output.rotation = 0;

    sdrv_g2dlite_update(&g2dlite_dev, &input);
    return HAL_SUCCESS;
}

int HAL_G2DLITE_BlendImg(const HAL_G2DLITE_BlendImgInfo *info)
{
    CHECK_NULLPTR_RETURN(HAL_MOD_G2DLITE, info, HAL_G2DLITE_NULL_PTR);
    CHECK_NULLPTR_RETURN(HAL_MOD_G2DLITE, info->bgSurface.buf, HAL_G2DLITE_NULL_PTR);
    CHECK_NULLPTR_RETURN(HAL_MOD_G2DLITE, info->fgSurface.buf, HAL_G2DLITE_NULL_PTR);
    CHECK_NULLPTR_RETURN(HAL_MOD_G2DLITE, info->output.buf, HAL_G2DLITE_NULL_PTR);

    HAL_LOG_D(HAL_MOD_G2DLITE, "fg :srcRect[%u, %u, %u, %u] dstRect[%u, %u, %u, %u] ",
        info->fgSurface.srcRect.x, info->fgSurface.srcRect.y, info->fgSurface.srcRect.w, info->fgSurface.srcRect.h,
        info->fgSurface.dstRect.x, info->fgSurface.dstRect.y, info->fgSurface.dstRect.w, info->fgSurface.dstRect.h);
    HAL_LOG_D(HAL_MOD_G2DLITE, "fg :buf[%#x] stride[%u] format[%d], alpha[%d] blendmode[%d]",
         info->fgSurface.buf, info->fgSurface.stride, info->fgSurface.fmt, info->fgSurface.alpha, info->fgSurface.blendMode);

    HAL_LOG_D(HAL_MOD_G2DLITE, "bg :srcRect[%u, %u, %u, %u] dstRect[%u, %u, %u, %u] ",
        info->bgSurface.srcRect.x, info->bgSurface.srcRect.y, info->bgSurface.srcRect.w, info->bgSurface.srcRect.h,
        info->bgSurface.dstRect.x, info->bgSurface.dstRect.y, info->bgSurface.dstRect.w, info->bgSurface.dstRect.h);
    HAL_LOG_D(HAL_MOD_G2DLITE, "bg :buf[%#x] stride[%u] format[%d], alpha[%d] blendmode[%d]",
          info->bgSurface.buf,  info->bgSurface.stride,  info->bgSurface.fmt, info->bgSurface.alpha, info->bgSurface.blendMode);

     HAL_LOG_D(HAL_MOD_G2DLITE, "out :buf[%#x] stride[%u] format[%d], rect[%u, %u, %u, %u] ",
         info->output.buf, info->output.stride, info->output.fmt,
         info->output.dstRect.x, info->output.dstRect.y, info->output.dstRect.w, info->output.dstRect.h);

    if ((info->bgSurface.srcRect.w != info->bgSurface.dstRect.w) || (info->bgSurface.srcRect.h != info->bgSurface.dstRect.h)) {
        HAL_LOG_E(HAL_MOD_G2DLITE, "bg surface not support scale!");
        return HAL_G2DLITE_NOT_SUPPORT;
    }

    struct g2dlite_input input;
    memset(&input, 0, sizeof(struct g2dlite_input));
    input.layer_num = 2;
    for (int i = 0; i < input.layer_num; i++) {
        struct g2dlite_input_cfg  *l = &input.layer[i];
        l->layer_en = 1;
        l->layer = i;
        l->zorder = 1 - i;
        l->ckey.en = 0;
        const HAL_G2DLITE_SurfaceInfo *surface = &info->fgSurface;

        if (i == 1) {
            surface = &info->bgSurface;
        }
        int ret = G2DLITE_TransformBlendMode(surface->blendMode, &l->blend);
        CHECK_RETURN(HAL_MOD_G2DLITE, ret, "transform blend mode");
        ret = G2DLITE_TransformFmtType(surface->fmt, &l->fmt);
        CHECK_RETURN(HAL_MOD_G2DLITE, ret, "transform fmt type");
        l->alpha = surface->alpha;
        l->addr[0] = (u64)surface->buf;
        l->src.x = surface->srcRect.x;
        l->src.y = surface->srcRect.y;
        l->src.w = surface->srcRect.w;
        l->src.h = surface->srcRect.h;
        l->src_stride[0] = surface->stride;

        l->dst.x = surface->dstRect.x;// canvas :output x y
        l->dst.y = surface->dstRect.y;
        l->dst.w = surface->dstRect.w;
        l->dst.h = surface->dstRect.h;
    }

    int out_bpp = COMM_getFmtBpp(info->output.fmt);
    if (out_bpp < 0){
        return HAL_G2DLITE_ERR_PARAM;
    }

    input.output.width = info->output.dstRect.w;
    input.output.height = info->output.dstRect.h;
    int ret = G2DLITE_TransformFmtType(info->output.fmt, &input.output.fmt);
    CHECK_RETURN(HAL_MOD_G2DLITE, ret, "transform fmt type");
    input.output.addr[0] = (u64)(info->output.buf +
        (info->output.dstRect.y * info->output.stride + info->output.dstRect.x * out_bpp));
    input.output.stride[0] = info->output.stride;
    input.output.rotation = 0;

    sdrv_g2dlite_update(&g2dlite_dev, &input);
    return HAL_SUCCESS;
}

int HAL_G2DLITE_BlendImgWithPd(const HAL_G2DLITE_BlendImgInfo *info, HAL_G2DLITE_PorterDuffInfo *pdInfo)
{
    CHECK_NULLPTR_RETURN(HAL_MOD_G2DLITE, info, HAL_G2DLITE_NULL_PTR);
    CHECK_NULLPTR_RETURN(HAL_MOD_G2DLITE, pdInfo, HAL_G2DLITE_NULL_PTR);
    CHECK_NULLPTR_RETURN(HAL_MOD_G2DLITE, info->bgSurface.buf, HAL_G2DLITE_NULL_PTR);
    CHECK_NULLPTR_RETURN(HAL_MOD_G2DLITE, info->fgSurface.buf, HAL_G2DLITE_NULL_PTR);
    CHECK_NULLPTR_RETURN(HAL_MOD_G2DLITE, info->output.buf, HAL_G2DLITE_NULL_PTR);
    if (pdInfo->pdMode < 0 || pdInfo->pdMode >= HAL_G2DLITE_PD_MODE_BUTT) {
        HAL_LOG_E(HAL_MOD_G2DLITE, "porterduff mode[%d] is invalid!", pdInfo->pdMode);
        return HAL_G2DLITE_ERR_PARAM;
    }

    PD_LAYER_TYPE bgPdType, fgPdType;
    int ret = G2DLITE_TransformPdType(pdInfo->pdType, &bgPdType, &fgPdType);
    CHECK_RETURN(HAL_MOD_G2DLITE, ret, "transform pd type");

    HAL_LOG_D(HAL_MOD_G2DLITE, "fg :srcRect[%u, %u, %u, %u] dstRect[%u, %u, %u, %u] ",
        info->fgSurface.srcRect.x, info->fgSurface.srcRect.y, info->fgSurface.srcRect.w, info->fgSurface.srcRect.h,
        info->fgSurface.dstRect.x, info->fgSurface.dstRect.y, info->fgSurface.dstRect.w, info->fgSurface.dstRect.h);
    HAL_LOG_D(HAL_MOD_G2DLITE, "fg :buf[%#x] stride[%u] format[%d], alpha[%d] ",
         info->fgSurface.buf, info->fgSurface.stride, info->fgSurface.fmt, info->fgSurface.alpha);

    HAL_LOG_D(HAL_MOD_G2DLITE, "bg :srcRect[%u, %u, %u, %u] dstRect[%u, %u, %u, %u] ",
        info->bgSurface.srcRect.x, info->bgSurface.srcRect.y, info->bgSurface.srcRect.w, info->bgSurface.srcRect.h,
        info->bgSurface.dstRect.x, info->bgSurface.dstRect.y, info->bgSurface.dstRect.w, info->bgSurface.dstRect.h);
    HAL_LOG_D(HAL_MOD_G2DLITE, "bg :buf[%#x] stride[%u] format[%d], alpha[%d] ",
        info->bgSurface.buf,  info->bgSurface.stride,  info->bgSurface.fmt, info->bgSurface.alpha);

    HAL_LOG_D(HAL_MOD_G2DLITE, "out :buf[%#x] stride[%u] format[%d], rect[%u, %u, %u, %u] ",
        info->output.buf, info->output.stride, info->output.fmt,
        info->output.dstRect.x, info->output.dstRect.y, info->output.dstRect.w, info->output.dstRect.h);

    HAL_LOG_D(HAL_MOD_G2DLITE, "pdInfo:mode[%d]", pdInfo->pdMode);
    if ((info->bgSurface.srcRect.w != info->bgSurface.dstRect.w) || (info->bgSurface.srcRect.h != info->bgSurface.dstRect.h)) {
        HAL_LOG_E(HAL_MOD_G2DLITE, "bg surface not support scale!");
        return HAL_G2DLITE_NOT_SUPPORT;
    }

    struct g2dlite_input input;
    memset(&input, 0, sizeof(struct g2dlite_input));
    input.layer_num = 2;
    input.pd_info.alpha_need = 1;
    input.pd_info.en = 1;
    input.pd_info.mode = (pd_mode_t)pdInfo->pdMode;
    input.pd_info.zorder = 0;
    for (int i = 0; i < input.layer_num; i++) {
        struct g2dlite_input_cfg  *l = &input.layer[i];
        l->layer_en = 1;
        l->layer = i;
        l->zorder = 1;
        l->ckey.en = 0;
        l->pd_type = fgPdType;
        const HAL_G2DLITE_SurfaceInfo *surface = &info->fgSurface;

        if (i == 1) {
            l->zorder = 0;
            l->pd_type = bgPdType;
            surface = &info->bgSurface;
        }
        ret = G2DLITE_TransformBlendMode(surface->blendMode, &l->blend);
        CHECK_RETURN(HAL_MOD_G2DLITE, ret, "transform blend mode");
        ret = G2DLITE_TransformFmtType(surface->fmt, &l->fmt);
        CHECK_RETURN(HAL_MOD_G2DLITE, ret, "transform fmt type");
        l->alpha = surface->alpha;
        l->addr[0] = (u64)surface->buf;
        l->src.x = surface->srcRect.x;
        l->src.y = surface->srcRect.y;
        l->src.w = surface->srcRect.w;
        l->src.h = surface->srcRect.h;
        l->src_stride[0] = surface->stride;

        l->dst.x = surface->dstRect.x;// canvas :output x y
        l->dst.y = surface->dstRect.y;
        l->dst.w = surface->dstRect.w;
        l->dst.h = surface->dstRect.h;
    }

    int out_bpp = COMM_getFmtBpp(info->output.fmt);
    if (out_bpp < 0){
        return HAL_G2DLITE_ERR_PARAM;
    }

    input.output.width = info->output.dstRect.w;
    input.output.height = info->output.dstRect.h;
    ret = G2DLITE_TransformFmtType(info->output.fmt, &input.output.fmt);
    CHECK_RETURN(HAL_MOD_G2DLITE, ret, "transform fmt type");
    input.output.addr[0] = (u64)(info->output.buf +
        (info->output.dstRect.y * info->output.stride + info->output.dstRect.x * out_bpp));
    input.output.stride[0] = info->output.stride;
    input.output.rotation = 0;

    sdrv_g2dlite_update(&g2dlite_dev, &input);
    return HAL_SUCCESS;
}

int HAL_G2DLITE_BlendMask(const HAL_G2DLITE_BlendMaskInfo *info)
{
    CHECK_NULLPTR_RETURN(HAL_MOD_G2DLITE, info, HAL_G2DLITE_NULL_PTR);
    CHECK_NULLPTR_RETURN(HAL_MOD_G2DLITE, info->surfaceInfo.buf, HAL_G2DLITE_NULL_PTR);
    CHECK_NULLPTR_RETURN(HAL_MOD_G2DLITE, info->maskInfo.buf, HAL_G2DLITE_NULL_PTR);
    CHECK_NULLPTR_RETURN(HAL_MOD_G2DLITE, info->output.buf, HAL_G2DLITE_NULL_PTR);
    if (info->maskInfo.rect.w < info->output.dstRect.w || info->maskInfo.rect.h < info->output.dstRect.h) {
            HAL_LOG_D(HAL_MOD_G2DLITE, "mask w[%u] and h [%u] Cannot be less than out w[%u] and h[%u]",
        info->maskInfo.rect.w, info->maskInfo.rect.h, info->output.dstRect.w, info->output.dstRect.h);
        return HAL_G2DLITE_ERR_PARAM;
    }

    const HAL_G2DLITE_SurfaceInfo *surface = &info->surfaceInfo;
    HAL_LOG_D(HAL_MOD_G2DLITE, "surface :srcRect[%u, %u, %u, %u] dstRect[%u, %u, %u, %u] ",
        surface->srcRect.x, surface->srcRect.y, surface->srcRect.w, surface->srcRect.h,
        surface->dstRect.x, surface->dstRect.y, surface->dstRect.w, surface->dstRect.h);
    HAL_LOG_D(HAL_MOD_G2DLITE, "surface :buf[%#x] stride[%u] format[%d], alpha[%d] ",
         surface->buf, surface->stride, surface->fmt, surface->alpha);

    HAL_LOG_D(HAL_MOD_G2DLITE, "mask :buf[%#x] stride[%u] color[%#x] ",
        info->maskInfo.buf, info->maskInfo.stride, info->maskInfo.color);

     HAL_LOG_D(HAL_MOD_G2DLITE, "out :buf[%#x] stride[%d] format[%d] rect[%u, %u, %u, %u] ",
         info->output.buf, info->output.stride, info->output.fmt,
         info->output.dstRect.x, info->output.dstRect.y, info->output.dstRect.w, info->output.dstRect.h);

    struct g2dlite_input input;
    memset(&input, 0, sizeof(struct g2dlite_input));

    input.layer_num = 1;
    input.pd_info.en = 1;
    input.pd_info.zorder = 0;
    input.pd_info.mode = DST_IN;
    input.pd_info.alpha_need = 1;

    struct g2dlite_input_cfg  *l = &input.layer[0];
    surface = &info->surfaceInfo;
    l->layer_en = 1;
    l->layer = 0;
    l->ckey.en = 0;
    l->zorder = 0;
    l->pd_type = PD_DST;
    l->addr[0] = (u64)surface->buf;
    int ret = G2DLITE_TransformBlendMode(surface->blendMode, &l->blend);
    CHECK_RETURN(HAL_MOD_G2DLITE, ret, "transform blend mode");
    ret = G2DLITE_TransformFmtType(surface->fmt, &l->fmt);
    CHECK_RETURN(HAL_MOD_G2DLITE, ret, "transform fmt type");
    l->alpha = surface->alpha;
    l->addr[0] = (u64)surface->buf;
    l->src.x = surface->srcRect.x;
    l->src.y = surface->srcRect.y;
    l->src.w = surface->srcRect.w;
    l->src.h = surface->srcRect.h;
    l->src_stride[0] = surface->stride;

    l->dst.x = surface->dstRect.x;// canvas :output x y
    l->dst.y = surface->dstRect.y;
    l->dst.w = surface->dstRect.w;
    l->dst.h = surface->dstRect.h;

    input.bg_layer.en = 1;
    input.bg_layer.pd_type = PD_SRC;
    ret = G2DLITE_TransformColor(info->maskInfo.color, HAL_FMT_ARGB8888, &input.bg_layer.color, &input.bg_layer.g_alpha);
    CHECK_RETURN(HAL_MOD_G2DLITE, ret, "transform color");
    input.bg_layer.aaddr = (addr_t)(info->maskInfo.buf + info->maskInfo.rect.y * info->maskInfo.stride + info->maskInfo.rect.x);
    input.bg_layer.bpa = 8;
    input.bg_layer.astride = info->maskInfo.stride;
    input.bg_layer.zorder = 1;


    int out_bpp = COMM_getFmtBpp(info->output.fmt);
    input.output.width = info->output.dstRect.w;
    input.output.height = info->output.dstRect.h;
    ret = G2DLITE_TransformFmtType(info->output.fmt, &input.output.fmt);
    CHECK_RETURN(HAL_MOD_G2DLITE, ret, "transform fmt type");
    input.output.addr[0] = (u64)(info->output.buf +
        (info->output.dstRect.y * info->output.stride + info->output.dstRect.x * out_bpp));
    input.output.stride[0] = info->output.stride;
    input.output.rotation = 0;

    sdrv_g2dlite_update(&g2dlite_dev, &input);
    return HAL_SUCCESS;
}

int HAL_G2DLITE_FillMask(const HAL_G2DLITE_FillMaskInfo *info)
{
    CHECK_NULLPTR_RETURN(HAL_MOD_G2DLITE, info, HAL_G2DLITE_NULL_PTR);
    CHECK_NULLPTR_RETURN(HAL_MOD_G2DLITE, info->output.buf, HAL_G2DLITE_NULL_PTR);
    CHECK_NULLPTR_RETURN(HAL_MOD_G2DLITE, info->maskInfo.buf, HAL_G2DLITE_NULL_PTR);
    if (info->maskInfo.rect.w < info->output.dstRect.w || info->maskInfo.rect.h < info->output.dstRect.h) {
            HAL_LOG_D(HAL_MOD_G2DLITE, "mask w[%u] and h [%u] Cannot be less than out w[%u] and h[%u]",
        info->maskInfo.rect.w, info->maskInfo.rect.h, info->output.dstRect.w, info->output.dstRect.h);
        return HAL_G2DLITE_ERR_PARAM;
    }

    HAL_LOG_D(HAL_MOD_G2DLITE, "mask :buf[%#x] stride[%u] color[%#x]",
        info->maskInfo.buf, info->maskInfo.stride, info->maskInfo.color);

     HAL_LOG_D(HAL_MOD_G2DLITE, "out :buf[%#x] stride[%d] format[%d], rect[%u, %u, %u, %u] ",
         info->output.buf, info->output.stride, info->output.fmt,
         info->output.dstRect.x, info->output.dstRect.y, info->output.dstRect.w, info->output.dstRect.h);

    struct g2dlite_input input;
    memset(&input, 0, sizeof(struct g2dlite_input));
    input.layer_num = 1;

    struct g2dlite_input_cfg  *l = &input.layer[0];
    l->layer_en = 1;
    l->layer = 0;
    l->zorder = 0;
    l->ckey.en = 0;

    l->blend = BLEND_PIXEL_COVERAGE;
    l->addr[0] = (u64)(info->output.buf);
    l->src.x = info->output.dstRect.x;
    l->src.y = info->output.dstRect.y;
    l->src.w = info->output.dstRect.w;
    l->src.h = info->output.dstRect.h;
    int ret = G2DLITE_TransformFmtType(info->output.fmt, &l->fmt);
    CHECK_RETURN(HAL_MOD_G2DLITE, ret, "transform fmt type");
    l->src_stride[0] = info->output.stride;

    l->dst.x = 0;// canvas :output x y
    l->dst.y = 0;
    l->dst.w = info->output.dstRect.w;
    l->dst.h = info->output.dstRect.h;
    l->alpha = 255;

    // bg layer
    input.bg_layer.en = 1;
    HAL_LOG_I(HAL_MOD_G2DLITE, "bg :color[%#x]", info->maskInfo.color);
    ret = G2DLITE_TransformColor(info->maskInfo.color, HAL_FMT_ARGB8888, &input.bg_layer.color, &input.bg_layer.g_alpha);
    CHECK_RETURN(HAL_MOD_G2DLITE, ret, "transform color");
    HAL_LOG_I(HAL_MOD_G2DLITE, "bg :color[%#x] alpha[%#x]", input.bg_layer.color, input.bg_layer.g_alpha);
    input.bg_layer.aaddr = (addr_t)(info->maskInfo.buf + info->maskInfo.rect.y * info->maskInfo.stride + info->maskInfo.rect.x);
    input.bg_layer.bpa = 8;
    input.bg_layer.astride = info->maskInfo.stride;
    input.bg_layer.zorder = 1;
    input.output.width = info->output.dstRect.w;
    input.output.height = info->output.dstRect.h;
    ret = G2DLITE_TransformFmtType(info->output.fmt, &input.output.fmt);
    CHECK_RETURN(HAL_MOD_G2DLITE, ret, "transform fmt type");
    int out_bpp = COMM_getFmtBpp(info->output.fmt);
    input.output.addr[0] = (u64)(info->output.buf +
        (info->output.dstRect.y * info->output.stride + info->output.dstRect.x * out_bpp));

    input.output.stride[0] = info->output.stride;
    input.output.rotation = 0;
    sdrv_g2dlite_update(&g2dlite_dev, &input);
    return HAL_SUCCESS;
}


int HAL_G2DLITE_FillRect(const HAL_FillRectInfo *info)
{
    CHECK_NULLPTR_RETURN(HAL_MOD_G2DLITE, info, HAL_G2DLITE_NULL_PTR);
    CHECK_NULLPTR_RETURN(HAL_MOD_G2DLITE, info->buf, HAL_G2DLITE_NULL_PTR);

    HAL_LOG_D(HAL_MOD_G2DLITE, "out :buf[%#x] stride[%d] format[%d], rect[%u, %u, %u, %u]",
        info->buf, info->stride, info->fmt,
        info->dstRect.x, info->dstRect.y, info->dstRect.w, info->dstRect.h);

    struct g2dlite_bg_cfg bgcfg;
    memset(&bgcfg, 0, sizeof(bgcfg));
    bgcfg.en = 1;
    int ret = G2DLITE_TransformColor(info->color, HAL_FMT_ARGB8888, &bgcfg.color, &bgcfg.g_alpha);
    CHECK_RETURN(HAL_MOD_G2DLITE, ret, "transform color");
    bgcfg.aaddr = 0;
    bgcfg.bpa = 0;
    bgcfg.astride = 0;

    struct g2dlite_output_cfg output;
    memset(&output, 0, sizeof(output));

    output.width = info->dstRect.w;
    output.height = info->dstRect.h;
    ret = G2DLITE_TransformFmtType(info->fmt, &output.fmt);
    CHECK_RETURN(HAL_MOD_G2DLITE, ret, "transform fmt type");
    int out_bpp = COMM_getFmtBpp(info->fmt);
    output.addr[0] = (u64)(info->buf +
        (info->dstRect.y * info->stride + info->dstRect.x * out_bpp));
    output.stride[0] =  info->stride;
    output.rotation = 0;
    HAL_LOG_I(HAL_MOD_G2DLITE, "output.addr[0] :[0x%lx] ", output.addr[0]);
    sdrv_g2dlite_fill_rect(&g2dlite_dev, &bgcfg, &output);

    HAL_LOG_D(HAL_MOD_G2DLITE, "out :color 0x%x: (bg_color) 0x%x, alpha %d", info->color, bgcfg.color, bgcfg.g_alpha);
    return HAL_SUCCESS;
}

int HAL_G2DLITE_FillTriangle(const HAL_FillRatriaInfo *info)
{
    HAL_LOG_E(HAL_MOD_G2DLITE, "g2d not support triangle!\n");
    return HAL_G2DLITE_NOT_SUPPORT;
}

/**
 *@in:
 *@@addr:input buffer address
 *@@width@height: input buffer size = in->width * in->height (uint is Word)
 *@@stride:input buffer pitch (uint is Byte)
 *@out:
 *@@addr:out buffer address
 *@@width@height: input buffer size = out->width * out->height (uint is Word)
 *@@stride:out buffer pitch (uint is Byte), must align to 128 bits
 */
int HAL_G2DLITE_FastCopy(const HAL_G2DLITE_FastCopyInfo *src,
    const HAL_G2DLITE_FastCopyInfo *dst)
{
    CHECK_NULLPTR_RETURN(HAL_MOD_G2DLITE, src, HAL_G2DLITE_NULL_PTR);
    CHECK_NULLPTR_RETURN(HAL_MOD_G2DLITE, dst, HAL_G2DLITE_NULL_PTR);
    CHECK_NULLPTR_RETURN(HAL_MOD_G2DLITE, src->buf, HAL_G2DLITE_NULL_PTR);
    CHECK_NULLPTR_RETURN(HAL_MOD_G2DLITE, dst->buf, HAL_G2DLITE_NULL_PTR);

    /* the stride align at 16 byte */
    if ((dst->stride % 16 != 0) || (src->stride % 16 != 0)) {
        HAL_LOG_E(HAL_MOD_G2DLITE, "dst stride[%u] must allgn to 16!\n", dst->stride);
        return HAL_G2DLITE_ERR_PARAM;
    }

    if ((src->fmt != dst->fmt) || (src->rect.w != dst->rect.w) || (src->rect.h != dst->rect.h)) {
        HAL_LOG_E(HAL_MOD_G2DLITE, "src w[%u] h[%u] and fmt[%u]"\
                  "Must be the same as dst w[%u] h[%u] and fmt[%u]!\n",
                  src->rect.w, src->rect.h, src->fmt,
                  dst->rect.w, dst->rect.h,dst->fmt);
        return HAL_G2DLITE_ERR_PARAM;
    }
    int bpp = COMM_getFmtBpp(src->fmt);
    if (bpp < 0){
        HAL_LOG_E(HAL_MOD_G2DLITE, "src->fmt[%u]is invalid!\n", src->fmt);
        return HAL_G2DLITE_ERR_PARAM;
    }

    /* The width size is calculated in 4 byte */
    int copy_width_size = G2DLITE_GetUintWordSize(src->fmt, src->rect.w);
    if (copy_width_size < 0 ){
        return HAL_G2DLITE_ERR_PARAM;
    }
    HAL_LOG_I(HAL_MOD_G2DLITE, "copy width size[%u], uint 32bit!\n", copy_width_size);

    struct fcopy_t in, out;
    memset(&in, 0, sizeof(in));
    memset(&out, 0, sizeof(out));
    in.addr = (u32)(src->buf + src->rect.y * src->stride + src->rect.x * bpp);
    in.width = copy_width_size;
    in.height = src->rect.h;
    in.stride = src->stride;
    out.addr = (u32)(dst->buf + dst->rect.y * dst->stride + dst->rect.x * bpp);
    out.width = copy_width_size;
    out.height = dst->rect.h;
    out.stride = dst->stride;
    HAL_LOG_D(HAL_MOD_G2DLITE, "src rect [%u, %u, %u, %u], buf[%#x], stride[%u], fmt[%d]!\n",
        src->rect.x, src->rect.y, src->rect.w, src->rect.h, src->buf, src->stride, src->fmt);
    HAL_LOG_D(HAL_MOD_G2DLITE, "dst rect [%u, %u, %u, %u], buf[%#x], stride[%u], fmt[%d]!\n",
        dst->rect.x, dst->rect.y, dst->rect.w, dst->rect.h, dst->buf, dst->stride, dst->fmt);
    sdrv_g2dlite_fastcopy(&g2dlite_dev, &in, &out);

    return HAL_SUCCESS;
}
