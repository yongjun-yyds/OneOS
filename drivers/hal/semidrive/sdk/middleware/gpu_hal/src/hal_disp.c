/*
* hal_disp.c
*@brief hal disp function file.
*
* Copyright (c) 2012 Semidrive Semiconductor.
* All rights reserved.
*
* Description:
*
* Revision History:
* -----------------
* 2022/09/19 create this file
*/
#include <string.h>
#include "FreeRTOS.h"
#include <dispss/disp.h>
#include <dispss/disp_data_type.h>
#include "hal_comm_inner.h"
#include "hal_disp.h"

extern struct dc_dev g_dc_dev;

static int DISP_TransformFmtType(HAL_Format fmtType, int *fmt)
{
    switch (fmtType) {
        case HAL_FMT_RGB565:
            *fmt = COLOR_RGB565;
            break;
        case HAL_FMT_ARGB1555:
            *fmt = COLOR_ARGB1555;
            break;
        case HAL_FMT_ARGB4444:
            *fmt = COLOR_ARGB4444;
            break;
        case HAL_FMT_RGB888:
            *fmt = COLOR_RGB888;
            break;
        case HAL_FMT_ARGB8888:
            *fmt = COLOR_ARGB8888;
            break;
        default:
            HAL_LOG_E(HAL_MOD_DISP, "can't support this fmt:%d!\n", fmtType);
            return HAL_DISP_ERR_PARAM;
    }

    return HAL_SUCCESS;
}

HAL_HANDLE HAL_DISP_GetHandle(HAL_DISP_ScreenType type)
{
    if (type != HAL_SCREEN_TYPE_CLUSTER) {
        HAL_LOG_E(HAL_MOD_DISP, "can't support this type:%d!\n", type);
        return NULL;
    }
    return (HAL_HANDLE)&g_dc_dev;
}
int HAL_DISP_Post(HAL_HANDLE handle, const HAL_DISP_Info *info)
{
    CHECK_NULLPTR_RETURN(HAL_MOD_DISP, handle, HAL_DISP_NULL_PTR);
    CHECK_NULLPTR_RETURN(HAL_MOD_DISP, info, HAL_DISP_NULL_PTR);

    struct post_cfg post;
    memset(&post, 0, sizeof(struct post_cfg));
    if ((info->winCnt == 0) || (info->winCnt > HAL_DISP_WIN_MAX_CNT)) {
        HAL_LOG_E(HAL_MOD_DISP, "info->winCnt[%d] can not be more than %d and has to be greater than 0 !\n",
            info->winCnt, HAL_DISP_WIN_MAX_CNT);
        return HAL_DISP_ERR_PARAM;
    }
    HAL_LOG_D(HAL_MOD_DISP, "winCnt[%d]!\n", info->winCnt, HAL_DISP_WIN_MAX_CNT);

    struct surface layer[HAL_DISP_WIN_MAX_CNT];
    memset(layer, 0, sizeof(struct surface) * HAL_DISP_WIN_MAX_CNT);
    post.num_layers = info->winCnt;
    post.layers = layer;
    for (int i = 0; i < info->winCnt; i++) {
        if (info->winInfo[i].en && info->winInfo[i].addr[0] == NULL) {
            HAL_LOG_E(HAL_MOD_DISP, "info->winInfo[%d].addr[0] is null!\n", layer[i].addr[0]);
            return HAL_DISP_NULL_PTR;
        }
        const HAL_DISP_WindowInfo *winInfo = &info->winInfo[i];
        HAL_LOG_D(HAL_MOD_DISP, "winInfo->src.w[%d] winInfo->src.h %d!\n", winInfo ->src.w, winInfo ->src.h);
        HAL_LOG_D(HAL_MOD_DISP, "winInfo->dst.w[%d] winInfo->dst.h %d!\n", winInfo->dst.w, winInfo->dst.h);
        HAL_LOG_D(HAL_MOD_DISP, "winInfo->src.x[%d] winInfo->src.y %d!\n", winInfo->src.x, winInfo->src.y);
        HAL_LOG_D(HAL_MOD_DISP, "winInfo->dst.x[%d] winInfo->dst.y %d!\n", winInfo->dst.x, winInfo->dst.y);
        layer[i].id = winInfo->id;
        layer[i].dirty = winInfo->dirty;
        layer[i].en = winInfo->en;
        int ret = DISP_TransformFmtType(winInfo->fmt, &layer[i].fmt);
        CHECK_RETURN(HAL_MOD_DISP, ret, "transform fmt type");
        layer[i].src.x = winInfo->src.x;
        layer[i].src.y = winInfo->src.y;
        layer[i].src.w = winInfo->src.w;
        layer[i].src.h = winInfo->src.h;

        layer[i].start.x = winInfo->src.x;
        layer[i].start.y = winInfo->src.y;
        layer[i].start.w = winInfo->src.w;
        layer[i].start.h = winInfo->src.h;
        for (int j = 0; j < HAL_DISP_YUVA_ADDR_CNT; j++) {
            layer[i].addr[j] = (unsigned long)winInfo->addr[j];
            layer[i].src_stride[j] = winInfo->srcStride[j];
            HAL_LOG_D(HAL_MOD_DISP, "winInfo->src_stride[%d] %d!\n", j, winInfo->srcStride[j]);
            HAL_LOG_D(HAL_MOD_DISP, "layer[%d].addr[%d]:%#x!\n", i, j, layer[i].addr[j]);
        }

        layer[i].dst.x = winInfo->dst.x;
        layer[i].dst.y = winInfo->dst.y;
        layer[i].dst.w = winInfo->dst.w;
        layer[i].dst.h = winInfo->dst.h;

        layer[i].z_order = winInfo->zOrder;
        layer[i].alpha_en = winInfo->enAlpha;
        layer[i].alpha = winInfo->alpha;
        layer[i].ckey_en = winInfo->enCkey;
        layer[i].security = winInfo->security;
    }
    sdrv_display_post((struct dc_dev *)handle, &post);

    return HAL_SUCCESS;
}

