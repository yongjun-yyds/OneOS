/*hal_gpu_define.h
*@brief hal gpu define header file.
*
* Copyright (c) 2012 Semidrive Semiconductor.
* All rights reserved.
*
* Description:
*
* Revision History:
* -----------------
* 2022/09/19  create this file
*/
#ifndef __HAL_GPU_DEFINE_H__
#define __HAL_GPU_DEFINE_H__

#include "hal_errno.h"
#include "hal_comm_define.h"

#define HAL_GPU_NOT_INIT    HAL_GENERATE_ERRNO(HAL_MOD_GPU, HAL_ERRNO_NOT_INIT)
#define HAL_GPU_NULL_PTR    HAL_GENERATE_ERRNO(HAL_MOD_GPU, HAL_ERRNO_NULL_PTR)
#define HAL_GPU_ERR_PARAM   HAL_GENERATE_ERRNO(HAL_MOD_GPU, HAL_ERRNO_ERR_PARAM)
#define HAL_GPU_NOT_SUPPORT HAL_GENERATE_ERRNO(HAL_MOD_GPU, HAL_ERRNO_NOT_SUPPORT)
#define HAL_GPU_NO_MEM      HAL_GENERATE_ERRNO(HAL_MOD_GPU, HAL_ERRNO_NO_MEM)


/************old define*************/
typedef struct {
    uint8_t * buf;  /* tmp buf for rotation */
    int buf_len;    /* tmp buf len for rotation */
} aswlite_rotate_buf;


typedef struct {
    int src_x;
    int src_y;
    int src_w;
    int src_h;
    uint8_t *src_buf;
    int src_fmt;
    int src_stride;

    uint8_t *dst_buf;
    int dst_fmt;
    int dst_w;
    int dst_h;
    int dst_stride;
    double angle;
    int bg_color;
} hal_asw_rotate_info;
#endif // __HAL_GPU_DEFINE_H__
