/*
* hal_gpu.h
*@brief hal gpu header file.
*
* Copyright (c) 2012 Semidrive Semiconductor.
* All rights reserved.
*
* Description:
*
* Revision History:
* -----------------
* 2022/06/01  create this file
*/
#ifndef __HAL_GPU_H__
#define __HAL_GPU_H__

#include "hal_gpu_define.h"


int HAL_GPU_Rotate(const HAL_RotateInfo *info);
int HAL_GPU_RotateEx(const HAL_RotateInfo *info);

#endif //__HAL_GPU_H__
