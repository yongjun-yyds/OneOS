/*
* hal_disp.h
*@brief hal disp header file.
*
* Copyright (c) 2012 Semidrive Semiconductor.
* All rights reserved.
*
* Description:
*
* Revision History:
* -----------------
* 2022/09/19  create this file
*/
#ifndef __HAL_DISP_H__
#define __HAL_DISP_H__

#include "hal_disp_define.h"
#include "hal_comm_define.h"

HAL_HANDLE HAL_DISP_GetHandle(HAL_DISP_ScreenType type);
int HAL_DISP_Post(HAL_HANDLE handle, const HAL_DISP_Info *info);

#endif // __HAL_DISP_H__