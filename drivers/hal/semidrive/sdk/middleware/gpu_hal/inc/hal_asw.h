/*
* hal_asw.h
*@brief hal asw header file.
*
* Copyright (c) 2012 Semidrive Semiconductor.
* All rights reserved.
*
* Description:
*
* Revision History:
* -----------------
* 2022/09/19  create this file
*/
#ifndef __HAL_ASW_H__
#define __HAL_ASW_H__

#include "hal_asw_define.h"
#include "hal_comm_define.h"

/* full buf stride, must align 8 bit */
int HAL_ASW_FillTriangle(const HAL_FillRatriaInfo *info);

/* full buf stride, must align 8 bit */
int HAL_ASW_FillRect(const HAL_FillRectInfo *info);

/* cpu copy, Multi-threading is not supported */
int HAL_ASW_Rotate(const HAL_RotateInfo *info);

int HAL_ASW_RotateEx(const HAL_RotateInfo *info);


/****************************************************
                 inner function
*****************************************************/
int ASW_Rotate(const ASW_RotateInfo *info);

#endif //__HAL_ASW_H__
