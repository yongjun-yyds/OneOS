/*
* hal_g2dlite_define.h
*@brief hal g2dlite define header file.
*
* Copyright (c) 2012 Semidrive Semiconductor.
* All rights reserved.
*
* Description:
*
* Revision History:
* -----------------
* 2022/09/23  create this file
*/
#ifndef __HAL_G2DLITE_DEFINE_H__
#define __HAL_G2DLITE_DEFINE_H__

#include "hal_errno.h"
#include "hal_comm_define.h"

#define HAL_G2DLITE_NOT_INIT    HAL_GENERATE_ERRNO(HAL_MOD_G2DLITE, HAL_ERRNO_NOT_INIT)
#define HAL_G2DLITE_NULL_PTR    HAL_GENERATE_ERRNO(HAL_MOD_G2DLITE, HAL_ERRNO_NULL_PTR)
#define HAL_G2DLITE_ERR_PARAM   HAL_GENERATE_ERRNO(HAL_MOD_G2DLITE, HAL_ERRNO_ERR_PARAM)
#define HAL_G2DLITE_NOT_SUPPORT HAL_GENERATE_ERRNO(HAL_MOD_G2DLITE, HAL_ERRNO_NOT_SUPPORT)

typedef enum {
    HAL_G2DLITE_RLE_IN_BG_SURFACE = 0,  /* run-length encoding in the background surface */
    HAL_G2DLITE_RLE_IN_FG_SURFACE,      /* run-length encoding in the foreground surface */
    HAL_G2DLITE_RLE_BUTT
} HAL_G2DLITE_RleSurfaceType;

typedef struct {
    HAL_G2DLITE_RleSurfaceType rleType; /* run-length encoding type */
} HAL_G2DLITE_RleInfo;

/* porter duff blend mode */
typedef enum {
    HAL_G2DLITE_PD_MODE_CLEAR = 0,
    HAL_G2DLITE_PD_MODE_SRC,
    HAL_G2DLITE_PD_MODE_DST,
    HAL_G2DLITE_PD_MODE_SRC_OVER,
    HAL_G2DLITE_PD_MODE_DST_OVER,
    HAL_G2DLITE_PD_MODE_SRC_IN,
    HAL_G2DLITE_PD_MODE_DST_IN,
    HAL_G2DLITE_PD_MODE_SRC_OUT,
    HAL_G2DLITE_PD_MODE_DST_OUT,
    HAL_G2DLITE_PD_MODE_SRC_ATOP,
    HAL_G2DLITE_PD_MODE_DST_ATOP,
    HAL_G2DLITE_PD_MODE_XOR,
    HAL_G2DLITE_PD_MODE_DARKEN,
    HAL_G2DLITE_PD_MODE_LIGHTEN,
    HAL_G2DLITE_PD_MODE_MULTIPLY,
    HAL_G2DLITE_PD_MODE_SCREEN,
    HAL_G2DLITE_PD_MODE_ADD,
    HAL_G2DLITE_PD_MODE_OVERLAY,
    HAL_G2DLITE_PD_MODE_SRC_SUB,
    HAL_G2DLITE_PD_MODE_DES_SUB,
    HAL_G2DLITE_PD_MODE_BUTT
} HAL_G2DLITE_PdMode;

typedef enum {
    HAL_G2DLITE_PD_TYPE_SRC_IN_BG = 0, /* porter duff src in background surface */
    HAL_G2DLITE_PD_TYPE_SRC_IN_FG,     /* porter duff src in in the foreground surface */
    HAL_G2DLITE_PD_TYPE_BUTT
} HAL_G2DLITE_PdSurfaceType;

typedef struct {
    HAL_G2DLITE_PdMode pdMode;        /* porter duff blend mode */
    HAL_G2DLITE_PdSurfaceType pdType; /* porter duff type */
} HAL_G2DLITE_PorterDuffInfo;

typedef enum {
    HAL_G2DLITE_BLEND_PIXEL_NONE = 0,  /* Use of global alpha */
    HAL_G2DLITE_BLEND_PIXEL_PREMULTI,  /* Premultiply, alpha is already premultiplied into data. */
    HAL_G2DLITE_BLEND_PIXEL_COVERAGE,  /* Use of iamge alpha */
    HAL_G2DLITE_BLEND_PIXEL_BUTT
} HAL_G2DLITE_BlendMode;

typedef struct {
    uint8_t *buf;                 // surface buf addr
    uint8_t alpha;                // surface alpha
    uint32_t stride;              // surface buf stride
    HAL_Format fmt;               // surface format type
    HAL_Rect srcRect;             // surface source rect
    HAL_Rect dstRect;             // surface destination rect
    HAL_G2DLITE_BlendMode blendMode;  // surface blend mode
} HAL_G2DLITE_SurfaceInfo;

typedef struct {
    uint8_t *buf;                // output buf addr
    uint32_t stride;             // output rect buf stride
    HAL_Format fmt;              // output rect buf format
    HAL_Rect dstRect;            // Output to the area of the output rectangle
} HAL_G2DLITE_OutputInfo;

typedef struct {
    HAL_G2DLITE_SurfaceInfo fgSurface;  // need to synthetic the upper surface
    HAL_G2DLITE_SurfaceInfo bgSurface;  // need to synthesize the lower surface
    HAL_G2DLITE_OutputInfo  output;     // outout rect info
} HAL_G2DLITE_BlendImgInfo;

typedef struct {
    uint8_t *buf;     // mask area buf
    uint32_t color;   // mask area color, ARGB888, A:bit[31:24], R:bit[23:16], G:bit[15:8], B:bit[7:0]
    uint32_t stride;  // mask area buf stride
    HAL_Rect rect;    // mask area
} HAL_G2DLITE_MaskInfo;

typedef struct {
    HAL_G2DLITE_SurfaceInfo surfaceInfo;  // need to synthetic the surface
    HAL_G2DLITE_OutputInfo output;        // outout area info
    HAL_G2DLITE_MaskInfo maskInfo;        // need to synthetic the mask area info
} HAL_G2DLITE_BlendMaskInfo;

typedef struct {
    HAL_G2DLITE_OutputInfo output;      // outout area info
    HAL_G2DLITE_MaskInfo maskInfo;      // need to synthetic the mask area info
} HAL_G2DLITE_FillMaskInfo;

typedef struct {
    uint8_t *buf;       // copy area buf addr
    HAL_Format fmt;    // copy area format
    HAL_Rect rect;      // copy region location
    uint32_t stride;    // copy area buf stride
} HAL_G2DLITE_FastCopyInfo;

#endif // __HAL_G2DLITE_DEFINE_H__
