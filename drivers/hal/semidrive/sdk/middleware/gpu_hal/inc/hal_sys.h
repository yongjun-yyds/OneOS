/*
* hal_sys.h
*@brief hal sys header file.
*
* Copyright (c) 2012 Semidrive Semiconductor.
* All rights reserved.
*
* Description:
*
* Revision History:
* -----------------
* 2022/09/19  create this file
*/
#ifndef __HAL_SYS_H__
#define __HAL_SYS_H__

#include "hal_sys_define.h"
#include "hal_comm_define.h"

int HAL_SYS_CleanCacheRange(const HAL_SYS_CacheInfo *info);
int HAL_SYS_InvalidateCacheRange(const HAL_SYS_CacheInfo *info);
int HAL_SYS_CleanInvalidateCacheRange(const HAL_SYS_CacheInfo *info);
void HAL_SYS_CleanInvalidateCacheAll(void);

#endif //__HAL_SYS_H__