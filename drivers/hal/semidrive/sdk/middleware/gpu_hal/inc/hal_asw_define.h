/*
* hal_asw_define.h
*@brief hal asw define header file.
*
* Copyright (c) 2012 Semidrive Semiconductor.
* All rights reserved.
*
* Description:
*
* Revision History:
* -----------------
* 2022/06/01  create this file
*/
#ifndef __HAL_ASW_DEFINE_H__
#define __HAL_ASW_DEFINE_H__

#include "hal_errno.h"
#include "hal_comm_define.h"

#define HAL_ASW_NOT_INIT    HAL_GENERATE_ERRNO(HAL_MOD_ASW, HAL_ERRNO_NOT_INIT)
#define HAL_ASW_NULL_PTR    HAL_GENERATE_ERRNO(HAL_MOD_ASW, HAL_ERRNO_NULL_PTR)
#define HAL_ASW_ERR_PARAM   HAL_GENERATE_ERRNO(HAL_MOD_ASW, HAL_ERRNO_ERR_PARAM)
#define HAL_ASW_NOT_SUPPORT HAL_GENERATE_ERRNO(HAL_MOD_ASW, HAL_ERRNO_NOT_SUPPORT)
#define HAL_ASW_NO_MEM      HAL_GENERATE_ERRNO(HAL_MOD_ASW, HAL_ERRNO_NO_MEM)

typedef struct {
    HAL_RotateInfo rotate;
    uint8_t *tmpBuf;     /* tmp rect buf */
    uint32_t tmpStride;  /* tmp rect buf strde*/
} ASW_RotateInfo;

#endif // __HAL_ASW_DEFINE_H__
