/*
* hal_sys_define.h
*@brief hal sys define header file.
*
* Copyright (c) 2012 Semidrive Semiconductor.
* All rights reserved.
*
* Description:
*
* Revision History:
* -----------------
* 2022/09/19  create this file
*/
#ifndef __HAL_SYS_DEFINE_H__
#define __HAL_SYS_DEFINE_H__

#include "hal_errno.h"
#include "hal_comm_define.h"

#define HAL_SYS_NOT_INIT    HAL_GENERATE_ERRNO(HAL_MOD_SYS, HAL_ERRNO_NOT_INIT)
#define HAL_SYS_NULL_PTR    HAL_GENERATE_ERRNO(HAL_MOD_SYS, HAL_ERRNO_NULL_PTR)
#define HAL_SYS_ERR_PARAM   HAL_GENERATE_ERRNO(HAL_MOD_SYS, HAL_ERRNO_ERR_PARAM)
#define HAL_SYS_NOT_SUPPORT HAL_GENERATE_ERRNO(HAL_MOD_SYS, HAL_ERRNO_NOT_SUPPORT)

typedef struct {
    uint8_t *buf;      /* cache buf addr */
    HAL_Rect rect;     /* clean cache rect */
    HAL_Format fmt;    /* cache format */
    uint32_t stride;   /* cache buf format */
} HAL_SYS_CacheInfo;

#endif // __HAL_SYS_DEFINE_H__
