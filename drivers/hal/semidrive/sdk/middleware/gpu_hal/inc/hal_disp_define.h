/*
* hal_disp_define.h
*@brief hal disp define header file.
*
* Copyright (c) 2012 Semidrive Semiconductor.
* All rights reserved.
*
* Description:
*
* Revision History:
* -----------------
* 2022/09/19  create this file
*/
#ifndef __HAL_DISP_DEFINE_H__
#define __HAL_DISP_DEFINE_H__

#include "hal_errno.h"
#include "hal_comm_define.h"

#define HAL_DISP_NOT_INIT    HAL_GENERATE_ERRNO(HAL_MOD_DISP, HAL_ERRNO_NOT_INIT)
#define HAL_DISP_NULL_PTR    HAL_GENERATE_ERRNO(HAL_MOD_DISP, HAL_ERRNO_NULL_PTR)
#define HAL_DISP_ERR_PARAM   HAL_GENERATE_ERRNO(HAL_MOD_DISP, HAL_ERRNO_ERR_PARAM)
#define HAL_DISP_NOT_SUPPORT HAL_GENERATE_ERRNO(HAL_MOD_DISP, HAL_ERRNO_NOT_SUPPORT)

#define HAL_DISP_WIN_MAX_CNT 2
#define HAL_DISP_YUVA_ADDR_CNT 4

typedef enum  {
    HAL_SCREEN_TYPE_CLUSTER = 0,
    HAL_SCREEN_TYPE_BUTT
} HAL_DISP_ScreenType;

typedef struct {
    uint32_t id;
    int dirty;
    int en;
    HAL_Format fmt;
    HAL_Rect src;
    uint8_t *addr[HAL_DISP_YUVA_ADDR_CNT];//YUVA
    uint32_t srcStride[HAL_DISP_YUVA_ADDR_CNT];
    HAL_Rect dst;
    int enCkey;
    int ckey;
    int enAlpha;
    char alpha;
    int zOrder;
    int security;
} HAL_DISP_WindowInfo;

typedef struct {
    uint32_t winCnt;
    HAL_DISP_WindowInfo winInfo[HAL_DISP_WIN_MAX_CNT];
} HAL_DISP_Info;

#endif // __HAL_DISP_DEFINE_H__
