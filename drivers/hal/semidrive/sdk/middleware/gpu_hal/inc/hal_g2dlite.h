/*
* hal_g2dlite.h
*@brief hal g2dlite header file.
*
* Copyright (c) 2012 Semidrive Semiconductor.
* All rights reserved.
*
* Description:
*
* Revision History:
* -----------------
* 2022/06/01  create this file
*/
#ifndef __HAL_G2DLITE_H__
#define __HAL_G2DLITE_H__

#include "hal_g2dlite_define.h"

int HAL_G2DLITE_Init();
int HAL_G2DLITE_Deinit();
int HAL_G2DLITE_BlendImg(const HAL_G2DLITE_BlendImgInfo *info);
int HAL_G2DLITE_BlendImgWithPd(const HAL_G2DLITE_BlendImgInfo *info, HAL_G2DLITE_PorterDuffInfo *pdInfo);
int HAL_G2DLITE_BlendRleImg(const HAL_G2DLITE_BlendImgInfo *info, HAL_G2DLITE_RleInfo *rleInfo);
int HAL_G2DLITE_BlendRleImgWithPd(const HAL_G2DLITE_BlendImgInfo *info, HAL_G2DLITE_RleInfo *rleInfo, HAL_G2DLITE_PorterDuffInfo *pdInfo);
int HAL_G2DLITE_BlendMask(const HAL_G2DLITE_BlendMaskInfo *info);
int HAL_G2DLITE_FillMask(const HAL_G2DLITE_FillMaskInfo *info);
int HAL_G2DLITE_FillTriangle(const HAL_FillRatriaInfo *triangle_info);
int HAL_G2DLITE_FillRect(const HAL_FillRectInfo *rect);
int HAL_G2DLITE_FastCopy(const HAL_G2DLITE_FastCopyInfo *src,
    const HAL_G2DLITE_FastCopyInfo *dst);

#endif //__HAL_G2DLITE_H__
