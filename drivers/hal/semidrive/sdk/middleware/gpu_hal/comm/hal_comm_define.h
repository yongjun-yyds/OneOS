/*
* hal_comm_define.h
*@brief hal common header file.
*
* Copyright (c) 2012 Semidrive Semiconductor.
* All rights reserved.
*
* Description:
*
* Revision History:
* -----------------
* 2022/09/06  create this file
*/
#ifndef __HAL_COMM_DEFINE_H__
#define __HAL_COMM_DEFINE_H__

#include <stdint.h>

#define CONFIG_ASW 1

typedef void* HAL_HANDLE;

/* module id */
typedef enum {
    HAL_MOD_COMMON = 0, /* common module */
    HAL_MOD_SYS,        /* system module */
    HAL_MOD_G2DLITE,    /* g2dlite module */
    HAL_MOD_ASW,        /* asw module */
    HAL_MOD_DISP,       /* disp module */
    HAL_MOD_SW,         /* Software implementation module */
    HAL_MOD_GPU,        /* gpu module */
    HAL_MOD_BUTT
} HAL_ModId;

/* fmt */
typedef enum {
    HAL_FMT_A8 = 0,             /* alpha 8bit */
    HAL_FMT_RGB565,             /* rgb565 */
    HAL_FMT_ARGB4444,           /* argb4444 */
    HAL_FMT_ARGB1555,           /* argb1555 */
    HAL_FMT_RGB888,             /* rgb888 */
    HAL_FMT_ARGB8888,           /* argb8888 */
    HAL_FMT_BUTT
} HAL_Format;

typedef struct {
    uint32_t x; /* The abscissa of a rectangle */
    uint32_t y; /* The ordinate of the rectangle */
    uint32_t w; /* the width of the rectangle */
    uint32_t h; /* the height of the rectangle */
} HAL_Rect;

typedef struct {
    uint32_t x; /* The abscissa of a rectangle */
    uint32_t y; /* The ordinate of the rectangle */
} HAL_Point;


typedef struct {
    uint32_t color;    /* full color */
    HAL_Format fmt;  /* buf fmt */
    uint8_t *buf;      /* buf addr */
    uint32_t stride;   /* buf stride */
    HAL_Rect dstRect; /* full rect */
} HAL_FillRectInfo;

/* Triangles are cut from rectangles */
typedef enum {
    HAL_TRIANGLE_TOP_LEFT = 0,  /* shows the triangle in the top left corner */
    HAL_TRIANGLE_BOTTOM_LEFT,   /* shows the triangle in the bottom left corner */
    HAL_TRIANGLE_TOP_RIGHT,     /* shows the triangle in the top right corner */
    HAL_TRIANGLE_BOTTOM_RIGHT,  /* shows the triangle in the bottom right corner */
    HAL_TRIANGLE_BUTT
} HAL_TRIANLE_Type;

typedef struct  {
    HAL_TRIANLE_Type type;  /* The area type of a triangle */
    HAL_Format fmt;       /* The format type of a triangle */
    uint32_t color;         /* The color of a triangle */
    HAL_Rect dstRect;      /* The color of a triangle */
    uint8_t *buf;           /* The buf of a triangle */
    uint32_t stride;        /* The stride of buf */
} HAL_FillRatriaInfo;

typedef struct {
    HAL_Rect srcRect;    /* the rect for rotate */
    uint8_t *srcBuf;     /* the rect buf for rotation */
    HAL_Format srcFmt; /* the rect format for rotation */
    uint32_t srcStride;  /* the rect buf stride */

    uint8_t *dstBuf;     /* output rect buf */
    HAL_Format dstFmt; /* output rect format*/
    uint32_t dstWidth;   /* output rect widtht*/
    uint32_t dstHeight;  /* output rect height*/
    uint32_t dstStride;  /* output rect buf strde*/

    double angle;         /* rotate angle */
    /* ARGB888, A:bit[31:24], R:bit[23:16], G:bit[15:8], B:bit[7:0]
       Pixels outside the rotated region fill the background color */
    uint32_t bgColor;
} HAL_RotateInfo;


#endif //__HAL_COMM_DEFINE_H__
