/*
* hal_comm_inner.h
*@brief hal common inner header file.
*
* Copyright (c) 2012 Semidrive Semiconductor.
* All rights reserved.
*
* Description:
*
* Revision History:
* -----------------
* 2022/09/06  create this file
*/
#ifndef __HAL_COMM_INNER_H__
#define __HAL_COMM_INNER_H__

#include "stdio.h"
#include "hal_comm_define.h"
#include "hal_errno.h"
#include <debug.h>

#define MOD_NAME_LEN 16

static char g_modName[HAL_MOD_BUTT + 1][MOD_NAME_LEN] = {
    "common",
    "sys",
    "g2dlite",
    "asw",
    "disp",
    "sw",
    "gpu",
    "unkown"
};

#if HAL_DEBUG_LEVEL <= HAL_LOG_INFO
#define HAL_LOG_I(mod_, info, ...) \
    printf("info [%s][%s][%d]: "info"!\r\n", g_modName[mod_], __FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#define HAL_LOG_I(mod_, info, ...)
#endif

#if HAL_DEBUG_LEVEL <= HAL_LOG_DUBUG
#define HAL_LOG_D(mod_, info, ...) \
    printf("debug [%s][%s][%d]: "info"!\r\n", g_modName[mod_], __FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#define HAL_LOG_D(mod_, info, ...)
#endif

#if HAL_DEBUG_LEVEL <= HAL_LOG_WARNING
#define HAL_LOG_W(mod_, info, ...) \
    printf("warning [%s][%s][%d]: "info"!\r\n", g_modName[mod_], __FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#define HAL_LOG_W(mod_, info, ...)
#endif

#if HAL_DEBUG_LEVEL <= HAL_LOG_ERROR
#define HAL_LOG_E(mod_, info, ...) \
    printf("error [%s][%s][%d]: "info"!\r\n", g_modName[mod_], __FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#define HAL_LOG_E(mod_, info, ...)
#endif

#if HAL_DEBUG_LEVEL <= HAL_LOG_FAIL
#define HAL_LOG_F(mod_, info, ...) \
    printf("fail [%s][%s][%d]: "info"!\r\n", g_modName[mod_], __FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#define HAL_LOG_F(mod_, info, ...)
#endif

#define CHECK_NULLPTR_RETURN(mod_, ptr_, errno_) \
do { \
    if ((ptr_) == NULL) { \
        HAL_LOG_E(mod_, "%s is null!", #ptr_); \
        return errno_; \
    } \
} while (0);

#define CHECK_RETURN(mod_, ret_, str_) \
do { \
    if ((ret_) != HAL_SUCCESS) { \
        HAL_LOG_E(mod_, "%s fail,ret:%#x!", str_, ret_); \
        return ret_; \
    } \
} while (0);


#define CHECK_GOTO(mod_, ret_, str_, goto_) \
do { \
    if ((ret_) != HAL_SUCCESS) { \
        HAL_LOG_E(mod_, "%s fail,ret:%#x!", str_, ret_); \
        goto goto_; \
    } \
} while (0);

typedef struct {
    uint8_t * buf;  /* tmp buf for rotation */
    int bufLen;    /* tmp buf len for rotation */
} COMM_RotateBuf;

typedef struct {
    HAL_RotateInfo rotateInfo; /* rotate info */
    int maxRotateAngle;        /* max rotate angle */
} COMM_RotateContext;

int COMM_getFmtBpp(HAL_Format fmt);

#endif //__G2D_HAL_H__
