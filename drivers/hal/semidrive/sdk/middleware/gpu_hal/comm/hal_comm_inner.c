/*
* hal_comm_inner.c
*@brief hal common inner file.
*
* Copyright (c) 2012 Semidrive Semiconductor.
* All rights reserved.
*
* Description:
*
* Revision History:
* -----------------
* 2022/09/19 create this file
*/
#include <string.h>
#include "FreeRTOS.h"
#include <dispss/disp.h>
#include <dispss/disp_data_type.h>
#include "hal_comm_inner.h"
#include "hal_disp.h"

extern struct dc_dev g_dc_dev;

int COMM_getFmtBpp(HAL_Format fmt)
{
    switch (fmt) {
        case HAL_FMT_A8:
            return 1;
        case HAL_FMT_RGB565:
        case HAL_FMT_ARGB1555:
        case HAL_FMT_ARGB4444:
            return 2;
        case HAL_FMT_RGB888:
            return 3;
        case HAL_FMT_ARGB8888:
            return 4;
        default:
            HAL_LOG_E(HAL_MOD_COMMON, "can't support this fmt:%d!\n", fmt);
            return -1;
    }
}

