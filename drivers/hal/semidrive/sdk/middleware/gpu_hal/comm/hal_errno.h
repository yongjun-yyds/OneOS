/*
* hal_errno.h
*@brief hal errno header file.
*
* Copyright (c) 2012 Semidrive Semiconductor.
* All rights reserved.
*
* Description:
*
* Revision History:
* -----------------
* 2022/09/06  create this file
*/
#ifndef __HAL_ERRNO_H__
#define __HAL_ERRNO_H__

enum {
    HAL_ERRNO_NOT_INIT = 0, /* The module is not initialized. */
    HAL_ERRNO_NULL_PTR,     /* Null pointer exception */
    HAL_ERRNO_ERR_PARAM,    /* Parameters of the abnormal */
    HAL_ERRNO_NOT_SUPPORT,  /* This operation is not supported */
    HAL_ERRNO_NO_MEM,       /* System memory is insufficient */
};

enum {
    HAL_FAILURE = -1,
    HAL_SUCCESS = 0
};

/* module: bit[31:16],errno: bit[15:0] */
#define HAL_GENERATE_ERRNO(mod_, errno_) ((mod_) << 16) + (errno_)

/* log level enum */
#define HAL_LOG_INFO     0
#define HAL_LOG_DUBUG    1
#define HAL_LOG_WARNING  2
#define HAL_LOG_ERROR    3
#define HAL_LOG_FAIL     4

/* define log level */
#define HAL_DEBUG_LEVEL HAL_LOG_ERROR

#endif //__G2D_HAL_H__
