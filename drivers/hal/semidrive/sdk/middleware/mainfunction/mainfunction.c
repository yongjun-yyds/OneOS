/**
 * @file mainfunction.c
 * @brief Main function source file
 *
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */

#include <irq.h>
#include <mainfunction/mainfunction.h>
#include <string.h>

/* Global array for module. */
static module_mainfunction_t mainfunction_nodes[MAINFUNCTION_NODE_MAX];

/**
 * @brief Mainfunction handler.
 *
 * This function handle all nodes and call module's callback function.
 *
 * This function is used by :
 * 1. Timer handler,
 * 2. RTOS Task.
 *
 */
void mainfunction_handler(void)
{
    uint32_t ulCpsr;
    module_mainfunction_t *node = NULL;
    ulCpsr = arch_irq_save();
    for (uint8_t i = 0; i < MAINFUNCTION_NODE_MAX; ++i) {
        if (mainfunction_nodes[i].valid) {
            node = &mainfunction_nodes[i];
            ++node->count;
            if (node->period_ms == node->count) {
                if (NULL != node->callback) {
                    node->callback(node->userdata);
                }
                node->count = 0;
            }
        }
    }
    arch_irq_restore(ulCpsr);
}

/**
 * @brief Attach main function feature.
 *
 * This function is used by module which need handle interrupt without VIC.
 *
 * @param[out] node Module mainfunction node ptr;
 * @param[in]  period_ms The period(ms) of calling module handler;
 * @param[in]  callback User callback function.
 * @param[in]  userdata The user's param of callback function.
 *
 * @return The result of this function.
 */
mainfunction_ret_status_t mainfunction_attach(module_mainfunction_t **node,
                                              uint32_t period_ms,
                                              module_callback_t callback,
                                              void *userdata)
{
    if ((NULL == node) || (NULL == callback))
        return STATUS_MAINFUNCTION_FAIL;

    uint32_t ulCpsr;
    module_mainfunction_t *module_node = NULL;
    ulCpsr = arch_irq_save();
    for (uint8_t i = 0; i < MAINFUNCTION_NODE_MAX; ++i) {
        if (false == mainfunction_nodes[i].valid) {
            *node = &mainfunction_nodes[i];
            module_node = *node;
            module_node->period_ms = period_ms;
            module_node->count = 0;
            module_node->callback = callback;
            module_node->userdata = userdata;
            module_node->valid = true;
            arch_irq_restore(ulCpsr);
            return STATUS_MAINFUNCTION_SUCCESS;
        }
    }
    arch_irq_restore(ulCpsr);

    return STATUS_MAINFUNCTION_NOSPACE;
}

/**
 * @brief Detach main function feature.
 *
 * This function is used by module to detach the feature of main function.
 *
 * @param[in] node The mainfunction node ptr of module.
 *
 * @return The result of this function.
 */
mainfunction_ret_status_t mainfunction_detach(module_mainfunction_t *node)
{
    if (NULL == node) {
        return STATUS_MAINFUNCTION_FAIL;
    }
    uint32_t ulCpsr;
    ulCpsr = arch_irq_save();
    node->valid = false;
    arch_irq_restore(ulCpsr);

    return STATUS_MAINFUNCTION_SUCCESS;
}