/**
 * @file mainfunction.h
 * @brief Main function api
 *
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef __MAIN_FUNCTION
#define __MAIN_FUNCTION

#include <compiler.h>
#include <types.h>

#ifdef CONFIG_MAINFUNCTION_NODE_MAX
#define MAINFUNCTION_NODE_MAX CONFIG_MAINFUNCTION_NODE_MAX
#else
#define MAINFUNCTION_NODE_MAX 4
#endif

/** @brief callback function. */
typedef void (*module_callback_t)(void *userdata);

/** @brief MainFunction module configurations struct. */
typedef struct module_mainfunction {
    bool valid; /**< This object is valid or not. */
    uint32_t
        period_ms; /**< The period(ms) of calling module's callback function. */
    uint32_t count;             /**< Current count of ms. */
    module_callback_t callback; /**< The callback function of user's module. */
    void *userdata;             /**< User's param of callback function. */
} module_mainfunction_t;

/** @brief MainFunction return status. */
typedef enum mainfunction_ret_status {
    STATUS_MAINFUNCTION_SUCCESS, /**< Success state.*/
    STATUS_MAINFUNCTION_NOSPACE, /**< Attached node is up to max. No space can
                                    be used by new module. */
    STATUS_MAINFUNCTION_UNATTACHED, /**< The module is not attached before. */
    STATUS_MAINFUNCTION_FAIL,       /**<Common fail state.*/
} mainfunction_ret_status_t;

/**
 * @brief MainFunction timer enable and start.
 *
 * This function need be implemented by user, icluding :
 * 1.Baremetal:
 *  (1)Initialize timer,
 *  (2)Set Tick_Handler as timer's callback function,
 *  (3)Configure timer and start it.
 * 2.Rtos
 *  (1)Create a task to call mainfunction_handler;
 *
 * @param[in] tick_handler Tick handler ptr.
 *
 */
void board_mainfunction_enable(void (*tick_handler)(void));

/**
 * @brief MainFunction handler.
 *
 * This function handle all nodes and call module's callback function.
 *
 * This function is used by :
 * 1. Timer handler,
 * 2. RTOS Task.
 *
 */
void mainfunction_handler(void);

/**
 * @brief Attach main function feature.
 *
 * This function is used by module which need handle interrupt without VIC.
 *
 * @param[out] node Module mainfunction node ptr;
 * @param[in]  period_ms The period(ms) of calling module handler;
 * @param[in]  callback User callback function.
 * @param[in]  userdata The user's param of callback function.
 *
 * @return The result of this function.
 */
mainfunction_ret_status_t mainfunction_attach(module_mainfunction_t **node,
                                              uint32_t period_ms,
                                              module_callback_t callback,
                                              void *userdata);

/**
 * @brief Detach main function feature.
 *
 * This function is used by module to detach the feature of main function.
 *
 * @param[in] node The mainfunction node ptr of module.
 *
 * @return The result of this function.
 */
mainfunction_ret_status_t mainfunction_detach(module_mainfunction_t *node);

#endif
