/*
 * debug.c
 *
 * Copyright (c) 2023 Semidrive Semiconductor.
 * All rights reserved.
 *
 * Description: debug interface.
 *
 * Revision History:
 * -----------------
 */
#include <types.h>
#include <stdio.h>
#include <compiler.h>
#include <param.h>
#include <ctype.h>
#include <debug.h>

#if CONFIG_DEBUG

/**
 * @brief hex dump function
 *
 * @param[in] ptr: dump data address
 * @param[in] len: dump data length
 */
void hexdump(const void *ptr, size_t len)
{
    addr_t address = (addr_t)ptr;
    size_t count;

    for (count = 0 ; count < len; count += 16) {
        union {
            uint32_t buf[4];
            uint8_t  cbuf[16];
        } u;
        size_t s = ROUNDUP(MIN(len - count, 16), 4);
        size_t i;

        ssdk_printf(SSDK_EMERG, "0x%08x: ", address);
        for (i = 0; i < s / 4; i++) {
            u.buf[i] = ((const uint32_t *)address)[i];
            ssdk_printf(SSDK_EMERG, "%08x ", u.buf[i]);
        }
        for (; i < 4; i++) {
            ssdk_printf(SSDK_EMERG, "         ");
        }
        ssdk_printf(SSDK_EMERG, "|");

        for (i=0; i < 16; i++) {
            unsigned char c = u.cbuf[i];
            if (i < s && isprint(c)) {
                ssdk_printf(SSDK_EMERG, "%c", c);
            } else {
                ssdk_printf(SSDK_EMERG, ".");
            }
        }
        ssdk_printf(SSDK_EMERG, "|\r\n");
        address += 16;
    }
}

/**
 * @brief hex dump8 function with display address
 *
 * @param[in] ptr: ptr dump data address
 * @param[in] len: len dump data length
 * @param[in] addr: disp_addr display address
 */
void hexdump8_ex(const void *ptr, size_t len, uint64_t addr)
{
    addr_t address = (addr_t)ptr;
    size_t count;
    size_t i;
    const char *addr_fmt = ((addr + len) > 0xFFFFFFFF)
                           ? "0x%016llx: "
                           : "0x%08llx: ";

    for (count = 0 ; count < len; count += 16) {
        ssdk_printf(SSDK_EMERG, addr_fmt, addr + count);

        for (i=0; i < MIN(len - count, 16); i++) {
            ssdk_printf(SSDK_EMERG, "%02hhx ", *(const uint8_t *)(address + i));
        }

        for (; i < 16; i++) {
            ssdk_printf(SSDK_EMERG, "   ");
        }

        ssdk_printf(SSDK_EMERG, "|");

        for (i=0; i < MIN(len - count, 16); i++) {
            unsigned char c = ((const char *)address)[i];
            ssdk_printf(SSDK_EMERG, "%c", isprint(c) ? c : '.');
        }

        ssdk_printf(SSDK_EMERG, "\r\n");
        address += 16;
    }
}

#endif