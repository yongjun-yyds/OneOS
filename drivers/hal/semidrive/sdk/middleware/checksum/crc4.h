/********************************************************
 *        Copyright(c) 2022    Semidrive                *
 *        All rights reserved.                          *
 ********************************************************/
#ifndef _CRC4_H
#define _CRC4_H

unsigned char crc4_calculate(unsigned char crc, const unsigned char *message,
                             unsigned char len);

#endif
