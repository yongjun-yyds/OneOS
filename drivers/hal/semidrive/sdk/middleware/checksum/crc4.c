/********************************************************
 *        Copyright(c) 2022    Semidrive                *
 *        All rights reserved.                          *
 ********************************************************/
#include "crc4.h"

const unsigned char CRC4_Table[16] = {0, 13, 7, 10, 14, 3, 9, 4,
                                      1, 12, 6, 11, 15, 2, 8, 5};

unsigned char crc4_calculate(unsigned char initial_value,
                             const unsigned char *message, unsigned char len)
{
    unsigned char crc_val = initial_value;
    int i = 0;
    for (; i < len; i++) {
        crc_val = CRC4_Table[crc_val] ^ message[i];
    }
    crc_val = 0 ^ CRC4_Table[crc_val];
    return crc_val;
}
