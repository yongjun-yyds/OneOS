/**
 * @file
 *
 * lwIP Options Configuration
 */

/*
 * Copyright (c) 2001-2004 Swedish Institute of Computer Science.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * This file is part of the lwIP TCP/IP stack.
 *
 * Author: Adam Dunkels <adam@sics.se>
 *
 */
#ifndef LWIP_LWIPOPTS_H
#define LWIP_LWIPOPTS_H

#define NO_SYS 1

#define LWIP_NETCONN                    0
#define LWIP_SOCKET                     0

/*
 * Include user defined options first. Anything not defined in these files
 * will be set to standard values. Override anything you don't like!
 */

// use libc malloc
#define MEM_LIBC_MALLOC 0
#define MEMP_USE_CUSTOM_POOLS 1
#define MEM_USE_POOLS 1
// Create static memory pools
#define MEMP_MEM_MALLOC 0
/* Memory block size alignes to 4 bytes, because ARM may access memory block
 * by using ldrd/strd instruction which needs 4 bytes alignment
 */
#define MEM_ALIGNMENT   4

// these don't actually affect anything
// unless MEMP_MEM_MALLOC is 0
#define MEM_SIZE (16 * 1024)
#define MEMP_NUM_UDP_PCB 128
#define MEMP_NUM_TCP_PCB 128
#define MEMP_NUM_TCP_PCB_LISTEN 128
#define MEMP_NUM_NETBUF 32
#define MEMP_NUM_NETCONN 32
#define MEMP_NUM_NETDB 32

#define LWIP_COMPAT_SOCKETS 1
#define LWIP_SO_RCVTIMEO    1
#define LWIP_SO_RCVBUF      1

#define LWIP_DHCP 0
#define LWIP_AUTOIP 0
#define LWIP_DHCP_AUTOIP_COOP 0

#define LWIP_DNS 0

#define LWIP_NETIF_HOSTNAME 1
#define LWIP_NETIF_STATUS_CALLBACK 1
#define LWIP_NETIF_HWADDRHINT 1
#define LWIP_NETIF_LOOPBACK 1

#define LWIP_HAVE_LOOPIF 1

#define TCPIP_THREAD_STACKSIZE 4096
#define TCPIP_THREAD_PRIO osPriorityRealtime

#define TCPIP_MBOX_SIZE 16

#define DEFAULT_THREAD_PRIO (osPriorityNormal - 1)
#define DEFAULT_THREAD_STACKSIZE 4096

#define DEFAULT_RAW_RECVMBOX_SIZE 16
#define DEFAULT_UDP_RECVMBOX_SIZE 16
#define DEFAULT_TCP_RECVMBOX_SIZE 16
#define DEFAULT_ACCEPTMBOX_SIZE 16

#define LWIP_STATS_DISPLAY 1
#define LWIP_ERRNO_STDINCLUDE
#define SYS_LIGHTWEIGHT_PROT 0
#define LWIP_DEBUG 1

#define LWIP_RAW 1

#define MEMP_NUM_SYS_TIMEOUT 17

//#define PBUF_DEBUG  LWIP_DBG_ON
//#define TCP_OUTPUT_DEBUG    LWIP_DBG_ON
//#define LWIP_DBG_MIN_LEVEL 0

/* TCP Maximum segment size. */
#define TCP_MSS                 1460

#define TCP_SND_BUF             (12 * TCP_MSS)
#define TCP_SND_QUEUELEN        80
#define MEMP_NUM_TCP_SEG        TCP_SND_QUEUELEN

#endif /* LWIP_LWIPOPTS_H */
