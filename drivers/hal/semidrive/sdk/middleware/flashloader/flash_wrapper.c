/**
 * @file flash_wrapper.c
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */

#include <armv7-r/arm.h>
#include <armv7-r/cache.h>
#include <armv7-r/pmu.h>
#include <armv7-r/register.h>
#include <clock_cfg.h>
#include <clock_ip.h>
#include <config.h>
#include <debug.h>
#include <flash_wrapper.h>
#include <param.h>
#include <part.h>
#include <pinmux_cfg.h>
#include <regs_base.h>
#include <reset_cfg.h>
#include <reset_ip.h>
#include <scr_cfg.h>
#include <scr_hw.h>
#include <sdrv_fuse.h>
#include <sdrv_pinctrl.h>
#include <sdrv_rstgen.h>
#include <sdrv_scr.h>
#include <sdrv_spi_nor.h>
#include <sdrv_xspi.h>
#include <stdlib.h>
#include <string.h>
#include <types.h>
#include <udelay/udelay.h>

#include "board.h"

#if CONFIG_HYPERBUS_MODE
#define FLASH_TYPE "hyperflash"
#else
#define FLASH_TYPE "norflash"
#endif

#define VERSION_NUMBER "V0.1.1"

#if (defined PART_ID) && (defined FLASH_TYPE) && (defined VERSION_NUMBER)

__USED char version_info[] =
    "flashloader_version:" PART_ID " " FLASH_TYPE " " VERSION_NUMBER;

#endif

#define HYPER_FLASH_MODE_FUSE_INDEX (181)
#define HYPER_FLASH_MODE_FUSE_VAL (0x00100000)

static struct spi_nor_host spi_nor_host = {0};
static struct xspi_pdata xspi = {0};
static struct spi_nor flash = {0};
static struct flash_info *flash_info;
static flash_wrapper_t flash_wrapper_entry;
static uint8_t *g_rb_buffer = (uint8_t *)IRAM1_BASE;
volatile uint8_t read_back_buffer_flag = 0;

static struct xspi_config host_config = {
    .id = 0,
    .apb_base = APB_XSPI1PORTA_BASE,
    .direct_base = XSPI1_XSPI1PORTA_BASE,
};

static struct spi_nor_config config = {
    .id = 0,
    .cs = 0,
    .baudrate = 20000000,
    .xfer_mode = SPI_NOR_XFER_POLLING_MODE,
    .dev_mode = SPI_NOR_DEV_SINGLE_MODE,
    .sw_rst = false, /* config soft reset spi norflash */
    .async_mode = false,
#ifdef CONFIG_HYPERBUS_MODE
    .hyperbus_mode = CONFIG_HYPERBUS_MODE,
#else
    .hyperbus_mode = false,
#endif
};

void xspi_lockstep_enable(bool flag)
{
#if (CONFIG_E3 || CONFIG_D3)
    scr_signal_t signal = SCR_SF_XSPI1_SRC_CFG_LOCKSTEP_MODE_N;
#else
    scr_signal_t signal = SCR_SF_XSPI1_LOCKSTEP_DISABLE;
#endif

    sdrv_rstgen_assert(&rstsig_xspi1b);
    sdrv_rstgen_assert(&rstsig_xspi1a);

    udelay(10u);
    scr_set(&g_scr_ctrl, &signal, !flag);
    udelay(10u);

    sdrv_rstgen_deassert(&rstsig_xspi1a);
    sdrv_rstgen_deassert(&rstsig_xspi1b);
}

void xspi_parallel_enable(bool flag)
{
    scr_signal_t signal = SCR_SF_XSPI1_SRC_CFG_PARALLEL_MODE;

    sdrv_rstgen_assert(&rstsig_xspi1b);
    sdrv_rstgen_assert(&rstsig_xspi1a);

    udelay(10u);
    scr_set(&g_scr_ctrl, &signal, flag);
    udelay(10u);

    sdrv_rstgen_deassert(&rstsig_xspi1a);
    sdrv_rstgen_deassert(&rstsig_xspi1b);
}

static void arch_disabled_alignment_fault_checking(void)
{
    uint32_t sctlr;

    sctlr = arm_read_sctlr();

    sctlr &= ~(SCTLR_A);

    arm_write_sctlr(sctlr);
}

void board_init(void)
{
    /* disable i&d cache */
    arch_disable_cache(UCACHE);

    /* disable alignment_fault_checking */
    arch_disabled_alignment_fault_checking();

    /* Stop and clear the cycle counter, failure to do so may result in slower
     * downloads */
    pmu_stop_clear_cycle_cntr();

    /* reset module */
    board_reset_init();

    /* initializes clock source */
    sdrv_ckgen_init(&g_clock_config);

    /* config spinor pinmux */
    sdrv_pinctrl_init(NUM_OF_CONFIGURED_PINS, g_pin_init_config);

    /* board debug console uart init */
    board_debug_console_init();
}

void board_norflash_init(void)
{
    bool locksetp_mode = false;
    bool parallel_mode = false;
    /* get xspi1a clk node */
    sdrv_ckgen_node_t *clk = CLK_NODE(g_ckgen_ip_xspi1a);

    /* config dev spi nor dev single mode */
    switch (config.dev_mode) {
    case SPI_NOR_DEV_LOCKSTEP_MODE:
        locksetp_mode = true;
        break;

    case SPI_NOR_DEV_PARALLEL_MODE:
        parallel_mode = true;
        break;

    default:
        break;
    }

    /* close locksetp/parallel mode */
    xspi_lockstep_enable(locksetp_mode);
    xspi_parallel_enable(parallel_mode);

    /* initializes xspi host */
    host_config.ref_clk = sdrv_ckgen_get_rate(clk);
    sdrv_xspi_host_init(&spi_nor_host, &xspi, &host_config);
    spi_nor_host.clk = clk;
    sdrv_ckgen_set_rate(clk, config.baudrate);
    ssdk_printf(SSDK_DEBUG, "xspi clock rate is %u!\r\n",
                sdrv_ckgen_get_rate(clk));

    /* initializes spi nor device */
    if (sdrv_spi_nor_init(&flash, &spi_nor_host, &config)) {
        ssdk_printf(SSDK_ERR, "spinor init failed!\r\n");
        return;
    }

    /* get flash info data information */
    flash_info = sdrv_spi_nor_get_info(&flash);

    ssdk_printf(SSDK_DEBUG,
                "spinor flash size = 0x%llx, sector_size = 0x%x \r\n",
                flash_info->size, flash_info->sector_size);
    return;
}

void flashloader_init(struct spi_nor **flashloader) { *flashloader = &flash; }

flash_wrapper_t *flash_wrapper_init(struct spi_nor *device)
{
    struct flash_info *flash_info;

    flash_info = sdrv_spi_nor_get_info(device);
    if (flash_info == NULL) {
        ssdk_printf(SSDK_ERR, "Get flash info fail\r\n");
        return NULL;
    }

    flash_wrapper_entry.device = device;
    flash_wrapper_entry.sector_size = flash_info->sector_size;
    flash_wrapper_entry.page_size = flash_info->page_size;
    flash_wrapper_entry.flash_size = flash_info->size;

    return &flash_wrapper_entry;
}

int flash_wrapper_read(flash_wrapper_t *flash_wrapper, flash_addr_t addr,
                       uint8_t *buf, flash_size_t size)
{
    if (!IS_ALIGNED(size, 4)) {
        ssdk_printf(SSDK_ERR, "Read address unaligned:0x%x\r\n", addr);
        return FL_DL_READ_LENGTH_UNALIGNED_ERROR;
    }

    if (sdrv_spi_nor_read(flash_wrapper->device, addr, buf, size) == 0)
        return 0;
    else
        return FL_DL_READ_ERROR;
}

int flash_wrapper_write(flash_wrapper_t *flash_wrapper, flash_addr_t addr,
                        const uint8_t *buf, flash_size_t size)
{
    if (!IS_ALIGNED(addr, 2)) {
        ssdk_printf(SSDK_ERR, "Write address unaligned:0x%x\r\n", addr);
        return FL_DL_PROGRAM_ADDRESS_UNALIGNED_ERROR;
    }
    if (sdrv_spi_nor_write(flash_wrapper->device, addr, buf, size) == 0)
        return 0;
    else
        return FL_DL_PROGRAM_ERROR;
}

int flash_wrapper_erase(flash_wrapper_t *flash_wrapper, flash_addr_t addr,
                        flash_size_t size)
{
    uint32_t sector_size = flash_wrapper->sector_size;
    uint32_t erase_addr;
    uint32_t erase_len;

    if (read_back_buffer_flag == 1) {
        if (CONFIG_READ_BACK_BUFFER_SIZE < sector_size) {
            ssdk_printf(SSDK_ERR, "Config read back buffer < sector size.\r\n");
            return -1;
        }

        while (size) {
            if (!IS_ALIGNED(addr, sector_size)) {
                erase_addr = ROUNDDOWN(addr, sector_size);
                erase_len = MIN(ROUNDUP(addr, sector_size) - addr, size);
            } else {
                erase_addr = addr;
                erase_len =
                    size > sector_size ? (size - size % sector_size) : size;
            }

            if (erase_len < sector_size) {
                if (sdrv_spi_nor_read(flash_wrapper->device, erase_addr,
                                      g_rb_buffer, sector_size)) {
                    ssdk_printf(SSDK_ERR,
                                "sdrv_spi_nor_read error, addr = 0x%08x length "
                                "= 0x%08x \r\n",
                                erase_addr, erase_len);
                    return -1;
                }

                if (sdrv_spi_nor_erase(flash_wrapper->device, erase_addr,
                                       sector_size)) {
                    ssdk_printf(SSDK_ERR,
                                "sdrv_spi_nor_erase error, addr = 0x%08x "
                                "length = 0x%08x \r\n",
                                erase_addr, sector_size);
                    return -1;
                }

                memset(g_rb_buffer + addr % sector_size, 0xff, erase_len);

                if (sdrv_spi_nor_write(flash_wrapper->device, erase_addr,
                                       g_rb_buffer, sector_size)) {
                    ssdk_printf(SSDK_ERR,
                                "sdrv_spi_nor_erase error, addr = 0x%08x "
                                "length = 0x%08x \r\n",
                                erase_addr, sector_size);
                    return -1;
                }
            } else {
                if (sdrv_spi_nor_erase(flash_wrapper->device, erase_addr,
                                       erase_len)) {
                    return -1;
                }
            }

            addr += erase_len;
            size -= erase_len;
        }
        return 0;
    } else {
        if (!IS_ALIGNED(addr, sector_size) || !IS_ALIGNED(size, sector_size)) {
            ssdk_printf(SSDK_ERR, "Erase address unaligned:0x%x\r\n", addr);
            return FL_DL_ERASE_ADDRESS_UNALIGNED_ERROR;
        } else {
            if (sdrv_spi_nor_erase(flash_wrapper->device, addr, size) == 0)
                return 0;
            else
                return FL_DL_ERASE_ERROR;
        }
    }
}

int32_t fuse_write(uint32_t index, uint32_t value)
{
    int32_t err = 0;
    uint32_t read_value = 0;
    if (0 != sdrv_fuse_sense(index, &read_value)) {
        ssdk_printf(SSDK_ERR, "Failed to read fuse %d\n", index);
        err = FL_FUSE_READ_ERROR;
        goto end;
    }

    if ((read_value & value) != value) {
        read_value |= value;
        ssdk_printf(SSDK_ERR, "write fuse %d val 0x%08x\n", index, read_value);

        if (0 != sdrv_fuse_program(index, read_value)) {
            ssdk_printf(SSDK_ERR, "Failed to write fuse %d val 0x%08x\n", index,
                        read_value);
            err = FL_FUSE_WRITE_ERROR;
            goto end;
        }

        read_value = 0;

        if (0 != sdrv_fuse_sense(index, &read_value)) {
            ssdk_printf(SSDK_ERR, "Failed to read fuse %d\n", index);
            err = FL_FUSE_READ_ERROR;
            goto end;
        }

        ssdk_printf(SSDK_DEBUG, "read back fuse %d val 0x%08x\n", index,
                    read_value);

        if ((read_value & value) != value) {
            ssdk_printf(SSDK_ERR, "read back error, fuse %d val 0x%08x\n",
                        index, read_value);
            err = FL_FUSE_READBACK_VERIFY_ERROR;
            goto end;
        }
    } else {
        ssdk_printf(SSDK_DEBUG, "no need to fuse\n");
    }

end:
    return err;
}

int32_t fuse_read(uint32_t index, uint32_t *value)
{
    int32_t ret = 0;
    ret = sdrv_fuse_sense(index, value);
    if (ret != 0) {
        ssdk_printf(SSDK_ERR, "Failed to read fuse %d\n", index);
        return FL_FUSE_READ_ERROR;
    }
    return 0;
}

static uint32_t crc32_table[256] = {
    0x00000000UL, 0x77073096UL, 0xee0e612cUL, 0x990951baUL, 0x076dc419UL,
    0x706af48fUL, 0xe963a535UL, 0x9e6495a3UL, 0x0edb8832UL, 0x79dcb8a4UL,
    0xe0d5e91eUL, 0x97d2d988UL, 0x09b64c2bUL, 0x7eb17cbdUL, 0xe7b82d07UL,
    0x90bf1d91UL, 0x1db71064UL, 0x6ab020f2UL, 0xf3b97148UL, 0x84be41deUL,
    0x1adad47dUL, 0x6ddde4ebUL, 0xf4d4b551UL, 0x83d385c7UL, 0x136c9856UL,
    0x646ba8c0UL, 0xfd62f97aUL, 0x8a65c9ecUL, 0x14015c4fUL, 0x63066cd9UL,
    0xfa0f3d63UL, 0x8d080df5UL, 0x3b6e20c8UL, 0x4c69105eUL, 0xd56041e4UL,
    0xa2677172UL, 0x3c03e4d1UL, 0x4b04d447UL, 0xd20d85fdUL, 0xa50ab56bUL,
    0x35b5a8faUL, 0x42b2986cUL, 0xdbbbc9d6UL, 0xacbcf940UL, 0x32d86ce3UL,
    0x45df5c75UL, 0xdcd60dcfUL, 0xabd13d59UL, 0x26d930acUL, 0x51de003aUL,
    0xc8d75180UL, 0xbfd06116UL, 0x21b4f4b5UL, 0x56b3c423UL, 0xcfba9599UL,
    0xb8bda50fUL, 0x2802b89eUL, 0x5f058808UL, 0xc60cd9b2UL, 0xb10be924UL,
    0x2f6f7c87UL, 0x58684c11UL, 0xc1611dabUL, 0xb6662d3dUL, 0x76dc4190UL,
    0x01db7106UL, 0x98d220bcUL, 0xefd5102aUL, 0x71b18589UL, 0x06b6b51fUL,
    0x9fbfe4a5UL, 0xe8b8d433UL, 0x7807c9a2UL, 0x0f00f934UL, 0x9609a88eUL,
    0xe10e9818UL, 0x7f6a0dbbUL, 0x086d3d2dUL, 0x91646c97UL, 0xe6635c01UL,
    0x6b6b51f4UL, 0x1c6c6162UL, 0x856530d8UL, 0xf262004eUL, 0x6c0695edUL,
    0x1b01a57bUL, 0x8208f4c1UL, 0xf50fc457UL, 0x65b0d9c6UL, 0x12b7e950UL,
    0x8bbeb8eaUL, 0xfcb9887cUL, 0x62dd1ddfUL, 0x15da2d49UL, 0x8cd37cf3UL,
    0xfbd44c65UL, 0x4db26158UL, 0x3ab551ceUL, 0xa3bc0074UL, 0xd4bb30e2UL,
    0x4adfa541UL, 0x3dd895d7UL, 0xa4d1c46dUL, 0xd3d6f4fbUL, 0x4369e96aUL,
    0x346ed9fcUL, 0xad678846UL, 0xda60b8d0UL, 0x44042d73UL, 0x33031de5UL,
    0xaa0a4c5fUL, 0xdd0d7cc9UL, 0x5005713cUL, 0x270241aaUL, 0xbe0b1010UL,
    0xc90c2086UL, 0x5768b525UL, 0x206f85b3UL, 0xb966d409UL, 0xce61e49fUL,
    0x5edef90eUL, 0x29d9c998UL, 0xb0d09822UL, 0xc7d7a8b4UL, 0x59b33d17UL,
    0x2eb40d81UL, 0xb7bd5c3bUL, 0xc0ba6cadUL, 0xedb88320UL, 0x9abfb3b6UL,
    0x03b6e20cUL, 0x74b1d29aUL, 0xead54739UL, 0x9dd277afUL, 0x04db2615UL,
    0x73dc1683UL, 0xe3630b12UL, 0x94643b84UL, 0x0d6d6a3eUL, 0x7a6a5aa8UL,
    0xe40ecf0bUL, 0x9309ff9dUL, 0x0a00ae27UL, 0x7d079eb1UL, 0xf00f9344UL,
    0x8708a3d2UL, 0x1e01f268UL, 0x6906c2feUL, 0xf762575dUL, 0x806567cbUL,
    0x196c3671UL, 0x6e6b06e7UL, 0xfed41b76UL, 0x89d32be0UL, 0x10da7a5aUL,
    0x67dd4accUL, 0xf9b9df6fUL, 0x8ebeeff9UL, 0x17b7be43UL, 0x60b08ed5UL,
    0xd6d6a3e8UL, 0xa1d1937eUL, 0x38d8c2c4UL, 0x4fdff252UL, 0xd1bb67f1UL,
    0xa6bc5767UL, 0x3fb506ddUL, 0x48b2364bUL, 0xd80d2bdaUL, 0xaf0a1b4cUL,
    0x36034af6UL, 0x41047a60UL, 0xdf60efc3UL, 0xa867df55UL, 0x316e8eefUL,
    0x4669be79UL, 0xcb61b38cUL, 0xbc66831aUL, 0x256fd2a0UL, 0x5268e236UL,
    0xcc0c7795UL, 0xbb0b4703UL, 0x220216b9UL, 0x5505262fUL, 0xc5ba3bbeUL,
    0xb2bd0b28UL, 0x2bb45a92UL, 0x5cb36a04UL, 0xc2d7ffa7UL, 0xb5d0cf31UL,
    0x2cd99e8bUL, 0x5bdeae1dUL, 0x9b64c2b0UL, 0xec63f226UL, 0x756aa39cUL,
    0x026d930aUL, 0x9c0906a9UL, 0xeb0e363fUL, 0x72076785UL, 0x05005713UL,
    0x95bf4a82UL, 0xe2b87a14UL, 0x7bb12baeUL, 0x0cb61b38UL, 0x92d28e9bUL,
    0xe5d5be0dUL, 0x7cdcefb7UL, 0x0bdbdf21UL, 0x86d3d2d4UL, 0xf1d4e242UL,
    0x68ddb3f8UL, 0x1fda836eUL, 0x81be16cdUL, 0xf6b9265bUL, 0x6fb077e1UL,
    0x18b74777UL, 0x88085ae6UL, 0xff0f6a70UL, 0x66063bcaUL, 0x11010b5cUL,
    0x8f659effUL, 0xf862ae69UL, 0x616bffd3UL, 0x166ccf45UL, 0xa00ae278UL,
    0xd70dd2eeUL, 0x4e048354UL, 0x3903b3c2UL, 0xa7672661UL, 0xd06016f7UL,
    0x4969474dUL, 0x3e6e77dbUL, 0xaed16a4aUL, 0xd9d65adcUL, 0x40df0b66UL,
    0x37d83bf0UL, 0xa9bcae53UL, 0xdebb9ec5UL, 0x47b2cf7fUL, 0x30b5ffe9UL,
    0xbdbdf21cUL, 0xcabac28aUL, 0x53b39330UL, 0x24b4a3a6UL, 0xbad03605UL,
    0xcdd70693UL, 0x54de5729UL, 0x23d967bfUL, 0xb3667a2eUL, 0xc4614ab8UL,
    0x5d681b02UL, 0x2a6f2b94UL, 0xb40bbe37UL, 0xc30c8ea1UL, 0x5a05df1bUL,
    0x2d02ef8dUL};

uint32_t flashloader_crc32(uint8_t *buf, int32_t len, uint32_t crc)
{
    while (len--) {
        crc = crc32_table[((int)crc ^ (*buf++)) & 0xff] ^ (crc >> 8);
    }
    return crc;
}

#if (CONFIG_HYPERBUS_MODE == 1) &&                                             \
    ((CONFIG_IAR_HYPERFLASH_FUSE == 1) ||                                      \
     (CONFIG_SDFACTORYTOOL_HYPERFLASH_FUSE == 1))

int32_t program_hyperflash_fuse(void)
{
    return fuse_write(HYPER_FLASH_MODE_FUSE_INDEX, HYPER_FLASH_MODE_FUSE_VAL);
}

#endif
