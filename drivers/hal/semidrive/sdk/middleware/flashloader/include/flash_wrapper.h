/**
 * @file flash_wrapper.h
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef _FLASH_WRAPPER_H_
#define _FLASH_WRAPPER_H_

#include <sdrv_spi_nor.h>

#define FLASH_ADDR_MASK(base) ((base)&0xFFFFFFF)

enum FL_STATUS {
    FL_OK = 0,
    FL_DL_INIT_ERROR = -1,
    FL_DL_ERASE_ADDRESS_UNALIGNED_ERROR = -2,
    FL_DL_ERASE_ERROR = -3,
    FL_DL_PROGRAM_ADDRESS_UNALIGNED_ERROR = -4,
    FL_DL_PROGRAM_ERROR = -5,
    FL_DL_READ_LENGTH_UNALIGNED_ERROR = -6,
    FL_DL_READ_ERROR = -7,
    FL_DL_VERIFY_ERROR = -8,
    FL_FUSE_CMD_ERROR = -9,
    FL_FUSE_READ_ERROR = -10,
    FL_FUSE_WRITE_ERROR = -11,
    FL_FUSE_READBACK_VERIFY_ERROR = -12
};

typedef struct flash_wrapper {
    struct spi_nor *device;
    uint32_t sector_size;
    uint32_t page_size;
    flash_size_t flash_size;
} flash_wrapper_t;

extern volatile uint8_t read_back_buffer_flag;

void flashloader_init(struct spi_nor **flashloader);
uint32_t flashloader_crc32(uint8_t *buf, int32_t len, uint32_t crc);

flash_wrapper_t *flash_wrapper_init(struct spi_nor *device);
int flash_wrapper_read(flash_wrapper_t *flash_wrapper, flash_addr_t addr,
                       uint8_t *buf, flash_size_t size);
int flash_wrapper_write(flash_wrapper_t *flash_wrapper, flash_addr_t addr,
                        const uint8_t *buf, flash_size_t size);
int flash_wrapper_erase(flash_wrapper_t *flash_wrapper, flash_addr_t addr,
                        flash_size_t size);

int32_t fuse_write(uint32_t index, uint32_t value);
int32_t fuse_read(uint32_t index, uint32_t *value);

void board_init(void);
void board_norflash_init(void);

#if (CONFIG_HYPERBUS_MODE == 1) &&                                             \
    ((CONFIG_IAR_HYPERFLASH_FUSE == 1) ||                                      \
     (CONFIG_SDFACTORYTOOL_HYPERFLASH_FUSE == 1))
int32_t program_hyperflash_fuse(void);
#endif

#endif
