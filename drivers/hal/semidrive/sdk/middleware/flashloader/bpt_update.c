/**
 * @file bpt_update.c
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */

#include <board.h>
#include <bpt_v2.h>
#include <config.h>
#include <ctype.h>
#include <debug.h>
#include <flash_wrapper.h>
#include <param.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

sfs_t *sfs = NULL;
static uint8_t sfs_buffer[SFS_SIZE] __ALIGNED(CONFIG_ARCH_CACHE_LINE);
uint8_t bpt_buffer[BPT_HEDAER_LEN] __ALIGNED(CONFIG_ARCH_CACHE_LINE);
bpt_iib_t iib_info_temp __ALIGNED(CONFIG_ARCH_CACHE_LINE) = {0};

/** CRC table for the CRC-32. The poly isPoly : 0x04C11DB7 */
const unsigned int crc32_table_sfs[256] = {
    0x00000000, 0x04C11DB7, 0x09823B6E, 0x0D4326D9, 0x130476DC, 0x17C56B6B,
    0x1A864DB2, 0x1E475005, 0x2608EDB8, 0x22C9F00F, 0x2F8AD6D6, 0x2B4BCB61,
    0x350C9B64, 0x31CD86D3, 0x3C8EA00A, 0x384FBDBD, 0x4C11DB70, 0x48D0C6C7,
    0x4593E01E, 0x4152FDA9, 0x5F15ADAC, 0x5BD4B01B, 0x569796C2, 0x52568B75,
    0x6A1936C8, 0x6ED82B7F, 0x639B0DA6, 0x675A1011, 0x791D4014, 0x7DDC5DA3,
    0x709F7B7A, 0x745E66CD, 0x9823B6E0, 0x9CE2AB57, 0x91A18D8E, 0x95609039,
    0x8B27C03C, 0x8FE6DD8B, 0x82A5FB52, 0x8664E6E5, 0xBE2B5B58, 0xBAEA46EF,
    0xB7A96036, 0xB3687D81, 0xAD2F2D84, 0xA9EE3033, 0xA4AD16EA, 0xA06C0B5D,
    0xD4326D90, 0xD0F37027, 0xDDB056FE, 0xD9714B49, 0xC7361B4C, 0xC3F706FB,
    0xCEB42022, 0xCA753D95, 0xF23A8028, 0xF6FB9D9F, 0xFBB8BB46, 0xFF79A6F1,
    0xE13EF6F4, 0xE5FFEB43, 0xE8BCCD9A, 0xEC7DD02D, 0x34867077, 0x30476DC0,
    0x3D044B19, 0x39C556AE, 0x278206AB, 0x23431B1C, 0x2E003DC5, 0x2AC12072,
    0x128E9DCF, 0x164F8078, 0x1B0CA6A1, 0x1FCDBB16, 0x018AEB13, 0x054BF6A4,
    0x0808D07D, 0x0CC9CDCA, 0x7897AB07, 0x7C56B6B0, 0x71159069, 0x75D48DDE,
    0x6B93DDDB, 0x6F52C06C, 0x6211E6B5, 0x66D0FB02, 0x5E9F46BF, 0x5A5E5B08,
    0x571D7DD1, 0x53DC6066, 0x4D9B3063, 0x495A2DD4, 0x44190B0D, 0x40D816BA,
    0xACA5C697, 0xA864DB20, 0xA527FDF9, 0xA1E6E04E, 0xBFA1B04B, 0xBB60ADFC,
    0xB6238B25, 0xB2E29692, 0x8AAD2B2F, 0x8E6C3698, 0x832F1041, 0x87EE0DF6,
    0x99A95DF3, 0x9D684044, 0x902B669D, 0x94EA7B2A, 0xE0B41DE7, 0xE4750050,
    0xE9362689, 0xEDF73B3E, 0xF3B06B3B, 0xF771768C, 0xFA325055, 0xFEF34DE2,
    0xC6BCF05F, 0xC27DEDE8, 0xCF3ECB31, 0xCBFFD686, 0xD5B88683, 0xD1799B34,
    0xDC3ABDED, 0xD8FBA05A, 0x690CE0EE, 0x6DCDFD59, 0x608EDB80, 0x644FC637,
    0x7A089632, 0x7EC98B85, 0x738AAD5C, 0x774BB0EB, 0x4F040D56, 0x4BC510E1,
    0x46863638, 0x42472B8F, 0x5C007B8A, 0x58C1663D, 0x558240E4, 0x51435D53,
    0x251D3B9E, 0x21DC2629, 0x2C9F00F0, 0x285E1D47, 0x36194D42, 0x32D850F5,
    0x3F9B762C, 0x3B5A6B9B, 0x0315D626, 0x07D4CB91, 0x0A97ED48, 0x0E56F0FF,
    0x1011A0FA, 0x14D0BD4D, 0x19939B94, 0x1D528623, 0xF12F560E, 0xF5EE4BB9,
    0xF8AD6D60, 0xFC6C70D7, 0xE22B20D2, 0xE6EA3D65, 0xEBA91BBC, 0xEF68060B,
    0xD727BBB6, 0xD3E6A601, 0xDEA580D8, 0xDA649D6F, 0xC423CD6A, 0xC0E2D0DD,
    0xCDA1F604, 0xC960EBB3, 0xBD3E8D7E, 0xB9FF90C9, 0xB4BCB610, 0xB07DABA7,
    0xAE3AFBA2, 0xAAFBE615, 0xA7B8C0CC, 0xA379DD7B, 0x9B3660C6, 0x9FF77D71,
    0x92B45BA8, 0x9675461F, 0x8832161A, 0x8CF30BAD, 0x81B02D74, 0x857130C3,
    0x5D8A9099, 0x594B8D2E, 0x5408ABF7, 0x50C9B640, 0x4E8EE645, 0x4A4FFBF2,
    0x470CDD2B, 0x43CDC09C, 0x7B827D21, 0x7F436096, 0x7200464F, 0x76C15BF8,
    0x68860BFD, 0x6C47164A, 0x61043093, 0x65C52D24, 0x119B4BE9, 0x155A565E,
    0x18197087, 0x1CD86D30, 0x029F3D35, 0x065E2082, 0x0B1D065B, 0x0FDC1BEC,
    0x3793A651, 0x3352BBE6, 0x3E119D3F, 0x3AD08088, 0x2497D08D, 0x2056CD3A,
    0x2D15EBE3, 0x29D4F654, 0xC5A92679, 0xC1683BCE, 0xCC2B1D17, 0xC8EA00A0,
    0xD6AD50A5, 0xD26C4D12, 0xDF2F6BCB, 0xDBEE767C, 0xE3A1CBC1, 0xE760D676,
    0xEA23F0AF, 0xEEE2ED18, 0xF0A5BD1D, 0xF464A0AA, 0xF9278673, 0xFDE69BC4,
    0x89B8FD09, 0x8D79E0BE, 0x803AC667, 0x84FBDBD0, 0x9ABC8BD5, 0x9E7D9662,
    0x933EB0BB, 0x97FFAD0C, 0xAFB010B1, 0xAB710D06, 0xA6322BDF, 0xA2F33668,
    0xBCB4666D, 0xB8757BDA, 0xB5365D03, 0xB1F740B4};

/* calc crc for sfs and bpt */
unsigned int sfs_crc32(uint32_t crc, uint8_t *buffer, uint32_t len)
{
    crc ^= 0xFFFFFFFFUL;

    while (len--)
        crc = ((crc << 8) & 0xFFFFFF00UL) ^
              crc32_table_sfs[((crc >> 24) & 0xFFUL) ^ (*buffer++)];

    return crc ^ 0xFFFFFFFFUL;
}

/* dump data */
void hexdump(const void *ptr, size_t len)
{
    addr_t address = (addr_t)ptr;
    size_t count;

    for (count = 0; count < len; count += 16) {
        union {
            uint32_t buf[4];
            uint8_t cbuf[16];
        } u;
        size_t s = ROUNDUP(MIN(len - count, 16), 4);
        size_t i;

        printf("0x%08x: ", address);
        for (i = 0; i < s / 4; i++) {
            u.buf[i] = ((const uint32_t *)address)[i];
            printf("%08x ", u.buf[i]);
        }
        for (; i < 4; i++) {
            printf("         ");
        }
        printf("|");

        for (i = 0; i < 16; i++) {
            unsigned char c = u.cbuf[i];
            if (i < s && isprint(c)) {
                printf("%c", c);
            } else {
                printf(".");
            }
        }
        printf("|\r\n");
        address += 16;
    }
}

/* dump data */
static void BptInit(bpt_v2_t *bpt)
{
    bpt->tag = BPT_TAG_VAL;
    bpt->reserved1 = 0;
    bpt->size = BPT_HEDAER_LEN;
    bpt->sec_ver = 0;
    bpt->digest_alg = 3;
    memset(bpt->reserved2, 0, 19);
    bpt->rcp.tag = BPT_RCP_TAG;
    bpt->rcp.size = BPT_RCP_SIZE;
    memset(&bpt->rcp.rot_id, 0, 1040);
    memset(bpt->signature, 0, 512);
    memset(bpt->key_wraps, 0, 512);
    memset(bpt->reserved3, 0, 984);
    bpt->sn = BPT_PAGE_SN;
    bpt->sn_inversion = ~(BPT_PAGE_SN);
    memset(bpt->reserved4, 0, 8);
}

/* add iib data */
static int BptAddIIB(bpt_iib_t *iib_info, uint32_t img_base, uint32_t bpt_base,
                     uint32_t link_base, uint32_t img_size, uint8_t core)
{
    uint32_t img_offset;

    img_offset = FLASH_ADDR_MASK((uint32_t)img_base);

    if (img_offset >= (bpt_base + BPT_HEDAER_LEN)) {
        img_offset -= bpt_base;
    } else {
        ssdk_printf(SSDK_ALERT, "img_base 0x%x < bpt base 0x%x\r\n", img_base,
                    bpt_base);
        return -1;
    }

    iib_info->tag = BPT_IIB_TAG_VAL;
    iib_info->size = BPT_IIB_SIZE;
    iib_info->reserved1 = 0;
    memset(iib_info->dbg_ctrl, 0, 8);
    memset(iib_info->did, 0, 8);
    iib_info->img_type = 0;
    iib_info->target_core = core;
    iib_info->dec_ctrl = 0;
    iib_info->reserved2 = 0;
    memset(iib_info->iv, 0, 8);
    iib_info->img_page = img_offset / BPT_PAGE_SIZE;
    iib_info->img_size = img_size;

    if (link_base == 0) {
        iib_info->load_addr = (uint32_t)img_base;
    } else {
        iib_info->load_addr = link_base;
    }

    iib_info->reserved3 = 0;
    iib_info->entry = iib_info->load_addr;
    iib_info->reserved4 = 0;
    memset(iib_info->hash, 0x00, 64);
    return 0;
}

/* get the iib image base */
uint32_t BptGetIIBImgbase(bpt_v2_t *bpt, uint8_t core)
{
    bpt_iib_t *iib_info;
    uint8_t idx;

    for (idx = 0; idx < BPT_IIB_NUM; idx++) {
        iib_info = (bpt_iib_t *)(bpt->iib[idx]);

        if (iib_info->tag == BPT_IIB_TAG_VAL && iib_info->target_core == core) {
            return (iib_info->img_page) * BPT_PAGE_SIZE;
        }
    }

    return 0;
}

/* mask an iib */
static void BptMaskIIB(bpt_v2_t *bpt, uint8_t core)
{
    bpt_iib_t *iib_info;
    uint8_t idx;

    for (idx = 0; idx < BPT_IIB_NUM; idx++) {
        iib_info = (bpt_iib_t *)(bpt->iib[idx]);

        if (iib_info->tag == BPT_IIB_TAG_VAL && iib_info->target_core == core) {
            memset(iib_info, 0, BPT_IIB_SIZE);
            break;
        }
    }
}

/* remove an empty iib*/
static void BptRemoveEmptyIIB(bpt_v2_t *bpt, uint8_t index)
{
    bpt_iib_t *iib_info;
    bpt_iib_t *iib_info_next;
    uint8_t idx = index;

    for (idx = index; idx < BPT_IIB_NUM - 1; idx++) {
        iib_info = (bpt_iib_t *)(bpt->iib[idx]);
        iib_info_next = (bpt_iib_t *)(bpt->iib[idx + 1]);

        if (iib_info->tag != BPT_IIB_TAG_VAL) {
            memcpy(iib_info, iib_info_next, BPT_IIB_SIZE);
        }
    }

    if (idx == (BPT_IIB_NUM - 1)) {
        iib_info = (bpt_iib_t *)(bpt->iib[idx]);

        if (iib_info->tag != BPT_IIB_TAG_VAL) {
            memset(iib_info, 0, BPT_IIB_SIZE);
        }
    }
}

/* exchange two iib's position */
static void BptExchangeIIB(bpt_v2_t *bpt, uint8_t index_des, uint8_t index_src)
{
    bpt_iib_t *iib_info_des;
    bpt_iib_t *iib_info_src;

    iib_info_des = (bpt_iib_t *)(bpt->iib[index_des]);
    iib_info_src = (bpt_iib_t *)(bpt->iib[index_src]);

    memcpy(&iib_info_temp, iib_info_des, BPT_IIB_SIZE);
    memcpy(iib_info_des, iib_info_src, BPT_IIB_SIZE);
    memcpy(iib_info_src, &iib_info_temp, BPT_IIB_SIZE);
}

/* search iib for one core */
static bpt_iib_t *BptSearchIIB(bpt_v2_t *bpt, uint8_t core)
{
    bpt_iib_t *iib_info;
    uint8_t idx;
    uint8_t core_temp = core;
    uint8_t core_remove = 0;

    if (core == BPT_CORE_CR5_SX) {
        core = BPT_CORE_CR5_SX0;
        core_remove = BPT_CORE_CR5_SX1;
    }

    if (core == BPT_CORE_CR5_SP) {
        core = BPT_CORE_CR5_SP0;
        core_remove = BPT_CORE_CR5_SP1;
    }

    iib_info = (bpt_iib_t *)(bpt->iib[0]);

    if ((iib_info->tag != BPT_IIB_TAG_VAL) || (iib_info->target_core != 0)) {
        ssdk_printf(SSDK_ALERT, "sf core not in bpt, download sf first\r\n");
        return NULL;
    }

    for (idx = 1; idx < BPT_IIB_NUM; idx++) {
        iib_info = (bpt_iib_t *)(bpt->iib[idx]);

        /* find same core */
        if (iib_info->tag == BPT_IIB_TAG_VAL &&
            ((iib_info->target_core == core) ||
             (iib_info->target_core == core_temp))) {

            /* if the core is lockstep, remove the split core sp1 or sx1*/
            if (core_temp != core) {
                BptMaskIIB(bpt, core_remove);
            }

            iib_info->target_core = core_temp;
            return iib_info;
        }
    }

    for (idx = 1; idx < BPT_IIB_NUM; idx++) {
        iib_info = (bpt_iib_t *)(bpt->iib[idx]);

        /* find the first invalid tag core */
        if (iib_info->tag != BPT_IIB_TAG_VAL) {
            return iib_info;
        }
    }

    return NULL;
}

/* sort iib */
static void BptSortIIB(bpt_v2_t *bpt)
{
    bpt_iib_t *iib_info;
    bpt_iib_t *iib_info_next;
    uint8_t idx;
    uint8_t idy;
    uint8_t min_idy;

    for (idx = 1; idx < BPT_IIB_NUM; idx++) {
        BptRemoveEmptyIIB(bpt, idx);
    }

    for (idx = 1; idx < BPT_IIB_NUM - 1; idx++) {

        iib_info = (bpt_iib_t *)(bpt->iib[idx]);

        if (iib_info->tag != BPT_IIB_TAG_VAL)
            break;

        min_idy = idx;

        for (idy = idx + 1; idy < BPT_IIB_NUM; idy++) {
            iib_info_next = (bpt_iib_t *)(bpt->iib[idy]);

            if (iib_info_next->tag != BPT_IIB_TAG_VAL)
                break;

            if (iib_info_next->target_core < iib_info->target_core) {
                min_idy = idy;
            } else if (iib_info_next->target_core == iib_info->target_core) {
                memset(iib_info_next, 0, BPT_IIB_SIZE);
                BptRemoveEmptyIIB(bpt, idy);
            }
        }

        if (min_idy != idx)
            BptExchangeIIB(bpt, idx, min_idy);
    }

    return;
}

/* dump bpt information */
static int32_t dump_bpt_info(bpt_v2_t *bpt)
{
    int i = 0;
    bpt_iib_t *iib;

    ssdk_printf(SSDK_ALERT, "=================  BPT 0  ================= \r\n");
    ssdk_printf(SSDK_ALERT, "bpt->tag                  = 0x%08x\r\n", bpt->tag);
    ssdk_printf(SSDK_ALERT, "bpt->size                 = 0x%x\r\n", bpt->size);
    ssdk_printf(SSDK_ALERT, "bpt->sec_version          = 0x%08x\r\n",
                bpt->sec_ver);
    ssdk_printf(SSDK_ALERT, "bpt->hash_alg             = 0x%x\r\n",
                bpt->digest_alg);
    ssdk_printf(SSDK_ALERT, "bpt->crc32                = 0x%08x\r\n", bpt->crc);
    ssdk_printf(SSDK_ALERT, "bpt->pac_serial_num       = 0x%08x\r\n", bpt->sn);
    ssdk_printf(SSDK_ALERT, "bpt->inver_pac_serial_num = 0x%08x\r\n",
                bpt->sn_inversion);

    for (i = 0; i < BPT_IIB_NUM; i++) {
        iib = (bpt_iib_t *)(bpt->iib[i]);

        if ((iib->tag != BPT_IIB_TAG_VAL)) {
            ssdk_printf(SSDK_ALERT, "iib[%d] is none\r\n", i);
            break;
        }

        ssdk_printf(SSDK_ALERT, "--------\r\n");
        ssdk_printf(SSDK_ALERT, "iib[%d]->tag            = 0x%x\r\n", i,
                    iib->tag);
        ssdk_printf(SSDK_ALERT, "iib[%d]->size           = 0x%x\r\n", i,
                    iib->size);
        ssdk_printf(SSDK_ALERT, "iib[%d]->image_type     = 0x%x\r\n", i,
                    iib->img_type);
        ssdk_printf(SSDK_ALERT, "iib[%d]->target_core    = 0x%x\r\n", i,
                    iib->target_core);
        ssdk_printf(SSDK_ALERT, "iib[%d]->decryp_ctl     = 0x%x\r\n", i,
                    iib->dec_ctrl);
        ssdk_printf(SSDK_ALERT, "iib[%d]->dev_logic_page = 0x%08x\r\n", i,
                    iib->img_page);
        ssdk_printf(SSDK_ALERT, "iib[%d]->image_size     = 0x%08x\r\n", i,
                    iib->img_size);
        ssdk_printf(SSDK_ALERT, "iib[%d]->load_base      = 0x%08x\r\n", i,
                    iib->load_addr);
        ssdk_printf(SSDK_ALERT, "iib[%d]->entry_point    = 0x%08x\r\n", i,
                    iib->entry);
    }

    return 0;
}

/* update bpt */
int BptUpdate(flash_wrapper_t *flash_wrapper, void *img_base,
              uint32_t link_base, uint32_t img_size, uint8_t core)
{
    bpt_v2_t *bpt;
    bpt_iib_t *iib_info;
    uint32_t bpt_base0, bpt_base1, bpt_base2;
    int crc;

    // read sfs
    if (sfs == NULL) {
        ssdk_printf(SSDK_ALERT, "read sfs error\r\n");
        return -1;
    }

    bpt_base0 = sfs->normal_img_base;
    bpt_base1 = sfs->backup_img_base;
    bpt_base2 = sfs->third_img_base;

    ssdk_printf(SSDK_ALERT, "bpt0: 0x%x, bpt1: 0x%x, bpt2: 0x%x\r\n", bpt_base0,
                bpt_base1, bpt_base2);

    flash_wrapper_read(flash_wrapper, (flash_addr_t)bpt_base0, bpt_buffer,
                       BPT_HEDAER_LEN);

    bpt = (bpt_v2_t *)bpt_buffer;

    if (core == 0) {
        BptInit(bpt);
        iib_info = (bpt_iib_t *)(bpt->iib[0]);
    } else {
        iib_info = BptSearchIIB(bpt, core);
    }

    if (iib_info == NULL) {
        ssdk_printf(SSDK_ALERT, "Find IIB error\r\n");
        return -1;
    }

    if (BptAddIIB(iib_info, (uint32_t)img_base, bpt_base0, link_base, img_size,
                  core) < 0) {
        return -1;
    }

    BptSortIIB(bpt);

    crc = sfs_crc32(0, (uint8_t *)bpt, BPT_CRC_CHECKSUM_OFFSET);
    memcpy(&bpt->crc, &crc, 4);

    if (flash_wrapper_erase(flash_wrapper, (flash_addr_t)bpt_base0,
                            BPT_HEDAER_LEN)) {
        ssdk_printf(SSDK_ALERT, " erase 0x%08x, length 0x%08x error",
                    (uint32_t)bpt_base0, BPT_HEDAER_LEN);
        return -1;
    }

    if (flash_wrapper_write(flash_wrapper, (flash_addr_t)bpt_base0, bpt_buffer,
                            BPT_HEDAER_LEN)) {
        ssdk_printf(SSDK_ALERT, " write 0x%08x, length 0x%08x error",
                    (uint32_t)bpt_base0, BPT_HEDAER_LEN);
        return -1;
    }

    if (flash_wrapper_read(flash_wrapper, (flash_addr_t)bpt_base0, bpt_buffer,
                           BPT_HEDAER_LEN)) {
        ssdk_printf(SSDK_ALERT, " read 0x%08x, length 0x%08x error",
                    (uint32_t)bpt_base0, BPT_HEDAER_LEN);
        return -1;
    }
    bpt = (bpt_v2_t *)bpt_buffer;
    dump_bpt_info(bpt);

    return 0;
}

/* read sfs in flash */
int read_sfs(flash_wrapper_t *flash_wrapper)
{
    uint32_t crc_val = 0;
    uint32_t crc_orig = 0;

    flash_wrapper_read(flash_wrapper, SFS_BASE, sfs_buffer, SFS_SIZE);

    ssdk_printf(SSDK_ALERT, "sfs dump\r\n");
    hexdump(sfs_buffer, SFS_SIZE);
    sfs = (sfs_t *)sfs_buffer;

    crc_orig = sfs->crc32;
    crc_val = sfs_crc32(0, sfs_buffer, SFS_SIZE - 4);

    if (crc_val != crc_orig) {
        ssdk_printf(SSDK_ALERT, "sfs crc_orig:0x%0x crc_val:0x%0x!\r\n",
                    crc_orig, crc_val);
        sfs = NULL;
        return -1;
    }

    ssdk_printf(SSDK_ALERT, "sfs tag = 0x%08x\r\n", sfs->tag);
    ssdk_printf(SSDK_ALERT, "sfs freq = 0x%02x\r\n", sfs->freq);
    ssdk_printf(SSDK_ALERT, "sfs sw_reset_info = 0x%02x\r\n",
                sfs->sw_reset_info);
    ssdk_printf(SSDK_ALERT, "sfs normal_img_base = 0x%08x\r\n",
                sfs->normal_img_base);
    ssdk_printf(SSDK_ALERT, "sfs backup_img_base = 0x%08x\r\n",
                sfs->backup_img_base);
    ssdk_printf(SSDK_ALERT, "sfs third_img_base = 0x%08x\r\n",
                sfs->third_img_base);
    ssdk_printf(SSDK_ALERT, "sfs crc32 = 0x%08x\r\n", sfs->crc32);

    return 0;
}