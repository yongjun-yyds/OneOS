/**
 * @file udelay.h
 * @brief udelay common api
 *
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef SDRV_UDELAY_H_
#define SDRV_UDELAY_H_

#include <types.h>

/**
 * @brief max udelay time, 5 seconds
 *
 */
#define MAX_UDELAY  (5000000UL)

/**
 * @brief Initialize ARM Performance Monitor cycle counter
 *
 *  sdrv udelay is implement by arm pmu cycle counter, you must
 *  call this function before use udelay().
 *
 *  note: this function must called after cpu clock has configurated.
 */
void sdrv_pmu_counter_init(void);


/**
 * @brief delay for microseconds
 *
 * @param us microseconds
 */
void udelay(uint32_t us);

#endif /* SDRV_UDELAY_H_ */
