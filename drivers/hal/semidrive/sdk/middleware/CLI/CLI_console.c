/*
 * console.c
 *
 * Copyright (c) 2020 Semidrive Semiconductor.
 * All rights reserved.
 *
 * Description: console function.
 *
 * Revision History:
 * -----------------
 */

#include <CLI.h>
#include <debug.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "FreeRTOS_CLI.h"

/* BS and DEL ASCII Number */
#define CLI_ASCII_BS 0x08
#define CLI_ASCII_DEL 0x7F

/* Command Buffer */
#define CLI_INPUT_BUFFER_SIZE 64
#define CLI_OUTPUT_BUFFER_SIZE 1024
static char gcInputBuffer[CLI_INPUT_BUFFER_SIZE];
static char gcOutputBuffer[CLI_OUTPUT_BUFFER_SIZE];
static uint8_t gucInputIndex;

/* a linear array of statically defined command blocks,
   defined in the linker script.
 */
extern CLI_Command_Definition_t __commands_start[];
extern CLI_Command_Definition_t __commands_end[];

/* Const messages output by the command console. */
static const char *const gpcWelcomeMessage =
    "\r\nFreeRTOS CLI.\r\nType Help to view a list of registered "
    "commands.\r\n>";
static const char *const gpcNewLine = "\r\n>";
static const char *const gpcErrMessage = "\r\nInput Command Error\r\n>";

/*
 * Register CLI commands.
 */
static void CLIRegisterCommands(void)
{
    CLI_Command_Definition_t *cmd;
    for (cmd = __commands_start; cmd != __commands_end; cmd++) {
        CLIRegisterCommand(cmd);
    }
}

/*
 * console task entry.
 *
 * @ arg   input param.
 */
void CLICommandHandler(char cRxedChar)
{
    int xReturned;

    /* Is end of the line? */
    if (cRxedChar == '\n' || cRxedChar == '\r') {
        /* Start New Line */
        CLIWriteData(gpcNewLine, strlen(gpcNewLine));

        if (gucInputIndex) {
            gcInputBuffer[gucInputIndex] = '\0';
            do {
                gcOutputBuffer[0] = '\0';

                /* Get the next output string from the command interpreter. */
                xReturned = CLIProcessCommand(gcInputBuffer, gcOutputBuffer,
                                              CLI_OUTPUT_BUFFER_SIZE);

                /* Write the generated string to the UART. */
                CLIWriteData(gcOutputBuffer, strlen(gcOutputBuffer));
                CLIWriteData(gpcNewLine, strlen(gpcNewLine));
            } while (xReturned != CLI_FALSE);

            gucInputIndex = 0;
        }
    } else if ((cRxedChar == CLI_ASCII_BS) || (cRxedChar == CLI_ASCII_DEL)) {
        /* Backspace was pressed.  Erase the last character in the
        string - if any. */
        if (gucInputIndex > 0) {
            gucInputIndex--;
            gcInputBuffer[gucInputIndex] = '\0';
            char bs = CLI_ASCII_BS;
            char space = ' ';
            CLIWriteData(&bs, 1);
            CLIWriteData(&space, 1);
            CLIWriteData(&bs, 1);
        }
    } else {
        /* echo */
        CLIWriteData(&cRxedChar, 1);

        if (gucInputIndex < CLI_INPUT_BUFFER_SIZE) {
            gcInputBuffer[gucInputIndex] = cRxedChar;
            gucInputIndex++;
        } else {
            CLIWriteData(gpcErrMessage, strlen(gpcErrMessage));
            gucInputIndex = 0;
        }
    }
}

/*
 * Tokenize Command.
 *
 * @ pcCommandString   input string.
 * @ argv argv array.
 */
int CLITokenizeCommand(const char *pcCommandString, char *argv[])
{
    int arg_num = 0;
    int arg_len = 0;
    char *arg_ptr = (char *)pcCommandString;

    /* Index the character pointer past the current word.  If this is the start
    of the command string then the first word is the command itself. */
    while (((*arg_ptr) != 0x00) && ((*arg_ptr) != ' ')) {
        arg_ptr++;
    }

    while ((arg_ptr = (char *)CLIGetParameter(arg_ptr, 1, &arg_len)) != NULL) {
        argv[arg_num++] = arg_ptr;
        arg_ptr += arg_len;
        if (*arg_ptr == ' ') {
            *arg_ptr = '\0';
            arg_ptr++;
        }
        if (arg_num >= CLI_MAX_ARGS) {
            ssdk_printf(SSDK_WARNING, "CLI command extend max args num\n");
            break;
        }
    }

    return arg_num;
}

/*
 * CLI Init.
 *
 * @ pcCommandString   input string.
 * @ argv argv array.
 */
int CLIInit(void)
{
    CLIRegisterCommands();
    CLIWriteData(gpcWelcomeMessage, strlen(gpcWelcomeMessage));
    return 0;
}