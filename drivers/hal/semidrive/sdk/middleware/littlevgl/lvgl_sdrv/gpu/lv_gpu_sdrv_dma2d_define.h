/**
 * @file lv_gpu_sdrv_dma2d_define.h
 *
 */

#ifndef LV_GPU_SEMIDRIVE_DMA2D_DEFINE_H
#define LV_GPU_SEMIDRIVE_DMA2D_DEFINE_H

#ifdef __cplusplus
extern "C" {
#endif

/*********************
 *      INCLUDES
 *********************/
#include "lvgl.h"

/*********************
 *      DEFINES
 *********************/
#define LV_GPU_WIN_MAX_CNT   2
#define LV_GPU_YUVA_ADDR_CNT 4

/* fmt */
typedef enum {
    LV_FMT_A8 = 0,             /* alpha 8bit */
    LV_FMT_RGB565,             /* rgb565 */
    LV_FMT_ARGB4444,           /* argb4444 */
    LV_FMT_ARGB1555,           /* argb1555 */
    LV_FMT_RGB888,             /* rgb888 */
    LV_FMT_ARGB8888,           /* argb8888 */
    LV_FMT_BUTT
} LV_GPU_Format;

typedef enum {
    LV_TRIANGLE_TOP_LEFT = 0,  /* shows the triangle in the top left corner */
    LV_TRIANGLE_BOTTOM_LEFT,   /* shows the triangle in the bottom left corner */
    LV_TRIANGLE_TOP_RIGHT,     /* shows the triangle in the top right corner */
    LV_TRIANGLE_BOTTOM_RIGHT,  /* shows the triangle in the bottom right corner */
    LV_TRIANGLE_BUTT
} LV_GPU_TRIANLE_Type;

typedef struct {
    lv_color_t color; /* full color, A:bit[31:24], R:bit[23:16], G:bit[15:8], B:bit[7:0] */
    LV_GPU_Format fmt;  /* buf fmt */
    lv_color_t *buf;  /* buf addr */
    lv_coord_t stride;  /* buf stride */
    lv_area_t area;   /* full rect */
} LV_GPU_FillRectInfo;

typedef struct  {
    LV_GPU_TRIANLE_Type type;  /* The area type of a triangle */
    LV_GPU_Format fmt;       /* The format type of a triangle */
    lv_color_t color;         /* The color of a triangle, A:bit[31:24], R:bit[23:16], G:bit[15:8], B:bit[7:0]*/
    lv_area_t area;      /* The color of a triangle */
    lv_color_t *buf;           /* The buf of a triangle */
    lv_coord_t stride;        /* The stride of buf */
} LV_GPU_FillRatriaInfo;

typedef enum {
    LV_GPU_RLE_IN_BG_SURFACE = 0,  /* run-length encoding in the background surface */
    LV_GPU_RLE_IN_FG_SURFACE,      /* run-length encoding in the foreground surface */
    LV_GPU_RLE_BUTT
} LV_GPU_RleSurfaceType;

typedef struct {
    LV_GPU_RleSurfaceType rleType;
} LV_GPU_RleInfo;

typedef struct {
    lv_color_t *buf;              // surface buf addr
    lv_opa_t alpha;                // surface alpha
    lv_coord_t stride;              // surface buf stride
    LV_GPU_Format fmt;             // surface format type
    lv_area_t srcArea;           // surface source rect
    lv_area_t dstArea;           // surface destination rect
    lv_draw_mask_res_t maskRes; // surface blend mode
} LV_GPU_SurfaceInfo;

typedef struct {
    lv_color_t *buf;                // output buf addr
    lv_coord_t stride;             // output rect buf stride
    LV_GPU_Format fmt;            // output rect buf format
    lv_area_t area;               // Output to the area of the output rectangle
} LV_GPU_OutputInfo;

typedef struct {
    LV_GPU_SurfaceInfo fgSurface;  // need to synthetic the upper surface
    LV_GPU_SurfaceInfo bgSurface;  // need to synthesize the lower surface
    LV_GPU_OutputInfo  output;     // outout rect info
} LV_GPU_BlendImgInfo;

typedef struct {
    lv_opa_t *buf;     // mask area buf
    lv_color32_t color;   // mask area color, ARGB888, A:bit[31:24], R:bit[23:16], G:bit[15:8], B:bit[7:0]
    lv_coord_t stride;  // mask area buf stride
} LV_GPU_MaskInfo;

typedef struct {
    LV_GPU_SurfaceInfo surfaceInfo; // need to synthetic the surface
    LV_GPU_OutputInfo output;        // outout area info
    LV_GPU_MaskInfo maskInfo;       // need to synthetic the mask area info
} LV_GPU_BlendMaskInfo;

typedef struct {
    LV_GPU_OutputInfo output;      // outout area info
    LV_GPU_MaskInfo maskInfo;     // need to synthetic the mask area info
} LV_GPU_FillMaskInfo;

typedef struct {
    lv_color_t *buf;       // copy area buf addr
    LV_GPU_Format fmt;   // copy area format
    lv_area_t area;      // copy region location
    lv_coord_t stride;    // copy area buf stride
} LV_GPU_FastCopyInfo;

typedef struct {
    lv_area_t srcArea;    /* the rect for rotate */
    lv_color_t *srcBuf;     /* the rect buf for rotation */
    LV_GPU_Format srcFmt; /* the rect format for rotation */
    lv_coord_t srcStride;  /* the rect buf stride */

    lv_color_t *dstBuf;     /* output rect buf */
    LV_GPU_Format dstFmt; /* output rect format*/
    lv_coord_t dstWidth;   /* output rect widtht*/
    lv_coord_t dstHeight;  /* output rect height*/
    lv_coord_t dstStride;  /* output rect buf strde*/

    double angle;         /* rotate angle */
    /* ARGB888, A:bit[31:24], R:bit[23:16], G:bit[15:8], B:bit[7:0]
       Pixels outside the rotated region fill the background color */
    lv_color32_t bgColor;
} LV_GPU_RotateInfo;

typedef struct {
    lv_color_t *buf;      /* cache buf addr */
    lv_area_t area;     /* clean cache rect */
    LV_GPU_Format fmt;  /* cache format */
    lv_coord_t stride;   /* cache buf format */
} LV_GPU_CacheInfo;

typedef struct {
    unsigned int id;
    int dirty;
    int en;
    LV_GPU_Format fmt;
    lv_area_t src;
    lv_color_t *addr[LV_GPU_YUVA_ADDR_CNT];//YUVA
    lv_coord_t srcStride[LV_GPU_YUVA_ADDR_CNT];
    lv_area_t dst;
    int enCkey;
    int ckey;
    int enAlpha;
    char alpha;
    int zOrder;
    int security;
} LV_GPU_WindowInfo;
typedef struct {
    unsigned int winCnt;
    LV_GPU_WindowInfo winInfo[LV_GPU_WIN_MAX_CNT];
} LV_GPU_DisplayInfo;

typedef enum  {
    LV_SCREEN_TYPE_CLUSTER = 0,
    LV_SCREEN_TYPE_BUTT
} LV_GPU_ScreenType;
/*************************
 *      DEFINES FUNCTION
 *************************/
#define LV_CHECK_NULLPTR_RETURN(ptr_, errno_) \
do { \
    if ((ptr_) == NULL) { \
    LV_LOG_ERROR("error [dma2d][%s][%d]: %s is null!\r\n", __FUNCTION__, __LINE__, #ptr_); \
    return errno_; \
    } \
} while (0);

#define LV_CHECK_AREA_RETURN(area_, errno_) \
do { \
    if ((area_.x1 > area_.x2) && (area_.y1 > area_.y2)) { \
    LV_LOG_ERROR("error [dma2d][%s][%d]: %s x1[%d] Must be smaller than x2[%d] and area y1[%d] Must be smaller than y2[%d]!\r\n", \
        __FUNCTION__, __LINE__, #area_, area_.x1, area_.x2, area_.y1, area_.y2); \
    return errno_; \
    } \
} while (0);

#define LV_CHECK_RETURN(ret_, str_) \
do { \
    if ((ret_) != HAL_SUCCESS) { \
        LV_LOG_ERROR("error [dma2d][%s][%d]: %s fail,ret:%#x!!\r\n", __FUNCTION__, __LINE__, str_, ret_); \
        return ret_; \
    } \
} while (0);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /*LV_GPU_SEMIDRIVE_DMA2D_DEFINE_H*/
