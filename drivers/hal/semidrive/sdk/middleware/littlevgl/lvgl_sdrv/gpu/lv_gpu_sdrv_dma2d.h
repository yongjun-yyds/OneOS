/**
 * @file lv_gpu_sdrv_dma2d.h
 *
 */

#ifndef LV_GPU_SEMIDRIVE_DMA2D_H
#define LV_GPU_SEMIDRIVE_DMA2D_H

#ifdef __cplusplus
extern "C" {
#endif

/*********************
 *      INCLUDES
 *********************/
#include "lv_gpu_sdrv_dma2d_define.h"
#include "lvgl.h"

/*********************
 *      DEFINES
 *********************/

#define LV_DMA2D_ARGB8888 0
#define LV_DMA2D_RGB888 1
#define LV_DMA2D_RGB565 2
#define LV_DMA2D_ARGB1555 3
#define LV_DMA2D_ARGB4444 4

/**********************
 *      TYPEDEFS
 **********************/

/**********************
 * GLOBAL PROTOTYPES
 **********************/

/**
 * Turn on the peripheral and set output color mode, this only needs to be done once
 */
void lv_gpu_sdrv_dma2d_init(void);

/**
 * Fill an area in the buffer with a color
 * @param buf a buffer which should be filled
 * @param buf_w width of the buffer in pixels
 * @param color fill color
 * @param fill_w width to fill in pixels (<= buf_w)
 * @param fill_h height to fill in pixels
 * @note `buf_w - fill_w` is offset to the next line after fill
 */
void lv_gpu_sdrv_dma2d_fill(lv_color_t * buf, lv_coord_t buf_w, lv_color_t color,
                                lv_coord_t fill_w, lv_coord_t fill_h, lv_opa_t opa, uint32_t fmt);

/**
 * Fill an area in the buffer with a color but take into account a mask which describes the opacity of each pixel
 * @param buf a buffer which should be filled using a mask
 * @param buf_w width of the buffer in pixels
 * @param color fill color
 * @param mask 0..255 values describing the opacity of the corresponding pixel. It's width is `fill_w`
 * @param opa overall opacity. 255 in `mask` should mean this opacity.
 * @param fill_w width to fill in pixels (<= buf_w)
 * @param fill_h height to fill in pixels
 * @note `buf_w - fill_w` is offset to the next line after fill
 */
void lv_gpu_sdrv_dma2d_fill_mask(lv_color_t * buf, lv_coord_t buf_w, lv_color_t color, const lv_opa_t * mask,
                                  lv_opa_t opa, lv_coord_t fill_w, lv_coord_t fill_h);
void lv_gpu_sdrv_dma2d_fill_mask_qd(lv_color_t * buf, lv_coord_t buf_w, lv_color_t color, const lv_opa_t * mask,
									lv_coord_t map_stride/*lq*/,
                                  lv_opa_t opa, lv_coord_t fill_w, lv_coord_t fill_h);

/**
 * Copy a map (typically RGB image) to a buffer
 * @param buf a buffer where map should be copied
 * @param buf_w width of the buffer in pixels
 * @param map an "image" to copy
 * @param map_w width of the map in pixels
 * @param copy_w width of the area to copy in pixels (<= buf_w)
 * @param copy_h height of the area to copy in pixels
 * @note `map_w - fill_w` is offset to the next line after copy
 */
int lv_gpu_sdrv_g2d_copy(lv_color_t * buf, lv_coord_t buf_stride/*buf_w lq*/, const lv_color_t * map, lv_coord_t /*map_w  qlj*/map_stride,
                             lv_coord_t copy_w, lv_coord_t copy_h);
/**
 * Blend a map (e.g. ARGB image or RGB image with opacity) to a buffer
 * @param buf a buffer where `map` should be copied
 * @param buf_w width of the buffer in pixels
 * @param map an "image" to copy
 * @param opa opacity of `map`
 * @param map_w width of the map in pixels
 * @param copy_w width of the area to copy in pixels (<= buf_w)
 * @param copy_h height of the area to copy in pixels
 * @note `map_w - fill_w` is offset to the next line after copy
 */
int lv_gpu_sdrv_g2d_blend(const lv_area_t * disp_area, lv_color_t * disp_buf,
                            const lv_area_t * draw_area,
                            const lv_area_t * map_area, const lv_color_t * map_buf,lv_coord_t	map_buf_stride/*lq added*/, lv_opa_t opa,
                            const lv_opa_t * mask, lv_draw_mask_res_t mask_res);
int lv_gpu_sdrv_g2d_blend_mask(const lv_area_t * disp_area, lv_color_t * disp_buf,
                             const lv_area_t * draw_area,
                             const lv_area_t * map_area,  const lv_color_t * map_buf, lv_coord_t	map_buf_stride/*lq added*/,
                             lv_opa_t opa,
                             const lv_opa_t * mask,lv_coord_t	mask_buf_stride/*lq added*/,
                             lv_area_t * mask_area, lv_draw_mask_res_t mask_res,lv_color_t bgcfg_color,bool bgcfg_en);

int lv_gpu_sdrv_g2d_blend_scale(const lv_area_t * disp_area, lv_color_t * disp_buf,
                            const lv_area_t * draw_area,
                            const lv_area_t * map_area, const lv_color_t * map_buf,
                            lv_coord_t	map_buf_stride/*lq added*/,
                            const lv_area_t * scale_map_area, lv_opa_t opa,
                            const lv_opa_t * mask, lv_draw_mask_res_t mask_res);
/**********************
 *      MACROS
 **********************/

void lv_gpu_sdrv_asw_fill(lv_color_t * buf, lv_coord_t buf_w, lv_color_t color, const lv_area_t * full_area, uint32_t fmt);


/*********************************************************************
*************************new define***********************************
*********************************************************************/
int LV_GPU_G2DLITE_Init(void);
int LV_GPU_G2DLITE_Deinit(void);
int LV_GPU_G2DLITE_FillRect(const LV_GPU_FillRectInfo *info);
int LV_GPU_G2DLITE_FillMask(LV_GPU_FillMaskInfo *info);
int LV_GPU_G2DLITE_BlendImg(const LV_GPU_BlendImgInfo *info);
int LV_GPU_G2DLITE_BlendRleImg(const LV_GPU_BlendImgInfo *info, LV_GPU_RleInfo *rleInfo);
int LV_GPU_G2DLITE_BlendMask(const LV_GPU_BlendMaskInfo *info);
int LV_GPU_G2DLITE_FastCopy(const LV_GPU_FastCopyInfo *in, LV_GPU_FastCopyInfo *out);

int LV_GPU_ASW_FillRect(const LV_GPU_FillRectInfo *info);
int LV_GPU_ASW_FillTriangle(const LV_GPU_FillRatriaInfo *info);

void *LV_GPU_DISP_GetHandle(LV_GPU_ScreenType type);
int LV_GPU_DISP_Post(void *handle, const LV_GPU_DisplayInfo *info);

int LV_GPU_Rotate(const LV_GPU_RotateInfo *info);
int LV_GPU_CleanCacheRange(const LV_GPU_CacheInfo *info);
int LV_GPU_InvalidateCacheRange(const LV_GPU_CacheInfo *info);
int LV_GPU_CleanInvalidateCacheRange(const LV_GPU_CacheInfo *info);
void LV_GPU_CleanInvalidateCacheAll(void);



#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /*lv_gpu_sdrv_DMA2D_H*/
