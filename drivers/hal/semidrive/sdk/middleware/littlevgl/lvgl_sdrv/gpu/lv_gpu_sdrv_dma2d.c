/**
 * @file lv_gpu_sdrv_dma2d.c
 *
 */

/*********************
 *      INCLUDES
 *********************/
#include "lv_gpu_sdrv_dma2d.h"
#include <stdio.h>
#include <stdlib.h>
#include "lvgl.h"
#include <dispss/disp.h>
#include <dispss/disp_data_type.h>
#include <g2dlite/g2dlite.h>
#include <asw/asw.h>
#include "hal_g2dlite.h"
#include "hal_asw.h"
#include "hal_sys.h"
#include "hal_gpu.h"
#include "hal_disp.h"
#if LV_USE_GPU_SEMIDRIVE_G2D

/*********************
 *      DEFINES
 *********************/

#if LV_COLOR_16_SWAP
    // TODO: F7 has red blue swap bit in control register for all layers and output
    #error "Can't use DMA2D with LV_COLOR_16_SWAP 1"
#endif

#if LV_COLOR_DEPTH == 8
    #error "Can't use DMA2D with LV_COLOR_DEPTH == 8"
#endif

#if LV_COLOR_DEPTH == 16
    #define LV_DMA2D_COLOR_FORMAT LV_DMA2D_RGB565
#elif LV_COLOR_DEPTH == 32
    #define LV_DMA2D_COLOR_FORMAT LV_DMA2D_ARGB8888
#else
    /*Can't use GPU with other formats*/
#endif

/**********************
 *      TYPEDEFS
 **********************/
#define DMA_SYNC_TIMEOUT (1000)
/**********************
 *  STATIC PROTOTYPES
 **********************/
static void invalidate_cache(void* color_p, size_t size);
//static void dma2d_wait(struct dma_desc *desc);

/**********************
 *  STATIC VARIABLES
 **********************/
// struct dma_chan *hal_dma_chan_req(enum dma_chan_tr_type ch_type);
// static struct dma_chan *DMA2D = NULL;
/**********************
 *      MACROS
 **********************/

/**********************
 *   GLOBAL FUNCTIONS
 **********************/
struct g2dlite {
    int index;
    addr_t reg_addr;
    uint32_t irq_num;
};
static void *G2D = NULL;

/**
 * Turn on the peripheral and set output color mode, this only needs to be done once
 */
void lv_gpu_sdrv_dma2d_init(void)
{
    // int ret;
    // DMA2D = hal_dma_chan_req(DMA_MEM);

    // ret = hal_g2dlite_creat_handle(&G2D, RES_G2D_G2D2);
    // if (!ret) {
    //     LOGD("g2dlite creat handle failed\n");
    // }

    hal_g2dlite_init(&G2D);

}

typedef union {
    struct
    {
        uint32_t blue :10;
        uint32_t green :10;
        uint32_t red :10;
    } ch;
    uint32_t full;
} color_10bit_t;

int bytesPerPixelFromPixelFormat(uint32_t fmt)
{
    switch (fmt) {
    case COLOR_ARGB8888:
    case COLOR_BGRA8888:
    case COLOR_ABGR8888:
        return 4;
    case COLOR_RGB888:
        return 3;
    case COLOR_RGB565:
        return 2;
    default:
        printf("bytesPerPixelFromHwPixelFormat Unsupported pixel format %d\n", fmt);
        return -1;
    }
}

static void gpu_fill_cb(lv_color_t * dest_buf, lv_coord_t dest_width,
                        const lv_area_t * fill_area, lv_color_t color, lv_opa_t opa)
{

    lv_coord_t x, y;

    dest_buf += dest_width * fill_area->y1; /*Go to the first line*/

    for(y = fill_area->y1; y < fill_area->y2; y++) {
        for(x = fill_area->x1; x < fill_area->x2; x++) {
            // lv_color_premult(color, opa, (uint16_t *)&dest_buf[x]);
            dest_buf[x] = color;
            LV_COLOR_SET_A(dest_buf[x], opa);
            // dest_buf[x] = lv_color_mix(LV_COLOR_ORANGE, color , LV_OPA_100);
        }
        dest_buf+=dest_width;    /*Go to the next line*/
    }
    //arch_clean_cache_range((addr_t)dest_buf, dest_width * (fill_area->y2 - fill_area->y1) * 4);
}


/**
 * Fill an area in the buffer with a color
 * @param buf a buffer which should be filled
 * @param buf_w width of the buffer in pixels
 * @param color fill color
 * @param fill_w width to fill in pixels (<= buf_w)
 * @param fill_h height to fill in pixels
 * @note `buf_w - fill_w` is offset to the next line after fill
 */

void lv_gpu_sdrv_dma2d_fill(lv_color_t * buf, lv_coord_t buf_w, lv_color_t color, lv_coord_t fill_w, lv_coord_t fill_h, lv_opa_t opa, uint32_t fmt)
{
    struct g2dlite_output_cfg output;
    memset(&output, 0, sizeof(output));
    color_10bit_t bg_color;

    bg_color.ch.blue = (color.ch.blue * 0x3FF)/ 255;
    bg_color.ch.green = (color.ch.green * 0x3FF) / 255;
    bg_color.ch.red = (color.ch.red * 0x3FF) / 255;

    output.width = fill_w;
    output.height = fill_h;
    output.fmt = fmt;
    output.addr[0] = (unsigned long)buf;
    output.stride[0] =  buf_w * bytesPerPixelFromPixelFormat(fmt);
    output.rotation = 0;

    hal_g2dlite_fill_rect(G2D, bg_color.full, opa, 0, 0, 0, &output);

    // LV_LOG_WARN("[%s] color 0x%x: (bg_color) 0x%x, opa %d, (%d, %d)", __func__, color.full, bg_color.full, opa, fill_w, fill_h);
}

static int asw_get_fmt_bpp(int fmt)
{
#if CONFIG_ASW
    switch (fmt) {
        case FMT_MONOTONIC_8BIT:
            return 1;
        case FMT_RGB565:
        case FMT_ARGB1555:
        case FMT_RGBA5551:
            return 2;
        case FMT_RGB_YUV888:
            return 3;
        case FMT_RGBA8888:
        case FMT_ARGB8888:
            return 4;
        default:
            printf("can't support this fmt\n");
            return -1;
    }
#else
    return -1;
#endif
}

void lv_gpu_sdrv_asw_fill(lv_color_t * buf, lv_coord_t buf_w, lv_color_t color, const lv_area_t * full_area, uint32_t fmt)
{
    int fill_w = lv_area_get_width(full_area);
    int fill_h = lv_area_get_height(full_area);
    int stride = buf_w * asw_get_fmt_bpp(fmt);
#ifdef CONFIG_ASW
    hal_asw_fill_rect(fmt, color.full, fill_w, fill_h,
                      full_area->x1, full_area->y1, (int)buf, stride);
#else
    LV_LOG_WARN("[%s]CONFIG_ASW not open, don't support asw!\n", __func__);
#endif
    LV_LOG_WARN("[%s] color 0x%x: (%d, %d)", __func__, color.full, fill_w, fill_h);
}

int lv_gpu_sdrv_asw_rotate(lv_color_t * dst_buf, lv_coord_t dst_w, lv_coord_t dst_h, lv_coord_t dst_stride, uint32_t dst_fmt,
    lv_color_t * map_buf, const lv_area_t * map_area, lv_coord_t map_stride, uint32_t map_fmt,
    double angle, lv_color_t bg_color)
{
    hal_asw_rotate_info info;
    memset(&info, 0, sizeof(info));
    info.src_x = map_area->x1;
    info.src_y = map_area->y1;
    info.src_w = lv_area_get_width(map_area);
    info.src_h = lv_area_get_height(map_area);
    info.src_fmt = FMT_ARGB8888;
    info.src_stride = map_stride;
    info.src_buf = (uint8_t*)map_buf;

    info.dst_w = dst_w;
    info.dst_h = dst_h;

    info.dst_fmt = FMT_ARGB8888;
    info.dst_stride = dst_stride;
    info.dst_buf = (uint8_t *)dst_buf;

    info.angle = angle;
    info.bg_color = bg_color.full;
#ifdef CONFIG_ASW
    return hal_aswlite_rotate(G2D, &info);
#else
    LV_LOG_WARN("[%s]CONFIG_ASW not open, don't support asw!\n", __func__);
#endif

}

static uint32_t argb8888_to_rgb2101010(unsigned int color)
{
    return (((color >> 16 & 0x000000ff) << 22) | ((color >> 8 & 0x000000ff) << 12) | ((color & 0x000000ff) << 2));
}

/**
 * Fill an area in the buffer with a color but take into account a mask which describes the opacity of each pixel
 * @param buf a buffer which should be filled using a mask
 * @param buf_w width of the buffer in pixels
 * @param color fill color
 * @param mask 0..255 values describing the opacity of the corresponding pixel. It's width is `fill_w`
 * @param opa overall opacity. 255 in `mask` should mean this opacity.
 * @param fill_w width to fill in pixels (<= buf_w)
 * @param fill_h height to fill in pixels
 * @note `buf_w - fill_w` is offset to the next line after fill
 */
void lv_gpu_sdrv_dma2d_fill_mask(lv_color_t * buf, lv_coord_t buf_w, lv_color_t color, const lv_opa_t * mask,
                                  lv_opa_t opa, lv_coord_t fill_w, lv_coord_t fill_h)
{
    static struct g2dlite_input input;
    memset(&input, 0, sizeof(struct g2dlite_input));
    input.layer_num = 1;

    struct g2dlite_input_cfg  *l = &input.layer[0];
    l->layer_en = 1;
    l->layer = 0;
    l->zorder = 0;
    l->ckey.en = 0;

    l->blend = BLEND_PIXEL_NONE;
    l->addr[0] = (unsigned long) (buf);
    l->src.x = 0;
    l->src.y = 0;
    l->src.w = fill_w;
    l->src.h = fill_h;
    l->fmt = COLOR_ARGB8888;
    l->src_stride[0] = buf_w * 4;

    l->dst.x = 0;// canvas :output x y
    l->dst.y = 0;
    l->dst.w = fill_w;
    l->dst.h = fill_h;
    l->alpha = opa;

    //bg layer
    struct g2dlite_bg_cfg bgcfg;
    memset(&bgcfg, 0, sizeof(struct g2dlite_bg_cfg));
    bgcfg.en = 1;
    bgcfg.color = argb8888_to_rgb2101010(color.full);
    bgcfg.g_alpha = 0;
    bgcfg.aaddr = (addr_t)mask;
    bgcfg.bpa = 8;
    bgcfg.astride = fill_w;
    bgcfg.zorder = 1;
    input.output.width = fill_w;
    input.output.height = fill_h;
    input.output.fmt = COLOR_ARGB8888;
    input.output.addr[0] = (unsigned long)buf;

    input.output.stride[0] = buf_w * 4;
    input.output.rotation = 0;
    // printf("fill_w = %d,fill_h = %d\n",fill_w,fill_h);
    input.bg_layer = bgcfg;
    arch_clean_cache_range((addr_t)mask, fill_w * fill_h);
    sdrv_g2dlite_update(G2D, &input);
    // free(temp);
}

void lv_gpu_sdrv_dma2d_fill_mask_qd(lv_color_t * buf, lv_coord_t buf_w, lv_color_t color, const lv_opa_t * mask,
    lv_coord_t map_stride/*lq*/,
    lv_opa_t opa, lv_coord_t fill_w, lv_coord_t fill_h)
{
    static struct g2dlite_input input;
    memset(&input, 0, sizeof(struct g2dlite_input));
    input.layer_num = 1;

    struct g2dlite_input_cfg  *l = &input.layer[0];
    l->layer_en = 1;
    l->layer = 0;
    l->zorder = 0;
    l->ckey.en = 0;

    l->blend = BLEND_PIXEL_NONE;
    l->addr[0] = (unsigned long) (buf);
    l->src.x = 0;
    l->src.y = 0;
    l->src.w = fill_w;
    l->src.h = fill_h;
    l->fmt = COLOR_ARGB8888;
    l->src_stride[0] = buf_w * 4;

    l->dst.x = 0;// canvas :output x y
    l->dst.y = 0;
    l->dst.w = fill_w;
    l->dst.h = fill_h;
    l->alpha = opa;

    // bg layer
    struct g2dlite_bg_cfg bgcfg;
    memset(&bgcfg, 0, sizeof(struct g2dlite_bg_cfg));
    bgcfg.en = 1;
    bgcfg.color = argb8888_to_rgb2101010(color.full);
    bgcfg.g_alpha = 0;
    bgcfg.aaddr = (addr_t)mask;
    bgcfg.bpa = 8;
    bgcfg.astride = map_stride;//fill_w;lq
    bgcfg.zorder = 1;
    input.output.width = fill_w;
    input.output.height = fill_h;
    input.output.fmt = COLOR_ARGB8888;
    input.output.addr[0] = (unsigned long)buf;

    input.output.stride[0] = buf_w * 4;
    input.output.rotation = 0;
    // printf("fill_w = %d,fill_h = %d\n",fill_w,fill_h);
    input.bg_layer = bgcfg;
    sdrv_g2dlite_update(G2D, &input);
    // free(temp);
}

/**
 * Copy a map (typically RGB image) to a buffer
 * @param buf a buffer where map should be copied
 * @param buf_w width of the buffer in pixels
 * @param map an "image" to copy
 * @param map_w width of the map in pixels
 * @param copy_w width of the area to copy in pixels (<= buf_w)
 * @param copy_h height of the area to copy in pixels
 * @note `map_w - fill_w` is offset to the next line after copy
 */
int lv_gpu_sdrv_g2d_copy(lv_color_t * buf, lv_coord_t buf_stride/*buf_w lq*/, const lv_color_t * map, lv_coord_t /*map_w  qlj*/map_stride,
                             lv_coord_t copy_w, lv_coord_t copy_h)
{
    struct fcopy_t in = {
        .addr = (addr_t)map,
        .width = copy_w,
        .height = copy_h,
        .stride = /*map_w * 4qlj */ map_stride,
    };

    struct fcopy_t out = {
        .addr = (addr_t)buf,
        .width = copy_w,
        .height = copy_h,
        .stride = buf_stride/*buf_w * 4  lq*/,
    };

    hal_g2dlite_fastcopy(G2D, &in, &out);
    //arch_invalidate_cache_range((addr_t)buf, buf_w * 4 * copy_h);
    return 0;
}


int lv_gpu_sdrv_g2d_blend(const lv_area_t * disp_area, lv_color_t * disp_buf,
                            const lv_area_t * draw_area,
                            const lv_area_t * map_area, const lv_color_t * map_buf,
                            lv_coord_t	map_buf_stride/*lq added*/,
                            lv_opa_t opa,
                            const lv_opa_t * mask, lv_draw_mask_res_t mask_res) {
    struct g2dlite_input input;
    memset(&input, 0, sizeof(struct g2dlite_input));

    input.layer_num = 2;
#if LV_COLOR_DEPTH == 32
    int blend_mode_ = 0;
    switch (mask_res) {
        case LV_DRAW_MASK_RES_TRANSP:
        // return 0;
        case LV_DRAW_MASK_RES_FULL_COVER:
        blend_mode_ = BLEND_PIXEL_NONE;
        break;
        case LV_DRAW_MASK_RES_CHANGED:
        blend_mode_ = BLEND_PIXEL_COVERAGE;
        break;
    }
    //arch_clean_cache_range((addr_t)map_buf, lv_area_get_size(map_area) * 4);
    //arch_clean_cache_range((addr_t)disp_buf, lv_area_get_size(disp_area) * 4);
    int map_width = lv_area_get_width(map_area);
    //int map_height = lv_area_get_height(map_area);
    int disp_width = lv_area_get_width(disp_area);
    //int disp_height = lv_area_get_height(disp_area);
    int draw_width = lv_area_get_width(draw_area);
    int draw_height = lv_area_get_height(draw_area);
    //uint32_t start  = lv_tick_get();

    for (int i = 0; i < input.layer_num; i++) {
        struct g2dlite_input_cfg  *l = &input.layer[i];
        l->layer_en = 1;
        l->layer = i;
        l->fmt = COLOR_ARGB8888;
        l->zorder = i;

        l->ckey.en = 0;
        l->blend = blend_mode_;
        l->alpha = opa;

        if (i == 0) {
            l->blend = BLEND_PIXEL_NONE;
            l->addr[0] = (unsigned long) disp_buf;
            l->src.x = draw_area->x1;
            l->src.y = draw_area->y1;
            l->src.w = lv_area_get_width(draw_area);
            l->src.h = lv_area_get_height(draw_area);
            l->src_stride[0] = disp_width * 4;

            l->dst.x = 0;// canvas :output x y
            l->dst.y = 0;
            l->dst.w = l->src.w;
            l->dst.h = l->src.h;

        } else {
            l->blend = blend_mode_;
            l->addr[0] = (unsigned long)map_buf;
            l->src.x = map_area->x1;
            l->src.y = map_area->y1;
            l->src.w = lv_area_get_width(map_area);
            l->src.h = lv_area_get_height(map_area);

            l->dst.x = 0;
            l->dst.y = 0;
            l->dst.w = l->src.w;
            l->dst.h = l->src.h;
            l->src_stride[0] = map_buf_stride;// lq map_width * 4;
        }
    }

    input.output.width = draw_width;
    input.output.height = draw_height;
    input.output.fmt = COLOR_ARGB8888;
    input.output.addr[0] = (unsigned long)(disp_buf+(draw_area->y1)*disp_width+(draw_area->x1));
    input.output.stride[0] = disp_width * 4;
    input.output.rotation = 0;
    hal_g2dlite_blend(G2D, &input);
    //LV_LOG_WARN("blend  %d\n", lv_tick_get() - start);

    #endif
    return 0;
}
int lv_gpu_sdrv_g2d_blend_mask(const lv_area_t * disp_area, lv_color_t * disp_buf,
                             const lv_area_t * draw_area,
                             const lv_area_t * map_area,  const lv_color_t * map_buf, lv_coord_t	map_buf_stride/*lq added*/,
                             lv_opa_t opa,
                             const lv_opa_t * mask,lv_coord_t	mask_buf_stride/*lq added*/,
                             lv_area_t * mask_area, lv_draw_mask_res_t mask_res,lv_color_t bgcfg_color,bool bgcfg_en) {
    struct g2dlite_input input;
    memset(&input, 0, sizeof(struct g2dlite_input));

    input.layer_num = 1;
    input.pd_info.en = 1;
    input.pd_info.zorder = 0;
    input.pd_info.mode = DST_IN;
    input.pd_info.alpha_need = 1;

#if LV_COLOR_DEPTH == 32
    int blend_mode_ = 0;
    switch (mask_res) {
        case LV_DRAW_MASK_RES_TRANSP:
        case LV_DRAW_MASK_RES_FULL_COVER:
        blend_mode_ = BLEND_PIXEL_NONE;
        break;
        case LV_DRAW_MASK_RES_CHANGED:
        blend_mode_ = BLEND_PIXEL_COVERAGE;
        break;
    }
    //arch_clean_cache_range((addr_t)map_buf, lv_area_get_size(map_area) * 4);
    //arch_clean_cache_range((addr_t)disp_buf, lv_area_get_size(disp_area) * 4);
    int map_width = lv_area_get_width(map_area);
    int map_height = lv_area_get_height(map_area);
     int disp_width = lv_area_get_width(disp_area);
    int disp_height = lv_area_get_height(disp_area);
    int draw_width = lv_area_get_width(draw_area);
    int draw_height = lv_area_get_height(draw_area);
    //uint32_t start  = lv_tick_get();

    struct g2dlite_input_cfg  *l = &input.layer[0];
    l->layer_en = 1;
    l->layer = 0;
    l->fmt = COLOR_ARGB8888;
    l->zorder = 0;
    l->ckey.en = 0;
    l->blend = blend_mode_;
    l->alpha = opa;
    l->pd_type = PD_DST;

    l->addr[0] = (unsigned long)map_buf;
    l->src.x = map_area->x1;
    l->src.y = map_area->y1;
    l->src.w = map_width;
    l->src.h = map_height;
    l->src_stride[0] = map_buf_stride;

    l->dst.x = 0;// canvas :output x y
    l->dst.y = 0;
    l->dst.w = l->src.w;
    l->dst.h = l->src.h;

    struct g2dlite_bg_cfg bgcfg;
    memset(&bgcfg, 0, sizeof(struct g2dlite_bg_cfg));
    bgcfg.en = bgcfg_en;
    bgcfg.color = bgcfg_color.full;
    bgcfg.g_alpha = 255;
    bgcfg.aaddr = (addr_t)mask;
    bgcfg.bpa = 8;
    bgcfg.pd_type = PD_SRC;
    bgcfg.astride = mask_buf_stride;//draw_width;lq
    bgcfg.zorder = 1;
    printf("map_buf_stride:%d,mask_buf_stride:%d, map_width = %d, map_height = %d\n", map_buf_stride, mask_buf_stride, map_width, map_height);

    input.output.width = draw_width;
    input.output.height = draw_height;
    input.output.fmt = COLOR_ARGB8888;
    input.output.addr[0] = (unsigned long)(disp_buf+(draw_area->y1)*disp_width+(draw_area->x1));
    input.output.stride[0] = disp_width * 4;
    input.output.rotation = 0;
    input.bg_layer = bgcfg;
    hal_g2dlite_blend(G2D, &input);
    //LV_LOG_WARN("blend  %d\n", lv_tick_get() - start);

    #endif
    return 0;
}

//layer 1 do not support scale !!!!!!!!!!!!!!!!!!!!!!!!!!!!
int lv_gpu_sdrv_g2d_blend_scale(const lv_area_t * disp_area, lv_color_t * disp_buf,
                            const lv_area_t * draw_area,
                            const lv_area_t * map_area, const lv_color_t * map_buf,
                            lv_coord_t	map_buf_stride/*lq added*/,
                            const lv_area_t * scale_map_area, lv_opa_t opa,
                            const lv_opa_t * mask, lv_draw_mask_res_t mask_res) {
    struct g2dlite_input input;
    memset(&input, 0, sizeof(struct g2dlite_input));

    input.layer_num = 2;
#if LV_COLOR_DEPTH == 32
    int blend_mode_ = 0;
    switch (mask_res) {
        case LV_DRAW_MASK_RES_TRANSP:
        // return 0;
        case LV_DRAW_MASK_RES_FULL_COVER:
        blend_mode_ = BLEND_PIXEL_NONE;
        break;
        case LV_DRAW_MASK_RES_CHANGED:
        blend_mode_ = BLEND_PIXEL_COVERAGE;
        break;
    }
    //arch_clean_cache_range((addr_t)map_buf, lv_area_get_size(map_area) * 4);
    //arch_clean_cache_range((addr_t)disp_buf, lv_area_get_size(disp_area) * 4);
 //   int map_width = lv_area_get_width(map_area);
    //int map_height = lv_area_get_height(map_area);
    int disp_width = lv_area_get_width(disp_area);
    //int disp_height = lv_area_get_height(disp_area);
    int draw_width = lv_area_get_width(draw_area);
    int draw_height = lv_area_get_height(draw_area);
    //uint32_t start  = lv_tick_get();

    for (int i = 0; i < input.layer_num; i++) {
        struct g2dlite_input_cfg  *l = &input.layer[i];
        l->layer_en = 1;
        l->layer = i;
        l->fmt = COLOR_ARGB8888;
        l->zorder = i;

        l->ckey.en = 0;
        l->blend = blend_mode_;
        l->alpha = opa;

        if (i == 1) {
            l->zorder = 0;
            l->blend = BLEND_PIXEL_NONE;
            l->addr[0] = (unsigned long) disp_buf;
            l->src.x = draw_area->x1;
            l->src.y = draw_area->y1;
            l->src.w = lv_area_get_width(draw_area);
            l->src.h = lv_area_get_height(draw_area);
            l->src_stride[0] = disp_width * 4;

            l->dst.x = 0;// canvas :output x y
            l->dst.y = 0;
            l->dst.w = l->src.w;
            l->dst.h = l->src.h;

        } else {
            l->zorder = 1;
            l->blend = BLEND_PIXEL_COVERAGE;
            l->addr[0] = (unsigned long) map_buf;
            l->src.x = map_area->x1;
            l->src.y = map_area->y1;
            l->src.w = lv_area_get_width(map_area);
            l->src.h = lv_area_get_height(map_area);
            l->src_stride[0] = map_buf_stride;// lq map_width * 4;
            l->dst.x = 0;
            l->dst.y = 0;
            l->dst.w = lv_area_get_width(scale_map_area);
            l->dst.h = lv_area_get_height(scale_map_area);
        }
    }

    input.output.width = draw_width;
    input.output.height = draw_height;
    input.output.fmt = COLOR_ARGB8888;
    input.output.addr[0] = (unsigned long)(disp_buf+(draw_area->y1)*disp_width+(draw_area->x1));
    input.output.stride[0] = disp_width * 4;
    input.output.rotation = 0;
    hal_g2dlite_blend(G2D, &input);
    //LV_LOG_WARN("blend  %d\n", lv_tick_get() - start);

    #endif
    return 0;
}


/**********************
 *   STATIC FUNCTIONS
 **********************/

static void invalidate_cache(void* color_p, size_t size)
{
    arch_clean_invalidate_cache_range((addr_t)color_p, size);
}
#endif


/************************************new define*********************************************/

static int LV_TransformFmtType(LV_GPU_Format fmtType, HAL_Format *outFmt)
{
    switch (fmtType) {
        case HAL_FMT_A8:
            *outFmt = LV_FMT_A8;
            break;
        case HAL_FMT_RGB565:
            *outFmt = LV_FMT_RGB565;
            break;
        case HAL_FMT_ARGB4444:
            *outFmt = LV_FMT_ARGB4444;
            break;
        case HAL_FMT_ARGB1555:
            *outFmt = LV_FMT_ARGB1555;
            break;
        case HAL_FMT_RGB888:
            *outFmt = LV_FMT_RGB888;
            break;
        case HAL_FMT_ARGB8888:
            *outFmt = LV_FMT_ARGB8888;
            break;
        default:
            LV_LOG_ERROR("error [dma2d][%s][%d]:can't support this fmt:%d!\r\n", __FUNCTION__, __LINE__, fmtType);
            return HAL_GPU_ERR_PARAM;
    }

    return HAL_SUCCESS;
}

static int LV_TransformBlendMode(lv_draw_mask_res_t maskRes, HAL_G2DLITE_BlendMode *blendMode)
{
    switch (maskRes) {
        case LV_DRAW_MASK_RES_TRANSP:
        case LV_DRAW_MASK_RES_FULL_COVER:
            *blendMode = BLEND_PIXEL_NONE;
        break;
        case LV_DRAW_MASK_RES_CHANGED:
            *blendMode = BLEND_PIXEL_COVERAGE;
        break;
        default:
            LV_LOG_ERROR("error [dma2d][%s][%d]:can't support this maskRes:%d!\r\n", __FUNCTION__, __LINE__, maskRes);
            return HAL_GPU_ERR_PARAM;
    }
    return HAL_SUCCESS;
}

int LV_GPU_G2DLITE_Init(void)
{
    return HAL_G2DLITE_Init();
}

int LV_GPU_G2DLITE_Deinit(void)
{
    return HAL_G2DLITE_Deinit();
}


int LV_GPU_G2DLITE_FillRect(const LV_GPU_FillRectInfo *info)
{
    LV_CHECK_NULLPTR_RETURN(info, HAL_G2DLITE_NULL_PTR);
    LV_CHECK_AREA_RETURN(info->area, HAL_G2DLITE_ERR_PARAM);
    HAL_FillRectInfo fillInfo = {0};
    fillInfo.buf = (uint8_t *)info->buf;
    fillInfo.color = info->color.full;
    int ret = LV_TransformFmtType(info->fmt, &fillInfo.fmt);
    LV_CHECK_RETURN(ret, "transform fmt type");
    fillInfo.stride = info->stride;
    fillInfo.dstRect.x = info->area.x1;
    fillInfo.dstRect.y = info->area.y1;
    fillInfo.dstRect.w = lv_area_get_width(&info->area);
    fillInfo.dstRect.h = lv_area_get_height(&info->area);

    return HAL_G2DLITE_FillRect(&fillInfo);
}

int LV_GPU_G2DLITE_FillMask(LV_GPU_FillMaskInfo *info)
{
    LV_CHECK_NULLPTR_RETURN(info, HAL_G2DLITE_NULL_PTR);
    LV_CHECK_AREA_RETURN(info->output.area, HAL_G2DLITE_ERR_PARAM);

    HAL_G2DLITE_FillMaskInfo fillMask = {0};
    fillMask.maskInfo.buf = (uint8_t *)info->maskInfo.buf;
    fillMask.maskInfo.color = info->maskInfo.color.full;
    fillMask.maskInfo.stride = info->maskInfo.stride;

    fillMask.output.buf = (uint8_t *)info->output.buf;
    fillMask.output.stride = info->output.stride;
    int ret = LV_TransformFmtType(info->output.fmt, &fillMask.output.fmt);
    LV_CHECK_RETURN(ret, "transform fmt type");
    fillMask.output.dstRect.x = info->output.area.x1;
    fillMask.output.dstRect.y = info->output.area.y1;
    fillMask.output.dstRect.w = lv_area_get_width(&info->output.area);
    fillMask.output.dstRect.h = lv_area_get_height(&info->output.area);

    return HAL_G2DLITE_FillMask(&fillMask);
}

int LV_GPU_G2DLITE_BlendImg(const LV_GPU_BlendImgInfo *info)
{
    LV_CHECK_NULLPTR_RETURN(info, HAL_G2DLITE_NULL_PTR);
    LV_CHECK_AREA_RETURN(info->output.area, HAL_G2DLITE_ERR_PARAM);
    LV_CHECK_AREA_RETURN(info->fgSurface.srcArea, HAL_G2DLITE_ERR_PARAM);
    LV_CHECK_AREA_RETURN(info->fgSurface.dstArea, HAL_G2DLITE_ERR_PARAM);
    LV_CHECK_AREA_RETURN(info->bgSurface.srcArea, HAL_G2DLITE_ERR_PARAM);
    LV_CHECK_AREA_RETURN(info->bgSurface.dstArea, HAL_G2DLITE_ERR_PARAM);

    HAL_G2DLITE_BlendImgInfo blendImg = {0};
    blendImg.bgSurface.alpha = info->bgSurface.alpha;
    int ret = LV_TransformBlendMode(info->bgSurface.maskRes, &blendImg.bgSurface.blendMode);
    LV_CHECK_RETURN(ret, "transform blend mode");
    blendImg.bgSurface.buf = (uint8_t *)info->bgSurface.buf;
    ret = LV_TransformFmtType(info->bgSurface.fmt, &blendImg.bgSurface.fmt);
    LV_CHECK_RETURN(ret, "transform fmt type");
    blendImg.bgSurface.stride = info->bgSurface.stride;
    blendImg.bgSurface.srcRect.x = info->bgSurface.srcArea.x1;
    blendImg.bgSurface.srcRect.y = info->bgSurface.srcArea.y1;
    blendImg.bgSurface.srcRect.w = lv_area_get_width(&info->bgSurface.srcArea);
    blendImg.bgSurface.srcRect.h = lv_area_get_height(&info->bgSurface.srcArea);
    blendImg.bgSurface.dstRect.x = info->bgSurface.dstArea.x1;
    blendImg.bgSurface.dstRect.y = info->bgSurface.dstArea.y1;
    blendImg.bgSurface.dstRect.w = lv_area_get_width(&info->bgSurface.dstArea);
    blendImg.bgSurface.dstRect.h = lv_area_get_height(&info->bgSurface.dstArea);

    blendImg.fgSurface.alpha = info->bgSurface.alpha;
    ret = LV_TransformBlendMode(info->fgSurface.maskRes, &blendImg.fgSurface.blendMode);
    LV_CHECK_RETURN(ret, "transform blend mode");
    blendImg.fgSurface.buf = (uint8_t *)info->fgSurface.buf;
    ret = LV_TransformFmtType(info->fgSurface.fmt, &blendImg.fgSurface.fmt);
    LV_CHECK_RETURN(ret, "transform fmt type");
    blendImg.fgSurface.stride = info->fgSurface.stride;
    blendImg.fgSurface.srcRect.x = info->fgSurface.srcArea.x1;
    blendImg.fgSurface.srcRect.y = info->fgSurface.srcArea.y1;
    blendImg.fgSurface.srcRect.w = lv_area_get_width(&info->fgSurface.srcArea);
    blendImg.fgSurface.srcRect.h = lv_area_get_height(&info->fgSurface.srcArea);
    blendImg.fgSurface.dstRect.x = info->fgSurface.dstArea.x1;
    blendImg.fgSurface.dstRect.y = info->fgSurface.dstArea.y1;
    blendImg.fgSurface.dstRect.w = lv_area_get_width(&info->fgSurface.dstArea);
    blendImg.fgSurface.dstRect.h = lv_area_get_height(&info->fgSurface.dstArea);

    blendImg.output.buf = (uint8_t *)info->output.buf;
    ret = LV_TransformFmtType(info->output.fmt, &blendImg.output.fmt);
    LV_CHECK_RETURN(ret, "transform fmt type");
    blendImg.output.stride = info->output.stride;
    blendImg.output.dstRect.x = info->output.area.x1;
    blendImg.output.dstRect.y = info->output.area.y1;
    blendImg.output.dstRect.w = lv_area_get_width(&info->output.area);
    blendImg.output.dstRect.h = lv_area_get_height(&info->output.area);

    return HAL_G2DLITE_BlendImg(&blendImg);
}

int LV_GPU_G2DLITE_BlendRleImg(const LV_GPU_BlendImgInfo *info, LV_GPU_RleInfo *rleInfo)
{
    LV_CHECK_NULLPTR_RETURN(info, HAL_G2DLITE_NULL_PTR);
    LV_CHECK_NULLPTR_RETURN(rleInfo, HAL_G2DLITE_NULL_PTR);
    LV_CHECK_AREA_RETURN(info->output.area, HAL_G2DLITE_ERR_PARAM);
    LV_CHECK_AREA_RETURN(info->fgSurface.srcArea, HAL_G2DLITE_ERR_PARAM);
    LV_CHECK_AREA_RETURN(info->fgSurface.dstArea, HAL_G2DLITE_ERR_PARAM);
    LV_CHECK_AREA_RETURN(info->bgSurface.srcArea, HAL_G2DLITE_ERR_PARAM);
    LV_CHECK_AREA_RETURN(info->bgSurface.dstArea, HAL_G2DLITE_ERR_PARAM);

    HAL_G2DLITE_BlendImgInfo blendImg = {0};
    blendImg.bgSurface.alpha = info->bgSurface.alpha;
    int ret = LV_TransformBlendMode(info->bgSurface.maskRes, &blendImg.bgSurface.blendMode);
    LV_CHECK_RETURN(ret, "transform blend mode");
    blendImg.bgSurface.buf = (uint8_t *)info->bgSurface.buf;
    ret = LV_TransformFmtType(info->bgSurface.fmt, &blendImg.bgSurface.fmt);
    LV_CHECK_RETURN(ret, "transform fmt type");
    blendImg.bgSurface.stride = info->bgSurface.stride;
    blendImg.bgSurface.srcRect.x = info->bgSurface.srcArea.x1;
    blendImg.bgSurface.srcRect.y = info->bgSurface.srcArea.y1;
    blendImg.bgSurface.srcRect.w = lv_area_get_width(&info->bgSurface.srcArea);
    blendImg.bgSurface.srcRect.h = lv_area_get_height(&info->bgSurface.srcArea);
    blendImg.bgSurface.dstRect.x = info->bgSurface.dstArea.x1;
    blendImg.bgSurface.dstRect.y = info->bgSurface.dstArea.y1;
    blendImg.bgSurface.dstRect.w = lv_area_get_width(&info->bgSurface.dstArea);
    blendImg.bgSurface.dstRect.h = lv_area_get_height(&info->bgSurface.dstArea);

    blendImg.fgSurface.alpha = info->fgSurface.alpha;
    ret = LV_TransformBlendMode(info->fgSurface.maskRes, &blendImg.fgSurface.blendMode);
    LV_CHECK_RETURN(ret, "transform blend mode");
    blendImg.fgSurface.buf = (uint8_t *)info->fgSurface.buf;
    ret = LV_TransformFmtType(info->fgSurface.fmt, &blendImg.fgSurface.fmt);
    LV_CHECK_RETURN(ret, "transform fmt type");
    blendImg.fgSurface.stride = info->fgSurface.stride;
    blendImg.fgSurface.srcRect.x = info->fgSurface.srcArea.x1;
    blendImg.fgSurface.srcRect.y = info->fgSurface.srcArea.y1;
    blendImg.fgSurface.srcRect.w = lv_area_get_width(&info->fgSurface.srcArea);
    blendImg.fgSurface.srcRect.h = lv_area_get_height(&info->fgSurface.srcArea);
    blendImg.fgSurface.dstRect.x = info->fgSurface.dstArea.x1;
    blendImg.fgSurface.dstRect.y = info->fgSurface.dstArea.y1;
    blendImg.fgSurface.dstRect.w = lv_area_get_width(&info->fgSurface.dstArea);
    blendImg.fgSurface.dstRect.h = lv_area_get_height(&info->fgSurface.dstArea);

    blendImg.output.buf = (uint8_t *)info->output.buf;
    ret = LV_TransformFmtType(info->output.fmt, &blendImg.output.fmt);
    LV_CHECK_RETURN(ret, "transform fmt type");
    blendImg.output.stride = info->output.stride;
    blendImg.output.dstRect.x = info->output.area.x1;
    blendImg.output.dstRect.y = info->output.area.y1;
    blendImg.output.dstRect.w = lv_area_get_width(&info->output.area);
    blendImg.output.dstRect.h = lv_area_get_height(&info->output.area);

    HAL_G2DLITE_RleInfo rle;
    rle.rleType = (HAL_G2DLITE_RleSurfaceType)rleInfo->rleType;

    return HAL_G2DLITE_BlendRleImg(&blendImg, &rle);
}


int LV_GPU_G2DLITE_BlendMask(const LV_GPU_BlendMaskInfo *info)
{
    LV_CHECK_NULLPTR_RETURN(info, HAL_G2DLITE_NULL_PTR);
    LV_CHECK_AREA_RETURN(info->output.area, HAL_G2DLITE_ERR_PARAM);
    LV_CHECK_AREA_RETURN(info->surfaceInfo.srcArea, HAL_G2DLITE_ERR_PARAM);
    LV_CHECK_AREA_RETURN(info->surfaceInfo.dstArea, HAL_G2DLITE_ERR_PARAM);

    HAL_G2DLITE_BlendMaskInfo blendMask = {0};
    blendMask.maskInfo.buf = (uint8_t *)info->maskInfo.buf;
    blendMask.maskInfo.color = info->maskInfo.color.full;
    blendMask.maskInfo.stride = info->maskInfo.stride;

    blendMask.surfaceInfo.alpha = info->surfaceInfo.alpha;
    int ret = LV_TransformBlendMode(info->surfaceInfo.maskRes, &blendMask.surfaceInfo.blendMode);
    LV_CHECK_RETURN(ret, "transform blend mode");
    blendMask.surfaceInfo.buf = (uint8_t *)info->surfaceInfo.buf;
    ret = LV_TransformFmtType(info->surfaceInfo.fmt, &blendMask.surfaceInfo.fmt);
    LV_CHECK_RETURN(ret, "transform fmt type");
    blendMask.surfaceInfo.stride = info->surfaceInfo.stride;
    blendMask.surfaceInfo.srcRect.x = info->surfaceInfo.srcArea.x1;
    blendMask.surfaceInfo.srcRect.y = info->surfaceInfo.srcArea.y1;
    blendMask.surfaceInfo.srcRect.w = lv_area_get_width(&info->surfaceInfo.srcArea);
    blendMask.surfaceInfo.srcRect.h = lv_area_get_height(&info->surfaceInfo.srcArea);
    blendMask.surfaceInfo.dstRect.x = info->surfaceInfo.dstArea.x1;
    blendMask.surfaceInfo.dstRect.y = info->surfaceInfo.dstArea.y1;
    blendMask.surfaceInfo.dstRect.w = lv_area_get_width(&info->surfaceInfo.dstArea);
    blendMask.surfaceInfo.dstRect.h = lv_area_get_height(&info->surfaceInfo.dstArea);

    blendMask.output.buf = (uint8_t *)info->output.buf;
    ret = LV_TransformFmtType(info->output.fmt, &blendMask.output.fmt);
    LV_CHECK_RETURN(ret, "transform fmt type");
    blendMask.output.stride = info->output.stride;
    blendMask.output.dstRect.x = info->output.area.x1;
    blendMask.output.dstRect.y = info->output.area.y1;
    blendMask.output.dstRect.w = lv_area_get_width(&info->output.area);
    blendMask.output.dstRect.h = lv_area_get_height(&info->output.area);
    return HAL_G2DLITE_BlendMask(&blendMask);
}

int LV_GPU_G2DLITE_FastCopy(const LV_GPU_FastCopyInfo *in, LV_GPU_FastCopyInfo *out)
{
    LV_CHECK_NULLPTR_RETURN(in, HAL_G2DLITE_NULL_PTR);
    LV_CHECK_NULLPTR_RETURN(out, HAL_G2DLITE_NULL_PTR);
    LV_CHECK_AREA_RETURN(in->area, HAL_G2DLITE_ERR_PARAM);
    LV_CHECK_AREA_RETURN(out->area, HAL_G2DLITE_ERR_PARAM);

    HAL_G2DLITE_FastCopyInfo src = {0};
    HAL_G2DLITE_FastCopyInfo dst = {0};
    int ret = LV_TransformFmtType(in->fmt, &src.fmt);
    LV_CHECK_RETURN(ret, "transform fmt type");
    src.buf = (uint8_t *)in->buf;
    src.rect.x = in->area.x1;
    src.rect.y = in->area.y1;
    src.rect.w = lv_area_get_width(&in->area);
    src.rect.h = lv_area_get_height(&in->area);
    src.stride = in->stride;

    ret = LV_TransformFmtType(out->fmt, &dst.fmt);
    LV_CHECK_RETURN(ret, "transform fmt type");
    dst.buf = (uint8_t *)out->buf;
    dst.rect.x = out->area.x1;
    dst.rect.y = out->area.y1;
    dst.rect.w = lv_area_get_width(&out->area);
    dst.rect.h = lv_area_get_height(&out->area);
    dst.stride = out->stride;

    return HAL_G2DLITE_FastCopy(&src, &dst);
}

int LV_GPU_ASW_FillRect(const LV_GPU_FillRectInfo *info)
{
    LV_CHECK_NULLPTR_RETURN(info, HAL_ASW_NULL_PTR);
    LV_CHECK_AREA_RETURN(info->area, HAL_ASW_ERR_PARAM);
    HAL_FillRectInfo fillInfo = {0};
    fillInfo.buf = (uint8_t *)info->buf;
    fillInfo.color = info->color.full;
    int ret = LV_TransformFmtType(info->fmt, &fillInfo.fmt);
    LV_CHECK_RETURN(ret, "transform fmt type");
    fillInfo.stride = info->stride;
    fillInfo.dstRect.x = info->area.x1;
    fillInfo.dstRect.y = info->area.y1;
    fillInfo.dstRect.w = lv_area_get_width(&info->area);
    fillInfo.dstRect.h = lv_area_get_height(&info->area);

    return HAL_ASW_FillRect(&fillInfo);
}

int LV_GPU_ASW_FillTriangle(const LV_GPU_FillRatriaInfo *info)
{
    LV_CHECK_NULLPTR_RETURN(info, HAL_ASW_NULL_PTR);
    LV_CHECK_AREA_RETURN(info->area, HAL_ASW_ERR_PARAM);

    HAL_FillRatriaInfo fillInfo = {0};
    fillInfo.type = (HAL_TRIANLE_Type)info->type;
    int ret = LV_TransformFmtType(info->fmt, &fillInfo.fmt);
    LV_CHECK_RETURN(ret, "transform fmt type");
    fillInfo.color = info->color.full;
    fillInfo.dstRect.w = lv_area_get_width(&info->area);
    fillInfo.dstRect.h = lv_area_get_height(&info->area);
    fillInfo.dstRect.x = info->area.x1;
    fillInfo.dstRect.y = info->area.y1;
    fillInfo.buf = (uint8_t *)info->buf;
    fillInfo.stride = info->stride;
    return HAL_ASW_FillTriangle(&fillInfo);
}

void *LV_GPU_DISP_GetHandle(LV_GPU_ScreenType type)
{
    if (type != LV_SCREEN_TYPE_CLUSTER) {
        LV_LOG_ERROR("error [dma2d]: can't support this type:%d!\n", type);
        return NULL;
    }
    return HAL_DISP_GetHandle(type);
}
int LV_GPU_DISP_Post(void *handle, const LV_GPU_DisplayInfo *info)
{
    LV_CHECK_NULLPTR_RETURN(info, HAL_ASW_NULL_PTR);
    LV_CHECK_NULLPTR_RETURN(handle, HAL_ASW_NULL_PTR);
    if (info->winCnt > LV_GPU_WIN_MAX_CNT) {
        LV_LOG_ERROR("error [dma2d]: info->winCnt[%u] No more than %d!\r\n", info->winCnt, LV_GPU_WIN_MAX_CNT);
        return HAL_DISP_ERR_PARAM;
    }
    HAL_DISP_Info halInfo;
    memset(&halInfo, 0, sizeof(halInfo));
    halInfo.winCnt = info->winCnt;
    for (int i = 0; i < info->winCnt; i++) {
        const LV_GPU_WindowInfo *winInfo = &info->winInfo[i];
        HAL_DISP_WindowInfo *halWinInfo = &halInfo.winInfo[i];
        LV_CHECK_AREA_RETURN(winInfo->src, HAL_DISP_ERR_PARAM);
        LV_CHECK_AREA_RETURN(winInfo->dst, HAL_DISP_ERR_PARAM);
        halWinInfo->id = winInfo->id;
        halWinInfo->dirty = winInfo->dirty;
        halWinInfo->en = winInfo->en;
        int ret = LV_TransformFmtType(winInfo->fmt, &halWinInfo->fmt);
        LV_CHECK_RETURN(ret, "transform fmt type");
        halWinInfo->src.x = winInfo->src.x1;
        halWinInfo->src.y = winInfo->src.y1;
        halWinInfo->src.w = lv_area_get_width(&winInfo->src);
        halWinInfo->src.h = lv_area_get_height(&winInfo->src);
        for (int j = 0; j < HAL_DISP_YUVA_ADDR_CNT; j++) {
            halWinInfo->addr[j] = (uint8_t *)winInfo->addr[j];
            halWinInfo->srcStride[j] = winInfo->srcStride[j];
        }
        halWinInfo->dst.x = winInfo->dst.x1;
        halWinInfo->dst.y = winInfo->dst.y1;
        halWinInfo->dst.w = lv_area_get_width(&winInfo->dst);;
        halWinInfo->dst.h = lv_area_get_height(&winInfo->dst);
        halWinInfo->zOrder = winInfo->zOrder;
        halWinInfo->enAlpha = winInfo->enAlpha;
        halWinInfo->alpha = winInfo->alpha;
        halWinInfo->enCkey = winInfo->enCkey;
        halWinInfo->security = winInfo->security;
    }

    return HAL_DISP_Post(handle, &halInfo);
}

int LV_GPU_Rotate(const LV_GPU_RotateInfo *info)
{
    LV_CHECK_NULLPTR_RETURN(info, HAL_ASW_NULL_PTR);
    LV_CHECK_AREA_RETURN(info->srcArea, HAL_ASW_ERR_PARAM);

    HAL_RotateInfo rotateInfo = {0};
    rotateInfo.srcRect.x = info->srcArea.x1;
    rotateInfo.srcRect.y = info->srcArea.y1;
    rotateInfo.srcRect.w = lv_area_get_width(&info->srcArea);
    rotateInfo.srcRect.h = lv_area_get_height(&info->srcArea);
    rotateInfo.srcBuf = (uint8_t *)info->srcBuf;
    int ret = LV_TransformFmtType(info->srcFmt, &rotateInfo.srcFmt);
    LV_CHECK_RETURN(ret, "transform fmt type");
    rotateInfo.srcStride = info->srcStride;

    rotateInfo.dstBuf = (uint8_t *)info->dstBuf;
    ret = LV_TransformFmtType(info->dstFmt, &rotateInfo.dstFmt);
    LV_CHECK_RETURN(ret, "transform fmt type");
    rotateInfo.dstWidth = info->dstWidth;
    rotateInfo.dstHeight = info->dstHeight;
    rotateInfo.dstStride = info->dstStride;

    rotateInfo.angle = info->angle;
    rotateInfo.bgColor = info->bgColor.full;

    return HAL_GPU_Rotate(&rotateInfo);
}

int LV_GPU_CleanCacheRange(const LV_GPU_CacheInfo *info)
{
    LV_CHECK_NULLPTR_RETURN(info, HAL_ASW_NULL_PTR);
    LV_CHECK_AREA_RETURN(info->area, HAL_ASW_ERR_PARAM);

    HAL_SYS_CacheInfo cacheInfo = {0};
    cacheInfo.buf = (uint8_t *)info->buf;
    int ret = LV_TransformFmtType(info->fmt, &cacheInfo.fmt);
    LV_CHECK_RETURN(ret, "transform fmt type");
    cacheInfo.rect.x = info->area.x1;
    cacheInfo.rect.y = info->area.y1;
    cacheInfo.rect.w = lv_area_get_width(&info->area);
    cacheInfo.rect.h = lv_area_get_height(&info->area);
    cacheInfo.stride = info->stride;

    return HAL_SYS_CleanCacheRange(&cacheInfo);
}

int LV_GPU_InvalidateCacheRange(const LV_GPU_CacheInfo *info)
{
    LV_CHECK_NULLPTR_RETURN(info, HAL_ASW_NULL_PTR);
    LV_CHECK_AREA_RETURN(info->area, HAL_ASW_ERR_PARAM);

    HAL_SYS_CacheInfo cacheInfo = {0};
    cacheInfo.buf = (uint8_t *)info->buf;
    int ret = LV_TransformFmtType(info->fmt, &cacheInfo.fmt);
    LV_CHECK_RETURN(ret, "transform fmt type");
    cacheInfo.rect.x = info->area.x1;
    cacheInfo.rect.y = info->area.y1;
    cacheInfo.rect.w = lv_area_get_width(&info->area);
    cacheInfo.rect.h = lv_area_get_height(&info->area);
    cacheInfo.stride = info->stride;

    return HAL_SYS_InvalidateCacheRange(&cacheInfo);
}

int LV_GPU_CleanInvalidateCacheRange(const LV_GPU_CacheInfo *info)
{
    LV_CHECK_NULLPTR_RETURN(info, HAL_ASW_NULL_PTR);
    LV_CHECK_AREA_RETURN(info->area, HAL_ASW_ERR_PARAM);

    HAL_SYS_CacheInfo cacheInfo = {0};
    cacheInfo.buf = (uint8_t *)info->buf;
    int ret = LV_TransformFmtType(info->fmt, &cacheInfo.fmt);
    LV_CHECK_RETURN(ret, "transform fmt type");
    cacheInfo.rect.x = info->area.x1;
    cacheInfo.rect.y = info->area.y1;
    cacheInfo.rect.w = lv_area_get_width(&info->area);
    cacheInfo.rect.h = lv_area_get_height(&info->area);
    cacheInfo.stride = info->stride;

    return HAL_SYS_CleanInvalidateCacheRange(&cacheInfo);
}

void LV_GPU_CleanInvalidateCacheAll(void)
{
    HAL_SYS_CleanInvalidateCacheAll();
}
