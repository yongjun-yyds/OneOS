CSRCS += lv_port_indev.c

DEPPATH += --dep-path $(LVGL_DIR)/$(LVGL_SDRV_DIR_NAME)/input
VPATH += :$(LVGL_DIR)/$(LVGL_SDRV_DIR_NAME)/input
CFLAGS += "-I$(LVGL_DIR)/$(LVGL_SDRV_DIR_NAME)/input"