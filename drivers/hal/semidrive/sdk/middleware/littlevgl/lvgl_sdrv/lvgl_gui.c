/**
 * @file lv_port_disp_templ.c
 *
 */

/*********************
 *      INCLUDES
 *********************/
#include "lvgl_gui.h"
#include "lvgl_disp_drv.h"
#if CONFIG_LVGL_SDRV_FAT_FS
#include "lv_fs_sdrv.h"
#endif
#if CONFIG_LVGL_SDRV_INPUT
#include "lv_port_indev.h"
#endif

#include <string.h>
//#include <cmsis_os2.h>
#include <board.h>
#include <debug.h>

#if CONFIG_LVGL_SDRV_DECODER_PNG
#include "lv_png.h"
#endif
#if CONFIG_LVGL_SDRV_DECODER_JPG
#include "lv_sjpg.h"
#endif

#include <FreeRTOS.h>
#include <task.h>
void lvgl_refresh_task(lv_task_t * task)
{
    lv_task_handler();
}

void lvgl_mainloop(void)
{
    while(1)
    {
        //ssdk_printf(SSDK_INFO, "mainloop ... %d", osKernelGetTickFreq());
        lv_task_handler();
        vTaskDelay(pdMS_TO_TICKS(50));
    }
}

lv_disp_t *g_registered_display[2] = {0};

lv_disp_t *get_display(int display_id)
{
    return g_registered_display[display_id];
}

void lvgl_init(void)
{
    static int lvgl_inited = 0;

    ssdk_printf(SSDK_INFO, "lvgl_init start\n");
    if (lvgl_inited)
    {
        ssdk_printf(SSDK_INFO, "lvgl_init already！！\n");
        return;
    }

    lv_init();
#if CONFIG_LVGL_SDRV_FAT_FS
    ssdk_printf(SSDK_ALERT, "init lvgl for fat fs \n");
    lv_port_fs_init();
    ssdk_printf(SSDK_ALERT, "init lvgl for fat fs end\n");
#endif
    ssdk_printf(SSDK_ALERT, "init lvgl for disp \n");
    lv_disp_t *disp = lvgl_lcd_display_init();
    g_registered_display[0] = disp;
    _lv_refr_set_disp_refreshing(disp); // lq
    ssdk_printf(SSDK_ALERT, "init lvgl for disp end %p\n", disp);

#if CONFIG_LVGL_SDRV_INPUT
    lv_port_indev_init(get_display(0));
#endif
#if CONFIG_LVGL_SDRV_DECODER_PNG
    lv_png_init();
#endif
#if CONFIG_LVGL_SDRV_DECODER_JPG
    lv_split_jpeg_init();
#endif


    lvgl_inited = 1;
    ssdk_printf(SSDK_ALERT, "lvgl_init end\n");
}
