LOCAL_DIR := $(GET_LOCAL_DIR)
LVGL_DIR := $(GET_LOCAL_DIR)
MODULE := $(LOCAL_DIR)
LVGL_DIR_NAME := lvgl-release-v7
LVGL_SDRV_DIR_NAME := lvgl_sdrv
LVGL_EXAMPLE_DIR_NAME := lv_examples-release-v7

GLOBAL_INCLUDES += \
	$(LVGL_DIR) \
	$(LVGL_DIR)/$(LVGL_DIR_NAME) \
	$(LVGL_DIR)/$(LVGL_SDRV_DIR_NAME) \
	$(LVGL_DIR)/$(LVGL_EXAMPLE_DIR_NAME) \

include $(LVGL_DIR)/$(LVGL_DIR_NAME)/lvgl.mk

include $(LVGL_DIR)/$(LVGL_SDRV_DIR_NAME)/lvgl_sdrv.mk

include $(LVGL_DIR)/$(LVGL_EXAMPLE_DIR_NAME)/lv_examples.mk

include $(MKROOT)/module.mk