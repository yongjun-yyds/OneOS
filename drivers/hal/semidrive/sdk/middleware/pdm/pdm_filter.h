#ifndef PDM_FILTER_H
#define PDM_FILTER_H
#define MAX_CH 4

typedef struct {
    short a0;
    short a1;
    short a2;
    short b0;
    short b1;
    short b2;
    short x1;
    short x2;
    short y1;
    short y2;
} pdm_coeff_t;

/**
 * @brief pdm filter handle
 *
 */
typedef struct {
    int sample_rate;
    int channel;
    pdm_coeff_t bq[MAX_CH];
} pdm_filter_t;

/**
 * @brief
 *
 * @param pdm_filter
 * @param sample_rate
 * @param channel
 * @return int
 */
int pdm_filter_init(pdm_filter_t *pdm_filter, int sample_rate, int channel);

/**
 * @brief
 *
 * @param pdm_filter
 * @param inputPtr
 * @param outputPtr
 * @param framesize
 * @return int
 */
int pdm_filter_process(pdm_filter_t *pdm_filter, void *inputPtr,
                       void *outputPtr, int framesize);
#endif