/*
 * segger_rtl.c
 *
 * Copyright (c) 2022 Semidrive Semiconductor.
 * All rights reserved.
 *
 * Description: SEGGER RunTime Library port
 *
 * Revision History:
 * -----------------
 */

#include <types.h>
#include <param.h>
#include <debug.h>
#include <compiler.h>

#if __SES_ARM
void __aeabi_assert(const char *expr, const char *file, int line)
{
    ssdk_printf(SSDK_ERR, "assert in file:%s expr:%s line:%d\n", file, expr, line);
    PANIC();
}

int __SEGGER_RTL_X_set_time_of_day(const struct timeval *__tp)
{
    return -1;
}

int __SEGGER_RTL_X_get_time_of_day (struct timeval *__tp)
{
    return -1;
}
#endif