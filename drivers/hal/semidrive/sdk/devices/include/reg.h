/*
 * Copyright (c) 2008 Travis Geiselbrecht
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef DEV_REG_H_
#define DEV_REG_H_

#include <stdint.h>

#define REG64(addr) ((volatile uint64_t *)(uintptr_t)(addr))
#define REG32(addr) ((volatile uint32_t *)(uintptr_t)(addr))
#define REG16(addr) ((volatile uint16_t *)(uintptr_t)(addr))
#define REG8(addr) ((volatile uint8_t *)(uintptr_t)(addr))

#define RMWREG64(addr, startbit, width, val) *REG64(addr) = (*REG64(addr) & ~((((uint64_t)1u<<(width)) - 1u) << (startbit))) | ((uint64_t)(val) << (startbit))
#define RMWREG32(addr, startbit, width, val) *REG32(addr) = (*REG32(addr) & ~((((uint32_t)1u<<(width)) - 1u) << (startbit))) | ((uint32_t)(val) << (startbit))
#define RMWREG16(addr, startbit, width, val) *REG16(addr) = (*REG16(addr) & ~((((uint16_t)1u<<(width)) - 1u) << (startbit))) | ((uint16_t)(val) << (startbit))
#define RMWREG8(addr, startbit, width, val) *REG8(addr) = (*REG8(addr) & ~((((uint8_t)1u<<(width)) - 1u) << (startbit))) | ((uint8_t)(val) << (startbit))

#define writeq(v, a) (*REG64(a) = (v))
#define readq(a) (*REG64(a))
#define writel(v, a) (*REG32(a) = (v))
#define readl(a) (*REG32(a))
#define writew(v, a) (*REG16(a) = (v))
#define readw(a) (*REG16(a))
#define writeb(v, a) (*REG8(a) = (v))
#define readb(a) (*REG8(a))

static inline
void clrbits_32(volatile uint32_t *addr, uint32_t clear)
{
    *addr &= ~clear;
}

static inline
void setbits_32(volatile uint32_t *addr, uint32_t set)
{
    *addr |= set;
}

static inline
void clrsetbits_32(volatile uint32_t *addr, uint32_t clear, uint32_t set)
{
    uint32_t temp;

    temp = *addr;
    temp &= ~clear;
    temp |= set;
    *addr = temp;
}

#define CLRBITS_32(addr, clear)         clrbits_32(REG32(addr), clear)
#define SETBITS_32(addr, set)           setbits_32(REG32(addr), set)
#define CLRSETBITS_32(addr, clear, set) clrsetbits_32(REG32(addr), clear, set)


#endif
