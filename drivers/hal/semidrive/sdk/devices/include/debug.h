/*
 * debug.h
 *
 * Copyright (c) 2020 Semidrive Semiconductor.
 * All rights reserved.
 *
 * Description: debug interface.
 *
 * Revision History:
 * -----------------
 */

#ifndef _INCLUDE_DEBUG_H
#define _INCLUDE_DEBUG_H

#include <types.h>
#include <stdio.h>
#include <compiler.h>
#include <param.h>
#include <ctype.h>

/* SSDK debug level */
#define SSDK_EMERG     0  /* System is unusable */
#define SSDK_ALERT     1  /* Action must be taken immediately */
#define SSDK_CRIT      2  /* Critical conditions */
#define SSDK_ERR       3  /* Error conditions */
#define SSDK_WARNING   4  /* Warning conditions */
#define SSDK_NOTICE    5  /* Normal, but significant, condition */
#define SSDK_INFO      6  /* Informational message */
#define SSDK_DEBUG     7  /* Debug-level message */

/* SSDK printf */
#if CONFIG_DEBUG
#if CONFIG_PRINTF_LIB
#include <printf/printf.h>
#else
#define printf(x...)
#endif
#define ssdk_printf(level, x...)    do { if ((level) <= CONFIG_DEBUG_LEVEL) {printf(x); } } while (0)
#define PANIC()         ssdk_panic((const uint8_t *)__FILE__, (int)__LINE__)
#define ASSERT(x)       do { if (!(x)) PANIC(); } while (0)

static void ssdk_panic(const uint8_t *filename, int linenum)
{
    ssdk_printf(SSDK_EMERG, "Assertion failed at file:%s line: %d\r\n",
                filename, linenum);
    __BKPT;
    for(;;);
}

/**
 * @brief hex dump function
 *
 * @param[in] ptr: dump data address
 * @param[in] len: dump data length
 */
void hexdump(const void *ptr, size_t len);

/**
 * @brief hex dump8 function with display address
 *
 * @param[in] ptr: ptr dump data address
 * @param[in] len: len dump data length
 * @param[in] addr: disp_addr display address
 */
void hexdump8_ex(const void *ptr, size_t len, uint64_t addr);

#else
#define ssdk_printf(level, x...)
#define PANIC()
#define ASSERT(x)
#define hexdump(ptr, len)
#define hexdump8_ex(ptr, len, disp_addr)
#endif

#endif
