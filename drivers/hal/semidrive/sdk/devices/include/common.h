/*
 * common.h
 *
 * Copyright (c) 2020 Semidrive Semiconductor.
 * All rights reserved.
 *
 * Description: common interface.
 *
 * Revision History:
 * -----------------
 */

#ifndef COMMON_H
#define COMMON_H

#include "part.h"
#include "reg.h"
#include "regs_base.h"

#if (CONFIG_E3 || CONFIG_D3 || CONFIG_E3L)

#define FUSE0_OFFSET 0x1000

/* Product ID fuse index, starting from address 0x18.
 * 0x18: PRODUCT_MINOR_ID
 * 0x19: PRODUCT_MAJOR_ID
 * 0x1A: [3:0] MASK_MINOR_ID, [7:4] MASK_MAJOR_ID
 */
#define PART_FUSE_INDEX 0x6
#define V_MAJOR_MASK 0x00f00000
#define V_MAJOR_POS 20
#define V_MINOR_MASK 0x000f0000
#define V_MINOR_POS 16

#ifndef ASSEMBLY

#if (CONFIG_E3 || CONFIG_D3)
#define IS_1P1 (sdrv_fuse_get_major_chipid() == 0 && \
                sdrv_fuse_get_minor_chipid() == 1)
#define IS_1P0 (sdrv_fuse_get_major_chipid() == 0 && \
                sdrv_fuse_get_minor_chipid() == 0)
#define IS_P1 (sdrv_fuse_get_minor_chipid() == 1)
#define IS_P0 (sdrv_fuse_get_minor_chipid() == 0)

#elif (CONFIG_E3L)
#define IS_1P1 (sdrv_fuse_get_major_chipid() == 0 && \
                sdrv_fuse_get_minor_chipid() == 2)
#define IS_1P0 (sdrv_fuse_get_major_chipid() == 0 && \
                sdrv_fuse_get_minor_chipid() <= 1)
#define IS_P1 (sdrv_fuse_get_minor_chipid() == 2)
#define IS_P0 (sdrv_fuse_get_minor_chipid() <= 1)

#endif

static inline uint32_t fuse_get_version(void)
{
    return readl(APB_EFUSEC_BASE + FUSE0_OFFSET + PART_FUSE_INDEX * 4);
}

static inline uint8_t sdrv_fuse_get_minor_chipid(void)
{
    uint32_t info = fuse_get_version();
    uint8_t minor_id = (info & V_MINOR_MASK) >> V_MINOR_POS;

    return minor_id;
}

static inline uint8_t sdrv_fuse_get_major_chipid(void)
{
    uint32_t info = fuse_get_version();
    uint8_t major_id = (info & V_MAJOR_MASK) >> V_MAJOR_POS;

    return major_id;
}

#endif /* !ASSEMBLY */

#endif /* CONFIG_E3 || CONFIG_D3 || CONFIG_E3L */

#endif /* !COMMON_H */
