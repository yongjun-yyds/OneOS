/*
 * types.h
 *
 * Copyright (c) 2020 Semidrive Semiconductor.
 * All rights reserved.
 *
 * Description: types interface.
 *
 * Revision History:
 * -----------------
 */

#ifndef DEV_TYPES_H
#define DEV_TYPES_H

#ifndef ASSEMBLY

#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>

typedef uintptr_t addr_t;
typedef uintptr_t vaddr_t;
typedef uintptr_t paddr_t;

#endif

#endif
