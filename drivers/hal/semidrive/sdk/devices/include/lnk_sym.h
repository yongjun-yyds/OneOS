/**
 * @file lnk_sym.h
 * @brief IAR link symbols.
 *
 * @copyright Copyright (c) 2021  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef DEV_LNK_SYM_H__
#define DEV_LNK_SYM_H__

#include <types.h>

#ifdef __ICCARM__

#define __commands_start COMMANDS$$Base
#define __commands_end COMMANDS$$Limit
#define _heap_start HEAP$$Base
#define _heap_end HEAP$$Limit
#define __except_stack_start CSTACK$$Base
#define __cstack_end CSTACK$$Limit
#define __data_start DATA$$Base
#define __data_start_rom DATA$$Base
#define __data_end DATA$$Limit
#define __bss_start BSS$$Base
#define __bss_end BSS$$Limit

#endif /* __ICCARM__ */

extern uint32_t *__except_stack_start;
extern uint32_t *__cstack_end;


#endif /* DEV_LNK_SYM_H__ */
