/*
 * part.h
 *
 * Copyright (c) 2020 Semidrive Semiconductor.
 * All rights reserved.
 *
 * Description: E3 part device IP macro.
 *
 * Revision History:
 * -----------------
 */

#ifndef SDRV_PART_H_
#define SDRV_PART_H_

#define CONFIG_E3106 1

/*
 * Bit mask of XSPI ports that require dummy fix up, using spi_nor.id as shift. For example, to
 * use XSPI1 Port A and XSPI2 Port A, define the bitmask as (1 << 0 | 1 << 2).
 */
#define CONFIG_IS25LP064A_DUMMY_FIXUP (1 << 0)

#define CONFIG_E3L 1

#endif /* SDRV_PART_H_ */
