/**
 * @file clock_default_cfg.c
 * @brief Semidrive clock default config file.
 *
 * This file supply a example clock config for use, it contain all IP,
 * you can pick up some of them in your application. Futhermore, you can modify
 * rate as you want.
 *
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */

#include <sdrv_ckgen.h>
#include <clock_ip.h>
#include <compiler.h>

/**
 * @brief g_pre_bus_config contains soc pre config bus clock.
 * Optional, we can change BUS to XTAL24M before PLL configuration.
 */
__WEAK const sdrv_ckgen_bus_config_t g_pre_bus_config = {
    .config_num = 0,
};

/**
 * @brief g_pll_config contains PLL rate config.
 *
 */
__WEAK const sdrv_ckgen_rate_config_t g_pll_config = {
    .config_num = 3,

    .config_nodes[0].clk_node = CLK_NODE(g_pll1_root),
    .config_nodes[0].rate = 500000000,

    .config_nodes[1].clk_node = CLK_NODE(g_pll2_root),
    .config_nodes[1].rate = 400000000,

    .config_nodes[2].clk_node = CLK_NODE(g_pll3_root),
    .config_nodes[2].rate = 600000000,
};

/**
 * @brief g_bus_config contains all bus core rate config.
 *
 */
__WEAK const sdrv_ckgen_bus_config_t g_bus_config = {
    .config_num = 2,

    /* SF core 300M, and AXI bus 150M, APB bus 75M */
    .config_nodes[0].clk_node = CLK_NODE(g_ckgen_bus_cr5_sf),
    .config_nodes[0].rate = 300000000,
    .config_nodes[0].post_div = CKGEN_BUS_DIV_4_2_1,

    /* AP bus 150M */
    .config_nodes[1].clk_node = CLK_NODE(g_ckgen_bus_ap_bus),
    .config_nodes[1].rate = 150000000,
    .config_nodes[1].post_div = CKGEN_BUS_DIV_4_2_1,
};

/**
 * @brief g_ip_config contains all IP rate config.
 *
 */
__WEAK const sdrv_ckgen_rate_config_t g_ip_config = {
    .config_num = 28,

    .config_nodes[0].clk_node = CLK_NODE(g_ckgen_ip_i2c_sf_1_to_3),
    .config_nodes[0].rate = 133300000,

    .config_nodes[1].clk_node = CLK_NODE(g_ckgen_ip_i2c_sf_4_to_6),
    .config_nodes[1].rate = 133300000,

    .config_nodes[2].clk_node = CLK_NODE(g_ckgen_ip_spi_sf_1_to_3),
    .config_nodes[2].rate = 133300000,

    .config_nodes[3].clk_node = CLK_NODE(g_ckgen_ip_spi_sf_4_to_6),
    .config_nodes[3].rate = 133300000,

    .config_nodes[4].clk_node = CLK_NODE(g_ckgen_ip_uart_sf_1_to_6),
    .config_nodes[4].rate = 83300000,

    .config_nodes[5].clk_node = CLK_NODE(g_ckgen_ip_uart_sf_7_to_12),
    .config_nodes[5].rate = 83300000,

    .config_nodes[6].clk_node = CLK_NODE(g_ckgen_ip_enet1_tx),
    .config_nodes[6].rate = 250000000,

    .config_nodes[7].clk_node = CLK_NODE(g_ckgen_ip_enet1_rmii),
    .config_nodes[7].rate = 50000000,

    .config_nodes[8].clk_node = CLK_NODE(g_ckgen_ip_enet1_phy_ref),
    .config_nodes[8].rate = 125000000,

    .config_nodes[9].clk_node = CLK_NODE(g_ckgen_ip_enet1_timer_sec),
    .config_nodes[9].rate = 500000000,

    .config_nodes[10].clk_node = CLK_NODE(g_ckgen_ip_i2s_mclk0),
    .config_nodes[10].rate = 133300000,

    .config_nodes[11].clk_node = CLK_NODE(g_ckgen_ip_xspi1a),
    .config_nodes[11].rate = 200000000,

    .config_nodes[12].clk_node = CLK_NODE(g_ckgen_ip_xspi1b),
    .config_nodes[12].rate = 200000000,

    .config_nodes[13].clk_node = CLK_NODE(g_ckgen_ip_saci2_clk),
    .config_nodes[13].rate = 100000000,

    .config_nodes[14].clk_node = CLK_NODE(g_ckgen_ip_saci2_pdm_clk),
    .config_nodes[14].rate = 100000000,

    .config_nodes[15].clk_node = CLK_NODE(g_ckgen_ip_xtrg),
    .config_nodes[15].rate = 150000000,

    .config_nodes[16].clk_node = CLK_NODE(g_ckgen_ip_etmr1),
    .config_nodes[16].rate = 150000000,

    .config_nodes[17].clk_node = CLK_NODE(g_ckgen_ip_etmr2),
    .config_nodes[17].rate = 150000000,

    .config_nodes[18].clk_node = CLK_NODE(g_ckgen_ip_epwm1),
    .config_nodes[18].rate = 150000000,

    .config_nodes[19].clk_node = CLK_NODE(g_ckgen_ip_epwm2),
    .config_nodes[19].rate = 150000000,

    .config_nodes[20].clk_node = CLK_NODE(g_ckgen_ip_sehc1),
    .config_nodes[20].rate = 300000000,

    .config_nodes[21].clk_node = CLK_NODE(g_ckgen_ip_can),
    .config_nodes[21].rate = 80000000,

    .config_nodes[22].clk_node = CLK_NODE(g_ckgen_ip_adc1),
    .config_nodes[22].rate = 150000000,

    .config_nodes[23].clk_node = CLK_NODE(g_ckgen_ip_adc2),
    .config_nodes[23].rate = 150000000,

    .config_nodes[24].clk_node = CLK_NODE(g_ckgen_ip_adc3),
    .config_nodes[24].rate = 150000000,

    .config_nodes[25].clk_node = CLK_NODE(g_ckgen_ip_acmp),
    .config_nodes[25].rate = 150000000,

    .config_nodes[26].clk_node = CLK_NODE(g_ckgen_ip_ioc),
    .config_nodes[26].rate = 200000000,

    .config_nodes[27].clk_node = CLK_NODE(g_ckgen_ip_pt_sns_sf),
    .config_nodes[27].rate = 100000,
};

/**
 * @brief g_enable_config contains IP clock enable/disable config
 *
 */
__WEAK const sdrv_ckgen_ip_clock_config_t g_enable_config = {
    .config_num = 58,

    .config_nodes[0].ip_nodes = g_ckgen_adc1,
    .config_nodes[0].mode = CKGEN_RUN_MODE,
    .config_nodes[0].enable = false,

    .config_nodes[1].ip_nodes = g_ckgen_adc2,
    .config_nodes[1].mode = CKGEN_RUN_MODE,
    .config_nodes[1].enable = false,

    .config_nodes[2].ip_nodes = g_ckgen_adc3,
    .config_nodes[2].mode = CKGEN_RUN_MODE,
    .config_nodes[2].enable = false,

    .config_nodes[3].ip_nodes = g_ckgen_acmp1,
    .config_nodes[3].mode = CKGEN_RUN_MODE,
    .config_nodes[3].enable = false,

    .config_nodes[4].ip_nodes = g_ckgen_acmp2,
    .config_nodes[4].mode = CKGEN_RUN_MODE,
    .config_nodes[4].enable = false,

    .config_nodes[5].ip_nodes = g_ckgen_btm1,
    .config_nodes[5].mode = CKGEN_RUN_MODE,
    .config_nodes[5].enable = false,

    .config_nodes[6].ip_nodes = g_ckgen_btm2,
    .config_nodes[6].mode = CKGEN_RUN_MODE,
    .config_nodes[6].enable = false,

    .config_nodes[7].ip_nodes = g_ckgen_btm3,
    .config_nodes[7].mode = CKGEN_RUN_MODE,
    .config_nodes[7].enable = false,

    .config_nodes[8].ip_nodes = g_ckgen_btm4,
    .config_nodes[8].mode = CKGEN_RUN_MODE,
    .config_nodes[8].enable = false,

    .config_nodes[9].ip_nodes = g_ckgen_canfd16,
    .config_nodes[9].mode = CKGEN_RUN_MODE,
    .config_nodes[9].enable = false,

    .config_nodes[10].ip_nodes = g_ckgen_canfd21,
    .config_nodes[10].mode = CKGEN_RUN_MODE,
    .config_nodes[10].enable = false,

    .config_nodes[11].ip_nodes = g_ckgen_canfd23,
    .config_nodes[11].mode = CKGEN_RUN_MODE,
    .config_nodes[11].enable = false,

    .config_nodes[12].ip_nodes = g_ckgen_canfd3,
    .config_nodes[12].mode = CKGEN_RUN_MODE,
    .config_nodes[12].enable = false,

    .config_nodes[13].ip_nodes = g_ckgen_canfd4,
    .config_nodes[13].mode = CKGEN_RUN_MODE,
    .config_nodes[13].enable = false,

    .config_nodes[14].ip_nodes = g_ckgen_canfd5,
    .config_nodes[14].mode = CKGEN_RUN_MODE,
    .config_nodes[14].enable = false,

    .config_nodes[15].ip_nodes = g_ckgen_canfd6,
    .config_nodes[15].mode = CKGEN_RUN_MODE,
    .config_nodes[15].enable = false,

    .config_nodes[16].ip_nodes = g_ckgen_canfd7,
    .config_nodes[16].mode = CKGEN_RUN_MODE,
    .config_nodes[16].enable = false,

    .config_nodes[17].ip_nodes = g_ckgen_dma_sf,
    .config_nodes[17].mode = CKGEN_RUN_MODE,
    .config_nodes[17].enable = false,

    .config_nodes[18].ip_nodes = g_ckgen_enet1,
    .config_nodes[18].mode = CKGEN_RUN_MODE,
    .config_nodes[18].enable = false,

    .config_nodes[19].ip_nodes = g_ckgen_epwm1,
    .config_nodes[19].mode = CKGEN_RUN_MODE,
    .config_nodes[19].enable = false,

    .config_nodes[20].ip_nodes = g_ckgen_epwm2,
    .config_nodes[20].mode = CKGEN_RUN_MODE,
    .config_nodes[20].enable = false,

    .config_nodes[21].ip_nodes = g_ckgen_etmr1,
    .config_nodes[21].mode = CKGEN_RUN_MODE,
    .config_nodes[21].enable = false,

    .config_nodes[22].ip_nodes = g_ckgen_etmr2,
    .config_nodes[22].mode = CKGEN_RUN_MODE,
    .config_nodes[22].enable = false,

    .config_nodes[23].ip_nodes = g_ckgen_i2c3,
    .config_nodes[23].mode = CKGEN_RUN_MODE,
    .config_nodes[23].enable = false,

    .config_nodes[24].ip_nodes = g_ckgen_i2c4,
    .config_nodes[24].mode = CKGEN_RUN_MODE,
    .config_nodes[24].enable = false,

    .config_nodes[25].ip_nodes = g_ckgen_i2c5,
    .config_nodes[25].mode = CKGEN_RUN_MODE,
    .config_nodes[25].enable = false,

    .config_nodes[26].ip_nodes = g_ckgen_i2c6,
    .config_nodes[26].mode = CKGEN_RUN_MODE,
    .config_nodes[26].enable = false,

    .config_nodes[27].ip_nodes = g_ckgen_ioc,
    .config_nodes[27].mode = CKGEN_RUN_MODE,
    .config_nodes[27].enable = false,

    .config_nodes[28].ip_nodes = g_ckgen_pt_sns_sf_ana,
    .config_nodes[28].mode = CKGEN_RUN_MODE,
    .config_nodes[28].enable = false,

    .config_nodes[29].ip_nodes = g_ckgen_pt_sns_sf_dig,
    .config_nodes[29].mode = CKGEN_RUN_MODE,
    .config_nodes[29].enable = false,

    .config_nodes[30].ip_nodes = g_ckgen_saci2,
    .config_nodes[30].mode = CKGEN_RUN_MODE,
    .config_nodes[30].enable = false,

    .config_nodes[31].ip_nodes = g_ckgen_sehc1,
    .config_nodes[31].mode = CKGEN_RUN_MODE,
    .config_nodes[31].enable = false,

    .config_nodes[32].ip_nodes = g_ckgen_seip,
    .config_nodes[32].mode = CKGEN_RUN_MODE,
    .config_nodes[32].enable = false,

    .config_nodes[33].ip_nodes = g_ckgen_spi1,
    .config_nodes[33].mode = CKGEN_RUN_MODE,
    .config_nodes[33].enable = false,

    .config_nodes[34].ip_nodes = g_ckgen_spi2,
    .config_nodes[34].mode = CKGEN_RUN_MODE,
    .config_nodes[34].enable = false,

    .config_nodes[35].ip_nodes = g_ckgen_spi3,
    .config_nodes[35].mode = CKGEN_RUN_MODE,
    .config_nodes[35].enable = false,

    .config_nodes[36].ip_nodes = g_ckgen_spi4,
    .config_nodes[36].mode = CKGEN_RUN_MODE,
    .config_nodes[36].enable = false,

    .config_nodes[37].ip_nodes = g_ckgen_spi5,
    .config_nodes[37].mode = CKGEN_RUN_MODE,
    .config_nodes[37].enable = false,

    .config_nodes[38].ip_nodes = g_ckgen_spi6,
    .config_nodes[38].mode = CKGEN_RUN_MODE,
    .config_nodes[38].enable = false,

    .config_nodes[39].ip_nodes = g_ckgen_uart1,
    .config_nodes[39].mode = CKGEN_RUN_MODE,
    .config_nodes[39].enable = false,

    .config_nodes[40].ip_nodes = g_ckgen_uart2,
    .config_nodes[40].mode = CKGEN_RUN_MODE,
    .config_nodes[40].enable = false,

    .config_nodes[41].ip_nodes = g_ckgen_uart3,
    .config_nodes[41].mode = CKGEN_RUN_MODE,
    .config_nodes[41].enable = false,

    .config_nodes[42].ip_nodes = g_ckgen_uart4,
    .config_nodes[42].mode = CKGEN_RUN_MODE,
    .config_nodes[42].enable = false,

    .config_nodes[43].ip_nodes = g_ckgen_uart5,
    .config_nodes[43].mode = CKGEN_RUN_MODE,
    .config_nodes[43].enable = false,

    .config_nodes[44].ip_nodes = g_ckgen_uart6,
    .config_nodes[44].mode = CKGEN_RUN_MODE,
    .config_nodes[44].enable = false,

    .config_nodes[45].ip_nodes = g_ckgen_uart7,
    .config_nodes[45].mode = CKGEN_RUN_MODE,
    .config_nodes[45].enable = false,

    .config_nodes[46].ip_nodes = g_ckgen_uart8,
    .config_nodes[46].mode = CKGEN_RUN_MODE,
    .config_nodes[46].enable = false,

    .config_nodes[47].ip_nodes = g_ckgen_uart9,
    .config_nodes[47].mode = CKGEN_RUN_MODE,
    .config_nodes[47].enable = false,

    .config_nodes[48].ip_nodes = g_ckgen_uart10,
    .config_nodes[48].mode = CKGEN_RUN_MODE,
    .config_nodes[48].enable = false,

    .config_nodes[49].ip_nodes = g_ckgen_uart11,
    .config_nodes[49].mode = CKGEN_RUN_MODE,
    .config_nodes[49].enable = false,

    .config_nodes[50].ip_nodes = g_ckgen_uart12,
    .config_nodes[50].mode = CKGEN_RUN_MODE,
    .config_nodes[50].enable = false,

    .config_nodes[51].ip_nodes = g_ckgen_usb,
    .config_nodes[51].mode = CKGEN_RUN_MODE,
    .config_nodes[51].enable = false,

    .config_nodes[52].ip_nodes = g_ckgen_wdt1,
    .config_nodes[52].mode = CKGEN_RUN_MODE,
    .config_nodes[52].enable = false,

    .config_nodes[53].ip_nodes = g_ckgen_wdt8,
    .config_nodes[53].mode = CKGEN_RUN_MODE,
    .config_nodes[53].enable = false,

    .config_nodes[54].ip_nodes = g_ckgen_xspi1a,
    .config_nodes[54].mode = CKGEN_RUN_MODE,
    .config_nodes[54].enable = false,

    .config_nodes[55].ip_nodes = g_ckgen_xspi1b,
    .config_nodes[55].mode = CKGEN_RUN_MODE,
    .config_nodes[55].enable = false,

    .config_nodes[56].ip_nodes = g_ckgen_xspi_slv,
    .config_nodes[56].mode = CKGEN_RUN_MODE,
    .config_nodes[56].enable = false,

    .config_nodes[57].ip_nodes = g_ckgen_xtrg,
    .config_nodes[57].mode = CKGEN_RUN_MODE,
    .config_nodes[57].enable = false,
};

#if CONFIG_CLK_DUMP

__WEAK sdrv_clk_config_t g_clk_config = {
    .config_num = 37,

    .config_nodes[0].ckgen_ref = CLK_NODE(g_rc_32k),
    .config_nodes[0].name = "rc32k",

    .config_nodes[1].ckgen_ref = CLK_NODE(g_fs_32k),
    .config_nodes[1].name = "fs32k",

    .config_nodes[2].ckgen_ref = CLK_NODE(g_rc_24m),
    .config_nodes[2].name = "rc24m",

    .config_nodes[3].ckgen_ref = CLK_NODE(g_fs_24m),
    .config_nodes[3].name = "fs24m",

    .config_nodes[4].ckgen_ref = CLK_NODE(g_pll1_root),
    .config_nodes[4].name = "pll1",

    .config_nodes[5].ckgen_ref = CLK_NODE(g_pll2_root),
    .config_nodes[5].name = "pll2",

    .config_nodes[6].ckgen_ref = CLK_NODE(g_pll3_root),
    .config_nodes[6].name = "pll3",

    .config_nodes[7].ckgen_ref = CLK_NODE(g_ckgen_bus_cr5_sf),
    .config_nodes[7].name = "cr5_sf",

    .config_nodes[8].ckgen_ref = CLK_NODE(g_ckgen_ip_i2c_sf_1_to_3),
    .config_nodes[8].name = "i2c_sf_1_to_3",

    .config_nodes[9].ckgen_ref = CLK_NODE(g_ckgen_ip_i2c_sf_4_to_6),
    .config_nodes[9].name = "i2c_sf_4_to_6",

    .config_nodes[10].ckgen_ref = CLK_NODE(g_ckgen_ip_spi_sf_1_to_3),
    .config_nodes[10].name = "spi_sf_1_to_3",

    .config_nodes[11].ckgen_ref = CLK_NODE(g_ckgen_ip_spi_sf_4_to_6),
    .config_nodes[11].name = "spi_sf_4_to_6",

    .config_nodes[12].ckgen_ref = CLK_NODE(g_ckgen_ip_uart_sf_1_to_6),
    .config_nodes[12].name = "uart_sf_1_to_6",

    .config_nodes[13].ckgen_ref = CLK_NODE(g_ckgen_ip_uart_sf_7_to_12),
    .config_nodes[13].name = "uart_sf_7_to_12",

    .config_nodes[14].ckgen_ref = CLK_NODE(g_ckgen_ip_enet1_tx),
    .config_nodes[14].name = "enet1_tx",

    .config_nodes[15].ckgen_ref = CLK_NODE(g_ckgen_ip_enet1_rmii),
    .config_nodes[15].name = "enet1_rmii",

    .config_nodes[16].ckgen_ref = CLK_NODE(g_ckgen_ip_enet1_phy_ref),
    .config_nodes[16].name = "enet1_phy_ref",

    .config_nodes[17].ckgen_ref = CLK_NODE(g_ckgen_ip_enet1_timer_sec),
    .config_nodes[17].name = "enet1_timer_sec",

    .config_nodes[18].ckgen_ref = CLK_NODE(g_ckgen_ip_i2s_mclk0),
    .config_nodes[18].name = "i2s_mclk0",

    .config_nodes[19].ckgen_ref = CLK_NODE(g_ckgen_ip_xspi1a),
    .config_nodes[19].name = "xspi1a",

    .config_nodes[20].ckgen_ref = CLK_NODE(g_ckgen_ip_xspi1b),
    .config_nodes[20].name = "xspi1b",

    .config_nodes[21].ckgen_ref = CLK_NODE(g_ckgen_ip_saci2_clk),
    .config_nodes[21].name = "saci2_clk",

    .config_nodes[22].ckgen_ref = CLK_NODE(g_ckgen_ip_saci2_pdm_clk),
    .config_nodes[22].name = "saci2_pdm_clk",

    .config_nodes[23].ckgen_ref = CLK_NODE(g_ckgen_ip_xtrg),
    .config_nodes[23].name = "xtrg",

    .config_nodes[24].ckgen_ref = CLK_NODE(g_ckgen_ip_etmr1),
    .config_nodes[24].name = "etmr1",

    .config_nodes[25].ckgen_ref = CLK_NODE(g_ckgen_ip_etmr2),
    .config_nodes[25].name = "etmr2",

    .config_nodes[26].ckgen_ref = CLK_NODE(g_ckgen_ip_epwm1),
    .config_nodes[26].name = "epwm1",

    .config_nodes[27].ckgen_ref = CLK_NODE(g_ckgen_ip_epwm2),
    .config_nodes[27].name = "epwm2",

    .config_nodes[28].ckgen_ref = CLK_NODE(g_ckgen_ip_sehc1),
    .config_nodes[28].name = "sehc1",

    .config_nodes[29].ckgen_ref = CLK_NODE(g_ckgen_ip_can),
    .config_nodes[29].name = "can",

    .config_nodes[30].ckgen_ref = CLK_NODE(g_ckgen_ip_adc1),
    .config_nodes[30].name = "adc1",

    .config_nodes[31].ckgen_ref = CLK_NODE(g_ckgen_ip_adc2),
    .config_nodes[31].name = "adc2",

    .config_nodes[32].ckgen_ref = CLK_NODE(g_ckgen_ip_adc3),
    .config_nodes[32].name = "adc3",

    .config_nodes[33].ckgen_ref = CLK_NODE(g_ckgen_ip_acmp),
    .config_nodes[33].name = "acmp",

    .config_nodes[34].ckgen_ref = CLK_NODE(g_ckgen_ip_ioc),
    .config_nodes[34].name = "ioc",

    .config_nodes[35].ckgen_ref = CLK_NODE(g_ckgen_ip_pt_sns_sf),
    .config_nodes[35].name = "pt_sns_sf",

    .config_nodes[36].ckgen_ref = CLK_NODE(g_ckgen_bus_ap_bus),
    .config_nodes[36].name = "ap_bus",
};

#endif /* CONFIG_CLK_DUMP */
