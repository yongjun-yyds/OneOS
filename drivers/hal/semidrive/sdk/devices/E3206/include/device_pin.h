/**
 * @file device_pin.h
 * @brief Sdrv device init pin header.
 *
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *          All rights reserved.
 */

#ifndef DEVICE_PIN_H_
#define DEVICE_PIN_H_
#include <types.h>

#define TAISHAN_PIN_NUM                  125
#define TAISHAN_SAFETY_PIN_START         0
#define TAISHAN_SAFETY_PIN_NUM           125

#define TAISHAN_PINCTRL_GET_BASE(pin)  (APB_IOMUXC_SF_BASE)

#define TAISHAN_PINCTRL_GET_INDEX(pin)  ((pin) + 1)

#define PINCTRL_IS_IDX(pin, mux)   (((pin) << 8) + (mux))

#define TAISHAN_GPIO_GET_BASE(pin)  (APB_GPIO_SF_BASE)

#define TAISHAN_GPIO_GET_INDEX(pin)  (pin)

#define RTC_DOMAIN_SYS_MODE0   0
#define RTC_DOMAIN_SYS_MODE1   1
#define RTC_DOMAIN_SYS_POR_B   2
#define RTC_DOMAIN_SYS_BUTTON  3
#define RTC_DOMAIN_SYS_WAKEUP0 4
#define RTC_DOMAIN_SYS_WAKEUP1 5
#define RTC_DOMAIN_SYS_CTRL0   6
#define RTC_DOMAIN_SYS_CTRL1   7

#define RTC_DOMAIN_PIN_NUM  8

#endif /* DEVICE_PIN_H_ */
