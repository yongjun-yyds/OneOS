//*****************************************************************************
//
// WARNING: Automatically generated file, don't modify anymore!!!
//
// Copyright (c) 2021-2029 Semidrive Incorporated.  All rights reserved.
// Software License Agreement
//
//*****************************************************************************

#ifndef REGS_BASE_H
#define REGS_BASE_H

/*------------------------------------------------------
 * Interrupt
 *------------------------------------------------------*/

/* Vectored Interrupt Controller (VIC) 1 Base Address, for SF Core (cluster 0) */
#define VIC1_BASE (0xF1C00000ul)


/*------------------------------------------------------
 * Bus Inter-Connect
 *------------------------------------------------------*/

/* Configuration Register for FAB_AP Fabric */
#define APB_GPV_AP_BASE (0xF0C60000ul)

/* Configuration Register for FAB_XB Fabric */
#define APB_GPV_XB_BASE (0xF0B50000ul)

/* Configuration Register for FAB_SAFETY Fabric */
#define APB_GPV_SF_BASE (0xF0B40000ul)


/*------------------------------------------------------
 * Security
 *------------------------------------------------------*/

/* Secure Element Intellectual Property */
#define APB_SEIP_BASE (0xF0C50000ul)

/* E-Fuse controller */
#define APB_EFUSEC_BASE (0xF0820000ul)

/* Secure Storage */
#define APB_SEC_STORAGE_BASE (0xF00A0000ul)

/* Tamper Monitor */
#define APB_TM_BASE (0xF0080000ul)

/* SEIP Secured Key Buffer */
#define SEIP_SEIPSECUREDKEYBUFFER_BASE (0x02200000ul)

/* SEIP Kbuf */
#define APB_KBUF_BASE (0xF21DE000ul)

/* SEIP APB System Reg */
#define APB_APBSYSTEMREG_BASE (0xF21DD800ul)

/* SEIP UART0 */
#define APB_SEIP_UART0_BASE (0xF21DD000ul)

/* SEIP Mailbox */
#define APB_SEIP_MAILBOX_BASE (0xF21D8000ul)

/* SEIP AHB System Reg */
#define APB_AHBSYSTEMREG_BASE (0xF21D0000ul)

/* SEIP HFE */
#define APB_SEIP_HFE_BASE (0xF21CC000ul)

/* SEIP HKE */
#define APB_SEIP_SKE_BASE (0xF21C8000ul)

/* SEIP TRNG */
#define APB_SEIP_TRNG_BASE (0xF21C4000ul)

/* SEIP PKE */
#define APB_SEIP_PKE_BASE (0xF21C0000ul)

/* SDIP SRAM1 */
#define APB_SEIP_SRAM1_BASE (0xF21A0000ul)

/* SEIP SRAM0 */
#define APB_SEIP_SRAM0_BASE (0xF2180000ul)

/* SEIP CPU FIO */
#define APB_SEIP_CPU_FIO_BASE (0xF2160000ul)

/* SEIP CPU PLIC */
#define APB_SEIP_CPU_PLIC_BASE (0xF2150000ul)

/* SEIP CPU CLINT */
#define APB_SEIP_CPU_CLINT_BASE (0xF2140000ul)

/* SEIP DTCM */
#define APB_SEIP_DTCM_BASE (0xF2120000ul)

/* SEIP ITCM */
#define APB_SEIP_ITCM_BASE (0xF2100000ul)

/* SEIP Controller */
#define SEIP_SEIP_BASE (0x02100000ul)


/*------------------------------------------------------
 * Peripherals
 *------------------------------------------------------*/

/* SF Domain General Porpose Intput Output (GPIO) */
#define APB_GPIO_SF_BASE (0xF0C40000ul)

/* Inter-Integrated Circuit (I2C) 4 */
#define APB_I2C4_BASE (0xF0A60000ul)

/* Inter-Integrated Circuit (I2C) 3 */
#define APB_I2C3_BASE (0xF0A50000ul)

/* Inter-Integrated Circuit (I2C) 2 */
#define APB_I2C2_BASE (0xF0A40000ul)

/* Inter-Integrated Circuit (I2C) 1 */
#define APB_I2C1_BASE (0xF0A30000ul)

/* Ethernet (ENET) 1 */
#define APB_ENET1_BASE (0xF0930000ul)

/* Universal Asynchronous Receiver/Transmitter (UART) 12 */
#define APB_UART12_BASE (0xF06B0000ul)

/* Universal Asynchronous Receiver/Transmitter (UART) 11 */
#define APB_UART11_BASE (0xF06A0000ul)

/* Universal Asynchronous Receiver/Transmitter (UART) 10 */
#define APB_UART10_BASE (0xF0690000ul)

/* Universal Asynchronous Receiver/Transmitter (UART) 9 */
#define APB_UART9_BASE (0xF0680000ul)

/* Universal Asynchronous Receiver/Transmitter (UART) 8 */
#define APB_UART8_BASE (0xF0670000ul)

/* Universal Asynchronous Receiver/Transmitter (UART) 7 */
#define APB_UART7_BASE (0xF0660000ul)

/* Universal Asynchronous Receiver/Transmitter (UART) 6 */
#define APB_UART6_BASE (0xF0650000ul)

/* Universal Asynchronous Receiver/Transmitter (UART) 5 */
#define APB_UART5_BASE (0xF0640000ul)

/* Universal Asynchronous Receiver/Transmitter (UART) 4 */
#define APB_UART4_BASE (0xF0630000ul)

/* Universal Asynchronous Receiver/Transmitter (UART) 3 */
#define APB_UART3_BASE (0xF0620000ul)

/* Universal Asynchronous Receiver/Transmitter (UART) 2 */
#define APB_UART2_BASE (0xF0610000ul)

/* Universal Asynchronous Receiver/Transmitter (UART) 1 */
#define APB_UART1_BASE (0xF0600000ul)

/* Serial Peripheral Interface (SPI) 6 */
#define APB_SPI6_BASE (0xF05D0000ul)

/* Serial Peripheral Interface (SPI) 5 */
#define APB_SPI5_BASE (0xF05C0000ul)

/* Serial Peripheral Interface (SPI) 4 */
#define APB_SPI4_BASE (0xF05B0000ul)

/* Serial Peripheral Interface (SPI) 3 */
#define APB_SPI3_BASE (0xF05A0000ul)

/* Serial Peripheral Interface (SPI) 2 */
#define APB_SPI2_BASE (0xF0590000ul)

/* Serial Peripheral Interface (SPI) 1 */
#define APB_SPI1_BASE (0xF0580000ul)

/* CAN with Flexible Data rate (CANFD) 8 */
#define APB_CANFD23_BASE (0xF0570000ul)

/* CAN with Flexible Data rate (CANFD) 7 */
#define APB_CANFD7_BASE (0xF0560000ul)

/* CAN with Flexible Data rate (CANFD) 6 */
#define APB_CANFD6_BASE (0xF0550000ul)

/* CAN with Flexible Data rate (CANFD) 5 */
#define APB_CANFD5_BASE (0xF0540000ul)

/* CAN with Flexible Data rate (CANFD) 4 */
#define APB_CANFD4_BASE (0xF0530000ul)

/* CAN with Flexible Data rate (CANFD) 3 */
#define APB_CANFD3_BASE (0xF0520000ul)

/* CAN with Flexible Data rate (CANFD) 2 */
#define APB_CANFD21_BASE (0xF0510000ul)

/* CAN with Flexible Data rate (CANFD) 1 */
#define APB_CANFD16_BASE (0xF0500000ul)


/*------------------------------------------------------
 * USB
 *------------------------------------------------------*/

/* USB Controller */
#define APB_USB_BASE (0xF0C30000ul)


/*------------------------------------------------------
 * Audio
 *------------------------------------------------------*/

/* Serial Audio Controller Interface 1 */
#define APB_SACI2_BASE (0xF0C20000ul)


/*------------------------------------------------------
 * External Memory Interface
 *------------------------------------------------------*/

/* SD/EMMC Host Controller 1 */
#define APB_SEHC1_BASE (0xF0C10000ul)

/* Extended SPI Slave */
#define APB_XSPI_SLV_BASE (0xF0950000ul)

/* Extended SPI 1 Port B Control Registers */
#define APB_XSPI1PORTB_BASE (0xF0780000ul)

/* Extended SPI 1 Port A Control Registers */
#define APB_XSPI1PORTA_BASE (0xF0770000ul)

/* Extended SPI1 Port B */
#define XSPI1_XSPI1PORTB_BASE (0x18000000ul)

/* Extended SPI1 */
#define XSPI1_BASE (0x10000000ul)

/* Extended SPI1 Port A */
#define XSPI1_XSPI1PORTA_BASE (0x10000000ul)


/*------------------------------------------------------
 * Internal Memory Configuration
 *------------------------------------------------------*/

/* IRAM1 RAM Controller */
#define APB_IRAMC1_BASE (0xF0BE0000ul)

/* Internal ROM Controller */
#define APB_IROMC_BASE (0xF08A0000ul)


/*------------------------------------------------------
 * FuSa
 *------------------------------------------------------*/

/* System Error Management 2 */
#define APB_SEM2_BASE (0xF0BA0000ul)

/* System Error Management 1 */
#define APB_SEM1_BASE (0xF0B90000ul)

/* Error Injection Controller for SF Domain */
#define APB_EIC_SF_BASE (0xF0840000ul)

/* Input/Output Consistency */
#define APB_IOC_BASE (0xF0750000ul)


/*------------------------------------------------------
 * ADC & ACMP
 *------------------------------------------------------*/

/* Analog Digital Converter 3 */
#define APB_ADC3_BASE (0xF0B80000ul)

/* Analog Digital Converter 2 */
#define APB_ADC2_BASE (0xF0B70000ul)

/* Analog Digital Converter 1 */
#define APB_ADC1_BASE (0xF0B60000ul)

/* Analog Compare (ACMP) 4 */
#define APB_ACMP4_BASE (0xF0AC0000ul)

/* Analog Compare (ACMP) 3 */
#define APB_ACMP3_BASE (0xF0AB0000ul)

/* Analog Compare (ACMP) 2 */
#define APB_ACMP2_BASE (0xF0AA0000ul)

/* Analog Compare (ACMP) 1 */
#define APB_ACMP1_BASE (0xF0A90000ul)


/*------------------------------------------------------
 * Access Permission Control
 *------------------------------------------------------*/

/* Memory Access Controller */
#define APB_MAC_BASE (0xF0B30000ul)


/*------------------------------------------------------
 * Timers
 *------------------------------------------------------*/

/* Watch Dog(WDT) 8 */
#define APB_WDT8_BASE (0xF0B10000ul)

/* Watch Dog(WDT) 2 */
#define APB_WDT2_BASE (0xF0AE0000ul)

/* Watch Dog(WDT) 1 */
#define APB_WDT1_BASE (0xF0AD0000ul)

/* Targeting general purpose timer 2 */
#define APB_ETMR2_BASE (0xF04D0000ul)

/* Targeting general purpose timer 1 */
#define APB_ETMR1_BASE (0xF04C0000ul)

/* Energy Pulse-Width Modulator 2 */
#define APB_EPWM2_BASE (0xF0490000ul)

/* Energy Pulse-Width Modulator 1 */
#define APB_EPWM1_BASE (0xF0480000ul)

/* Basic Timer Module (BTM) 4 */
#define APB_BTM4_BASE (0xF0450000ul)

/* Basic Timer Module (BTM) 3 */
#define APB_BTM3_BASE (0xF0440000ul)

/* Basic Timer Module (BTM) 2 */
#define APB_BTM2_BASE (0xF0430000ul)

/* Basic Timer Module (BTM) 1 */
#define APB_BTM1_BASE (0xF0420000ul)

/* Cross Trigger */
#define APB_XTRG_BASE (0xF0410000ul)

/* Real Time Clock 2 */
#define APB_RTC2_BASE (0xF0020000ul)

/* Real Time Clock 1 */
#define APB_RTC1_BASE (0xF0010000ul)


/*------------------------------------------------------
 * Debug
 *------------------------------------------------------*/

/* CoreSight */
#define APB_CSSYS_BASE (0xF0A00000ul)


/*------------------------------------------------------
 * DMA
 *------------------------------------------------------*/

/* SF Domain Direct Memory Access (DMA) 1 */
#define APB_DMA_SF1_BASE (0xF09B0000ul)

/* SF Domain Direct Memory Access (DMA) 0 */
#define APB_DMA_SF0_BASE (0xF0970000ul)


/*------------------------------------------------------
 * Power Management
 *------------------------------------------------------*/

/* Temperature Sensor for SF Domain */
#define APB_PT_SNS_SF_BASE (0xF0920000ul)

/* Voltage Detector for SF Domain */
#define APB_VD_SF_BASE (0xF0910000ul)

/* Power on Reset for Safety Domain */
#define APB_POR_SF_BASE (0xF0880000ul)

/* System Work Mode Controller(SMC) */
#define APB_SMC_BASE (0xF0870000ul)

/* Digital controller of bulk mode DC-DC converter(DCDC) */
#define APB_DCDC1_BASE (0xF0830000ul)

/* Power Management Unit */
#define APB_PMU_CORE_BASE (0xF0060000ul)


/*------------------------------------------------------
 * Clock & Reset
 *------------------------------------------------------*/

/* Phase Locked Loop (PLL) 3 */
#define APB_PLL3_BASE (0xF0900000ul)

/* Phase Locked Loop (PLL) 2 */
#define APB_PLL2_BASE (0xF08F0000ul)

/* Phase Locked Loop (PLL) 1 */
#define APB_PLL1_BASE (0xF08E0000ul)

/* SF Domain Clock Generator (CKGEN) */
#define APB_CKGEN_SF_BASE (0xF08D0000ul)

/* SF Domain Reset Generator (RSTGEN) */
#define APB_RSTGEN_SF_BASE (0xF08C0000ul)

/* 24MHZ generation block with failsafe function(FS24M) */
#define APB_FS_24M_BASE (0xF0890000ul)

/* 32KHZ generation block with failsafe function (FS32K) */
#define APB_FS_32K_BASE (0xF0030000ul)


/*------------------------------------------------------
 * System Control
 *------------------------------------------------------*/

/* SF Domain Status and Controller Register (SCR) */
#define APB_SCR_SF_BASE (0xF08B0000ul)


/*------------------------------------------------------
 * I/O Control
 *------------------------------------------------------*/

/* SF Domain IO Multiplexing Controller(IOMUXC) */
#define APB_IOMUXC_SF_BASE (0xF0860000ul)

/* RTC Domain IO Multiplexing Controller (IOMUXC) */
#define APB_IOMUXC_RTC_BASE (0xF0070000ul)


/*------------------------------------------------------
 * Misc
 *------------------------------------------------------*/

/* In System Test Controller */
#define APB_ISTC_BASE (0xF0850000ul)


/*------------------------------------------------------
 * CPU
 *------------------------------------------------------*/

/* Cluster 0 D-Cache */
#define R5_SF_CACHE_R5_SF_DCACHE_BASE (0x01978000ul)

/* Cluster 0 I-Cache */
#define R5_SF_CACHE_R5_SF_ICACHE_BASE (0x01970000ul)

/* Cluster 0 TCM A */
#define R5_SF_TCM_R5_SF_TCMA_BASE (0x01940000ul)

/* Cluster 0 TCM B */
#define R5_SF_TCM_R5_SF_TCMB_BASE (0x01930000ul)


/*------------------------------------------------------
 * IRAM
 *------------------------------------------------------*/

/* IRAM1 Aliased Address */
#define IRAM1ALIASED_BASE (0x00C00000ul)

/* IRAM1_ECC Aliased Address */
#define IRAM1_ECCALIASED_BASE (0x00BE0000ul)

/* Internal RAM 1 */
#define IRAM1_BASE (0x00500000ul)

/* RAM for IRAM1 ECC. Used as internal RAM when ECC is disabled. */
#define IRAM1_ECC_BASE (0x004E0000ul)

#endif /* REGS_BASE_H */
