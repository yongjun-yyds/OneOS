/**
 * @file device_init.c
 * @brief Semidrive device ealryinit.
 *
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */

#include <common.h>
#include <sdrv_ckgen.h>
#include <sdrv_power.h>
#include <regs_base.h>
#include <reg.h>

extern const sdrv_ckgen_node_t **g_ckgen_unused[];

static int mcu_unused_ip_cglist_mask(const sdrv_ckgen_node_t *ckgen_ip[], bool mask)
{
    sdrv_ckgen_node_t *node;
    uint32_t i = 0;
    int ret = 0;

    while (ckgen_ip[i] && !ret) {
        node = (sdrv_ckgen_node_t *)ckgen_ip[i];
        ret = sdrv_ckgen_cg_mask(node, mask);
        i++;
    }

    return ret;
}

static int mcu_unused_ip_config(void)
{
    uint32_t i = 0;
    int ret = 0;

    while (g_ckgen_unused[i]) {
        ret = sdrv_ckgen_ip_clock_enable(g_ckgen_unused[i], CKGEN_HIB_MODE, false);
        ret = sdrv_ckgen_ip_clock_enable(g_ckgen_unused[i], CKGEN_SLP_MODE, false);
        ret = sdrv_ckgen_ip_clock_enable(g_ckgen_unused[i], CKGEN_RUN_MODE, false);
        ret = mcu_unused_ip_cglist_mask(g_ckgen_unused[i], true);
        i++;
    }

    return ret;
}

/**
 * @brief initializes the device.
 *
 * This function initializes the device before call main function.
 */
void device_init(void)
{
    mcu_unused_ip_config();
}