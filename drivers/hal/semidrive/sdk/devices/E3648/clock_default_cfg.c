/**
 * @file clock_default_cfg.c
 * @brief Semidrive clock default config file.
 *
 * This file supply a example clock config for use, it contain all IP,
 * you can pick up some of them in your application. Futhermore, you can modify
 * rate as you want.
 *
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */

#include <sdrv_ckgen.h>
#include <clock_ip.h>
#include <compiler.h>

/**
 * @brief g_pre_bus_config contains soc pre config bus clock.
 * Optional, we can change BUS to XTAL24M before PLL configuration.
 */
__WEAK const sdrv_ckgen_bus_config_t g_pre_bus_config = {
    .config_num = 0,
};

/**
 * @brief g_pll_config contains PLL rate config.
 *
 */
__WEAK const sdrv_ckgen_rate_config_t g_pll_config = {
    .config_num = 10,

    .config_nodes[0].clk_node = CLK_NODE(g_pll1_root),
    .config_nodes[0].rate = 500000000,

    .config_nodes[1].clk_node = CLK_NODE(g_pll2_root),
    .config_nodes[1].rate = 800000000,

    .config_nodes[2].clk_node = CLK_NODE(g_pll3_root),
    .config_nodes[2].rate = 667000000,

    .config_nodes[3].clk_node = CLK_NODE(g_pll4_root),
    .config_nodes[3].rate = 600000000,

    .config_nodes[4].clk_node = CLK_NODE(g_pll5_root),
    .config_nodes[4].rate = 799968000,

    .config_nodes[5].clk_node = CLK_NODE(g_pll_lvds_root),
    .config_nodes[5].rate = 1039500000,

    .config_nodes[6].clk_node = CLK_NODE(g_pll_lvds_nodiv),
    .config_nodes[6].rate = 1039500000,

    .config_nodes[7].clk_node = CLK_NODE(g_pll_lvds_div2),
    .config_nodes[7].rate = 519750000,

    .config_nodes[8].clk_node = CLK_NODE(g_pll_lvds_div7),
    .config_nodes[8].rate = 74250000,

    .config_nodes[9].clk_node = CLK_NODE(g_pll_lvds_ckgen),
    .config_nodes[9].rate = 519750000,
};

/**
 * @brief g_bus_config contains all bus core rate config.
 *
 */
__WEAK const sdrv_ckgen_bus_config_t g_bus_config = {
    .config_num = 5,

    /* SF core 800M, and AXI bus 400M, APB bus 200M */
    .config_nodes[0].clk_node = CLK_NODE(g_ckgen_bus_cr5_sf),
    .config_nodes[0].rate = 800000000,
    .config_nodes[0].post_div = CKGEN_BUS_DIV_4_2_1,

    /* SP core 800M, and AXI bus 400M, APB bus 200M */
    .config_nodes[1].clk_node = CLK_NODE(g_ckgen_bus_cr5_sp),
    .config_nodes[1].rate = 800000000,
    .config_nodes[1].post_div = CKGEN_BUS_DIV_4_2_1,

    /* AP bus 400M */
    .config_nodes[2].clk_node = CLK_NODE(g_ckgen_bus_ap_bus),
    .config_nodes[2].rate = 400000000,
    .config_nodes[2].post_div = CKGEN_BUS_DIV_4_2_1,

    /* Disp bus 400M */
    .config_nodes[3].clk_node = CLK_NODE(g_ckgen_bus_disp_bus),
    .config_nodes[3].rate = 400000000,
    .config_nodes[3].post_div = CKGEN_BUS_DIV_4_2_1,

    /* SEIP bus 400M */
    .config_nodes[4].clk_node = CLK_NODE(g_ckgen_bus_seip),
    .config_nodes[4].rate = 400000000,
    .config_nodes[4].post_div = CKGEN_BUS_DIV_4_2_1,
};

/**
 * @brief g_core_config contains SX core rate config.
 *
 */
__WEAK const sdrv_ckgen_rate_config_t g_core_config = {
    .config_num = 1,

    /* SX core 800M */
    .config_nodes[0].clk_node = CLK_NODE(g_ckgen_core_cr5_sx),
    .config_nodes[0].rate = 800000000,
};

/**
 * @brief g_ip_config contains all IP rate config.
 *
 */
__WEAK const sdrv_ckgen_rate_config_t g_ip_config = {
    .config_num = 43,

    .config_nodes[0].clk_node = CLK_NODE(g_ckgen_ip_i2c_sf_1_to_4),
    .config_nodes[0].rate = 83380000,

    .config_nodes[1].clk_node = CLK_NODE(g_ckgen_ip_i2c_sf_5_to_8),
    .config_nodes[1].rate = 83380000,

    .config_nodes[2].clk_node = CLK_NODE(g_ckgen_ip_spi_sf_1_to_4),
    .config_nodes[2].rate = 133400000,

    .config_nodes[3].clk_node = CLK_NODE(g_ckgen_ip_spi_sf_5_to_8),
    .config_nodes[3].rate = 133400000,

    .config_nodes[4].clk_node = CLK_NODE(g_ckgen_ip_uart_sf_1_to_8),
    .config_nodes[4].rate = 83380000,

    .config_nodes[5].clk_node = CLK_NODE(g_ckgen_ip_uart_sf_9_to_16),
    .config_nodes[5].rate = 83380000,

    .config_nodes[6].clk_node = CLK_NODE(g_ckgen_ip_enet1_tx),
    .config_nodes[6].rate = 250000000,

    .config_nodes[7].clk_node = CLK_NODE(g_ckgen_ip_enet1_rmii),
    .config_nodes[7].rate = 50000000,

    .config_nodes[8].clk_node = CLK_NODE(g_ckgen_ip_enet1_phy_ref),
    .config_nodes[8].rate = 125000000,

    .config_nodes[9].clk_node = CLK_NODE(g_ckgen_ip_enet1_timer_sec),
    .config_nodes[9].rate = 125000000,

    .config_nodes[10].clk_node = CLK_NODE(g_ckgen_ip_enet2_tx),
    .config_nodes[10].rate = 250000000,

    .config_nodes[11].clk_node = CLK_NODE(g_ckgen_ip_enet2_rmii),
    .config_nodes[11].rate = 50000000,

    .config_nodes[12].clk_node = CLK_NODE(g_ckgen_ip_enet2_phy_ref),
    .config_nodes[12].rate = 125000000,

    .config_nodes[13].clk_node = CLK_NODE(g_ckgen_ip_xspi1a),
    .config_nodes[13].rate = 333500000,

    .config_nodes[14].clk_node = CLK_NODE(g_ckgen_ip_xspi1b),
    .config_nodes[14].rate = 250000000,

    .config_nodes[15].clk_node = CLK_NODE(g_ckgen_ip_xspi2a),
    .config_nodes[15].rate = 333500000,

    .config_nodes[16].clk_node = CLK_NODE(g_ckgen_ip_xspi2b),
    .config_nodes[16].rate = 333500000,

    .config_nodes[17].clk_node = CLK_NODE(g_ckgen_ip_xtrg),
    .config_nodes[17].rate = 250000000,

    .config_nodes[18].clk_node = CLK_NODE(g_ckgen_ip_etmr1),
    .config_nodes[18].rate = 250000000,

    .config_nodes[19].clk_node = CLK_NODE(g_ckgen_ip_etmr2),
    .config_nodes[19].rate = 250000000,

    .config_nodes[20].clk_node = CLK_NODE(g_ckgen_ip_etmr3),
    .config_nodes[20].rate = 250000000,

    .config_nodes[21].clk_node = CLK_NODE(g_ckgen_ip_etmr4),
    .config_nodes[21].rate = 250000000,

    .config_nodes[22].clk_node = CLK_NODE(g_ckgen_ip_epwm1),
    .config_nodes[22].rate = 250000000,

    .config_nodes[23].clk_node = CLK_NODE(g_ckgen_ip_epwm2),
    .config_nodes[23].rate = 250000000,

    .config_nodes[24].clk_node = CLK_NODE(g_ckgen_ip_epwm3),
    .config_nodes[24].rate = 250000000,

    .config_nodes[25].clk_node = CLK_NODE(g_ckgen_ip_epwm4),
    .config_nodes[25].rate = 250000000,

    .config_nodes[26].clk_node = CLK_NODE(g_ckgen_ip_can),
    .config_nodes[26].rate = 40000000,

    .config_nodes[27].clk_node = CLK_NODE(g_ckgen_ip_adc1),
    .config_nodes[27].rate = 166670000,

    .config_nodes[28].clk_node = CLK_NODE(g_ckgen_ip_adc2),
    .config_nodes[28].rate = 166670000,

    .config_nodes[29].clk_node = CLK_NODE(g_ckgen_ip_adc3),
    .config_nodes[29].rate = 166670000,

    .config_nodes[30].clk_node = CLK_NODE(g_ckgen_ip_acmp),
    .config_nodes[30].rate = 166670000,

    .config_nodes[31].clk_node = CLK_NODE(g_ckgen_ip_flexray),
    .config_nodes[31].rate = 40000000,

    .config_nodes[32].clk_node = CLK_NODE(g_ckgen_ip_ioc),
    .config_nodes[32].rate = 400000000,

    .config_nodes[33].clk_node = CLK_NODE(g_ckgen_ip_pt_sns_sf),
    .config_nodes[33].rate = 100000,

    .config_nodes[34].clk_node = CLK_NODE(g_ckgen_ip_sehc1),
    .config_nodes[34].rate = 50000000,

    .config_nodes[35].clk_node = CLK_NODE(g_ckgen_ip_sehc2),
    .config_nodes[35].rate = 50000000,

    .config_nodes[36].clk_node = CLK_NODE(g_ckgen_ip_i2s_mclk0),
    .config_nodes[36].rate = 12290000,

    .config_nodes[37].clk_node = CLK_NODE(g_ckgen_ip_i2s_mclk1),
    .config_nodes[37].rate = 12290000,

    .config_nodes[38].clk_node = CLK_NODE(g_ckgen_ip_saci1_clk),
    .config_nodes[38].rate = 12290000,

    .config_nodes[39].clk_node = CLK_NODE(g_ckgen_ip_saci1_pdm_clk),
    .config_nodes[39].rate = 12290000,

    .config_nodes[40].clk_node = CLK_NODE(g_ckgen_ip_saci2_clk),
    .config_nodes[40].rate = 12290000,

    .config_nodes[41].clk_node = CLK_NODE(g_ckgen_ip_saci2_pdm_clk),
    .config_nodes[41].rate = 12290000,

    .config_nodes[42].clk_node = CLK_NODE(g_ckgen_ip_pt_sns_ap),
    .config_nodes[42].rate = 100000,
};

/**
 * @brief g_enable_config contains IP clock enable/disable config
 *
 */
__WEAK const sdrv_ckgen_ip_clock_config_t g_enable_config = {
    .config_num = 108,

    .config_nodes[0].ip_nodes = g_ckgen_adc1,
    .config_nodes[0].mode = CKGEN_RUN_MODE,
    .config_nodes[0].enable = false,

    .config_nodes[1].ip_nodes = g_ckgen_adc2,
    .config_nodes[1].mode = CKGEN_RUN_MODE,
    .config_nodes[1].enable = false,

    .config_nodes[2].ip_nodes = g_ckgen_adc3,
    .config_nodes[2].mode = CKGEN_RUN_MODE,
    .config_nodes[2].enable = false,

    .config_nodes[3].ip_nodes = g_ckgen_acmp1,
    .config_nodes[3].mode = CKGEN_RUN_MODE,
    .config_nodes[3].enable = false,

    .config_nodes[4].ip_nodes = g_ckgen_acmp2,
    .config_nodes[4].mode = CKGEN_RUN_MODE,
    .config_nodes[4].enable = false,

    .config_nodes[5].ip_nodes = g_ckgen_acmp3,
    .config_nodes[5].mode = CKGEN_RUN_MODE,
    .config_nodes[5].enable = false,

    .config_nodes[6].ip_nodes = g_ckgen_acmp4,
    .config_nodes[6].mode = CKGEN_RUN_MODE,
    .config_nodes[6].enable = false,

    .config_nodes[7].ip_nodes = g_ckgen_btm1,
    .config_nodes[7].mode = CKGEN_RUN_MODE,
    .config_nodes[7].enable = false,

    .config_nodes[8].ip_nodes = g_ckgen_btm2,
    .config_nodes[8].mode = CKGEN_RUN_MODE,
    .config_nodes[8].enable = false,

    .config_nodes[9].ip_nodes = g_ckgen_btm3,
    .config_nodes[9].mode = CKGEN_RUN_MODE,
    .config_nodes[9].enable = false,

    .config_nodes[10].ip_nodes = g_ckgen_btm4,
    .config_nodes[10].mode = CKGEN_RUN_MODE,
    .config_nodes[10].enable = false,

    .config_nodes[11].ip_nodes = g_ckgen_btm5,
    .config_nodes[11].mode = CKGEN_RUN_MODE,
    .config_nodes[11].enable = false,

    .config_nodes[12].ip_nodes = g_ckgen_btm6,
    .config_nodes[12].mode = CKGEN_RUN_MODE,
    .config_nodes[12].enable = false,

    .config_nodes[13].ip_nodes = g_ckgen_canfd1,
    .config_nodes[13].mode = CKGEN_RUN_MODE,
    .config_nodes[13].enable = false,

    .config_nodes[14].ip_nodes = g_ckgen_canfd2,
    .config_nodes[14].mode = CKGEN_RUN_MODE,
    .config_nodes[14].enable = false,

    .config_nodes[15].ip_nodes = g_ckgen_canfd3,
    .config_nodes[15].mode = CKGEN_RUN_MODE,
    .config_nodes[15].enable = false,

    .config_nodes[16].ip_nodes = g_ckgen_canfd4,
    .config_nodes[16].mode = CKGEN_RUN_MODE,
    .config_nodes[16].enable = false,

    .config_nodes[17].ip_nodes = g_ckgen_canfd5,
    .config_nodes[17].mode = CKGEN_RUN_MODE,
    .config_nodes[17].enable = false,

    .config_nodes[18].ip_nodes = g_ckgen_canfd6,
    .config_nodes[18].mode = CKGEN_RUN_MODE,
    .config_nodes[18].enable = false,

    .config_nodes[19].ip_nodes = g_ckgen_canfd7,
    .config_nodes[19].mode = CKGEN_RUN_MODE,
    .config_nodes[19].enable = false,

    .config_nodes[20].ip_nodes = g_ckgen_canfd8,
    .config_nodes[20].mode = CKGEN_RUN_MODE,
    .config_nodes[20].enable = false,

    .config_nodes[21].ip_nodes = g_ckgen_canfd9,
    .config_nodes[21].mode = CKGEN_RUN_MODE,
    .config_nodes[21].enable = false,

    .config_nodes[22].ip_nodes = g_ckgen_canfd10,
    .config_nodes[22].mode = CKGEN_RUN_MODE,
    .config_nodes[22].enable = false,

    .config_nodes[23].ip_nodes = g_ckgen_canfd11,
    .config_nodes[23].mode = CKGEN_RUN_MODE,
    .config_nodes[23].enable = false,

    .config_nodes[24].ip_nodes = g_ckgen_canfd12,
    .config_nodes[24].mode = CKGEN_RUN_MODE,
    .config_nodes[24].enable = false,

    .config_nodes[25].ip_nodes = g_ckgen_canfd13,
    .config_nodes[25].mode = CKGEN_RUN_MODE,
    .config_nodes[25].enable = false,

    .config_nodes[26].ip_nodes = g_ckgen_canfd14,
    .config_nodes[26].mode = CKGEN_RUN_MODE,
    .config_nodes[26].enable = false,

    .config_nodes[27].ip_nodes = g_ckgen_canfd15,
    .config_nodes[27].mode = CKGEN_RUN_MODE,
    .config_nodes[27].enable = false,

    .config_nodes[28].ip_nodes = g_ckgen_canfd16,
    .config_nodes[28].mode = CKGEN_RUN_MODE,
    .config_nodes[28].enable = false,

    .config_nodes[29].ip_nodes = g_ckgen_canfd17,
    .config_nodes[29].mode = CKGEN_RUN_MODE,
    .config_nodes[29].enable = false,

    .config_nodes[30].ip_nodes = g_ckgen_canfd18,
    .config_nodes[30].mode = CKGEN_RUN_MODE,
    .config_nodes[30].enable = false,

    .config_nodes[31].ip_nodes = g_ckgen_canfd19,
    .config_nodes[31].mode = CKGEN_RUN_MODE,
    .config_nodes[31].enable = false,

    .config_nodes[32].ip_nodes = g_ckgen_canfd20,
    .config_nodes[32].mode = CKGEN_RUN_MODE,
    .config_nodes[32].enable = false,

    .config_nodes[33].ip_nodes = g_ckgen_canfd21,
    .config_nodes[33].mode = CKGEN_RUN_MODE,
    .config_nodes[33].enable = false,

    .config_nodes[34].ip_nodes = g_ckgen_canfd22,
    .config_nodes[34].mode = CKGEN_RUN_MODE,
    .config_nodes[34].enable = false,

    .config_nodes[35].ip_nodes = g_ckgen_canfd23,
    .config_nodes[35].mode = CKGEN_RUN_MODE,
    .config_nodes[35].enable = false,

    .config_nodes[36].ip_nodes = g_ckgen_canfd24,
    .config_nodes[36].mode = CKGEN_RUN_MODE,
    .config_nodes[36].enable = false,

    .config_nodes[37].ip_nodes = g_ckgen_dma_sf,
    .config_nodes[37].mode = CKGEN_RUN_MODE,
    .config_nodes[37].enable = false,

    .config_nodes[38].ip_nodes = g_ckgen_dma_ap,
    .config_nodes[38].mode = CKGEN_RUN_MODE,
    .config_nodes[38].enable = false,

    .config_nodes[39].ip_nodes = g_ckgen_enet1,
    .config_nodes[39].mode = CKGEN_RUN_MODE,
    .config_nodes[39].enable = false,

    .config_nodes[40].ip_nodes = g_ckgen_enet2,
    .config_nodes[40].mode = CKGEN_RUN_MODE,
    .config_nodes[40].enable = false,

    .config_nodes[41].ip_nodes = g_ckgen_epwm1,
    .config_nodes[41].mode = CKGEN_RUN_MODE,
    .config_nodes[41].enable = false,

    .config_nodes[42].ip_nodes = g_ckgen_epwm2,
    .config_nodes[42].mode = CKGEN_RUN_MODE,
    .config_nodes[42].enable = false,

    .config_nodes[43].ip_nodes = g_ckgen_epwm3,
    .config_nodes[43].mode = CKGEN_RUN_MODE,
    .config_nodes[43].enable = false,

    .config_nodes[44].ip_nodes = g_ckgen_epwm4,
    .config_nodes[44].mode = CKGEN_RUN_MODE,
    .config_nodes[44].enable = false,

    .config_nodes[45].ip_nodes = g_ckgen_etmr1,
    .config_nodes[45].mode = CKGEN_RUN_MODE,
    .config_nodes[45].enable = false,

    .config_nodes[46].ip_nodes = g_ckgen_etmr2,
    .config_nodes[46].mode = CKGEN_RUN_MODE,
    .config_nodes[46].enable = false,

    .config_nodes[47].ip_nodes = g_ckgen_etmr3,
    .config_nodes[47].mode = CKGEN_RUN_MODE,
    .config_nodes[47].enable = false,

    .config_nodes[48].ip_nodes = g_ckgen_etmr4,
    .config_nodes[48].mode = CKGEN_RUN_MODE,
    .config_nodes[48].enable = false,

    .config_nodes[49].ip_nodes = g_ckgen_flexray1,
    .config_nodes[49].mode = CKGEN_RUN_MODE,
    .config_nodes[49].enable = false,

    .config_nodes[50].ip_nodes = g_ckgen_flexray2,
    .config_nodes[50].mode = CKGEN_RUN_MODE,
    .config_nodes[50].enable = false,

    .config_nodes[51].ip_nodes = g_ckgen_gama1,
    .config_nodes[51].mode = CKGEN_RUN_MODE,
    .config_nodes[51].enable = false,

    .config_nodes[52].ip_nodes = g_ckgen_i2c1,
    .config_nodes[52].mode = CKGEN_RUN_MODE,
    .config_nodes[52].enable = false,

    .config_nodes[53].ip_nodes = g_ckgen_i2c2,
    .config_nodes[53].mode = CKGEN_RUN_MODE,
    .config_nodes[53].enable = false,

    .config_nodes[54].ip_nodes = g_ckgen_i2c3,
    .config_nodes[54].mode = CKGEN_RUN_MODE,
    .config_nodes[54].enable = false,

    .config_nodes[55].ip_nodes = g_ckgen_i2c4,
    .config_nodes[55].mode = CKGEN_RUN_MODE,
    .config_nodes[55].enable = false,

    .config_nodes[56].ip_nodes = g_ckgen_i2c5,
    .config_nodes[56].mode = CKGEN_RUN_MODE,
    .config_nodes[56].enable = false,

    .config_nodes[57].ip_nodes = g_ckgen_i2c6,
    .config_nodes[57].mode = CKGEN_RUN_MODE,
    .config_nodes[57].enable = false,

    .config_nodes[58].ip_nodes = g_ckgen_i2c7,
    .config_nodes[58].mode = CKGEN_RUN_MODE,
    .config_nodes[58].enable = false,

    .config_nodes[59].ip_nodes = g_ckgen_i2c8,
    .config_nodes[59].mode = CKGEN_RUN_MODE,
    .config_nodes[59].enable = false,

    .config_nodes[60].ip_nodes = g_ckgen_ioc,
    .config_nodes[60].mode = CKGEN_RUN_MODE,
    .config_nodes[60].enable = false,

    .config_nodes[61].ip_nodes = g_ckgen_mb,
    .config_nodes[61].mode = CKGEN_RUN_MODE,
    .config_nodes[61].enable = false,

    .config_nodes[62].ip_nodes = g_ckgen_pt_sns_ap,
    .config_nodes[62].mode = CKGEN_RUN_MODE,
    .config_nodes[62].enable = false,

    .config_nodes[63].ip_nodes = g_ckgen_pt_sns_sf_ana,
    .config_nodes[63].mode = CKGEN_RUN_MODE,
    .config_nodes[63].enable = false,

    .config_nodes[64].ip_nodes = g_ckgen_pt_sns_sf_dig,
    .config_nodes[64].mode = CKGEN_RUN_MODE,
    .config_nodes[64].enable = false,

    .config_nodes[65].ip_nodes = g_ckgen_saci1,
    .config_nodes[65].mode = CKGEN_RUN_MODE,
    .config_nodes[65].enable = false,

    .config_nodes[66].ip_nodes = g_ckgen_saci2,
    .config_nodes[66].mode = CKGEN_RUN_MODE,
    .config_nodes[66].enable = false,

    .config_nodes[67].ip_nodes = g_ckgen_sehc1,
    .config_nodes[67].mode = CKGEN_RUN_MODE,
    .config_nodes[67].enable = false,

    .config_nodes[68].ip_nodes = g_ckgen_sehc2,
    .config_nodes[68].mode = CKGEN_RUN_MODE,
    .config_nodes[68].enable = false,

    .config_nodes[69].ip_nodes = g_ckgen_seip,
    .config_nodes[69].mode = CKGEN_RUN_MODE,
    .config_nodes[69].enable = false,

    .config_nodes[70].ip_nodes = g_ckgen_spi1,
    .config_nodes[70].mode = CKGEN_RUN_MODE,
    .config_nodes[70].enable = false,

    .config_nodes[71].ip_nodes = g_ckgen_spi2,
    .config_nodes[71].mode = CKGEN_RUN_MODE,
    .config_nodes[71].enable = false,

    .config_nodes[72].ip_nodes = g_ckgen_spi3,
    .config_nodes[72].mode = CKGEN_RUN_MODE,
    .config_nodes[72].enable = false,

    .config_nodes[73].ip_nodes = g_ckgen_spi4,
    .config_nodes[73].mode = CKGEN_RUN_MODE,
    .config_nodes[73].enable = false,

    .config_nodes[74].ip_nodes = g_ckgen_spi5,
    .config_nodes[74].mode = CKGEN_RUN_MODE,
    .config_nodes[74].enable = false,

    .config_nodes[75].ip_nodes = g_ckgen_spi6,
    .config_nodes[75].mode = CKGEN_RUN_MODE,
    .config_nodes[75].enable = false,

    .config_nodes[76].ip_nodes = g_ckgen_spi7,
    .config_nodes[76].mode = CKGEN_RUN_MODE,
    .config_nodes[76].enable = false,

    .config_nodes[77].ip_nodes = g_ckgen_spi8,
    .config_nodes[77].mode = CKGEN_RUN_MODE,
    .config_nodes[77].enable = false,

    .config_nodes[78].ip_nodes = g_ckgen_uart1,
    .config_nodes[78].mode = CKGEN_RUN_MODE,
    .config_nodes[78].enable = false,

    .config_nodes[79].ip_nodes = g_ckgen_uart2,
    .config_nodes[79].mode = CKGEN_RUN_MODE,
    .config_nodes[79].enable = false,

    .config_nodes[80].ip_nodes = g_ckgen_uart3,
    .config_nodes[80].mode = CKGEN_RUN_MODE,
    .config_nodes[80].enable = false,

    .config_nodes[81].ip_nodes = g_ckgen_uart4,
    .config_nodes[81].mode = CKGEN_RUN_MODE,
    .config_nodes[81].enable = false,

    .config_nodes[82].ip_nodes = g_ckgen_uart5,
    .config_nodes[82].mode = CKGEN_RUN_MODE,
    .config_nodes[82].enable = false,

    .config_nodes[83].ip_nodes = g_ckgen_uart6,
    .config_nodes[83].mode = CKGEN_RUN_MODE,
    .config_nodes[83].enable = false,

    .config_nodes[84].ip_nodes = g_ckgen_uart7,
    .config_nodes[84].mode = CKGEN_RUN_MODE,
    .config_nodes[84].enable = false,

    .config_nodes[85].ip_nodes = g_ckgen_uart8,
    .config_nodes[85].mode = CKGEN_RUN_MODE,
    .config_nodes[85].enable = false,

    .config_nodes[86].ip_nodes = g_ckgen_uart9,
    .config_nodes[86].mode = CKGEN_RUN_MODE,
    .config_nodes[86].enable = false,

    .config_nodes[87].ip_nodes = g_ckgen_uart10,
    .config_nodes[87].mode = CKGEN_RUN_MODE,
    .config_nodes[87].enable = false,

    .config_nodes[88].ip_nodes = g_ckgen_uart11,
    .config_nodes[88].mode = CKGEN_RUN_MODE,
    .config_nodes[88].enable = false,

    .config_nodes[89].ip_nodes = g_ckgen_uart12,
    .config_nodes[89].mode = CKGEN_RUN_MODE,
    .config_nodes[89].enable = false,

    .config_nodes[90].ip_nodes = g_ckgen_uart13,
    .config_nodes[90].mode = CKGEN_RUN_MODE,
    .config_nodes[90].enable = false,

    .config_nodes[91].ip_nodes = g_ckgen_uart14,
    .config_nodes[91].mode = CKGEN_RUN_MODE,
    .config_nodes[91].enable = false,

    .config_nodes[92].ip_nodes = g_ckgen_uart15,
    .config_nodes[92].mode = CKGEN_RUN_MODE,
    .config_nodes[92].enable = false,

    .config_nodes[93].ip_nodes = g_ckgen_uart16,
    .config_nodes[93].mode = CKGEN_RUN_MODE,
    .config_nodes[93].enable = false,

    .config_nodes[94].ip_nodes = g_ckgen_usb,
    .config_nodes[94].mode = CKGEN_RUN_MODE,
    .config_nodes[94].enable = false,

    .config_nodes[95].ip_nodes = g_ckgen_wdt1,
    .config_nodes[95].mode = CKGEN_RUN_MODE,
    .config_nodes[95].enable = false,

    .config_nodes[96].ip_nodes = g_ckgen_wdt2,
    .config_nodes[96].mode = CKGEN_RUN_MODE,
    .config_nodes[96].enable = false,

    .config_nodes[97].ip_nodes = g_ckgen_wdt3,
    .config_nodes[97].mode = CKGEN_RUN_MODE,
    .config_nodes[97].enable = false,

    .config_nodes[98].ip_nodes = g_ckgen_wdt4,
    .config_nodes[98].mode = CKGEN_RUN_MODE,
    .config_nodes[98].enable = false,

    .config_nodes[99].ip_nodes = g_ckgen_wdt5,
    .config_nodes[99].mode = CKGEN_RUN_MODE,
    .config_nodes[99].enable = false,

    .config_nodes[100].ip_nodes = g_ckgen_wdt6,
    .config_nodes[100].mode = CKGEN_RUN_MODE,
    .config_nodes[100].enable = false,

    .config_nodes[101].ip_nodes = g_ckgen_wdt8,
    .config_nodes[101].mode = CKGEN_RUN_MODE,
    .config_nodes[101].enable = false,

    .config_nodes[102].ip_nodes = g_ckgen_xspi1a,
    .config_nodes[102].mode = CKGEN_RUN_MODE,
    .config_nodes[102].enable = false,

    .config_nodes[103].ip_nodes = g_ckgen_xspi1b,
    .config_nodes[103].mode = CKGEN_RUN_MODE,
    .config_nodes[103].enable = false,

    .config_nodes[104].ip_nodes = g_ckgen_xspi2a,
    .config_nodes[104].mode = CKGEN_RUN_MODE,
    .config_nodes[104].enable = false,

    .config_nodes[105].ip_nodes = g_ckgen_xspi2b,
    .config_nodes[105].mode = CKGEN_RUN_MODE,
    .config_nodes[105].enable = false,

    .config_nodes[106].ip_nodes = g_ckgen_xspi_slv,
    .config_nodes[106].mode = CKGEN_RUN_MODE,
    .config_nodes[106].enable = false,

    .config_nodes[107].ip_nodes = g_ckgen_xtrg,
    .config_nodes[107].mode = CKGEN_RUN_MODE,
    .config_nodes[107].enable = false,
};

#if CONFIG_CLK_DUMP

__WEAK sdrv_clk_config_t g_clk_config = {
    .config_num = 63,

    .config_nodes[0].ckgen_ref = CLK_NODE(g_rc_32k),
    .config_nodes[0].name = "rc32k",

    .config_nodes[1].ckgen_ref = CLK_NODE(g_fs_32k),
    .config_nodes[1].name = "fs32k",

    .config_nodes[2].ckgen_ref = CLK_NODE(g_rc_24m),
    .config_nodes[2].name = "rc24m",

    .config_nodes[3].ckgen_ref = CLK_NODE(g_fs_24m),
    .config_nodes[3].name = "fs24m",

    .config_nodes[4].ckgen_ref = CLK_NODE(g_pll1_root),
    .config_nodes[4].name = "pll1",

    .config_nodes[5].ckgen_ref = CLK_NODE(g_pll2_root),
    .config_nodes[5].name = "pll2",

    .config_nodes[6].ckgen_ref = CLK_NODE(g_pll3_root),
    .config_nodes[6].name = "pll3",

    .config_nodes[7].ckgen_ref = CLK_NODE(g_ckgen_bus_cr5_sf),
    .config_nodes[7].name = "cr5_sf",

    .config_nodes[8].ckgen_ref = CLK_NODE(g_ckgen_bus_cr5_sp),
    .config_nodes[8].name = "cr5_sp",

    .config_nodes[9].ckgen_ref = CLK_NODE(g_ckgen_core_cr5_sx),
    .config_nodes[9].name = "cr5_sx",

    .config_nodes[10].ckgen_ref = CLK_NODE(g_ckgen_ip_i2c_sf_1_to_4),
    .config_nodes[10].name = "i2c_sf_1_to_4",

    .config_nodes[11].ckgen_ref = CLK_NODE(g_ckgen_ip_i2c_sf_5_to_8),
    .config_nodes[11].name = "i2c_sf_5_to_8",

    .config_nodes[12].ckgen_ref = CLK_NODE(g_ckgen_ip_spi_sf_1_to_4),
    .config_nodes[12].name = "spi_sf_1_to_4",

    .config_nodes[13].ckgen_ref = CLK_NODE(g_ckgen_ip_spi_sf_5_to_8),
    .config_nodes[13].name = "spi_sf_5_to_8",

    .config_nodes[14].ckgen_ref = CLK_NODE(g_ckgen_ip_uart_sf_1_to_8),
    .config_nodes[14].name = "uart_sf_1_to_8",

    .config_nodes[15].ckgen_ref = CLK_NODE(g_ckgen_ip_uart_sf_9_to_16),
    .config_nodes[15].name = "uart_sf_9_to_16",

    .config_nodes[16].ckgen_ref = CLK_NODE(g_ckgen_ip_enet1_tx),
    .config_nodes[16].name = "enet1_tx",

    .config_nodes[17].ckgen_ref = CLK_NODE(g_ckgen_ip_enet1_rmii),
    .config_nodes[17].name = "enet1_rmii",

    .config_nodes[18].ckgen_ref = CLK_NODE(g_ckgen_ip_enet1_phy_ref),
    .config_nodes[18].name = "enet1_phy_ref",

    .config_nodes[19].ckgen_ref = CLK_NODE(g_ckgen_ip_enet1_timer_sec),
    .config_nodes[19].name = "enet1_timer_sec",

    .config_nodes[20].ckgen_ref = CLK_NODE(g_ckgen_ip_enet2_tx),
    .config_nodes[20].name = "enet2_tx",

    .config_nodes[21].ckgen_ref = CLK_NODE(g_ckgen_ip_enet2_rmii),
    .config_nodes[21].name = "enet2_rmii",

    .config_nodes[22].ckgen_ref = CLK_NODE(g_ckgen_ip_enet2_phy_ref),
    .config_nodes[22].name = "enet2_phy_ref",

    .config_nodes[23].ckgen_ref = CLK_NODE(g_ckgen_ip_xspi1a),
    .config_nodes[23].name = "xspi1a",

    .config_nodes[24].ckgen_ref = CLK_NODE(g_ckgen_ip_xspi1b),
    .config_nodes[24].name = "xspi1b",

    .config_nodes[25].ckgen_ref = CLK_NODE(g_ckgen_ip_xspi2a),
    .config_nodes[25].name = "xspi2a",

    .config_nodes[26].ckgen_ref = CLK_NODE(g_ckgen_ip_xspi2b),
    .config_nodes[26].name = "xspi2b",

    .config_nodes[27].ckgen_ref = CLK_NODE(g_ckgen_ip_xtrg),
    .config_nodes[27].name = "xtrg",

    .config_nodes[28].ckgen_ref = CLK_NODE(g_ckgen_ip_etmr1),
    .config_nodes[28].name = "etmr1",

    .config_nodes[29].ckgen_ref = CLK_NODE(g_ckgen_ip_etmr2),
    .config_nodes[29].name = "etmr2",

    .config_nodes[30].ckgen_ref = CLK_NODE(g_ckgen_ip_etmr3),
    .config_nodes[30].name = "etmr3",

    .config_nodes[31].ckgen_ref = CLK_NODE(g_ckgen_ip_etmr4),
    .config_nodes[31].name = "etmr4",

    .config_nodes[32].ckgen_ref = CLK_NODE(g_ckgen_ip_epwm1),
    .config_nodes[32].name = "epwm1",

    .config_nodes[33].ckgen_ref = CLK_NODE(g_ckgen_ip_epwm2),
    .config_nodes[33].name = "epwm2",

    .config_nodes[34].ckgen_ref = CLK_NODE(g_ckgen_ip_epwm3),
    .config_nodes[34].name = "epwm3",

    .config_nodes[35].ckgen_ref = CLK_NODE(g_ckgen_ip_epwm4),
    .config_nodes[35].name = "epwm4",

    .config_nodes[36].ckgen_ref = CLK_NODE(g_ckgen_ip_can),
    .config_nodes[36].name = "can",

    .config_nodes[37].ckgen_ref = CLK_NODE(g_ckgen_ip_adc1),
    .config_nodes[37].name = "adc1",

    .config_nodes[38].ckgen_ref = CLK_NODE(g_ckgen_ip_adc2),
    .config_nodes[38].name = "adc2",

    .config_nodes[39].ckgen_ref = CLK_NODE(g_ckgen_ip_adc3),
    .config_nodes[39].name = "adc3",

    .config_nodes[40].ckgen_ref = CLK_NODE(g_ckgen_ip_acmp),
    .config_nodes[40].name = "acmp",

    .config_nodes[41].ckgen_ref = CLK_NODE(g_ckgen_ip_flexray),
    .config_nodes[41].name = "flexray",

    .config_nodes[42].ckgen_ref = CLK_NODE(g_ckgen_ip_ioc),
    .config_nodes[42].name = "ioc",

    .config_nodes[43].ckgen_ref = CLK_NODE(g_ckgen_ip_pt_sns_sf),
    .config_nodes[43].name = "pt_sns_sf",

    .config_nodes[44].ckgen_ref = CLK_NODE(g_pll4_root),
    .config_nodes[44].name = "pll4",

    .config_nodes[45].ckgen_ref = CLK_NODE(g_pll5_root),
    .config_nodes[45].name = "pll5",

    .config_nodes[46].ckgen_ref = CLK_NODE(g_pll_lvds_root),
    .config_nodes[46].name = "pll_lvds",

    .config_nodes[47].ckgen_ref = CLK_NODE(g_pll_lvds_nodiv),
    .config_nodes[47].name = "lvds_clk0",

    .config_nodes[48].ckgen_ref = CLK_NODE(g_pll_lvds_div2),
    .config_nodes[48].name = "lvds_clk1",

    .config_nodes[49].ckgen_ref = CLK_NODE(g_pll_lvds_div7),
    .config_nodes[49].name = "lvds_clk2",

    .config_nodes[50].ckgen_ref = CLK_NODE(g_pll_lvds_ckgen),
    .config_nodes[50].name = "lvds_clk3",

    .config_nodes[51].ckgen_ref = CLK_NODE(g_ckgen_bus_ap_bus),
    .config_nodes[51].name = "ap_bus",

    .config_nodes[52].ckgen_ref = CLK_NODE(g_ckgen_bus_disp_bus),
    .config_nodes[52].name = "disp_bus",

    .config_nodes[53].ckgen_ref = CLK_NODE(g_ckgen_bus_seip),
    .config_nodes[53].name = "seip_bus",

    .config_nodes[54].ckgen_ref = CLK_NODE(g_ckgen_ip_sehc1),
    .config_nodes[54].name = "sehc1",

    .config_nodes[55].ckgen_ref = CLK_NODE(g_ckgen_ip_sehc2),
    .config_nodes[55].name = "sehc2",

    .config_nodes[56].ckgen_ref = CLK_NODE(g_ckgen_ip_i2s_mclk0),
    .config_nodes[56].name = "i2s_mclk0",

    .config_nodes[57].ckgen_ref = CLK_NODE(g_ckgen_ip_i2s_mclk1),
    .config_nodes[57].name = "i2s_mclk1",

    .config_nodes[58].ckgen_ref = CLK_NODE(g_ckgen_ip_saci1_clk),
    .config_nodes[58].name = "saci1_clk",

    .config_nodes[59].ckgen_ref = CLK_NODE(g_ckgen_ip_saci1_pdm_clk),
    .config_nodes[59].name = "saci1_pdm_clk",

    .config_nodes[60].ckgen_ref = CLK_NODE(g_ckgen_ip_saci2_clk),
    .config_nodes[60].name = "saci2_clk",

    .config_nodes[61].ckgen_ref = CLK_NODE(g_ckgen_ip_saci2_pdm_clk),
    .config_nodes[61].name = "saci2_pdm_clk",

    .config_nodes[62].ckgen_ref = CLK_NODE(g_ckgen_ip_pt_sns_ap),
    .config_nodes[62].name = "pt_sns_ap",
};

#endif /* CONFIG_CLK_DUMP */
