/*
 * core_id.h
 *
 * Copyright (c) 2020 Semidrive Semiconductor.
 * All rights reserved.
 *
 * Description: E3 core id interface.
 *
 * Revision History:
 * -----------------
 */

#ifndef _CORE_ID_H_
#define _CORE_ID_H_

#ifndef ASSEMBLY
#include "armv7-r/register.h"

/* core numbers */
#define CORE_NUM    5

/* core id */
#define CORE_SF     0
#define CORE_SP0    1
#define CORE_SP1    2
#define CORE_SX0    3
#define CORE_SX1    4

/* core numbers in SMP (Symmetrical Multi-Processing) system */
#if CONFIG_MULTI_CORE_SMP
#define CORE_NUM_SMP    CORE_NUM
#else
#define CORE_NUM_SMP    1
#endif

/**
 * @brief Get the core id
 *
 * SF   0
 * SP0  1
 * SP1  2
 * SX0  3
 * SX1  4
 *
 * @return core id
 */
static inline int get_core_id(void)
{
    uint32_t mpidr = arm_read_mpidr();
    /* SF only lockstep 0x0000
     * SP0 0x0100, SP1 0x0101
     * SX0 0x0200, SX1 0x0201
     */
    uint8_t cluster_id = (mpidr >> 8) & 0xFF;
    uint8_t core_id = mpidr & 0xFF;

    return cluster_id ?
           (((cluster_id << 1) - 1) + core_id) :
           core_id;
}

/* Get the core id in SMP (Symmetrical Multi-Processing) system */
#if CONFIG_MULTI_CORE_SMP
#define get_core_id_smp() get_core_id()
#else
#define get_core_id_smp() 0
#endif

#endif

#endif
