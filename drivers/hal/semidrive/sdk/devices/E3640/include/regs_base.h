//*****************************************************************************
//
// WARNING: Automatically generated file, don't modify anymore!!!
//
// Copyright (c) 2021-2029 Semidrive Incorporated.  All rights reserved.
// Software License Agreement
//
//*****************************************************************************

#ifndef REGS_BASE_H
#define REGS_BASE_H

/*------------------------------------------------------
 * Mailbox
 *------------------------------------------------------*/

/* Mailbox Register */
#define MB_REG_BASE (0xF4000000ul)

/* Mailbox Buffer */
#define MB_MEM_BASE (0x02001000ul)


/*------------------------------------------------------
 * Interrupt
 *------------------------------------------------------*/

/* Vectored Interrupt Controller (VIC) 3 Port B Base Address, for SX1 Core (cluster 1, core 1) */
#define VIC3PORTB_BASE (0xF1C30000ul)

/* Vectored Interrupt Controller (VIC) 3 Port A Base Address, for SX0 Core (cluster 1, core 0) */
#define VIC3PORTA_BASE (0xF1C20000ul)

/* Vectored Interrupt Controller (VIC) 2 Port B Base Address, for SP1 Core (cluster 2, core 1) */
#define VIC2PORTB_BASE (0xF2C10000ul)

/* Vectored Interrupt Controller (VIC) 2 Port A Base Address, for SP0 Core (cluster 1, core 0) */
#define VIC2PORTA_BASE (0xF2C00000ul)

/* Vectored Interrupt Controller (VIC) 1 Base Address, for SF Core (cluster 0) */
#define VIC1_BASE (0xF1C00000ul)


/*------------------------------------------------------
 * Bus Inter-Connect
 *------------------------------------------------------*/

/* Configuration Register for FAB_DISP Fabric */
#define APB_GPV_DISP_BASE (0xF34F0000ul)

/* Configuration Register for FAB_AP Fabric */
#define APB_GPV_AP_BASE (0xF31B0000ul)

/* Configuration Register for FAB_SP Fabric */
#define APB_GPV_SP_BASE (0xF2180000ul)

/* Configuration Register for FAB_SX Fabric */
#define APB_GPV_SX_BASE (0xF07A0000ul)

/* Configuration Register for FAB_XB Fabric */
#define APB_GPV_XB_BASE (0xF0790000ul)

/* Configuration Register for FAB_SAFETY Fabric */
#define APB_GPV_SF_BASE (0xF0780000ul)

/* Configuration Register for FAB_AP Fabric */
#define APB_GPV_AAHB_BASE (0xF0770000ul)


/*------------------------------------------------------
 * Display
 *------------------------------------------------------*/

/* Display Mux */
#define APB_DISP_MUX_BASE (0xF34C0000ul)


/*------------------------------------------------------
 * DMA
 *------------------------------------------------------*/

/* AP Domain Direct Memory Access (DMA) */
#define APB_DMA_AP_BASE (0xF3180000ul)

/* SF Domain Direct Memory Access (DMA) 1 */
#define APB_DMA_SF1_BASE (0xF0540000ul)

/* SF Domain Direct Memory Access (DMA) 0 */
#define APB_DMA_SF0_BASE (0xF0500000ul)


/*------------------------------------------------------
 * Peripherals
 *------------------------------------------------------*/

/* AP Domain General Porpose Intput Output (GPIO) */
#define APB_GPIO_AP_BASE (0xF3120000ul)

/* SF Domain General Porpose Intput Output (GPIO) */
#define APB_GPIO_SF_BASE (0xF0740000ul)

/* CAN with Flexible Data rate (CANFD) 1 */
#define APB_CANFD1_BASE (0xF0580000ul)

/* CAN with Flexible Data rate (CANFD) 2 */
#define APB_CANFD2_BASE (0xF0590000ul)

/* CAN with Flexible Data rate (CANFD) 3 */
#define APB_CANFD3_BASE (0xF05A0000ul)

/* CAN with Flexible Data rate (CANFD) 4 */
#define APB_CANFD4_BASE (0xF05B0000ul)

/* CAN with Flexible Data rate (CANFD) 5 */
#define APB_CANFD5_BASE (0xF0AD0000ul)

/* CAN with Flexible Data rate (CANFD) 6 */
#define APB_CANFD6_BASE (0xF0EB0000ul)

/* CAN with Flexible Data rate (CANFD) 7 */
#define APB_CANFD7_BASE (0xF0AE0000ul)

/* CAN with Flexible Data rate (CANFD) 8 */
#define APB_CANFD8_BASE (0xF0EC0000ul)

/* CAN with Flexible Data rate (CANFD) 9 */
#define APB_CANFD9_BASE (0xF0AF0000ul)

/* CAN with Flexible Data rate (CANFD) 10 */
#define APB_CANFD10_BASE (0xF0ED0000ul)

/* CAN with Flexible Data rate (CANFD) 11 */
#define APB_CANFD11_BASE (0xF0B00000ul)

/* CAN with Flexible Data rate (CANFD) 12 */
#define APB_CANFD12_BASE (0xF0EE0000ul)

/* CAN with Flexible Data rate (CANFD) 13 */
#define APB_CANFD13_BASE (0xF0B10000ul)

/* CAN with Flexible Data rate (CANFD) 14 */
#define APB_CANFD14_BASE (0xF0EF0000ul)

/* CAN with Flexible Data rate (CANFD) 15 */
#define APB_CANFD15_BASE (0xF0B20000ul)

/* CAN with Flexible Data rate (CANFD) 16 */
#define APB_CANFD16_BASE (0xF0F00000ul)

/* CAN with Flexible Data rate (CANFD) 17 */
#define APB_CANFD17_BASE (0xF0B30000ul)

/* CAN with Flexible Data rate (CANFD) 18 */
#define APB_CANFD18_BASE (0xF0F10000ul)

/* CAN with Flexible Data rate (CANFD) 19 */
#define APB_CANFD19_BASE (0xF0B40000ul)

/* CAN with Flexible Data rate (CANFD) 20 */
#define APB_CANFD20_BASE (0xF0F20000ul)

/* CAN with Flexible Data rate (CANFD) 21 */
#define APB_CANFD21_BASE (0xF0B50000ul)

/* CAN with Flexible Data rate (CANFD) 22 */
#define APB_CANFD22_BASE (0xF0F30000ul)

/* CAN with Flexible Data rate (CANFD) 23 */
#define APB_CANFD23_BASE (0xF0B60000ul)

/* CAN with Flexible Data rate (CANFD) 24 */
#define APB_CANFD24_BASE (0xF0F40000ul)

/* Universal Asynchronous Receiver/Transmitter (UART) 1 */
#define APB_UART1_BASE (0xF0930000ul)

/* Universal Asynchronous Receiver/Transmitter (UART) 2 */
#define APB_UART2_BASE (0xF0C60000ul)

/* Universal Asynchronous Receiver/Transmitter (UART) 3 */
#define APB_UART3_BASE (0xF0940000ul)

/* Universal Asynchronous Receiver/Transmitter (UART) 4 */
#define APB_UART4_BASE (0xF0C70000ul)

/* Universal Asynchronous Receiver/Transmitter (UART) 5 */
#define APB_UART5_BASE (0xF0950000ul)

/* Universal Asynchronous Receiver/Transmitter (UART) 6 */
#define APB_UART6_BASE (0xF0C80000ul)

/* Universal Asynchronous Receiver/Transmitter (UART) 7 */
#define APB_UART7_BASE (0xF0960000ul)

/* Universal Asynchronous Receiver/Transmitter (UART) 8 */
#define APB_UART8_BASE (0xF0C90000ul)

/* Universal Asynchronous Receiver/Transmitter (UART) 9 */
#define APB_UART9_BASE (0xF0970000ul)

/* Universal Asynchronous Receiver/Transmitter (UART) 10 */
#define APB_UART10_BASE (0xF0CA0000ul)

/* Universal Asynchronous Receiver/Transmitter (UART) 11 */
#define APB_UART11_BASE (0xF0980000ul)

/* Universal Asynchronous Receiver/Transmitter (UART) 12 */
#define APB_UART12_BASE (0xF0CB0000ul)

/* Universal Asynchronous Receiver/Transmitter (UART) 13 */
#define APB_UART13_BASE (0xF0990000ul)

/* Universal Asynchronous Receiver/Transmitter (UART) 14 */
#define APB_UART14_BASE (0xF0CC0000ul)

/* Universal Asynchronous Receiver/Transmitter (UART) 15 */
#define APB_UART15_BASE (0xF09A0000ul)

/* Universal Asynchronous Receiver/Transmitter (UART) 16 */
#define APB_UART16_BASE (0xF0CD0000ul)

/* Ethernet (ENET) 1 */
#define APB_ENET1_BASE (0xF2010000ul)

/* Ethernet (ENET) 2 */
#define APB_ENET2_BASE (0xF2020000ul)

/* Inter-Integrated Circuit (I2C) 1 */
#define APB_I2C1_BASE (0xF0A30000ul)

/* Inter-Integrated Circuit (I2C) 2 */
#define APB_I2C2_BASE (0xF0DD0000ul)

/* Inter-Integrated Circuit (I2C) 3 */
#define APB_I2C3_BASE (0xF0A40000ul)

/* Inter-Integrated Circuit (I2C) 4 */
#define APB_I2C4_BASE (0xF0DE0000ul)

/* Inter-Integrated Circuit (I2C) 5 */
#define APB_I2C5_BASE (0xF0A50000ul)

/* Inter-Integrated Circuit (I2C) 6 */
#define APB_I2C6_BASE (0xF0DF0000ul)

/* Inter-Integrated Circuit (I2C) 7 */
#define APB_I2C7_BASE (0xF0A60000ul)

/* Inter-Integrated Circuit (I2C) 8 */
#define APB_I2C8_BASE (0xF0E00000ul)

/* Serial Peripheral Interface (SPI) 1 */
#define APB_SPI1_BASE (0xF09F0000ul)

/* Serial Peripheral Interface (SPI) 2 */
#define APB_SPI2_BASE (0xF0D90000ul)

/* Serial Peripheral Interface (SPI) 3 */
#define APB_SPI3_BASE (0xF0A00000ul)

/* Serial Peripheral Interface (SPI) 4 */
#define APB_SPI4_BASE (0xF0DA0000ul)

/* Serial Peripheral Interface (SPI) 5 */
#define APB_SPI5_BASE (0xF0A10000ul)

/* Serial Peripheral Interface (SPI) 6 */
#define APB_SPI6_BASE (0xF0DB0000ul)

/* Serial Peripheral Interface (SPI) 7 */
#define APB_SPI7_BASE (0xF0A20000ul)

/* Serial Peripheral Interface (SPI) 8 */
#define APB_SPI8_BASE (0xF0DC0000ul)

/* Flexray Controller 1 */
#define APB_FLEXRAY1_BASE (0xF0E60000ul)

/* Flexray Controller 2 */
#define APB_FLEXRAY2_BASE (0xF0E50000ul)


/*------------------------------------------------------
 * Security
 *------------------------------------------------------*/

/* Secure Element Intellectual Property */
#define APB_SEIP_BASE (0xF3100000ul)

/* SEIP Kbuf */
#define APB_KBUF_BASE (0xF21DE000ul)

/* SEIP APB System Reg */
#define APB_APBSYSTEMREG_BASE (0xF21DD800ul)

/* SEIP UART0 */
#define APB_SEIP_UART0_BASE (0xF21DD000ul)

/* SEIP Mailbox */
#define APB_SEIP_MAILBOX_BASE (0xF21D8000ul)

/* SEIP AHB System Reg */
#define APB_AHBSYSTEMREG_BASE (0xF21D0000ul)

/* SEIP HFE */
#define APB_SEIP_HFE_BASE (0xF21CC000ul)

/* SEIP HKE */
#define APB_SEIP_SKE_BASE (0xF21C8000ul)

/* SEIP TRNG */
#define APB_SEIP_TRNG_BASE (0xF21C4000ul)

/* SEIP PKE */
#define APB_SEIP_PKE_BASE (0xF21C0000ul)

/* SDIP SRAM1 */
#define APB_SEIP_SRAM1_BASE (0xF21A0000ul)

/* SEIP SRAM0 */
#define APB_SEIP_SRAM0_BASE (0xF2180000ul)

/* SEIP CPU FIO */
#define APB_SEIP_CPU_FIO_BASE (0xF2160000ul)

/* SEIP CPU PLIC */
#define APB_SEIP_CPU_PLIC_BASE (0xF2150000ul)

/* SEIP CPU CLINT */
#define APB_SEIP_CPU_CLINT_BASE (0xF2140000ul)

/* SEIP DTCM */
#define APB_SEIP_DTCM_BASE (0xF2120000ul)

/* SEIP ITCM */
#define APB_SEIP_ITCM_BASE (0xF2100000ul)

/* E-Fuse controller */
#define APB_EFUSEC_BASE (0xF05F0000ul)

/* Secure Storage */
#define APB_SEC_STORAGE_BASE (0xF00A0000ul)

/* Tamper Monitor */
#define APB_TM_BASE (0xF0080000ul)

/* SEIP Secured Key Buffer */
#define SEIP_SEIPSECUREDKEYBUFFER_BASE (0x02200000ul)

/* SEIP */
#define SEIP_SEIP_BASE (0x02100000ul)

/*------------------------------------------------------
 * System Control
 *------------------------------------------------------*/

/* AP Domain Status and Controller Register (SCR) */
#define APB_SCR_AP_BASE (0xF30F0000ul)

/* SF Domain Status and Controller Register (SCR) */
#define APB_SCR_SF_BASE (0xF0680000ul)

/*------------------------------------------------------
 * I/O Control
 *------------------------------------------------------*/

/* AP Domain IO Multiplexing Controller(IOMUXC) */
#define APB_IOMUXC_AP_BASE (0xF30E0000ul)

/* SF Domain IO Multiplexing Controller(IOMUXC) */
#define APB_IOMUXC_SF_BASE (0xF0630000ul)

/* RTC Domain IO Multiplexing Controller (IOMUXC) */
#define APB_IOMUXC_RTC_BASE (0xF0070000ul)


/*------------------------------------------------------
 * Clock & Reset
 *------------------------------------------------------*/

/* AP Domain Reset Generator (RSTGEN) */
#define APB_RSTGEN_AP_BASE (0xF30D0000ul)

/* AP Domain Clock Generator (CKGEN) */
#define APB_CKGEN_AP_BASE (0xF30C0000ul)

/* Phase Locked Loop (PLL) LVDS */
#define APB_PLL_LVDS_BASE (0xF30A0000ul)

/* Phase Locked Loop (PLL) 5 */
#define APB_PLL5_BASE (0xF3090000ul)

/* Phase Locked Loop (PLL) 4 */
#define APB_PLL4_BASE (0xF3080000ul)

/* Phase Locked Loop (PLL) 3 */
#define APB_PLL3_BASE (0xF06D0000ul)

/* Phase Locked Loop (PLL) 2 */
#define APB_PLL2_BASE (0xF06C0000ul)

/* Phase Locked Loop (PLL) 1 */
#define APB_PLL1_BASE (0xF06B0000ul)

/* SF Domain Clock Generator (CKGEN) */
#define APB_CKGEN_SF_BASE (0xF06A0000ul)

/* SF Domain Reset Generator (RSTGEN) */
#define APB_RSTGEN_SF_BASE (0xF0690000ul)

/* 24MHZ generation block with failsafe function(FS24M) */
#define APB_FS_24M_BASE (0xF0660000ul)

/* 32KHZ generation block with failsafe function (FS32K) */
#define APB_FS_32K_BASE (0xF0030000ul)


/*------------------------------------------------------
 * Power Management
 *------------------------------------------------------*/

/* Temperature Sensor for AP Domain */
#define APB_PT_SNS_AP_BASE (0xF3060000ul)

/* Power on Reset for AP Domain */
#define APB_POR_AP_BASE (0xF3050000ul)

/* Voltage Detector for AP Domain */
#define APB_VD_AP_BASE (0xF3040000ul)

/* Temperature Sensor for SF Domain */
#define APB_PT_SNS_SF_BASE (0xF06F0000ul)

/* Voltage Detector for SF Domain */
#define APB_VD_SF_BASE (0xF06E0000ul)

/* Power on Reset for Safety Domain */
#define APB_POR_SF_BASE (0xF0650000ul)

/* System Work Mode Controller(SMC) */
#define APB_SMC_BASE (0xF0640000ul)

/* Linear Mode Regulator Controller(LDO) */
#define APB_LDO_DIG_BASE (0xF0610000ul)

/* Digital controller of bulk mode DC-DC converter(DCDC) */
#define APB_DCDC1_BASE (0xF0600000ul)

/* Power Management Unit */
#define APB_PMU_CORE_BASE (0xF0060000ul)


/*------------------------------------------------------
 * Debug
 *------------------------------------------------------*/

/* CoreSight */
#define APB_CSSYS_BASE (0xF2080000ul)


/*------------------------------------------------------
 * FuSa
 *------------------------------------------------------*/

/* Error Injection Controller for SP Domain */
#define APB_EIC_SP_BASE (0xF2050000ul)

/* Error Injection Controller for SF Domain */
#define APB_EIC_SF_BASE (0xF0FE0000ul)

/* System Error Management 2 */
#define APB_SEM2_BASE (0xF07D0000ul)

/* System Error Management 1 */
#define APB_SEM1_BASE (0xF07C0000ul)

/* Input/Output Consistency */
#define APB_IOC_BASE (0xF0750000ul)

/* Error Injection Controller for Boot */
#define APB_EIC_BOOT_BASE (0xF0720000ul)


/*------------------------------------------------------
 * External Memory Interface
 *------------------------------------------------------*/

/* Extended SPI Slave */
#define APB_XSPI_SLV_BASE (0xF2040000ul)

/* Extended SPI1 Port B */
#define XSPI1_XSPI1PORTB_BASE (0x18000000ul)

/* Extended SPI1 */
#define XSPI1_BASE (0x10000000ul)

/* Extended SPI1 Port A */
#define XSPI1_XSPI1PORTA_BASE (0x10000000ul)

/* Extended SPI 1 Port B Control Registers */
#define APB_XSPI1PORTB_BASE (0xF0710000ul)

/* Extended SPI 1 Port A Control Registers */
#define APB_XSPI1PORTA_BASE (0xF0700000ul)

/* Extended SPI2 Port B */
#define XSPI2_XSPI2PORTB_BASE (0x28000000ul)

/* Extended SPI2 */
#define XSPI2_BASE (0x20000000ul)

/* Extended SPI2 Port A */
#define XSPI2_XSPI2PORTA_BASE (0x20000000ul)

/* Extended SPI 2 Port B Control Registers */
#define APB_XSPI2PORTB_BASE (0xF0BD0000ul)

/* Extended SPI 2 Port A Control Registers */
#define APB_XSPI2PORTA_BASE (0xF0BC0000ul)

/* SD/EMMC Host Controller 2 */
#define APB_SEHC2_BASE (0xF3170000ul)

/* SD/EMMC Host Controller 1 */
#define APB_SEHC1_BASE (0xF3160000ul)


/*------------------------------------------------------
 * Access Permission Control
 *------------------------------------------------------*/

/* Memory Access Controller */
#define APB_MAC_BASE (0xF07B0000ul)


/*------------------------------------------------------
 * Internal Memory Configuration
 *------------------------------------------------------*/

/* Internal ROM Controller */
#define APB_IROMC_BASE (0xF0670000ul)

/* IRAM1 RAM Controller */
#define APB_IRAMC1_BASE (0xF0D20000ul)

/* IRAM2 RAM Controller */
#define APB_IRAMC2_BASE (0xF0D30000ul)

/* IRAM3 RAM Controller */
#define APB_IRAMC3_BASE (0xF0D40000ul)

/* IRAM4 RAM Controller */
#define APB_IRAMC4_BASE (0xF2030000ul)


/*------------------------------------------------------
 * Misc
 *------------------------------------------------------*/

/* In System Test Controller */
#define APB_ISTC_BASE (0xF0620000ul)


/*------------------------------------------------------
 * Timers
 *------------------------------------------------------*/

/* Basic Timer Module (BTM) 6 */
#define APB_BTM6_BASE (0xF0470000ul)

/* Basic Timer Module (BTM) 5 */
#define APB_BTM5_BASE (0xF0460000ul)

/* Basic Timer Module (BTM) 4 */
#define APB_BTM4_BASE (0xF0450000ul)

/* Basic Timer Module (BTM) 3 */
#define APB_BTM3_BASE (0xF0440000ul)

/* Basic Timer Module (BTM) 2 */
#define APB_BTM2_BASE (0xF0430000ul)

/* Basic Timer Module (BTM) 1 */
#define APB_BTM1_BASE (0xF0420000ul)

/* Cross Trigger */
#define APB_XTRG_BASE (0xF0410000ul)

/* Real Time Clock 2 */
#define APB_RTC2_BASE (0xF0020000ul)

/* Real Time Clock 1 */
#define APB_RTC1_BASE (0xF0010000ul)

/* Targeting general purpose timer 1 */
#define APB_ETMR1_BASE (0xF04C0000ul)

/* Targeting general purpose timer 2 */
#define APB_ETMR2_BASE (0xF04D0000ul)

/* Targeting general purpose timer 3 */
#define APB_ETMR3_BASE (0xF04E0000ul)

/* Targeting general purpose timer 4 */
#define APB_ETMR4_BASE (0xF04F0000ul)

/* Energy Pulse-Width Modulator 1 */
#define APB_EPWM1_BASE (0xF0480000ul)

/* Energy Pulse-Width Modulator 2 */
#define APB_EPWM2_BASE (0xF0490000ul)

/* Energy Pulse-Width Modulator 3 */
#define APB_EPWM3_BASE (0xF04A0000ul)

/* Energy Pulse-Width Modulator 4 */
#define APB_EPWM4_BASE (0xF04B0000ul)

/* Watch Dog(WDT) 1 */
#define APB_WDT1_BASE (0xF07E0000ul)

/* Watch Dog(WDT) 2 */
#define APB_WDT2_BASE (0xF07F0000ul)

/* Watch Dog(WDT) 3 */
#define APB_WDT3_BASE (0xF2100000ul)

/* Watch Dog(WDT) 4 */
#define APB_WDT4_BASE (0xF2110000ul)

/* Watch Dog(WDT) 5 */
#define APB_WDT5_BASE (0xF0BE0000ul)

/* Watch Dog(WDT) 6 */
#define APB_WDT6_BASE (0xF0BF0000ul)

/* Watch Dog(WDT) 8 */
#define APB_WDT8_BASE (0xF31F0000ul)


/*------------------------------------------------------
 * CPU
 *------------------------------------------------------*/

/* Cluster 1 core 1 D-Cache */
#define R5_SX1_CACHE_R5_SX1_DCACHE_BASE (0x01AFC000ul)

/* Cluster 1 core 1 I-Cache */
#define R5_SX1_CACHE_R5_SX1_ICACHE_BASE (0x01AF8000ul)

/* Cluster 1 core 1 TCM A */
#define R5_SX1TCM_R5_SX1_TCMA_BASE (0x01AC0000ul)

/* Cluster 1 core 1 TCM B */
#define R5_SX1TCM_R5_SX1_TCMB_BASE (0x01AB8000ul)

/* Cluster 1 core 0 D-Cache */
#define R5_SX0_CACHE_R5_SX0_DCACHE_BASE (0x01A7C000ul)

/* Cluster 1 core 0 I-Cache */
#define R5_SX0_CACHE_R5_SX0_ICACHE_BASE (0x01A78000ul)

/* Cluster 1 core 0 TCM A */
#define R5_SX0TCM_R5_SX0_TCMA_BASE (0x01A40000ul)

/* Cluster 1 core 0 TCM B */
#define R5_SX0TCM_R5_SX0_TCMB_BASE (0x01A38000ul)

/* Cluster 0 D-Cache */
#define R5_SF_CACHE_R5_SF_DCACHE_BASE (0x01978000ul)

/* Cluster 0 I-Cache */
#define R5_SF_CACHE_R5_SF_ICACHE_BASE (0x01970000ul)

/* Cluster 0 TCM A */
#define R5_SF_TCM_R5_SF_TCMA_BASE (0x01940000ul)

/* Cluster 0 TCM B */
#define R5_SF_TCM_R5_SF_TCMB_BASE (0x01930000ul)

/* Cluster 2 core 1 D-Cache */
#define R5_SP1_CACHE_R5_SP1_DCACHE_BASE (0x018FC000ul)

/* Cluster 2 core 1 I-Cache */
#define R5_SP1_CACHE_R5_SP1_ICACHE_BASE (0x018F8000ul)

/* Cluster 2 core 1 TCM A */
#define R5_SP1TCM_R5_SP1_TCMA_BASE (0x018C0000ul)

/* Cluster 2 core 1 TCM B */
#define R5_SP1TCM_R5_SP1_TCMB_BASE (0x018B8000ul)

/* Cluster 2 core 0 D-Cache */
#define R5_SP0_CACHE_R5_SP0_DCACHE_BASE (0x0187C000ul)

/* Cluster 2 core 0 I-Cache */
#define R5_SP0_CACHE_R5_SP0_ICACHE_BASE (0x01878000ul)

/* Cluster 2 core 0 TCM A */
#define R5_SP0TCM_R5_SP0_TCMA_BASE (0x01840000ul)

/* Cluster 2 core 0 TCM B */
#define R5_SP0TCM_R5_SP0_TCMB_BASE (0x01838000ul)


/*------------------------------------------------------
 * IRAM
 *------------------------------------------------------*/

/* Internal RAM 1 */
#define IRAM1_BASE (0x00400000ul)

/* RAM for IRAM1 ECC. Used as internal RAM when ECC is disabled. */
#define IRAM1_ECC_BASE (0x00860000ul)

/* IRAM1_ECC Aliased Address */
#define IRAM1_ECCALIASED_BASE (0x00B80000ul)

/* IRAM1 Aliased Address */
#define IRAM1ALIASED_BASE (0x00C00000ul)

/* Internal RAM 2 */
#define IRAM2_BASE (0x00500000ul)

/* RAM for IRAM2 ECC. Used as internal RAM when ECC is disabled. */
#define IRAM2_ECC_BASE (0x00840000ul)

/* IRAM2_ECC Aliased Address */
#define IRAM2_ECCALIASED_BASE (0x00BA0000ul)

/* IRAM2 Aliased Address */
#define IRAM2ALIASED_BASE (0x00D00000ul)

/* Internal RAM 3 */
#define IRAM3_BASE (0x00600000ul)

/* RAM for IRAM3 ECC. Used as internal RAM when ECC is disabled. */
#define IRAM3_ECC_BASE (0x00820000ul)

/* IRAM3_ECC Aliased Address */
#define IRAM3_ECCALIASED_BASE (0x00BC0000ul)

/* IRAM3 Aliased Address */
#define IRAM3ALIASED_BASE (0x00E00000ul)

/* IRAM4 */
#define IRAM4_BASE (0x00700000ul)

/* RAM for IRAM4 ECC. Used as internal RAM when ECC is disabled. */
#define IRAM4_ECC_BASE (0x00800000ul)

/* IRAM4_ECC Aliased Address */
#define IRAM4_ECCALIASED_BASE (0x00BE0000ul)

/* IRAM4 Aliased Address */
#define IRAM4ALIASED_BASE (0x00F00000ul)


/*------------------------------------------------------
 * USB
 *------------------------------------------------------*/

/* USB Controller */
#define APB_USB_BASE (0xF30B0000ul)


/*------------------------------------------------------
 * Audio
 *------------------------------------------------------*/

/* Serial Audio Controller Interface 1 */
#define APB_SACI1_BASE (0xF3010000ul)

/* Serial Audio Controller Interface 2 */
#define APB_SACI2_BASE (0xF3020000ul)


/*------------------------------------------------------
 * ADC & ACMP
 *------------------------------------------------------*/

/* Analog Digital Converter 1 */
#define APB_ADC1_BASE (0xF0F70000ul)

/* Analog Digital Converter 2 */
#define APB_ADC2_BASE (0xF0F80000ul)

/* Analog Digital Converter 3 */
#define APB_ADC3_BASE (0xF0F90000ul)

/* Analog Compare (ACMP) 1 */
#define APB_ACMP1_BASE (0xF0FA0000ul)

/* Analog Compare (ACMP) 2 */
#define APB_ACMP2_BASE (0xF0FB0000ul)

/* Analog Compare (ACMP) 3 */
#define APB_ACMP3_BASE (0xF0FC0000ul)

/* Analog Compare (ACMP) 4 */
#define APB_ACMP4_BASE (0xF0FD0000ul)


/*------------------------------------------------------
 * Camera
 *------------------------------------------------------*/


/*------------------------------------------------------
 * Energy Processing Unit
 *------------------------------------------------------*/

#endif /* REGS_BASE_H */
