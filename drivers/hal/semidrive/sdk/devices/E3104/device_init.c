/**
 * @file device_init.c
 * @brief Semidrive device ealryinit.
 *
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */

#include <common.h>
#include <sdrv_ckgen.h>
#include <sdrv_power.h>
#include <regs_base.h>
#include <reg.h>

#ifndef APB_ACMP4_BASE
/* Analog Compare (ACMP) 4 */
#define APB_ACMP4_BASE (0xF0AC0000ul)
#endif

#ifndef APB_ACMP3_BASE
/* Analog Compare (ACMP) 3 */
#define APB_ACMP3_BASE (0xF0AB0000ul)
#endif

extern const sdrv_ckgen_node_t **g_ckgen_unused[];

static const mcu_power_e g_unused_power[] = {
    MCU_POWER_ACMP3,
    MCU_POWER_ACMP4,
    MCU_POWER_END
};

static int mcu_unused_ip_cglist_mask(const sdrv_ckgen_node_t *ckgen_ip[], bool mask)
{
    sdrv_ckgen_node_t *node;
    uint32_t i = 0;
    int ret = 0;

    while (ckgen_ip[i] && !ret) {
        node = (sdrv_ckgen_node_t *)ckgen_ip[i];
        ret = sdrv_ckgen_cg_mask(node, mask);
        i++;
    }

    return ret;
}

static void mcu_unused_power_pre_config(const mcu_power_e *powers)
{
    uint32_t i = 0;

    while (powers[i] != MCU_POWER_END) {
        switch (powers[i]) {
            case MCU_POWER_ACMP3:
                RMWREG32(APB_ACMP3_BASE + 0xBC, 0, 1, 1);
                RMWREG32(APB_ACMP3_BASE + 0xBC, 1, 1, 1);
            break;

            case MCU_POWER_ACMP4:
                RMWREG32(APB_ACMP4_BASE + 0xBC, 0, 1, 1);
                RMWREG32(APB_ACMP4_BASE + 0xBC, 1, 1, 1);
            break;

            default:
            break;
        }

        i++;
    }
}

static int mcu_unused_ip_config(void)
{
    uint32_t i = 0;
    int ret = 0;

    mcu_unused_power_pre_config(g_unused_power);

    while (g_ckgen_unused[i]) {
        ret = sdrv_ckgen_ip_clock_enable(g_ckgen_unused[i], CKGEN_HIB_MODE, false);
        ret = sdrv_ckgen_ip_clock_enable(g_ckgen_unused[i], CKGEN_SLP_MODE, false);
        ret = sdrv_ckgen_ip_clock_enable(g_ckgen_unused[i], CKGEN_RUN_MODE, false);
        ret = mcu_unused_ip_cglist_mask(g_ckgen_unused[i], true);
        i++;
    }

    return ret;
}

/**
 * @brief initializes the device.
 *
 * This function initializes the device before call main function.
 */
void device_init(void)
{
    mcu_unused_ip_config();
}