
//*****************************************************************************
//
// WARNING: Automatically generated file, don't modify anymore!!!
//
// Copyright (c) 2021-2029 Semidrive Incorporated.  All rights reserved.
// Software License Agreement
//
//*****************************************************************************

#ifndef RESET_IP_H
#define RESET_IP_H

#include <sdrv_rstgen.h>

/* SAF rstgen controller */
extern sdrv_rstgen_t g_rstgen_saf;

/* AP rstgen controller */
extern sdrv_rstgen_t g_rstgen_ap;

/* global reset */
extern sdrv_rstgen_glb_t rstctl_glb;

/* SAF core */
extern sdrv_rstgen_sig_t rstsig_rstgen_ap;
extern sdrv_rstgen_sig_t rstsig_cr5_saf;

/**
 * @brief SAF latent
 *
 * latent signals will reset automatically after power on.
 *
 * signals in SAF latent:
 * MAC
 * SCR_BOOT
 * EIC_BOOT
 * FUSE_LSP_CMP
 * AHB2APB1
 * AHB2APB2
 * AHB2APB3
 * AHB2APB4
 * APBMUX2
 * APBMUX3
 * APBMUX4
 * IROMC
 * GPIO_SF
 * IOMUXC_SF
 * WDT1
 * WDT2
 * SEM1
 * SEM2
 * VD_SF_DIG
 * IOC
 * ETMR1
 * ETMR2
 * EPWM1
 * EPWM2
 * MPC_XSPI1A
 * MPC_XSPI1B
 * PPC_APBMUX2
 * PPC_APBMUX3
 * PPC_APBMUX4
 * PPC_APBMUX1
 * MPC_ROMC
 * XB_SF
 * POR_SF_DIG
 * PT_SNS_SF_DIG
 * APB_MAC_SP_SLV
 * APB_MAC_AP_SLV
 * APB_APBMUX1_SLV
 * MPC_VIC1
 * BTM1
 * BTM2
 * BTM3
 * BTM4
 * BTM5
 * BTM6
 * AAPB_XSPI1A
 * AAPB_XSPI1B
*/
extern sdrv_rstgen_sig_t rstsig_saf_latent;

/*
 * @brief SAF mission
 *
 * SAF mission signals will reset automatically after power on.
 *
 */

/**
 * @brief SAF mission 0
 *
 * signals in SAF mission 0:
 * FAB_SF
 * IOMUXC_SF
 * SCR_SF
 * SMC
 * MPC_CR5_SF
 * PMU_CORE
 * APB_APBMUX1_MST
 * APB_PMUX2_DEC_SLV
 * APB_PMUX2_DEC_MST
*/
extern sdrv_rstgen_sig_t rstsig_saf_mission0;

/**
 * @brief SAF mission 1
 *
 * signals in mission 1:
 * PLL1
 * PLL2
 * PLL3
 */
extern sdrv_rstgen_sig_t rstsig_saf_mission1;

/**
 * @brief SAF mission 2
 *
 * signals in SAF mission 2:
 * ANA_SF_SADC1
 * ANA_SF_SADC2
 * ANA_SF_SADC3
 * ANA_SF_ACMP1
 * ANA_SF_ACMP2
 * ANA_SF_ACMP3
 * ANA_SF_ACMP4
 */
extern sdrv_rstgen_sig_t rstsig_saf_mission2;

/**
 * @brief SAF mission 3
 *
 * signals in SAF mission 3:
 * APBMUX3
 * APBMUX4
 * IOMUXC_SF_COMP
 * EIC_SF
 * UART1
 * UART2
 * UART3
 * UART4
 * UART5
 * UART6
 * UART7
 * UART8
 * UART9
 * UART10
 * UART11
 * UART12
 * SPI1
 * SPI2
 * SPI3
 * SPI4
 * SPI5
 * SPI6
 * I2C3
 * I2C4
 * I2C5
 * I2C6
 * I2C7
 * I2C8
 * FLEXRAY1
 * FLEXRAY2
 * WDT5
 * WDT6
 * MPC_MB
 * APB_APBMUX3_SLV
 * APB_APBMUX3_MST
 * APB_APBMUX4_SLV
 * APB_APBMUX4_MST
 * APB_SEIP_NVM_MST
 * APB_SEIP_NVM_SLV
 * ADB_SFAP_SLV
 * ADB_SFAP_MST
 * ADB_APSF_SLV
 * ADB_APSF_MST
 * ADB_DISPSF_SLV
 * ADB_DISPSF_MST
 * BTI_DISPSF
 * BTI_APSF
 */
extern sdrv_rstgen_sig_t rstsig_saf_mission3;

/**
 * @brief SAF mission 4
 *
 * signals in SAF mission 4:
 * IRAMC1
 * IRAMC2
 * IRAMC3
 * IRAM_MUX
 * MPC_IRAMC1
 * MPC_IRAMC2
 * MPC_IRAMC3
 */
extern sdrv_rstgen_sig_t rstsig_saf_mission4;

/**
 * @brief SAF mission 5
 *
 * signals in SAF mission 5:
 * IRAMC4
 * APBMUX5
 * WDT3
 * WDT4
 * PTB
 * FAB_SP
 * MPC_IRAMC4
 * PPC_APBMUX5
 * EIC_SP
 * CSLITE
 * AAHB_SPSF_SLV
 * AAHB_SPSF_MST
 * APB_MAC_SP_SLV
 * APB_MAC_SP_MST
 * ADB_SPAP_SLV
 * ADB_SPAP_MST
 */
extern sdrv_rstgen_sig_t rstsig_saf_mission5;

/**
 * @brief SAF mission 6
 *
 * signals in SAF mission 6:
 * reserved
 */
extern sdrv_rstgen_sig_t rstsig_saf_mission6;

/**
 * @brief SAF mission 8
 *
 * signals in SAF mission 8:
 * LDO_DIG
 * APB_LDO_DIG_MST
 * APB_LDO_DIG_SLV
 */
extern sdrv_rstgen_sig_t rstsig_saf_mission8;

/**
 * @brief SAF mission 9
 *
 * signals in SAF mission 9:
 * DCDC1
 * APB_DCDC1_MST
 * APB_DCDC1_SLV
 */
extern sdrv_rstgen_sig_t rstsig_saf_mission9;

/* SAF module */
extern sdrv_rstgen_sig_t rstsig_canfd3_4;
extern sdrv_rstgen_sig_t rstsig_canfd5_8;
extern sdrv_rstgen_sig_t rstsig_canfd9_16;
extern sdrv_rstgen_sig_t rstsig_canfd17_24;
extern sdrv_rstgen_sig_t rstsig_xspi1a;
extern sdrv_rstgen_sig_t rstsig_xspi1b;
extern sdrv_rstgen_sig_t rstsig_dma_rst0;
extern sdrv_rstgen_sig_t rstsig_dma_rst1;
extern sdrv_rstgen_sig_t rstsig_enet1;
extern sdrv_rstgen_sig_t rstsig_vic1;
extern sdrv_rstgen_sig_t rstsig_vic2_porta;
extern sdrv_rstgen_sig_t rstsig_vic2_portb;
extern sdrv_rstgen_sig_t rstsig_vic3_porta;
extern sdrv_rstgen_sig_t rstsig_vic3_portb;
extern sdrv_rstgen_sig_t rstsig_xspi_slv;
extern sdrv_rstgen_sig_t rstsig_mb;
extern sdrv_rstgen_sig_t rstsig_xtrg;

/*
 * @brief AP mission
 *
 * AP mission signals will reset automatically after power on.
 *
 */

/**
 * @brief AP mision 0
 *
 * signals in AP mission 0:
 * PLL4
 * PLL5
 * PLL_LVDS
 */
extern sdrv_rstgen_sig_t rstsig_ap_mission0;

/**
 * @brief AP mision 1
 *
 * signals in AP mission 1:
 * APBMUX7
 * DISP_MUX
 * FAB_AP
 * VD_AP
 * APBMUX6
 * GPIO_AP
 * IOMUXC_AP
 * WDT8
 * MPC_SEIP
 * MPC_SDRAMC
 * PPC_APBMUX6
 * PPC_APBMUX7
 * FAB_DISP
 * AHBDEC_SEIP
 * PT_SNS_AP
 * POR_AP
 * SCR_AP
 * RSTGEN_AP
 * APB_MAC_AP_SLV
 * APB_MAC_AP_MST
 * APB_APBMUX7_SLV
 * APB_APBMUX7_MST
 * APB_SEC_STORAGE1_SLV
 * APB_SEC_STORAGE1_MST
 * APB_SEIP_NVM_MST
 * APB_SEIP_NVM_SLV
 * ADB_SPAP_SLV
 * ADB_SPAP_MST
 * ADB_SFAP_SLV
 * ADB_SFAP_MST
 * ADB_APSF_SLV
 * ADB_APSF_MST
 * ADB_DISPSF_SLV
 * ADB_DISPSF_MST
 * BTI_DISPSF
 * BTI_APSF
 */
extern sdrv_rstgen_sig_t rstsig_ap_mission1;

/* AP module */
extern sdrv_rstgen_sig_t rstsig_saci2;
extern sdrv_rstgen_sig_t rstsig_dma_ap;
extern sdrv_rstgen_sig_t rstsig_sehc1;
extern sdrv_rstgen_sig_t rstsig_sehc2;
extern sdrv_rstgen_sig_t rstsig_usb;
extern sdrv_rstgen_sig_t rstsig_seip;
extern sdrv_rstgen_sig_t rstsig_lvds_ss;

/* SAF debug */
extern sdrv_rstgen_sig_t rstsig_saf_debug;

/* AP debug */
extern sdrv_rstgen_sig_t rstsig_ap_debug;

/* general register */
extern sdrv_rstgen_general_reg_t reset_general_reg_sf_remap;
extern sdrv_rstgen_general_reg_t reset_general_reg_sf_boot;
extern sdrv_rstgen_general_reg_t reset_general_reg_rom_ctrl;

/* recovery module */
extern sdrv_recovery_btm_t recovery_btm_list;
extern sdrv_recovery_etimer_t recovery_etimer_list;
extern sdrv_recovery_epwm_t recovery_epwm_list;
extern sdrv_recovery_module_t recovery_module_array;
#endif