/*
 * core_id.h
 *
 * Copyright (c) 2020 Semidrive Semiconductor.
 * All rights reserved.
 *
 * Description: E3 core id interface.
 *
 * Revision History:
 * -----------------
 */

#ifndef _CORE_ID_H_
#define _CORE_ID_H_

#ifndef ASSEMBLY
#include "armv7-r/register.h"

/* core numbers */
#define CORE_NUM    1

/* core id */
#define CORE_SF     0

/* core numbers in SMP (Symmetrical Multi-Processing) system */
#define CORE_NUM_SMP    1

/**
 * @brief Get the core id
 *
 * SF   0
 *
 * @return core id
 */
static inline int get_core_id(void)
{
    return CORE_SF;
}

/* Get the core id in SMP (Symmetrical Multi-Processing) system */
#define get_core_id_smp() get_core_id()

#endif

#endif
