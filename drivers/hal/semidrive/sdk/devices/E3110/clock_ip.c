//*****************************************************************************
//
// WARNING: Automatically generated file, don't modify anymore!!!
//
// Copyright (c) 2021-2029 Semidrive Incorporated.  All rights reserved.
// Software License Agreement
//
//*****************************************************************************

#include <types.h>
#include <regs_base.h>
#include <clock_ip.h>

const sdrv_pll_node_t g_rc_32k = {
    .clk_node = {
        .type = CKGEN_RC32K_TYPE,
        .id = 0x0,
    },
    .parent = NULL,
};

const sdrv_pll_node_t g_fs_32k = {
    .clk_node = {
        .base = APB_FS_32K_BASE,
        .type = CKGEN_FS32K_TYPE,
        .id = 0x1,
    },
    .parent = NULL,
};

const sdrv_pll_node_t g_rc_24m = {
    .clk_node = {
        .base = 0,
        .type = CKGEN_RC24M_TYPE,
        .id = 0x2,
    },
    .parent = NULL,
};

const sdrv_pll_node_t g_fs_24m = {
    .clk_node = {
        .base = APB_FS_24M_BASE,
        .type = CKGEN_FS24M_TYPE,
        .id = 0x3,
    },
    .parent = NULL,
};

const sdrv_pll_node_t g_pll1_root = {
    .clk_node = {
        .base = APB_PLL1_BASE,
        .type = CKGEN_PLL_CTRL_TYPE,
        .id = 0x0,
    },
    .parent = CLK_NODE(g_fs_24m),
};

const sdrv_pll_node_t g_pll2_root = {
    .clk_node = {
        .base = APB_PLL2_BASE,
        .type = CKGEN_PLL_CTRL_TYPE,
        .id = 0x1,
    },
    .parent = CLK_NODE(g_fs_24m),
};

const sdrv_pll_node_t g_pll3_root = {
    .clk_node = {
        .base = APB_PLL3_BASE,
        .type = CKGEN_PLL_CTRL_TYPE,
        .id = 0x2,
    },
    .parent = CLK_NODE(g_fs_24m),
};

const sdrv_ckgen_slice_node_t g_ckgen_bus_cr5_sf = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_SF_BUS_SLICE_TYPE,
        .id = 0x0,
    },
    .parents_num = 5,
    .parents[0] = CLK_NODE(g_rc_24m),
    .parents[1] = CLK_NODE(g_fs_24m),
    .parents[2] = CLK_NODE(g_pll1_root),
    .parents[3] = CLK_NODE(g_pll3_root),
    .parents[4] = CLK_NODE(g_pll2_root),
};

const sdrv_ckgen_slice_node_t g_ckgen_bus_cr5_sp = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_SF_BUS_SLICE_TYPE,
        .id = 0x1,
    },
    .parents_num = 5,
    .parents[0] = CLK_NODE(g_rc_24m),
    .parents[1] = CLK_NODE(g_fs_24m),
    .parents[2] = CLK_NODE(g_pll1_root),
    .parents[3] = CLK_NODE(g_pll3_root),
    .parents[4] = CLK_NODE(g_pll2_root),
};

const sdrv_ckgen_slice_node_t g_ckgen_ip_i2c_sf_1_to_4 = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_IP_SLICE_TYPE,
        .id = 0x0,
    },
    .parents_num = 5,
    .parents[0] = CLK_NODE(g_rc_24m),
    .parents[1] = CLK_NODE(g_fs_24m),
    .parents[2] = CLK_NODE(g_pll1_root),
    .parents[3] = CLK_NODE(g_pll3_root),
    .parents[4] = CLK_NODE(g_pll2_root),
};

const sdrv_ckgen_slice_node_t g_ckgen_ip_i2c_sf_5_to_8 = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_IP_SLICE_TYPE,
        .id = 0x1,
    },
    .parents_num = 5,
    .parents[0] = CLK_NODE(g_rc_24m),
    .parents[1] = CLK_NODE(g_fs_24m),
    .parents[2] = CLK_NODE(g_pll1_root),
    .parents[3] = CLK_NODE(g_pll3_root),
    .parents[4] = CLK_NODE(g_pll2_root),
};

const sdrv_ckgen_slice_node_t g_ckgen_ip_spi_sf_1_to_4 = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_IP_SLICE_TYPE,
        .id = 0x2,
    },
    .parents_num = 5,
    .parents[0] = CLK_NODE(g_rc_24m),
    .parents[1] = CLK_NODE(g_fs_24m),
    .parents[2] = CLK_NODE(g_pll1_root),
    .parents[3] = CLK_NODE(g_pll3_root),
    .parents[4] = CLK_NODE(g_pll2_root),
};

const sdrv_ckgen_slice_node_t g_ckgen_ip_spi_sf_5_to_8 = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_IP_SLICE_TYPE,
        .id = 0x3,
    },
    .parents_num = 5,
    .parents[0] = CLK_NODE(g_rc_24m),
    .parents[1] = CLK_NODE(g_fs_24m),
    .parents[2] = CLK_NODE(g_pll1_root),
    .parents[3] = CLK_NODE(g_pll3_root),
    .parents[4] = CLK_NODE(g_pll2_root),
};

const sdrv_ckgen_slice_node_t g_ckgen_ip_uart_sf_1_to_8 = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_IP_SLICE_TYPE,
        .id = 0x4,
    },
    .parents_num = 5,
    .parents[0] = CLK_NODE(g_rc_24m),
    .parents[1] = CLK_NODE(g_fs_24m),
    .parents[2] = CLK_NODE(g_pll1_root),
    .parents[3] = CLK_NODE(g_pll3_root),
    .parents[4] = CLK_NODE(g_pll2_root),
};

const sdrv_ckgen_slice_node_t g_ckgen_ip_uart_sf_9_to_16 = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_IP_SLICE_TYPE,
        .id = 0x5,
    },
    .parents_num = 5,
    .parents[0] = CLK_NODE(g_rc_24m),
    .parents[1] = CLK_NODE(g_fs_24m),
    .parents[2] = CLK_NODE(g_pll1_root),
    .parents[3] = CLK_NODE(g_pll3_root),
    .parents[4] = CLK_NODE(g_pll2_root),
};

const sdrv_ckgen_slice_node_t g_ckgen_ip_enet1_tx = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_IP_SLICE_TYPE,
        .id = 0x6,
    },
    .parents_num = 5,
    .parents[0] = CLK_NODE(g_rc_24m),
    .parents[1] = CLK_NODE(g_fs_24m),
    .parents[2] = CLK_NODE(g_pll2_root),
    .parents[3] = CLK_NODE(g_pll3_root),
    .parents[4] = CLK_NODE(g_pll1_root),
};

const sdrv_ckgen_slice_node_t g_ckgen_ip_enet1_rmii = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_IP_SLICE_TYPE,
        .id = 0x7,
    },
    .parents_num = 5,
    .parents[0] = CLK_NODE(g_rc_24m),
    .parents[1] = CLK_NODE(g_fs_24m),
    .parents[2] = CLK_NODE(g_pll2_root),
    .parents[3] = CLK_NODE(g_pll3_root),
    .parents[4] = CLK_NODE(g_pll1_root),
};

const sdrv_ckgen_slice_node_t g_ckgen_ip_enet1_phy_ref = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_IP_SLICE_TYPE,
        .id = 0x8,
    },
    .parents_num = 5,
    .parents[0] = CLK_NODE(g_rc_24m),
    .parents[1] = CLK_NODE(g_fs_24m),
    .parents[2] = CLK_NODE(g_pll2_root),
    .parents[3] = CLK_NODE(g_pll3_root),
    .parents[4] = CLK_NODE(g_pll1_root),
};

const sdrv_ckgen_slice_node_t g_ckgen_ip_enet1_timer_sec = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_IP_SLICE_TYPE,
        .id = 0x9,
    },
    .parents_num = 5,
    .parents[0] = CLK_NODE(g_rc_24m),
    .parents[1] = CLK_NODE(g_fs_24m),
    .parents[2] = CLK_NODE(g_pll2_root),
    .parents[3] = CLK_NODE(g_pll3_root),
    .parents[4] = CLK_NODE(g_pll1_root),
};

const sdrv_ckgen_slice_node_t g_ckgen_ip_xspi1a = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_IP_SLICE_TYPE,
        .id = 0xd,
    },
    .parents_num = 5,
    .parents[0] = CLK_NODE(g_rc_24m),
    .parents[1] = CLK_NODE(g_fs_24m),
    .parents[2] = CLK_NODE(g_pll1_root),
    .parents[3] = CLK_NODE(g_pll3_root),
    .parents[4] = CLK_NODE(g_pll2_root),
};

const sdrv_ckgen_slice_node_t g_ckgen_ip_xspi1b = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_IP_SLICE_TYPE,
        .id = 0xe,
    },
    .parents_num = 5,
    .parents[0] = CLK_NODE(g_rc_24m),
    .parents[1] = CLK_NODE(g_fs_24m),
    .parents[2] = CLK_NODE(g_pll1_root),
    .parents[3] = CLK_NODE(g_pll3_root),
    .parents[4] = CLK_NODE(g_pll2_root),
};

const sdrv_ckgen_slice_node_t g_ckgen_ip_xtrg = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_IP_SLICE_TYPE,
        .id = 0x11,
    },
    .parents_num = 5,
    .parents[0] = CLK_NODE(g_rc_24m),
    .parents[1] = CLK_NODE(g_fs_24m),
    .parents[2] = CLK_NODE(g_pll1_root),
    .parents[3] = CLK_NODE(g_pll3_root),
    .parents[4] = CLK_NODE(g_pll2_root),
};

const sdrv_ckgen_slice_node_t g_ckgen_ip_etmr1 = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_IP_SLICE_TYPE,
        .id = 0x12,
    },
    .parents_num = 5,
    .parents[0] = CLK_NODE(g_rc_24m),
    .parents[1] = CLK_NODE(g_fs_24m),
    .parents[2] = CLK_NODE(g_pll1_root),
    .parents[3] = CLK_NODE(g_pll3_root),
    .parents[4] = CLK_NODE(g_pll2_root),
};

const sdrv_ckgen_slice_node_t g_ckgen_ip_etmr2 = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_IP_SLICE_TYPE,
        .id = 0x13,
    },
    .parents_num = 5,
    .parents[0] = CLK_NODE(g_rc_24m),
    .parents[1] = CLK_NODE(g_fs_24m),
    .parents[2] = CLK_NODE(g_pll1_root),
    .parents[3] = CLK_NODE(g_pll3_root),
    .parents[4] = CLK_NODE(g_pll2_root),
};

const sdrv_ckgen_slice_node_t g_ckgen_ip_epwm1 = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_IP_SLICE_TYPE,
        .id = 0x16,
    },
    .parents_num = 5,
    .parents[0] = CLK_NODE(g_rc_24m),
    .parents[1] = CLK_NODE(g_fs_24m),
    .parents[2] = CLK_NODE(g_pll1_root),
    .parents[3] = CLK_NODE(g_pll3_root),
    .parents[4] = CLK_NODE(g_pll2_root),
};

const sdrv_ckgen_slice_node_t g_ckgen_ip_epwm2 = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_IP_SLICE_TYPE,
        .id = 0x17,
    },
    .parents_num = 5,
    .parents[0] = CLK_NODE(g_rc_24m),
    .parents[1] = CLK_NODE(g_fs_24m),
    .parents[2] = CLK_NODE(g_pll1_root),
    .parents[3] = CLK_NODE(g_pll3_root),
    .parents[4] = CLK_NODE(g_pll2_root),
};

const sdrv_ckgen_slice_node_t g_ckgen_ip_can = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_IP_SLICE_TYPE,
        .id = 0x1a,
    },
    .parents_num = 5,
    .parents[0] = CLK_NODE(g_rc_24m),
    .parents[1] = CLK_NODE(g_fs_24m),
    .parents[2] = CLK_NODE(g_pll1_root),
    .parents[3] = CLK_NODE(g_pll3_root),
    .parents[4] = CLK_NODE(g_pll2_root),
};

const sdrv_ckgen_slice_node_t g_ckgen_ip_adc1 = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_IP_SLICE_TYPE,
        .id = 0x1b,
    },
    .parents_num = 5,
    .parents[0] = CLK_NODE(g_rc_24m),
    .parents[1] = CLK_NODE(g_fs_24m),
    .parents[2] = CLK_NODE(g_pll1_root),
    .parents[3] = CLK_NODE(g_pll3_root),
    .parents[4] = CLK_NODE(g_pll2_root),
};

const sdrv_ckgen_slice_node_t g_ckgen_ip_adc2 = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_IP_SLICE_TYPE,
        .id = 0x1c,
    },
    .parents_num = 5,
    .parents[0] = CLK_NODE(g_rc_24m),
    .parents[1] = CLK_NODE(g_fs_24m),
    .parents[2] = CLK_NODE(g_pll1_root),
    .parents[3] = CLK_NODE(g_pll3_root),
    .parents[4] = CLK_NODE(g_pll2_root),
};

const sdrv_ckgen_slice_node_t g_ckgen_ip_adc3 = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_IP_SLICE_TYPE,
        .id = 0x1d,
    },
    .parents_num = 5,
    .parents[0] = CLK_NODE(g_rc_24m),
    .parents[1] = CLK_NODE(g_fs_24m),
    .parents[2] = CLK_NODE(g_pll1_root),
    .parents[3] = CLK_NODE(g_pll3_root),
    .parents[4] = CLK_NODE(g_pll2_root),
};

const sdrv_ckgen_slice_node_t g_ckgen_ip_acmp = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_IP_SLICE_TYPE,
        .id = 0x1e,
    },
    .parents_num = 5,
    .parents[0] = CLK_NODE(g_rc_24m),
    .parents[1] = CLK_NODE(g_fs_24m),
    .parents[2] = CLK_NODE(g_pll1_root),
    .parents[3] = CLK_NODE(g_pll3_root),
    .parents[4] = CLK_NODE(g_pll2_root),
};

const sdrv_ckgen_slice_node_t g_ckgen_ip_ioc = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_IP_SLICE_TYPE,
        .id = 0x20,
    },
    .parents_num = 5,
    .parents[0] = CLK_NODE(g_rc_24m),
    .parents[1] = CLK_NODE(g_fs_24m),
    .parents[2] = CLK_NODE(g_pll1_root),
    .parents[3] = CLK_NODE(g_pll3_root),
    .parents[4] = CLK_NODE(g_pll2_root),
};

const sdrv_ckgen_slice_node_t g_ckgen_ip_pt_sns_sf = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_IP_SLICE_TYPE,
        .id = 0x21,
    },
    .parents_num = 5,
    .parents[0] = CLK_NODE(g_rc_24m),
    .parents[1] = CLK_NODE(g_fs_24m),
    .parents[2] = CLK_NODE(g_pll1_root),
    .parents[3] = CLK_NODE(g_pll3_root),
    .parents[4] = CLK_NODE(g_pll2_root),
};

const sdrv_ckgen_slice_node_t g_ckgen_bus_sf_test = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_SF_BUS_SLICE_TYPE,
        .id = 0x2,
    },
    .parents_num = 5,
    .parents[0] = CLK_NODE(g_rc_24m),
    .parents[1] = CLK_NODE(g_fs_24m),
    .parents[2] = CLK_NODE(g_pll1_root),
    .parents[3] = CLK_NODE(g_pll2_root),
    .parents[4] = CLK_NODE(g_pll3_root),
};

const sdrv_pll_node_t g_pll4_root = {
    .clk_node = {
        .base = APB_PLL4_BASE,
        .type = CKGEN_PLL_CTRL_TYPE,
        .id = 0x3,
    },
    .parent = CLK_NODE(g_fs_24m),
};

const sdrv_pll_node_t g_pll5_root = {
    .clk_node = {
        .base = APB_PLL5_BASE,
        .type = CKGEN_PLL_CTRL_TYPE,
        .id = 0x4,
    },
    .parent = CLK_NODE(g_fs_24m),
};

const sdrv_pll_node_t g_pll_lvds_root = {
    .clk_node = {
        .base = APB_PLL_LVDS_BASE,
        .type = CKGEN_PLL_CTRL_TYPE,
        .id = 0x5,
    },
    .parent = CLK_NODE(g_fs_24m),
};

const sdrv_pll_node_t g_pll_lvds_nodiv = {
    .clk_node = {
        .base = APB_PLL_LVDS_BASE,
        .type = CKGEN_PLL_LVDS_TYPE,
        .id = 0x0,
    },
    .parent = CLK_NODE(g_pll_lvds_root),
};

const sdrv_pll_node_t g_pll_lvds_div2 = {
    .clk_node = {
        .base = APB_PLL_LVDS_BASE,
        .type = CKGEN_PLL_LVDS_TYPE,
        .id = 0x1,
    },
    .parent = CLK_NODE(g_pll_lvds_root),
};

const sdrv_pll_node_t g_pll_lvds_div7 = {
    .clk_node = {
        .base = APB_PLL_LVDS_BASE,
        .type = CKGEN_PLL_LVDS_TYPE,
        .id = 0x2,
    },
    .parent = CLK_NODE(g_pll_lvds_div2),
};

const sdrv_pll_node_t g_pll_lvds_ckgen = {
    .clk_node = {
        .base = APB_PLL_LVDS_BASE,
        .type = CKGEN_PLL_LVDS_TYPE,
        .id = 0x3,
    },
    .parent = CLK_NODE(g_pll_lvds_root),
};

const sdrv_ckgen_slice_node_t g_ckgen_bus_ap_bus = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_BUS_SLICE_TYPE,
        .id = 0x0,
    },
    .parents_num = 5,
    .parents[0] = CLK_NODE(g_rc_24m),
    .parents[1] = CLK_NODE(g_fs_24m),
    .parents[2] = CLK_NODE(g_pll4_root),
    .parents[3] = CLK_NODE(g_pll_lvds_ckgen),
    .parents[4] = CLK_NODE(g_pll5_root),
};

const sdrv_ckgen_slice_node_t g_ckgen_bus_disp_bus = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_BUS_SLICE_TYPE,
        .id = 0x1,
    },
    .parents_num = 5,
    .parents[0] = CLK_NODE(g_rc_24m),
    .parents[1] = CLK_NODE(g_fs_24m),
    .parents[2] = CLK_NODE(g_pll4_root),
    .parents[3] = CLK_NODE(g_pll_lvds_ckgen),
    .parents[4] = CLK_NODE(g_pll5_root),
};

const sdrv_ckgen_slice_node_t g_ckgen_bus_seip = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_BUS_SLICE_TYPE,
        .id = 0x2,
    },
    .parents_num = 5,
    .parents[0] = CLK_NODE(g_rc_24m),
    .parents[1] = CLK_NODE(g_fs_24m),
    .parents[2] = CLK_NODE(g_pll4_root),
    .parents[3] = CLK_NODE(g_pll_lvds_ckgen),
    .parents[4] = CLK_NODE(g_pll5_root),
};

const sdrv_ckgen_slice_node_t g_ckgen_ip_reserved = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_IP_SLICE_TYPE,
        .id = 0x0,
    },
    .parents_num = 5,
    .parents[0] = CLK_NODE(g_rc_24m),
    .parents[1] = CLK_NODE(g_fs_24m),
    .parents[2] = CLK_NODE(g_pll4_root),
    .parents[3] = CLK_NODE(g_pll_lvds_ckgen),
    .parents[4] = CLK_NODE(g_pll5_root),
};

const sdrv_ckgen_slice_node_t g_ckgen_ip_seip_test = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_IP_SLICE_TYPE,
        .id = 0x1,
    },
    .parents_num = 5,
    .parents[0] = CLK_NODE(g_rc_24m),
    .parents[1] = CLK_NODE(g_fs_24m),
    .parents[2] = CLK_NODE(g_pll4_root),
    .parents[3] = CLK_NODE(g_pll5_root),
    .parents[4] = CLK_NODE(g_pll_lvds_ckgen),
};

const sdrv_ckgen_slice_node_t g_ckgen_ip_sehc1 = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_IP_SLICE_TYPE,
        .id = 0x3,
    },
    .parents_num = 5,
    .parents[0] = CLK_NODE(g_rc_24m),
    .parents[1] = CLK_NODE(g_fs_24m),
    .parents[2] = CLK_NODE(g_pll4_root),
    .parents[3] = CLK_NODE(g_pll_lvds_ckgen),
    .parents[4] = CLK_NODE(g_pll5_root),
};

const sdrv_ckgen_slice_node_t g_ckgen_ip_sehc2 = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_IP_SLICE_TYPE,
        .id = 0x4,
    },
    .parents_num = 5,
    .parents[0] = CLK_NODE(g_rc_24m),
    .parents[1] = CLK_NODE(g_fs_24m),
    .parents[2] = CLK_NODE(g_pll4_root),
    .parents[3] = CLK_NODE(g_pll_lvds_ckgen),
    .parents[4] = CLK_NODE(g_pll5_root),
};

const sdrv_ckgen_slice_node_t g_ckgen_ip_i2s_mclk0 = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_IP_SLICE_TYPE,
        .id = 0x5,
    },
    .parents_num = 5,
    .parents[0] = CLK_NODE(g_rc_24m),
    .parents[1] = CLK_NODE(g_fs_24m),
    .parents[2] = CLK_NODE(g_pll4_root),
    .parents[3] = CLK_NODE(g_pll5_root),
    .parents[4] = CLK_NODE(g_pll_lvds_ckgen),
};

const sdrv_ckgen_slice_node_t g_ckgen_ip_i2s_mclk1 = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_IP_SLICE_TYPE,
        .id = 0x6,
    },
    .parents_num = 5,
    .parents[0] = CLK_NODE(g_rc_24m),
    .parents[1] = CLK_NODE(g_fs_24m),
    .parents[2] = CLK_NODE(g_pll4_root),
    .parents[3] = CLK_NODE(g_pll5_root),
    .parents[4] = CLK_NODE(g_pll_lvds_ckgen),
};

const sdrv_ckgen_slice_node_t g_ckgen_ip_saci2_clk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_IP_SLICE_TYPE,
        .id = 0x9,
    },
    .parents_num = 5,
    .parents[0] = CLK_NODE(g_rc_24m),
    .parents[1] = CLK_NODE(g_fs_24m),
    .parents[2] = CLK_NODE(g_pll4_root),
    .parents[3] = CLK_NODE(g_pll5_root),
    .parents[4] = CLK_NODE(g_pll_lvds_ckgen),
};

const sdrv_ckgen_slice_node_t g_ckgen_ip_saci2_pdm_clk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_IP_SLICE_TYPE,
        .id = 0xa,
    },
    .parents_num = 5,
    .parents[0] = CLK_NODE(g_rc_24m),
    .parents[1] = CLK_NODE(g_fs_24m),
    .parents[2] = CLK_NODE(g_pll4_root),
    .parents[3] = CLK_NODE(g_pll5_root),
    .parents[4] = CLK_NODE(g_pll_lvds_ckgen),
};

const sdrv_ckgen_slice_node_t g_ckgen_ip_pt_sns_ap = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_IP_SLICE_TYPE,
        .id = 0xf,
    },
    .parents_num = 5,
    .parents[0] = CLK_NODE(g_rc_24m),
    .parents[1] = CLK_NODE(g_fs_24m),
    .parents[2] = CLK_NODE(g_pll4_root),
    .parents[3] = CLK_NODE(g_pll5_root),
    .parents[4] = CLK_NODE(g_pll_lvds_ckgen),
};

const sdrv_ckgen_slice_node_t g_ckgen_ip_ap_test = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_IP_SLICE_TYPE,
        .id = 0x10,
    },
    .parents_num = 5,
    .parents[0] = CLK_NODE(g_rc_24m),
    .parents[1] = CLK_NODE(g_fs_24m),
    .parents[2] = CLK_NODE(g_pll4_root),
    .parents[3] = CLK_NODE(g_pll5_root),
    .parents[4] = CLK_NODE(g_pll_lvds_ckgen),
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_sf_cr5_sxclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_BCG_TYPE,
        .id = 0x0,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_sx_mainclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_BCG_TYPE,
        .id = 0x1,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_sf_cr5_sfclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_BCG_TYPE,
        .id = 0x2,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_sx_sf_mainclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_BCG_TYPE,
        .id = 0x3,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_sf_mainclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_BCG_TYPE,
        .id = 0x4,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_sf_perclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_BCG_TYPE,
        .id = 0x5,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_sx_sf_perclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_BCG_TYPE,
        .id = 0x6,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_xspi1_aclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_BCG_TYPE,
        .id = 0x7,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_xspi1_bclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_BCG_TYPE,
        .id = 0x8,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_xspi2_aclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_BCG_TYPE,
        .id = 0x9,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_xspi2_bclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_BCG_TYPE,
        .id = 0xa,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_sp_cr5_spclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_BCG_TYPE,
        .id = 0xb,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_sp_mainclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_BCG_TYPE,
        .id = 0xc,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_sp_sf_mainclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_BCG_TYPE,
        .id = 0xd,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_sp_perclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_BCG_TYPE,
        .id = 0xe,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_cr5_sx0_clkin = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_CCG_TYPE,
        .id = 0x0,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_cr5_sx1_clkin = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_CCG_TYPE,
        .id = 0x1,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_cr5_sf_clkin = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_CCG_TYPE,
        .id = 0x2,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_cr5_sp0_clkin = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_CCG_TYPE,
        .id = 0x3,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_cr5_sp1_clkin = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_CCG_TYPE,
        .id = 0x4,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_iram_mux_aclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x0,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_iramc1_aclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x1,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_iramc2_aclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x2,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_iramc3_aclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x3,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_iromc_aclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x4,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_sf_iram_mix_mainclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x5,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_sf_periph_mainclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x6,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_sf_boot_mainclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x7,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_mb_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x8,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_dma_sf_clk0 = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x9,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_dma_sf_clk1 = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xa,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_dma_sf_aclk0 = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xb,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_dma_sf_aclk1 = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xc,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_dma_sf_hclk0 = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xd,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_dma_sf_hclk1 = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xe,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_flexray1_hclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xf,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_flexray2_hclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x10,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_i2c1_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x11,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_i2c2_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x12,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_i2c3_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x13,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_i2c4_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x14,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_i2c5_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x15,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_i2c6_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x16,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_i2c7_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x17,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_i2c8_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x18,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_spi1_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x19,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_spi2_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x1a,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_spi3_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x1b,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_spi4_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x1c,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_spi5_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x1d,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_spi6_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x1e,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_spi7_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x1f,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_spi8_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x20,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_uart1_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x21,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_uart2_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x22,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_uart3_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x23,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_uart4_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x24,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_uart5_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x25,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_uart6_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x26,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_uart7_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x27,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_uart8_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x28,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_uart9_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x29,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_uart10_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x2a,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_uart11_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x2b,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_uart12_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x2c,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_uart13_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x2d,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_uart14_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x2e,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_uart15_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x2f,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_uart16_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x30,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd1_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x31,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd2_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x32,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd3_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x33,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd4_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x34,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd5_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x35,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd6_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x36,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd7_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x37,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd8_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x38,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd9_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x39,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd10_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x3a,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd11_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x3b,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd12_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x3c,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd13_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x3d,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd14_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x3e,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd15_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x3f,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd16_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x40,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd17_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x41,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd18_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x42,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd19_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x43,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd20_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x44,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd21_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x45,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd22_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x46,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd23_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x47,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd24_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x48,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_etmr1_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x49,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_etmr2_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x4a,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_etmr3_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x4b,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_etmr4_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x4c,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_pt_sns_sf_dig_i_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x4d,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_mpc_cr5_sx0_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x4e,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_mpc_cr5_sx1_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x4f,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_epwm1_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x50,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_epwm2_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x51,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_epwm3_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x52,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_epwm4_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x53,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_iram_mux_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x54,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_iramc1_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x55,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_iramc2_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x56,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_iramc3_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x57,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_iromc_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x58,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_gpio_sf_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x59,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_wdt1_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x5a,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_wdt2_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x5b,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_wdt1_bus_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x5c,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_wdt2_bus_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x5d,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_wdt5_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x5e,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_wdt5_bus_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x5f,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_wdt6_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x60,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_wdt6_bus_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x61,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_sem1_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x62,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_sem2_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x63,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_iomuxc_sf_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x64,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_iomuxc_sf_comp_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x65,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_efusec_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x66,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_mac_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x67,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_sf_periph_mac_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x68,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_sf_iram_mix_mac_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x69,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_mpc_cr5_sf_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x6a,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_scr_sf_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x6b,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_ckgen_sf_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x6c,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_rstgen_sf_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x6d,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_dma_sf_pclk0 = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x6e,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_dma_sf_pclk1 = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x6f,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_xb_sf_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x70,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_apbmux2_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x71,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_apbmux3_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x72,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_apbmux4_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x73,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_apbmux2_sf_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x74,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_ppc_apbmux1_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x75,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_ppc_apbmux2_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x76,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_ppc_apbmux3_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x77,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_ppc_apbmux4_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x78,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_smc_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x79,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_vd_sf_dig_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x7a,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_por_sf_dig_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x7b,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_xtrg_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x7c,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_apb_mac_sp_slv_src_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x7d,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_apb_mac_ap_slv_src_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x7e,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_fs_24m_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x7f,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_acmp1_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x80,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_acmp2_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x81,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_acmp3_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x82,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_acmp4_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x83,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_sadc1_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x84,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_sadc2_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x85,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_sadc3_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x86,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_ioc_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x87,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_btm1_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x88,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_btm2_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x89,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_btm3_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x8a,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_btm4_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x8b,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_btm5_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x8c,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_btm6_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x8d,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_eic_sf_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x8e,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_eic_boot_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x8f,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_istc_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x90,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_sf_periph_apbmux3_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x91,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_apb_seip_nvm_mst_dst_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x92,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_xspi1a_dma_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x93,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_xspi1b_dma_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x94,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_xspi2a_dma_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x95,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_xspi2b_dma_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x96,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_adma_enet1_dmaclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x97,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_adma_enet2_dmaclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x98,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_xb_sf_hclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x99,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_bti_sf_ahb_hclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x9a,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_aahb_spsf_mst_slvclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x9b,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_vic1_hclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x9c,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_vic3a_hclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x9d,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_vic3b_hclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x9e,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_vic_sf_irqsync = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x9f,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_i2c1_sclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xa0,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_i2c2_sclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xa1,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_i2c3_sclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xa2,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_i2c4_sclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xa3,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_i2c5_sclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xa4,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_i2c6_sclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xa5,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_i2c7_sclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xa6,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_i2c8_sclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xa7,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_spi1_spi_per_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xa8,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_spi2_spi_per_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xa9,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_spi3_spi_per_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xaa,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_spi4_spi_per_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xab,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_spi5_spi_per_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xac,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_spi6_spi_per_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xad,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_spi7_spi_per_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xae,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_spi8_spi_per_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xaf,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_uart1_sclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xb0,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_uart2_sclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xb1,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_uart3_sclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xb2,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_uart4_sclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xb3,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_uart5_sclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xb4,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_uart6_sclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xb5,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_uart7_sclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xb6,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_uart8_sclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xb7,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_uart9_sclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xb8,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_uart10_sclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xb9,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_uart11_sclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xba,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_uart12_sclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xbb,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_uart13_sclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xbc,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_uart14_sclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xbd,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_uart15_sclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xbe,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_uart16_sclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xbf,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_enet1_ref_clk_tx = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xc0,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_enet1_ptp_ref_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xc1,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_enet2_ptp_ref_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xc2,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_enet2_ref_clk_tx = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xc3,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_xspi1a_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xc4,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_mpc_xspi1a_aclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xc5,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_xspi1b_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xc6,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_mpc_xspi1b_aclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xc7,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_xspi2a_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xc8,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_mpc_xspi2a_aclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xc9,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_xspi2b_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xca,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_mpc_xspi2b_aclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xcb,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_xtrg_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xcc,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_etmr1_ahf_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xcd,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_etmr2_ahf_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xce,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_etmr3_ahf_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xcf,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_etmr4_ahf_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xd0,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_epwm1_ahf_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xd1,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_epwm2_ahf_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xd2,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_epwm3_ahf_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xd3,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_epwm4_ahf_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xd4,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_etmr1_hf_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xd5,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_etmr2_hf_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xd6,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_etmr3_hf_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xd7,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_etmr4_hf_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xd8,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_epwm1_hf_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xd9,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_epwm2_hf_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xda,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_epwm3_hf_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xdb,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_epwm4_hf_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xdc,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd1_ipg_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xdd,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd2_ipg_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xde,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd3_ipg_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xdf,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd4_ipg_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xe0,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd5_ipg_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xe1,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd6_ipg_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xe2,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd7_ipg_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xe3,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd8_ipg_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xe4,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd9_ipg_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xe5,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd10_ipg_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xe6,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd11_ipg_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xe7,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd12_ipg_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xe8,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd13_ipg_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xe9,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd14_ipg_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xea,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd15_ipg_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xeb,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd16_ipg_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xec,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd17_ipg_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xed,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd18_ipg_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xee,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd19_ipg_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xef,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd20_ipg_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xf0,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd21_ipg_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xf1,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd22_ipg_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xf2,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd23_ipg_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xf3,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd24_ipg_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xf4,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_sadc1_ctrl_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xf5,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_sadc2_ctrl_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xf6,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_sadc3_ctrl_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xf7,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_acmp1_ctrl_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xf8,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_acmp2_ctrl_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xf9,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_acmp3_ctrl_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xfa,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_acmp4_ctrl_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xfb,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_flexray1_ipg_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xfc,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_flexray2_ipg_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xfd,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_ioc_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xfe,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_pt_sns_sf_clkin = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xff,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_istc_i_istc_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x100,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_smc_clk_24m = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x101,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd1_clk24m = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x102,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd2_clk24m = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x103,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd3_clk24m = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x104,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd4_clk24m = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x105,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd5_clk24m = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x106,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd6_clk24m = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x107,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd7_clk24m = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x108,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd8_clk24m = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x109,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd9_clk24m = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x10a,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd10_clk24m = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x10b,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd11_clk24m = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x10c,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd12_clk24m = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x10d,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd13_clk24m = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x10e,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd14_clk24m = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x10f,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd15_clk24m = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x110,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd16_clk24m = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x111,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd17_clk24m = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x112,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd18_clk24m = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x113,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd19_clk24m = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x114,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd20_clk24m = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x115,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd21_clk24m = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x116,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd22_clk24m = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x117,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd23_clk24m = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x118,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_canfd24_clk24m = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x119,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_xtrg_wdt_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x11a,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_ldo_dig_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x11b,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_dcdc1_clk_24m = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x11c,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_vic1_wdt_ref_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x11d,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_vic3a_wdt_ref_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x11e,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_vic3b_wdt_ref_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x11f,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_wdt1_main_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x120,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_wdt2_main_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x121,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_wdt5_main_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x122,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_wdt6_main_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x123,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_btm1_i_xtal24mhz_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x124,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_btm2_i_xtal24mhz_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x125,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_btm3_i_xtal24mhz_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x126,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_btm4_i_xtal24mhz_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x127,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_btm5_i_xtal24mhz_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x128,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_btm6_i_xtal24mhz_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x129,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_pt_sns_sf_dig_clk_24m = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x12a,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_gama1_hclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x12b,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_xspi_slv_aclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x12c,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_vic2a_hclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x12d,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_vic2b_hclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x12e,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_vic_sp_irqsync = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x12f,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_enet1_aclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x130,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_enet2_aclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x131,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_gama1_aclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x132,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_ahb_syncup_gama1_mainclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x133,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_iramc4_aclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x134,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_wdt3_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x135,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_wdt3_bus_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x136,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_wdt4_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x137,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_wdt4_bus_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x138,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_ppc_apbmux5_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x139,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_apbmux5_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x13a,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_xspi_slv_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x13b,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_enet1_clk_csr = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x13c,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_enet2_clk_csr = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x13d,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_adma_enet1_perclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x13e,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_adma_enet2_perclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x13f,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_sp_mac_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x140,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_cr5_sp_mix_mac_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x141,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_eic_sp_i_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x142,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_cslite_pclkdbg = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x143,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_cslite_pclksys = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x144,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_apb_cr5sx0_dbg_slv_src_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x145,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_apb_cr5sx1_dbg_slv_src_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x146,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_apb_cr5sf_dbg_slv_src_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x147,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_iramc4_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x148,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_wdt3_main_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x149,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_wdt4_main_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x14a,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_vic2a_wdt_ref_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x14b,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_vic2b_wdt_ref_clk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x14c,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_rtc_pclk = {
    .clk_node = {
        .base = APB_CKGEN_SF_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x14d,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_ap_apmainclk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_BCG_TYPE,
        .id = 0x0,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_ap_dispclk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_BCG_TYPE,
        .id = 0x1,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_ap_perclk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_BCG_TYPE,
        .id = 0x2,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_ap_hsmperclk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_BCG_TYPE,
        .id = 0x3,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_ap_sdramclk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_BCG_TYPE,
        .id = 0x4,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_disp_mainclk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_BCG_TYPE,
        .id = 0x5,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_disp_perclk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_BCG_TYPE,
        .id = 0x6,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_sehc1_aclk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x0,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_sehc2_aclk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x1,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_dma_ap_clk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x2,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_dma_ap_aclk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x3,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_dma_ap_hclk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x4,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_seip_sh_clk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x5,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_apbmux6_pclk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x6,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_sdramc_pclk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x7,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_gpio_ap_pclk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x8,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_iomuxc_ap_pclk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x9,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_ckgen_ap_pclk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xa,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_usb2_pclk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xb,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_sehc1_pclk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xc,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_sehc2_pclk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xd,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_rstgen_ap_pclk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xe,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_wdt8_pclk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0xf,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_wdt8_bus_clk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x10,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_saci1_pclk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x11,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_saci2_pclk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x12,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_dma_ap_pclk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x13,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_ap_mac_pclk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x14,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_vd_ap_pclk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x15,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_pt_sns_ap_pclk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x16,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_scr_ap_pclk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x17,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_por_ap_pclk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x18,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_seip_pclk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x19,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_seip_hclk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x1a,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_seip_fd_ref_clk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x1b,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_sdramc_aclk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x1c,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_sehc1_main_clk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x1d,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_sehc2_main_clk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x1e,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_saci1_i2s_mclk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x1f,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_saci2_i2s_mclk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x20,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_saci1_ext_clk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x21,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_saci2_ext_clk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x22,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_saci1_i2s_tx_clk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x23,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_saci2_i2s_tx_clk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x24,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_saci1_i2s_rx_clk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x25,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_saci2_i2s_rx_clk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x26,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_saci1_pdm_per_clk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x27,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_saci2_pdm_per_clk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x28,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_pt_sns_ap_clkin = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x29,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_usb_phy_ref_clk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x2a,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_wdt8_main_clk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x2b,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_sehc1_cqe_sqs_clk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x2c,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_sehc2_cqe_sqs_clk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x2d,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_sehc1_tm_clk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x2e,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_sehc2_tm_clk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x2f,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_csi_axi_clk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x30,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_dc_axi_clk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x31,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_g2d_axi_clk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x32,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_csi_pclk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x33,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_apbmux7_pclk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x34,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_disp_mux_pclk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x35,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_dc_pix_clk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x36,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_csi_proc_clk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x37,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_disp_mux_disp_ckgen_pix_clk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x38,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_lvds_ch0_clk_x14 = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x39,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_lvds_ch0_clk_x7 = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x3a,
    },
};

const sdrv_ckgen_cg_node_t g_ckgen_gating_dc_dsp_clk = {
    .clk_node = {
        .base = APB_CKGEN_AP_BASE,
        .type = CKGEN_PCG_TYPE,
        .id = 0x3b,
    },
};

const sdrv_ckgen_node_t *g_ckgen_adc1[] = {
    CLK_NODE(g_ckgen_gating_sadc1_pclk),
    CLK_NODE(g_ckgen_gating_sadc1_ctrl_clk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_adc2[] = {
    CLK_NODE(g_ckgen_gating_sadc2_pclk),
    CLK_NODE(g_ckgen_gating_sadc2_ctrl_clk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_adc3[] = {
    CLK_NODE(g_ckgen_gating_sadc3_pclk),
    CLK_NODE(g_ckgen_gating_sadc3_ctrl_clk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_acmp1[] = {
    CLK_NODE(g_ckgen_gating_acmp1_pclk),
    CLK_NODE(g_ckgen_gating_acmp1_ctrl_clk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_acmp2[] = {
    CLK_NODE(g_ckgen_gating_acmp2_pclk),
    CLK_NODE(g_ckgen_gating_acmp2_ctrl_clk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_acmp3[] = {
    CLK_NODE(g_ckgen_gating_acmp3_pclk),
    CLK_NODE(g_ckgen_gating_acmp3_ctrl_clk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_acmp4[] = {
    CLK_NODE(g_ckgen_gating_acmp4_pclk),
    CLK_NODE(g_ckgen_gating_acmp4_ctrl_clk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_btm1[] = {
    CLK_NODE(g_ckgen_gating_btm1_pclk),
    CLK_NODE(g_ckgen_gating_btm1_i_xtal24mhz_clk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_btm2[] = {
    CLK_NODE(g_ckgen_gating_btm2_pclk),
    CLK_NODE(g_ckgen_gating_btm2_i_xtal24mhz_clk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_btm3[] = {
    CLK_NODE(g_ckgen_gating_btm3_pclk),
    CLK_NODE(g_ckgen_gating_btm3_i_xtal24mhz_clk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_btm4[] = {
    CLK_NODE(g_ckgen_gating_btm4_pclk),
    CLK_NODE(g_ckgen_gating_btm4_i_xtal24mhz_clk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_btm5[] = {
    CLK_NODE(g_ckgen_gating_btm5_pclk),
    CLK_NODE(g_ckgen_gating_btm5_i_xtal24mhz_clk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_btm6[] = {
    CLK_NODE(g_ckgen_gating_btm6_pclk),
    CLK_NODE(g_ckgen_gating_btm6_i_xtal24mhz_clk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_canfd1[] = {
    CLK_NODE(g_ckgen_gating_canfd1_pclk),
    CLK_NODE(g_ckgen_gating_canfd1_ipg_clk),
    CLK_NODE(g_ckgen_gating_canfd1_clk24m),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_canfd10[] = {
    CLK_NODE(g_ckgen_gating_canfd10_pclk),
    CLK_NODE(g_ckgen_gating_canfd10_ipg_clk),
    CLK_NODE(g_ckgen_gating_canfd10_clk24m),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_canfd11[] = {
    CLK_NODE(g_ckgen_gating_canfd11_pclk),
    CLK_NODE(g_ckgen_gating_canfd11_ipg_clk),
    CLK_NODE(g_ckgen_gating_canfd11_clk24m),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_canfd12[] = {
    CLK_NODE(g_ckgen_gating_canfd12_pclk),
    CLK_NODE(g_ckgen_gating_canfd12_ipg_clk),
    CLK_NODE(g_ckgen_gating_canfd12_clk24m),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_canfd13[] = {
    CLK_NODE(g_ckgen_gating_canfd13_pclk),
    CLK_NODE(g_ckgen_gating_canfd13_ipg_clk),
    CLK_NODE(g_ckgen_gating_canfd13_clk24m),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_canfd14[] = {
    CLK_NODE(g_ckgen_gating_canfd14_pclk),
    CLK_NODE(g_ckgen_gating_canfd14_ipg_clk),
    CLK_NODE(g_ckgen_gating_canfd14_clk24m),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_canfd15[] = {
    CLK_NODE(g_ckgen_gating_canfd15_pclk),
    CLK_NODE(g_ckgen_gating_canfd15_ipg_clk),
    CLK_NODE(g_ckgen_gating_canfd15_clk24m),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_canfd16[] = {
    CLK_NODE(g_ckgen_gating_canfd16_pclk),
    CLK_NODE(g_ckgen_gating_canfd16_ipg_clk),
    CLK_NODE(g_ckgen_gating_canfd16_clk24m),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_canfd17[] = {
    CLK_NODE(g_ckgen_gating_canfd17_pclk),
    CLK_NODE(g_ckgen_gating_canfd17_ipg_clk),
    CLK_NODE(g_ckgen_gating_canfd17_clk24m),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_canfd18[] = {
    CLK_NODE(g_ckgen_gating_canfd18_pclk),
    CLK_NODE(g_ckgen_gating_canfd18_ipg_clk),
    CLK_NODE(g_ckgen_gating_canfd18_clk24m),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_canfd19[] = {
    CLK_NODE(g_ckgen_gating_canfd19_pclk),
    CLK_NODE(g_ckgen_gating_canfd19_ipg_clk),
    CLK_NODE(g_ckgen_gating_canfd19_clk24m),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_canfd2[] = {
    CLK_NODE(g_ckgen_gating_canfd2_pclk),
    CLK_NODE(g_ckgen_gating_canfd2_ipg_clk),
    CLK_NODE(g_ckgen_gating_canfd2_clk24m),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_canfd20[] = {
    CLK_NODE(g_ckgen_gating_canfd20_pclk),
    CLK_NODE(g_ckgen_gating_canfd20_ipg_clk),
    CLK_NODE(g_ckgen_gating_canfd20_clk24m),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_canfd21[] = {
    CLK_NODE(g_ckgen_gating_canfd21_pclk),
    CLK_NODE(g_ckgen_gating_canfd21_ipg_clk),
    CLK_NODE(g_ckgen_gating_canfd21_clk24m),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_canfd22[] = {
    CLK_NODE(g_ckgen_gating_canfd22_pclk),
    CLK_NODE(g_ckgen_gating_canfd22_ipg_clk),
    CLK_NODE(g_ckgen_gating_canfd22_clk24m),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_canfd23[] = {
    CLK_NODE(g_ckgen_gating_canfd23_pclk),
    CLK_NODE(g_ckgen_gating_canfd23_ipg_clk),
    CLK_NODE(g_ckgen_gating_canfd23_clk24m),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_canfd24[] = {
    CLK_NODE(g_ckgen_gating_canfd24_pclk),
    CLK_NODE(g_ckgen_gating_canfd24_ipg_clk),
    CLK_NODE(g_ckgen_gating_canfd24_clk24m),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_canfd3[] = {
    CLK_NODE(g_ckgen_gating_canfd3_pclk),
    CLK_NODE(g_ckgen_gating_canfd3_ipg_clk),
    CLK_NODE(g_ckgen_gating_canfd3_clk24m),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_canfd4[] = {
    CLK_NODE(g_ckgen_gating_canfd4_pclk),
    CLK_NODE(g_ckgen_gating_canfd4_ipg_clk),
    CLK_NODE(g_ckgen_gating_canfd4_clk24m),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_canfd5[] = {
    CLK_NODE(g_ckgen_gating_canfd5_pclk),
    CLK_NODE(g_ckgen_gating_canfd5_ipg_clk),
    CLK_NODE(g_ckgen_gating_canfd5_clk24m),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_canfd6[] = {
    CLK_NODE(g_ckgen_gating_canfd6_pclk),
    CLK_NODE(g_ckgen_gating_canfd6_ipg_clk),
    CLK_NODE(g_ckgen_gating_canfd6_clk24m),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_canfd7[] = {
    CLK_NODE(g_ckgen_gating_canfd7_pclk),
    CLK_NODE(g_ckgen_gating_canfd7_ipg_clk),
    CLK_NODE(g_ckgen_gating_canfd7_clk24m),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_canfd8[] = {
    CLK_NODE(g_ckgen_gating_canfd8_pclk),
    CLK_NODE(g_ckgen_gating_canfd8_ipg_clk),
    CLK_NODE(g_ckgen_gating_canfd8_clk24m),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_canfd9[] = {
    CLK_NODE(g_ckgen_gating_canfd9_pclk),
    CLK_NODE(g_ckgen_gating_canfd9_ipg_clk),
    CLK_NODE(g_ckgen_gating_canfd9_clk24m),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_cr5_sf[] = {
    CLK_NODE(g_ckgen_gating_cr5_sf_clkin),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_cr5_sp[] = {
    CLK_NODE(g_ckgen_gating_cr5_sp0_clkin),
    CLK_NODE(g_ckgen_gating_cr5_sp1_clkin),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_cr5_sx[] = {
    CLK_NODE(g_ckgen_gating_sf_cr5_sxclk),
    CLK_NODE(g_ckgen_gating_cr5_sx0_clkin),
    CLK_NODE(g_ckgen_gating_cr5_sx1_clkin),
    CLK_NODE(g_ckgen_gating_sx_mainclk),
    CLK_NODE(g_ckgen_gating_sx_sf_mainclk),
    CLK_NODE(g_ckgen_gating_sx_sf_perclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_csi[] = {
    CLK_NODE(g_ckgen_gating_csi_axi_clk),
    CLK_NODE(g_ckgen_gating_csi_pclk),
    CLK_NODE(g_ckgen_gating_csi_proc_clk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_cslite[] = {
    CLK_NODE(g_ckgen_gating_cslite_pclkdbg),
    CLK_NODE(g_ckgen_gating_cslite_pclksys),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_dc[] = {
    CLK_NODE(g_ckgen_gating_dc_axi_clk),
    CLK_NODE(g_ckgen_gating_dc_pix_clk),
    CLK_NODE(g_ckgen_gating_dc_dsp_clk),
    CLK_NODE(g_ckgen_gating_disp_mux_disp_ckgen_pix_clk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_dcdc1[] = {
    CLK_NODE(g_ckgen_gating_dcdc1_clk_24m),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_dma_ap[] = {
    CLK_NODE(g_ckgen_gating_dma_ap_clk),
    CLK_NODE(g_ckgen_gating_dma_ap_aclk),
    CLK_NODE(g_ckgen_gating_dma_ap_hclk),
    CLK_NODE(g_ckgen_gating_dma_ap_pclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_dma_sf[] = {
    CLK_NODE(g_ckgen_gating_dma_sf_clk0),
    CLK_NODE(g_ckgen_gating_dma_sf_clk1),
    CLK_NODE(g_ckgen_gating_dma_sf_aclk0),
    CLK_NODE(g_ckgen_gating_dma_sf_aclk1),
    CLK_NODE(g_ckgen_gating_dma_sf_hclk0),
    CLK_NODE(g_ckgen_gating_dma_sf_hclk1),
    CLK_NODE(g_ckgen_gating_dma_sf_pclk0),
    CLK_NODE(g_ckgen_gating_dma_sf_pclk1),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_efusec[] = {
    CLK_NODE(g_ckgen_gating_efusec_pclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_eic_boot[] = {
    CLK_NODE(g_ckgen_gating_eic_boot_pclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_eic_sf[] = {
    CLK_NODE(g_ckgen_gating_eic_sf_pclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_eic_sp[] = {
    CLK_NODE(g_ckgen_gating_eic_sp_i_pclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_enet1[] = {
    CLK_NODE(g_ckgen_gating_enet1_ref_clk_tx),
    CLK_NODE(g_ckgen_gating_enet1_ptp_ref_clk),
    CLK_NODE(g_ckgen_gating_enet1_aclk),
    CLK_NODE(g_ckgen_gating_enet1_clk_csr),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_enet2[] = {
    CLK_NODE(g_ckgen_gating_enet2_ptp_ref_clk),
    CLK_NODE(g_ckgen_gating_enet2_ref_clk_tx),
    CLK_NODE(g_ckgen_gating_enet2_aclk),
    CLK_NODE(g_ckgen_gating_enet2_clk_csr),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_epwm1[] = {
    CLK_NODE(g_ckgen_gating_epwm1_pclk),
    CLK_NODE(g_ckgen_gating_epwm1_ahf_clk),
    CLK_NODE(g_ckgen_gating_epwm1_hf_clk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_epwm2[] = {
    CLK_NODE(g_ckgen_gating_epwm2_pclk),
    CLK_NODE(g_ckgen_gating_epwm2_ahf_clk),
    CLK_NODE(g_ckgen_gating_epwm2_hf_clk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_epwm3[] = {
    CLK_NODE(g_ckgen_gating_epwm3_pclk),
    CLK_NODE(g_ckgen_gating_epwm3_ahf_clk),
    CLK_NODE(g_ckgen_gating_epwm3_hf_clk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_epwm4[] = {
    CLK_NODE(g_ckgen_gating_epwm4_pclk),
    CLK_NODE(g_ckgen_gating_epwm4_ahf_clk),
    CLK_NODE(g_ckgen_gating_epwm4_hf_clk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_etmr1[] = {
    CLK_NODE(g_ckgen_gating_etmr1_pclk),
    CLK_NODE(g_ckgen_gating_etmr1_ahf_clk),
    CLK_NODE(g_ckgen_gating_etmr1_hf_clk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_etmr2[] = {
    CLK_NODE(g_ckgen_gating_etmr2_pclk),
    CLK_NODE(g_ckgen_gating_etmr2_ahf_clk),
    CLK_NODE(g_ckgen_gating_etmr2_hf_clk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_etmr3[] = {
    CLK_NODE(g_ckgen_gating_etmr3_pclk),
    CLK_NODE(g_ckgen_gating_etmr3_ahf_clk),
    CLK_NODE(g_ckgen_gating_etmr3_hf_clk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_etmr4[] = {
    CLK_NODE(g_ckgen_gating_etmr4_pclk),
    CLK_NODE(g_ckgen_gating_etmr4_ahf_clk),
    CLK_NODE(g_ckgen_gating_etmr4_hf_clk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_flexray1[] = {
    CLK_NODE(g_ckgen_gating_flexray1_hclk),
    CLK_NODE(g_ckgen_gating_flexray1_ipg_clk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_flexray2[] = {
    CLK_NODE(g_ckgen_gating_flexray2_hclk),
    CLK_NODE(g_ckgen_gating_flexray2_ipg_clk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_g2d[] = {
    CLK_NODE(g_ckgen_gating_g2d_axi_clk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_gama1[] = {
    CLK_NODE(g_ckgen_gating_gama1_hclk),
    CLK_NODE(g_ckgen_gating_gama1_aclk),
    CLK_NODE(g_ckgen_gating_ahb_syncup_gama1_mainclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_gpio_ap[] = {
    CLK_NODE(g_ckgen_gating_gpio_ap_pclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_gpio_sf[] = {
    CLK_NODE(g_ckgen_gating_gpio_sf_pclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_i2c1[] = {
    CLK_NODE(g_ckgen_gating_i2c1_pclk),
    CLK_NODE(g_ckgen_gating_i2c1_sclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_i2c2[] = {
    CLK_NODE(g_ckgen_gating_i2c2_pclk),
    CLK_NODE(g_ckgen_gating_i2c2_sclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_i2c3[] = {
    CLK_NODE(g_ckgen_gating_i2c3_pclk),
    CLK_NODE(g_ckgen_gating_i2c3_sclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_i2c4[] = {
    CLK_NODE(g_ckgen_gating_i2c4_pclk),
    CLK_NODE(g_ckgen_gating_i2c4_sclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_i2c5[] = {
    CLK_NODE(g_ckgen_gating_i2c5_pclk),
    CLK_NODE(g_ckgen_gating_i2c5_sclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_i2c6[] = {
    CLK_NODE(g_ckgen_gating_i2c6_pclk),
    CLK_NODE(g_ckgen_gating_i2c6_sclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_i2c7[] = {
    CLK_NODE(g_ckgen_gating_i2c7_pclk),
    CLK_NODE(g_ckgen_gating_i2c7_sclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_i2c8[] = {
    CLK_NODE(g_ckgen_gating_i2c8_pclk),
    CLK_NODE(g_ckgen_gating_i2c8_sclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_ioc[] = {
    CLK_NODE(g_ckgen_gating_ioc_pclk),
    CLK_NODE(g_ckgen_gating_ioc_clk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_iomuxc_ap[] = {
    CLK_NODE(g_ckgen_gating_iomuxc_ap_pclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_iomuxc_sf[] = {
    CLK_NODE(g_ckgen_gating_iomuxc_sf_pclk),
    CLK_NODE(g_ckgen_gating_iomuxc_sf_comp_pclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_iramc1[] = {
    CLK_NODE(g_ckgen_gating_iramc1_aclk),
    CLK_NODE(g_ckgen_gating_iramc1_pclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_iramc2[] = {
    CLK_NODE(g_ckgen_gating_iramc2_aclk),
    CLK_NODE(g_ckgen_gating_iramc2_pclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_iramc3[] = {
    CLK_NODE(g_ckgen_gating_iramc3_aclk),
    CLK_NODE(g_ckgen_gating_iramc3_pclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_iramc4[] = {
    CLK_NODE(g_ckgen_gating_iramc4_aclk),
    CLK_NODE(g_ckgen_gating_iramc4_pclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_iromc[] = {
    CLK_NODE(g_ckgen_gating_iromc_aclk),
    CLK_NODE(g_ckgen_gating_iromc_pclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_ldo_dig[] = {
    CLK_NODE(g_ckgen_gating_ldo_dig_pclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_lvds_ss[] = {
    CLK_NODE(g_ckgen_gating_lvds_ch0_clk_x14),
    CLK_NODE(g_ckgen_gating_lvds_ch0_clk_x7),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_mb[] = {
    CLK_NODE(g_ckgen_gating_mb_clk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_por_ap[] = {
    CLK_NODE(g_ckgen_gating_por_ap_pclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_por_sf_dig[] = {
    CLK_NODE(g_ckgen_gating_por_sf_dig_pclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_pt_sns_ap[] = {
    CLK_NODE(g_ckgen_gating_pt_sns_ap_pclk),
    CLK_NODE(g_ckgen_gating_pt_sns_ap_clkin),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_pt_sns_sf_ana[] = {
    CLK_NODE(g_ckgen_gating_pt_sns_sf_clkin),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_pt_sns_sf_dig[] = {
    CLK_NODE(g_ckgen_gating_pt_sns_sf_dig_i_pclk),
    CLK_NODE(g_ckgen_gating_pt_sns_sf_clkin),
    CLK_NODE(g_ckgen_gating_pt_sns_sf_dig_clk_24m),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_rstgen_ap[] = {
    CLK_NODE(g_ckgen_gating_rstgen_ap_pclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_rstgen_sf[] = {
    CLK_NODE(g_ckgen_gating_rstgen_sf_pclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_saci1[] = {
    CLK_NODE(g_ckgen_gating_saci1_pclk),
    CLK_NODE(g_ckgen_gating_saci1_i2s_mclk),
    CLK_NODE(g_ckgen_gating_saci1_ext_clk),
    CLK_NODE(g_ckgen_gating_saci1_i2s_tx_clk),
    CLK_NODE(g_ckgen_gating_saci1_i2s_rx_clk),
    CLK_NODE(g_ckgen_gating_saci1_pdm_per_clk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_saci2[] = {
    CLK_NODE(g_ckgen_gating_saci2_pclk),
    CLK_NODE(g_ckgen_gating_saci2_i2s_mclk),
    CLK_NODE(g_ckgen_gating_saci2_ext_clk),
    CLK_NODE(g_ckgen_gating_saci2_i2s_tx_clk),
    CLK_NODE(g_ckgen_gating_saci2_i2s_rx_clk),
    CLK_NODE(g_ckgen_gating_saci2_pdm_per_clk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_scr_ap[] = {
    CLK_NODE(g_ckgen_gating_scr_ap_pclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_scr_sf[] = {
    CLK_NODE(g_ckgen_gating_scr_sf_pclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_sdramc[] = {
    CLK_NODE(g_ckgen_gating_ap_sdramclk),
    CLK_NODE(g_ckgen_gating_sdramc_pclk),
    CLK_NODE(g_ckgen_gating_sdramc_aclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_sehc1[] = {
    CLK_NODE(g_ckgen_gating_sehc1_aclk),
    CLK_NODE(g_ckgen_gating_sehc1_pclk),
    CLK_NODE(g_ckgen_gating_sehc1_main_clk),
    CLK_NODE(g_ckgen_gating_sehc1_cqe_sqs_clk),
    CLK_NODE(g_ckgen_gating_sehc1_tm_clk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_sehc2[] = {
    CLK_NODE(g_ckgen_gating_sehc2_aclk),
    CLK_NODE(g_ckgen_gating_sehc2_pclk),
    CLK_NODE(g_ckgen_gating_sehc2_main_clk),
    CLK_NODE(g_ckgen_gating_sehc2_cqe_sqs_clk),
    CLK_NODE(g_ckgen_gating_sehc2_tm_clk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_seip[] = {
    CLK_NODE(g_ckgen_gating_seip_sh_clk),
    CLK_NODE(g_ckgen_gating_seip_pclk),
    CLK_NODE(g_ckgen_gating_seip_hclk),
    CLK_NODE(g_ckgen_gating_seip_fd_ref_clk),
    CLK_NODE(g_ckgen_gating_ap_hsmperclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_sem1[] = {
    CLK_NODE(g_ckgen_gating_sem1_pclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_sem2[] = {
    CLK_NODE(g_ckgen_gating_sem2_pclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_smc[] = {
    CLK_NODE(g_ckgen_gating_smc_pclk),
    CLK_NODE(g_ckgen_gating_smc_clk_24m),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_spi1[] = {
    CLK_NODE(g_ckgen_gating_spi1_pclk),
    CLK_NODE(g_ckgen_gating_spi1_spi_per_clk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_spi2[] = {
    CLK_NODE(g_ckgen_gating_spi2_pclk),
    CLK_NODE(g_ckgen_gating_spi2_spi_per_clk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_spi3[] = {
    CLK_NODE(g_ckgen_gating_spi3_pclk),
    CLK_NODE(g_ckgen_gating_spi3_spi_per_clk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_spi4[] = {
    CLK_NODE(g_ckgen_gating_spi4_pclk),
    CLK_NODE(g_ckgen_gating_spi4_spi_per_clk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_spi5[] = {
    CLK_NODE(g_ckgen_gating_spi5_pclk),
    CLK_NODE(g_ckgen_gating_spi5_spi_per_clk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_spi6[] = {
    CLK_NODE(g_ckgen_gating_spi6_pclk),
    CLK_NODE(g_ckgen_gating_spi6_spi_per_clk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_spi7[] = {
    CLK_NODE(g_ckgen_gating_spi7_pclk),
    CLK_NODE(g_ckgen_gating_spi7_spi_per_clk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_spi8[] = {
    CLK_NODE(g_ckgen_gating_spi8_pclk),
    CLK_NODE(g_ckgen_gating_spi8_spi_per_clk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_uart1[] = {
    CLK_NODE(g_ckgen_gating_uart1_pclk),
    CLK_NODE(g_ckgen_gating_uart1_sclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_uart10[] = {
    CLK_NODE(g_ckgen_gating_uart10_pclk),
    CLK_NODE(g_ckgen_gating_uart10_sclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_uart11[] = {
    CLK_NODE(g_ckgen_gating_uart11_pclk),
    CLK_NODE(g_ckgen_gating_uart11_sclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_uart12[] = {
    CLK_NODE(g_ckgen_gating_uart12_pclk),
    CLK_NODE(g_ckgen_gating_uart12_sclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_uart13[] = {
    CLK_NODE(g_ckgen_gating_uart13_pclk),
    CLK_NODE(g_ckgen_gating_uart13_sclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_uart14[] = {
    CLK_NODE(g_ckgen_gating_uart14_pclk),
    CLK_NODE(g_ckgen_gating_uart14_sclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_uart15[] = {
    CLK_NODE(g_ckgen_gating_uart15_pclk),
    CLK_NODE(g_ckgen_gating_uart15_sclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_uart16[] = {
    CLK_NODE(g_ckgen_gating_uart16_pclk),
    CLK_NODE(g_ckgen_gating_uart16_sclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_uart2[] = {
    CLK_NODE(g_ckgen_gating_uart2_pclk),
    CLK_NODE(g_ckgen_gating_uart2_sclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_uart3[] = {
    CLK_NODE(g_ckgen_gating_uart3_pclk),
    CLK_NODE(g_ckgen_gating_uart3_sclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_uart4[] = {
    CLK_NODE(g_ckgen_gating_uart4_pclk),
    CLK_NODE(g_ckgen_gating_uart4_sclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_uart5[] = {
    CLK_NODE(g_ckgen_gating_uart5_pclk),
    CLK_NODE(g_ckgen_gating_uart5_sclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_uart6[] = {
    CLK_NODE(g_ckgen_gating_uart6_pclk),
    CLK_NODE(g_ckgen_gating_uart6_sclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_uart7[] = {
    CLK_NODE(g_ckgen_gating_uart7_pclk),
    CLK_NODE(g_ckgen_gating_uart7_sclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_uart8[] = {
    CLK_NODE(g_ckgen_gating_uart8_pclk),
    CLK_NODE(g_ckgen_gating_uart8_sclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_uart9[] = {
    CLK_NODE(g_ckgen_gating_uart9_pclk),
    CLK_NODE(g_ckgen_gating_uart9_sclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_usb[] = {
    CLK_NODE(g_ckgen_gating_usb2_pclk),
    CLK_NODE(g_ckgen_gating_usb_phy_ref_clk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_vd_ap[] = {
    CLK_NODE(g_ckgen_gating_vd_ap_pclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_vd_sf_dig[] = {
    CLK_NODE(g_ckgen_gating_vd_sf_dig_pclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_vic1[] = {
    CLK_NODE(g_ckgen_gating_vic1_hclk),
    CLK_NODE(g_ckgen_gating_vic1_wdt_ref_clk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_vic2[] = {
    CLK_NODE(g_ckgen_gating_vic2a_hclk),
    CLK_NODE(g_ckgen_gating_vic2b_hclk),
    CLK_NODE(g_ckgen_gating_vic2a_wdt_ref_clk),
    CLK_NODE(g_ckgen_gating_vic2b_wdt_ref_clk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_vic3[] = {
    CLK_NODE(g_ckgen_gating_vic3a_hclk),
    CLK_NODE(g_ckgen_gating_vic3b_hclk),
    CLK_NODE(g_ckgen_gating_vic3a_wdt_ref_clk),
    CLK_NODE(g_ckgen_gating_vic3b_wdt_ref_clk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_wdt1[] = {
    CLK_NODE(g_ckgen_gating_wdt1_pclk),
    CLK_NODE(g_ckgen_gating_wdt1_bus_clk),
    CLK_NODE(g_ckgen_gating_wdt1_main_clk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_wdt2[] = {
    CLK_NODE(g_ckgen_gating_wdt2_pclk),
    CLK_NODE(g_ckgen_gating_wdt2_bus_clk),
    CLK_NODE(g_ckgen_gating_wdt2_main_clk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_wdt3[] = {
    CLK_NODE(g_ckgen_gating_wdt3_pclk),
    CLK_NODE(g_ckgen_gating_wdt3_bus_clk),
    CLK_NODE(g_ckgen_gating_wdt3_main_clk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_wdt4[] = {
    CLK_NODE(g_ckgen_gating_wdt4_pclk),
    CLK_NODE(g_ckgen_gating_wdt4_bus_clk),
    CLK_NODE(g_ckgen_gating_wdt4_main_clk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_wdt5[] = {
    CLK_NODE(g_ckgen_gating_wdt5_pclk),
    CLK_NODE(g_ckgen_gating_wdt5_bus_clk),
    CLK_NODE(g_ckgen_gating_wdt5_main_clk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_wdt6[] = {
    CLK_NODE(g_ckgen_gating_wdt6_pclk),
    CLK_NODE(g_ckgen_gating_wdt6_bus_clk),
    CLK_NODE(g_ckgen_gating_wdt6_main_clk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_wdt8[] = {
    CLK_NODE(g_ckgen_gating_wdt8_pclk),
    CLK_NODE(g_ckgen_gating_wdt8_bus_clk),
    CLK_NODE(g_ckgen_gating_wdt8_main_clk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_xspi1a[] = {
    CLK_NODE(g_ckgen_gating_xspi1a_clk),
    CLK_NODE(g_ckgen_gating_mpc_xspi1a_aclk),
    CLK_NODE(g_ckgen_gating_xspi1_aclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_xspi1b[] = {
    CLK_NODE(g_ckgen_gating_xspi1b_clk),
    CLK_NODE(g_ckgen_gating_mpc_xspi1b_aclk),
    CLK_NODE(g_ckgen_gating_xspi1_bclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_xspi2a[] = {
    CLK_NODE(g_ckgen_gating_xspi2a_clk),
    CLK_NODE(g_ckgen_gating_mpc_xspi2a_aclk),
    CLK_NODE(g_ckgen_gating_xspi2_aclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_xspi2b[] = {
    CLK_NODE(g_ckgen_gating_xspi2b_clk),
    CLK_NODE(g_ckgen_gating_mpc_xspi2b_aclk),
    CLK_NODE(g_ckgen_gating_xspi2_bclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_xspi_slv[] = {
    CLK_NODE(g_ckgen_gating_xspi_slv_aclk),
    CLK_NODE(g_ckgen_gating_xspi_slv_pclk),
    NULL
};

const sdrv_ckgen_node_t *g_ckgen_xtrg[] = {
    CLK_NODE(g_ckgen_gating_xtrg_pclk),
    CLK_NODE(g_ckgen_gating_xtrg_clk),
    CLK_NODE(g_ckgen_gating_xtrg_wdt_clk),
    NULL
};

const sdrv_ckgen_node_t **g_ckgen_unused[] = {
    g_ckgen_cr5_sx,
    g_ckgen_cr5_sp,
    g_ckgen_iramc2,
    g_ckgen_iramc3,
    g_ckgen_iramc4,
    g_ckgen_canfd1,
    g_ckgen_canfd2,
    g_ckgen_canfd8,
    g_ckgen_canfd9,
    g_ckgen_canfd10,
    g_ckgen_canfd11,
    g_ckgen_canfd12,
    g_ckgen_canfd13,
    g_ckgen_canfd14,
    g_ckgen_canfd15,
    g_ckgen_canfd17,
    g_ckgen_canfd18,
    g_ckgen_canfd19,
    g_ckgen_canfd20,
    g_ckgen_canfd22,
    g_ckgen_canfd24,
    g_ckgen_uart13,
    g_ckgen_uart14,
    g_ckgen_uart15,
    g_ckgen_uart16,
    g_ckgen_enet2,
    g_ckgen_xspi2a,
    g_ckgen_xspi2b,
    g_ckgen_i2c1,
    g_ckgen_i2c2,
    g_ckgen_i2c7,
    g_ckgen_i2c8,
    g_ckgen_spi7,
    g_ckgen_spi8,
    g_ckgen_saci1,
    g_ckgen_acmp3,
    g_ckgen_acmp4,
    g_ckgen_csi,
    g_ckgen_g2d,
    g_ckgen_lvds_ss,
    g_ckgen_dc,
    g_ckgen_sdramc,
    g_ckgen_gama1,
    g_ckgen_etmr3,
    g_ckgen_etmr4,
    g_ckgen_epwm3,
    g_ckgen_epwm4,
    g_ckgen_flexray1,
    g_ckgen_flexray2,
    g_ckgen_wdt3,
    g_ckgen_wdt4,
    g_ckgen_wdt5,
    g_ckgen_wdt6,
    NULL
};
