一、SES FlashDebug模式使用方法：
1. 添加SemiDrive devices。
   在JLink AppData目录下（常见为C:\Users\Administrator\AppData\Roaming\SEGGER）添加SemiDrive devices。
   具体方法为：将ssdk\prebuilts\JLinkDevices目录拷贝到Link AppData目录下的SEGGER文件夹内。
   其中，包含JLinkDevices.xml文件和Devices\SemiDrive文件夹所有芯片Part的Flashloader。

2. 将Devices信息拷贝到Jlink安装目录（Jlink版本大于V7.62不需要这一步）。
   如果Jlink版本低于V7.62（可以在Windows开始菜单栏搜索Jlink查看），还需要额外再将Devices信息拷贝到Jlink安装目录。
   先将ssdk_dev\prebuilts\JLinkDevices\Devices\SemiDrive目录拷贝到Jlink安装目录下的Devices文件夹内（如C:\Program Files\SEGGER\JLink\Devices）。
   再将ssdk_dev\prebuilts\JLinkDevices\JLinkDevices.xml文件替换Jlink安装目录原有的JLinkDevices.xml文件（建议先将原文件备份再替换）。

3. 重新打开SES工程（如在完成第1步之前先打开了SES工程，需要关闭后重新打开工程）。

4. 编译FlashDebug模式工程（xip需要先编译bootloader工程，再编译app工程的Debug或FlashDebug模式）。
   编译完成后会在SES\Output\FlashDebug\Exe目录中自动生成签名bin文件，如sf_signed.bin。

5. 在SES IDE中点击Target——Download xxx。
   IDE调用Flashloader完成flash文件下载。

二、FlashDebug模式的配置包含如下内容（已默认添加到工程中）：
1.post-build
    "$(ProjectDir)/../../../../../prebuilts/windows/python-3.7.0/python.exe" "$(ProjectDir)/../../../../../devices/script/SES/cmd_download.py" "$(JLinkDir)" "$(SolutionPath)" "$(PART_ID)" "HYPERFLASH" "$(OPS)"
    或者
    "$(ProjectDir)/../../../../../prebuilts/windows/python-3.7.0/python.exe" "$(ProjectDir)/../../../../../devices/script/SES/cmd_download.py" "$(JLinkDir)" "$(SolutionPath)" "$(PART_ID)" "NORFLASH" "$(OPS)"

2.Target Script
Reset Script:
init_before_download("$(ProjectDir)/../../../../../devices/$(PART_ID)/flashloader/init_before_download_hyperflash.txt")
或者
init_before_download("$(ProjectDir)/../../../../../devices/$(PART_ID)/flashloader/init_before_download_norflash.txt")
Target Script File:
$(ProjectDir)/../../../../../devices/script/SES/init_before_download.js

3.loader
driver类:
    所有工程
        debug：
            无需配置
        FlashDebug:
            Download_HYPERFLASH(Download_NORFLASH):
                $(ProjectDir)/Output/FlashDebug/Exe/sf_signed.bin - 0x100C0000(0x10007000)
                $(ProjectDir)/../../../../../tools/sdtools/sfs/sfs_s26h-hyperflash.bin - 0x10000000
xip类:（签名bootloader，所有核的app不签名）
    bootloader工程：
        debug
            无需配置
        FlashDebug:
            不支持
    sf工程中
        debug：
            配置device name
            配置init_before_download
                init_before_download("$(ProjectDir)/../../../../../../devices/$(PART_ID)/flashloader/init_before_download.txt")
                $(ProjectDir)/../../../../../../devices/script/SES/init_before_download.js
        FlashDebug:
            Download_HYPERFLASH(Download_NORFLASH):
                $(ProjectDir)/../../bootloader/SES/Output/Debug/Exe/sf_signed.bin - 0x100C0000(0x10007000)
                $(ProjectDir)/../../../../../../tools/sdtools/sfs/sfs_s26h-hyperflash.bin - 0x10000000
                $(ProjectDir)/Output/FlashDebug/Exe/sf.bin - 0x10140000
            Download_HYPERFLASH_Multicore(Download_NORFLASH_Multicore):
                $(ProjectDir)/../../sp0/SES/Output/FlashDebug/Exe/sp0.bin - 0x10340000(0x10288000)
                $(ProjectDir)/../../sp1/SES/Output/FlashDebug/Exe/sp1.bin - 0x103C0000(0x10308000)
                $(ProjectDir)/../../sx0/SES/Output/FlashDebug/Exe/sx0.bin - 0x10440000(0x10388000)
                $(ProjectDir)/../../sx1/SES/Output/FlashDebug/Exe/sx1.bin - 0x104C0000(0x10408000)
    sp/sx工程中
        debug:
            配置device name
        FlashDebug:
            Download_HYPERFLASH(Download_NORFLASH):
                $(ProjectDir)Output/FlashDebug/Exe/sp0.bin - 0x10340000(0x10288000)
                $(ProjectDir)Output/FlashDebug/Exe/sp1.bin - 0x103C0000(0x10308000)
                $(ProjectDir)Output/FlashDebug/Exe/sx0.bin - 0x10440000(0x10388000)
                $(ProjectDir)Output/FlashDebug/Exe/sx1.bin - 0x104C0000(0x10408000)
多核类：（所有核都签名）
    sf工程中
        debug：
            无需配置
        FlashDebug:
            Download_HYPERFLASH(Download_NORFLASH):
                $(ProjectDir)/Output/FlashDebug/Exe/sf_signed.bin - 0x100C0000(0x10007000)
                $(ProjectDir)/../../../../../../tools/sdtools/sfs/sfs_s26h-hyperflash.bin - 0x10000000
            Download_HYPERFLASH_Multicore(Download_NORFLASH_Multicore):
                $(ProjectDir)/../../sp0/SES/Output/FlashDebug/Exe/sp0_signed.bin - 0x10340000(0x10288000)
                $(ProjectDir)/../../sp1/SES/Output/FlashDebug/Exe/sp1_signed.bin - 0x103C0000(0x10308000)
                $(ProjectDir)/../../sx0/SES/Output/FlashDebug/Exe/sx0_signed.bin - 0x10440000(0x10388000)
                $(ProjectDir)/../../sx1/SES/Output/FlashDebug/Exe/sx1_signed.bin - 0x104C0000(0x10408000)
    sp/sx工程中
        debug：
            无需配置
        FlashDebug:
            不支持

4.target device
  $(PART_ID)_HYPERFLASH/$(PART_ID)_NORFLASH

注意：  norflash和hyperflash下载地址和sfs类型不同

三、命令行下载：
1. 条件：
    安装SEGEER Embeded Studio时请在安装对话框Additional Component中确认勾选Install Little Endian Library Files（默认已勾选）。
2. 下载命令：
    例：
    "D:\Program Files\SEGGER\SEGGER Embedded Studio for ARM 7.10a\bin\emBuild" -config "FlashDebug" -D OPS=download -project "sf" "E:\WSL\git_repo\E3_DEV\boards\e3_gateway\driver_demo\acmp\SES\acmp.emProject"
    "D:\Program Files\SEGGER\SEGGER Embedded Studio for ARM 7.10a\bin\emBuild" -config "FlashDebug" -D OPS=download -project "sf" "E:\WSL\git_repo\E3_DEV\boards\e3_gateway\app_demo\xip\sf\SES\sf.emProject"
