function init_before_download(cmd_file_path_name)
{
    // TargetInterface.message(cmd_file_path_name)
    var JLinkDir = TargetInterface.expandMacro("$(JLinkDir)");
    var DeviceName = TargetInterface.expandMacro("$(DeviceName)");
    var JLinkDir2 = JLinkDir.replace('\\', '/');
    // TargetInterface.message(JLinkDir2);
    var cmd = '"' + JLinkDir2 + "/JLink.exe" + '"' + " -device " + DeviceName + " -if SWD -speed 4000 -autoconnect 1 -CommandFile " + '"' + cmd_file_path_name + '"';
    // TargetInterface.resetAndStop()
    // TargetInterface.delay(1000)
    CWSys.run(cmd, true);
}
