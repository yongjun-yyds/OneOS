@echo off
@REM chcp 65001
setlocal EnableDelayedExpansion



@REM 配置路径
set JLinkPathName="C:\Program Files (x86)\SEGGER\JLink\JLink.exe"
set JFlashPathName="C:\Program Files (x86)\SEGGER\JLink\JFlash.exe"
set device="E3110_norflash"

@REM E3:=0x3E0001/0x3F0001 E3L:0x4E0001/0x4F0001
set TCMBConfig=0x3E0001
set TCMAConfig=0x3F0001

@REM 可不修改
set JFlashLogPathName="%cd%\JFlash.log"
set JLinkARMLogPathName="%cd%\JLinkARM.log"
set JFlashProjPathName="%cd%\SemiDrive.jflash"
set JLinkScriptPathName="%cd%\SemiDrive.JLinkScript"



call :PREPROCESS_BEFORE_DOWNLOAD



@REM 
@REM norflash 下载
@REM 

set flash_folder=temp

@REM sfs和rfd在一个扇区，需merge再download
set DownloadFilePathName="%cd%\%flash_folder%\sfs_mt35-1-1-1.bin"
set DownloadFileAddr=0x10000000
call :JFlash_EXE %DownloadFilePathName% %DownloadFileAddr% 

set DownloadFilePathName="%cd%\%flash_folder%\xip.bin"
set DownloadFileAddr=0x10007000
call :JFlash_EXE %DownloadFilePathName% %DownloadFileAddr%

set DownloadFilePathName="%cd%\%flash_folder%\oneos_yuanyong.bin"
set DownloadFileAddr=0x10140000
call :JFlash_EXE %DownloadFilePathName% %DownloadFileAddr%

echo Download Done
pause



REM  REM ================================================================================================================================================
REM  REM hyperflash 下载
REM  REM ================================================================================================================================================
REM  
REM  set flash_folder=E3640_hyperflash
REM  
REM  @REM sfs和rfd在一个扇区，需merge再download
REM  set DownloadFilePathName="%cd%\%flash_folder%\sfs.bin"
REM  set DownloadFileAddr=0x10000000
REM  set MergeFilePathName="%cd%\%flash_folder%\rfd.bin"
REM  set MergeFileAddr=0x100000F0
REM  call :JFlash_EXE %DownloadFilePathName% %DownloadFileAddr% %MergeFilePathName% %MergeFileAddr%
REM  
REM  set DownloadFilePathName="%cd%\%flash_folder%\boot0.bin"
REM  set DownloadFileAddr=0x100C0000
REM  call :JFlash_EXE %DownloadFilePathName% %DownloadFileAddr%
REM  
REM  set DownloadFilePathName="%cd%\%flash_folder%\boot1.bin"
REM  set DownloadFileAddr=0x104C0000
REM  call :JFlash_EXE %DownloadFilePathName% %DownloadFileAddr%
REM  
REM  set DownloadFilePathName="%cd%\%flash_folder%\boot2.bin"
REM  set DownloadFileAddr=0x108C0000
REM  call :JFlash_EXE %DownloadFilePathName% %DownloadFileAddr%
REM  
REM  set DownloadFilePathName="%cd%\%flash_folder%\0x140000.bin"
REM  set DownloadFileAddr=0x10140000
REM  call :JFlash_EXE %DownloadFilePathName% %DownloadFileAddr%
REM  
REM  echo Download Done
REM  pause





































































































@REM ================================================================================================================================================
@REM 自定义函数
@REM ================================================================================================================================================

@REM JFlash_EXE <DownloadFilePathName> <Address> [<MergeFilePathName> <Address>]
@REM 执行jflash
:JFlash_EXE
    SETLOCAL
    call :DEL_FILE_IF_EXIST jflashlog_tmp.txt
    call :DEL_FILE_IF_EXIST jlinklog_tmp.txt
    if exist "%~1" (
        set DownloadFile=%~1,%~2
        if "%~3" equ "" (
            start /min /wait "J-Flash" %JFlashPathName% ^
            -openprj%JFlashProjPathName% ^
            -open"!DownloadFile!" ^
            -jflashlog"jflashlog_tmp.txt" ^
            -jlinklog"jlinklog_tmp.txt" ^
            -auto ^
            -exit
            if ERRORLEVEL 1 goto ERROR
        ) else (
            if exist "%~3" (
                set MergeFile=%~3,%~4
                start /min /wait "J-Flash" %JFlashPathName% ^
                -openprj%JFlashProjPathName% ^
                -open"!DownloadFile!" ^
                -jflashlog"jflashlog_tmp.txt" ^
                -jlinklog"jlinklog_tmp.txt" ^
                -merge"!MergeFile!" ^
                -auto ^
                -exit
                if ERRORLEVEL 1 goto ERROR
            ) else (
                echo %~3 Not Exist
                goto END
            )
        )

        echo !DownloadFile! Download Succeed
        if "!MergeFile!" neq "" echo !MergeFile! Download Succeed

        goto DONE

    ) else (
        echo %~1 Not Exist
        goto END
    )
    :ERROR
    echo !DownloadFile! Download Failed
    if "!MergeFile!" neq "" echo !MergeFile! Download Failed
    type jflashlog_tmp.txt >> %JFlashLogPathName%
    type jlinklog_tmp.txt >> %JLinkARMLogPathName%
    del jflashlog_tmp.txt
    del jlinklog_tmp.txt
    exit /b 1
    :DONE
    type jflashlog_tmp.txt >> %JFlashLogPathName%
    type jlinklog_tmp.txt >> %JLinkARMLogPathName%
    del jflashlog_tmp.txt
    del jlinklog_tmp.txt
    :END
    ENDLOCAL
goto:eof


:DEL_FILE_IF_EXIST
    if exist %1 (
        del %1
    )
goto:eof


:PREPROCESS_BEFORE_DOWNLOAD
    call :DEL_FILE_IF_EXIST %JFlashLogPathName%
    call :DEL_FILE_IF_EXIST %JLinkARMLogPathName%
    call :DEL_FILE_IF_EXIST cmd.txt

    echo r >> cmd.txt
    echo h >> cmd.txt
    echo WCP15Ex 0 1 0 0 0x08E7087A >> cmd.txt
    echo WCP15Ex 0 1 0 1 0x00000020 >> cmd.txt
    echo WCP15Ex 0 9 1 1 %TCMBConfig% >> cmd.txt
    echo WCP15Ex 0 9 1 0 %TCMAConfig% >> cmd.txt
    echo q >> cmd.txt
    %JLinkPathName% -device %device% -if SWD -speed 4000 -autoconnect 1 -CommandFile cmd.txt -JLinkScriptFile %JLinkScriptPathName% -log %JLinkARMLogPathName% > %JFlashLogPathName%
    if ERRORLEVEL 1 goto ERROR
    :ERROR
    del cmd.txt
goto:eof
