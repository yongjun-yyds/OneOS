define memory mem with size = 4G;

/*-Memory Regions-*/
define symbol __ICFEDIT_region_RAM_start__      = 0x00404000;
define symbol __ICFEDIT_region_RAM_end__        = 0x004FFFFF;
define symbol __ICFEDIT_region_TCM_start__      = 0x00000040;
define symbol __ICFEDIT_region_TCM_end__        = 0x0001FFFF;
define symbol __ICFEDIT_region_RAMECC_start__   = 0x00860000;
define symbol __ICFEDIT_region_RAMECC_end__     = 0x0087FFFF;
define symbol __ICFEDIT_region_ROM_start__      = 0x10140000;
define symbol __ICFEDIT_region_ROM_end__        = 0x1053FFFF;
define symbol __ICFEDIT_region_SFS_start__          = 0x10000000;
define symbol __ICFEDIT_region_BOOTLOADER_start__   = 0x100C1000;

/*-Specials-*/
define symbol __ICFEDIT_intvec_start__ = __ICFEDIT_region_ROM_start__;

/*-Sizes-*/
define symbol __ICFEDIT_size_cstack__   = 0x1000;
define symbol __ICFEDIT_size_svcstack__ = 0x1000;
define symbol __ICFEDIT_size_irqstack__ = 0x80;
define symbol __ICFEDIT_size_fiqstack__ = 0x1000;
define symbol __ICFEDIT_size_undstack__ = 0x80;
define symbol __ICFEDIT_size_abtstack__ = 0x80;

/*-blocks-*/
define block RAM_TEXT        with alignment = 4 { section .ram.text, section .textrw};
define block BSS             with alignment = 4 { section .bss, section .tbss };
define block EXCEPTION_STACK with alignment = 4 { section .except.stack };
define block RODATA          with alignment = 4 { section .rodata.fault_handler_table, section .rodata};
define block COMMANDS                           { section .commands };
define block TCMCODE         with alignment = 4 { section .tcmcode};
define block TCMDATA         with alignment = 4 { section .tcmdata};
define block NONEECCDATA     with alignment = 4 { section .noneeccdata};
define block CSTACK          with alignment = 8, size = __ICFEDIT_size_cstack__   { };
define block SVC_STACK       with alignment = 8, size = __ICFEDIT_size_svcstack__ { };
define block IRQ_STACK       with alignment = 8, size = __ICFEDIT_size_irqstack__ { };
define block FIQ_STACK       with alignment = 8, size = __ICFEDIT_size_fiqstack__ { };
define block UND_STACK       with alignment = 8, size = __ICFEDIT_size_undstack__ { };
define block ABT_STACK       with alignment = 8, size = __ICFEDIT_size_abtstack__ { };
define block HEAP            with alignment = 8, expanding size { section .heap };
define block RO              { readonly };
define block RW              { readwrite };
define block ZI              { zi };
define block Region_Table    with alignment = 32 { section .iar.init_table };

/*-regions-*/
define region RAM_region      = mem:[from __ICFEDIT_region_RAM_start__    to __ICFEDIT_region_RAM_end__];
define region TCM_region      = mem:[from __ICFEDIT_region_TCM_start__    to __ICFEDIT_region_TCM_end__];
define region RAMECC_region   = mem:[from __ICFEDIT_region_RAMECC_start__ to __ICFEDIT_region_RAMECC_end__];
define region ROM_region      = mem:[from __ICFEDIT_region_ROM_start__    to __ICFEDIT_region_ROM_end__];

/*-placement-*/
place at address mem:__ICFEDIT_intvec_start__ { readonly section .intvec };
place in ROM_region    { section .boot };
place in ROM_region    { section .text };
place in ROM_region    { block RODATA };
place in RAM_region    { block COMMANDS };
place in ROM_region    { block RO };
place in ROM_region    { block Region_Table };

place in RAM_region    { block RAM_TEXT };
place in RAM_region    { block BSS };
place in TCM_region    { block EXCEPTION_STACK };
place in RAM_region    { block RW };
place in RAM_region    { block ZI };
place in TCM_region    { block CSTACK };
place in TCM_region    { block SVC_STACK };
place in TCM_region    { block IRQ_STACK };
place in TCM_region    { block FIQ_STACK };
place in TCM_region    { block UND_STACK };
place in TCM_region    { block ABT_STACK };
place in RAM_region    { block HEAP };
place in TCM_region    { block TCMCODE, block TCMDATA};
place in RAMECC_region { block NONEECCDATA};
place at address mem:__ICFEDIT_region_SFS_start__           { section SFS_BIN };
place at address mem:__ICFEDIT_region_BOOTLOADER_start__    { section BOOTLOADER_BIN };

initialize by copy { section .tcmcode, section .tcmdata, section .ram.text, section .textrw, section .commands };
initialize by copy { readwrite };
do not initialize  { section .except.stack, section .noneeccdata, section .noinit, section .heap};

