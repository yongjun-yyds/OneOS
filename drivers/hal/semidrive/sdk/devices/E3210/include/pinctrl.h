/**
 * @file pinctrl.h
 * @brief Sdrv pinctrl device header.
 *        pinctrl for E3210
 *
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef PINCTRL_H_
#define PINCTRL_H_

/* safety domain pin */
#define JTAG_TDI        0
#define JTAG_TDO        1
#define JTAG_TRST_N     2
#define JTAG_TMS        3
#define JTAG_TCK        4
#define GPIO_X0         5
#define GPIO_X1         6
#define GPIO_X2         7
#define GPIO_X3         8
#define GPIO_X4         9
#define GPIO_X5         10
#define GPIO_X6         11
#define GPIO_X7         12
#define GPIO_X8         13
#define GPIO_X9         14
#define GPIO_X10        15
#define GPIO_X11        16
#define GPIO_Y0         17
#define GPIO_Y1         18
#define GPIO_Y2         19
#define GPIO_Y3         20
#define GPIO_Y4         21
#define GPIO_Y5         22
#define GPIO_Y6         23
#define GPIO_Y7         24
#define GPIO_Y8         25
#define GPIO_Y9         26
#define GPIO_Y10        27
#define GPIO_Y11        28
#define GPIO_A0         29
#define GPIO_A1         30
#define GPIO_A2         31
#define GPIO_A3         32
#define GPIO_A4         33
#define GPIO_A5         34
#define GPIO_A6         35
#define GPIO_A7         36
#define GPIO_A8         37
#define GPIO_A9         38
#define GPIO_A10        39
#define GPIO_A11        40
#define GPIO_A12        41
#define GPIO_A13        42
#define GPIO_A14        43
#define GPIO_A15        44
#define GPIO_B0         45
#define GPIO_B1         46
#define GPIO_B2         47
#define GPIO_B3         48
#define GPIO_B4         49
#define GPIO_B5         50
#define GPIO_B6         51
#define GPIO_B7         52
#define GPIO_B8         53
#define GPIO_B9         54
#define GPIO_B10        55
#define GPIO_B11        56
#define GPIO_B12        57
#define GPIO_B13        58
#define GPIO_B14        59
#define GPIO_B15        60
#define GPIO_C0         61
#define GPIO_C1         62
#define GPIO_C2         63
#define GPIO_C3         64
#define GPIO_C4         65
#define GPIO_C5         66
#define GPIO_C6         67
#define GPIO_C7         68
#define GPIO_G0         77
#define GPIO_G1         78
#define GPIO_G2         79
#define GPIO_G3         80
#define GPIO_G4         81
#define GPIO_G5         82
#define GPIO_G6         83
#define GPIO_G7         84
#define GPIO_G8         85
#define GPIO_G9         86
#define GPIO_G10        87
#define GPIO_G11        88
#define GPIO_S0         101
#define GPIO_S1         102
#define GPIO_S2         103
#define GPIO_S3         104
#define GPIO_S4         105
#define GPIO_S5         106
#define GPIO_S6         107
#define GPIO_S7         108
#define GPIO_H0         115
#define GPIO_H1         116
#define GPIO_H2         117
#define GPIO_H3         118
#define GPIO_H4         119
#define GPIO_H5         120
#define GPIO_H6         121
#define GPIO_H7         122
#define GPIO_H8         123
#define GPIO_H9         124
#define GPIO_H10        125
#define GPIO_H11        126
#define GPIO_H12        127
#define GPIO_H13        128

/* ap domain pin */
#define GPIO_L0         135
#define GPIO_L1         136
#define GPIO_L2         137
#define GPIO_L3         138
#define GPIO_L4         139
#define GPIO_L5         140
#define GPIO_L6         141
#define GPIO_L7         142
#define GPIO_L8         143
#define GPIO_L9         144
#define GPIO_E0         187
#define GPIO_E1         188
#define GPIO_E2         189
#define GPIO_E3         190
#define GPIO_E4         191
#define GPIO_E5         192

#endif /* PINCTRL_H_ */
