//*****************************************************************************
//
// WARNING: Automatically generated file, don't modify anymore!!!
//
// Copyright (c) 2021-2029 Semidrive Incorporated.  All rights reserved.
// Software License Agreement
//
//*****************************************************************************
#include <types.h>
#include <regs_base.h>
#include <reset_ip.h>
#include <compiler.h>

/**
 * @brief SAF reset signal id.
 */
typedef enum reset_signal_safety {
    RSTSIG_SAF_RSTGEN_AP        = SDRV_RSTGEN_SIG_ID(SDRV_RSTGEN_CORE, 0),
    RSTSIG_SAF_CR5_SAF,
    RSTSIG_SAF_CR5_SP0,
    RSTSIG_SAF_CR5_SP1,
    RSTSIG_SAF_CR5_SX0,
    RSTSIG_SAF_CR5_SX1,
    RSTSIG_SAF_LATENT           = SDRV_RSTGEN_SIG_ID(SDRV_RSTGEN_LATENT, 0),
    RSTSIG_SAF_MISSION0         = SDRV_RSTGEN_SIG_ID(SDRV_RSTGEN_MISSION, 0),
    RSTSIG_SAF_MISSION1,
    RSTSIG_SAF_MISSION2,
    RSTSIG_SAF_MISSION3,
    RSTSIG_SAF_MISSION4,
    RSTSIG_SAF_MISSION5,
    RSTSIG_SAF_MISSION6         = SDRV_RSTGEN_SIG_ID(SDRV_RSTGEN_MISSION, 6),
    RSTSIG_SAF_MISSION8         = SDRV_RSTGEN_SIG_ID(SDRV_RSTGEN_MISSION, 8),
    RSTSIG_SAF_MISSION9         = SDRV_RSTGEN_SIG_ID(SDRV_RSTGEN_MISSION, 9),
    RSTSIG_SAF_CANFD1           = SDRV_RSTGEN_SIG_ID(SDRV_RSTGEN_MODULE, 0),
    RSTSIG_SAF_CANFD2,
    RSTSIG_SAF_CANFD3_4,
    RSTSIG_SAF_CANFD5_8,
    RSTSIG_SAF_CANFD9_16,
    RSTSIG_SAF_CANFD17_24,
    RSTSIG_SAF_XSPI1A           = SDRV_RSTGEN_SIG_ID(SDRV_RSTGEN_MODULE, 10),
    RSTSIG_SAF_XSPI1B,
    RSTSIG_SAF_XSPI2A,
    RSTSIG_SAF_XSPI2B,
    RSTSIG_SAF_DMA_RST0         = SDRV_RSTGEN_SIG_ID(SDRV_RSTGEN_MODULE, 15),
    RSTSIG_SAF_DMA_RST1,
    RSTSIG_SAF_ENET1,
    RSTSIG_SAF_ENET2,
    RSTSIG_SAF_GAMA1            = SDRV_RSTGEN_SIG_ID(SDRV_RSTGEN_MODULE, 21),
    RSTSIG_SAF_AHB_SYNCUP_GAMA1 = SDRV_RSTGEN_SIG_ID(SDRV_RSTGEN_MODULE, 21),
    RSTSIG_SAF_GAMA2,
    RSTSIG_SAF_AHB_SYNCUP_GAMA2 = SDRV_RSTGEN_SIG_ID(SDRV_RSTGEN_MODULE, 22),
    RSTSIG_SAF_VIC1,
    RSTSIG_SAF_VIC2_PORTA,
    RSTSIG_SAF_VIC2_PORTB,
    RSTSIG_SAF_VIC3_PORTA       = SDRV_RSTGEN_SIG_ID(SDRV_RSTGEN_MODULE, 27),
    RSTSIG_SAF_VIC3_PORTB,
    RSTSIG_SAF_XSPI_SLV,
    RSTSIG_SAF_MB,
    RSTSIG_SAF_XTRG,
    RSTSIG_SAF_DEBUG            = SDRV_RSTGEN_SIG_ID(SDRV_RSTGEN_DEBUG, 0),
} reset_signal_safety_e;

/**
 * @brief AP reset signal id.
 */
typedef enum reset_signal_ap {
    RSTSIG_AP_MISSION0     = SDRV_RSTGEN_SIG_ID(SDRV_RSTGEN_MISSION, 0),
    RSTSIG_AP_MISSION1     = SDRV_RSTGEN_SIG_ID(SDRV_RSTGEN_MISSION, 1),
    RSTSIG_AP_CSI          = SDRV_RSTGEN_SIG_ID(SDRV_RSTGEN_MODULE, 0),
    RSTSIG_AP_DC,
    RSTSIG_AP_G2D,
    RSTSIG_AP_SDRAMC,
    RSTSIG_AP_SACI1,
    RSTSIG_AP_SACI2,//5
    RSTSIG_AP_DMA_AP,
    RSTSIG_AP_SEHC1,
    RSTSIG_AP_SEHC2,
    RSTSIG_AP_USB,
    RSTSIG_AP_SEIP,//10
    RSTSIG_AP_LVDS_SS     = SDRV_RSTGEN_SIG_ID(SDRV_RSTGEN_MODULE, 12),
    RSTSIG_AP_DEBUG       = SDRV_RSTGEN_SIG_ID(SDRV_RSTGEN_DEBUG, 0),
} reset_signal_ap_e;

/* SAF rstgen controller */
sdrv_rstgen_t g_rstgen_saf = {
    .base = APB_RSTGEN_SF_BASE,
};

/* AP rstgen controller */
sdrv_rstgen_t g_rstgen_ap = {
    .base = APB_RSTGEN_AP_BASE,
};

/* global reset */
sdrv_rstgen_glb_t rstctl_glb = {
    .rst_sf_ctl = &g_rstgen_saf,
    .rst_ap_ctl = &g_rstgen_ap,
};

/* SAF core */
sdrv_rstgen_sig_t rstsig_rstgen_ap = {
    .rst_ctl = &g_rstgen_saf,
    .id      = RSTSIG_SAF_RSTGEN_AP,
};

sdrv_rstgen_sig_t rstsig_cr5_saf = {
    .rst_ctl = &g_rstgen_saf,
    .id      = RSTSIG_SAF_CR5_SAF,
};

/**
 * @brief SAF latent
 *
 * latent signals will reset automatically after power on.
 *
 * signals in SAF latent:
 * MAC
 * SCR_BOOT
 * EIC_BOOT
 * FUSE_LSP_CMP
 * AHB2APB1
 * AHB2APB2
 * AHB2APB3
 * AHB2APB4
 * APBMUX2
 * APBMUX3
 * APBMUX4
 * IROMC
 * GPIO_SF
 * IOMUXC_SF
 * WDT1
 * WDT2
 * SEM1
 * SEM2
 * VD_SF_DIG
 * IOC
 * ETMR1
 * ETMR2
 * EPWM1
 * EPWM2
 * MPC_XSPI1A
 * MPC_XSPI1B
 * PPC_APBMUX2
 * PPC_APBMUX3
 * PPC_APBMUX4
 * PPC_APBMUX1
 * MPC_ROMC
 * XB_SF
 * POR_SF_DIG
 * PT_SNS_SF_DIG
 * APB_MAC_SP_SLV
 * APB_MAC_AP_SLV
 * APB_APBMUX1_SLV
 * MPC_VIC1
 * BTM1
 * BTM2
 * BTM3
 * BTM4
 * BTM5
 * BTM6
 * AAPB_XSPI1A
 * AAPB_XSPI1B
*/
sdrv_rstgen_sig_t rstsig_saf_latent = {
    .rst_ctl = &g_rstgen_saf,
    .id      = RSTSIG_SAF_LATENT,
};

/*
 * @brief SAF mission
 *
 * SAF mission signals will reset automatically after power on.
 *
 */

/**
 * @brief SAF mission 0
 *
 * signals in SAF mission 0:
 * FAB_SF
 * IOMUXC_SF
 * SCR_SF
 * SMC
 * MPC_CR5_SF
 * PMU_CORE
 * APB_APBMUX1_MST
 * APB_PMUX2_DEC_SLV
 * APB_PMUX2_DEC_MST
*/
sdrv_rstgen_sig_t rstsig_saf_mission0 = {
    .rst_ctl = &g_rstgen_saf,
    .id      = RSTSIG_SAF_MISSION0,
};

/**
 * @brief SAF mission 1
 *
 * signals in mission 1:
 * PLL1
 * PLL2
 * PLL3
 */
sdrv_rstgen_sig_t rstsig_saf_mission1 = {
    .rst_ctl = &g_rstgen_saf,
    .id      = RSTSIG_SAF_MISSION1,
};

/**
 * @brief SAF mission 2
 *
 * signals in SAF mission 2:
 * ANA_SF_SADC1
 * ANA_SF_SADC2
 * ANA_SF_SADC3
 * ANA_SF_ACMP1
 * ANA_SF_ACMP2
 * ANA_SF_ACMP3
 * ANA_SF_ACMP4
 */
sdrv_rstgen_sig_t rstsig_saf_mission2 = {
    .rst_ctl = &g_rstgen_saf,
    .id      = RSTSIG_SAF_MISSION2,
};

/**
 * @brief SAF mission 3
 *
 * signals in SAF mission 3:
 * APBMUX3
 * APBMUX4
 * IOMUXC_SF_COMP
 * EIC_SF
 * UART1
 * UART2
 * UART3
 * UART4
 * UART5
 * UART6
 * UART7
 * UART8
 * UART9
 * UART10
 * UART11
 * UART12
 * SPI1
 * SPI2
 * SPI3
 * SPI4
 * SPI5
 * SPI6
 * I2C3
 * I2C4
 * I2C5
 * I2C6
 * I2C7
 * I2C8
 * FLEXRAY1
 * FLEXRAY2
 * WDT5
 * WDT6
 * MPC_MB
 * APB_APBMUX3_SLV
 * APB_APBMUX3_MST
 * APB_APBMUX4_SLV
 * APB_APBMUX4_MST
 * APB_SEIP_NVM_MST
 * APB_SEIP_NVM_SLV
 * ADB_SFAP_SLV
 * ADB_SFAP_MST
 * ADB_APSF_SLV
 * ADB_APSF_MST
 * ADB_DISPSF_SLV
 * ADB_DISPSF_MST
 * BTI_DISPSF
 * BTI_APSF
 */
sdrv_rstgen_sig_t rstsig_saf_mission3 = {
    .rst_ctl = &g_rstgen_saf,
    .id      = RSTSIG_SAF_MISSION3,
};

/**
 * @brief SAF mission 4
 *
 * signals in SAF mission 4:
 * IRAMC1
 * IRAMC2
 * IRAMC3
 * IRAM_MUX
 * MPC_IRAMC1
 * MPC_IRAMC2
 * MPC_IRAMC3
 */
sdrv_rstgen_sig_t rstsig_saf_mission4 = {
    .rst_ctl = &g_rstgen_saf,
    .id      = RSTSIG_SAF_MISSION4,
};

/**
 * @brief SAF mission 5
 *
 * signals in SAF mission 5:
 * IRAMC4
 * APBMUX5
 * WDT3
 * WDT4
 * PTB
 * FAB_SP
 * MPC_IRAMC4
 * PPC_APBMUX5
 * EIC_SP
 * CSLITE
 * AAHB_SPSF_SLV
 * AAHB_SPSF_MST
 * APB_MAC_SP_SLV
 * APB_MAC_SP_MST
 * ADB_SPAP_SLV
 * ADB_SPAP_MST
 */
sdrv_rstgen_sig_t rstsig_saf_mission5 = {
    .rst_ctl = &g_rstgen_saf,
    .id      = RSTSIG_SAF_MISSION5,
};

/**
 * @brief SAF mission 6
 *
 * signals in SAF mission 6:
 * reserved
 */
sdrv_rstgen_sig_t rstsig_saf_mission6 = {
    .rst_ctl = &g_rstgen_saf,
    .id      = RSTSIG_SAF_MISSION6,
};

/**
 * @brief SAF mission 8
 *
 * signals in SAF mission 8:
 * LDO_DIG
 * APB_LDO_DIG_MST
 * APB_LDO_DIG_SLV
 */
sdrv_rstgen_sig_t rstsig_saf_mission8 = {
    .rst_ctl = &g_rstgen_saf,
    .id      = RSTSIG_SAF_MISSION8,
};

/**
 * @brief SAF mission 9
 *
 * signals in SAF mission 9:
 * DCDC1
 * APB_DCDC1_MST
 * APB_DCDC1_SLV
 */
sdrv_rstgen_sig_t rstsig_saf_mission9 = {
    .rst_ctl = &g_rstgen_saf,
    .id      = RSTSIG_SAF_MISSION9,
};

/* SAF module */
sdrv_rstgen_sig_t rstsig_canfd3_4 = {
    .rst_ctl = &g_rstgen_saf,
    .id      = RSTSIG_SAF_CANFD3_4,
};

sdrv_rstgen_sig_t rstsig_canfd5_8 = {
    .rst_ctl = &g_rstgen_saf,
    .id      = RSTSIG_SAF_CANFD5_8,
};

sdrv_rstgen_sig_t rstsig_canfd9_16 = {
    .rst_ctl = &g_rstgen_saf,
    .id      = RSTSIG_SAF_CANFD9_16,
};

sdrv_rstgen_sig_t rstsig_canfd17_24 = {
    .rst_ctl = &g_rstgen_saf,
    .id      = RSTSIG_SAF_CANFD17_24,
};

sdrv_rstgen_sig_t rstsig_xspi1a = {
    .rst_ctl = &g_rstgen_saf,
    .id      = RSTSIG_SAF_XSPI1A,
};

sdrv_rstgen_sig_t rstsig_xspi1b = {
    .rst_ctl = &g_rstgen_saf,
    .id      = RSTSIG_SAF_XSPI1B,
};

sdrv_rstgen_sig_t rstsig_dma_rst0 = {
    .rst_ctl = &g_rstgen_saf,
    .id      = RSTSIG_SAF_DMA_RST0,
};

sdrv_rstgen_sig_t rstsig_dma_rst1 = {
    .rst_ctl = &g_rstgen_saf,
    .id      = RSTSIG_SAF_DMA_RST1,
};

sdrv_rstgen_sig_t rstsig_enet1 = {
    .rst_ctl = &g_rstgen_saf,
    .id      = RSTSIG_SAF_ENET1,
};

sdrv_rstgen_sig_t rstsig_vic1 = {
    .rst_ctl = &g_rstgen_saf,
    .id      = RSTSIG_SAF_VIC1,
};

sdrv_rstgen_sig_t rstsig_vic2_porta = {
    .rst_ctl = &g_rstgen_saf,
    .id      = RSTSIG_SAF_VIC2_PORTA,
};

sdrv_rstgen_sig_t rstsig_vic2_portb = {
    .rst_ctl = &g_rstgen_saf,
    .id      = RSTSIG_SAF_VIC2_PORTB,
};

sdrv_rstgen_sig_t rstsig_vic3_porta = {
    .rst_ctl = &g_rstgen_saf,
    .id      = RSTSIG_SAF_VIC3_PORTA,
};

sdrv_rstgen_sig_t rstsig_vic3_portb = {
    .rst_ctl = &g_rstgen_saf,
    .id      = RSTSIG_SAF_VIC3_PORTB,
};

sdrv_rstgen_sig_t rstsig_xspi_slv = {
    .rst_ctl = &g_rstgen_saf,
    .id      = RSTSIG_SAF_XSPI_SLV,
};

sdrv_rstgen_sig_t rstsig_mb = {
    .rst_ctl = &g_rstgen_saf,
    .id      = RSTSIG_SAF_MB,
};

sdrv_rstgen_sig_t rstsig_xtrg = {
    .rst_ctl = &g_rstgen_saf,
    .id      = RSTSIG_SAF_XTRG,
};

/*
 * @brief AP mission
 *
 * AP mission signals will reset automatically after power on.
 *
 */

/**
 * @brief AP mision 0
 *
 * signals in AP mission 0:
 * PLL4
 * PLL5
 * PLL_LVDS
 */
sdrv_rstgen_sig_t rstsig_ap_mission0 = {
    .rst_ctl = &g_rstgen_ap,
    .id      = RSTSIG_AP_MISSION0,
};

/**
 * @brief AP mision 1
 *
 * signals in AP mission 1:
 * APBMUX7
 * DISP_MUX
 * FAB_AP
 * VD_AP
 * APBMUX6
 * GPIO_AP
 * IOMUXC_AP
 * WDT8
 * MPC_SEIP
 * MPC_SDRAMC
 * PPC_APBMUX6
 * PPC_APBMUX7
 * FAB_DISP
 * AHBDEC_SEIP
 * PT_SNS_AP
 * POR_AP
 * SCR_AP
 * RSTGEN_AP
 * APB_MAC_AP_SLV
 * APB_MAC_AP_MST
 * APB_APBMUX7_SLV
 * APB_APBMUX7_MST
 * APB_SEC_STORAGE1_SLV
 * APB_SEC_STORAGE1_MST
 * APB_SEIP_NVM_MST
 * APB_SEIP_NVM_SLV
 * ADB_SPAP_SLV
 * ADB_SPAP_MST
 * ADB_SFAP_SLV
 * ADB_SFAP_MST
 * ADB_APSF_SLV
 * ADB_APSF_MST
 * ADB_DISPSF_SLV
 * ADB_DISPSF_MST
 * BTI_DISPSF
 * BTI_APSF
 */
sdrv_rstgen_sig_t rstsig_ap_mission1 = {
    .rst_ctl = &g_rstgen_ap,
    .id      = RSTSIG_AP_MISSION1,
};

/* AP module */
sdrv_rstgen_sig_t rstsig_saci2 = {
    .rst_ctl = &g_rstgen_ap,
    .id      = RSTSIG_AP_SACI2,
};

sdrv_rstgen_sig_t rstsig_dma_ap = {
    .rst_ctl = &g_rstgen_ap,
    .id      = RSTSIG_AP_DMA_AP,
};

sdrv_rstgen_sig_t rstsig_sehc1 = {
    .rst_ctl = &g_rstgen_ap,
    .id      = RSTSIG_AP_SEHC1,
};

sdrv_rstgen_sig_t rstsig_sehc2 = {
    .rst_ctl = &g_rstgen_ap,
    .id      = RSTSIG_AP_SEHC2,
};

sdrv_rstgen_sig_t rstsig_usb = {
    .rst_ctl = &g_rstgen_ap,
    .id      = RSTSIG_AP_USB,
};

sdrv_rstgen_sig_t rstsig_seip = {
    .rst_ctl = &g_rstgen_ap,
    .id      = RSTSIG_AP_SEIP,
    .need_clr_rst = true,
};

sdrv_rstgen_sig_t rstsig_lvds_ss = {
    .rst_ctl = &g_rstgen_ap,
    .id      = RSTSIG_AP_LVDS_SS,
};

/* SAF debug */
sdrv_rstgen_sig_t rstsig_saf_debug = {
    .rst_ctl = &g_rstgen_saf,
    .id      = RSTSIG_SAF_DEBUG,
};

/* AP debug */
sdrv_rstgen_sig_t rstsig_ap_debug = {
    .rst_ctl = &g_rstgen_ap,
    .id      = RSTSIG_AP_DEBUG,
};

/* general register */
sdrv_rstgen_general_reg_t reset_general_reg_sf_remap = {
    .rst_ctl = &g_rstgen_saf,
    .id      = 1,
};

sdrv_rstgen_general_reg_t reset_general_reg_sf_boot = {
    .rst_ctl = &g_rstgen_saf,
    .id      = 7,
};

sdrv_rstgen_general_reg_t reset_general_reg_rom_ctrl = {
    .rst_ctl = &g_rstgen_saf,
    .id      = 0,
};

/* recovery module */
__WEAK sdrv_recovery_btm_t recovery_btm_list = {
    .btm_num = 6,
    .btm_base = {
        APB_BTM1_BASE,
        APB_BTM2_BASE,
        APB_BTM3_BASE,
        APB_BTM4_BASE,
        APB_BTM5_BASE,
        APB_BTM6_BASE,
    },
};

__WEAK sdrv_recovery_etimer_t recovery_etimer_list = {
    .etimer_num = 2,
    .etimer_base = {
        APB_ETMR1_BASE,
        APB_ETMR2_BASE,
    },
};

__WEAK sdrv_recovery_epwm_t recovery_epwm_list = {
    .epwm_num = 2,
    .epwm_base = {
        APB_EPWM1_BASE,
        APB_EPWM2_BASE,
    },
};

__WEAK sdrv_recovery_module_t recovery_module_array = {
    .btm_list = &recovery_btm_list,
    .etimer_list = &recovery_etimer_list,
    .epwm_list = &recovery_epwm_list,
};
