/*
 * part.h
 *
 * Copyright (c) 2020 Semidrive Semiconductor.
 * All rights reserved.
 *
 * Description: E3 part device IP macro.
 *
 * Revision History:
 * -----------------
 */

#ifndef SDRV_PART_H_
#define SDRV_PART_H_

#define CONFIG_E3340 1

#define CONFIG_E3 1

#endif /* SDRV_PART_H_ */
