//*****************************************************************************
//
// WARNING: Automatically generated file, don't modify anymore!!!
//
// Copyright (c) 2021-2029 Semidrive Incorporated.  All rights reserved.
// Software License Agreement
//
//*****************************************************************************
#include <types.h>
#include <regs_base.h>
#include <reset_ip.h>
#include <compiler.h>

/**
 * @brief E3L SAF reset signal id.
 */
typedef enum e3l_reset_signal_safety {
    E3L_RSTSIG_SAF_CR5_SAF          = SDRV_RSTGEN_SIG_ID(SDRV_RSTGEN_CORE, 0),
    E3L_RSTSIG_SAF_LATENT           = SDRV_RSTGEN_SIG_ID(SDRV_RSTGEN_LATENT, 0),
    E3L_RSTSIG_SAF_MISSION0         = SDRV_RSTGEN_SIG_ID(SDRV_RSTGEN_MISSION, 0),
    E3L_RSTSIG_SAF_MISSION1         = SDRV_RSTGEN_SIG_ID(SDRV_RSTGEN_MISSION, 1),
    E3L_RSTSIG_SAF_MISSION2         = SDRV_RSTGEN_SIG_ID(SDRV_RSTGEN_MISSION, 2),
    E3L_RSTSIG_SAF_MISSION3         = SDRV_RSTGEN_SIG_ID(SDRV_RSTGEN_MISSION, 3),
    E3L_RSTSIG_SAF_MISSION4         = SDRV_RSTGEN_SIG_ID(SDRV_RSTGEN_MISSION, 4),
    E3L_RSTSIG_SAF_MISSION5         = SDRV_RSTGEN_SIG_ID(SDRV_RSTGEN_MISSION, 5),
    E3L_RSTSIG_SAF_CANFD16          = SDRV_RSTGEN_SIG_ID(SDRV_RSTGEN_MODULE, 0),
    E3L_RSTSIG_SAF_CANFD21,
    E3L_RSTSIG_SAF_CANFD3,
    E3L_RSTSIG_SAF_CANFD4,
    E3L_RSTSIG_SAF_CANFD5,
    E3L_RSTSIG_SAF_CANFD6           = SDRV_RSTGEN_SIG_ID(SDRV_RSTGEN_MODULE, 5),
    E3L_RSTSIG_SAF_CANFD7,
    E3L_RSTSIG_SAF_CANFD23,
    E3L_RSTSIG_SAF_XSPI1A,
    E3L_RSTSIG_SAF_XSPI1B,
    E3L_RSTSIG_SAF_DMA_RST0         = SDRV_RSTGEN_SIG_ID(SDRV_RSTGEN_MODULE, 10),
    E3L_RSTSIG_SAF_DMA_RST1,
    E3L_RSTSIG_SAF_ENET1,
    E3L_RSTSIG_SAF_VIC1,
    E3L_RSTSIG_SAF_XSPI_SLV,
    E3L_RSTSIG_SAF_XTRG             = SDRV_RSTGEN_SIG_ID(SDRV_RSTGEN_MODULE, 15),
    E3L_RSTSIG_SAF_SACI2,
    E3L_RSTSIG_SAF_SEHC1,
    E3L_RSTSIG_SAF_USB,
    E3L_RSTSIG_SAF_SEIP,
    E3L_RSTSIG_SAF_CSLITE           = SDRV_RSTGEN_SIG_ID(SDRV_RSTGEN_MODULE, 20),
} e3l_reset_signal_safety_e;

/* SAF rstgen controller */
sdrv_rstgen_t g_rstgen_saf = {
    .base = APB_RSTGEN_SF_BASE,
};

/* global reset */
sdrv_rstgen_glb_t rstctl_glb = {
    .rst_sf_ctl = &g_rstgen_saf,
};

/* SAF core */
sdrv_rstgen_sig_t rstsig_cr5_saf = {
    .rst_ctl = &g_rstgen_saf,
    .id      = E3L_RSTSIG_SAF_CR5_SAF,
};

/**
 * @brief reset latent
 *
 * latent signals will reset automatically after power on.
 *
 * signals in latent:
 * AHB2APB1
 * AHB2APB2
 * AHB2APB3
 * AHB2APB4
 * APBMUX2
 * APBMUX3
 * IROMC
 * IOMUXC_SF_COMP
 * MAC
 * SCR_SF
 * WDT1
 * SEM1
 * SEM2
 * VD_SF_DIG
 * IOC
 * EIC_SF
 * FUSE_LSP_CMP
 * FAB_SF
 * UART1
 * UART2
 * UART3
 * UART4
 * UART5
 * UART6
 * UART7
 * UART8
 * UART9
 * UART10
 * UART11
 * UART12
 * SPI1
 * SPI2
 * SPI3
 * SPI4
 * SPI5
 * SPI6
 * I2C1
 * I2C2
 * I2C3
 * I2C4
 * I2C5
 * I2C6
 * ETMR1
 * ETMR2
 * EPWM1
 * EPWM2
 * MPC_XSPI1A
 * MPC_XSPI1B
 * AAPB_MPC_XSPI1A
 * AAPB_MPC_XSPI1B
 * AAPB_MPC_SEIP
 * WDT8
 * PPC_APBMUX2
 * PPC_APBMUX3
 * PPC_APBMUX4
 * PPC_APBMUX1
 * MPC_ROMC
 * XB_SF
 * MPC_IRAMC1
 * MPC_CR5_SF
 * POR_SF_DIG
 * PT_SNS_SF_DIG
 * MPC_VIC1
 * BTM1
 * BTM2
 * BTM3
 * BTM4
 * AAPB_XSPI1A
 * AAPB_XSPI1B
 * MPC_SEIP
 * APB_APBMUX1_SLV
 * APB_PMUX2_DEC_SLV
 * AAPB_APBMUX3
 * APB_DCDC1_SLV
 * APB_APBMUX4_SLV
 * AAXI_APSF_MST
 */
sdrv_rstgen_sig_t rstsig_latent = {
    .rst_ctl = &g_rstgen_saf,
    .id      = E3L_RSTSIG_SAF_LATENT,
};

/*
 * @brief reset mission
 *
 * reset mission signals will reset automatically after power on.
 *
 */

/**
 * @brief reset mission 0
 *
 * signals in mission 0:
 * GPIO_SF
 * IOMUXC_SF
 * SMC
 * PMU_CORE
 * APB_APBMUX1_MST
 * APB_PMUX2_DEC_MST
 * APB_SEIP_NVM_MST
 */
sdrv_rstgen_sig_t rstsig_mission0 = {
    .rst_ctl = &g_rstgen_saf,
    .id      = E3L_RSTSIG_SAF_MISSION0,
};

/**
 * @brief reset mission 1
 *
 * signals in mission 1:
 * PLL1
 * PLL2
 * Pll3
 */
sdrv_rstgen_sig_t rstsig_mission1 = {
    .rst_ctl = &g_rstgen_saf,
    .id      = E3L_RSTSIG_SAF_MISSION1,
};

/**
 * @brief reset mission 2
 *
 * signals in mission 2:
 * ANA_SF_SADC1
 * ANA_SF_SADC2
 * ANA_SF_SADC3
 * ANA_SF_SACMP1
 * ANA_SF_SACMP2
 */
sdrv_rstgen_sig_t rstsig_mission2 = {
    .rst_ctl = &g_rstgen_saf,
    .id      = E3L_RSTSIG_SAF_MISSION2,
};

/**
 * @brief reset mission 3
 *
 * signals in mission 3:
 * IRAM1
 */
sdrv_rstgen_sig_t rstsig_mission3 = {
    .rst_ctl = &g_rstgen_saf,
    .id      = E3L_RSTSIG_SAF_MISSION3,
};

/**
 * @brief reset mission 4
 *
 * signals in mission 4:
 * DCDC1
 * APB_DCDC1_MST
 */
sdrv_rstgen_sig_t rstsig_mission4 = {
    .rst_ctl = &g_rstgen_saf,
    .id      = E3L_RSTSIG_SAF_MISSION4,
};

/**
 * @brief reset mission 5
 *
 * signals in mission 5:
 * APBMUX4
 * PTB
 * AHBDEC_SEIP
 * APB_SEC_STORAGE1_SLV
 * APB_SEIP_NVM_SLV
 * APB_APBMUX4_MST
 * AAXI_APSF_SLV
 */
sdrv_rstgen_sig_t rstsig_mission5 = {
    .rst_ctl = &g_rstgen_saf,
    .id      = E3L_RSTSIG_SAF_MISSION5,
};

/* reset module */
sdrv_rstgen_sig_t rstsig_canfd16 = {
    .rst_ctl = &g_rstgen_saf,
    .id      = E3L_RSTSIG_SAF_CANFD16,
};

sdrv_rstgen_sig_t rstsig_canfd21 = {
    .rst_ctl = &g_rstgen_saf,
    .id      = E3L_RSTSIG_SAF_CANFD21,
};

sdrv_rstgen_sig_t rstsig_canfd3 = {
    .rst_ctl = &g_rstgen_saf,
    .id      = E3L_RSTSIG_SAF_CANFD3,
};

sdrv_rstgen_sig_t rstsig_canfd4 = {
    .rst_ctl = &g_rstgen_saf,
    .id      = E3L_RSTSIG_SAF_CANFD4,
};

sdrv_rstgen_sig_t rstsig_canfd5 = {
    .rst_ctl = &g_rstgen_saf,
    .id      = E3L_RSTSIG_SAF_CANFD5,
};

sdrv_rstgen_sig_t rstsig_canfd6 = {
    .rst_ctl = &g_rstgen_saf,
    .id      = E3L_RSTSIG_SAF_CANFD6,
};

sdrv_rstgen_sig_t rstsig_canfd7 = {
    .rst_ctl = &g_rstgen_saf,
    .id      = E3L_RSTSIG_SAF_CANFD7,
};

sdrv_rstgen_sig_t rstsig_canfd23 = {
    .rst_ctl = &g_rstgen_saf,
    .id      = E3L_RSTSIG_SAF_CANFD23,
};

sdrv_rstgen_sig_t rstsig_xspi1a = {
    .rst_ctl = &g_rstgen_saf,
    .id      = E3L_RSTSIG_SAF_XSPI1A,
};

sdrv_rstgen_sig_t rstsig_xspi1b = {
    .rst_ctl = &g_rstgen_saf,
    .id      = E3L_RSTSIG_SAF_XSPI1B,
};

sdrv_rstgen_sig_t rstsig_dma_rst0 = {
    .rst_ctl = &g_rstgen_saf,
    .id      = E3L_RSTSIG_SAF_DMA_RST0,
};

sdrv_rstgen_sig_t rstsig_dma_rst1 = {
    .rst_ctl = &g_rstgen_saf,
    .id      = E3L_RSTSIG_SAF_DMA_RST1,
};

sdrv_rstgen_sig_t rstsig_enet1 = {
    .rst_ctl = &g_rstgen_saf,
    .id      = E3L_RSTSIG_SAF_ENET1,
};

sdrv_rstgen_sig_t rstsig_vic1 = {
    .rst_ctl = &g_rstgen_saf,
    .id      = E3L_RSTSIG_SAF_VIC1,
};

sdrv_rstgen_sig_t rstsig_xspi_slv = {
    .rst_ctl = &g_rstgen_saf,
    .id      = E3L_RSTSIG_SAF_XSPI_SLV,
};

sdrv_rstgen_sig_t rstsig_xtrg = {
    .rst_ctl = &g_rstgen_saf,
    .id      = E3L_RSTSIG_SAF_XTRG,
};

sdrv_rstgen_sig_t rstsig_saci2 = {
    .rst_ctl = &g_rstgen_saf,
    .id      = E3L_RSTSIG_SAF_SACI2,
};

sdrv_rstgen_sig_t rstsig_sehc1 = {
    .rst_ctl = &g_rstgen_saf,
    .id      = E3L_RSTSIG_SAF_SEHC1,
};

sdrv_rstgen_sig_t rstsig_usb = {
    .rst_ctl = &g_rstgen_saf,
    .id      = E3L_RSTSIG_SAF_USB,
};

sdrv_rstgen_sig_t rstsig_seip = {
    .rst_ctl = &g_rstgen_saf,
    .id      = E3L_RSTSIG_SAF_SEIP,
    .need_clr_rst = true,
};

sdrv_rstgen_sig_t rstsig_cslite = {
    .rst_ctl = &g_rstgen_saf,
    .id      = E3L_RSTSIG_SAF_CSLITE,
};

/* general register */
sdrv_rstgen_general_reg_t reset_general_reg_sf_remap = {
    .rst_ctl = &g_rstgen_saf,
    .id      = 1,
};

sdrv_rstgen_general_reg_t reset_general_reg_sf_boot = {
    .rst_ctl = &g_rstgen_saf,
    .id      = 7,
};

sdrv_rstgen_general_reg_t reset_general_reg_rom_ctrl = {
    .rst_ctl = &g_rstgen_saf,
    .id      = 0,
};

/* recovery module */
__WEAK sdrv_recovery_btm_t recovery_btm_list = {
    .btm_num = 2,
    .btm_base = {
        APB_BTM1_BASE,
        APB_BTM2_BASE,
        APB_BTM3_BASE,
        APB_BTM4_BASE,
    },
};

__WEAK sdrv_recovery_etimer_t recovery_etimer_list = {
    .etimer_num = 2,
    .etimer_base = {
        APB_ETMR1_BASE,
        APB_ETMR2_BASE,
    },
};

__WEAK sdrv_recovery_epwm_t recovery_epwm_list = {
    .epwm_num = 2,
    .epwm_base = {
        APB_EPWM1_BASE,
        APB_EPWM2_BASE,
    },
};

__WEAK sdrv_recovery_module_t recovery_module_array = {
    .btm_list = &recovery_btm_list,
    .etimer_list = &recovery_etimer_list,
    .epwm_list = &recovery_epwm_list,
};
