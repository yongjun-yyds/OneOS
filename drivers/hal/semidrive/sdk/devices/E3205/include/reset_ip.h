
//*****************************************************************************
//
// WARNING: Automatically generated file, don't modify anymore!!!
//
// Copyright (c) 2021-2029 Semidrive Incorporated.  All rights reserved.
// Software License Agreement
//
//*****************************************************************************

#ifndef RESET_IP_H
#define RESET_IP_H

#include <sdrv_rstgen.h>

/* SAF rstgen controller */
extern sdrv_rstgen_t g_rstgen_saf;

/* global reset */
extern sdrv_rstgen_glb_t rstctl_glb;

/* SAF core */
extern sdrv_rstgen_sig_t rstsig_cr5_saf;

/**
 * @brief reset latent
 *
 * latent signals will reset automatically after power on.
 *
 * signals in latent:
 * AHB2APB1
 * AHB2APB2
 * AHB2APB3
 * AHB2APB4
 * APBMUX2
 * APBMUX3
 * IROMC
 * IOMUXC_SF_COMP
 * MAC
 * SCR_SF
 * WDT1
 * SEM1
 * SEM2
 * VD_SF_DIG
 * IOC
 * EIC_SF
 * FUSE_LSP_CMP
 * FAB_SF
 * UART1
 * UART2
 * UART3
 * UART4
 * UART5
 * UART6
 * UART7
 * UART8
 * UART9
 * UART10
 * UART11
 * UART12
 * SPI1
 * SPI2
 * SPI3
 * SPI4
 * SPI5
 * SPI6
 * I2C1
 * I2C2
 * I2C3
 * I2C4
 * I2C5
 * I2C6
 * ETMR1
 * ETMR2
 * EPWM1
 * EPWM2
 * MPC_XSPI1A
 * MPC_XSPI1B
 * AAPB_MPC_XSPI1A
 * AAPB_MPC_XSPI1B
 * AAPB_MPC_SEIP
 * WDT8
 * PPC_APBMUX2
 * PPC_APBMUX3
 * PPC_APBMUX4
 * PPC_APBMUX1
 * MPC_ROMC
 * XB_SF
 * MPC_IRAMC1
 * MPC_CR5_SF
 * POR_SF_DIG
 * PT_SNS_SF_DIG
 * MPC_VIC1
 * BTM1
 * BTM2
 * BTM3
 * BTM4
 * AAPB_XSPI1A
 * AAPB_XSPI1B
 * MPC_SEIP
 * APB_APBMUX1_SLV
 * APB_PMUX2_DEC_SLV
 * AAPB_APBMUX3
 * APB_DCDC1_SLV
 * APB_APBMUX4_SLV
 * AAXI_APSF_MST
 */
extern sdrv_rstgen_sig_t rstsig_latent;

/*
 * @brief reset mission
 *
 * reset mission signals will reset automatically after power on.
 *
 */

/**
 * @brief reset mission 0
 *
 * signals in mission 0:
 * GPIO_SF
 * IOMUXC_SF
 * SMC
 * PMU_CORE
 * APB_APBMUX1_MST
 * APB_PMUX2_DEC_MST
 * APB_SEIP_NVM_MST
 */
extern sdrv_rstgen_sig_t rstsig_mission0;

/**
 * @brief reset mission 1
 *
 * signals in mission 1:
 * PLL1
 * PLL2
 * Pll3
 */
extern sdrv_rstgen_sig_t rstsig_mission1;

/**
 * @brief reset mission 2
 *
 * signals in mission 2:
 * ANA_SF_SADC1
 * ANA_SF_SADC2
 * ANA_SF_SADC3
 * ANA_SF_SACMP1
 * ANA_SF_SACMP2
 */
extern sdrv_rstgen_sig_t rstsig_mission2;

/**
 * @brief reset mission 3
 *
 * signals in mission 3:
 * IRAM1
 */
extern sdrv_rstgen_sig_t rstsig_mission3;

/**
 * @brief reset mission 4
 *
 * signals in mission 4:
 * DCDC1
 * APB_DCDC1_MST
 */
extern sdrv_rstgen_sig_t rstsig_mission4;

/**
 * @brief reset mission 5
 *
 * signals in mission 5:
 * APBMUX4
 * PTB
 * AHBDEC_SEIP
 * APB_SEC_STORAGE1_SLV
 * APB_SEIP_NVM_SLV
 * APB_APBMUX4_MST
 * AAXI_APSF_SLV
 */
extern sdrv_rstgen_sig_t rstsig_mission5;

/* reset module */
extern sdrv_rstgen_sig_t rstsig_canfd16;
extern sdrv_rstgen_sig_t rstsig_canfd21;
extern sdrv_rstgen_sig_t rstsig_canfd3;
extern sdrv_rstgen_sig_t rstsig_canfd4;
extern sdrv_rstgen_sig_t rstsig_canfd5;
extern sdrv_rstgen_sig_t rstsig_canfd6;
extern sdrv_rstgen_sig_t rstsig_canfd7;
extern sdrv_rstgen_sig_t rstsig_canfd23;
extern sdrv_rstgen_sig_t rstsig_xspi1a;
extern sdrv_rstgen_sig_t rstsig_xspi1b;
extern sdrv_rstgen_sig_t rstsig_dma_rst0;
extern sdrv_rstgen_sig_t rstsig_dma_rst1;
extern sdrv_rstgen_sig_t rstsig_enet1;
extern sdrv_rstgen_sig_t rstsig_vic1;
extern sdrv_rstgen_sig_t rstsig_xspi_slv;
extern sdrv_rstgen_sig_t rstsig_xtrg;
extern sdrv_rstgen_sig_t rstsig_saci2;
extern sdrv_rstgen_sig_t rstsig_sehc1;
extern sdrv_rstgen_sig_t rstsig_usb;
extern sdrv_rstgen_sig_t rstsig_seip;
extern sdrv_rstgen_sig_t rstsig_cslite;

/* general register */
extern sdrv_rstgen_general_reg_t reset_general_reg_sf_remap;
extern sdrv_rstgen_general_reg_t reset_general_reg_sf_boot;
extern sdrv_rstgen_general_reg_t reset_general_reg_rom_ctrl;

/* recovery module */
extern sdrv_recovery_btm_t recovery_btm_list;
extern sdrv_recovery_etimer_t recovery_etimer_list;
extern sdrv_recovery_epwm_t recovery_epwm_list;
extern sdrv_recovery_module_t recovery_module_array;
#endif