/**
 * @file ex_tp2825.h
 * @brief export header for sensor OV tp2825
 *
 * @copyright Copyright (c) 2021  Semidrive Semiconductor.
 *			  All rights reserved.
 */

#ifndef __EX_tp2825_H__
#define __EX_tp2825_H__

#include "exvdev.h"

struct vdev_device *tp2825_init(void *init_data);

#endif /* __EX_tp2825_H__ */
