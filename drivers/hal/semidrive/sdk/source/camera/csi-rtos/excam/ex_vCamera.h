/**
 * @file ex_vCamera.h
 * @brief export header for virtual camera
 *
 * @copyright Copyright (c) 2021  Semidrive Semiconductor.
 *			  All rights reserved.
 */

#ifndef __EX_VCAMERA_H__
#define __EX_VCAMERA_H__

#include "exvdev.h"

extern struct vdev_device *ex_vCamera_init(void *init_data);

#endif /* __EX_VCAMERA_H__ */
