/**
 * @file ex_max96706.h
 * @brief export header for max96706
 *
 * @copyright Copyright (c) 2021  Semidrive Semiconductor.
 *			  All rights reserved.
 */

#ifndef __EX_MAX96706_H__
#define __EX_MAX96706_H__

#include "exvdev.h"

struct vdev_device *max96706_init(void *init_data);

#endif /* __EX_MAX96706_H__ */
