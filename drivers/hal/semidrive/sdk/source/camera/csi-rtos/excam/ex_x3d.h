/**
 * @file ex_x3d.h
 * @brief export header for sensor OV X3D
 *
 * @copyright Copyright (c) 2021  Semidrive Semiconductor.
 *			  All rights reserved.
 */

#ifndef __EX_X3D_H__
#define __EX_X3D_H__

#include "exvdev.h"

struct vdev_device *x3d_init(void *init_data);

#endif /* __EX_X3D_H__ */
