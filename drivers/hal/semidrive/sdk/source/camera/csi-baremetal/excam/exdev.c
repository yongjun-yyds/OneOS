/*
 * Copyright (C) 2020-2030 Semidrive, Inc. All Rights Reserved.
 *
 * This program is vPortFree software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#include "exvdev.h"

#if CONFIG_OV5640
#include "ex_ov5640.h"
struct external_camera_api ov5640_interface = {
    .name = "ov5640",
    .init = ex_ov5640_init,
};
#endif

#if CONFIG_MAX96706
#include "ex_max96706.h"
struct external_camera_api max96706_interface = {
    .name = "max96706",
    .init = max96706_init,
};
#endif

#if CONFIG_TI926
#include "ex_ti926.h"
struct external_camera_api ti926_interface = {
    .name = "ti926",
    .init = ti926_init,
};
#endif
#if CONFIG_TC9591
#include "ex_tc9591.h"
struct external_camera_api tc9591_interface = {
    .name = "tc9591",
    .init = tc9591_init,
};
#endif
#if CONFIG_TP2825
#include "ex_tp2825.h"
struct external_camera_api tp2825_interface = {
    .name = "tp2825",
    .init = tp2825_init,
};
#endif

#if CONFIG_VIRTUAL_CAMERA
#include "ex_vCamera.h"
struct external_camera_api vcamera_interface = {
    .name = "vcamera",
    .init = ex_vCamera_init,
};
#endif

struct external_camera_api dummy_interface = {
    .name = "dummy",
    .init = NULL,
};

struct external_camera_api *excam_api_list[] = {
#if CONFIG_OV5640
    &ov5640_interface,
#endif

#if CONFIG_MAX96706
    &max96706_interface,
#endif

#if CONFIG_TI926
    &ti926_interface,
#endif

#if CONFIG_TC9591
    &tc9591_interface,
#endif
#if CONFIG_TP2825
    &tp2825_interface,
#endif
#if CONFIG_VIRTUAL_CAMERA
    &vcamera_interface,
#endif

    &dummy_interface,
};

const void *get_excam_api(const char *name)
{
    int i, num;

    num = sizeof(excam_api_list) / sizeof(excam_api_list[0]);
    for (i = 0; i < num; i++) {
        if ((excam_api_list[i]->init != NULL) &&
            !strcmp(excam_api_list[i]->name, name)) {
            return excam_api_list[i];
        }
    }

    return NULL;
}
