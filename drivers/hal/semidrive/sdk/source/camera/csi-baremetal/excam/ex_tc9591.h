/**
 * @file ex_tc9591.h
 * @brief export header for Toshiba tc9591
 *
 * @copyright Copyright (c) 2021  Semidrive Semiconductor.
 *			  All rights reserved.
 */

#ifndef __EX_TC9591_H__
#define __EX_TC9591_H__

#include "exvdev.h"

struct vdev_device *tc9591_init(void *init_data);

#endif /* __EX_TC9591_H__ */
