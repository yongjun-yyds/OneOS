/**
 * @file sdrv_recovery_hw.h
 * @brief semidrive recovery latent module hardware header file
 *
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */
#ifndef SDRV_RECOVERY_HW_H
#define SDRV_RECOVERY_HW_H
#include <types.h>

/* BTM recovery register offset */
#define BTM_G0_EN_OFFSET 0x04
#define BTM_G1_EN_OFFSET 0x24
#define BTM_INT_STA_OFFSET 0x40
#define BTM_INT_STA_EN_OFFSET 0x44
#define BTM_INT_SIG_EN_OFFSET 0x48

/* ETIMER recovery register offset */
#define ETIMER_SW_RST_OFFSET 0xA8U
#define ETIMER_DMA_CTRL_OFFSET 0xB0U
#define ETIMER_G0_EN_OFFSET 0x10CU
#define ETIMER_G1_EN_OFFSET 0x12CU
#define ETIMER_LCNT_A_EN_OFFSET 0x14CU
#define ETIMER_LCNT_B_EN_OFFSET 0x16CU
#define ETIMER_LCNT_C_EN_OFFSET 0x18CU
#define ETIMER_LCNT_D_EN_OFFSET 0x1ACU
#define ETIMER_CPT_CTRL_OFFSET 0x210U
#define ETIMER_CMP_CTRL_OFFSET 0x2E0U
#define ETIMER_INT_STA_OFFSET 0x0U
#define ETIMER_INT_STA_EN_OFFSET 0x4U
#define ETIMER_INT_SIG_EN_OFFSET 0x8U

/* EPWM recovery register offset */
#define EPWM_SW_RST_OFFSET 0xA8U
#define EPWM_DMA_CTRL_OFFSET 0xB8U
#define EPWM_G0_EN_OFFSET 0x10CU
#define EPWM_G1_EN_OFFSET 0x12CU
#define EPWM_CMP_CTRL_OFFSET 0x2E0U
#define EPWM_INT_STA_OFFSET 0x0U
#define EPWM_INT_STA_EN_OFFSET 0x4U
#define EPWM_INT_SIG_EN_OFFSET 0x8U

#endif /* SDRV_RECOVERY_HW_H */
