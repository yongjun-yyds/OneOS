/**
 * @file sdrv_crypto_sharemem.c
 * @brief crypto sharememory api
 *
 * @copyright Copyright (c) 2021  Semidrive Semiconductor.
 *            All rights reserved.
 */

#include <sdrv_crypto_sharemem.h>
#include <compiler.h>

#define SEIP_MEM_NODE_ITEM_NUM_MAX      10
#define CRYPTO_offsetof(TYPE, MEMBER)   ((unsigned int)&((TYPE *)0)->MEMBER)

/**
 * container_of - cast a member of a structure out to the containing structure
 * @ptr:    the pointer to the member.
 * @type:   the type of the container struct this is embedded in.
 * @member: the name of the member within the struct.
 *
 */
#define container_of(ptr, type, member) ({                      \
        void *__mptr = (void *)(ptr);                           \
        ((type *)(__mptr - CRYPTO_offsetof(type, member))); })

uint8_t __ALIGNED(CONFIG_ARCH_CACHE_LINE) update_buff[ROUNDUP(CRYPTO_SHARE_MEM_DIG_MAX, CONFIG_ARCH_CACHE_LINE)];
uint8_t __ALIGNED(CONFIG_ARCH_CACHE_LINE) msg_padding[ROUNDUP(CRYPTO_SHARE_MEM_RSA_MAX, CONFIG_ARCH_CACHE_LINE)];
uint8_t __ALIGNED(CONFIG_ARCH_CACHE_LINE) rev_cpy_buf[ROUNDUP(CRYPTO_SHARE_MEM_RSA_MAX, CONFIG_ARCH_CACHE_LINE)];
uint8_t __ALIGNED(CONFIG_ARCH_CACHE_LINE) msg_padding_ext[ROUNDUP(CRYPTO_SHARE_MEM_RSA_MAX_EXT, CONFIG_ARCH_CACHE_LINE)];
uint8_t __ALIGNED(CONFIG_ARCH_CACHE_LINE) dbMask[ROUNDUP(CRYPTO_SHARE_MEM_RSA_MAX, CONFIG_ARCH_CACHE_LINE)];
uint8_t __ALIGNED(CONFIG_ARCH_CACHE_LINE) hash[ROUNDUP(CRYPTO_SHARE_MEM_DIG_MAX, CONFIG_ARCH_CACHE_LINE)];
uint8_t __ALIGNED(CONFIG_ARCH_CACHE_LINE) hash_padding[ROUNDUP(CRYPTO_SHARE_MEM_HASH_PAD_MAX, CONFIG_ARCH_CACHE_LINE)];
uint8_t __ALIGNED(CONFIG_ARCH_CACHE_LINE) comm_buff[ROUNDUP(CRYPTO_SHARE_MEM_COMM_BUFF_MAX, CONFIG_ARCH_CACHE_LINE)];
uint8_t __ALIGNED(CONFIG_ARCH_CACHE_LINE) comm_small_buff[ROUNDUP(CRYPTO_SHARE_MEM_COMM_SMALL_BUFF_MAX, CONFIG_ARCH_CACHE_LINE)];
uint8_t __ALIGNED(CONFIG_ARCH_CACHE_LINE) cmd_mac_buff[ROUNDUP(CRYPTO_SHARE_MEM_COMM_SMALL_BUFF_MAX, CONFIG_ARCH_CACHE_LINE)];

struct mem_node {
    uint32_t size;
    uint8_t is_used;
    uint8_t *ptr;
};

struct seip_heap {
    uint8_t item_num;
    uint8_t used_num;
    uint8_t used_num_max;
    struct mem_node *mem_node_list;
};

struct mem_node mem_node_vlaue[SEIP_MEM_NODE_ITEM_NUM_MAX]={
    {
        .size = CRYPTO_SHARE_MEM_DIG_MAX,
        .is_used = 0,
        .ptr = hash,
    },
    {
        .size = CRYPTO_SHARE_MEM_DIG_MAX,
        .is_used = 0,
        .ptr = update_buff,
    },
    {
        .size = CRYPTO_SHARE_MEM_COMM_SMALL_BUFF_MAX,
        .is_used = 0,
        .ptr = comm_small_buff,
    },
    {
        .size = CRYPTO_SHARE_MEM_COMM_SMALL_BUFF_MAX,
        .is_used = 0,
        .ptr = cmd_mac_buff,
    },
    {
        .size = CRYPTO_SHARE_MEM_HASH_PAD_MAX,
        .is_used = 0,
        .ptr = hash_padding,
    },
    {
        .size = CRYPTO_SHARE_MEM_RSA_MAX,
        .is_used = 0,
        .ptr = msg_padding,
    },
    {
        .size = CRYPTO_SHARE_MEM_RSA_MAX,
        .is_used = 0,
        .ptr = dbMask,
    },
    {
        .size = CRYPTO_SHARE_MEM_RSA_MAX,
        .is_used = 0,
        .ptr = rev_cpy_buf,
    },
    {
        .size = CRYPTO_SHARE_MEM_RSA_MAX_EXT,
        .is_used = 0,
        .ptr = msg_padding_ext,
    },
    {
        .size = CRYPTO_SHARE_MEM_COMM_BUFF_MAX,
        .is_used = 0,
        .ptr = comm_buff,
    },
};

/* seip_heap static vars */
static struct seip_heap inheap = {
    .item_num = SEIP_MEM_NODE_ITEM_NUM_MAX,
    .used_num = 0,
    .used_num_max = 0,
    .mem_node_list = &mem_node_vlaue[0],
};

#define to_mem_node(priv) container_of((priv), struct mem_node, ptr)

uint8_t *ShareMem_GetBlock(uint32_t size)
{
    uint8_t i = 0;
    struct mem_node *return_node = NULL;

    for (i = 0; i < inheap.item_num; i++) {
        if ((inheap.mem_node_list[i].size >= size) &&
            (inheap.mem_node_list[i].is_used == 0)) {
            inheap.mem_node_list[i].is_used = 1;
            inheap.used_num++;

            if (inheap.used_num > inheap.used_num_max) {
                inheap.used_num_max = inheap.used_num;
            }

            return_node = &(inheap.mem_node_list[i]);
            break;
        }
    }

    if (return_node != NULL){
        return return_node->ptr;
    }

    return NULL;
}

sdrv_crypto_error_status_e ShareMem_ReleaseBlock(uint8_t *ptr)
{
    sdrv_crypto_error_status_e RetVal = E_NOT_OK;
    uint8_t i = 0;

    if(ptr != NULL){
        for (i = 0; i < inheap.item_num; i++) {
            if ((inheap.mem_node_list[i].ptr == ptr) &&
                (inheap.mem_node_list[i].is_used == 1)) {
                inheap.mem_node_list[i].is_used = 0;
                inheap.used_num--;
                RetVal = E_OK;
                break;
            }
        }
    }

    return RetVal;
}
