/**
 * @file sdrv_crypto_init.c
 * @brief Semidrive CRYPTO init source file.
 *
 * @copyright Copyright (c) 2021  Semidrive Semiconductor.
 *            All rights reserved.
 */

#include <sdrv_crypto_init.h>
#include <sdrv_crypto_mailbox_soc_seip_reg.h>
#include <sdrv_crypto_utility.h>
#include <regs_base.h>

#define EFUSEC_EHSM_CFG_OFFSET 0x1300

#define RSTGEN_POLLs 0x10000

#define REG32(addr) ((volatile uint32_t *)(uintptr_t)(addr))
#define REG8(addr) ((volatile uint8_t *)(uintptr_t)(addr))

#define ce_writel(a, v) (*REG32(a) = (v))
#define ce_readl(a) (*REG32(a))
#define ce_writeb(a, v) (*REG8(a) = (v))
#define ce_readb(a) (*REG8(a))

#define SEIP_ERROR (0x37)
#define SEIP_FW_VER_SUCCESS (0xE6)

uint32_t sdrv_crypto_type = 0;

void init_efuse_shadow_register_for_test(void)
{
    unsigned int data;
    /*Step1 wati efuse init done (not busy)*/
    printf("[seip test] Polling for efusec busy released\n");

    /*bit[10] busy*/
    data = ce_readl(APB_EFUSEC_BASE + 0x0);

    if ((data & (1 << 10)) == 0) {
        printf("efusec is ready\n");
    } else {
        printf("[seip test] Efusec still busy, check delay time or efusec "
               "behavior\n");
        return;
    }

    /*Step2 write otp*/
    printf("[seip test] Initializing OTP...\n");

    /*Self_Ctrl_For_Boot*/
    /*seip_control field, seip_mode_sel = 10 CACC mode 00 MAILBOX mode
     * 0x3F000000*/
#if CONFIG_OSR_CRYPTO_TEST_CACC
    ce_writel(APB_EFUSEC_BASE + 0x1300, 0x3F020000);
    printf("[seip test] CACC MODE SELECT...\n");
#else
    ce_writel(APB_EFUSEC_BASE + 0x1300, 0x00000000);
    printf("[seip test] MIALBOX MODE SELECT...\n");
#endif

    /*life cycle mode*/
    ce_writel(APB_EFUSEC_BASE + 0x1304, 0x57C1EC96);

    /*analog timming 1*/
    ce_writel(APB_EFUSEC_BASE + 0x1308, 0x00000000);
    ce_writel(APB_EFUSEC_BASE + 0x130C, 0x00000000);

    /*secure flag*/
    ce_writel(APB_EFUSEC_BASE + 0x1310, 0xFFFFFFFF);

    /*PKD Public Key Digest 8x32*/
    ce_writel(APB_EFUSEC_BASE + 0x1080, 0x5C4CE1B2);
    ce_writel(APB_EFUSEC_BASE + 0x1084, 0x5BDFC679);
    ce_writel(APB_EFUSEC_BASE + 0x1088, 0x7EFEF485);
    ce_writel(APB_EFUSEC_BASE + 0x108c, 0x267ADBD8);
    ce_writel(APB_EFUSEC_BASE + 0x1090, 0xE0A79D2B);
    ce_writel(APB_EFUSEC_BASE + 0x1094, 0xA90ECB7C);
    ce_writel(APB_EFUSEC_BASE + 0x1098, 0x8C7B74F4);
    ce_writel(APB_EFUSEC_BASE + 0x109c, 0xF3A4A8CD);

    /*SSK CTRL0 CTRL1*/
    ce_writel(APB_EFUSEC_BASE + 0x1318, 0x20009100);
    ce_writel(APB_EFUSEC_BASE + 0x131C, 0x81910000);

    /*analog timming 2*/
    ce_writel(APB_EFUSEC_BASE + 0x1314, 0x00000000);

    /*FSRK*/
    ce_writel(APB_EFUSEC_BASE + 0x10c0, 0x01cca048);
    ce_writel(APB_EFUSEC_BASE + 0x10c4, 0x13e16fc6);
    ce_writel(APB_EFUSEC_BASE + 0x10c8, 0x468c8fc3);
    ce_writel(APB_EFUSEC_BASE + 0x10cc, 0x9b33e294);

    /*Debug Passphrase(DPK)*/
    ce_writel(APB_EFUSEC_BASE + 0x10E0, 0x555a8f01);
    ce_writel(APB_EFUSEC_BASE + 0x10E4, 0x97445894);
    ce_writel(APB_EFUSEC_BASE + 0x10E8, 0xe4f7ffc0);
    ce_writel(APB_EFUSEC_BASE + 0x10EC, 0xfc163902);
    ce_writel(APB_EFUSEC_BASE + 0x10F0, 0x69bffe04);
    ce_writel(APB_EFUSEC_BASE + 0x10F4, 0x6afaab9f);
    ce_writel(APB_EFUSEC_BASE + 0x10F8, 0xb2909223);
    ce_writel(APB_EFUSEC_BASE + 0x10FC, 0x605c925b);

    /*SSRK*/
    ce_writel(APB_EFUSEC_BASE + 0x10d0, 0x6f5a1159);
    ce_writel(APB_EFUSEC_BASE + 0x10d4, 0xc4c128be);
    ce_writel(APB_EFUSEC_BASE + 0x10d8, 0x51d3ab6d);
    ce_writel(APB_EFUSEC_BASE + 0x10dc, 0x79dfe324);

    /*Firmware Update Key(fuk)*/
    ce_writel(APB_EFUSEC_BASE + 0x1100, 0xe526a831);
    ce_writel(APB_EFUSEC_BASE + 0x1104, 0x91d949bb);
    ce_writel(APB_EFUSEC_BASE + 0x1108, 0xe981ea4b);
    ce_writel(APB_EFUSEC_BASE + 0x110c, 0x482b906c);
    ce_writel(APB_EFUSEC_BASE + 0x1110, 0x51422e72);
    ce_writel(APB_EFUSEC_BASE + 0x1114, 0x84e87752);
    ce_writel(APB_EFUSEC_BASE + 0x1118, 0x38da4eec);
    ce_writel(APB_EFUSEC_BASE + 0x111c, 0x588dfa57);

    /*Flash Decryption Key(fdk)*/
    ce_writel(APB_EFUSEC_BASE + 0x1120, 0x045ade7e);
    ce_writel(APB_EFUSEC_BASE + 0x1124, 0xc994f43a);
    ce_writel(APB_EFUSEC_BASE + 0x1128, 0xefaeeaea);
    ce_writel(APB_EFUSEC_BASE + 0x112c, 0xb0a913c3);
    ce_writel(APB_EFUSEC_BASE + 0x1130, 0x51422e72);
    ce_writel(APB_EFUSEC_BASE + 0x1134, 0x84e87752);
    ce_writel(APB_EFUSEC_BASE + 0x1138, 0x38da4eec);
    ce_writel(APB_EFUSEC_BASE + 0x113c, 0x588dfa57);

    /*SSK2 (GP_KEY0)*/
    ce_writel(APB_EFUSEC_BASE + 0x1140, 0xb7125e26);
    ce_writel(APB_EFUSEC_BASE + 0x1144, 0xdc6dea26);
    ce_writel(APB_EFUSEC_BASE + 0x1148, 0x67aadb08);
    ce_writel(APB_EFUSEC_BASE + 0x114c, 0x7f5d3ac6);
    ce_writel(APB_EFUSEC_BASE + 0x1150, 0x84b3ba9b);
    ce_writel(APB_EFUSEC_BASE + 0x1154, 0x9407ff29);
    ce_writel(APB_EFUSEC_BASE + 0x1158, 0x9bc6c26d);
    ce_writel(APB_EFUSEC_BASE + 0x115c, 0x93da36b5);

    /*SSK3 (GD_KEY1)*/
    ce_writel(APB_EFUSEC_BASE + 0x1160, 0xc2c6cf73);
    ce_writel(APB_EFUSEC_BASE + 0x1164, 0x3a6a7aee);
    ce_writel(APB_EFUSEC_BASE + 0x1168, 0x6419d405);
    ce_writel(APB_EFUSEC_BASE + 0x116c, 0x098bea55);
    ce_writel(APB_EFUSEC_BASE + 0x1170, 0x51422e72);
    ce_writel(APB_EFUSEC_BASE + 0x1174, 0x84e87752);
    ce_writel(APB_EFUSEC_BASE + 0x1178, 0x38da4eec);
    ce_writel(APB_EFUSEC_BASE + 0x117c, 0x588dfa57);

    /*SSK4 (GD_KEY2)*/
    ce_writel(APB_EFUSEC_BASE + 0x1180, 0x512fdc13);
    ce_writel(APB_EFUSEC_BASE + 0x1184, 0x5f9dd394);
    ce_writel(APB_EFUSEC_BASE + 0x1188, 0xcd43a99b);
    ce_writel(APB_EFUSEC_BASE + 0x118c, 0x3364117f);
    ce_writel(APB_EFUSEC_BASE + 0x1190, 0x7f3b7c37);
    ce_writel(APB_EFUSEC_BASE + 0x1194, 0x2f59b8c8);
    ce_writel(APB_EFUSEC_BASE + 0x1198, 0xa5487ae8);
    ce_writel(APB_EFUSEC_BASE + 0x119c, 0x1b9cc114);

    /*CRC (KCRC)*/
    ce_writel(APB_EFUSEC_BASE + 0x12a0, 0xD274ED53);
    ce_writel(APB_EFUSEC_BASE + 0x12a4, 0x7C1985E9);
    ce_writel(APB_EFUSEC_BASE + 0x12a8, 0xDF4C8867);
    ce_writel(APB_EFUSEC_BASE + 0x12ac, 0xB05F2E42);
    ce_writel(APB_EFUSEC_BASE + 0x12b0, 0x8F6AF678);
    ce_writel(APB_EFUSEC_BASE + 0x12b4, 0x179935B0);
    ce_writel(APB_EFUSEC_BASE + 0x12b8, 0x17797D10);
    ce_writel(APB_EFUSEC_BASE + 0x12bc, 0x4EE0CBE3);

    ce_writeb(APB_EFUSEC_BASE + 0x12c9, 0x1);

    printf("[seip test] init_efuse_shadow_register_default completed\n");
}

/*write i_kbuf_ctrl*/
void write_kbuf_ctrl(unsigned int data)
{
    int reg = 0;
    int rdata = 0;
    rdata = ce_readl(APB_EFUSEC_BASE + 0x12C8);
    reg = (rdata & 0xFFFF) | (data << 16);
    ce_writel(APB_EFUSEC_BASE + 0x12C8, reg);
}

/*write keys with default algorithm*/
void check_efuse_shadow_register_for_test(void)
{
    int rdata;

    printf("AHB Reading and Checking Key..........\n");
    /*read key through AHB*/
    /*FSRT READY KEY*/
    write_kbuf_ctrl(0xe78a);

    /*sys_tb_ctrl_delay_us_for_sim_only(2);*/
    rdata = ce_readl(0x021de800);

    if (rdata != 0x107119e7) {
        printf("FISRT KEY ERROR! 1rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021de804);

    if (rdata != 0x5fe88b1e) {
        printf("FISRT KEY ERROR! 2rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021de808);

    if (rdata != 0x8922e4ae) {
        printf("FISRT KEY ERROR! 3rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021de80c);

    if (rdata != 0xceae16cf) {
        printf("FISRT KEY ERROR! 4rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021de810);

    if (rdata != 0x00000000) {
        printf("FISRT KEY ERROR! 5rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021de814);

    if (rdata != 0x00000000) {
        printf("FISRT KEY ERROR! 6rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021de818);

    if (rdata != 0x00000000) {
        printf("FISRT KEY ERROR! 7rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021de81c);

    if (rdata != 0x00000000) {
        printf("FISRT KEY ERROR! 8rdata =0x%x\n", rdata);
    }

    /*DEBUG PASSPHRASE KEY*/
    write_kbuf_ctrl(0x5d4);

    rdata = ce_readl(0x021de820);

    if (rdata != 0x0ee9f560) {
        printf("DEBUG PASSPHRASE ERROR!1rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021de824);

    if (rdata != 0x9a22ecba) {
        printf("DEBUG PASSPHRASE ERROR!2rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021de828);

    if (rdata != 0xa3a43744) {
        printf("DEBUG PASSPHRASE ERROR!3rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021de82c);

    if (rdata != 0x8a9d9b8d) {
        printf("DEBUG PASSPHRASE ERROR!4rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021de830);

    if (rdata != 0x00000000) {
        printf("DEBUG PASSPHRASE ERROR!5rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021de834);

    if (rdata != 0x00000000) {
        printf("DEBUG PASSPHRASE ERROR!6rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021de838);

    if (rdata != 0x00000000) {
        printf("DEBUG PASSPHRASE ERROR!7rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021de83c);

    if (rdata != 0x00000000) {
        printf("DEBUG PASSPHRASE ERROR!8rdata =0x%x\n", rdata);
    }

    /*SSRK*/
    write_kbuf_ctrl(0x2355);

    rdata = ce_readl(0x021de840);

    if (rdata != 0xf30e2676) {
        printf("SECOND KEY ERROR!1rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021de844);

    if (rdata != 0xeac1222c) {
        printf("SECOND KEY ERROR!2rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021de848);

    if (rdata != 0x02e596c5) {
        printf("SECOND KEY ERROR!3rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021de84c);

    if (rdata != 0x79a8eb9f) {
        printf("SECOND KEY ERROR!4rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021de850);

    if (rdata != 0x00000000) {
        printf("SECOND KEY ERROR!5rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021de854);

    if (rdata != 0x00000000) {
        printf("SECOND KEY ERROR!6rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021de858);

    if (rdata != 0x00000000) {
        printf("SECOND KEY ERROR!7rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021de85c);

    if (rdata != 0x00000000) {
        printf("SECOND KEY ERROR!8rdata =0x%x\n", rdata);
    }

    /*FIRMWARE UPDATE KEY*/
    write_kbuf_ctrl(0xa55a);

    rdata = ce_readl(0x021de860);

    if (rdata != 0x45a4079f) {
        printf("FIREMWARE UPDATE KEY ERROR!1rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021de864);

    if (rdata != 0x92fcde09) {
        printf("FIREMWARE UPDATE KEY ERROR!2rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021de868);

    if (rdata != 0x2a553d1f) {
        printf("FIREMWARE UPDATE KEY ERROR!3rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021de86c);

    if (rdata != 0x8e06c0cb) {
        printf("FIREMWARE UPDATE KEY ERROR!4rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021de870);

    if (rdata != 0x00000000) {
        printf("FIREMWARE UPDATE KEY ERROR!5rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021de874);

    if (rdata != 0x00000000) {
        printf("FIREMWARE UPDATE KEY ERROR!6rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021de878);

    if (rdata != 0x00000000) {
        printf("FIREMWARE UPDATE KEY ERROR!7rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021de87c);

    if (rdata != 0x00000000) {
        printf("FIREMWARE UPDATE KEY ERROR!8rdata =0x%x\n", rdata);
    }

    /*FLASH DECRYPTION KEY*/
    write_kbuf_ctrl(0x2ca1);

    rdata = ce_readl(0x021de880);

    if (rdata != 0x104bab38) {
        printf("FLASH DECRYPTION KEY ERROR!1rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021de884);

    if (rdata != 0xebe73c79) {
        printf("FLASH DECRYPTION KEY ERROR!2rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021de888);

    if (rdata != 0x1cb5491c) {
        printf("FLASH DECRYPTION KEY ERROR!3rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021de88c);

    if (rdata != 0x52e8dc31) {
        printf("FLASH DECRYPTION KEY ERROR!4rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021de890);

    if (rdata != 0x00000000) {
        printf("FLASH DECRYPTION KEY ERROR!5rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021de894);

    if (rdata != 0x00000000) {
        printf("FLASH DECRYPTION KEY ERROR!6rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021de898);

    if (rdata != 0x00000000) {
        printf("FLASH DECRYPTION KEY ERROR!7rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021de89c);

    if (rdata != 0x00000000) {
        printf("FLASH DECRYPTION KEY ERROR!8rdata =0x%x\n", rdata);
    }

    /*SSK2*/
    write_kbuf_ctrl(0xcb5b);

    rdata = ce_readl(0x021de8a0);

    if (rdata != 0x8d7972b0) {
        printf("SSK2  ERROR!1rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021de8a4);

    if (rdata != 0xcccb6db2) {
        printf("SSK2  ERROR!2rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021de8a8);

    if (rdata != 0x36948442) {
        printf("SSK2  ERROR!3rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021de8ac);

    if (rdata != 0xc4f5b5f0) {
        printf("SSK2  ERROR!4rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021de8b0);

    if (rdata != 0x697770aa) {
        printf("SSK2  ERROR!5rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021de8b4);

    if (rdata != 0x08a35e00) {
        printf("SSK2  ERROR!6rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021de8b8);

    if (rdata != 0xbc9c08b3) {
        printf("SSK2  ERROR!7rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021de8bc);

    if (rdata != 0x362d70f8) {
        printf("SSK2  ERROR!8rdata =0x%x\n", rdata);
    }

    /*SSK3*/
    write_kbuf_ctrl(0x38d1);

    rdata = ce_readl(0x021de8c0);

    if (rdata != 0x95d63843) {
        printf("SSK3  ERROR!1rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021de8c4);

    if (rdata != 0xc1698e12) {
        printf("SSK3  ERROR!2rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021de8c8);

    if (rdata != 0x5d6dec1b) {
        printf("SSK3  ERROR!3rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021de8cc);

    if (rdata != 0x1cf9cfe3) {
        printf("SSK3  ERROR!4rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021de8d0);

    if (rdata != 0x00000000) {
        printf("SSK3  ERROR!5rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021de8d4);

    if (rdata != 0x00000000) {
        printf("SSK3  ERROR!6rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021de8d8);

    if (rdata != 0x00000000) {
        printf("SSK3  ERROR!7rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021de8dc);

    if (rdata != 0x00000000) {
        printf("SSK3  ERROR!8rdata =0x%x\n", rdata);
    }

    /*SSK4*/
    write_kbuf_ctrl(0x7841);

    rdata = ce_readl(0x021de8e0);

    if (rdata != 0x3cacc647) {
        printf("SSK4  ERROR!1rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021de8e4);

    if (rdata != 0xec7f7074) {
        printf("SSK4  ERROR!2rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021de8e8);

    if (rdata != 0xfa2d7f2f) {
        printf("SSK4  ERROR!3rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021de8ec);

    if (rdata != 0x55b12764) {
        printf("SSK4  ERROR!4rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021de8f0);

    if (rdata != 0x51c6c3f2) {
        printf("SSK4  ERROR!5rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021de8f4);

    if (rdata != 0x7e542a12) {
        printf("SSK4  ERROR!6rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021de8f8);

    if (rdata != 0x922d9146) {
        printf("SSK4  ERROR!7rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021de8fc);

    if (rdata != 0x55711916) {
        printf("SSK4  ERROR!8rdata =0x%x\n", rdata);
    }

    rdata = ce_readl(0x021def00);
    printf("SSK_CTRL0 rdata =0x%x\n", rdata);
    rdata = ce_readl(0x021def04);
    printf("SSK_CTRL1 rdata =0x%x\n", rdata);

    printf("[seip test] KEY CHECK DONE!\n");
}

void sdrv_crypto_set_type(uint32_t crypto_type)
{
    sdrv_crypto_type = crypto_type;
}

/**
 * @brief Get the type of CRYPTO type.
 *
 * This function return crypto type
 *
 * @return The result of CRYPTO type.
 */
uint32_t sdrv_crypto_get_type(void) { return sdrv_crypto_type; }

/**
 * @brief Sdrv CRYPTO initialization.
 *
 * This function init hsm
 */
void sdrv_crypto_init(void)
{
    unsigned int rdata = 0;
    unsigned int rdata_lifecycle = 0;

    printf("sdrv_crypto_init enter \n");

    /*polling status seip boot_done*/
    do {
        rdata = ce_readl(APB_SEIP_BASE + 0x14);
    } while ((rdata & 0x100) == 0);
    printf("seip boot done\n");

    /* if fuse config offset 16, seip_mode_sel = 10 CACC mode, 00 MAILBOX mode*/
    rdata = ce_readl(APB_EFUSEC_BASE + EFUSEC_EHSM_CFG_OFFSET);

    /*get hsm life cycle value*/
    rdata_lifecycle = ce_readl(APB_EFUSEC_BASE + 0x1304);

    /* wait for seip ready for mailbox */
    if ((rdata & 0x30000) == 0x0) {
        if (rdata_lifecycle == 0x0) {
            /*test mode not rsp value*/
            printf("mailbox test mode init success\n");

        } else {

            while (rSOCMBOX_RSP_D0 == 0x0) {
            }

            rSOCMBOX_INT_STA = 0x02;

            if (rSOCMBOX_RSP_D0 == SEIP_FW_VER_SUCCESS) {
                printf("mailbox init success\n");
            } else {
                printf("mailbox init fail value=0x%x \n", rSOCMBOX_RSP_D0);
            }
        }

        sdrv_crypto_set_type(SDRV_CRYPTO_TYPE_MAILBOX);
    } else {
        sdrv_crypto_set_type(SDRV_CRYPTO_TYPE_CACC);
        printf("cacc init success\n");
    }
}
