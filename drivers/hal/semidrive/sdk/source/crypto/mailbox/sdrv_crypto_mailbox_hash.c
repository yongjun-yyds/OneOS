/**
 * @file sdrv_crypto_mailbox_hash.c
 * @brief SemiDrive CRYPTO hash interface source file.
 *
 * @copyright Copyright (c) 2021  Semidrive Semiconductor.
 *            All rights reserved.
 */

#include <sdrv_crypto_mailbox_hash.h>

/**
 * @brief get hash value.
 *
 * This function get hash.
 *
 * @param[in] mode hash mode.
 * @param[in] msg msg buff.
 * @param[in] msg_bytes msg len.
 * @param[out] digest hash value.
 * @return CMD_RETURN_SUCCESS or error code.
 */
sdrv_crypto_error_status_e cmd_hash_calc(uint8_t mode, uint8_t *msg,
                                         uint32_t msg_bytes, uint8_t *digest)
{
    uint32_t cmd_buf[8] = {0};
    uint32_t ret;
    cmd_hash_t *cmd_generate_digest = NULL;

    /*********** generate digest ***********/
    cmd_generate_digest = (cmd_hash_t *)cmd_buf;
    cmd_generate_digest->cmd_id = HASH_CALC;
    cmd_generate_digest->mode = mode;

    set_big_endian_4byte((uint8_t *)cmd_generate_digest->data_ptr,
                         (uint32_t)msg);
    set_big_endian_4byte((uint8_t *)cmd_generate_digest->data_len,
                         (uint32_t)msg_bytes);
    set_big_endian_4byte((uint8_t *)cmd_generate_digest->digest_ptr,
                         (uint32_t)digest);
    ret = send_cmd_and_wait_response((uint32_t *)cmd_buf);

    if (SEIP_SUCCESS == ret) {

    }
    else {
        printf("HASH generate digest failure, mode=%d, ret=0x%x \n", mode, ret);
        return CMD_RETURN_FAIL;
    }

    return CMD_RETURN_SUCCESS;
}

/**
 * @brief get hash value.
 *
 * This function get segment-hash.
 *
 * @param[in] mode hash mode.
 * @param[in] process_mode segment-hash mode.
 * @param[in] msg msg buff.
 * @param[in] msg_bytes msg len.
 * @param[out] digest hash value.
 * @param[in] byte length of previously processed messages.
 * @return CMD_RETURN_SUCCESS or error code.
 */
sdrv_crypto_error_status_e cmd_hash_update_calc(uint8_t mode,
                                                uint8_t process_mode,
                                                uint8_t *msg,
                                                uint32_t msg_bytes,
                                                uint8_t *digest,
                                                uint64_t predata_len)
{
    uint32_t cmd_buf[8] = {0};
    uint32_t ret;
    uint32_t lenl, lenh;
    cmd_hash_t *cmd_generate_digest = NULL;

    /*********** generate digest ***********/
    cmd_generate_digest = (cmd_hash_t *)cmd_buf;
    cmd_generate_digest->cmd_id = HASH_CALC;
    cmd_generate_digest->mode = mode;
    cmd_generate_digest->process_mode = process_mode;

    set_big_endian_4byte((uint8_t *)cmd_generate_digest->data_ptr,
                        (uint32_t)msg);
    set_big_endian_4byte((uint8_t *)cmd_generate_digest->data_len,
                        (uint32_t)msg_bytes);
    set_big_endian_4byte((uint8_t *)cmd_generate_digest->digest_ptr,
                        (uint32_t)digest);
    lenh = predata_len >> 32;
    lenl = predata_len & 0xffffffff;
    set_big_endian_4byte((uint8_t *)cmd_generate_digest->predata_len, lenh);
    set_big_endian_4byte((uint8_t *)cmd_generate_digest->predata_len + 4, lenl);
    ret = send_cmd_and_wait_response((uint32_t *)cmd_buf);

    if (SEIP_SUCCESS == ret) {

    } else {
        printf("HASH generate digest failure, mode=%d, ret=0x%x \n", mode, ret);
        return CMD_RETURN_FAIL;
    }

    return CMD_RETURN_SUCCESS;
}
