/**
 * @file sdrv_pinctrl.c
 * @brief Sdrv pinctrl driver
 *
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */

#include <types.h>
#include <debug.h>
#include <param.h>
#include <bits.h>
#include <reg.h>
#include <regs_base.h>
#include <sdrv_pinctrl.h>
#include <sdrv_gpio.h>
#include <device_pin.h>
#include <part.h>

#define TAISHAN_PAD_CONFIG_REG_OFFSET    0x1000
#define TAISHAN_MUX_CONFIG_REG_OFFSET    0x2000
#define TAISHAN_INPUT_SELECT_REG_OFFSET  0x3000

#define MUX_FUNC_SHIFT              0
#define MUX_FUNC_WIDTH              4
#define MUX_ODE_SHIFT               4
#define MUX_ODE_WIDTH               1
#define MUX_FIN_SHIFT               8
#define MUX_FIN_WIDTH               2
#define MUX_FIN_IP_SHIFT            10
#define MUX_FIN_IP_WIDTH            16
#define MUX_FV_SHIFT                29
#define MUX_FV_WIDTH                1
#define MUX_DO_FORCE_EN_SHIFT       30
#define MUX_DO_FORCE_EN_WIDTH       1
#define MUX_DO_FORCE_VALUE_SHIFT    31
#define MUX_DO_FORCE_VALUE_WIDTH    1

#define PAD_PULL_SHIFT              0
#define PAD_PULL_WIDTH              2
#define PAD_DS_SHIFT                4
#define PAD_DS_WIDTH                2
#define PAD_SR_SHIFT                8
#define PAD_SR_WIDTH                1
#define PAD_IS_SHIFT                12
#define PAD_IS_WIDTH                1
#define PAD_POE_SHIFT               16
#define PAD_POE_WIDTH               1
#define PAD_MS_SHIFT                20
#define PAD_MS_WIDTH                1

#define INPUT_SELECT_SRC_SEL_SHIFT  0
#define INPUT_SELECT_SRC_SEL_WIDTH  4

/* Alternative function selection. */
#define MUX_FUNC_GPIO               (0x0)
#define MUX_FUNC_ALT1               (0x1)
#define MUX_FUNC_ALT2               (0x2)
#define MUX_FUNC_ALT3               (0x3)
#define MUX_FUNC_ALT4               (0x4)
#define MUX_FUNC_ALT5               (0x5)
#define MUX_FUNC_ALT6               (0x6)
#define MUX_FUNC_ALT7               (0x7)
#define MUX_FUNC_ALT8               (0x8)
#define MUX_FUNC_ALT9               (0x9)

/* Open drain or push pull, only for output pads. */
#define MUX_ODE_OPENDRAIN           (0x1)
#define MUX_ODE_PUSHPULL            (0x0)

/* Pulls. */
#define PAD_NOPULL                  (0x0)
#define PAD_PULLDOWN                (0x1)
#define PAD_PULLUP                  (0x3)

/* Output drive strength. */
#define PAD_DS_2mA                  (0x0)
#define PAD_DS_8mA                  (0x1)
#define PAD_DS_4mA                  (0x2)
#define PAD_DS_12mA                 (0x3)

/* Slew rate. */
#define PAD_SR_FAST                 (0x0)
#define PAD_SR_SLOW                 (0x1)

/* Input Schmitt Select. Schmitt trigger mode provides input
 * glitch filtering (default is normal mode).
 */
#define PAD_IS_SCHMITT              (0x1)
#define PAD_IS_NORMAL               (0x0)

/* Force input ON/OFF.
 *  - no force:  Pad input/output is selected by ALT function.
 *  - force enable: Enable pad input, regardless of ALT function.
 *  - force disable: Disable pad input. Internal IP read the value of
 *    FORCE_INPUT bit.
 */
#define PAD_INPUT_NO_FORCE           (0x0)
#define PAD_INPUT_FORCE_ENABLE       (0x1)
#define PAD_INPUT_FORCE_DISABLE      (0x2)
/* Force input value. Used in PAD_INPUT_FORCE_DISABLE mode. */
#define PAD_FORCE_INPUT_VALUE_0      (0)
#define PAD_FORCE_INPUT_VALUE_1      (1)

#define LVDS_CH0_PAD_SET_(i)        (0x1010 + 0x4 * (i))
#define LVDS_CH0_PAD_COM_SET_(i)    (0x1100)
#define CH0_RTERM_EN_SHIFT          (1)
#define CH0_RTERM_EN_MASK           (0x1 << CH0_RTERM_EN_SHIFT)
#define CH0_TXEN_SHIFT              (7)
#define CH0_TXEN_MASK               (0x1 << CH0_TXEN_SHIFT)
#define CH0_RXEN_SHIFT              (6)
#define CH0_RXEN_MASK               (0x1 << CH0_RXEN_SHIFT)
#define CH0_RTERM_SHIFT             (1)
#define CH0_RTERM_MASK              (0xF << CH0_RTERM_SHIFT)

#define CALL_RET_CHECK(ret, funcall) \
    ret = funcall; \
    if (ret != SDRV_STATUS_OK) { return ret; }

#if (CONFIG_E3L)
const sdrv_input_select_t sdrv_pinctrl_is[] = {
    {    JTAG_TDI,   2,   0x3000U,  0,    APB_IOMUXC_SF_BASE},
    {    JTAG_TDI,   4,   0x3004U,  0,    APB_IOMUXC_SF_BASE},
    {    JTAG_TDI,   5,   0x3008U,  0,    APB_IOMUXC_SF_BASE},
    {    JTAG_TDO,   4,   0x300cU,  0,    APB_IOMUXC_SF_BASE},
    {    JTAG_TDO,   5,   0x3010U,  0,    APB_IOMUXC_SF_BASE},
    { JTAG_TRST_N,   5,   0x3014U,  0,    APB_IOMUXC_SF_BASE},
    {    JTAG_TMS,   5,   0x3018U,  0,    APB_IOMUXC_SF_BASE},
    {    JTAG_TCK,   5,   0x301cU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_X7,   3,   0x3020U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_X7,   4,   0x3024U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_X7,   5,   0x3028U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_X8,   3,   0x302cU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_X8,   4,   0x3030U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_X8,   5,   0x3034U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_X9,   3,   0x3038U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_X9,   4,   0x303cU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_X9,   5,   0x3040U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_X9,   6,   0x3044U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_X10,   3,   0x3048U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_X10,   4,   0x304cU,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_X10,   5,   0x3050U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_X10,   6,   0x3054U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_X11,   5,   0x3058U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y0,   2,   0x305cU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y0,   4,   0x3060U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y0,   5,   0x3064U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y0,   7,   0x3068U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y1,   3,   0x306cU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y1,   4,   0x3070U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y1,   5,   0x3074U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y1,   7,   0x3078U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y2,   2,   0x307cU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y2,   3,   0x3080U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y2,   4,   0x3084U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y2,   5,   0x3088U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y2,   6,   0x308cU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y2,   7,   0x3090U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y3,   3,   0x3094U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y3,   4,   0x3098U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y3,   6,   0x309cU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y3,   7,   0x30a0U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y4,   2,   0x30a4U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y4,   3,   0x30a8U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y4,   4,   0x30acU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y4,   7,   0x30b0U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y5,   3,   0x30b4U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y5,   4,   0x30b8U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y5,   5,   0x30bcU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y5,   7,   0x30c0U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y6,   3,   0x30c4U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y6,   4,   0x30c8U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y6,   5,   0x30ccU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y6,   6,   0x30d0U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y6,   7,   0x30d4U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y7,   3,   0x30d8U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y7,   4,   0x30dcU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y7,   5,   0x30e0U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y7,   6,   0x30e4U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y7,   7,   0x30e8U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y8,   2,   0x30ecU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y8,   3,   0x30f0U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y8,   4,   0x30f4U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y8,   5,   0x30f8U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y8,   6,   0x30fcU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y8,   7,   0x3100U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y9,   3,   0x3104U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y9,   4,   0x3108U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y9,   5,   0x310cU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y9,   6,   0x3110U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y9,   7,   0x3114U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_Y10,   3,   0x3118U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_Y10,   4,   0x311cU,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_Y10,   5,   0x3120U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_Y11,   3,   0x3124U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_Y11,   4,   0x3128U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_Y11,   5,   0x312cU,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_Y11,   7,   0x3130U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A0,   2,   0x305cU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_A0,   3,   0x3134U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A0,   5,   0x3138U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A0,   6,   0x313cU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A1,   3,   0x3140U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A1,   5,   0x3144U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A1,   6,   0x3148U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A2,   1,   0x314cU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A2,   2,   0x3150U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A2,   3,   0x3154U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A2,   5,   0x3158U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A3,   1,   0x315cU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A3,   3,   0x3160U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A3,   5,   0x3164U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A4,   2,   0x3168U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A4,   3,   0x316cU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A4,   5,   0x3170U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A4,   6,   0x3068U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_A5,   3,   0x3174U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A5,   5,   0x3178U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A5,   6,   0x3078U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_A6,   2,   0x317cU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A6,   3,   0x3180U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A6,   5,   0x3184U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A6,   6,   0x3090U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_A7,   3,   0x3188U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A7,   5,   0x318cU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A7,   6,   0x30a0U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_A8,   2,   0x3190U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A8,   3,   0x3194U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A8,   4,   0x3198U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A8,   5,   0x319cU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A8,   6,   0x30b0U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_A9,   3,   0x31a0U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A9,   4,   0x31a4U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A9,   5,   0x31a8U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A9,   6,   0x30c0U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_A10,   2,   0x31acU,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_A10,   3,   0x31b0U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_A10,   4,   0x31b4U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_A10,   6,   0x30d4U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_A11,   3,   0x31b8U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_A11,   4,   0x31bcU,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_A11,   6,   0x30e8U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_A12,   2,   0x31c0U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_A12,   3,   0x31c4U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_A12,   4,   0x31c8U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_A12,   6,   0x3100U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_A13,   3,   0x31ccU,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_A13,   4,   0x31d0U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_A13,   6,   0x3114U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_A14,   2,   0x31d4U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_A14,   4,   0x311cU,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_A15,   4,   0x3128U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_A15,   6,   0x3130U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_B0,   1,   0x307cU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_B0,   3,   0x31d8U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_B0,   4,   0x30f4U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_B0,   5,   0x30bcU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_B1,   3,   0x31dcU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_B1,   4,   0x3108U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_B1,   5,   0x30ccU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_B2,   3,   0x31e0U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_B2,   4,   0x30c8U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_B2,   5,   0x30e0U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_B3,   3,   0x31e4U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_B3,   4,   0x30dcU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_B3,   5,   0x30f8U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_B4,   2,   0x31acU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_B4,   3,   0x31e8U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_B4,   4,   0x30acU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_B4,   5,   0x310cU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_B4,   6,   0x3044U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_B5,   3,   0x31ecU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_B5,   4,   0x30b8U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_B5,   5,   0x3120U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_B5,   6,   0x3054U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_B6,   2,   0x3190U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_B6,   3,   0x30f0U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_B6,   4,   0x3084U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_B6,   5,   0x312cU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_B7,   3,   0x3104U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_B7,   4,   0x3098U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_B7,   5,   0x31f0U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_B7,   7,   0x31f4U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_B8,   2,   0x317cU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_B8,   3,   0x3118U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_B8,   4,   0x3060U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_B8,   5,   0x31f8U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_B8,   6,   0x308cU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_B8,   7,   0x31fcU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_B9,   3,   0x3124U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_B9,   4,   0x3070U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_B9,   5,   0x3200U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_B9,   6,   0x309cU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_B9,   7,   0x3204U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_B10,   2,   0x3168U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_B10,   4,   0x303cU,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_B11,   3,   0x3208U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_B11,   4,   0x304cU,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_B12,   2,   0x3150U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_B12,   3,   0x3038U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_B12,   4,   0x3024U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_B12,   6,   0x30d0U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_B13,   3,   0x3048U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_B13,   4,   0x3030U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_B13,   6,   0x30e4U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_B14,   2,   0x305cU,  2,    APB_IOMUXC_SF_BASE},
    {    GPIO_B14,   3,   0x3020U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_B14,   4,   0x3004U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_B15,   3,   0x302cU,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_B15,   4,   0x300cU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_C0,   2,   0x31c0U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_C0,   5,   0x3008U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_C1,   1,   0x3000U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_C1,   5,   0x3010U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_C2,   2,   0x31d4U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_C2,   3,   0x3080U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_C2,   5,   0x3014U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_C2,   6,   0x313cU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_C3,   3,   0x3094U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_C3,   5,   0x3018U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_C3,   6,   0x3148U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_C4,   3,   0x30a8U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_C4,   5,   0x301cU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_C5,   3,   0x30b4U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_C5,   5,   0x320cU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_C6,   1,   0x307cU,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_C6,   4,   0x3198U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_C6,   5,   0x3210U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_C6,   6,   0x314cU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_C7,   4,   0x31a4U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_C7,   5,   0x3214U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_C7,   6,   0x315cU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_G0,   2,   0x30ecU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_G0,   3,   0x30c4U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_G0,   4,   0x30c8U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_G0,   5,   0x3218U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_G0,   6,   0x3068U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_G0,   8,   0x321cU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_G1,   2,   0x3044U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_G1,   3,   0x30d8U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_G1,   4,   0x30dcU,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_G1,   5,   0x3220U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_G1,   6,   0x3078U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_G2,   2,   0x3054U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_G2,   3,   0x3080U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_G2,   4,   0x30acU,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_G2,   5,   0x3224U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_G2,   6,   0x3090U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_G3,   2,   0x307cU,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_G3,   3,   0x3094U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_G3,   4,   0x30b8U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_G3,   5,   0x3228U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_G3,   6,   0x30a0U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_G3,   7,   0x305cU,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_G4,   3,   0x30a8U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_G4,   4,   0x3084U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_G4,   5,   0x322cU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_G4,   6,   0x30b0U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_G5,   2,   0x308cU,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_G5,   3,   0x30b4U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_G5,   4,   0x3098U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_G5,   5,   0x3230U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_G5,   6,   0x30c0U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_G5,   7,   0x31f4U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_G6,   2,   0x30a4U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_G6,   3,   0x306cU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_G6,   4,   0x3060U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_G6,   5,   0x3234U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_G6,   6,   0x30d4U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_G6,   7,   0x31fcU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_G7,   2,   0x309cU,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_G7,   4,   0x3070U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_G7,   5,   0x3238U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_G7,   6,   0x30e8U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_G7,   7,   0x3204U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_G8,   2,   0x30d0U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_G8,   3,   0x30f0U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_G8,   4,   0x303cU,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_G8,   5,   0x323cU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_G8,   6,   0x3100U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_G8,   7,   0x3150U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_G9,   3,   0x3104U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_G9,   4,   0x304cU,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_G9,   5,   0x3240U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_G9,   6,   0x3114U,  2,    APB_IOMUXC_SF_BASE},
    {    GPIO_G10,   3,   0x3118U,  2,    APB_IOMUXC_SF_BASE},
    {    GPIO_G10,   4,   0x3024U,  2,    APB_IOMUXC_SF_BASE},
    {    GPIO_G10,   5,   0x3244U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_G10,   8,   0x321cU,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_G11,   2,   0x30e4U,  2,    APB_IOMUXC_SF_BASE},
    {    GPIO_G11,   3,   0x3124U,  2,    APB_IOMUXC_SF_BASE},
    {    GPIO_G11,   4,   0x3030U,  2,    APB_IOMUXC_SF_BASE},
    {    GPIO_G11,   5,   0x3248U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_G11,   6,   0x3130U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_S0,   1,   0x324cU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_S0,   2,   0x3250U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_S0,   3,   0x3254U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_S0,   4,   0x31b4U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_S0,   5,   0x30e0U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_S0,   6,   0x30fcU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_S1,   1,   0x3258U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_S1,   2,   0x325cU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_S1,   3,   0x3260U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_S1,   4,   0x31bcU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_S1,   5,   0x30f8U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_S1,   6,   0x3110U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_S2,   1,   0x3264U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_S2,   3,   0x3268U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_S2,   4,   0x31c8U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_S2,   5,   0x310cU,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_S3,   1,   0x326cU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_S3,   3,   0x3270U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_S3,   4,   0x31d0U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_S3,   5,   0x3120U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_S4,   1,   0x3274U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_S4,   3,   0x3278U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_S4,   4,   0x311cU,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_S4,   5,   0x312cU,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_S5,   1,   0x327cU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_S5,   3,   0x3280U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_S5,   4,   0x3128U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_S5,   5,   0x31f0U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_S6,   1,   0x3284U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_S6,   5,   0x31f8U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_S6,   6,   0x3044U,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_S6,   7,   0x3150U,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_S7,   1,   0x3288U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_S7,   2,   0x328cU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_S7,   5,   0x3200U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_S7,   6,   0x3054U,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_H0,   1,   0x324cU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_H0,   2,   0x3168U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_H0,   3,   0x31b0U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_H0,   5,   0x3218U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_H0,   6,   0x3198U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_H1,   1,   0x3258U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_H1,   3,   0x31b8U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_H1,   5,   0x3220U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_H1,   6,   0x31a4U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_H1,   8,   0x3290U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_H1,   9,   0x3290U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_H2,   1,   0x3264U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_H2,   2,   0x317cU,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_H2,   3,   0x31c4U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_H2,   5,   0x3224U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_H2,   6,   0x31b4U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_H2,   8,   0x3294U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_H2,   9,   0x3294U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_H3,   1,   0x326cU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_H3,   3,   0x31ccU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_H3,   5,   0x3228U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_H3,   6,   0x31bcU,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_H3,   8,   0x3298U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_H3,   9,   0x3298U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_H4,   1,   0x3274U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_H4,   2,   0x3190U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_H4,   3,   0x3188U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_H4,   5,   0x322cU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_H4,   6,   0x31c8U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_H4,   8,   0x329cU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_H4,   9,   0x329cU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_H5,   1,   0x327cU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_H5,   3,   0x3194U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_H5,   5,   0x3230U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_H5,   6,   0x31d0U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_H5,   8,   0x32a0U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_H5,   9,   0x32a0U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_H6,   1,   0x3284U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_H6,   2,   0x31acU,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_H6,   3,   0x31a0U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_H6,   4,   0x3280U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_H6,   5,   0x3234U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_H6,   6,   0x311cU,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_H6,   8,   0x32a4U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_H6,   9,   0x32a4U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_H7,   1,   0x3288U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_H7,   3,   0x3020U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_H7,   4,   0x3278U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_H7,   5,   0x3238U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_H7,   6,   0x3128U,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_H7,   8,   0x32a8U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_H7,   9,   0x32a8U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_H8,   3,   0x3038U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_H8,   4,   0x3270U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_H8,   5,   0x323cU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_H8,   6,   0x3150U,  4,    APB_IOMUXC_SF_BASE},
    {     GPIO_H8,   8,   0x32acU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_H8,   9,   0x32acU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_H9,   3,   0x3048U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_H9,   4,   0x3268U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_H9,   5,   0x3240U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_H9,   8,   0x32b0U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_H9,   9,   0x32b0U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_H10,   1,   0x3250U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_H10,   3,   0x302cU,  2,    APB_IOMUXC_SF_BASE},
    {    GPIO_H10,   4,   0x3260U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_H10,   5,   0x3244U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_H10,   6,   0x314cU,  2,    APB_IOMUXC_SF_BASE},
    {    GPIO_H10,   8,   0x32b4U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_H10,   9,   0x32b4U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_H11,   1,   0x325cU,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_H11,   3,   0x3208U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_H11,   4,   0x3254U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_H11,   5,   0x3248U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_H11,   6,   0x315cU,  2,    APB_IOMUXC_SF_BASE},
    {    GPIO_H11,   8,   0x32b8U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_H11,   9,   0x32b8U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_H12,   3,   0x32bcU,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_H12,   5,   0x3028U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_H12,   6,   0x32c0U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_H12,   7,   0x30fcU,  2,    APB_IOMUXC_SF_BASE},
    {    GPIO_H12,   8,   0x32c4U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_H12,   9,   0x32c4U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_H13,   1,   0x328cU,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_H13,   5,   0x3034U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_H13,   6,   0x32c8U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_H13,   7,   0x3110U,  2,    APB_IOMUXC_SF_BASE},
    {    GPIO_H13,   8,   0x32ccU,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_H13,   9,   0x32ccU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_F0,   1,   0x31c0U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_F0,   3,   0x32bcU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_F0,   5,   0x3040U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_F0,   6,   0x3198U,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_F0,   7,   0x30d0U,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_F1,   3,   0x3208U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_F1,   5,   0x3050U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_F1,   6,   0x31a4U,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_F1,   7,   0x30e4U,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_F2,   1,   0x305cU,  4,    APB_IOMUXC_SF_BASE},
    {     GPIO_F2,   3,   0x3020U,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_F2,   5,   0x3058U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_F2,   6,   0x31b4U,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_F3,   3,   0x3038U,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_F3,   5,   0x3064U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_F3,   6,   0x31bcU,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_F4,   3,   0x3048U,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_F4,   5,   0x3074U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_F4,   6,   0x31c8U,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_F5,   3,   0x302cU,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_F5,   5,   0x3088U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_F5,   6,   0x31d0U,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_L0,   2,   0x3134U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_L0,   3,   0x31d4U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_L0,   5,   0x32d0U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_L0,   6,   0x301cU,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_L0,   7,   0x311cU,  4,    APB_IOMUXC_SF_BASE},
    {     GPIO_L1,   2,   0x3140U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_L1,   4,   0x32d4U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_L1,   6,   0x320cU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_L1,   7,   0x3128U,  4,    APB_IOMUXC_SF_BASE},
    {     GPIO_L2,   2,   0x3154U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_L2,   3,   0x3150U,  5,    APB_IOMUXC_SF_BASE},
    {     GPIO_L2,   5,   0x32c0U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_L2,   6,   0x3210U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_L2,   7,   0x30f4U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_L3,   2,   0x3160U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_L3,   4,   0x32d8U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_L3,   5,   0x32c8U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_L3,   6,   0x3214U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_L3,   7,   0x3108U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_L4,   2,   0x316cU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_L4,   3,   0x3168U,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_L4,   5,   0x3044U,  4,    APB_IOMUXC_SF_BASE},
    {     GPIO_L4,   7,   0x30c8U,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_L5,   2,   0x3174U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_L5,   5,   0x3054U,  4,    APB_IOMUXC_SF_BASE},
    {     GPIO_L5,   7,   0x30dcU,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_L6,   2,   0x3180U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_L6,   3,   0x317cU,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_L6,   6,   0x3138U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_L6,   7,   0x30acU,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_L7,   6,   0x3144U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_L7,   7,   0x30b8U,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_L8,   2,   0x3190U,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_L8,   3,   0x32c0U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_L8,   5,   0x308cU,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_L8,   6,   0x3158U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_L8,   7,   0x3084U,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_L9,   3,   0x32c8U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_L9,   5,   0x309cU,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_L9,   6,   0x3164U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_L9,   7,   0x3098U,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_E0,   2,   0x32d0U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_E0,   3,   0x30fcU,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_E0,   4,   0x324cU,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_E0,   5,   0x3024U,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_E0,   6,   0x3170U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_E0,   7,   0x31d8U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_E1,   2,   0x32d8U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_E1,   3,   0x3110U,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_E1,   4,   0x3258U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_E1,   5,   0x3030U,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_E1,   6,   0x3178U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_E1,   7,   0x31dcU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_E2,   2,   0x32d4U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_E2,   3,   0x31acU,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_E2,   4,   0x3264U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_E2,   5,   0x303cU,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_E2,   6,   0x3184U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_E2,   7,   0x31e0U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_E3,   4,   0x326cU,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_E3,   5,   0x304cU,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_E3,   6,   0x318cU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_E3,   7,   0x31e4U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_E4,   2,   0x32c0U,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_E4,   3,   0x31c0U,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_E4,   4,   0x3274U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_E4,   5,   0x3060U,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_E4,   6,   0x319cU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_E4,   7,   0x31e8U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_E5,   2,   0x32c8U,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_E5,   4,   0x327cU,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_E5,   5,   0x3070U,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_E5,   6,   0x31a8U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_E5,   7,   0x31ecU,  1,    APB_IOMUXC_SF_BASE},
};

#else

const sdrv_input_select_t sdrv_pinctrl_is[] = {
    {    JTAG_TDI,   2,   0x3000U,  0,    APB_IOMUXC_SF_BASE},
    {    JTAG_TDI,   3,   0x3004U,  0,    APB_IOMUXC_SF_BASE},
    {    JTAG_TDI,   4,   0x3008U,  0,    APB_IOMUXC_SF_BASE},
    {    JTAG_TDI,   5,   0x300cU,  0,    APB_IOMUXC_SF_BASE},
    {    JTAG_TDI,   6,   0x3010U,  0,    APB_IOMUXC_SF_BASE},
    {    JTAG_TDO,   3,   0x3014U,  0,    APB_IOMUXC_SF_BASE},
    {    JTAG_TDO,   4,   0x3018U,  0,    APB_IOMUXC_SF_BASE},
    {    JTAG_TDO,   5,   0x301cU,  0,    APB_IOMUXC_SF_BASE},
    {    JTAG_TDO,   6,   0x3020U,  0,    APB_IOMUXC_SF_BASE},
    { JTAG_TRST_N,   3,   0x3024U,  0,    APB_IOMUXC_SF_BASE},
    { JTAG_TRST_N,   5,   0x3028U,  0,    APB_IOMUXC_SF_BASE},
    {    JTAG_TMS,   3,   0x302cU,  0,    APB_IOMUXC_SF_BASE},
    {    JTAG_TMS,   5,   0x3030U,  0,    APB_IOMUXC_SF_BASE},
    {    JTAG_TCK,   3,   0x3034U,  0,    APB_IOMUXC_SF_BASE},
    {    JTAG_TCK,   5,   0x3038U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_X7,   2,   0x303cU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_X7,   3,   0x3040U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_X7,   4,   0x3044U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_X7,   5,   0x3048U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_X8,   3,   0x304cU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_X8,   4,   0x3050U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_X8,   5,   0x3054U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_X9,   2,   0x3058U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_X9,   3,   0x305cU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_X9,   4,   0x3060U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_X9,   5,   0x3064U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_X9,   6,   0x3068U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_X10,   3,   0x306cU,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_X10,   4,   0x3070U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_X10,   5,   0x3074U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_X10,   6,   0x3078U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_X11,   5,   0x307cU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y0,   2,   0x3080U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y0,   4,   0x3084U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y0,   5,   0x3088U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y1,   3,   0x308cU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y1,   4,   0x3090U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y1,   5,   0x3094U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y1,   7,   0x3098U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y2,   2,   0x309cU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y2,   3,   0x30a0U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y2,   4,   0x30a4U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y2,   5,   0x30a8U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y2,   6,   0x30acU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y3,   3,   0x30b0U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y3,   4,   0x30b4U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y3,   6,   0x30b8U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y4,   2,   0x30bcU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y4,   3,   0x30c0U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y4,   4,   0x30c4U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y4,   6,   0x30c8U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y4,   7,   0x30ccU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y5,   3,   0x30d0U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y5,   4,   0x30d4U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y5,   5,   0x30d8U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y5,   6,   0x30dcU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y6,   3,   0x30e0U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y6,   4,   0x30e4U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y6,   5,   0x30e8U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y7,   3,   0x30ecU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y7,   4,   0x30f0U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y7,   5,   0x30f4U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y7,   6,   0x30f8U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y7,   7,   0x30fcU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y8,   2,   0x3100U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y8,   3,   0x3104U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y8,   4,   0x3108U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y8,   5,   0x310cU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y8,   6,   0x3110U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y9,   3,   0x3114U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y9,   4,   0x3118U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y9,   5,   0x311cU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_Y9,   6,   0x3120U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_Y10,   3,   0x3124U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_Y10,   4,   0x3128U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_Y10,   5,   0x312cU,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_Y10,   6,   0x3130U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_Y10,   7,   0x3134U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_Y11,   3,   0x3138U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_Y11,   4,   0x313cU,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_Y11,   5,   0x3140U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_Y11,   6,   0x3144U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A0,   2,   0x3148U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A0,   3,   0x314cU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A0,   4,   0x3150U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A0,   5,   0x3154U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A0,   6,   0x3158U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A1,   3,   0x315cU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A1,   4,   0x3160U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A1,   5,   0x3164U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A1,   6,   0x3168U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A2,   1,   0x316cU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A2,   2,   0x3170U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A2,   3,   0x3174U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A2,   4,   0x3178U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A2,   5,   0x317cU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A2,   7,   0x3180U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A3,   1,   0x3184U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A3,   3,   0x3188U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A3,   4,   0x318cU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A3,   5,   0x3190U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A3,   7,   0x3194U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A4,   1,   0x3198U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A4,   2,   0x319cU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A4,   3,   0x31a0U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A4,   4,   0x31a4U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A4,   5,   0x31a8U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A5,   1,   0x31acU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A5,   3,   0x31b0U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A5,   4,   0x31b4U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A5,   5,   0x31b8U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A5,   7,   0x3098U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_A6,   1,   0x31bcU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A6,   2,   0x31c0U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A6,   3,   0x31c4U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A6,   4,   0x31c8U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A6,   5,   0x31ccU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A7,   1,   0x31d0U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A7,   4,   0x31d4U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A7,   5,   0x31d8U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A8,   1,   0x31dcU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A8,   2,   0x31e0U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A8,   4,   0x31e4U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A8,   5,   0x31e8U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A8,   7,   0x30ccU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_A9,   1,   0x31ecU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A9,   3,   0x31f0U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A9,   4,   0x31f4U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_A9,   5,   0x31f8U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_A10,   1,   0x31fcU,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_A10,   2,   0x3200U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_A10,   3,   0x3204U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_A10,   4,   0x3208U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_A10,   5,   0x320cU,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_A11,   1,   0x3210U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_A11,   3,   0x3214U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_A11,   4,   0x3218U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_A11,   5,   0x321cU,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_A12,   1,   0x3220U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_A12,   2,   0x3224U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_A12,   3,   0x3228U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_A12,   4,   0x322cU,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_A12,   5,   0x3230U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_A13,   1,   0x3234U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_A13,   3,   0x3238U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_A13,   4,   0x323cU,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_A13,   5,   0x3240U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_A14,   1,   0x3244U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_A14,   2,   0x3248U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_A14,   4,   0x3128U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_A14,   5,   0x324cU,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_A15,   4,   0x313cU,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_A15,   5,   0x3250U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_B0,   1,   0x309cU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_B0,   2,   0x3254U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_B0,   4,   0x3108U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_B0,   5,   0x30d8U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_B1,   4,   0x3118U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_B1,   5,   0x30e8U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_B1,   7,   0x3000U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_B2,   2,   0x3258U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_B2,   4,   0x30e4U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_B2,   5,   0x30f4U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_B2,   7,   0x3004U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_B3,   4,   0x30f0U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_B3,   5,   0x310cU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_B3,   7,   0x3008U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_B4,   2,   0x325cU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_B4,   4,   0x30c4U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_B4,   5,   0x311cU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_B4,   6,   0x3068U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_B4,   7,   0x300cU,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_B5,   1,   0x3260U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_B5,   4,   0x30d4U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_B5,   5,   0x312cU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_B5,   6,   0x3078U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_B5,   7,   0x3010U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_B6,   1,   0x3264U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_B6,   2,   0x3268U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_B6,   3,   0x326cU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_B6,   4,   0x30a4U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_B6,   5,   0x3140U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_B6,   7,   0x3014U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_B7,   1,   0x3270U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_B7,   3,   0x3274U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_B7,   4,   0x30b4U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_B7,   5,   0x3278U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_B7,   7,   0x3018U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_B8,   1,   0x327cU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_B8,   2,   0x3280U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_B8,   3,   0x3284U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_B8,   4,   0x3084U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_B8,   5,   0x3288U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_B8,   7,   0x301cU,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_B9,   1,   0x328cU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_B9,   3,   0x3290U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_B9,   4,   0x3090U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_B9,   5,   0x3294U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_B9,   7,   0x3020U,  0,    APB_IOMUXC_AP_BASE},
    {    GPIO_B10,   1,   0x3298U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_B10,   2,   0x303cU,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_B10,   4,   0x3060U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_B10,   5,   0x329cU,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_B10,   7,   0x3024U,  0,    APB_IOMUXC_AP_BASE},
    {    GPIO_B11,   1,   0x32a0U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_B11,   3,   0x32a4U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_B11,   4,   0x3070U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_B11,   5,   0x32a8U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_B11,   7,   0x3028U,  0,    APB_IOMUXC_AP_BASE},
    {    GPIO_B12,   1,   0x32acU,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_B12,   2,   0x3058U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_B12,   3,   0x305cU,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_B12,   4,   0x3044U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_B12,   5,   0x32b0U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_B12,   7,   0x302cU,  0,    APB_IOMUXC_AP_BASE},
    {    GPIO_B13,   1,   0x32b4U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_B13,   3,   0x306cU,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_B13,   4,   0x3050U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_B13,   5,   0x32b8U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_B13,   7,   0x3030U,  0,    APB_IOMUXC_AP_BASE},
    {    GPIO_B14,   1,   0x32bcU,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_B14,   2,   0x3080U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_B14,   3,   0x3040U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_B14,   4,   0x3008U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_B14,   5,   0x32c0U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_B14,   7,   0x3034U,  0,    APB_IOMUXC_AP_BASE},
    {    GPIO_B15,   1,   0x32c4U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_B15,   3,   0x304cU,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_B15,   4,   0x3018U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_B15,   5,   0x32c8U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_B15,   7,   0x3038U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_C0,   2,   0x32ccU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_C0,   3,   0x3034U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_C0,   4,   0x3178U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_C0,   5,   0x300cU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_C0,   7,   0x303cU,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_C1,   1,   0x3000U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_C1,   4,   0x318cU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_C1,   5,   0x301cU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_C1,   7,   0x3040U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_C2,   1,   0x32d0U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_C2,   2,   0x32d4U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_C2,   3,   0x3004U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_C2,   4,   0x31a4U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_C2,   5,   0x3028U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_C2,   6,   0x3158U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_C2,   7,   0x3044U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_C3,   3,   0x3014U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_C3,   4,   0x31b4U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_C3,   5,   0x3030U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_C3,   6,   0x3168U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_C3,   7,   0x3048U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_C4,   2,   0x32d8U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_C4,   3,   0x3024U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_C4,   4,   0x31c8U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_C4,   5,   0x3038U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_C4,   7,   0x304cU,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_C5,   3,   0x302cU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_C5,   4,   0x31d4U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_C5,   7,   0x3050U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_C6,   1,   0x309cU,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_C6,   2,   0x32dcU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_C6,   4,   0x31e4U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_C6,   6,   0x316cU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_C6,   7,   0x3054U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_C7,   4,   0x31f4U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_C7,   6,   0x3184U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_C7,   7,   0x3058U,  0,    APB_IOMUXC_AP_BASE},

#ifdef GPIO_C8
    {     GPIO_C8,   2,   0x32e0U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_C8,   3,   0x32e4U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_C8,   7,   0x305cU,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_C9,   3,   0x326cU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_C9,   7,   0x3060U,  0,    APB_IOMUXC_AP_BASE},
    {    GPIO_C10,   2,   0x32e8U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_C10,   3,   0x3274U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_C10,   5,   0x3154U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_C10,   7,   0x3064U,  0,    APB_IOMUXC_AP_BASE},
    {    GPIO_C11,   1,   0x32ecU,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_C11,   3,   0x3284U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_C11,   5,   0x3164U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_C11,   6,   0x30acU,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_C11,   7,   0x3068U,  0,    APB_IOMUXC_AP_BASE},
    {    GPIO_C12,   2,   0x32f0U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_C12,   3,   0x3290U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_C12,   5,   0x317cU,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_C12,   6,   0x30b8U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_C12,   7,   0x306cU,  0,    APB_IOMUXC_AP_BASE},
    {    GPIO_C13,   5,   0x3190U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_C14,   2,   0x32f4U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_C14,   5,   0x31a8U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_C15,   1,   0x32f8U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_C15,   5,   0x31b8U,  1,    APB_IOMUXC_SF_BASE},
#endif /* GPIO_C8 */

    {     GPIO_G0,   2,   0x3100U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_G0,   4,   0x30e4U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_G0,   5,   0x32fcU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_G0,   7,   0x3130U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_G1,   4,   0x30f0U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_G1,   5,   0x3300U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_G1,   6,   0x3098U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_G1,   7,   0x3144U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_G2,   3,   0x30a0U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_G2,   4,   0x30c4U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_G2,   5,   0x3304U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_G3,   2,   0x309cU,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_G3,   3,   0x30b0U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_G3,   4,   0x30d4U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_G3,   5,   0x3308U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_G3,   7,   0x3148U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_G4,   3,   0x30c0U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_G4,   4,   0x30a4U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_G4,   5,   0x330cU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_G4,   6,   0x30ccU,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_G5,   3,   0x30d0U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_G5,   4,   0x30b4U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_G5,   5,   0x3310U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_G6,   2,   0x30bcU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_G6,   3,   0x308cU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_G6,   4,   0x3084U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_G6,   5,   0x3314U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_G7,   3,   0x32e4U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_G7,   4,   0x3090U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_G7,   5,   0x3318U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_G7,   6,   0x30fcU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_G8,   3,   0x326cU,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_G8,   4,   0x3060U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_G8,   5,   0x331cU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_G8,   7,   0x3170U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_G9,   3,   0x3274U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_G9,   4,   0x3070U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_G9,   5,   0x3320U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_G10,   3,   0x3284U,  2,    APB_IOMUXC_SF_BASE},
    {    GPIO_G10,   4,   0x3044U,  2,    APB_IOMUXC_SF_BASE},
    {    GPIO_G10,   5,   0x3324U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_G10,   6,   0x3134U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_G11,   3,   0x3290U,  2,    APB_IOMUXC_SF_BASE},
    {    GPIO_G11,   4,   0x3050U,  2,    APB_IOMUXC_SF_BASE},
    {    GPIO_G11,   5,   0x3328U,  0,    APB_IOMUXC_SF_BASE},

#ifdef GPIO_M0
    {     GPIO_M0,   2,   0x32f8U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_M0,   3,   0x314cU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_M0,   4,   0x3150U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_M0,   5,   0x31ccU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_M0,   6,   0x30f8U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_M1,   3,   0x315cU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_M1,   4,   0x3160U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_M1,   5,   0x31d8U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_M2,   3,   0x3174U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_M2,   4,   0x3178U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_M2,   5,   0x31e8U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_M3,   2,   0x32d0U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_M3,   3,   0x3188U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_M3,   4,   0x318cU,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_M3,   5,   0x31f8U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_M4,   3,   0x31a0U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_M4,   4,   0x31a4U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_M4,   5,   0x320cU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_M4,   6,   0x3110U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_M5,   3,   0x31b0U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_M5,   4,   0x31b4U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_M5,   5,   0x321cU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_M5,   6,   0x3120U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_M6,   2,   0x32ecU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_M6,   3,   0x31c4U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_M6,   4,   0x31c8U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_M6,   5,   0x3230U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_M6,   6,   0x3130U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_M7,   1,   0x332cU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_M7,   3,   0x3204U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_M7,   4,   0x31d4U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_M7,   5,   0x3240U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_M7,   6,   0x3144U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_M8,   1,   0x3330U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_M8,   3,   0x3214U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_M8,   5,   0x324cU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_M9,   2,   0x3334U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_M9,   3,   0x3228U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_M9,   5,   0x3250U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_M10,   2,   0x3338U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_M10,   3,   0x3238U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_M10,   4,   0x31e4U,  2,    APB_IOMUXC_SF_BASE},
    {    GPIO_M10,   5,   0x30d8U,  2,    APB_IOMUXC_SF_BASE},
    {    GPIO_M10,   6,   0x3010U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_M11,   1,   0x333cU,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_M11,   3,   0x31f0U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_M11,   4,   0x31f4U,  2,    APB_IOMUXC_SF_BASE},
    {    GPIO_M11,   5,   0x30e8U,  2,    APB_IOMUXC_SF_BASE},
    {    GPIO_M11,   6,   0x3020U,  1,    APB_IOMUXC_SF_BASE},
#endif /* GPIO_M0 */

    {     GPIO_S0,   1,   0x3070U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_S0,   2,   0x3074U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_S0,   3,   0x3078U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_S0,   4,   0x3208U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_S0,   5,   0x30f4U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_S0,   7,   0x307cU,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_S1,   1,   0x3080U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_S1,   2,   0x3084U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_S1,   3,   0x3088U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_S1,   4,   0x3218U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_S1,   5,   0x310cU,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_S1,   6,   0x3098U,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_S1,   7,   0x308cU,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_S2,   1,   0x3090U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_S2,   3,   0x3094U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_S2,   4,   0x322cU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_S2,   5,   0x311cU,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_S2,   7,   0x3098U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_S3,   1,   0x309cU,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_S3,   3,   0x30a0U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_S3,   4,   0x323cU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_S3,   5,   0x312cU,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_S3,   7,   0x30a4U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_S4,   1,   0x30a8U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_S4,   3,   0x30acU,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_S4,   4,   0x3128U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_S4,   5,   0x3140U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_S4,   6,   0x30ccU,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_S4,   7,   0x30b0U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_S5,   1,   0x30b4U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_S5,   3,   0x30b8U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_S5,   4,   0x313cU,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_S5,   5,   0x3278U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_S5,   7,   0x30bcU,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_S6,   1,   0x30c0U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_S6,   4,   0x30c4U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_S6,   5,   0x3288U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_S6,   7,   0x32e0U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_S7,   1,   0x30c8U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_S7,   2,   0x30ccU,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_S7,   5,   0x3294U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_S7,   6,   0x30fcU,  2,    APB_IOMUXC_SF_BASE},

#ifdef GPIO_S8
    {     GPIO_S8,   1,   0x30d0U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_S8,   2,   0x30c8U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_S8,   3,   0x30e0U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_S8,   4,   0x30c4U,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_S8,   5,   0x329cU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_S8,   7,   0x32e8U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_S9,   1,   0x30d4U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_S9,   2,   0x30dcU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_S9,   3,   0x3138U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_S9,   4,   0x30d4U,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_S9,   5,   0x32a8U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_S10,   1,   0x3074U,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_S10,   2,   0x30d8U,  0,    APB_IOMUXC_AP_BASE},
    {    GPIO_S10,   3,   0x3114U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_S10,   4,   0x30e4U,  3,    APB_IOMUXC_SF_BASE},
    {    GPIO_S10,   5,   0x32b0U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_S10,   6,   0x3134U,  2,    APB_IOMUXC_SF_BASE},
    {    GPIO_S10,   7,   0x32f0U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_S11,   1,   0x3084U,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_S11,   3,   0x3124U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_S11,   4,   0x30f0U,  3,    APB_IOMUXC_SF_BASE},
    {    GPIO_S11,   5,   0x32b8U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_S12,   2,   0x30dcU,  0,    APB_IOMUXC_AP_BASE},
    {    GPIO_S12,   3,   0x30ecU,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_S12,   4,   0x3108U,  2,    APB_IOMUXC_SF_BASE},
    {    GPIO_S12,   5,   0x32c0U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_S12,   7,   0x32f4U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_S13,   1,   0x30ccU,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_S13,   3,   0x3104U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_S13,   4,   0x3118U,  2,    APB_IOMUXC_SF_BASE},
    {    GPIO_S13,   5,   0x32c8U,  1,    APB_IOMUXC_SF_BASE},
#endif /* GPIO_S8 */

    {     GPIO_H0,   1,   0x30e0U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_H0,   2,   0x30e4U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_H0,   3,   0x3010U,  1,    APB_IOMUXC_AP_BASE},
    {     GPIO_H0,   5,   0x32fcU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_H0,   6,   0x31e4U,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_H0,   7,   0x30e8U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_H1,   1,   0x30ecU,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_H1,   2,   0x30f0U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_H1,   3,   0x3014U,  1,    APB_IOMUXC_AP_BASE},
    {     GPIO_H1,   5,   0x3300U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_H1,   6,   0x31f4U,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_H1,   7,   0x30f4U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_H1,   8,   0x3340U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_H1,   9,   0x3340U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_H2,   1,   0x30f8U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_H2,   3,   0x3018U,  1,    APB_IOMUXC_AP_BASE},
    {     GPIO_H2,   5,   0x3304U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_H2,   6,   0x3208U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_H2,   7,   0x30fcU,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_H2,   8,   0x3344U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_H2,   9,   0x3344U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_H3,   1,   0x3100U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_H3,   3,   0x301cU,  1,    APB_IOMUXC_AP_BASE},
    {     GPIO_H3,   5,   0x3308U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_H3,   6,   0x3218U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_H3,   7,   0x3104U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_H3,   8,   0x3348U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_H3,   9,   0x3348U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_H4,   1,   0x3108U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_H4,   3,   0x3020U,  1,    APB_IOMUXC_AP_BASE},
    {     GPIO_H4,   5,   0x330cU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_H4,   6,   0x322cU,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_H4,   7,   0x310cU,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_H4,   8,   0x334cU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_H4,   9,   0x334cU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_H5,   1,   0x3110U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_H5,   2,   0x3350U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_H5,   3,   0x3024U,  1,    APB_IOMUXC_AP_BASE},
    {     GPIO_H5,   5,   0x3310U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_H5,   6,   0x323cU,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_H5,   7,   0x3114U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_H5,   8,   0x3354U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_H5,   9,   0x3354U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_H6,   1,   0x3118U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_H6,   3,   0x3028U,  1,    APB_IOMUXC_AP_BASE},
    {     GPIO_H6,   4,   0x30b8U,  1,    APB_IOMUXC_AP_BASE},
    {     GPIO_H6,   5,   0x3314U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_H6,   6,   0x3128U,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_H6,   7,   0x311cU,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_H6,   8,   0x3358U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_H6,   9,   0x3358U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_H7,   1,   0x3120U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_H7,   2,   0x3124U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_H7,   3,   0x302cU,  1,    APB_IOMUXC_AP_BASE},
    {     GPIO_H7,   4,   0x30acU,  1,    APB_IOMUXC_AP_BASE},
    {     GPIO_H7,   5,   0x3318U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_H7,   6,   0x313cU,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_H7,   7,   0x3128U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_H7,   8,   0x335cU,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_H7,   9,   0x335cU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_H8,   1,   0x312cU,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_H8,   2,   0x3130U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_H8,   3,   0x3004U,  1,    APB_IOMUXC_AP_BASE},
    {     GPIO_H8,   4,   0x30a0U,  1,    APB_IOMUXC_AP_BASE},
    {     GPIO_H8,   5,   0x331cU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_H8,   6,   0x32e0U,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_H8,   7,   0x3134U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_H8,   8,   0x3360U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_H8,   9,   0x3360U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_H9,   1,   0x3138U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_H9,   3,   0x3008U,  1,    APB_IOMUXC_AP_BASE},
    {     GPIO_H9,   4,   0x3094U,  1,    APB_IOMUXC_AP_BASE},
    {     GPIO_H9,   5,   0x3320U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_H9,   7,   0x313cU,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_H9,   8,   0x3364U,  0,    APB_IOMUXC_SF_BASE},
    {     GPIO_H9,   9,   0x3364U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_H10,   1,   0x30e4U,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_H10,   2,   0x30d8U,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_H10,   3,   0x300cU,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_H10,   4,   0x3088U,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_H10,   5,   0x3324U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_H10,   6,   0x32e8U,  2,    APB_IOMUXC_SF_BASE},
    {    GPIO_H10,   7,   0x3140U,  0,    APB_IOMUXC_AP_BASE},
    {    GPIO_H10,   8,   0x3368U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_H10,   9,   0x3368U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_H11,   1,   0x30f0U,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_H11,   3,   0x3000U,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_H11,   4,   0x3078U,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_H11,   5,   0x3328U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_H11,   7,   0x3144U,  0,    APB_IOMUXC_AP_BASE},
    {    GPIO_H11,   8,   0x336cU,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_H11,   9,   0x336cU,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_H12,   2,   0x30dcU,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_H12,   5,   0x3048U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_H12,   6,   0x3148U,  0,    APB_IOMUXC_AP_BASE},
    {    GPIO_H12,   7,   0x307cU,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_H12,   8,   0x3370U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_H12,   9,   0x3370U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_H13,   1,   0x3124U,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_H13,   5,   0x3054U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_H13,   6,   0x314cU,  0,    APB_IOMUXC_AP_BASE},
    {    GPIO_H13,   7,   0x308cU,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_H13,   8,   0x3374U,  0,    APB_IOMUXC_SF_BASE},
    {    GPIO_H13,   9,   0x3374U,  1,    APB_IOMUXC_SF_BASE},

#ifdef GPIO_F0
    {     GPIO_F0,   1,   0x3150U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_F0,   5,   0x3064U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_F1,   1,   0x3154U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_F1,   3,   0x32a4U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_F1,   5,   0x3074U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_F2,   1,   0x3158U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_F2,   3,   0x3040U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_F2,   5,   0x307cU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_F3,   1,   0x315cU,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_F3,   3,   0x305cU,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_F3,   5,   0x3088U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_F4,   1,   0x3160U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_F4,   3,   0x306cU,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_F4,   5,   0x3094U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_F5,   1,   0x3164U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_F5,   3,   0x304cU,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_F5,   5,   0x30a8U,  1,    APB_IOMUXC_SF_BASE},
#endif /* GPIO_F0 */

    {     GPIO_L0,   2,   0x32d0U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_L0,   3,   0x32f0U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_L0,   5,   0x3350U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_L0,   7,   0x3168U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_L1,   4,   0x316cU,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_L1,   7,   0x3170U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_L2,   2,   0x3334U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_L2,   3,   0x32f4U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_L2,   5,   0x3148U,  1,    APB_IOMUXC_AP_BASE},
    {     GPIO_L2,   7,   0x3174U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_L3,   4,   0x3178U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_L3,   5,   0x314cU,  1,    APB_IOMUXC_AP_BASE},
    {     GPIO_L3,   7,   0x317cU,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_L4,   3,   0x30c4U,  1,    APB_IOMUXC_AP_BASE},
    {     GPIO_L4,   4,   0x3150U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_L4,   7,   0x3180U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_L5,   3,   0x3184U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_L5,   4,   0x3160U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_L5,   7,   0x3188U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_L6,   2,   0x3338U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_L6,   4,   0x3178U,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_L6,   7,   0x318cU,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_L7,   2,   0x332cU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_L7,   4,   0x318cU,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_L7,   7,   0x3190U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_L8,   2,   0x3330U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_L8,   3,   0x3148U,  2,    APB_IOMUXC_AP_BASE},
    {     GPIO_L8,   4,   0x31a4U,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_L8,   7,   0x3194U,  0,    APB_IOMUXC_AP_BASE},
    {     GPIO_L9,   2,   0x333cU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_L9,   3,   0x314cU,  2,    APB_IOMUXC_AP_BASE},
    {     GPIO_L9,   4,   0x31b4U,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_L9,   7,   0x3198U,  0,    APB_IOMUXC_AP_BASE},

#ifdef GPIO_D0
    {     GPIO_D0,   2,   0x3148U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_D0,   5,   0x3198U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_D1,   4,   0x3000U,  2,    APB_IOMUXC_AP_BASE},
    {     GPIO_D1,   5,   0x31acU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_D2,   2,   0x3170U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_D2,   4,   0x3004U,  2,    APB_IOMUXC_AP_BASE},
    {     GPIO_D2,   5,   0x31bcU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_D3,   4,   0x3008U,  2,    APB_IOMUXC_AP_BASE},
    {     GPIO_D3,   5,   0x31d0U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_D4,   2,   0x319cU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_D4,   4,   0x300cU,  2,    APB_IOMUXC_AP_BASE},
    {     GPIO_D4,   5,   0x31dcU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_D5,   4,   0x3010U,  2,    APB_IOMUXC_AP_BASE},
    {     GPIO_D5,   5,   0x31ecU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_D6,   2,   0x31c0U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_D6,   4,   0x3014U,  2,    APB_IOMUXC_AP_BASE},
    {     GPIO_D6,   5,   0x31fcU,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_D7,   4,   0x3018U,  2,    APB_IOMUXC_AP_BASE},
    {     GPIO_D7,   5,   0x3210U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_D8,   2,   0x31e0U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_D8,   4,   0x301cU,  2,    APB_IOMUXC_AP_BASE},
    {     GPIO_D8,   5,   0x3220U,  1,    APB_IOMUXC_SF_BASE},
    {     GPIO_D9,   4,   0x3020U,  2,    APB_IOMUXC_AP_BASE},
    {     GPIO_D9,   5,   0x3234U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_D10,   2,   0x3200U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_D10,   4,   0x3024U,  2,    APB_IOMUXC_AP_BASE},
    {    GPIO_D10,   5,   0x3244U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_D11,   4,   0x3028U,  2,    APB_IOMUXC_AP_BASE},
    {    GPIO_D11,   5,   0x3260U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_D12,   2,   0x3224U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_D12,   4,   0x302cU,  2,    APB_IOMUXC_AP_BASE},
    {    GPIO_D12,   5,   0x3264U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_D13,   4,   0x3030U,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_D13,   5,   0x3270U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_D13,   6,   0x3148U,  3,    APB_IOMUXC_AP_BASE},
    {    GPIO_D14,   2,   0x3248U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_D14,   4,   0x3034U,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_D14,   5,   0x327cU,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_D14,   6,   0x314cU,  3,    APB_IOMUXC_AP_BASE},
    {    GPIO_D14,   7,   0x308cU,  2,    APB_IOMUXC_AP_BASE},
    {    GPIO_D15,   4,   0x3038U,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_D15,   5,   0x328cU,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_D15,   7,   0x3098U,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_D16,   2,   0x3254U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_D16,   4,   0x303cU,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_D16,   5,   0x3298U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_D16,   7,   0x30a4U,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_D17,   4,   0x3040U,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_D17,   5,   0x32a0U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_D17,   7,   0x30b0U,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_D18,   2,   0x3258U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_D18,   4,   0x3044U,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_D18,   5,   0x32acU,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_D18,   7,   0x30bcU,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_D19,   4,   0x3048U,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_D19,   5,   0x32b4U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_D19,   7,   0x30e8U,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_D20,   2,   0x325cU,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_D20,   4,   0x304cU,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_D20,   5,   0x32bcU,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_D20,   7,   0x307cU,  2,    APB_IOMUXC_AP_BASE},
    {    GPIO_D21,   4,   0x3050U,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_D21,   5,   0x32c4U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_D21,   7,   0x30f4U,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_D22,   2,   0x3268U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_D22,   4,   0x3054U,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_D22,   7,   0x30fcU,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_D23,   4,   0x3058U,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_D23,   7,   0x3104U,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_D24,   2,   0x3280U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_D24,   4,   0x305cU,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_D24,   5,   0x3180U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_D24,   7,   0x310cU,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_D25,   4,   0x3060U,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_D25,   5,   0x3194U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_D25,   7,   0x3114U,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_D26,   2,   0x303cU,  2,    APB_IOMUXC_SF_BASE},
    {    GPIO_D26,   4,   0x3064U,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_D26,   7,   0x311cU,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_D27,   4,   0x3068U,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_D27,   7,   0x3128U,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_D28,   2,   0x3058U,  2,    APB_IOMUXC_SF_BASE},
    {    GPIO_D28,   4,   0x306cU,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_D28,   7,   0x3134U,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_D29,   7,   0x313cU,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_D30,   2,   0x3080U,  2,    APB_IOMUXC_SF_BASE},
    {    GPIO_D30,   7,   0x3140U,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_D31,   7,   0x3144U,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_D32,   2,   0x32ccU,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_D32,   7,   0x3168U,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_D33,   7,   0x3170U,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_D34,   2,   0x32d4U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_D34,   7,   0x3174U,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_D35,   7,   0x317cU,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_D36,   2,   0x32d8U,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_D36,   7,   0x3180U,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_D37,   7,   0x3188U,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_D38,   2,   0x32dcU,  1,    APB_IOMUXC_SF_BASE},
    {    GPIO_D38,   7,   0x318cU,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_D39,   7,   0x3190U,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_D40,   2,   0x32e0U,  4,    APB_IOMUXC_SF_BASE},
    {    GPIO_D40,   4,   0x3148U,  4,    APB_IOMUXC_AP_BASE},
    {    GPIO_D40,   7,   0x3194U,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_D41,   4,   0x314cU,  4,    APB_IOMUXC_AP_BASE},
    {    GPIO_D41,   7,   0x3198U,  1,    APB_IOMUXC_AP_BASE},
#endif /* GPIO_D0 */

    {     GPIO_E0,   2,   0x3350U,  2,    APB_IOMUXC_SF_BASE},
    {     GPIO_E0,   3,   0x3058U,  2,    APB_IOMUXC_AP_BASE},
    {     GPIO_E0,   4,   0x3070U,  1,    APB_IOMUXC_AP_BASE},
    {     GPIO_E0,   5,   0x3044U,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_E0,   6,   0x3150U,  1,    APB_IOMUXC_AP_BASE},
    {     GPIO_E0,   7,   0x308cU,  3,    APB_IOMUXC_AP_BASE},
    {     GPIO_E1,   2,   0x3178U,  1,    APB_IOMUXC_AP_BASE},
    {     GPIO_E1,   3,   0x305cU,  2,    APB_IOMUXC_AP_BASE},
    {     GPIO_E1,   4,   0x3080U,  1,    APB_IOMUXC_AP_BASE},
    {     GPIO_E1,   5,   0x3050U,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_E1,   6,   0x3154U,  1,    APB_IOMUXC_AP_BASE},
    {     GPIO_E1,   7,   0x3098U,  2,    APB_IOMUXC_AP_BASE},
    {     GPIO_E2,   2,   0x316cU,  1,    APB_IOMUXC_AP_BASE},
    {     GPIO_E2,   3,   0x3060U,  2,    APB_IOMUXC_AP_BASE},
    {     GPIO_E2,   4,   0x3090U,  1,    APB_IOMUXC_AP_BASE},
    {     GPIO_E2,   5,   0x3060U,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_E2,   6,   0x3158U,  1,    APB_IOMUXC_AP_BASE},
    {     GPIO_E2,   7,   0x30a4U,  2,    APB_IOMUXC_AP_BASE},
    {     GPIO_E3,   3,   0x3064U,  2,    APB_IOMUXC_AP_BASE},
    {     GPIO_E3,   4,   0x309cU,  1,    APB_IOMUXC_AP_BASE},
    {     GPIO_E3,   5,   0x3070U,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_E3,   6,   0x315cU,  1,    APB_IOMUXC_AP_BASE},
    {     GPIO_E3,   7,   0x30b0U,  2,    APB_IOMUXC_AP_BASE},
    {     GPIO_E4,   2,   0x3148U,  5,    APB_IOMUXC_AP_BASE},
    {     GPIO_E4,   3,   0x3068U,  2,    APB_IOMUXC_AP_BASE},
    {     GPIO_E4,   4,   0x30a8U,  1,    APB_IOMUXC_AP_BASE},
    {     GPIO_E4,   5,   0x3084U,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_E4,   6,   0x3160U,  1,    APB_IOMUXC_AP_BASE},
    {     GPIO_E4,   7,   0x30bcU,  2,    APB_IOMUXC_AP_BASE},
    {     GPIO_E5,   2,   0x314cU,  5,    APB_IOMUXC_AP_BASE},
    {     GPIO_E5,   3,   0x306cU,  2,    APB_IOMUXC_AP_BASE},
    {     GPIO_E5,   4,   0x30b4U,  1,    APB_IOMUXC_AP_BASE},
    {     GPIO_E5,   5,   0x3090U,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_E5,   6,   0x3164U,  1,    APB_IOMUXC_AP_BASE},
    {     GPIO_E5,   7,   0x30e8U,  2,    APB_IOMUXC_AP_BASE},

#ifdef GPIO_E6
    {     GPIO_E6,   4,   0x30c0U,  1,    APB_IOMUXC_AP_BASE},
    {     GPIO_E6,   7,   0x30f4U,  2,    APB_IOMUXC_AP_BASE},
    {     GPIO_E7,   2,   0x3350U,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_E7,   4,   0x30c8U,  1,    APB_IOMUXC_AP_BASE},
    {     GPIO_E7,   7,   0x30fcU,  2,    APB_IOMUXC_AP_BASE},
    {     GPIO_E8,   2,   0x3130U,  1,    APB_IOMUXC_AP_BASE},
    {     GPIO_E8,   4,   0x30d0U,  1,    APB_IOMUXC_AP_BASE},
    {     GPIO_E8,   5,   0x30a4U,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_E8,   7,   0x3104U,  2,    APB_IOMUXC_AP_BASE},
    {     GPIO_E9,   4,   0x30d4U,  1,    APB_IOMUXC_AP_BASE},
    {     GPIO_E9,   5,   0x30b4U,  3,    APB_IOMUXC_SF_BASE},
    {     GPIO_E9,   7,   0x310cU,  2,    APB_IOMUXC_AP_BASE},
    {    GPIO_E10,   2,   0x3010U,  3,    APB_IOMUXC_AP_BASE},
    {    GPIO_E10,   4,   0x3074U,  2,    APB_IOMUXC_AP_BASE},
    {    GPIO_E10,   7,   0x3114U,  2,    APB_IOMUXC_AP_BASE},
    {    GPIO_E11,   2,   0x3014U,  3,    APB_IOMUXC_AP_BASE},
    {    GPIO_E11,   4,   0x3084U,  2,    APB_IOMUXC_AP_BASE},
    {    GPIO_E11,   5,   0x30ccU,  2,    APB_IOMUXC_AP_BASE},
    {    GPIO_E11,   7,   0x311cU,  2,    APB_IOMUXC_AP_BASE},
    {    GPIO_E12,   2,   0x3018U,  3,    APB_IOMUXC_AP_BASE},
    {    GPIO_E12,   4,   0x30d8U,  2,    APB_IOMUXC_AP_BASE},
    {    GPIO_E12,   5,   0x3148U,  6,    APB_IOMUXC_AP_BASE},
    {    GPIO_E12,   7,   0x3128U,  2,    APB_IOMUXC_AP_BASE},
    {    GPIO_E13,   2,   0x301cU,  3,    APB_IOMUXC_AP_BASE},
    {    GPIO_E13,   4,   0x30e0U,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_E13,   5,   0x314cU,  6,    APB_IOMUXC_AP_BASE},
    {    GPIO_E13,   7,   0x3134U,  2,    APB_IOMUXC_AP_BASE},
    {    GPIO_E14,   2,   0x3020U,  3,    APB_IOMUXC_AP_BASE},
    {    GPIO_E14,   4,   0x30ecU,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_E14,   5,   0x30c4U,  4,    APB_IOMUXC_SF_BASE},
    {    GPIO_E14,   7,   0x313cU,  2,    APB_IOMUXC_AP_BASE},
    {    GPIO_E15,   2,   0x3024U,  3,    APB_IOMUXC_AP_BASE},
    {    GPIO_E15,   4,   0x30f8U,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_E15,   5,   0x30d4U,  4,    APB_IOMUXC_SF_BASE},
    {    GPIO_E15,   7,   0x3140U,  2,    APB_IOMUXC_AP_BASE},
    {    GPIO_E16,   2,   0x3028U,  3,    APB_IOMUXC_AP_BASE},
    {    GPIO_E16,   4,   0x3100U,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_E16,   5,   0x30e4U,  4,    APB_IOMUXC_SF_BASE},
    {    GPIO_E16,   7,   0x3144U,  2,    APB_IOMUXC_AP_BASE},
    {    GPIO_E17,   2,   0x302cU,  3,    APB_IOMUXC_AP_BASE},
    {    GPIO_E17,   4,   0x3108U,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_E17,   5,   0x30f0U,  4,    APB_IOMUXC_SF_BASE},
    {    GPIO_E17,   7,   0x3168U,  2,    APB_IOMUXC_AP_BASE},
    {    GPIO_E18,   2,   0x3004U,  3,    APB_IOMUXC_AP_BASE},
    {    GPIO_E18,   4,   0x3110U,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_E18,   5,   0x3108U,  3,    APB_IOMUXC_SF_BASE},
    {    GPIO_E18,   7,   0x3170U,  2,    APB_IOMUXC_AP_BASE},
    {    GPIO_E19,   2,   0x3008U,  3,    APB_IOMUXC_AP_BASE},
    {    GPIO_E19,   4,   0x3118U,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_E19,   5,   0x3118U,  3,    APB_IOMUXC_SF_BASE},
    {    GPIO_E19,   7,   0x3174U,  2,    APB_IOMUXC_AP_BASE},
    {    GPIO_E20,   2,   0x300cU,  3,    APB_IOMUXC_AP_BASE},
    {    GPIO_E20,   4,   0x3120U,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_E20,   5,   0x3178U,  2,    APB_IOMUXC_AP_BASE},
    {    GPIO_E20,   7,   0x317cU,  2,    APB_IOMUXC_AP_BASE},
    {    GPIO_E21,   2,   0x3000U,  3,    APB_IOMUXC_AP_BASE},
    {    GPIO_E21,   4,   0x312cU,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_E21,   5,   0x316cU,  2,    APB_IOMUXC_AP_BASE},
    {    GPIO_E21,   7,   0x3180U,  2,    APB_IOMUXC_AP_BASE},
    {    GPIO_E22,   4,   0x3138U,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_E22,   7,   0x3188U,  2,    APB_IOMUXC_AP_BASE},
    {    GPIO_E23,   4,   0x30e4U,  2,    APB_IOMUXC_AP_BASE},
    {    GPIO_E23,   7,   0x318cU,  2,    APB_IOMUXC_AP_BASE},
    {    GPIO_E24,   2,   0x30c4U,  2,    APB_IOMUXC_AP_BASE},
    {    GPIO_E24,   4,   0x30f0U,  2,    APB_IOMUXC_AP_BASE},
    {    GPIO_E24,   5,   0x3124U,  2,    APB_IOMUXC_AP_BASE},
    {    GPIO_E24,   7,   0x3190U,  2,    APB_IOMUXC_AP_BASE},
    {    GPIO_E25,   2,   0x3184U,  1,    APB_IOMUXC_AP_BASE},
    {    GPIO_E25,   4,   0x30dcU,  2,    APB_IOMUXC_AP_BASE},
    {    GPIO_E25,   6,   0x3148U,  7,    APB_IOMUXC_AP_BASE},
    {    GPIO_E25,   7,   0x3194U,  2,    APB_IOMUXC_AP_BASE},
    {    GPIO_E26,   6,   0x314cU,  7,    APB_IOMUXC_AP_BASE},
    {    GPIO_E26,   7,   0x3198U,  2,    APB_IOMUXC_AP_BASE},
#endif /* GPIO_E6 */
};
#endif /* CONFIG_E3L */

static void taishan_set_input_select_reg(uint32_t pin, uint32_t mux,
    const sdrv_input_select_t *input_select, uint32_t input_select_size)
{
    int left = 0;
    int right = input_select_size - 1;

    while (left <= right) {
        int mid = (left + right) / 2;
        const sdrv_input_select_t *select = &input_select[mid];

        if (PINCTRL_IS_IDX(select->pin, select->mux) == PINCTRL_IS_IDX(pin, mux)) {
            writel(select->value, select->base + select->offset);
            break;
        }
        else if (PINCTRL_IS_IDX(select->pin, select->mux) > PINCTRL_IS_IDX(pin, mux)) {
            right = mid - 1;
        }
        else {
            left = mid + 1;
        }
    }
}

static inline paddr_t pin_reg(paddr_t base, uint32_t offset, uint32_t pin)
{
    return base + offset + pin * 4;
}

static inline void write_pin_reg(paddr_t base, uint32_t offset,
                                 uint32_t pin, uint32_t val)
{
    writel(val, pin_reg(base, offset, pin));
}

static inline uint32_t read_pin_reg(paddr_t base, uint32_t offset, uint32_t pin)
{
    return readl(pin_reg(base, offset, pin));
}

static inline void modify_pin_reg(paddr_t base, uint32_t offset, uint32_t pin,
                                  uint32_t bit, uint32_t width, uint32_t val)
{
    uint32_t v = read_pin_reg(base, offset, pin);
    v &= ~(BIT_MASK(width) << bit);
    v |= (val & BIT_MASK(width)) << bit;
    write_pin_reg(base, offset, pin, v);
}

static inline status_t
taishan_pinctrl_set_function(paddr_t base, uint32_t offset, unsigned int pin,
                             unsigned int value)
{
    if ((int)value >= MUX_FUNC_GPIO && value <= MUX_FUNC_ALT9) {
        modify_pin_reg(base, offset, pin, MUX_FUNC_SHIFT,
                       MUX_FUNC_WIDTH, value);

        return SDRV_STATUS_OK;
    }
    else {
        return SDRV_STATUS_INVALID_PARAM;
    }
}

static inline void
taishan_pinctrl_set_drive(paddr_t base, uint32_t offset, unsigned int pin,
                          unsigned int value)
{
    modify_pin_reg(base, offset, pin, MUX_ODE_SHIFT, MUX_ODE_WIDTH, value);
}

static inline void
taishan_pinctrl_set_pull_gpio(paddr_t base, uint32_t offset, unsigned int pin,
                              unsigned int value)
{
    modify_pin_reg(base, offset, pin, PAD_PULL_SHIFT, PAD_PULL_WIDTH, value);
}

static inline void
taishan_pinctrl_set_drive_strength_gpio(paddr_t base, uint32_t offset,
                                        unsigned int pin, unsigned int value)
{
    modify_pin_reg(base, offset, pin, PAD_DS_SHIFT, PAD_DS_WIDTH, value);
}

static inline void
taishan_pinctrl_set_input_schmitt_gpio(paddr_t base, uint32_t offset,
                                       unsigned int pin, unsigned int value)
{
    modify_pin_reg(base, offset, pin, PAD_IS_SHIFT, PAD_IS_WIDTH, value);
}

static inline void
taishan_pinctrl_set_slew_rate_gpio(paddr_t base, uint32_t offset,
                                   unsigned int pin, unsigned int value)
{
    modify_pin_reg(base, offset, pin, PAD_SR_SHIFT, PAD_SR_WIDTH, value);
}

static inline void
taishan_pinctrl_set_force_input(paddr_t base, uint32_t offset, unsigned int pin,
                                unsigned int fin, unsigned int fv)
{
    modify_pin_reg(base, offset, pin, MUX_FIN_SHIFT, MUX_FIN_WIDTH, fin);
    modify_pin_reg(base, offset, pin, MUX_FV_SHIFT, MUX_FV_WIDTH, fv);
}

static inline void
taishan_pinctrl_set_mode_select(paddr_t base, uint32_t offset,
                                unsigned int pin, unsigned int value)
{
    modify_pin_reg(base, offset, pin, PAD_MS_SHIFT, PAD_MS_WIDTH, value);
}

static status_t do_sdrv_pinctrl_set_mux(uint32_t pin_index, pin_mux_e mux,
                                        paddr_t base, uint32_t offset, uint32_t ctrl_index,
                                        const sdrv_input_select_t *input_select,
                                        uint32_t input_select_size)
{
#if APB_DISP_MUX_BASE
    if ((mux == PIN_MUX_ALT1) && (pin_index >= GPIO_L0) && (pin_index <= GPIO_L9)) {
        for (uint32_t i = 0; i < 5; i++) {
            uint32_t val;

            val = readl(APB_DISP_MUX_BASE + LVDS_CH0_PAD_SET_(i));
            val |= CH0_RTERM_EN_MASK;
            writel(val, APB_DISP_MUX_BASE + LVDS_CH0_PAD_SET_(i));
        }
    }
#endif

    taishan_set_input_select_reg(pin_index, mux, input_select, input_select_size);
    return taishan_pinctrl_set_function(base, offset, ctrl_index, mux);
}

static status_t do_sdrv_pinctrl_set_opendrain(paddr_t base, uint32_t offset, uint32_t ctrl_index,
                                              pin_open_drain_e open_drain)
{
    uint32_t value;

    value = (open_drain == PIN_OPEN_DRAIN) ? MUX_ODE_OPENDRAIN : MUX_ODE_PUSHPULL;
    taishan_pinctrl_set_drive(base, offset, ctrl_index, value);

    return SDRV_STATUS_OK;
}

static status_t do_sdrv_pinctrl_set_pull(paddr_t base, uint32_t offset, uint32_t ctrl_index,
                                         pin_pull_config_e pull_config)
{
    uint32_t value = PAD_NOPULL;

    if (pull_config == PIN_PULL_UP) {
        value = PAD_PULLUP;
    }
    else if (pull_config == PIN_PULL_DOWN) {
        value = PAD_PULLDOWN;
    }

    taishan_pinctrl_set_pull_gpio(base, offset, ctrl_index, value);

    return SDRV_STATUS_OK;
}

static status_t do_sdrv_pinctrl_set_force_input(paddr_t base, uint32_t offset, uint32_t ctrl_index,
                                                pin_force_input_e force_input)
{
    uint32_t fin = PAD_INPUT_FORCE_DISABLE;
    uint32_t fv = PAD_FORCE_INPUT_VALUE_0;

    if (force_input <= PIN_FORCE_INPUT_ENABLE) {
        fin = force_input;
    }
    else if (force_input == PIN_FORCE_INPUT_HIGH) {
        fv = PAD_FORCE_INPUT_VALUE_1;
    }

    taishan_pinctrl_set_force_input(base, offset, ctrl_index, fin, fv);

    return SDRV_STATUS_OK;
}

static status_t do_sdrv_pinctrl_set_mode_select(paddr_t base, uint32_t mux_offset, uint32_t pad_offset,
                                                uint32_t ctrl_index, pin_mode_select_e mode_select)
{
    uint32_t value = 0;

    if (mode_select != PIN_MODE_DIGITAL) {
        value = (mode_select == PIN_MODE_SELECT_NORMAL) ? 0 : 1;
        taishan_pinctrl_set_force_input(base, mux_offset, ctrl_index,
            PAD_INPUT_FORCE_DISABLE, PAD_FORCE_INPUT_VALUE_0);
    }

    taishan_pinctrl_set_mode_select(base, pad_offset, ctrl_index, value);

    return SDRV_STATUS_OK;
}

#if CONFIG_E3 || CONFIG_D3
static status_t do_sdrv_pinctrl_clear_dispmux_config(uint32_t base)
{
    volatile uint32_t val;

    for (uint32_t i = 0; i < 5; i++) {
        val = readl(base + LVDS_CH0_PAD_SET_(i));
        val &= ~CH0_RTERM_EN_MASK;
        val &= ~CH0_TXEN_MASK;
        val &= ~CH0_RXEN_MASK;
        writel(val, base + LVDS_CH0_PAD_SET_(i));
    }

    val = readl(base + LVDS_CH0_PAD_COM_SET_(0));
    val &= ~CH0_RTERM_MASK;
    writel(val, base + LVDS_CH0_PAD_COM_SET_(0));

    return SDRV_STATUS_OK;
}
#endif

/**
 * @brief Initialize pins as pre-defined.
 *
 * @param [in] count pin numbers to be initilized.
 * @param [in] configs pre-defined pin config array list.
 */
status_t sdrv_pinctrl_init(uint32_t count, const pin_settings_config_t *configs)
{
    status_t ret;

    for (uint32_t i = 0; i < count; i++, configs++) {
        CALL_RET_CHECK(ret, sdrv_pinctrl_set_mux(configs->pin_index, configs->mux));
        CALL_RET_CHECK(ret, sdrv_pinctrl_set_opendrain(configs->pin_index, configs->open_drain));
        CALL_RET_CHECK(ret, sdrv_pinctrl_set_pull(configs->pin_index, configs->pull_config));
        CALL_RET_CHECK(ret, sdrv_pinctrl_set_drive_strength(configs->pin_index, configs->drive_strength));
        CALL_RET_CHECK(ret, sdrv_pinctrl_set_slew_rate(configs->pin_index, configs->slew_rate));
        CALL_RET_CHECK(ret, sdrv_pinctrl_set_input_select(configs->pin_index, configs->input_select));
        CALL_RET_CHECK(ret, sdrv_pinctrl_set_force_input(configs->pin_index, configs->force_input));
        CALL_RET_CHECK(ret, sdrv_pinctrl_set_mode_select(configs->pin_index, configs->mode_select));

        if(configs->mux == PIN_MUX_ALT0) {
            gpio_interrupt_e gpio_intr_type = GPIO_INTR_DISABLE;

            if (configs->initial_value == PIN_LEVEL_LOW) {
                CALL_RET_CHECK(ret, sdrv_gpio_set_pin_output_level(configs->pin_index, false));
            }
            else {
                CALL_RET_CHECK(ret, sdrv_gpio_set_pin_output_level(configs->pin_index, true));
            }

            if (configs->data_direction == PIN_OUTPUT_DIRECTION) {
                CALL_RET_CHECK(ret, sdrv_gpio_set_pin_direction(configs->pin_index, GPIO_DIR_OUT));
            }
            else {
                CALL_RET_CHECK(ret, sdrv_gpio_set_pin_direction(configs->pin_index, GPIO_DIR_IN));

                switch (configs->interrupt_config) {
                    case PIN_INTERRUPT_DISABLED:
                        gpio_intr_type = GPIO_INTR_DISABLE;
                    break;

                    case PIN_INTERRUPT_HIGH_LEVEL:
                        gpio_intr_type = GPIO_INTR_HIGH_LEVEL;
                    break;

                    case PIN_INTERRUPT_LOW_LEVEL:
                        gpio_intr_type = GPIO_INTR_LOW_LEVEL;
                    break;

                    case PIN_INTERRUPT_RISING_EDGE:
                        gpio_intr_type = GPIO_INTR_RISING_EDGE;
                    break;

                    case PIN_INTERRUPT_FALLING_EDGE:
                        gpio_intr_type = GPIO_INTR_FALLING_EDGE;
                    break;

                    case PIN_INTERRUPT_BOTH_EDGE:
                        gpio_intr_type = GPIO_INTR_BOTH_EDGE;
                    break;

                    default:
                        return SDRV_STATUS_INVALID_PARAM;
                }

                CALL_RET_CHECK(ret, sdrv_gpio_set_pin_interrupt(configs->pin_index, gpio_intr_type));
            }
        }
    }

    return SDRV_STATUS_OK;
}

/**
 * @brief Config mux function for specific pin.
 *
 * This function select a mux function for specific pin.
 *
 * @param [in] pin_index defined pin macro in pinctrl.h.
 * @param [in] mux mux function
 */
status_t sdrv_pinctrl_set_mux(uint32_t pin_index, pin_mux_e mux)
{
    uint32_t ctrl_base;
    uint32_t ctrl_index;

    if (pin_index < TAISHAN_PIN_NUM) {
        ctrl_base = TAISHAN_PINCTRL_GET_BASE(pin_index);
        ctrl_index = TAISHAN_PINCTRL_GET_INDEX(pin_index);

        return do_sdrv_pinctrl_set_mux(pin_index, mux, ctrl_base, TAISHAN_MUX_CONFIG_REG_OFFSET,
                ctrl_index, &sdrv_pinctrl_is[0], ARRAY_SIZE(sdrv_pinctrl_is));
    }
    else {
        return SDRV_PINCTRL_BAD_PIN;
    }
}

/**
 * @brief Config output type for specific pin.
 *
 * This function config pin output use open drain or push pull.
 *
 * @param [in] pin_index defined pin macro in pinctrl.h.
 * @param [in] open_drain output type defined in pin_open_drain_e.
 */
status_t sdrv_pinctrl_set_opendrain(uint32_t pin_index, pin_open_drain_e open_drain)
{
    uint32_t ctrl_base;
    uint32_t ctrl_index;

    if (pin_index < TAISHAN_PIN_NUM) {
        ctrl_base = TAISHAN_PINCTRL_GET_BASE(pin_index);
        ctrl_index = TAISHAN_PINCTRL_GET_INDEX(pin_index);

        return do_sdrv_pinctrl_set_opendrain(ctrl_base, TAISHAN_MUX_CONFIG_REG_OFFSET, ctrl_index, open_drain);
    }
    else {
        return SDRV_PINCTRL_BAD_PIN;
    }
}

/**
 * @brief Config pull type for specific pin.
 *
 * This function config pin internal pull config.
 *
 * @param [in] pin_index defined pin macro in pinctrl.h.
 * @param [in] pull_config pull type defined in pin_pull_config_e.
 */
status_t sdrv_pinctrl_set_pull(uint32_t pin_index, pin_pull_config_e pull_config)
{
    uint32_t ctrl_base;
    uint32_t ctrl_index;

    if (pin_index < TAISHAN_PIN_NUM) {
        ctrl_base = TAISHAN_PINCTRL_GET_BASE(pin_index);
        ctrl_index = TAISHAN_PINCTRL_GET_INDEX(pin_index);

        return do_sdrv_pinctrl_set_pull(ctrl_base, TAISHAN_PAD_CONFIG_REG_OFFSET, ctrl_index, pull_config);
    }
    else {
        return SDRV_PINCTRL_BAD_PIN;
    }
}

/**
 * @brief Config drive strength for specific pin.
 *
 * This function config pin current drive strength.
 *
 * @param [in] pin_index defined pin macro in pinctrl.h.
 * @param [in] drive_strength current drive strength defined in pin_drive_strength_e.
 */
status_t sdrv_pinctrl_set_drive_strength(uint32_t pin_index, pin_drive_strength_e drive_strength)
{
    uint32_t ctrl_base;
    uint32_t ctrl_index;

    if (pin_index < TAISHAN_PIN_NUM) {
        ctrl_base = TAISHAN_PINCTRL_GET_BASE(pin_index);
        ctrl_index = TAISHAN_PINCTRL_GET_INDEX(pin_index);

        taishan_pinctrl_set_drive_strength_gpio(ctrl_base, TAISHAN_PAD_CONFIG_REG_OFFSET, ctrl_index, drive_strength);

        return SDRV_STATUS_OK;
    }
    else {
        return SDRV_PINCTRL_BAD_PIN;
    }
}

/**
 * @brief Config slew rate for specific pin.
 *
 * This function config pin slew rate.
 *
 * @param [in] pin_index defined pin macro in pinctrl.h.
 * @param [in] slew_rate slew rate type defined in pin_slew_rate_e.
 */
status_t sdrv_pinctrl_set_slew_rate(uint32_t pin_index, pin_slew_rate_e slew_rate)
{
    uint32_t ctrl_base;
    uint32_t ctrl_index;

    if (pin_index < TAISHAN_PIN_NUM) {
        ctrl_base = TAISHAN_PINCTRL_GET_BASE(pin_index);
        ctrl_index = TAISHAN_PINCTRL_GET_INDEX(pin_index);

        taishan_pinctrl_set_slew_rate_gpio(ctrl_base, TAISHAN_PAD_CONFIG_REG_OFFSET, ctrl_index, slew_rate);

        return SDRV_STATUS_OK;
    }
    else {
        return SDRV_PINCTRL_BAD_PIN;
    }
}

/**
 * @brief Config input select for specific pin.
 *
 * This function config pin input select cmos or cmos schmitt.
 *
 * @param [in] pin_index defined pin macro in pinctrl.h.
 * @param [in] input_select input select defined in pin_input_select_e.
 */
status_t sdrv_pinctrl_set_input_select(uint32_t pin_index, pin_input_select_e input_select)
{
    uint32_t ctrl_base;
    uint32_t ctrl_index;

    if (pin_index < TAISHAN_PIN_NUM) {
        ctrl_base = TAISHAN_PINCTRL_GET_BASE(pin_index);
        ctrl_index = TAISHAN_PINCTRL_GET_INDEX(pin_index);

        taishan_pinctrl_set_input_schmitt_gpio(ctrl_base, TAISHAN_PAD_CONFIG_REG_OFFSET, ctrl_index, input_select);

        return SDRV_STATUS_OK;
    }
    else {
        return SDRV_PINCTRL_BAD_PIN;
    }
}

/**
 * @brief Config force input for specific pin.
 *
 * This function config force input type for a specific pin. For Multiply slave device in bus, cs pin should
 * config to force input enable, such as I2C clock etc.
 *
 * @param [in] pin_index defined pin macro in pinctrl.h.
 * @param [in] force_input force input defined in pin_force_input_e.
 */
status_t sdrv_pinctrl_set_force_input(uint32_t pin_index, pin_force_input_e force_input)
{
    uint32_t ctrl_base;
    uint32_t ctrl_index;

    if (pin_index < TAISHAN_PIN_NUM) {
        ctrl_base = TAISHAN_PINCTRL_GET_BASE(pin_index);
        ctrl_index = TAISHAN_PINCTRL_GET_INDEX(pin_index);

        return do_sdrv_pinctrl_set_force_input(ctrl_base, TAISHAN_MUX_CONFIG_REG_OFFSET, ctrl_index, force_input);
    }
    else {
        return SDRV_PINCTRL_BAD_PIN;
    }
}

/**
 * @brief Config mode select for specific pin.
 *
 * This function config mode select register for one pin.
 * normal/down coversion only valid for Analog-digital combo IO (GPIOA/B/C)
 * normal: passes pad signal as is
 * down coversion: enable down coversion of pad signal before passing to core
 *
 * @param [in] pin_index defined pin macro in pinctrl.h.
 * @param [in] mode_select mode select defined in pin_mode_select_e.
 */
status_t sdrv_pinctrl_set_mode_select(uint32_t pin_index, pin_mode_select_e mode_select)
{
    uint32_t ctrl_base;
    uint32_t ctrl_index;

    if (pin_index < TAISHAN_PIN_NUM) {
        ctrl_base = TAISHAN_PINCTRL_GET_BASE(pin_index);
        ctrl_index = TAISHAN_PINCTRL_GET_INDEX(pin_index);

        return do_sdrv_pinctrl_set_mode_select(ctrl_base, TAISHAN_MUX_CONFIG_REG_OFFSET,
                TAISHAN_PAD_CONFIG_REG_OFFSET, ctrl_index, mode_select);
    }
    else {
        return SDRV_PINCTRL_BAD_PIN;
    }
}

/**
 * @brief Clear display mux config.
 *
 * This function used to clear display mux config. If GPIO_L0 - GPIO_L9 not use for LVDS,
 * you must call this function.
 *
 */
status_t sdrv_pinctrl_clear_dispmux_config(void)
{
#if CONFIG_E3 || CONFIG_D3
    do_sdrv_pinctrl_clear_dispmux_config(APB_DISP_MUX_BASE);
#endif

    return SDRV_STATUS_OK;
}

/**
 * @brief Config pin to high resistance.
 *
 * This function use to config pin as high resistance.
 *
 * @param [in] pin_index defined pin macro in pinctrl.h.
 */
status_t sdrv_pinctrl_set_high_resistance(uint32_t pin_index)
{
    status_t ret;

    CALL_RET_CHECK(ret, sdrv_pinctrl_set_mux(pin_index, PIN_MUX_ALT0));
    CALL_RET_CHECK(ret, sdrv_pinctrl_set_pull(pin_index, PIN_NOPULL));
    CALL_RET_CHECK(ret, sdrv_pinctrl_set_opendrain(pin_index, PIN_PUSH_PULL));
    CALL_RET_CHECK(ret, sdrv_pinctrl_set_force_input(pin_index, PIN_FORCE_INPUT_LOW));
    CALL_RET_CHECK(ret, sdrv_gpio_set_pin_direction(pin_index, GPIO_DIR_IN));

    return SDRV_STATUS_OK;
}

/**
 * @brief Initialize one pin as pre-defined.
 *
 * @param [in] pinctrl pin belongs PINCTRL contoller instance.
 * @param [in] gpio pin belongs GPIO contoller instance.
 * @param [in] pin_cfg pre-defined pin config.
 */
status_t sdrv_pinctrl_ctrl_init_pin(sdrv_pinctrl_t *pinctrl, struct sdrv_gpio *gpio, const pin_settings_config_t *pin_cfg)
{
    status_t ret;

    if (!pinctrl) {
        return SDRV_PINCTRL_CTRL_NULL;
    }

    CALL_RET_CHECK(ret, sdrv_pinctrl_ctrl_set_mux(pinctrl, pin_cfg->pin_index, pin_cfg->mux));
    CALL_RET_CHECK(ret, sdrv_pinctrl_ctrl_set_opendrain(pinctrl, pin_cfg->pin_index, pin_cfg->open_drain));
    CALL_RET_CHECK(ret, sdrv_pinctrl_ctrl_set_pull(pinctrl, pin_cfg->pin_index, pin_cfg->pull_config));
    CALL_RET_CHECK(ret, sdrv_pinctrl_ctrl_set_drive_strength(pinctrl, pin_cfg->pin_index, pin_cfg->drive_strength));
    CALL_RET_CHECK(ret, sdrv_pinctrl_ctrl_set_slew_rate(pinctrl, pin_cfg->pin_index, pin_cfg->slew_rate));
    CALL_RET_CHECK(ret, sdrv_pinctrl_ctrl_set_input_select(pinctrl, pin_cfg->pin_index, pin_cfg->input_select));
    CALL_RET_CHECK(ret, sdrv_pinctrl_ctrl_set_force_input(pinctrl, pin_cfg->pin_index, pin_cfg->force_input));
    CALL_RET_CHECK(ret, sdrv_pinctrl_ctrl_set_mode_select(pinctrl, pin_cfg->pin_index, pin_cfg->mode_select));

    if(pin_cfg->mux == PIN_MUX_ALT0) {
        gpio_interrupt_e gpio_intr_type = GPIO_INTR_DISABLE;

        if (pin_cfg->initial_value == PIN_LEVEL_LOW) {
            CALL_RET_CHECK(ret, sdrv_gpio_ctrl_set_pin_output_level(gpio, pin_cfg->pin_index, false));
        }
        else {
            CALL_RET_CHECK(ret, sdrv_gpio_ctrl_set_pin_output_level(gpio, pin_cfg->pin_index, true));
        }

        if (pin_cfg->data_direction == PIN_OUTPUT_DIRECTION) {
            CALL_RET_CHECK(ret, sdrv_gpio_ctrl_set_pin_direction(gpio, pin_cfg->pin_index, GPIO_DIR_OUT));
        }
        else {
            CALL_RET_CHECK(ret, sdrv_gpio_ctrl_set_pin_direction(gpio, pin_cfg->pin_index, GPIO_DIR_IN));

            switch (pin_cfg->interrupt_config) {
                case PIN_INTERRUPT_DISABLED:
                    gpio_intr_type = GPIO_INTR_DISABLE;
                break;

                case PIN_INTERRUPT_HIGH_LEVEL:
                    gpio_intr_type = GPIO_INTR_HIGH_LEVEL;
                break;

                case PIN_INTERRUPT_LOW_LEVEL:
                    gpio_intr_type = GPIO_INTR_LOW_LEVEL;
                break;

                case PIN_INTERRUPT_RISING_EDGE:
                    gpio_intr_type = GPIO_INTR_RISING_EDGE;
                break;

                case PIN_INTERRUPT_FALLING_EDGE:
                    gpio_intr_type = GPIO_INTR_FALLING_EDGE;
                break;

                case PIN_INTERRUPT_BOTH_EDGE:
                    gpio_intr_type = GPIO_INTR_BOTH_EDGE;
                break;

                default:
                    return SDRV_STATUS_INVALID_PARAM;
            }

            CALL_RET_CHECK(ret, sdrv_gpio_ctrl_set_pin_interrupt(gpio, pin_cfg->pin_index, gpio_intr_type));
        }
    }

    return SDRV_STATUS_OK;
}

/**
 * @brief Config mux function for specific pin.
 *
 * This function select a mux function for specific pin.
 *
 * @param [in] pinctrl PINCTRL contoller instance.
 * @param [in] pin_index defined pin macro in pinctrl.h.
 * @param [in] mux mux function
 */
status_t sdrv_pinctrl_ctrl_set_mux(sdrv_pinctrl_t *pinctrl, uint32_t pin_index, pin_mux_e mux)
{
    uint32_t ctrl_index;

    if (!pinctrl) {
        return SDRV_PINCTRL_CTRL_NULL;
    }

    if ((pin_index >= pinctrl->pin_start) &&
        (pin_index < (pinctrl->pin_start + pinctrl->pin_num))) {
        ctrl_index = pin_index - pinctrl->pin_start;

        return do_sdrv_pinctrl_set_mux(pin_index, mux, pinctrl->base, pinctrl->mux_offset,
                ctrl_index, pinctrl->input_select, pinctrl->input_select_size);
    } else {
        return SDRV_PINCTRL_BAD_PIN;
    }
}

/**
 * @brief Config output type for specific pin.
 *
 * This function config pin output use open drain or push pull.
 *
 * @param [in] pinctrl PINCTRL contoller instance.
 * @param [in] pin_index defined pin macro in pinctrl.h.
 * @param [in] open_drain output type defined in pin_open_drain_e.
 */
status_t sdrv_pinctrl_ctrl_set_opendrain(sdrv_pinctrl_t *pinctrl, uint32_t pin_index, pin_open_drain_e open_drain)
{
    uint32_t ctrl_index;

    if (!pinctrl) {
        return SDRV_PINCTRL_CTRL_NULL;
    }

    if ((pin_index >= pinctrl->pin_start) &&
        (pin_index < (pinctrl->pin_start + pinctrl->pin_num))) {
        ctrl_index = pin_index - pinctrl->pin_start;

        return do_sdrv_pinctrl_set_opendrain(pinctrl->base, pinctrl->mux_offset, ctrl_index, open_drain);
    } else {
        return SDRV_PINCTRL_BAD_PIN;
    }
}

/**
 * @brief Config pull type for specific pin.
 *
 * This function config pin internal pull config.
 *
 * @param [in] pinctrl PINCTRL contoller instance.
 * @param [in] pin_index defined pin macro in pinctrl.h.
 * @param [in] pull_config pull type defined in pin_pull_config_e.
 */
status_t sdrv_pinctrl_ctrl_set_pull(sdrv_pinctrl_t *pinctrl, uint32_t pin_index, pin_pull_config_e pull_config)
{
    uint32_t ctrl_index;

    if (!pinctrl) {
        return SDRV_PINCTRL_CTRL_NULL;
    }

    if ((pin_index >= pinctrl->pin_start) &&
        (pin_index < (pinctrl->pin_start + pinctrl->pin_num))) {
        ctrl_index = pin_index - pinctrl->pin_start;
        return do_sdrv_pinctrl_set_pull(pinctrl->base, pinctrl->pad_offset, ctrl_index, pull_config);
    } else {
        return SDRV_PINCTRL_BAD_PIN;
    }
}

/**
 * @brief Config drive strength for specific pin.
 *
 * This function config pin current drive strength.
 *
 * @param [in] pinctrl PINCTRL contoller instance.
 * @param [in] pin_index defined pin macro in pinctrl.h.
 * @param [in] drive_strength current drive strength defined in pin_drive_strength_e.
 */
status_t sdrv_pinctrl_ctrl_set_drive_strength(sdrv_pinctrl_t *pinctrl, uint32_t pin_index, pin_drive_strength_e drive_strength)
{
    uint32_t ctrl_index;

    if (!pinctrl) {
        return SDRV_PINCTRL_CTRL_NULL;
    }

    if ((pin_index >= pinctrl->pin_start) &&
        (pin_index < (pinctrl->pin_start + pinctrl->pin_num))) {
        ctrl_index = pin_index - pinctrl->pin_start;
        taishan_pinctrl_set_drive_strength_gpio(pinctrl->base, pinctrl->pad_offset, ctrl_index, drive_strength);

        return SDRV_STATUS_OK;
    } else {
        return SDRV_PINCTRL_BAD_PIN;
    }
}

/**
 * @brief Config slew rate for specific pin.
 *
 * This function config pin slew rate.
 *
 * @param [in] pinctrl PINCTRL contoller instance.
 * @param [in] pin_index defined pin macro in pinctrl.h.
 * @param [in] slew_rate slew rate type defined in pin_slew_rate_e.
 */
status_t sdrv_pinctrl_ctrl_set_slew_rate(sdrv_pinctrl_t *pinctrl, uint32_t pin_index, pin_slew_rate_e slew_rate)
{
    uint32_t ctrl_index;

    if (!pinctrl) {
        return SDRV_PINCTRL_CTRL_NULL;
    }

    if ((pin_index >= pinctrl->pin_start) &&
        (pin_index < (pinctrl->pin_start + pinctrl->pin_num))) {
        ctrl_index = pin_index - pinctrl->pin_start;
        taishan_pinctrl_set_slew_rate_gpio(pinctrl->base, pinctrl->pad_offset, ctrl_index, slew_rate);

        return SDRV_STATUS_OK;
    } else {
        return SDRV_PINCTRL_BAD_PIN;
    }
}

/**
 * @brief Config input select for specific pin.
 *
 * This function config pin input select cmos or cmos schmitt.
 *
 * @param [in] pinctrl PINCTRL contoller instance.
 * @param [in] pin_index defined pin macro in pinctrl.h.
 * @param [in] input_select input select defined in pin_input_select_e.
 */
status_t sdrv_pinctrl_ctrl_set_input_select(sdrv_pinctrl_t *pinctrl, uint32_t pin_index, pin_input_select_e input_select)
{
    uint32_t ctrl_index;

    if (!pinctrl) {
        return SDRV_PINCTRL_CTRL_NULL;
    }

    if ((pin_index >= pinctrl->pin_start) &&
        (pin_index < (pinctrl->pin_start + pinctrl->pin_num))) {
        ctrl_index = pin_index - pinctrl->pin_start;
        taishan_pinctrl_set_input_schmitt_gpio(pinctrl->base, pinctrl->pad_offset, ctrl_index, input_select);

        return SDRV_STATUS_OK;
    } else {
        return SDRV_PINCTRL_BAD_PIN;
    }
}

/**
 * @brief Config force input for specific pin.
 *
 * This function config force input type for a specific pin. For Multiply slave device in bus, cs pin should
 * config to force input enable, such as I2C clock etc.
 *
 * @param [in] pinctrl PINCTRL contoller instance.
 * @param [in] pin_index defined pin macro in pinctrl.h.
 * @param [in] force_input force input defined in pin_force_input_e.
 */
status_t sdrv_pinctrl_ctrl_set_force_input(sdrv_pinctrl_t *pinctrl, uint32_t pin_index, pin_force_input_e force_input)
{
    uint32_t ctrl_index;

    if (!pinctrl) {
        return SDRV_PINCTRL_CTRL_NULL;
    }

    if ((pin_index >= pinctrl->pin_start) &&
        (pin_index < (pinctrl->pin_start + pinctrl->pin_num))) {
        ctrl_index = pin_index - pinctrl->pin_start;
        return do_sdrv_pinctrl_set_force_input(pinctrl->base, pinctrl->mux_offset, ctrl_index, force_input);
    } else {
        return SDRV_PINCTRL_BAD_PIN;
    }
}

/**
 * @brief Config mode select for specific pin.
 *
 * This function config mode select register for one pin.
 * normal/down coversion only valid for Analog-digital combo IO (GPIOA/B/C)
 * normal: passes pad signal as is
 * down coversion: enable down coversion of pad signal before passing to core
 *
 * @param [in] pinctrl PINCTRL contoller instance.
 * @param [in] pin_index defined pin macro in pinctrl.h.
 * @param [in] mode_select mode select defined in pin_mode_select_e.
 */
status_t sdrv_pinctrl_ctrl_set_mode_select(sdrv_pinctrl_t *pinctrl, uint32_t pin_index, pin_mode_select_e mode_select)
{
    uint32_t ctrl_index;

    if (!pinctrl) {
        return SDRV_PINCTRL_CTRL_NULL;
    }

    if ((pin_index >= pinctrl->pin_start) &&
        (pin_index < (pinctrl->pin_start + pinctrl->pin_num))) {
        ctrl_index = pin_index - pinctrl->pin_start;
        return do_sdrv_pinctrl_set_mode_select(pinctrl->base, pinctrl->mux_offset,
                pinctrl->pad_offset, ctrl_index, mode_select);
    } else {
        return SDRV_PINCTRL_BAD_PIN;
    }
}

/**
 * @brief Clear display mux config.
 *
 * This function used to clear display mux config. If GPIO_L0 - GPIO_L9 not use for LVDS,
 * you must call this function.
 *
 * @param [in] pinctrl PINCTRL contoller instance.
 */
status_t sdrv_pinctrl_ctrl_clear_dispmux_config(sdrv_pinctrl_t *pinctrl)
{
#if CONFIG_E3 || CONFIG_D3
    if (!pinctrl) {
        return SDRV_PINCTRL_CTRL_NULL;
    }

    if (pinctrl->disp_mux_base) {
        do_sdrv_pinctrl_clear_dispmux_config(pinctrl->disp_mux_base);
    }
#endif

    return SDRV_STATUS_OK;
}

/**
 * @brief Config pin to high resistance.
 *
 * This function use to config pin as high resistance.
 *
 * @param [in] pinctrl PINCTRL contoller instance.
 * @param [in] gpio GPIO contoller instance.
 * @param [in] pin_index defined pin macro in pinctrl.h.
 */
status_t sdrv_pinctrl_ctrl_set_high_resistance(sdrv_pinctrl_t *pinctrl, sdrv_gpio_t *gpio, uint32_t pin_index)
{
    status_t ret;

    if (!pinctrl) {
        return SDRV_PINCTRL_CTRL_NULL;
    }

    CALL_RET_CHECK(ret, sdrv_pinctrl_ctrl_set_mux(pinctrl, pin_index, PIN_MUX_ALT0));
    CALL_RET_CHECK(ret, sdrv_pinctrl_ctrl_set_pull(pinctrl, pin_index, PIN_NOPULL));
    CALL_RET_CHECK(ret, sdrv_pinctrl_ctrl_set_opendrain(pinctrl, pin_index, PIN_PUSH_PULL));
    CALL_RET_CHECK(ret, sdrv_pinctrl_ctrl_set_force_input(pinctrl, pin_index, PIN_FORCE_INPUT_LOW));
    CALL_RET_CHECK(ret, sdrv_gpio_ctrl_set_pin_direction(gpio, pin_index, GPIO_DIR_IN));

    return SDRV_STATUS_OK;
}
