/**
 * @file sdrv_i2c_reg.h
 * @i2c register head file.
 *
 * @Copyright (c) 2022 Semidrive Semiconductor.
 * @All rights reserved.
 *
 **/

#ifndef SDRV_I2C_REG_H_
#define SDRV_I2C_REG_H_

#include <math.h>
#include <types.h>

/* register address */
#define I2C_MCR0      (0x00)
#define I2C_PRDATAINJ (0x04)
#define I2C_MCR2      (0x08)
#define I2C_MCR3      (0x0C)
#define I2C_MCR4      (0x10)
#define I2C_MSR0      (0x18)
#define I2C_MSR1      (0x1c)

#define I2C_INTR0   (0x20)
#define I2C_INTR1   (0x24)
#define I2C_INTR2   (0x28)
#define I2C_INTR3   (0x2C)
#define I2C_INTEN0  (0x30)
#define I2C_INTEN1  (0x34)
#define I2C_INTEN2  (0x38)
#define I2C_INTEN3  (0x3C)

#define I2C_CMDCSR0 (0x40)
#define I2C_CMDCSR1 (0x44)
#define I2C_CMDCSR2 (0x48)
#define I2C_CMDCSR3 (0x4C)

#define I2C_FCR     (0x50)
#define I2C_FSR     (0x60)
#define I2C_DMACR   (0x70)
#define I2C_DMASR   (0x74)

#define I2C_PCR0    (0x80)
#define I2C_PCR1    (0x84)
#define I2C_PCR2    (0x88)
#define I2C_PCR3    (0x8C)
#define I2C_PCR4    (0x90)
#define I2C_PCR5    (0x94)
#define I2C_PCR6    (0x98)
#define I2C_PCR7    (0x9C)
#define I2C_PCR8    (0xA0)
#define I2C_PCR9    (0xA4)
#define I2C_PCR10   (0xA8)
#define I2C_PCR11   (0xAC)
#define I2C_PCR12   (0xB0)
#define I2C_PCR13   (0xB4)
#define I2C_PCR14   (0xB8)
#define I2C_PCR15   (0xBC)
#define I2C_PSR0    (0x100)
#define I2C_PSR1    (0x104)
#define I2C_PSR2    (0x108)
#define I2C_PSR3    (0x10c)

#define I2C_TXFIFO  (0x200)
#define I2C_RXFIFO  (0x300)

#define I2C_PARITY_ERR_INT_STAT    (0xE0)
#define I2C_PARITY_ERR_INT_STAT_EN (0xE4)
#define I2C_PARITY_ERR_INT_SIG_EN  (0xE8)

/* adapter opmode bitmask */
#define SDRV_I2C_SLAVE  (0x0)
#define SDRV_SM_SLAVE   (0x1)
#define SDRV_PM_SLAVE   (0x2)
#define SDRV_I2C_MASTER (0x8)

/* adapter speedmode bitmask */
#define SDRV_I2C_NSPEED (0x0)
#define SDRV_I2C_HSPEED (0x3)
#define SDRV_I2C_USPEED (0x4)

/* fifo watermark level */
#define SDRV_I2C_RX_WML (0x1f)
#define SDRV_I2C_TX_WML (0x1f)
#define SDRV_I2C_RXTX_WML (SDRV_I2C_TX_WML | SDRV_I2C_RX_WML << 8)

/* int0 bitmask */
#define SDRV_I2C_INT0_TXFWE           (1 << 0)
#define SDRV_I2C_INT0_RXFWF           (1 << 1)
#define SDRV_I2C_INT0_TXFUDF          (1 << 2)
#define SDRV_I2C_INT0_RXFUDF          (1 << 3)
#define SDRV_I2C_INT0_TXFOVF          (1 << 4)
#define SDRV_I2C_INT0_RXFOVF          (1 << 5)
#define SDRV_I2C_INT0_TXFABT          (1 << 6)
#define SDRV_I2C_INT0_RXFABT          (1 << 7)
#define SDRV_I2C_INT0_SCLSTUCKLOW     (1 << 8)
#define SDRV_I2C_INT0_SDASTUCKLOW     (1 << 9)
#define SDRV_I2C_INT0_BUSCLRPASS      (1 << 10)
#define SDRV_I2C_INT0_BUSCLRERR       (1 << 11)
#define SDRV_I2C_INT0_APBCMDDONE      (1 << 12)
#define SDRV_I2C_INT0_APBCMDABORT     (1 << 13)
#define SDRV_I2C_INT0_SLVWRTRANSDONE  (1 << 14)
#define SDRV_I2C_INT0_SLVRDTRANSDONE  (1 << 15)
#define SDRV_I2C_INT0_SLVWRTRANSABORT (1 << 16)
#define SDRV_I2C_INT0_SLVRDTRANSABORT (1 << 17)
#define SDRV_I2C_INT0_SCLHLDTX        (1 << 18)
#define SDRV_I2C_INT0_SCLHLDRX        (1 << 19)
#define SDRV_I2C_INT0_BUSCLRDET       (1 << 20)

#define SDRV_I2C_INT0_DEFAULT_MASK \
    (SDRV_I2C_INT0_TXFUDF |\
     SDRV_I2C_INT0_RXFUDF |\
     SDRV_I2C_INT0_TXFOVF |\
     SDRV_I2C_INT0_RXFOVF |\
     SDRV_I2C_INT0_TXFABT |\
     SDRV_I2C_INT0_RXFABT |\
     SDRV_I2C_INT0_SCLSTUCKLOW |\
     SDRV_I2C_INT0_SDASTUCKLOW |\
     SDRV_I2C_INT0_APBCMDDONE  |\
     SDRV_I2C_INT0_APBCMDABORT)

#define SDRV_I2C_INT0_ERR_STAT \
    (SDRV_I2C_INT0_TXFUDF |\
     SDRV_I2C_INT0_RXFUDF |\
     SDRV_I2C_INT0_TXFOVF |\
     SDRV_I2C_INT0_RXFOVF |\
     SDRV_I2C_INT0_TXFABT |\
     SDRV_I2C_INT0_RXFABT |\
     SDRV_I2C_INT0_SCLSTUCKLOW |\
     SDRV_I2C_INT0_SDASTUCKLOW |\
     SDRV_I2C_INT0_APBCMDABORT |\
     SDRV_I2C_INT0_SLVWRTRANSABORT |\
     SDRV_I2C_INT0_SLVRDTRANSABORT)

#define SDRV_I2C_INT0_SLV_DEFAULT_MASK \
    (SDRV_I2C_INT0_TXFUDF |\
     SDRV_I2C_INT0_RXFUDF |\
     SDRV_I2C_INT0_TXFOVF |\
     SDRV_I2C_INT0_RXFOVF |\
     SDRV_I2C_INT0_TXFABT |\
     SDRV_I2C_INT0_RXFABT |\
     SDRV_I2C_INT0_SCLSTUCKLOW |\
     SDRV_I2C_INT0_SDASTUCKLOW |\
     SDRV_I2C_INT0_APBCMDABORT |\
     SDRV_I2C_INT0_SLVWRTRANSDONE |\
     SDRV_I2C_INT0_SLVRDTRANSDONE |\
     SDRV_I2C_INT0_SLVWRTRANSABORT|\
     SDRV_I2C_INT0_SLVRDTRANSABORT)

#define SDRV_I2C_INT0_SLV_ERR_STAT \
    (SDRV_I2C_INT0_TXFUDF |\
     SDRV_I2C_INT0_RXFUDF |\
     SDRV_I2C_INT0_TXFOVF |\
     SDRV_I2C_INT0_RXFOVF |\
     SDRV_I2C_INT0_TXFABT |\
     SDRV_I2C_INT0_RXFABT |\
     SDRV_I2C_INT0_SCLSTUCKLOW |\
     SDRV_I2C_INT0_SDASTUCKLOW |\
     SDRV_I2C_INT0_APBCMDABORT)

/* int2 bitmask */
#define SDRV_I2C_INT2_STARTDET    (1 << 0)
#define SDRV_I2C_INT2_RESTARTDET  (1 << 1)
#define SDRV_I2C_INT2_STOPDET     (1 << 2)
#define SDRV_I2C_INT2_SLVRDREQDET (1 << 18)
#define SDRV_I2C_INT2_SLVWRREQDET (1 << 19)
#define SDRV_I2C_INT2_CTLBYTEDET  (1 << 28)

#define SDRV_I2C_INT2_SLV_MASK \
    (SDRV_I2C_INT2_SLVRDREQDET | SDRV_I2C_INT2_SLVWRREQDET)

/* int3 bitmask */
#define SDRV_I2C_INT3_DMAEOBAERR     (1 << 0)
#define SDRV_I2C_INT3_DMAEOBCERR     (1 << 1)
#define SDRV_I2C_INT3_DMABWUNCERR    (1 << 2)
#define SDRV_I2C_INT3_DMABWCORERR    (1 << 3)
#define SDRV_I2C_INT3_DMABWFATALERR  (1 << 4)
#define SDRV_I2C_INT3_PCTL0UNCERR    (1 << 16)
#define SDRV_I2C_INT3_PCTL1UNCERR    (1 << 17)
#define SDRV_I2C_INT3_PUSERUNCERR    (1 << 18)
#define SDRV_I2C_INT3_PADDRUNCERR    (1 << 19)
#define SDRV_I2C_INT3_PWDATACORERR   (1 << 20)
#define SDRV_I2C_INT3_PWDATAUNCERR   (1 << 21)
#define SDRV_I2C_INT3_PWDATAFATALERR (1 << 22)
#define SDRV_I2C_INT3_SELFTESTMODERR (1 << 25)
#define SDRV_I2C_INT3_REGPAREJENERR  (1 << 26)

#define SDRV_I2C_INT3_MASK \
    (SDRV_I2C_INT3_DMAEOBAERR    |\
     SDRV_I2C_INT3_DMAEOBCERR    |\
     SDRV_I2C_INT3_DMABWUNCERR   |\
     SDRV_I2C_INT3_DMABWFATALERR |\
     SDRV_I2C_INT3_PCTL0UNCERR   |\
     SDRV_I2C_INT3_PCTL1UNCERR   |\
     SDRV_I2C_INT3_PUSERUNCERR   |\
     SDRV_I2C_INT3_PADDRUNCERR   |\
     SDRV_I2C_INT3_PWDATAUNCERR  |\
     SDRV_I2C_INT3_PWDATAFATALERR)

/* slave mode cmdcsr bitmask */
#define SDRV_I2C_CMD0_SLV_MASK (0x0)
#define SDRV_I2C_CMD1_SLV_MASK (0x0)
#define SDRV_I2C_CMD2_SLV_MASK (0xffff0000)
#define SDRV_I2C_CMD3_SLV_MASK (0xffff0000)

/* i2c timing calculate model */
#define CAL_BASE(value, clk_khz)     (value * clk_khz / 1000000)
#define CAL_SCLL(value, clk_khz)     (CAL_BASE(value, clk_khz))
#define CAL_TA(value, clk_khz)       (CAL_BASE(value, clk_khz))
#define CAL_DVDAT(value, clk_khz)    (CAL_BASE(value, clk_khz) / 2 - 1)
#define CAL_UNIT(value)              (value)
#define CAL_MEXT(value, clk_khz)     (CAL_BASE(value, clk_khz))
#define CAL_SEXT(value, clk_khz)     (CAL_BASE(value, clk_khz))
#define CAL_STUCK(value, clk_khz)    (value * clk_khz / 65536)
#define CAL_SDARXHLD(value, clk_khz) (CAL_BASE(value, clk_khz))
#define CAL_DATSPL(value)            (value)
#define CAL_BUF(value, clk_khz)      ((uint32_t)(log2(CAL_BASE(value, clk_khz) / 4)))
#define CAL_IDLE(value, clk_khz)     (CAL_BASE(value, clk_khz))

uint8_t  sdrv_i2c_recv_data(paddr_t base);
void     sdrv_i2c_send_data(paddr_t base, uint8_t data);
uint32_t sdrv_i2c_get_psr_stat(paddr_t base, uint32_t psr_reg);
uint32_t sdrv_i2c_get_wspace(paddr_t base);
uint32_t sdrv_i2c_get_rspace(paddr_t base);
uint32_t sdrv_i2c_get_fifo_empty_stat(paddr_t base);
void     sdrv_i2c_set_watermark(paddr_t base, uint32_t wml);
void     sdrv_i2c_clear_fifo(paddr_t base);
void     sdrv_i2c_set_cmdcsr(paddr_t base, uint32_t cmdcsr_reg,
                             uint32_t val);
uint32_t sdrv_i2c_get_cmdcsr(paddr_t base, uint32_t cmdcsr_reg);
void     sdrv_i2c_clear_int(paddr_t base);
void     sdrv_i2c_clear_int_bits(paddr_t base, uint32_t intr_reg, uint32_t bits);
void     sdrv_i2c_disable_int(paddr_t base);
uint32_t sdrv_i2c_get_int_stat(paddr_t base, uint32_t intr_reg);
void     sdrv_i2c_unmask_int(paddr_t base, uint32_t inten_reg, uint32_t int_flag);
void     sdrv_i2c_set_IO(paddr_t base, uint32_t io_reg, uint32_t val);
void     sdrv_i2c_set_speed(paddr_t base, uint32_t clk, uint32_t speed_mode);
void     sdrv_i2c_set_nack(paddr_t base);
void     sdrv_i2c_set_slvaddr(paddr_t base, uint8_t addr);
void     sdrv_i2c_set_opmode(paddr_t base, uint32_t opmode);
uint32_t sdrv_i2c_get_parity_stat(paddr_t base);
void     sdrv_i2c_set_parity_stat(paddr_t base, bool enable);
void     sdrv_i2c_set_parity_sig(paddr_t base, bool enable);
void     sdrv_i2c_set_timing(paddr_t base, uint32_t clk, int idx);
void     sdrv_i2c_disable(paddr_t base);
void     sdrv_i2c_enable(paddr_t base);
int      sdrv_i2c_reset(paddr_t base);
void     sdrv_i2c_dump_reg(paddr_t base, uint32_t *reg_val);
void     sdrv_i2c_write_reg(paddr_t base, uint32_t reg, uint32_t val);
void     sdrv_i2c_read_reg(paddr_t base, uint32_t reg, uint32_t *val);
#endif
