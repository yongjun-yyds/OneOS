/**
 * @file sdrv_wdt_reg.h
 * @brief Sdrv watchdog head source.
 *
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef SDRV_TAISHAN_WDT_REG_H_
#define SDRV_TAISHAN_WDT_REG_H_

#define WDT_CTRL (0x00u)
#define WDT_CTRL_SOFT_RST (0u)
#define WDT_CTRL_WDT_EN (1u)
#define WDT_CTRL_CLK_SRC_LSB (2u)
#define WDT_CTRL_CLK_SRC_MASK (0x7u)
#define WDT_CTRL_WTC_SRC (5u)
#define WDT_CTRL_AUTO_RESTART (6u)
#define WDT_CTRL_DBG_HALT_EN (7u)
#define WDT_CTRL_WDT_EN_SRC (8u)
#define WDT_CTRL_SELFTEST_TRIG (9u)
#define WDT_CTRL_WDT_EN_STA (10u)
#define WDT_CTRL_PRE_DIV_NUM_LSB (16u)
#define WDT_CTRL_PRE_DIV_NUM_MASK (0xFFFFu)

#define WDT_WTC (0x04u)
#define WDT_WTC_VAL_MASK (0xFFFFFFFFu)

#define WDT_WRC_CTRL (0x08u)
#define WDT_WRC_CTRL_MODEM0 (0u)
#define WDT_WRC_CTRL_MODEM1 (1u)
#define WDT_WRC_CTRL_SEQ_REFR (2u)
#define WDT_WRC_CTRL_REFR_TRIG (3u)

#define WDT_WRC_VAL (0x0Cu)
#define WDT_WRC_VAL_MASK (0xFFFFFFFFu)

#define WDT_WRC_SEQ (0x10u)
#define WDT_WRC_SEQ_MASK (0xFFFFFFFFu)

#define WDT_RST_CTRL (0x14u)
#define WDT_RST_CTRL_RST_CNT_MASK (0xFFFFu)
#define WDT_RST_CTRL_INT_RST_EN (16u)
#define WDT_RST_CTRL_INT_RST_MODE (17u)
#define WDT_RST_CTRL_WDT_RST_EN (18u)
#define WDT_RST_CTRL_RST_WIN_LSB (20u)
#define WDT_RST_CTRL_RST_WIN_MASK (0xFFu)

#define WDT_EXT_RST_CTRL (0x18u)
#define WDT_EXT_RST_CTRL_RST_CNT_MASK (0xFFFFu)
#define WDT_EXT_RST_CTRL_EXT_RST_EN (16u)
#define WDT_EXT_RST_CTRL_EXT_RST_MODE (17u)
#define WDT_EXT_RST_CTRL_RST_WIN_LSB (20u)
#define WDT_EXT_RST_CTRL_RST_WIN_MASK (0xFFu)
#define WDT_EXT_RST_CTRL_EXT_RST_REQ_POL (28u)

#define WDT_CNT (0x1Cu)
#define WDT_TWS (0x20u)

#define WDT_INT (0x24u)
#define WDT_INT_ILL_WIN_REFE_INT_EN (0u)
#define WDT_INT_ILL_SEQ_REFE_INT_EN (1u)
#define WDT_INT_OVERFLOW_INT_EN (2u)
#define WDT_INT_ILL_WIN_REFE_INT_STA (3u)
#define WDT_INT_ILL_SEQ_REFE_INT_STA (4u)
#define WDT_INT_OVERFLOW_INT_STA (5u)
#define WDT_INT_ILL_WIN_REFE_INT_CLR (6u)
#define WDT_INT_ILL_SEQ_REFE_INT_CLR (7u)
#define WDT_INT_OVERFLOW_INT_CLR (8u)

#define WDT_RST_REQ_MON (0x28u)

#define WDT_LOCK (0x40u)
#define WDT_LOCK_CTL_LOCK (0u)
#define WDT_LOCK_WTC_LOCK (1u)
#define WDT_LOCK_WRC_LOCK (2u)
#define WDT_LOCK_RST_LOCK (3u)
#define WDT_LOCK_EXT_RST_LOCK (4u)
#define WDT_LOCK_INT_LOCK (5u)
#define WDT_LOCK_CLK_SRC_LOCK (6u)


#endif /* SDRV_TAISHAN_WDT_REG_H_ */
