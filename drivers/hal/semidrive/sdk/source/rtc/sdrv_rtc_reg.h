/**
 * @file sdrv_rtc_reg.h
 * @brief sdrv rtc_reg api header.
 *
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */
#ifndef _SDRV_RTC_REG_H_
#define _SDRV_RTC_REG_H_

#include <stdint.h>

#ifdef __cplusplus
#define __I volatile /**< Defines 'read-only'  permissions */
#else
#define __I volatile const /**< Defines 'read-only'  permissions */
#endif
#define __O volatile  /**< Defines 'write-only' permissions */
#define __IO volatile /**< Defines 'read/write' permissions */

#if !(defined(__ASSEMBLY__) || defined(__IAR_SYSTEMS_ASM__))
typedef struct {
    __IO uint32_t SEC_RTC_CTRL;               /*offset 0*/
    __IO uint32_t RTC_H;                      /*offset 4*/
    __IO uint32_t RTC_L;                      /*offset 8*/
    __IO uint32_t AUTO_ADJUST;                /*offset c*/
    __IO uint32_t TIMER_H;                    /*offset 10*/
    __IO uint32_t TIMER_L;                    /*offset 14*/
    __IO uint32_t WAKEUP_CTRL;                /*offset 18*/
    __IO uint32_t PERIODICAL_CTRL;            /*offset 1c*/
    __IO uint32_t VIOLATION_INT;              /*offset 20*/
    __IO uint32_t APB_MONITOR_ERR_INJ_EN;     /*offset 24*/
    __IO uint32_t APB_MONITOR_ERR_INJ_ECC;    /*offset 28*/
    __IO uint32_t APB_MONITOR_ERR_INJ_WDATA;  /*offset 2C*/
    __IO uint32_t SAFETY_ERR_IRQ_STAT;        /*offset 0x30*/
    __IO uint32_t SAFETY_ERR_IRQ_STAT_EN;     /*offset 0x34*/
    __IO uint32_t SAFETY_ERR_IRQ_SIG_EN;      /*offset 0x38*/
    __IO uint32_t IRQ_ERR_INJ;                /*offset 0x3C*/
    __IO uint32_t IRQ_ERR_EJ_EN;              /*offset 0x40*/
    __IO uint32_t GP0;                        /*offset 0x44*/
    __IO uint32_t GP1;                        /*offset 0X48*/
    __IO uint32_t GP2;                        /*offset 0x4c*/
    __IO uint32_t GP3;                        /*offset 0x50*/
    __IO uint32_t RTC_H_HOLD_SHADOW;          /*offset 0x54*/
    __IO uint32_t RTC_L_HOLD_SHADOW;          /*offset 0x58*/
    __IO uint32_t RTC_WORKING_MODE;           /*offset 0x5c*/
    __IO uint32_t RTC_REGISTER_CROSS_CLOCK;   /*offset 60*/
    __IO uint32_t RTC_PARITY_ERR_INT_STAT;    /*offset 0x64*/
    __IO uint32_t RTC_PARITY_ERR_INT_STAT_EN; /*offset 0x68*/
    __IO uint32_t RTC_PARITY_ERR_INT_SIG_EN;  /*offset 0x6c*/
    __IO uint32_t PRDATAINJ;                  /*offset 0x70*/
    __IO uint32_t RTC_OUT_ERR_INJ;            /*offset 0x74*/
} Rtc;
#endif

#define BM_SEC_RTC_CTRL_LOCK    (0x01U << 31U)
#define BM_SEC_RTC_CTRL_RTC_LOCAL_ENABLE    (0x01U << 0U)
#define BM_WAKEUP_CTRL_STATUS   (0x01U << 3U)
#define BM_WAKEUP_CTRL_ENABLE   (0x01U << 0U)
#define BM_WAKEUP_CTRL_IRQ_ENABLE   (0x01U << 1U)
#define BM_WAKEUP_CTRL_REQ_ENABLE   (0x01U << 2U)
#define BM_WAKEUP_CTRL_CLEAR    (0x01U << 4U)
#define BM_SEC_RTC_CTRL_SECURE_ENABLE   (0x01U << 1U)
#define BM_SEC_RTC_CTRL_PRIVILEGE_ENABLE    (0x01U << 2U)
#define BM_RTC_REGISTER_CROSS_CLOCK_REGISTER_CROSS_CLOCK_EN (0x01U << 0U)
#define BM_WAKEUP_CTRL_OVERFLOW_CLEAR_BIT   (5U)
#define BM_WAKEUP_CTRL_DISABLE_CLEAR_BIT    (6U)
#define BM_RTC_WORKING_MODE_RTC_MASTER_SLAVE    (0x01U << 0U)
#define BM_RTC_WORKING_MODE_RTC_COUNT_MODE  (0x01U << 1U)
#define BM_VIOLATION_INT_OVERFLOW_STATS (0x01U << 2U)
#define BM_VIOLATION_INT_OVERFLOW_STATS_BIT (2U)
#define BM_PERIODICAL_CRTL_IRQ_ENABLE   (0x01U << 0U)
#define BM_VIOLATION_INT_DISABLE_STATUS (0x01U << 3U)
#define FM_VIOLATION_INT_MASK   (0x3U << 0U)
#define FV_VIOLATION_INT_MASK(v) \
    (((v) << 0U) & FM_VIOLATION_INT_MASK)

#define FM_PERIODICAL_CRTL_FREQ (0xfU << 3U)
#define FV_PERIODICAL_CRTL_FREQ(v) \
    (((v) << 3U) & FM_PERIODICAL_CRTL_FREQ)

#define FM_PERIODICAL_CRTL_FREQ (0xfU << 3U)
#define GFV_PERIODICAL_CRTL_FREQ(v) \
    (((v) & FM_PERIODICAL_CRTL_FREQ) >> 3U)

void sec_rtc_set_cross_clk_en(Rtc *rtc);
void sec_rtc_set_rtc_ctrl(Rtc *rtc, uint32_t ctrl);
void sec_rtc_wakeup_enable(Rtc *rtc, uint8_t vtype, uint8_t en);
void sec_rtc_clear_wakeup_status(Rtc *rtc);
void sec_rtc_clear_violation_status(Rtc *rtc, uint8_t vio);
void sec_rtc_set_violation_enable(Rtc *rtc, uint8_t mask);
void sec_rtc_set_violation_disable(Rtc *rtc, uint8_t mask);
uint64_t sec_rtc_get_tick(Rtc *rtc);
uint8_t sec_rtc_get_violation_status(Rtc *rtc);
void sec_rtc_enable_periodical(Rtc *rtc, uint8_t en);
int sec_rtc_local_enable(Rtc *rtc, bool en);
void sec_rtc_set_tick(Rtc *rtc, uint64_t v);
uint64_t sec_rtc_get_alarm_tick(Rtc *rtc);
uint8_t sec_rtc_is_alarm_enable(Rtc *rtc);
bool sec_rtc_get_wakeup_status(Rtc *rtc);
void sec_rtc_set_alarm_tick(Rtc *rtc, uint64_t val);
uint64_t sec_rtc_get_hld_tick(Rtc *rtc, uint64_t *tick);
uint32_t sec_rtc_read_general_purpose_reg(Rtc *rtc, uint8_t gp_num);
void sec_rtc_write_general_purpose_reg(Rtc *rtc, uint8_t gp_num, uint32_t value);
void sec_rtc_lock(Rtc *rtc);
#endif /*_SDRV_RTC_REG_H_*/
