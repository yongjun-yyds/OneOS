/******************************************************************************
  * @project: LT9211
  * @file: lt9211.c
  * @author: zll
  * @company: LONTIUM COPYRIGHT and CONFIDENTIAL
  * @date:
        1��2019.04.10����2022.2.22 modify RXPLL to Adaptive BW tracking PLL.
 8225 to 0x07 &8227 to 0x32 2��V4.2 2022.5.7 add the 8223 to 0x02,disable the
 BTA function
 ******************************************************************************/
#include <clock_ip.h>
#include <i2c_cfg.h>
#include <pinmux_cfg.h>
#include <sdrv_gpio.h>
#include <sdrv_i2c.h>

#include "lvds_lt9211_1port_to_2port.h"

#include "i2c_cfg.h"

#define BACKLIGHT_GPIO GPIO_E5
#define LT9211_RESET_GPIO GPIO_E15

sdrv_i2c_t lt9211_ctrl = {
    .cfg = &g_i2c8_cfg,
};

/* define i2c bus stat and transfer stat instance */
sdrv_i2cdrv_bus_stat_t bus_stat;
sdrv_i2cdrv_trans_stat_t trans_stat;

uint16_t i2c_slave_addr = 0x2D;

uint16_t hact, vact;
uint16_t hs, vs;
uint16_t hbp, vbp;
uint16_t htotal, vtotal;
uint16_t hfp, vfp;
uint8_t VideoFormat = 0;
uint32_t lvds_clk_in = 0;

#define OFFSET 5

struct video_timing *pVideo_Format;

// hfp, hs, hbp, hact, htotal, vfp, vs, vbp, vact, vtotal, pixclk

struct video_timing video_1920x1080_60Hz = {88, 44, 148,  1920, 2200,  4,
                                            5,  36, 1080, 1125, 148500};
struct video_timing video_1280x720_60Hz = {110, 40, 220, 1280, 1650, 5,
                                           5,   20, 720, 750,  74250};

struct video_timing video_1366x768_60Hz = {26, 110, 110, 1366, 1592, 13,
                                           6,  13,  768, 800,  81000};

void LT9211_Reset(void)
{
    sdrv_gpio_set_pin_output_level(LT9211_RESET_GPIO, false);
    mdelay(100);
    sdrv_gpio_set_pin_output_level(LT9211_RESET_GPIO, true);
    mdelay(100);
}

int lt9211_i2c_write(sdrv_i2c_t *ctrl, uint16_t addr, uint8_t reg,
                     uint8_t value)
{
    int ret = 0;

    uint8_t wbuf[2] = {0};

    wbuf[0] = reg;
    wbuf[1] = value;

    sdrv_i2c_write(ctrl, addr, wbuf, 2, 1000, true);

    if (sdrv_i2c_get_trans_stat(ctrl) == SDRV_I2CDRV_TRANS_OK) {
        ret = 0;
    } else {
        ret = -1;
        ssdk_printf(SSDK_INFO, "i2c write failed\r\n");
    }

    return ret;
}

int lt9211_i2c_write_read(sdrv_i2c_t *ctrl, uint16_t addr, uint8_t reg,
                          uint8_t *data)
{
    int ret = 0;

    uint8_t wbuf[1] = {0};
    wbuf[0] = reg;

    sdrv_i2c_write(ctrl, addr, wbuf, 1, 1000, false);

    if (sdrv_i2c_get_trans_stat(ctrl) == SDRV_I2CDRV_TRANS_OK) {
        ret = 0;
    } else {
        ret = -1;
        ssdk_printf(SSDK_INFO, "i2c write failed\r\n");
        return ret;
    }

    sdrv_i2c_read(ctrl, addr, data, 1, 1000, true);

    if (sdrv_i2c_get_trans_stat(ctrl) == SDRV_I2CDRV_TRANS_OK) {
        ret = 0;
    } else {
        ret = -1;
        ssdk_printf(SSDK_INFO, "i2c read failed\r\n");
    }

    return ret;
}

bool HDMI_WriteI2C_Byte(uint8_t RegAddr, uint8_t data)
{
    int ret = 0;

    ret = lt9211_i2c_write(&lt9211_ctrl, i2c_slave_addr, RegAddr, data);

    return ret;
}

uint8_t HDMI_ReadI2C_Byte(uint8_t RegAddr)
{
    uint8_t value = 0;

    lt9211_i2c_write_read(&lt9211_ctrl, i2c_slave_addr, RegAddr, &value);
    return value;
}

void LT9211_ChipID(void)
{
    HDMI_WriteI2C_Byte(0xff, 0x81); // register bank
    DISPSS_LOG_INFO("LT9211 Chip ID:%x, %x, %x", HDMI_ReadI2C_Byte(0x00),
                    HDMI_ReadI2C_Byte(0x01), HDMI_ReadI2C_Byte(0x02));
}

/** video chk soft rst **/
void lt9211_vid_chk_rst(void)
{
    HDMI_WriteI2C_Byte(0xff, 0x81);
    HDMI_WriteI2C_Byte(0x10, 0xbe);
    mdelay(10);
    HDMI_WriteI2C_Byte(0x10, 0xfe);
}

/** lvds rx logic rst **/
void lt9211_lvdsrx_logic_rst(void)
{
    HDMI_WriteI2C_Byte(0xff, 0x81);
    HDMI_WriteI2C_Byte(0x0c, 0xeb);
    mdelay(10);
    HDMI_WriteI2C_Byte(0x0c, 0xfb);
}

void LT9211_SystemInt(void)
{
    /* system clock init */
    HDMI_WriteI2C_Byte(0xff, 0x82);
    HDMI_WriteI2C_Byte(0x01, 0x18);

    HDMI_WriteI2C_Byte(0xff, 0x86);
    HDMI_WriteI2C_Byte(0x06, 0x61);
    HDMI_WriteI2C_Byte(0x07, 0xa8); // fm for sys_clk

    HDMI_WriteI2C_Byte(0xff, 0x87); //��ʼ�� txpll �Ĵ����б�Ĭ��ֵ������
    HDMI_WriteI2C_Byte(0x14, 0x08); // default value
    HDMI_WriteI2C_Byte(0x15, 0x00); // default value
    HDMI_WriteI2C_Byte(0x18, 0x0f);
    HDMI_WriteI2C_Byte(0x22, 0x08); // default value
    HDMI_WriteI2C_Byte(0x23, 0x00); // default value
    HDMI_WriteI2C_Byte(0x26, 0x0f);
}

void LT9211_LvdsRxPhy(void)
{
#ifdef INPUT_PORTA
    DISPSS_LOG_INFO("Port A PHY Config");
    HDMI_WriteI2C_Byte(0xff, 0x82);
    HDMI_WriteI2C_Byte(0x02, 0x8B); // Port A LVDS mode enable
    HDMI_WriteI2C_Byte(0x05, 0x21); // port A CLK lane swap
    HDMI_WriteI2C_Byte(0x07, 0x1f); // port A clk enable
    HDMI_WriteI2C_Byte(0x04, 0xa0); // select port A clk as byteclk
    // HDMI_WriteI2C_Byte(0x09,0xFC); //port A P/N swap

    HDMI_WriteI2C_Byte(0xff, 0x86);
    HDMI_WriteI2C_Byte(0x33, 0xe4); // Port A Lane swap
#endif

#ifdef INPUT_PORTB
    DISPSS_LOG_INFO("Port B PHY Config");
    HDMI_WriteI2C_Byte(0xff, 0x82);
    HDMI_WriteI2C_Byte(0x02, 0x88); // Port A/B LVDS mode enable
    HDMI_WriteI2C_Byte(0x05, 0x21); // port A CLK lane swap and rterm turn-off
    HDMI_WriteI2C_Byte(0x0d, 0x21); // port B CLK lane swap
    HDMI_WriteI2C_Byte(
        0x07, 0x1f); // port A clk enable  (ֻ��Portbʱ,porta��lane0 clkҪ��)
    HDMI_WriteI2C_Byte(0x0f, 0x1f); // port B clk enable
    // HDMI_WriteI2C_Byte(0x10,0x00);   //select port B clk as byteclk
    HDMI_WriteI2C_Byte(0x04, 0xa1); // reserve
    // HDMI_WriteI2C_Byte(0x11,0x01);   //port B P/N swap
    HDMI_WriteI2C_Byte(0x10, 0xfc);

    HDMI_WriteI2C_Byte(0xff, 0x86);
    HDMI_WriteI2C_Byte(0x34, 0xe4); // Port B Lane swap

    HDMI_WriteI2C_Byte(0xff, 0xd8);
    HDMI_WriteI2C_Byte(0x16, 0x80);
#endif

    HDMI_WriteI2C_Byte(0xff, 0x81);
    HDMI_WriteI2C_Byte(0x20, 0x7f);
    HDMI_WriteI2C_Byte(0x20, 0xff); // mlrx calib reset
}

void LT9211_LvdsRxDigital(void)
{
    HDMI_WriteI2C_Byte(0xff, 0x85);
    HDMI_WriteI2C_Byte(0x88, 0x10); // LVDS input, MIPI output

    HDMI_WriteI2C_Byte(0xff, 0xd8);

    if (INPUT_PORT_NUM == 1) { // 1Port LVDS Input
        HDMI_WriteI2C_Byte(0x10, 0x80);
        DISPSS_LOG_INFO("LVDS Port Num: 1");
    } else if (INPUT_PORT_NUM == 2) { // 2Port LVDS Input
        HDMI_WriteI2C_Byte(0x10, 0x00);
        DISPSS_LOG_INFO("LVDS Port Num: 2");
    } else {
        DISPSS_LOG_INFO("Port Num Set Error");
    }

    lt9211_vid_chk_rst();      // video chk soft rst
    lt9211_lvdsrx_logic_rst(); // lvds rx logic rst

    HDMI_WriteI2C_Byte(0xff, 0x86);
    HDMI_WriteI2C_Byte(0x30, 0x45); // port AB input port sel

    if (LVDS_FORMAT == JEDIA_FORMAT) {
        HDMI_WriteI2C_Byte(0xff, 0x85);
        HDMI_WriteI2C_Byte(0x59, 0xd0);
        HDMI_WriteI2C_Byte(0xff, 0xd8);
        HDMI_WriteI2C_Byte(0x11, 0x40);
    }
}

void LT9211_ClockCheckDebug(void)
{
#ifdef _uart_debug_
    uint32_t fm_value;

    lvds_clk_in = 0;
#ifdef INPUT_PORTA
    HDMI_WriteI2C_Byte(0xff, 0x86);
    HDMI_WriteI2C_Byte(0x00, 0x01);
    mdelay(50);
    fm_value = 0;
    fm_value = (HDMI_ReadI2C_Byte(0x08) & (0x0f));
    fm_value = (fm_value << 8);
    fm_value = fm_value + HDMI_ReadI2C_Byte(0x09);
    fm_value = (fm_value << 8);
    fm_value = fm_value + HDMI_ReadI2C_Byte(0x0a);
    DISPSS_LOG_INFO("Port A lvds clock: %d", fm_value);
    lvds_clk_in = fm_value;
#endif

#ifdef INPUT_PORTB
    HDMI_WriteI2C_Byte(0xff, 0x86);
    HDMI_WriteI2C_Byte(0x00, 0x02);
    mdelay(50);
    fm_value = 0;
    fm_value = (HDMI_ReadI2C_Byte(0x08) & (0x0f));
    fm_value = (fm_value << 8);
    fm_value = fm_value + HDMI_ReadI2C_Byte(0x09);
    fm_value = (fm_value << 8);
    fm_value = fm_value + HDMI_ReadI2C_Byte(0x0a);
    mdelay("Port B lvds clock: %d\n", fm_value);
    lvds_clk_in = fm_value;
#endif

#endif
}

void LT9211_LvdsRxPll(void)
{
    uint8_t loopx = 0;

    HDMI_WriteI2C_Byte(0xff, 0x82);
    HDMI_WriteI2C_Byte(0x25, 0x07);
    HDMI_WriteI2C_Byte(0x27, 0x32);

    if (INPUT_PORT_NUM == 1) { // 1Port LVDS Input
        HDMI_WriteI2C_Byte(0x24,
                           0x24); // RXPLL_LVDSCLK_MUXSEL,PIXCLK_MUXSEL	0x2c.
        HDMI_WriteI2C_Byte(0x28, 0x44);
    } else if (INPUT_PORT_NUM == 2) { // 2Port LVDS Input
        HDMI_WriteI2C_Byte(0x24,
                           0x2c); // RXPLL_LVDSCLK_MUXSEL,PIXCLK_MUXSEL	0x2c.
        HDMI_WriteI2C_Byte(0x28, 0x64); // 0x64
    } else {
        DISPSS_LOG_INFO("LvdsRxPll: lvds port count error");
    }
    mdelay(10);
    HDMI_WriteI2C_Byte(0xff, 0x81);
    HDMI_WriteI2C_Byte(0x20, 0xdf); // rx pll reset
    HDMI_WriteI2C_Byte(0x20, 0xff);
    mdelay(10);
    for (loopx = 0; loopx < 10; loopx++) { // Check Rx PLL lock
        HDMI_WriteI2C_Byte(0xff, 0x87);
        if (HDMI_ReadI2C_Byte(0x12) & 0x80) {
            //            if(HDMI_ReadI2C_Byte(0x11)& 0x80)
            //            {
            //                print("\r\nLT9211 rx cal done");
            //            }
            //            else
            //            {
            //                print("\r\nLT9211 rx cal undone!!");
            //            }
            DISPSS_LOG_INFO("LT9211 rx pll lock");
            break;
        } else {
            DISPSS_LOG_INFO("LT9211 rx pll unlocked");
        }
    }
}

void LT9211_VideoCheck(void)
{
    uint8_t sync_polarity;

    HDMI_WriteI2C_Byte(0xff, 0x86);
    HDMI_WriteI2C_Byte(0x20, 0x00);

    sync_polarity = HDMI_ReadI2C_Byte(0x70);
    vs = HDMI_ReadI2C_Byte(0x71);

    hs = HDMI_ReadI2C_Byte(0x72);
    hs = (hs << 8) + HDMI_ReadI2C_Byte(0x73);

    vbp = HDMI_ReadI2C_Byte(0x74);
    vfp = HDMI_ReadI2C_Byte(0x75);

    hbp = HDMI_ReadI2C_Byte(0x76);
    hbp = (hbp << 8) + HDMI_ReadI2C_Byte(0x77);

    hfp = HDMI_ReadI2C_Byte(0x78);
    hfp = (hfp << 8) + HDMI_ReadI2C_Byte(0x79);

    vtotal = HDMI_ReadI2C_Byte(0x7A);
    vtotal = (vtotal << 8) + HDMI_ReadI2C_Byte(0x7B);

    htotal = HDMI_ReadI2C_Byte(0x7C);
    htotal = (htotal << 8) + HDMI_ReadI2C_Byte(0x7D);

    vact = HDMI_ReadI2C_Byte(0x7E);
    vact = (vact << 8) + HDMI_ReadI2C_Byte(0x7F);

    hact = HDMI_ReadI2C_Byte(0x80);
    hact = (hact << 8) + HDMI_ReadI2C_Byte(0x81);

    DISPSS_LOG_INFO("sync_polarity = %x", sync_polarity);
    if (!(sync_polarity & 0x01)) { // hsync
        HDMI_WriteI2C_Byte(0xff, 0xd8);
        HDMI_WriteI2C_Byte(0x10, (HDMI_ReadI2C_Byte(0x10) | 0x10));
    }

    if (!(sync_polarity & 0x02)) { // vsync
        HDMI_WriteI2C_Byte(0xff, 0xd8);
        HDMI_WriteI2C_Byte(0x10, (HDMI_ReadI2C_Byte(0x10) | 0x20));
    }

    DISPSS_LOG_INFO("hfp = %d, hs = %d, hbp = %d, hact = %d, htotal = %d", hfp,
                    hs, hbp, hact, htotal);
    DISPSS_LOG_INFO("vfp = %d, vs = %d, vbp = %d, vact = %d, vtotal = %d", vfp,
                    vs, vbp, vact, vtotal);

    if ((hact == video_1920x1080_60Hz.hact) &&
        (vact == video_1920x1080_60Hz.vact)) {
        DISPSS_LOG_INFO("video_1920x1080_60Hz");
        pVideo_Format = &video_1920x1080_60Hz;
    } else if ((hact == video_1280x720_60Hz.hact) &&
               (vact == video_1280x720_60Hz.vact)) {
        DISPSS_LOG_INFO("video_1280x720_60Hz");
        pVideo_Format = &video_1280x720_60Hz;
    } else if ((hact == video_1366x768_60Hz.hact) &&
               (vact == video_1366x768_60Hz.vact)) {
        DISPSS_LOG_INFO("video_1366x768_60Hz");
        pVideo_Format = &video_1366x768_60Hz;
    } else {
        pVideo_Format = NULL;
        DISPSS_LOG_INFO("video_none");
    }
}

void LT9211_TxPhy(void)
{
    HDMI_WriteI2C_Byte(0xff, 0x82);
    HDMI_WriteI2C_Byte(0x23, 0x02); // disable the BTA
    HDMI_WriteI2C_Byte(0x62, 0x00); // ttl output disable
    if (LVDS_PORTNUM == LVDS_2PORT) {
        HDMI_WriteI2C_Byte(0x3b, 0x88); // dual-port lvds output disable
    } else {
        HDMI_WriteI2C_Byte(0x3b, 0x08); // signal-port lvds output disable
    }
    HDMI_WriteI2C_Byte(0x3e, 0x92);
    HDMI_WriteI2C_Byte(0x3f, 0x48);
    HDMI_WriteI2C_Byte(0x40, 0x31);
    HDMI_WriteI2C_Byte(0x43, 0x80);
    HDMI_WriteI2C_Byte(0x44, 0x00);
    HDMI_WriteI2C_Byte(0x45, 0x00);
    HDMI_WriteI2C_Byte(0x49, 0x00);
    HDMI_WriteI2C_Byte(0x4a, 0x01);
    HDMI_WriteI2C_Byte(0x4e, 0x00);
    HDMI_WriteI2C_Byte(0x4f, 0x00);
    HDMI_WriteI2C_Byte(0x50, 0x00);
    HDMI_WriteI2C_Byte(0x53, 0x00);
    HDMI_WriteI2C_Byte(0x54, 0x01);

    HDMI_WriteI2C_Byte(0xff, 0x86);
    HDMI_WriteI2C_Byte(0x46, 0x10);
#ifdef LVDS_2PORT_SWAP
    DISPSS_LOG_INFO("LVDS Output Port Swap!");
    HDMI_WriteI2C_Byte(0x46, 0x40);
#endif

    HDMI_WriteI2C_Byte(0xff, 0x81);
    HDMI_WriteI2C_Byte(0x20, 0x79); // TX PHY RESET & mlrx mltx calib reset
    HDMI_WriteI2C_Byte(0x20, 0xff);
}

void LT9211_TxDigital(void)
{
    DISPSS_LOG_INFO("LT9211 LVDS_OUTPUT_MODE: ");
    HDMI_WriteI2C_Byte(0xff, 0x85); /* lvds tx controller */
    HDMI_WriteI2C_Byte(0x59, 0x40);
    if (LVDS_DATAFORMAT == VESA) {
        DISPSS_LOG_INFO("Data Format: VESA");
        HDMI_WriteI2C_Byte(0x59, (HDMI_ReadI2C_Byte(0x59) & 0x7f));
    } else if (LVDS_DATAFORMAT == JEIDA) {
        DISPSS_LOG_INFO("Data Format: JEIDA");
        HDMI_WriteI2C_Byte(0x59, (HDMI_ReadI2C_Byte(0x59) | 0x80));
    }
    if (LVDS_COLORDEPTH == DEPTH_6BIT) {
        DISPSS_LOG_INFO("ColorDepth: 6Bit");
        HDMI_WriteI2C_Byte(0x59, (HDMI_ReadI2C_Byte(0x59) & 0xef));
    } else if (LVDS_COLORDEPTH == DEPTH_8BIT) {
        DISPSS_LOG_INFO("ColorDepth: 8Bit");
        HDMI_WriteI2C_Byte(0x59, (HDMI_ReadI2C_Byte(0x59) | 0x10));
    }
    if (LVDS_MODE == SYNC_MODE) {
        DISPSS_LOG_INFO("LVDS_MODE: Sync Mode");
        HDMI_WriteI2C_Byte(0x59, (HDMI_ReadI2C_Byte(0x59) & 0xdf));
    } else if (LVDS_MODE == DE_MODE) {
        DISPSS_LOG_INFO("LVDS_MODE: De Mode");
        HDMI_WriteI2C_Byte(0x59, (HDMI_ReadI2C_Byte(0x59) | 0x20));
    }

    HDMI_WriteI2C_Byte(0x5a, 0xaa);
    HDMI_WriteI2C_Byte(0x5b, 0xaa);
    if (LVDS_PORTNUM == LVDS_2PORT) {
        DISPSS_LOG_INFO("LVDS Output Port Num: 2Port");
        HDMI_WriteI2C_Byte(0x5c, 0x03); // lvdstx port sel 01:dual;00:single
    } else {
        DISPSS_LOG_INFO("LVDS Output Port Num: 1Port");
        HDMI_WriteI2C_Byte(0x5c, 0x00);
    }
    HDMI_WriteI2C_Byte(0xa1, 0x77);
    HDMI_WriteI2C_Byte(0xff, 0x86);
    HDMI_WriteI2C_Byte(0x40, 0x40); // tx_src_sel
    /*port src sel*/
    HDMI_WriteI2C_Byte(0x41, 0x34);
    HDMI_WriteI2C_Byte(0x42, 0x10);
    HDMI_WriteI2C_Byte(0x43, 0x23); // pt0_tx_src_sel
    HDMI_WriteI2C_Byte(0x44, 0x41);
    HDMI_WriteI2C_Byte(0x45, 0x02); // pt1_tx_src_scl
}

void LT9211_Txpll(void)
{
    uint8_t loopx;

    HDMI_WriteI2C_Byte(0xff, 0x82);
    HDMI_WriteI2C_Byte(0x36, 0x01); // b7:txpll_pd
    if (LVDS_PORTNUM == LVDS_1PORT) {
        HDMI_WriteI2C_Byte(0x37, 0x29);
    } else {
        HDMI_WriteI2C_Byte(0x37, 0x2a);
    }
    HDMI_WriteI2C_Byte(0x38, 0x06);
    HDMI_WriteI2C_Byte(0x39, 0x30);
    HDMI_WriteI2C_Byte(0x3a, 0x8e);

    HDMI_WriteI2C_Byte(0xFF, 0x81);
    HDMI_WriteI2C_Byte(0x20, 0xF7); // LVDS Txpll soft reset
    HDMI_WriteI2C_Byte(0x20, 0xFF);

    HDMI_WriteI2C_Byte(0xff, 0x87);
    HDMI_WriteI2C_Byte(0x37, 0x14);
    HDMI_WriteI2C_Byte(0x13, 0x00);
    HDMI_WriteI2C_Byte(0x13, 0x80);
    mdelay(100);
    for (loopx = 0; loopx < 10; loopx++) { // Check Tx PLL cal
        HDMI_WriteI2C_Byte(0xff, 0x87);
        if (HDMI_ReadI2C_Byte(0x1f) & 0x80) {
            if (HDMI_ReadI2C_Byte(0x20) & 0x80) {
                DISPSS_LOG_INFO("LT9211 tx pll lock");
            } else {
                DISPSS_LOG_INFO("LT9211 tx pll unlocked");
            }
            DISPSS_LOG_INFO("LT9211 tx pll cal done");
            break;
        } else {
            DISPSS_LOG_INFO("LT9211 tx pll unlocked");
        }
    }
}

void LT9211_LvdsClkDebug(void)
{
#ifdef _uart_debug_
    uint32_t fm_value;

    HDMI_WriteI2C_Byte(0xff, 0x86);
    HDMI_WriteI2C_Byte(0x00, 0x12);
    mdelay(100);
    fm_value = 0;
    fm_value = (HDMI_ReadI2C_Byte(0x08) & (0x0f));
    fm_value = (fm_value << 8);
    fm_value = fm_value + HDMI_ReadI2C_Byte(0x09);
    fm_value = (fm_value << 8);
    fm_value = fm_value + HDMI_ReadI2C_Byte(0x0a);
    DISPSS_LOG_INFO("lvds output pixclk: %d", fm_value);
#endif
}

int lt9211_lvds_clkstb_check(void)
{
    uint8_t porta_clk_state = 0;
    uint8_t portb_clk_state = 0;

    HDMI_WriteI2C_Byte(0xff, 0x86);
    HDMI_WriteI2C_Byte(0x00, 0x01);
    mdelay(300);
    porta_clk_state = (HDMI_ReadI2C_Byte(0x08) & (0x20));

    HDMI_WriteI2C_Byte(0xff, 0x86);
    HDMI_WriteI2C_Byte(0x00, 0x02);
    mdelay(300);
    portb_clk_state = (HDMI_ReadI2C_Byte(0x08) & (0x20));

    if (INPUT_PORT_NUM == 1) {
#ifdef INPUT_PORTA
        if (porta_clk_state) {
            return 1;
        } else {
            return 0;
        }
#endif
#ifdef INPUT_PORTB
        if (portb_clk_state) {
            return 1;
        } else {
            return 0;
        }
#endif
    } else if (INPUT_PORT_NUM == 2) {
        if (porta_clk_state && portb_clk_state) {
            return 1;
        } else {
            return 0;
        }
    }
    return 0;
}

void lt9211_lvds_tx_logic_rst(void)
{
    HDMI_WriteI2C_Byte(0xff, 0x81);
    HDMI_WriteI2C_Byte(0x0d, 0xfb); // LVDS TX LOGIC RESET
    mdelay(10);
    HDMI_WriteI2C_Byte(0x0d, 0xff); // LVDS TX LOGIC RESET  RELEASE
}

void lt9211_lvds_tx_en(void)
{
    HDMI_WriteI2C_Byte(0xff, 0x82);

    if (LVDS_PORTNUM == LVDS_2PORT) {
        HDMI_WriteI2C_Byte(0x3b, 0xb8); // dual-port lvds output Enable
    } else {
        HDMI_WriteI2C_Byte(0x3b, 0x38); // signal-port lvds output Enable
    }
}

void lt9211_lvds_tx_disable(void)
{
    HDMI_WriteI2C_Byte(0xff, 0x82);

    if (LVDS_PORTNUM == LVDS_2PORT) {
        HDMI_WriteI2C_Byte(0x3b, 0x88); // dual-port lvds output disable
    } else {
        HDMI_WriteI2C_Byte(0x3b, 0x08); // signal-port lvds output disable
    }
}

void LT9211_LVDS2LVDS_Config(void)
{
    DISPSS_LOG_INFO("*************LT9211 LVDS2LVDS Config**V4.1***********");
    LT9211_ChipID();
    LT9211_SystemInt();

    /********LVDS INPUT CONFIG********/
    LT9211_LvdsRxPhy();
    LT9211_LvdsRxDigital();
    LT9211_LvdsRxPll();
}

void Backlight_Set(void)
{
    sdrv_gpio_set_pin_output_level(BACKLIGHT_GPIO, true);
}

void LT9211_I2C_Release(sdrv_i2c_t *ctrl)
{
    sdrv_i2c_set_bus_stat(ctrl, SDRV_I2CDRV_BUS_IDLE);
}

void LT9211_LVDS2LVDS_MainLoop(void)
{
    static int flag_lvds_chg = 1;

    if (lt9211_lvds_clkstb_check()) {
        if (flag_lvds_chg) {
            DISPSS_LOG_INFO("lvds clk stable");

            LT9211_LvdsRxPll();
            lt9211_vid_chk_rst(); // video chk soft rst
            lt9211_lvdsrx_logic_rst();
            mdelay(10);
            LT9211_ClockCheckDebug();
            LT9211_VideoCheck();

            /********LVDS OUTPUT CONFIG********/
            LT9211_Txpll();
            LT9211_TxPhy();
            LT9211_TxDigital();

            lt9211_lvds_tx_logic_rst(); // LVDS TX LOGIC RESET
            lt9211_lvds_tx_en();        // LVDS TX output enable
            LT9211_LvdsClkDebug();

            flag_lvds_chg = 0;
        }
    } else {
        if (!flag_lvds_chg) {
            DISPSS_LOG_INFO("lvds clk not stable");
            lt9211_lvds_tx_disable(); // LVDS TX output disable
            flag_lvds_chg = 1;
        }
    }
    mdelay(1000);
    LT9211_I2C_Release(&lt9211_ctrl);
    Backlight_Set();
}

void LT9211_I2C_Config(sdrv_i2c_t *ctrl)
{
    int ret = 0;
    /* get i2c8 clk frequence */
    ctrl->cfg->clk = sdrv_ckgen_get_rate(CLK_NODE(g_ckgen_ip_i2c_sf_5_to_8));

    /* get bus stat, if busy can not use */
    bus_stat = sdrv_i2c_get_bus_stat(ctrl);
    if (bus_stat == SDRV_I2CDRV_BUS_BUSY) {
        ssdk_printf(SSDK_CRIT, "i2c ctrl %u busy", ctrl->cfg->id);
    }

    /* set bus stat to busy indicate bus be occupied now */
    ret = sdrv_i2c_set_bus_stat(ctrl, SDRV_I2CDRV_BUS_BUSY);
    if (ret < 0) {
        ssdk_printf(SSDK_CRIT, "i2c ctrl %u set bus stat fail", ctrl->cfg->id);
    }

    /* set interrupt mode and 400khz transfer mode */
    /* init ctrl use interrupt and 400Khz */
    // sdrv_i2c_set_transmode(ctrl, SDRV_I2CDRV_INTP);
    sdrv_i2c_set_speedmode(ctrl, SDRV_I2CDRV_FSPEED);
    sdrv_i2c_init_ctrl(ctrl);
}

void LT9211_LVDS2LVDS_Main(void)
{
    LT9211_Reset();
    LT9211_I2C_Config(&lt9211_ctrl);
    LT9211_LVDS2LVDS_Config();
}
