/**
 * @file lvds_1920x720_lcd.c
 * @brief lvds panel driver source.
 *
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */

#include <dispss/disp.h>
#include <sdrv_gpio.h>
#include <sdrv_pinctrl.h>

static struct display_timing timing = {
    .hactive = 1920,
    .hfront_porch = 64,
    .hback_porch = 32,
    .hsync_len = 32,

    .vactive = 720,
    .vfront_porch = 16,
    .vback_porch = 37,
    .vsync_len = 8,

    .dsp_clk_pol = LCM_POLARITY_RISING,
    .de_pol = LCM_POLARITY_RISING,
    .vsync_pol = LCM_POLARITY_FALLING,
    .hsync_pol = LCM_POLARITY_FALLING,

    .map_format = LVDS_MAP_FORMAT_SWPG,
};

struct sdrv_panel lvds_1920x720_lcd = {
    .panel_name = "lvds_1920x720_lcd",
    .if_type = IF_TYPE_LVDS,
    .cmd_intf = IF_TYPE_NONE,
    .pixel_bpp = 24,
    .timing = &timing,
};
