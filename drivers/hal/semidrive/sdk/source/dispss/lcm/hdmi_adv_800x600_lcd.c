/**
 * @file hdmi_adv_800x600.c
 * @brief hdmi adv panel driver source.
 *
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */

#include <dispss/disp.h>

static struct display_timing timing = {
    .hactive = 800,
    .hfront_porch = 40,
    .hback_porch = 88,
    .hsync_len = 128,

    .vactive = 600,
    .vfront_porch = 2,
    .vback_porch = 23,
    .vsync_len = 4,

    .dsp_clk_pol = LCM_POLARITY_RISING,
    .de_pol = LCM_POLARITY_RISING,
    .vsync_pol = LCM_POLARITY_RISING,
    .hsync_pol = LCM_POLARITY_RISING,
};

struct sdrv_panel hdmi_adv_800x600_lcd = {
    .panel_name = "hdmi_adv_800x600_lcd",
    .if_type = IF_TYPE_NONE,
    .cmd_intf = IF_TYPE_NONE, // or CMD_INTF_NONE
    .pixel_bpp = 24,
    .timing = &timing,
};
