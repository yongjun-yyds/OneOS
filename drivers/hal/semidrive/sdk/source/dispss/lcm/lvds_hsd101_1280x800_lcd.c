/**
 * @file lvds_hsd101_1280x800_lcd.c
 * @brief atk lvds panel driver source.
 *
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */

#include <dispss/disp.h>

static struct display_timing timing = {
    .hactive = 1280,
    .hfront_porch = 48,
    .hback_porch = 80,
    .hsync_len = 32,

    .vactive = 800,
    .vfront_porch = 10,
    .vback_porch = 10,
    .vsync_len = 3,
    .fps = 60,

    .dsp_clk_pol = LCM_POLARITY_RISING,
    .de_pol = LCM_POLARITY_RISING,
    .vsync_pol = LCM_POLARITY_RISING,
    .hsync_pol = LCM_POLARITY_RISING,

    .map_format = LVDS_MAP_FORMAT_JEIDA,
};

struct sdrv_panel lvds_hsd101_1280x800_lcd = {
    .panel_name = "lvds_hsd101_1280x800_lcd",
    .if_type = IF_TYPE_LVDS,
    .cmd_intf = IF_TYPE_NONE, // or CMD_INTF_NONE
    .pixel_bpp = 18,
    .timing = &timing,
};
