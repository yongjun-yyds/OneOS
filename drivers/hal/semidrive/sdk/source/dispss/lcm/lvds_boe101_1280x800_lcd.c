/**
 * @file lvds_boe101_1280x800_lcd.c
 * @brief GV lvds panel driver source.
 *
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */

#include <dispss/disp.h>

static struct display_timing timing = {
    .hactive = 1280,
    .hfront_porch = 12,
    .hback_porch = 78,
    .hsync_len = 10,

    .vactive = 800,
    .vfront_porch = 8,
    .vback_porch = 14,
    .vsync_len = 2,
    //.fps = 54,
    .dsp_clk_pol = LCM_POLARITY_RISING,
    .de_pol = LCM_POLARITY_RISING,
    .vsync_pol = LCM_POLARITY_RISING,
    .hsync_pol = LCM_POLARITY_RISING,

    .map_format = LVDS_MAP_FORMAT_SWPG,
};

struct sdrv_panel lvds_boe101_1280x800_lcd = {
    .panel_name = "lvds_boe101_1280x800_lcd",
    .if_type = IF_TYPE_LVDS,
    .cmd_intf = IF_TYPE_NONE, // or CMD_INTF_NONE
    .pixel_bpp = 24,
    .timing = &timing,
};
