/**
 * @file rgb_800x600_lcd_z1.c
 * @brief rgb panel driver source.
 *
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */

#include <dispss/disp.h>

static struct display_timing timing = {
    .hactive = 800,
    .hfront_porch = 176,
    .hback_porch = 44,
    .hsync_len = 20,

    .vactive = 600,
    .vfront_porch = 43,
    .vback_porch = 13,
    .vsync_len = 10,

    .dsp_clk_pol = LCM_POLARITY_RISING,
    .de_pol = LCM_POLARITY_RISING,
    .vsync_pol = LCM_POLARITY_RISING,
    .hsync_pol = LCM_POLARITY_RISING,
};

struct sdrv_panel rgb_800x600_lcd_z1 = {
    .panel_name = "rgb_800x600_lcd_z1",
    .if_type = IF_TYPE_NONE,
    .cmd_intf = IF_TYPE_NONE, // or CMD_INTF_NONE
    .pixel_bpp = 24,
    .timing = &timing,
};
