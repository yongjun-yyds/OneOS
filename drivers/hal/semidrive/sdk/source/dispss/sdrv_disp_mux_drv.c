/**
 * @file sdrv_disp_mux_drv.c
 * @brief sdrv display mux driver source.
 *
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */

#include <dispss/disp.h>
#include <dispss/disp_reg_rw.h>
#include <dispss/dispss_log.h>
#include <dispss/lvds_reg.h>

static unsigned int disp_mux_get_reg_base(void)
{
    static unsigned int base = 0;

    if (base == 0)
        base = LVDS_BASE;

    return base;
}

int get_paral_out_bpp(int bpp)
{
    switch (bpp) {
    case 16:
        return 0;
    case 18:
        return 1;
    case 24:
        return 2;
    case 30:
        return 3;
    default:
        DISPSS_LOG_ERR(
            "This bpp(%d) is not supported!  We only support 16, 18, 24, 30.",
            bpp);
        DISPSS_LOG_ERR("Using defaut 24 instead!");
        return 2;
    }
}

status_t sdrv_paral_to_csi_en(bool enable)
{
    unsigned int base = disp_mux_get_reg_base();
    unsigned int val;

    val = disp_read(base, DC1_MUX_CTRL);
    val = reg_value(enable ? 1 : 0, val, PARAL_OUT_EN_SHIFT, PARAL_OUT_EN_MASK);
    disp_write(base, DC1_MUX_CTRL, val);

    return SDRV_DISPSS_STATUS_OK;
}

status_t sdrv_crc_mode_set(bool is_internal)
{
    unsigned int base = disp_mux_get_reg_base();
    unsigned int val;

    val = disp_read(base, DC1_MUX_CTRL);
    val = reg_value(is_internal ? 0 : 1, val, CRC_SRC_SHIFT, CRC_SRC_MASK);
    disp_write(base, DC1_MUX_CTRL, val);

    return SDRV_DISPSS_STATUS_OK;
}

status_t sdrv_disp_mux_init(struct sdrv_panel *panel)
{
    unsigned int base = disp_mux_get_reg_base();
    unsigned int val;

    val = disp_read(base, DC1_MUX_CTRL);

    switch (panel->if_type) {
    case IF_TYPE_LVDS:
        val = reg_value(1, val, PARAL_TO_LVDS_EN_SHIFT, PARAL_TO_LVDS_EN_MASK);
        break;
    case IF_TYPE_NONE:
        val = reg_value(get_paral_out_bpp(panel->pixel_bpp), val,
                        PARAL_OUT_BPP_SHIFT, PARAL_OUT_BPP_MASK);
        val = reg_value(1, val, PARAL_OUT_EN_SHIFT, PARAL_OUT_EN_MASK);
        break;
    default:
        break;
    }

    /*Using LVDS PLL CLK for default*/
    val = reg_value(0, val, DSP_CLK_SHIFT, DSP_CLK_MASK);
    disp_write(base, DC1_MUX_CTRL, val);

    return SDRV_DISPSS_STATUS_OK;
}
