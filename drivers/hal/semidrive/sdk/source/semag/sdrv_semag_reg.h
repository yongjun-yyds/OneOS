/**
 * @file sdrv_semag_reg.h
 * @brief Sdrv SEMAG register head source.
 *
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef SDRV_SEMAG_REG_H_
#define SDRV_SEMAG_REG_H_

#define SEMAG_OFF(n) (0x200U + 8U * (n))

#define BM_SEMAG_SGC (0x1U << 0U)

#define BM_SEMAG_IESGLF (0x1U << 8U)

#define BM_SEMAG_IESGSC (0x1U << 9U)

#define FM_SEMAG_SGLS (0xFFU << 16U)
#define FV_SEMAG_SGLS(v) (((v) << 16U) & FM_SEMAG_SGLS)
#define GFV_SEMAG_SGLS(v) (((v)&FM_SEMAG_SGLS) >> 16U)

#define BM_SEMAG_SGLIC (0x1U << 24U)

#define SGR_OFF (0x400U)

#define BM_SGR_PSGR (0x1U << 0U)

#define FM_SGR_GSGR (0xFFU << 16U)
#define FV_SGR_GSGR(v) (((v) << 16U) & FM_SGR_GSGR)
#define GFV_SGR_GSGR(v) (((v)&FM_SGR_GSGR) >> 16U)

#define BM_SGR_GSGRE (0x1U << 24U)

#define BM_SGR_GSGREL (0x1U << 25U)

#define SGI_OFF (0x404U)

#endif /* SDRV_SEMAG_REG_H_ */
