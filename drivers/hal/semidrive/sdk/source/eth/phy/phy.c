/**
 * phy.c
 *
 * Copyright (c) 2020 Semidrive Semiconductor.
 * All rights reserved.
 *
 * Description: eth phy driver
 *
 * Revision History:
 * -----------------
 */

#include "armv7-r/irq.h"
#include "phy.h"

void phy_dev_register(phy_bus_t *bus, phy_dev_t *dev)
{
    irq_state_t state;

    state = arch_irq_save();
    list_add_tail(&bus->phy_dev_list, &dev->node);
    bus->ref_cnt++;
    dev->bus = bus;
    arch_irq_restore(state);
}

void phy_init(phy_bus_t *bus)
{
    phy_dev_t *phy;

    list_for_every_entry(&bus->phy_dev_list, phy, phy_dev_t, node) {
        if (phy->init)
            phy->init(phy);
    }
}
