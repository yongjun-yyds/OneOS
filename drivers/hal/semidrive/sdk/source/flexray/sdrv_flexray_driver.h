/**
 * @file sdrv_flexray_driver.h
 * @brief SemiDrive Flexray low level driver header file.
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef SDRV_FLEXRAY_DRIVER_H_
#define SDRV_FLEXRAY_DRIVER_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "sdrv_flexray_general_types.h"

/* FlexRay module register address offsets */
#define FLEXRAY_MVR_ADDR16                                              ((uint16)0x0000U)
#define FLEXRAY_MCR_ADDR16                                              ((uint16)0x0004U)
#define FLEXRAY_SYMBADHR_ADDR16                                         ((uint16)0x0008U)
#define FLEXRAY_SYMBADLR_ADDR16                                         ((uint16)0x000CU)
#define FLEXRAY_STBSCR_ADDR16                                           ((uint16)0x0010U)
#define FLEXRAY_STBPCR_ADDR16                                           ((uint16)0x0014U)
#define FLEXRAY_MBDSR_ADDR16                                            ((uint16)0x0018U)
#define FLEXRAY_MBSSUTR_ADDR16                                          ((uint16)0x001CU)
#define FLEXRAY_DBGADRR_ADDR16                                          ((uint16)0x0020U)
#define FLEXRAY_DBGDATR_ADDR16                                          ((uint16)0x0024U)
#define FLEXRAY_POCR_ADDR16                                             ((uint16)0x0028U)
#define FLEXRAY_GIFER_ADDR16                                            ((uint16)0x002CU)
#define FLEXRAY_PIFR0_ADDR16                                            ((uint16)0x0030U)
#define FLEXRAY_PIFR1_ADDR16                                            ((uint16)0x0034U)
#define FLEXRAY_PIER0_ADDR16                                            ((uint16)0x0038U)
#define FLEXRAY_PIER1_ADDR16                                            ((uint16)0x003CU)
#define FLEXRAY_CHIERFR_ADDR16                                          ((uint16)0x0040U)

#define FLEXRAY_MBIVEC_ADDR16                                           ((uint16)0x0044U)
#define FLEXRAY_CASERCR_ADDR16                                          ((uint16)0x0048U)
#define FLEXRAY_CBSERCR_ADDR16                                          ((uint16)0x004CU)
#define FLEXRAY_PSR0_ADDR16                                             ((uint16)0x0050U)
#define FLEXRAY_PSR1_ADDR16                                             ((uint16)0x0054U)
#define FLEXRAY_PSR2_ADDR16                                             ((uint16)0x0058U)
#define FLEXRAY_PSR3_ADDR16                                             ((uint16)0x005CU)
#define FLEXRAY_MTCTR_ADDR16                                            ((uint16)0x0060U)

#define FLEXRAY_CYCTR_ADDR16                                            ((uint16)0x0064U)
#define FLEXRAY_SLTCTAR_ADDR16                                          ((uint16)0x0068U)
#define FLEXRAY_SLTCTBR_ADDR16                                          ((uint16)0x006CU)
#define FLEXRAY_RTCORVR_ADDR16                                          ((uint16)0x0070U)
#define FLEXRAY_OFCORVR_ADDR16                                          ((uint16)0x0074U)
#define FLEXRAY_CIFRR_ADDR16                                            ((uint16)0x0078U)
#define FLEXRAY_SYMATOR_ADDR16                                          ((uint16)0x007CU)
#define FLEXRAY_SFCNTR_ADDR16                                           ((uint16)0x0080U)
#define FLEXRAY_SFTOR_ADDR16                                            ((uint16)0x0084U)
#define FLEXRAY_SFTCCSR_ADDR16                                          ((uint16)0x0088U)
#define FLEXRAY_SFIDRFR_ADDR16                                          ((uint16)0x008CU)
#define FLEXRAY_SFIDAFVR_ADDR16                                         ((uint16)0x0090U)
#define FLEXRAY_SFIDAFMR_ADDR16                                         ((uint16)0x0094U)
#define FLEXRAY_NMVR0_ADDR16                                            ((uint16)0x0098U)
#define FLEXRAY_NMVR1_ADDR16                                            ((uint16)0x009CU)
#define FLEXRAY_NMVR2_ADDR16                                            ((uint16)0x00A0U)
#define FLEXRAY_NMVR3_ADDR16                                            ((uint16)0x00A4U)
#define FLEXRAY_NMVR4_ADDR16                                            ((uint16)0x00A8U)
#define FLEXRAY_NMVR5_ADDR16                                            ((uint16)0x00ACU)
#define FLEXRAY_NMVLR_ADDR16                                            ((uint16)0x00B0U)
#define FLEXRAY_TICCR_ADDR16                                            ((uint16)0x00B4U)
#define FLEXRAY_TI1CYSR_ADDR16                                          ((uint16)0x00B8U)
#define FLEXRAY_TI1MTOR_ADDR16                                          ((uint16)0x00BCU)
#define FLEXRAY_TI2CR0_ADDR16                                           ((uint16)0x00C0U)
#define FLEXRAY_TI2CR1_ADDR16                                           ((uint16)0x00C4U)
#define FLEXRAY_SSSR_ADDR16                                             ((uint16)0x00C8U)
#define FLEXRAY_SSCCR_ADDR16                                            ((uint16)0x00CCU)
#define FLEXRAY_SSR0_ADDR16                                             ((uint16)0x00D0U)
#define FLEXRAY_SSR1_ADDR16                                             ((uint16)0x00D4U)
#define FLEXRAY_SSR2_ADDR16                                             ((uint16)0x00D8U)
#define FLEXRAY_SSR3_ADDR16                                             ((uint16)0x00DCU)
#define FLEXRAY_SSR4_ADDR16                                             ((uint16)0x00E0U)
#define FLEXRAY_SSR5_ADDR16                                             ((uint16)0x00E4U)
#define FLEXRAY_SSR6_ADDR16                                             ((uint16)0x00E8U)
#define FLEXRAY_SSR7_ADDR16                                             ((uint16)0x00ECU)
#define FLEXRAY_SSCR0_ADDR16                                            ((uint16)0x00F0U)
#define FLEXRAY_SSCR1_ADDR16                                            ((uint16)0x00F4U)
#define FLEXRAY_SSCR2_ADDR16                                            ((uint16)0x00F8U)
#define FLEXRAY_SSCR3_ADDR16                                            ((uint16)0x00FCU)
#define FLEXRAY_MTSACFR_ADDR16                                          ((uint16)0x0100U)
#define FLEXRAY_MTSBCFR_ADDR16                                          ((uint16)0x0104U)
#define FLEXRAY_RSBIR_ADDR16                                            ((uint16)0x0108U)
#define FLEXRAY_RFSR_ADDR16                                             ((uint16)0x010CU)
#define FLEXRAY_RFSIR_ADDR16                                            ((uint16)0x0110U)
#define FLEXRAY_RFDSR_ADDR16                                            ((uint16)0x0114U)
#define FLEXRAY_RFARIR_ADDR16                                           ((uint16)0x0118U)
#define FLEXRAY_RFBRIR_ADDR16                                           ((uint16)0x011CU)
#define FLEXRAY_RFMIDAFVR_ADDR16                                        ((uint16)0x0120U)
#define FLEXRAY_RFMIDAFMR_ADDR16                                        ((uint16)0x0124U)
#define FLEXRAY_RFFIDRFVR_ADDR16                                        ((uint16)0x0128U)
#define FLEXRAY_RFFIDRFMR_ADDR16                                        ((uint16)0x012CU)
#define FLEXRAY_RFRFCFR_ADDR16                                          ((uint16)0x0130U)
#define FLEXRAY_RFRFCTR_ADDR16                                          ((uint16)0x0134U)
#define FLEXRAY_LDTXSLAR_ADDR16                                         ((uint16)0x0138U)
#define FLEXRAY_LDTXSLBR_ADDR16                                         ((uint16)0x013CU)

#define FLEXRAY_PCR0_ADDR16                                             ((uint16)0x0140U)
#define FLEXRAY_PCR1_ADDR16                                             ((uint16)0x0144U)
#define FLEXRAY_PCR2_ADDR16                                             ((uint16)0x0148U)
#define FLEXRAY_PCR3_ADDR16                                             ((uint16)0x014CU)
#define FLEXRAY_PCR4_ADDR16                                             ((uint16)0x0150U)
#define FLEXRAY_PCR5_ADDR16                                             ((uint16)0x0154U)
#define FLEXRAY_PCR6_ADDR16                                             ((uint16)0x0158U)
#define FLEXRAY_PCR7_ADDR16                                             ((uint16)0x015CU)
#define FLEXRAY_PCR8_ADDR16                                             ((uint16)0x0160U)
#define FLEXRAY_PCR9_ADDR16                                             ((uint16)0x0164U)
#define FLEXRAY_PCR10_ADDR16                                            ((uint16)0x0168U)
#define FLEXRAY_PCR11_ADDR16                                            ((uint16)0x016CU)
#define FLEXRAY_PCR12_ADDR16                                            ((uint16)0x0170U)
#define FLEXRAY_PCR13_ADDR16                                            ((uint16)0x0174U)
#define FLEXRAY_PCR14_ADDR16                                            ((uint16)0x0178U)
#define FLEXRAY_PCR15_ADDR16                                            ((uint16)0x017CU)
#define FLEXRAY_PCR16_ADDR16                                            ((uint16)0x0180U)
#define FLEXRAY_PCR17_ADDR16                                            ((uint16)0x0184U)
#define FLEXRAY_PCR18_ADDR16                                            ((uint16)0x0188U)
#define FLEXRAY_PCR19_ADDR16                                            ((uint16)0x018CU)
#define FLEXRAY_PCR20_ADDR16                                            ((uint16)0x0190U)
#define FLEXRAY_PCR21_ADDR16                                            ((uint16)0x0194U)
#define FLEXRAY_PCR22_ADDR16                                            ((uint16)0x0198U)
#define FLEXRAY_PCR23_ADDR16                                            ((uint16)0x019CU)
#define FLEXRAY_PCR24_ADDR16                                            ((uint16)0x01A0U)
#define FLEXRAY_PCR25_ADDR16                                            ((uint16)0x01A4U)
#define FLEXRAY_PCR26_ADDR16                                            ((uint16)0x01A8U)
#define FLEXRAY_PCR27_ADDR16                                            ((uint16)0x01ACU)
#define FLEXRAY_PCR28_ADDR16                                            ((uint16)0x01B0U)
#define FLEXRAY_PCR29_ADDR16                                            ((uint16)0x01B4U)
#define FLEXRAY_PCR30_ADDR16                                            ((uint16)0x01B8U)

#define FLEXRAY_MBCCSR0_ADDR16                                          ((uint16)0x0200U)
#define FLEXRAY_MBCCFR0_ADDR16                                          ((uint16)0x0204U)
#define FLEXRAY_MBFIDR0_ADDR16                                          ((uint16)0x0208U)
#define FLEXRAY_MBIDXR0_ADDR16                                          ((uint16)0x020CU)

/* FR_MCR bitfields */
#define FLEXRAY_MCR_MEN_U16                                             ((uint16)0x8000U)
#define FLEXRAY_MCR_SCM_U16                                             ((uint16)0x2000U)
#define FLEXRAY_MCR_CHB_U16                                             ((uint16)0x1000U)
#define FLEXRAY_MCR_CHA_U16                                             ((uint16)0x0800U)

#define FLEXRAY_MCR_SFFE_U16                                            ((uint16)0x0400U)
#define FLEXRAY_MCR_BITRATE_MASK_U16                                    ((uint16)0x000EU)
/* FR_SYMBADHR bitfield*/
#define FLEXRAY_SYMBADLR_SMBA_MASK_U16                                  ((uint16)0xFFF0U)
/* FR_MBDSR bitfields */
#define FLEXRAY_MBDSR_MBSEG1DS_MASK_U16                                 ((uint16)0x007FU)
#define FLEXRAY_MBDSR_MBSEG2DS_MASK_U16                                 ((uint16)0x7F00U)
/* FR_MBSSUTR bitfields */
#define FLEXRAY_MBSSUTR_LAST_MB_UTIL_MASK_U16                           ((uint16)0x007FU)
#define FLEXRAY_MBSSUTR_LAST_MB_SEG1_MASK_U16                           ((uint16)0x7F00U)
/* FR_POCR bitfields */
#define FLEXRAY_POCR_WME_U16                                            ((uint16)0x8000U)
#define FLEXRAY_POCR_BSY_U16                                            ((uint16)0x0080U)
#define FLEXRAY_POCR_WMC_U16                                            ((uint16)0x0080U)
/* FR_POCR EOC_AP values */
#define FLEXRAY_POCR_EOC_ADD_U16                                        ((uint16)0x0C00U)
#define FLEXRAY_POCR_EOC_SUB_U16                                        ((uint16)0x0800U)
#define FLEXRAY_POCR_EOC_DONT_U16                                       ((uint16)0x0000U)
/* FR_POCR ERC_AP values */
#define FLEXRAY_POCR_ERC_ADD_U16                                        ((uint16)0x0300U)
#define FLEXRAY_POCR_ERC_SUB_U16                                        ((uint16)0x0200U)
#define FLEXRAY_POCR_ERC_DONT_U16                                       ((uint16)0x0000U)
/* FR_POCR POCCMD commands */
#define FLEXRAY_POCR_CMDALLOWCOLDSTART_U16                              ((uint16)0x0000U)
#define FLEXRAY_POCR_CMD_ALL_SLOTS_U16                                  ((uint16)0x0001U)
#define FLEXRAY_POCR_CMD_CONFIG_U16                                     ((uint16)0x0002U)
#define FLEXRAY_POCR_CMD_FREEZE_U16                                     ((uint16)0x0003U)
#define FLEXRAY_POCR_CMDCONFIGCOMPLETE_U16                              ((uint16)0x0004U)
#define FLEXRAY_POCR_CMD_RUN_U16                                        ((uint16)0x0005U)
#define FLEXRAY_POCR_CMD_DEFAULTCONFIG_U16                              ((uint16)0x0006U)
#define FLEXRAY_POCR_CMD_HALT_U16                                       ((uint16)0x0007U)
#define FLEXRAY_POCR_CMD_WAKEUP_U16                                     ((uint16)0x0008U)


/* Module Interrupt Enable */
#define FLEXRAY_GIFER_MIE_U16                                           ((uint16)0x0080U)
/* Protocol Interrupt Enable */
#define FLEXRAY_GIFER_PRIE_U16                                          ((uint16)0x0040U)
/* CHI Interrupt Enable */
#define FLEXRAY_GIFER_CHIIE_U16                                         ((uint16)0x0020U)
/* Wakeup Interrupt Enable */
#define FLEXRAY_GIFER_WUPIE_U16                                         ((uint16)0x0010U)
/* Receive FIFO Channel B Not Empty Interrupt Enable */
#define FLEXRAY_GIFER_FNEBIE_U16                                        ((uint16)0x0008U)
/* Receive FIFO Channel A Not Empty Interrupt Enable */
#define FLEXRAY_GIFER_FNEAIE_U16                                        ((uint16)0x0004U)
/* Receive Buffer Interrupt Enable */
#define FLEXRAY_GIFER_RBIE_U16                                          ((uint16)0x0002U)
/* Transmit Buffer Interrupt Enable */
#define FLEXRAY_GIFER_TBIE_U16                                          ((uint16)0x0001U)

#define FLEXRAY_GIFER_TBIF_U16                                          ((uint16)0x0100U)
#define FLEXRAY_GIFER_RBIF_U16                                          ((uint16)0x0200U)
#define FLEXRAY_GIFER_FNEAIF_U16                                        ((uint16)0x0400U)
#define FLEXRAY_GIFER_FNEBIF_U16                                        ((uint16)0x0800U)
#define FLEXRAY_GIFER_WUPIF_U16                                         ((uint16)0x1000U)
#define FLEXRAY_GIFER_INT_FLAGS_MASK_U16                                ((uint16)0xFF00U)
#define FLEXRAY_GIFER_CTRL_FLAGS_MASK_U16                               ((uint16)0x00FFU)
/* FR_PIFR0 bitfields */
#define FLEXRAY_PIFR0_TI2_IF_U16                                        ((uint16)0x0004U)
#define FLEXRAY_PIFR0_TI1_IF_U16                                        ((uint16)0x0002U)
/* FR_PIER0 bitfields */
#define FLEXRAY_PIER0_TI2_IE_U16                                        ((uint16)0x0004U)
#define FLEXRAY_PIER0_TI1_IE_U16                                        ((uint16)0x0002U)
/* FR_PSR0 bitfields */
#define FLEXRAY_PSR0_ERRMODE_MASK_U16                                   ((uint16)0xC000U)
#define FLEXRAY_PSR0_SLOTMODE_MASK_U16                                  ((uint16)0x3000U)
#define FLEXRAY_PSR0_PROTSTATE_MASK_U16                                 ((uint16)0x0700U)
#define FLEXRAY_PSR0_STARTUP_MASK_U16                                   ((uint16)0x00F0U)
#define FLEXRAY_PSR0_WUP_MASK_U16                                       ((uint16)0x0007U)
/* FR_PSR0 protocol state */
#define FLEXRAY_PSR0_PROTSTATE_DEFAULT_CONFIG_U16                       ((uint16)0x0000U)
#define FLEXRAY_PSR0_PROTSTATE_CONFIG_U16                               ((uint16)0x0100U)
#define FLEXRAY_PSR0_PROTSTATE_WAKEUP_U16                               ((uint16)0x0200U)
#define FLEXRAY_PSR0_PROTSTATE_READY_U16                                ((uint16)0x0300U)
#define FLEXRAY_PSR0_PROTSTATE_NORMAL_PASSIVE_U16                       ((uint16)0x0400U)
#define FLEXRAY_PSR0_PROTSTATE_NORMAL_ACTIVE_U16                        ((uint16)0x0500U)
#define FLEXRAY_PSR0_PROTSTATE_HALT_U16                                 ((uint16)0x0600U)
#define FLEXRAY_PSR0_PROTSTATE_STARTUP_U16                              ((uint16)0x0700U)
/* FR_PSR0 startup state */
#define FLEXRAY_PSR0_STARTUP_CCR_U16                                    ((uint16)0x0020U)
#define FLEXRAY_PSR0_STARTUP_CL_U16                                     ((uint16)0x0030U)
#define FLEXRAY_PSR0_STARTUP_ICOC_U16                                   ((uint16)0x0040U)
#define FLEXRAY_PSR0_STARTUP_IL_U16                                     ((uint16)0x0050U)
#define FLEXRAY_PSR0_STARTUP_IS_U16                                     ((uint16)0x0070U)
#define FLEXRAY_PSR0_STARTUP_CCC_U16                                    ((uint16)0x00A0U)
#define FLEXRAY_PSR0_STARTUP_ICLC_U16                                   ((uint16)0x00D0U)
#define FLEXRAY_PSR0_STARTUP_CG_U16                                     ((uint16)0x00E0U)
#define FLEXRAY_PSR0_STARTUP_CJ_U16                                     ((uint16)0x00F0U)
/* FR_PSR0 wakeup State */
#define FLEXRAY_PSR0_WUP_UD_U16                                         ((uint16)0x0000U)
#define FLEXRAY_PSR0_WUP_RH_U16                                         ((uint16)0x0001U)
#define FLEXRAY_PSR0_WUP_RW_U16                                         ((uint16)0x0002U)
#define FLEXRAY_PSR0_WUP_HC_U16                                         ((uint16)0x0003U)
#define FLEXRAY_PSR0_WUP_WC_U16                                         ((uint16)0x0004U)
#define FLEXRAY_PSR0_WUP_UC_U16                                         ((uint16)0x0005U)
#define FLEXRAY_PSR0_WUP_T_U16                                          ((uint16)0x0006U)
/* FR_PSR0 bitfields */
#define FLEXRAY_PSR0_ERRMODE_MASK_U16                                   ((uint16)0xC000U)
#define FLEXRAY_PSR0_SLOTMODE_MASK_U16                                  ((uint16)0x3000U)
#define FLEXRAY_PSR0_PROTSTATE_MASK_U16                                 ((uint16)0x0700U)
#define FLEXRAY_PSR0_STARTUP_MASK_U16                                   ((uint16)0x00F0U)
#define FLEXRAY_PSR0_WUP_MASK_U16                                       ((uint16)0x0007U)
/* FR_PSR0 slot mode */
#define FLEXRAY_PSR0_SLOTMODE_SINGLE_U16                                ((uint16)0x0000U)
#define FLEXRAY_PSR0_SLOTMODE_ALL_PENDING_U16                           ((uint16)0x1000U)
#define FLEXRAY_PSR0_SLOTMODE_ALL_U16                                   ((uint16)0x2000U)
/* FR_PSR0 error mode */
#define FLEXRAY_PSR0_ERRMODE_ACTIVE_U16                                 ((uint16)0x0000U)
#define FLEXRAY_PSR0_ERRMODE_PASSIVE_U16                                ((uint16)0x4000U)
#define FLEXRAY_PSR0_ERRMODE_COMM_HALT_U16                              ((uint16)0x8000U)
/* FR_PSR1 bitfields */
#define FLEXRAY_PSR1_CPN_U16                                            ((uint16)0x0080U)
#define FLEXRAY_PSR1_HHR_U16                                            ((uint16)0x0040U)
#define FLEXRAY_PSR1_FRZ_U16                                            ((uint16)0x0020U)
/* FR_PSR2 bitfields */
#define FLEXRAY_PSR2_CHB_MASK_U16                                       ((uint16)0xFC00U)
#define FLEXRAY_PSR2_CHA_MASK_U16                                       ((uint16)0x03F0U)
#define FLEXRAY_PSR2_STCB_U16                                           ((uint16)0x2000U)
#define FLEXRAY_PSR2_STCA_U16                                           ((uint16)0x0080U)
/* FR_PSR3 bitfields */
#define FLEXRAY_PSR3_CHB_MASK_U16                                       ((uint16)0x1F00U)
#define FLEXRAY_PSR3_CHA_MASK_U16                                       ((uint16)0x001FU)
#define FLEXRAY_PSR3_WUB_U16                                            ((uint16)0x2000U)
#define FLEXRAY_PSR3_WUA_U16                                            ((uint16)0x0020U)
/* FR_SFTCCSR bitfields */
#define FLEXRAY_SFTCCSR_SIDEN_U16                                       ((uint16)0x0001U)
#define FLEXRAY_SFTCCSR_ELKT_U16                                        ((uint16)0x8000U)
#define FLEXRAY_SFTCCSR_OLKT_U16                                        ((uint16)0x4000U)
#define FLEXRAY_SFTCCSR_ELKS_U16                                        ((uint16)0x0080U)
#define FLEXRAY_SFTCCSR_OLKS_U16                                        ((uint16)0x0040U)
#define FLEXRAY_SFTCCSR_EVAL_U16                                        ((uint16)0x0020U)
#define FLEXRAY_SFTCCSR_OVAL_U16                                        ((uint16)0x0010U)
/* FR_NMVLR bitfield */
#define FLEXRAY_NMVLR_MASK_U16                                          ((uint16)0x000FU)
/* FR_TICCR bitfields */
#define FLEXRAY_TICCR_T2SP_U16                                          ((uint16)0x0400U)
#define FLEXRAY_TICCR_T2TR_U16                                          ((uint16)0x0200U)
#define FLEXRAY_TICCR_T1SP_U16                                          ((uint16)0x0004U)
#define FLEXRAY_TICCR_T1TR_U16                                          ((uint16)0x0002U)
#define FLEXRAY_TICCR_CONFIG_MASK_U16                                   ((uint16)0x3010U)
/* FR_TI1CYSR bitfield */
#define FLEXRAY_TI1CYSR_T1_CYC_MSK_U16                                  ((uint16)0x003FU)
/* FR_RSBIR selector values */
#define FLEXRAY_RSBIR_SEL_RSBIR_A1_U16                                  ((uint16)0x0000U)
#define FLEXRAY_RSBIR_SEL_RSBIR_A2_U16                                  ((uint16)0x1000U)
#define FLEXRAY_RSBIR_SEL_RSBIR_B1_U16                                  ((uint16)0x2000U)
#define FLEXRAY_RSBIR_SEL_RSBIR_B2_U16                                  ((uint16)0x3000U)
/* FR_RFWMSR selector value */
#define FLEXRAY_RFWMSR_SEL_U16                                          ((uint16)0x0001U)
/* FR_RFSIR bitfield */
#define FLEXRAY_RFSIR_SIDX_MASK_U16                                     ((uint16)0x03FFU)
/* FR_RFDSR bitfields */
#define FLEXRAY_RFDSR_ENTRY_SIZE_MASK_U16                               ((uint16)0x007FU)
#define FLEXRAY_RFDSR_FIFO_DEPTH_MASK_U16                               ((uint16)0xFF00U)
/* FR_RFRFCFR selector values */
#define FLEXRAY_RFRFCFR_IBD_LOWINT_U16                                  ((uint16)0x0000U)
#define FLEXRAY_RFRFCFR_IBD_UPPINT_U16                                  ((uint16)0x4000U)
/* range filter 0 */
#define FLEXRAY_RFRFCFR_SEL_F0_U16                                      ((uint16)0x0000U)
/* range filter 1 */
#define FLEXRAY_RFRFCFR_SEL_F1_U16                                      ((uint16)0x1000U)
/* range filter 2 */
#define FLEXRAY_RFRFCFR_SEL_F2_U16                                      ((uint16)0x2000U)
/* range filter 3 */
#define FLEXRAY_RFRFCFR_SEL_F3_U16                                      ((uint16)0x3000U)
/* FR_RFRFCFR bitfield */
#define FLEXRAY_RFRFCFR_SID_MASK_U16                                    ((uint16)0x07FFU)
/* RFRFCTR range control bits */
#define FLEXRAY_RFRFCTR_F3MD_U16                                        ((uint16)0x0800U)
#define FLEXRAY_RFRFCTR_F2MD_U16                                        ((uint16)0x0400U)
#define FLEXRAY_RFRFCTR_F1MD_U16                                        ((uint16)0x0200U)
#define FLEXRAY_RFRFCTR_F0MD_U16                                        ((uint16)0x0100U)
#define FLEXRAY_RFRFCTR_F3EN_U16                                        ((uint16)0x0008U)
#define FLEXRAY_RFRFCTR_F2EN_U16                                        ((uint16)0x0004U)
#define FLEXRAY_RFRFCTR_F1EN_U16                                        ((uint16)0x0002U)
#define FLEXRAY_RFRFCTR_F0EN_U16                                        ((uint16)0x0001U)
/* FR_PCR10 bitfield */
#define FLEXRAY_PCR10_WUP_CH_U16                                        ((uint16)0x4000U)
/* FR_MBCCSR bitfields */
#define FLEXRAY_MBCCSR0_CONFIG_MASK_U16                                 ((uint16)0x7900U)
#define FLEXRAY_MBCCSR_MTD_U16                                          ((uint16)0x1000U)
#define FLEXRAY_MBCCSR_CMT_U16                                          ((uint16)0x0800U)
#define FLEXRAY_MBCCSR_EDT_U16                                          ((uint16)0x0400U)
#define FLEXRAY_MBCCSR_LCKT_U16                                         ((uint16)0x0200U)
#define FLEXRAY_MBCCSR_MBIE_U16                                         ((uint16)0x0100U)
#define FLEXRAY_MBCCSR_DUP_U16                                          ((uint16)0x0010U)
#define FLEXRAY_MBCCSR_EDS_U16                                          ((uint16)0x0004U)
#define FLEXRAY_MBCCSR_LCKS_U16                                         ((uint16)0x0002U)
#define FLEXRAY_MBCCSR_MBIF_U16                                         ((uint16)0x0001U)
/* FR_MBCCFR bitfields */
#define FLEXRAY_MBCCFR_CHA_U16                                          ((uint16)0x4000U)
#define FLEXRAY_MBCCFR_CHB_U16                                          ((uint16)0x2000U)
#define FLEXRAY_MBCCFR_CCFE_U16                                         ((uint16)0x1000U)
#define FLEXRAY_MBCCFR_CCFVAL_MASK_U16                                  ((uint16)0x003FU)
#define FLEXRAY_MBCCFR_CCFMSK_MASK_U16                                  ((uint16)0x0FC0U)
/* FR_MBFIDR bitfields */
#define FLEXRAY_MBFIDR_FID_MASK_U16                                     ((uint16)0x07FFU)
#define FLEXRAY_FRAMEHEADER0_PPI_MASK_U16                               ((uint16)0x4000U)
#define FLEXRAY_FRAMEHEADER0_PPI_MASK_U32                               ((uint32)0x40000000U)

#define FLEXRAY_FRAMEHEADER0_FID_MASK_U32                               ((uint32)0x07FF0000U)
#define FLEXRAY_FRAMEHEADER1_PLDLEN_MASK_U16                            ((uint16)0x007FU)
#define FLEXRAY_FRAMEHEADER1_PLDLEN_MASK_LITTLE                         ((uint32)0xFFFFFF80U)
#define FLEXRAY_FRAMEHEADER1_CRC_MASK_LITTLE                            ((uint32)0xF800FFFFU)
#define FLEXRAY_FRAMEHEADER1_CRC_MASK_U16                               ((uint16)0x7FFU)

#define FLEXRAY_FRAMEHEADER1_CYCCNT_MASK_U32                            ((uint16)0x3F00U)

#define FLEXRAY_FRAMEHEADER1_CYCCNT_OFFSET_U32                          ((uint16)8U)

#if (CPU_BYTE_ORDER == LOW_BYTE_FIRST)
/*FR 16-bit registers mask */
#define FLEXRAY_REGISTER_MASK_HIGH_U32                                  ((uint32)0xFFFF0000U)
#define FLEXRAY_REGISTER_MASK_LOW_U32                                   ((uint32)0x0000FFFFU)
#define FLEXRAY_REGISTER_OFFSET_U8                                      ((uint8)16U)
#define FLEXRAY_EVEN_PAYLOAD_BYTE_MASK_U32                              ((uint32)0x00FF00FF00U)
#define FLEXRAY_ODD_PAYLOAD_BYTE_MASK_U32                               ((uint32)0x0000FF00FFU)
#define FLEXRAY_EVEN_PAYLOAD_BYTE_MASK_U16                              ((uint16)0xFF00U)
#define FLEXRAY_ODD_PAYLOAD_BYTE_MASK_U16                               ((uint16)0x00FFU)
#define FLEXRAY_PAYLOAD_BYTE_OFFSET_U32                                 ((uint32)8U)
#endif

#define FR_MAX_WAITCYCLES_U8                                            ((uint16)100U)
#define FLEXRAY_HEADER_SIZE_U8                                          ((uint16)10U)
#define FLEXRAY_HEADER_CACHE_SIZE_U8                                    ((uint32)12U)
#define FLEXRAY_STATUS_SLOT_U8                                          ((uint8)10U)
#define FLEXRAY_STATUS_SLOT_U32                                         ((uint8)2U)
#define FLEXRAY_MB_REG_NUMBER                                           ((uint8)4U)
#define FLEXRAY_REG_SIEZ                                                ((uint8)4U)
#define FLEXRAY_MB_REG_OFFSET                                           (FLEXRAY_MB_REG_NUMBER * FLEXRAY_REG_SIEZ)

/* This value is used for never-ending NMV reading protection */
#define FRLEXRAY_MAX_NMVREAD_U8                                         ((uint8)5U)
/* Address of NMVR0 reg. for 8-bit operations */
#define FRLEXRAY_NMVR0_8BIT                                             ((uint16)0x004CU)
/*These values are used for address clash */
#define FLEXRAY_CHANNELBEVENLIST_ADDRESSOFFSET_U16                      ((uint16)0x001EU)
#define FLEXRAY_CHANNELAODDLIST_ADDRESSOFFSET_U16                       ((uint16)0x003CU)
#define FLEXRAY_CHANNELBODDLIST_ADDRESSOFFSET_U16                       ((uint16)0x005AU)
/* FR_PCR14 bitfields */
#define FLEXRAY_PCR14_MASK_U16                                          ((uint16)0x005AU)
/* FR_PCR16 bitfields */
#define FLEXRAY_PCR9_MASK_U16                                           ((uint16)0x3FFFU)

/* Maximum number of communication controllers that the driver supports */
#define FR_NUM_CTRL_SUPPORTED 1U

/* Maximum number of available message buffers */
#define FR_NUMBER_MB 252U

/* On/Off PreCompile swtiches */
#define FR_RXSTRINGENTCHECK         (STD_ON)
#define FR_RXSTRINGENTLENGTHCHECK   (STD_ON)
#define FR_PREPARE_LPDU_SUPPORT     (STD_OFF)
#define FR_DISABLE_LPDU_SUPPORT     (STD_ON)
#define FR_RECONFIG_LPDU_SUPPORT    (STD_OFF)

typedef enum {
    FR_FIFOA_BUFFER = 0U,  /* Receive FIFO A */
    FR_FIFOB_BUFFER,       /* Receive FIFO B */
    FR_TRANSMIT_BUFFER,    /* Individual transmit message buffer */
    FR_RECEIVE_BUFFER      /* individual receive message buffer */
} Fr_BufferType;

typedef enum {
    FR_ACCEPTANCE = 0U,  /* Filter is set to accept selected IDs */
    FR_REJECTION         /* Filter is set to reject selected IDs */
} Fr_CCFifoRangeFilterModeType;

typedef enum {
    FR_RECEIVE_FIFOA = 0U,  /* Receive FIFO A */
    FR_RECEIVE_FIFOB        /* Receive FIFO B */
} Fr_CCFifoChannelType;


typedef void Fr_ConfigType;

typedef struct {
    /* Base address of the FlexRay module */
    const uint32 CCBaseAddress;
    /* Base address of the FlexRay memory */
    const uint8 *const CCFlexRayMemoryBaseAddress;
    /* Channel to which the node is connected */
    const Fr_ChannelType Channels;
    /* Enabling of single channel mode (FALSE - dual channel mode) */
    const boolean SingleChannelModeEnabled;     /* nonstandard */
    /* Channel Bit Rate selection */
    const uint8 Bitrate;                        /* nonstandard */
    /* Timeout value in the MCR reg. */
    const uint8 Timeout;                        /* nonstandard */
    /* Offset for sync frame tables */
    const uint16 SyncFrameTableOffset;
    /* Second absolute timer enable/disable */
    const boolean EnableSecondaryAbsTimer;
} Fr_CCHardwareConfigType;

typedef struct {
    /* Buffer type */
    const Fr_BufferType BufferType;
    /* Reference to buffer configuration structure */
    const void *const BufferConfigPtr;
    /* Individual Message Buffer Number */
    const uint16 MessageBufferNumber;
    /* Buffer should be used for the initialization? */
    const boolean FirstInitialization;
    /* Buffer is Reconfigurable */
    const boolean Reconfigurable;
} Fr_CCLpduInfoType;

typedef struct {
    uint8 *const MbDataAddrPtr;
    /* Address pointer of the message buffer data field */
} Fr_CCBufferAddressType;

typedef struct {
    const uint16 DataOffset16;
    /* Offset value of the message buffer data field */
} Fr_CCBufferOffset16Type;

typedef struct {
    /* Reference to configuration information of one message buffer configuration set */
    const Fr_CCLpduInfoType *const LPduInfoPtr;
    /* Initial MB offsets 32-bit */
    const Fr_CCBufferAddressType *const BufferAddressTable;
    /* Initial MB offsets 16-bit */
    const Fr_CCBufferOffset16Type *const BufferOffsetTable;
    /* The number of items in the configuration structure of the type Fr_CCLpduInfoType - i.e. the
    the number of virtual resources */
    const uint16 BuffersConfiguredCount;
    /* Data size - segment 1 */
    const uint8 MessageBufferSegment1DataSize;
    /* Data size - segment 2 */
    const uint8 MessageBufferSegment2DataSize;
    /* Last MB in segment 1 (Number of MB in Segment1 - 1) */
    const uint16 LastMBSEG1;
    /* Last individual MB;(Number of MB in Segment1 + Number of MB in Segment2 - 1) */
    const uint16 LastMBUTIL;
    /* Ch A, seg 1 - the initial index of the MB header field */
    const uint16 RSBIR_A1BufferIndexInit;
    /* Ch B, seg 1 - the initial index of the MB header field */
    const uint16 RSBIR_B1BufferIndexInit;
    /* Ch A, seg 2 - the initial index of the MB header field */
    const uint16 RSBIR_A2BufferIndexInit;
    /* Ch B, seg 2 - the initial index of the MB header field */
    const uint16 RSBIR_B2BufferIndexInit;

} Fr_CCBufferConfigSetType;


typedef struct {
    const uint16 RegPCR0;
    const uint16 RegPCR1;
    const uint16 RegPCR2;
    const uint16 RegPCR3;
    const uint16 RegPCR4;
    const uint16 RegPCR5;
    const uint16 RegPCR6;
    const uint16 RegPCR7;
    const uint16 RegPCR8;
    const uint16 RegPCR9;
    const uint16 RegPCR10;
    const uint16 RegPCR11;
    const uint16 RegPCR12;
    const uint16 RegPCR13;
    const uint16 RegPCR14;
    const uint16 RegPCR15;
    const uint16 RegPCR16;
    const uint16 RegPCR17;
    const uint16 RegPCR18;
    const uint16 RegPCR19;
    const uint16 RegPCR20;
    const uint16 RegPCR21;
    const uint16 RegPCR22;
    const uint16 RegPCR23;
    const uint16 RegPCR24;
    const uint16 RegPCR25;
    const uint16 RegPCR26;
    const uint16 RegPCR27;
    const uint16 RegPCR28;
    const uint16 RegPCR29;
    const uint16 RegPCR30;

    //const uint16 RegPCR[31];

    /* FlexRay parameter gNumberoftaticslots */
    const uint16 gNumberOfStaticSlots;
    /* FlexRay parameter gNetworkManagementVectorLength */
    const uint8 gNetworkManagementVectorLength;
    /* pPayloadLengthDynMax */
    const uint8 pPayloadLengthDynMax;
    /* gPayloadLengthStatic */
    const uint8 gPayloadLengthStatic;
} Fr_CCLowLevelConfigSetType;

typedef struct {
    /* Enable/Disable reporting of DEM messages */
    const uint32 state;
    /* ID of DEM message */
    const uint32 id;
} Fr_DemErrorType;

typedef struct {
    /* Controller index of the FlexRay module */
    const uint32 CtrlIdx;
    /* Reference to array of the Hardware Configuration structures */
    const Fr_CCHardwareConfigType *const CCHardwareConfigPtr;
    /* Reference to array of Buffer Configuration structures */
    const Fr_CCBufferConfigSetType *const BufferConfigSetPtr;
    /* Reference to array of Low Level Configuration structures */
    const Fr_CCLowLevelConfigSetType *LowLevelConfigSetPtr;
    /* Reference to array of FlexRay POC Configuration parameters */
    const uint32 *const  CCReadBackConfigSetPtr;
    /* Referece to FrDemCtrlTestResult */
    const Fr_DemErrorType *const FrDemCtrlTestResultPtr;
} Fr_CtrlCfgType;


typedef struct {
    const Fr_CtrlCfgType *Fr_pController;
    const uint32 u32ParCoreId;
} Fr_ConfigurationType;


typedef struct {
    /* Transmit frame ID */
    const uint16 TxFrameID;
    /* Header CRC */
    const uint16 HeaderCRC;
    /* Payload length [in Words] */
    const uint8 TxPayloadLength;
    /* Transmission mode */
    const boolean TxChannelAEnable;
    /* Transmit channel B enable */
    const boolean TxChannelBEnable;
    /* Payload preamble */
    const boolean PayloadPreamble;
    /* Transmit cycle counter filter enable */
    const boolean TxCycleCounterFilterEnable;
    /* Transmit cycle counter filter value */
    const uint8 TxCycleCounterFilterValue;
    /* Transmit cycle counter filter mask */
    const uint8 TxCycleCounterFilterMask;
    /* Value of the FrIfAllowDynamicLSduLength parameter */
    const boolean AllowDynamicLength;
    /* DemEventId */
    const uint16 DemFTSlotStatus;
    /* DemFTSlotSTatusRefExist exist */
    const boolean DemFTSlotSTatusRefExist;
} Fr_CCTxBufferConfigType;


typedef struct {
    /* Receive frame ID */
    const uint16 RxFrameID;
    /* Maximum payload length [in Words] - only for checking */
    const uint8 RxPayloadLength;
    /* Receive channel A enable */
    const boolean RxChannelAEnable;
    /* Receive channel B enable */
    const boolean RxChannelBEnable;
    /* Receive cycle counter filter enable */
    const boolean RxCycleCounterFilterEnable;
    /* Receive cycle counter filter value */
    const uint8 RxCycleCounterFilterValue;
    /* Receive cycle counter filter mask */
    const uint8 RxCycleCounterFilterMask;
    /* DemEventId */
    const uint16 DemFTSlotStatus;
    /* DemFTSlotSTatusRefExist exist */
    const boolean DemFTSlotSTatusRefExist;
} Fr_CCRxBufferConfigType;


typedef struct {
    /* TRUE - Range Filter is enabled */
    const boolean RangeFilterEnable;
    /* Acceptance or Rejection mode */
    const Fr_CCFifoRangeFilterModeType RangeFilterMode;
    /* SID0 - Slot ID - Lower interval boundary */
    const uint16 RangeFilterLowerInterval;
    /* SID1 - Slot ID - Upper interval boundary */
    const uint16 RangeFilterUpperInterval;
} Fr_CCFifoRangeFiltersType;


typedef struct {
    /* Selects the receive FIFO (FIFO A, FIFO B). */
    const Fr_CCFifoChannelType FIFOChannel;
    /* Configures the index of the first FIFO message buffer.*/
    const uint16 FIFOStartIndex;
    /* Configures the FIFO depth. */
    const uint8 FIFODepth;
    /* Configures the FIFO entry size [in Words]. */
    const uint8 FIFOEntrySize;
    /* Message ID filter mask value */
    const uint16 MessageIDFilterMask;
    /* Message ID filter match value */
    const uint16 MessageIDFilterMatch;
    /* Contains range filters configuration. */
    const Fr_CCFifoRangeFiltersType FIFORangeFiltersConfig[4];
} Fr_CCFifoConfigType;

/***************************************************************************************************
* @function_name    Fr_Driver_GetPOCStatus
*
* @brief            FrIP function for quering the controller POC status
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[out]       pPocStatus - Address of the variable where the Protocol status is stored to
* @return           ::Std_ReturnType
*                       - E_OK      Function call finished successfully
*                       - E_NOT_OK  Function call aborted due to hardware errors
***************************************************************************************************/
Std_ReturnType Fr_Driver_GetPOCStatus(
    const Fr_CtrlCfgType *pCtrlCfg,
    Fr_POCStatusType *pPocState
);

/***************************************************************************************************
* @function_name    Fr_Driver_GetPOCState
*
* @brief            FrIP function for quering the controller POC status
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @return           none
***************************************************************************************************/
uint16 Fr_Driver_GetPOCState(
    const Fr_CtrlCfgType *pCtrlCfg
);

/***************************************************************************************************
* @function_name    Fr_Driver_WaitForCmdProcess
*
* @brief            FrIP function for waiting until CC process current command or timeout expires
* @param[in]        ctrlIdx - Index of FlexRay CC
* @return           ::Std_ReturnType
*                       - E_OK      Function call finished successfully:
*                       - E_NOT_OK  Waiting loop timeout reached before CHI processed previous
*                                   command.
***************************************************************************************************/
Std_ReturnType Fr_Driver_WaitForCmdProcess(
    const Fr_CtrlCfgType *pCtrlCfg
);

/***************************************************************************************************
* @function_name    Fr_Driver_InvokeCHICommand
*
* @brief            FrIP function for invoking POCCMD commands
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[in]        CHICommand - required CHI command
* @return           ::Std_ReturnType:
*                       - E_OK      CHI command was accepted by PE
*                       - E_NOT_OK  CHI command was not accepted by PE due to BSY flag
*
* @implements       Fr_Flexray_InvokeCHICommand_Activity
***************************************************************************************************/
Std_ReturnType Fr_Driver_InvokeCHICommand(
    const Fr_CtrlCfgType *pCtrlCfg,
    uint16 CHICommand
);

/***************************************************************************************************
* @function_name    Fr_Driver_WaitForPOCState
*
* @brief            Wait for selected POC state
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[in]        u16PocState indicates which POC state should be reached
* @return           ::Std_ReturnType
*                       - E_OK      API call has been successful
*                       - E_NOT_OK  API call aborted due to errors
***************************************************************************************************/
Std_ReturnType Fr_Driver_WaitForPOCState(
    const Fr_CtrlCfgType *pCtrlCfg,
    const uint16 u16POCState
);

/***************************************************************************************************
* @function_name    Fr_Driver_CCInit
*
* @brief            FrIP function for FlexRay CC configuration
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @return           none
***************************************************************************************************/
Std_ReturnType Fr_Driver_CCInit(
    const Fr_CtrlCfgType *pCtrlCfg
);

/***************************************************************************************************
* @function_name    Fr_Driver_EnterPOCConfigState
*
* @brief            FrIP function for entering the POC:Config state
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @return           ::Std_ReturnType
*                       - E_OK      API call has been successful
*                       - E_NOT_OK  Error occurred during transition into POC:Config
***************************************************************************************************/
Std_ReturnType Fr_Driver_EnterPOCConfigState(
    const Fr_CtrlCfgType *pCtrlCfg
);


/***************************************************************************************************
* @function_name    Fr_Driver_ClusterNodeParamCfg
*
* @brief            FrIP function for cluster and node parameters configuration
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @return           none
***************************************************************************************************/
void Fr_Driver_ClusterNodeParamCfg(
    const Fr_CtrlCfgType *pCtrlCfg
);

/***************************************************************************************************
* @function_name    Fr_Driver_BuffersInit
*
* @brief            FrIP function for message buffers configuration
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @return           ::Std_ReturnType
*                       - E_OK      Function call finished successfully
*                       - E_NOT_OK  Function call aborted due to errors
***************************************************************************************************/
Std_ReturnType Fr_Driver_BuffersInit(
    const Fr_CtrlCfgType *pCtrlCfg
);

/***************************************************************************************************
* @function_name    Fr_Driver_DisableTimers
*
* @brief            IP function for disabling timer interrupts
* @description      Disables timers
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @return           none
***************************************************************************************************/
void Fr_Driver_DisableTimers(
    const Fr_CtrlCfgType *pCtrlCfg
);

/***************************************************************************************************
* @function_name    Fr_Driver_LeavePOCConfigState
*
* @brief            FrIP function for leaving the POC:Config state - entering POC:Ready state
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @return           ::Std_ReturnType:
*                       - E_OK      API call has been successful
*                       - E_NOT_OK  Error occurred during transition into POC:Ready
***************************************************************************************************/
Std_ReturnType Fr_Driver_LeavePOCConfigState(
    const Fr_CtrlCfgType *pCtrlCfg
);

/***************************************************************************************************
* @function_name    Fr_Driver_ClearDisableIRQs
*
* @brief            FrIP function which clears all interrupt flags and disables all interrupts.
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @return           ::Std_ReturnType
*                       - E_OK      All interrupt flags were cleared and all interrupts were
*                                   disabled
*                       - E_NOT_OK  Some interrupt flag was not clear or some interrupt was not
*                                   disabled
***************************************************************************************************/
Std_ReturnType Fr_Driver_ClearDisableIRQs(
    const Fr_CtrlCfgType *pCtrlCfg
);

/***************************************************************************************************
* @function_name    Fr_Driver_SetWakeupChannel
*
* @brief            FrIP function for the Fr_SetWakeupChannel API
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[in]        chnlIdx - Index of FlexRay channel
* @return           ::Std_ReturnType:
*                       - E_OK      API call has been successful
*                       - E_NOT_OK  Error occurred during set wakeup channel
***************************************************************************************************/
Std_ReturnType Fr_Driver_SetWakeupChannel
(
    const Fr_CtrlCfgType *pCtrlCfg,
    Fr_ChannelType chnlIdx
);

/***************************************************************************************************
* @function_name    Fr_Driver_SetAbsoluteTimer
*
* @brief            FrIP function for the Fr_SetAbsoluteTimer API
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[in]        u8TimerIdx - Index of FlexRay timer
* @param[in]        u8Cycle - Cycle the timer shall elapse in
* @param[in]        u16Offset - Offset within cycle Fr_Cycle in units of macrotick the timer shall
*                            elapse at
* @return           none
***************************************************************************************************/
void Fr_Driver_SetAbsoluteTimer
(
    const Fr_CtrlCfgType *pCtrlCfg,
    uint8 u8TimerIdx,
    uint8 u8Cycle,
    uint16 u16Offset
);

/***************************************************************************************************
* @function_name    Fr_Driver_TransmitTxLPdu
*
* @brief            FrIP function for the Fr_TransmitTxLPdu API
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[in]        LPduIdx - Index of LPdu to be transmitted
* @param[in]        pLsdu - Address of data to be transmitted
* @param[in]        LsduLength - Payload length of data to be transmitted
* @param[out]       pSlotAssignment - This reference points to the memory location where the
                    actual cycle, slot ID, and channel of the frame identified by
                    Fr_u16LPduIdx shall be stored. A NULL_PTR indicates that the
* @return           ::Std_ReturnType
*                       - E_OK      API call has been successful
*                       - E_NOT_OK  API call aborted due to errors
*
* @implements       Fr_Flexray_TransmitTxLPdu_Activity
***************************************************************************************************/
Std_ReturnType Fr_Driver_TransmitTxLPdu(
    const Fr_CtrlCfgType *pCtrlCfg,
    uint16 LPduIdx,
    const uint8 *pLsdu,
    uint8 LsduLength,
    Fr_SlotAssignmentType *pSlotAssignment
);

/***************************************************************************************************
* @function_name    Fr_Flexray_ReceiveRxLPdu
*
* @brief            FrIP function for the Fr_ReceiveRxLPdu API
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[in]        LPduIdx - Index of LPdu to be transmitted
* @param[out]       Lsdu - Address of field that data is copied to
* @param[out]       pLPduStatus - Address of the variable the status is stored to
* @param[out]       pLsduLength - Address of the variable the payload is stored to
* @param[out]       pSlotAssignment - Address of the variable the payload is stored to
* @return           ::Std_ReturnType
*                       - E_OK      Function call finished successfully:
*                       - E_NOT_OK  Function call aborted due to no access to the controller
***************************************************************************************************/
Std_ReturnType Fr_Driver_ReceiveRxLPdu(
    const Fr_CtrlCfgType *pCtrlCfg,
    uint16 LPduIdx,
    uint8 *pLsdu,
    Fr_RxLPduStatusType *pLPduStatus,
    uint8 *pLsduLength,
    Fr_SlotAssignmentType *pSlotAssignment
);

/***************************************************************************************************
* @function_name    Fr_Driver_ReceiveFifo
*
* @brief            FrIP function for the Fr_ReceiveRxLPdu API
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[in]        u16LPduIdx - Index of LPdu to be transmitted
* @param[out]       pu8Lsdu - Address of field that data is copied to
* @param[out]       pLPduStatus - Address of the variable the status is stored to
* @param[out]       pu8LsduLength - Address of the variable the payload is stored to
* @return           none
*
* @implements       Fr_Flexray_ReceiveFifo_Activity
***************************************************************************************************/
void Fr_Driver_ReceiveFifo(
    const Fr_CtrlCfgType *pCtrlCfg,
    uint16 LPduIdx,
    uint8 *pLsdu,
    Fr_RxLPduStatusType *pLPduStatus,
    uint8 *pLsduLength,
    Fr_SlotAssignmentType *pSlotAssignment,
    const Fr_CCFifoChannelType nFIFOChannel
);

/***************************************************************************************************
* @function_name    Fr_Driver_CancelTxLPdu
*
* @brief            FrIP function for the Fr_CancelTxLPdu API
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[in]        LPduIdx - Index of LPdu to be canceled
* @param[out]       pbPendingStatus - Information whether the LPdu was canceled or not
* @return           ::Std_ReturnType
*                       - E_OK      Function call finished successfully:
*                       - E_NOT_OK  Function call aborted due to no access to the FlexRay CC
***************************************************************************************************/
Std_ReturnType Fr_Driver_CancelTxLPdu(
    const Fr_CtrlCfgType *pCtrlCfg,
    uint16 LPduIdx,
    boolean *pbPendingStatus
);

/***************************************************************************************************
* @function_name    Fr_Driver_CheckTxLPduStatus
*
* @brief            FrIP function for the Fr_CheckTxLPduStatus API
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[in]        LPduIdx - Index of LPdu to be canceled
* @param[out]       pTxLPduStatus - Address of the variable where the status information is stored to
* @param[out]       pSlotAssignment - Address of the variable where the status information is stored to
* @return           ::Std_ReturnType
*                       - E_OK      Function call finished successfully:
*                       - E_NOT_OK  Function call aborted due to no access to the FlexRay CC
*
* @implements       Fr_Flexray_CheckTxLPduStatus_Activity
***************************************************************************************************/
Std_ReturnType Fr_Driver_CheckTxLPduStatus(
    const Fr_CtrlCfgType *pCtrlCfg,
    uint16 LPduIdx,
    Fr_TxLPduStatusType *pTxLPduStatus,
    Fr_SlotAssignmentType *pSlotAssignment
);

/***************************************************************************************************
* @function_name    Fr_Driver_GetGlobalTime
*
* @brief            FrIP function for the Fr_GetGlobalTime API
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[out]       pu8Cycle - Address where current cycle is stored to
* @param[out]       pu16MacroTick - Address where current macrotick is stored to
* @return           none
***************************************************************************************************/
Std_ReturnType Fr_Driver_GetGlobalTime
(
    const Fr_CtrlCfgType *pCtrlCfg,
    uint8 *pu8Cycle,
    uint16 *pu16MacroTick
);

/***************************************************************************************************
* @function_name    Fr_Driver_GetNmVector
*
* @brief            FrIP function for the Fr_GetNmVector API
* @param[in]        ctrlIdx - Index of FlexRay CC
* @param[out]       pu8NmVector - Address of the variable the NmVector is stored to
* @return           ::Std_ReturnType
*                       - E_OK      API call has been successful
*                       - E_NOT_OK  API call aborted due to errors
***************************************************************************************************/
Std_ReturnType Fr_Driver_GetNmVector
(
    const Fr_CtrlCfgType *pCtrlCfg,
    uint8 *pu8NmVector,
    boolean *pbHwErr
);

/***************************************************************************************************
* @function_name    Fr_Driver_GetNumOfStartupFrames
*
* @brief            FrIP function for the Fr_GetNumOfStartupFrames
* @description      Reads number of startup frames
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[in]        pu8NumOfStartupFrames - Index of LPdu to be reconfigured
* @return           ::Std_ReturnType
*                       - E_OK      Function call finished successfully:
*                       - E_NOT_OK  Function call aborted due to no access to the FlexRay CC
***************************************************************************************************/
Std_ReturnType Fr_Driver_GetNumOfStartupFrames
(
    const Fr_CtrlCfgType *pCtrlCfg,
    uint8 *pu8NumOfStartupFrames
);

/***************************************************************************************************
* @function_name    Fr_Driver_GetChannelStatus
*
* @brief            FrIP function for the Fr_GetChannelStatus API
* @description      Reads the aggregated channel status, NIT status and symbol window status from
*                   FLEXRAY_PSR2_ADDR16, and FLEXRAY_PSR3_ADDR16 registers
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[out]       pu16ChannelAStatus - Address of the variable the channel A status is stored to
* @param[out]       pu16ChannelBStatus - Address of the variable the channel B status is stored to
* @return           none
*
* @implements       Fr_Flexray_GetChannelStatus_Activity
***************************************************************************************************/
void Fr_Driver_GetChannelStatus
(
    const Fr_CtrlCfgType *pCtrlCfg,
    uint16 *pu16ChannelAStatus,
    uint16 *pu16ChannelBStatus
);

/***************************************************************************************************
* @function_name    Fr_Driver_GetClockCorrection
*
* @brief            FrIP function for the Fr_GetChannelStatus API
* @description      Read rate and offset correction values from FR_RTCORVR and FR_OFCORVR registers
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[out]       ps16RateCorrection - Address of the variable the rate correction value is
*                                       stored to
* @param[out]       ps16OffsetCorrection - Address of the variable the offset correction value is
*                                         stored to
*
* @implements       Fr_Flexray_GetClockCorrection_Activity
***************************************************************************************************/
Std_ReturnType Fr_Driver_GetClockCorrection
(
    const Fr_CtrlCfgType *pCtrlCfg,
    sint16 *ps16RateCorrection,
    sint32 *ps32OffsetCorrection
);

/***************************************************************************************************
* @function_name    Fr_Driver_GetSyncFrameList
*
* @brief            FrIP function for the Fr_GetSyncFrameList API
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[out]       pu16ChannelAEvenList - Address of the variable the channel A even list is
*                                         stored to
* @param[out]       pu16ChannelBEvenList - Address of the variable the channel B even list is
*                                         stored to
* @param[out]       pu16ChannelAOddList - Address of the variable the channel A odd list is
*                                        stored to
* @param[out]       pu16ChannelBOddList - Address of the variable the channel B odd list is
*                                        stored to
* @return           ::Std_ReturnType
*                       - E_OK      API call has been successful
*                       - E_NOT_OK  API call aborted due to errors
***************************************************************************************************/
Std_ReturnType Fr_Driver_GetSyncFrameList
(
    const Fr_CtrlCfgType *pCtrlCfg,
    uint8 u8ListSize,
    uint16 *pu16ChannelAEvenList,
    uint16 *pu16ChannelBEvenList,
    uint16 *pu16ChannelAOddList,
    uint16 *pu16ChannelBOddList
);

/***************************************************************************************************
* @function_name    Fr_Driver_GetWakeupRxStatus
*
* @brief            FrIP function for the Fr_GetWakeupRxStatus API
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[out]       pu8WakeupRxStatus - Address of a variable where the wakeup Rx status is stored to
* @return           none
***************************************************************************************************/
Std_ReturnType Fr_Driver_GetWakeupRxStatus
(
    const Fr_CtrlCfgType *pCtrlCfg,
    uint8 *pu8WakeupRxStatus
);

/***************************************************************************************************
* @function_name    Fr_Flexray_CancelAbsTimer
*
* @brief            FrIP function for the Fr_CancelAbsoluteTimer API
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[in]        u8TimerIdx - Index of absolute timer within the context of the FlexRay CC
* @return           none
***************************************************************************************************/
void Fr_Driver_CancelAbsoluteTimer
(
    const Fr_CtrlCfgType *pCtrlCfg,
    uint8 u8TimerIdx
);

/***************************************************************************************************
* @function_name    Fr_Driver_ReadCCConfig
*
* @brief            FrIP function for the Fr_ReadCCConfig API
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @return           ::Std_ReturnType
*                       - E_OK      Configuration is OK
*                       - E_NOT_OK  Error in configuration was found
***************************************************************************************************/
Std_ReturnType Fr_Driver_ReadCCConfig
(
    const Fr_CtrlCfgType *pCtrlCfg
);

#if(FR_PREPARE_LPDU_SUPPORT == STD_ON)
/***************************************************************************************************
* @function_name    Fr_Driver_PrepareLPdu
*
* @brief            FrIP function for the Fr_PrepareLPdu API
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[in]        LPduIdx - Index of LPdu to be prepared
* @return           ::Std_ReturnType
*                       - E_OK      Function call finished successfully:
*                       - E_NOT_OK  Function call aborted due to no access to the FlexRay CC
***************************************************************************************************/
Std_ReturnType Fr_Driver_PrepareLPdu(
    const Fr_CtrlCfgType *pCtrlCfg,
    uint16 LPduIdx
);
#endif

#if(FR_RECONFIG_LPDU_SUPPORT == STD_ON)
/***************************************************************************************************
* @function_name    Fr_Driver_ReconfigLPdu
*
* @brief            FrIP function for the Fr_ReconfigLPdu API
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[in]        LPduIdx - Index of LPdu to be reconfigured
* @param[in]        FrameId - Frame ID for reconfiguration
* @param[in]        CycleRepetition - Repetition value for cycle filter mechanism
* @param[in]        cycleOffste - Offste value for cycle filter mechanism
* @param[in]        PayloadLength - payload for data to be reconfigured
* @param[in]        HeaderCRC - hedaer CRC value
* @return           ::Std_ReturnType
*                       - E_OK      Function call finished successfully:
*                       - E_NOT_OK  Function call aborted due to no access to the FlexRay CC
*
* @implements       Fr_Flexray_ReconfigLPdu_Activity
***************************************************************************************************/
Std_ReturnType Fr_Driver_ReconfigLPdu(
    const Fr_CtrlCfgType *pCtrlCfg,
    uint16 LPduIdx,
    uint16 FrameId,
    Fr_ChannelType chnlIdx,
    uint8 CycleRepetition,
    uint8 CycleOffset,
    uint8 PayloadLength,
    uint16 HeaderCRC
);
#endif

#if(FR_DISABLE_LPDU_SUPPORT == STD_ON)
/***************************************************************************************************
* @function_name    Fr_Driver_DisableLPdu
*
* @brief            FrIP function for the Fr_Flexray_DisableLPdu
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[in]        LPduIdx - Index of LPdu to be reconfigured
* @return           ::Std_ReturnType
*                       - E_OK      Function call finished successfully:
*                       - E_NOT_OK  Function call aborted due to no access to the FlexRay CC
***************************************************************************************************/
Std_ReturnType Fr_Driver_DisableLPdu(
    const Fr_CtrlCfgType *pCtrlCfg,
    uint16 LPduIdx
);
#endif

/***************************************************************************************************
* @function_name    Fr_Driver_EnableAbsTimerIRQ
*
* @brief            FrIP function for the Fr_EnableAbsoluteTimerIRQ
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[in]        TimerIdx - Index of absolute timer within the context of the FlexRay CC
* @return           none
***************************************************************************************************/
void Fr_Driver_EnableAbsTimerIRQ(
    const Fr_CtrlCfgType *pCtrlCfg,
    uint8 TimerIdx
);

/***************************************************************************************************
* @function_name    Fr_Driver_AckAbsTimerIRQ
*
* @brief            FrIP function for the Fr_AckAbsoluteTimerIRQ API
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[in]        TimerIdx Index of absolute timer within the context of the FlexRay CC
* @return           none
***************************************************************************************************/
void Fr_Driver_AckAbsTimerIRQ(
    const Fr_CtrlCfgType *pCtrlCfg,
    uint8 TimerIdx
);

/***************************************************************************************************
* @function_name    Fr_Driver_DisableAbsTimerIRQ
*
* @brief            FrIP function for the Fr_DisableAbsoluteTimerIRQ API
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[in]        TimerIdx - Index of absolute timer within the context of the FlexRay CC
* @return           none
***************************************************************************************************/
void Fr_Driver_DisableAbsTimerIRQ(
    const Fr_CtrlCfgType *pCtrlCfg,
    uint8 TimerIdx
);

/***************************************************************************************************
* @function_name    Fr_Driver_GetAbsTimerIRQStatus
*
* @brief            FrIP function for the Fr_GetAbsoluteTimerIRQStatus
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[in]        TimerIdx - Index of absolute timer within the context of the FlexRay CC
* @return           ::boolean
*                     - TRUE - Interrupt is pending
*                     - FALSE - Interrupt is not pending
*
* @implements       Fr_Flexray_GetAbsTimerIRQStatus_Activity
***************************************************************************************************/
boolean Fr_Driver_GetAbsTimerIRQStatus(
    const Fr_CtrlCfgType *pCtrlCfg,
    uint8 TimerIdx
);

/***************************************************************************************************
* @function_name    Fr_Driver_CheckCCAccess
*
* @brief            IP function for checking access to the CC
* @description      Checks access to the CC
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[in]        bCCEnabled - Indicates whether CC module should be enabled or not
* @return           ::Std_ReturnType
*                       - E_OK      Function call finished successfully:
*                       - E_NOT_OK  Function call aborted due to no access to the FlexRay CC
***************************************************************************************************/
Std_ReturnType Fr_Driver_CheckCCAccess(
    const Fr_CtrlCfgType *pCtrlCfg,
    const boolean bCCEnabled
);

/***************************************************************************************************
* @function_name    Fr_Driver_InterruptEnable
*
* @brief            Enable interruption
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[in]        GIE - Interrupt flag bit
* @return           void
***************************************************************************************************/
void Fr_Driver_InterruptEnable(
    const Fr_CtrlCfgType *pCtrlCfg,
    uint16 GIE
);

/***************************************************************************************************
* @function_name    Fr_Driver_CheckCCAccess
*
* @brief            Disable interruption
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[in]        GIE - Interrupt flag bit
* @return           void
***************************************************************************************************/
void Fr_Driver_InterruptDisable(
    const Fr_CtrlCfgType *pCtrlCfg,
    uint16 GIE
);

#ifdef __cplusplus
}
#endif

#endif /* SDRV_FLEXRAY_DRIVER_H_ */