/*
 * SEMIDRIVE Copyright Statement
 * Copyright (c) SEMIDRIVE. All rights reserved
 *
 * This software and all rights therein are owned by SEMIDRIVE, and are
 * protected by copyright law and other relevant laws, regulations and
 * protection. Without SEMIDRIVE's prior written consent and/or related rights,
 * please do not use this software or any potion thereof in any form or by any
 * means. You may not reproduce, modify or distribute this software except in
 * compliance with the License. Unless required by applicable law or agreed to
 * in writing, software distributed under the License is distributed on
 * an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND, either express or implied.
 *
 * You should have received a copy of the License along with this program.
 * If not, see <http:/www.semidrive.com/licenses/>.
 */

/**
 * @file  Fr_Driver.c
 * @brief Semidrive. AUTOSAR 4.3.1 MCAL Fr plugins.
 */

#include "sdrv_flexray_driver.h"

#ifdef CPU_TYPE
#if (CPU_TYPE == CPU_TYPE_64)
/**
* @brief size of pointer type. on CPU_TYPE_64, the size is 64bits.
*/
typedef uint64 Fr_Driver_PointerSizeType;
#elif (CPU_TYPE == CPU_TYPE_32)
/**
* @brief size of pointer type. on CPU_TYPE_32, the size is 32bits.
*/
typedef uint32 Fr_Driver_PointerSizeType;
#elif (CPU_TYPE == CPU_TYPE_16)
/**
* @brief size of pointer type. on CPU_TYPE_16, the size is 16bits.
*/
typedef uint16 Fr_Driver_PointerSizeType;
#else
typedef uint32 Fr_Driver_PointerSizeType;
#endif
#endif

#if (FR_RXSTRINGENTCHECK == STD_OFF)
#define FLEXRAY_STRINGENT_MASK_A  0x0080U
#define FLEXRAY_STRINGENT_MASK_B  0x8000U
#else
#define FLEXRAY_STRINGENT_MASK_A  0x008EU   /*Check SEA, CEA and BVA for Error*/
#define FLEXRAY_STRINGENT_MASK_B  0x8E00U   /*Check SEA, CEA and BVA for Error*/
#endif

#if CPU_BYTE_ORDER
#define IDX_CONVERT(x)  (((x) & 0xFCU) + 3U - ((x) & 3U))
#else
#define IDX_CONVERT(x)  (x)
#endif

#if (FR_PREPARE_LPDU_SUPPORT == STD_ON)
/* Only if FR_PREPARE_LPDU_SUPPORT is enabled: */
/* Array is used for storage of information which virtual message buffer is currently configured
for the physical message buffer each array element represents one physical MB and stored value
corresponds with configured virtual MB index.
Example: VirtualResourceAllocation[15] means that MB with index 15 (MBCCSR15,MBIDX15...) is
configured for virtual MB with index 26 */
uint16 Fr_VirtualResourceAllocation[FR_NUM_CTRL_SUPPORTED][FR_NUMBER_MB] = {{0U}};
#endif  /* FR_PREPARE_LPDU_SUPPORT */



/***************************************************************************************************
* @function_name    Fr_Driver_GetData16Pointer
*
* @brief            FrIP function which returns pointer to 16bit data in the FlexRay memory
* @param[in]        data16Address - Address in FlexRay memory
* @return           16bit data pointer
***************************************************************************************************/
static volatile uint16 *Fr_Driver_GetData16Pointer(const
        Fr_Driver_PointerSizeType data16Address);

/***************************************************************************************************
* @function_name    Fr_Driver_GetData32Pointer
*
* @brief            FrIP function which returns pointer to 32bit data in the FlexRay memory
* @param[in]        data32Address - Address in FlexRay memory
* @return           32bit data pointer
***************************************************************************************************/
static uint32 *Fr_Driver_GetData32Pointer(const Fr_Driver_PointerSizeType
        data32Address);
#if (FR_PREPARE_LPDU_SUPPORT == STD_ON)

/***************************************************************************************************
* @function_name    Fr_Driver_PrepareTxBuffer
*
* @brief            Fr function for the Fr_Flexray_PrepareLPdu function.
* @param[in]        ctrlIdx - Index of FlexRay CC
* @param[in]        u16LPduIdx - Index of LPdu to be prepared.
* @param[in]        u16TmpMBNum_31 - Temporary variable for the MB index.
***************************************************************************************************/
static Std_ReturnType Fr_Driver_PrepareTxBuffer(
    const Fr_CtrlCfgType *pCtrlCfg,
    uint16 LPduIdx,
    uint16 TempMBNum
);

/***************************************************************************************************
* @function_name    Fr_Driver_PrepareRxBuffer
*
* @brief            Fr function for the Fr_Flexray_PrepareLPdu function.
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[in]        LPduIdx - Index of LPdu to be prepared.
* @param[in]        TempMBNum - Temporary variable for the MB index.
***************************************************************************************************/
static Std_ReturnType Fr_Driver_PrepareRxBuffer(
    const Fr_CtrlCfgType *pCtrlCfg,
    uint16 LPduIdx,
    uint16 TempMBNum
);
#endif

/***************************************************************************************************
* @function_name    Fr_Driver_FrExtendedLPduReport
*
* @brief            Fr function for the Fr_Flexray_PrepareLPdu function.
* @param[in]        pTxCfg_11 - Pointer to one instance of Fr_CCTxBufferConfigType
* @param[out]       pSlotAssignment - This reference points to the memory location where the
                    actual cycle, slot ID, and channel of the frame identified by
                    Fr_u16LPduIdx shall be stored. A NULL indicates that the
***************************************************************************************************/
static void Fr_Driver_FrExtendedLPduReport (
    const Fr_CtrlCfgType *pCtrlCfg,
    const Fr_CCTxBufferConfigType *pTxCfg_11,
    Fr_SlotAssignmentType *pSlotAssignment
);

/***************************************************************************************************
* @function_name    Fr_Driver_ReadRegister
*
* @brief            FrIP function for reading the value from the FlexRay CC register
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[in]        u16RegOffset - Offset to FlexRay CC register area
* @return           16bit register data
***************************************************************************************************/
static uint16 Fr_Driver_ReadRegister (
    const Fr_CtrlCfgType *pCtrlCfg,
    const uint16 u16RegOffset
);

/***************************************************************************************************
* @function_name    Fr_Driver_WriteRegister
*
* @brief            FrIP function for writing into the FlexRay CC register
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[in]        u16RegOffset - Offset to FlexRay CC register area
* @param[in]        u16RegValue - Value to be written into FlexRay CC register
* @return           none
***************************************************************************************************/
static void Fr_Driver_WriteRegister
(
    const Fr_CtrlCfgType *pCtrlCfg,
    const uint16 u16RegOffset,
    const uint16 u16RegValue
);

/***************************************************************************************************
* @function_name    Fr_Driver_Calcu16HeaderCRC
*
* @brief            Header CRC calculating
* @param[in]        FrameId Frame ID
* @param[in]        PayloadLen Data payload length
* @return           ::uint16 - Calculated CRC Value
***************************************************************************************************/
static uint16 Fr_Driver_Calcu16HeaderCRC(const uint16 FrameId,
        const uint16 PayloadLen);

/***************************************************************************************************
* @function_name    Fr_Driver_GetErrorMode
*
* @brief            FrIP function for the Fr_GetPOCStatus API
* @param[in]        u16RegValuePSR0 - Value of the FLEXRAY_PSR0_ADDR16 register.
* @param[out]       pErrorMode - Address the Error mode is stored to.
* @return           ::Std_ReturnType
*                       - E_OK      Function call finished successfully
*                       - E_NOT_OK  Function call aborted due to hardware error
***************************************************************************************************/
static Std_ReturnType Fr_Driver_GetErrorMode
(
    uint16 u16RegValuePSR0,
    Fr_ErrorModeType *pErrorMode
);

/***************************************************************************************************
* @function_name    Fr_Driver_ReadMB
*
* @brief            FrIP function in order to read the contents into massage buffer.
* @param[in]        pCtrlCfg  - Controller config
* @param[in]        pu8Lsdu - Sdu pointer
* @param[in]        u8TmpPayloadLen_12 - Lenght of sdu
* @param[in]        u16TmpMBIdx_12 - MB index.
***************************************************************************************************/
static void Fr_Driver_ReadMB
(
    const Fr_CtrlCfgType *pCtrlCfg,
    uint8 *pLsdu,
    uint8 TempPayloadLen,
    uint16 TempMBIdx
);

/***************************************************************************************************
* @function_name    Fr_Driver_FrExtendedLPduReport
*
* @brief            Fr function for the Fr_Flexray_PrepareLPdu function.
* @param[in]        pTxCfg_11 - Pointer to one instance of Fr_CCTxBufferConfigType
* @param[out]       pSlotAssignment - This reference points to the memory location where the
                    actual cycle, slot ID, and channel of the frame identified by
                    Fr_u16LPduIdx shall be stored. A NULL indicates that the
***************************************************************************************************/
static void Fr_Driver_FrExtendedLPduReport
(
    const Fr_CtrlCfgType *pCtrlCfg,
    const Fr_CCTxBufferConfigType *pTxCfg_11,
    Fr_SlotAssignmentType *pSlotAssignment
)
{
    if (NULL != pSlotAssignment) {
        (pSlotAssignment->Cycle) = ((uint8)Fr_Driver_ReadRegister(pCtrlCfg,
                                    FLEXRAY_CYCTR_ADDR16));
        (pSlotAssignment->SlotId) = (pTxCfg_11->TxFrameID);

        /* channel transmit data */
        if (((boolean)TRUE == (pTxCfg_11->TxChannelAEnable))
                && ((boolean)TRUE == (pTxCfg_11->TxChannelBEnable))) {
            /* Transmit on both A and B channels */
            pSlotAssignment->channelId = FR_CHANNEL_AB;
        }
        else if (((boolean)TRUE == (pTxCfg_11->TxChannelAEnable))
                 && ((boolean)FALSE == (pTxCfg_11->TxChannelBEnable))) {
            /* Transmit on channel A only */
            pSlotAssignment->channelId = FR_CHANNEL_A;
        }
        else if (((boolean)FALSE == (pTxCfg_11->TxChannelAEnable))
                 && ((boolean)TRUE == (pTxCfg_11->TxChannelBEnable))) {
            /* Transmit on channel B only */
            pSlotAssignment->channelId = FR_CHANNEL_B;
        }
        else {
            /* Do nothing */
        }
    }
}

/***************************************************************************************************
* @function_name    Fr_Driver_GetData16Pointer
*
* @brief            FrIP function which returns pointer to 16bit data in the FlexRay memory
* @param[in]        data16Address - Address in FlexRay memory
* @return           16bit data pointer
***************************************************************************************************/
static volatile uint16 *Fr_Driver_GetData16Pointer
(
    const Fr_Driver_PointerSizeType data16Address
)
{
    return (volatile uint16 *) (data16Address);
}

/***************************************************************************************************
* @function_name    Fr_Flexray_GetData32Pointer
*
* @brief            FrIP function which returns pointer to 32bit data in the FlexRay memory
* @param[in]        data32Address - Address in FlexRay memory
* @return           32bit data pointer
***************************************************************************************************/
static uint32 *Fr_Driver_GetData32Pointer
(
    const Fr_Driver_PointerSizeType data32Address
)
{
    return (uint32 *) (data32Address);
}


/***************************************************************************************************
* @function_name    Fr_Driver_ReadRegister
*
* @brief            FrIP function for reading the value from the FlexRay CC register
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[in]        u16RegOffset - Offset to FlexRay CC register area
* @return           16bit register data
***************************************************************************************************/
static uint16 Fr_Driver_ReadRegister
(
    const Fr_CtrlCfgType *pCtrlCfg,
    const uint16 u16RegOffset
)
{
    uint16 u16RegValue = 0U;
    u16RegValue = (uint16)readl((volatile uint32 *)(
                                    pCtrlCfg->CCHardwareConfigPtr->CCBaseAddress + u16RegOffset));
    u16RegValue &= (uint16)FLEXRAY_REGISTER_MASK_LOW_U32;


    return u16RegValue;
}

/***************************************************************************************************
* @function_name    Fr_Driver_WriteRegister
*
* @brief            FrIP function for writing into the FlexRay CC register
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[in]        u16RegOffset - Offset to FlexRay CC register area
* @param[in]        u16RegValue - Value to be written into FlexRay CC register
* @return           none
***************************************************************************************************/
static void Fr_Driver_WriteRegister
(
    const Fr_CtrlCfgType *pCtrlCfg,
    const uint16 u16RegOffset,
    const uint16 u16RegValue
)
{
    writel((uint32)u16RegValue & FLEXRAY_REGISTER_MASK_LOW_U32, \
           (volatile uint32 *)(pCtrlCfg->CCHardwareConfigPtr->CCBaseAddress +
                               u16RegOffset));

}

/***************************************************************************************************
* @function_name    Fr_Driver_TxBufferCfg
*
* @brief            FrIP function for transmit message buffer configuration
* @param[in]        ctrlIdx - Index of FlexRay CC
* @param[in]        pTxLPduInfo Pointer to the structure with information for configuring MB
* @return           ::Std_ReturnType
*                       - E_OK      Message buffer was successfully configured
*                       - E_NOT_OK  Error occured within message buffer enabling/disabling
***************************************************************************************************/
static Std_ReturnType Fr_Driver_TxBufferCfg
(
    const Fr_CtrlCfgType *pCtrlCfg,
    const Fr_CCLpduInfoType *pTxLPduInfo
);

/***************************************************************************************************
* @function_name    Fr_Driver_RxBufferCfg
*
* @brief            FrIP function for receive message buffer configuration
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[in]        pRxLPduInfo Pointer to the structure with information for message buffer
*                   configuration.
* @return           ::Std_ReturnType
*                       - E_OK      Message buffer was successfully configured
*                       - E_NOT_OK  Error occured within message buffer enabling/disabling
***************************************************************************************************/
static Std_ReturnType Fr_Driver_RxBufferCfg
(
    const Fr_CtrlCfgType *pCtrlCfg,
    const Fr_CCLpduInfoType *pRxLPduInfo
);

/***************************************************************************************************
* @function_name    Fr_Driver_ConfigureFifo
*
* @brief            FrIP function for receive FIFO configuration
* @details          This function will configure receive FIFO
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[in]        pCCFIFOConfig Pointer to the structure with information for configuring MB
* @return           none
***************************************************************************************************/
static void Fr_Driver_ConfigureFifo
(
    const Fr_CtrlCfgType *pCtrlCfg,
    const Fr_CCFifoConfigType *pCCFIFOConfig
);

/***************************************************************************************************
* @function_name    Fr_Driver_GetProtocolState
*
* @brief            FrIP function for quering the state of the protocol
* @description      The FlexRay controller Protocol State is determined here.
* @param[in]        u16RegValuePSR0 - Value of the FLEXRAY_PSR0_ADDR16 register.
* @param[out]       pPOCState - Address the Protocol State is stored to.
* @return           none
***************************************************************************************************/
static Std_ReturnType Fr_Driver_GetProtocolState(
    uint16 PSR0,
    Fr_POCStateType *pPOCState
)
{
    Std_ReturnType retVal = (Std_ReturnType)E_OK;

    switch (PSR0 & FLEXRAY_PSR0_PROTSTATE_MASK_U16) {
        case FLEXRAY_PSR0_PROTSTATE_DEFAULT_CONFIG_U16: {
            *pPOCState = FR_POCSTATE_DEFAULT_CONFIG;
            break;
        }

        case FLEXRAY_PSR0_PROTSTATE_CONFIG_U16: {
            *pPOCState = FR_POCSTATE_CONFIG;
            break;
        }

        case FLEXRAY_PSR0_PROTSTATE_WAKEUP_U16: {
            *pPOCState = FR_POCSTATE_WAKEUP;
            break;
        }

        case FLEXRAY_PSR0_PROTSTATE_READY_U16: {
            *pPOCState = FR_POCSTATE_READY;
            break;
        }

        case FLEXRAY_PSR0_PROTSTATE_NORMAL_PASSIVE_U16: {
            *pPOCState = FR_POCSTATE_NORMAL_PASSIVE;
            break;
        }

        case FLEXRAY_PSR0_PROTSTATE_NORMAL_ACTIVE_U16: {
            *pPOCState = FR_POCSTATE_NORMAL_ACTIVE;
            break;
        }

        case FLEXRAY_PSR0_PROTSTATE_HALT_U16: {
            *pPOCState = FR_POCSTATE_HALT;
            break;
        }

        case FLEXRAY_PSR0_PROTSTATE_STARTUP_U16: {
            *pPOCState = FR_POCSTATE_STARTUP;
            break;
        }

        default: {
            retVal = (Std_ReturnType)E_NOT_OK;
            break;
        }
    }

    return retVal;
}

/***************************************************************************************************
* @function_name    Fr_Driver_GetWakeupState
*
* @brief            FrIP function for the Fr_GetPOCStatus API
* @param[in]        PSR0 - Value of the FLEXRAY_PSR0_ADDR16 register.
* @param[out]       pWakeupState - Address the Wakeup status is stored to.
* @return           ::Std_ReturnType
*                       - E_OK      Function call finished successfully
*                       - E_NOT_OK  Function call aborted due to hardware error
***************************************************************************************************/
static Std_ReturnType Fr_Driver_GetWakeupState(
    uint16 PSR0,
    Fr_WakeupStateType *pWakeupState
)
{
    Std_ReturnType retVal = (Std_ReturnType)E_OK;

    switch (PSR0 & FLEXRAY_PSR0_WUP_MASK_U16) {
        case FLEXRAY_PSR0_WUP_RH_U16: {
            *pWakeupState = FR_WAKEUP_RECEIVED_HEADER;
            break;
        }

        case FLEXRAY_PSR0_WUP_RW_U16: {
            *pWakeupState = FR_WAKEUP_RECEIVED_WUP;
            break;
        }

        case FLEXRAY_PSR0_WUP_HC_U16: {
            *pWakeupState = FR_WAKEUP_COLLISION_HEADER;
            break;
        }

        case FLEXRAY_PSR0_WUP_WC_U16: {
            *pWakeupState = FR_WAKEUP_COLLISION_WUP;
            break;
        }

        case FLEXRAY_PSR0_WUP_UC_U16: {
            *pWakeupState = FR_WAKEUP_COLLISION_UNKNOWN;
            break;
        }

        case FLEXRAY_PSR0_WUP_T_U16: {
            *pWakeupState = FR_WAKEUP_TRANSMITTED;
            break;
        }

        case FLEXRAY_PSR0_WUP_UD_U16: {
            *pWakeupState = FR_WAKEUP_UNDEFINED;
            break;
        }

        default: {
            *pWakeupState = FR_WAKEUP_UNDEFINED;
            retVal = (Std_ReturnType)E_NOT_OK;
            break;
        }
    }

    return retVal;
}

/***************************************************************************************************
* @function_name    Fr_Driver_GetStartupState
*
* @brief            FrIP function for the Fr_GetPOCStatus API
* @param[in]        PSR0 - Value of the FLEXRAY_PSR0_ADDR16 register.
* @param[out]       pStartupState - Address the Startup state is stored to.
* @return           ::Std_ReturnType
*                       - E_OK      Function call finished successfully
*                       - E_NOT_OK  Function call aborted due to hardware error
***************************************************************************************************/
static Std_ReturnType Fr_Driver_GetStartupState(
    uint16 PSR0,
    Fr_StartupStateType *pStartupState
)
{
    Std_ReturnType retVal = (Std_ReturnType)E_OK;

    switch (PSR0 & FLEXRAY_PSR0_STARTUP_MASK_U16) {
        case FLEXRAY_PSR0_STARTUP_CCR_U16: {
            *pStartupState = FR_STARTUP_COLDSTART_COLLISION_RESOLUTION;
            break;
        }

        case FLEXRAY_PSR0_STARTUP_CL_U16: {
            *pStartupState = FR_STARTUP_COLDSTART_LISTEN;
            break;
        }

        case FLEXRAY_PSR0_STARTUP_ICOC_U16: {
            *pStartupState = FR_STARTUP_INTEGRATION_CONSISTENCY_CHECK;
            break;
        }

        case FLEXRAY_PSR0_STARTUP_IL_U16: {
            *pStartupState = FR_STARTUP_INTEGRATION_LISTEN;
            break;
        }

        case FLEXRAY_PSR0_STARTUP_IS_U16: {
            *pStartupState = FR_STARTUP_INITIALIZE_SCHEDULE;
            break;
        }

        case FLEXRAY_PSR0_STARTUP_CCC_U16: {
            *pStartupState = FR_STARTUP_COLDSTART_CONSISTENCY_CHECK;
            break;
        }

        case FLEXRAY_PSR0_STARTUP_ICLC_U16: {
            *pStartupState = FR_STARTUP_INTEGRATION_COLDSTART_CHECK;
            break;
        }

        case FLEXRAY_PSR0_STARTUP_CG_U16: {
            *pStartupState = FR_STARTUP_COLDSTART_GAP;
            break;
        }

        case FLEXRAY_PSR0_STARTUP_CJ_U16: {
            *pStartupState = FR_STARTUP_COLDSTART_JOIN;
            break;
        }

        default : {
            *pStartupState = FR_STARTUP_UNDEFINED;
            retVal = (Std_ReturnType)E_NOT_OK;
            break;
        }
    }

    return retVal;
}

/***************************************************************************************************
* @function_name    Fr_Driver_GetSlotMode
*
* @brief            FrIP function for the Fr_GetPOCStatus API
* @param[in]        PSR0 - Value of the FLEXRAY_PSR0_ADDR16 register.
* @param[out]       pSlotMode -  Address the Slot mode is stored to.
* @return           ::Std_ReturnType
*                       - E_OK      Function call finished successfully
*                       - E_NOT_OK  Function call aborted due to hardware error
***************************************************************************************************/
static Std_ReturnType Fr_Driver_GetSlotMode(
    uint16 PSR0,
    Fr_SlotModeType *pSlotMode
)
{
    Std_ReturnType retVal = (Std_ReturnType)E_OK;

    switch (PSR0 & FLEXRAY_PSR0_SLOTMODE_MASK_U16) {
        case FLEXRAY_PSR0_SLOTMODE_SINGLE_U16: {
            *pSlotMode = FR_SLOTMODE_KEYSLOT;
            break;
        }

        case FLEXRAY_PSR0_SLOTMODE_ALL_PENDING_U16: {
            *pSlotMode = FR_SLOTMODE_ALL_PENDING;
            break;
        }

        case FLEXRAY_PSR0_SLOTMODE_ALL_U16: {
            *pSlotMode = FR_SLOTMODE_ALL;
            break;
        }

        default: {
            *pSlotMode = FR_SLOTMODE_SINGLE;
            retVal = (Std_ReturnType)E_NOT_OK;
            break;
        }
    }

    return retVal;
}

/***************************************************************************************************
* @function_name    Fr_Driver_GetErrorMode
*
* @brief            FrIP function for the Fr_GetPOCStatus API
* @param[in]        u16RegValuePSR0 - Value of the FLEXRAY_PSR0_ADDR16 register.
* @param[out]       pErrorMode - Address the Error mode is stored to.
* @return           ::Std_ReturnType
*                       - E_OK      Function call finished successfully
*                       - E_NOT_OK  Function call aborted due to hardware error
***************************************************************************************************/
static Std_ReturnType Fr_Driver_GetErrorMode
(
    uint16 u16RegValuePSR0,
    Fr_ErrorModeType *pErrorMode
)
{
    /* Initialize variable */
    Std_ReturnType retVal = (Std_ReturnType)(E_OK);

    /* Determine the error mode */
    switch (u16RegValuePSR0 & FLEXRAY_PSR0_ERRMODE_MASK_U16) {
        case FLEXRAY_PSR0_ERRMODE_ACTIVE_U16:
            /* Store the Error mode to location referenced by given pointer */
            *pErrorMode = FR_ERRORMODE_ACTIVE;
            break;

        case FLEXRAY_PSR0_ERRMODE_PASSIVE_U16:
            /* Store the Error mode to location referenced by given pointer */
            *pErrorMode = FR_ERRORMODE_PASSIVE;
            break;

        case FLEXRAY_PSR0_ERRMODE_COMM_HALT_U16:
            /* Store the Error mode to location referenced by given pointer */
            *pErrorMode = FR_ERRORMODE_COMM_HALT;
            break;

        default:
            /* Reserved value cannot occur */
            *pErrorMode = FR_ERRORMODE_ACTIVE;
            retVal = (Std_ReturnType)(E_NOT_OK);
            break;
    }

    return retVal;
}

/***************************************************************************************************
* @function_name    Fr_Driver_Calcu16HeaderCRC
*
* @brief            Header CRC calculating
* @note             Not intended for the startup and sync frames CRC calculation
* @param[in]        FrameId Frame ID
* @param[in]        PayloadLen Data payload length
* @return           ::uint16 - Calculated CRC Value
***************************************************************************************************/
static uint16 Fr_Driver_Calcu16HeaderCRC
(
    const uint16 FrameId,
    const uint16 PayloadLen
)
{
    uint16 tempCrc = 0U;
    uint16 nextBit = 0U;
    uint16 crcNextBit = 0U;
    sint16 s16fr_i = 0;

    /* Calculate crc over sync bit */
    /* Calculate crc over startup bit */
    tempCrc = 0x68U;

    /* Calculate crc over FrameId field in a network order */
    for (s16fr_i = 10; s16fr_i >= 0; s16fr_i--) {
        nextBit = (uint16)((FrameId >> (uint8)s16fr_i) & 0x0001U);
        crcNextBit = (uint16)(nextBit ^ ((tempCrc >> 10U) & 0x0001U));
        tempCrc = (uint16)(tempCrc << 1U);

        if (0U != crcNextBit) {
            tempCrc ^= 0x0385U;
        }
    }

    /* Calculate crc over u8PayloadLength field in a network order */
    for (s16fr_i = 6; s16fr_i >= 0; s16fr_i--) {
        nextBit = (uint16)((PayloadLen >> (uint8)s16fr_i) & 0x0001U);
        crcNextBit = (uint16)(nextBit ^ ((tempCrc >> 10U) & 0x0001U));
        tempCrc = (uint16)(tempCrc << 1U);

        if (0U != crcNextBit) {
            tempCrc ^= 0x0385U;
        }
    }

    return  (uint16)(tempCrc & 0x7FFU);
}

/***************************************************************************************************
* @function_name    Fr_Driver_ReadMB
*
* @brief            FrIP function in order to read the contents into massage buffer.
* @param[in]        pCtrlCfg  - Controller config
* @param[in]        pLsdu - Sdu pointer
* @param[in]        TempPayloadLen - Lenght of sdu
* @param[in]        TempMBIdx - MB index.
***************************************************************************************************/
static void Fr_Driver_ReadMB
(
    const Fr_CtrlCfgType *pCtrlCfg,
    uint8 *pLsdu,
    uint8 TempPayloadLen,
    uint16 TempMBIdx
)
{
    uint8 u8Fr_p = 0U;
    uint8 u32Index = 0U;
    const uint32 *pu32Data = NULL;

    pu32Data = Fr_Driver_GetData32Pointer((Fr_Driver_PointerSizeType)
                                          pCtrlCfg->BufferConfigSetPtr->BufferAddressTable[TempMBIdx].MbDataAddrPtr);
    arch_invalidate_cache_range_pri(pu32Data,
                                    (uint32)((uint32)TempPayloadLen << 1U));

    for (u32Index = 0U; u32Index < (TempPayloadLen >> 1U); u32Index++) {
        pLsdu[u8Fr_p] = (uint8)(pu32Data[u32Index]);
        pLsdu[u8Fr_p + (uint8)1] = (uint8)(pu32Data[u32Index] >> 8);
        pLsdu[u8Fr_p + (uint8)2] = (uint8)(pu32Data[u32Index] >> 16);
        pLsdu[u8Fr_p + (uint8)3] = (uint8)(pu32Data[u32Index] >> 24);
        u8Fr_p += (uint8)4;
    }

    arch_clean_cache_range_pri(pLsdu, (uint32)((uint32)TempPayloadLen << 1U));
}

#if (FR_PREPARE_LPDU_SUPPORT == STD_ON)

/***************************************************************************************************
* @function_name    Fr_Driver_PrepareTxBuffer
*
* @brief            Fr function for the Fr_Flexray_PrepareLPdu function.
* @description      Prepare transmission buffer.
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[in]        LPduIdx - Index of LPdu to be prepared.
* @param[in]        TempMBNum - Temporary variable for the MB index.
***************************************************************************************************/
static Std_ReturnType Fr_Driver_PrepareTxBuffer
(
    const Fr_CtrlCfgType *pCtrlCfg,
    uint16 LPduIdx,
    uint16 TempMBNum
)
{
    uint16 MbIdx = 0U;
    uint16 MbRegOffset = 0U;
    Std_ReturnType ret = (Std_ReturnType)(E_NOT_OK);
    uint16 TempVal = 0U;
    uint32 TempHeaderVal = 0U;
    uint32 *pHeaderMB = NULL;
    const Fr_CCTxBufferConfigType *pTxCfg = NULL;

    /* Temporary offset address of MB configuration registers */
    MbRegOffset = TempMBNum * FLEXRAY_MB_REG_OFFSET;
    /* Get the index from MBIDXn */
    MbIdx = Fr_Driver_ReadRegister(pCtrlCfg, FLEXRAY_MBIDXR0_ADDR16 + MbRegOffset);
    /* Store the reference to transmit buffer configuration */
    pTxCfg = (const Fr_CCTxBufferConfigType *)(
                 pCtrlCfg->BufferConfigSetPtr->LPduInfoPtr[LPduIdx].BufferConfigPtr);

    /* MBIDXn must not be written because it could be changed by
    Rx shadow buffer index swap */
    MbIdx = Fr_Driver_ReadRegister(pCtrlCfg, FLEXRAY_MBIDXR0_ADDR16 + MbRegOffset);

    /* Check whether the index value is in expected range */
    if (MbIdx <= pCtrlCfg->BufferConfigSetPtr->RSBIR_B2BufferIndexInit) {
        /* Disable appropriate MB */
        if (FLEXRAY_MBCCSR_EDS_U16 == (Fr_Driver_ReadRegister(pCtrlCfg,
                                       FLEXRAY_MBCCSR0_ADDR16 + MbRegOffset) & FLEXRAY_MBCCSR_EDS_U16)) {
            Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_MBCCSR0_ADDR16 + MbRegOffset,
                                    FLEXRAY_MBCCSR_EDT_U16);
        }

        /* Check if buffer has been successfully disabled */
        if (FLEXRAY_MBCCSR_EDS_U16 != (Fr_Driver_ReadRegister(pCtrlCfg,
                                       FLEXRAY_MBCCSR0_ADDR16 + MbRegOffset) & FLEXRAY_MBCCSR_EDS_U16)) {
            /* Appropriate message buffer is disabled and can be reconfigured */

            /* Fr_CtrlIdx: FR_MBCCSRn: MTD */
            TempVal = FLEXRAY_MBCCSR_MTD_U16;
            TempVal |= FLEXRAY_MBCCSR_MBIF_U16;

            /* Store configuration into the FR_MBCCSRn register */
            /* Fr_CtrlIdx: FR_MBCCSRn */
            Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_MBCCSR0_ADDR16 + MbRegOffset,
                                    TempVal);

            /* Configure FR_MBCCFRn register */
            TempVal = 0U;

            if ((boolean)TRUE == pTxCfg->TxChannelAEnable) {
                TempVal |= FLEXRAY_MBCCFR_CHA_U16;    /* Channel assignment - ch. A */
            }

            if ((boolean)TRUE == pTxCfg->TxChannelBEnable) {
                TempVal |= FLEXRAY_MBCCFR_CHB_U16;    /* Channel assignment - ch. B */
            }

            /* Assign Message Buffer in the dynamic segment to one channel only.
            This is also as per the specification as it does'nt allow to assign a message buffer
            to both the channels in a dynamic segment. */
            if (((pTxCfg->TxFrameID) > pCtrlCfg->LowLevelConfigSetPtr->gNumberOfStaticSlots)
                    && (((boolean)TRUE == (pTxCfg->TxChannelAEnable))
                        && ((boolean)TRUE == (pTxCfg->TxChannelBEnable)))) {
                TempVal = FLEXRAY_MBCCFR_CHA_U16;
            }

            /* Cycle counter filter settings */
            /* Should the cycle counter filter be enabled? */
            if ((boolean)TRUE == pTxCfg->TxCycleCounterFilterEnable) {
                /* Cycle counter filter value */
                TempVal |= (uint16)(pTxCfg->TxCycleCounterFilterValue) &
                           FLEXRAY_MBCCFR_CCFVAL_MASK_U16;
                /* Cycle counter filter mask */
                TempVal |= (uint16)((uint16)(pTxCfg->TxCycleCounterFilterMask) << 6U) &
                           FLEXRAY_MBCCFR_CCFMSK_MASK_U16;
                /* Cycle counter filter enabled */
                TempVal |= FLEXRAY_MBCCFR_CCFE_U16;
            }

            /* Store configuration into FR_MBCCFRn register */
            /* Fr_CtrlIdx: FR_MBCCFRn */
            Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_MBCCFR0_ADDR16 + MbRegOffset,
                                    TempVal);

            /* Configure FR_MBFIDRn register */
            /* Fr_CtrlIdx: FR_MBFIDRn */
            Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_MBFIDR0_ADDR16 + MbRegOffset,
                                    (uint16)(pTxCfg->TxFrameID & FLEXRAY_MBFIDR_FID_MASK_U16));

            /* Configure data field offset */
            /* According to a FlexRay Memory Layout described in FlexRay module documentation */

            /* Calculate the message buffer header */
            if ((MbIdx % 2U) == 0U) {
                pHeaderMB = Fr_Driver_GetData32Pointer((Fr_Driver_PointerSizeType)
                                                       pCtrlCfg->CCHardwareConfigPtr->CCFlexRayMemoryBaseAddress
                                                       + ((Fr_Driver_PointerSizeType)MbIdx * FLEXRAY_HEADER_SIZE_U8));
            }
            else {
                pHeaderMB = Fr_Driver_GetData32Pointer((Fr_Driver_PointerSizeType)
                                                       pCtrlCfg->CCHardwareConfigPtr->CCFlexRayMemoryBaseAddress
                                                       + (((Fr_Driver_PointerSizeType)MbIdx * FLEXRAY_HEADER_SIZE_U8 - 2)));
            }

            arch_invalidate_cache_range_pri(pHeaderMB, FLEXRAY_HEADER_CACHE_SIZE_U8);

            if ((MbIdx % 2U) == 0U) {
                /* Configure Frame Header registers */
                /* Fr_CtrlIdx: Memory: FrameHeader: FID */
                TempHeaderVal = (uint32)((pTxCfg->TxFrameID & 0x07FFU) <<
                                         16U);    /* Frame ID */

                /* Fr_CtrlIdx: Memory: FrameHeader: PPI */
                /* Is the payload preamble indicator configured? */
                if ((boolean)TRUE == pTxCfg->PayloadPreamble) {
                    TempHeaderVal |=
                        FLEXRAY_FRAMEHEADER0_PPI_MASK_U32;  /* Configure Payload preamble indicator */
                }

                /* Determines whether the MB segment is static or dynamic */
                if ((pTxCfg->TxFrameID) >
                        pCtrlCfg->LowLevelConfigSetPtr->gNumberOfStaticSlots) {
                    /* Use configured payload length in dynamic segment */
                    TempHeaderVal |= ((uint16)(pTxCfg->TxPayloadLength) & 0x7FU);
                }
                else {
                    /* Use gPayloadLengthStatic for all frames in static segment */
                    TempHeaderVal |= ((uint16)(pCtrlCfg->LowLevelConfigSetPtr->gPayloadLengthStatic)
                                      & 0x7FU);
                }

                pHeaderMB[0U] = TempHeaderVal;
                /* Fr_CtrlIdx: Memory: FrameHeader2 */
                TempHeaderVal = 0U;
                TempHeaderVal |= ((pTxCfg->HeaderCRC & 0x07FFU) << 16U);       /* Header CRC */

                pHeaderMB[1U] &= 0xF800FFFFU;    /*clear HDCRC*/
                pHeaderMB[1U] |= TempHeaderVal;
            }
            else {
                TempHeaderVal = (uint32)((pTxCfg->TxFrameID & 0x07FFU));    /* Frame ID */

                if ((boolean)TRUE == pTxCfg->PayloadPreamble) {
                    TempHeaderVal |=
                        FLEXRAY_FRAMEHEADER0_PPI_MASK_U16;  /* Configure Payload preamble indicator */
                }

                pHeaderMB[0U] &= 0xFFFF0000U;
                pHeaderMB[0U] |= TempHeaderVal;
                TempHeaderVal = 0U;
                TempHeaderVal |= (pTxCfg->HeaderCRC & 0x07FFU);       /* Header CRC */

                pHeaderMB[1U] &= 0xFFFFF800U;    /*clear HDCRC*/
                pHeaderMB[1U] |= TempHeaderVal;
            }

            arch_clean_cache_range_pri(pHeaderMB, FLEXRAY_HEADER_CACHE_SIZE_U8);



            /* Enable message buffer */
            SchM_Enter_EXCLUSIVE_AREA();
            TempVal = (uint16)(Fr_Driver_ReadRegister(pCtrlCfg,
                               FLEXRAY_MBCCSR0_ADDR16 + MbRegOffset) | FLEXRAY_MBCCSR_EDT_U16);

            /* Enable the MB in the FR_MBCCSRn register */
            /* Fr_CtrlIdx: FR_MBCCSRn */
            Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_MBCCSR0_ADDR16 + MbRegOffset,
                                    TempVal);
            SchM_Exit_EXCLUSIVE_AREA();

            /* Check if buffer has been successfully enabled */
            if (FLEXRAY_MBCCSR_EDS_U16 == ((Fr_Driver_ReadRegister(pCtrlCfg,
                                            FLEXRAY_MBCCSR0_ADDR16 + MbRegOffset) & FLEXRAY_MBCCSR_EDS_U16))) {
                ret = (Std_ReturnType)(E_OK);
            }
        }
    }

    return ret;
}

/***************************************************************************************************
* @function_name    Fr_Driver_PrepareRxBuffer
*
* @brief            Fr function for the Fr_Flexray_PrepareLPdu function.
* @description      Prepare transmission buffer.
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[in]        LPduIdx - Index of LPdu to be prepared.
* @param[in]        TempMBNum - Temporary variable for the MB index.
***************************************************************************************************/
static Std_ReturnType Fr_Driver_PrepareRxBuffer
(
    const Fr_CtrlCfgType *pCtrlCfg,
    uint16 LPduIdx,
    uint16 TempMBNum
)
{
    uint16 MbIdx = 0U;
    uint16 MbRegOffset = 0U;
    uint16 TmpVal = 0U;

    const Fr_CCRxBufferConfigType *pRxCfg = NULL;
    Std_ReturnType ret = (Std_ReturnType)(E_NOT_OK);

    /* pRxCfg points to configuration referenced by Fr_BufConfSetIdx */
    pRxCfg = (const Fr_CCRxBufferConfigType *)(
                 pCtrlCfg->BufferConfigSetPtr->LPduInfoPtr[LPduIdx].BufferConfigPtr);
    /* Temporary offset address of MB configuration registers */
    MbRegOffset = TempMBNum * FLEXRAY_MB_REG_OFFSET;
    /* Get the index from MBIDXn */
    MbIdx = Fr_Driver_ReadRegister(pCtrlCfg, FLEXRAY_MBIDXR0_ADDR16 + MbRegOffset);

    /* Check whether the index value is in expected range */
    if (MbIdx <= pCtrlCfg->BufferConfigSetPtr->RSBIR_B2BufferIndexInit) {
        /* Disable appropriate MB */
        if (FLEXRAY_MBCCSR_EDS_U16 == (Fr_Driver_ReadRegister(pCtrlCfg,
                                       FLEXRAY_MBCCSR0_ADDR16 + MbRegOffset) & FLEXRAY_MBCCSR_EDS_U16)) {
            Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_MBCCSR0_ADDR16 + MbRegOffset,
                                    FLEXRAY_MBCCSR_EDT_U16);
        }

        /* Check if buffer has been successfully disabled */
        if (FLEXRAY_MBCCSR_EDS_U16 != (Fr_Driver_ReadRegister(pCtrlCfg,
                                       FLEXRAY_MBCCSR0_ADDR16 + MbRegOffset) & FLEXRAY_MBCCSR_EDS_U16)) {
            /* Appropriate message buffer is disabled and can be configured */
            TmpVal = 0U;
            /* Store configuration into the FR_MBCCSRn register */
            Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_MBCCSR0_ADDR16 + MbRegOffset, TmpVal);

            /* Configure FR_MBCCFRn register */
            TmpVal = 0U;

            if ((boolean)TRUE == pRxCfg->RxChannelAEnable) {
                TmpVal |= FLEXRAY_MBCCFR_CHA_U16;    /* Channel assignment - ch. A */
            }

            if ((boolean)TRUE == pRxCfg->RxChannelBEnable) {
                TmpVal |= FLEXRAY_MBCCFR_CHB_U16;    /* Channel assignment - ch. B */
            }

            /* Cycle counter filter settings */
            /* Should the cycle counter filter be enabled? */
            if ((boolean)TRUE == pRxCfg->RxCycleCounterFilterEnable) {
                /* Cycle counter filter value */
                TmpVal |= (uint16)(pRxCfg->RxCycleCounterFilterValue) &
                          FLEXRAY_MBCCFR_CCFVAL_MASK_U16;
                /* Cycle counter filter mask */
                TmpVal |= (uint16)((uint16)(pRxCfg->RxCycleCounterFilterMask) << 6U) &
                          FLEXRAY_MBCCFR_CCFMSK_MASK_U16;
                /* Cycle counter filter enabled */
                TmpVal |= FLEXRAY_MBCCFR_CCFE_U16;
            }

            /* Store configuration into FR_MBCCFRn register */
            /* Fr_CtrlIdx: FR_MBCCFRn */
            Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_MBCCFR0_ADDR16 + MbRegOffset, TmpVal);

            /* Configure FR_MBFIDRn register */
            /* Fr_CtrlIdx: FR_MBFIDRn */
            Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_MBFIDR0_ADDR16 + MbRegOffset,
                                    (uint16)(pRxCfg->RxFrameID & FLEXRAY_MBFIDR_FID_MASK_U16));

            /* Calculate the message buffer header */


            /* Enable message buffer */
            SchM_Enter_EXCLUSIVE_AREA();
            TmpVal = (uint16)(Fr_Driver_ReadRegister(pCtrlCfg,
                              FLEXRAY_MBCCSR0_ADDR16 + MbRegOffset) | FLEXRAY_MBCCSR_EDT_U16);
            /* Enable the MB in the FR_MBCCSRn register */
            Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_MBCCSR0_ADDR16 + MbRegOffset, TmpVal);

            if (FLEXRAY_MBCCSR_EDS_U16 == (Fr_Driver_ReadRegister(pCtrlCfg,
                                           FLEXRAY_MBCCSR0_ADDR16 + MbRegOffset) & FLEXRAY_MBCCSR_EDS_U16)) {
                /* Clear Buffer Interrupt Flag which is set after MB enabling */
                /* Select only necessary bits */
                TmpVal = (uint16)(Fr_Driver_ReadRegister(pCtrlCfg,
                                  FLEXRAY_MBCCSR0_ADDR16 + MbRegOffset)
                                  & FLEXRAY_MBCCSR0_CONFIG_MASK_U16);
                TmpVal |= FLEXRAY_MBCCSR_MBIF_U16;      /* Set MBIF flag - clear Rx MB interrupt flag */
                /* Fr_CtrlIdx: FR_MBCCSR */
                /* Clear Transmit Buffer Interrupt Flag */
                Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_MBCCSR0_ADDR16 + MbRegOffset, TmpVal);

                ret = (Std_ReturnType)(E_OK);
            }

            SchM_Exit_EXCLUSIVE_AREA();
        }
    }

    return ret;
}
#endif /* FR_PREPARE_LPDU_SUPPORT == STD_ON */

/***************************************************************************************************
* @function_name    Fr_Driver_GetPOCStatus
*
* @brief            FrIP function for quering the controller POC status
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[out]       pPocState - Address of the variable where the Protocol status is stored to
* @return           ::Std_ReturnType
*                       - E_OK      Function call finished successfully
*                       - E_NOT_OK  Function call aborted due to hardware errors
***************************************************************************************************/
Std_ReturnType Fr_Driver_GetPOCStatus(
    const Fr_CtrlCfgType *pCtrlCfg,
    Fr_POCStatusType *pPocState
)
{
    Std_ReturnType retVal = (Std_ReturnType)E_OK;

    uint16 PSR0 = Fr_Driver_ReadRegister(pCtrlCfg, FLEXRAY_PSR0_ADDR16);
    uint16 PSR1 = Fr_Driver_ReadRegister(pCtrlCfg, FLEXRAY_PSR1_ADDR16);

    if ((PSR1 & FLEXRAY_PSR1_FRZ_U16) == FLEXRAY_PSR1_FRZ_U16) {
        pPocState->State = FR_POCSTATE_HALT;
        pPocState->Freeze = (boolean)TRUE;
    }
    else {
        /*PRQA S 2992,2996 2*/
        if (Fr_Driver_GetProtocolState(PSR0, &pPocState->State) == E_NOT_OK) {
            retVal = (Std_ReturnType)E_NOT_OK;
        }

        pPocState->Freeze = (boolean)FALSE;
    }

    if (pPocState->State == FR_POCSTATE_STARTUP) {
        if (Fr_Driver_GetStartupState(PSR0,
                                      &pPocState->StartupState) == (Std_ReturnType)E_NOT_OK) {
            retVal = (Std_ReturnType)E_NOT_OK;
        }
    }
    else {
        pPocState->StartupState = FR_STARTUP_UNDEFINED;
    }

    if (Fr_Driver_GetWakeupState(PSR0,
                                 &pPocState->WakeupState) == (Std_ReturnType)E_NOT_OK) {
        retVal = (Std_ReturnType)E_NOT_OK;
    }

    if (Fr_Driver_GetSlotMode(PSR0,
                              &pPocState->SlotMode) == (Std_ReturnType)E_NOT_OK) {
        retVal = (Std_ReturnType)E_NOT_OK;
    }

    if (Fr_Driver_GetErrorMode(PSR0,
                               &pPocState->ErrorMode) == (Std_ReturnType)E_NOT_OK) {
        retVal = (Std_ReturnType)E_NOT_OK;
    }

    if ((PSR1 & FLEXRAY_PSR1_HHR_U16) == FLEXRAY_PSR1_HHR_U16) {
        pPocState->CHIHaltRequest = (boolean)TRUE;
    }
    else {
        pPocState->CHIHaltRequest = (boolean)FALSE;
    }

    if ((PSR1 & FLEXRAY_PSR1_CPN_U16) == FLEXRAY_PSR1_CPN_U16) {
        pPocState->ColdstartNoise = (boolean)TRUE;
    }
    else {
        pPocState->ColdstartNoise = (boolean)FALSE;
    }

    return retVal;
}

/***************************************************************************************************
* @function_name    Fr_Driver_GetPOCState
* @brief            FrIP function for quering the controller POC status
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @return           none
***************************************************************************************************/
uint16 Fr_Driver_GetPOCState(
    const Fr_CtrlCfgType *pCtrlCfg
)
{
    uint16 PSR0 = Fr_Driver_ReadRegister(pCtrlCfg, FLEXRAY_PSR0_ADDR16);
    uint16 PSR1 = Fr_Driver_ReadRegister(pCtrlCfg, FLEXRAY_PSR1_ADDR16);

    if ((PSR1 & FLEXRAY_PSR1_FRZ_U16) == FLEXRAY_PSR1_FRZ_U16) {
        return FLEXRAY_PSR0_PROTSTATE_HALT_U16;
    }
    else {
        return PSR0 & FLEXRAY_PSR0_PROTSTATE_MASK_U16;
    }
}

/***************************************************************************************************
* @function_name    Fr_Driver_WaitForCmdProcess
*
* @brief            FrIP function for waiting until CC process current command or timeout expires
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @return           ::Std_ReturnType
*                       - E_OK      Function call finished successfully:
*                       - E_NOT_OK  Waiting loop timeout reached before CHI processed previous
*                                   command.
***************************************************************************************************/
Std_ReturnType Fr_Driver_WaitForCmdProcess(
    const Fr_CtrlCfgType *pCtrlCfg
)
{
    Std_ReturnType retVal = (Std_ReturnType)E_NOT_OK;
    uint16 u16Timeout = FR_MAX_WAITCYCLES_U8;

    while (u16Timeout > 0U) {
        if ((Fr_Driver_ReadRegister(pCtrlCfg,
                                    FLEXRAY_POCR_ADDR16) & FLEXRAY_POCR_BSY_U16) != FLEXRAY_POCR_BSY_U16) {
            retVal = (Std_ReturnType)E_OK;
            break;
        }

        u16Timeout--;
    }

    return retVal;
}

/***************************************************************************************************
* @function_name    Fr_Driver_InvokeCHICommand
*
* @brief            FrIP function for invoking POCCMD commands
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[in]        CHICommand - required CHI command
* @return           ::Std_ReturnType:
*                       - E_OK      CHI command was accepted by PE
*                       - E_NOT_OK  CHI command was not accepted by PE due to BSY flag
***************************************************************************************************/
Std_ReturnType Fr_Driver_InvokeCHICommand(
    const Fr_CtrlCfgType *pCtrlCfg,
    uint16 CHICommand
)
{
    Std_ReturnType retVal = (Std_ReturnType)E_NOT_OK;

    if (Fr_Driver_WaitForCmdProcess(pCtrlCfg) == (Std_ReturnType)E_OK) {
        Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_POCR_ADDR16,
                                (uint16)(CHICommand | FLEXRAY_POCR_WME_U16));
        retVal = (Std_ReturnType)E_OK;
    }

    return retVal;
}

/***************************************************************************************************
* @function_name    Fr_Flexray_WaitForPOCState
*
* @brief            Wait for selected POC state
* @param[in]        ctrlIdx - Index of FlexRay CC
* @param[in]        u16PocState indicates which POC state should be reached
* @return           ::Std_ReturnType
*                       - E_OK      API call has been successful
*                       - E_NOT_OK  API call aborted due to errors
***************************************************************************************************/
Std_ReturnType Fr_Driver_WaitForPOCState(
    const Fr_CtrlCfgType *pCtrlCfg,
    const uint16 u16POCState
)
{
    Std_ReturnType retVal = (Std_ReturnType)E_NOT_OK;
    uint16 u16Timeout = FR_MAX_WAITCYCLES_U8;
    uint16 u16StatusVal = 0U;

    while (u16Timeout > 0U) {
        u16StatusVal = Fr_Driver_GetPOCState(pCtrlCfg);

        if (u16StatusVal == u16POCState) {
            retVal = (Std_ReturnType)E_OK;
            break;
        }

        u16Timeout--;

    }

    return retVal;
}

/***************************************************************************************************
* @function_name    Fr_Driver_CCInit
*
* @brief            FrIP function for FlexRay CC configuration
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @return           none
***************************************************************************************************/
Std_ReturnType Fr_Driver_CCInit(
    const Fr_CtrlCfgType *pCtrlCfg
)
{
    Std_ReturnType retVal = (Std_ReturnType)E_NOT_OK;
    uint16 RegVal = 0U;

    if ((Fr_Driver_ReadRegister(pCtrlCfg,
                                FLEXRAY_MCR_ADDR16) & FLEXRAY_MCR_MEN_U16) == FLEXRAY_MCR_MEN_U16) {
        if (Fr_Driver_InvokeCHICommand(pCtrlCfg,
                                       FLEXRAY_POCR_CMD_FREEZE_U16) == (Std_ReturnType)E_OK) {
            if (Fr_Driver_WaitForPOCState(pCtrlCfg,
                                          FLEXRAY_PSR0_PROTSTATE_HALT_U16) == E_NOT_OK) {
                if (Fr_Driver_InvokeCHICommand(pCtrlCfg,
                                               FLEXRAY_POCR_CMD_DEFAULTCONFIG_U16) == (Std_ReturnType)E_OK) {
                    if (Fr_Driver_WaitForPOCState(pCtrlCfg,
                                                  FLEXRAY_PSR0_PROTSTATE_DEFAULT_CONFIG_U16) == E_NOT_OK) {
                        Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_MCR_ADDR16, 0U);
                    }
                }
            }
        }
    }

    /* Check if FR CC is disable */
    if ((Fr_Driver_ReadRegister(pCtrlCfg,
                                FLEXRAY_MCR_ADDR16) & FLEXRAY_MCR_MEN_U16) == 0U) {
        if (pCtrlCfg->CCHardwareConfigPtr->SingleChannelModeEnabled == (boolean)TRUE) {
            RegVal |= FLEXRAY_MCR_SCM_U16;
        }

        switch (pCtrlCfg->CCHardwareConfigPtr->Channels) {
            case FR_CHANNEL_A: {
                RegVal |= FLEXRAY_MCR_CHA_U16;
                break;
            }

            case FR_CHANNEL_B: {
                RegVal |= FLEXRAY_MCR_CHB_U16;
                break;
            }

            case FR_CHANNEL_AB: {
                RegVal |= (uint16)(FLEXRAY_MCR_CHA_U16 | FLEXRAY_MCR_CHB_U16);
                break;
            }

            default: {
                break;
            }
        }

        RegVal |= (((uint16)pCtrlCfg->CCHardwareConfigPtr->Bitrate) << 1) &
                  FLEXRAY_MCR_BITRATE_MASK_U16;

        Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_MCR_ADDR16, RegVal);

        Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_SYMATOR_ADDR16,
                                (uint16)pCtrlCfg->CCHardwareConfigPtr->Timeout);

        Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_SYMBADHR_ADDR16,
                                (uint16)((uint32)(pCtrlCfg->CCHardwareConfigPtr->CCFlexRayMemoryBaseAddress)
                                         >> 16U));
        Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_SYMBADLR_ADDR16,
                                (uint16)((uint32)(pCtrlCfg->CCHardwareConfigPtr->CCFlexRayMemoryBaseAddress)
                                         &
                                         FLEXRAY_SYMBADLR_SMBA_MASK_U16));

        RegVal |= FLEXRAY_MCR_MEN_U16;

        Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_MCR_ADDR16, RegVal);

        retVal = (Std_ReturnType)E_OK;
    }

    return retVal;
}

/***************************************************************************************************
* @function_name    Fr_Driver_EnterPOCConfigState
*
* @brief            FrIP function for entering the POC:Config state
* @param[in]        ctrlIdx - Index of FlexRay CC
* @return           ::Std_ReturnType
*                       - E_OK      API call has been successful
*                       - E_NOT_OK  Error occurred during transition into POC:Config
***************************************************************************************************/
Std_ReturnType Fr_Driver_EnterPOCConfigState(
    const Fr_CtrlCfgType *pCtrlCfg
)
{
    Std_ReturnType retVal = (Std_ReturnType)E_OK;
    uint16 u16Timeout = FR_MAX_WAITCYCLES_U8;
    uint16 u16POCState = 0U;
    u16POCState = Fr_Driver_GetPOCState(pCtrlCfg);


    if ((u16POCState != FLEXRAY_PSR0_PROTSTATE_READY_U16)
            && (u16POCState != FLEXRAY_PSR0_PROTSTATE_DEFAULT_CONFIG_U16)) {
        retVal = (Std_ReturnType)E_NOT_OK;

        if (Fr_Driver_InvokeCHICommand(pCtrlCfg,
                                       FLEXRAY_POCR_CMD_FREEZE_U16) == (Std_ReturnType)E_OK) {
            if (Fr_Driver_WaitForPOCState(pCtrlCfg,
                                          FLEXRAY_PSR0_PROTSTATE_HALT_U16) == (Std_ReturnType)E_OK) {
                while (u16Timeout > 0U) {
                    if (Fr_Driver_InvokeCHICommand(pCtrlCfg,
                                                   FLEXRAY_POCR_CMD_DEFAULTCONFIG_U16) == E_OK) {/*do nothing*/}

                    if (Fr_Driver_GetPOCState(pCtrlCfg) ==
                            FLEXRAY_PSR0_PROTSTATE_DEFAULT_CONFIG_U16) {
                        retVal = (Std_ReturnType)E_OK;
                        break;
                    }

                    u16Timeout--;
                }
            }
        }
    }

    if (retVal == (Std_ReturnType)E_OK) {
        if (Fr_Driver_InvokeCHICommand(pCtrlCfg,
                                       FLEXRAY_POCR_CMD_CONFIG_U16) == (Std_ReturnType)E_OK) {
            if (Fr_Driver_WaitForPOCState(pCtrlCfg,
                                          FLEXRAY_PSR0_PROTSTATE_CONFIG_U16) == (Std_ReturnType)E_OK) {
                retVal = (Std_ReturnType)E_OK;
            }
            else {
                retVal = (Std_ReturnType)E_NOT_OK;
            }
        }
        else {
            retVal = (Std_ReturnType)E_NOT_OK;
        }
    }

    return retVal;
}

/***************************************************************************************************
* @function_name    Fr_Driver_ClusterNodeParamCfg
*
* @brief            FrIP function for cluster and node parameters configuration
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @return           none
***************************************************************************************************/
void Fr_Driver_ClusterNodeParamCfg(
    const Fr_CtrlCfgType *pCtrlCfg
)
{

    Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_PCR0_ADDR16,
                            pCtrlCfg->LowLevelConfigSetPtr->RegPCR0);
    Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_PCR1_ADDR16,
                            pCtrlCfg->LowLevelConfigSetPtr->RegPCR1);
    Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_PCR2_ADDR16,
                            pCtrlCfg->LowLevelConfigSetPtr->RegPCR2);
    Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_PCR3_ADDR16,
                            pCtrlCfg->LowLevelConfigSetPtr->RegPCR3);
    Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_PCR4_ADDR16,
                            pCtrlCfg->LowLevelConfigSetPtr->RegPCR4);
    Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_PCR5_ADDR16,
                            pCtrlCfg->LowLevelConfigSetPtr->RegPCR5);
    Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_PCR6_ADDR16,
                            pCtrlCfg->LowLevelConfigSetPtr->RegPCR6);
    Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_PCR7_ADDR16,
                            pCtrlCfg->LowLevelConfigSetPtr->RegPCR7);
    Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_PCR8_ADDR16,
                            pCtrlCfg->LowLevelConfigSetPtr->RegPCR8);
    Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_PCR9_ADDR16,
                            pCtrlCfg->LowLevelConfigSetPtr->RegPCR9);
    Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_PCR10_ADDR16,
                            pCtrlCfg->LowLevelConfigSetPtr->RegPCR10);
    Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_PCR11_ADDR16,
                            pCtrlCfg->LowLevelConfigSetPtr->RegPCR11);
    Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_PCR12_ADDR16,
                            pCtrlCfg->LowLevelConfigSetPtr->RegPCR12);
    Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_PCR13_ADDR16,
                            pCtrlCfg->LowLevelConfigSetPtr->RegPCR13);
    Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_PCR14_ADDR16,
                            pCtrlCfg->LowLevelConfigSetPtr->RegPCR14);
    Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_PCR15_ADDR16,
                            pCtrlCfg->LowLevelConfigSetPtr->RegPCR15);
    Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_PCR16_ADDR16,
                            pCtrlCfg->LowLevelConfigSetPtr->RegPCR16);
    Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_PCR17_ADDR16,
                            pCtrlCfg->LowLevelConfigSetPtr->RegPCR17);
    Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_PCR18_ADDR16,
                            pCtrlCfg->LowLevelConfigSetPtr->RegPCR18);
    Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_PCR19_ADDR16,
                            pCtrlCfg->LowLevelConfigSetPtr->RegPCR19);
    Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_PCR20_ADDR16,
                            pCtrlCfg->LowLevelConfigSetPtr->RegPCR20);
    Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_PCR21_ADDR16,
                            pCtrlCfg->LowLevelConfigSetPtr->RegPCR21);
    Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_PCR22_ADDR16,
                            pCtrlCfg->LowLevelConfigSetPtr->RegPCR22);
    Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_PCR23_ADDR16,
                            pCtrlCfg->LowLevelConfigSetPtr->RegPCR23);
    Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_PCR24_ADDR16,
                            pCtrlCfg->LowLevelConfigSetPtr->RegPCR24);
    Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_PCR25_ADDR16,
                            pCtrlCfg->LowLevelConfigSetPtr->RegPCR25);
    Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_PCR26_ADDR16,
                            pCtrlCfg->LowLevelConfigSetPtr->RegPCR26);
    Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_PCR27_ADDR16,
                            pCtrlCfg->LowLevelConfigSetPtr->RegPCR27);
    Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_PCR28_ADDR16,
                            pCtrlCfg->LowLevelConfigSetPtr->RegPCR28);
    Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_PCR29_ADDR16,
                            pCtrlCfg->LowLevelConfigSetPtr->RegPCR29);
    Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_PCR30_ADDR16,
                            pCtrlCfg->LowLevelConfigSetPtr->RegPCR30);

    Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_NMVLR_ADDR16, (uint16)
                            pCtrlCfg->LowLevelConfigSetPtr->gNetworkManagementVectorLength);

    Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_SFTOR_ADDR16,
                            pCtrlCfg->CCHardwareConfigPtr->SyncFrameTableOffset);

    Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_SFTCCSR_ADDR16,
                            FLEXRAY_SFTCCSR_SIDEN_U16);

}

/***************************************************************************************************
* @function_name    Fr_Driver_TxBufferCfg
*
* @brief            FrIP function for transmit message buffer configuration
* @details          This function will configure selected message buffer as a transmit
*                   message buffer
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[in]        pTxLPduInfo Pointer to the structure with information for configuring MB
* @return           ::Std_ReturnType
*                       - E_OK      Message buffer was successfully configured
*                       - E_NOT_OK  Error occured within message buffer enabling/disabling
***************************************************************************************************/
static Std_ReturnType Fr_Driver_TxBufferCfg
(
    const Fr_CtrlCfgType *pCtrlCfg,
    const Fr_CCLpduInfoType *pTxLPduInfo
)
{
    Std_ReturnType retVal = (Std_ReturnType)E_OK;
    uint16 u16RegVal = 0U;

    volatile uint16 u16MbRegOffset = (((uint16)
                                       pTxLPduInfo->MessageBufferNumber) * FLEXRAY_MB_REG_OFFSET);
    const Fr_CCTxBufferConfigType *pTxCfg = NULL;
    uint32 *pHeaderMB = NULL;

    pTxCfg = (const Fr_CCTxBufferConfigType *)(pTxLPduInfo->BufferConfigPtr);

    if ((Fr_Driver_ReadRegister(pCtrlCfg,
                                FLEXRAY_MBCCSR0_ADDR16 + u16MbRegOffset) & FLEXRAY_MBCCSR_EDS_U16) ==
            FLEXRAY_MBCCSR_EDS_U16) {
        Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_MBCCSR0_ADDR16 + u16MbRegOffset,
                                FLEXRAY_MBCCSR_EDT_U16);
    }

    if ((Fr_Driver_ReadRegister(pCtrlCfg,
                                FLEXRAY_MBCCSR0_ADDR16 + u16MbRegOffset) & FLEXRAY_MBCCSR_EDS_U16) ==
            FLEXRAY_MBCCSR_EDS_U16) {
        retVal = (Std_ReturnType)(E_NOT_OK);
    }
    else {
        /* Set FR_MBCCSRn: MTD -> Configure MB as Transmit */
        u16RegVal = FLEXRAY_MBCCSR_MTD_U16 | FLEXRAY_MBCCSR_MBIE_U16;
        Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_MBCCSR0_ADDR16 + u16MbRegOffset,
                                u16RegVal);

        u16RegVal = 0;

        /* Transmit on channel A enabled? */
        if ((boolean)TRUE == pTxCfg->TxChannelAEnable) {
            u16RegVal |= FLEXRAY_MBCCFR_CHA_U16;
        }

        /* Transmit on channel B enabled? */
        if ((boolean)TRUE == pTxCfg->TxChannelBEnable) {
            u16RegVal |= FLEXRAY_MBCCFR_CHB_U16;
        }

        if ((pTxCfg->TxFrameID) >
                pCtrlCfg->LowLevelConfigSetPtr->gNumberOfStaticSlots) {
            if (((boolean)TRUE == (pTxCfg->TxChannelAEnable))
                    && ((boolean)TRUE == (pTxCfg->TxChannelBEnable))) {
                u16RegVal = FLEXRAY_MBCCFR_CHA_U16;
            }
        }

        /* Cycle counter filter settings */
        /* Should the cycle counter filter be enabled? */
        if ((boolean)TRUE == pTxCfg->TxCycleCounterFilterEnable) {
            /* Store cycle counter filter value */
            u16RegVal |= (uint16)(pTxCfg->TxCycleCounterFilterValue) &
                         FLEXRAY_MBCCFR_CCFVAL_MASK_U16;
            /* Store cycle counter filter mask */
            u16RegVal |= (uint16)((uint16)(pTxCfg->TxCycleCounterFilterMask) << 6U) &
                         FLEXRAY_MBCCFR_CCFMSK_MASK_U16;
            /* Enable cycle counter filter for this message buffer*/
            u16RegVal |= FLEXRAY_MBCCFR_CCFE_U16;
        }

        Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_MBCCFR0_ADDR16 + u16MbRegOffset,
                                u16RegVal);

        Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_MBFIDR0_ADDR16 + u16MbRegOffset,
                                (uint16)(pTxCfg->TxFrameID & FLEXRAY_MBFIDR_FID_MASK_U16));

        if ((pTxLPduInfo->MessageBufferNumber % 2U) == 0U) {
            pHeaderMB = Fr_Driver_GetData32Pointer((Fr_Driver_PointerSizeType)
                                                   pCtrlCfg->CCHardwareConfigPtr->CCFlexRayMemoryBaseAddress
                                                   + ((Fr_Driver_PointerSizeType)pTxLPduInfo->MessageBufferNumber *
                                                           FLEXRAY_HEADER_SIZE_U8));
        }
        else {
            pHeaderMB = Fr_Driver_GetData32Pointer((Fr_Driver_PointerSizeType)
                                                   pCtrlCfg->CCHardwareConfigPtr->CCFlexRayMemoryBaseAddress
                                                   + (((Fr_Driver_PointerSizeType)pTxLPduInfo->MessageBufferNumber *
                                                           FLEXRAY_HEADER_SIZE_U8) -
                                                           (Fr_Driver_PointerSizeType)2));
        }

        arch_invalidate_cache_range_pri(pHeaderMB, FLEXRAY_HEADER_CACHE_SIZE_U8);
        /* Store frame ID (FrameHeader: FID) */
        u16RegVal =  (uint16)(pTxCfg->TxFrameID & 0x07FFU);

        /* Fr_CtrlIdx: Memory: FrameHeader: PPI */
        /* Is the payload preamble indicator configured? */
        if (pTxCfg->PayloadPreamble == (boolean)TRUE) {
            u16RegVal |= FLEXRAY_FRAMEHEADER0_PPI_MASK_U16;
        }

        /* Frame header 0-1 byte */
        if ((pTxLPduInfo->MessageBufferNumber % 2U) == 0U) {
            pHeaderMB[0U] = (uint32)((uint32)u16RegVal << 16U);
        }
        else {
            pHeaderMB[0U] &= 0xFFFF0000U;
            pHeaderMB[0U] |= u16RegVal;
        }

        u16RegVal = 0U;

        if ((pTxCfg->TxFrameID) >
                pCtrlCfg->LowLevelConfigSetPtr->gNumberOfStaticSlots) {
            u16RegVal |= ((uint16)(pTxCfg->TxPayloadLength) & 0x7FU);
        }
        else {
            u16RegVal |= ((uint16)(pCtrlCfg->LowLevelConfigSetPtr->gPayloadLengthStatic) &
                          0x7FU);
        }

        /* Frame header 2-3 byte - payload length */
        if (((uint32)(pTxLPduInfo->MessageBufferNumber) % 2U) == 0U) {
            pHeaderMB[0U] |= (uint32)(u16RegVal);
        }
        else {
            pHeaderMB[1U] = (uint32)((uint32)u16RegVal << 16U);
        }

        u16RegVal = 0U;
        u16RegVal |= (pTxCfg->HeaderCRC & 0x07FFU);

        if (((uint32)(pTxLPduInfo->MessageBufferNumber) % 2U) == 0U) {
            pHeaderMB[1U] = (uint32)((uint32)u16RegVal << 16U);
        }
        else {
            pHeaderMB[1U] |=  (uint32)(u16RegVal);
        }

        u16RegVal = 0U;

        u16RegVal |=
            pCtrlCfg->BufferConfigSetPtr->BufferOffsetTable[pTxLPduInfo->MessageBufferNumber].DataOffset16;


        if (((uint32)(pTxLPduInfo->MessageBufferNumber) % 2U) == 0U) {
            pHeaderMB[1U] |= (uint32)(u16RegVal);
        }
        else {
            pHeaderMB[2U] = (uint32)((uint32)(u16RegVal) << 16U);
        }

        arch_clean_cache_range_pri(pHeaderMB, FLEXRAY_HEADER_CACHE_SIZE_U8);
        Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_MBIDXR0_ADDR16 + u16MbRegOffset,
                                (uint16)(pTxLPduInfo->MessageBufferNumber));

        if ((boolean)FALSE == (pTxLPduInfo->Reconfigurable)) {
            SchM_Enter_EXCLUSIVE_AREA();
            u16RegVal = (uint16)(Fr_Driver_ReadRegister(pCtrlCfg,
                                 FLEXRAY_MBCCSR0_ADDR16 + u16MbRegOffset) | FLEXRAY_MBCCSR_EDT_U16);
            Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_MBCCSR0_ADDR16 + u16MbRegOffset,
                                    u16RegVal);
            SchM_Exit_EXCLUSIVE_AREA();

            if ((Fr_Driver_ReadRegister(pCtrlCfg,
                                        FLEXRAY_MBCCSR0_ADDR16 + u16MbRegOffset) & FLEXRAY_MBCCSR_EDS_U16) !=
                    FLEXRAY_MBCCSR_EDS_U16) {
                retVal = (Std_ReturnType)(E_NOT_OK);
            }
        }
    }

    return retVal;
}

/***************************************************************************************************
* @function_name    Fr_Driver_RxBufferCfg
*
* @brief            FrIP function for receive message buffer configuration
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[in]        pRxLPduInfo Pointer to the structure with information for message buffer
*                   configuration.
* @return           ::Std_ReturnType
*                       - E_OK      Message buffer was successfully configured
*                       - E_NOT_OK  Error occured within message buffer enabling/disabling
***************************************************************************************************/
static Std_ReturnType Fr_Driver_RxBufferCfg
(
    const Fr_CtrlCfgType *pCtrlCfg,
    const Fr_CCLpduInfoType *pRxLPduInfo
)
{
    /* rxCfg points to configuration referenced by Fr_BufConfSetIdx */
    const Fr_CCRxBufferConfigType *pRxCfg = (const Fr_CCRxBufferConfigType *)(
            pRxLPduInfo->BufferConfigPtr);
    /* Offset from FR base address to the MB registers */
    volatile uint16 u16MbRegOffset = (((uint16)pRxLPduInfo->MessageBufferNumber) *
                                      FLEXRAY_MB_REG_OFFSET);
    /* Pointer to buffer header in the FlexRay memory space */
    uint32 *pHeaderMB = NULL;
    /* Temporary variable for register access */
    uint16 u16RegVal = 0U;

    /* Temporary return value to signal succes or failure of this function */
    Std_ReturnType returnValue = (Std_ReturnType)(E_OK);

    /* Disable appropriate MB */
    if (FLEXRAY_MBCCSR_EDS_U16 == (Fr_Driver_ReadRegister(pCtrlCfg,
                                   FLEXRAY_MBCCSR0_ADDR16 + u16MbRegOffset) & FLEXRAY_MBCCSR_EDS_U16)) {
        /* Disable it */
        Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_MBCCSR0_ADDR16 + u16MbRegOffset,
                                FLEXRAY_MBCCSR_EDT_U16);
    }

    /* Check if buffer has been successfully disabled */
    if (FLEXRAY_MBCCSR_EDS_U16 == (Fr_Driver_ReadRegister(pCtrlCfg,
                                   FLEXRAY_MBCCSR0_ADDR16 + u16MbRegOffset) & FLEXRAY_MBCCSR_EDS_U16)) {
        returnValue = (Std_ReturnType)(E_NOT_OK); /* Signal error */
    }
    else {
        /* Appropriate message buffer is disabled and can be configured */
        /***************************** Configure FR_MBCCSRn register ********************************/
        u16RegVal = FLEXRAY_MBCCSR_MBIE_U16;          /* Clear variable */
        /* Store configuration into the FR_MBCCSRn register */
        Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_MBCCSR0_ADDR16 + u16MbRegOffset,
                                u16RegVal);

        /******************************** Configure FR_MBCCFRn register *****************************/
        u16RegVal = 0U;                          /* Clear variable */

        if ((boolean)TRUE == pRxCfg->RxChannelAEnable) {
            u16RegVal |= FLEXRAY_MBCCFR_CHA_U16;      /* Channel assignment - ch. A */
        }

        if ((boolean)TRUE == pRxCfg->RxChannelBEnable) {
            u16RegVal |= FLEXRAY_MBCCFR_CHB_U16;      /* Channel assignment - ch. B */
        }

        /* Cycle counter filter settings */
        /* Should the cycle counter filter be enabled? */
        if ((boolean)TRUE == pRxCfg->RxCycleCounterFilterEnable) {
            /* Cycle counter filter value */
            u16RegVal |= (uint16)(pRxCfg->RxCycleCounterFilterValue) &
                         FLEXRAY_MBCCFR_CCFVAL_MASK_U16;
            /* Cycle counter filter mask */
            u16RegVal |= (uint16)((uint16)(pRxCfg->RxCycleCounterFilterMask) << 6U) &
                         FLEXRAY_MBCCFR_CCFMSK_MASK_U16;
            /* Cycle counter filter enabled */
            u16RegVal |= FLEXRAY_MBCCFR_CCFE_U16;
        }

        /* Store configuration into FR_MBCCFRn register */
        Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_MBCCFR0_ADDR16 + u16MbRegOffset,
                                u16RegVal);

        /**************************** Configure FR_MBIDXRn register *********************************/
        Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_MBFIDR0_ADDR16 + u16MbRegOffset,
                                pRxCfg->RxFrameID);

        /********************************* Configure FR memory ***********************************/
        /* According to a FlexRay Memory Layout described in FlexRay module documentation */

        /* Calculate the message buffer header:
           FR_memory_base_address + (Configuring_MB_number * MB_header_length) */
        if (((uint32)pRxLPduInfo->MessageBufferNumber % 2U) == 0U) {
            pHeaderMB = Fr_Driver_GetData32Pointer((Fr_Driver_PointerSizeType)
                                                   pCtrlCfg->CCHardwareConfigPtr->CCFlexRayMemoryBaseAddress
                                                   + ((Fr_Driver_PointerSizeType)pRxLPduInfo->MessageBufferNumber *
                                                           FLEXRAY_HEADER_SIZE_U8));
        }
        else {
            pHeaderMB = Fr_Driver_GetData32Pointer((Fr_Driver_PointerSizeType)
                                                   pCtrlCfg->CCHardwareConfigPtr->CCFlexRayMemoryBaseAddress
                                                   + ((((Fr_Driver_PointerSizeType)pRxLPduInfo->MessageBufferNumber *
                                                           FLEXRAY_HEADER_SIZE_U8) - 2U)));
        }

        arch_invalidate_cache_range_pri(pHeaderMB, FLEXRAY_HEADER_CACHE_SIZE_U8);
        /* Configure Frame Header registers */
        /* Fr_CtrlIdx: Memory: FrameHeader3 */
        u16RegVal = 0U;            /* Clear temporary variable */
        /* Data offset load from structure of type BufferOffsetTable */
        u16RegVal |=
            pCtrlCfg->BufferConfigSetPtr->BufferOffsetTable[pRxLPduInfo->MessageBufferNumber].DataOffset16;

        if (((uint32)pRxLPduInfo->MessageBufferNumber % 2U) == 0U) {
            /* Frame header 6-7 byte - Data Field Offset */
            pHeaderMB[1U] &= 0xFFFF0000U;
            pHeaderMB[1U] |= (uint32)(u16RegVal);
            /* Frame header 8-9 byte - slot status */
            pHeaderMB[2U] &= 0x0000FFFFU;   /* Clear slot status */

            /* Store frame ID (FrameHeader: FID) */

        }
        else {
            pHeaderMB[2U] &= 0x0000FFFFU;
            pHeaderMB[2U] |= ((uint32)(u16RegVal) << 16U);
            pHeaderMB[2U] &= 0xFFFF0000U;

        }

        /******************************** Configure FR_MBIDXRn register *****************************/
        /* Message Buffer Index Registers initialization */
        /* Fr_CtrlIdx: FR_MBIDXRn */
        u16RegVal = pRxLPduInfo->MessageBufferNumber;
        Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_MBIDXR0_ADDR16 + u16MbRegOffset,
                                u16RegVal);
        arch_clean_cache_range_pri(pHeaderMB, FLEXRAY_HEADER_CACHE_SIZE_U8);

        /* MB for LPdu wich is Reconfigurable stays disabled */
        if ((boolean)FALSE == (pRxLPduInfo->Reconfigurable)) {
            /* Enable message buffer */
            SchM_Enter_EXCLUSIVE_AREA();
            u16RegVal = (uint16)(Fr_Driver_ReadRegister(pCtrlCfg,
                                 FLEXRAY_MBCCSR0_ADDR16 + u16MbRegOffset) | FLEXRAY_MBCCSR_EDT_U16);
            /* Enable the MB in the FR_MBCCSRn register */
            /* Fr_CtrlIdx: FR_MBCCSRn */
            Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_MBCCSR0_ADDR16 + u16MbRegOffset,
                                    u16RegVal);
            SchM_Exit_EXCLUSIVE_AREA();

            /* Check if buffer is again enabled */
            if (FLEXRAY_MBCCSR_EDS_U16 != (Fr_Driver_ReadRegister(pCtrlCfg,
                                           FLEXRAY_MBCCSR0_ADDR16 + u16MbRegOffset) & FLEXRAY_MBCCSR_EDS_U16)) {
                /* No */
                returnValue = (Std_ReturnType)(E_NOT_OK); /* Signal error */
            }
        }
    }

    return returnValue;
}

/***************************************************************************************************
* @function_name    Fr_Driver_ConfigureFifo
*
* @brief            FrIP function for receive FIFO configuration
* @details          This function will configure receive FIFO
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[in]        pCCFIFOConfig Pointer to the structure with information for configuring MB
* @return           none
***************************************************************************************************/
static void Fr_Driver_ConfigureFifo
(
    const Fr_CtrlCfgType *pCtrlCfg,
    const Fr_CCFifoConfigType *pCCFIFOConfig
)
{
    uint16 u16RegVal = 0U;

    uint32 *pHeaderMB = NULL;

    if (pCCFIFOConfig->FIFOChannel == FR_RECEIVE_FIFOB) {
        u16RegVal |= FLEXRAY_RFWMSR_SEL_U16;
    }

    /* Set SEL bit in FLEXRAY_RFWMSR_ADDR16 register */
    Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_RFSR_ADDR16, u16RegVal);

    /* Configure Receive FIFO Start Data Offset */
    for (uint32 offset = 0U; offset < pCCFIFOConfig->FIFODepth; offset++) {
        if (((pCCFIFOConfig->FIFOStartIndex + offset) % 2U) == 0U) {
            pHeaderMB = Fr_Driver_GetData32Pointer((Fr_Driver_PointerSizeType)
                                                   pCtrlCfg->CCHardwareConfigPtr->CCFlexRayMemoryBaseAddress
                                                   + (((Fr_Driver_PointerSizeType)pCCFIFOConfig->FIFOStartIndex + offset) *
                                                           FLEXRAY_HEADER_SIZE_U8));
        }
        else {
            pHeaderMB = Fr_Driver_GetData32Pointer((Fr_Driver_PointerSizeType)
                                                   pCtrlCfg->CCHardwareConfigPtr->CCFlexRayMemoryBaseAddress
                                                   + (((((Fr_Driver_PointerSizeType)pCCFIFOConfig->FIFOStartIndex + offset) *
                                                           FLEXRAY_HEADER_SIZE_U8) -
                                                           2U)));
        }

        u16RegVal = 0U;
        u16RegVal =
            pCtrlCfg->BufferConfigSetPtr->BufferOffsetTable[pCCFIFOConfig->FIFOStartIndex +
                                                  offset].DataOffset16;

        if (((((uint32)pCCFIFOConfig->FIFOStartIndex) + offset) % 2U) == 0U) {
            /* Frame header 6-7 byte - Data Field Offset */
            pHeaderMB[1U] &= 0xFFFF0000U;
            pHeaderMB[1U] |= u16RegVal;
        }
        else {
            pHeaderMB[2U] &= 0x0000FFFFU;
            pHeaderMB[2U] |= (((uint32) u16RegVal) << 16U);
        }

        arch_clean_cache_range_pri(pHeaderMB, FLEXRAY_HEADER_CACHE_SIZE_U8);
    }



    Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_RFSIR_ADDR16,
                            (uint16)(pCCFIFOConfig->FIFOStartIndex
                                     & FLEXRAY_RFSIR_SIDX_MASK_U16));

    u16RegVal = (uint16)((uint16)(pCCFIFOConfig->FIFOEntrySize) &
                         FLEXRAY_RFDSR_ENTRY_SIZE_MASK_U16);
    u16RegVal |= (uint16)((uint16)(pCCFIFOConfig->FIFODepth) << 8U) &
                 FLEXRAY_RFDSR_FIFO_DEPTH_MASK_U16;

    Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_RFDSR_ADDR16, u16RegVal);

    Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_RFMIDAFVR_ADDR16,
                            (uint16)(pCCFIFOConfig->MessageIDFilterMatch));

    Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_RFMIDAFMR_ADDR16,
                            (uint16)(pCCFIFOConfig->MessageIDFilterMask));

    Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_RFFIDRFVR_ADDR16, 0U);

    Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_RFFIDRFMR_ADDR16, 0x07FFU);


    /* Configure Range Filters */
    /* Range filter 0 enabled? */
    if ((boolean)TRUE ==
            pCCFIFOConfig->FIFORangeFiltersConfig[0U].RangeFilterEnable) {
        u16RegVal =
            FLEXRAY_RFRFCFR_SEL_F0_U16;         /* Set filter Selector - filter 0 */
        u16RegVal  |=
            FLEXRAY_RFRFCFR_IBD_LOWINT_U16;   /* Set lower interval boundary selection */
        /* Set Lower Interval Boundary Slot ID */
        u16RegVal |= (pCCFIFOConfig->FIFORangeFiltersConfig[0U].RangeFilterLowerInterval
                      &
                      FLEXRAY_RFRFCFR_SID_MASK_U16);
        /* Store settings for lower interval boundary */
        Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_RFRFCFR_ADDR16, u16RegVal);

        u16RegVal =
            FLEXRAY_RFRFCFR_SEL_F0_U16;         /* Set filter Selector - filter 0 */
        u16RegVal |=
            FLEXRAY_RFRFCFR_IBD_UPPINT_U16;    /* Set upper interval boundary selection */
        /* Set Upper Interval Boundary Slot ID */
        u16RegVal |= (pCCFIFOConfig->FIFORangeFiltersConfig[0U].RangeFilterUpperInterval
                      &
                      FLEXRAY_RFRFCFR_SID_MASK_U16);
        /* Store settings for upper interval boundary */
        Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_RFRFCFR_ADDR16, u16RegVal);
    }

    /* Range filter 1 enabled? */
    if ((boolean)TRUE ==
            pCCFIFOConfig->FIFORangeFiltersConfig[1U].RangeFilterEnable) {
        u16RegVal =
            FLEXRAY_RFRFCFR_SEL_F1_U16;         /* Set filter Selector - filter 1 */
        u16RegVal  |=
            FLEXRAY_RFRFCFR_IBD_LOWINT_U16;   /* Set lower interval boundary selection */
        /* Set Lower Interval Boundary Slot ID */
        u16RegVal |= (pCCFIFOConfig->FIFORangeFiltersConfig[1U].RangeFilterLowerInterval
                      &
                      FLEXRAY_RFRFCFR_SID_MASK_U16);
        /* Store settings for lower interval boundary */
        Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_RFRFCFR_ADDR16, u16RegVal);

        u16RegVal =
            FLEXRAY_RFRFCFR_SEL_F1_U16;         /* Set filter Selector - filter 1 */
        u16RegVal |=
            FLEXRAY_RFRFCFR_IBD_UPPINT_U16;    /* Set upper interval boundary selection */
        /* Set Upper Interval Boundary Slot ID */
        u16RegVal |= (pCCFIFOConfig->FIFORangeFiltersConfig[1U].RangeFilterUpperInterval
                      &
                      FLEXRAY_RFRFCFR_SID_MASK_U16);
        /* Store settings for upper interval boundary */
        Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_RFRFCFR_ADDR16, u16RegVal);
    }

    /* Range filter 2 enabled? */
    if ((boolean)TRUE ==
            pCCFIFOConfig->FIFORangeFiltersConfig[2U].RangeFilterEnable) {
        u16RegVal =
            FLEXRAY_RFRFCFR_SEL_F2_U16;         /* Set filter Selector - filter 2 */
        u16RegVal  |=
            FLEXRAY_RFRFCFR_IBD_LOWINT_U16;   /* Set lower interval boundary selection */
        /* Set Lower Interval Boundary Slot ID */
        u16RegVal |= (pCCFIFOConfig->FIFORangeFiltersConfig[2U].RangeFilterLowerInterval
                      &
                      FLEXRAY_RFRFCFR_SID_MASK_U16);
        /* Store settings for lower interval boundary */
        Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_RFRFCFR_ADDR16, u16RegVal);

        u16RegVal =
            FLEXRAY_RFRFCFR_SEL_F2_U16;         /* Set filter Selector - filter 2 */
        u16RegVal |=
            FLEXRAY_RFRFCFR_IBD_UPPINT_U16;    /* Set upper interval boundary selection */
        /* Set Upper Interval Boundary Slot ID */
        u16RegVal |= (pCCFIFOConfig->FIFORangeFiltersConfig[2U].RangeFilterUpperInterval
                      &
                      FLEXRAY_RFRFCFR_SID_MASK_U16);
        /* Store settings for upper interval boundary */
        Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_RFRFCFR_ADDR16, u16RegVal);
    }

    /* Range filter 3 */
    /* Range filter 3 enabled? */
    if ((boolean)TRUE ==
            pCCFIFOConfig->FIFORangeFiltersConfig[3U].RangeFilterEnable) {
        u16RegVal =
            FLEXRAY_RFRFCFR_SEL_F3_U16;         /* Set filter Selector - filter 3 */
        u16RegVal  |=
            FLEXRAY_RFRFCFR_IBD_LOWINT_U16;   /* Set lower interval boundary selection */
        /* Set Lower Interval Boundary Slot ID */
        u16RegVal |= (pCCFIFOConfig->FIFORangeFiltersConfig[3U].RangeFilterLowerInterval
                      &
                      FLEXRAY_RFRFCFR_SID_MASK_U16);
        /* Store settings for lower interval boundary */
        Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_RFRFCFR_ADDR16, u16RegVal);

        u16RegVal =
            FLEXRAY_RFRFCFR_SEL_F3_U16;         /* Set filter Selector - filter 3 */
        u16RegVal |=
            FLEXRAY_RFRFCFR_IBD_UPPINT_U16;    /* Set upper interval boundary selection */
        /* Set Upper Interval Boundary Slot ID */
        u16RegVal |= (pCCFIFOConfig->FIFORangeFiltersConfig[3U].RangeFilterUpperInterval
                      &
                      FLEXRAY_RFRFCFR_SID_MASK_U16);
        /* Store settings for upper interval boundary */
        Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_RFRFCFR_ADDR16, u16RegVal);
    }

    /* Configure the FIFO Range Filter Control reg. */
    u16RegVal = 0U;            /* Clear temporary variable */

    /* Range filter 0 enabled? */
    if ((boolean)TRUE ==
            pCCFIFOConfig->FIFORangeFiltersConfig[0U].RangeFilterEnable) {
        u16RegVal |= FLEXRAY_RFRFCTR_F0EN_U16;             /* Enable Range Filter 0 */
    }

    /* Range filter 1 enabled? */
    if ((boolean)TRUE ==
            pCCFIFOConfig->FIFORangeFiltersConfig[1U].RangeFilterEnable) {
        u16RegVal |= FLEXRAY_RFRFCTR_F1EN_U16;             /* Enable Range Filter 1 */
    }

    /* Range filter 2 enabled? */
    if ((boolean)TRUE ==
            pCCFIFOConfig->FIFORangeFiltersConfig[2U].RangeFilterEnable) {
        u16RegVal |= FLEXRAY_RFRFCTR_F2EN_U16;             /* Enable Range Filter 2 */
    }

    /* Range filter 3 enabled? */
    if ((boolean)TRUE ==
            pCCFIFOConfig->FIFORangeFiltersConfig[3U].RangeFilterEnable) {
        u16RegVal |= FLEXRAY_RFRFCTR_F3EN_U16;             /* Enable Range Filter 3  */
    }

    /* Filter 0 as rejection? */
    if (FR_REJECTION == pCCFIFOConfig->FIFORangeFiltersConfig[0U].RangeFilterMode) {
        u16RegVal |=
            FLEXRAY_RFRFCTR_F0MD_U16;             /* Range filter 0 as rejection filter */
    }

    /* Filter 1 as rejection? */
    if (FR_REJECTION == pCCFIFOConfig->FIFORangeFiltersConfig[1U].RangeFilterMode) {
        u16RegVal |=
            FLEXRAY_RFRFCTR_F1MD_U16;             /* Range filter 1 as rejection filter */
    }

    /* Filter 2 as rejection? */
    if (FR_REJECTION == pCCFIFOConfig->FIFORangeFiltersConfig[2U].RangeFilterMode) {
        u16RegVal |=
            FLEXRAY_RFRFCTR_F2MD_U16;             /* Range filter 2 as rejection filter */
    }

    /* Filter 3 as rejection? */
    if (FR_REJECTION == pCCFIFOConfig->FIFORangeFiltersConfig[3U].RangeFilterMode) {
        u16RegVal |=
            FLEXRAY_RFRFCTR_F3MD_U16;             /* Range filter 3 as rejection filter */
    }

    /* Write configuration settings to the FIFO Range Filter Control reg. */
    Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_RFRFCTR_ADDR16, u16RegVal);

}

/***************************************************************************************************
* @function_name    Fr_Driver_BuffersInit
*
* @brief            FrIP function for message buffers configuration
* @param[in]        ctrlIdx - Index of FlexRay CC
* @return           ::Std_ReturnType
*                       - E_OK      Function call finished successfully
*                       - E_NOT_OK  Function call aborted due to errors
***************************************************************************************************/
Std_ReturnType Fr_Driver_BuffersInit(
    const Fr_CtrlCfgType *pCtrlCfg
)
{
    Std_ReturnType retVal = (Std_ReturnType)E_OK;

    const Fr_CCLpduInfoType *plpduInfo = NULL;
    uint16 u16itemIdx = 0U;
    uint16 u16RegVal = 0U;
    boolean bconfigureFifoA = (boolean)TRUE;
    boolean bconfigureFifoB = (boolean)TRUE;
    uint32 *pHeaderMB = NULL;
    uint16 shadowA1Index = 0U;
    uint16 shadowA2Index = 0U;
    uint16 shadowB1Index = 0U;
    uint16 shadowB2Index = 0U;
    uint16 dataOffset = 0U;

    u16RegVal = (uint16)((uint16)(
                             pCtrlCfg->BufferConfigSetPtr->MessageBufferSegment1DataSize) &
                         FLEXRAY_MBDSR_MBSEG1DS_MASK_U16);
    u16RegVal |= (uint16)((uint16)(
                              pCtrlCfg->BufferConfigSetPtr->MessageBufferSegment2DataSize) << 8U) &
                 FLEXRAY_MBDSR_MBSEG2DS_MASK_U16;

    Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_MBDSR_ADDR16, u16RegVal);

    u16RegVal = (uint16)((uint16)(pCtrlCfg->BufferConfigSetPtr->LastMBUTIL) &
                         FLEXRAY_MBSSUTR_LAST_MB_UTIL_MASK_U16);
    u16RegVal |= (uint16)((uint16)(pCtrlCfg->BufferConfigSetPtr->LastMBSEG1) << 8U)
                 &
                 FLEXRAY_MBSSUTR_LAST_MB_SEG1_MASK_U16;

    Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_MBSSUTR_ADDR16, u16RegVal);

    plpduInfo = pCtrlCfg->BufferConfigSetPtr->LPduInfoPtr;

    shadowA1Index = (uint16)(pCtrlCfg->BufferConfigSetPtr->RSBIR_A1BufferIndexInit);
    shadowB1Index = (uint16)(pCtrlCfg->BufferConfigSetPtr->RSBIR_B1BufferIndexInit);
    shadowA2Index = (uint16)(pCtrlCfg->BufferConfigSetPtr->RSBIR_A2BufferIndexInit);
    shadowB2Index = (uint16)(pCtrlCfg->BufferConfigSetPtr->RSBIR_B2BufferIndexInit);

    if (shadowA1Index > 0) {
        u16RegVal = (uint16)(pCtrlCfg->BufferConfigSetPtr->RSBIR_A1BufferIndexInit);
        u16RegVal |= FLEXRAY_RSBIR_SEL_RSBIR_A1_U16;

        Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_RSBIR_ADDR16, u16RegVal);
        dataOffset = 0U;
        dataOffset |=
            pCtrlCfg->BufferConfigSetPtr->BufferOffsetTable[shadowA1Index].DataOffset16;

        if ((shadowA1Index % 2U) == 0U) {
            pHeaderMB = Fr_Driver_GetData32Pointer((Fr_Driver_PointerSizeType)
                                                   pCtrlCfg->CCHardwareConfigPtr->CCFlexRayMemoryBaseAddress
                                                   + ((Fr_Driver_PointerSizeType)shadowA1Index * FLEXRAY_HEADER_SIZE_U8));
            pHeaderMB[1U] &= 0xFFFF0000U;
            pHeaderMB[1U] |= dataOffset;
        }
        else {
            pHeaderMB = Fr_Driver_GetData32Pointer((Fr_Driver_PointerSizeType)
                                                   pCtrlCfg->CCHardwareConfigPtr->CCFlexRayMemoryBaseAddress
                                                   + (((Fr_Driver_PointerSizeType)shadowA1Index * FLEXRAY_HEADER_SIZE_U8) - 2U));
            pHeaderMB[2U] &= 0x0000FFFFU;
            pHeaderMB[2U] |= (((uint32) dataOffset) << 16U);
            pHeaderMB[2U] &= 0xFFFF0000U;
        }

        arch_clean_cache_range_pri(pHeaderMB, FLEXRAY_HEADER_CACHE_SIZE_U8);
    }

    if (shadowB1Index > shadowA1Index) {
        u16RegVal = (uint16)(pCtrlCfg->BufferConfigSetPtr->RSBIR_B1BufferIndexInit);
        u16RegVal |= FLEXRAY_RSBIR_SEL_RSBIR_B1_U16;

        Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_RSBIR_ADDR16, u16RegVal);
        dataOffset = 0U;
        dataOffset |=
            pCtrlCfg->BufferConfigSetPtr->BufferOffsetTable[shadowB1Index].DataOffset16;

        if ((shadowB1Index % 2U) == 0U) {
            pHeaderMB = Fr_Driver_GetData32Pointer((Fr_Driver_PointerSizeType)
                                                   pCtrlCfg->CCHardwareConfigPtr->CCFlexRayMemoryBaseAddress
                                                   + ((Fr_Driver_PointerSizeType)shadowB1Index * FLEXRAY_HEADER_SIZE_U8));
            pHeaderMB[1U] &= 0xFFFF0000U;
            pHeaderMB[1U] |= dataOffset;
        }
        else {
            pHeaderMB = Fr_Driver_GetData32Pointer((Fr_Driver_PointerSizeType)
                                                   pCtrlCfg->CCHardwareConfigPtr->CCFlexRayMemoryBaseAddress
                                                   + (((Fr_Driver_PointerSizeType)shadowB1Index * FLEXRAY_HEADER_SIZE_U8) - 2U));
            pHeaderMB[2U] &= 0x0000FFFFU;
            pHeaderMB[2U] |= (((uint32) dataOffset) << 16U);
            pHeaderMB[2U] &= 0xFFFF0000U;
        }

        arch_clean_cache_range_pri(pHeaderMB, FLEXRAY_HEADER_CACHE_SIZE_U8);
    }

    if (shadowA2Index > shadowB1Index) {
        u16RegVal = (uint16)(pCtrlCfg->BufferConfigSetPtr->RSBIR_A2BufferIndexInit);
        u16RegVal |= FLEXRAY_RSBIR_SEL_RSBIR_A2_U16;

        Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_RSBIR_ADDR16, u16RegVal);
        dataOffset = 0U;
        dataOffset |=
            pCtrlCfg->BufferConfigSetPtr->BufferOffsetTable[shadowA2Index].DataOffset16;

        if ((shadowA2Index % 2U) == 0U) {
            pHeaderMB = Fr_Driver_GetData32Pointer((Fr_Driver_PointerSizeType)
                                                   pCtrlCfg->CCHardwareConfigPtr->CCFlexRayMemoryBaseAddress
                                                   + ((Fr_Driver_PointerSizeType)shadowA2Index * FLEXRAY_HEADER_SIZE_U8));
            pHeaderMB[1U] &= 0xFFFF0000U;
            pHeaderMB[1U] |= dataOffset;
        }
        else {
            pHeaderMB = Fr_Driver_GetData32Pointer((Fr_Driver_PointerSizeType)
                                                   pCtrlCfg->CCHardwareConfigPtr->CCFlexRayMemoryBaseAddress
                                                   + (((Fr_Driver_PointerSizeType)shadowA2Index * FLEXRAY_HEADER_SIZE_U8) - 2U));
            pHeaderMB[2U] &= 0x0000FFFFU;
            pHeaderMB[2U] |= (((uint32) dataOffset) << 16U);
            pHeaderMB[2U] &= 0xFFFF0000U;
        }

        arch_clean_cache_range_pri(pHeaderMB, FLEXRAY_HEADER_CACHE_SIZE_U8);
    }

    if (shadowB2Index > shadowA2Index) {
        u16RegVal = (uint16)(pCtrlCfg->BufferConfigSetPtr->RSBIR_B2BufferIndexInit);
        u16RegVal |= FLEXRAY_RSBIR_SEL_RSBIR_B2_U16;

        Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_RSBIR_ADDR16, u16RegVal);
        dataOffset = 0U;
        dataOffset |=
            pCtrlCfg->BufferConfigSetPtr->BufferOffsetTable[shadowB2Index].DataOffset16;

        if ((shadowB2Index % 2U) == 0U) {
            pHeaderMB = Fr_Driver_GetData32Pointer((Fr_Driver_PointerSizeType)
                                                   pCtrlCfg->CCHardwareConfigPtr->CCFlexRayMemoryBaseAddress
                                                   + ((Fr_Driver_PointerSizeType)shadowB2Index * FLEXRAY_HEADER_SIZE_U8));
            pHeaderMB[1U] &= 0xFFFF0000U;
            pHeaderMB[1U] |= dataOffset;
        }
        else {
            pHeaderMB = Fr_Driver_GetData32Pointer((Fr_Driver_PointerSizeType)
                                                   pCtrlCfg->CCHardwareConfigPtr->CCFlexRayMemoryBaseAddress
                                                   + (((Fr_Driver_PointerSizeType)shadowB2Index * FLEXRAY_HEADER_SIZE_U8) - 2U));
            pHeaderMB[2U] &= 0x0000FFFFU;
            pHeaderMB[2U] |= (((uint32) dataOffset) << 16U);
            pHeaderMB[2U] &= 0xFFFF0000U;
        }

        arch_clean_cache_range_pri(pHeaderMB, FLEXRAY_HEADER_CACHE_SIZE_U8);
    }

    for (u16itemIdx = 0U;
            u16itemIdx < pCtrlCfg->BufferConfigSetPtr->BuffersConfiguredCount;
            u16itemIdx++) {
        if (plpduInfo->FirstInitialization == (boolean)TRUE) {
            switch (plpduInfo->BufferType) {
                case FR_TRANSMIT_BUFFER: {
                    retVal = Fr_Driver_TxBufferCfg(pCtrlCfg, plpduInfo);
                    break;
                }

                case FR_RECEIVE_BUFFER: {
                    retVal = Fr_Driver_RxBufferCfg(pCtrlCfg, plpduInfo);
                    break;
                }

                case FR_FIFOA_BUFFER: {
                    if (bconfigureFifoA == (boolean)TRUE) {
                        Fr_Driver_ConfigureFifo(pCtrlCfg,
                                                (const Fr_CCFifoConfigType *)(plpduInfo->BufferConfigPtr));
                        bconfigureFifoA = (boolean)FALSE;
                    }

                    break;
                }

                case FR_FIFOB_BUFFER: {
                    if (bconfigureFifoB == (boolean)TRUE) {
                        Fr_Driver_ConfigureFifo(pCtrlCfg,
                                                (const Fr_CCFifoConfigType *)(plpduInfo->BufferConfigPtr));
                        bconfigureFifoB = (boolean)FALSE;
                    }
                }

                default: {
                    break;
                }
            }
        }

        plpduInfo++;

        if (retVal == (Std_ReturnType)(E_NOT_OK) ) {
            break;
        }
    }

    return retVal;
}

/***************************************************************************************************
* @function_name    Fr_Driver_DisableTimers
*
* @brief            IP function for disabling timer interrupts
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @return           none
***************************************************************************************************/
void Fr_Driver_DisableTimers(
    const Fr_CtrlCfgType *pCtrlCfg
)
{
    Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_TICCR_ADDR16,
                            (uint16)(FLEXRAY_TICCR_T1SP_U16 | FLEXRAY_TICCR_T2SP_U16));
}

/***************************************************************************************************
* @function_name    Fr_Driver_LeavePOCConfigState
*
* @brief            FrIP function for leaving the POC:Config state - entering POC:Ready state
* @param[in]        ctrlIdx - Index of FlexRay CC
* @return           ::Std_ReturnType:
*                       - E_OK      API call has been successful
*                       - E_NOT_OK  Error occurred during transition into POC:Ready
***************************************************************************************************/
Std_ReturnType Fr_Driver_LeavePOCConfigState(
    const Fr_CtrlCfgType *pCtrlCfg
)
{
    Std_ReturnType retVal = (Std_ReturnType)E_NOT_OK;

    if (Fr_Driver_InvokeCHICommand(pCtrlCfg,
                                   FLEXRAY_POCR_CMDCONFIGCOMPLETE_U16) == (Std_ReturnType)E_OK) {
        if (Fr_Driver_WaitForPOCState(pCtrlCfg,
                                      FLEXRAY_PSR0_PROTSTATE_READY_U16) == (Std_ReturnType)E_OK) {
            retVal = E_OK;
        }
    }

    return retVal;
}

/***************************************************************************************************
* @function_name    Fr_Driver_ClearDisableIRQs
*
* @brief            FrIP function which clears all interrupt flags and disables all interrupts.
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @return           ::Std_ReturnType
*                       - E_OK      All interrupt flags were cleared and all interrupts were
*                                   disabled
*                       - E_NOT_OK  Some interrupt flag was not clear or some interrupt was not
*                                   disabled
***************************************************************************************************/
Std_ReturnType Fr_Driver_ClearDisableIRQs(
    const Fr_CtrlCfgType *pCtrlCfg
)
{
    Std_ReturnType retVal = (Std_ReturnType)E_NOT_OK;
    uint16 RegVal = 0U;
    uint16 TmpVal = 0U;
    boolean bEnd = (boolean)FALSE;
    boolean bEnd2 = (boolean)FALSE;
    uint16 Timeout = 0U;

    Timeout = 0U;

    for (TmpVal = 0U; TmpVal < FR_NUMBER_MB; TmpVal++) {
        SchM_Enter_EXCLUSIVE_AREA();
        RegVal = Fr_Driver_ReadRegister(pCtrlCfg,
                                        FLEXRAY_MBCCSR0_ADDR16 + (TmpVal * FLEXRAY_MB_REG_OFFSET));
        RegVal &= ~(uint16)(FLEXRAY_MBCCSR_CMT_U16 | FLEXRAY_MBCCSR_MBIE_U16);
        RegVal |= FLEXRAY_MBCCSR_MBIF_U16;
        Fr_Driver_WriteRegister(pCtrlCfg,
                                FLEXRAY_MBCCSR0_ADDR16 + (TmpVal * FLEXRAY_MB_REG_OFFSET),
                                RegVal);
        SchM_Exit_EXCLUSIVE_AREA();
    }

    while ((bEnd == (boolean)FALSE) && (bEnd2 == (boolean)FALSE)) {
        SchM_Enter_EXCLUSIVE_AREA();
        RegVal = Fr_Driver_ReadRegister(pCtrlCfg, FLEXRAY_GIFER_ADDR16);

        if (((RegVal & FLEXRAY_GIFER_FNEAIF_U16) == FLEXRAY_GIFER_FNEAIF_U16)
                || ((RegVal & FLEXRAY_GIFER_FNEBIF_U16) == FLEXRAY_GIFER_FNEBIF_U16)) {
            RegVal |= (uint16)(FLEXRAY_GIFER_FNEAIF_U16 | FLEXRAY_GIFER_FNEBIF_U16);
            Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_GIFER_ADDR16, RegVal);
        }
        else {
            bEnd = (boolean)TRUE;
        }

        SchM_Exit_EXCLUSIVE_AREA();

        if (Timeout > 255U) {
            bEnd2 = (boolean)TRUE;
        }

        Timeout++;
    }

    if (bEnd2 == (boolean)FALSE) {
        Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_GIFER_ADDR16,
                                (uint16)(FLEXRAY_GIFER_WUPIF_U16 |
                                         FLEXRAY_GIFER_FNEBIF_U16 |
                                         FLEXRAY_GIFER_FNEAIF_U16));
        Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_PIFR0_ADDR16, 0xFFFFU);
        Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_PIFR1_ADDR16, 0xFFFFU);
        Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_CHIERFR_ADDR16, 0xFFFFU);

        Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_GIFER_ADDR16, 0U);

        if (Fr_Driver_ReadRegister(pCtrlCfg, FLEXRAY_GIFER_ADDR16) == 0U) {
            Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_PIER0_ADDR16, 0U);

            if (Fr_Driver_ReadRegister(pCtrlCfg, FLEXRAY_PIER0_ADDR16) == 0U) {
                Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_PIER1_ADDR16, 0U);

                if (Fr_Driver_ReadRegister(pCtrlCfg, FLEXRAY_PIER1_ADDR16) == 0U) {
                    retVal = (Std_ReturnType)E_OK;
                }
            }
        }

    }

    return retVal;
}

/***************************************************************************************************
* @function_name    Fr_Driver_SetWakeupChannel
*
* @brief            FrIP function for the Fr_SetWakeupChannel API
* @param[in]        ctrlIdx - Index of FlexRay CC
* @param[in]        chnlIdx - Index of FlexRay channel
* @return           ::Std_ReturnType:
*                       - E_OK      API call has been successful
*                       - E_NOT_OK  Error occurred during set wakeup channel
***************************************************************************************************/
Std_ReturnType Fr_Driver_SetWakeupChannel
(
    const Fr_CtrlCfgType *pCtrlCfg,
    Fr_ChannelType chnlIdx
)
{
    Std_ReturnType retVal = (Std_ReturnType)(E_NOT_OK);
    uint16 TmpVal = 0U;

    if (Fr_Driver_InvokeCHICommand(pCtrlCfg,
                                   FLEXRAY_POCR_CMD_CONFIG_U16) == (Std_ReturnType)(E_OK)) {
        if (Fr_Driver_WaitForPOCState(pCtrlCfg,
                                      FLEXRAY_PSR0_PROTSTATE_CONFIG_U16) == (Std_ReturnType)(E_OK)) {
            TmpVal = Fr_Driver_ReadRegister(pCtrlCfg, FLEXRAY_PCR10_ADDR16);

            if (chnlIdx == FR_CHANNEL_A) {
                TmpVal &= ~FLEXRAY_PCR10_WUP_CH_U16;
            }
            else {
                TmpVal |= FLEXRAY_PCR10_WUP_CH_U16;
            }

            Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_PCR10_ADDR16, TmpVal);

            if (Fr_Driver_InvokeCHICommand(pCtrlCfg,
                                           FLEXRAY_POCR_CMDCONFIGCOMPLETE_U16) == (Std_ReturnType)(E_OK)) {
                if (Fr_Driver_WaitForPOCState(pCtrlCfg,
                                              FLEXRAY_PSR0_PROTSTATE_READY_U16) == (Std_ReturnType)(E_OK)) {
                    retVal = (Std_ReturnType)(E_OK);
                }
            }
        }
    }

    return retVal;
}

/***************************************************************************************************
* @function_name    Fr_Driver_SetAbsoluteTimer
*
* @brief            FrIP function for the Fr_SetAbsoluteTimer API
* @details          Store information about absolute timer into the FlexRay CC registers
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[in]        u8TimerIdx - Index of FlexRay timer
* @param[in]        u8Cycle - Cycle the timer shall elapse in
* @param[in]        u16Offset - Offset within cycle Fr_Cycle in units of macrotick the timer shall
*                            elapse at
* @return           none
***************************************************************************************************/
void Fr_Driver_SetAbsoluteTimer
(
    const Fr_CtrlCfgType *pCtrlCfg,
    uint8 u8TimerIdx,
    uint8 u8Cycle,
    uint16 u16Offset
)
{
    uint16 u16StartBit = 0U;
    uint16 u16StopBit = 0U;
    uint16 u16CysrReg = 0U;
    uint16 u16MtorReg = 0U;


    SchM_Enter_EXCLUSIVE_AREA();

    if (u8TimerIdx == 0U) {
        u16StopBit = (uint16)(Fr_Driver_ReadRegister(pCtrlCfg,
                              FLEXRAY_TICCR_ADDR16) | FLEXRAY_TICCR_T1SP_U16);
        u16StartBit = (uint16)(Fr_Driver_ReadRegister(pCtrlCfg,
                               FLEXRAY_TICCR_ADDR16) | FLEXRAY_TICCR_T1TR_U16);
        u16CysrReg = FLEXRAY_TI1CYSR_ADDR16;
        u16MtorReg = FLEXRAY_TI1MTOR_ADDR16;
    }
    else {
        u16StopBit = (uint16)(Fr_Driver_ReadRegister(pCtrlCfg,
                              FLEXRAY_TICCR_ADDR16) | FLEXRAY_TICCR_T2SP_U16);
        u16StartBit = (uint16)(Fr_Driver_ReadRegister(pCtrlCfg,
                               FLEXRAY_TICCR_ADDR16) | FLEXRAY_TICCR_T2TR_U16);
        u16CysrReg = FLEXRAY_TI2CR0_ADDR16;
        u16MtorReg = FLEXRAY_TI2CR1_ADDR16;
    }

    Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_TICCR_ADDR16, u16StopBit);
    Fr_Driver_WriteRegister(pCtrlCfg, u16CysrReg,
                            (uint16)((uint16)(((uint16)u8Cycle) << 8U) | FLEXRAY_TI1CYSR_T1_CYC_MSK_U16));
    Fr_Driver_WriteRegister(pCtrlCfg, u16MtorReg, u16Offset);
    Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_TICCR_ADDR16, u16StartBit);

    SchM_Exit_EXCLUSIVE_AREA();
}
#include "debug.h"
/***************************************************************************************************
* @function_name    Fr_Driver_TransmitTxLPdu
*
* @brief            FrIP function for the Fr_TransmitTxLPdu API
* @param[in]        ctrlIdx - Index of FlexRay CC
* @param[in]        u16LPduIdx - Index of LPdu to be transmitted
* @param[in]        pu8Lsdu - Address of data to be transmitted
* @param[in]        u8LsduLength - Payload length of data to be transmitted
* @param[out]       pSlotAssignment - This reference points to the memory location where the
                    actual cycle, slot ID, and channel of the frame identified by
                    Fr_u16LPduIdx shall be stored. A NULL indicates that the
* @return           ::Std_ReturnType
*                       - E_OK      API call has been successful
*                       - E_NOT_OK  API call aborted due to errors
***************************************************************************************************/
Std_ReturnType Fr_Driver_TransmitTxLPdu
(
    const Fr_CtrlCfgType *pCtrlCfg,
    uint16 LPduIdx,
    const uint8 *pLsdu,
    uint8 LsduLength,
    Fr_SlotAssignmentType *pSlotAssignment
)
{
    Std_ReturnType ret_tx = (Std_ReturnType)(E_NOT_OK);
    uint16 MbRegOffset = 0U;
    uint16 TempBuffIdx = 0U;
    const Fr_CCTxBufferConfigType *pTxCfg = NULL;
    uint32 *pHeaderMB = NULL;
    uint16 RegValue = 0U;
    uint8 *DataMB = NULL;
    uint8 index = 0U;              /* Temporary counter used for copying */
    uint8 tempLen = 0U;
    uint16 tempCrc = 0U;

    Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_GIFER_ADDR16, 0U);
    Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_CHIERFR_ADDR16, 0xFFFFU);

    pTxCfg = (const Fr_CCTxBufferConfigType *)(
                 pCtrlCfg->BufferConfigSetPtr->LPduInfoPtr[LPduIdx].BufferConfigPtr);
    TempBuffIdx =
        pCtrlCfg->BufferConfigSetPtr->LPduInfoPtr[LPduIdx].MessageBufferNumber;
    MbRegOffset = TempBuffIdx * FLEXRAY_MB_REG_OFFSET;

    TempBuffIdx = (uint8)(Fr_Driver_ReadRegister(pCtrlCfg,
                          FLEXRAY_MBIDXR0_ADDR16 + MbRegOffset));

    if (TempBuffIdx <= pCtrlCfg->BufferConfigSetPtr->RSBIR_B2BufferIndexInit) {
        RegValue = Fr_Driver_ReadRegister(pCtrlCfg,
                                          FLEXRAY_MBCCSR0_ADDR16 + MbRegOffset);

        if (FLEXRAY_MBCCSR_MTD_U16 == (RegValue & FLEXRAY_MBCCSR_MTD_U16)) {

            Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_MBCCSR0_ADDR16 + MbRegOffset,
                                    FLEXRAY_MBCCSR_LCKT_U16);
            /* wait for message buffer is locked */
            RegValue = Fr_Driver_ReadRegister(pCtrlCfg,
                                              FLEXRAY_MBCCSR0_ADDR16 + MbRegOffset);

            if (FLEXRAY_MBCCSR_LCKS_U16 == (RegValue & FLEXRAY_MBCCSR_LCKS_U16)) {
                /* MB is locked */
                if ((TempBuffIdx % 2U) == 0U) {
                    pHeaderMB = Fr_Driver_GetData32Pointer((Fr_Driver_PointerSizeType)
                                                           pCtrlCfg->CCHardwareConfigPtr->CCFlexRayMemoryBaseAddress
                                                           + ((Fr_Driver_PointerSizeType)TempBuffIdx * FLEXRAY_HEADER_SIZE_U8));
                }
                else {
                    pHeaderMB = Fr_Driver_GetData32Pointer((Fr_Driver_PointerSizeType)
                                                           pCtrlCfg->CCHardwareConfigPtr->CCFlexRayMemoryBaseAddress
                                                           + (((Fr_Driver_PointerSizeType)TempBuffIdx * FLEXRAY_HEADER_SIZE_U8) - 2U));
                }

                arch_invalidate_cache_range_pri(pHeaderMB, FLEXRAY_HEADER_CACHE_SIZE_U8);
                DataMB = pCtrlCfg->BufferConfigSetPtr->BufferAddressTable[TempBuffIdx].MbDataAddrPtr;

                SchM_Enter_EXCLUSIVE_AREA();

                for (index = 0U; index < LsduLength; index++) {
                    DataMB[index] = pLsdu[index];
                }

                SchM_Exit_EXCLUSIVE_AREA();
                arch_clean_cache_range_pri(DataMB, LsduLength);
                Fr_Driver_FrExtendedLPduReport(pCtrlCfg, pTxCfg, pSlotAssignment);

                if ((boolean)TRUE == pTxCfg->AllowDynamicLength) {
                    /* byte convert to Words */
                    tempLen = LsduLength >> 1U;

                    /* Check if PayloadLength is odd value */
                    if (0U != (LsduLength % 2U)) {
                        tempLen += 1U;
                    }

                    if ((TempBuffIdx % 2U) == 0U) {
                        if ((pHeaderMB[0U] & FLEXRAY_FRAMEHEADER1_PLDLEN_MASK_U16) != tempLen) {
                            /*Clear plaod len*/
                            pHeaderMB[0U] &= FLEXRAY_FRAMEHEADER1_PLDLEN_MASK_LITTLE;
                            pHeaderMB[0U] |= tempLen;
                            /* Calculate new header_crc - it is not calculation of configuration data */
                            tempCrc = Fr_Driver_Calcu16HeaderCRC(Fr_Driver_ReadRegister(pCtrlCfg,
                                                                 FLEXRAY_MBFIDR0_ADDR16 + MbRegOffset),
                                                                 (uint16)tempLen);
                            /*Clean Header CRC*/
                            pHeaderMB[1U] &= FLEXRAY_FRAMEHEADER1_CRC_MASK_LITTLE;
                            pHeaderMB[1U] |= (uint32)(((uint32)((uint32)tempCrc
                                                                & (uint32)FLEXRAY_FRAMEHEADER1_CRC_MASK_U16)) << 16U);
                        }
                    }
                    else {
                        if (((pHeaderMB[1U] >> 16) & FLEXRAY_FRAMEHEADER1_PLDLEN_MASK_U16) != tempLen) {
                            /*Clear plaod len*/
                            pHeaderMB[1U] &= 0xFF80FFFFU;
                            pHeaderMB[1U] |= (uint32)((uint32)tempLen << 16U);
                            /* Calculate new header_crc - it is not calculation of configuration data */
                            tempCrc = Fr_Driver_Calcu16HeaderCRC(Fr_Driver_ReadRegister(pCtrlCfg,
                                                                 FLEXRAY_MBFIDR0_ADDR16 + MbRegOffset),
                                                                 (uint16)tempLen);
                            /*Clean Header CRC*/
                            pHeaderMB[1U] &= 0xFFFFF800U;
                            pHeaderMB[1U] |= (uint32)((uint32)tempCrc & (uint32)
                                                      FLEXRAY_FRAMEHEADER1_CRC_MASK_U16);
                        }
                    }
                }

                /* Avoid repeated definitions after inline expansion */
                {
                    SchM_Enter_EXCLUSIVE_AREA();
                    RegValue = (uint16)(Fr_Driver_ReadRegister(pCtrlCfg,
                                        FLEXRAY_MBCCSR0_ADDR16 + MbRegOffset) & FLEXRAY_MBCCSR0_CONFIG_MASK_U16);
                    RegValue |= (uint16)(FLEXRAY_MBCCSR_CMT_U16 | FLEXRAY_MBCCSR_MBIF_U16);
                    Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_MBCCSR0_ADDR16 + MbRegOffset,
                                            RegValue);
                    SchM_Exit_EXCLUSIVE_AREA();
                }
                arch_clean_cache_range_pri(pHeaderMB, FLEXRAY_HEADER_CACHE_SIZE_U8);
                Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_MBCCSR0_ADDR16 + MbRegOffset,
                                        FLEXRAY_MBCCSR_LCKT_U16);

                ret_tx = (Std_ReturnType)(E_OK);
            }
            else {
                /* MB is not locked */
                ret_tx = (Std_ReturnType)(E_NOT_OK);
            }
        }
        else {
            /* HW error MB is not configured for Tx -> return E_NOT_OK */
        }
    }

    return ret_tx;
}

/***************************************************************************************************
* @function_name    Fr_Driver_CancelTxLPdu
*
* @brief            FrIP function for the Fr_CancelTxLPdu API
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[in]        LPduIdx - Index of LPdu to be canceled
* @param[out]       pbPendingStatus - Information whether the LPdu was canceled or not
* @return           ::Std_ReturnType
*                       - E_OK      Function call finished successfully:
*                       - E_NOT_OK  Function call aborted due to no access to the FlexRay CC
***************************************************************************************************/
Std_ReturnType Fr_Driver_CancelTxLPdu
(
    const Fr_CtrlCfgType *pCtrlCfg,
    uint16 LPduIdx,
    boolean *pbPendingStatus
)
{
    /* Set initial return value */
    Std_ReturnType ret = (Std_ReturnType)(E_NOT_OK);
    uint16 TempMBIdx = 0U;
    uint16 MbRegOffset = 0U;
    uint16 TempRegVal = 0U;

    /* Store the current MB index */
    TempMBIdx =
        pCtrlCfg->BufferConfigSetPtr->LPduInfoPtr[LPduIdx].MessageBufferNumber;

    /* Temporary offset address of MB configuration registers */
    MbRegOffset = TempMBIdx * FLEXRAY_MB_REG_OFFSET;

    /* Read FR_MBCCSR register value */
    SchM_Enter_EXCLUSIVE_AREA();
    TempRegVal = Fr_Driver_ReadRegister(pCtrlCfg,
                                        FLEXRAY_MBCCSR0_ADDR16 + MbRegOffset);

    /* Message buffer configured for Tx ? */
    if (FLEXRAY_MBCCSR_MTD_U16 == (TempRegVal & FLEXRAY_MBCCSR_MTD_U16)) {
        /* Message buffer data ready for transmission ? */
        if (FLEXRAY_MBCCSR_CMT_U16 == (TempRegVal & FLEXRAY_MBCCSR_CMT_U16)) {
            /* Yes - Data are ready for transmission */
            *pbPendingStatus = (boolean)(TRUE);
            /* Lock MB -> write 1 to LCKT bit and 0 to the EDT bit in FR_MBCCSR register for current MB
              (write access to all other bits is ignored) */
            Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_MBCCSR0_ADDR16 + MbRegOffset,
                                    FLEXRAY_MBCCSR_LCKT_U16);
            /* Select only necessary bits from MBCCSRx register */
            TempRegVal &= FLEXRAY_MBCCSR0_CONFIG_MASK_U16;
            /* Clear CMT bit */
            TempRegVal &= ~FLEXRAY_MBCCSR_CMT_U16;
            Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_MBCCSR0_ADDR16 + MbRegOffset,
                                    TempRegVal);
            /* Unlock MB -> write 1 to LCKT bit and 0 to the EDT bit in FR_MBCCSR register for current MB
              (write access to all other bits is ignored) */
            Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_MBCCSR0_ADDR16 + MbRegOffset,
                                    FLEXRAY_MBCCSR_LCKT_U16);

            /* Is the MB Unlocked and CMT cleared? */
            TempRegVal = Fr_Driver_ReadRegister(pCtrlCfg,
                                                FLEXRAY_MBCCSR0_ADDR16 + MbRegOffset);

            if ((FLEXRAY_MBCCSR_LCKS_U16 != (TempRegVal & FLEXRAY_MBCCSR_LCKS_U16))
                    && (FLEXRAY_MBCCSR_CMT_U16 != (TempRegVal & FLEXRAY_MBCCSR_CMT_U16))) {
                /* Yes */
                ret = (Std_ReturnType)(E_OK);
            }
            else {
                /* This is HW error return E_NOT_OK and pbPendingStatus TRUE */
            }
        }
        else {
            /* Data is not ready for transmission return E_OK pbPendingStatus FALSE */
            ret = (Std_ReturnType)(E_OK);
        }
    }
    else {
        /* MB is not configured for Tx -> return E_NOT_OK */
    }

    SchM_Exit_EXCLUSIVE_AREA();
    return ret;
}

/***************************************************************************************************
* @function_name    Fr_Flexray_ReceiveRxLPdu
*
* @brief            FrIP function for the Fr_ReceiveRxLPdu API
* @param[in]        ctrlIdx - Index of FlexRay CC
* @param[in]        u16LPduIdx - Index of LPdu to be transmitted
* @param[out]       pu8Lsdu - Address of field that data is copied to
* @param[out]       pLPduStatus - Address of the variable the status is stored to
* @param[out]       pu8LsduLength - Address of the variable the payload is stored to
* @param[out]       pSlotAssignment - Address of the variable the payload is stored to
* @return           ::Std_ReturnType
*                       - E_OK      Function call finished successfully:
*                       - E_NOT_OK  Function call aborted due to no access to the controller
***************************************************************************************************/
Std_ReturnType Fr_Driver_ReceiveRxLPdu
(
    const Fr_CtrlCfgType *pCtrlCfg,
    uint16 LPduIdx,
    uint8 *pLsdu,
    Fr_RxLPduStatusType *pLPduStatus,
    uint8 *pLsduLength,
    Fr_SlotAssignmentType *pSlotAssignment
)
{
    Std_ReturnType ret = (Std_ReturnType)(E_NOT_OK);
    uint16 RegTempVal = 0U;
    uint16 TempSlotStatusInf = 0U;
    uint16 TempMBIdx = 0U;
    uint16 MbRegOffset = 0U;
    const uint32 *pHeaderMB = NULL;
    const Fr_CCRxBufferConfigType *pRxCfg = NULL;
    uint8 TempPayloadLen = 0U;
    boolean bStatusVal = FALSE;

    TempMBIdx =
        pCtrlCfg->BufferConfigSetPtr->LPduInfoPtr[LPduIdx].MessageBufferNumber;
    MbRegOffset = TempMBIdx * FLEXRAY_MB_REG_OFFSET;
    TempMBIdx = Fr_Driver_ReadRegister(pCtrlCfg,
                                       FLEXRAY_MBIDXR0_ADDR16 + MbRegOffset);

    pRxCfg = (const Fr_CCRxBufferConfigType *)
             pCtrlCfg->BufferConfigSetPtr->LPduInfoPtr[LPduIdx].BufferConfigPtr;

    /* Check whether the read value not greater than the last utilized MB index */
    if (TempMBIdx <= pCtrlCfg->BufferConfigSetPtr->RSBIR_B2BufferIndexInit) {
        /* Read FR_MBCCSR register value */
        RegTempVal = Fr_Driver_ReadRegister(pCtrlCfg,
                                            FLEXRAY_MBCCSR0_ADDR16 + MbRegOffset);

        /* Message buffer configured for Rx ? */
        if (FLEXRAY_MBCCSR_MTD_U16 != (RegTempVal & FLEXRAY_MBCCSR_MTD_U16)) {
            /*Check whether an unread valid frame was received*/
            if ((FLEXRAY_MBCCSR_DUP_U16 == (RegTempVal & FLEXRAY_MBCCSR_DUP_U16))
                    && (FLEXRAY_MBCCSR_MBIF_U16 == (RegTempVal & FLEXRAY_MBCCSR_MBIF_U16))) {
                /* Lock Rx MB */
                /* application writes 1 to LCKT bit and 0 to the EDT bit */

                if ((TempMBIdx % 2U) == 0U) {
                    pHeaderMB = Fr_Driver_GetData32Pointer((Fr_Driver_PointerSizeType)
                                                           pCtrlCfg->CCHardwareConfigPtr->CCFlexRayMemoryBaseAddress
                                                           + ((Fr_Driver_PointerSizeType)TempMBIdx * FLEXRAY_HEADER_SIZE_U8));
                    arch_invalidate_cache_range_pri(pHeaderMB, FLEXRAY_HEADER_CACHE_SIZE_U8);
                    /* Load data length from Frame header reg. [Words] */
                    TempPayloadLen = (uint8)(pHeaderMB[0U] & FLEXRAY_FRAMEHEADER1_PLDLEN_MASK_U16);
                }
                else {
                    pHeaderMB = Fr_Driver_GetData32Pointer((Fr_Driver_PointerSizeType)
                                                           pCtrlCfg->CCHardwareConfigPtr->CCFlexRayMemoryBaseAddress
                                                           + (((Fr_Driver_PointerSizeType)TempMBIdx * FLEXRAY_HEADER_SIZE_U8) - 2U));
                    arch_invalidate_cache_range_pri(pHeaderMB, FLEXRAY_HEADER_CACHE_SIZE_U8);
                    /* Load data length from Frame header reg. [Words] */
                    TempPayloadLen = (uint8)((pHeaderMB[1U] >> 16) &
                                             FLEXRAY_FRAMEHEADER1_PLDLEN_MASK_U16);
                }


#if (FR_RXSTRINGENTLENGTHCHECK == STD_ON)

                /* Payload data length is also limited by Rx MB configuration */
                if (TempPayloadLen == pRxCfg->RxPayloadLength) {
#else

                /* Copy only limited length of data */
                if (TempPayloadLen > pRxCfg->RxPayloadLength) {
                    TempPayloadLen = pRxCfg->RxPayloadLength;
                }

#endif /* FR_RX_STRINGENT_LENGTH_CHECK */
                    /* Read the FR_MBCCFRn register */
                    RegTempVal = Fr_Driver_ReadRegister(pCtrlCfg,
                                                        FLEXRAY_MBCCFR0_ADDR16 + MbRegOffset);

                    if ((TempMBIdx % 2U) == 0U) {
                        TempSlotStatusInf = (uint16)(pHeaderMB[FLEXRAY_STATUS_SLOT_U32] >> 16U);
                    }
                    else {
                        TempSlotStatusInf = (uint16)(pHeaderMB[FLEXRAY_STATUS_SLOT_U32] & 0x0000FFFFU);
                    }

                    if ((FLEXRAY_MBCCFR_CHA_U16 == (RegTempVal & FLEXRAY_MBCCFR_CHA_U16))
                            && (FLEXRAY_MBCCFR_CHB_U16 == (RegTempVal & FLEXRAY_MBCCFR_CHB_U16))) {
                        /* MB is assigned to channels AB */
                        /* Check Slot Status information */
                        if ((0x0080U == (TempSlotStatusInf & FLEXRAY_STRINGENT_MASK_A))
                                || (0x8000U == (TempSlotStatusInf & FLEXRAY_STRINGENT_MASK_B))) {
                            /* channel receive data */
                            /*PRQA S 2812 2*/
                            pSlotAssignment->channelId = FR_CHANNEL_AB;
                            bStatusVal = TRUE;
                        }
                    }
                    else if (FLEXRAY_MBCCFR_CHA_U16 == (RegTempVal & FLEXRAY_MBCCFR_CHA_U16)) {
                        /* MB is assigned to channel A */
                        /* Check Slot Status information */
                        if (0x0080U == (TempSlotStatusInf & FLEXRAY_STRINGENT_MASK_A)) {
                            /* channel receive data */
                            pSlotAssignment->channelId = FR_CHANNEL_A;
                            bStatusVal = TRUE;
                        }
                    }
                    else {
                        /* MB is assigned to channel B */
                        /* Check Slot Status information */
                        if (0x8000U == (TempSlotStatusInf & FLEXRAY_STRINGENT_MASK_B)) {
                            /* channel receive data */
                            pSlotAssignment->channelId = FR_CHANNEL_B;
                            bStatusVal = TRUE;
                        }
                    }

                    if ((boolean)TRUE == bStatusVal) {
                        /* Stringent check pass */
                        Fr_Driver_ReadMB(pCtrlCfg, pLsdu, TempPayloadLen, TempMBIdx);
                        /* Store the reception status */
                        *pLPduStatus = FR_RECEIVED;
                        /* Store the number of copied bytes */
                        *pLsduLength = (uint8)(TempPayloadLen << 1U);

                        if (NULL != pSlotAssignment) {
                            if ((TempMBIdx % 2U) == 0U) {
                                /* copy cycle of the frame to Fr_SlotAssignment*/
                                pSlotAssignment->Cycle = (uint8)((pHeaderMB[0U]
                                                                  & FLEXRAY_FRAMEHEADER1_CYCCNT_MASK_U32) >>
                                                                 FLEXRAY_FRAMEHEADER1_CYCCNT_OFFSET_U32);
                                /* copy slot ID of the frame to Fr_SlotAssignment*/
                                pSlotAssignment->SlotId = (uint16)((pHeaderMB[0U] &
                                                                    FLEXRAY_FRAMEHEADER0_FID_MASK_U32) >> 16);
                            }
                            else {
                                /* copy cycle of the frame to Fr_SlotAssignment*/
                                pSlotAssignment->Cycle = (uint8)((pHeaderMB[1U] & 0x3F000000U) >> 24U);
                                /* copy slot ID of the frame to Fr_SlotAssignment*/
                                pSlotAssignment->SlotId = (uint16)(pHeaderMB[0U] & 0x000007FFU);
                            }
                        }
                    }
                    else {
                        /* Store the reception status */
                        *pLPduStatus = FR_NOT_RECEIVED;
                        /* Store the number of copied bytes */
                        *pLsduLength = 0U;
                    }

#if (FR_RXSTRINGENTLENGTHCHECK == STD_ON)
                }
                else {
                    /* Data are not accepted */
                    /* Store the reception status */
                    *pLPduStatus = FR_NOT_RECEIVED;
                    /* Store the number of copied bytes */
                    *pLsduLength = 0U;
                }

#endif /* FR_RX_STRINGENT_LENGTH_CHECK */

                /* Clear Message Buffer Interrupt Flag */
                /* Load FR_MBCCSRn register and select only necessary bits */
                SchM_Enter_EXCLUSIVE_AREA();
                RegTempVal = Fr_Driver_ReadRegister(pCtrlCfg,
                                                    FLEXRAY_MBCCSR0_ADDR16 + MbRegOffset) & FLEXRAY_MBCCSR0_CONFIG_MASK_U16;
                /*Clear flag*/
                RegTempVal |= FLEXRAY_MBCCSR_MBIF_U16;
                Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_MBCCSR0_ADDR16 + MbRegOffset,
                                        RegTempVal);
                SchM_Exit_EXCLUSIVE_AREA();
                /* If the application writes 1 to LCKT bit and 0 to the EDT bit,
                   the write access to all other bits is ignored */
                /* Unlock MB */
                ret = (Std_ReturnType)(E_OK);

            }
            else {
                /* MBIF == 0 of DUP == 0 (data already read or no valid frame received) */
                /* Store the length "0" into pLsduLength */
                *pLsduLength = 0U;
                /* Store the reception status */
                *pLPduStatus = FR_NOT_RECEIVED;
                ret = (Std_ReturnType)(E_OK);
                /* Clear Message Buffer Interrupt Flag */
                /* Load FR_MBCCSRn register and select only necessary bits */
                SchM_Enter_EXCLUSIVE_AREA();
                RegTempVal = Fr_Driver_ReadRegister(pCtrlCfg,
                                                    FLEXRAY_MBCCSR0_ADDR16 + MbRegOffset) & FLEXRAY_MBCCSR0_CONFIG_MASK_U16;
                RegTempVal |= FLEXRAY_MBCCSR_MBIF_U16;
                Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_MBCCSR0_ADDR16 + MbRegOffset,
                                        RegTempVal);
                SchM_Exit_EXCLUSIVE_AREA();
            }
        }
    }

    return ret;
}

/***************************************************************************************************
* @function_name    Fr_Driver_ReceiveFifo
*
* @brief            FrIP function for the Fr_ReceiveRxLPdu API
* @description      Copy Fifo data
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[in]        LPduIdx - Index of LPdu to be transmitted
* @param[out]       pLsdu - Address of field that data is copied to
* @param[out]       pLPduStatus - Address of the variable the status is stored to
* @param[out]       pLsduLength - Address of the variable the payload is stored to
* @return           none
***************************************************************************************************/
void Fr_Driver_ReceiveFifo
(
    const Fr_CtrlCfgType *pCtrlCfg,
    uint16 LPduIdx,
    uint8 *pLsdu,
    Fr_RxLPduStatusType *pLPduStatus,
    uint8 *pLsduLength,
    Fr_SlotAssignmentType *pSlotAssignment,
    const Fr_CCFifoChannelType nFIFOChannel
)
{
    uint16 TempVal = 0U;
    uint16 TmpMBIdx = 0U;
    const uint32 *pHeaderMB = NULL;
    uint8 TempPayloadLen = 0U;
    boolean bReceive = FALSE;

    if ((FLEXRAY_GIFER_FNEAIF_U16 == (Fr_Driver_ReadRegister(pCtrlCfg,
                                      FLEXRAY_GIFER_ADDR16) & FLEXRAY_GIFER_FNEAIF_U16))
            && (FR_RECEIVE_FIFOA == nFIFOChannel)) {
        /* FIFO A is select and not empty  */
        /* Load Read index - MB header index of the next available FIFO buffer that can be read */
        TmpMBIdx = (uint16)(Fr_Driver_ReadRegister(pCtrlCfg, FLEXRAY_RFARIR_ADDR16));
    }
    else if ((FLEXRAY_GIFER_FNEBIF_U16 == (Fr_Driver_ReadRegister(pCtrlCfg,
                                           FLEXRAY_GIFER_ADDR16) & FLEXRAY_GIFER_FNEBIF_U16))
             && (FR_RECEIVE_FIFOB == nFIFOChannel)) {
        /* FIFO B is select and not empty  */
        /* Load Read index - MB header index of the next available FIFO buffer that can be read */
        TmpMBIdx = (uint16)(Fr_Driver_ReadRegister(pCtrlCfg, FLEXRAY_RFBRIR_ADDR16));
    }
    else {
        /* selected FIFO  is empty */
        /* Store the length "0" into pLsduLength and return*/
        *pLsduLength = 0U;
        /* Store the reception status */
        *pLPduStatus = FR_NOT_RECEIVED;
        return;
    }

    if ((TmpMBIdx % 2U) == 0U) {
        pHeaderMB = Fr_Driver_GetData32Pointer((Fr_Driver_PointerSizeType)
                                               pCtrlCfg->CCHardwareConfigPtr->CCFlexRayMemoryBaseAddress
                                               + ((Fr_Driver_PointerSizeType)TmpMBIdx * FLEXRAY_HEADER_SIZE_U8));
        arch_invalidate_cache_range_pri(pHeaderMB, FLEXRAY_HEADER_CACHE_SIZE_U8);
        /* Load data length from Frame header reg. [Words] */
        TempPayloadLen = (uint8)(pHeaderMB[0U] & FLEXRAY_FRAMEHEADER1_PLDLEN_MASK_U16);
    }
    else {
        pHeaderMB = Fr_Driver_GetData32Pointer((Fr_Driver_PointerSizeType)
                                               pCtrlCfg->CCHardwareConfigPtr->CCFlexRayMemoryBaseAddress
                                               + (((Fr_Driver_PointerSizeType)TmpMBIdx * FLEXRAY_HEADER_SIZE_U8) -
                                                       (Fr_Driver_PointerSizeType)2));
        arch_invalidate_cache_range_pri(pHeaderMB, FLEXRAY_HEADER_CACHE_SIZE_U8);
        /* Load data length from Frame header reg. [Words] */
        TempPayloadLen = (uint8)((pHeaderMB[1U] >> 16) &
                                 FLEXRAY_FRAMEHEADER1_PLDLEN_MASK_U16);
    }

    /* Load configured payload length into TempVal */
    TempVal = (((const Fr_CCFifoConfigType *)(
                    pCtrlCfg->BufferConfigSetPtr->LPduInfoPtr[LPduIdx].BufferConfigPtr))->FIFOEntrySize);
#if (FR_RXSTRINGENTLENGTHCHECK == STD_ON)

    /* Payload data length is also limited by Rx MB configuration */
    if (TempPayloadLen == TempVal) {
#else

    /* Copy only limited length of data */
    if (TempPayloadLen > TempVal) {
        TempPayloadLen = (uint8)TempVal;
    }

#endif /* FR_RX_STRINGENT_LENGTH_CHECK */
        bReceive = FALSE;

        if (FR_RECEIVE_FIFOA == nFIFOChannel) {
            /* Stringent check for FIFO A */
            if ((TmpMBIdx % 2U) == 0U) {
                if (0x0080U == ((pHeaderMB[FLEXRAY_STATUS_SLOT_U32] >> 16) &
                                FLEXRAY_STRINGENT_MASK_A)) {
                    bReceive = TRUE;
                }
            }
            else {
                if (0x0080U == (pHeaderMB[FLEXRAY_STATUS_SLOT_U32] &
                                FLEXRAY_STRINGENT_MASK_A)) {
                    bReceive = TRUE;
                }
            }
        }
        else {
            if ((TmpMBIdx % 2U) == 0U) {
                /* Stringent check for FIFO B */
                if (0x8000U == ((pHeaderMB[FLEXRAY_STATUS_SLOT_U32] >> 16) &
                                FLEXRAY_STRINGENT_MASK_B)) {
                    /* stringent check passed */
                    bReceive = TRUE;
                }
            }
            else {
                /* Stringent check for FIFO B */
                if (0x8000U == (pHeaderMB[FLEXRAY_STATUS_SLOT_U32] &
                                FLEXRAY_STRINGENT_MASK_B)) {
                    /* stringent check passed */
                    bReceive = TRUE;
                }
            }
        }

        if ((boolean)TRUE == bReceive ) {
            /* Copy data in MB */
            Fr_Driver_ReadMB(pCtrlCfg, pLsdu, TempPayloadLen, TmpMBIdx);

            if (NULL != pSlotAssignment) {
                /* copy cycle of the frame to Fr_SlotAssignment*/
                (pSlotAssignment->Cycle) = (uint8)((pHeaderMB[0U]
                                                    & FLEXRAY_FRAMEHEADER1_CYCCNT_MASK_U32) >>
                                                   FLEXRAY_FRAMEHEADER1_CYCCNT_OFFSET_U32);
                /* copy slot ID of the frame to Fr_SlotAssignment*/
                (pSlotAssignment->SlotId) = (uint16)(pHeaderMB[0U] &
                                                     FLEXRAY_FRAMEHEADER0_FID_MASK_U32);

                /* channel receive data */
                if (FR_RECEIVE_FIFOA == nFIFOChannel) {
                    pSlotAssignment->channelId = FR_CHANNEL_A;
                }
                else {
                    pSlotAssignment->channelId = FR_CHANNEL_B;
                }
            }

            /* Store the number of copied bytes */
            *pLsduLength = (uint8)(TempPayloadLen << 1U);
            /* Store the reception status */
            *pLPduStatus = FR_RECEIVED_MORE_DATA_AVAILABLE;
        }
        else {
            /* Store the number of copied bytes */
            *pLsduLength = 0U;
            /* Store the reception status */
            *pLPduStatus = FR_NOT_RECEIVED;
        }

#if (FR_RXSTRINGENTLENGTHCHECK == STD_ON)
    }
    else   /* received payload length does not match configured payload length */
    {
        /* Store the number of copied bytes */
        *pLsduLength = 0U;
        /* Store the reception status */
        *pLPduStatus = FR_NOT_RECEIVED;
    }

#endif /* FR_RX_STRINGENT_LENGTH_CHECK */

    if (FR_RECEIVE_FIFOA == nFIFOChannel)
    {
        /* Clear FIFO A not empty flag */
        /* Load GIFER register and select only necessary bits */
        SchM_Enter_EXCLUSIVE_AREA();
        TempVal = (uint16)(Fr_Driver_ReadRegister(pCtrlCfg, FLEXRAY_GIFER_ADDR16)
                           & (uint16)(~FLEXRAY_GIFER_INT_FLAGS_MASK_U16));
        TempVal |= FLEXRAY_GIFER_FNEAIF_U16;
        Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_GIFER_ADDR16, TempVal);
        SchM_Exit_EXCLUSIVE_AREA();
    }
    else
    {
        /* Clear FIFO B not empty flag */
        /* Load GIFER register and select only necessary bits */
        SchM_Enter_EXCLUSIVE_AREA();
        TempVal = (uint16)(Fr_Driver_ReadRegister(pCtrlCfg, FLEXRAY_GIFER_ADDR16)
                           & (uint16)(~FLEXRAY_GIFER_INT_FLAGS_MASK_U16));
        TempVal |= FLEXRAY_GIFER_FNEBIF_U16;
        Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_GIFER_ADDR16, TempVal);
        SchM_Exit_EXCLUSIVE_AREA();
    }
}

/***************************************************************************************************
* @function_name    Fr_Driver_CheckTxLPduStatus
*
* @brief            FrIP function for the Fr_CheckTxLPduStatus API
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[in]        LPduIdx - Index of LPdu to be canceled
* @param[out]       pTxLPduStatus - Address of the variable where the status information is stored to
* @param[out]       pSlotAssignment - Address of the variable where the status information is stored to
* @return           ::Std_ReturnType
*                       - E_OK      Function call finished successfully:
*                       - E_NOT_OK  Function call aborted due to no access to the FlexRay CC
***************************************************************************************************/
Std_ReturnType Fr_Driver_CheckTxLPduStatus
(
    const Fr_CtrlCfgType *pCtrlCfg,
    uint16 LPduIdx,
    Fr_TxLPduStatusType *pTxLPduStatus,
    Fr_SlotAssignmentType *pSlotAssignment
)
{
    Std_ReturnType ret = (Std_ReturnType)(E_NOT_OK);
    uint16 MbIdx = 0U;
    uint16 MbRegOffset = 0U;
    uint16 TempRegVal = 0U;
    const Fr_CCTxBufferConfigType *pTxCfg = NULL;

    /* Store the configured MB index */
    MbIdx = pCtrlCfg->BufferConfigSetPtr->LPduInfoPtr[LPduIdx].MessageBufferNumber;
    /* Pointer to one instance of Fr_CCReceiveBufferConfigType */
    pTxCfg = (const Fr_CCTxBufferConfigType *)
             pCtrlCfg->BufferConfigSetPtr->LPduInfoPtr[LPduIdx].BufferConfigPtr;

    /* Temporary offset address of MB configuration registers */
    MbRegOffset = MbIdx * FLEXRAY_MB_REG_OFFSET;
    /* Read FR_MBCCSR register value */
    TempRegVal = Fr_Driver_ReadRegister(pCtrlCfg,
                                        FLEXRAY_MBCCSR0_ADDR16 + MbRegOffset);

    if (NULL != pSlotAssignment) {
        /* Read  the number of the current communication cycle in the CYCTR registers */
        /* copy expected cycle of the checked frame to Fr_SlotAssignment */
        (pSlotAssignment->Cycle) = ((uint8)Fr_Driver_ReadRegister(pCtrlCfg,
                                    FLEXRAY_CYCTR_ADDR16));
        /* copy slot ID of the checked frame to Fr_SlotAssignment*/
        (pSlotAssignment->SlotId) = (pTxCfg->TxFrameID);

        /* channel transmit data */
        if (((boolean)TRUE == (pTxCfg->TxChannelAEnable))
                && ((boolean)TRUE == (pTxCfg->TxChannelBEnable))) {
            /* Transmit on both A and B channels */
            pSlotAssignment->channelId = FR_CHANNEL_AB;
        }
        else if (((boolean)TRUE == (pTxCfg->TxChannelAEnable))
                 && ((boolean)FALSE == (pTxCfg->TxChannelBEnable))) {
            /* Transmit on channel A only */
            pSlotAssignment->channelId = FR_CHANNEL_A;
        }
        else if (((boolean)FALSE == (pTxCfg->TxChannelAEnable))
                 && ((boolean)TRUE == (pTxCfg->TxChannelBEnable))) {
            /* Transmit on channel B only */
            pSlotAssignment->channelId = FR_CHANNEL_B;
        }
        else {
            /* Do nothing */
        }
    }

    /* Check that MTD is configured to Tx */
    if (FLEXRAY_MBCCSR_MTD_U16 == (TempRegVal & FLEXRAY_MBCCSR_MTD_U16)) {
        /* Check whether the MB is in the sending state */
        if (FLEXRAY_MBCCSR_CMT_U16 != (TempRegVal & FLEXRAY_MBCCSR_CMT_U16)) {
            /* No transmission request is pending */
            /* Read FR_PSR2 register value */
            TempRegVal = Fr_Driver_ReadRegister(pCtrlCfg, FLEXRAY_PSR2_ADDR16);

            /* Check for transmission conflicts */
            if ((((boolean)TRUE == pTxCfg->TxChannelAEnable)
                    && (FLEXRAY_PSR2_STCA_U16 == (TempRegVal & FLEXRAY_PSR2_STCA_U16)))
                    || (((boolean)TRUE == pTxCfg->TxChannelBEnable)
                        && (FLEXRAY_PSR2_STCB_U16 == (TempRegVal & FLEXRAY_PSR2_STCB_U16)))
               ) {
                *pTxLPduStatus = FR_TRANSMITTED_CONFLICT;
            }
            else {
                *pTxLPduStatus = FR_TRANSMITTED;
            }
        }
        else {
            /* Transmission request is still pending */
            *pTxLPduStatus = FR_NOT_TRANSMITTED;
        }

        ret = (Std_ReturnType)(E_OK);    /* API call was successful */
    }
    else {
        /* HW error MB is not configured for Tx -> return E_NOT_OK */
    }

    return ret;
}

/***************************************************************************************************
* @function_name    Fr_Driver_GetGlobalTime
*
* @brief            FrIP function for the Fr_GetGlobalTime API
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[out]       pu8Cycle - Address where current cycle is stored to
* @param[out]       pu16MacroTick - Address where current macrotick is stored to
* @return           none
***************************************************************************************************/
Std_ReturnType Fr_Driver_GetGlobalTime
(
    const Fr_CtrlCfgType *pCtrlCfg,
    uint8 *pu8Cycle,
    uint16 *pu16MacroTick
)
{
    Std_ReturnType retVal = (Std_ReturnType)(E_OK);

    /* Read MTCTR and CYCTR */
    /*  contains CYCTR - cycle count 0:5 */
    /*  contains MTCTR - macrotick 0:13 */
    *pu8Cycle = (uint8)(Fr_Driver_ReadRegister(pCtrlCfg,
                        FLEXRAY_CYCTR_ADDR16) & 0x3FU) ;
    *pu16MacroTick = Fr_Driver_ReadRegister(pCtrlCfg, FLEXRAY_MTCTR_ADDR16);

    return retVal;
}

/***************************************************************************************************
* @function_name    Fr_Driver_GetNmVector
*
* @brief            FrIP function for the Fr_GetNmVector API
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[out]       pu8NmVector - Address of the variable the NmVector is stored to
* @return           ::Std_ReturnType
*                       - E_OK      API call has been successful
*                       - E_NOT_OK  API call aborted due to errors
***************************************************************************************************/
Std_ReturnType Fr_Driver_GetNmVector
(
    const Fr_CtrlCfgType *pCtrlCfg,
    uint8 *pu8NmVector,
    boolean *pbHwErr
)
{
    Std_ReturnType retVal = (Std_ReturnType)(E_NOT_OK);
    const volatile uint8 *pu8Reg = NULL;
    uint16 u16NmvLen = 0U;
    uint8 i = 0U;
    boolean bDataCons = FALSE;
    uint8 u8ReadCnt = FRLEXRAY_MAX_NMVREAD_U8;


    pu8Reg = (const volatile uint8 *)pCtrlCfg->CCHardwareConfigPtr->CCBaseAddress;
    u16NmvLen = (uint16)(Fr_Driver_ReadRegister(pCtrlCfg,
                         FLEXRAY_NMVLR_ADDR16) & FLEXRAY_NMVLR_MASK_U16);

    /* Check validity of the register value */
    if (u16NmvLen !=
            pCtrlCfg->LowLevelConfigSetPtr->gNetworkManagementVectorLength) {
        *pbHwErr = (boolean)TRUE;
        /* Use valid value */
        u16NmvLen = (uint16)(
                        pCtrlCfg->LowLevelConfigSetPtr->gNetworkManagementVectorLength);
    }
    else {
        *pbHwErr = (boolean)FALSE;
    }

    while ((boolean)(u8ReadCnt) > 0U) {
        /* assume that data are consistent. If not so, this value will be updated */
        bDataCons = (boolean)TRUE;

        /* Copy all network management vector items */
        for (i = 0; i < u16NmvLen; i++) {
            /* Check if data are consistent or need to be updated */
            if (pu8NmVector[i] != pu8Reg[FRLEXRAY_NMVR0_8BIT + i]) {
                /* Copy vector value into memory location given by reference*/
                pu8NmVector[i] = pu8Reg[FRLEXRAY_NMVR0_8BIT + i];
                /* Indicate data inconsitency => another check is required */
                bDataCons = (boolean)FALSE;
            }
        }

        u8ReadCnt--;

        if ((boolean)TRUE == bDataCons) {
            /* Data is consistent */
            retVal = (Std_ReturnType)E_OK; /* API call was successful */
            break;
        }
    }

    /* In the case of hw error update the return value */
    if ((boolean)TRUE == *pbHwErr) {
        retVal = (Std_ReturnType)(E_NOT_OK);
    }

    return retVal;
}

/***************************************************************************************************
* @function_name    Fr_Driver_GetNumOfStartupFrames
*
* @brief            FrIP function for the Fr_GetNumOfStartupFrames
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[in]        pu8NumOfStartupFrames - Num Of Startup Frames
* @return           ::Std_ReturnType
*                       - E_OK      Function call finished successfully:
*                       - E_NOT_OK  Function call aborted due to no access to the FlexRay CC
***************************************************************************************************/
Std_ReturnType Fr_Driver_GetNumOfStartupFrames
(
    const Fr_CtrlCfgType *pCtrlCfg,
    uint8 *pu8NumOfStartupFrames
)
{
    /* Set initial return value */
    Std_ReturnType  retVal = (Std_ReturnType) E_OK;

    (void) pCtrlCfg;
    /* FlexRay 2.1 Rev A compliant hardware does not suppport accumulating the number of
    startupframes. The driver shall always assume 2 startup frames available */
    *pu8NumOfStartupFrames = 2U;

    return retVal;
}

/***************************************************************************************************
* @function_name    Fr_Driver_GetChannelStatus
*
* @brief            FrIP function for the Fr_GetChannelStatus API
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[out]       pu16ChannelAStatus - Address of the variable the channel A status is stored to
* @param[out]       pu16ChannelBStatus - Address of the variable the channel B status is stored to
* @return           none
***************************************************************************************************/
void Fr_Driver_GetChannelStatus
(
    const Fr_CtrlCfgType *pCtrlCfg,
    uint16 *pu16ChannelAStatus,
    uint16 *pu16ChannelBStatus
)
{
    uint16 u16TmpRegVal = 0U; /* Temporary variable for register access */

    /* Read PSR2 register */
    u16TmpRegVal = Fr_Driver_ReadRegister(pCtrlCfg, FLEXRAY_PSR2_ADDR16);

    *pu16ChannelBStatus = (uint16)((u16TmpRegVal & FLEXRAY_PSR2_CHB_MASK_U16) >>
                                   2U);
    *pu16ChannelAStatus = (uint16)((u16TmpRegVal & FLEXRAY_PSR2_CHA_MASK_U16) <<
                                   4U);

    /* Read PSR3 register */
    u16TmpRegVal = Fr_Driver_ReadRegister(pCtrlCfg, FLEXRAY_PSR3_ADDR16);

    *pu16ChannelBStatus |= ((u16TmpRegVal & FLEXRAY_PSR3_CHB_MASK_U16) >> 8U);
    *pu16ChannelAStatus |= (u16TmpRegVal & FLEXRAY_PSR3_CHA_MASK_U16);
}

/***************************************************************************************************
* @function_name    Fr_Driver_GetClockCorrection
*
* @brief            FrIP function for the Fr_GetChannelStatus API
* @param[in]        ctrlIdx - Index of FlexRay CC
* @param[out]       ps16RateCorrection - Address of the variable the rate correction value is
*                                       stored to
* @param[out]       ps32OffsetCorrection - Address of the variable the offset correction value is
*                                         stored to
***************************************************************************************************/
Std_ReturnType Fr_Driver_GetClockCorrection
(
    const Fr_CtrlCfgType *pCtrlCfg,
    sint16 *ps16RateCorrection,
    sint32 *ps32OffsetCorrection
)
{
    uint32 u32RateOffsetCorrMax = 0U;
    sint16 s16TmpVal = 0U;
    Std_ReturnType retVal = (Std_ReturnType)E_OK;

    /* Get pRateCorrectionOut maximum magnitude from configuration */
    u32RateOffsetCorrMax = (uint32)((uint32)(
                                        pCtrlCfg->LowLevelConfigSetPtr->RegPCR14) & (uint32)FLEXRAY_PCR14_MASK_U16);
    /* Read signed extended rate correction value */
    *ps16RateCorrection = (sint16)(Fr_Driver_ReadRegister(pCtrlCfg,
                                   FLEXRAY_RTCORVR_ADDR16));
    /* Prepare absolute value */
    s16TmpVal = *ps16RateCorrection;

    if (s16TmpVal < 0) {
        s16TmpVal = (sint16)(-(s16TmpVal));
    }

    /* Check range */
    if (s16TmpVal > (sint16)u32RateOffsetCorrMax) {
        /* Not-In-Range, use saturated value */
        *ps16RateCorrection = (sint16)u32RateOffsetCorrMax;
        /* Indicate error */
        retVal = (Std_ReturnType)E_NOT_OK;
    }

    /* Get pOffsetCorrectionOut maximum magnitude from configuration */
    u32RateOffsetCorrMax = (uint32)((uint32)pCtrlCfg->LowLevelConfigSetPtr->RegPCR9
                                    & (uint32)FLEXRAY_PCR9_MASK_U16);
    /* Read offset correction value */
    s16TmpVal = (sint16)(Fr_Driver_ReadRegister(pCtrlCfg, FLEXRAY_OFCORVR_ADDR16));

    if (s16TmpVal < 0) {
        s16TmpVal = (sint16)(-(s16TmpVal));
    }

    /* Check range */
    if (s16TmpVal > (sint16)(u32RateOffsetCorrMax)) {
        /* Not-In-Range, use saturated value */
        *ps32OffsetCorrection = (sint16)u32RateOffsetCorrMax;
        /* Indicate error */
        retVal = (Std_ReturnType)E_NOT_OK;
    }
    else {
        /* Store the output value */
        *ps32OffsetCorrection = (sint32)s16TmpVal;
    }

    return retVal;
}

/***************************************************************************************************
* @function_name    Fr_Flexray_GetSyncFrameList
*
* @brief            FrIP function for the Fr_GetSyncFrameList API
* @param[in]        ctrlIdx - Index of FlexRay CC
* @param[out]       pu16ChannelAEvenList - Address of the variable the channel A even list is
*                                         stored to
* @param[out]       pu16ChannelBEvenList - Address of the variable the channel B even list is
*                                         stored to
* @param[out]       pu16ChannelAOddList - Address of the variable the channel A odd list is
*                                        stored to
* @param[out]       pu16ChannelBOddList - Address of the variable the channel B odd list is
*                                        stored to
* @return           ::Std_ReturnType
*                       - E_OK      API call has been successful
*                       - E_NOT_OK  API call aborted due to errors
***************************************************************************************************/
Std_ReturnType Fr_Driver_GetSyncFrameList
(
    const Fr_CtrlCfgType *pCtrlCfg,
    uint8 u8ListSize,
    uint16 *pu16ChannelAEvenList,
    uint16 *pu16ChannelBEvenList,
    uint16 *pu16ChannelAOddList,
    uint16 *pu16ChannelBOddList
)
{
    Std_ReturnType retVal = (Std_ReturnType)E_NOT_OK;
    uint8 i = 0U;         /* Temporary variable for copying process access */
    uint16 u16tmpSyncFrameCnt =
        0U;   /* Temporary variable for Sync Frame Counter */
    uint8 u8tmpListSizeChAEven =
        0U; /* Temporary variable for u8ListSize chA even */
    uint8 u8tmpListSizeChBEven =
        0U; /* Temporary variable for u8ListSize chB even */
    uint8 u8tmpListSizeChAOdd = 0U;  /* Temporary variable for u8ListSize chA odd */
    uint8 u8tmpListSizeChBOdd = 0U;  /* Temporary variable for u8ListSize chB odd */
    uint8 u8tmpListSizeMax = 0U;    /* Temporary variable for max u8ListSize */

    if (15U >= u8ListSize) {
        u8tmpListSizeMax = u8ListSize;
    }
    else {
        /* Maximum 15 syncframes shall be supported */
        u8tmpListSizeMax = 15U;
    }

    /* Check if both EVAL OVAL bits are set */
    if ((uint16)(FLEXRAY_SFTCCSR_EVAL_U16 | FLEXRAY_SFTCCSR_OVAL_U16) ==
            ((Fr_Driver_ReadRegister(pCtrlCfg, FLEXRAY_SFTCCSR_ADDR16))
             & (uint16)(FLEXRAY_SFTCCSR_EVAL_U16 | FLEXRAY_SFTCCSR_OVAL_U16))) {
        /* Lock even/odd cycle tables */
        Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_SFTCCSR_ADDR16,
                                (FLEXRAY_SFTCCSR_OLKT_U16 | FLEXRAY_SFTCCSR_ELKT_U16));

        /* Check Even/Odd Cycle Table Lock Status */
        if ((uint16)(FLEXRAY_SFTCCSR_ELKS_U16 | FLEXRAY_SFTCCSR_OLKS_U16) ==
                ((Fr_Driver_ReadRegister(pCtrlCfg, FLEXRAY_SFTCCSR_ADDR16))
                 & (uint16)(FLEXRAY_SFTCCSR_ELKS_U16 | (uint16)(FLEXRAY_SFTCCSR_OLKS_U16 |
                            FLEXRAY_SFTCCSR_SIDEN_U16)))) {
            /* Read Sync Frame Counter for all subset of sync table */
            u16tmpSyncFrameCnt = Fr_Driver_ReadRegister(pCtrlCfg, FLEXRAY_SFCNTR_ADDR16);
            /* Prepare list size for channel A Even Sync Frames */
            u8tmpListSizeChAEven = (uint8)((u16tmpSyncFrameCnt & 0x0F00U) >> 8U);
            /* Prepare list size for channel B Even Sync Frames */
            u8tmpListSizeChBEven = (uint8)((u16tmpSyncFrameCnt & 0xF000U) >> 12U);
            /* Prepare list size for channel A Odd Sync Frames */
            u8tmpListSizeChAOdd = (uint8)(u16tmpSyncFrameCnt & 0x000FU);
            /* Prepare list size for channel B Odd Sync Frames */
            u8tmpListSizeChBOdd = (uint8)((u16tmpSyncFrameCnt & 0x00F0U) >> 4U);

            for (i = 0U; i < u8tmpListSizeMax; i++) {
                pu16ChannelAEvenList[i] = 0U;
                pu16ChannelBEvenList[i] = 0U;
                pu16ChannelAOddList[i] = 0U;
                pu16ChannelBOddList[i] = 0U;

                if (i < u8tmpListSizeChAEven) {
                    /* Copy channel A Even Sync Frame ID Table */
                    pu16ChannelAEvenList[i] = *(Fr_Driver_GetData16Pointer((
                                                    Fr_Driver_PointerSizeType)
                                                pCtrlCfg->CCHardwareConfigPtr->CCFlexRayMemoryBaseAddress
                                                + (Fr_Driver_PointerSizeType)
                                                pCtrlCfg->CCHardwareConfigPtr->SyncFrameTableOffset) + i);
                }

                if (i < u8tmpListSizeChBEven) {
                    /* Copy channel B Even Sync Frame ID Table */
                    pu16ChannelBEvenList[i] = *(Fr_Driver_GetData16Pointer((
                                                    Fr_Driver_PointerSizeType)
                                                pCtrlCfg->CCHardwareConfigPtr->CCFlexRayMemoryBaseAddress
                                                + (Fr_Driver_PointerSizeType)pCtrlCfg->CCHardwareConfigPtr->SyncFrameTableOffset
                                                +
                                                FLEXRAY_CHANNELBEVENLIST_ADDRESSOFFSET_U16) + i);
                }

                if (i < u8tmpListSizeChAOdd) {
                    /* Copy channel A Odd Sync Frame ID Table */
                    pu16ChannelAOddList[i] = *(Fr_Driver_GetData16Pointer((
                                                   Fr_Driver_PointerSizeType)
                                               pCtrlCfg->CCHardwareConfigPtr->CCFlexRayMemoryBaseAddress
                                               + (Fr_Driver_PointerSizeType)pCtrlCfg->CCHardwareConfigPtr->SyncFrameTableOffset
                                               +
                                               FLEXRAY_CHANNELAODDLIST_ADDRESSOFFSET_U16) + i);
                }

                if (i < u8tmpListSizeChBOdd) {
                    /* Copy channel B Odd Sync Frame ID Table */
                    pu16ChannelBOddList[i] = *(Fr_Driver_GetData16Pointer((
                                                   Fr_Driver_PointerSizeType)
                                               pCtrlCfg->CCHardwareConfigPtr->CCFlexRayMemoryBaseAddress
                                               + (Fr_Driver_PointerSizeType)pCtrlCfg->CCHardwareConfigPtr->SyncFrameTableOffset
                                               +
                                               FLEXRAY_CHANNELBODDLIST_ADDRESSOFFSET_U16) + i);
                }
            }

            /* Unlock even/odd cycle table and enable generation*/
            Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_SFTCCSR_ADDR16,
                                    (uint16)(FLEXRAY_SFTCCSR_ELKT_U16 | (uint16)(FLEXRAY_SFTCCSR_OLKT_U16 |
                                             FLEXRAY_SFTCCSR_SIDEN_U16)));

            /* Check Even Cycle Table Lock Status */
            if (FLEXRAY_SFTCCSR_ELKS_U16 == (Fr_Driver_ReadRegister(pCtrlCfg,
                                             FLEXRAY_SFTCCSR_ADDR16) & FLEXRAY_SFTCCSR_ELKS_U16)) {
                /* Tables were not unlocked */
            }
            /* Check Odd Cycle Table Lock Status */
            else if (FLEXRAY_SFTCCSR_OLKS_U16 == (Fr_Driver_ReadRegister(pCtrlCfg,
                                                  FLEXRAY_SFTCCSR_ADDR16) & FLEXRAY_SFTCCSR_OLKS_U16)) {
                /* Tables were not unlocked */
            }
            else {
                /* Tables were unlocked successfully*/
                retVal = (Std_ReturnType)(E_OK);
            }
        }
        else {
            /* Tables were not locked return E_NOT_OK */
        }
    }
    else {
        /* EVAL OVAL bits are not set, table is not valid return E_NOT_OK  */
    }

    return retVal;
}

/***************************************************************************************************
* @function_name    Fr_Driver_GetWakeupRxStatus
*
* @brief            FrIP function for the Fr_GetWakeupRxStatus API
* @param[in]        ctrlIdx - Index of FlexRay CC
* @param[out]       pu8WakeupRxStatus - Address of a variable where the wakeup Rx status is stored to
* @return           none
***************************************************************************************************/
Std_ReturnType Fr_Driver_GetWakeupRxStatus
(
    const Fr_CtrlCfgType *pCtrlCfg,
    uint8 *pu8WakeupRxStatus
)
{
    Std_ReturnType retVal = (Std_ReturnType)(E_NOT_OK);
    uint16 u16TmpRegVal = 0U; /* Temporary variable for register access */

    /* Read PSR3 register */
    u16TmpRegVal = Fr_Driver_ReadRegister(pCtrlCfg, FLEXRAY_PSR3_ADDR16);

    /* Store wakeup symbol received on channel A */
    *pu8WakeupRxStatus = (uint8)((u16TmpRegVal & FLEXRAY_PSR3_WUA_U16) >> 5U);

    /* Store wakeup symbol recieved on channel B */
    *pu8WakeupRxStatus |= (uint8)((u16TmpRegVal & FLEXRAY_PSR3_WUB_U16) >> 12U);

    /* Clear both WUA and WUB flags */
    if (0U != (u16TmpRegVal & (uint16)(FLEXRAY_PSR3_WUB_U16 |
                                       FLEXRAY_PSR3_WUA_U16))) {
        Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_PSR3_ADDR16,
                                FLEXRAY_PSR3_WUB_U16 | FLEXRAY_PSR3_WUA_U16);
    }

    if (0U == (Fr_Driver_ReadRegister(pCtrlCfg,
                                      FLEXRAY_PSR3_ADDR16) & (uint16)(FLEXRAY_PSR3_WUB_U16 | FLEXRAY_PSR3_WUA_U16))) {
        retVal = (Std_ReturnType)(E_OK); /* Flags were cleared */
    }

    return retVal;
}

/***************************************************************************************************
* @function_name    Fr_Driver_CancelAbsoluteTimer
*
* @brief            FrIP function for the Fr_CancelAbsoluteTimer API
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[in]        u8TimerIdx - Index of absolute timer within the context of the FlexRay CC
* @return           none
***************************************************************************************************/
void Fr_Driver_CancelAbsoluteTimer
(
    const Fr_CtrlCfgType *pCtrlCfg,
    uint8 u8TimerIdx
)
{
    uint16 u16TmpVal = 0U;

    /* Stop timer */
    SchM_Enter_EXCLUSIVE_AREA();
    u16TmpVal = (uint16)(Fr_Driver_ReadRegister(pCtrlCfg,
                         FLEXRAY_TICCR_ADDR16) & FLEXRAY_TICCR_CONFIG_MASK_U16);

    /* Determine which timer should be stoped */
    if (0U == u8TimerIdx) {
        /* Timer 1 should be stoped */
        Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_TICCR_ADDR16,
                                (uint16)(u16TmpVal | FLEXRAY_TICCR_T1SP_U16));
    }
    else {
        /* Timer 2 should be stoped */
        Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_TICCR_ADDR16,
                                (uint16)(u16TmpVal | FLEXRAY_TICCR_T2SP_U16));
    }

    SchM_Exit_EXCLUSIVE_AREA();
}

/***************************************************************************************************
* @function_name    Fr_Driver_ReadCCConfig
*
* @brief            FrIP function for the Fr_ReadCCConfig API
* @param[in]        ctrlIdx - Index of FlexRay CC
* @return           ::Std_ReturnType
*                       - E_OK      Configuration is OK
*                       - E_NOT_OK  Error in configuration was found
***************************************************************************************************/
Std_ReturnType Fr_Driver_ReadCCConfig
(
    const Fr_CtrlCfgType *pCtrlCfg
)
{
    Std_ReturnType retVal = (Std_ReturnType)(E_OK);

    if (pCtrlCfg->LowLevelConfigSetPtr->RegPCR0 != Fr_Driver_ReadRegister(pCtrlCfg,
            FLEXRAY_PCR0_ADDR16)) {
        retVal = (Std_ReturnType)(E_NOT_OK);
    }
    else if (pCtrlCfg->LowLevelConfigSetPtr->RegPCR1 != Fr_Driver_ReadRegister(
                 pCtrlCfg,
                 FLEXRAY_PCR1_ADDR16)) {
        retVal = (Std_ReturnType)(E_NOT_OK);
    }
    else if (pCtrlCfg->LowLevelConfigSetPtr->RegPCR2 != Fr_Driver_ReadRegister(
                 pCtrlCfg,
                 FLEXRAY_PCR2_ADDR16)) {
        retVal = (Std_ReturnType)(E_NOT_OK);
    }
    else if (pCtrlCfg->LowLevelConfigSetPtr->RegPCR3 != Fr_Driver_ReadRegister(
                 pCtrlCfg,
                 FLEXRAY_PCR3_ADDR16)) {
        retVal = (Std_ReturnType)(E_NOT_OK);
    }
    else if (pCtrlCfg->LowLevelConfigSetPtr->RegPCR4 != Fr_Driver_ReadRegister(
                 pCtrlCfg,
                 FLEXRAY_PCR4_ADDR16)) {
        retVal = (Std_ReturnType)(E_NOT_OK);
    }
    else if (pCtrlCfg->LowLevelConfigSetPtr->RegPCR5 != Fr_Driver_ReadRegister(
                 pCtrlCfg,
                 FLEXRAY_PCR5_ADDR16)) {
        retVal = (Std_ReturnType)(E_NOT_OK);
    }
    else if (pCtrlCfg->LowLevelConfigSetPtr->RegPCR6 != Fr_Driver_ReadRegister(
                 pCtrlCfg,
                 FLEXRAY_PCR6_ADDR16)) {
        retVal = (Std_ReturnType)(E_NOT_OK);
    }
    else if (pCtrlCfg->LowLevelConfigSetPtr->RegPCR7 != Fr_Driver_ReadRegister(
                 pCtrlCfg,
                 FLEXRAY_PCR7_ADDR16)) {
        retVal = (Std_ReturnType)(E_NOT_OK);
    }
    else if (pCtrlCfg->LowLevelConfigSetPtr->RegPCR8 != Fr_Driver_ReadRegister(
                 pCtrlCfg,
                 FLEXRAY_PCR8_ADDR16)) {
        retVal = (Std_ReturnType)(E_NOT_OK);
    }
    else if (pCtrlCfg->LowLevelConfigSetPtr->RegPCR9 != Fr_Driver_ReadRegister(
                 pCtrlCfg,
                 FLEXRAY_PCR9_ADDR16)) {
        retVal = (Std_ReturnType)(E_NOT_OK);
    }
    else if (pCtrlCfg->LowLevelConfigSetPtr->RegPCR10 != Fr_Driver_ReadRegister(
                 pCtrlCfg,
                 FLEXRAY_PCR10_ADDR16)) {
        retVal = (Std_ReturnType)(E_NOT_OK);
    }
    else if (pCtrlCfg->LowLevelConfigSetPtr->RegPCR11 != Fr_Driver_ReadRegister(
                 pCtrlCfg,
                 FLEXRAY_PCR11_ADDR16)) {
        retVal = (Std_ReturnType)(E_NOT_OK);
    }
    else if (pCtrlCfg->LowLevelConfigSetPtr->RegPCR12 != Fr_Driver_ReadRegister(
                 pCtrlCfg,
                 FLEXRAY_PCR12_ADDR16)) {
        retVal = (Std_ReturnType)(E_NOT_OK);
    }
    else if (pCtrlCfg->LowLevelConfigSetPtr->RegPCR13 != Fr_Driver_ReadRegister(
                 pCtrlCfg,
                 FLEXRAY_PCR13_ADDR16)) {
        retVal = (Std_ReturnType)(E_NOT_OK);
    }
    else if (pCtrlCfg->LowLevelConfigSetPtr->RegPCR14 != Fr_Driver_ReadRegister(
                 pCtrlCfg,
                 FLEXRAY_PCR14_ADDR16)) {
        retVal = (Std_ReturnType)(E_NOT_OK);
    }
    else if (pCtrlCfg->LowLevelConfigSetPtr->RegPCR15 != Fr_Driver_ReadRegister(
                 pCtrlCfg,
                 FLEXRAY_PCR15_ADDR16)) {
        retVal = (Std_ReturnType)(E_NOT_OK);
    }
    else if (pCtrlCfg->LowLevelConfigSetPtr->RegPCR16 != Fr_Driver_ReadRegister(
                 pCtrlCfg,
                 FLEXRAY_PCR16_ADDR16)) {
        retVal = (Std_ReturnType)(E_NOT_OK);
    }
    else if (pCtrlCfg->LowLevelConfigSetPtr->RegPCR17 != Fr_Driver_ReadRegister(
                 pCtrlCfg,
                 FLEXRAY_PCR17_ADDR16)) {
        retVal = (Std_ReturnType)(E_NOT_OK);
    }
    else if (pCtrlCfg->LowLevelConfigSetPtr->RegPCR18 != Fr_Driver_ReadRegister(
                 pCtrlCfg,
                 FLEXRAY_PCR18_ADDR16)) {
        retVal = (Std_ReturnType)(E_NOT_OK);
    }
    else if (pCtrlCfg->LowLevelConfigSetPtr->RegPCR19 != Fr_Driver_ReadRegister(
                 pCtrlCfg,
                 FLEXRAY_PCR19_ADDR16)) {
        retVal = (Std_ReturnType)(E_NOT_OK);
    }
    else if (pCtrlCfg->LowLevelConfigSetPtr->RegPCR20 != Fr_Driver_ReadRegister(
                 pCtrlCfg,
                 FLEXRAY_PCR20_ADDR16)) {
        retVal = (Std_ReturnType)(E_NOT_OK);
    }
    else if (pCtrlCfg->LowLevelConfigSetPtr->RegPCR21 != Fr_Driver_ReadRegister(
                 pCtrlCfg,
                 FLEXRAY_PCR21_ADDR16)) {
        retVal = (Std_ReturnType)(E_NOT_OK);
    }
    else if (pCtrlCfg->LowLevelConfigSetPtr->RegPCR22 != Fr_Driver_ReadRegister(
                 pCtrlCfg,
                 FLEXRAY_PCR22_ADDR16)) {
        retVal = (Std_ReturnType)(E_NOT_OK);
    }
    else if (pCtrlCfg->LowLevelConfigSetPtr->RegPCR23 != Fr_Driver_ReadRegister(
                 pCtrlCfg,
                 FLEXRAY_PCR23_ADDR16)) {
        retVal = (Std_ReturnType)(E_NOT_OK);
    }
    else if (pCtrlCfg->LowLevelConfigSetPtr->RegPCR24 != Fr_Driver_ReadRegister(
                 pCtrlCfg,
                 FLEXRAY_PCR24_ADDR16)) {
        retVal = (Std_ReturnType)(E_NOT_OK);
    }
    else if (pCtrlCfg->LowLevelConfigSetPtr->RegPCR25 != Fr_Driver_ReadRegister(
                 pCtrlCfg,
                 FLEXRAY_PCR25_ADDR16)) {
        retVal = (Std_ReturnType)(E_NOT_OK);
    }
    else if (pCtrlCfg->LowLevelConfigSetPtr->RegPCR26 != Fr_Driver_ReadRegister(
                 pCtrlCfg,
                 FLEXRAY_PCR26_ADDR16)) {
        retVal = (Std_ReturnType)(E_NOT_OK);
    }
    else if (pCtrlCfg->LowLevelConfigSetPtr->RegPCR27 != Fr_Driver_ReadRegister(
                 pCtrlCfg,
                 FLEXRAY_PCR27_ADDR16)) {
        retVal = (Std_ReturnType)(E_NOT_OK);
    }
    else if (pCtrlCfg->LowLevelConfigSetPtr->RegPCR28 != Fr_Driver_ReadRegister(
                 pCtrlCfg,
                 FLEXRAY_PCR28_ADDR16)) {
        retVal = (Std_ReturnType)(E_NOT_OK);
    }
    else if (pCtrlCfg->LowLevelConfigSetPtr->RegPCR29 != Fr_Driver_ReadRegister(
                 pCtrlCfg,
                 FLEXRAY_PCR29_ADDR16)) {
        retVal = (Std_ReturnType)(E_NOT_OK);
    }
    else if (pCtrlCfg->LowLevelConfigSetPtr->RegPCR30 != Fr_Driver_ReadRegister(
                 pCtrlCfg,
                 FLEXRAY_PCR30_ADDR16)) {
        retVal = (Std_ReturnType)(E_NOT_OK);
    }
    else {
        retVal = (Std_ReturnType)(E_OK);
    }

    return retVal;
}

#if (FR_PREPARE_LPDU_SUPPORT == STD_ON)

/***************************************************************************************************
* @function_name    Fr_Driver_PrepareLPdu
*
* @brief            FrIP function for the Fr_PrepareLPdu API
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[in]        LPduIdx - Index of LPdu to be prepared
* @return           ::Std_ReturnType
*                       - E_OK      Function call finished successfully:
*                       - E_NOT_OK  Function call aborted due to no access to the FlexRay CC
***************************************************************************************************/
Std_ReturnType Fr_Driver_PrepareLPdu
(
    const Fr_CtrlCfgType *pCtrlCfg,
    uint16 LPduIdx
)
{
    /* Set initial return value */
    Std_ReturnType ret = (Std_ReturnType)(E_NOT_OK);
    uint16 TempMBNum = 0U;  /* Temporary variable for the MB index */

    /* Store the initial MB index */
    TempMBNum =
        pCtrlCfg->BufferConfigSetPtr->LPduInfoPtr[LPduIdx].MessageBufferNumber;

    /* Determine the type of buffer */
    switch (pCtrlCfg->BufferConfigSetPtr->LPduInfoPtr[LPduIdx].BufferType) {
        /* Transmit MB configuration */
        case FR_TRANSMIT_BUFFER:
            ret = Fr_Driver_PrepareTxBuffer(pCtrlCfg, LPduIdx, TempMBNum);
            break;

        /* Receive MB configuration */
        case FR_RECEIVE_BUFFER:
            ret = Fr_Driver_PrepareRxBuffer(pCtrlCfg, LPduIdx, TempMBNum);
            break;

        case FR_FIFOA_BUFFER: /* Intentional fall-through */

        case FR_FIFOB_BUFFER: /* Intentional fall-through */

        default: {
            break;
        }
    }

    if ((Std_ReturnType)(E_OK) == ret) { /* Continue only on success */

        /* [SWS_Fr_00440],[SWS_Fr_00441]: Only if FR_PREPARE_LPDU_SUPPORT is enabled: */
        /* Store information which LPdu is currently configured for given physical
           resource (individual MB) */
        Fr_VirtualResourceAllocation[pCtrlCfg->CtrlIdx][TempMBNum] = LPduIdx;
    }   /* E_OK == ret */

    return ret;
}
#endif /* FR_PREPARE_LPDU_SUPPORT == STD_ON */

#if (FR_RECONFIG_LPDU_SUPPORT == STD_ON)

/***************************************************************************************************
* @function_name    Fr_Driver_ReconfigLPdu
*
* @brief            FrIP function for the Fr_ReconfigLPdu API
* @param[in]        ctrlIdx - Index of FlexRay CC
* @param[in]        u16LPduIdx - Index of LPdu to be reconfigured
* @param[in]        u16FrameId - Frame ID for reconfiguration
* @param[in]        u8CycleRepetition - Repetition value for cycle filter mechanism
* @param[in]        cycleOffste - Offste value for cycle filter mechanism
* @param[in]        u8PayloadLength - payload for data to be reconfigured
* @param[in]        u16HeaderCRC - hedaer CRC value
* @return           ::Std_ReturnType
*                       - E_OK      Function call finished successfully:
*                       - E_NOT_OK  Function call aborted due to no access to the FlexRay CC
***************************************************************************************************/
Std_ReturnType Fr_Driver_ReconfigLPdu
(
    const Fr_CtrlCfgType *pCtrlCfg,
    uint16 LPduIdx,
    uint16 FrameId,
    Fr_ChannelType chnlIdx,
    uint8 CycleRepetition,
    uint8 CycleOffset,
    uint8 PayloadLength,
    uint16 HeaderCRC
)
{
    Std_ReturnType ret = (Std_ReturnType)(E_NOT_OK);
    uint16 TempMBIdx = 0U;
    uint16 MbRegOffset = 0U;
    uint16 TmpRegVal = 0U;
    uint32 TempHeaderVal = 0U;
    uint32 *pHeaderMB = NULL;

    /* Get current MB Idx */
    TempMBIdx =
        pCtrlCfg->BufferConfigSetPtr->LPduInfoPtr[LPduIdx].MessageBufferNumber;
    /* Offset from FR base address to the MB registers */
    MbRegOffset = TempMBIdx * FLEXRAY_MB_REG_OFFSET;

    if (FLEXRAY_MBCCSR_EDS_U16 == (Fr_Driver_ReadRegister(pCtrlCfg,
                                   FLEXRAY_MBCCSR0_ADDR16 + MbRegOffset) & FLEXRAY_MBCCSR_EDS_U16)) {
        Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_MBCCSR0_ADDR16 + MbRegOffset,
                                FLEXRAY_MBCCSR_EDT_U16);
    }

    /* Check if buffer has been successfully disabled */
    if (FLEXRAY_MBCCSR_EDS_U16 != (Fr_Driver_ReadRegister(pCtrlCfg,
                                   FLEXRAY_MBCCSR0_ADDR16 + MbRegOffset) & FLEXRAY_MBCCSR_EDS_U16)) {
        /* Yes */
        /* Read FR_MBCCFRx register */
        TmpRegVal = Fr_Driver_ReadRegister(pCtrlCfg,
                                           FLEXRAY_MBCCFR0_ADDR16 + MbRegOffset);

        TmpRegVal &= 0x8000U;

        if (FR_CHANNEL_AB == chnlIdx) {
            /* For static segment configure both channels */
            TmpRegVal |= (uint16)(FLEXRAY_MBCCFR_CHA_U16 | FLEXRAY_MBCCFR_CHB_U16);
        }
        else if (FR_CHANNEL_A == chnlIdx) {
            TmpRegVal |= FLEXRAY_MBCCFR_CHA_U16;
            /* Clear CHB bit */
            TmpRegVal &= ~FLEXRAY_MBCCFR_CHB_U16;
        }
        else {
            TmpRegVal |= FLEXRAY_MBCCFR_CHB_U16;
            /* Clear CHA bit */
            TmpRegVal &= ~FLEXRAY_MBCCFR_CHA_U16;
        }

        TmpRegVal |= (uint16)((((uint16)CycleRepetition & 0x3FU) - 1U) << 6U);

        TmpRegVal |= (uint16)(CycleOffset) & 0x003FU;

        TmpRegVal |= FLEXRAY_MBCCFR_CCFE_U16; /* Enable Cycle counter filtering */

        /* Reconfigure MBCCFRx register */
        Fr_Driver_WriteRegister(pCtrlCfg, (FLEXRAY_MBCCFR0_ADDR16 + MbRegOffset),
                                TmpRegVal);

        /* Reconfigure MBFIDRx register */
        Fr_Driver_WriteRegister(pCtrlCfg, (FLEXRAY_MBFIDR0_ADDR16 + MbRegOffset),
                                (uint16)(FrameId & FLEXRAY_MBFIDR_FID_MASK_U16));

        /* Calculate the message buffer header:
           FR_memory_base_address + (Configuring_MB_number * MB_header_length) */
        if ((TempMBIdx % 2U) == 0U) {
            pHeaderMB = Fr_Driver_GetData32Pointer(((Fr_Driver_PointerSizeType)
                                                    pCtrlCfg->CCHardwareConfigPtr->CCFlexRayMemoryBaseAddress
                                                    + (TempMBIdx * FLEXRAY_HEADER_SIZE_U8)));
            TempHeaderVal = (uint16)(pHeaderMB[0U] & 0xF8000000U);
            TempHeaderVal |= ((FrameId & 0x07FFU) << 16);
            arch_invalidate_cache_range_pri(pHeaderMB, FLEXRAY_HEADER_CACHE_SIZE_U8);
            /* Configure Frame ID */
            pHeaderMB[0U] = TempHeaderVal;

            /* Configure Payload Length */
            pHeaderMB[0U] |= (uint16)(((((uint16)PayloadLength + 1U) & 0xFEU) >> 1) &
                                      FLEXRAY_FRAMEHEADER1_PLDLEN_MASK_U16);

            /* Configure Header CRC */
            pHeaderMB[1U] &= 0xF800FFFFU;    /*Clear CRC*/
            pHeaderMB[1U] |= (uint32)((HeaderCRC & 0x07FFU) << 16);
        }
        else {
            pHeaderMB = Fr_Driver_GetData32Pointer(((Fr_Driver_PointerSizeType)
                                                    pCtrlCfg->CCHardwareConfigPtr->CCFlexRayMemoryBaseAddress
                                                    + (TempMBIdx * FLEXRAY_HEADER_SIZE_U8 - 2)));

            TempHeaderVal = (uint16)(pHeaderMB[0U] & 0x0000F800U);
            TempHeaderVal |= (FrameId & 0x07FFU);
            arch_invalidate_cache_range_pri(pHeaderMB, FLEXRAY_HEADER_CACHE_SIZE_U8);

            /* Configure Frame ID */
            pHeaderMB[0U] &= 0xFFFF0000U;
            pHeaderMB[0U] |= TempHeaderVal;

            /* Configure Payload Length */
            pHeaderMB[1U] &= 0XFF80F800U;    /*Clear length and CRC*/
            pHeaderMB[1U] |= (uint32)((((((uint16)PayloadLength + 1U) & 0xFEU) >> 1) &
                                       FLEXRAY_FRAMEHEADER1_PLDLEN_MASK_U16) << 16);

            /* Configure Header CRC */
            pHeaderMB[1U] |= (uint32)((HeaderCRC & 0x07FFU));
        }

        arch_clean_cache_range_pri(pHeaderMB, FLEXRAY_HEADER_CACHE_SIZE_U8);

        /* Enable MB */
        Fr_Driver_WriteRegister(pCtrlCfg,
                                (uint16)(FLEXRAY_MBCCSR0_ADDR16 + MbRegOffset),
                                FLEXRAY_MBCCSR_EDT_U16);

        /* Check MB is enabled*/
        if (FLEXRAY_MBCCSR_EDS_U16 == (Fr_Driver_ReadRegister(pCtrlCfg,
                                       FLEXRAY_MBCCSR0_ADDR16 + MbRegOffset) & FLEXRAY_MBCCSR_EDS_U16)) {
            /*Yes*/
            ret = (Std_ReturnType)(E_OK);
        }
    }
    else {
        /* Buffer was not disabled return E_NOT_OK */
    }

    return ret;
}
#endif /* FR_RECONFIG_LPDU_SUPPORT */

#if (FR_DISABLE_LPDU_SUPPORT == STD_ON)
/***************************************************************************************************
* @function_name    Fr_Driver_DisableLPdu
*
* @brief            FrIP function for the Fr_Flexray_DisableLPdu
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[in]        LPduIdx - Index of LPdu to be reconfigured
* @return           ::Std_ReturnType
*                       - E_OK      Function call finished successfully:
*                       - E_NOT_OK  Function call aborted due to no access to the FlexRay CC
***************************************************************************************************/
Std_ReturnType Fr_Driver_DisableLPdu
(
    const Fr_CtrlCfgType *pCtrlCfg,
    uint16 LPduIdx
)
{
    /* Set initial return value */
    Std_ReturnType ret = (Std_ReturnType)(E_NOT_OK);
    uint16 TempMBIdx = 0U;
    uint16 MbRegOffset = 0U;
    uint16 PocStatus = 0U;

    /* Workaround for Errata e50119: Do not disable Message Buffer, while FlexRay is in STARTUP protocol State */
    PocStatus = Fr_Driver_ReadRegister(pCtrlCfg, FLEXRAY_PSR0_ADDR16);

    if ((PocStatus & FLEXRAY_PSR0_PROTSTATE_MASK_U16) !=
            FLEXRAY_PSR0_PROTSTATE_STARTUP_U16) {
        /* Get current MB Idx */
        TempMBIdx =
            pCtrlCfg->BufferConfigSetPtr->LPduInfoPtr[LPduIdx].MessageBufferNumber;
        /* Offset from FR base address to the MB registers */
        MbRegOffset = TempMBIdx * FLEXRAY_MB_REG_OFFSET;

        if (FLEXRAY_MBCCSR_EDS_U16 == (Fr_Driver_ReadRegister(pCtrlCfg,
                                       FLEXRAY_MBCCSR0_ADDR16 + MbRegOffset) & FLEXRAY_MBCCSR_EDS_U16)) {
            Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_MBCCSR0_ADDR16 + MbRegOffset,
                                    FLEXRAY_MBCCSR_EDT_U16);
        }

        /* Check if buffer has been successfully disabled */
        if (FLEXRAY_MBCCSR_EDS_U16 != (Fr_Driver_ReadRegister(pCtrlCfg,
                                       FLEXRAY_MBCCSR0_ADDR16 + MbRegOffset) & FLEXRAY_MBCCSR_EDS_U16)) {
            ret = (Std_ReturnType)(E_OK);
        }
        else {
            /* Buffer was not disabled return E_NOT_OK */
        }
    }

    return ret;
}
#endif /* FR_DISABLE_LPDU_SUPPORT */

/***************************************************************************************************
* @function_name    Fr_Driver_EnableAbsTimerIRQ
*
* @brief            FrIP function for the Fr_EnableAbsoluteTimerIRQ
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[in]        TimerIdx - Index of absolute timer within the context of the FlexRay CC
* @return           none
***************************************************************************************************/
void Fr_Driver_EnableAbsTimerIRQ
(
    const Fr_CtrlCfgType *pCtrlCfg,
    uint8 TimerIdx
)
{
    uint16 TempVal = 0U;
    SchM_Enter_EXCLUSIVE_AREA();
    TempVal = Fr_Driver_ReadRegister(pCtrlCfg, FLEXRAY_PIER0_ADDR16);

    /* Determine for which timer should be enabled the interrupt */
    if (0U == TimerIdx) {
        /* Timer 1 Set control flag */
        Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_PIER0_ADDR16,
                                (uint16)(TempVal | FLEXRAY_PIER0_TI1_IE_U16));

    }
    else {
        /* Timer 2 Set control flag*/
        Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_PIER0_ADDR16,
                                (uint16)(TempVal | FLEXRAY_PIER0_TI2_IE_U16));
    }

    /* Load FLEXRAY_GIFER_ADDR16 reg. */
    TempVal = (uint16)(Fr_Driver_ReadRegister(pCtrlCfg,
                       FLEXRAY_GIFER_ADDR16) & FLEXRAY_GIFER_CTRL_FLAGS_MASK_U16);
    /* Set control flag */
    Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_GIFER_ADDR16,
                            (uint16)(TempVal |  FLEXRAY_GIFER_PRIE_U16));
    SchM_Exit_EXCLUSIVE_AREA();
}

/***************************************************************************************************
* @function_name    Fr_Driver_AckAbsTimerIRQ
*
* @brief            FrIP function for the Fr_AckAbsoluteTimerIRQ API
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[in]        TimerIdx Index of absolute timer within the context of the FlexRay CC
* @return           none
***************************************************************************************************/
void Fr_Driver_AckAbsTimerIRQ
(
    const Fr_CtrlCfgType *pCtrlCfg,
    uint8 TimerIdx
)
{
    /* Determine for which timer should be cleared the interrupt flag */
    if (0U == TimerIdx) {
        /* Timer 1 Clear flag*/
        Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_PIFR0_ADDR16,
                                FLEXRAY_PIFR0_TI1_IF_U16);
    }
    else {
        /* Timer 2 Clear flag*/
        Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_PIFR0_ADDR16,
                                FLEXRAY_PIFR0_TI2_IF_U16);
    }
}

/***************************************************************************************************
* @function_name    Fr_Driver_DisableAbsTimerIRQ
*
* @brief            FrIP function for the Fr_DisableAbsoluteTimerIRQ API
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[in]        TimerIdx - Index of absolute timer within the context of the FlexRay CC
* @return           none
***************************************************************************************************/
void Fr_Driver_DisableAbsTimerIRQ
(
    const Fr_CtrlCfgType *pCtrlCfg,
    uint8 TimerIdx
)
{
    uint16 TempVal = 0U;

    /* Determine for which timer should be disabled the interrupt line */
    SchM_Enter_EXCLUSIVE_AREA();
    TempVal = Fr_Driver_ReadRegister(pCtrlCfg, FLEXRAY_PIER0_ADDR16);

    if (0U == TimerIdx) {
        /* Timer 1 should be disabled, Clear control flag */
        TempVal &= ~FLEXRAY_PIER0_TI1_IE_U16;
    }
    else {
        /* Timer 2 should be disabled, Clear control flag*/
        TempVal &= ~FLEXRAY_PIER0_TI2_IE_U16;
    }

    Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_PIER0_ADDR16, TempVal);
    SchM_Exit_EXCLUSIVE_AREA();
}

/***************************************************************************************************
* @function_name    Fr_Driver_GetAbsTimerIRQStatus
*
* @brief            FrIP function for the Fr_GetAbsoluteTimerIRQStatus
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[in]        TimerIdx - Index of absolute timer within the context of the FlexRay CC
* @return           ::boolean
*                     - TRUE - Interrupt is pending
*                     - FALSE - Interrupt is not pending
***************************************************************************************************/
boolean Fr_Driver_GetAbsTimerIRQStatus
(
    const Fr_CtrlCfgType *pCtrlCfg,
    uint8 TimerIdx
)
{
    uint16 TempVal = 0U;
    boolean bStatusIRQ = (boolean)FALSE;

    TempVal = Fr_Driver_ReadRegister(pCtrlCfg, FLEXRAY_PIFR0_ADDR16);

    /* Determine for which timer should be checked the interrupt flag */
    if (0U == TimerIdx) {
        /* Timer 1 */
        if (0U != (TempVal & FLEXRAY_PIFR0_TI1_IF_U16)) {
            /* Interrupt flag has been set */
            bStatusIRQ = (boolean)TRUE;
        }
    }
    else {
        /* Timer 2 */
        if (0U != (TempVal & FLEXRAY_PIFR0_TI2_IF_U16)) {
            /* Interrupt flag has been set */
            bStatusIRQ = (boolean)TRUE;
        }
    }

    return bStatusIRQ;
}

/***************************************************************************************************
* @function_name    Fr_Driver_CheckCCAccess
*
* @brief            IP function for checking access to the CC
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[in]        bCCEnabled - Indicates whether CC module should be enabled or not
* @return           ::Std_ReturnType
*                       - E_OK      Function call finished successfully:
*                       - E_NOT_OK  Function call aborted due to no access to the FlexRay CC
***************************************************************************************************/
Std_ReturnType Fr_Driver_CheckCCAccess
(
    const Fr_CtrlCfgType *pCtrlCfg,
    const boolean bCCEnabled
)
{
    uint16 RegVal = 0U;   /* Temporary variable for register access */
    /* Set initial return value */
    Std_ReturnType ret = (Std_ReturnType)(E_OK);

    if ((boolean)TRUE == bCCEnabled) {
        /* Check if FlexRay module is enabled */
        RegVal = Fr_Driver_ReadRegister(pCtrlCfg, FLEXRAY_MCR_ADDR16);

        if ((RegVal & FLEXRAY_MCR_MEN_U16) != FLEXRAY_MCR_MEN_U16) {
            /* FlexRay module is not enabled, report error */
            ret = (Std_ReturnType)(E_NOT_OK);
        }
    }

    return ret;
}

/***************************************************************************************************
* @function_name    Fr_Driver_InterruptEnable
*
* @brief            Enable interruption
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[in]        GIE - Interrupt flag bit
* @return           void
***************************************************************************************************/
void Fr_Driver_InterruptEnable(
    const Fr_CtrlCfgType *pCtrlCfg,
    uint16 GIE
)
{
    uint16 regVal = 0;
    regVal = Fr_Driver_ReadRegister(pCtrlCfg, FLEXRAY_GIFER_ADDR16);
    regVal |= GIE;
    /* Enable interruption, interrupt setting to 1 */
    Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_GIFER_ADDR16,
                            (regVal & FLEXRAY_GIFER_CTRL_FLAGS_MASK_U16));
}

/***************************************************************************************************
* @function_name    Fr_Driver_CheckCCAccess
*
* @brief            Disable interruption
* @param[in]        pCtrlCfg - Index of FlexRay CC
* @param[in]        GIE - Interrupt flag bit
* @return           void
***************************************************************************************************/
void Fr_Driver_InterruptDisable(
    const Fr_CtrlCfgType *pCtrlCfg,
    uint16 GIE
)
{
    uint16 regVal = 0;
    regVal = Fr_Driver_ReadRegister(pCtrlCfg, FLEXRAY_GIFER_ADDR16);
    regVal &= ~GIE;
    /* Disable interruption, interrupt setting to 0 */
    Fr_Driver_WriteRegister(pCtrlCfg, FLEXRAY_GIFER_ADDR16,
                            (regVal & FLEXRAY_GIFER_CTRL_FLAGS_MASK_U16));
}

