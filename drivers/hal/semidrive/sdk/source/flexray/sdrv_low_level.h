/**
 * @file low_level.h
 *
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef LOW_LEVEL_H_
#define LOW_LEVEL_H_

/* current file used for SSDK/MCAL */
#define LOW_LEVEL_PLATFORM  SSDK

#if (LOW_LEVEL_PLATFORM == SSDK)

#include "irq.h"
#include "reg.h"
#include "armv7-r/cache.h"
#include <udelay/udelay.h>

#ifndef TRUE
#define TRUE (1u)
#endif
#ifndef FALSE
#define FALSE (0u)
#endif

#define CPU_TYPE_8          8u
#define CPU_TYPE_16         16u
#define CPU_TYPE_32         32u
#define CPU_TYPE_64         64u

#define MSB_FIRST           0u
#define LSB_FIRST           1u

#define HIGH_BYTE_FIRST     0u
#define LOW_BYTE_FIRST      1u
#define CPU_BIT_ORDER       (LSB_FIRST)
#define CPU_BYTE_ORDER      (LOW_BYTE_FIRST)

#define CPU_TYPE            CPU_TYPE_32

#if (CPU_TYPE == CPU_TYPE_64)
typedef unsigned char       boolean;
typedef unsigned char       uint8;
typedef unsigned short      uint16;
typedef unsigned int        uint32;
typedef unsigned long long  uint64;
typedef signed char         sint8;
typedef signed short        sint16;
typedef signed int          sint32;
typedef signed long long    sint64;
typedef unsigned int        uint8_least;
typedef unsigned int        uint16_least;
typedef unsigned int        uint32_least;
typedef signed int          sint8_least;
typedef signed int          sint16_least;
typedef signed int          sint32_least;

typedef float               float32;
typedef double              float64;

#elif (CPU_TYPE == CPU_TYPE_32)
typedef unsigned char       boolean;
typedef unsigned char       uint8;
typedef unsigned short      uint16;
typedef unsigned long       uint32;
typedef unsigned long long  uint64;
typedef signed char         sint8;
typedef signed short        sint16;
typedef signed long         sint32;
typedef signed long long    sint64;
typedef unsigned long       uint8_least;
typedef unsigned long       uint16_least;
typedef unsigned long       uint32_least;
typedef signed long         sint8_least;
typedef signed long         sint16_least;
typedef signed long         sint32_least;

typedef float               float32;
typedef double              float64;

#elif (CPU_TYPE == CPU_TYPE_16)
#error "This SSDK release has no CPU_TYPE_16 support."
#endif

#define E_OK                0x00u
#define E_NOT_OK            0x01u
typedef uint8 Std_ReturnType;

#define SchM_Enter_EXCLUSIVE_AREA() IRQ_SAVE
#define SchM_Exit_EXCLUSIVE_AREA()  IRQ_RESTORE


static inline void arch_clean_cache_range_pri(const void *start, uint32 len)
{
    arch_clean_invalidate_cache_range((uint32)start, len);
}

static inline void arch_invalidate_cache_range_pri(const void *start,
        uint32 len)
{
    arch_invalidate_cache_range((addr_t)start, len);
}

#elif (LOW_LEVEL_PLATFORM == MCAL)

#include "types.h"
#include "SchM_Fr.h"
#include "RegHelper.h"
#include "arch.h"

#if defined(AUTOSAR_OS_NOT_USED)
#define SchM_Enter_EXCLUSIVE_AREA() SchM_Enter_EXCLUSIVE_AREA();
#define SchM_Exit_EXCLUSIVE_AREA()  SchM_Exit_EXCLUSIVE_AREA();
#else
#define SchM_Enter_EXCLUSIVE_AREA() SuspendAllInterrupts();
#define SchM_Exit_EXCLUSIVE_AREA()  ResumeAllInterrupts();
#endif

static inline void arch_clean_cache_range_pri(const void *start, uint32 len)
{
    arch_clean_invalidate_cache_range(start, len);
}

static inline void arch_invalidate_cache_range_pri(const void *start,
        uint32 len)
{
    arch_invalidate_cache_range(start, len);
}

#else
#define SchM_Enter_EXCLUSIVE_AREA()
#define SchM_Exit_EXCLUSIVE_AREA()

#endif

#endif /* LOW_LEVEL_H_ */