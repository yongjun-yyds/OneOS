/**
 * @file sdrv_flexray.c
 * @brief SemiDrive Flexray driver source file.
 *
 * @Copyright (c) 2021  Semidrive Semiconductor.
 * @All rights reserved.
 *
 **/

#include "debug.h"
#include "sdrv_flexray.h"
#include "regs_base.h"

/***************************************************************************************************
* @function_name    sdrv_flexray_parameter_check
*
* @brief            Verify whether the parameters used are correct
* @param[in]        sdrv flexray handle
* @return           SDRV_STATUS_OK or error code.
***************************************************************************************************/
static status_t sdrv_flexray_parameter_check(sdrv_flexray_t *handle)
{
    status_t ret = SDRV_STATUS_FAIL;

    if (FR_INIT != handle->status) { /* Check the initialization status */
        /* The Fr was not initialized successfully prior to this API function call */
        ssdk_printf(SSDK_ERR, "\r\n sdrv_flexray_init is not called\r\n");
        ret = SDRV_FLEXRAY_INIT_FAILED;
    }
    else if ((APB_FLEXRAY1_BASE !=
              handle->internal_cfg->Fr_pController->CCHardwareConfigPtr->CCBaseAddress)
             && (APB_FLEXRAY2_BASE !=
                 handle->internal_cfg->Fr_pController->CCHardwareConfigPtr->CCBaseAddress)) {
        /* An attempt to configure unsupported CC, APB base has an invalid value */
        ssdk_printf(SSDK_ERR, "\r\n APB base adress is error\r\n");
        ret = SDRV_FLEXRAY_INV_BASE_ADDR;
    }
    else if (NULL == handle->internal_cfg) {
        ssdk_printf(SSDK_ERR, "\r\n controllers has no static configuration code\r\n");
        ret = SDRV_FLEXRAY_INIT_FAILED;
    }
    else if (NULL == handle->internal_cfg->Fr_pController) {
        ssdk_printf(SSDK_ERR, "\r\n controllers has no static configuration code\r\n");
        ret = SDRV_FLEXRAY_PARAM_CONFIG;
    }
    else {
        ret = SDRV_STATUS_OK;
    }

    return ret;
}

/***************************************************************************************************
* @function_name    sdrv_flexray_init
*
* @brief            Controller initialization function
* @param[in]        sdrv flexray handle
* @return           SDRV_STATUS_OK or error code.
***************************************************************************************************/
status_t sdrv_flexray_init(sdrv_flexray_t *handle)
{
    status_t ret = SDRV_STATUS_FAIL;

    /* Check state of flexray_edriver_states */
    if (FR_UNINIT == handle->status) {
        if (NULL == handle->internal_cfg) {
            /* If config_ptr equals NULL, FR_E_PARAM_POINTER shall be reported */
            ssdk_printf(SSDK_ERR, "\r\n internal_cfg pointer is null\r\n");
        }
        else {
            /* Successful initialization done store for other API functions
               to check for proper module initialization */
            handle->status = FR_INIT;
            ret = SDRV_STATUS_OK;
        }
    }
    else {
        ssdk_printf(SSDK_ERR, "\r\n sdrv_flexray_init is called repeatedly\r\n");
    }

    return ret;
}

/***************************************************************************************************
* @function_name    sdrv_flexray_controller_init
*
* @brief            CC configuration
* @param[in]        sdrv flexray handle
* @return           SDRV_STATUS_OK or error code.
***************************************************************************************************/
status_t sdrv_flexray_controller_init(sdrv_flexray_t *handle)
{
    status_t ret = SDRV_STATUS_FAIL;
    const sdrv_flexray_ctrl_cfg_type *ctrl_cfg = NULL;

    ret = sdrv_flexray_parameter_check(handle);

    if (SDRV_STATUS_OK == ret) {

        if ((boolean)TRUE ==
                handle->internal_cfg->Fr_pController->CCHardwareConfigPtr->EnableSecondaryAbsTimer) {
            /* Timer 2 can be configured as an absolute timer -> abs_timer_idx = 1 */
            handle->abs_timers_num = 2U;    /* Two absolute timers are configured */
            /* Check whether at least the timer 1 can be configured as an absolute timer */
        }
        else {
            /* Timer 1 can be configured as an absolute timer -> abs_timer_idx = 0 */
            handle->abs_timers_num = 1U;    /* One absolute timer is configured */
        }

        ctrl_cfg = handle->internal_cfg->Fr_pController;

        if ((Std_ReturnType)(E_NOT_OK) == Fr_Driver_CheckCCAccess(ctrl_cfg,
                (boolean)(FALSE))) {
            ssdk_printf(SSDK_ERR, "\r\n flexray module is not access\r\n");
        }
        else {

            if (Fr_Driver_CCInit(ctrl_cfg) == (Std_ReturnType)E_NOT_OK) {
                ssdk_printf(SSDK_ERR, "\r\n flexray CC init failure\r\n");
            }
            else {

                if (Fr_Driver_EnterPOCConfigState(ctrl_cfg) == (Std_ReturnType)E_NOT_OK) {
                    ssdk_printf(SSDK_ERR, "\r\n flexray CC enter config state failure\r\n");
                }
                else {

                    Fr_Driver_ClusterNodeParamCfg(ctrl_cfg);

                    if (Fr_Driver_BuffersInit(ctrl_cfg) == (Std_ReturnType)E_NOT_OK) {
                        ssdk_printf(SSDK_ERR, "\r\n flexray CC buffer init failure\r\n");
                    }
                    else {

                        Fr_Driver_DisableTimers(ctrl_cfg);

                        if (Fr_Driver_LeavePOCConfigState(ctrl_cfg) == (Std_ReturnType)E_NOT_OK) {
                            ssdk_printf(SSDK_ERR, "\r\n flexray CC leave config state failure\r\n");
                        }
                        else {
                            if ((Std_ReturnType)(E_OK) == Fr_Driver_ClearDisableIRQs(ctrl_cfg)) {
                                ret = SDRV_STATUS_OK;
                            }
                            else {
                                ssdk_printf(SSDK_ERR, "\r\n flexray clear disable irq failure\r\n");
                            }
                        }
                    }
                }
            }
        }
    }

    return ret;
}

/***************************************************************************************************
* @function_name    sdrv_flexray_start_communication
*
* @brief            Starts the communication
* @param[in]        sdrv flexray handle
* @return           SDRV_STATUS_OK or error code.
***************************************************************************************************/
status_t sdrv_flexray_start_communication(sdrv_flexray_t *handle)
{
    status_t ret = SDRV_STATUS_FAIL;
    const sdrv_flexray_ctrl_cfg_type *ctrl_cfg = NULL;
    uint16_t temp_poc_state = 0U;

    ret = sdrv_flexray_parameter_check(handle);

    if (SDRV_STATUS_OK == ret) {
        ctrl_cfg = handle->internal_cfg->Fr_pController;

        if ((Std_ReturnType)(E_NOT_OK) == Fr_Driver_CheckCCAccess(ctrl_cfg,
                (boolean)(TRUE))) {
            /* CC is not accessible, report error */
            ssdk_printf(SSDK_ERR, "\r\n flexray module is not access\r\n");
        }
        else {
            temp_poc_state = Fr_Driver_GetPOCState(ctrl_cfg);

            if (temp_poc_state != FLEXRAY_PSR0_PROTSTATE_READY_U16) {
                ssdk_printf(SSDK_ERR, "\r\n flexray get poc state failure\r\n");
            }
            else {
                if ((Std_ReturnType)E_OK == Fr_Driver_InvokeCHICommand(ctrl_cfg,
                        FLEXRAY_POCR_CMD_RUN_U16)) {
                    ret = SDRV_STATUS_OK;
                }
                else {
                    ssdk_printf(SSDK_ERR, "\r\n flexray invoke chi command failure\r\n");
                }
            }
        }
    }

    return ret;
}

/***************************************************************************************************
* @function_name    sdrv_flexray_allow_coldstart
*
* @brief            Invokes the CC CHI command ALLOW_COLDSTART
* @param[in]        sdrv flexray handle
*
* @return           SDRV_STATUS_OK or error code.
***************************************************************************************************/
status_t sdrv_flexray_allow_coldstart(sdrv_flexray_t *handle)
{
    status_t ret = SDRV_STATUS_FAIL;
    const sdrv_flexray_ctrl_cfg_type *ctrl_cfg = NULL;
    uint16_t temp_poc_state = 0U;

    ret = sdrv_flexray_parameter_check(handle);

    if (SDRV_STATUS_OK == ret) {
        ctrl_cfg = handle->internal_cfg->Fr_pController;

        if ((Std_ReturnType)(E_NOT_OK) == Fr_Driver_CheckCCAccess(ctrl_cfg,
                (boolean)(TRUE))) {
            /* CC is not accessible, report error */
            ssdk_printf(SSDK_ERR, "\r\n flexray module is not access\r\n");
        }
        else {
            /* Determine the POC state */
            temp_poc_state = Fr_Driver_GetPOCState(ctrl_cfg);

            /* Check whether the CC is not in POC:default config, POC:config or POC:halt */
            if ((FLEXRAY_PSR0_PROTSTATE_DEFAULT_CONFIG_U16 == temp_poc_state)
                    || (FLEXRAY_PSR0_PROTSTATE_CONFIG_U16 == temp_poc_state)
                    || (FLEXRAY_PSR0_PROTSTATE_HALT_U16 == temp_poc_state)) {
                /* The CC is in POC:default config or POC:config or POC:halt */
                ssdk_printf(SSDK_ERR, "\r\n flexray get poc state failure\r\n");
            }
            else {   /* CC is in required state */
                /* Invoke CHI command ALLOW_COLDSTART and wait till Protocol Command Write is not busy*/
                if ((Std_ReturnType)(E_OK) == Fr_Driver_InvokeCHICommand(ctrl_cfg,
                        FLEXRAY_POCR_CMDALLOWCOLDSTART_U16)) {
                    /* API call was successful */
                    ret = SDRV_STATUS_OK;
                }
                else {
                    ssdk_printf(SSDK_ERR, "\r\n flexray invoke chi command failure\r\n");
                }
            }
        }
    }

    return ret;
}

/***************************************************************************************************
* @function_name    sdrv_flexray_all_slots
*
* @brief            Invokes the CC CHI command ALL_SLOTS
* @param[in]        sdrv flexray handle
* @return           SDRV_STATUS_OK or error code.
***************************************************************************************************/
status_t sdrv_flexray_all_slots(sdrv_flexray_t *handle)
{
    status_t ret = SDRV_STATUS_FAIL;
    const sdrv_flexray_ctrl_cfg_type *ctrl_cfg = NULL;
    uint16_t temp_poc_state = 0U;

    ret = sdrv_flexray_parameter_check(handle);

    if (SDRV_STATUS_OK == ret) {
        ctrl_cfg = handle->internal_cfg->Fr_pController;

        if ((Std_ReturnType)(E_NOT_OK) == Fr_Driver_CheckCCAccess(ctrl_cfg,
                (boolean)(TRUE))) {
            /* CC is not accessible, report error */
            ssdk_printf(SSDK_ERR, "\r\n flexray module is not access\r\n");
        }
        else {
            /* Determine the POC state */
            temp_poc_state = Fr_Driver_GetPOCState(ctrl_cfg);

            /* The state of the PSR0 register (PROSTATE field) is not updated if FREEZE occurred */
            /* Check whether the CC is synchronous to cluster */
            if ((FLEXRAY_PSR0_PROTSTATE_NORMAL_PASSIVE_U16 != temp_poc_state)
                    && (FLEXRAY_PSR0_PROTSTATE_NORMAL_ACTIVE_U16 != temp_poc_state)) {
                /* The CC is in POC:default config or POC:config or POC:halt */
                ssdk_printf(SSDK_ERR, "\r\n flexray get poc state failure\r\n");
            }
            else {
                /* Invoke CHI command ALL_SLOTS */
                if ((Std_ReturnType)(E_OK) == Fr_Driver_InvokeCHICommand(ctrl_cfg,
                        FLEXRAY_POCR_CMD_ALL_SLOTS_U16)) {
                    /* API call was successful */
                    ret = SDRV_STATUS_OK;
                }
                else {
                    ssdk_printf(SSDK_ERR, "\r\n flexray invoke chi command failure\r\n");
                }
            }
        }
    }

    return ret;        /* Return the state of function execution */
}

/***************************************************************************************************
* @function_name    sdrv_flexray_halt_communication
*
* @brief            This API call stops communication.
* @param[in]        sdrv flexray handle
* @return           SDRV_STATUS_OK or error code.
***************************************************************************************************/
status_t sdrv_flexray_halt_communication(sdrv_flexray_t *handle)
{
    status_t ret = SDRV_STATUS_FAIL;
    const sdrv_flexray_ctrl_cfg_type *ctrl_cfg = NULL;
    uint16_t temp_poc_state = 0U;

    ret = sdrv_flexray_parameter_check(handle);

    if (SDRV_STATUS_OK == ret) {
        ctrl_cfg = handle->internal_cfg->Fr_pController;

        if ((Std_ReturnType)(E_NOT_OK) == Fr_Driver_CheckCCAccess(ctrl_cfg,
                (boolean)(TRUE))) {
            /* CC is not accessible, report error */
            ssdk_printf(SSDK_ERR, "\r\n flexray module is not access\r\n");
        }
        else {
            /* Determine the POC state */
            temp_poc_state = Fr_Driver_GetPOCState(ctrl_cfg);

            /* Check whether the CC is synchronous to cluster */
            if ((FLEXRAY_PSR0_PROTSTATE_NORMAL_PASSIVE_U16 != temp_poc_state)
                    && (FLEXRAY_PSR0_PROTSTATE_NORMAL_ACTIVE_U16 != temp_poc_state)) {
                /* The CC is not synchronized to the flexray global time */
                /* If CC is not synchronized to the flexray global time,
                FR_E_INV_POCSTATE shall be reported */
                /* to the DET and the API function shall return. */
                ssdk_printf(SSDK_ERR, "\r\n flexray get poc state failure\r\n");
            }
            else {
                /* The CC is synchronized to the flexray global time */
                /* Invoke CHI command HALT */
                if ((Std_ReturnType)(E_OK) == Fr_Driver_InvokeCHICommand(ctrl_cfg,
                        FLEXRAY_POCR_CMD_HALT_U16)) {
                    /* API call was successful */
                    ret = SDRV_STATUS_OK;
                }
                else {
                    ssdk_printf(SSDK_ERR, "\r\n flexray invoke chi command failure\r\n");
                }
            }
        }
    }

    return ret;
}

/***************************************************************************************************
* @function_name    sdrv_flexray_abort_communication
*
* @brief            Abort the communication
* @param[in]        sdrv flexray handle
* @return           SDRV_STATUS_OK or error code.
***************************************************************************************************/
status_t sdrv_flexray_abort_communication(sdrv_flexray_t *handle)
{
    status_t ret = SDRV_STATUS_FAIL;
    const sdrv_flexray_ctrl_cfg_type *ctrl_cfg = NULL;

    ret = sdrv_flexray_parameter_check(handle);

    if (SDRV_STATUS_OK == ret) {
        ctrl_cfg = handle->internal_cfg->Fr_pController;

        if ((Std_ReturnType)(E_NOT_OK) == Fr_Driver_CheckCCAccess(ctrl_cfg,
                (boolean)(TRUE))) {
            /* CC is not accessible, report error */
            ssdk_printf(SSDK_ERR, "\r\n flexray module is not access\r\n");
        }
        else {
            /* Invoke CHI command FREEZE */
            if ((Std_ReturnType)(E_OK) == Fr_Driver_InvokeCHICommand(ctrl_cfg,
                    FLEXRAY_POCR_CMD_FREEZE_U16)) {
                /* API call was successful */
                ret = SDRV_STATUS_OK;
            }
            else {
                ssdk_printf(SSDK_ERR, "\r\n flexray invoke chi command failure\r\n");
            }
        }
    }

    return ret;
}

/***************************************************************************************************
* @function_name    sdrv_flexray_send_wakeup
*
* @brief            Initiates transition to POC:Wakeup
* @param[in]        handle - sdrv flexray handle
* @return           SDRV_STATUS_OK or error code.
***************************************************************************************************/
status_t sdrv_flexray_send_wakeup(sdrv_flexray_t *handle)
{
    status_t ret = SDRV_STATUS_FAIL;
    const sdrv_flexray_ctrl_cfg_type *ctrl_cfg = NULL;
    uint16_t temp_poc_state = 0U;

    ret = sdrv_flexray_parameter_check(handle);

    if (SDRV_STATUS_OK == ret) {
        ctrl_cfg = handle->internal_cfg->Fr_pController;

        if ((Std_ReturnType)(E_NOT_OK) == Fr_Driver_CheckCCAccess(ctrl_cfg,
                (boolean)(TRUE))) {
            /* CC is not accessible, report error */
            ssdk_printf(SSDK_ERR, "\r\n flexray module is not access\r\n");
        }
        else {
            /* Determine the POC state */
            temp_poc_state = Fr_Driver_GetPOCState(ctrl_cfg);

            /* The state of the PSR0 register (PROSTATE field) is not updated if FREEZE occurred */
            /* Check whether the CC is in POC:ready */
            if (FLEXRAY_PSR0_PROTSTATE_READY_U16 != temp_poc_state) {
                /* The CC is not in POC:ready */
                ssdk_printf(SSDK_ERR, "\r\n flexray get poc state failure\r\n");
            }
            else {
                /* Invoke CHI command WAKEUP */
                if ((Std_ReturnType)(E_OK) == Fr_Driver_InvokeCHICommand(ctrl_cfg,
                        FLEXRAY_POCR_CMD_WAKEUP_U16)) {
                    /* API call was successful */
                    ret = SDRV_STATUS_OK;
                }
                else {
                    ssdk_printf(SSDK_ERR, "\r\n flexray invoke chi command failure\r\n");
                }
            }
        }
    }

    return ret;        /* Return the state of function execution */
}

/***************************************************************************************************
* @function_name    sdrv_flexray_set_wakeup_channel
*
* @brief            Selects which channel sends WUPs
* @param[in]        sdrv flexray handle
* @param[in]        channel_idx Index of flexray channel within the context of the flexray CC
*                   ctrl_idx
* @return           SDRV_STATUS_OK or error code.
***************************************************************************************************/
status_t sdrv_flexray_set_wakeup_channel(sdrv_flexray_t *handle,
        sdrv_flexray_channel_type channel_idx)
{
    status_t ret = SDRV_STATUS_FAIL;
    const sdrv_flexray_ctrl_cfg_type *ctrl_cfg = NULL;
    uint16_t temp_poc_state = 0U;

    ret = sdrv_flexray_parameter_check(handle);

    if (SDRV_STATUS_OK == ret) {
        if ((FR_CHANNEL_A != channel_idx) && (FR_CHANNEL_B != channel_idx)) {
            ssdk_printf(SSDK_ERR, "\r\nFr_ChnlIdx select error\r\n");
            ret = SDRV_FLEXRAY_INV_CHNL_IDX;
        }
        else {

            ctrl_cfg = handle->internal_cfg->Fr_pController;

            if ((Std_ReturnType)(E_NOT_OK) == Fr_Driver_CheckCCAccess(ctrl_cfg,
                    (boolean)(TRUE))) {
                /* CC is not accessible, report error */
                ssdk_printf(SSDK_ERR, "\r\n flexray module is not access\r\n");
            }
            else {
                /* Determine the POC state */
                temp_poc_state = Fr_Driver_GetPOCState(ctrl_cfg);

                /* Check whether the CC is in POC:ready */
                if (FLEXRAY_PSR0_PROTSTATE_READY_U16 != temp_poc_state) {
                    /* The CC is not in POC:ready */
                    ssdk_printf(SSDK_ERR, "\r\n flexray get poc state failure\r\n");
                }
                else {
                    /* Call LLD function to set the Wakeup channel */
                    if ((Std_ReturnType)(E_OK) == Fr_Driver_SetWakeupChannel(ctrl_cfg,
                            channel_idx)) {
                        ret = SDRV_STATUS_OK;
                    }
                    else {
                        ssdk_printf(SSDK_ERR, "\r\n flexray set wakeup channel failure\r\n");
                    }
                }
            }
        }
    }

    return ret;        /* Return the state of function execution */
}

/***************************************************************************************************
* @function_name    sdrv_flexray_get_poc_status
*
* @brief            Query for the controller status
* @param[in]        sdrv flexray handle
* @param[out]       poc_status_ptr Address the output value is stored to
* @return           SDRV_STATUS_OK or error code.
***************************************************************************************************/
status_t sdrv_flexray_get_poc_status(sdrv_flexray_t *handle,
                                     sdrv_flexray_poc_status_type *poc_status_ptr)
{
    status_t ret = SDRV_STATUS_FAIL;
    const sdrv_flexray_ctrl_cfg_type *ctrl_cfg = NULL;

    ret = sdrv_flexray_parameter_check(handle);

    if (SDRV_STATUS_OK == ret) {
        if (NULL == poc_status_ptr) {
            /* If Fr_SyncStatePtr equals NULL, FR_E_PARAM_POINTER shall be reported */
            /* to the DET and the API function shall return. */
            ssdk_printf(SSDK_ERR, "\r\n poc_status_ptr pointer pointer is null\r\n");
            ret = SDRV_FLEXRAY_PARAM_POINTER;
        }
        else {   /* No error yet -> execute the following code */

            ctrl_cfg = handle->internal_cfg->Fr_pController;

            if ((Std_ReturnType)(E_NOT_OK) == Fr_Driver_CheckCCAccess(ctrl_cfg,
                    (boolean)(TRUE))) {
                /* CC is not accessible, report error */
                ssdk_printf(SSDK_ERR, "\r\n flexray module is not access\r\n");
            }
            else {
                if ((Std_ReturnType)(E_OK) == Fr_Driver_GetPOCStatus(ctrl_cfg,
                        poc_status_ptr)) {
                    ret = SDRV_STATUS_OK;
                }
                else {
                    ssdk_printf(SSDK_ERR, "\r\n flexray get poc status failure\r\n");
                }
            }
        }
    }

    return ret;        /* Return the state of function execution */
}

/***************************************************************************************************
* @function_name    sdrv_flexray_set_absolute_timer
*
* @brief            Sets the absolute flexray timer
* @param[in]        sdrv flexray handle
* @param[in]        abs_timer_idx Index of absolute timer within the context of the flexray CC
* @param[in]        cycle Absolute cycle the timer shall elapse in
* @param[in]        offset Offset within cycle cycle in units of macrotick the timer shall
*                   elapse at
* @return           SDRV_STATUS_OK or error code.
***************************************************************************************************/
status_t sdrv_flexray_set_absolute_timer(sdrv_flexray_t *handle,
        uint8_t abs_timer_idx,
        uint8_t cycle, uint16_t offset)
{
    status_t ret = SDRV_STATUS_FAIL;
    const sdrv_flexray_ctrl_cfg_type *ctrl_cfg = NULL;
    uint16_t temp_poc_state = 0U;

    ret = sdrv_flexray_parameter_check(handle);

    if (SDRV_STATUS_OK == ret) {
        if (!(abs_timer_idx < handle->abs_timers_num)) {
            /* An attempt to configure an invalid timer, abs_timer_idx has an invalid value */
            ssdk_printf(SSDK_ERR, "\r\n abs_timer_idx has an invalid value\r\n");
            ret = SDRV_FLEXRAY_INV_TIMER_IDX;
        }
        else {
            if (cycle > 63U) { /* Check the cycle value, max. cycle no. 63 */
                /* An attempt to configure timer for invalid cycle number, cycle has an invalid value */
                ssdk_printf(SSDK_ERR, "\r\n cycle has an invalid value\r\n");
                ret = SDRV_FLEXRAY_INV_CYCLE;
            }
            /* Compare offset value with maximum gMacroPerCycle from configuration,
                the absolute timer shall elapsed at */
            else {

                ctrl_cfg = handle->internal_cfg->Fr_pController;

                if (offset >= (((uint16_t)(
                                    handle->internal_cfg->Fr_pController->LowLevelConfigSetPtr->RegPCR10))
                               &
                               0x3FFFU)) {
                    /* An attempt to configure timer for invalid macrotick offset number,
                        offset has an invalid value */
                    ssdk_printf(SSDK_ERR, "\r\n offset has an invalid value\r\n");
                    ret = SDRV_FLEXRAY_INV_OFFSET;
                }
                else if ((Std_ReturnType)(E_NOT_OK) == Fr_Driver_CheckCCAccess(ctrl_cfg,
                         (boolean)(TRUE))) {
                    /* CC is not accessible, report error */
                    ssdk_printf(SSDK_ERR, "\r\n flexray module is not access\r\n");
                }
                else {
                    /* Determine the POC state */
                    temp_poc_state = Fr_Driver_GetPOCState(ctrl_cfg);

                    /* Check whether the CC is synchronous to cluster */
                    if ((FLEXRAY_PSR0_PROTSTATE_NORMAL_PASSIVE_U16 != temp_poc_state)
                            && (FLEXRAY_PSR0_PROTSTATE_NORMAL_ACTIVE_U16 != temp_poc_state)) {
                        /* The CC is not synchronized to the cluster */
                        ssdk_printf(SSDK_ERR, "\r\n flexray get poc state failure\r\n");
                    }
                    else {
                        /* The CC is synchronized to the cluster */
                        /* Call LLD function to set the Absolute Timer */
                        Fr_Driver_SetAbsoluteTimer(ctrl_cfg, abs_timer_idx, cycle, offset);
                        /* API call was successful */
                        ret = SDRV_STATUS_OK;
                    }
                }
            }
        }
    }

    return ret;
}

/***************************************************************************************************
* @function_name    sdrv_flexray_transmit_tx_lpdu
*
* @brief            Update selected message buffer with new data
* @param[in]        sdrv flexray handle
* @param[in]        lpdu_idx The index is used to uniquely identify a flexray frame
* @param[in]        lsdu_ptr Pointer to a buffer where the assembled LSdu to be transmitted
*                   within this LPdu is stored at
* @param[in]        lsdu_length Determines the length of the data (in Bytes) to be transmitted
* @param[out]       slot_assignment_ptr This reference points to the memory location where the
                    actual cycle, slot ID, and channel of the frame identified by
                    lpdu_idx shall be stored. A NULL indicates that the
                    information is not required by the caller.
* @return           SDRV_STATUS_OK or error code.
***************************************************************************************************/
status_t sdrv_flexray_transmit_tx_lpdu(sdrv_flexray_t *handle,
                                       uint16_t lpdu_idx,
                                       const uint8_t *lsdu_ptr,
                                       uint8_t lsdu_length, sdrv_flexray_slot_assignment_type *slot_assignment_ptr)
{
    status_t ret = SDRV_STATUS_FAIL;
    const sdrv_flexray_ctrl_cfg_type *ctrl_cfg = NULL;

    /* Pointer to one instance of sdrv_flexray_cc_tx_buffer_config_type */
    const sdrv_flexray_cc_tx_buffer_config_type *ptxCfgPtr = NULL;
    bool error = false;

    ret = sdrv_flexray_parameter_check(handle);

    if (SDRV_STATUS_OK == ret) {
        ctrl_cfg = handle->internal_cfg->Fr_pController;

        if (lpdu_idx >
                (handle->internal_cfg->Fr_pController->BufferConfigSetPtr->BuffersConfiguredCount
                 - 1U)) {
            /* lpdu_idx has an invalid value */
            ssdk_printf(SSDK_ERR, "\r\n lpdu_idx has an invalid value\r\n");
            ret = SDRV_FLEXRAY_INV_LPDU_IDX;
        }
        else {
            if (NULL == lsdu_ptr) {
                /* If lsdu_ptr equals NULL, FR_E_PARAM_POINTER shall be reported */
                /* to the DET and the API function shall return. */
                ssdk_printf(SSDK_ERR, "\r\n lsdu_ptr pointer pointer is null\r\n");
                ret = SDRV_FLEXRAY_PARAM_POINTER;
            }
            else {
                if (NULL == slot_assignment_ptr) {
                    /* If slot_assignment_ptr equals NULL, FR_E_PARAM_POINTER shall be reported */
                    /* to the DET and the API function shall return. */
                    ssdk_printf(SSDK_ERR, "\r\n slot_assignment_ptr pointer pointer is null\r\n");
                    ret = SDRV_FLEXRAY_PARAM_POINTER;
                }
                else {   /* 1st phase; No error yet -> execute the following code */
                    /* Store the reference to transmit buffer configuration */
                    ptxCfgPtr = (const sdrv_flexray_cc_tx_buffer_config_type *)
                                (handle->internal_cfg->Fr_pController->BufferConfigSetPtr->LPduInfoPtr[lpdu_idx].BufferConfigPtr);

                    /* Check whether the lsdu_length has valid value; transformation from Bytes to Words
                        has to be done */
                    /* Check whether the MB is configured for dynamic/static segment of communication cycle and
                        check whether the required data payload length is not greater than maximal data length
                        in dynamic/static segment */
                    if (ptxCfgPtr->TxFrameID >
                            handle->internal_cfg->Fr_pController->LowLevelConfigSetPtr->gNumberOfStaticSlots) {
                        /* Buffer configured for the dynamic segment */
                        if (lsdu_length >
                                (handle->internal_cfg->Fr_pController->LowLevelConfigSetPtr->pPayloadLengthDynMax
                                 * 2U)) {
                            /* lsdu_length has an invalid value */
                            ssdk_printf(SSDK_ERR, "\r\n lsdu_length has an invalid value\r\n");
                            /* indicate error */
                            error = true;
                            ret = SDRV_FLEXRAY_INV_LENGTH;
                        }
                    }
                    else {
                        /* Static segment */
                        if (lsdu_length >
                                (handle->internal_cfg->Fr_pController->LowLevelConfigSetPtr->gPayloadLengthStatic
                                 * 2U)) {
                            /* lsdu_length has an invalid value */
                            ssdk_printf(SSDK_ERR, "\r\n lsdu_length has an invalid value\r\n");
                            /* indicate error */
                            error = true;
                            ret = SDRV_FLEXRAY_INV_LENGTH;
                        }
                    }

                    if (!(true == error)) {  /* Check if some error occured */
                        /* 2nd phase; No error yet -> execute the following code */

                        if (FR_TRANSMIT_BUFFER ==
                                handle->internal_cfg->Fr_pController->BufferConfigSetPtr->LPduInfoPtr[lpdu_idx].BufferType) {

                            if ((Std_ReturnType)(E_NOT_OK) == Fr_Driver_CheckCCAccess(ctrl_cfg,
                                    (boolean)(TRUE))) {
                                /* CC is not accessible, report error */
                                ssdk_printf(SSDK_ERR, "\r\n flexray module is not access\r\n");
                            }
                            else {
                                /* Temporary offset address of MB configuration registers */
                                if ((Std_ReturnType)(E_OK) == Fr_Driver_TransmitTxLPdu(ctrl_cfg,
                                        lpdu_idx, lsdu_ptr, lsdu_length, slot_assignment_ptr)) {
                                    ret = SDRV_STATUS_OK;
                                }
                                else {
                                    ssdk_printf(SSDK_ERR, "\r\n flexray transmit tx lpdu failure\r\n");
                                }
                            } /* Fr_check_CC_access */

                        }
                        else {
                            /* LPdu is not configured for Tx -> abort the function execution and return E_NOT_OK  */
                            ssdk_printf(SSDK_ERR, "\r\n lpdu is not configured for Tx\r\n");
                            ret = SDRV_FLEXRAY_PARAM_CONFIG;
                        }
                    }
                }
            }
        }
    }

    return ret;
}

/***************************************************************************************************
* @function_name    sdrv_flexray_cancel_tx_lpdu
*
* @brief            Reconfigures physical resource
* @param[in]        sdrv flexray handle
* @param[in]        lpdu_idx This index is used to uniquely identify a flexray frame
* @return           SDRV_STATUS_OK or error code.
***************************************************************************************************/
status_t sdrv_flexray_cancel_tx_lpdu(sdrv_flexray_t *handle,
                                     uint16_t lpdu_idx)
{
    status_t ret = SDRV_STATUS_FAIL;
    const sdrv_flexray_ctrl_cfg_type *ctrl_cfg = NULL;
    boolean pending_status = (boolean)FALSE;

    ret = sdrv_flexray_parameter_check(handle);

    if (SDRV_STATUS_OK == ret) {
        ctrl_cfg = handle->internal_cfg->Fr_pController;

        if (lpdu_idx > ((
                            handle->internal_cfg->Fr_pController->BufferConfigSetPtr->BuffersConfiguredCount)
                        - 1U)) {
            /* lpdu_idx has an invalid value */
            ssdk_printf(SSDK_ERR, "\r\nFr_LPduIdx has an invalid value\r\n");
            ret = SDRV_FLEXRAY_INV_LPDU_IDX;
        }
        else {   /* No error yet -> execute the following code */

            /* Check whether a Tx MB is configured for given LPdu */
            if (FR_TRANSMIT_BUFFER ==
                    handle->internal_cfg->Fr_pController->BufferConfigSetPtr->LPduInfoPtr[lpdu_idx].BufferType) {
                /* Transmit MB is configured for given LPdu */

                if ((Std_ReturnType)(E_NOT_OK) == Fr_Driver_CheckCCAccess(ctrl_cfg,
                        (boolean)(TRUE))) {
                    /* CC is not accessible, report error */
                    ssdk_printf(SSDK_ERR, "\r\n flexray module is not access\r\n");
                }
                else {
                    /* @violates @ref Fr_c_REF_7 */
                    if ((Std_ReturnType)(E_OK) == Fr_Driver_CancelTxLPdu(ctrl_cfg, lpdu_idx,
                            &pending_status)) {
                        /* Transmit request was terminated*/
                        if ((boolean)(TRUE) == pending_status) {
                            ret = SDRV_STATUS_OK;
                        }
                    }
                    else {
                        /* Do nothing for FR_DEM_EVENT_DETECT == STD_OFF */
                        ssdk_printf(SSDK_ERR, "\r\n cancel tx lpdu failure\r\n");
                    }
                } /* Fr_check_CC_access */
            }
            else {
                /* LPdu is not configured for Tx -> abort the function execution and return E_NOT_OK  */
                ssdk_printf(SSDK_ERR, "\r\n lpdu is not configured for Tx\r\n");
                ret = SDRV_FLEXRAY_PARAM_CONFIG;
            }
        }
    }

    return ret;
}

/***************************************************************************************************
* @function_name    sdrv_flexray_receive_rx_lpdu
*
* @brief            Receives data
* @param[in]        sdrv flexray handle
* @param[in]        lpdu_idx This index is used to uniquely identify a flexray frame
* @param[out]       lsdu_ptr Pointer to a buffer where the LSdu
* @param[out]       lpdu_status_ptr Pointer to the memory location where the status of the LPdu
*                   shall be stored
* @param[out]       lsdu_length_ptr Pointer to the memory location where the length of the LSdu
*                   (in bytes) shall be stored
* @param[out]       slot_assignment_ptr  This reference points to the memory location where the
                    actual cycle, slot ID, and channel of the frame identified by
                    lpdu_idx shall be stored. A NULL indicates that the
                    information is not required by the caller.
* @return           SDRV_STATUS_OK or error code.
***************************************************************************************************/
status_t sdrv_flexray_receive_rx_lpdu(sdrv_flexray_t *handle,
                                      uint16_t lpdu_idx,
                                      uint8_t *lsdu_ptr,
                                      sdrv_flexray_rx_lpdu_status_type *lpdu_status_ptr, uint8_t *lsdu_length_ptr,
                                      sdrv_flexray_slot_assignment_type *slot_assignment_ptr)
{
    status_t ret = SDRV_STATUS_FAIL;
    const sdrv_flexray_ctrl_cfg_type *ctrl_cfg = NULL;

    ret = sdrv_flexray_parameter_check(handle);

    if (SDRV_STATUS_OK == ret) {
        ctrl_cfg = handle->internal_cfg->Fr_pController;

        if (lpdu_idx >
                (handle->internal_cfg->Fr_pController->BufferConfigSetPtr->BuffersConfiguredCount
                 - 1U)) {
            /* lpdu_idx has an invalid value */
            ssdk_printf(SSDK_ERR, "\r\n lpdu_idx has an invalid value\r\n");
            ret = SDRV_FLEXRAY_INV_LPDU_IDX;
        }
        else {
            if (NULL == lsdu_ptr) {
                /* If lsdu_ptr equals NULL, FR_E_PARAM_POINTER shall be reported */
                /* to the DET and the API function shall return. */
                ssdk_printf(SSDK_ERR, "\r\n lsdu_ptr pointer pointer is null\r\n");
                ret = SDRV_FLEXRAY_PARAM_POINTER;
            }
            else {
                if (NULL == lpdu_status_ptr) {
                    /* If lpdu_status_ptr equals NULL, FR_E_PARAM_POINTER shall be reported */
                    /* to the DET and the API function shall return. */
                    ssdk_printf(SSDK_ERR, "\r\n lpdu_status_ptr pointer pointer is null\r\n");
                    ret = SDRV_FLEXRAY_PARAM_POINTER;
                }
                else {
                    if (NULL == lsdu_length_ptr) {
                        /* If lsdu_length_ptr equals NULL, FR_E_PARAM_POINTER shall be reported */
                        /* to the DET and the API function shall return. */
                        ssdk_printf(SSDK_ERR, "\r\n lsdu_length_ptr pointer pointer is null\r\n");
                        ret = SDRV_FLEXRAY_PARAM_POINTER;
                    }
                    else {
                        if (NULL == slot_assignment_ptr) {
                            /* If slot_assignment_ptr equals NULL, FR_E_PARAM_POINTER shall be reported */
                            /* to the DET and the API function shall return. */
                            ssdk_printf(SSDK_ERR, "\r\n slot_assignment_ptr pointer pointer is null\r\n");
                            ret = SDRV_FLEXRAY_PARAM_POINTER;
                        }
                        else {

                            if (FR_RECEIVE_BUFFER ==
                                    handle->internal_cfg->Fr_pController->BufferConfigSetPtr->LPduInfoPtr[lpdu_idx].BufferType) {

                                if ((Std_ReturnType)(E_NOT_OK) == Fr_Driver_CheckCCAccess(ctrl_cfg,
                                        (boolean)(TRUE))) {
                                    /* CC is not accessible, report error */
                                    ssdk_printf(SSDK_ERR, "\r\n flexray module is not access\r\n");
                                }
                                else {
                                    /* Copy received data */
                                    if ((Std_ReturnType)(E_OK) == Fr_Driver_ReceiveRxLPdu(ctrl_cfg, lpdu_idx,
                                            lsdu_ptr, lpdu_status_ptr, lsdu_length_ptr, slot_assignment_ptr)
                                       ) {
                                        ret = SDRV_STATUS_OK;
                                    }
                                    else {
                                        ssdk_printf(SSDK_ERR, "\r\n flexray receive rx lpdu failure\r\n");
                                    }
                                } /* Check CC access */
                            }
                            /* Check if FIFO A or FIFO B is used */
                            else {
                                if (FR_FIFOA_BUFFER ==
                                        handle->internal_cfg->Fr_pController->BufferConfigSetPtr->LPduInfoPtr[lpdu_idx].BufferType) {
                                    /* FIFO A is used */
                                    if ((Std_ReturnType)(E_NOT_OK) == Fr_Driver_CheckCCAccess(ctrl_cfg,
                                            (boolean)(TRUE))) {
                                        /* CC is not accessible, report error */
                                        ssdk_printf(SSDK_ERR, "\r\n flexray module is not access\r\n");
                                    }
                                    else {
                                        /* Copy received data */
                                        Fr_Driver_ReceiveFifo(ctrl_cfg, lpdu_idx, lsdu_ptr, lpdu_status_ptr,
                                                              lsdu_length_ptr, slot_assignment_ptr, FR_RECEIVE_FIFOA);
                                        ret = SDRV_STATUS_OK;
                                    }
                                }
                                else {
                                    if (FR_FIFOB_BUFFER ==
                                            handle->internal_cfg->Fr_pController->BufferConfigSetPtr->LPduInfoPtr[lpdu_idx].BufferType) {
                                        /* FIFO B is used */
                                        if ((Std_ReturnType)(E_NOT_OK) == Fr_Driver_CheckCCAccess(ctrl_cfg,
                                                (boolean)(TRUE))) {
                                            /* CC is not accessible, report error */
                                            ssdk_printf(SSDK_ERR, "\r\n flexray module is not access\r\n");
                                        }
                                        else {
                                            /* Copy received data */
                                            Fr_Driver_ReceiveFifo(ctrl_cfg, lpdu_idx, lsdu_ptr, lpdu_status_ptr,
                                                                  lsdu_length_ptr, slot_assignment_ptr, FR_RECEIVE_FIFOB);
                                            ret = SDRV_STATUS_OK;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    return ret;        /* Return the state of function execution */
}

/***************************************************************************************************
* @function_name    sdrv_flexray_check_tx_lpdu_status
*
* @brief            Checks if data have been transmitted.
* @param[in]        sdrv flexray handle
* @param[in]        lpdu_idx This index is used to uniquely identify a flexray frame
* @param[out]       tx_lpdu_status_ptr Pointer used to store the transmit status of the LSdu
* @param[out]       slot_assignment_ptr Pointer used to store cycle, slot ID, and channel of the frame identified by lpdu_idx
* @return           SDRV_STATUS_OK or error code.
***************************************************************************************************/
status_t sdrv_flexray_check_tx_lpdu_status(sdrv_flexray_t *handle,
        uint16_t lpdu_idx,
        sdrv_flexray_tx_lpdu_status_type *tx_lpdu_status_ptr,
        sdrv_flexray_slot_assignment_type *slot_assignment_ptr)
{
    status_t ret = SDRV_STATUS_FAIL;
    const sdrv_flexray_ctrl_cfg_type *ctrl_cfg = NULL;

    ret = sdrv_flexray_parameter_check(handle);

    if (SDRV_STATUS_OK == ret) {
        ctrl_cfg = handle->internal_cfg->Fr_pController;

        if (lpdu_idx >
                (handle->internal_cfg->Fr_pController->BufferConfigSetPtr->BuffersConfiguredCount
                 - 1U)) {
            /* lpdu_idx has an invalid value */
            ssdk_printf(SSDK_ERR, "\r\nFr_LPduIdx has an invalid value\r\n");
            ret = SDRV_FLEXRAY_INV_LPDU_IDX;
        }
        else {
            if (NULL == tx_lpdu_status_ptr) {
                /* If tx_lpdu_status_ptr equals NULL, FR_E_PARAM_POINTER shall be reported */
                /* to the DET and the API function shall return. */
                ssdk_printf(SSDK_ERR, "\r\nFr_TxLPduStatusPtr pointer pointer is null\r\n");
                ret = SDRV_FLEXRAY_PARAM_POINTER;
            }
            else {
                if (NULL == slot_assignment_ptr) {
                    /* If slot_assignment_ptr equals NULL, FR_E_PARAM_POINTER shall be reported */
                    /* to the DET and the API function shall return. */
                    ssdk_printf(SSDK_ERR, "\r\nFr_SlotAssignmentPtr pointer pointer is null\r\n");
                    ret = SDRV_FLEXRAY_PARAM_POINTER;
                }
                else {

                    if (FR_TRANSMIT_BUFFER ==
                            handle->internal_cfg->Fr_pController->BufferConfigSetPtr->LPduInfoPtr[lpdu_idx].BufferType) {
                        /* Transmit MB is configured for given LPdu */

                        if ((Std_ReturnType)(E_NOT_OK) == Fr_Driver_CheckCCAccess(ctrl_cfg,
                                (boolean)TRUE)) {
                            /* CC is not accessible, report error */
                            ssdk_printf(SSDK_ERR, "\r\n flexray module is not access\r\n");
                        }
                        else {
                            if ((Std_ReturnType)(E_OK) == Fr_Driver_CheckTxLPduStatus(ctrl_cfg, lpdu_idx,
                                    tx_lpdu_status_ptr, slot_assignment_ptr)) {
                                ret = SDRV_STATUS_OK;
                            }
                            else {
                                /* Data was not transmitted */
                                /* Do nothing for FR_DEM_EVENT_DETECT == STD_OFF return E_NOT_OK */
                                ssdk_printf(SSDK_ERR, "\r\n check tx lpdu status failure\r\n");
                            }
                        } /* CC access check */
                    }
                    else {
                        /* LPdu is not configured for Tx -> abort the function execution and return E_NOT_OK  */
                        ssdk_printf(SSDK_ERR, "\r\n LPdu is not configured for Tx\r\n");
                        ret = SDRV_FLEXRAY_PARAM_CONFIG;
                    }
                }
            }
        }
    }

    return ret;
}

/***************************************************************************************************
* @function_name    sdrv_flexray_get_global_time
*
* @brief            Function gets flexray cluster global time
* @param[in]        sdrv flexray handle
* @param[out]       cycle_ptr Address where the current flexray communication cycle value
*                   shall be stored
* @param[out]       macro_tick_ptr Address where the current macrotick value shall be stored
* @return           SDRV_STATUS_OK or error code.
***************************************************************************************************/
status_t sdrv_flexray_get_global_time(sdrv_flexray_t *handle,
                                      uint8_t *cycle_ptr,
                                      uint16_t *macro_tick_ptr)
{
    status_t ret = SDRV_STATUS_FAIL;
    const sdrv_flexray_ctrl_cfg_type *ctrl_cfg = NULL;
    uint16_t temp_poc_state = 0U;

    ret = sdrv_flexray_parameter_check(handle);

    if (SDRV_STATUS_OK == ret) {
        if (NULL == cycle_ptr) {
            /* If cycle_ptr equals NULL, FR_E_PARAM_POINTER shall be reported */
            /* to the DET and the API function shall return. */
            ssdk_printf(SSDK_ERR, "\r\n cycle_ptr pointer is null\r\n");
            ret = SDRV_FLEXRAY_PARAM_POINTER;
        }
        else {
            if (NULL == macro_tick_ptr) {
                /* If macro_tick_ptr equals NULL, FR_E_PARAM_POINTER shall be reported */
                /* to the DET and the API function shall return. */
                ssdk_printf(SSDK_ERR, "\r\n macro_tick_ptr pointer is null\r\n");
                ret = SDRV_FLEXRAY_PARAM_POINTER;
            }
            else {

                ctrl_cfg = handle->internal_cfg->Fr_pController;

                if ((Std_ReturnType)(E_NOT_OK) == Fr_Driver_CheckCCAccess(ctrl_cfg,
                        (boolean)(TRUE))) {
                    /* CC is not accessible, report error */
                    ssdk_printf(SSDK_ERR, "\r\n flexray module is not access\r\n");
                }
                else {
                    /* Determine the POC state */
                    temp_poc_state = Fr_Driver_GetPOCState(ctrl_cfg);

                    /* The state of the PSR0 register (PROSTATE field) is not updated if FREEZE occurred */
                    /* Check whether the CC is synchronous to cluster */
                    if ((FLEXRAY_PSR0_PROTSTATE_NORMAL_PASSIVE_U16 == temp_poc_state)
                            || (FLEXRAY_PSR0_PROTSTATE_NORMAL_ACTIVE_U16 == temp_poc_state)) {
                        /* The CC is in POC:Normal Active or POC:Normal Passive */
                        /* Call function to get Cycle and Macrotick */
                        /* HW issue detected, Report error */
                        if ((Std_ReturnType)(E_OK) == Fr_Driver_GetGlobalTime(ctrl_cfg, cycle_ptr,
                                macro_tick_ptr)) {
                            ret = SDRV_STATUS_OK;
                        }
                        else {
                            ssdk_printf(SSDK_ERR, "\r\n flexray get global time failure\r\n");
                        }
                    }
                    else {
                        ssdk_printf(SSDK_ERR, "\r\n flexray get poc state failure\r\n");
                    }
                }
            }
        }
    }

    return ret;  /* Return the state of function execution */
}/* End of Fr_GetGlobalTime */

/***************************************************************************************************
* @function_name    sdrv_flexray_get_network_vector
*
* @brief            Read network management vector
* @param[in]        handle - sdrv flexray handle
* @param[out]       network_vector - Address where the NmVector of the last communication cycle
*                   shall be stored
* @return           SDRV_STATUS_OK or error code.
***************************************************************************************************/
status_t sdrv_flexray_get_network_vector(sdrv_flexray_t *handle,
        uint8_t *network_vector)
{
    status_t ret = SDRV_STATUS_FAIL;
    const sdrv_flexray_ctrl_cfg_type *ctrl_cfg = NULL;
    uint16_t temp_poc_state = 0U;
    boolean hw_error = (boolean)FALSE;

    ret = sdrv_flexray_parameter_check(handle);

    if (SDRV_STATUS_OK == ret) {
        if (NULL == network_vector) {
            /* If Fr_NmVectorPtr equals NULL, FR_E_PARAM_POINTER shall be reported */
            /* to the DET and the API function shall return. */
            ssdk_printf(SSDK_ERR, "\r\n network_vector pointer is null\r\n");
            ret = SDRV_FLEXRAY_PARAM_POINTER;
        }
        else {

            ctrl_cfg = handle->internal_cfg->Fr_pController;

            if ((Std_ReturnType)(E_NOT_OK) == Fr_Driver_CheckCCAccess(ctrl_cfg,
                    (boolean)(TRUE))) {
                /* CC is not accessible, report error */
                ssdk_printf(SSDK_ERR, "\r\n flexray module is not access\r\n");
            }
            else {
                temp_poc_state = Fr_Driver_GetPOCState(ctrl_cfg);

                /* Check whether the CC is synchronous to cluster */
                if ((FLEXRAY_PSR0_PROTSTATE_NORMAL_PASSIVE_U16 == temp_poc_state)
                        || (FLEXRAY_PSR0_PROTSTATE_NORMAL_ACTIVE_U16 == temp_poc_state)) {
                    /* The CC is in POC:Normal Active or POC:Normal Passive */
                    /* Read Network Management Vector */
                    /* @violates @ref Fr_c_REF_7 */
                    if ((Std_ReturnType)(E_OK) == Fr_Driver_GetNmVector(ctrl_cfg, network_vector,
                            &hw_error)) {

                        if ((boolean)TRUE == hw_error) {
                            ssdk_printf(SSDK_ERR, "\r\n flexray get network vector failure\r\n");
                        }
                        else {
                            ret = SDRV_STATUS_OK;
                        }
                    }
                }
                else {
                    ssdk_printf(SSDK_ERR, "\r\n flexray get poc state failure\r\n");
                }
            }
        }
    }

    return ret;        /* Return the state of function execution */
}

/***************************************************************************************************
* @function_name    sdrv_flexray_get_number_startup_frames
*
* @brief            Gets the current number of startup frames seen on the cluster.
* @param[in]        sdrv flexray handle
* @param[in]        number_startup_frame_ptr Address where the number of startup frames seen
*                                            within the last even or odd cycle pair shall be stored.
* @return           SDRV_STATUS_OK or error code.
***************************************************************************************************/
status_t sdrv_flexray_get_number_startup_frames(sdrv_flexray_t *handle,
        uint8_t *number_startup_frame_ptr)
{
    status_t ret = SDRV_STATUS_FAIL;
    const sdrv_flexray_ctrl_cfg_type *ctrl_cfg = NULL;
    uint16_t temp_poc_state = 0U;

    ret = sdrv_flexray_parameter_check(handle);

    if (SDRV_STATUS_OK == ret) {
        if (number_startup_frame_ptr == NULL) {
            /* config_para_value_ptr has an invalid value */
            ssdk_printf(SSDK_ERR, "\r\nFr_ConfigParamValuePtr has an invalid value\r\n");
            ret = SDRV_FLEXRAY_PARAM_POINTER;
        }
        else {

            ctrl_cfg = handle->internal_cfg->Fr_pController;

            if ((Std_ReturnType)(E_NOT_OK) == Fr_Driver_CheckCCAccess(ctrl_cfg,
                    (boolean)(TRUE))) {
                /* CC is not accessible, report error */
                ssdk_printf(SSDK_ERR, "\r\n flexray module is not access\r\n");
            }
            else {

                /* Determine the POC state */
                temp_poc_state = Fr_Driver_GetPOCState(ctrl_cfg);

                /* Check whether the CC is synchronous to cluster */
                if ((FLEXRAY_PSR0_PROTSTATE_NORMAL_PASSIVE_U16 == temp_poc_state)
                        || (FLEXRAY_PSR0_PROTSTATE_NORMAL_ACTIVE_U16 == temp_poc_state)) {
                    /* The CC is synchronized to the cluster */
                    /* Call LLD function to get number of startup frames */
                    if ((Std_ReturnType)(E_OK) == Fr_Driver_GetNumOfStartupFrames(ctrl_cfg,
                            number_startup_frame_ptr)) {
                        /* API call was successful */
                        ret = SDRV_STATUS_OK;
                    }
                    else {
                        ssdk_printf(SSDK_ERR, "\r\n flexray get number startup frame failure\r\n");
                    }
                }
                else {
                    /* The CC is not synchronized to the cluster */
                    ssdk_printf(SSDK_ERR, "\r\n flexray get poc state failure\r\n");
                }
            }
        }
    }

    return ret;
}

/***************************************************************************************************
* @function_name    sdrv_flexray_get_channel_status
*
* @brief            Read channel status
* @param[in]        handle - sdrv flexray handle
* @param[out]       channel_a_status_ptr - Address where the bitcoded channel A status information
*                                          shall be stored.
* @param[out]       channel_b_status_ptr - Address where the bitcoded channel B status information
*                                          shall be stored.
* @return           SDRV_STATUS_OK or error code.
***************************************************************************************************/
status_t sdrv_flexray_get_channel_status(sdrv_flexray_t *handle,
        uint16_t *channel_a_status_ptr, uint16_t *channel_b_status_ptr)
{
    status_t ret = SDRV_STATUS_FAIL;
    const sdrv_flexray_ctrl_cfg_type *ctrl_cfg = NULL;
    uint16_t u16tTmpPOCState = 0U;

    ret = sdrv_flexray_parameter_check(handle);

    if (SDRV_STATUS_OK == ret) {
        if (NULL == channel_a_status_ptr) {
            /* If channel_a_status_ptr equals NULL, FR_E_PARAM_POINTER shall be reported */
            /* to the DET and the API function shall return. */
            ssdk_printf(SSDK_ERR, "\r\nFr_ChannelAStatusPtr pointer is null\r\n");
            ret = SDRV_FLEXRAY_PARAM_POINTER;
        }
        else {
            if (NULL == channel_b_status_ptr) {
                /* If channel_b_status_ptr equals NULL, FR_E_PARAM_POINTER shall be reported */
                /* to the DET and the API function shall return. */
                ssdk_printf(SSDK_ERR, "\r\nFr_ChannelBStatusPtr pointer is null\r\n");
                ret = SDRV_FLEXRAY_PARAM_POINTER;
            }
            else {          /* No error yet -> execute the following code */

                ctrl_cfg = handle->internal_cfg->Fr_pController;

                if ((Std_ReturnType)(E_NOT_OK) == Fr_Driver_CheckCCAccess(ctrl_cfg,
                        (boolean)(TRUE))) {
                    /* CC is not accessible, report error */
                    ssdk_printf(SSDK_ERR, "\r\n flexray module is not access\r\n");
                }
                else {

                    u16tTmpPOCState = Fr_Driver_GetPOCState(ctrl_cfg);

                    /* Check whether the CC is synchronous to cluster */
                    if ((FLEXRAY_PSR0_PROTSTATE_NORMAL_PASSIVE_U16 == u16tTmpPOCState)
                            || (FLEXRAY_PSR0_PROTSTATE_NORMAL_ACTIVE_U16 == u16tTmpPOCState)) {
                        /* Read aggregated channel status, NIT staus and symbol window status */
                        Fr_Driver_GetChannelStatus(ctrl_cfg, channel_a_status_ptr,
                                                   channel_b_status_ptr);
                        ret = SDRV_STATUS_OK;
                    }
                    else {
                        ssdk_printf(SSDK_ERR, "\r\n flexray get poc state failure\r\n");
                    }
                }
            }
        }
    }

    return ret;
}

/***************************************************************************************************
* @function_name    sdrv_flexray_get_clock_correction
*
* @brief            Gets clock correction values
* @param[in]        handle - sdrv flexray handle
* @param[out]       rate_correction_ptr - Address where the current rate correction value shall
*                                          be stored.
* @param[out]       offset_correction_ptr - Address where the current offset correction value
*                                            shall be stored.
* @return           SDRV_STATUS_OK or error code.
***************************************************************************************************/
status_t sdrv_flexray_get_clock_correction(sdrv_flexray_t *handle,
        sint16 *rate_correction_ptr, sint32 *offset_correction_ptr)
{
    status_t ret = SDRV_STATUS_FAIL;
    const sdrv_flexray_ctrl_cfg_type *ctrl_cfg = NULL;

    ret = sdrv_flexray_parameter_check(handle);

    if (SDRV_STATUS_OK == ret) {
        if (NULL == rate_correction_ptr) {
            /* If channel_a_status_ptr equals NULL, FR_E_PARAM_POINTER shall be reported */
            /* to the DET and the API function shall return. */
            ssdk_printf(SSDK_ERR, "\r\n rate_correction_ptr pointer is null\r\n");
            ret = SDRV_FLEXRAY_PARAM_POINTER;
        }
        else {
            if (NULL == offset_correction_ptr) {
                /* If channel_b_status_ptr equals NULL, FR_E_PARAM_POINTER shall be reported */
                /* to the DET and the API function shall return. */
                ssdk_printf(SSDK_ERR, "\r\n offset_correction_ptr pointer is null\r\n");
                ret = SDRV_FLEXRAY_PARAM_POINTER;
            }
            else {

                ctrl_cfg = handle->internal_cfg->Fr_pController;

                if ((Std_ReturnType)(E_NOT_OK) == Fr_Driver_CheckCCAccess(ctrl_cfg,
                        (boolean)(TRUE))) {
                    /* CC is not accessible, report error */
                    ssdk_printf(SSDK_ERR, "\r\n flexray module is not access\r\n");
                }
                else {
                    /* Read clock correction values */
                    if ((Std_ReturnType)(E_OK) == Fr_Driver_GetClockCorrection(ctrl_cfg,
                            rate_correction_ptr, offset_correction_ptr)) {
                        ret = SDRV_STATUS_OK;
                    }
                    else {
                        ssdk_printf(SSDK_ERR, "\r\n flexray get clock correction failure\r\n");
                    }
                }
            }
        }
    }

    return ret;
}

/***************************************************************************************************
* @function_name    sdrv_flexray_get_sync_frame_list
*
* @brief            Gets clock correction values
* @param[in]        handle - sdrv flexray handle
* @param[in]        list_size - Size of the arrays passed via parameters:
*                                channel_a_even_list_ptr, channel_b_even_list_ptr,
*                                channel_a_odd_list_ptr channel_b_odd_list_ptr.
* @param[out]       channel_a_even_list_ptr - Address the list of syncframes on channel A within the
*                                            even communication cycle is written to.
* @param[out]       channel_b_even_list_ptr - Address the list of syncframes on channel B within the
*                                            even communication cycle is written to.
* @param[out]       channel_a_odd_list_ptr - Address the list of syncframes on channel A within the
*                                            odd communication cycle is written to.
* @param[out]       channel_b_odd_list_ptr - Address the list of syncframes on channel B within the
*                                           odd communication cycle is written to.
* @return           SDRV_STATUS_OK or error code.
***************************************************************************************************/
status_t sdrv_flexray_get_sync_frame_list(sdrv_flexray_t *handle,
        uint8_t list_size,
        uint16_t *channel_a_even_list_ptr,
        uint16_t *channel_b_even_list_ptr, uint16_t *channel_a_odd_list_ptr,
        uint16_t *channel_b_odd_list_ptr)
{
    status_t ret = SDRV_STATUS_FAIL;
    const sdrv_flexray_ctrl_cfg_type *ctrl_cfg = NULL;

    ret = sdrv_flexray_parameter_check(handle);

    if (SDRV_STATUS_OK == ret) {
        if (list_size > 15U) {
            /* list_size is larger than 15, FR_E_INV_FRAMELIST_SIZE shall be reported */
            ssdk_printf(SSDK_ERR, "\r\nFr_ListSize is larger than 15\r\n");
            ret = SDRV_FLEXRAY_INV_FRAMELIST_SIZE;
        }
        else {
            if (NULL == channel_a_even_list_ptr) {
                /* If channel_a_even_list_ptr equals NULL, FR_E_PARAM_POINTER shall be reported */
                /* to the DET and the API function shall return. */
                ssdk_printf(SSDK_ERR, "\r\n channel_a_even_list_ptr pointer is is null\r\n");
                ret = SDRV_FLEXRAY_PARAM_POINTER;
            }
            else {
                if (NULL == channel_b_even_list_ptr) {
                    /* If channel_b_even_list_ptr equals NULL, FR_E_PARAM_POINTER shall be reported */
                    /* to the DET and the API function shall return. */
                    ssdk_printf(SSDK_ERR, "\r\n channel_b_even_list_ptr pointer is is null\r\n");
                    ret = SDRV_FLEXRAY_PARAM_POINTER;
                }
                else {
                    if (NULL == channel_a_odd_list_ptr) {
                        /* If channel_a_odd_list_ptr equals NULL, FR_E_PARAM_POINTER shall be reported */
                        /* to the DET and the API function shall return. */
                        ssdk_printf(SSDK_ERR, "\r\n channel_a_odd_list_ptr pointer is null\r\n");
                        ret = SDRV_FLEXRAY_PARAM_POINTER;
                    }
                    else {
                        if (NULL == channel_b_odd_list_ptr) {
                            /* If channel_b_odd_list_ptr equals NULL, FR_E_PARAM_POINTER shall be reported */
                            /* to the DET and the API function shall return. */
                            ssdk_printf(SSDK_ERR, "\r\n channel_b_odd_list_ptr pointer is null\r\n");
                            ret = SDRV_FLEXRAY_PARAM_POINTER;
                        }
                        else {

                            ctrl_cfg = handle->internal_cfg->Fr_pController;

                            if ((Std_ReturnType)(E_NOT_OK) == Fr_Driver_CheckCCAccess(ctrl_cfg,
                                    (boolean)(TRUE))) {
                                /* CC is not accessible, report error */
                                ssdk_printf(SSDK_ERR, "\r\n flexray module is not access\r\n");
                            }
                            else {
                                /* Check list_size parameter is higher than 0 */
                                if (list_size > 0U) {
                                    /* Get list of Sync frames */
                                    if ((Std_ReturnType)(E_OK) == Fr_Driver_GetSyncFrameList(ctrl_cfg, list_size,
                                            channel_a_even_list_ptr
                                            , channel_b_even_list_ptr, channel_a_odd_list_ptr
                                            , channel_b_odd_list_ptr)) {
                                        ret = SDRV_STATUS_OK;
                                    }
                                }
                                else {
                                    /* do nothing */
                                    ssdk_printf(SSDK_ERR, "\r\n list_size is less than 0\r\n");
                                    ret = SDRV_FLEXRAY_INV_FRAMELIST_SIZE;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    return ret;
}

/***************************************************************************************************
* @function_name    sdrv_flexray_get_wakeup_rx_status
*
* @brief            Gets clock correction values
* @param[in]        handle - sdrv flexray handle
*
* @param[out]       wakeup_rx_status_ptr - Address where bitcoded wakeup reception status shall be
*                                          stored.
* @return           SDRV_STATUS_OK or error code.
***************************************************************************************************/
status_t sdrv_flexray_get_wakeup_rx_status(sdrv_flexray_t *handle,
        uint8_t *wakeup_rx_status_ptr)
{
    status_t ret = SDRV_STATUS_FAIL;
    const sdrv_flexray_ctrl_cfg_type *ctrl_cfg = NULL;

    ret = sdrv_flexray_parameter_check(handle);

    if (SDRV_STATUS_OK == ret) {
        if (NULL == wakeup_rx_status_ptr) {
            /* If channel_a_status_ptr equals NULL, FR_E_PARAM_POINTER shall be reported */
            /* to the DET and the API function shall return. */
            ssdk_printf(SSDK_ERR, "\r\n wakeup_rx_status_ptr pointer is null\r\n");
            ret = SDRV_FLEXRAY_PARAM_POINTER;
        }
        else {

            ctrl_cfg = handle->internal_cfg->Fr_pController;

            if ((Std_ReturnType)(E_NOT_OK) == Fr_Driver_CheckCCAccess(ctrl_cfg,
                    (boolean)TRUE)) {
                /* CC is not accessible, report error */
                ssdk_printf(SSDK_ERR, "\r\n flexray module is not access\r\n");
            }
            else {
                if ((Std_ReturnType)(E_OK) == Fr_Driver_GetWakeupRxStatus(ctrl_cfg,
                        wakeup_rx_status_ptr)) {
                    ret = SDRV_STATUS_OK;
                }
            }
        }
    }

    return ret;
}

/***************************************************************************************************
* @function_name    sdrv_flexray_cancel_absolute_timer
*
* @brief            Stops an absolute timer
* @param[in]        sdrv flexray handle
* @param[in]        abs_timer_idx Index of absolute timer within the context of the flexray CC
* @return           SDRV_STATUS_OK or error code.
***************************************************************************************************/
status_t sdrv_flexray_cancel_absolute_timer(sdrv_flexray_t *handle,
        uint8_t abs_timer_idx)
{
    status_t ret = SDRV_STATUS_FAIL;
    const sdrv_flexray_ctrl_cfg_type *ctrl_cfg = NULL;

    ret = sdrv_flexray_parameter_check(handle);

    if (SDRV_STATUS_OK == ret) {
        if (!(abs_timer_idx < handle->abs_timers_num)) {
            /* An attempt to configure an invalid timer, abs_timer_idx has an invalid value */
            ssdk_printf(SSDK_ERR, "\r\n abs_timer_idx has an invalid value\r\n");
            ret = SDRV_FLEXRAY_INV_TIMER_IDX;
        }
        else {   /* No error yet -> execute the following code */

            ctrl_cfg = handle->internal_cfg->Fr_pController;

            if ((Std_ReturnType)(E_NOT_OK) == Fr_Driver_CheckCCAccess(ctrl_cfg,
                    (boolean)(TRUE))) {
                /* CC is not accessible, report error */
                ssdk_printf(SSDK_ERR, "\r\n flexray module is not access\r\n");
            }
            else {
                /* Call LLD function to cancel the Absolute Timer */
                Fr_Driver_CancelAbsoluteTimer(ctrl_cfg, abs_timer_idx);
                ret = SDRV_STATUS_OK;    /* API call was successful */
            } /* CC access check */
        }
    }

    return ret;
}

/***************************************************************************************************
* @function_name    sdrv_flexray_read_cc_config
*
* @brief            Reconfigures physical resource
* @param[in]        sdrv flexray handle
* @param[in]        config_paramter_idx Index that identifies the configuration parameter
*                                          to read. See macros FR_CIDX_<config_parameter_name>.
* @param[out]       config_para_value_ptr Address the output value is stored to.
* @return           SDRV_STATUS_OK or error code.
***************************************************************************************************/
status_t sdrv_flexray_read_cc_config(sdrv_flexray_t *handle,
                                     uint8_t config_paramter_idx, uint32_t *config_para_value_ptr)
{
    status_t ret = SDRV_STATUS_FAIL;
    const sdrv_flexray_ctrl_cfg_type *ctrl_cfg = NULL;

    ret = sdrv_flexray_parameter_check(handle);

    if (SDRV_STATUS_OK == ret) {
        if (config_para_value_ptr == NULL) {
            /* config_para_value_ptr has an invalid value */
            ssdk_printf(SSDK_ERR, "\r\n config_para_value_ptr pointer is null\r\n");
            ret = SDRV_FLEXRAY_PARAM_POINTER;
        }
        else {

            ctrl_cfg = handle->internal_cfg->Fr_pController;

            if ((Std_ReturnType)(E_NOT_OK) == Fr_Driver_ReadCCConfig(ctrl_cfg)) {
                /* CC contains error in the configuration */
                ssdk_printf(SSDK_ERR, "\r\n read CC config failure\r\n");
            }
            else {
                ret = SDRV_STATUS_OK;

                switch (config_paramter_idx) {
                    case FR_CIDX_GDCYCLE:
                    case FR_CIDX_PMICROPERCYCLE:
                    case FR_CIDX_PDLISTENTIMEOUT:
                    case FR_CIDX_GMACROPERCYCLE:
                    case FR_CIDX_GDMACROTICK:
                    case FR_CIDX_GNUMBEROFMINISLOTS:
                    case FR_CIDX_GNUMBEROFSTATICSLOTS:
                    case FR_CIDX_GDNIT:
                    case FR_CIDX_GDSTATICSLOT:
                    case FR_CIDX_GDWAKEUPRXWINDOW:
                    case FR_CIDX_PKEYSLOTID:
                    case FR_CIDX_PLATESTTX:
                    case FR_CIDX_POFFSETCORRECTIONOUT:
                    case FR_CIDX_POFFSETCORRECTIONSTART:
                    case FR_CIDX_PRATECORRECTIONOUT:
                    case FR_CIDX_PSECONDKEYSLOTID:
                    case FR_CIDX_PDACCEPTEDSTARTUPRANGE:
                    case FR_CIDX_GCOLDSTARTATTEMPTS:
                    case FR_CIDX_GCYCLECOUNTMAX:
                    case FR_CIDX_GLISTENNOISE:
                    case FR_CIDX_GMAXWITHOUTCLOCKCORRECTFATAL:
                    case FR_CIDX_GMAXWITHOUTCLOCKCORRECTPASSIVE:
                    case FR_CIDX_GNETWORKMANAGEMENTVECTORLENGTH:
                    case FR_CIDX_GPAYLOADLENGTHSTATIC:
                    case FR_CIDX_GSYNCFRAMEIDCOUNTMAX:
                    case FR_CIDX_GDACTIONPOINTOFFSET:
                    case FR_CIDX_GDBIT:
                    case FR_CIDX_GDCASRXLOWMAX:
                    case FR_CIDX_GDDYNAMICSLOTIDLEPHASE:
                    case FR_CIDX_GDMINISLOTACTIONPOINTOFFSET:
                    case FR_CIDX_GDMINISLOT:
                    case FR_CIDX_GDSAMPLECLOCKPERIOD:
                    case FR_CIDX_GDSYMBOLWINDOW:
                    case FR_CIDX_GDSYMBOLWINDOWACTIONPOINTOFFSET:
                    case FR_CIDX_GDTSSTRANSMITTER:
                    case FR_CIDX_GDWAKEUPRXIDLE:
                    case FR_CIDX_GDWAKEUPRXLOW:
                    case FR_CIDX_GDWAKEUPTXACTIVE:
                    case FR_CIDX_GDWAKEUPTXIDLE:
                    case FR_CIDX_PALLOWPASSIVETOACTIVE:
                    case FR_CIDX_PCHANNELS:
                    case FR_CIDX_PCLUSTERDRIFTDAMPING:
                    case FR_CIDX_PDECODINGCORRECTION:
                    case FR_CIDX_PDELAYCOMPENSATIONA:
                    case FR_CIDX_PDELAYCOMPENSATIONB:
                    case FR_CIDX_PMACROINITIALOFFSETA:
                    case FR_CIDX_PMACROINITIALOFFSETB:
                    case FR_CIDX_PMICROINITIALOFFSETA:
                    case FR_CIDX_PMICROINITIALOFFSETB:
                    case FR_CIDX_PPAYLOADLENGTHDYNMAX:
                    case FR_CIDX_PSAMPLESPERMICROTICK:
                    case FR_CIDX_PWAKEUPCHANNEL:
                    case FR_CIDX_PWAKEUPPATTERN:
                    case FR_CIDX_PDMICROTICK:
                    case FR_CIDX_GDIGNOREAFTERTX:
                    case FR_CIDX_PALLOWHALTDUETOCLOCK:
                    case FR_CIDX_PEXTERNALSYNC:
                    case FR_CIDX_PFALLBACKINTERNAL:
                    case FR_CIDX_PKEYSLOTONLYENABLED:
                    case FR_CIDX_PKEYSLOTUSEDFORSTARTUP:
                    case FR_CIDX_PKEYSLOTUSEDFORSYNC:
                    case FR_CIDX_PNMVECTOREARLYUPDATE:
                    case FR_CIDX_PTWOKEYSLOTMODE:
                        *config_para_value_ptr = ctrl_cfg->CCReadBackConfigSetPtr[config_paramter_idx];
                        break;
                    default:
                        /* config_paramter_idx has an invalid value */
                        ssdk_printf(SSDK_ERR, "\r\n config_paramter_idx has an invalid value\r\n");
                        ret = SDRV_FLEXRAY_INV_CONFIG_IDX;
                        break;
                }
            }
        }
    }

    return ret;
}

/***************************************************************************************************
* @function_name    sdrv_flexray_enable_absolute_timer_irq
*
* @brief            Enables absolute timer interrupts
* @param[in]        ctrl_idx      Index of flexray CC within the context of the flexray Driver
* @param[in]        abs_timer_idx  Index of absolute timer within the context of the flexray C
* @return           SDRV_STATUS_OK or error code.
***************************************************************************************************/
status_t sdrv_flexray_enable_absolute_timer_irq(sdrv_flexray_t *handle,
        uint8_t abs_timer_idx)
{
    status_t ret = SDRV_STATUS_FAIL;
    const sdrv_flexray_ctrl_cfg_type *ctrl_cfg = NULL;

    ret = sdrv_flexray_parameter_check(handle);

    if (SDRV_STATUS_OK == ret) {
        if (!(abs_timer_idx < handle->abs_timers_num)) {
            /* An attempt to configure an invalid timer, abs_timer_idx has an invalid value */
            ssdk_printf(SSDK_ERR, "\r\n abs_timer_idx has an invalid value\r\n");
            ret = SDRV_FLEXRAY_INV_TIMER_IDX;
        }
        else {

            ctrl_cfg = handle->internal_cfg->Fr_pController;

            if ((Std_ReturnType)(E_NOT_OK) == Fr_Driver_CheckCCAccess(ctrl_cfg,
                    (boolean)(TRUE))) {
                /* CC is not accessible, report error */
                ssdk_printf(SSDK_ERR, "\r\n flexray module is not access\r\n");
            }
            else {
                /* Call LLD function to enable the Absolute Timer */
                Fr_Driver_EnableAbsTimerIRQ(ctrl_cfg, abs_timer_idx);
                ret = SDRV_STATUS_OK;    /* API call was successful */
            }  /* CC access check */
        }
    }

    return ret;
}

/***************************************************************************************************
* @function_name    sdrv_flexray_ack_absolute_timer_irq
*
* @brief            Clears absolute timer interrupt flag
* @param[in]        sdrv flexray handle
* @param[in]        abs_timer_idx Index of absolute timer within the context of the flexray CC
* @return           SDRV_STATUS_OK or error code.
***************************************************************************************************/
status_t sdrv_flexray_ack_absolute_timer_irq(sdrv_flexray_t *handle,
        uint8_t abs_timer_idx)
{
    status_t ret = SDRV_STATUS_FAIL;
    const sdrv_flexray_ctrl_cfg_type *ctrl_cfg = NULL;

    ret = sdrv_flexray_parameter_check(handle);

    if (SDRV_STATUS_OK == ret) {
        if (!(abs_timer_idx < handle->abs_timers_num)) {
            /* An attempt to configure an invalid timer, abs_timer_idx has an invalid value */
            ssdk_printf(SSDK_ERR, "\r\n abs_timer_idx has an invalid value\r\n");
            ret = SDRV_FLEXRAY_INV_TIMER_IDX;
        }
        else {

            ctrl_cfg = handle->internal_cfg->Fr_pController;

            if ((Std_ReturnType)(E_NOT_OK) == Fr_Driver_CheckCCAccess(ctrl_cfg,
                    (boolean)(TRUE))) {
                /* CC is not accessible, report error */
                ssdk_printf(SSDK_ERR, "\r\n flexray module is not access\r\n");
            }
            else {
                /* Call LLD function to ack the Absolute Timer */
                Fr_Driver_AckAbsTimerIRQ(ctrl_cfg, abs_timer_idx);

                ret = SDRV_STATUS_OK;   /* API call was successful */
            } /* CC access check */
        }
    }

    return ret;
}

/***************************************************************************************************
* @function_name    sdrv_flexray_disable_absolute_timer_irq
*
* @brief            Disables absolute timer interrupt generation
* @param[in]        handle - sdrv flexray handle
* @param[in]        abs_timer_idx - Index of absolute timer within the context of the flexray CC
* @return           SDRV_STATUS_OK or error code.
***************************************************************************************************/
status_t sdrv_flexray_disable_absolute_timer_irq(sdrv_flexray_t *handle,
        uint8_t abs_timer_idx)
{
    status_t ret = SDRV_STATUS_FAIL;
    const sdrv_flexray_ctrl_cfg_type *ctrl_cfg = NULL;

    ret = sdrv_flexray_parameter_check(handle);

    if (SDRV_STATUS_OK == ret) {
        if (!(abs_timer_idx <
                handle->abs_timers_num)) { /* Check the abs_timer_idx value */
            /* An attempt to configure an invalid timer, abs_timer_idx has an invalid value */
            ssdk_printf(SSDK_ERR, "\r\n abs_timer_idx has an invalid value\r\n");
            ret = SDRV_FLEXRAY_INV_TIMER_IDX;
        }
        else {

            ctrl_cfg = handle->internal_cfg->Fr_pController;

            if ((Std_ReturnType)(E_NOT_OK) == Fr_Driver_CheckCCAccess(ctrl_cfg,
                    (boolean)(TRUE))) {
                /* CC is not accessible, report error */
                ssdk_printf(SSDK_ERR, "\r\n flexray module is not access\r\n");
            }
            else {
                /* Call LLD function to disable the Absolute Timer */
                Fr_Driver_DisableAbsTimerIRQ(ctrl_cfg, abs_timer_idx);

                ret = SDRV_STATUS_OK; /* API call was successful */
            } /* CC access check */
        }
    }

    return ret;
}

/***************************************************************************************************
* @function_name    sdrv_flexray_get_absolute_timer_irq_status
*
* @brief            Checks if the absolute timer flag is set
* @param[in]        handle - sdrv flexray handle
* @param[in]        abs_timer_idx - Index of absolute timer within the context of the flexray CC
* @param[out]       irq_status_ptr - Address the output value is stored to
* @return           SDRV_STATUS_OK or error code.
***************************************************************************************************/
status_t sdrv_flexray_get_absolute_timer_irq_status(sdrv_flexray_t *handle,
        uint8_t abs_timer_idx,
        boolean *irq_status_ptr)
{
    status_t ret = SDRV_STATUS_FAIL;
    const sdrv_flexray_ctrl_cfg_type *ctrl_cfg = NULL;

    ret = sdrv_flexray_parameter_check(handle);

    if (SDRV_STATUS_OK == ret) {
        if (!(abs_timer_idx <
                handle->abs_timers_num)) { /* Check the abs_timer_idx value */
            /* An attempt to configure an invalid timer, abs_timer_idx has an invalid value */
            ssdk_printf(SSDK_ERR, "\r\n abs_timer_idx has an invalid value\r\n");
            ret = SDRV_FLEXRAY_INV_TIMER_IDX;
        }
        else {
            if (NULL == irq_status_ptr) {
                /* If irq_status_ptr equals NULL, FR_E_PARAM_POINTER shall be reported */
                /* to the DET and the API function shall return. */
                ssdk_printf(SSDK_ERR, "\r\nFr_IRQStatusPtr pointer is null\r\n");
                ret = SDRV_FLEXRAY_PARAM_POINTER;
            }
            else {   /* No error yet -> execute the following code */

                ctrl_cfg = handle->internal_cfg->Fr_pController;

                if ((Std_ReturnType)(E_NOT_OK) == Fr_Driver_CheckCCAccess(ctrl_cfg,
                        (boolean)(TRUE))) {
                    /* CC is not accessible, report error */
                    ssdk_printf(SSDK_ERR, "\r\n flexray module is not access\r\n");
                }
                else {
                    /* Call LLD function to get Asolute Timer IRQ status */
                    *irq_status_ptr = Fr_Driver_GetAbsTimerIRQStatus(ctrl_cfg, abs_timer_idx);

                    ret = SDRV_STATUS_OK; /* API call was successful */

                } /* CC access check */
            }
        }
    }

    return ret;
}

/***************************************************************************************************
* @function_name    sdrv_flexray_interrupt_enable
*
* @brief            open flexray interrupt
* @param[in]        handle - sdrv flexray handle
* @param[in]        interrupt_source - interrupt source
* @return           SDRV_STATUS_OK or error code.
***************************************************************************************************/
status_t sdrv_flexray_interrupt_enable(sdrv_flexray_t *handle,
                                       sdrv_flexray_global_interrupt_type interrupt_source)
{
    status_t ret = SDRV_STATUS_FAIL;
    const sdrv_flexray_ctrl_cfg_type *ctrl_cfg = NULL;
    uint16_t regVal = 0U;

    ret = sdrv_flexray_parameter_check(handle);

    if (SDRV_STATUS_OK == ret) {

        ctrl_cfg = handle->internal_cfg->Fr_pController;

        if ((Std_ReturnType)(E_NOT_OK) == Fr_Driver_CheckCCAccess(ctrl_cfg,
                (boolean)(TRUE))) {
            /* CC is not accessible, report error */
            ssdk_printf(SSDK_ERR, "\r\n flexray module is not access\r\n");
        }
        else {

            switch (interrupt_source) {
                case FR_MODULEIE:
                    regVal = FLEXRAY_GIFER_MIE_U16;
                    break;

                case FR_PROTOCOLIE:
                    regVal = FLEXRAY_GIFER_PRIE_U16;
                    break;

                case FR_CHIIE:
                    regVal = FLEXRAY_GIFER_CHIIE_U16;
                    break;

                case FR_WAKEUPIE:
                    regVal = FLEXRAY_GIFER_WUPIE_U16;
                    break;

                case FR_FIFOBIE:
                    regVal = FLEXRAY_GIFER_FNEBIE_U16;
                    break;

                case FR_FIFOAIE:
                    regVal = FLEXRAY_GIFER_FNEAIE_U16;
                    break;

                case FR_RECEIVEIE:
                    regVal = FLEXRAY_GIFER_RBIE_U16;
                    break;

                case FR_TRANSMITIE:
                    regVal = FLEXRAY_GIFER_TBIE_U16;
                    break;

                default:
                    break;
            }

            if (regVal != ((uint16_t)0)) {
                Fr_Driver_InterruptEnable(ctrl_cfg, regVal);
                ret = SDRV_STATUS_OK;
            }
        }
    }

    return ret;
}

/***************************************************************************************************
* @function_name    sdrv_flexray_interrupt_disable
*
* @brief            disable flexray interrupt
* @param[in]        handle - sdrv flexray handle
* @param[in]        interrupt_source - interrupt source
* @return           SDRV_STATUS_OK or error code.
***************************************************************************************************/
status_t sdrv_flexray_interrupt_disable(sdrv_flexray_t *handle,
                                        sdrv_flexray_global_interrupt_type interrupt_source)
{
    status_t ret = SDRV_STATUS_FAIL;
    const sdrv_flexray_ctrl_cfg_type *ctrl_cfg = NULL;
    uint16_t regVal = 0U;

    ret = sdrv_flexray_parameter_check(handle);

    if (SDRV_STATUS_OK == ret) {
        ctrl_cfg = handle->internal_cfg->Fr_pController;

        if ((Std_ReturnType)(E_NOT_OK) == Fr_Driver_CheckCCAccess(ctrl_cfg,
                (boolean)(TRUE))) {
            /* CC is not accessible, report error */
            ssdk_printf(SSDK_ERR, "\r\n flexray module is not access\r\n");
        }
        else {

            switch (interrupt_source) {
                case FR_MODULEIE:
                    regVal = FLEXRAY_GIFER_MIE_U16;
                    break;

                case FR_PROTOCOLIE:
                    regVal = FLEXRAY_GIFER_PRIE_U16;
                    break;

                case FR_CHIIE:
                    regVal = FLEXRAY_GIFER_CHIIE_U16;
                    break;

                case FR_WAKEUPIE:
                    regVal = FLEXRAY_GIFER_WUPIE_U16;
                    break;

                case FR_FIFOBIE:
                    regVal = FLEXRAY_GIFER_FNEBIE_U16;
                    break;

                case FR_FIFOAIE:
                    regVal = FLEXRAY_GIFER_FNEAIE_U16;
                    break;

                case FR_RECEIVEIE:
                    regVal = FLEXRAY_GIFER_RBIE_U16;
                    break;

                case FR_TRANSMITIE:
                    regVal = FLEXRAY_GIFER_TBIE_U16;
                    break;

                default:
                    break;
            }

            if (regVal != ((uint16_t)0)) {
                Fr_Driver_InterruptDisable(ctrl_cfg, regVal);
                ret = E_OK;
            }
        }
    }

    return ret;
}
