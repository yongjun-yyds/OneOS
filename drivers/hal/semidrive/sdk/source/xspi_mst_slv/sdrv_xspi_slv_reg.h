/*
* sdrv_xspi_slv_reg.h
*
* Copyright (c) 2021 Semidrive Semiconductor.
* All rights reserved.
*
* Description: implement semidrive xspi slave drv head file
*
*/

#ifndef SDRV_XSPI_SLAVE_H_
#define SDRV_XSPI_SLAVE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <param.h>
#include <reg.h>
#include <types.h>

#define XSPI_SLV_MODE_CTRL_BASE (0x0u)
#define XSPI_SLV_MODE_CTRL_XSPI_SLV_EN_WIDTH (1u)
#define XSPI_SLV_MODE_CTRL_XSPI_SLV_EN_OFFSET (31u)
#define XSPI_SLV_MODE_CTRL_SOFT_RESET_WIDTH (1u)
#define XSPI_SLV_MODE_CTRL_SOFT_RESET_OFFSET (30u)
#define XSPI_SLV_MODE_CTRL_CRC_EN_WIDTH (1u)
#define XSPI_SLV_MODE_CTRL_CRC_EN_OFFSET (23u)
#define XSPI_SLV_MODE_CTRL_BIT_ENDIAN_WIDTH (1u)
#define XSPI_SLV_MODE_CTRL_BIT_ENDIAN_OFFSET (22u)
#define XSPI_SLV_MODE_CTRL_DUMMY_CYCLE_WIDTH (3u)
#define XSPI_SLV_MODE_CTRL_DUMMY_CYCLE_OFFSET (19u)
#define XSPI_SLV_MODE_CTRL_DIRECT_DECODE_BUSY_WIDTH (1u)
#define XSPI_SLV_MODE_CTRL_DIRECT_DECODE_BUSY_OFFSET (18u)
#define XSPI_SLV_MODE_CTRL_DIRECT_ENCODE_BUSY_WIDTH (1u)
#define XSPI_SLV_MODE_CTRL_DIRECT_ENCODE_BUSY_OFFSET (17u)
#define XSPI_SLV_MODE_CTRL_INDIRECT_BUSY_WIDTH (1u)
#define XSPI_SLV_MODE_CTRL_INDIRECT_BUSY_OFFSET (16u)
#define XSPI_SLV_MODE_CTRL_PAD_RESET_MASK_WIDTH (1u)
#define XSPI_SLV_MODE_CTRL_PAD_RESET_MASK_OFFSET (10u)
#define XSPI_SLV_MODE_CTRL_WRITE_CHK_EN_WIDTH (1u)
#define XSPI_SLV_MODE_CTRL_WRITE_CHK_EN_OFFSET (9u)
#define XSPI_SLV_MODE_CTRL_TX_MODE_WIDTH (1u)
#define XSPI_SLV_MODE_CTRL_TX_MODE_OFFSET (8u)
#define XSPI_SLV_MODE_CTRL_WRITE_EN_WIDTH (1u)
#define XSPI_SLV_MODE_CTRL_WRITE_EN_OFFSET (2u)
#define XSPI_SLV_MODE_CTRL_READ_EN_WIDTH (1u)
#define XSPI_SLV_MODE_CTRL_READ_EN_OFFSET (1u)
#define XSPI_SLV_MODE_CTRL_MODE_WIDTH (1u)
#define XSPI_SLV_MODE_CTRL_MODE_OFFSET (0u)


#define XSPI_SLV_SLV_SET_BASE (0x4u)
#define XSPI_SLV_SLV_SET_SCLK_IDLE_TIME_EN_WIDTH (1u)
#define XSPI_SLV_SLV_SET_SCLK_IDLE_TIME_EN_OFFSET (16u)
#define XSPI_SLV_SLV_SET_SCLK_IDLE_TIME_WIDTH (8u)
#define XSPI_SLV_SLV_SET_SCLK_IDLE_TIME_OFFSET (8u)
#define XSPI_SLV_SLV_SET_RATE_WIDTH (1u)
#define XSPI_SLV_SLV_SET_RATE_OFFSET (2u)
#define XSPI_SLV_SLV_SET_LINE_WIDTH (2u)
#define XSPI_SLV_SLV_SET_LINE_OFFSET (0u)


#define XSPI_SLV_DEBOUNCE_CTRL_BASE (0x8u)
#define XSPI_SLV_DEBOUNCE_CTRL_TIME_WIDTH (4u)
#define XSPI_SLV_DEBOUNCE_CTRL_TIME_OFFSET (4u)
#define XSPI_SLV_DEBOUNCE_CTRL_EN_WIDTH (1u)
#define XSPI_SLV_DEBOUNCE_CTRL_EN_OFFSET (0u)


#define XSPI_SLV_TIMEOUT_CTRL_BASE (0x10u)
#define XSPI_SLV_TIMEOUT_CTRL_EN_WIDTH (1u)
#define XSPI_SLV_TIMEOUT_CTRL_EN_OFFSET (31u)
#define XSPI_SLV_TIMEOUT_CTRL_TIME_WIDTH (16u)
#define XSPI_SLV_TIMEOUT_CTRL_TIME_OFFSET (0u)


#define XSPI_SLV_STATUS_ACCESS_BASE (0x14u)
#define XSPI_SLV_STATUS_ACCESS_EN_RESET_WIDTH (1u)
#define XSPI_SLV_STATUS_ACCESS_EN_RESET_OFFSET (13u)
#define XSPI_SLV_STATUS_ACCESS_EN_SEQ_ID_ERR_WIDTH (1u)
#define XSPI_SLV_STATUS_ACCESS_EN_SEQ_ID_ERR_OFFSET (12u)
#define XSPI_SLV_STATUS_ACCESS_EN_PERMISSION_ERR_WIDTH (1u)
#define XSPI_SLV_STATUS_ACCESS_EN_PERMISSION_ERR_OFFSET (11u)
#define XSPI_SLV_STATUS_ACCESS_EN_TIMEOUT_ERR_WIDTH (1u)
#define XSPI_SLV_STATUS_ACCESS_EN_TIMEOUT_ERR_OFFSET (10u)
#define XSPI_SLV_STATUS_ACCESS_EN_READ_ERR_WIDTH (1u)
#define XSPI_SLV_STATUS_ACCESS_EN_READ_ERR_OFFSET (9u)
#define XSPI_SLV_STATUS_ACCESS_EN_WRITE_ERR_WIDTH (1u)
#define XSPI_SLV_STATUS_ACCESS_EN_WRITE_ERR_OFFSET (8u)
#define XSPI_SLV_STATUS_ACCESS_EN_DUMMY_CYCLE_WIDTH (3u)
#define XSPI_SLV_STATUS_ACCESS_EN_DUMMY_CYCLE_OFFSET (5u)
#define XSPI_SLV_STATUS_ACCESS_EN_READ_DONE_WIDTH (1u)
#define XSPI_SLV_STATUS_ACCESS_EN_READ_DONE_OFFSET (2u)
#define XSPI_SLV_STATUS_ACCESS_EN_WRITE_DONE_WIDTH (1u)
#define XSPI_SLV_STATUS_ACCESS_EN_WRITE_DONE_OFFSET (1u)

#define XSPI_SLV_ID0_BASE (0x20u)

#define XSPI_SLV_ID1_BASE (0x24u)

#define XSPI_SLV_PAD_INTB_MASK_BASE (0x28u)

#define XSPI_SLV_PAD_CRCB_MASK_BASE (0x2cu)

#define XSPI_SLV_INT_ST_BASE (0x30u)
#define XSPI_SLV_INT_ST_FORCE_INT_WIDTH (1u)
#define XSPI_SLV_INT_ST_FORCE_INT_OFFSET (31u)
#define XSPI_SLV_INT_ST_AXI_R_UTID_ERR_WIDTH (1u)
#define XSPI_SLV_INT_ST_AXI_R_UTID_ERR_OFFSET (17u)
#define XSPI_SLV_INT_ST_AXI_W_UTID_ERR_WIDTH (1u)
#define XSPI_SLV_INT_ST_AXI_W_UTID_ERR_OFFSET (16u)
#define XSPI_SLV_INT_ST_INDIRECT_STATUS_ERR_WIDTH (1u)
#define XSPI_SLV_INT_ST_INDIRECT_STATUS_ERR_OFFSET (15u)
#define XSPI_SLV_INT_ST_INDIRECT_CMD_ERR_WIDTH (1u)
#define XSPI_SLV_INT_ST_INDIRECT_CMD_ERR_OFFSET (14u)
#define XSPI_SLV_INT_ST_INDIRECT_ADDR_ERR_WIDTH (1u)
#define XSPI_SLV_INT_ST_INDIRECT_ADDR_ERR_OFFSET (13u)
#define XSPI_SLV_INT_ST_AR_ILLEGAL_WIDTH (1u)
#define XSPI_SLV_INT_ST_AR_ILLEGAL_OFFSET (12u)
#define XSPI_SLV_INT_ST_AW_ILLEGAL_WIDTH (1u)
#define XSPI_SLV_INT_ST_AW_ILLEGAL_OFFSET (11u)
#define XSPI_SLV_INT_ST_TIMEOUT_ERR_WIDTH (1u)
#define XSPI_SLV_INT_ST_TIMEOUT_ERR_OFFSET (10u)
#define XSPI_SLV_INT_ST_DIRECT_CRC_ERR_WIDTH (1u)
#define XSPI_SLV_INT_ST_DIRECT_CRC_ERR_OFFSET (9u)
#define XSPI_SLV_INT_ST_INDIRECT_CRC_ERR_WIDTH (1u)
#define XSPI_SLV_INT_ST_INDIRECT_CRC_ERR_OFFSET (8u)
#define XSPI_SLV_INT_ST_AXI_R_MISMATCH_WIDTH (1u)
#define XSPI_SLV_INT_ST_AXI_R_MISMATCH_OFFSET (7u)
#define XSPI_SLV_INT_ST_AXI_R_ERR_WIDTH (1u)
#define XSPI_SLV_INT_ST_AXI_R_ERR_OFFSET (6u)
#define XSPI_SLV_INT_ST_WRITE_LEN_ERR_WIDTH (1u)
#define XSPI_SLV_INT_ST_WRITE_LEN_ERR_OFFSET (5u)
#define XSPI_SLV_INT_ST_WRITE_ID_ERR_WIDTH (1u)
#define XSPI_SLV_INT_ST_WRITE_ID_ERR_OFFSET (4u)
#define XSPI_SLV_INT_ST_AXI_W_ERR_WIDTH (1u)
#define XSPI_SLV_INT_ST_AXI_W_ERR_OFFSET (3u)
#define XSPI_SLV_INT_ST_SEQ_ID_ERR_WIDTH (1u)
#define XSPI_SLV_INT_ST_SEQ_ID_ERR_OFFSET (2u)
#define XSPI_SLV_INT_ST_READ_DONE_WIDTH (1u)
#define XSPI_SLV_INT_ST_READ_DONE_OFFSET (1u)
#define XSPI_SLV_INT_ST_WRITE_DONE_WIDTH (1u)
#define XSPI_SLV_INT_ST_WRITE_DONE_OFFSET (0u)


#define XSPI_SLV_INT_MASK_BASE (0x34u)
#define XSPI_SLV_INT_MASK_AXI_R_UTID_ERR_WIDTH (1u)
#define XSPI_SLV_INT_MASK_AXI_R_UTID_ERR_OFFSET (17u)
#define XSPI_SLV_INT_MASK_AXI_W_UTID_ERR_WIDTH (1u)
#define XSPI_SLV_INT_MASK_AXI_W_UTID_ERR_OFFSET (16u)
#define XSPI_SLV_INT_MASK_INDIRECT_STATUS_ERR_WIDTH (1u)
#define XSPI_SLV_INT_MASK_INDIRECT_STATUS_ERR_OFFSET (15u)
#define XSPI_SLV_INT_MASK_INDIRECT_CMD_ERR_WIDTH (1u)
#define XSPI_SLV_INT_MASK_INDIRECT_CMD_ERR_OFFSET (14u)
#define XSPI_SLV_INT_MASK_INDIRECT_ADDR_ERR_WIDTH (1u)
#define XSPI_SLV_INT_MASK_INDIRECT_ADDR_ERR_OFFSET (13u)
#define XSPI_SLV_INT_MASK_AR_ILLEGAL_WIDTH (1u)
#define XSPI_SLV_INT_MASK_AR_ILLEGAL_OFFSET (12u)
#define XSPI_SLV_INT_MASK_AW_ILLEGAL_WIDTH (1u)
#define XSPI_SLV_INT_MASK_AW_ILLEGAL_OFFSET (11u)
#define XSPI_SLV_INT_MASK_TIMEOUT_ERR_WIDTH (1u)
#define XSPI_SLV_INT_MASK_TIMEOUT_ERR_OFFSET (10u)
#define XSPI_SLV_INT_MASK_DIRECT_CRC_ERR_WIDTH (1u)
#define XSPI_SLV_INT_MASK_DIRECT_CRC_ERR_OFFSET (9u)
#define XSPI_SLV_INT_MASK_INDIRECT_CRC_ERR_WIDTH (1u)
#define XSPI_SLV_INT_MASK_INDIRECT_CRC_ERR_OFFSET (8u)
#define XSPI_SLV_INT_MASK_AXI_R_MISMATCH_WIDTH (1u)
#define XSPI_SLV_INT_MASK_AXI_R_MISMATCH_OFFSET (7u)
#define XSPI_SLV_INT_MASK_AXI_R_ERR_WIDTH (1u)
#define XSPI_SLV_INT_MASK_AXI_R_ERR_OFFSET (6u)
#define XSPI_SLV_INT_MASK_WRITE_LEN_ERR_WIDTH (1u)
#define XSPI_SLV_INT_MASK_WRITE_LEN_ERR_OFFSET (5u)
#define XSPI_SLV_INT_MASK_WRITE_ID_ERR_WIDTH (1u)
#define XSPI_SLV_INT_MASK_WRITE_ID_ERR_OFFSET (4u)
#define XSPI_SLV_INT_MASK_AXI_W_ERR_WIDTH (1u)
#define XSPI_SLV_INT_MASK_AXI_W_ERR_OFFSET (3u)
#define XSPI_SLV_INT_MASK_SEQ_ID_ERR_WIDTH (1u)
#define XSPI_SLV_INT_MASK_SEQ_ID_ERR_OFFSET (2u)
#define XSPI_SLV_INT_MASK_READ_DONE_WIDTH (1u)
#define XSPI_SLV_INT_MASK_READ_DONE_OFFSET (1u)
#define XSPI_SLV_INT_MASK_WRITE_DONE_WIDTH (1u)
#define XSPI_SLV_INT_MASK_WRITE_DONE_OFFSET (0u)


#define XSPI_SLV_STATUS_BASE (0x38u)
#define XSPI_SLV_STATUS_INDIRECT_ERR_WIDTH (1u)
#define XSPI_SLV_STATUS_INDIRECT_ERR_OFFSET (14u)
#define XSPI_SLV_STATUS_RESET_WIDTH (1u)
#define XSPI_SLV_STATUS_RESET_OFFSET (13u)
#define XSPI_SLV_STATUS_SEQ_ID_ERR_WIDTH (1u)
#define XSPI_SLV_STATUS_SEQ_ID_ERR_OFFSET (12u)
#define XSPI_SLV_STATUS_PERMISSION_ERR_WIDTH (1u)
#define XSPI_SLV_STATUS_PERMISSION_ERR_OFFSET (11u)
#define XSPI_SLV_STATUS_TIMEOUT_ERR_WIDTH (1u)
#define XSPI_SLV_STATUS_TIMEOUT_ERR_OFFSET (10u)
#define XSPI_SLV_STATUS_READ_ERR_WIDTH (1u)
#define XSPI_SLV_STATUS_READ_ERR_OFFSET (9u)
#define XSPI_SLV_STATUS_WRITE_ERR_WIDTH (1u)
#define XSPI_SLV_STATUS_WRITE_ERR_OFFSET (8u)
#define XSPI_SLV_STATUS_DUMMY_CYCLE_WIDTH (3u)
#define XSPI_SLV_STATUS_DUMMY_CYCLE_OFFSET (5u)
#define XSPI_SLV_STATUS_READ_DONE_WIDTH (1u)
#define XSPI_SLV_STATUS_READ_DONE_OFFSET (2u)
#define XSPI_SLV_STATUS_WRITE_DONE_WIDTH (1u)
#define XSPI_SLV_STATUS_WRITE_DONE_OFFSET (1u)
#define XSPI_SLV_STATUS_BUSY_OR_IDLE_WIDTH (1u)
#define XSPI_SLV_STATUS_BUSY_OR_IDLE_OFFSET (0u)


#define XSPI_SLV_STATUS_UPDATE_BASE (0x3cu)
#define XSPI_SLV_STATUS_UPDATE_INDIRECT_ERR_WIDTH (1u)
#define XSPI_SLV_STATUS_UPDATE_INDIRECT_ERR_OFFSET (14u)
#define XSPI_SLV_STATUS_UPDATE_RESET_WIDTH (1u)
#define XSPI_SLV_STATUS_UPDATE_RESET_OFFSET (13u)
#define XSPI_SLV_STATUS_UPDATE_SEQ_ID_ERR_WIDTH (1u)
#define XSPI_SLV_STATUS_UPDATE_SEQ_ID_ERR_OFFSET (12u)
#define XSPI_SLV_STATUS_UPDATE_PERMISSION_ERR_WIDTH (1u)
#define XSPI_SLV_STATUS_UPDATE_PERMISSION_ERR_OFFSET (11u)
#define XSPI_SLV_STATUS_UPDATE_TIMEOUT_ERR_WIDTH (1u)
#define XSPI_SLV_STATUS_UPDATE_TIMEOUT_ERR_OFFSET (10u)
#define XSPI_SLV_STATUS_UPDATE_READ_ERR_WIDTH (1u)
#define XSPI_SLV_STATUS_UPDATE_READ_ERR_OFFSET (9u)
#define XSPI_SLV_STATUS_UPDATE_WRITE_ERR_WIDTH (1u)
#define XSPI_SLV_STATUS_UPDATE_WRITE_ERR_OFFSET (8u)
#define XSPI_SLV_STATUS_UPDATE_DUMMY_CYCLE_WIDTH (3u)
#define XSPI_SLV_STATUS_UPDATE_DUMMY_CYCLE_OFFSET (5u)
#define XSPI_SLV_STATUS_UPDATE_READ_DONE_WIDTH (1u)
#define XSPI_SLV_STATUS_UPDATE_READ_DONE_OFFSET (2u)
#define XSPI_SLV_STATUS_UPDATE_WRITE_DONE_WIDTH (1u)
#define XSPI_SLV_STATUS_UPDATE_WRITE_DONE_OFFSET (1u)


#define XSPI_SLV_CMD_WRITE_TRIGGER_BASE (0x40u)
#define XSPI_SLV_CMD_WRITE_TRIGGER_CMD_WIDTH (8u)
#define XSPI_SLV_CMD_WRITE_TRIGGER_CMD_OFFSET (0u)


#define XSPI_SLV_CMD_READ_TRIGGER_BASE (0x44u)
#define XSPI_SLV_CMD_READ_TRIGGER_CMD_WIDTH (8u)
#define XSPI_SLV_CMD_READ_TRIGGER_CMD_OFFSET (0u)


#define XSPI_SLV_CMD_WRITE_PACKET0_BASE (0x48u)
#define XSPI_SLV_CMD_WRITE_PACKET0_CMD_WIDTH (8u)
#define XSPI_SLV_CMD_WRITE_PACKET0_CMD_OFFSET (0u)


#define XSPI_SLV_CMD_WRITE_PACKET1_BASE (0x4cu)
#define XSPI_SLV_CMD_WRITE_PACKET1_CMD_WIDTH (8u)
#define XSPI_SLV_CMD_WRITE_PACKET1_CMD_OFFSET (0u)


#define XSPI_SLV_CMD_WRITE_PAYLOAD_BASE (0x50u)
#define XSPI_SLV_CMD_WRITE_PAYLOAD_CMD_WIDTH (8u)
#define XSPI_SLV_CMD_WRITE_PAYLOAD_CMD_OFFSET (0u)


#define XSPI_SLV_CMD_WRITE_WDATA_BASE (0x54u)
#define XSPI_SLV_CMD_WRITE_WDATA_CMD_WIDTH (8u)
#define XSPI_SLV_CMD_WRITE_WDATA_CMD_OFFSET (0u)


#define XSPI_SLV_CMD_WRITE_STATUS_BASE (0x58u)
#define XSPI_SLV_CMD_WRITE_STATUS_CMD_WIDTH (8u)
#define XSPI_SLV_CMD_WRITE_STATUS_CMD_OFFSET (0u)


#define XSPI_SLV_CMD_READ_ID_BASE (0x5cu)
#define XSPI_SLV_CMD_READ_ID_CMD_WIDTH (8u)
#define XSPI_SLV_CMD_READ_ID_CMD_OFFSET (0u)


#define XSPI_SLV_CMD_READ_STATUS_BASE (0x60u)
#define XSPI_SLV_CMD_READ_STATUS_CMD_WIDTH (8u)
#define XSPI_SLV_CMD_READ_STATUS_CMD_OFFSET (0u)


#define XSPI_SLV_CMD_READ_RDATA_BASE (0x64u)
#define XSPI_SLV_CMD_READ_RDATA_CMD_WIDTH (8u)
#define XSPI_SLV_CMD_READ_RDATA_CMD_OFFSET (0u)


#define XSPI_SLV_CMD_READ_RDATA_CRC_BASE (0x68u)
#define XSPI_SLV_CMD_READ_RDATA_CRC_WIDTH (8u)
#define XSPI_SLV_CMD_READ_RDATA_CRC_OFFSET (0u)


#define XSPI_SLV_READ_CTRL_BASE (0x80u)
#define XSPI_SLV_READ_CTRL_ARID_OVERRIDE_WIDTH (1u)
#define XSPI_SLV_READ_CTRL_ARID_OVERRIDE_OFFSET (3u)
#define XSPI_SLV_READ_CTRL_ARCACHE_OVERRIDE_WIDTH (1u)
#define XSPI_SLV_READ_CTRL_ARCACHE_OVERRIDE_OFFSET (2u)
#define XSPI_SLV_READ_CTRL_ARPROT_OVERRIDE_WIDTH (1u)
#define XSPI_SLV_READ_CTRL_ARPROT_OVERRIDE_OFFSET (1u)
#define XSPI_SLV_READ_CTRL_ADDR_EN_WIDTH (1u)
#define XSPI_SLV_READ_CTRL_ADDR_EN_OFFSET (0u)


#define XSPI_SLV_R_OVERRIDE_VAL_BASE (0x84u)
#define XSPI_SLV_R_OVERRIDE_VAL_ARID_WIDTH (2u)
#define XSPI_SLV_R_OVERRIDE_VAL_ARID_OFFSET (7u)
#define XSPI_SLV_R_OVERRIDE_VAL_ARCACHE_WIDTH (4u)
#define XSPI_SLV_R_OVERRIDE_VAL_ARCACHE_OFFSET (3u)
#define XSPI_SLV_R_OVERRIDE_VAL_ARPROT_WIDTH (3u)
#define XSPI_SLV_R_OVERRIDE_VAL_ARPROT_OFFSET (0u)


#define XSPI_SLV_WRITE_CTRL_BASE (0x90u)
#define XSPI_SLV_WRITE_CTRL_AWID_OVERRIDE_WIDTH (1u)
#define XSPI_SLV_WRITE_CTRL_AWID_OVERRIDE_OFFSET (3u)
#define XSPI_SLV_WRITE_CTRL_AWCACHE_OVERRIDE_WIDTH (1u)
#define XSPI_SLV_WRITE_CTRL_AWCACHE_OVERRIDE_OFFSET (2u)
#define XSPI_SLV_WRITE_CTRL_AWPROT_OVERRIDE_WIDTH (1u)
#define XSPI_SLV_WRITE_CTRL_AWPROT_OVERRIDE_OFFSET (1u)
#define XSPI_SLV_WRITE_CTRL_ADDR_EN_WIDTH (1u)
#define XSPI_SLV_WRITE_CTRL_ADDR_EN_OFFSET (0u)


#define XSPI_SLV_W_OVERRIDE_VAL_BASE (0x94u)
#define XSPI_SLV_W_OVERRIDE_VAL_AWID_WIDTH (2u)
#define XSPI_SLV_W_OVERRIDE_VAL_AWID_OFFSET (7u)
#define XSPI_SLV_W_OVERRIDE_VAL_AWCACHE_WIDTH (4u)
#define XSPI_SLV_W_OVERRIDE_VAL_AWCACHE_OFFSET (3u)
#define XSPI_SLV_W_OVERRIDE_VAL_AWPROT_WIDTH (3u)
#define XSPI_SLV_W_OVERRIDE_VAL_AWPROT_OFFSET (0u)


#define XSPI_SLV_DEBUG_CTRL_BASE (0xc0u)
#define XSPI_SLV_DEBUG_CTRL_LOCK_WIDTH (1u)
#define XSPI_SLV_DEBUG_CTRL_LOCK_OFFSET (31u)
#define XSPI_SLV_DEBUG_CTRL_FIFO_SEL_WIDTH (2u)
#define XSPI_SLV_DEBUG_CTRL_FIFO_SEL_OFFSET (1u)
#define XSPI_SLV_DEBUG_CTRL_EN_WIDTH (1u)
#define XSPI_SLV_DEBUG_CTRL_EN_OFFSET (0u)


#define XSPI_SLV_DEBUG_FIFO_STATE_BASE (0xc4u)
#define XSPI_SLV_DEBUG_FIFO_STATE_AR_FIFO_NUM_WIDTH (8u)
#define XSPI_SLV_DEBUG_FIFO_STATE_AR_FIFO_NUM_OFFSET (24u)
#define XSPI_SLV_DEBUG_FIFO_STATE_AW_FIFO_NUM_WIDTH (8u)
#define XSPI_SLV_DEBUG_FIFO_STATE_AW_FIFO_NUM_OFFSET (16u)
#define XSPI_SLV_DEBUG_FIFO_STATE_W_FIFO_NUM_WIDTH (8u)
#define XSPI_SLV_DEBUG_FIFO_STATE_W_FIFO_NUM_OFFSET (8u)
#define XSPI_SLV_DEBUG_FIFO_STATE_R_FIFO_NUM_WIDTH (8u)
#define XSPI_SLV_DEBUG_FIFO_STATE_R_FIFO_NUM_OFFSET (0u)


#define XSPI_SLV_DEBUG_FIFO_VAL1_BASE (0xc8u)


#define XSPI_SLV_DEBUG_FIFO_VAL2_BASE (0xccu)



#define XSPI_SLV_REGION_MAP_CTRL_BASE (0x100u)
#define XSPI_SLV_REGION_MAP_CTRL_ADDR_WIDTH (20u)
#define XSPI_SLV_REGION_MAP_CTRL_ADDR_OFFSET (12u)
#define XSPI_SLV_REGION_MAP_CTRL_EN_WIDTH (1u)
#define XSPI_SLV_REGION_MAP_CTRL_EN_OFFSET (0u)


#define XSPI_SLV_REGION_MAP_BEGIN_BASE (0x104u)
#define XSPI_SLV_REGION_MAP_BEGIN_ADDR_WIDTH (20u)
#define XSPI_SLV_REGION_MAP_BEGIN_ADDR_OFFSET (12u)


#define XSPI_SLV_REGION_MAP_END_BASE (0x108u)
#define XSPI_SLV_REGION_MAP_END_ADDR_WIDTH (20u)
#define XSPI_SLV_REGION_MAP_END_ADDR_OFFSET (12u)


#define XSPI_SLV_READ_MON_PERM_CTRL_BASE (0x180u)
#define XSPI_SLV_READ_MON_PERM_CTRL_ADDR_LOCK_WIDTH (1u)
#define XSPI_SLV_READ_MON_PERM_CTRL_ADDR_LOCK_OFFSET (1u)
#define XSPI_SLV_READ_MON_PERM_CTRL_ADDR_EN_WIDTH (1u)
#define XSPI_SLV_READ_MON_PERM_CTRL_ADDR_EN_OFFSET (0u)


#define XSPI_SLV_R_MON_BEGIN_ADDR_BASE (0x184u)


#define XSPI_SLV_R_MON_END_ADDR_BASE (0x188u)


#define XSPI_SLV_WRITE_MON_PERM_CTRL_BASE (0x1c0u)
#define XSPI_SLV_WRITE_MON_PERM_CTRL_ADDR_LOCK_WIDTH (1u)
#define XSPI_SLV_WRITE_MON_PERM_CTRL_ADDR_LOCK_OFFSET (1u)
#define XSPI_SLV_WRITE_MON_PERM_CTRL_ADDR_EN_WIDTH (1u)
#define XSPI_SLV_WRITE_MON_PERM_CTRL_ADDR_EN_OFFSET (0u)


#define XSPI_SLV_W_MON_BEGIN_ADDR_BASE (0x1c4u)


#define XSPI_SLV_W_MON_END_ADDR_BASE (0x1c8u)


static inline void sdrv_xspi_slv_set_en(paddr_t base, bool enable)
{
    RMWREG32(base + XSPI_SLV_MODE_CTRL_BASE,
             XSPI_SLV_MODE_CTRL_XSPI_SLV_EN_OFFSET,
             XSPI_SLV_MODE_CTRL_XSPI_SLV_EN_WIDTH, !!enable);
}

static inline bool sdrv_xspi_slv_get_en(paddr_t base)
{
    return !!(readl(base + XSPI_SLV_MODE_CTRL_BASE) &
           (1u << XSPI_SLV_MODE_CTRL_XSPI_SLV_EN_OFFSET));
}

static inline void sdrv_xspi_slv_soft_reset(paddr_t base)
{
    RMWREG32(base + XSPI_SLV_MODE_CTRL_BASE,
             XSPI_SLV_MODE_CTRL_SOFT_RESET_OFFSET,
             XSPI_SLV_MODE_CTRL_SOFT_RESET_WIDTH, 1);
    RMWREG32(base + XSPI_SLV_MODE_CTRL_BASE,
             XSPI_SLV_MODE_CTRL_SOFT_RESET_OFFSET,
             XSPI_SLV_MODE_CTRL_SOFT_RESET_WIDTH, 0);
}

static inline void sdrv_xspi_slv_set_crc_en(paddr_t base, bool enable)
{
    RMWREG32(base + XSPI_SLV_MODE_CTRL_BASE, XSPI_SLV_MODE_CTRL_CRC_EN_OFFSET,
             XSPI_SLV_MODE_CTRL_CRC_EN_WIDTH, !!enable);
}

static inline bool sdrv_xspi_slv_get_crc_en(paddr_t base)
{
    return !!(readl(base + XSPI_SLV_MODE_CTRL_BASE) &
              (1u << XSPI_SLV_MODE_CTRL_CRC_EN_OFFSET));
}


static inline void sdrv_xspi_slv_set_bit_endian(paddr_t base, bool is_little)
{
    RMWREG32(base + XSPI_SLV_MODE_CTRL_BASE,
             XSPI_SLV_MODE_CTRL_BIT_ENDIAN_OFFSET,
             XSPI_SLV_MODE_CTRL_BIT_ENDIAN_WIDTH, !!is_little);
}

static inline uint8_t sdrv_xspi_slv_get_dummy_cycle(paddr_t base)
{
    uint32_t reg = readl(base + XSPI_SLV_MODE_CTRL_BASE);

    return (reg >> XSPI_SLV_MODE_CTRL_DUMMY_CYCLE_OFFSET) & 0x7;
}


static inline void sdrv_xspi_slv_set_dummy_cycle(paddr_t base, uint8_t dummy_cycle)
{
    RMWREG32(base + XSPI_SLV_STATUS_ACCESS_BASE,
             XSPI_SLV_STATUS_ACCESS_EN_DUMMY_CYCLE_OFFSET,
             XSPI_SLV_STATUS_ACCESS_EN_DUMMY_CYCLE_WIDTH, dummy_cycle & 0x7);
}

static inline bool sdrv_xspi_slv_is_direct_decode_busy(paddr_t base)
{
    uint32_t reg = readl(base + XSPI_SLV_MODE_CTRL_BASE);

    return (reg >> XSPI_SLV_MODE_CTRL_DIRECT_DECODE_BUSY_OFFSET) & 0x1;
}

static inline bool sdrv_xspi_slv_is_direct_encode_busy(paddr_t base)
{
    uint32_t reg = readl(base + XSPI_SLV_MODE_CTRL_BASE);

    return (reg >> XSPI_SLV_MODE_CTRL_DIRECT_ENCODE_BUSY_OFFSET) & 0x1;
}

static inline bool sdrv_xspi_slv_is_indirect_busy(paddr_t base)
{
    uint32_t reg = readl(base + XSPI_SLV_MODE_CTRL_BASE);

    return (reg >> XSPI_SLV_MODE_CTRL_INDIRECT_BUSY_OFFSET) & 0x1;
}

static inline void sdrv_xspi_slv_set_pad_reset_mask(paddr_t base, bool mask)
{
    RMWREG32(base + XSPI_SLV_MODE_CTRL_BASE,
             XSPI_SLV_MODE_CTRL_PAD_RESET_MASK_OFFSET,
             XSPI_SLV_MODE_CTRL_PAD_RESET_MASK_WIDTH, !!mask);
}

static inline bool sdrv_xspi_slv_get_pad_reset_mask(paddr_t base)
{
    return !!(readl(base + XSPI_SLV_MODE_CTRL_BASE) &
          (1u << XSPI_SLV_MODE_CTRL_PAD_RESET_MASK_OFFSET));
}


static inline void sdrv_xspi_slv_set_write_chk_en(paddr_t base, bool chk_en)
{
    RMWREG32(base + XSPI_SLV_MODE_CTRL_BASE,
             XSPI_SLV_MODE_CTRL_WRITE_CHK_EN_OFFSET,
             XSPI_SLV_MODE_CTRL_WRITE_CHK_EN_WIDTH, !!chk_en);
}

static inline bool sdrv_xspi_slv_get_write_chk_en(paddr_t base)
{
    return !!(readl(base + XSPI_SLV_MODE_CTRL_BASE) &
          (1u << XSPI_SLV_MODE_CTRL_WRITE_CHK_EN_OFFSET));
}


static inline void sdrv_xspi_slv_set_tx_mode(paddr_t base, bool is_sclk)
{
    RMWREG32(base + XSPI_SLV_MODE_CTRL_BASE,
             XSPI_SLV_MODE_CTRL_TX_MODE_OFFSET,
             XSPI_SLV_MODE_CTRL_TX_MODE_WIDTH, !!is_sclk);
}

static inline bool sdrv_xspi_slv_get_tx_mode(paddr_t base)
{
    return !!(readl(base + XSPI_SLV_MODE_CTRL_BASE) &
          (1u << XSPI_SLV_MODE_CTRL_TX_MODE_OFFSET));
}

static inline void sdrv_xspi_slv_set_write_en(paddr_t base, bool enable)
{
    RMWREG32(base + XSPI_SLV_MODE_CTRL_BASE,
             XSPI_SLV_MODE_CTRL_WRITE_EN_OFFSET,
             XSPI_SLV_MODE_CTRL_WRITE_EN_WIDTH, !!enable);
}

static inline bool sdrv_xspi_slv_get_write_en(paddr_t base)
{
    return !!(readl(base + XSPI_SLV_MODE_CTRL_BASE) &
          (1u << XSPI_SLV_MODE_CTRL_WRITE_EN_OFFSET));
}

static inline void sdrv_xspi_slv_set_read_en(paddr_t base, bool enable)
{
    RMWREG32(base + XSPI_SLV_MODE_CTRL_BASE,
             XSPI_SLV_MODE_CTRL_READ_EN_OFFSET,
             XSPI_SLV_MODE_CTRL_READ_EN_WIDTH, !!enable);
}

static inline bool sdrv_xspi_slv_get_read_en(paddr_t base)
{
    return !!(readl(base + XSPI_SLV_MODE_CTRL_BASE) &
          (1u << XSPI_SLV_MODE_CTRL_READ_EN_OFFSET));
}

static inline void sdrv_xspi_slv_set_mode(paddr_t base, bool is_direct)
{
    RMWREG32(base + XSPI_SLV_MODE_CTRL_BASE,
             XSPI_SLV_MODE_CTRL_MODE_OFFSET,
             XSPI_SLV_MODE_CTRL_MODE_WIDTH, !!is_direct);
}

static inline bool sdrv_xspi_slv_get_mode(paddr_t base)
{
    return !!(readl(base + XSPI_SLV_MODE_CTRL_BASE) &
          (1u << XSPI_SLV_MODE_CTRL_MODE_OFFSET));
}


static inline void sdrv_xspi_slv_set_sclk_idle_time_en(paddr_t base, bool enable)
{
    RMWREG32(base + XSPI_SLV_SLV_SET_BASE,
             XSPI_SLV_SLV_SET_SCLK_IDLE_TIME_EN_OFFSET,
             XSPI_SLV_SLV_SET_SCLK_IDLE_TIME_EN_WIDTH, !!enable);
}


static inline bool sdrv_xspi_slv_get_sclk_idle_time_en(paddr_t base)
{
    return !!(readl(base + XSPI_SLV_SLV_SET_BASE) &
            (1u << XSPI_SLV_SLV_SET_SCLK_IDLE_TIME_EN_OFFSET));
}

static inline void sdrv_xspi_slv_set_sclk_idle_time(paddr_t base, uint8_t time)
{
    RMWREG32(base + XSPI_SLV_SLV_SET_BASE,
             XSPI_SLV_SLV_SET_SCLK_IDLE_TIME_OFFSET,
             XSPI_SLV_SLV_SET_SCLK_IDLE_TIME_WIDTH, time & 0xff);
}

static inline uint8_t sdrv_xspi_slv_get_sclk_idle_time(paddr_t base)
{
    return (readl(base + XSPI_SLV_SLV_SET_BASE) >>
            XSPI_SLV_SLV_SET_SCLK_IDLE_TIME_OFFSET) & 0xff;
}


static inline void sdrv_xspi_slv_set_rate(paddr_t base, bool is_ddr)
{
    RMWREG32(base + XSPI_SLV_SLV_SET_BASE,
             XSPI_SLV_SLV_SET_RATE_OFFSET,
             XSPI_SLV_SLV_SET_RATE_WIDTH, !!is_ddr);
}

static inline bool sdrv_xspi_slv_get_rate(paddr_t base)
{
    return !!(readl(base + XSPI_SLV_SLV_SET_BASE) &
             (1u << XSPI_SLV_SLV_SET_RATE_OFFSET));
}


static inline void sdrv_xspi_slv_set_line(paddr_t base, uint8_t line)
{
    RMWREG32(base + XSPI_SLV_SLV_SET_BASE,
             XSPI_SLV_SLV_SET_LINE_OFFSET,
             XSPI_SLV_SLV_SET_LINE_WIDTH, line & 0x3);
}

static inline uint8_t sdrv_xspi_slv_get_line(paddr_t base)
{
    return (readl(base + XSPI_SLV_SLV_SET_BASE) >>
            XSPI_SLV_SLV_SET_LINE_OFFSET) & 0x3;
}

static inline void sdrv_xspi_slv_set_debounce_time(paddr_t base, uint8_t time)
{
    RMWREG32(base + XSPI_SLV_DEBOUNCE_CTRL_BASE,
             XSPI_SLV_DEBOUNCE_CTRL_TIME_OFFSET,
             XSPI_SLV_DEBOUNCE_CTRL_TIME_WIDTH, time & 0xf);
}

static inline uint8_t sdrv_xspi_slv_get_debounce_time(paddr_t base)
{
    return (readl(base + XSPI_SLV_DEBOUNCE_CTRL_BASE) >>
            XSPI_SLV_DEBOUNCE_CTRL_TIME_OFFSET) & 0xf;
}


static inline void sdrv_xspi_slv_set_debounce_en(paddr_t base, bool enable)
{
    RMWREG32(base + XSPI_SLV_DEBOUNCE_CTRL_BASE,
             XSPI_SLV_DEBOUNCE_CTRL_EN_OFFSET,
             XSPI_SLV_DEBOUNCE_CTRL_EN_WIDTH, !!enable);
}

static inline bool sdrv_xspi_slv_get_debounce_en(paddr_t base)
{
    return !!(readl(base + XSPI_SLV_DEBOUNCE_CTRL_BASE) &
             1u << XSPI_SLV_DEBOUNCE_CTRL_EN_OFFSET);
}

static inline void sdrv_xspi_slv_set_timeout_en(paddr_t base, bool enable)
{
    RMWREG32(base + XSPI_SLV_TIMEOUT_CTRL_BASE,
             XSPI_SLV_TIMEOUT_CTRL_EN_OFFSET,
             XSPI_SLV_TIMEOUT_CTRL_EN_WIDTH, !!enable);
}

static inline bool sdrv_xspi_slv_get_timeout_en(paddr_t base)
{
    return !!(readl(base + XSPI_SLV_TIMEOUT_CTRL_BASE) &
             1u << XSPI_SLV_TIMEOUT_CTRL_EN_OFFSET);
}

static inline void sdrv_xspi_slv_set_timeout_time(paddr_t base, uint16_t time)
{
    RMWREG32(base + XSPI_SLV_TIMEOUT_CTRL_BASE,
             XSPI_SLV_TIMEOUT_CTRL_TIME_OFFSET,
             XSPI_SLV_TIMEOUT_CTRL_TIME_WIDTH, time & 0xffff);
}

static inline uint16_t sdrv_xspi_slv_get_timeout_time(paddr_t base)
{
    return (readl(base + XSPI_SLV_TIMEOUT_CTRL_BASE) >>
            XSPI_SLV_TIMEOUT_CTRL_TIME_OFFSET) & 0xffff;
}

static inline void sdrv_xspi_slv_set_status_access_en(paddr_t base, uint16_t access)
{
    uint32_t reg = readl(base + XSPI_SLV_STATUS_ACCESS_BASE);

    reg |= access;

    writel(reg, base + XSPI_SLV_STATUS_ACCESS_BASE);
}

static inline uint16_t sdrv_xspi_slv_get_status_access_en(paddr_t base)
{
    return readl(base + XSPI_SLV_STATUS_ACCESS_BASE) & 0xffff;
}

static inline void sdrv_xspi_slv_clr_status_access_en(paddr_t base, uint16_t access)
{
    uint32_t reg = readl(base + XSPI_SLV_STATUS_ACCESS_BASE);

    reg &= ~(access);

    writel(reg, base + XSPI_SLV_STATUS_ACCESS_BASE);
}

static inline void sdrv_xspi_slv_set_id0(paddr_t base, uint32_t id0)
{
    writel(id0, base + XSPI_SLV_ID0_BASE);
}

static inline uint32_t sdrv_xspi_slv_get_id0(paddr_t base)
{
    return readl(base + XSPI_SLV_ID0_BASE);
}

static inline void sdrv_xspi_slv_set_id1(paddr_t base, uint32_t id1)
{
    writel(id1, base + XSPI_SLV_ID1_BASE);
}

static inline uint32_t sdrv_xspi_slv_get_id1(paddr_t base)
{
    return readl(base + XSPI_SLV_ID1_BASE);
}

static inline void sdrv_xspi_slv_set_pad_intb_mask(paddr_t base, uint8_t mask)
{
    writel(mask, base + XSPI_SLV_PAD_INTB_MASK_BASE);
}

static inline uint8_t sdrv_xspi_slv_get_pad_intb_mask(paddr_t base)
{
    return readl(base + XSPI_SLV_PAD_INTB_MASK_BASE) & 0xff;
}

static inline void sdrv_xspi_slv_set_pad_crcb_mask(paddr_t base, uint8_t mask)
{
    writel(mask, base + XSPI_SLV_PAD_CRCB_MASK_BASE);
}

static inline uint8_t sdrv_xspi_slv_get_pad_crcb_mask(paddr_t base)
{
    return readl(base + XSPI_SLV_PAD_CRCB_MASK_BASE) & 0xff;
}


static inline uint32_t sdrv_xspi_slv_get_int_st(paddr_t base)
{
    return readl(base + XSPI_SLV_INT_ST_BASE);
}

static inline void sdrv_xspi_slv_clr_int_st(paddr_t base, uint32_t status)
{
    writel(status, base + XSPI_SLV_INT_ST_BASE);
}

static inline void sdrv_xspi_slv_set_int_mask(paddr_t base, uint32_t mask)
{
    uint32_t reg = readl(base + XSPI_SLV_INT_MASK_BASE);

    reg |= mask;

    writel(reg, base + XSPI_SLV_INT_MASK_BASE);
}

static inline uint32_t sdrv_xspi_slv_get_int_mask(paddr_t base)
{
    return readl(base + XSPI_SLV_INT_MASK_BASE);
}

static inline void sdrv_xspi_slv_clr_int_mask(paddr_t base, uint32_t mask)
{
    uint32_t reg = readl(base + XSPI_SLV_INT_MASK_BASE);

    reg &= ~(mask);

    writel(reg, base + XSPI_SLV_INT_MASK_BASE);
}

static inline uint32_t sdrv_xspi_slv_get_slv_status(paddr_t base)
{
    return readl(base + XSPI_SLV_STATUS_BASE);
}

static inline void sdrv_xspi_slv_set_slv_status_update(paddr_t base, uint16_t status)
{
    uint32_t reg = readl(base + XSPI_SLV_STATUS_UPDATE_BASE);

    reg |= status;

    writel(reg, base + XSPI_SLV_STATUS_UPDATE_BASE);
}

static inline uint16_t sdrv_xspi_slv_get_slv_status_update(paddr_t base)
{
    return readl(base + XSPI_SLV_STATUS_UPDATE_BASE);
}


static inline void sdrv_xspi_slv_set_cmd_write_trigger(paddr_t base, uint8_t cmd)
{
    RMWREG32(base + XSPI_SLV_CMD_WRITE_TRIGGER_BASE,
             XSPI_SLV_CMD_WRITE_TRIGGER_CMD_OFFSET,
             XSPI_SLV_CMD_WRITE_TRIGGER_CMD_WIDTH, cmd);
}

static inline uint8_t sdrv_xspi_slv_get_cmd_write_trigger(paddr_t base)
{
    return readl(base + XSPI_SLV_CMD_WRITE_TRIGGER_BASE) & 0xff;
}

static inline void sdrv_xspi_slv_set_cmd_read_trigger(paddr_t base, uint8_t cmd)
{
    RMWREG32(base + XSPI_SLV_CMD_READ_TRIGGER_BASE,
             XSPI_SLV_CMD_READ_TRIGGER_CMD_OFFSET,
             XSPI_SLV_CMD_READ_TRIGGER_CMD_WIDTH, cmd);
}

static inline uint8_t sdrv_xspi_slv_get_cmd_read_trigger(paddr_t base)
{
    return readl(base + XSPI_SLV_CMD_READ_TRIGGER_BASE) & 0xff;
}

static inline void sdrv_xspi_slv_set_cmd_write_packet0(paddr_t base, uint8_t cmd)
{
    RMWREG32(base + XSPI_SLV_CMD_WRITE_PACKET0_BASE,
             XSPI_SLV_CMD_WRITE_PACKET0_CMD_OFFSET,
             XSPI_SLV_CMD_WRITE_PACKET0_CMD_WIDTH, cmd);
}

static inline uint8_t sdrv_xspi_slv_get_cmd_write_packet0(paddr_t base)
{
    return readl(base + XSPI_SLV_CMD_WRITE_PACKET0_BASE) & 0xff;
}

static inline void sdrv_xspi_slv_set_cmd_write_packet1(paddr_t base, uint8_t cmd)
{
    RMWREG32(base + XSPI_SLV_CMD_WRITE_PACKET1_BASE,
             XSPI_SLV_CMD_WRITE_PACKET1_CMD_OFFSET,
             XSPI_SLV_CMD_WRITE_PACKET1_CMD_WIDTH, cmd);
}

static inline uint8_t sdrv_xspi_slv_get_cmd_write_packet1(paddr_t base)
{
    return readl(base + XSPI_SLV_CMD_WRITE_PACKET1_BASE) & 0xff;
}

static inline void sdrv_xspi_slv_set_cmd_write_payload(paddr_t base, uint8_t cmd)
{
    RMWREG32(base + XSPI_SLV_CMD_WRITE_PAYLOAD_BASE,
             XSPI_SLV_CMD_WRITE_PAYLOAD_CMD_OFFSET,
             XSPI_SLV_CMD_WRITE_PAYLOAD_CMD_WIDTH, cmd);
}

static inline uint8_t sdrv_xspi_slv_get_cmd_write_payload(paddr_t base)
{
    return readl(base + XSPI_SLV_CMD_WRITE_PAYLOAD_BASE) & 0xff;
}

static inline void sdrv_xspi_slv_set_cmd_write_wdata(paddr_t base, uint8_t cmd)
{
    RMWREG32(base + XSPI_SLV_CMD_WRITE_WDATA_BASE,
             XSPI_SLV_CMD_WRITE_WDATA_CMD_OFFSET,
             XSPI_SLV_CMD_WRITE_WDATA_CMD_WIDTH, cmd);
}

static inline uint8_t sdrv_xspi_slv_get_cmd_write_wdata(paddr_t base)
{
    return readl(base + XSPI_SLV_CMD_WRITE_WDATA_BASE) & 0xff;
}

static inline void sdrv_xspi_slv_set_cmd_write_status(paddr_t base, uint8_t cmd)
{
    RMWREG32(base + XSPI_SLV_CMD_WRITE_STATUS_BASE,
             XSPI_SLV_CMD_WRITE_STATUS_CMD_OFFSET,
             XSPI_SLV_CMD_WRITE_STATUS_CMD_WIDTH, cmd);
}

static inline uint8_t sdrv_xspi_slv_get_cmd_write_status(paddr_t base)
{
    return readl(base + XSPI_SLV_CMD_WRITE_STATUS_BASE) & 0xff;
}

static inline void sdrv_xspi_slv_set_cmd_read_id(paddr_t base, uint8_t cmd)
{
    RMWREG32(base + XSPI_SLV_CMD_READ_ID_BASE,
             XSPI_SLV_CMD_READ_ID_CMD_OFFSET,
             XSPI_SLV_CMD_READ_ID_CMD_WIDTH, cmd);
}

static inline uint8_t sdrv_xspi_slv_get_cmd_read_id(paddr_t base)
{
    return readl(base + XSPI_SLV_CMD_READ_ID_BASE) & 0xff;
}

static inline void sdrv_xspi_slv_set_cmd_read_status(paddr_t base, uint8_t cmd)
{
    RMWREG32(base + XSPI_SLV_CMD_READ_STATUS_BASE,
             XSPI_SLV_CMD_READ_STATUS_CMD_OFFSET,
             XSPI_SLV_CMD_READ_STATUS_CMD_WIDTH, cmd);
}

static inline uint8_t sdrv_xspi_slv_get_cmd_read_status(paddr_t base)
{
    return readl(base + XSPI_SLV_CMD_READ_STATUS_BASE) & 0xff;
}

static inline void sdrv_xspi_slv_set_cmd_read_rdata(paddr_t base, uint8_t cmd)
{
    RMWREG32(base + XSPI_SLV_CMD_READ_RDATA_BASE,
             XSPI_SLV_CMD_READ_RDATA_CMD_OFFSET,
             XSPI_SLV_CMD_READ_RDATA_CMD_WIDTH, cmd);
}

static inline uint8_t sdrv_xspi_slv_get_cmd_read_rdata(paddr_t base)
{
    return readl(base + XSPI_SLV_CMD_READ_RDATA_BASE) & 0xff;
}

static inline void sdrv_xspi_slv_set_cmd_read_rdata_crc(paddr_t base, uint8_t cmd)
{
    RMWREG32(base + XSPI_SLV_CMD_READ_RDATA_CRC_BASE,
             XSPI_SLV_CMD_READ_RDATA_CRC_OFFSET,
             XSPI_SLV_CMD_READ_RDATA_CRC_WIDTH, cmd);
}

static inline uint8_t sdrv_xspi_slv_get_cmd_read_rdata_crc(paddr_t base)
{
    return readl(base + XSPI_SLV_CMD_READ_RDATA_CRC_BASE) & 0xff;
}

static inline void sdrv_xspi_slv_set_arid_override_en(paddr_t base, bool enable)
{
    RMWREG32(base + XSPI_SLV_READ_CTRL_BASE,
             XSPI_SLV_READ_CTRL_ARID_OVERRIDE_OFFSET,
             XSPI_SLV_READ_CTRL_ARID_OVERRIDE_WIDTH, !!enable);
}

static inline bool sdrv_xspi_slv_get_arid_override_en(paddr_t base)
{
    return !!(readl(base + XSPI_SLV_READ_CTRL_BASE) &
             (1u << XSPI_SLV_READ_CTRL_ARID_OVERRIDE_OFFSET));
}

static inline void sdrv_xspi_slv_set_arcache_override_en(paddr_t base, bool enable)
{
    RMWREG32(base + XSPI_SLV_READ_CTRL_BASE,
             XSPI_SLV_READ_CTRL_ARCACHE_OVERRIDE_OFFSET,
             XSPI_SLV_READ_CTRL_ARCACHE_OVERRIDE_WIDTH, !!enable);
}

static inline bool sdrv_xspi_slv_get_arcache_override_en(paddr_t base)
{
    return !!(readl(base + XSPI_SLV_READ_CTRL_BASE) &
             (1u << XSPI_SLV_READ_CTRL_ARCACHE_OVERRIDE_OFFSET));
}

static inline void sdrv_xspi_slv_set_arprot_override_en(paddr_t base, bool enable)
{
    RMWREG32(base + XSPI_SLV_READ_CTRL_BASE,
             XSPI_SLV_READ_CTRL_ARPROT_OVERRIDE_OFFSET,
             XSPI_SLV_READ_CTRL_ARPROT_OVERRIDE_WIDTH, !!enable);
}

static inline void sdrv_xspi_slv_set_read_ctrl_addr_en(paddr_t base, bool enable)
{
    RMWREG32(base + XSPI_SLV_READ_CTRL_BASE,
             XSPI_SLV_READ_CTRL_ADDR_EN_OFFSET,
             XSPI_SLV_READ_CTRL_ADDR_EN_WIDTH, !!enable);
}

static inline bool sdrv_xspi_slv_get_read_ctrl_addr_en(paddr_t base)
{
    return !!(readl(base + XSPI_SLV_READ_CTRL_BASE) &
           (1u << XSPI_SLV_READ_CTRL_ADDR_EN_OFFSET));
}

static inline void sdrv_xspi_slv_set_override_arid(paddr_t base, uint8_t arid)
{
    RMWREG32(base + XSPI_SLV_R_OVERRIDE_VAL_BASE,
             XSPI_SLV_R_OVERRIDE_VAL_ARID_OFFSET,
             XSPI_SLV_R_OVERRIDE_VAL_ARID_WIDTH, arid & 0x3);
}

static inline uint8_t sdrv_xspi_slv_get_override_arid(paddr_t base)
{
    return (readl(base + XSPI_SLV_R_OVERRIDE_VAL_BASE) >>
            XSPI_SLV_R_OVERRIDE_VAL_ARID_OFFSET) & 0x3;
}

static inline void sdrv_xspi_slv_set_override_arcache(paddr_t base, uint8_t arcache)
{
    RMWREG32(base + XSPI_SLV_R_OVERRIDE_VAL_BASE,
             XSPI_SLV_R_OVERRIDE_VAL_ARCACHE_OFFSET,
             XSPI_SLV_R_OVERRIDE_VAL_ARCACHE_WIDTH, arcache & 0xf);
}

static inline uint8_t sdrv_xspi_slv_get_override_arcache(paddr_t base)
{
    return (readl(base + XSPI_SLV_R_OVERRIDE_VAL_BASE) >>
           XSPI_SLV_R_OVERRIDE_VAL_ARCACHE_OFFSET) & 0xf;
}

static inline void sdrv_xspi_slv_set_override_arprot(paddr_t base, uint8_t arprot)
{
    RMWREG32(base + XSPI_SLV_R_OVERRIDE_VAL_BASE,
             XSPI_SLV_R_OVERRIDE_VAL_ARPROT_OFFSET,
             XSPI_SLV_R_OVERRIDE_VAL_ARPROT_WIDTH, arprot & 0x7);
}

static inline void sdrv_xspi_slv_set_awid_override_en(paddr_t base, bool enable)
{
    RMWREG32(base + XSPI_SLV_WRITE_CTRL_BASE,
             XSPI_SLV_WRITE_CTRL_AWID_OVERRIDE_OFFSET,
             XSPI_SLV_WRITE_CTRL_AWID_OVERRIDE_WIDTH, !!enable);
}

static inline bool sdrv_xspi_slv_get_awid_override_en(paddr_t base)
{
    return !!(readl(base + XSPI_SLV_WRITE_CTRL_BASE) &
           (1u << XSPI_SLV_WRITE_CTRL_AWID_OVERRIDE_OFFSET));
}

static inline void sdrv_xspi_slv_set_awcache_override_en(paddr_t base, bool enable)
{
    RMWREG32(base + XSPI_SLV_WRITE_CTRL_BASE,
             XSPI_SLV_WRITE_CTRL_AWCACHE_OVERRIDE_OFFSET,
             XSPI_SLV_WRITE_CTRL_AWCACHE_OVERRIDE_WIDTH, !!enable);
}

static inline bool sdrv_xspi_slv_get_awcache_override_en(paddr_t base)
{
    return !!(readl(base + XSPI_SLV_WRITE_CTRL_BASE) &
           (1u << XSPI_SLV_WRITE_CTRL_AWCACHE_OVERRIDE_OFFSET));
}

static inline void sdrv_xspi_slv_set_awprot_override_en(paddr_t base, bool enable)
{
    RMWREG32(base + XSPI_SLV_WRITE_CTRL_BASE,
             XSPI_SLV_WRITE_CTRL_AWPROT_OVERRIDE_OFFSET,
             XSPI_SLV_WRITE_CTRL_AWPROT_OVERRIDE_WIDTH, !!enable);
}

static inline bool sdrv_xspi_slv_get_awprot_override_en(paddr_t base)
{
    return !!(readl(base + XSPI_SLV_WRITE_CTRL_BASE) &
            (1u << XSPI_SLV_WRITE_CTRL_AWPROT_OVERRIDE_OFFSET));
}

static inline void sdrv_xspi_slv_set_write_ctrl_addr_en(paddr_t base, bool enable)
{
    RMWREG32(base + XSPI_SLV_WRITE_CTRL_BASE,
             XSPI_SLV_WRITE_CTRL_ADDR_EN_OFFSET,
             XSPI_SLV_WRITE_CTRL_ADDR_EN_WIDTH, !!enable);
}

static inline bool sdrv_xspi_slv_get_write_ctrl_addr_en(paddr_t base)
{
    return !!(readl(base + XSPI_SLV_WRITE_CTRL_BASE) &
             (1u << XSPI_SLV_WRITE_CTRL_ADDR_EN_OFFSET));
}

static inline void sdrv_xspi_slv_set_override_awid(paddr_t base, uint8_t awid)
{
    RMWREG32(base + XSPI_SLV_W_OVERRIDE_VAL_BASE,
             XSPI_SLV_W_OVERRIDE_VAL_AWID_OFFSET,
             XSPI_SLV_W_OVERRIDE_VAL_AWID_WIDTH, awid & 0x3);
}

static inline uint8_t sdrv_xspi_slv_get_override_awid(paddr_t base)
{
    return (readl(base + XSPI_SLV_W_OVERRIDE_VAL_BASE) >>
            XSPI_SLV_W_OVERRIDE_VAL_AWID_OFFSET) & 0x3;
}

static inline void sdrv_xspi_slv_set_override_awcache(paddr_t base, uint8_t awcache)
{
    RMWREG32(base + XSPI_SLV_W_OVERRIDE_VAL_BASE,
             XSPI_SLV_W_OVERRIDE_VAL_AWCACHE_OFFSET,
             XSPI_SLV_W_OVERRIDE_VAL_AWCACHE_WIDTH, awcache & 0xf);
}

static inline uint8_t sdrv_xspi_slv_get_override_awcache(paddr_t base)
{
    return (readl(base + XSPI_SLV_W_OVERRIDE_VAL_BASE) >>
            XSPI_SLV_W_OVERRIDE_VAL_AWCACHE_OFFSET) & 0xf;
}

static inline void sdrv_xspi_slv_set_override_awprot(paddr_t base, uint8_t prot)
{
    RMWREG32(base + XSPI_SLV_W_OVERRIDE_VAL_BASE,
             XSPI_SLV_W_OVERRIDE_VAL_AWPROT_OFFSET,
             XSPI_SLV_W_OVERRIDE_VAL_AWPROT_WIDTH, prot & 0x7);
}

static inline uint8_t sdrv_xspi_slv_get_override_awprot(paddr_t base)
{
    return (readl(base + XSPI_SLV_W_OVERRIDE_VAL_BASE) >>
            XSPI_SLV_W_OVERRIDE_VAL_AWPROT_OFFSET) & 0x7;
}


static inline void sdrv_xspi_slv_set_region_map_offset(paddr_t base, uint32_t addr, uint32_t id)
{
    RMWREG32(base + XSPI_SLV_REGION_MAP_CTRL_BASE + (id << 4),
             XSPI_SLV_REGION_MAP_CTRL_ADDR_OFFSET,
             XSPI_SLV_REGION_MAP_CTRL_ADDR_WIDTH, (addr >> 12) & 0xfffff);
}

static inline uint32_t sdrv_xspi_slv_get_region_map_addr(paddr_t base, uint32_t id)
{
    return readl(base + XSPI_SLV_REGION_MAP_CTRL_BASE + (id << 4));
}

static inline void sdrv_xspi_slv_set_region_map_en(paddr_t base, bool enable, uint32_t id)
{
    RMWREG32(base + XSPI_SLV_REGION_MAP_CTRL_BASE + (id << 4),
             XSPI_SLV_REGION_MAP_CTRL_EN_OFFSET,
             XSPI_SLV_REGION_MAP_CTRL_EN_WIDTH, !!enable);
}

static inline bool sdrv_xspi_slv_get_region_map_en(paddr_t base, uint32_t id)
{
    return !!(readl(base + XSPI_SLV_REGION_MAP_CTRL_BASE + (id << 4)) &
             (1u << XSPI_SLV_REGION_MAP_CTRL_EN_OFFSET));
}

static inline void sdrv_xspi_slv_set_region_map_begin_addr(paddr_t base, uint32_t addr, uint32_t id)
{
    RMWREG32(base + XSPI_SLV_REGION_MAP_BEGIN_BASE + (id << 4),
             XSPI_SLV_REGION_MAP_BEGIN_ADDR_OFFSET,
             XSPI_SLV_REGION_MAP_BEGIN_ADDR_WIDTH, (addr >> 12) & 0xfffff);
}

static inline uint32_t sdrv_xspi_slv_get_region_map_begin_addr(paddr_t base, uint32_t id)
{
    return readl(base + XSPI_SLV_REGION_MAP_BEGIN_BASE + (id << 4));
}

static inline void sdrv_xspi_slv_set_region_map_end_addr(paddr_t base, uint32_t addr, uint32_t id)
{
    RMWREG32(base + XSPI_SLV_REGION_MAP_END_BASE + (id << 4),
             XSPI_SLV_REGION_MAP_END_ADDR_OFFSET,
             XSPI_SLV_REGION_MAP_END_ADDR_WIDTH, (addr >> 12) & 0xfffff);
}

static inline uint32_t sdrv_xspi_slv_get_region_map_end_addr(paddr_t base, uint32_t id)
{
    return readl(base + XSPI_SLV_REGION_MAP_END_BASE + (id << 4));
}


static inline void sdrv_xspi_slv_set_read_mon_perm_addr_lock(paddr_t base, bool is_lock, uint32_t id)
{
    RMWREG32(base + XSPI_SLV_READ_MON_PERM_CTRL_BASE + (id << 4),
             XSPI_SLV_READ_MON_PERM_CTRL_ADDR_LOCK_OFFSET,
             XSPI_SLV_READ_MON_PERM_CTRL_ADDR_LOCK_WIDTH, !!is_lock);
}

static inline bool sdrv_xspi_slv_get_read_mon_perm_addr_lock(paddr_t base, uint32_t id)
{
    return !!(readl(base + XSPI_SLV_READ_MON_PERM_CTRL_BASE + (id << 4)) &
            (1u << XSPI_SLV_READ_MON_PERM_CTRL_ADDR_LOCK_OFFSET));
}

static inline void sdrv_xspi_slv_set_read_mon_perm_addr_en(paddr_t base, bool enable, uint32_t id)
{
    RMWREG32(base + XSPI_SLV_READ_MON_PERM_CTRL_BASE + (id << 4),
             XSPI_SLV_READ_MON_PERM_CTRL_ADDR_EN_OFFSET,
             XSPI_SLV_READ_MON_PERM_CTRL_ADDR_EN_WIDTH, !!enable);
}

static inline bool sdrv_xspi_slv_get_read_mon_perm_addr_en(paddr_t base, uint32_t id)
{
    return !!(readl(base + XSPI_SLV_READ_MON_PERM_CTRL_BASE + (id << 4)) &
           (1u << XSPI_SLV_READ_MON_PERM_CTRL_ADDR_EN_OFFSET));
}

static inline void sdrv_xspi_slv_set_r_mon_begin_addr(paddr_t base, uint32_t addr, uint32_t id)
{
    writel(addr, base + XSPI_SLV_R_MON_BEGIN_ADDR_BASE + (id << 4));
}

static inline uint32_t sdrv_xspi_slv_get_r_mon_begin_addr(paddr_t base, uint32_t id)
{
    return readl(base + XSPI_SLV_R_MON_BEGIN_ADDR_BASE + (id << 4));
}

static inline void sdrv_xspi_slv_set_r_mon_end_addr(paddr_t base, uint32_t addr, uint32_t id)
{
    writel(addr, base + XSPI_SLV_R_MON_END_ADDR_BASE + (id << 4));
}

static inline uint32_t sdrv_xspi_slv_get_r_mon_end_addr(paddr_t base, uint32_t id)
{
    return readl(base + XSPI_SLV_R_MON_END_ADDR_BASE + (id << 4));
}

static inline void sdrv_xspi_slv_set_write_mon_perm_addr_lock(paddr_t base, bool is_lock, uint32_t id)
{
    RMWREG32(base + XSPI_SLV_WRITE_MON_PERM_CTRL_BASE + (id << 4),
             XSPI_SLV_WRITE_MON_PERM_CTRL_ADDR_LOCK_OFFSET,
             XSPI_SLV_WRITE_MON_PERM_CTRL_ADDR_LOCK_WIDTH, !!is_lock);
}

static inline bool sdrv_xspi_slv_get_write_mon_perm_addr_lock(paddr_t base, uint32_t id)
{
    return !!(readl(base + XSPI_SLV_WRITE_MON_PERM_CTRL_BASE + (id << 4)) &
           (1u << XSPI_SLV_WRITE_MON_PERM_CTRL_ADDR_LOCK_OFFSET));
}

static inline void sdrv_xspi_slv_set_write_mon_perm_addr_en(paddr_t base, bool enable, uint32_t id)
{
    RMWREG32(base + XSPI_SLV_WRITE_MON_PERM_CTRL_BASE + (id << 4),
             XSPI_SLV_WRITE_MON_PERM_CTRL_ADDR_EN_OFFSET,
             XSPI_SLV_WRITE_MON_PERM_CTRL_ADDR_EN_WIDTH, !!enable);
}

static inline bool sdrv_xspi_slv_get_write_mon_perm_addr_en(paddr_t base, uint32_t id)
{
    return !!(readl(base + XSPI_SLV_WRITE_MON_PERM_CTRL_BASE + (id << 4)) &
             (1u << XSPI_SLV_WRITE_MON_PERM_CTRL_ADDR_EN_OFFSET));
}

static inline void sdrv_xspi_slv_set_w_mon_begin_addr(paddr_t base, uint32_t addr, uint32_t id)
{
    writel(addr, base + XSPI_SLV_W_MON_BEGIN_ADDR_BASE + (id << 4));
}

static inline uint32_t sdrv_xspi_slv_get_w_mon_begin_addr(paddr_t base, uint32_t id)
{
    return readl(base + XSPI_SLV_W_MON_BEGIN_ADDR_BASE + (id << 4));
}

static inline void sdrv_xspi_slv_set_w_mon_end_addr(paddr_t base, uint32_t addr, uint32_t id)
{
    writel(addr, base + XSPI_SLV_W_MON_END_ADDR_BASE + (id << 4));
}

static inline uint32_t sdrv_xspi_slv_get_w_mon_end_addr(paddr_t base, uint32_t id)
{
    return readl(base + XSPI_SLV_W_MON_END_ADDR_BASE + (id << 4));
}


#ifdef __cplusplus
}
#endif

#endif


