/*
* sdrv_xspi_mst_reg.h
*
* Copyright (c) 2021 Semidrive Semiconductor.
* All rights reserved.
*
* Description: implement semidrive xspi master drv head file
*
*/

#ifndef SDRV_XSPI_MST_REG_
#define SDRV_XSPI_MST_REG_

#ifdef __cplusplus
extern "C" {
#endif

#include <param.h>
#include <reg.h>
#include <types.h>

#define XSPI_MASTER_MODE_CTRL_BASE (0x0u)
#define XSPI_MASTER_MODE_CTRL_SW_RST_WIDTH (1u)
#define XSPI_MASTER_MODE_CTRL_SW_RST_OFFSET (31u)
#define XSPI_MASTER_MODE_CTRL_IDLE_WIDTH (1u)
#define XSPI_MASTER_MODE_CTRL_IDLE_OFFSET (30u)
#define XSPI_MASTER_MODE_CTRL_RX_MODE_WIDTH (1u)
#define XSPI_MASTER_MODE_CTRL_RX_MODE (8u)
#define XSPI_MASTER_MODE_CTRL_MS_MODE_WIDTH (1u)
#define XSPI_MASTER_MODE_CTRL_MS_MODE_OFFSET (0u)

#define XSPI_MASTER_INT_ST_FUC_BASE (0x10u)
#define XSPI_MASTER_INT_ST_FUC_TX_PRE_EMPTY_OFFSET (9u)
#define XSPI_MASTER_INT_ST_FUC_RX_PRE_FULL_OFFSET (8u)
#define XSPI_MASTER_INT_ST_FUC_MS_TRANS_DONE_OFFSET (4u)
#define XSPI_MASTER_INT_ST_FUC_INDIRECT_RD_DONE_OFFSET (1u)
#define XSPI_MASTER_INT_ST_FUC_INDIRECT_WR_DONE_OFFSET (0u)

#define XSPI_MASTER_INT_EN_FUC_BASE (0x14u)
#define XSPI_MASTER_INT_EN_FUC_TX_PRE_EMPTY_OFFSET (9u)
#define XSPI_MASTER_INT_EN_FUC_RX_PRE_FULL_OFFSET (8u)
#define XSPI_MASTER_INT_EN_FUC_MS_TRANS_DONE_OFFSET (4u)
#define XSPI_MASTER_INT_EN_FUC_INDIRECT_RD_DONE_OFFSET (1u)
#define XSPI_MASTER_INT_EN_FUC_INDIRECT_WR_DONE_OFFSET (0u)

#define XSPI_MASTER_INT_ST_ERR_BASE (0x18u)
#define XSPI_MASTER_INT_ST_ERR_IO_CRC_OFFSET (7u)
#define XSPI_MASTER_INT_ST_ERR_IO_INT_OFFSET (6u)

#define XSPI_MASTER_INT_EN_ERR_BASE (0x1cu)
#define XSPI_MASTER_INT_EN_ERR_IO_CRC_OFFSET (7u)
#define XSPI_MASTER_INT_EN_ERR_IO_INT_OFFSET (6u)

#define XSPI_MASTER_INT_EN_ERR_FUC_BASE (0x20u)
#define XSPI_MASTER_INT_EN_ERR_FUC_IO_CRC_OFFSET (7u)
#define XSPI_MASTER_INT_EN_ERR_FUC_IO_INT_OFFSET (6u)

#define XSPI_MASTER_IO_CTRL_BASE (0x60u)
#define XSPI_MASTER_IO_CTRL_CRCB_RST_EN_OFFSET (3u)
#define XSPI_MASTER_IO_CTRL_CRCB_RST_EN_WIDTH (1u)
#define XSPI_MASTER_IO_CTRL_INTB_RST_EN_OFFSET (2u)
#define XSPI_MASTER_IO_CTRL_INTB_RST_EN_WIDTH (1u)
#define XSPI_MASTER_IO_CTRL_SW_PAD_RSTN_OFFSET (1u)
#define XSPI_MASTER_IO_CTRL_SW_PAD_RSTN_WIDTH (1u)
#define XSPI_MASTER_IO_CTRL_SW_PAD_RST_EN_OFFSET (0u)
#define XSPI_MASTER_IO_CTRL_SW_PAD_RST_EN_WIDTH (1u)

#define XSPI_MASTER_ADDR_OFFSET_BASE (0x70u)
#define XSPI_MASTER_ADDR_OFFSET_VAL_WIDTH (28u)
#define XSPI_MASTER_ADDR_OFFSET_VAL_OFFSET (4u)
#define XSPI_MASTER_ADDR_OFFSET_EN_WIDTH (1u)
#define XSPI_MASTER_ADDR_OFFSET_EN_OFFSET (0u)

#define XSPI_MASTER_MISC_CTRL_BASE (0x80u)
#define XSPI_MASTER_MISC_CTRL_RXENABLE_MODE_WIDTH (1u)
#define XSPI_MASTER_MISC_CTRL_RXENABLE_MODE (9u)


#define XSPI_MASTER_CS_CTRL_BASE (0xc4u)
#define XSPI_MASTER_CS_CTRL_CSS_TIME_OFFSET (16u)
#define XSPI_MASTER_CS_CTRL_CSS_TIME_WIDTH (4u)
#define XSPI_MASTER_CS_CTRL_SHSL_TIME_OFFSET (8u)
#define XSPI_MASTER_CS_CTRL_SHSL_TIME_WIDTH (5u)
#define XSPI_MASTER_CS_CTRL_CSH_TIME_OFFSET (0u)
#define XSPI_MASTER_CS_CTRL_CSH_TIME_WIDTH (4u)

#define XSPI_MASTER_INDIRECT_WR_CTRL_BASE (0x200u)
#define XSPI_MASTER_INDIRECT_WR_CTRL_W_TRIGGER_WIDTH (1u)
#define XSPI_MASTER_INDIRECT_WR_CTRL_W_TRIGGER_OFFSET (0u)

#define XSPI_MASTER_INDIRECT_WR_PH_CTRL_BASE (0x204u)
#define XSPI_MASTER_INDIRECT_WR_PH_CTRL_D_EN_WIDTH (1u)
#define XSPI_MASTER_INDIRECT_WR_PH_CTRL_D_EN_OFFSET (27u)
#define XSPI_MASTER_INDIRECT_WR_PH_CTRL_D_RATE_WIDTH (1u)
#define XSPI_MASTER_INDIRECT_WR_PH_CTRL_D_RATE_OFFSET (26u)
#define XSPI_MASTER_INDIRECT_WR_PH_CTRL_D_LINE_WIDTH (2u)
#define XSPI_MASTER_INDIRECT_WR_PH_CTRL_D_LINE_OFFSET (24u)
#define XSPI_MASTER_INDIRECT_WR_PH_CTRL_A_EN_WIDTH (1u)
#define XSPI_MASTER_INDIRECT_WR_PH_CTRL_A_EN_OFFSET (13u)
#define XSPI_MASTER_INDIRECT_WR_PH_CTRL_A_SIZE_WIDTH (2u)
#define XSPI_MASTER_INDIRECT_WR_PH_CTRL_A_SIZE_OFFSET (11u)
#define XSPI_MASTER_INDIRECT_WR_PH_CTRL_A_RATE_WIDTH (1u)
#define XSPI_MASTER_INDIRECT_WR_PH_CTRL_A_RATE_OFFSET (10u)
#define XSPI_MASTER_INDIRECT_WR_PH_CTRL_A_LINE_WIDTH (2u)
#define XSPI_MASTER_INDIRECT_WR_PH_CTRL_A_LINE_OFFSET (8u)
#define XSPI_MASTER_INDIRECT_WR_PH_CTRL_C_EN_WIDTH (1u)
#define XSPI_MASTER_INDIRECT_WR_PH_CTRL_C_EN_OFFSET (5u)
#define XSPI_MASTER_INDIRECT_WR_PH_CTRL_C_SIZE_WIDTH (2u)
#define XSPI_MASTER_INDIRECT_WR_PH_CTRL_C_SIZE_OFFSET (3u)
#define XSPI_MASTER_INDIRECT_WR_PH_CTRL_C_RATE_WIDTH (1u)
#define XSPI_MASTER_INDIRECT_WR_PH_CTRL_C_RATE_OFFSET (2u)
#define XSPI_MASTER_INDIRECT_WR_PH_CTRL_C_LINE_WIDTH (2u)
#define XSPI_MASTER_INDIRECT_WR_PH_CTRL_C_LINE_OFFSET (0u)

#define XSPI_MASTER_INDIRECT_WR_CMD_CODE_BASE (0x208u)
#define XSPI_MASTER_INDIRECT_WR_CMD_CODE_WIDTH (16u)
#define XSPI_MASTER_INDIRECT_WR_CMD_CODE_OFFSET (0u)

#define XSPI_MASTER_INDIRECT_WR_ADDR_BASE (0x20cu)

#define XSPI_MASTER_INDIRECT_WR_SIZE_BASE (0x210u)
#define XSPI_MASTER_INDIRECT_WR_SIZE_WIDTH (24u)
#define XSPI_MASTER_INDIRECT_WR_SIZE_OFFSET (0u)

#define XSPI_MASTER_INDIRECT_WR_CYC_BASE (0x214u)
#define XSPI_MASTER_INDIRECT_WR_CYC_WIDTH (5u)
#define XSPI_MASTER_INDIRECT_WR_CYC_OFFSET (0u)

#define XSPI_MASTER_INDIRECT_TX_BUF_BASE (0x240u)
#define XSPI_MASTER_INDIRECT_TX_BUF_OVERFLOW_WIDTH (1u)
#define XSPI_MASTER_INDIRECT_TX_BUF_OVERFLOW_OFFSET (24u)
#define XSPI_MASTER_INDIRECT_TX_BUF_ENTRY_WIDTH (8u)
#define XSPI_MASTER_INDIRECT_TX_BUF_ENTRY_OFFSET (16u)
#define XSPI_MASTER_INDIRECT_TX_BUF_LEVEL_WIDTH (8u)
#define XSPI_MASTER_INDIRECT_TX_BUF_LEVEL_OFFSET (8u)
#define XSPI_MASTER_INDIRECT_TX_BUF_FLUSH_WIDTH (1u)
#define XSPI_MASTER_INDIRECT_TX_BUF_FLUSH_OFFSET (0u)

#define XSPI_MASTER_INDIRECT_WDATA_BASE (0x280u)

#define XSPI_MASTER_INDIRECT_RD_CTRL_BASE (0x300u)
#define XSPI_MASTER_INDIRECT_RD_CTRL_R_TRIGGER_WIDTH (1u)
#define XSPI_MASTER_INDIRECT_RD_CTRL_R_TRIGGER_OFFSET (0u)

#define XSPI_MASTER_INDIRECT_RD_PH_CTRL_BASE (0x304u)
#define XSPI_MASTER_INDIRECT_RD_PH_CTRL_D_EN_WIDTH (1u)
#define XSPI_MASTER_INDIRECT_RD_PH_CTRL_D_EN_OFFSET (27u)
#define XSPI_MASTER_INDIRECT_RD_PH_CTRL_D_RATE_WIDTH (1u)
#define XSPI_MASTER_INDIRECT_RD_PH_CTRL_D_RATE_OFFSET (26u)
#define XSPI_MASTER_INDIRECT_RD_PH_CTRL_D_LINE_WIDTH (2u)
#define XSPI_MASTER_INDIRECT_RD_PH_CTRL_D_LINE_OFFSET (24u)
#define XSPI_MASTER_INDIRECT_RD_PH_CTRL_A_EN_WIDTH (1u)
#define XSPI_MASTER_INDIRECT_RD_PH_CTRL_A_EN_OFFSET (13u)
#define XSPI_MASTER_INDIRECT_RD_PH_CTRL_A_SIZE_WIDTH (2u)
#define XSPI_MASTER_INDIRECT_RD_PH_CTRL_A_SIZE_OFFSET (11u)
#define XSPI_MASTER_INDIRECT_RD_PH_CTRL_A_RATE_WIDTH (1u)
#define XSPI_MASTER_INDIRECT_RD_PH_CTRL_A_RATE_OFFSET (10u)
#define XSPI_MASTER_INDIRECT_RD_PH_CTRL_A_LINE_WIDTH (2u)
#define XSPI_MASTER_INDIRECT_RD_PH_CTRL_A_LINE_OFFSET (8u)
#define XSPI_MASTER_INDIRECT_RD_PH_CTRL_C_EN_WIDTH (1u)
#define XSPI_MASTER_INDIRECT_RD_PH_CTRL_C_EN_OFFSET (5u)
#define XSPI_MASTER_INDIRECT_RD_PH_CTRL_C_SIZE_WIDTH (2u)
#define XSPI_MASTER_INDIRECT_RD_PH_CTRL_C_SIZE_OFFSET (3u)
#define XSPI_MASTER_INDIRECT_RD_PH_CTRL_C_RATE_WIDTH (1u)
#define XSPI_MASTER_INDIRECT_RD_PH_CTRL_C_RATE_OFFSET (2u)
#define XSPI_MASTER_INDIRECT_RD_PH_CTRL_C_LINE_WIDTH (2u)
#define XSPI_MASTER_INDIRECT_RD_PH_CTRL_C_LINE_OFFSET (0u)

#define XSPI_MASTER_INDIRECT_RD_CMD_CODE_BASE (0x308u)
#define XSPI_MASTER_INDIRECT_RD_CMD_CODE_WIDTH (16u)
#define XSPI_MASTER_INDIRECT_RD_CMD_CODE_OFFSET (0u)

#define XSPI_MASTER_INDIRECT_RD_ADDR_BASE (0x30cu)

#define XSPI_MASTER_INDIRECT_RD_SIZE_BASE (0x310u)
#define XSPI_MASTER_INDIRECT_RD_SIZE_WIDTH (24u)
#define XSPI_MASTER_INDIRECT_RD_SIZE_OFFSET (0u)

#define XSPI_MASTER_INDIRECT_RD_CYC_BASE (0x314u)
#define XSPI_MASTER_INDIRECT_RD_CYC_WIDTH (5u)
#define XSPI_MASTER_INDIRECT_RD_CYC_OFFSET (0u)

#define XSPI_MASTER_INDIRECT_RX_BUF_BASE (0x340u)
#define XSPI_MASTER_INDIRECT_RX_BUF_OVERFLOW_WIDTH (1u)
#define XSPI_MASTER_INDIRECT_RX_BUF_OVERFLOW_OFFSET (24u)
#define XSPI_MASTER_INDIRECT_RX_BUF_ENTRY_WIDTH (8u)
#define XSPI_MASTER_INDIRECT_RX_BUF_ENTRY_OFFSET (16u)
#define XSPI_MASTER_INDIRECT_RX_BUF_LEVEL_WIDTH (8u)
#define XSPI_MASTER_INDIRECT_RX_BUF_LEVEL_OFFSET (8u)
#define XSPI_MASTER_INDIRECT_RX_BUF_FLUSH_WIDTH (1u)
#define XSPI_MASTER_INDIRECT_RX_BUF_FLUSH_OFFSET (0u)

#define XSPI_MASTER_INDIRECT_RDATA_BASE (0x380u)

#define XSPI_MASTER_MS_CTRL_BASE (0x800u)
#define XSPI_MASTER_MS_CTRL_IDLE_WIDTH (1u)
#define XSPI_MASTER_MS_CTRL_IDLE_OFFSET (30u)
#define XSPI_MASTER_MS_CTRL_SLV_ERR_RESP_EN_WIDTH (1u)
#define XSPI_MASTER_MS_CTRL_SLV_ERR_RESP_EN_OFFSET (17u)
#define XSPi_MASTER_MS_CTRL_CRC_EN_WIDTH (1u)
#define XSPI_MASTER_MS_CTRL_CRC_EN_OFFSET (16u)
#define XSPI_MASTER_MS_CTRL_DUMMY_WIDTH (5u)
#define XSPI_MASTER_MS_CTRL_DUMMY_OFFSET (4u)
#define XSPI_MASTER_MS_CTRL_RATE_WIDTH (1u)
#define XSPI_MASTER_MS_CTRL_RATE_OFFSET (2u)
#define XSPI_MASTER_MS_CTRL_LINE_WIDTH (2u)
#define XSPI_MASTER_MS_CTRL_LINE_OFFSET (0u)

#define XSPI_MASTER_MS_WAIT_TIME_CYCLE_BASE (0x810u)

#define XSPI_MASTER_MS_TIMEOUT_THRE_BASE (0x81cu)

#define XSPI_MASTER_MS_INT_ST_BASE (0x820u)
#define XSPI_MASTER_MS_INT_ST_SLV_AR_ACCESS_ILLEGAL_OFFSET (29u)
#define XSPI_MASTER_MS_INT_ST_SLV_RD_CRC_ERR_OFFSET (28u)
#define XSPI_MASTER_MS_INT_ST_SLV_RD_SID_ERR_OFFSET (27u)
#define XSPI_MASTER_MS_INT_ST_SLV_RD_ERR (26u)
#define XSPI_MASTER_MS_INT_ST_SLV_RD_MISMATCH_OFFSET (25u)
#define XSPI_MASTER_MS_INT_ST_SLV_RD_UTID_ERR_OFFSET (24u)
#define XSPI_MASTER_MS_INT_ST_SLV_WR_LEN_ERR_OFFSET (22u)
#define XSPI_MASTER_MS_INT_ST_SLV_AW_ACCESS_ILLEGAL_OFFSET (21u)
#define XSPI_MASTER_MS_INT_ST_SLV_WR_CRC_ERR_OFFSET (20u)
#define XSPI_MASTER_MS_INT_ST_SLV_WR_SID_ERR_OFFSET (19u)
#define XSPI_MASTER_MS_INT_ST_SLV_WR_ERR_OFFSET (18u)
#define XSPI_MASTER_MS_INT_ST_SLV_WR_MISMATCH_OFFSET (17u)
#define XSPI_MASTER_MS_INT_ST_SLV_WR_UTID_ERR_OFFSET (16u)
#define XSPI_MASTER_MS_INT_ST_WR_CRC_CHK_FAIL_OFFSET (12u)
#define XSPI_MASTER_MS_INT_ST_WR_SID_CHK_FAIL_OFFSET (11u)
#define XSPI_MASTER_MS_INT_ST_WR_USR_CHK_FAIL_OFFSET (10u)
#define XSPI_MASTER_MS_INT_ST_WR_AID_CHK_FAIL_OFFSET (9u)
#define XSPI_MASTER_MS_INT_ST_WR_TIMEOUT_OFFSET (8u)
#define XSPI_MASTER_MS_INT_ST_RD_LEN_CHK_FAIL_OFFSET (5u)
#define XSPI_MASTER_MS_INT_ST_RD_CRC_FAIL_OFFSET (4u)
#define XSPI_MASTER_MS_INT_ST_RD_SID_CHK_FAIL_OFFSET (3u)
#define XSPI_MASTER_MS_INT_ST_RD_USR_CHK_FAIL_OFFSET (2u)
#define XSPI_MASTER_MS_INT_ST_RD_AID_CHK_FAIL_OFFSET (1u)
#define XSPI_MASTER_MS_INT_ST_RD_TIMEOUT_OFFSET (0u)

#define XSPI_MASTER_MS_INT_EN_BASE (0x828u)
#define XSPI_MASTER_MS_INT_EN_SLV_AR_ACCESS_ILLEGAL_OFFSET (29u)
#define XSPI_MASTER_MS_INT_EN_SLV_RD_CRC_ERR_OFFSET (28u)
#define XSPI_MASTER_MS_INT_EN_SLV_RD_SID_ERR_OFFSET (27u)
#define XSPI_MASTER_MS_INT_EN_SLV_RD_ERR (26u)
#define XSPI_MASTER_MS_INT_EN_SLV_RD_MISMATCH_OFFSET (25u)
#define XSPI_MASTER_MS_INT_EN_SLV_RD_UTID_ERR_OFFSET (24u)
#define XSPI_MASTER_MS_INT_EN_SLV_WR_LEN_ERR_OFFSET (22u)
#define XSPI_MASTER_MS_INT_EN_SLV_AW_ACCESS_ILLEGAL_OFFSET (21u)
#define XSPI_MASTER_MS_INT_EN_SLV_WR_CRC_ERR_OFFSET (20u)
#define XSPI_MASTER_MS_INT_EN_SLV_WR_SID_ERR_OFFSET (19u)
#define XSPI_MASTER_MS_INT_EN_SLV_WR_ERR_OFFSET (18u)
#define XSPI_MASTER_MS_INT_EN_SLV_WR_MISMATCH_OFFSET (17u)
#define XSPI_MASTER_MS_INT_EN_SLV_WR_UTID_ERR_OFFSET (16u)
#define XSPI_MASTER_MS_INT_EN_WR_CRC_CHK_FAIL_OFFSET (12u)
#define XSPI_MASTER_MS_INT_EN_WR_SID_CHK_FAIL_OFFSET (11u)
#define XSPI_MASTER_MS_INT_EN_WR_USR_CHK_FAIL_OFFSET (10u)
#define XSPI_MASTER_MS_INT_EN_WR_AID_CHK_FAIL_OFFSET (9u)
#define XSPI_MASTER_MS_INT_EN_WR_TIMEOUT_OFFSET (8u)
#define XSPI_MASTER_MS_INT_EN_RD_LEN_CHK_FAIL_OFFSET (5u)
#define XSPI_MASTER_MS_INT_EN_RD_CRC_FAIL_OFFSET (4u)
#define XSPI_MASTER_MS_INT_EN_RD_SID_CHK_FAIL_OFFSET (3u)
#define XSPI_MASTER_MS_INT_EN_RD_USR_CHK_FAIL_OFFSET (2u)
#define XSPI_MASTER_MS_INT_EN_RD_AID_CHK_FAIL_OFFSET (1u)
#define XSPI_MASTER_MS_INT_EN_RD_TIMEOUT_OFFSET (0u)

#define XSPI_MASTER_SCLK_CTRL_BASE (0xc0u)
#define XSPI_MASTER_SCLK_CTRL_HW_SW_WIDTH (1u)
#define XSPI_MASTER_SCLK_CTRL_HW_SW_OFFSET (5u)
#define XSPI_MASTER_SCLK_CTRL_PHASE_WIDTH (1u)
#define XSPI_MASTER_SCLK_CTRL_PHASE_OFFSET (4u)
#define XSPI_MASTER_SCLK_CTRL_RX_SEL_WIDTH (2u)
#define XSPI_MASTER_SCLK_CTRL_RX_SEL_OFFSET (2u)
#define XSPI_MASTER_SCLK_CTRL_DLL_EN_WIDTH (1u)
#define XSPI_MASTER_SCLK_CTRL_DLL_EN_OFFSET (1u)
#define XSPI_MASTER_SCLK_CTRL_MODE_WIDTH (1u)
#define XSPI_MASTER_SCLK_CTRL_MODE_OFFSET (0u)

#define XSPI_MASTER_CS_CTRL_BASE (0xc4u)
#define XSPI_MASTER_CS_CTRL_CS_SEL_WIDTH (4u)
#define XSPI_MASTER_CS_CTRL_CS_SEL_OFFSET (25u)
#define XSPI_MASTER_CS_CTRL_CS_SW_WIDTH (1u)
#define XSPI_MASTER_CS_CTRL_CS_SW_OFFSET (24u)
#define XSPI_MASTER_CS_CTRL_CSS_TIME_WIDTH (4u)
#define XSPI_MASTER_CS_CTRL_CSS_TIME_OFFSET (16u)
#define XSPI_MASTER_CS_CTRL_SHSL_TIME_WIDTH (5u)
#define XSPI_MASTER_CS_CTRL_SHSL_TIME_OFFSET (8u)
#define XSPI_MASTER_CS_CTRL_CSH_TIME_WIDTH (4u)
#define XSPI_MASTER_CS_CTRL_CSH_TIME_OFFSET (0u)

#define XSPI_MASTER_DLL_CTRL_BASE (0xe0u)
#define XSPI_MASTER_DLL_CTRL_ENABLE_OFFSET (0u)
#define XSPI_MASTER_DLL_CTRL_ENABLE_WIDTH (1u)
#define XSPI_MASTER_DLL_CTRL_SLV_TARGET_OFFSET (1u)
#define XSPI_MASTER_DLL_CTRL_SLV_TARGET_WIDTH (5u)
#define XSPI_MASTER_DLL_SLV_UPD_GATE_OFFSET (31u)
#define XSPI_MASTER_DLL_SLV_UPD_GATE_WIDTH (1u)

#define XSPI_MASTER_DLL_ST_BASE (0xe4u)
#define XSPI_MASTER_DLL_ST_SLV_LOCK_OFFSET (16u)
#define XSPI_MASTER_DLL_ST_SLV_LOCK_WIDTH (1u)

static inline bool sdrv_xspi_master_get_slv_lock(paddr_t base)
{
    return !!(readl(base + XSPI_MASTER_DLL_ST_BASE) &
              (1u << XSPI_MASTER_DLL_ST_SLV_LOCK_OFFSET));
}

static inline void sdrv_xspi_master_set_dll_enable(paddr_t base, bool enable)
{
    RMWREG32(base + XSPI_MASTER_DLL_CTRL_BASE,
             XSPI_MASTER_DLL_CTRL_ENABLE_OFFSET,
             XSPI_MASTER_DLL_CTRL_ENABLE_WIDTH, !!enable);
}

static inline void sdrv_xspi_master_set_dll_slv_target(paddr_t base,
                                                      uint8_t target)
{
    RMWREG32(base + XSPI_MASTER_DLL_CTRL_BASE,
             XSPI_MASTER_DLL_CTRL_SLV_TARGET_OFFSET,
             XSPI_MASTER_DLL_CTRL_SLV_TARGET_WIDTH, target & 0x1f);
}

static inline void sdrv_xspi_master_set_slv_upd_date_enable(paddr_t base, bool enable)
{
    RMWREG32(base + XSPI_MASTER_DLL_CTRL_BASE,
             XSPI_MASTER_DLL_SLV_UPD_GATE_OFFSET,
             XSPI_MASTER_DLL_SLV_UPD_GATE_WIDTH, !!enable);
}

static inline void sdrv_xspi_master_sw_rst(paddr_t base)
{
    RMWREG32(base + XSPI_MASTER_MODE_CTRL_BASE,
             XSPI_MASTER_MODE_CTRL_SW_RST_OFFSET,
             XSPI_MASTER_MODE_CTRL_SW_RST_WIDTH, 1);
    RMWREG32(base + XSPI_MASTER_MODE_CTRL_BASE,
             XSPI_MASTER_MODE_CTRL_SW_RST_OFFSET,
             XSPI_MASTER_MODE_CTRL_SW_RST_WIDTH, 0);
}

static inline void sdrv_xspi_master_set_ms_mode(paddr_t base, bool is_ms)
{
    RMWREG32(base + XSPI_MASTER_MODE_CTRL_BASE,
             XSPI_MASTER_MODE_CTRL_MS_MODE_OFFSET,
             XSPI_MASTER_MODE_CTRL_MS_MODE_WIDTH, !!is_ms);
}

static inline uint32_t sdrv_xspi_master_get_int_st_fuc(paddr_t base)
{
    return readl(base + XSPI_MASTER_INT_ST_FUC_BASE);
}

static inline void sdrv_xspi_master_clr_int_st_fuc(paddr_t base, uint32_t st)
{
    writel(st, base + XSPI_MASTER_INT_ST_FUC_BASE);
}

static inline void sdrv_xspi_master_set_int_en_fuc(paddr_t base, uint32_t en)
{
    uint32_t reg = readl(base + XSPI_MASTER_INT_EN_FUC_BASE);

    reg |= (en);

    writel(reg, base + XSPI_MASTER_INT_EN_FUC_BASE);
}

static inline uint32_t sdrv_xspi_master_get_int_en_fuc(paddr_t base)
{
    return readl(base + XSPI_MASTER_INT_EN_FUC_BASE);
}

static inline void sdrv_xspi_master_clr_int_en_fuc(paddr_t base, uint32_t en)
{
    uint32_t reg = readl(base + XSPI_MASTER_INT_EN_FUC_BASE);

    reg &= ~(en);

    writel(reg, base + XSPI_MASTER_INT_EN_FUC_BASE);
}

static inline uint32_t sdrv_xspi_master_get_int_st_err(paddr_t base)
{
    return readl(base + XSPI_MASTER_INT_ST_ERR_BASE);
}

static inline void sdrv_xspi_master_clr_int_st_err(paddr_t base, uint32_t st)
{
    writel(st, base + XSPI_MASTER_INT_ST_ERR_BASE);
}

static inline  void sdrv_xspi_master_set_int_en_err(paddr_t base, uint32_t st)
{
    uint32_t reg_val = readl(base + XSPI_MASTER_INT_EN_ERR_BASE);

    reg_val |= st;

    writel(reg_val, base + XSPI_MASTER_INT_EN_ERR_BASE);
}

static inline void sdrv_xspi_master_clr_int_en_err(paddr_t base, uint32_t st)
{
    uint32_t reg_val = readl(base + XSPI_MASTER_INT_EN_ERR_BASE);

    reg_val &= ~st;

    writel(reg_val, base + XSPI_MASTER_INT_EN_ERR_BASE);
}

static inline void sdrv_xspi_master_set_int_en_err_fuc(paddr_t base, uint32_t st)
{
    uint32_t reg_val = readl(base + XSPI_MASTER_INT_EN_ERR_FUC_BASE);

    reg_val |= st;

    writel(reg_val, base + XSPI_MASTER_INT_EN_ERR_FUC_BASE);
}

static inline void sdrv_xspi_master_clr_int_en_err_fuc(paddr_t base, uint32_t st)
{
    uint32_t reg_val = readl(base + XSPI_MASTER_INT_EN_ERR_FUC_BASE);

    reg_val &= ~st;

    writel(reg_val, base + XSPI_MASTER_INT_EN_ERR_FUC_BASE);
}


static inline void sdrv_xspi_master_set_addr_offset_val(paddr_t base, uint32_t val)
{
    RMWREG32(base + XSPI_MASTER_ADDR_OFFSET_BASE,
             XSPI_MASTER_ADDR_OFFSET_VAL_OFFSET,
             XSPI_MASTER_ADDR_OFFSET_VAL_WIDTH, val);
}

static inline uint32_t sdrv_xspi_master_get_addr_offset_val(paddr_t base)
{
    return readl(base + XSPI_MASTER_ADDR_OFFSET_BASE);
}

static inline void sdrv_xspi_master_set_addr_offset_en(paddr_t base, bool enable)
{
    RMWREG32(base + XSPI_MASTER_ADDR_OFFSET_BASE,
             XSPI_MASTER_ADDR_OFFSET_EN_OFFSET,
             XSPI_MASTER_ADDR_OFFSET_EN_WIDTH, !!enable);
}

static inline bool sdrv_xspi_master_get_addr_offset_en(paddr_t base)
{
    return !!(readl(base + XSPI_MASTER_ADDR_OFFSET_BASE) &
              (1u << XSPI_MASTER_ADDR_OFFSET_EN_OFFSET));
}

static inline bool sdrv_xspi_master_is_busy(paddr_t base)
{
    uint32_t reg = readl(base + XSPI_MASTER_MS_CTRL_BASE);

    return !(reg & (1u << XSPI_MASTER_MS_CTRL_IDLE_OFFSET));
}

static inline void sdrv_xspi_master_set_slv_err_resp_en(paddr_t base, bool enable)
{
    RMWREG32(base + XSPI_MASTER_MS_CTRL_BASE,
             XSPI_MASTER_MS_CTRL_SLV_ERR_RESP_EN_OFFSET,
             XSPI_MASTER_MS_CTRL_SLV_ERR_RESP_EN_WIDTH, !!enable);
}

static inline bool sdrv_xspi_master_get_slv_err_resp_en(paddr_t base)
{
    return !!(readl(base + XSPI_MASTER_MS_CTRL_BASE) &
             (1u << XSPI_MASTER_MS_CTRL_SLV_ERR_RESP_EN_OFFSET));
}

static inline void sdrv_xspi_master_set_crc_en(paddr_t base, bool enable)
{
    RMWREG32(base + XSPI_MASTER_MS_CTRL_BASE,
             XSPI_MASTER_MS_CTRL_CRC_EN_OFFSET,
             XSPi_MASTER_MS_CTRL_CRC_EN_WIDTH, !!enable);
}

static inline bool sdrv_xspi_master_get_crc_en(paddr_t base)
{
    return !!(readl(base + XSPI_MASTER_MS_CTRL_BASE) &
              (1u << XSPI_MASTER_MS_CTRL_CRC_EN_OFFSET));
}

static inline void sdrv_xspi_master_set_dummy(paddr_t base, uint8_t dummy)
{
    RMWREG32(base + XSPI_MASTER_MS_CTRL_BASE,
             XSPI_MASTER_MS_CTRL_DUMMY_OFFSET,
             XSPI_MASTER_MS_CTRL_DUMMY_WIDTH, dummy & 0x1f);
}

static inline uint8_t sdrv_xspi_master_get_dummy(paddr_t base)
{
    return (readl(base + XSPI_MASTER_MS_CTRL_BASE) >>
            XSPI_MASTER_MS_CTRL_DUMMY_OFFSET) & 0x1f;
}

static inline void sdrv_xspi_master_set_rate(paddr_t base, bool is_ddr)
{
    RMWREG32(base + XSPI_MASTER_MS_CTRL_BASE,
             XSPI_MASTER_MS_CTRL_RATE_OFFSET,
             XSPI_MASTER_MS_CTRL_RATE_WIDTH, !!is_ddr);
}

static inline bool sdrv_xspi_master_get_rate(paddr_t base)
{
    return !!(readl(base + XSPI_MASTER_MS_CTRL_BASE) &
              (1u << XSPI_MASTER_MS_CTRL_RATE_OFFSET));
}

static inline void sdrv_xspi_master_set_line(paddr_t base, uint8_t line)
{
    RMWREG32(base + XSPI_MASTER_MS_CTRL_BASE,
             XSPI_MASTER_MS_CTRL_LINE_OFFSET,
             XSPI_MASTER_MS_CTRL_LINE_WIDTH, line & 0x3);
}

static inline uint8_t sdrv_xspi_master_get_line(paddr_t base)
{
    return (readl(base + XSPI_MASTER_MS_CTRL_BASE) >>
            XSPI_MASTER_MS_CTRL_LINE_OFFSET) & 0x3;
}


static inline uint32_t sdrv_xspi_master_get_wait_time_cycle(paddr_t base)
{
    return readl(base + XSPI_MASTER_MS_WAIT_TIME_CYCLE_BASE);
}

static inline void sdrv_xspi_master_set_timeout_thrd(paddr_t base, uint32_t thrd)
{
    writel(thrd, base + XSPI_MASTER_MS_TIMEOUT_THRE_BASE);
}

static inline uint32_t sdrv_xspi_master_get_timeout_thrd(paddr_t base)
{
    return readl(base + XSPI_MASTER_MS_TIMEOUT_THRE_BASE);
}

static inline uint32_t sdrv_xspi_master_get_ms_int_st(paddr_t base)
{
    return readl(base + XSPI_MASTER_MS_INT_ST_BASE);
}

static inline void sdrv_xspi_master_clr_ms_int_st(paddr_t base, uint32_t st)
{
    writel(st, base + XSPI_MASTER_MS_INT_ST_BASE);
}

static inline void sdrv_xspi_master_set_sw_pad_rst_en(paddr_t base, bool enable)
{
    RMWREG32(base + XSPI_MASTER_IO_CTRL_BASE,
             XSPI_MASTER_IO_CTRL_SW_PAD_RST_EN_OFFSET,
             XSPI_MASTER_IO_CTRL_SW_PAD_RST_EN_WIDTH, !!enable);
}

static inline void sdrv_xspi_master_set_sw_pad_rstn(paddr_t base, bool enable)
{
    RMWREG32(base + XSPI_MASTER_IO_CTRL_BASE,
             XSPI_MASTER_IO_CTRL_SW_PAD_RSTN_OFFSET,
             XSPI_MASTER_IO_CTRL_SW_PAD_RSTN_WIDTH, !!enable);
}

static inline void sdrv_xspi_master_sw_rst_pad(paddr_t base)
{
    sdrv_xspi_master_set_sw_pad_rst_en(base, 1);
    sdrv_xspi_master_set_sw_pad_rstn(base, 0);
    sdrv_xspi_master_set_sw_pad_rstn(base, 1);
}

static inline void sdrv_xspi_master_set_ms_int_en(paddr_t base, uint32_t en)
{
    uint32_t reg = readl(base + XSPI_MASTER_MS_INT_EN_BASE);

    reg |= (en);

    writel(reg, base + XSPI_MASTER_MS_INT_EN_BASE);
}

static inline void sdrv_xspi_master_clr_ms_int_en(paddr_t base, uint32_t en)
{
    uint32_t reg = readl(base + XSPI_MASTER_MS_INT_EN_BASE);

    reg &= ~(en);

    writel(reg, base + XSPI_MASTER_MS_INT_EN_BASE);
}

static inline void sdrv_xspi_master_set_sclk_sw(paddr_t base, bool sw)
{
    RMWREG32(base + XSPI_MASTER_SCLK_CTRL_BASE,
             XSPI_MASTER_SCLK_CTRL_HW_SW_OFFSET,
             XSPI_MASTER_SCLK_CTRL_HW_SW_WIDTH, !!sw);
}

static inline void sdrv_xspi_master_set_sclk_phase(paddr_t base, bool is_high)
{
    RMWREG32(base + XSPI_MASTER_SCLK_CTRL_BASE,
             XSPI_MASTER_SCLK_CTRL_PHASE_OFFSET,
             XSPI_MASTER_SCLK_CTRL_PHASE_WIDTH, !!is_high);
}

static inline void sdrv_xspi_master_set_sclk_rx_sel(paddr_t base, uint8_t rx_sel)
{
    RMWREG32(base + XSPI_MASTER_SCLK_CTRL_BASE,
             XSPI_MASTER_SCLK_CTRL_RX_SEL_OFFSET,
             XSPI_MASTER_SCLK_CTRL_RX_SEL_WIDTH, rx_sel & 0x3);
}

static inline void sdrv_xspi_master_set_sclk_mode(paddr_t base, bool is_ddr)
{
    RMWREG32(base + XSPI_MASTER_SCLK_CTRL_BASE,
             XSPI_MASTER_SCLK_CTRL_MODE_OFFSET,
             XSPI_MASTER_SCLK_CTRL_MODE_WIDTH, !!is_ddr);
}

static inline void sdrv_xspi_master_set_sclk_dll_en(paddr_t base, bool enable)
{
    RMWREG32(base + XSPI_MASTER_SCLK_CTRL_BASE,
             XSPI_MASTER_SCLK_CTRL_DLL_EN_OFFSET,
             XSPI_MASTER_SCLK_CTRL_DLL_EN_WIDTH, !!enable);
}

static inline void sdrv_xspi_master_set_cs_sel(paddr_t base, uint8_t sel)
{
    RMWREG32(base + XSPI_MASTER_CS_CTRL_BASE,
             XSPI_MASTER_CS_CTRL_CS_SEL_OFFSET,
             XSPI_MASTER_CS_CTRL_CS_SEL_WIDTH, sel & 0xf);
}

static inline void sdrv_xspi_master_set_cs_sw(paddr_t base, bool is_sw)
{
    RMWREG32(base + XSPI_MASTER_CS_CTRL_BASE,
             XSPI_MASTER_CS_CTRL_CS_SW_OFFSET,
             XSPI_MASTER_CS_CTRL_CS_SW_WIDTH, !!is_sw);
}

static inline void sdrv_xspi_master_set_css_time(paddr_t base, uint8_t css_time)
{
    RMWREG32(base + XSPI_MASTER_CS_CTRL_BASE,
             XSPI_MASTER_CS_CTRL_CSS_TIME_OFFSET,
             XSPI_MASTER_CS_CTRL_CSS_TIME_WIDTH, css_time & 0xf);
}

static inline void sdrv_xspi_master_set_shsl_time(paddr_t base, uint8_t shsl_time)
{
    RMWREG32(base + XSPI_MASTER_CS_CTRL_BASE,
             XSPI_MASTER_CS_CTRL_SHSL_TIME_OFFSET,
             XSPI_MASTER_CS_CTRL_SHSL_TIME_WIDTH, shsl_time & 0x1f);
}

static inline void sdrv_xspi_master_set_csh_time(paddr_t base, uint8_t csh_time)
{
    RMWREG32(base + XSPI_MASTER_CS_CTRL_BASE,
             XSPI_MASTER_CS_CTRL_CSH_TIME_OFFSET,
             XSPI_MASTER_CS_CTRL_CSH_TIME_WIDTH, csh_time & 0xf);
}

static inline bool sdrv_xspi_master_core_is_busy(paddr_t base)
{
    uint32_t reg = readl(base + XSPI_MASTER_MODE_CTRL_BASE);

    return !(reg & (1u << XSPI_MASTER_MODE_CTRL_IDLE_OFFSET));
}

static inline void sdrv_xspi_master_indirect_w_trigger(paddr_t base)
{
    RMWREG32(base + XSPI_MASTER_INDIRECT_WR_CTRL_BASE,
             XSPI_MASTER_INDIRECT_WR_CTRL_W_TRIGGER_OFFSET,
             XSPI_MASTER_INDIRECT_WR_CTRL_W_TRIGGER_WIDTH, 1);
}

static inline bool sdrv_xspi_master_indirect_w_get_trigger(paddr_t base)
{
    return (readl(base + XSPI_MASTER_INDIRECT_WR_CTRL_BASE) & 0x1);
}


static inline void sdrv_xspi_master_indirect_wr_set_d_en(paddr_t base, bool enable)
{
    RMWREG32(base + XSPI_MASTER_INDIRECT_WR_PH_CTRL_BASE,
             XSPI_MASTER_INDIRECT_WR_PH_CTRL_D_EN_OFFSET,
             XSPI_MASTER_INDIRECT_WR_PH_CTRL_D_EN_WIDTH, !!enable);
}

static inline void sdrv_xspi_master_indirect_wr_set_d_rate(paddr_t base, bool is_ddr)
{
    RMWREG32(base + XSPI_MASTER_INDIRECT_WR_PH_CTRL_BASE,
             XSPI_MASTER_INDIRECT_WR_PH_CTRL_D_RATE_OFFSET,
             XSPI_MASTER_INDIRECT_WR_PH_CTRL_D_RATE_WIDTH, !!is_ddr);
}

static inline void sdrv_xspi_master_indirect_wr_set_d_line(paddr_t base, uint8_t line)
{
    RMWREG32(base + XSPI_MASTER_INDIRECT_WR_PH_CTRL_BASE,
             XSPI_MASTER_INDIRECT_WR_PH_CTRL_D_LINE_OFFSET,
             XSPI_MASTER_INDIRECT_WR_PH_CTRL_D_LINE_WIDTH, line & 0x3);
}

static inline void sdrv_xspi_master_indirect_wr_set_a_en(paddr_t base, bool enable)
{
    RMWREG32(base + XSPI_MASTER_INDIRECT_WR_PH_CTRL_BASE,
             XSPI_MASTER_INDIRECT_WR_PH_CTRL_A_EN_OFFSET,
             XSPI_MASTER_INDIRECT_WR_PH_CTRL_A_EN_WIDTH, !!enable);
}

static inline void sdrv_xspi_master_indirect_wr_set_a_size(paddr_t base, uint8_t size)
{
    RMWREG32(base + XSPI_MASTER_INDIRECT_WR_PH_CTRL_BASE,
             XSPI_MASTER_INDIRECT_WR_PH_CTRL_A_SIZE_OFFSET,
             XSPI_MASTER_INDIRECT_WR_PH_CTRL_A_SIZE_WIDTH, size & 0x3);
}

static inline void sdrv_xspi_master_indirect_wr_set_a_rate(paddr_t base, bool is_ddr)
{
    RMWREG32(base + XSPI_MASTER_INDIRECT_WR_PH_CTRL_BASE,
             XSPI_MASTER_INDIRECT_WR_PH_CTRL_A_RATE_OFFSET,
             XSPI_MASTER_INDIRECT_WR_PH_CTRL_A_RATE_WIDTH, !!is_ddr);
}

static inline void sdrv_xspi_master_indirect_wr_set_a_line(paddr_t base, uint8_t line)
{
    RMWREG32(base + XSPI_MASTER_INDIRECT_WR_PH_CTRL_BASE,
             XSPI_MASTER_INDIRECT_WR_PH_CTRL_A_LINE_OFFSET,
             XSPI_MASTER_INDIRECT_WR_PH_CTRL_A_LINE_WIDTH, line & 0x3);
}

static inline void sdrv_xspi_master_indirect_wr_set_c_en(paddr_t base, bool enable)
{
    RMWREG32(base + XSPI_MASTER_INDIRECT_WR_PH_CTRL_BASE,
             XSPI_MASTER_INDIRECT_WR_PH_CTRL_C_EN_OFFSET,
             XSPI_MASTER_INDIRECT_WR_PH_CTRL_C_EN_WIDTH, !!enable);
}

static inline void sdrv_xspi_master_indirect_wr_set_c_size(paddr_t base, uint8_t size)
{
    RMWREG32(base + XSPI_MASTER_INDIRECT_WR_PH_CTRL_BASE,
             XSPI_MASTER_INDIRECT_WR_PH_CTRL_C_SIZE_OFFSET,
             XSPI_MASTER_INDIRECT_WR_PH_CTRL_C_SIZE_WIDTH, size & 0x3);
}

static inline void sdrv_xspi_master_indirect_wr_set_c_rate(paddr_t base, bool is_ddr)
{
    RMWREG32(base + XSPI_MASTER_INDIRECT_WR_PH_CTRL_BASE,
             XSPI_MASTER_INDIRECT_WR_PH_CTRL_C_RATE_OFFSET,
             XSPI_MASTER_INDIRECT_WR_PH_CTRL_C_RATE_WIDTH, !!is_ddr);
}

static inline void sdrv_xspi_master_indirect_wr_set_c_line(paddr_t base, uint8_t line)
{
    RMWREG32(base + XSPI_MASTER_INDIRECT_WR_PH_CTRL_BASE,
             XSPI_MASTER_INDIRECT_WR_PH_CTRL_C_LINE_OFFSET,
             XSPI_MASTER_INDIRECT_WR_PH_CTRL_C_LINE_WIDTH, line & 0x3);
}

static inline void sdrv_xspi_master_indirect_wr_set_cmd(paddr_t base, uint16_t cmd)
{
    RMWREG32(base + XSPI_MASTER_INDIRECT_WR_CMD_CODE_BASE,
             XSPI_MASTER_INDIRECT_WR_CMD_CODE_OFFSET,
             XSPI_MASTER_INDIRECT_WR_CMD_CODE_WIDTH, cmd & 0xffff);
}

static inline void sdrv_xspi_master_indirect_wr_set_addr(paddr_t base, uint32_t addr)
{
    writel(addr, base + XSPI_MASTER_INDIRECT_WR_ADDR_BASE);
}

static inline void sdrv_xspi_master_indirect_wr_set_size(paddr_t base, uint32_t size)
{
    RMWREG32(base + XSPI_MASTER_INDIRECT_WR_SIZE_BASE,
             XSPI_MASTER_INDIRECT_WR_SIZE_OFFSET,
             XSPI_MASTER_INDIRECT_WR_SIZE_WIDTH, size & 0xffffff);
}

static inline void sdrv_xspi_master_indirect_wr_set_dummy(paddr_t base, uint8_t dummy)
{
    RMWREG32(base + XSPI_MASTER_INDIRECT_WR_CYC_BASE,
             XSPI_MASTER_INDIRECT_WR_CYC_OFFSET,
             XSPI_MASTER_INDIRECT_WR_CYC_WIDTH, dummy & 0x1f);
}

static inline bool sdrv_xspi_master_indirect_tx_buf_is_overflow(paddr_t base)
{
    uint32_t reg = readl(base + XSPI_MASTER_INDIRECT_TX_BUF_BASE);

    return (reg & (1u << XSPI_MASTER_INDIRECT_TX_BUF_OVERFLOW_OFFSET));
}

static inline void sdrv_xspi_master_indirect_tx_buf_clr_overflow(paddr_t base)
{
    RMWREG32(base + XSPI_MASTER_INDIRECT_TX_BUF_BASE,
             XSPI_MASTER_INDIRECT_TX_BUF_OVERFLOW_OFFSET,
             XSPI_MASTER_INDIRECT_TX_BUF_OVERFLOW_WIDTH, 0);
}

static inline uint8_t sdrv_xspi_master_indirect_tx_buf_get_entry_num(paddr_t base)
{
    uint32_t reg = readl(base + XSPI_MASTER_INDIRECT_TX_BUF_BASE);

    return ((reg >> XSPI_MASTER_INDIRECT_TX_BUF_ENTRY_OFFSET) & 0xff);
}

static inline void sdrv_xspi_master_indirect_tx_buf_set_level(paddr_t base, uint8_t level)
{
    RMWREG32(base + XSPI_MASTER_INDIRECT_TX_BUF_BASE,
             XSPI_MASTER_INDIRECT_TX_BUF_LEVEL_OFFSET,
             XSPI_MASTER_INDIRECT_TX_BUF_LEVEL_WIDTH, level & 0xff);
}

static inline void sdrv_xspi_master_indirect_tx_buf_flush(paddr_t base)
{
    RMWREG32(base + XSPI_MASTER_INDIRECT_TX_BUF_BASE,
             XSPI_MASTER_INDIRECT_TX_BUF_FLUSH_OFFSET,
             XSPI_MASTER_INDIRECT_TX_BUF_FLUSH_WIDTH, 1);

    while (readl(base + XSPI_MASTER_INDIRECT_TX_BUF_BASE) &
          (1u << XSPI_MASTER_INDIRECT_TX_BUF_FLUSH_OFFSET));
}

static inline void sdrv_xspi_master_indirect_wdata(paddr_t base, uint32_t data)
{
    writel(data, base + XSPI_MASTER_INDIRECT_WDATA_BASE);
}

static inline void sdrv_xspi_master_indirect_r_trigger(paddr_t base)
{
    RMWREG32(base + XSPI_MASTER_INDIRECT_RD_CTRL_BASE,
             XSPI_MASTER_INDIRECT_RD_CTRL_R_TRIGGER_OFFSET,
             XSPI_MASTER_INDIRECT_RD_CTRL_R_TRIGGER_WIDTH, 1);
}

static inline void sdrv_xspi_master_indirect_rd_set_d_en(paddr_t base, bool enable)
{
    RMWREG32(base + XSPI_MASTER_INDIRECT_RD_PH_CTRL_BASE,
             XSPI_MASTER_INDIRECT_RD_PH_CTRL_D_EN_OFFSET,
             XSPI_MASTER_INDIRECT_RD_PH_CTRL_D_EN_WIDTH, !!enable);
}

static inline void sdrv_xspi_master_indirect_rd_set_d_rate(paddr_t base, bool is_ddr)
{
    RMWREG32(base + XSPI_MASTER_INDIRECT_RD_PH_CTRL_BASE,
             XSPI_MASTER_INDIRECT_RD_PH_CTRL_D_RATE_OFFSET,
             XSPI_MASTER_INDIRECT_RD_PH_CTRL_D_RATE_WIDTH, !!is_ddr);
}

static inline void sdrv_xspi_master_indirect_rd_set_d_line(paddr_t base, uint8_t line)
{
    RMWREG32(base + XSPI_MASTER_INDIRECT_RD_PH_CTRL_BASE,
             XSPI_MASTER_INDIRECT_RD_PH_CTRL_D_LINE_OFFSET,
             XSPI_MASTER_INDIRECT_RD_PH_CTRL_D_LINE_WIDTH, line & 0x3);
}

static inline void sdrv_xspi_master_indirect_rd_set_a_en(paddr_t base, bool enable)
{
    RMWREG32(base + XSPI_MASTER_INDIRECT_RD_PH_CTRL_BASE,
             XSPI_MASTER_INDIRECT_RD_PH_CTRL_A_EN_OFFSET,
             XSPI_MASTER_INDIRECT_RD_PH_CTRL_A_EN_WIDTH, !!enable);
}

static inline void sdrv_xspi_master_indirect_rd_set_a_size(paddr_t base, uint8_t size)
{
    RMWREG32(base + XSPI_MASTER_INDIRECT_RD_PH_CTRL_BASE,
             XSPI_MASTER_INDIRECT_RD_PH_CTRL_A_SIZE_OFFSET,
             XSPI_MASTER_INDIRECT_RD_PH_CTRL_A_SIZE_WIDTH, size & 0x3);
}

static inline void sdrv_xspi_master_indirect_rd_set_a_rate(paddr_t base, bool is_ddr)
{
    RMWREG32(base + XSPI_MASTER_INDIRECT_RD_PH_CTRL_BASE,
             XSPI_MASTER_INDIRECT_RD_PH_CTRL_A_RATE_OFFSET,
             XSPI_MASTER_INDIRECT_RD_PH_CTRL_A_RATE_WIDTH, !!is_ddr);
}

static inline void sdrv_xspi_master_indirect_rd_set_a_line(paddr_t base, uint8_t line)
{
    RMWREG32(base + XSPI_MASTER_INDIRECT_RD_PH_CTRL_BASE,
             XSPI_MASTER_INDIRECT_RD_PH_CTRL_A_LINE_OFFSET,
             XSPI_MASTER_INDIRECT_RD_PH_CTRL_A_LINE_WIDTH, line & 0x3);
}

static inline void sdrv_xspi_master_indirect_rd_set_c_en(paddr_t base, bool enable)
{
    RMWREG32(base + XSPI_MASTER_INDIRECT_RD_PH_CTRL_BASE,
             XSPI_MASTER_INDIRECT_RD_PH_CTRL_C_EN_OFFSET,
             XSPI_MASTER_INDIRECT_RD_PH_CTRL_C_EN_WIDTH, !!enable);
}

static inline void sdrv_xspi_master_indirect_rd_set_c_size(paddr_t base, uint8_t size)
{
    RMWREG32(base + XSPI_MASTER_INDIRECT_RD_PH_CTRL_BASE,
             XSPI_MASTER_INDIRECT_RD_PH_CTRL_C_SIZE_OFFSET,
             XSPI_MASTER_INDIRECT_RD_PH_CTRL_C_SIZE_WIDTH, size & 0x3);
}

static inline void sdrv_xspi_master_indirect_rd_set_c_rate(paddr_t base, bool is_ddr)
{
    RMWREG32(base + XSPI_MASTER_INDIRECT_RD_PH_CTRL_BASE,
             XSPI_MASTER_INDIRECT_RD_PH_CTRL_C_RATE_OFFSET,
             XSPI_MASTER_INDIRECT_RD_PH_CTRL_C_RATE_WIDTH, !!is_ddr);
}

static inline void sdrv_xspi_master_indirect_rd_set_c_line(paddr_t base, uint8_t line)
{
    RMWREG32(base + XSPI_MASTER_INDIRECT_RD_PH_CTRL_BASE,
             XSPI_MASTER_INDIRECT_RD_PH_CTRL_C_LINE_OFFSET,
             XSPI_MASTER_INDIRECT_RD_PH_CTRL_C_LINE_WIDTH, line & 0x3);
}

static inline void sdrv_xspi_master_indirect_rd_set_cmd(paddr_t base, uint16_t cmd)
{
    RMWREG32(base + XSPI_MASTER_INDIRECT_RD_CMD_CODE_BASE,
             XSPI_MASTER_INDIRECT_RD_CMD_CODE_OFFSET,
             XSPI_MASTER_INDIRECT_RD_CMD_CODE_WIDTH, cmd & 0xffff);
}

static inline void sdrv_xspi_master_indirect_rd_set_addr(paddr_t base, uint32_t addr)
{
    writel(addr, base + XSPI_MASTER_INDIRECT_RD_ADDR_BASE);
}

static inline void sdrv_xspi_master_indirect_rd_set_size(paddr_t base, uint32_t size)
{
    RMWREG32(base + XSPI_MASTER_INDIRECT_RD_SIZE_BASE,
             XSPI_MASTER_INDIRECT_RD_SIZE_OFFSET,
             XSPI_MASTER_INDIRECT_RD_SIZE_WIDTH, size & 0xffffff);
}

static inline void sdrv_xspi_master_indirect_rd_set_dummy(paddr_t base, uint8_t dummy)
{
    RMWREG32(base + XSPI_MASTER_INDIRECT_RD_CYC_BASE,
             XSPI_MASTER_INDIRECT_RD_CYC_OFFSET,
             XSPI_MASTER_INDIRECT_RD_CYC_WIDTH, dummy & 0x1f);
}

static inline bool sdrv_xspi_master_indirect_rx_buf_is_overflow(paddr_t base)
{
    uint32_t reg = readl(base + XSPI_MASTER_INDIRECT_RX_BUF_BASE);

    return (reg & (1u << XSPI_MASTER_INDIRECT_RX_BUF_OVERFLOW_OFFSET));
}

static inline void sdrv_xspi_master_indirect_rx_buf_clr_overflow(paddr_t base)
{
    RMWREG32(base + XSPI_MASTER_INDIRECT_RX_BUF_BASE,
             XSPI_MASTER_INDIRECT_RX_BUF_OVERFLOW_OFFSET,
             XSPI_MASTER_INDIRECT_RX_BUF_OVERFLOW_WIDTH, 0);
}

static inline uint8_t sdrv_xspi_master_indirect_rx_buf_get_entry_num(paddr_t base)
{
    uint32_t reg = readl(base + XSPI_MASTER_INDIRECT_RX_BUF_BASE);

    return (reg >> XSPI_MASTER_INDIRECT_RX_BUF_ENTRY_OFFSET) & 0xff;
}

static inline void sdrv_xspi_master_indirect_rx_buf_set_level(paddr_t base, uint8_t level)
{
    RMWREG32(base + XSPI_MASTER_INDIRECT_RX_BUF_BASE,
             XSPI_MASTER_INDIRECT_RX_BUF_LEVEL_OFFSET,
             XSPI_MASTER_INDIRECT_RX_BUF_LEVEL_WIDTH, level & 0xff);
}

static inline void sdrv_xspi_master_indirect_rx_buf_flush(paddr_t base)
{
    RMWREG32(base + XSPI_MASTER_INDIRECT_RX_BUF_BASE,
             XSPI_MASTER_INDIRECT_RX_BUF_FLUSH_OFFSET,
             XSPI_MASTER_INDIRECT_RX_BUF_FLUSH_WIDTH, 1);

    while (readl(base + XSPI_MASTER_INDIRECT_RX_BUF_BASE) &
          (1u << XSPI_MASTER_INDIRECT_RX_BUF_FLUSH_OFFSET));
}

static inline void sdrv_xspi_master_set_cs_time(paddr_t base, uint8_t css, uint8_t shsl, uint8_t csh)
{
    RMWREG32(base + XSPI_MASTER_CS_CTRL_BASE,
             XSPI_MASTER_CS_CTRL_CSS_TIME_OFFSET,
             XSPI_MASTER_CS_CTRL_CSS_TIME_WIDTH, css);

    RMWREG32(base + XSPI_MASTER_CS_CTRL_BASE,
             XSPI_MASTER_CS_CTRL_SHSL_TIME_OFFSET,
             XSPI_MASTER_CS_CTRL_SHSL_TIME_WIDTH, shsl);

    RMWREG32(base + XSPI_MASTER_CS_CTRL_BASE,
             XSPI_MASTER_CS_CTRL_CSH_TIME_OFFSET,
             XSPI_MASTER_CS_CTRL_CSH_TIME_WIDTH, csh);
}

static inline void sdrv_xspi_master_set_rxmode(paddr_t base, uint8_t mode)
{
    RMWREG32(base + XSPI_MASTER_MODE_CTRL_BASE,
             XSPI_MASTER_MODE_CTRL_RX_MODE,
             XSPI_MASTER_MODE_CTRL_RX_MODE_WIDTH, !!mode);
}

static inline void sdrv_xspi_master_set_rxenable_mode(paddr_t base, bool enable)
{
   RMWREG32(base + XSPI_MASTER_MISC_CTRL_BASE,
            XSPI_MASTER_MISC_CTRL_RXENABLE_MODE,
            XSPI_MASTER_MISC_CTRL_RXENABLE_MODE_WIDTH, !!enable);
}

static inline uint32_t sdrv_xspi_master_indirect_rdata(paddr_t base)
{
    return readl(base + XSPI_MASTER_INDIRECT_RDATA_BASE);
}

#ifdef __cplusplus
}
#endif

#endif



