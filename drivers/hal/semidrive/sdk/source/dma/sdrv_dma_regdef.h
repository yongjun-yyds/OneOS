/**
 * @file sdrv_dma_regdef.h
 * @brief SemiDrive DMA register definitions.
 *
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef __SDRV_DMA_REGDEF_H__
#define __SDRV_DMA_REGDEF_H__

#include <types.h>

#define SDRV_DMA_FIFO_OFFSET (0xA000)
#define SDRV_DMA_FIFO_SIZE (8192)
#define SDRV_DMA_INT_CFG_BASE_OFFSET (0x7E0)
#define SDRV_DMA_CHANNEL_BASE_OFFSET (0x10000)
#define SDRV_DMA_CHANNEL_SIZE (0x2000)

/**
 * @brief DMA channel config register structure.
 */
typedef union {
    struct {
        uint32_t stop : 1;
        uint32_t flush : 1;
        uint32_t req_flush : 1;
        uint32_t halt : 1;
        uint32_t en : 1;
        uint32_t resume : 1;
        uint32_t abort : 1;
        uint32_t reserved : 25;
    };
    uint32_t v;
} sdrv_dma_ch_cfg_reg;

/**
 * @brief DMA channel port config register structure.
 */
typedef union {
    struct {
        uint32_t wqos : 4;
        uint32_t wot : 4;
        uint32_t rqos : 4;
        uint32_t rot : 4;
        uint32_t csel : 8;
        uint32_t reserved : 4;
        uint32_t pspdis : 1;
        uint32_t reserved1 : 2;
        uint32_t lock : 1;
    };
    uint32_t v;
} sdrv_dma_ch_port_cfg_reg;

/**
 * @brief DMA channel stride config register structure.
 */
typedef union {
    struct {
        uint32_t src_fine_stride : 16;
        uint32_t dst_fine_stride : 16;
    };
    uint32_t v;
} sdrv_dma_ch_stride_cfg_reg;

/**
 * @brief DMA channel firewall_up boundary register structure.
 */
typedef union {
    struct {
        uint32_t lock : 1;
        uint32_t fw_up_en : 1;
        uint32_t fw_addr_up : 30;
    };
    uint32_t v;
} sdrv_dma_ch_fw_up_boundary_reg;

/**
 * @brief DMA channel firewall_dw boundary register structure.
 */
typedef union {
    struct {
        uint32_t lock : 1;
        uint32_t fw_dw_en : 1;
        uint32_t fw_addr_dw : 30;
    };
    uint32_t v;
} sdrv_dma_ch_fw_dw_boundary_reg;

/**
 * @brief DMA channel pattern match config register structure.
 */
typedef union {
    struct {
        uint32_t pattern : 32;
    };
    uint32_t v;
} sdrv_dma_ch_patn_match_cfg_reg;

/**
 * @brief DMA channel pattern mask config register structure.
 */
typedef union {
    struct {
        uint32_t mask : 32;
    };
    uint32_t v;
} sdrv_dma_ch_patn_mask_cfg_reg;

/**
 * @brief DMA channel second operation register structure.
 */
typedef union {
    struct {
        uint32_t opd2 : 32;
    };
    uint32_t v;
} sdrv_dma_ch_op2_data_reg;

/**
 * @brief DMA channel source address register structure.
 */
typedef union {
    struct {
        uint32_t src;
    };
    uint32_t v;
} sdrv_dma_ch_src_reg;

/**
 * @brief DMA channel destination address register structure.
 */
typedef union {
    struct {
        uint32_t dst;
    };
    uint32_t v;
} sdrv_dma_ch_dst_reg;

/**
 * @brief DMA channel transaction control register structure.
 */
typedef union {
    struct {
        uint32_t src_bst_len : 4;
        uint32_t src_beat_size : 3;
        uint32_t src_port_sel : 1;
        uint32_t src_bst_type : 1;
        uint32_t src_stride_pol : 1;
        uint32_t src_protect : 2;
        uint32_t src_cache : 4;
        uint32_t dst_bst_len : 4;
        uint32_t dst_beat_size : 3;
        uint32_t dst_port_sel : 1;
        uint32_t dst_bst_type : 1;
        uint32_t dst_stride_pol : 1;
        uint32_t dst_protect : 2;
        uint32_t dst_cache : 4;
    };
    uint32_t v;
} sdrv_dma_ch_tr_ctrl_reg;

/**
 * @brief DMA channel block config register structure.
 */
typedef union {
    struct {
        uint32_t buf_size : 24;
        uint32_t lop_mode : 2;
        uint32_t swt_evt_ctl : 2;
        uint32_t src_stride_en : 1;
        uint32_t dst_stride_en : 1;
        uint32_t buf_mode : 2;
    };
    uint32_t v;
} sdrv_dma_ch_blk_cfg_reg;

/**
 * @brief DMA channel operation mode register structure.
 */
typedef union {
    struct {
        uint32_t flow_ctrl : 3;
        uint32_t rsvd : 1;
        uint32_t transfer_mode : 2;
        uint32_t mad_crc_mode : 2;
        uint32_t data_crc_mode : 2;
        uint32_t data_crc_sel : 2;
        uint32_t pn_det_mode : 3;
        uint32_t ack_assert : 1;
        uint32_t first_mad : 1;
        uint32_t last_mad : 1;
        uint32_t src_stride_sel : 1;
        uint32_t src_coarse_stride : 4;
        uint32_t dst_stride_sel : 1;
        uint32_t dst_coarse_stride : 4;
        uint32_t halt_en : 1;
        uint32_t mad_intr : 1;
        uint32_t trigger_mode : 2;
    };
    uint32_t v;
} sdrv_dma_ch_op_mod_reg;

/**
 * @brief DMA channel operation data register structure.
 */
typedef union {
    struct {
        uint32_t opd;
    };
    uint32_t v;
} sdrv_dma_ch_op_data_reg;

/**
 * @brief DMA channel mad crc register structure.
 */
typedef union {
    struct {
        uint32_t mad_crc;
    };
    uint32_t v;
} sdrv_dma_ch_mad_crc_reg;

/**
 * @brief DMA channel link address register structure.
 */
typedef union {
    struct {
        uint32_t ll_addr;
    };
    struct {
        uint32_t link_ch_num : 5;
        uint32_t rsvd : 27;
    };
    uint32_t v;
} sdrv_dma_ch_ll_addr_reg;

/**
 * @brief DMA channel fireware time register structure.
 */
typedef union {
    struct {
        uint32_t ch_timeout_value : 16;
        uint32_t ch_timeout_en : 1;
        uint32_t gtmr_sel : 2;
        uint32_t reserved : 1;
        uint32_t fw_in_perm : 2;
        uint32_t fw_out_perm : 2;
        uint32_t ch_pre_divider : 4;
        uint32_t reserved1 : 4;
    };
    uint32_t v;
} sdrv_dma_ch_fw_time_reg;

/**
 * @brief DMA channel program sequence control register structure.
 */
typedef union {
    struct {
        uint32_t ps_cnt : 8;
        uint32_t reserved : 24;
    };
    uint32_t v;
} sdrv_dma_ch_ps_ctrl_reg;

/**
 * @brief DMA channel software handshake register structure.
 */
typedef union {
    struct {
        uint32_t req : 1;
        uint32_t reserved : 31;
    };
    uint32_t v;
} sdrv_dma_ch_swhs_reg;

/**
 * @brief DMA channel turbo register structure.
 */
typedef union {
    struct {
        uint32_t ch_turbo : 1;
        uint32_t reserved : 1;
        uint32_t pre_rd_md : 1;
        uint32_t reserved1 : 29;
    };
    uint32_t v;
} sdrv_dma_ch_turbo_reg;

/**
 * @brief DMA channel low power register structure.
 */
typedef union {
    struct {
        uint32_t lpower : 1;
        uint32_t reserved : 31;
    };
    uint32_t v;
} sdrv_dma_ch_lp_reg;

/**
 * @brief DMA channel interrupt enable register structure.
 */
typedef union {
    struct {
        uint32_t hs_e2e_cor_err : 1;
        uint32_t hs_e2e_unc_err : 1;
        uint32_t ch_fifo_ecc_cor_err : 1;
        uint32_t ch_fifo_ecc_unc_err : 1;
        uint32_t ahb_rd_err : 1;
        uint32_t ahb_wr_err : 1;
        uint32_t axi_rd_err : 1;
        uint32_t axi_wr_err : 1;
        uint32_t ch_link_err : 1;
        uint32_t mad_crc_err : 1;
        uint32_t fw_rd_err : 1;
        uint32_t fw_wr_err : 1;
        uint32_t data_crc_err : 1;
        uint32_t patn_det : 1;
        uint32_t last_mad_done : 1;
        uint32_t every_mad_done : 1;
        uint32_t ch_stop : 1;
        uint32_t ch_flush : 1;
        uint32_t req_flush : 1;
        uint32_t ch_halt : 1;
        uint32_t ch_abort : 1;
        uint32_t req_timeout : 1;
        uint32_t swt_evt : 1;
        uint32_t hs_comp : 1;
        uint32_t patn_poll_mismatch : 1;
        uint32_t hs_e2e_fatal : 1;
        uint32_t axi_ot_tuid_err : 1;
        uint32_t ps_err : 1;
        uint32_t reserved : 1;
    };
    uint32_t v;
} sdrv_dma_ch_int_reg;

/**
 * @brief DMA channel crc init register structure.
 */
typedef union {
    struct {
        uint32_t init_value : 32;
    };
    uint32_t v;
} sdrv_dma_ch_crc_init_reg;

/**
 * @brief DMA channel status register structure.
 */
typedef union {
    struct {
        uint32_t ctrl_fsm : 4;
        uint32_t reserved : 4;
        uint32_t src_fsm : 4;
        uint32_t reserved1 : 4;
        uint32_t dst_fsm : 4;
        uint32_t reserved2 : 12;
    };
    uint32_t v;
} sdrv_dma_ch_status_reg;

/**
 * @brief DMA channel flow ctrl register structure.
 */
typedef union {
    struct {
        uint32_t reg : 32;
    };
    uint32_t v;
} sdrv_dma_ch_reg_fc_reg;

/**
 * @brief DMA channel data transfer register structure.
 */
typedef union {
    struct {
        uint32_t num : 32;
    };
    uint32_t v;
} sdrv_dma_ch_data_xfer_reg;

/**
 * @brief DMA channel interrupt error inject register structure.
 */
typedef union {
    struct {
        uint32_t err_inj : 1;
        uint32_t en : 1;
        uint32_t reserved : 1;
    };
    uint32_t v;
} sdrv_dma_ch_int_err_inj_reg;

/**
 * @brief DMA channel handshake error inject register structure.
 */
typedef union {
    struct {
        uint32_t hs_ej_en : 1;
        uint32_t dma_bw_cod_inj : 4;
        uint32_t dma_bw_data_inj : 4;
        uint32_t dma_fw_cod_inj : 4;
        uint32_t dma_fw_data_inj : 4;
        uint32_t reserved : 15;
    };
    uint32_t v;
} sdrv_dma_ch_hs_err_inj_reg;

/**
 * @brief DMA channel link status register structure.
 */
typedef union {
    struct {
        uint32_t req : 24;
        uint32_t reserved : 8;
    };
    uint32_t v;
} sdrv_dma_ch_link_status_reg;

/**
 * @brief DMA core config register structure.
 */
typedef union {
    struct {
        uint32_t reserved : 32;
    };
    uint32_t v;
} sdrv_dma_core_config_reg;

/**
 * @brief DMA generic timer pre dvi register structure.
 */
typedef union {
    struct {
        uint32_t lock : 1;
        uint32_t reserved : 15;
        uint32_t gtmrpd : 16;
    };
    uint32_t v;
} sdrv_dma_gtmrpd_reg;

/**
 * @brief DMA generic timer register structure.
 */
typedef union {
    struct {
        uint32_t lock : 1;
        uint32_t reserved : 12;
        uint32_t mode : 2;
        uint32_t enable : 1;
        uint32_t gtmrov : 16;
    };
    uint32_t v;
} sdrv_dma_gtmr_reg;

/**
 * @brief DMA ch arbitration scheme register structure.
 */
typedef union {
    struct {
        uint32_t reserved1 : 7;
        uint32_t fifo_arbit_scm : 1;
        uint32_t reserved2 : 6;
        uint32_t ahb_arbit_scm : 2;
        uint32_t reserved3 : 6;
        uint32_t axi_r_arbit_scm : 2;
        uint32_t reserved4 : 6;
        uint32_t axi_w_arbit_scm : 2;
    };
    uint32_t v;
} sdrv_dma_ch_arbit_scm_reg;

/**
 * @brief DMA ctrl register structure.
 */
typedef union {
    struct {
        uint32_t reserved1 : 28;
        uint32_t low_power_accept : 1;
        uint32_t reserved2 : 1;
        uint32_t low_power_req : 1;
        uint32_t soft_rst : 1;
    };
    uint32_t v;
} sdrv_dma_ctrl_reg;

/**
 * @brief DMA err inj ctrl register structure.
 */
typedef union {
    struct {
        uint32_t lock : 1;
        uint32_t reserved1 : 25;
        uint32_t reserved2 : 5;
        uint32_t err_inj_en : 1;
    };
    uint32_t v;
} sdrv_dma_err_inj_ctrl_reg;

/**
 * @brief DMA step err ctrl register structure.
 */
typedef union {
    struct {
        uint32_t step_err_inj_sel : 31;
        uint32_t step_err_inj : 1;
    };
    uint32_t v;
} sdrv_dma_step_err_ctrl_reg;

/**
 * @brief DMA apb err inj register structure.
 */
typedef union {
    struct {
        uint32_t reserved : 25;
        uint32_t wecc_inj : 7;
    };
    uint32_t v;
} sdrv_dma_apb_err_inj_reg;

/**
 * @brief DMA apb wdata inj register structure.
 */
typedef union {
    struct {
        uint32_t wdata_inj : 32;
    };
    uint32_t v;
} sdrv_dma_apb_wdata_reg;

/**
 * @brief DMA axi recc inj register structure.
 */
typedef union {
    struct {
        uint32_t reserved : 24;
        uint32_t recc_inj : 8;
    };
    uint32_t v;
} sdrv_dma_axi_recc_inj_reg;

/**
 * @brief DMA axi rdata inj register structure.
 */
typedef union {
    struct {
        uint32_t rdata_inj : 32;
    };
    uint32_t v;
} sdrv_dma_axi_rdata_inj_reg;

/**
 * @brief DMA axi rdata inj count register structure.
 */
typedef union {
    struct {
        uint32_t rdata_inj_cnt : 32;
    };
    uint32_t v;
} sdrv_dma_axi_rdata_inj_cnt_reg;

/**
 * @brief DMA ahb recc inj register structure.
 */
typedef union {
    struct {
        uint32_t reserved : 25;
        uint32_t recc_inj : 7;
    };
    uint32_t v;
} sdrv_dma_ahb_recc_inj_reg;

/**
 * @brief DMA ahb rdata inj register structure.
 */
typedef union {
    struct {
        uint32_t rdata_inj : 32;
    };
    uint32_t v;
} sdrv_dma_ahb_rdata_inj_reg;

/**
 * @brief DMA data err mask register structure.
 */
typedef union {
    struct {
        uint32_t data_err_mask : 32;
    };
    uint32_t v;
} sdrv_dma_data_err_mask_reg;

/**
 * @brief DMA code err mask register structure.
 */
typedef union {
    struct {
        uint32_t code_err_mask : 32;
    };
    uint32_t v;
} sdrv_dma_code_err_mask_reg;

/**
 * @brief DMA err addr register structure.
 */
typedef union {
    struct {
        uint32_t reserved1 : 6;
        uint32_t unc_err_addr : 10;
        uint32_t reserved2 : 6;
        uint32_t cor_err_addr : 10;
    };
    uint32_t v;
} sdrv_dma_err_addr_reg;

/**
 * @brief DMA lock step stat register structure.
 */
typedef union {
    struct {
        uint32_t reserved : 21;
        uint32_t misc_lock : 1;
        uint32_t apb_lock : 1;
        uint32_t ahb_lock : 1;
        uint32_t dma_per_lock : 1;
        uint32_t lbc_lock : 1;
        uint32_t dma_ch_lock : 1;
        uint32_t w_lock : 1;
        uint32_t r_lock : 1;
        uint32_t b_lock : 1;
        uint32_t ar_lock : 1;
        uint32_t aw_lock : 1;
    };
    uint32_t v;
} sdrv_dma_lock_step_stat_reg;

/**
 * @brief DMA ppbase addr ctrl register structure.
 */
typedef union {
    struct {
        uint32_t lock : 1;
        uint32_t reserved : 6;
        uint32_t over_write : 1;
        uint32_t base_size : 8;
        uint32_t base_addr : 16;
    };
    uint32_t v;
} sdrv_dma_base_addr_ctrl_reg;

/**
 * @brief DMA fifo err addr register structure.
 */
typedef union {
    struct {
        uint32_t reserved1 : 6;
        uint32_t fifo_unc_err : 10;
        uint32_t reserved2 : 6;
        uint32_t fifo_cor_err : 10;
    };
    uint32_t v;
} sdrv_dma_fifo_err_addr_reg;

/**
 * @brief DMA fifo rdata mask register structure.
 */
typedef union {
    struct {
        uint32_t fifo_rdata_mask : 32;
    };
    uint32_t v;
} sdrv_dma_fifo_rdata_mask_reg;

/**
 * @brief DMA fifo rdata mask count register structure.
 */
typedef union {
    struct {
        uint32_t fifo_rdata_cnt : 32;
    };
    uint32_t v;
} sdrv_dma_fifo_rdata_mask_cnt_reg;

/**
 * @brief DMA fifo rcode mask register structure.
 */
typedef union {
    struct {
        uint32_t fifo_rcode_mask : 32;
    };
    uint32_t v;
} sdrv_dma_fifo_rcode_mask_reg;

/**
 * @brief DMA fifo rcode mask count register structure.
 */
typedef union {
    struct {
        uint32_t fifo_rcode_cnt : 32;
    };
    uint32_t v;
} sdrv_dma_fifo_rcode_mask_cnt_reg;

/**
 * @brief DMA ch qack status register structure.
 */
typedef union {
    struct {
        uint32_t qack_status : 32;
    };
    uint32_t v;
} sdrv_dma_ch_qack_status_reg;

/**
 * @brief DMA debug status register structure.
 */
typedef union {
    struct {
        uint32_t dbg_status : 32;
    };
    uint32_t v;
} sdrv_dma_dbg_status_reg;

/**
 * @brief DMA int ctrl register structure.
 */
typedef union {
    struct {
        uint32_t input_sig_err : 1;
        uint32_t core1_monitor_err : 1;
        uint32_t lock_step_err : 1;
        uint32_t axi_rd_fatal : 1;
        uint32_t hrdata_fatal : 1;
        uint32_t pwdata_fatal : 1;
        uint32_t hrdata_corerr : 1;
        uint32_t rdata_corerr : 1;
        uint32_t pwdata_corerr : 1;
        uint32_t fifo_corerr : 1;
        uint32_t ch_corerr : 1;
        uint32_t fifo_uncerr : 1;
        uint32_t ch_uncerr : 1;
        uint32_t lbc_corerr : 1;
        uint32_t lbc_uncerr : 1;
        uint32_t wready_uncerr : 1;
        uint32_t bvalid_uncerr : 1;
        uint32_t bid_uncerr : 1;
        uint32_t bctl_uncerr : 1;
        uint32_t awready_uncerr : 1;
        uint32_t aready_uncerr : 1;
        uint32_t rctl_uncerr : 1;
        uint32_t rdata_uncerr : 1;
        uint32_t reobi_uncerr : 1;
        uint32_t rid_uncerr : 1;
        uint32_t rvalid_uncerr : 1;
        uint32_t hresp_uncerr : 1;
        uint32_t hrdata_uncerr : 1;
        uint32_t paddr_uncerr : 1;
        uint32_t pctl1_uncerr : 1;
        uint32_t pctl0_uncerr : 1;
        uint32_t pwdata_uncerr : 1;
    };
    uint32_t v;
} sdrv_dma_int_ctrl_reg;

/**
 * @brief DMA cont int register structure.
 */
typedef union {
    struct {
        uint32_t reserved : 27;
        uint32_t butid_err : 1;
        uint32_t rutid_err : 1;
        uint32_t ahb_err : 1;
        uint32_t axi_wr_err : 1;
        uint32_t axi_rd_err : 1;
    };
    uint32_t v;
} sdrv_dma_cont_int_reg;

/**
 * @brief DMA interrupt config register structure.
 */
typedef volatile struct {
    sdrv_dma_int_ctrl_reg int_en;          /* 0x0 */
    sdrv_dma_int_ctrl_reg int_clr;         /* 0x4 */
    sdrv_dma_int_ctrl_reg int_status;      /* 0x8 */
    uint32_t reserved_c[1];                /* 0xc */
    sdrv_dma_cont_int_reg cont_int_en;     /* 0x10*/
    sdrv_dma_cont_int_reg cont_int_clr;    /* 0x14*/
    sdrv_dma_cont_int_reg cont_int_status; /* 0x18*/
} sdrv_dma_interrupt_config;

/**
 * @brief DMA channel controller register structure.
 */
typedef volatile struct {
    sdrv_dma_ch_cfg_reg ch_cfg;                    /* 0x0 */
    sdrv_dma_ch_port_cfg_reg port_cfg;             /* 0x4 */
    sdrv_dma_ch_stride_cfg_reg stride_cfg;         /* 0x8 */
    sdrv_dma_ch_fw_up_boundary_reg up_bdry;        /* 0xC */
    sdrv_dma_ch_fw_dw_boundary_reg dw_bdry;        /* 0x10 */
    sdrv_dma_ch_patn_match_cfg_reg patn_match_cfg; /* 0x14 */
    sdrv_dma_ch_patn_mask_cfg_reg patn_mask_cfg;   /* 0x18 */
    sdrv_dma_ch_op2_data_reg op2_data;             /* 0x1C */
    sdrv_dma_ch_src_reg src;                       /* 0x20 */
    sdrv_dma_ch_dst_reg dst;                       /* 0x24 */
    sdrv_dma_ch_tr_ctrl_reg xfer_ctrl;             /* 0x28 */
    sdrv_dma_ch_blk_cfg_reg blk_cfg;               /* 0x2C */
    sdrv_dma_ch_op_mod_reg op_mod;                 /* 0x30 */
    sdrv_dma_ch_op_data_reg op_data;               /* 0x34 */
    sdrv_dma_ch_mad_crc_reg mad_crc;               /* 0x38 */
    sdrv_dma_ch_ll_addr_reg link_addr;             /* 0x3C */
    sdrv_dma_ch_src_reg shadow_src;                /* 0x40 */
    sdrv_dma_ch_dst_reg shadow_dst;                /* 0x44 */
    sdrv_dma_ch_tr_ctrl_reg shadow_xfer_ctrl;      /* 0x48 */
    sdrv_dma_ch_blk_cfg_reg shadow_blk_cfg;        /* 0x4C */
    sdrv_dma_ch_op_mod_reg shadow_op_mod;          /* 0x50 */
    sdrv_dma_ch_op_data_reg shadow_op_data;        /* 0x54 */
    sdrv_dma_ch_mad_crc_reg shadow_mad_crc;        /* 0x58 */
    sdrv_dma_ch_ll_addr_reg shadow_link_addr;      /* 0x5C */
    sdrv_dma_ch_fw_time_reg fw_regime;             /* 0x60 */
    sdrv_dma_ch_ps_ctrl_reg ps_ctrl;               /* 0x64 */
    sdrv_dma_ch_swhs_reg swhs;                     /* 0x68 */
    sdrv_dma_ch_turbo_reg ch_regurbo;              /* 0x6C */
    sdrv_dma_ch_lp_reg lpower;                     /* 0x70 */
    sdrv_dma_ch_int_reg int_en;                    /* 0x74 */
    sdrv_dma_ch_int_reg int_clr;                   /* 0x78 */
    sdrv_dma_ch_int_reg int_status;                /* 0x7C */
    sdrv_dma_ch_crc_init_reg data_rd_crc_int;      /* 0x80 */
    sdrv_dma_ch_crc_init_reg data_wr_crc_int;      /* 0x84 */
    sdrv_dma_ch_crc_init_reg mad_crc_int;          /* 0x88 */
    sdrv_dma_ch_status_reg ch_status;              /* 0x8C */
    sdrv_dma_ch_reg_fc_reg reg1_fc;                /* 0x90 */
    sdrv_dma_ch_reg_fc_reg reg2_fc;                /* 0x94 */
    sdrv_dma_ch_data_xfer_reg xferred_num;         /* 0x98 */
    sdrv_dma_ch_int_err_inj_reg int_err_inj;       /* 0x9C */
    uint32_t reserved[24];                         /* 0xA0~0xFC */
    sdrv_dma_ch_hs_err_inj_reg hs_err_inj;         /* 0x100 */
    sdrv_dma_ch_link_status_reg link_req_out;      /* 0x104 */
    sdrv_dma_ch_link_status_reg link_req_in;       /* 0x108 */
} sdrv_dma_channel_ctrl_t;

/**
 * @brief DMA controller register structure.
 */
typedef volatile struct sdrv_dma_ctrl {
    sdrv_dma_core_config_reg core_config;             /* 0x0 */
    sdrv_dma_gtmrpd_reg generic_timer_div;            /* 0x4 */
    sdrv_dma_gtmr_reg generic_timer0;                 /* 0x8 */
    sdrv_dma_gtmr_reg generic_timer1;                 /* 0xC */
    sdrv_dma_gtmr_reg generic_timer2;                 /* 0x10 */
    sdrv_dma_gtmr_reg generic_timer4;                 /* 0x14 */
    sdrv_dma_ch_arbit_scm_reg ch_arbit_scm;           /* 0x18 */
    uint32_t reserved_1c[32];                         /* 0x1C~0x98 */
    sdrv_dma_ctrl_reg dma_ctrl;                       /* 0x9C */
    sdrv_dma_err_inj_ctrl_reg err_inj;                /* 0xA0 */
    sdrv_dma_step_err_ctrl_reg step_err;              /* 0xA4 */
    sdrv_dma_apb_err_inj_reg apb_err_inj;             /* 0xA8 */
    sdrv_dma_apb_wdata_reg apb_wdata_inj;             /* 0xAC */
    sdrv_dma_axi_recc_inj_reg axi_recc_inj;           /* 0xB0 */
    sdrv_dma_axi_rdata_inj_reg axi_rdata_inj;         /* 0xB4 */
    sdrv_dma_axi_rdata_inj_cnt_reg axi_rdata_inj_cnt; /* 0xB8 */
    sdrv_dma_ahb_recc_inj_reg ahb_recc_inj;           /* 0xBC */
    sdrv_dma_ahb_rdata_inj_reg ahb_rdata_inj;         /* 0xC0 */
    sdrv_dma_data_err_mask_reg data_err_mask;         /* 0xC4 */
    sdrv_dma_code_err_mask_reg code_err_mask;         /* 0xC8 */
    sdrv_dma_err_addr_reg err_addr_mask;              /* 0xCC */
    sdrv_dma_lock_step_stat_reg lock_step_stat;       /* 0xD0 */
    uint32_t reserved_d4[7];                          /* 0xD4~0xEC */
    sdrv_dma_base_addr_ctrl_reg ppbase_addr_ctrl;     /* 0xF0 */
    sdrv_dma_base_addr_ctrl_reg lbcbase_addr_ctrl;    /* 0xF4 */
    sdrv_dma_fifo_err_addr_reg fifo_err_addr;         /* 0xF8 */
    uint32_t reserved_fc[1];                          /* 0xFC */
    sdrv_dma_fifo_rdata_mask_reg fifo_rdata_mask;     /* 0x100 */
    sdrv_dma_fifo_rdata_mask_cnt_reg fifo_rdata_cnt;  /* 0x104 */
    sdrv_dma_fifo_rcode_mask_reg fifo_rcode_mask;     /* 0x108 */
    sdrv_dma_fifo_rcode_mask_cnt_reg fifo_rcode_cnt;  /* 0x10C */
    sdrv_dma_ch_qack_status_reg qack_status;          /* 0x110 */
    uint32_t reserved_114[4];                         /* 0x114 */
    sdrv_dma_dbg_status_reg dbg_status;               /* 0x120 */
    uint32_t reserved_124[431];                       /* 0x124 */
    sdrv_dma_interrupt_config int_cfg;                /* 0x7E0 */
} sdrv_dma_ctrl_t;

#endif