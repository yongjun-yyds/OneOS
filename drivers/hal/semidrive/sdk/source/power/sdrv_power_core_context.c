/**
 * @file sdrv_power_core_context.c
 * @brief Sdrv power driver source.
 *
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */

#include <armv7-r/fpu.h>
#include <armv7-r/cache.h>
#include <armv7-r/register.h>
#include <armv7-r/irq.h>
#include <armv7-r/pmu.h>
#include <compiler.h>
#include <reset_ip.h>
#include <sdrv_rstgen.h>
#include "sdrv_power_core_context.h"

extern int arm_saveusercontext(uint32_t *saveregs);
extern void arm_fullcontextrestore(uint32_t *restoreregs);


static uint32_t cpu_regs[XCPTCONTEXT_REGS + MPUCONTEXT_REGS];


__WEAK void sdrv_power_tcm_save(void)
{
}

__WEAK void sdrv_power_tcm_restore(void)
{
}

uint32_t arm_context_read_flag(void)
{
    return sdrv_rstgen_read_general(&reset_general_reg_sf_boot);
}

void arm_context_write_flag(uint32_t val)
{
    sdrv_rstgen_write_general(&reset_general_reg_sf_boot, val);
}

void arm_context_save(void)
{
    sdrv_power_tcm_save();

    arm_saveusercontext(cpu_regs);
}

void arm_context_restore(void)
{
    if (sdrv_rstgen_read_general(&reset_general_reg_sf_boot) == CONTEXT_RESTORE_FLAG) {
        sdrv_power_tcm_restore();

        arch_enable_cache(ICACHE);
        arch_enable_cache(DCACHE);

#if CONFIG_ARM_WITH_PMU
        /* udelay use cycle counter, must re-start */
        pmu_enable();
        pmu_start_cycle_cntr(false);
#endif

#if CONFIG_ARCH_WITH_FPU
        arm_fpu_enable();
#endif

#if CONFIG_VIC_IRQ_INTERRUPT_MODE
        arch_vectored_irq_enable(1U);
#else
        arch_vectored_irq_enable(0U);
#endif

        arm_fullcontextrestore(cpu_regs);
    }
}
