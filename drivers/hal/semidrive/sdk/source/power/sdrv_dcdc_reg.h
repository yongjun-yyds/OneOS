/**
 * @file sdrv_dcdc_reg.h
 * @brief Semidrive DCDC register.
 *
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef SDRV_POWER_DCDC_REG_H_
#define SDRV_POWER_DCDC_REG_H_

#include <types.h>

#define DCDC_REG_8_LSB  (0u)
#define DCDC_REG_8_MASK (0xFFu)
#define DCDC_REG_7_LSB  (8u)
#define DCDC_REG_7_MASK (0xFFu)
#define DCDC_REG_0_LSB  (16u)
#define DCDC_REG_0_MASK (0xFFu)

#define DCDC_REG_3_LSB  (0u)
#define DCDC_REG_3_MASK (0xFFu)
#define DCDC_REG_2_LSB  (8u)
#define DCDC_REG_2_MASK (0xFFu)
#define DCDC_REG_1_LSB  (16u)
#define DCDC_REG_1_MASK (0xFFu)

#define DCDC_REG_6_LSB  (0u)
#define DCDC_REG_6_MASK (0xFFu)
#define DCDC_REG_5_LSB  (8u)
#define DCDC_REG_5_MASK (0xFFu)
#define DCDC_REG_4_LSB  (16u)
#define DCDC_REG_4_MASK (0xFFu)

#define ANA_COM_CFG  (0xCu)

#define HP_MOD_CFG0  (0x10u)
#define HP_MOD_CFG1  (0x14u)

#define LP_MOD_CFG0  (0x18u)
#define LP_MOD_CFG1  (0x1Cu)

#endif /* SDRV_POWER_DCDC_REG_H_ */
