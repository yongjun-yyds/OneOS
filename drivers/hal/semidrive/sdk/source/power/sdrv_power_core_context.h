/**
 * @file sdrv_power_core_context.h
 * @brief Taishan power manager head source.
 *
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef SDRV_TAISHAN_POWER_CORE_CONTEXT_H_
#define SDRV_TAISHAN_POWER_CORE_CONTEXT_H_

/* saf rstgen general 7 */
#define CONTEXT_RESTORE_FLAG_REG  0xf069203c
#define CONTEXT_RESTORE_FLAG      0x1a2b3c4d

#define MPUCONTEXT_REGS    (3 * 16 + 1)

uint32_t arm_context_read_flag(void);

void arm_context_write_flag(uint32_t val);

void arm_context_save(void);

void arm_context_restore(void);

#endif /* SDRV_TAISHAN_POWER_CORE_CONTEXT_H_ */
