/**
 * @file sdrv_fs32k_hw_access.c
 * @brief taishan fs32k hw access source file.
 *
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */

#include <bits.h>

#include "sdrv_fs32k_hw_access.h"
#include "sdrv_ckgen_common.h"

void sdrv_fs32k_fs_sel(uint32_t base, uint8_t src)
{
    RMWREG32(base + FS32K_GLB_CTL, FS32K_GLB_CTL_FS_SRC_SEL, 1u, src);
}

uint32_t sdrv_fs32k_fs_sel_get(uint32_t base)
{
    if (BIT(readl(base + FS32K_GLB_CTL), FS32K_GLB_CTL_XTAL_ACTIVE)) {
        return FS32K_FS_SEL_XTAL;
    }
    else {
        return FS32K_FS_SEL_RC;
    }
}

int sdrv_fs32k_wait_active(uint32_t base, uint8_t src, uint32_t count)
{
    uint32_t reg = base + FS32K_GLB_CTL;
    uint32_t offset;

    if (src == FS32K_FS_SEL_RC) {
        offset = FS32K_GLB_CTL_RC_ACTIVE;
    }
    else {
        offset = FS32K_GLB_CTL_XTAL_ACTIVE;
    }

    return sdrv_ckgen_wait(reg, offset, 1u, count);
}

int sdrv_fs32k_wait_ready(uint32_t base, uint8_t src, uint32_t count)
{
    uint32_t reg = base + FS32K_GLB_CTL;
    uint32_t offset;
    uint32_t val;

    if (src == FS32K_FS_SEL_RC) {
        offset = FS32K_GLB_CTL_RC_RDY;
    }
    else {
        offset = FS32K_GLB_CTL_XTAL_RDY;
    }

    val = readl(reg);
    while (!(val & (0x1u << offset))) {
        val = readl(reg);
    }

    return true;
}

void sdrv_fs32k_xtal_fs_enable(uint32_t base, bool enable)
{
    RMWREG32(base + FS32K_GLB_CTL, FS32K_GLB_CTL_FS_XTAL_EN, 1u, enable);
}

void sdrv_fs32k_rc_fs_enable(uint32_t base, bool enable)
{
    RMWREG32(base + FS32K_GLB_CTL, FS32K_GLB_CTL_FS_OSC_EN, 1u, enable);
}

void sdrv_fs32k_xtal_enable(uint32_t base, bool enable)
{
    RMWREG32(base + FS32K_XTAL_CTL, FS32K_XTAL_CTL_XTAL_ENABLE, 1u, enable);
}

void sdrv_fs32k_xtal_test_enable(uint32_t base, bool enable)
{
    RMWREG32(base + FS32K_XTAL_CTL, FS32K_XTAL_CTL_XTAL_TEST_ENABLE, 1u, enable);
}

void sdrv_fs32k_set_rc_freq_tune_b(uint32_t base, uint32_t tune)
{
    RMWREG32(base + FS32K_RC_CTL, FS32K_RC_CTL_FREQ_TUNE_B_LSB, 4,
        tune & FS32K_RC_CTL_FREQ_TUNE_B_MASK);
}

uint32_t sdrv_fs32k_get_rc_freq_tune_b(uint32_t base)
{
    return (readl(base + FS32K_RC_CTL) >> FS32K_RC_CTL_FREQ_TUNE_B_LSB)
        & FS32K_RC_CTL_FREQ_TUNE_B_MASK;
}

void sdrv_fs32k_lpvd_power_ctl(uint32_t base, bool power_on)
{
    RMWREG32(base + FS32K_LPVD_CTRL, FS32K_LPVD_CTRL_PD, 1, !power_on);
}
