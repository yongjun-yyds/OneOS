/**
 * @file sdrv_ckgen_hw_access.h
 * @brief taishan ckgen hw access header file.
 *
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef SDRV_CKGEN_HW_ACCESS_H_
#define SDRV_CKGEN_HW_ACCESS_H_

#include <sdrv_ckgen.h>
#include <types.h>

/* Domain access permission */
#define CKGEN_DOM_PER                       (0x0u)
#define CKGEN_DOM_PER_SEC_PER_LSB           (0u)
#define CKGEN_DOM_PER_NSEC_PER_LSB          (2u)
#define CKGEN_DOM_PER_PRI_PER_LSB           (4u)
#define CKGEN_DOM_PER_USE_PER_LSB           (6u)
#define CKGEN_DOM_PER_MASK                  (0x3u)

#define CKGEN_DOM_PER_LOCK                  (0x8u)

/* RS register */
#define CKGEN_RS_EN                         (0u)
#define CKGEN_RS_RS_LSB                     (1u)
#define CKGEN_RS_RS_MASK                    (0xFu)
#define CKGEN_RS_LOCK                       (31u)

/* Global Control */
#define CKGEN_GLB_CTL                       (0x204u)
#define CKGEN_GLB_CTL_24M_DIVA_DIS          (0u)
#define CKGEN_GLB_CTL_24M_DIVB_DIS          (1u)
#define CKGEN_GLB_CTL_24M_DIVC_DIS          (2u)
#define CKGEN_GLB_CTL_24M_DIVA_NUM_LSB      (3u)
#define CKGEN_GLB_CTL_24M_DIVA_NUM_MASK     (0x3FFu)
#define CKGEN_GLB_CTL_24M_DIVB_NUM_LSB      (18u)
#define CKGEN_GLB_CTL_24M_DIVB_NUM_MASK     (0x3FFu)
#define CKGEN_GLB_CTL_24M_DIVC_NUM_LSB      (28u)
#define CKGEN_GLB_CTL_24M_DIVC_NUM_MASK     (0xFu)

/* SUP DOM */
#define CKGEN_SUP_DOM                       (0x300u)
#define CKGEN_SUP_DOM_DID_LSB               (0u)
#define CKGEN_SUP_DOM_DID_MASK              (0xFu)
#define CKGEN_SUP_DOM_SEC_EN                (4u)
#define CKGEN_SUP_DOM_PPROT_LSB             (5u)
#define CKGEN_SUP_DOM_PPROT_MASK            (0x3u)
#define CKGEN_SUP_DOM_LOCK                  (31u)

/* CKGEN RES */
#define CKGEN_RES_RS                        (0x400u)
#define CKGEN_RES_RS_SF_RATIO_SEL           (0u)
#define CKGEN_RES_RS_SP_RATIO_SEL           (1u)
#define CKGEN_RES                           (0x404u)

/* IP slice register */
#define CKGEN_IP_RS                         (0x1000u)
#define CKGEN_IP_CTL                        (0x1004u)
#define CKGEN_IP_CTL_SRC_SEL_LSB            (0u)
#define CKGEN_IP_CTL_SRC_SEL_CKIN0_3        (0u)
#define CKGEN_IP_CTL_SRC_SEL_CKIN4          (2u)
#define CKGEN_IP_CTL_SRC_SEL_MASK           (0x7u)
#define CKGEN_IP_CTL_PRE_EN                 (4u)
#define CKGEN_IP_CTL_MAIN_EN                (5u)
#define CKGEN_IP_CTL_DBG_EN                 (6u)
#define CKGEN_IP_CTL_HW_DIS_EN              (7u)
#define CKGEN_IP_CTL_DIV_NUM_LSB            (8u)
#define CKGEN_IP_CTL_DIV_NUM_MASK           (0xFFu)
#define CKGEN_IP_CTL_D0_ACTIVE              (27u)
#define CKGEN_IP_CTL_DIV_CHG_BUSY           (28u)
#define CKGEN_IP_CTL_PRE_EN_STATUS          (29u)
#define CKGEN_IP_CTL_MAIN_EN_STATUS         (30u)
#define CKGEN_IP_CTL_HW_CG_EN_STATUS        (31u)

#define CKGEN_IP_MON_CTL                    (0x1008u)
#define CKGEN_IP_MON_CTL_EN                 (0u)
#define CKGEN_IP_MON_CTL_ACTIVE_DISABLE     (1u)
#define CKGEN_IP_MON_CTL_SRC_SEL            (2u)
#define CKGEN_IP_MON_CTL_SLP_EXP            (3u)
#define CKGEN_IP_MON_CTL_HIB_EXP            (4u)
#define CKGEN_IP_MON_CTL_FREQ_UPD           (8u)
#define CKGEN_IP_MON_CTL_EN_STA             (9u)
#define CKGEN_IP_MON_CTL_FREQ_LSB           (16u)
#define CKGEN_IP_MON_CTL_FREQ_MASK          (0xFFFFu)

#define CKGEN_IP_MON_THRD                   (0x100Cu)
#define CKGEN_IP_MON_THRD_LOW_LSB           (0u)
#define CKGEN_IP_MON_THRD_LOW_MASK          (0xFFFFu)
#define CKGEN_IP_MON_THRD_HIGH_LSB          (16u)
#define CKGEN_IP_MON_THRD_HIGH_MASK         (0xFFFFu)

/* BUS slice register */
#define CKGEN_BUS_RS                        (0x2000u)
#define CKGEN_BUS_CTL                       (0x2004u)
#define CKGEN_BUS_CTL_SRC_SEL_LSB           (0u)
#define CKGEN_BUS_CTL_SRC_SEL_CKIN0_3       (0u)
#define CKGEN_BUS_CTL_SRC_SEL_CKIN4         (2u)
#define CKGEN_BUS_CTL_SRC_SEL_CKOUT         (3u)
#define CKGEN_BUS_CTL_SRC_SEL_MASK          (0xFu)
#define CKGEN_BUS_CTL_PRE_EN                (4u)
#define CKGEN_BUS_CTL_MAIN_EN               (5u)
#define CKGEN_BUS_CTL_DBG_EN                (6u)
#define CKGEN_BUS_CTL_HW_DIS_EN             (7u)
#define CKGEN_BUS_CTL_DIV_NUM_LSB           (8u)
#define CKGEN_BUS_CTL_DIV_NUM_MASK          (0x1Fu)
#define CKGEN_BUS_CTL_PRE_MUX_D0_ACTIVE     (26u)
#define CKGEN_BUS_CTL_POST_MUX_D0_ACTIVE    (27u)
#define CKGEN_BUS_CTL_DIV_CHG_BUSY          (28u)
#define CKGEN_BUS_CTL_PRE_EN_STATUS         (29u)
#define CKGEN_BUS_CTL_MAIN_EN_STATUS        (30u)
#define CKGEN_BUS_CTL_HW_CG_EN_STATUS       (31u)

#define CKGEN_BUS_SYNC_CTL                  (0x2008u)
#define CKGEN_BUS_SYNC_CTL_DIV_M_LSB        (0u)
#define CKGEN_BUS_SYNC_CTL_DIV_N_LSB        (4u)
#define CKGEN_BUS_SYNC_CTL_DIV_P_LSB        (8u)
#define CKGEN_BUS_SYNC_CTL_DIV_Q_LSB        (12u)
#define CKGEN_BUS_SYNC_CTL_DIV_MASK         (0xFu)
#define CKGEN_BUS_SYNC_CTL_DIV_CHG_BUSY_LSB (28u)

#define CKGEN_BUS_MON_CTL0                  (0x200Cu)
#define CKGEN_BUS_MON_CTL0_EN               (0u)
#define CKGEN_BUS_MON_CTL0_ACTIVE_DISABLE   (1u)
#define CKGEN_BUS_MON_CTL0_SRC_SEL          (2u)
#define CKGEN_BUS_MON_CTL0_SLP_EXP          (3u)
#define CKGEN_BUS_MON_CTL0_HIB_EXP          (4u)
#define CKGEN_BUS_MON_CTL0_EN_STA           (5u)
#define CKGEN_BUS_MON_CTL0_FREQ_UPD(m)      (8u + (m))

#define CKGEN_BUS_MON_CTL1                  (0x2010u)
#define CKGEN_BUS_MON_CTL1_FREQ0_LSB        (0u)
#define CKGEN_BUS_MON_CTL1_FREQ1_LSB        (16u)

#define CKGEN_BUS_MON_CTL2                  (0x2014u)
#define CKGEN_BUS_MON_CTL2_FREQ2_LSB        (0u)
#define CKGEN_BUS_MON_CTL2_FREQ3_LSB        (16u)
#define CKGEN_BUS_MON_CTL_FREQ_MASK         (0xFFFFu)

#define CKGEN_BUS_MON_THRD0                 (0x2018u)
#define CKGEN_BUS_MON_THRD1                 (0x201Cu)
#define CKGEN_BUS_MON_THRD2                 (0x2020u)
#define CKGEN_BUS_MON_THRD3                 (0x2024u)
#define CKGEN_BUS_MON_THRD_LOW_LSB          (0u)
#define CKGEN_BUS_MON_THRD_HIGH_LSB         (16u)
#define CKGEN_BUS_MON_THRD_MASK             (0xFFFFu)

/* CORE slice register */
#define CKGEN_CORE_RS                       (0x3000u)
#define CKGEN_CORE_CTL                      (0x3004u)
#define CKGEN_CORE_CTL_SRC_SEL_LSB          (0u)
#define CKGEN_CORE_CTL_SRC_SEL_CKIN0_3      (0u)
#define CKGEN_CORE_CTL_SRC_SEL_CKIN4        (2u)
#define CKGEN_CORE_CTL_SRC_SEL_MASK         (0x7u)
#define CKGEN_CORE_CTL_PRE_EN               (4u)
#define CKGEN_CORE_CTL_MAIN_EN              (5u)
#define CKGEN_CORE_CTL_DBG_EN               (6u)
#define CKGEN_CORE_CTL_HW_DIS_EN            (7u)
#define CKGEN_CORE_CTL_DIV_NUM_LSB          (8u)
#define CKGEN_CORE_CTL_DIV_NUM_MASK         (0x1Fu)
#define CKGEN_CORE_CTL_D0_ACTIVE            (27u)
#define CKGEN_CORE_CTL_DIV_CHG_BUSY         (28u)
#define CKGEN_CORE_CTL_PRE_EN_STATUS        (29u)
#define CKGEN_CORE_CTL_MAIN_EN_STATUS       (30u)
#define CKGEN_CORE_CTL_HW_CG_EN_STATUS      (31u)

#define CKGEN_CORE_MON_CTL                  (0x3008u)
#define CKGEN_CORE_MON_CTL_EN               (0u)
#define CKGEN_CORE_MON_CTL_ACTIVE_DISABLE   (1u)
#define CKGEN_CORE_MON_CTL_SRC_SEL          (2u)
#define CKGEN_CORE_MON_CTL_SLP_EXP          (3u)
#define CKGEN_CORE_MON_CTL_HIB_EXP          (4u)
#define CKGEN_CORE_MON_CTL_FREQ_UPD         (8u)
#define CKGEN_CORE_MON_CTL_EN_STA           (9u)
#define CKGEN_CORE_MON_CTL_FREQ_LSB         (16u)
#define CKGEN_CORE_MON_CTL_FREQ_MASK        (0xFFFFu)

#define CKGEN_CORE_MON_THRD                 (0x300cu)
#define CKGEN_CORE_MON_THRD_LOW_LSB         (0u)
#define CKGEN_CORE_MON_THRD_LOW_MASK        (0xFFFFu)
#define CKGEN_CORE_MON_THRD_HIGH_LSB        (16u)
#define CKGEN_CORE_MON_THRD_HIGH_MASK       (0xFFFFu)

/* XCG register */
#define CKGEN_PCG_RS                        (0x4000u)
#define CKGEN_PCG_CTL                       (0x4004u)
#define CKGEN_BCG_RS                        (0x5000u)
#define CKGEN_BCG_CTL                       (0x5004u)
#define CKGEN_CCG_RS                        (0x6000u)
#define CKGEN_CCG_CTL                       (0x6004u)
#define CKGEN_XCG_RS(m)                     (CKGEN_PCG_RS + (m) * 0x1000u)
#define CKGEN_XCG_CTL(m)                    (CKGEN_PCG_CTL + (m) * 0x1000u)

#define CKGEN_CG_CTL_RUN_MODE               (0u)
#define CKGEN_CG_CTL_HIB_MODE               (1u)
#define CKGEN_CG_CTL_SLP_MODE               (2u)
#define CKGEN_CG_CTL_ACTIVE_MON_EN          (3u)
#define CKGEN_CG_CTL_ACTIVE_MON_STA         (4u)
#define CKGEN_CG_CTL_CG_GATED               (5u)
#define CKGEN_CG_CTL_DBG_EN                 (6u)
#define CKGEN_CG_CTL_LP_MASK                (8u)

#define CKGEN_PLL_RS                        (0x7000u)
#define CKGEN_PLL_CTL                       (0x7004u)
#define CKGEN_PLL_CTL_RUN_MODE              (0u)
#define CKGEN_PLL_CTL_HIB_MODE              (1u)
#define CKGEN_PLL_CTL_SLP_MODE              (2u)
#define CKGEN_PLL_CTL_IGNORE                (3u)
#define CKGEN_PLL_PD_RUN_MODE               (4u)
#define CKGEN_PLL_PD_HIB_MODE               (5u)
#define CKGEN_PLL_PD_SLP_MODE               (6u)

#define CKGEN_PLL_MON_CTL                   (0x7008u)
#define CKGEN_PLL_MON_CTL_EN                (0u)
#define CKGEN_PLL_MON_CTL_TOUT_VAL_LSB      (8u)
#define CKGEN_PLL_MON_CTL_TOUT_VAL_MASK     (0xFFFFu)
#define CKGEN_PLL_MON_CTL_CLK_READY         (31u)

#define CKGEN_XTAL_RS                       (0x7500u)
#define CKGEN_XTAL_CTL                      (0x7504u)
#define CKGEN_XTAL_CTL_RUN_MODE             (0u)
#define CKGEN_XTAL_CTL_HIB_MODE             (1u)
#define CKGEN_XTAL_CTL_SLP_MODE             (2u)
#define CKGEN_XTAL_CTL_IGNORE               (3u)

#define CKGEN_XTAL_MON_CTL                  (0x7508u)
#define CKGEN_XTAL_MON_CTL_EN               (0u)
#define CKGEN_XTAL_MON_CTL_EN_STA           (1u)
#define CKGEN_XTAL_MON_CTL_TOUT_VAL_LSB     (8u)
#define CKGEN_XTAL_MON_CTL_TOUT_VAL_MASK    (0xFFFFu)
#define CKGEN_XTAL_MON_CTL_RC24M_READY      (28u)
#define CKGEN_XTAL_MON_CTL_XTAL_READY       (29u)
#define CKGEN_XTAL_MON_CTL_RC24M_ACTIVE     (30u)
#define CKGEN_XTAL_MON_CTL_XTAL_ACTIVE      (31u)

#define CKGEN_DBG_MON_RS                    (0x8000u)

#define CKGEN_DBG_MON_CLK_SRC               (0x8004u)
#define CKGEN_DBG_MON_CLK_SRC_IP_LSB        (0u)
#define CKGEN_DBG_MON_CLK_SRC_BUS_LSB       (8u)
#define CKGEN_DBG_MON_CLK_SRC_CORE_LSB      (16u)
#define CKGEN_DBG_MON_CLK_SRC_EXT_LSB       (31u)
#define CKGEN_DBG_MON_CLK_SRC_LSB(src)      ((src) * 8u)
#define CKGEN_DBG_MON_CLK_SRC_MASK          (0xFFu)

#define CKGEN_DBG_MON_CTL                   (0x8008u)
#define CKGEN_DBG_MON_CTL_DBG_GATING_EN     (0u)
#define CKGEN_DBG_MON_CTL_MON_GATING_EN     (1u)
#define CKGEN_DBG_MON_CTL_CQM_GATING_EN     (2u)
#define CKGEN_DBG_MON_CTL_CLK_SEL_LSB       (3u)
#define CKGEN_DBG_MON_CTL_CLK_SEL_MASK      (0x3u)

#define CKGEN_DBG_CTL                       (0x800Cu)
#define CKGEN_DBG_CTL_DIV_LSB               (0u)
#define CKGEN_DBG_CTL_DIV_MASK              (0xFu)

#define CKGEN_MON_CTL                       (0x8010u)
#define CKGEN_MON_CTL_EN                    (0u)
#define CKGEN_MON_CTL_SRC_SEL               (1u)
#define CKGEN_MON_CTL_ACT_LOSS_DIS          (2u)
#define CKGEN_MON_CTL_FREQ_UPD              (8u)
#define CKGEN_MON_CTL_EN_STA                (9u)
#define CKGEN_MON_CTL_FREQ_LSB              (16u)
#define CKGEN_MON_CTL_FREQ_MASK             (0xFFFFu)

#define CKGEN_MON_CHK_THRD                  (0x8014u)
#define CKGEN_MON_CHK_THRD_LOW_LSB          (0u)
#define CKGEN_MON_CHK_THRD_HIGH_LSB         (16u)
#define CKGEN_MON_CHK_THRD_MASK             (0xFFFFu)

#define CKGEN_LOW_SPD_CHK_CTL               (0x8020u)
#define CKGEN_LOW_SPD_CHK_CTL_LOSS_DIS      (0u)
#define CKGEN_LOW_SPD_CHK_CTL_MON_EN        (1u)
#define CKGEN_LOW_SPD_CHK_CTL_24M_SEL       (2u)
#define CKGEN_LOW_SPD_CHK_CTL_32K_SEL       (8u)
#define CKGEN_LOW_SPD_CHK_CTL_MON_UPD       (14u)
#define CKGEN_LOW_SPD_CHK_CTL_MON_EN_STA    (15u)
#define CKGEN_LOW_SPD_CHK_CTL_FREQ_MON_LSB  (16u)
#define CKGEN_LOW_SPD_CHK_CTL_FREQ_MON_MASK (0xFFFFu)

#define CKGEN_LOW_SPD_CHK_THRD              (0x8024u)
#define CKGEN_LOW_SPD_CHK_THRD_LOW_LSB      (0u)
#define CKGEN_LOW_SPD_CHK_THRD_HIGH_LSB     (16u)
#define CKGEN_LOW_SPD_CHK_THRD_MASK         (0xFFFFu)

#define CKGEN_CQM_CTL                       (0x8040u)
#define CKGEN_CQM_CTL_MON_EN                (0u)
#define CKGEN_CQM_CTL_SRC_SEL_LSB           (1u)
#define CKGEN_CQM_CTL_SRC_SEL_MASK          (0x3u)
#define CKGEN_CQM_CTL_JITTER_RATE_LSB       (3u)
#define CKGEN_CQM_CTL_JITTER_RATE_MASK      (0x3Fu)
#define CKGEN_CQM_CTL_DUTY_RATE_LSB         (9u)
#define CKGEN_CQM_CTL_DUTY_RATE_MASK        (0x3Fu)

#define CKGEN_IP_CLK_COR_EN                 (0xA300u)
#define CKGEN_IP_CLK_UNC_EN                 (0xA304u)
#define CKGEN_IP_CLK_INT_STA                (0xA308u)

#define CKGEN_BUS_CLK_COR_EN                (0xA400u)
#define CKGEN_BUS_CLK_UNC_EN                (0xA404u)
#define CKGEN_BUS_CLK_INT_STA               (0xA408u)

#define CKGEN_CORE_CLK_COR_EN               (0xA500u)
#define CKGEN_CORE_CLK_UNC_EN               (0xA504u)
#define CKGEN_CORE_CLK_INT_STA              (0xA508u)

#define CKGEN_PCG_COR_EN                    (0xA600u)
#define CKGEN_XCG_COR_EN(m)                 (CKGEN_PCG_COR_EN + (m) * 0x100u)
#define CKGEN_PCG_UNC_EN                    (0xA604u)
#define CKGEN_XCG_UNC_EN(m)                 (CKGEN_PCG_UNC_EN + (m) * 0x100u)
#define CKGEN_PCG_INT_STA                   (0xA608u)
#define CKGEN_XCG_INT_STA(m)                (CKGEN_PCG_INT_STA + (m) * 0x100u)

#define CKGEN_PLL_UNC_EN                    (0xA904u)
#define CKGEN_PLL_INT_STA                   (0xA908u)

#define CKGEN_XTAL_UNC_EN                   (0xAA04u)
#define CKGEN_XTAL_INT_STA                  (0xAA08u)

#define CKGEN_MON_COR_EN                    (0xAB00u)
#define CKGEN_MON_UNC_EN                    (0xAB04u)
#define CKGEN_MON_INT_STA                   (0xAB08u)

#define CKGEN_LOW_SPD_COR_EN                (0xAC00u)
#define CKGEN_LOW_SPD_UNC_EN                (0xAC04u)
#define CKGEN_LOW_SPD_INT_STA               (0xAC08u)

#define CKGEN_CQM_COR_EN                    (0xAD00u)
#define CKGEN_CQM_UNC_EN                    (0xAD04u)
#define CKGEN_CQM_DUTY_INT_STA              (0xAD08u)
#define CKGEN_CQM_JITTER_INT_STA            (0xAD0Cu)

#define CKGEN_FUNC_INT_RS                   (0xB000u)
#define CKGEN_FUNC_INT                      (0xB004u)
#define CKGEN_FUNC_INT_ACCESS_ERR_EN        (0u)
#define CKGEN_FUNC_INT_ACCESS_ERR_STA       (8u)
#define CKGEN_FUNC_INT_ACCESS_ERR_CLR       (16u)

/* Domain access permission type */
#define CKGEN_DOM_PER_RS_NUM                (8u)
#define CKGEN_DOM_PER_DOMAIN_NUM            (8u)

#define CKGEN_DOM_PER_RW                    (0u)
#define CKGEN_DOM_PER_RO                    (1u)
#define CKGEN_DOM_PER_WO                    (2u)
#define CKGEN_DOM_PER_NONE                  (3u)

/* Ckgen sleep mode */
#define CKGEN_RUN_MODE                      (0u)
#define CKGEN_HIB_MODE                      (1u)
#define CKGEN_SLP_MODE                      (2u)

/* CKGEN FUSA RS */
#define CKGEN_FUSA_RS                       (0xA000U)
#define CKGEN_FUSA_RS_EN                    (0U)
#define CKGEN_FUSA_RS_RS                    (1U)
#define CKGEN_FUSA_RS_LOCK                  (31U)

/* CKGEN APB ERROR INT */
#define CKGEN_APB_ERR_INT                   (0xA004U)
#define CKGEN_APB_ERR_INT_PADDR_INT_CLR     (23U)
#define CKGEN_APB_ERR_INT_PUSER_INT_CLR     (22U)
#define CKGEN_APB_ERR_INT_PCTRL1_INT_CLR    (21U)
#define CKGEN_APB_ERR_INT_PCTRL0_INT_CLR    (20U)
#define CKGEN_APB_ERR_INT_PWDAT_C_INT_CLR   (19U)
#define CKGEN_APB_ERR_INT_PWDAT_U_INT_CLR   (18U)
#define CKGEN_APB_ERR_INT_PWDAT_F_INT_CLR   (17U)
#define CKGEN_APB_ERR_INT_PADDR_INT_STA     (15U)
#define CKGEN_APB_ERR_INT_PUSER_INT_STA     (14U)
#define CKGEN_APB_ERR_INT_PCTRL1_INT_STA    (13U)
#define CKGEN_APB_ERR_INT_PCTRL0_INT_STA    (12U)
#define CKGEN_APB_ERR_INT_PWDAT_C_INT_STA   (11U)
#define CKGEN_APB_ERR_INT_PWDAT_U_INT_STA   (10U)
#define CKGEN_APB_ERR_INT_PWDAT_F_INT_STA   (9U)
#define CKGEN_APB_ERR_INT_PADDR_INT_EN      (7U)
#define CKGEN_APB_ERR_INT_PUSER_INT_EN      (6U)
#define CKGEN_APB_ERR_INT_PCTRL1_INT_EN     (5U)
#define CKGEN_APB_ERR_INT_PCTRL0_INT_EN     (4U)
#define CKGEN_APB_ERR_INT_PWDAT_C_INT_EN    (3U)
#define CKGEN_APB_ERR_INT_PWDAT_U_INT_EN    (2U)
#define CKGEN_APB_ERR_INT_PWDAT_F_INT_EN    (1U)

/* CKGEN APB LKSTEP INT */
#define CKGEN_APB_LKSTEP_INT                        (0xA008U)
#define CKGEN_APB_LKSTEP_INT_SYNC_ERR_INT_CLR       (19U)
#define CKGEN_APB_LKSTEP_INT_RESP_ERR_INT_CLR       (18U)
#define CKGEN_APB_LKSTEP_INT_REQ_ERR_INT_CLR        (17U)
#define CKGEN_APB_LKSTEP_INT_CMP_ERR_INT_CLR        (16U)
#define CKGEN_APB_LKSTEP_INT_SYNC_ERR_INT_STA       (11U)
#define CKGEN_APB_LKSTEP_INT_RESP_ERR_INT_STA       (10U)
#define CKGEN_APB_LKSTEP_INT_REQ_ERR_INT_STA        (9U)
#define CKGEN_APB_LKSTEP_INT_CMP_ERR_INT_STA        (8U)
#define CKGEN_APB_LKSTEP_INT_SYNC_ERR_INT_EN        (3U)
#define CKGEN_APB_LKSTEP_INT_RESP_ERR_INT_EN        (2U)
#define CKGEN_APB_LKSTEP_INT_REQ_ERR_INT_EN         (1U)
#define CKGEN_APB_LKSTEP_INT_CMP_ERR_INT_EN         (0U)

/* CKGEN WDT LKSTEP INT */
#define CKGEN_WDT_LKSTEP_INT                        (0xA00C)
#define CKGEN_WDT_LKSTEP_INT_SYNC_ERR_INT_CLR       (17U)
#define CKGEN_WDT_LKSTEP_INT_CMP_ERR_INT_CLR        (16U)
#define CKGEN_WDT_LKSTEP_INT_SYNC_ERR_INT_STA       (9U)
#define CKGEN_WDT_LKSTEP_INT_CMP_ERR_INT_STA        (8U)
#define CKGEN_WDT_LKSTEP_INT_SYNC_ERR_INT_EN        (1U)
#define CKGEN_WDT_LKSTEP_INT_CMP_ERR_INT_EN         (0U)

/* CKGEN FUSA INT */
#define CKGEN_FUSA_INT                              (0xA100)
#define CKGEN_FUSA_INT_LKSTEP_CMP_ERR_CLR           (19U)
#define CKGEN_FUSA_INT_SYNC_ERR_CLR                 (18U)
#define CKGEN_FUSA_INT_SWM_TRANS_ERR_CLR            (17U)
#define CKGEN_FUSA_INT_SWM_CHK_ERR_CLR              (16U)
#define CKGEN_FUSA_INT_LKSTEP_CMP_ERR_STA           (11U)
#define CKGEN_FUSA_INT_SYNC_ERR_STA                 (10U)
#define CKGEN_FUSA_INT_SWM_TRANS_ERR_STA            (9U)
#define CKGEN_FUSA_INT_SWM_CHK_ERR_STA              (8U)
#define CKGEN_FUSA_INT_LKSTEP_CMP_ERR_EN            (3U)
#define CKGEN_FUSA_INT_SYNC_ERR_EN                  (2U)
#define CKGEN_FUSA_INT_SWM_TRANS_ERR_EN             (1U)
#define CKGEN_FUSA_INT_SWM_CHK_ERR_EN               (0U)

/* CKGEN_INJ_EN */
#define CKGEN_INJ_EN                                (0xA200)
#define CKGEN_INJ_EN_WDT_OUTPUT_ERR_INJ_EN          (9U)
#define CKGEN_INJ_EN_WDT_CMP_ERR_INJ_EN             (8U)
#define CKGEN_INJ_EN_XTAL_TOUT_ERR_INJ_EN           (7U)
#define CKGEN_INJ_EN_RSTGEN_OUTPUT_ERR_INJ_EN       (6U)
#define CKGEN_INJ_EN_AAPB_RESP_DED_ERR_INJ_EN       (5U)
#define CKGEN_INJ_EN_AAPB_OUTPUT_ERR_INJ_EN         (4U)
#define CKGEN_INJ_EN_AAPB_REQ_DED_ERR_INJ_EN        (3U)
#define CKGEN_INJ_EN_AAPB_LKSTEP_ERR_INJ_EN         (2U)
#define CKGEN_INJ_EN_CKGEN_LKSTEP_ERR_INJ_EN        (1U)
#define CKGEN_INJ_EN_IRQ_ERR_INJ_EN                 (0U)

/* CKGEN INJ BIT */
#define CKGEN_INJ_BIT                               (0xA204)
#define CKGEN_INJ_BIT_CKGEN_IRQ_INJ                 (31U)
#define CKGEN_INJ_BIT_COR_IRQ_INJ                   (30U)
#define CKGEN_INJ_BIT_UNC_IRQ_INJ                   (29U)
#define CKGEN_INJ_BIT_ERR_INJ_BIT                   (28U)
#define CKGEN_INJ_BIT_CKGEN_DOUT_ERR_INJ            (21U)
#define CKGEN_INJ_BIT_CKGEN_LKSTEP_ERR_INJ          (8U)
#define CKGEN_INJ_BIT_AAPB_RESP_DED_ERR_INJ         (0U)

#define CKGEN_DOUT_ERR_WIDTH                        (8U)
#define CKGEN_LKSTEP_ERR_WIDTH                      (13U)
#define CKGEN_AAPB_RESP_DED_ERR_WIDTH               (4U)

/* CKGEN INJ BIT 1 */
#define CKGEN_INJ_BIT_1                             (0xA208)
#define CKGEN_INJ_BIT_1_XTAL_TOUT_ERR_INJ           (16U)
#define CKGEN_INJ_BIT_1_AAPB_OUTPUT_ERR_INJ         (12U)
#define CKGEN_INJ_BIT_1_AAPB_REQ_DED_ERR_INJ        (8U)
#define CKGEN_INJ_BIT_1_AAPB_LKSTEP_ERR_INJ         (0U)

#define CKGEN_XTAL_TOUT_ERR_WIDTH                   (3U)
#define CKGEN_AAPB_OUTPUT_ERR_WIDTH                 (4U)
#define CKGEN_AAPB_REQ_DED_ERR_WIDTH                (4U)
#define CKGEN_AAPB_LKSTEP_ERR_WIDTH                 (8U)

/* CKGEN INJ BIT 2 */
#define CKGEN_INJ_BIT_2                             (0xA20C)
#define CKGEN_INJ_BIT_2_WDT_LKSTEP_ERR_INJ          (3U)
#define CKGEN_INJ_BIT_2_WDT_OUTPUT_ERR_INJ          (0U)

#define CKGEN_WDT_LKSTEP_ERR_WIDTH                  (4U)
#define CKGEN_WDT_OUTPUT_ERR_WIDTH                  (3U)

#define CKGEN_CG_DISABLE                    0
#define CKGEN_CG_ENABLE                     1

#define CKGEN_MON_DIVA                      0
#define CKGEN_MON_DIVB                      1
#define CKGEN_MON_DIVC                      2

#define CKGEN_MAX_PRE_DIV        32
#define CKGEN_MAX_DIV_M          16

#define CKGEN_IP_CK_IN4_SEL(sel)            ((sel) & (1u << 2))

#define CKGEN_DOM_PER_DOMAIN_OFF(did)       ((did)*8u)

#define CKGEN_DOM_PER_LOCK_OFF(did)         (did)

#define CKGEN_DOM_PER_BASE(base, rs_id) \
    ((base) + (CKGEN_DOM_PER + (rs_id)*12u))

#define CKGEN_DOM_PER_LOCK_BASE(base, rs_id) \
    ((base) + (CKGEN_DOM_PER_LOCK + (rs_id)*12u))

#define CKGEN_GLB_CTL_BASE(base) \
    ((base) + CKGEN_GLB_CTL)

#define CKGEN_IP_RS_BASE(base, id) \
    ((base) + (CKGEN_IP_RS + (id)*16u))

#define CKGEN_IP_CTL_BASE(base, id) \
    ((base) + (CKGEN_IP_CTL + (id)*16u))

#define CKGEN_IP_MON_CTL_BASE(base, id) \
    ((base) + (CKGEN_IP_MON_CTL + (id)*16u))

#define CKGEN_IP_MON_THRD_BASE(base, id) \
    ((base) + (CKGEN_IP_MON_THRD + (id)*16u))

#define CKGEN_BUS_RS_BASE(base, id) \
    ((base) + (CKGEN_BUS_RS + (id)*40u))

#define CKGEN_BUS_CTL_BASE(base, id) \
    ((base) + (CKGEN_BUS_CTL + (id)*40u))

#define CKGEN_BUS_SYNC_CTL_BASE(base, id) \
    ((base) + (CKGEN_BUS_SYNC_CTL + (id)*40u))

#define CKGEN_BUS_MON_CTL0_BASE(base, id) \
    ((base) + (CKGEN_BUS_MON_CTL0 + (id)*40u))

#define CKGEN_BUS_MON_CTL1_BASE(base, id) \
    ((base) + (CKGEN_BUS_MON_CTL1 + (id)*40u))

#define CKGEN_BUS_MON_CTL2_BASE(base, id) \
    ((base) + (CKGEN_BUS_MON_CTL2 + (id)*40u))

#define CKGEN_BUS_MON_THRD0_BASE(base, id) \
    ((base) + (CKGEN_BUS_MON_THRD0 + (id)*40u))

#define CKGEN_BUS_MON_THRD1_BASE(base, id) \
    ((base) + (CKGEN_BUS_MON_THRD1 + (id)*40u))

#define CKGEN_BUS_MON_THRD2_BASE(base, id) \
    ((base) + (CKGEN_BUS_MON_THRD2 + (id)*40u))

#define CKGEN_BUS_MON_THRD3_BASE(base, id) \
    ((base) + (CKGEN_BUS_MON_THRD3 + (id)*40u))

#define CKGEN_CORE_RS_BASE(base, id) \
    ((base) + (CKGEN_CORE_RS + (id)*16u))

#define CKGEN_CORE_CTL_BASE(base, id) \
    ((base) + (CKGEN_CORE_CTL + (id)*16u))

#define CKGEN_CORE_MON_CTL_BASE(base, id) \
    ((base) + (CKGEN_CORE_MON_CTL + (id)*16u))

#define CKGEN_CORE_MON_THRD_BASE(base, id) \
    ((base) + (CKGEN_CORE_MON_THRD + (id)*16u))

#define CKGEN_XCG_RS_BASE(base, id, m) \
    ((base) + (CKGEN_XCG_RS(m) + (id)*8u))

#define CKGEN_XCG_CTL_BASE(base, id, m) \
    ((base) + (CKGEN_XCG_CTL(m) + (id)*8u))

#define CKGEN_PLL_RS_BASE(base, id) \
    ((base) + (CKGEN_PLL_RS + (id)*12u))

#define CKGEN_PLL_CTL_BASE(base, id) \
    ((base) + (CKGEN_PLL_CTL + (id)*12u))

#define CKGEN_PLL_MON_CTL_BASE(base, id) \
    ((base) + (CKGEN_PLL_MON_CTL + (id)*12u))

#define CKGEN_IP_CLK_COR_EN_BASE(base, id) \
    ((base) + (CKGEN_IP_CLK_COR_EN + (id)*12u))

#define CKGEN_IP_CLK_UNC_EN_BASE(base, id) \
    ((base) + (CKGEN_IP_CLK_UNC_EN + (id)*12u))

#define CKGEN_IP_CLK_INT_STA_BASE(base, id) \
    ((base) + (CKGEN_IP_CLK_INT_STA + (id)*12u))

#define CKGEN_BUS_CLK_COR_EN_BASE(base, id) \
    ((base) + (CKGEN_BUS_CLK_COR_EN + (id)*12u))

#define CKGEN_BUS_CLK_UNC_EN_BASE(base, id) \
    ((base) + (CKGEN_BUS_CLK_UNC_EN + (id)*12u))

#define CKGEN_BUS_CLK_INT_STA_BASE(base, id) \
    ((base) + (CKGEN_BUS_CLK_INT_STA + (id)*12u))

#define CKGEN_CORE_CLK_COR_EN_BASE(base, id) \
    ((base) + (CKGEN_CORE_CLK_COR_EN + (id)*12u))

#define CKGEN_CORE_CLK_UNC_EN_BASE(base, id) \
    ((base) + (CKGEN_CORE_CLK_UNC_EN + (id)*12u))

#define CKGEN_CORE_CLK_INT_STA_BASE(base, id) \
    ((base) + (CKGEN_CORE_CLK_INT_STA + (id)*12u))

#define CKGEN_XCG_COR_EN_BASE(base, id, m) \
    ((base) + (CKGEN_XCG_COR_EN(m) + (id)*12u))

#define CKGEN_XCG_UNC_EN_BASE(base, id, m) \
    ((base) + (CKGEN_XCG_UNC_EN(m) + (id)*12u))

#define CKGEN_XCG_INT_STA_BASE(base, id, m) \
    ((base) + (CKGEN_XCG_INT_STA(m) + (id)*12u))

#define CKGEN_CQM_CTL_BASE(base, id) \
    ((base) + (CKGEN_CQM_CTL + (id)*4u))

#define CKGEN_PERMISSION(u, p, s, n) \
    ((((u) & CKGEN_DOM_PER_MASK) << CKGEN_DOM_PER_USE_PER_LSB) | \
    (((p) & CKGEN_DOM_PER_MASK) << CKGEN_DOM_PER_PRI_PER_LSB) | \
    (((n) & CKGEN_DOM_PER_MASK) << CKGEN_DOM_PER_NSEC_PER_LSB) | \
    (((s) & CKGEN_DOM_PER_MASK) << CKGEN_DOM_PER_SEC_PER_LSB))

/* Bus Slice Div Type */
#define CKGEN_HW_BUS_DIV_M          0       /**< divm, use for core */
#define CKGEN_HW_BUS_DIV_N          1       /**< divn, use for axi bus */
#define CKGEN_HW_BUS_DIV_P          2       /**< divp, use for apb bus */
#define CKGEN_HW_BUS_DIV_Q          3       /**< divq, not used */

/* XCG Type */
#define CKGEN_HW_PCG_TYPE           0       /**< PCG type */
#define CKGEN_HW_BCG_TYPE           1       /**< BCG type */
#define CKGEN_HW_CCG_TYPE           2       /**< CCG type */

#define CKGEN_WAIT_TIME                     500000u

#define CKGEN_AXI_MAX_RATE                  400000000U


bool sdrv_ckgen_ip_slice_gated(uint32_t base, uint32_t id);

bool sdrv_ckgen_core_slice_gated(uint32_t base, uint32_t id);

bool sdrv_ckgen_bus_slice_gated(uint32_t base, uint32_t id);

/**
 * @brief IP Slice Set Mux.
 *
 * This function change ip slice mux.
 *
 * @param [in] base ckgen base
 * @param [in] id ip slice id
 * @param [in] mxu ip slice mux
 * @return set result
 */
int sdrv_ckgen_ip_slice_set_mux(uint32_t base, uint32_t id, uint8_t mux);

/**
 * @brief IP Slice Get Mux.
 *
 * This function get ip slice mux.
 *
 * @param [in] base ckgen base
 * @param [in] id ip slice id
 * @return mux value
 */
uint8_t sdrv_ckgen_ip_slice_get_mux(uint32_t base, uint32_t id);

/**
 * @brief IP Slice Set Div.
 *
 * This function change ip slice div.
 *
 * @param [in] base ckgen base
 * @param [in] id ip slice id
 * @param [in] div ip slice div
 * @return set result
 */
int sdrv_ckgen_ip_slice_set_div(uint32_t base, uint32_t id, uint32_t div);

/**
 * @brief IP Slice Get Div.
 *
 * This function get ip slice div.
 *
 * @param [in] base ckgen base
 * @param [in] id ip slice id
 * @return div value
 */
uint32_t sdrv_ckgen_ip_slice_get_div(uint32_t base, uint32_t id);

/**
 * @brief Core Slice Set Mux.
 *
 * This function change core slice mux.
 *
 * @param [in] base ckgen base
 * @param [in] id core slice id
 * @param [in] mxu core slice mux
 * @return set result
 */
int sdrv_ckgen_core_slice_set_mux(uint32_t base, uint32_t id, uint8_t mux);

/**
 * @brief Core Slice Get Mux.
 *
 * This function get core slice mux.
 *
 * @param [in] base ckgen base
 * @param [in] id core slice id
 * @return mux value
 */
uint8_t sdrv_ckgen_core_slice_get_mux(uint32_t base, uint32_t id);

/**
 * @brief Core Slice Set Div.
 *
 * This function change core slice div.
 *
 * @param [in] base ckgen base
 * @param [in] id core slice id
 * @param [in] div core slice div
 * @return set result
 */
int sdrv_ckgen_core_slice_set_div(uint32_t base, uint32_t id, uint32_t div);

/**
 * @brief Core Slice Get Div.
 *
 * This function get core slice div.
 *
 * @param [in] base ckgen base
 * @param [in] id core slice id
 * @return div value
 */
uint32_t sdrv_ckgen_core_slice_get_div(uint32_t base, uint32_t id);

/**
 * @brief Bus Slice Set Post Mux.
 *
 * This function set bus slice post mux.
 *
 * @param [in] base ckgen base
 * @param [in] id bus slice id
 * @param [in] mux post mux value (0 or 1)
 * @return set result
 */
int sdrv_ckgen_bus_slice_set_post_mux(uint32_t base, uint32_t id, uint8_t mux);

/**
 * @brief Bus Slice Set Pre Mux.
 *
 * This function set bus slice pre mux.
 *
 * @param [in] base ckgen base
 * @param [in] id bus slice id
 * @param [in] mux pre mux value (0 ~ 4)
 * @return set result
 */
int sdrv_ckgen_bus_slice_set_pre_mux(uint32_t base, uint32_t id, uint8_t mux);

/**
 * @brief Bus Slice Set Pre Div.
 *
 * This function change bus slice pre div.
 *
 * @param [in] base ckgen base
 * @param [in] id bus slice id
 * @param [in] div bus slice pre div
 * @return set result
 */
int sdrv_ckgen_bus_slice_set_pre_div(uint32_t base, uint32_t id, uint32_t div);

/**
 * @brief Bus Slice Set SF/SP Post Div.
 *
 * This function change bus slice sf post div.
 *
 * @param [in] base ckgen base
 * @param [in] id bus slice id (0:SF 1:SP)
 * @param [in] ratio core/axi/apb ratio (0:4/2/1, 1:2/2/1)
 * @return set result
 */
int sdrv_ckgen_bus_slice_set_sf_post_div(uint32_t base, uint32_t id, uint32_t ratio);

/**
 * @brief Bus Slice Set MNPQ Post Div.
 *
 * This function change bus slice MNPQ div.
 *
 * @param [in] base ckgen base
 * @param [in] id bus slice id
 * @param [in] type mnpq type (0:m 1:n 2:p 3:q)
 * @param [in] div div value
 * @return set result
 */
int sdrv_ckgen_bus_slice_set_mnpq_div(uint32_t base, uint32_t id,
                                      uint8_t type, uint32_t div);

/**
 * @brief Bus Slice Get Pre Mux.
 *
 * This function get bus slice pre mux.
 *
 * @param [in] base ckgen base
 * @param [in] id bus slice id
 * @return pre mux num (0~4)
 */
uint8_t sdrv_ckgen_bus_slice_get_pre_mux(uint32_t base, uint32_t id);

/**
 * @brief Bus Slice Get Pre Div.
 *
 * This function get bus slice pre div.
 *
 * @param [in] base ckgen base
 * @param [in] id bus slice id
 * @return pre div num
 */
uint32_t sdrv_ckgen_bus_slice_get_pre_div(uint32_t base, uint32_t id);

/**
 * @brief Bus Slice Get MNPQ Div.
 *
 * This function get bus slice mnpq div.
 *
 * @param [in] base ckgen base
 * @param [in] id bus slice id
 * @param [in] type mnpq type (0:m 1:n 2:p 3:q)
 * @return mnpq div num
 */
uint32_t sdrv_ckgen_bus_slice_get_mnpq_div(uint32_t base, uint32_t id, uint8_t type);

/**
 * @brief XCG Set Gate In RUN/HIB/SLP Mode.
 *
 * This function set XCG enable or disable in run/hib/slp mode.
 *
 * @param [in] base ckgen base
 * @param [in] cg_type cg type (0:PCG, 1:BCG 2:CCG)
 * @param [in] id xcg id
 * @param [in] lp_mode run/hib/slp mode
 * @param [in] gating gate or active
 * @return set result
 */
int sdrv_ckgen_xcg_set_gate(uint32_t base, uint8_t cg_type, uint32_t id,
                            uint8_t lp_mode, bool gating);

/**
 * @brief XCG Get Gate Status In Run Mode.
 *
 * This function get XCG gate status.
 *
 * @param [in] base ckgen base
 * @param [in] cg_type cg type (0:PCG, 1:BCG 2:CCG)
 * @param [in] id xcg id
 * @return gated status (0:active 1:gated)
 */
bool sdrv_ckgen_xcg_is_gated(uint32_t base, uint8_t cg_type, uint32_t id);

/**
 * @brief XCG Set Sleep Mode Mask.
 *
 * This function set XCG sleep mode mask or unmask.
 *
 * @param [in] base ckgen base
 * @param [in] cg_type cg type (0:PCG 1:BCG 2:CCG)
 * @param [in] id xcg id
 * @param [in] mask mask or unmask
 * @return set result
 */
int sdrv_ckgen_xcg_lp_mask(uint32_t base, uint8_t cg_type, uint32_t id, bool mask);

/**
 * @brief Set PLL Gate In RUN/HIB/SLP Mode.
 *
 * This function set PLL enable or disable in run/hib/slp mode.
 *
 * @param [in] base ckgen base
 * @param [in] id pll id
 * @param [in] lp_mode run/hib/slp mode
 * @param [in] gating gate or active
 * @return set result
 */
int sdrv_ckgen_pll_set_gate(uint32_t base, uint32_t id, uint8_t lp_mode, bool gating);

/**
 * @brief Set PLL Power Down Mode In RUN/HIB/SLP Mode.
 *
 * This function set PLL pd mode in run/hib/slp mode.
 *
 * @param [in] base ckgen base
 * @param [in] id pll id
 * @param [in] lp_mode run/hib/slp mode
 * @param [in] pd_state pd mode (0:power on, 1:power down)
 * @return set result
 */
int sdrv_ckgen_pll_set_pd(uint32_t base, uint32_t id, uint8_t lp_mode, bool pd_state);

/**
 * @brief Set XTAL Gate In RUN/HIB/SLP Mode.
 *
 * This function set XTAL enable or disable in run/hib/slp mode.
 *
 * @param [in] base ckgen base
 * @param [in] lp_mode run/hib/slp mode
 * @param [in] gating gate or active
 * @return set result
 */
int sdrv_ckgen_xtal_set_gate(uint32_t base, uint8_t lp_mode, bool gating);

/**
 * @brief CKGEN RS Config.
 *
 * This function config RS regs.
 *
 * @param [in] base rs base
 * @param [in] rs rs id
 * @param [in] lock rs
 */
void sdrv_ckgen_enable_rs(uint32_t rs_base, uint8_t rs, bool lock);

/**
 * @brief CKGEN RS Config.
 *
 * This function config RS regs.
 *
 * @param [in] base rs base
 */
void sdrv_ckgen_disable_rs(uint32_t rs_base);

/**
 * @brief CKGEN IP Correctable Int Enable.
 *
 * @param [in] base ckgen base
 * @param [in] id ip slice id
 * @param [in] en enable or disable
 */
void sdrv_ckgen_ip_cor_int_enable(uint32_t base, uint32_t id, bool en);

/**
 * @brief CKGEN IP Uncorrectable Int Enable.
 *
 * @param [in] base ckgen base
 * @param [in] id ip slice id
 * @param [in] en enable or disable
 */
void sdrv_ckgen_ip_unc_int_enable(uint32_t base, uint32_t id, bool en);

/**
 * @brief Get CKGEN IP Int.
 *
 * @param [in] base ckgen base
 * @param [in] id ip slice id
 * @return int state
 */
bool sdrv_ckgen_ip_get_int_state(uint32_t base, uint32_t id);

/**
 * @brief Clear CKGEN IP Int.
 *
 * @param [in] base ckgen base
 * @param [in] id ip slice id
 */
void sdrv_ckgen_ip_clear_int_state(uint32_t base, uint32_t id);

/**
 * @brief CKGEN Bus Correctable Int Enable.
 *
 * @param [in] base ckgen base
 * @param [in] id slice id
 * @param [in] en enable or disable
 */
void sdrv_ckgen_bus_cor_int_enable(uint32_t base, uint32_t id, bool en);

/**
 * @brief CKGEN Bus Uncorrectable Int Enable.
 *
 * @param [in] base ckgen base
 * @param [in] id slice id
 * @param [in] en enable or disable
 */
void sdrv_ckgen_bus_unc_int_enable(uint32_t base, uint32_t id, bool en);

/**
 * @brief Get CKGEN Bus Int.
 *
 * @param [in] base ckgen base
 * @param [in] id slice id
 * @return int state
 */
bool sdrv_ckgen_bus_get_int_state(uint32_t base, uint32_t id);

/**
 * @brief Clear CKGEN Bus Int.
 *
 * @param [in] base ckgen base
 * @param [in] id slice id
 */
void sdrv_ckgen_bus_clear_int_state(uint32_t base, uint32_t id);

/**
 * @brief CKGEN Core Correctable Int Enable.
 *
 * @param [in] base ckgen base
 * @param [in] id slice id
 * @param [in] en enable or disable
 */
void sdrv_ckgen_core_cor_int_enable(uint32_t base, uint32_t id, bool en);

/**
 * @brief CKGEN Core Uncorrectable Int Enable.
 *
 * @param [in] base ckgen base
 * @param [in] id slice id
 * @param [in] en enable or disable
 */
void sdrv_ckgen_core_unc_int_enable(uint32_t base, uint32_t id, bool en);

/**
 * @brief Get CKGEN Core Int.
 *
 * @param [in] base ckgen base
 * @param [in] id slice id
 * @return int state
 */
bool sdrv_ckgen_core_get_int_state(uint32_t base, uint32_t id);

/**
 * @brief Clear CKGEN Core Int.
 *
 * @param [in] base ckgen base
 * @param [in] id slice id
 */
void sdrv_ckgen_core_clear_int_state(uint32_t base, uint32_t id);

/**
 * @brief CKGEN XCG Correctable Int Enable.
 *
 * @param [in] base ckgen base
 * @param [in] type xcg type
 * @param [in] id xcg id
 * @param [in] en enable or disable
 */
void sdrv_ckgen_xcg_cor_int_enable(uint32_t base, uint8_t type, uint32_t id, bool en);

/**
 * @brief CKGEN XCG Uncorrectable Int Enable.
 *
 * @param [in] base ckgen base
 * @param [in] type xcg type
 * @param [in] id slice id
 * @param [in] en enable or disable
 */
void sdrv_ckgen_xcg_unc_int_enable(uint32_t base, uint8_t type, uint32_t id, bool en);

/**
 * @brief Get CKGEN XCG Int.
 *
 * @param [in] base ckgen base
 * @param [in] type xcg type
 * @param [in] id xcg id
 * @return int state
 */
bool sdrv_ckgen_xcg_get_int_state(uint32_t base, uint8_t type, uint32_t id);

/**
 * @brief Clear XCG Int.
 *
 * @param [in] base ckgen base
 * @param [in] type xcg type
 * @param [in] id xcg id
 */
void sdrv_ckgen_xcg_clear_int_state(uint32_t base, uint8_t type,  uint32_t id);

/**
 * @brief Set Mon DIV.
 *
 * This function set monitor source div.
 *
 * @param [in] base ckgen base
 * @param [in] div_type mon div type
 * @param [in] div div number
 * @return rate
 */
void sdrv_ckgen_set_mon_div(uint32_t base, uint8_t div_type, uint32_t div);

/**
 * @brief Get IP Monitor Rate.
 *
 * This function get ip slice rate use monitor.
 *
 * @param [in] base ckgen base
 * @param [in] id ip slice id
 * @return rate
 */
uint32_t sdrv_ckgen_ip_mon_get_rate(uint32_t base, uint32_t id);

/**
 * @brief Set IP Monitor Threshold.
 *
 * This function get ip slice rate use monitor.
 *
 * @param [in] base ckgen base
 * @param [in] id ip slice id
 * @param [in] low_thrd lowest threshold
 * @param [in] high_thrd highest threshold
 */
void sdrv_ckgen_ip_mon_set_thrd(uint32_t base, uint32_t id,
                                uint16_t low_thrd, uint16_t high_thrd);

/**
 * @brief Get Bus Monitor Rate.
 *
 * This function get bus slice rate use monitor.
 *
 * @param [in] base ckgen base
 * @param [in] type bus type
 * @param [in] id bus slice id
 * @return rate
 */
uint32_t sdrv_ckgen_bus_mon_get_rate(uint32_t base, uint32_t id, uint8_t type);

/**
 * @brief Set BUS Monitor Threshold.
 *
 * This function get bus slice rate use monitor.
 *
 * @param [in] base ckgen base
 * @param [in] type bus type
 * @param [in] id bus slice id
 * @param [in] low_thrd lowest threshold
 * @param [in] high_thrd highest threshold
 */
void sdrv_ckgen_bus_mon_set_thrd(uint32_t base, uint32_t id, uint8_t type,
                                 uint16_t low_thrd, uint16_t high_thrd);

/**
 * @brief Get Core Monitor Rate.
 *
 * This function get core slice rate use monitor.
 *
 * @param [in] base ckgen base
 * @param [in] id core slice id
 * @return rate
 */
uint32_t sdrv_ckgen_core_mon_get_rate(uint32_t base, uint32_t id);

/**
 * @brief Set CORE Monitor Threshold.
 *
 * This function get core slice rate use monitor.
 *
 * @param [in] base ckgen base
 * @param [in] id core slice id
 * @param [in] low_thrd lowest threshold
 * @param [in] high_thrd highest threshold
 */
void sdrv_ckgen_core_mon_set_thrd(uint32_t base, uint32_t id,
                                  uint16_t low_thrd, uint16_t high_thrd);

/**
 * @brief Get XCG Gate Status In Run Mode.
 *
 * This function get XCG gate status.
 *
 * @param [in] base ckgen base
 * @param [in] cg_type cg type (0:PCG 1:BCG 2:CCG)
 * @param [in] id xcg id
 * @return gated status (0:active 1:gated)
 */
bool sdrv_ckgen_mon_xcg_is_gated(uint32_t base, uint8_t cg_type, uint32_t id);

/**
 * @brief Get PLL Gate Status In Run Mode.
 *
 * This function get PLL gate status.
 *
 * @param [in] base ckgen base
 * @param [in] id pll id
 * @return gated status (0:active 1:gated)
 */
bool sdrv_ckgen_mon_pll_is_gated(uint32_t base, uint32_t id);

/**
 * @brief Get XTAL Gate Status In Run Mode.
 *
 * This function get PLL gate status.
 *
 * @param [in] base ckgen base
 * @return gated status (0:active 1:gated)
 */
bool sdrv_ckgen_mon_xtal_is_gated(uint32_t base);

/**
 * @brief Select Debug Monitor Source.
 *
 * This function select ip slice for debug monitor.
 *
 * @param [in] base ckgen base
 * @param [in] id ip slice id
 */
void sdrv_ckgen_dbg_mon_ip_select(uint32_t base, uint32_t id);

/**
 * @brief Use Debug Monitor To Get IP Rate.
 *
 * This function use debug monitor get ip slice clk out rate.
 *
 * @param [in] base ckgen base
 * @param [in] id ip slice id
 * @return rate
 */
uint32_t sdrv_ckgen_dbg_mon_get_ip_rate(uint32_t base, uint32_t id);

/**
 * @brief Select Debug Monitor Source.
 *
 * This function select bus slice for debug monitor.
 *
 * @param [in] base ckgen base
 * @param [in] id bus slice id
 */
void sdrv_ckgen_dbg_mon_bus_select(uint32_t base, uint32_t id);

/**
 * @brief Use Debug Monitor To Get Bus Rate.
 *
 * This function use debug monitor get bus slice clk out rate.
 *
 * @param [in] base ckgen base
 * @param [in] id bus slice id
 * @return rate
 */
uint32_t sdrv_ckgen_dbg_mon_get_bus_rate(uint32_t base, uint32_t id);

/**
 * @brief Select Debug Monitor Source.
 *
 * This function select core slice for debug monitor.
 *
 * @param [in] base ckgen base
 * @param [in] id core slice id
 */
void sdrv_ckgen_dbg_mon_core_select(uint32_t base, uint32_t id);

/**
 * @brief Use Debug Monitor To Get Core Rate.
 *
 * This function use debug monitor get core slice clk out rate.
 *
 * @param [in] base ckgen base
 * @param [in] id core slice id
 * @return rate
 */
uint32_t sdrv_ckgen_dbg_mon_get_core_rate(uint32_t base, uint32_t id);

/**
 * @brief Use Debug Monitor To Get Ext Clk Rate.
 *
 * This function use debug monitor get ext clk rate.
 *
 * @param [in] base ckgen base
 * @param [in] id ext clk id
 * @return rate
 */
uint32_t sdrv_ckgen_dbg_mon_get_ext_rate(uint32_t base, uint32_t id);

/**
 * @brief Monitor fs24M.
 *
 * This function monitor fs24M.
 *
 * @param [in] base ckgen base
 * @return rate
 */
uint32_t sdrv_ckgen_24M_mon_get_rate(uint32_t base);

/**
 * @brief Enable CQM.
 *
 * @param [in] base ckgen base
 * @param [in] id cqm id
 * @param [in] duty cqm duty
 * @param [in] jitter cqm jitter
 */
void sdrv_ckgen_cqm_enable(uint32_t base, uint32_t id,
                           uint16_t duty, uint16_t jitter);

/**
 * @brief Disable CQM.
 *
 * @param [in] base ckgen base
 * @param [in] id cqm id
 */
void sdrv_ckgen_cqm_disable(uint32_t base, uint32_t id);

/**
 * @brief Enable CQM Correctable Int.
 *
 * @param [in] base ckgen base
 * @param [in] id cqm id
 * @param [in] en enable or disable
 */
void sdrv_ckgen_cqm_cor_int_enable(uint32_t base, uint32_t id, bool en);

/**
 * @brief Enable CQM Uncorrectable Int.
 *
 * @param [in] base ckgen base
 * @param [in] id cqm id
 * @param [in] en enable or disable
 */
void sdrv_ckgen_cqm_unc_int_enable(uint32_t base, uint32_t id, bool en);

/**
 * @brief Get CQM duty Int.
 *
 * @param [in] base ckgen base
 * @param [in] id cqm id
 * @return int state
 */
bool sdrv_ckgen_cqm_get_duty_int_state(uint32_t base, uint32_t id);

/**
 * @brief Get CQM jitter Int.
 *
 * @param [in] base ckgen base
 * @param [in] id cqm id
 * @return int state
 */
bool sdrv_ckgen_cqm_get_jitter_int_state(uint32_t base, uint32_t id);

/**
 * @brief Clear CQM duty Int.
 *
 * @param [in] base ckgen base
 * @param [in] id cqm id
 */
void sdrv_ckgen_cqm_clear_duty_int_state(uint32_t base, uint32_t id);

/**
 * @brief Clear CQM jitter Int.
 *
 * @param [in] base ckgen base
 * @param [in] id cqm id
 */
void sdrv_ckgen_cqm_clear_jitter_int_state(uint32_t base, uint32_t id);

#endif /* SDRV_CKGEN_HW_ACCESS_H_ */
