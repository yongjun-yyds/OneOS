/**
 * @file sdrv_ckgen_common.h
 * @brief taishan ckgen common header file.
 *
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef SDRV_CKGEN_COMMON_H_
#define SDRV_CKGEN_COMMON_H_

#include <reg.h>

#ifndef DIV_ROUND_UP
#define DIV_ROUND_UP(n, d)          (((n) + (d) - 1u) / (d))
#endif

#ifndef DIV_ROUND_CLOSEST
#define DIV_ROUND_CLOSEST(n, d)     (((n) + (d)/2) / (d))
#endif

#ifndef ABS
#define ABS(x)                      (((int)(x) < 0) ? -(int)(x) : (int)(x))
#endif

#ifndef BIT
#define BIT(x, bit)                 ((x) & (1UL << (bit)))
#endif

/**
 * @brief Wait register specific bit to expect value until timeout.
 *
 * This function busy read register value, if specific bit equal to expect value,
 * it will return true. Otherwise, after timeout, return false.
 *
 * @param base register address
 * @param offset register bit offset.
 * @param val expect value, 1 or 0.
 * @param count busy loop count.
 * @return true represents bit in register equal to expect value, false represents timeout.
 */
static inline bool sdrv_ckgen_wait(uint32_t base, uint32_t offset,
                                   uint32_t val, uint32_t count)
{
    volatile uint32_t times_out = count;

    while ((times_out != 0u) &&
           (val != ((readl(base) & ((uint32_t)1u << offset)) >> offset))) {
        times_out--;
    }

    return times_out > 0u ? true : false;
}

#endif