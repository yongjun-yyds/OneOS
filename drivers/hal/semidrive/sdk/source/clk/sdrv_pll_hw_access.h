/**
 * @file sdrv_pll_hw_access.h
 * @brief taishan pll hw access header file.
 *
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef SDRV_PLL_HW_ACCESS_H_
#define SDRV_PLL_HW_ACCESS_H_

#include <sdrv_ckgen.h>
#include <types.h>

/* Control register */
#define PLL_CTRL                        (0x0u)
#define PLL_CTRL_PD                     (0u)
#define PLL_CTRL_GATING_MODE            (1u)
#define PLL_CTRL_LOCK_MODE              (2u)
#define PLL_CTRL_LOCK_TIME_LSB          (3u)
#define PLL_CTRL_LOCK_TIME_MASK         (0xFu)
#define PLL_CTRL_POSTDIV                (7u)
#define PLL_CTRL_FORCE_LOCK             (8u)
#define PLL_CTRL_DSM_DISABLE            (9u)
#define PLL_CTRL_RESETN                 (10u)
#define PLL_CTRL_RESETN_SEL             (11u)
#define PLL_CTRL_ICP_LSB                (16u)
#define PLL_CTRL_ICP_MASK               (0x7u)
#define PLL_CTRL_LDO_V0P8_LSB           (19u)
#define PLL_CTRL_LDO_V0P8_MASK          (0x3u)
#define PLL_CTRL_VCO_BAND_LSB           (21u)
#define PLL_CTRL_VCO_BAND_MASK          (0x7u)
#define PLL_CTRL_RESETN_DLY_LSB         (25u)
#define PLL_CTRL_RESETN_DLY_MASK        (0xFu)
#define PLL_CTRL_FORCE_BYPASS           (29u)
#define PLL_CTRL_SEM_SWITCH_EN          (30u)
#define PLL_CTRL_SEMM_SWITCH_SRC_SEL    (31u)

/* N Number register */
#define PLL_N_NUM                       (0x4u)
#define PLL_N_NUM_VAL_LSB               (0u)
#define PLL_N_NUM_VAL_MASK              (0x7Fu)

/* M Number register */
#define PLL_M_NUM                       (0x8u)
#define PLL_M_NUM_VAL_LSB               (0u)
#define PLL_M_NUM_VAL_MASK              (0x3FFFFFFu)
#define PLL_M_NUM_SW_READY              (30u)
#define PLL_M_NUM_READY_SEL             (31u)

/* DSM register */
#define PLL_DSM                         (0xCu)
#define PLL_DSM_SSC_OFFSET_LSB          (0u)
#define PLL_DSM_SSc_OFFSET_MASK         (0x3u)
#define PLL_DSM_SSC_FREQ_LSB            (2u)
#define PLL_DSM_SSC_FREQ_MASK           (0x3u)
#define PLL_DSM_SSC_MODE_LSB            (4u)
#define PLL_DSM_SSC_MODE_MASK           (0x3u)
#define PLL_DSM_SSC_DEP_LSB             (6u)
#define PLL_DSM_SSC_DEP_MASK            (0x1Fu)

/* DTEST register */
#define PLL_DTEST                       (0x10u)
#define PLL_DTEST_EN                    (0u)
#define PLL_DTEST_SEL_LSB               (1u)
#define PLL_DTEST_SEL_MASK              (0x3u)
#define PLL_DTEST_REF_TEST_SEL          (3u)
#define PLL_DTEST_DTESTO                (31u)

/* ATEST register */
#define PLL_ATEST                       (0x14u)
#define PLL_ATEST_EN                    (0u)
#define PLL_ATEST_SEL_LSB               (1u)
#define PLL_ATEST_SEL_MASK              (0x3u)

/* STATUS register */
#define PLL_STATUS                      (0x20u)
#define PLL_STATUS_LOCK                 (0u)
#define PLL_STATUS_DUTY_DET             (1u)
#define PLL_STATUS_NO_REFCK             (2u)
#define PLL_STATUS_PLL_M_READY          (3u)
#define PLL_STATUS_LOCK_LOSS            (8u)
#define PLL_STATUS_DUTY_LOSS            (9u)
#define PLL_STATUS_REFCK_LOSS           (10u)

/* LVDS_CTL register */
#define PLL_LVDS_CTL                    (0xA0u)
#define PLL_LVDS_CTL_DIV2_GATING_EN     (0u)
#define PLL_LVDS_CTL_DIV7_GATING_EN     (1u)
#define PLL_LVDS_CTL_DIV2_NUM_LSB       (2u)
#define PLL_LVDS_CTL_DIV2_NUM_MASK      (0x1Fu)
#define PLL_LVDS_CTL_DIV7_NUM_LSB       (7u)
#define PLL_LVDS_CTL_DIV7_NUM_MASK      (0x1Fu)
#define PLL_LVDS_CTL_GATING_EN          (12u)
#define PLL_LVDS_CTL_CKGEN_GATING_EN    (16u)
#define PLL_LVDS_CTL_CKGEN_DIV_NUM_LSB  (17u)
#define PLL_LVDS_CTL_CKGEN_DIV_NUM_MASK (0xFu)
#define PLL_LVDS_CTL_CKGEN_GATING_STATUS  (25u)
#define PLL_LVDS_CTL_CKGEN_CHG_BUSY       (26u)
#define PLL_LVDS_CTL_GATING_STATUS      (27u)
#define PLL_LVDS_CTL_DIV7_GATING_STATUS (28u)
#define PLL_LVDS_CTL_DIV2_GATING_STATUS (29u)
#define PLL_LVDS_CTL_DIV7_CHG_BUSY      (30u)
#define PLL_LVDS_CTL_DIV2_CHG_BUSY      (31u)

#define PLL_LVDS_OUT_DIV_MIN            (1u)
#define PLL_LVDS_OUT_DIV_MAX            (32u)
#define PLL_LVDS_CKGEN_IN_DIV_MAX       (16u)

#define PLL_VCO_FMAX                    2400000000ul
#define PLL_VCO_FMIN                    500000000ul
#define PLL_VCO_BAND                    1000000000ul

/* PLL lock wait time */
#define PLL_WAIT_TIME                   10000u

/**
 * @brief PLL VCO Enable.
 *
 * This function enable pll vco, set vco in run status.
 *
 * @param [in] base pll base
 * @param [in] enable enable or disable
 * @return enable result
 */
status_t sdrv_pll_vco_enable(uint32_t base, bool enable);

/**
 * @brief PLL Set VCO Rate.
 *
 * This function set pll vco rate.
 *
 * @param [in] base pll base
 * @param [in] src_rate pll source clk rate
 * @param [in] rate set rate
 * @return set result
 */
status_t sdrv_pll_set_vco_rate(uint32_t base, uint32_t src_rate,
                          uint32_t rate, bool dsm_en);

/**
 * @brief PLL Get VCO Rate.
 *
 * This function get pll vco rate.
 *
 * @param [in] base pll base
 * @param [in] src_rate pll source clk rate
 * @return get rate
 */
uint32_t sdrv_pll_get_vco_rate(uint32_t base, uint32_t src_rate);

/**
 * @brief PLL Enable LVDS Clk0 Gate.
 *
 * This function enable LVDS Clk0 gate.
 *
 * @param [in] base pll base
 * @param [in] enable enable or disable
 * @return enable result
 */
status_t sdrv_pll_lvds_clk0_enable(uint32_t base, bool enable);

/**
 * @brief PLL Enable LVDS Clk1 Gate.
 *
 * This function enable LVDS Clk1 gate.
 *
 * @param [in] base pll base
 * @param [in] enable enable or disable
 * @return enable result
 */
status_t sdrv_pll_lvds_clk1_enable(uint32_t base, bool enable);

/**
 * @brief PLL Enable LVDS Clk2 Gate.
 *
 * This function enable LVDS Clk2 gate.
 *
 * @param [in] base pll base
 * @param [in] enable enable or disable
 * @return enable result
 */
status_t sdrv_pll_lvds_clk2_enable(uint32_t base, bool enable);

/**
 * @brief PLL Enable LVDS ckgen Gate.
 *
 * This function enable LVDS ckgen gate.
 *
 * @param [in] base pll base
 * @param [in] enable enable or disable
 * @return enable result
 */
status_t sdrv_pll_lvds_ckgen_enable(uint32_t base, bool enable);

/**
 * @brief PLL Set LVDS Div2.
 *
 * This function set LVDS div2 value.
 *
 * @param [in] base pll base
 * @param [in] div div2 number
 * @return set result
 */
status_t sdrv_pll_set_lvds_clk1_div(uint32_t base, uint8_t div);

/**
 * @brief PLL Set LVDS Div7.
 *
 * This function set LVDS div7 value.
 *
 * @param [in] base pll base
 * @param [in] div div7 number
 * @return set result
 */
status_t sdrv_pll_set_lvds_clk2_div(uint32_t base, uint8_t div);

/**
 * @brief PLL Set LVDS div ckgen
 *
 * This function set LVDS div ckgen value.
 *
 * @param [in] base pll base
 * @param [in] div div ckgen number
 * @return set result
 */
status_t sdrv_pll_set_lvds_ckgen_div(uint32_t base, uint8_t div);

/**
 * @brief PLL Get LVDS Div2.
 *
 * This function Get LVDS div2 value.
 *
 * @param [in] base pll base
 * @return div2 number
 */
uint8_t sdrv_pll_get_lvds_clk1_div(uint32_t base);

/**
 * @brief PLL Get LVDS Div7.
 *
 * This function Get LVDS div7 value.
 *
 * @param [in] base pll base
 * @return div7 number
 */
uint8_t sdrv_pll_get_lvds_clk2_div(uint32_t base);

/**
 * @brief PLL Get LVDS div ckgen.
 *
 * This function Get LVDS div ckgen value.
 *
 * @param [in] base pll base
 * @return div ckgen number
 */
uint8_t sdrv_pll_get_lvds_ckgen_div(uint32_t base);

/**
 * @brief PLL Get Lock Status.
 *
 * This function get pll lock status.
 *
 * @param [in] base pll base
 * @return lock status
 */
bool sdrv_pll_get_lock_status(uint32_t base);

void sdrv_pll_set_dsm_ssc_dep(uint32_t base, uint32_t ssc_dep);

void sdrv_pll_set_dsm_ssc_freq(uint32_t base, uint32_t ssc_freq);

status_t sdrv_pll_set_dsm_ssc_mode(uint32_t base, uint32_t mode);

#endif /* SDRV_PLL_HW_ACCESS_H_ */