/**
 * @file sdrv_fs32k_hw_access.h
 * @brief taishan fs32k hw access header file.
 *
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef SDRV_FS32K_HW_ACCESS_H_
#define SDRV_FS32K_HW_ACCESS_H_

#include <types.h>

/* GLB CTL register */
#define FS32K_GLB_CTL                   (0x0u)
#define FS32K_GLB_CTL_XTAL_SRC_SEL      (0u)
#define FS32K_GLB_CTL_FS_SRC_SEL        (1u)
#define FS32K_GLB_CTL_FS_XTAL_EN        (2u)
#define FS32K_GLB_CTL_FS_OSC_EN         (3u)
#define FS32K_GLB_CTL_EXT_XTAL_EN       (4u)
#define FS32K_GLB_CTL_EXT_OSC_EN        (5u)
#define FS32K_GLB_CTL_CMP_FORCE_CLR     (8u)
#define FS32K_GLB_CTL_RC_RDY            (28u)
#define FS32K_GLB_CTL_XTAL_RDY          (29u)
#define FS32K_GLB_CTL_RC_ACTIVE         (30u)
#define FS32K_GLB_CTL_XTAL_ACTIVE       (31u)

#define FS32K_RC_CTL                    (0x10u)
#define FS32K_RC_CTL_FREQ_TUNE_B_LSB    (0u)
#define FS32K_RC_CTL_FREQ_TUNE_B_MASK   (0xfu)

#define FS32K_XTAL_CTL                  (0X20U)
#define FS32K_XTAL_CTL_XTAL_ENABLE      (0X0U)
#define FS32K_XTAL_CTL_XTAL_TEST_ENABLE (0X1U)

#define FS32K_LPVD_CTRL                 (0X30U)
#define FS32K_LPVD_CTRL_PD              (0U)


#define FS32K_FS_SEL_RC                 0
#define FS32K_FS_SEL_XTAL               1

/**
 * @brief FS32K FS Source Select.
 *
 * This function select fs source.
 *
 * @param [in] base fs32k base
 * @param [in] src fs source
 */
void sdrv_fs32k_fs_sel(uint32_t base, uint8_t src);

/**
 * @brief FS32K FS Source Get.
 *
 * This function get fs source.
 *
 * @param [in] base fs32k base
 * @return FS32K_FS_SEL_RC or FS32K_FS_SEL_XTAL
 */
uint32_t sdrv_fs32k_fs_sel_get(uint32_t base);

/**
 * @brief FS32K Check FS Active Source.
 *
 * This function check fs32k active source.
 *
 * @param [in] base fs32k base
 * @param [in] src fs source
 * @param [in] count wait count
 * @return active status
 */
int sdrv_fs32k_wait_active(uint32_t base, uint8_t src, uint32_t count);

/**
 * @brief FS32K Check XTAL/RC Ready status
 *
 * This function check XTAL/RC Ready status
 *
 * @param [in] base fs32k base
 * @param [in] src fs source
 * @param [in] count wait count
 * @return ready status
 */
int sdrv_fs32k_wait_ready(uint32_t base, uint8_t src, uint32_t count);

/**
 * @brief FS32K Fail Safe Enable.
 *
 * This function enable or disable fail safe.
 *
 * @param [in] base fs32k base
 * @param [in] enable enable or disable failsafe
 */
void sdrv_fs32k_xtal_fs_enable(uint32_t base, bool enable);

/**
 * @brief FS32K Fail Safe Enable.
 *
 * This function enable or disable fail safe.
 *
 * @param [in] base fs32k base
 * @param [in] enable enable or disable failsafe
 */
void sdrv_fs32k_rc_fs_enable(uint32_t base, bool enable);

/**
 * @brief FS32K xtal enable or disable
 *
 * @param [in] base fs32k base
 * @param [in] enable true or false
 */
void sdrv_fs32k_xtal_enable(uint32_t base, bool enable);

/**
 * @brief FS32K xtal test enable or disable
 *
 * This function control test enable, if enabled, clk from xtal_i pin.
 *
 * @param [in] base fs32k base
 * @param [in] enable true or false
 */
void sdrv_fs32k_xtal_test_enable(uint32_t base, bool enable);

/**
 * @brief RC32K frequency tune.
 *
 * This function tune rc32k frequency, the central frequency should be 32Khz. Step is 0.5KHz.
 * 0x0: Fastest, 0x8: 32KHz, 0xF: Lowest.
 *
 * @param [in] base fs32k base
 * @param [in] tune tune value to be set
 */
void sdrv_fs32k_set_rc_freq_tune_b(uint32_t base, uint32_t tune);

/**
 * @brief Get RC32K frequency tune value.
 *
 * @param [in] base fs32k base
 * @return tune value
 */
uint32_t sdrv_fs32k_get_rc_freq_tune_b(uint32_t base);

/**
 * @brief Set low power voltage detector power control.
 *
 * @param [in] base fs32k base
 * @param [in] power_on true:power on, false:power down.
 */
void sdrv_fs32k_lpvd_power_ctl(uint32_t base, bool power_on);

#endif