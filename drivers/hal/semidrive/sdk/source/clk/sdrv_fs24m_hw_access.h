/**
 * @file sdrv_fs24m_hw_access.h
 * @brief taishan fs24M hw access header file.
 *
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */

#ifndef SDRV_FS24M_HW_ACCESS_H_
#define SDRV_FS24M_HW_ACCESS_H_

#include <types.h>

/* GLB CTL register */
#define FS24M_GLB_CTL                   (0x0u)
#define FS24M_GLB_CTL_XTAL_SRC_SEL      (0u)
#define FS24M_GLB_CTL_FS_SRC_SEL        (1u)
#define FS24M_GLB_CTL_FS_XTAL_EN        (2u)
#define FS24M_GLB_CTL_FS_OSC_EN         (3u)
#define FS24M_GLB_CTL_EXT_XTAL_EN       (4u)
#define FS24M_GLB_CTL_EXT_OSC_EN        (5u)
#define FS24M_GLB_CTL_CMP_FORCE_CLR     (8u)
#define FS24M_GLB_CTL_SLP_EXP           (16u)
#define FS24M_GLB_CTL_HIB_EXP           (17u)
#define FS24M_GLB_CTL_RC_RDY            (28u)
#define FS24M_GLB_CTL_XTAL_RDY          (29u)
#define FS24M_GLB_CTL_RC_ACTIVE         (30u)
#define FS24M_GLB_CTL_XTAL_ACTIVE       (31u)

#define FS24M_XTAL_CTRL                  (0x20u)
#define FS24M_XTAL_CTRL_XTAL_ENABLE      (0u)
#define FS24M_XTAL_CTRL_XTAL_TEST_ENABLE (1u)

/* RC32K CHK CTL register */
#define FS24M_RC32K_CHK_CTL             (0x500u)
#define FS24M_RC32K_CHK_CTL_FREQ_CNT_STA_LSB  (16u)
#define FS24M_RC32K_CHK_CTL_FREQ_CNT_STA_MASK (0xFFFFu)
#define FS24M_RC32K_CHK_CTL_FREQ_VIO_CLR_LSB  (3u)
#define FS24M_RC32K_CHK_CTL_FREQ_VIO_STA_LSB  (2u)
#define FS24M_RC32K_CHK_CTL_MON_EN_STA_LSB    (1u)
#define FS24M_RC32K_CHK_CTL_MON_EN_LSB        (0u)

#define FS24M_XTAL_SEL_PAD              0
#define FS24M_XTAL_SEL_EXT              1

#define FS24M_FS_SEL_RC                 0
#define FS24M_FS_SEL_XTAL               1

/**
 * @brief FS24M XTAL Source Select.
 *
 * This function select xtal source.
 *
 * @param [in] base fs24m base
 * @param [in] src xtal source
 */
void sdrv_fs24m_xtal_sel(uint32_t base, uint8_t src);

/**
 * @brief FS24M FS Source Select.
 *
 * This function select fs source.
 *
 * @param [in] base fs24m base
 * @param [in] src fs source
 */
void sdrv_fs24m_fs_sel(uint32_t base, uint8_t src);

/**
 * @brief FS24M FS Source Get.
 *
 * This function get fs source.
 *
 * @param [in] base fs24m base
 * @return FS24M_FS_SEL_RC or FS24M_FS_SEL_XTAL.
 */
uint32_t sdrv_fs24m_fs_sel_get(uint32_t base);

/**
 * @brief FS24M Check FS Active Source.
 *
 * This function check fs24m active source.
 *
 * @param [in] base fs24m base
 * @param [in] src fs source
 * @param [in] count wait count
 * @return active status
 */
int sdrv_fs24m_wait_active(uint32_t base, uint8_t src, uint32_t count);

/**
 * @brief FS24M Check ready status
 *
 * This function check fs24m xtal/rc ready status
 *
 * @param [in] base fs24m base
 * @param [in] src fs source
 * @param [in] count wait count
 * @return ready status
 */
int sdrv_fs24m_wait_ready(uint32_t base, uint8_t src, uint32_t count);

/**
 * @brief FS24M Fail Safe Enable.
 *
 * This function enable or disable fail safe.
 *
 * @param [in] base fs24m base
 * @param [in] enable enable or disable failsafe
 */
void sdrv_fs24m_xtal_fs_enable(uint32_t base, bool enable);

/**
 * @brief FS24M Fail Safe Enable.
 *
 * This function enable or disable fail safe.
 *
 * @param [in] base fs24m base
 * @param [in] enable enable or disable failsafe
 */
void sdrv_fs24m_rc_fs_enable(uint32_t base, bool enable);

/**
 * @brief Xtal 24m enable or disable
 *
 * @param [in] base fs24m base
 * @param [in] enable true or false
 */
void sdrv_fs24m_xtal_enable(uint32_t base, bool enable);


/**
 * @brief Xtal 24m test enable or disable
 *
 * @param [in] base fs24m base
 * @param [in] enable true or false
 */
void sdrv_fs24m_xtal_test_enable(uint32_t base, bool enable);

/**
 * @brief Use 24M check 32K clock.
 *
 * This function use FS24M clock to check FS32K clock frequency. For example, FS24M clock source choose XTAL24M, make it
 * accurate to 24000000, and config FS32K clock source to RC or xtal you want to get real frequency. After do above all,
 * call this function, you will get frequency counter. Real 32K frequency is 24000000 / counter.
 *
 * @param [in] base fs24m base
 * @return frequency counter
 */
uint32_t sdrv_fs24m_get_32k_frequency_counter(uint32_t base);

#endif