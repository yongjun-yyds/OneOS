/**
 * @file sdrv_ckgen_dump.h
 * @brief semidrive ckgen dump header file.
 *
 * @copyright Copyright (c) 2022  Semidrive Semiconductor.
 *            All rights reserved.
 */

#if CONFIG_CLK_DUMP

#ifndef SDRV_CKGEN_DUMP_H_
#define SDRV_CKGEN_DUMP_H_

#include <sdrv_ckgen.h>
#include <types.h>

#define SDRV_CKGEN_REF_MAX_NUM 4
#define SDRV_CKGEN_PLL_CTRL_MAX_NUM 6
#define SDRV_CKGEN_PLL_LVDS_MAX_NUM 4
#define SDRV_CKGEN_SF_BUS_MAX_NUM 3
#define SDRV_CKGEN_BUS_MAX_NUM 3
#define SDRV_CKGEN_CORE_MAX_NUM 1
#define SDRV_CKGEN_SAF_IP_MAX_NUM 34
#define SDRV_CKGEN_AP_IP_MAX_NUM 17

typedef struct sdrv_ckgen_clk {
    sdrv_clk_t *ref_clk[SDRV_CKGEN_REF_MAX_NUM];
    sdrv_clk_t *pll_ctrl[SDRV_CKGEN_PLL_CTRL_MAX_NUM];
    sdrv_clk_t *pll_lvds[SDRV_CKGEN_PLL_LVDS_MAX_NUM];
    sdrv_clk_t *sf_bus[SDRV_CKGEN_SF_BUS_MAX_NUM];
    sdrv_clk_t *bus[SDRV_CKGEN_BUS_MAX_NUM];
    sdrv_clk_t *core[SDRV_CKGEN_CORE_MAX_NUM];
    sdrv_clk_t *saf_ip[SDRV_CKGEN_SAF_IP_MAX_NUM];
    sdrv_clk_t *ap_ip[SDRV_CKGEN_AP_IP_MAX_NUM];
} sdrv_ckgen_clk_t;


status_t sdrv_ckgen_construct_clktree(sdrv_clk_t *clk_nodes, uint32_t node_num);

status_t sdrv_ckgen_dump_clktree(sdrv_clk_t *clk_node);

#endif /* SDRV_CKGEN_DUMP_H_ */

#endif
