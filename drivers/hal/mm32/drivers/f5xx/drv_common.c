/* drv_common.c */

#include "drv_common.h"
#include "drv_gpio.h"
#include <oneos_config.h>
#include <os_types.h>
#include <os_stddef.h>
#include <os_clock.h>
#include <os_memory.h>
#include "drv_common.h"
#include "board.h"
#include <drv_cfg.h>
#include <timer/hrtimer.h>
#include "board_init.h"

#ifdef OS_USING_CLOCKSOURCE
#include <timer/clocksource.h>
#include <timer/clocksource_cortexm.h>
#endif

static volatile os_bool_t hardware_init_done = OS_FALSE;

static uint32_t mult_systick2msec  = 1;
static uint32_t shift_systick2msec = 0;

int hardware_init(void)
{
    BOARD_Init();

    return 0;
}

void os_tick_handler(void)
{
    os_tick_increase();

#ifdef OS_USING_CLOCKSOURCE
    os_clocksource_update();
#endif
}

#ifdef OS_USING_SYSTICK_FOR_KERNEL_TICK
void SysTick_Handler(void)
{
    os_tick_handler();
}

static void cortexm_systick_kernel_tick_init(void)
{
    SysTick_Config(CLOCK_SYS_FREQ / OS_TICK_PER_SECOND);
    /* set pend exception priority */
    NVIC_SetPriority(PendSV_IRQn, (1 << __NVIC_PRIO_BITS) - 1);
}

#elif defined(OS_USING_SYSTICK_FOR_CLOCKEVENT)

void SysTick_Handler(void)
{
    if (hardware_init_done)
    {
        cortexm_systick_clockevent_isr();
    }
    else
    {
        os_tick_handler();
    }
}
#endif



static os_err_t os_hw_board_init()
{
    hardware_init_done = OS_FALSE;
    hardware_init();

    /* Heap initialization */
#if defined(OS_USING_HEAP)
    if ((os_size_t)HEAP_END > (os_size_t)HEAP_BEGIN)
    {
        os_default_heap_init();
        os_default_heap_add((void *)HEAP_BEGIN, (os_size_t)HEAP_END - (os_size_t)HEAP_BEGIN, OS_MEM_ALG_DEFAULT);
//        os_default_heap_add((void *)0x30010000, 0x00008000, OS_MEM_ALG_FIRSTFIT);
    }
#endif
    return OS_SUCCESS;
}
OS_INIT_CALL(os_hw_board_init, OS_INIT_LEVEL_PRE_KERNEL_1, OS_INIT_SUBLEVEL_MIDDLE);


void cortexm_systick_init(void)
{
#ifdef OS_USING_SYSTICK_FOR_KERNEL_TICK
    cortexm_systick_kernel_tick_init();
#elif defined(OS_USING_SYSTICK_FOR_CLOCKSOURCE)
    cortexm_systick_clocksource_init();
#elif defined(OS_USING_SYSTICK_FOR_CLOCKEVENT)
    cortexm_systick_clockevent_init();
#endif
}

static os_err_t board_post_init(void)
{
#ifdef OS_USING_PIN
    os_hw_pin_init();
#endif

#if defined(OS_USING_DWT_FOR_CLOCKSOURCE) && defined(DWT)
    cortexm_dwt_init();
#endif

    calc_mult_shift(&mult_systick2msec, &shift_systick2msec, OS_TICK_PER_SECOND, 1000, 1);

    cortexm_systick_init();

    hardware_init_done = OS_TRUE;

    return OS_SUCCESS;
}

OS_INIT_CALL(board_post_init, OS_INIT_LEVEL_POST_KERNEL, OS_INIT_SUBLEVEL_HIGH);
