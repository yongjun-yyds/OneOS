/* drv_common.h */

#ifndef __DRV_COMMON_H__
#define __DRV_COMMON_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus. */

#include <device.h>
#include "board_init.h"

#if defined(__CC_ARM) || defined(__CLANG_ARM)
extern int      Image$$ARM_LIB_STACK$$ZI$$Base;
extern int      Image$$ARM_LIB_STACK$$ZI$$Limit;
extern uint32_t __Vectors[];

#define CORTEX_M_VECTORS_BASE __Vectors
#define CORTEX_M_STACK_END    Image$$ARM_LIB_STACK$$ZI$$Limit
#define CORTEX_M_STACK_SIZE   ((unsigned long)&Image$$ARM_LIB_STACK$$ZI$$Limit - (unsigned long)&Image$$ARM_LIB_STACK$$ZI$$Base)
#elif defined(__ICCARM__) || defined(__ICCRX__)
#error : not support iar
#elif defined(__GNUC__)
extern uint32_t __isr_vector[];
extern int      _sstack;
extern int      _estack;

#define CORTEX_M_VECTORS_BASE __isr_vector
#define CORTEX_M_STACK_END    _estack
#define CORTEX_M_STACK_SIZE   ((unsigned long)&_estack - (unsigned long)&_sstack)
#endif

#ifdef __cplusplus
}
#endif /* __cplusplus. */

#endif /* __DRV_COMMON_H__. */
