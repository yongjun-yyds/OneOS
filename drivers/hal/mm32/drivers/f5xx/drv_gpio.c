/* drv_gpio.c */

#include "drv_gpio.h"
#include "board.h"
#include "hal_gpio.h"
#include "hal_rcc.h"

static void pin_mode(os_device_t *dev, os_base_t pin, os_base_t mode);
static int pin_read(os_device_t *dev, os_base_t pin);
static void pin_write(os_device_t *dev, os_base_t pin, os_base_t value);
static os_err_t pin_attach_irq(struct os_device *device,
                                   int32_t        pin,
                                   uint32_t       mode,
                                   void              (*hdr)(void *args),
                                   void             *args);
static os_err_t pin_detach_irq(struct os_device *device, int32_t pin);
static os_err_t pin_irq_enable(struct os_device *device, os_base_t pin, uint32_t enabled);

/* os_pin_ops. */
static const struct os_pin_ops pin_ops = 
{
    .pin_mode       = pin_mode,
    .pin_read       = pin_read,
    .pin_write      = pin_write,
    .pin_attach_irq = pin_attach_irq,
    .pin_detach_irq = pin_detach_irq,
    .pin_irq_enable = pin_irq_enable,
};

/* os_base_t mode -> GPIO_PinMode_Type GPIO_PinMode. */
static const GPIO_PinMode_Type pin_mode_shift[] = 
{
    [PIN_MODE_OUTPUT]         = GPIO_PinMode_Out_PushPull,
    [PIN_MODE_INPUT ]         = GPIO_PinMode_In_Floating,
    [PIN_MODE_INPUT_PULLUP]   = GPIO_PinMode_In_PullUp,
    [PIN_MODE_INPUT_PULLDOWN] = GPIO_PinMode_In_PullDown,
    [PIN_MODE_OUTPUT_OD]      = GPIO_PinMode_Out_OpenDrain,
    [PIN_MODE_DISABLE]        = GPIO_PinMode_In_Floating,
};

#define GET_GPIO_PORT(x) (x / 16u)
#define GET_GPIO_PIN(x)  (1u << (x % 16u))

/* gpio port table. */
static const GPIO_Type * gpio_table[] = 
{
#ifdef GPIOA
    GPIOA,
#endif /* GPIOA. */
#ifdef GPIOB
    GPIOB,
#endif /* GPIOB. */
#ifdef GPIOC
    GPIOC,
#endif /* GPIOC. */
#ifdef GPIOD
    GPIOD,
#endif /* GPIOD. */
#ifdef GPIOE
    GPIOE,
#endif /* GPIOE. */
#ifdef GPIOF
    GPIOF,
#endif /* GPIOF. */
#ifdef GPIOG
    GPIOG,
#endif /* GPIOG. */
#ifdef GPIOH
    GPIOH,
#endif /* GPIOH. */
#ifdef GPIOI
    GPIOI,
#endif /* GPIOI. */
};

/* gpio port clock table. */
static const uint32_t rcc_table_table[] = 
{
    RCC_AHB1_PERIPH_GPIOA,
    RCC_AHB1_PERIPH_GPIOB,
    RCC_AHB1_PERIPH_GPIOC,
    RCC_AHB1_PERIPH_GPIOD,
    RCC_AHB1_PERIPH_GPIOE,
    RCC_AHB1_PERIPH_GPIOF,
    RCC_AHB1_PERIPH_GPIOG,
    RCC_AHB1_PERIPH_GPIOH,
    RCC_AHB1_PERIPH_GPIOI,
};

/* pin init. */
static void pin_mode(os_device_t *dev, os_base_t pin, os_base_t mode)
{
    os_ubase_t level;
    (void)dev;

    GPIO_Init_Type gpio_init;
    gpio_init.PinMode = pin_mode_shift[mode];
    gpio_init.Pins    = GET_GPIO_PIN(pin);
    gpio_init.Speed   = GPIO_Speed_50MHz;

    os_spin_lock_irqsave(&gs_device_lock, &level);
    RCC_EnableAHB1Periphs(GET_GPIO_PORT(pin), true);
    GPIO_Init((GPIO_Type*)gpio_table[GET_GPIO_PORT(pin)], &gpio_init);
    GPIO_PinAFConf((GPIO_Type*)gpio_table[GET_GPIO_PORT(pin)], gpio_init.Pins, GPIO_AF_15);
    os_spin_unlock_irqrestore(&gs_device_lock, level);
}

/* pin read. */
static int pin_read(os_device_t *dev, os_base_t pin)
{
    if (GPIO_ReadInDataBit((GPIO_Type*)gpio_table[GET_GPIO_PORT(pin)], GET_GPIO_PIN(pin) ) )
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

/* pin write. */
static void pin_write(os_device_t *dev, os_base_t pin, os_base_t value)
{
    GPIO_WriteBit((GPIO_Type*)gpio_table[GET_GPIO_PORT(pin)], GET_GPIO_PIN(pin), (uint16_t)value);
}

/* register interrupt handler with pin. */
static os_err_t pin_attach_irq(struct os_device *device,
                                   int32_t        pin,
                                   uint32_t       mode,
                                   void              (*hdr)(void *args),
                                   void             *args)
{
    return OS_SUCCESS;
}

/* release interrupt handler with pin. */
static os_err_t pin_detach_irq(struct os_device *device, int32_t pin)
{
    return OS_SUCCESS;
}

/* enable or disable interrupt. */
static os_err_t pin_irq_enable(struct os_device *device, os_base_t pin, uint32_t enabled)
{
    return OS_SUCCESS;
}



/* pin init. */
int os_hw_pin_init(void)
{
    return os_device_pin_register(0, &pin_ops, OS_NULL);
}

/* EOF. */