/* drv_uart.c */

#include "drv_uart.h"
#include "board.h"
#include "hal_gpio.h"
#include "hal_rcc.h"

static int mm32_uart_early_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    return OS_SUCCESS;
}

OS_DRIVER_INFO mm32_uart_early_driver = {
    .name  = "UART",
    .probe = mm32_uart_early_probe,
};

OS_DRIVER_DEFINE(mm32_uart_early_driver, CORE, OS_INIT_SUBLEVEL_LOW);

/* EOF. */
