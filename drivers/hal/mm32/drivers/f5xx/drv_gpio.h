/* drv_gpio.h */

#ifndef __DRV_GPIO_H__
#define __DRV_GPIO_H__

#define GET_PIN(PORTx, PINx) ((PORTx) * 16u + PINx)

int os_hw_pin_init(void);

#endif /* __DRV_GPIO_H__ */
