#ifndef __MM32_HAL_H_
#define __MM32_HAL_H_

#include "os_types.h"
#include "board.h"


#include "hal_adc.h"
#include "hal_common.h"
#include "hal_comp.h"
#include "hal_cordic.h"
#include "hal_crc.h"
#include "hal_dac.h"
#include "hal_device_registers.h"
#include "hal_dma.h"
#include "hal_dma_request.h"
#include "hal_exti.h"
#include "hal_flexcan.h"
#include "hal_fsmc.h"
#include "hal_gpio.h"
#include "hal_i2c.h"
#include "hal_i2s.h"
#include "hal_iwdg.h"
#include "hal_lptim.h"
#include "hal_lpuart.h"
#include "hal_mds.h"
#include "hal_mds_remap.h"
#include "hal_power.h"
#include "hal_pwr.h"
#include "hal_qspi.h"
#include "hal_rcc.h"
#include "hal_rtc.h"
#include "hal_spi.h"
#include "hal_syscfg.h"
#include "hal_tim.h"
#include "hal_uart.h"
#include "hal_usb.h"
#include "hal_usb_bdt.h"
#include "hal_wwdg.h"
#include "mm32f5277e.h"
#include "mm32f5277e_features.h"


#endif /* __MM32_HAL_H_ */

