/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_common.c
 *
 * @brief       This file provides common functions.
 *
 * @revision
 * Date         Author          Notes
 * 2020-11-17   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include <os_types.h>
#include <os_memory.h>
#include <drv_common.h>
#include <string.h>
#include <os_stddef.h>
#include <os_task.h>
#include <exception.h>
#include <arch_hw.h>
#include <board.h>
#include <arch_interrupt.h>
#include <os_stddef.h>
#include <os_util.h>
#include <driver.h>

os_err_t os_hw_board_init(void)
{
    char             flag = 0;
    extern const int _system_stack;

    /* Since mips uses an empty stack, point the top of the stack to an available memory address. */
    interrupt_stack_addr = (void *)&_system_stack - sizeof(os_size_t);
    interrupt_stack_addr = (os_ubase_t *)interrupt_stack_addr;

    interrupt_stack_size = _system_stack;

    os_hw_exception_init();
    os_hw_interrupt_init();

#if defined(OS_USING_HEAP)
    os_default_heap_init();
    os_default_heap_add(OS_HW_HEAP_BEGIN, OS_HW_HEAP_END - OS_HW_HEAP_BEGIN, OS_MEM_ALG_DEFAULT);
#endif

    os_hw_timer_init();
    os_irq_enable();
    return OS_SUCCESS;
}

OS_INIT_CALL(os_hw_board_init, OS_INIT_LEVEL_PRE_KERNEL_1, OS_INIT_SUBLEVEL_MIDDLE);
