#ifndef __LOONGSON_CLOCK_H
#define __LOONGSON_CLOCK_H

unsigned long clk_get_pll_rate(void);
unsigned long clk_get_cpu_rate(void);
unsigned long clk_get_ddr_rate(void);
unsigned long clk_get_apb_rate(void);
unsigned long clk_get_dc_rate(void);
#endif

