/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        dev_usart.c
 *
 * @brief       This file implements usart devices for loongson uart
 *
 * @revision
 * Date         Author          Notes
 * 2020-11-17   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <drv_usart.h>

#ifdef BSP_USING_USART2
struct loongson_usart_info usart2_info = {.uart_base = (void *)(LS2K_UART2_BASE), .irqno = LS2K_UART2_IRQ};
OS_HAL_DEVICE_DEFINE("UART_HandleTypeDef", "uart2", usart2_info);
#endif
#ifdef BSP_USING_USART3
struct loongson_usart_info usart3_info = {.uart_base = (void *)(LS2K_UART3_BASE), .irqno = LS2K_UART3_IRQ};
OS_HAL_DEVICE_DEFINE("UART_HandleTypeDef", "uart3", usart3_info);
#endif
