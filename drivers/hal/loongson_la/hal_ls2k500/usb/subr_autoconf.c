/*	$OpenBSD: subr_autoconf.c,v 1.27 2000/04/09 22:46:03 deraadt Exp $	*/
/*	$NetBSD: subr_autoconf.c,v 1.21 1996/04/04 06:06:18 cgd Exp $	*/

/*
 * Copyright (c) 1992, 1993
 *	The Regents of the University of California.  All rights reserved.
 *
 * This software was developed by the Computer Systems Engineering group
 * at Lawrence Berkeley Laboratory under DARPA contract BG 91-66 and
 * contributed to Berkeley.
 *
 * All advertising materials mentioning features or use of this software
 * must display the following acknowledgement:
 *	This product includes software developed by the University of
 *	California, Lawrence Berkeley Laboratories.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by the University of
 *	California, Berkeley and its contributors.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * from: Header: subr_autoconf.c,v 1.12 93/02/01 19:31:48 torek Exp  (LBL)
 *
 *	@(#)subr_autoconf.c	8.1 (Berkeley) 6/10/93
 */
#include <string.h>
#include <os_memory.h>
#include <os_assert.h>
#include "la_device.h"

#define	ROOT ((struct la_device *)NULL)

struct matchinfo {
    cfmatch_t fn;
    struct	la_device *parent;
    void	*match, *aux;
    int	indirect, pri;
};

extern short cfroots[];

/* just like sprintf(buf, "%d") except that it works from the end */
char *number(register char *ep, register int n)
{
    *--ep = 0;
    while (n >= 10) {
        *--ep = (n % 10) + '0';
        n /= 10;
    }
    *--ep = n + '0';
    return (ep);
}

struct la_device *config_make_softc(struct la_device *parent, struct cfdata *cf)
{
    register struct la_device *dev;
    register struct cfdriver *cd;
    register struct cfattach *ca;
    register size_t lname, lunit;
    register char *xunit;
    char num[10];

    cd = cf->cf_driver;
    ca = cf->cf_attach;

    OS_ASSERT(ca->ca_devsize >= sizeof(struct la_device));

    /* get memory for all device vars */
    dev = (struct la_device *)os_malloc(ca->ca_devsize);

    OS_ASSERT(dev);

    memset((void *)dev,0,ca->ca_devsize);
    dev->dv_class = cd->cd_class;
    dev->dv_cfdata = cf;
    dev->dv_flags = DVF_ACTIVE;	/* always initially active */

    /* If this is a STAR device, search for a free unit number */
    if (cf->cf_fstate == FSTATE_STAR) {
        for (dev->dv_unit = cf->cf_starunit1;
                dev->dv_unit < cf->cf_unit; dev->dv_unit++)
            if (cd->cd_ndevs == 0 ||
                    cd->cd_devs[dev->dv_unit] == NULL)
                break;
    } else
        dev->dv_unit = cf->cf_unit;

    /* compute length of name and decimal expansion of unit number */
    lname = strlen(cd->cd_name);
    xunit = number(&num[sizeof num], dev->dv_unit);
    lunit = &num[sizeof num] - xunit;

    OS_ASSERT(lname + lunit < sizeof(dev->dv_xname));

    memcpy(dev->dv_xname,cd->cd_name,  lname);
    memcpy(dev->dv_xname + lname,xunit, lunit);
    dev->dv_parent = parent;

    /* put this device in the devices array */
    if (dev->dv_unit >= cd->cd_ndevs) {
        /*
         * Need to expand the array.
         */
        int old = cd->cd_ndevs, new;
        void **nsp;

        if (old == 0)
            new = 1;
        else
            new = old * 2;
        while (new <= dev->dv_unit)
            new *= 2;
        cd->cd_ndevs = new;
        nsp = os_malloc(new * sizeof(void *));
        OS_ASSERT(nsp);

        memset(nsp + old,0, (new - old) * sizeof(void *));
        if (old != 0) {
            memcpy( nsp,cd->cd_devs, old * sizeof(void *));
            os_free(cd->cd_devs);
        }
        cd->cd_devs = nsp;
    }

    OS_ASSERT(cd->cd_devs[dev->dv_unit] == 0);

    dev->dv_ref = 1;

    return (dev);
}

void mapply(register struct matchinfo *m, register struct cfdata *cf)
{
    register int pri;
    void *match;

    if (m->indirect)
        match = config_make_softc(m->parent, cf);
    else
        match = cf;

    if (m->fn != NULL)
        pri = (*m->fn)(m->parent, match, m->aux);
    else {
        OS_ASSERT(cf->cf_attach->ca_match);
        pri = (*cf->cf_attach->ca_match)(m->parent, match, m->aux);
    }

    if (pri > m->pri) {
        if (m->indirect && m->match)
            os_free(m->match);
        m->match = match;
        m->pri = pri;
    } else {
        if (m->indirect)
            os_free(match);
    }
}

void *config_rootsearch(register cfmatch_t fn, register char *rootname, register void *aux)
{
    register struct cfdata *cf;
    register short *p;
    struct matchinfo m;

    m.fn = fn;
    m.parent = ROOT;
    m.match = NULL;
    m.aux = aux;
    m.indirect = 0;
    m.pri = 0;
    /*
     * Look at root entries for matching name.  We do not bother
     * with found-state here since only one root should ever be
     * searched (and it must be done first).
     */
    for (p = cfroots; *p >= 0; p++) {
        cf = &cfdata[*p];
        if (strcmp(cf->cf_driver->cd_name, rootname) == 0)
            mapply(&m, cf);
    }
    return (m.match);
}

/*
 * Attach a found device.  Allocates memory for device variables.
 */
struct la_device *config_attach(register struct la_device *parent, void *match, register void *aux, cfprint_t print)
{
    register struct cfdata *cf;
    register struct la_device *dev;
    register struct cfdriver *cd;
    register struct cfattach *ca;

    if (parent && parent->dv_cfdata->cf_driver->cd_indirect) {
        dev = match;
        cf = dev->dv_cfdata;
    } else {
        cf = match;
        dev = config_make_softc(parent, cf);
    }

    cd = cf->cf_driver;
    ca = cf->cf_attach;

    cd->cd_devs[dev->dv_unit] = dev;

    /*
     * If this is a "STAR" device and we used the last unit, prepare for
     * another one.
     */
    if(cf->cf_fstate == FSTATE_STAR)
    {
        if (dev->dv_unit == cf->cf_unit)
            cf->cf_unit++;
    }
    else
    {
        cf->cf_fstate = FSTATE_FOUND;
    }
    (*ca->ca_attach)(parent, dev, aux);
    return (dev);
}


/*
 * Iterate over all potential children of some device, calling the given
 * function (default being the child's match function) for each one.
 * Nonzero returns are matches; the highest value returned is considered
 * the best match.  Return the `found child' if we got a match, or NULL
 * otherwise.  The `aux' pointer is simply passed on through.
 *
 * Note that this function is designed so that it can be used to apply
 * an arbitrary function to all potential children (its return value
 * can be ignored).
 */
void *config_search(cfmatch_t fn, register struct la_device *parent, void *aux)
{
    unsigned int i = 0;
    register struct cfdata *cf;
    register short *p;
    struct matchinfo m;

    m.fn = fn;
    m.parent = parent;
    m.match = NULL;
    m.aux = aux;
    m.indirect = parent && parent->dv_cfdata->cf_driver->cd_indirect;
    m.pri = 0;

    while(1)
    {
        if(cfdata[i].cf_attach == 0)
            break;
        /*
         * Skip cf if no longer eligible, otherwise scan
         * through parents for one matching `parent',
         * and try match function.
         */
        cf = &(cfdata[i]);
        i++;
        if (cf->cf_fstate == FSTATE_FOUND)
            continue;
        if (cf->cf_fstate == FSTATE_DNOTFOUND ||
                cf->cf_fstate == FSTATE_DSTAR)
            continue;
        for (p = cf->cf_parents; *p >= 0; p++)
            if (parent->dv_cfdata == &(cfdata)[*p])
                mapply(&m, cf);
    }
    return (m.match);
}

/*
 * The given `aux' argument describes a device that has been found
 * on the given parent, but not necessarily configured.  Locate the
 * configuration data for that device (using the submatch function
 * provided, or using candidates' cd_match configuration driver
 * functions) and attach it, and return true.  If the device was
 * not configured, call the given `print' function and return 0.
 */
struct la_device *config_found_sm(struct la_device *parent, void *aux, cfprint_t print, cfmatch_t submatch)
{
    void *match;

    struct la_device *dev = NULL;

    if ((match = config_search(submatch, parent, aux)) != NULL) {
        dev = config_attach(parent, match, aux, print);
        return dev;

    }
    return (NULL);
}

int la_device_init(void)
{
    struct cfdata *cf = &cfdata[0];//ohci cfdata
    struct confargs lba;

    if (cf->cf_loc[0] == -1) {
        lba.ca_baseaddr = 0;
        lba.ca_intr = 0;
        lba.ca_nintr = 0;
    } else {    
        lba.ca_baseaddr = cf->cf_loc[0];
        lba.ca_nintr= cf->cf_loc[1];
        lba.ca_intr = 0;
    }

    config_attach(ROOT,(void *)cf,(void *)&lba,NULL);

    return 0;
}

OS_INIT_CALL(la_device_init, OS_INIT_LEVEL_DEVICE, OS_INIT_SUBLEVEL_LOW);
