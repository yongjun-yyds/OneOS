/*
 * URB OHCI HCD (Host Controller Driver) for USB on the AU1x00.
 *
 * (C) Copyright 2003
 * Gary Jennejohn, DENX Software Engineering <gj@denx.de>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 *
 * Note: Part of this code has been derived from linux
 *
 * (C) Copyright 2006
 *  Imported to godson pmon by yanhua@ict.ac.cn
 */
/*
 * IMPORTANT NOTES
 * 1 - you MUST define LITTLEENDIAN in the configuration file for the
 *     board or this driver will NOT work!
 * 2 - this driver is intended for use with USB Mass Storage Devices
 *     (BBB) ONLY. There is NO support for Interrupt or Isochronous pipes!
 */

/************************************************************************

  Copyright (C)
  File name:     usb-ohci.c
Author:  ***      Version:  ***      Date: ***
Description:   This C file is the main implementation of usb ohci controller
according to the USB-OHCI Interface spec 1.0a.
If you want to understand this file well, please see the
USB spec 1.1 and USB-OHCI interface spec 1.0a carefully, you
can acquire them from www.usb.org.
Others:	The version of PMON which this C file belongs to is used on
Loongson based Platform to do the necessary initialization
and load the Linux kernel.
Function List:

Revision History:

--------------------------------------------------------------------------
Date	  Author	  Activity ID     Activity Headline
2008-03-10    QianYuli	PMON00000001    Add comment to each function
2008-02-28    QianYuli	PMON00000001    Add multi-usb-devices supports
 *************************************************************************/
#include <os_util.h>
#include <os_stddef.h>
#include <os_clock.h>
#include <os_memory.h>
#include <driver.h>
#include <timer/timer.h>
#include "cpu.h"
#include "la_device.h"
#include "usb.h"
#include "usb_ohci.h"

#define OHCI_USE_NPS		/* force NoPowerSwitching mode */
#define OHCI_VERBOSE_DEBUG	0	/* not always helpful */

#define USBH_ENABLE_BE (1 << 0)
#define USBH_ENABLE_C  (1 << 1)
#define USBH_ENABLE_E  (1 << 2)
#define USBH_ENABLE_CE (1 << 3)
#define USBH_ENABLE_RD (1 << 4)

#define USBH_ENABLE_INIT (USBH_ENABLE_CE | USBH_ENABLE_E | USBH_ENABLE_C)

/* For initializing controller (mask in an HCFS mode too) */
#define OHCI_CONTROL_INIT \
    (OHCI_CTRL_CBSR & 0x3) | OHCI_CTRL_IE | OHCI_CTRL_PLE
//      (OHCI_CTRL_CBSR & 0x3) | OHCI_CTRL_IE | OHCI_CTRL_PLE

#define min_t(type, x, y) ({ type __x = (x); type __y = (y); __x < __y ? __x: __y; })

#undef DEBUG
/* #define DEBUG */
#ifdef DEBUG
#define dbg(format, arg...) os_kprintf("DEBUG: " format "\n", ## arg)
#else
#define dbg(format, arg...) do {} while (0)
#endif /* DEBUG */
#define err(format, arg...) os_kprintf("ERROR: " format "\n", ## arg)
//#define SHOW_INFO
#ifdef SHOW_INFO
#define info(format, arg...) os_kprintf("INFO: " format "\n", ## arg)
#else
#define info(format, arg...) do {} while (0)
#endif

#define m16_swap(x) swap_16(x)
#define m32_swap(x) swap_32(x)

#undef readl
#undef writel

#define readl(addr) *(volatile unsigned int*)(addr)
#define writel(val, addr) *(volatile unsigned int*)(addr) = val

#define MAX_OHCI_C 32		/*In most case it is enough */
//#define MAX_OHCI_C 8		/* Changed for RS690 */
ohci_t *usb_ohci_dev[MAX_OHCI_C];
/* RHSC flag */
int got_rhsc;
/* device which was disconnected */
struct usb_device *devgone;

//QYL-2008-03-07
#define OHCI_MAX_USBDEVICES 8
#define OHCI_MAX_ENDPOINTS_PER_DEVICE 32
urb_priv_t ohci_urb[OHCI_MAX_USBDEVICES][OHCI_MAX_ENDPOINTS_PER_DEVICE];
extern struct usb_device usb_dev[];
extern int dev_index;

/*
 * Hook to initialize hostcontroller
 */
static int ohci_submit_bulk_msg(struct usb_device *dev, unsigned long pipe,
        void *buffer, int transfer_len);
static int ohci_submit_control_msg(struct usb_device *dev, unsigned long pipe,
        void *buffer, int transfer_len,
        struct devrequest *setup);
static int ohci_submit_int_msg(struct usb_device *dev, unsigned long pipe,
        void *buffer, int transfer_len, int interval);
//static void ohci_device_notify(struct usb_device *dev, int port);

int usb_lowlevel_init(ohci_t *ohci);

int ohci_index = 0;
int dl_ohci_kbd(void);		// deal with usb kbd
void delay_usb_ohci(int us);

int ohci_debug = 0;

struct usb_ops ohci_usb_op = {
    .submit_bulk_msg = ohci_submit_bulk_msg,
    .submit_control_msg = ohci_submit_control_msg,
    .submit_int_msg = ohci_submit_int_msg,
};

/* forward declaration */
static int hc_interrupt(void *);

//QYL-2008-03-07
void arouse_usb_int_pipe(ohci_t *);
unsigned int check_device_sequence(ohci_t *pohci);

//extern struct hostcontroller host_controller;
extern struct usb_device *usb_alloc_new_device(void *hc_private);
static int lohci_match(struct la_device *parent, void *match, void *aux)
{
    return 1;
}

static void lohci_attach(struct la_device *parent, struct la_device *self, void *aux)
{
    struct ohci *ohci = (struct ohci *)self;
    struct confargs *cf = (struct confargs *)aux;
    static int ohci_dev_index = 0;

    /* Or we just return false in the match function */
    if (ohci_dev_index >= MAX_OHCI_C) {
        os_kprintf("Exceed max controller limits\n\r");
        return;
    }

    usb_ohci_dev[ohci_dev_index++] = ohci;
    ohci_index++;
    ohci->sc_sh = CACHED_TO_UNCACHED(cf->ca_baseaddr);

    usb_lowlevel_init(ohci);
    ohci->hc.uop = &ohci_usb_op;
    /*
     * Now build the device tree
     */
    ohci->rdev = usb_alloc_new_device(ohci);

    /*do the enumeration of  the USB devices attached to the USB HUB(here root hub)
      ports. */
    usb_new_device(ohci->rdev);
}

struct cfattach lohci_ca = {
    .ca_devsize = sizeof(struct ohci),
    .ca_match = lohci_match,
    .ca_attach = lohci_attach,
};

struct cfdriver lohci_cd = {
    .cd_devs = NULL,
    .cd_name = "ohci",
    .cd_class = DV_DULL,
};

int dl_ohci_kbd(void)
{
    return 0;
}

/*This founction is to deal with problem caused by usb kbd*/
void delay_usb_ohci(int us)
{
    volatile int i, k;

    dl_ohci_kbd();
    for (k = 0; k < us; k++) {
        for (i = 0; i < 50; i++) ;
    }

}

#define NOTUSBIRQ -0x100
/* AMD-756 (D2 rev) reports corrupt register contents in some cases.
 * The erratum (#4) description is incorrect.  AMD's workaround waits
 * till some bits (mostly reserved) are clear; ok for all revs.
 */

#define OHCI_QUIRK_AMD756 0xabcd
#define read_roothub(hc, register, mask) ({ \
        unsigned int temp = readl (&hc->regs->roothub.register); \
        if (hc->flags & OHCI_QUIRK_AMD756) \
        while (temp & mask) \
        temp = readl (&hc->regs->roothub.register); \
        temp; })

static unsigned int roothub_a(struct ohci *hc)
{
    return read_roothub(hc, a, 0xfc0fe000);
}

static inline unsigned int roothub_b(struct ohci *hc)
{
    return readl(&hc->regs->roothub.b);
}

static inline unsigned int roothub_status(struct ohci *hc)
{
    return readl(&hc->regs->roothub.status);
}

static unsigned int roothub_portstatus(struct ohci *hc, int i)
{
    return read_roothub(hc, portstatus[i], 0xffe0fce0);
}

static void
td_submit_job(struct usb_device *dev, unsigned long pipe, void *buffer,
        int transfer_len, struct devrequest *setup, urb_priv_t * urb,
        int interval);

/*===========================================================================
 *
 *FUNTION: urb_free_priv
 *
 *DESCRIPTION: This function is used to free HCD-private data associated with
 *	     this URB.
 *
 *PARAMETERS:
 *	  [IN] urb: pointer to the struct urb_priv
 *
 *RETURN VALUE: None
 *
 *===========================================================================*/
static void urb_free_priv(urb_priv_t * urb)
{
    int i;
    int last;
    struct td *td;

    last = urb->length - 1;
    if (last >= 0) {
        for (i = 0; i <= last; i++) {
            td = urb->td[i];
            if (td) {
                td->usb_dev = NULL;
                td->retbuf = NULL;
                urb->td[i] = NULL;
            }
        }
    }
}

/*-------------------------------------------------------------------------*
 * Interface functions (URB)
 *-------------------------------------------------------------------------*/

/*===========================================================================
 *
 *FUNTION: sochi_submit_job
 *
 *DESCRIPTION: This function is used to get a transfer request.
 *
 *PARAMETERS:
 *	  [IN] dev:  a pointer to the USB device the TDs belongs to.
 *	  [IN] pipe:  describe the property of a pipe,more details about it,
 *		      please see the usb.h.
 *	  [IN] buffer:an all-purpose pointer to the data that would be transfered
 *		      through USB channel.
 *	  [IN] transfer_len: the length of data to be transfered.
 *	  [IN] setup: an pointer to the struct devrequest *,which is used when
 *		       the transfer type is setup of control transfer.
 *	  [IN] interval: not used here.
 *RETURN VALUE: -1 : indicates that something wrong happened during submiting job.
 *	       0 : function works ok.
 *===========================================================================*/
int sohci_submit_job(struct usb_device *dev, unsigned long pipe, void *buffer,
        int transfer_len, struct devrequest *setup, int interval)
{
    ohci_t *ohci;
    ed_t *ed;
    urb_priv_t *purb_priv;
    int i, size = 0;

    //QYL-2008-03-07
    unsigned int dev_num, ed_num;

    ohci = dev->hc_private;

    /* when controller's hung, permit only roothub cleanup attempts
     * such as powering down ports */
    if (ohci->disabled) {
        err("sohci_submit_job: EPIPE");
        return -1;
    }

    /* every endpoint has a ed, locate and fill it */
    if (!(ed = ep_add_ed(dev, pipe))) {
        err("sohci_submit_job: ENOMEM");
        return -1;
    }

    ed->usb_dev = dev;
    ed->int_interval = interval;

    /* for the private part of the URB we need the number of TDs (size) */
    switch (usb_pipetype(pipe)) {
        case PIPE_BULK:	/* one TD for every 4096 Byte */
            size = (transfer_len - 1) / 4096 + 1;
            break;
        case PIPE_CONTROL:	/* 1 TD for setup, 1 for ACK and 1 for every 4096 B */
            size = (transfer_len == 0) ? 2 : (transfer_len - 1) / 4096 + 3;
            break;
        case PIPE_INTERRUPT:
            size = (transfer_len - 1) / 4096 + 1;
            break;
        default:
            break;
    }

    if (size >= (N_URB_TD - 1)) {
        err("need %d TDs, only have %d", size, N_URB_TD);
        return -1;
    }

    for (dev_num = 0; dev_num < USB_MAX_DEVICE; dev_num++) {
        if (dev == &usb_dev[dev_num]) {
            break;
        }
    }
    ed_num = usb_pipeendpoint(pipe) | (usb_pipecontrol(pipe) ? 0: (usb_pipeout(pipe) << 4));
    purb_priv = &ohci_urb[dev_num][ed_num];

    purb_priv->pipe = pipe;
    purb_priv->trans_buffer = buffer;
    purb_priv->setup_buffer = (unsigned char *)setup;

    /* fill the private part of the URB */
    purb_priv->length = size;
    purb_priv->ed = ed;
    purb_priv->actual_length = 0;
    purb_priv->trans_length = transfer_len;

    /* allocate the TDs */
    /* note that td[0] was allocated in ep_add_ed */
    for (i = 0; i < size; i++) {
        purb_priv->td[i] = td_alloc(dev);
        if (!purb_priv->td[i]) {
            purb_priv->length = i;
            urb_free_priv(purb_priv);
            err("sohci_submit_job: ENOMEM");
            return -1;
        }
    }

    if (ed->state == ED_NEW || (ed->state & ED_DEL)) {
        urb_free_priv(purb_priv);
        err("sohci_submit_job: EINVAL");
        return -1;
    }

    /* link the ed into a chain if is not already */
    if (ed->state != ED_OPER)
        ep_link(ohci, ed);

    /* fill the TDs and link it to the ed */
    td_submit_job(dev, pipe, buffer, transfer_len, setup, purb_priv,
            interval);

    return 0;
}

/*===========================================================================
 *
 *FUNTION: usb_calc_bus_time
 *
 *DESCRIPTION: This function is used to caculate the usb bus time in bits.
 *
 *PARAMETERS:
 *	  [IN] speed: indicates the usb transfer speed(USB_SPEED_LOW&USB_SPEED_FULL).
 *	  [IN] is_input:  indicates that whether the transfer direction is input.
 *	  [IN] isoc : indicates that whether it is a isochronous transfer.
 *	  [IN] bytecount: indicates number of bytes to be transfered.
 *
 *RETURN VALUE: -1 : indicates the bogus device speed
 *	      other case,return the bits time calculated.
 *
 *===========================================================================*/
long usb_calc_bus_time(int speed, int is_input, int isoc, int bytecount)
{
    unsigned long tmp;

    switch (speed) {
        case USB_SPEED_LOW:	/* INTR only */
            if (is_input) {
                tmp =
                    (67667L * (31L + 10L * BitTime(bytecount))) / 1000L;
                return (64060L + (2 * BW_HUB_LS_SETUP) + BW_HOST_DELAY +
                        tmp);
            } else {
                tmp =
                    (66700L * (31L + 10L * BitTime(bytecount))) / 1000L;
                return (64107L + (2 * BW_HUB_LS_SETUP) + BW_HOST_DELAY +
                        tmp);
            }
        case USB_SPEED_FULL:	/* ISOC or INTR */
            if (isoc) {
                tmp =
                    (8354L * (31L + 10L * BitTime(bytecount))) / 1000L;
                return (((is_input) ? 7268L : 6265L) + BW_HOST_DELAY +
                        tmp);
            } else {
                tmp =
                    (8354L * (31L + 10L * BitTime(bytecount))) / 1000L;
                return (9107L + BW_HOST_DELAY + tmp);
            }
        default:
            os_kprintf("bogus device speed!\n\r");
            return -1;
    }
}

/*===========================================================================
 *
 *FUNTION: balance
 *
 *DESCRIPTION: This function is used to search for the least loaded schedule of
 *	     that period that has enough bandwidth left unreserved to balance
 *	     usb frame bandwidth.
 *
 *PARAMETERS:[IN] ohci: A pointer to the struct ohci which stores useful
 *		     information of the USB OHCI controller.
 *	   [IN] interval: The period between consecutive requests for data input
 *		       to a Universal Serial Bus Endpoint.
 *	   [IN] load: The bit time the TD needs.
 *
 *RETURN VALUE: value indicates the branch index.
 *
 *===========================================================================*/
static int balance(ohci_t * ohci, int interval, int load)
{
    int i, branch = -1;

    /* iso periods can be huge; iso tds specify frame numbers */
    if (interval > NUM_INTS)
        interval = NUM_INTS;

    /* search for the least loaded schedule branch of that period
     *      * that has enough bandwidth left unreserved.
     *	   */
    for (i = 0; i < interval; i++) {
        if (branch < 0 || ohci->load[branch] > ohci->load[i]) {
            int j;

            /* usb 1.1 says 90% of one frame */
            for (j = i; j < NUM_INTS; j += interval) {
                if ((ohci->load[j] + load) > 900)
                    break;
            }
            if (j < NUM_INTS)
                continue;
            branch = i;
        }
    }
    return branch;
}

/*===========================================================================
 *
 *FUNTION: periodic_link
 *
 *DESCRIPTION: This function is used to add EDs to  the periodic table .
 *
 *PARAMETERS:
 *	  [IN] ohci: a pointer to the struct ohci which stores useful information
 *		     of the USB OHCI controller.
 *	  [IN] ed :  a pointer to the stuctr ed which contains informaiton
 *		     about ed.
 *
 *RETURN VALUE: none.
 *
 *===========================================================================*/
static void periodic_link(ohci_t *ohci, ed_t *ed)
{
    unsigned i;

    for (i = ed->int_branch; i < NUM_INTS; i += ed->int_interval) {
        ed_t **prev = &ohci->periodic[i];
        volatile unsigned int *prev_p = &ohci->hcca->int_table[i];
        ed_t *here = *prev;

        /* sorting each branch by period (slow before fast)
         * lets us share the faster parts of the tree.
         * (plus maybe: put interrupt eds before iso)
         */
        while (here && ed != here) {
            if (ed->int_interval > here->int_interval)
                break;
            prev = &here->ed_next;
            prev_p = &here->hwNextED;
        }
        if (ed != here) {
            ed->ed_next = here;
            if (here)
                ed->hwNextED = *prev_p;
            *prev = ed;
            {
                *prev_p = (volatile unsigned int)vtophys(ed);
            }
        }
        ohci->load[i] += ed->int_load;
    }
}

/*===========================================================================
 *
 *FUNTION: periodic_unlink
 *
 *DESCRIPTION: This function is used to scan the periodic table to find and
 *	     unlink this ED.
 *
 *PARAMETERS:
 *	  [IN] ohci: a pointer to the struct ohci which stores useful information
 *		     of the USB OHCI controller.
 *	  [IN] edi:  a pointer to the stuctr ed which contains informaiton
 *		     about ED.
 *
 *RETURN VALUE: none.
 *
 *===========================================================================*/
static void periodic_unlink(ohci_t * ohci, ed_t * ed)
{
    int i;

    for (i = ed->int_branch; i < NUM_INTS; i += ed->int_interval) {
        ed_t *temp;
        ed_t **prev = &ohci->periodic[i];
        volatile unsigned int *prev_p = &ohci->hcca->int_table[i];

        while (*prev && (temp = *prev) != ed) {
            prev_p = &temp->hwNextED;
            prev = &temp->ed_next;
        }
        if (*prev) {
            *prev_p = ed->hwNextED;
            *prev = ed->ed_next;
        }
        ohci->load[i] -= ed->int_load;
    }

}

/*-------------------------------------------------------------------------*
 * ED handling functions
 *-------------------------------------------------------------------------*/
/*===========================================================================
 *
 *FUNTION: ep_link
 *
 *DESCRIPTION: This function is used to link an ed(endpoint descriptor) to one
 *	     of the HC chains.
 *
 *PARAMETERS:
 *	  [IN] ohci: a pointer to the struct ohci which stores useful information
 *		     of the USB OHCI controller.
 *	  [IN] edi:  a pointer to the stuctr ed which contains informaiton
 *		     about ED.
 *
 *RETURN VALUE: always returns zero.
 *
 *===========================================================================*/
static int ep_link(ohci_t *ohci, ed_t *edi)
{
    int branch = -1;
    volatile ed_t *ed = edi;

    ed->state = ED_OPER;

    switch (ed->type) {
        case PIPE_CONTROL:
            ed->hwNextED = 0;
            if (ohci->ed_controltail == NULL) {
                {
                    writel(vtophys(ed),
                            &ohci->regs->ed_controlhead);
                }
            } else {
                {
                    ohci->ed_controltail->hwNextED =
                        m32_swap(vtophys(ed));
                }
            }
            ed->ed_prev = ohci->ed_controltail;
            if (!ohci->ed_controltail && !ohci->ed_rm_list[0] && !ohci->ed_rm_list[1] && !ohci->sleeping) {
                ohci->hc_control |= OHCI_CTRL_CLE;
                writel(ohci->hc_control, &ohci->regs->control);
            }
            ohci->ed_controltail = edi;
            break;

        case PIPE_BULK:
            ed->hwNextED = 0;
            if (ohci->ed_bulktail == NULL) {
                {
                    writel((long)vtophys(ed),
                            &ohci->regs->ed_bulkhead);
                }
            } else {
                {
                    ohci->ed_bulktail->hwNextED = vtophys(ed);
                }
            }
            ed->ed_prev = ohci->ed_bulktail;
            if (!ohci->ed_bulktail && !ohci->ed_rm_list[0] && !ohci->ed_rm_list[1] && !ohci->sleeping) {
                ohci->hc_control |= OHCI_CTRL_BLE;
                writel(ohci->hc_control, &ohci->regs->control);
            }
            ohci->ed_bulktail = edi;
            break;
        case PIPE_INTERRUPT:
            ed->hwNextED = 0;
            branch = balance(ohci, ed->int_interval, ed->int_load);
            ed->int_branch = branch;
            periodic_link(ohci, (ed_t *) ed);
            break;
    }
    return 0;
}

/*===========================================================================
 *
 *FUNTION: ep_unlink
 *
 *DESCRIPTION: This function is used to unlink an ed from one of the HC chains.
 *	     Just the link to the ed is unlinked.The link from the ed still
 *	     points to the another opeartional ed or NULL,so the HC can eventually
 *	     finish the processing of the unlinked ed.
 *
 *PARAMETERS:
 *	  [IN] ohci: a pointer to the struct ohci which stores useful information
 *		     of the USB OHCI controller.
 *	  [IN] edi:  a pointer to the stuctr ed which contains informaiton
 *		     about ED.
 *
 *RETURN VALUE: always returns zero.
 *
 *===========================================================================*/
static int ep_unlink(ohci_t * ohci, ed_t * ed)
{
    ed->hwINFO |= m32_swap(OHCI_ED_SKIP);

    switch (ed->type) {
        case PIPE_CONTROL:
            if (ed->ed_prev == NULL) {
                if (!ed->hwNextED) {
                    ohci->hc_control &= ~OHCI_CTRL_CLE;
                    writel(ohci->hc_control, &ohci->regs->control);
                }
                writel(m32_swap(*((unsigned int *) & ed->hwNextED)),
                        &ohci->regs->ed_controlhead);
            } else {
                ed->ed_prev->hwNextED = ed->hwNextED;
            }
            if (ohci->ed_controltail == ed) {
                ohci->ed_controltail = ed->ed_prev;
            } else {
                {
                    ((ed_t *) PHYS_TO_CACHED(*((unsigned int *) & ed->hwNextED)))->ed_prev
                        = ed->ed_prev;
                }
            }
            break;

        case PIPE_BULK:
            if (ed->ed_prev == NULL) {
                if (!ed->hwNextED) {
                    ohci->hc_control &= ~OHCI_CTRL_BLE;
                    writel(ohci->hc_control, &ohci->regs->control);
                }
                writel(m32_swap(*((unsigned int *) & ed->hwNextED)),
                        &ohci->regs->ed_bulkhead);
            } else {
                ed->ed_prev->hwNextED = ed->hwNextED;
            }
            if (ohci->ed_bulktail == ed) {
                ohci->ed_bulktail = ed->ed_prev;
            } else {
                {
                    ((ed_t *) PHYS_TO_CACHED(ed->hwNextED))->ed_prev = ed->ed_prev;
                }
            }
            break;
        case PIPE_INTERRUPT:
            periodic_unlink(ohci, ed);
            break;
    }
    ed->state = ED_UNLINK;
    return 0;
}

static int find_index(struct ohci_device *ohci_dev, unsigned long pipe)
{
    int i;

    if (ohci_dev == NULL || pipe == 0) {
        os_kprintf("argv is valid\n\r");
        return -1;
    }

    for (i = 0; i < NUM_EDS; i++) {
        if (ohci_dev->cpu_ed[i].pipe == 0) {	// pipe will not be 0,since bit30~bit31 never be 0.
            break;
        } else {
            if (ohci_dev->cpu_ed[i].pipe == pipe)
                return i;
        }
    }
    if (i >= NUM_EDS)
        return -2;
    else
        return i;
}

/*===========================================================================
 *
 *FUNTION: ep_add_ed
 *
 *DESCRIPTION: This function is used to add/reinit an endpoint.This should be
 *	     done once at the usb_set_configuration command,but the USB stack
 *	     is a little bit stateless, so we do it at every transaction.If
 *	     the state of the ed is ED_NEW,then a dummy td is added and the
 *	     state is changed to ED_UNLINK,in all other cases the state is
 *	     left unchanged.The ed info fields are setted anyway even though
 *	     most of them should not be changed.
 *
 *PARAMETERS:
 *	  [IN] usb_dev: a pointer to the struct usb_device which contains useful
 *			information of the relevant usb device.
 *	  [IN] pipe:  describe the property of a pipe,more details about it,
 *		      please see the usb.h.
 *
 *RETURN VALUE: a pointer to struct ed.
 *
 *===========================================================================*/
static ed_t *ep_add_ed(struct usb_device *usb_dev, unsigned long pipe)
{
    td_t *td;
    ed_t *ed_ret;
    volatile ed_t *ed;

    ohci_t *ohci = usb_dev->hc_private;
    struct ohci_device *ohci_dev = ohci->ohci_dev;

    //QYL-2008-03-07
    unsigned int cpued_num = 0;

    cpued_num = find_index(ohci_dev, pipe);
    if (cpued_num < 0) {
        err("Why you need so much? No more ed left\n");
        return NULL;
    }
    ed = ed_ret = &ohci_dev->cpu_ed[cpued_num];

    if ((ed->state & ED_DEL) || (ed->state & ED_URB_DEL)) {
        err("ep_add_ed: pending delete %x/%d\n", ed->state,
                (usb_pipeendpoint(pipe) << 1) | (usb_pipecontrol(pipe) ? 0 :
                    usb_pipeout(pipe)));
        /* pending delete request */
        return NULL;
    }

    if (ed->state == ED_NEW) {
        ed->hwINFO = m32_swap(OHCI_ED_SKIP);	/* skip ed */
        /* dummy td; end of td list for ed */
        td = td_alloc(usb_dev);
        {
            ed->hwTailP = vtophys(td);
        }

        ed->hwHeadP = ed->hwTailP;
        ed->state = ED_UNLINK;
        ed->type = usb_pipetype(pipe);
        ed->pipe = pipe;
        ohci_dev->ed_cnt++;
    }

    ed->hwINFO = m32_swap(usb_pipedevice(pipe)
            | usb_pipeendpoint(pipe) << 7
            | (usb_pipeisoc(pipe) ? 0x8000 : 0)
            | (usb_pipecontrol(pipe) ? 0
                : (usb_pipeout(pipe) ? 0x800 : 0x1000))
            | usb_pipeslow(pipe) << 13 |
            usb_maxpacket(usb_dev, pipe) << 16);
    ed->oINFO = ed->hwINFO;

    if (usb_pipetype(pipe) == PIPE_INTERRUPT)
        ed->int_load =
            usb_calc_bus_time(USB_SPEED_LOW, !usb_pipeout(pipe), 0,
                    64) / 1000;
    /*FIXME*/ return ed_ret;
}

/*-------------------------------------------------------------------------*
 * TD handling functions
 *-------------------------------------------------------------------------*/
/*===========================================================================
 *
 *FUNTION: td_fill
 *
 *DESCRIPTION: This function is used to enqueue next TD for this URB(see OHCI
 *	     sepc 5.2.8.2).
 *
 *PARAMETERS:
 *	  [IN] ohci: a pointer to the struct ohci which stores useful information
 *		     of the USB OHCI controller.
 *	  [IN] info: discribe the hardware information of td(see OHCI spec 4.3.1).
 *	  [IN] data: an all-purpose pointer to the data that would be transfered
 *		     through USB channel.
 *	  [IN] dev:  a pointer to the USB device this TD belongs to.
 *	  [IN] index: the order of this TD.
 *	  [IN] urb_priv: a pointer to the struct urb_priv.
 *	  [OUT] retbuf: an all-purpose pointer to the returned data if exist.
 *
 *RETURN VALUE: none.
 *
 *===========================================================================*/
static void td_fill(ohci_t * ohci, unsigned int info,
        void *data, int len,
        struct usb_device *dev, int index, urb_priv_t * urb_priv,
        void *retbuf)
{
    volatile td_t *td, *td_pt;

    if (index > urb_priv->length) {
        err("index > length \n");
        return;
    }

    if (index != urb_priv->length - 1)
        info |= (7 << 21);

    /* use this td as the next dummy */
    {
        td_pt = (td_t *) (urb_priv->td[index]);
    }
    td_pt->hwNextTD = 0;

    /* fill the old dummy TD */
    {
        td = urb_priv->td[index] =
            (td_t *) (PHYS_TO_CACHED(urb_priv->ed->hwTailP) & ~0xf);
    }

    td->ed = urb_priv->ed;
    td->next_dl_td = NULL;
    td->index = index;
    td->data = (unsigned long)(data);
    td->transfer_len = len;	//for debug purpose
    td->retbuf = retbuf;
    if (!len)
        data = 0;

    td->hwINFO = m32_swap(info);

    if (len == 0)
        td->hwCBP = 0;	//take special care
    else {
        {
            td->hwCBP = vtophys(data);
        }
    }

    if (data) {
        {
            td->hwBE = vtophys(data + len - 1);
        }
    } else
        td->hwBE = 0;

#ifdef DEBUG
    os_kprintf("td_fill: td=%x\n\r", td);
    os_kprintf("hwINFO =%x, hwCBP=%x, hwNextTD=%x, hwBE=%x\n\r",
            td->hwINFO, td->hwCBP, td->hwNextTD, td->hwBE);
#endif

    {
        td->hwNextTD = m32_swap(vtophys(td_pt));
    }
    td->hwPSW[0] = (unsigned short)(((unsigned long)data & 0x0FFF) | 0xE000);
    td->ed->hwTailP = td->hwNextTD;
}

/*===========================================================================
 *
 *FUNTION: td_submit_job
 *
 *DESCRIPTION: This function is used to prepare all TDs of a transfer.
 *
 *PARAMETERS:
 *	  [IN] dev:  a pointer to the USB device the TDs belongs to.
 *	  [IN] pipe:  describe the property of a pipe,more details about it,
 *		      please see the usb.h.
 *	  [IN] buffer:an all-purpose pointer to the data that would be transfered
 *		      through USB channel.
 *	  [IN] transfer_len: the length of data to be transfered.
 *	  [IN] setup: an pointer to the struct devrequest *,which is used when
 *		       the transfer type is setup of control transfer.
 *	  [IN] urb: a pointer to the struct urb_priv.
 *	  [IN] interval: not used here.
 *
 *RETURN VALUE: none.
 *
 *===========================================================================*/
static void td_submit_job(struct usb_device *dev, unsigned long pipe, void
        *buffer, int transfer_len, struct devrequest *setup,
        urb_priv_t *urb, int interval)
{
    ohci_t *ohci = dev->hc_private;
    int data_len = transfer_len;
    void *data;
    int cnt = 0;
    unsigned int info = 0;
    int periodic = 0;
    unsigned int toggle = 0;
    unsigned int dev_num;

    for (dev_num = 0; dev_num < USB_MAX_DEVICE; dev_num++) {
        if (dev == &usb_dev[dev_num]) {
            break;
        }
    }

    /* OHCI handles the DATA-toggles itself, we just use the USB-toggle bits for reseting */

    if (usb_gettoggle(dev, usb_pipeendpoint(pipe), usb_pipeout(pipe))) {
        toggle = TD_T_TOGGLE;
    } else {
        toggle = TD_T_DATA0;
        usb_settoggle(dev, usb_pipeendpoint(pipe), usb_pipeout(pipe),
                1);
    }
    urb->td_cnt = 0;
    if (data_len) {
        {
            data = buffer;	//XXX
        }
    } else {
        data = 0;
    }

    switch (usb_pipetype(pipe)) {
        case PIPE_INTERRUPT:
            info = usb_pipeout(urb->pipe) ?
                TD_CC | TD_DP_OUT | toggle : TD_CC | TD_R | TD_DP_IN |
                toggle;
            {
                td_fill(ohci, info, data, data_len, dev, cnt++, urb,
                        NULL);
            }
            periodic = 1;
            break;
        case PIPE_BULK:
            info = usb_pipeout(pipe) ? TD_CC | TD_DP_OUT : TD_CC | TD_DP_IN;
            while (data_len > 4096) {
                td_fill(ohci, info | (cnt ? TD_T_TOGGLE : toggle), data,
                        4096, dev, cnt, urb, NULL);
                data += 4096;
                data_len -= 4096;
                cnt++;
            }
            info = usb_pipeout(pipe) ?
                TD_CC | TD_DP_OUT : TD_CC | TD_R | TD_DP_IN;
            td_fill(ohci, info | (cnt ? TD_T_TOGGLE : toggle), data,
                    data_len, dev, cnt, urb, NULL);
            cnt++;

            if (!ohci->sleeping)
                writel(OHCI_BLF, &ohci->regs->cmdstatus);	/* start bulk list */
            break;

        case PIPE_CONTROL:
            info = TD_CC | TD_DP_SETUP | TD_T_DATA0;
            memcpy(ohci->setup, setup, 8);
            td_fill(ohci, info, (void *)ohci->setup, 8, dev, cnt++, urb,
                    NULL);
            if (data_len > 0) {
                info = usb_pipeout(pipe) ?
                    TD_CC | TD_R | TD_DP_OUT | TD_T_DATA1 : TD_CC | TD_R
                    | TD_DP_IN | TD_T_DATA1;
                /* NOTE:  mishandles transfers >8K, some >4K */
                if (usb_pipeout(pipe)) {
                    memcpy(ohci->control_buf, data, data_len);
                    td_fill(ohci, info, ohci->control_buf, data_len,
                            dev, cnt++, urb, NULL);
                } else {
                    td_fill(ohci, info, ohci->control_buf, data_len,
                            dev, cnt++, urb, buffer);
                }
            }
            info = (usb_pipeout(pipe) || data_len == 0) ?
                TD_CC | TD_DP_IN | TD_T_DATA1 : TD_CC | TD_DP_OUT |
                TD_T_DATA1;
            td_fill(ohci, info, data, 0, dev, cnt++, urb, NULL);
            if (!ohci->sleeping)
                writel(OHCI_CLF, &ohci->regs->cmdstatus);	/* start Control list */
            break;
    }

    if (periodic) {
        ohci->hc_control |= OHCI_CTRL_PLE | OHCI_CTRL_IE;
        writel(ohci->hc_control, &ohci->regs->control);
    }

    if (urb->length != cnt)
        dbg("TD LENGTH %d != CNT %d", urb->length, cnt);
}

/*-------------------------------------------------------------------------*
 * Done List handling functions
 *-------------------------------------------------------------------------*/
/*===========================================================================
 *
 *FUNTION: dl_transfer_length
 *
 *DESCRIPTION: This function is used to calculate the transfer length and update
 *	     the urb.
 *
 *PARAMETERS:
 *	  [IN] td:  a pointer to the stuct td which contains informaiton
 *		     about TD.
 *
 *RETURN VALUE: none.
 *
 *===========================================================================*/
static void dl_transfer_length(td_t * td)
{
    unsigned int tdBE;
    unsigned int tdCBP;
    urb_priv_t *lurb_priv = NULL;
    int length = 0;

    //QYL-2008-03-07
    unsigned int dev_num, ed_num;
    struct usb_device *p_dev = NULL;
    ed_t *p_ed = NULL;

    tdBE = m32_swap(td->hwBE);
    tdCBP = PHYS_TO_CACHED(m32_swap(td->hwCBP));

    //QYL-2008-03-07
    if (td != NULL) {
        p_dev = td->usb_dev;
        for (dev_num = 0; dev_num < USB_MAX_DEVICE; dev_num++) {
            if (p_dev == &usb_dev[dev_num]) {
                break;
            }
        }
        p_ed = td->ed;
        ed_num = (p_ed->hwINFO & 0xf80) >> 7;
        lurb_priv = &ohci_urb[dev_num][ed_num];
    }

    if (!(usb_pipetype(lurb_priv->pipe) == PIPE_CONTROL &&
                ((td->index == 0) || (td->index == lurb_priv->length - 1)))) {
        if (tdBE != 0) {
            if (td->hwCBP == 0) {
                length =
                    PHYS_TO_CACHED(tdBE) - (td->data) +
                    1;
                lurb_priv->actual_length += length;
                if (usb_pipecontrol(lurb_priv->pipe)
                        && usb_pipein(lurb_priv->pipe)) {
                    {
                        memcpy((void *)td->retbuf,
                                (void *)td->data,
                                length);
                    }
                }

            } else {
                {
                    length = tdCBP - (td->data);
                }
                lurb_priv->actual_length += length;
                if (usb_pipein(lurb_priv->pipe)
                        && usb_pipecontrol(lurb_priv->pipe)) {
                    memcpy(td->retbuf, (void *)td->data,
                            length);
                }
            }
        }
    }

}

/*===========================================================================
 *
 *FUNTION: dl_reverse_done_list
 *
 *DESCRIPTION: This function is used to reverse the reversed done-list,for replies
 *	     to the request have to be on a FIFO basis.
 *
 *PARAMETERS:
 *	  [IN] ohci: a pointer to the struct ohci which stores useful information
 *		     of the USB OHCI controller.
 *
 *RETURN VALUE: a pointer to the struct td,which is the head of the td done-list
 *	      chain.
 *
 *===========================================================================*/
static td_t *dl_reverse_done_list(ohci_t * ohci)
{
    unsigned int td_list_hc;
    td_t *td_rev = NULL;
    td_t *td_list = NULL;
    urb_priv_t *lurb_priv = NULL;

    //QYL-2008-03-07
    unsigned int dev_num, ed_num;
    struct usb_device *p_dev = NULL;
    ed_t *p_ed = NULL;

    td_list_hc = ohci->hcca->done_head & 0xfffffff0;
    ohci->hcca->done_head = 0;

    while (td_list_hc) {

        {
            td_list =
                (td_t *) PHYS_TO_CACHED(td_list_hc & 0x1fffffff);
        }
        td_list->hwINFO |= TD_DEL;

        if (TD_CC_GET(m32_swap(td_list->hwINFO))) {
            p_dev = td_list->usb_dev;
            for (dev_num = 0; dev_num < USB_MAX_DEVICE; dev_num++) {
                if (p_dev == &usb_dev[dev_num]) {
                    break;
                }
            }
            p_ed = td_list->ed;
            ed_num = (p_ed->hwINFO & 0xf80) >> 7;
            lurb_priv = &ohci_urb[dev_num][ed_num];

            if (td_list->ed->hwHeadP & m32_swap(0x1)) {	//ED halted
                if (lurb_priv
                        && ((td_list->index + 1) <
                            lurb_priv->length)) {
                    td_list->ed->hwHeadP =
                        (lurb_priv->td[lurb_priv->length -
                         1]->hwNextTD &
                         m32_swap(0xfffffff0)) | (td_list->
                         ed->hwHeadP
                         &
                         m32_swap
                         (0x2));
                    lurb_priv->td_cnt +=
                        lurb_priv->length - td_list->index -
                        1;
                } else
                    td_list->ed->hwHeadP &=
                        m32_swap(0xfffffff2);
            }
        }

        td_list->next_dl_td = td_rev;
        td_rev = td_list;
        td_list_hc = m32_swap(td_list->hwNextTD) & 0xfffffff0;
    }
    return td_list;
}


int process_interrupt_urb(ohci_t *ohci)
{
    int i;
    int ed_num;
    for (i = 0; i < MAX_INTS; i++) {
        struct usb_device *pInt_dev = NULL;
        urb_priv_t *pInt_urb_priv = NULL;
        ed_t *pInt_ed = NULL;
        int ret = 1;

        pInt_dev = ohci->g_pInt_dev[i];
        pInt_urb_priv = ohci->g_pInt_urb_priv[i];
        pInt_ed = ohci->g_pInt_ed[i];

        if (pInt_dev == NULL || pInt_urb_priv == NULL
                || pInt_ed == NULL)
            continue;

        ed_num = usb_pipeendpoint(pInt_urb_priv->pipe) |(usb_pipecontrol(pInt_urb_priv->pipe) ? 0: (usb_pipeout(pInt_urb_priv->pipe)<<4));
        pInt_dev->irq_status = 0;
        pInt_dev->irq_act_len = pInt_urb_priv->actual_length;
        if (pInt_dev->irq_handle_ep[ed_num]) {
            pInt_dev->irq_handle_ep[ed_num](pInt_dev);
            if (!pInt_dev->irq_handle_ep[ed_num])
                ret = 0;
        } else if (pInt_dev->irq_handle) {
            pInt_dev->irq_handle(pInt_dev);
            if (!pInt_dev->irq_handle)
                ret = 0;
        }

        pInt_urb_priv->actual_length = 0;
        pInt_dev->irq_act_len = 0;
        pInt_ed->hwINFO = pInt_ed->oINFO;

        if (ret == 1) {
            ep_link(ohci, pInt_ed);
            td_submit_job(pInt_ed->usb_dev, pInt_urb_priv->pipe,
                    pInt_urb_priv->trans_buffer,
                    pInt_urb_priv->trans_length,
                    (struct devrequest *)pInt_urb_priv->setup_buffer,
                    pInt_urb_priv, pInt_ed->int_interval);
        }
        else {

            urb_free_priv(pInt_urb_priv);
        }

        ohci->g_pInt_dev[i] = NULL;
        ohci->g_pInt_urb_priv[i] = NULL;
        ohci->g_pInt_ed[i] = NULL;

    }
    return 0;
}

/*===========================================================================
 *
 *FUNTION: dl_done_list
 *
 *DESCRIPTION: This function is used to deal with the td done list,and if the
 *	     pipe type is PIPE_INTERRUPT,here it needs to submit a new job
 *	     for PIPE_INTERRUPT,in fact,this feature is used for USB keypad.
 *
 *PARAMETERS:
 *	  [IN] ohci: a pointer to the struct ohci which stores useful information
 *		     of the USB OHCI controller.
 *	  [IN] td_list: a pointer to the struct td,which is the head of the td done-list
 *			chain.
 *
 *RETURN VALUE: zero :it is ok.
 *	      not zero:something wrong.
 *
 *===========================================================================*/
static int dl_done_list(ohci_t *ohci, td_t *td_list)
{
    td_t *td_list_next = NULL;
    ed_t *ed;
    int cc = 0;
    int stat = 0;
    int i;
    urb_priv_t *lurb_priv = NULL;
    unsigned int tdINFO, edHeadP, edTailP;
    unsigned int dev_num, ed_num;
    struct usb_device *p_dev = NULL;
    ed_t *p_ed = NULL;

    //QYL-2008-03-07
    urb_priv_t *pInt_urb_priv = NULL;
    struct usb_device *pInt_dev = NULL;
    ed_t *pInt_ed = NULL;

    while (td_list) {

        td_list_next = td_list->next_dl_td;

        //QYL-2008-03-07
        p_dev = td_list->usb_dev;
        for (dev_num = 0; dev_num < USB_MAX_DEVICE; dev_num++) {
            if (p_dev == &usb_dev[dev_num]) {
                break;
            }
        }
        p_ed = td_list->ed;
        ed_num = (p_ed->hwINFO & 0xf80) >> 7;
        lurb_priv = &ohci_urb[dev_num][ed_num];

        //QYL-2008-03-07
        if (p_ed->type == PIPE_INTERRUPT || p_ed->usb_dev->irq_handle_ep[ed_num]) {
            pInt_ed = p_ed;
            pInt_urb_priv = lurb_priv;
            pInt_dev = p_ed->usb_dev;
        }
        if (p_ed->type == PIPE_BULK) {
            //bPipeBulk = OS_TRUE;
        }

        tdINFO = m32_swap(td_list->hwINFO);

        ed = td_list->ed;
        //dev = ed->usb_dev;

        dl_transfer_length(td_list);

        /* error code of transfer */
        cc = TD_CC_GET(tdINFO);
        if (cc != 0) {
            stat = cc_to_error[cc];
        }

        if (ed->state != ED_NEW) {
            edHeadP = m32_swap(ed->hwHeadP) & 0xfffffff0;
            edTailP = m32_swap(ed->hwTailP);

            /* unlink eds if they are not busy */
            if ((edHeadP == edTailP) && (ed->state == ED_OPER)) {
                ep_unlink(ohci, ed);
            }
        }
        lurb_priv->state = stat;

        td_list = td_list_next;
        if (NULL != pInt_urb_priv) {	/* FIXME */
            //int i;
            for (i = 0; i < MAX_INTS; i++) {
                if (ohci->g_pInt_urb_priv[i] == pInt_urb_priv)
                    break;
                if (ohci->g_pInt_dev[i] == NULL) {
                    ohci->g_pInt_dev[i] = pInt_dev;
                    ohci->g_pInt_ed[i] = pInt_ed;
                    ohci->g_pInt_urb_priv[i] = pInt_urb_priv;
                    break;
                }
            }
            pInt_urb_priv = NULL;
        }
    }
    return stat;
}
/*-------------------------------------------------------------------------*
 * Virtual Root Hub
 *-------------------------------------------------------------------------*/
/* Device descriptor */
static unsigned char root_hub_dev_des[] = {
    0x12,			/*  __u8  bLength; */
    0x01,			/*  __u8  bDescriptorType; Device */
    0x10,			/*  __u16 bcdUSB; v1.1 */
    0x01,
    0x09,			/*  __u8  bDeviceClass; HUB_CLASSCODE */
    0x00,			/*  __u8  bDeviceSubClass; */
    0x00,			/*  __u8  bDeviceProtocol; */
    0x08,			/*  __u8  bMaxPacketSize0; 8 Bytes */
    0x00,			/*  __u16 idVendor; */
    0x00,
    0x00,			/*  __u16 idProduct; */
    0x00,
    0x00,			/*  __u16 bcdDevice; */
    0x00,
    0x00,			/*  __u8  iManufacturer; */
    0x01,			/*  __u8  iProduct; */
    0x00,			/*  __u8  iSerialNumber; */
    0x01			/*  __u8  bNumConfigurations; */
};

/* Configuration descriptor */
static unsigned char root_hub_config_des[] = {
    0x09,			/*  __u8  bLength; */
    0x02,			/*  __u8  bDescriptorType; Configuration */
    0x19,			/*  __u16 wTotalLength; */
    0x00,
    0x01,			/*  __u8  bNumInterfaces; */
    0x01,			/*  __u8  bConfigurationValue; */
    0x00,			/*  __u8  iConfiguration; */
    0x40,			/*  __u8  bmAttributes;
                        Bit 7: Bus-powered, 6: Self-powered, 5 Remote-wakwup, 4..0: resvd */
    0x00,			/*  __u8  MaxPower; */

    /* interface */
    0x09,			/*  __u8  if_bLength; */
    0x04,			/*  __u8  if_bDescriptorType; Interface */
    0x00,			/*  __u8  if_bInterfaceNumber; */
    0x00,			/*  __u8  if_bAlternateSetting; */
    0x01,			/*  __u8  if_bNumEndpoints; */
    0x09,			/*  __u8  if_bInterfaceClass; HUB_CLASSCODE */
    0x00,			/*  __u8  if_bInterfaceSubClass; */
    0x00,			/*  __u8  if_bInterfaceProtocol; */
    0x00,			/*  __u8  if_iInterface; */

    /* endpoint */
    0x07,			/*  __u8  ep_bLength; */
    0x05,			/*  __u8  ep_bDescriptorType; Endpoint */
    0x81,			/*  __u8  ep_bEndpointAddress; IN Endpoint 1 */
    0x03,			/*  __u8  ep_bmAttributes; Interrupt */
    0x02,			/*  __u16 ep_wMaxPacketSize; ((MAX_ROOT_PORTS + 1) / 8 */
    0x00,
    0xff			/*  __u8  ep_bInterval; 255 ms */
};

static unsigned char root_hub_str_index0[] = {
    0x04,			/*  __u8  bLength; */
    0x03,			/*  __u8  bDescriptorType; String-descriptor */
    0x09,			/*  __u8  lang ID */
    0x04,			/*  __u8  lang ID */
};

static unsigned char root_hub_str_index1[] = {
    28,			/*  __u8  bLength; */
    0x03,			/*  __u8  bDescriptorType; String-descriptor */
    'O',			/*  __u8  Unicode */
    0,			/*  __u8  Unicode */
    'H',			/*  __u8  Unicode */
    0,			/*  __u8  Unicode */
    'C',			/*  __u8  Unicode */
    0,			/*  __u8  Unicode */
    'I',			/*  __u8  Unicode */
    0,			/*  __u8  Unicode */
    ' ',			/*  __u8  Unicode */
    0,			/*  __u8  Unicode */
    'R',			/*  __u8  Unicode */
    0,			/*  __u8  Unicode */
    'o',			/*  __u8  Unicode */
    0,			/*  __u8  Unicode */
    'o',			/*  __u8  Unicode */
    0,			/*  __u8  Unicode */
    't',			/*  __u8  Unicode */
    0,			/*  __u8  Unicode */
    ' ',			/*  __u8  Unicode */
    0,			/*  __u8  Unicode */
    'H',			/*  __u8  Unicode */
    0,			/*  __u8  Unicode */
    'u',			/*  __u8  Unicode */
    0,			/*  __u8  Unicode */
    'b',			/*  __u8  Unicode */
    0,			/*  __u8  Unicode */
};

/* Hub class-specific descriptor is constructed dynamically */
/*-------------------------------------------------------------------------*/

#define OK(x)			len = (x); break
#ifdef DEBUG
#define WR_RH_STAT(x)		{info("addr %llx WR:status %#8x",&gohci->regs->roothub.status,(x));writel((x), &gohci->regs->roothub.status);}
#define WR_RH_PORTSTAT(x)	{info("WR:portstatus[%d] %#8x", wIndex-1, (x));writel((x), &gohci->regs->roothub.portstatus[wIndex-1]);}
#else
#define WR_RH_STAT(x)		writel((x), &gohci->regs->roothub.status)
#define WR_RH_PORTSTAT(x)	writel((x), &gohci->regs->roothub.portstatus[wIndex-1])
#endif
#define RD_RH_STAT		roothub_status(gohci)
#define RD_RH_PORTSTAT		roothub_portstatus(gohci,wIndex-1)

/*===========================================================================
 *
 *FUNTION: rh_check_port_status
 *
 *DESCRIPTION: This function is used to answer the request to the virtual root
 *	     hub,checking whether a device disconnected just now.
 *
 *PARAMETERS:
 *	  [IN] controller: a pointer to the struct ohci which stores useful
 *			   information of the USB OHCI controller.
 *
 *RETURN VALUE: zero :nothing happened.
 *	      > zero:some device disconnected.
 *
 *===========================================================================*/
int rh_check_port_status(ohci_t * controller)
{
    unsigned int temp, ndp, i;
    int res;

    res = -1;
    temp = roothub_a(controller);
    ndp = (temp & RH_A_NDP);
    for (i = 0; i < ndp; i++) {
        temp = roothub_portstatus(controller, i);
        /* check for a device disconnect */
        if (((temp & (RH_PS_PESC | RH_PS_CSC)) ==
                    (RH_PS_PESC | RH_PS_CSC)) && ((temp & RH_PS_CCS) == 0)) {
            res = i;
            break;
        }
    }
    return res;
}

/*===========================================================================
 *
 *FUNTION: ohci_submit_rh_msg
 *
 *DESCRIPTION: This function is used to submit a message to virtual root hub
 *		in fact,for this root hub is virtual,so it will return data(
 *		if exists) immediatelly.
 *
 *PARAMETERS:
 *	[IN] dev:  a pointer to the USB device( here root hub,a special USB
 *		 device) the message belongs to.
 *	[IN] pipe:  describe the property of a pipe,more details about it,
 *		please see the usb.h.
 *	[IN] buffer:an all-purpose pointer to the data that would be returned
 *		from virtual root hub.
 *	[IN] transfer_len: the length of data to be transfered.
 *	[IN] cmd: an pointer to the struct devrequest *,which is used when
 *		the transfer type is setup of control transfer.
 *
 *RETURN VALUE: zero :indicates that root hub has not implemented PIPE_INTERRUPT.
 *			not zero:indicates the status of this transfer.
 *
 *===========================================================================*/
static int ohci_submit_rh_msg(struct usb_device *dev, unsigned long pipe,
        void *buffer, int transfer_len,
        struct devrequest *cmd)
{
    void *data = buffer;
    int leni = transfer_len;
    int len = 0;
    int stat = 0;
    unsigned int datab[4];
    unsigned char *data_buf = (unsigned char *) datab;
    unsigned short bmRType_bReq;
    unsigned short wValue;
    unsigned short wIndex;
    unsigned short wLength;
    struct ohci *gohci = dev->hc_private;

    wait_ms(1);

    if (usb_pipeint(pipe)) {
        info("Root-Hub submit IRQ: NOT implemented");
        return 0;
    }

    bmRType_bReq = cmd->requesttype | (cmd->request << 8);
    wValue = m16_swap(cmd->value);
    wIndex = m16_swap(cmd->index);
    wLength = m16_swap(cmd->length);

    /*
       info("Root-Hub: adr: %2x cmd(%1x): %08x %04x %04x %04x",
       dev->devnum, 8, bmRType_bReq, wValue, wIndex, wLength);
       */

    switch (bmRType_bReq) {
        /* Request Destination:
           without flags: Device,
RH_INTERFACE: interface,
RH_ENDPOINT: endpoint,
RH_CLASS means HUB here,
RH_OTHER | RH_CLASS  almost ever means HUB_PORT here
*/

        case RH_GET_STATUS:
            *(unsigned short *) data_buf = m16_swap(1);
            OK(2);
        case RH_GET_STATUS | RH_INTERFACE:
            *(unsigned short *) data_buf = m16_swap(0);
            OK(2);
        case RH_GET_STATUS | RH_ENDPOINT:
            *(unsigned short *) data_buf = m16_swap(0);
            OK(2);
        case RH_GET_STATUS | RH_CLASS:
            *(unsigned int *) data_buf =
                m32_swap(RD_RH_STAT & ~(RH_HS_CRWE | RH_HS_DRWE));
            OK(4);
        case RH_GET_STATUS | RH_OTHER | RH_CLASS:
            *(unsigned int *) data_buf = m32_swap(RD_RH_PORTSTAT);
            OK(4);

        case RH_CLEAR_FEATURE | RH_ENDPOINT:
            switch (wValue) {
                case (RH_ENDPOINT_STALL):
                    OK(0);
            }
            break;

        case RH_CLEAR_FEATURE | RH_CLASS:
            switch (wValue) {
                case RH_C_HUB_LOCAL_POWER:
                    OK(0);
                case (RH_C_HUB_OVER_CURRENT):
                    WR_RH_STAT(RH_HS_OCIC);
                    OK(0);
            }
            break;

        case RH_CLEAR_FEATURE | RH_OTHER | RH_CLASS:
            switch (wValue) {
                case (RH_PORT_ENABLE):
                    WR_RH_PORTSTAT(RH_PS_CCS);
                    OK(0);
                case (RH_PORT_SUSPEND):
                    WR_RH_PORTSTAT(RH_PS_POCI);
                    OK(0);
                case (RH_PORT_POWER):
                    WR_RH_PORTSTAT(RH_PS_LSDA);
                    OK(0);
                case (RH_C_PORT_CONNECTION):
                    WR_RH_PORTSTAT(RH_PS_CSC);
                    OK(0);
                case (RH_C_PORT_ENABLE):
                    WR_RH_PORTSTAT(RH_PS_PESC);
                    OK(0);
                case (RH_C_PORT_SUSPEND):
                    WR_RH_PORTSTAT(RH_PS_PSSC);
                    OK(0);
                case (RH_C_PORT_OVER_CURRENT):
                    WR_RH_PORTSTAT(RH_PS_OCIC);
                    OK(0);
                case (RH_C_PORT_RESET):
                    WR_RH_PORTSTAT(RH_PS_PRSC);
                    OK(0);
            }
            break;

        case RH_SET_FEATURE | RH_OTHER | RH_CLASS:
            switch (wValue) {
                case (RH_PORT_SUSPEND):
                    WR_RH_PORTSTAT(RH_PS_PSS);
                    OK(0);
                case (RH_PORT_RESET):	/* BUG IN HUP CODE ******** */
                    if (RD_RH_PORTSTAT & RH_PS_CCS)
                        WR_RH_PORTSTAT(RH_PS_PRS);
                    OK(0);
                case (RH_PORT_POWER):
                    WR_RH_PORTSTAT(RH_PS_PPS);
                    OK(0);
                case (RH_PORT_ENABLE):	/* BUG IN HUP CODE ******** */
                    if (RD_RH_PORTSTAT & RH_PS_CCS)
                        WR_RH_PORTSTAT(RH_PS_PES);
                    OK(0);
            }
            break;

        case RH_SET_ADDRESS:
            gohci->rh.devnum = wValue;
            OK(0);

        case RH_GET_DESCRIPTOR:
            switch ((wValue & 0xff00) >> 8) {
                case (0x01):	/* device descriptor */
                    len = min_t(unsigned int,
                            leni,
                            min_t(unsigned int,
                                sizeof(root_hub_dev_des), wLength));
                    data_buf = root_hub_dev_des;
                    OK(len);
                case (0x02):	/* configuration descriptor */
                    len = min_t(unsigned int,
                            leni,
                            min_t(unsigned int,
                                sizeof(root_hub_config_des),
                                wLength));
                    data_buf = root_hub_config_des;
                    OK(len);
                case (0x03):	/* string descriptors */
                    if (wValue == 0x0300) {
                        len = min_t(unsigned int,
                                leni,
                                min_t(unsigned int,
                                    sizeof(root_hub_str_index0),
                                    wLength));
                        data_buf = root_hub_str_index0;
                        OK(len);
                    }
                    if (wValue == 0x0301) {
                        len = min_t(unsigned int,
                                leni,
                                min_t(unsigned int,
                                    sizeof(root_hub_str_index1),
                                    wLength));
                        data_buf = root_hub_str_index1;
                        OK(len);
                    }
                default:
                    stat = USB_ST_STALLED;
            }
            break;

        case RH_GET_DESCRIPTOR | RH_CLASS:
            {
                unsigned int temp = roothub_a(gohci);

                data_buf[0] = 9;	/* min length; */
                data_buf[1] = 0x29;
                data_buf[2] = temp & RH_A_NDP;
                data_buf[3] = 0;
                if (temp & RH_A_PSM)	/* per-port power switching? */
                    data_buf[3] |= 0x1;
                if (temp & RH_A_NOCP)	/* no overcurrent reporting? */
                    data_buf[3] |= 0x10;
                else if (temp & RH_A_OCPM)	/* per-port overcurrent reporting? */
                    data_buf[3] |= 0x8;

                /* corresponds to data_buf[4-7] */
                datab[1] = 0;
                data_buf[5] = (temp & RH_A_POTPGT) >> 24;
                temp = roothub_b(gohci);
                data_buf[7] = temp & RH_B_DR;
                if (data_buf[2] < 7) {
                    data_buf[8] = 0xff;
                } else {
                    data_buf[0] += 2;
                    data_buf[8] = (temp & RH_B_DR) >> 8;
                    data_buf[10] = data_buf[9] = 0xff;
                }

                len = min_t(unsigned int, leni,
                        min_t(unsigned int, data_buf[0], wLength));
                OK(len);
            }

        case RH_GET_CONFIGURATION:
            *(unsigned char *) data_buf = 0x01;
            OK(1);

        case RH_SET_CONFIGURATION:
            WR_RH_STAT(0x10000);
            OK(0);

        default:
            dbg("unsupported root hub command");
            stat = USB_ST_STALLED;
    }

#ifdef	DEBUG
    //ohci_dump_roothub (gohci, 1);
#else
    wait_ms(1);
#endif

    len = min_t(int, len, leni);
    if (data != data_buf)
        memcpy(data, data_buf, len);
    dev->act_len = len;
    dev->status = stat;
    wait_ms(1);
    return stat;
}

/*===========================================================================
 *
 *FUNTION: reset_controller
 *
 *DESCRIPTION: This function is used to reset the USB OHCI controller.
 *
 *PARAMETERS:
 *	  [IN] hc_data: an all-purpose pointer to be cast to struct ohci*,
 *			points to the buffer which stores information of ohci.
 *
 *RETURN VALUE: none
 *
 *===========================================================================*/
void reset_controller(void *hc_data)
{
    ohci_t *ohci = hc_data;
    memset(&ohci->rh, 0, sizeof(ohci->rh));
}

/*===========================================================================
 *
 *FUNTION: submit_common_msg
 *
 *DESCRIPTION: This function is the common code for handling submit messages,which
 *	     are used for all but root hub accesses.
 *
 *PARAMETERS:
 *	  [IN] dev:  a pointer to the USB device the messages belongs to.
 *	  [IN] pipe:  describe the property of a pipe,more details about it,
 *		      please see the usb.h.
 *	  [IN] buffer:an all-purpose pointer to the data that would be transfered
 *		      through USB channel.
 *	  [IN] transfer_len: the length of data to be transfered.
 *	  [IN] setup: an pointer to the struct devrequest *,which is used when
 *		       the transfer type is setup of control transfer.
 *	  [IN] interval: not used here.
 *
 *RETURN VALUE: 0 : indicates that device has been pulled or function return ok.
 *	      -1: indicates that pipesize is zero or sohci_submit_job failed.
 *
 *note:      unlike ohci_submit_rh_msg(),this function would access the real
 *	   OHCI controller hardware,so it needs to wait to check whether hardware
 *	   has fininshed transfering TDs in limited timeouts by calling hc_check_
 *	   ohci_controller().
 *
 *===========================================================================*/
int submit_common_msg(struct usb_device *dev, unsigned long pipe, void *buffer,
        int transfer_len, struct devrequest *setup, int interval)
{
    int maxsize = usb_maxpacket(dev, pipe);
    int timeout;
    struct ohci *gohci = dev->hc_private;

    //QYL-2008-03-07
    unsigned int dev_num, ed_num;
    urb_priv_t *lurb_priv = NULL;

    /* device pulled? Shortcut the action. */
    if (devgone == dev) {
        dev->status = USB_ST_CRC_ERR;
        return 0;
    }

    if (!maxsize) {
        err("submit_common_message: pipesize for pipe %lx is zero",
                pipe);
        return -1;
    }

    for (dev_num = 0; dev_num < USB_MAX_DEVICE; dev_num++) {
        if (dev == &usb_dev[dev_num]) {
            break;
        }
    }
    ed_num = usb_pipeendpoint(pipe) |(usb_pipecontrol(pipe) ? 0: (usb_pipeout(pipe)<<4));
    lurb_priv = &ohci_urb[dev_num][ed_num];
    lurb_priv->state = USB_ST_NOT_PROC;

    gohci->transfer_lock++;
    if (pipe != PIPE_INTERRUPT) {
        gohci->hc_control &= ~OHCI_CTRL_PLE;
        writel(gohci->hc_control, &gohci->regs->control);
    }

    if (sohci_submit_job(dev, pipe, buffer, transfer_len, setup, interval) <
            0) {
        err("sohci_submit_job failed");
        return -1;
    }

    /* allow more time for a BULK device to react - some are slow */
#define BULK_TO	5000   /* timeout in milliseconds */
    if (usb_pipetype(pipe) == PIPE_BULK)
        timeout = BULK_TO;
    else
        timeout = 500;

    /* wait for it to complete */
#if 0
    for (;;) {
        /* check whether the controller is done */
        stat = hc_interrupt(gohci);
        if (stat == NOTUSBIRQ)
            continue;
        if (stat < 0) {
            stat = USB_ST_CRC_ERR << 8;
            break;
        }

        if (stat >= 0 && stat != 0xff) {
            /* 0xff is returned for an SF-interrupt */
            if (stat != 303 || stat != TD_CC_STALL) {
                os_kprintf("OHCI: unexpected stat %x\n", stat);
                break;
            }
        }
    }
#else
    os_tick_t tick_base = os_tick_get_value();

    while (((os_tick_get_value() - tick_base) * (MSEC_PER_SEC/OS_TICK_PER_SECOND)) < timeout) {
        dev->status = lurb_priv->state;
        if (!(dev->status & USB_ST_NOT_PROC)) {
            timeout = 0;
            break;
        }
        delay_usb_ohci(200);
        hc_interrupt(gohci);
    }
    gohci->transfer_lock--;
    if (pipe != PIPE_INTERRUPT) {
        gohci->hc_control |= OHCI_CTRL_PLE;
        writel(gohci->hc_control, &gohci->regs->control);
    }

    if (timeout)
        os_kprintf("USB timeout dev:%p\n\r",dev);

#endif

    /* we got an Root Hub Status Change interrupt */
    if (got_rhsc) {
        got_rhsc = 0;
        /* abuse timeout */
        timeout = rh_check_port_status(gohci);
        if (timeout >= 0) {
            /*
             * XXX
             * This is potentially dangerous because it assumes
             * that only one device is ever plugged in!
             */
            devgone = dev;
        }
    }
    //QYL-2008-03-07

    if (!(usb_pipetype(pipe) == PIPE_INTERRUPT || dev->irq_handle_ep[ed_num])) {	/*FIXME, might not done bulk */
        if(!dev->status && !timeout)
            dev->act_len = transfer_len;
        /* free TDs in urb_priv */
        urb_free_priv(lurb_priv);
        memset(lurb_priv, 0, sizeof(*lurb_priv));
    }

    if (dev->status || timeout)
        return -1;

    return 0;
}

/*===========================================================================
 *
 *FUNTION: ohci_submit_bulk_msg
 *
 *DESCRIPTION: This function is used to submit a bulk message to OHCI controller.
 *	     This is a submit routine called from usb.c.
 *
 *PARAMETERS:
 *	  [IN] dev:  a pointer to the USB device the message belongs to.
 *	  [IN] pipe:  describe the property of a pipe,more details about it,
 *		      please see the usb.h.
 *	  [IN] buffer:an all-purpose pointer to the data that would be returned
 *		      through usb channel.
 *	  [IN] transfer_len: the length of data to be transfered.
 *
 *RETURN VALUE: same as the value returned from submit_common_msg().
 *
 *===========================================================================*/
static int ohci_submit_bulk_msg(struct usb_device *dev, unsigned long pipe,
        void *buffer, int transfer_len)
{
    int s;

    if (ohci_debug)
        os_kprintf("submit_bulk_msg %x/%d\n\r", buffer, transfer_len);
    //if ((unsigned long) buffer & 0x0f)
    //    os_kprintf("bulk buffer %x/%d not aligned\n\r", buffer, transfer_len);
    s = submit_common_msg(dev, pipe, buffer, transfer_len, NULL, 0);
    if (ohci_debug)
        os_kprintf("submit_bulk_msg END\n\r");

    return s;
}

/*===========================================================================
 *
 *FUNTION: ohci_submit_control_msg
 *
 *DESCRIPTION: This function is used to submit a control message to OHCI controller
 *	     or root hub. This is a submit routine called from usb.c.
 *
 *PARAMETERS:
 *	  [IN] dev:  a pointer to the USB device the message belongs to.
 *	  [IN] pipe:  describe the property of a pipe,more details about it,
 *		      please see the usb.h.
 *	  [IN] buffer:an all-purpose pointer to the data that would be returned
 *		      through usb channel.
 *	  [IN] transfer_len: the length of data to be transfered.
 *	  [IN] setup: an pointer to the struct devrequest *,which is used when
 *		       the transfer type is setup of control transfer.
 *
 *RETURN VALUE: same as the value returned from submit_common_msg()or ohci_
 *	      _submit_rh_msg().
 *
 *===========================================================================*/
static int ohci_submit_control_msg(struct usb_device *dev, unsigned long pipe,
        void *buffer, int transfer_len,
        struct devrequest *setup)
{
    int maxsize = usb_maxpacket(dev, pipe);
    struct ohci *gohci = dev->hc_private;

    if (ohci_debug)
        os_kprintf("submit_control_msg(%d) %x/%d\n\r", (pipe >> 8) & 0x7f,
                buffer, transfer_len);
    if (!maxsize) {
        err("submit_control_message: pipesize for pipe %lx is zero",
                pipe);
        return -1;
    }
    if (((pipe >> 8) & 0x7f) == gohci->rh.devnum) {
        gohci->rh.dev = dev;
        /* root hub - redirect */
        return ohci_submit_rh_msg(dev, pipe, buffer, transfer_len,
                setup);
    }
    return submit_common_msg(dev, pipe, buffer, transfer_len, setup, 0);
}

/*===========================================================================
 *
 *FUNTION: ohci_submit_int_msg
 *
 *DESCRIPTION: This function is used to submit a interrupt message to OHCI
 *	     controller. This is a submit routine called from usb.c.
 *
 *PARAMETERS:
 *	  [IN] dev:  a pointer to the USB device the message belongs to.
 *	  [IN] pipe:  describe the property of a pipe,more details about it,
 *		      please see the usb.h.
 *	  [IN] buffer:an all-purpose pointer to the data that would be returned
 *		      through usb channel.
 *	  [IN] transfer_len: the length of data to be transfered.
 *	  [IN] interval: not used here.
 *
 *RETURN VALUE: same as the value returned from submit_common_msg().
 *
 *===========================================================================*/
static int ohci_submit_int_msg(struct usb_device *dev, unsigned long pipe,
        void *buffer, int transfer_len, int interval)
{
    int s;

    if (ohci_debug)
        os_kprintf("submit int(%08x)\n\r", dev);
    s = submit_common_msg(dev, pipe, buffer, transfer_len, NULL, interval);
    return s;
}

/*-------------------------------------------------------------------------*
 * HC functions
 *-------------------------------------------------------------------------*/

/*===========================================================================
 *
 *FUNTION: hc_reset
 *
 *DESCRIPTION: This function is used to reset the HC and BUS.
 *
 *PARAMETERS:
 *	  [IN] ohci: a pointer to the struct ohci which stores useful
 *		     information of the USB OHCI controller.
 *
 *RETURN VALUE: -1: indicates that USB HC TakeOver failed or USB HC reset time out.
 *	      0 : normal return.
 *
 *===========================================================================*/
static int hc_reset(ohci_t *ohci)
{
    int timeout = 30;
    int smm_timeout = 50;	/* 0,5 sec */

    if (readl(&ohci->regs->control) & OHCI_CTRL_IR) {	/* SMM owns the HC */
        writel(OHCI_OCR, &ohci->regs->cmdstatus);	/* request ownership */
        info("USB HC TakeOver from SMM");
        while (readl(&ohci->regs->control) & OHCI_CTRL_IR) {
            wait_ms(10);
            if (--smm_timeout == 0) {
                err("USB HC TakeOver failed!");
                return -1;
            }
        }
    }

    /* Disable HC interrupts */
    writel(OHCI_INTR_MIE, &ohci->regs->intrdisable);

    dbg("USB HC reset_hc usb-%s: ctrl = 0x%X ;",
            ohci->slot_name, readl(&ohci->regs->control));

    /* Reset USB (needed by some controllers) */
    writel(0, &ohci->regs->control);

    /* HC Reset requires max 10 us delay */
    writel(OHCI_HCR, &ohci->regs->cmdstatus);
    while ((readl(&ohci->regs->cmdstatus) & OHCI_HCR) != 0) {
        if (--timeout == 0) {
            err("USB HC reset timed out!");
            return -1;
        }
        delay_usb_ohci(1);
    }
    return 0;
}

/*===========================================================================
 *
 *FUNTION: hc_start
 *
 *DESCRIPTION: This function is used to start an OHCI controller,set the BUS
 *	     operational ,enable interrupts and connect the virtual root hub.
 *
 *PARAMETERS:
 *	  [IN] ohci: a pointer to the struct ohci which stores useful
 *		     information of the USB OHCI controller.
 *
 *RETURN VALUE: always returns 0.
 *===========================================================================*/
static int hc_start(ohci_t * ohci)
{
    unsigned int mask;
    unsigned int fminterval;

    ohci->disabled = 1;

    /* Tell the controller where the control and bulk lists are
     * The lists are empty now. */

    writel(0, &ohci->regs->ed_controlhead);
    writel(0, &ohci->regs->ed_bulkhead);
    writel(0, &ohci->regs->ed_periodcurrent);
    writel((unsigned int)(vtophys(ohci->hcca)), &ohci->regs->hcca);	/* a reset clears this */

    os_kprintf("early period(0x%x)\n\r", readl(&ohci->regs->ed_periodcurrent));
    
    fminterval = 0x2edf;
    writel((fminterval * 9) / 10, &ohci->regs->periodicstart);
    fminterval |= ((((fminterval - 210) * 6) / 7) << 16);
    writel(fminterval, &ohci->regs->fminterval);
    writel(0x628, &ohci->regs->lsthresh);
    /* start controller operations */
    ohci->hc_control = OHCI_CONTROL_INIT | OHCI_USB_OPER;
    ohci->disabled = 0;
    writel(ohci->hc_control, &ohci->regs->control);

    {
        int val;
        val = readl(&ohci->regs->intrstatus);
        while (val & OHCI_INTR_SF) {
            udelay(10);
            //delay_usb_ohci(10);
            val = readl(&ohci->regs->intrstatus);
        }
    }
    /* disable all interrupts */
    mask = (OHCI_INTR_SO | OHCI_INTR_WDH | OHCI_INTR_SF | OHCI_INTR_RD |
            OHCI_INTR_UE | OHCI_INTR_FNO | OHCI_INTR_RHSC |
            OHCI_INTR_OC | OHCI_INTR_MIE);
    writel(mask, &ohci->regs->intrdisable);
    /* clear all interrupts */
    mask &= ~OHCI_INTR_MIE;
    writel(mask, &ohci->regs->intrstatus);
    /* Choose the interrupts we care about now  - but w/o MIE */
    mask =
        OHCI_INTR_RHSC | OHCI_INTR_UE | OHCI_INTR_WDH | OHCI_INTR_SO |
        OHCI_INTR_MIE;
    writel(mask, &ohci->regs->intrenable);

#define mdelay(n) do {unsigned long msec=(n); while (msec--) udelay(1000);} while (0)
    /* POTPGT delay is bits 24-31, in 2 ms units. */
    mdelay((roothub_a(ohci) >> 23) & 0x1fe);

    mdelay(1);

    /* connect the virtual root hub */
    ohci->rh.devnum = 0;

    return 0;
}

/*===========================================================================
 *
 *FUNTION: hc_interrupt
 *
 *DESCRIPTION: This function is used to check whether OHCI controller has finished
 *	     transfering the PIPE_INTERRUPT TDs.It is a INTR-hanlder-like routine,
 *	     and will be called by system frequently.
 *PARAMETERS:
 *	  [IN] hc_data: an all-purpose pointer to be cast to struct ohci*,
 *			points to the buffer which stores information of ohci.
 *
 *RETURN VALUE: 0 :   indicates that no TDs has been transfered.
 *	      stat: indicates that PIPE_INTERRUPT TDs has been transfered and be
 *		    dealed with.*
 *
 *===========================================================================*/
static int hc_interrupt(void *hc_data)
{
    ohci_t *ohci = hc_data;
    struct ohci_regs *regs = ohci->regs;

    urb_priv_t *lurb_priv = NULL;	//&urb_priv;
    td_t *td = NULL;
    int ints;

    int stat = NOTUSBIRQ;

    //QYL-2008-03-07
    unsigned int dev_num, ed_num;
    struct usb_device *p_dev = NULL;
    ed_t *p_ed = NULL;

    if ((ohci->hcca->done_head != 0) && !(m32_swap(ohci->hcca->done_head) & 0x01)) {
        ints = OHCI_INTR_WDH;
        {
            td = (td_t *)PHYS_TO_CACHED(ohci->hcca->done_head);
        }
    } else {
        ints = readl(&regs->intrstatus);
        if (ints == ~0ul){
            return 0;
        }
        if (ints == 0){
            return 0;
        }
    }

    if (ints & OHCI_INTR_RHSC) {
        writel(OHCI_INTR_RD | OHCI_INTR_RHSC, &regs->intrstatus);
        got_rhsc = 1;
    }

    if (got_rhsc) {
        int timeout;
        got_rhsc = 0;
        /* abuse timeout */
        delay_usb_ohci(250);
        timeout = rh_check_port_status(ohci);
        if (timeout >= 0) {
            /*
             * XXX
             * This is potentially dangerous because it assumes
             * that only one device is ever plugged in!
             */
            //os_kprintf("**hc_interrupt 2008-03-07** device disconnected\n");//QYL-2008-03-07
        }
    }

    if (ints & OHCI_INTR_UE) {
        ohci->disabled++;
        /* e.g. due to PCI Master/Target Abort */
        //#ifdef	DEBUG
        /* FIXME: be optimistic, hope that bug won't repeat often. */
        /* Make some non-interrupt context restart the controller. */
        /* Count and limit the retries though; either hardware or */
        /* software errors can go forever... */
        hc_reset(ohci);
        ohci->disabled--;
        return -1;
    }

    if (ints & OHCI_INTR_WDH && ohci->hcca->done_head) {

        writel(OHCI_INTR_WDH, &regs->intrdisable);

        if (td == NULL) {
            {
                td = (td_t *)PHYS_TO_CACHED(ohci->hcca->done_head & ~0x1f);
            }
        }

        if (td == NULL) {
            os_kprintf("Bad td in donehead\n\r");
        } else if ((td != NULL) && (td->ed != NULL)) {
            p_dev = td->usb_dev;
            for (dev_num = 0; dev_num < USB_MAX_DEVICE; dev_num++) {
                if (p_dev == &usb_dev[dev_num]) {
                    break;
                }
            }
            p_ed = td->ed;
            ed_num = (p_ed->hwINFO & 0xf80) >> 7;
            lurb_priv = &ohci_urb[dev_num][ed_num];

            if (td->index != lurb_priv->length - 1) {
                stat = 
                    dl_done_list(ohci,
                            dl_reverse_done_list(ohci));
            } else {
                stat = 
                    dl_done_list(ohci,
                            dl_reverse_done_list(ohci));
            }
        }

        writel(OHCI_INTR_WDH, &regs->intrenable);

    }
    if (ints & OHCI_INTR_SO) {
        {
            int val;
            writel(OHCI_INTR_SO, &regs->intrstatus);
            val = readl(&regs->intrdisable);
            val |= OHCI_INTR_SO;
            writel(val, &regs->intrdisable);
        }
        stat = -1;
    }

    /* FIXME:  this assumes SOF (1/ms) interrupts don't get lost... */
    if (ints & OHCI_INTR_SF) {
        unsigned int frame = m16_swap(ohci->hcca->frame_no) & 1;
        writel(OHCI_INTR_SF, &regs->intrdisable);
        if (ohci->ed_rm_list[frame] != NULL)
            writel(OHCI_INTR_SF, &regs->intrenable);
        stat = 0xff;
    }

    writel(ints, &regs->intrstatus);
    (void)readl(&regs->control);

    return stat;
}

/*===========================================================================
 *
 *FUNTION: hc_release_ohci
 *
 *DESCRIPTION: This function is used to De-allocate all resources.
 *
 *PARAMETERS:
 *	  [IN] ohci: a pointer to the struct ohci which stores useful
 *		     information of the USB OHCI controller.
 *
 *RETURN VALUE: none.
 *===========================================================================*/
static void hc_release_ohci(ohci_t * ohci)
{
    dbg("USB HC release ohci usb-%s", ohci->slot_name);

    if (!ohci->disabled)
        hc_reset(ohci);
}

/*===========================================================================
 *
 *FUNTION: usb_lowlevel_init
 *
 *DESCRIPTION: This is the low level initialization routine,called from usb.c.
 *
 *PARAMETERS:
 *	  [IN] ghci: a pointer to the struct ohci which stores useful
 *		     information of the USB OHCI controller.
 *
 *RETURN VALUE: -1: indicates that HCCA not aligned or EDs not aligned or TDs
 *		  not aligned or OHCI initialization error.
 *	      0 : indicates that initialization finished successfully.
 *===========================================================================*/
int usb_lowlevel_init(ohci_t * gohci)
{

    struct ohci_hcca *hcca = NULL;
    struct ohci_device *ohci_dev = NULL;
    td_t *gtd = NULL;
    unsigned char *tmpbuf;

    dbg("in usb_lowlevel_init\n");
    hcca = os_aligned_malloc(256,sizeof(*gohci->hcca));
    memset(hcca, 0, sizeof(*hcca));

    /* align the storage */
    if ((unsigned long) & hcca[0] & 0xff) {
        err("HCCA not aligned!! %x\n", hcca);
        return -1;
    }

    ohci_dev = os_aligned_malloc(32,sizeof *ohci_dev);
    memset(ohci_dev, 0, sizeof(struct ohci_device));

    if ((unsigned long) & ohci_dev->ed[0] & 31) {
        err("EDs not aligned!!");
        return -1;
    } else {
        ohci_dev->cpu_ed = (ed_t *) (&ohci_dev->ed);
    }

    gtd = os_aligned_malloc(32,sizeof(td_t) * (NUM_TD + 1));
    memset(gtd, 0, sizeof(td_t) * (NUM_TD + 1));

    if ((unsigned long) gtd & 0x0f) {
        err("TDs not aligned!!");
        return -1;
    }
    gohci->hcca = (struct ohci_hcca *)(hcca);
    gohci->gtd = (td_t *) (gtd);
    gohci->ohci_dev = ohci_dev;

    tmpbuf = os_aligned_malloc(32,512);
    if (tmpbuf == NULL) {
        os_kprintf("No mem for control buffer\n\r");
        goto errout;
    }
    if ((unsigned long) tmpbuf & 0x1f)
        os_kprintf("Malloc return not cache line aligned\n\r");
    memset(tmpbuf, 0, 512);
    gohci->control_buf = (unsigned char *)(tmpbuf);

    tmpbuf = os_aligned_malloc(32,64);
    if (tmpbuf == NULL) {
        os_kprintf("No mem for setup buffer\n\r");
        goto errout;
    }
    if ((unsigned long) tmpbuf & 0x1f)
        os_kprintf("Malloc return not cache line aligned\n\r");
    memset(tmpbuf, 0, 64);
    gohci->setup = (unsigned char *)(tmpbuf);
    dbg(" gohci->setup =%llx\n\r", gohci->setup);

    gohci->disabled = 1;
    gohci->sleeping = 0;
    gohci->irq = -1;
    dbg("original OHCI: regs base %llx\n", gohci->sc_sh);	//0xc4808000
    gohci->regs = (struct ohci_regs *)(gohci->sc_sh);
    dbg("OHCI: regs base %llx\n", gohci->regs);
    gohci->slot_name = "Godson";
    dbg("OHCI revision: 0x%08x\n" "  RH: a: 0x%08x b: 0x%08x\n",
            readl(&gohci->regs->revision), readl(&gohci->regs->roothub.a),
            readl(&gohci->regs->roothub.b));
    if (hc_reset(gohci) < 0)
        goto errout;
    /* FIXME this is a second HC reset; why?? */
    writel(gohci->hc_control = OHCI_USB_RESET, &gohci->regs->control);
    wait_ms(10);
    if (hc_start(gohci) < 0)
        goto errout;
#ifdef	DEBUG
    //ohci_dump (gohci, 1);
#else
    wait_ms(1);
#endif
    os_kprintf("OHCI %llx initialized ok\n\r", gohci);
    return 0;

errout:
    err("OHCI initialization error\n\r");
    hc_release_ohci(gohci);
    /* Initialization failed */
    return -1;
}
/*===========================================================================
 *
 *FUNTION: usb_lowlevel_stop
 *
 *DESCRIPTION: This function is used to reset OHCI controller.
 *
 *PARAMETERS:
 *	  [IN] ohci: a pointer to the struct ohci which stores useful
 *		     information of the USB OHCI controller.
 *
 *RETURN VALUE: always return 0
 *
 *===========================================================================*/
int usb_lowlevel_stop(void *hc_data)
{
    /* this gets called really early - before the controller has */
    /* even been initialized! */
    struct ohci *ohci = hc_data;
    /* TODO release any interrupts, etc. */
    /* call hc_release_ohci() here ? */
    hc_reset(ohci);
    /* may not want to do this */
    /* Disable clock */
    return 0;
}

void usb_ohci_stop_one(ohci_t *ohci)
{
    writel(0, &ohci->regs->control);
    writel(OHCI_HCR, &ohci->regs->cmdstatus);
    (void)readl(&ohci->regs->cmdstatus);
}

void usb_ohci_stop()
{
    int i;

    for (i = 0; i < MAX_OHCI_C && usb_ohci_dev[i]; i++)
        usb_ohci_stop_one(usb_ohci_dev[i]);
}

#ifdef DEBUG
#include  <pmon.h>
extern unsigned long strtoul(const char *nptr, char **endptr, int base);

int cmd_setdebug(int ac, char *av[])
{
    int val;

    if (ac == 1) {
        if (ohci_debug)
            os_kprintf("ohci debug on\n");
        else
            os_kprintf("ohci debug off\n");
        return 0;
    }

    val = strtoul(av[1], 0, 0);
    if (val)
        ohci_debug = 1;
    else
        ohci_debug = 0;

    return 0;
}

static const struct Optdesc ohci_opts[] = {
    {"ohci controller", "debug"},
    {"ohci", ""},
    {}
};

static const struct Cmd Cmds[] = {
    {"ohci"},
    {"dohci", "",
        0,
        "swith debug for ohci driver", cmd_setdebug, 1, 2, 0},
    {0, 0},
};

static void init_cmd __P((void)) __attribute__ ((constructor));

static void init_cmd()
{
    cmdlist_expand(Cmds, 1);
}
#endif
