/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        ls2k_stmmac.h
 *
 * @brief       The head file of eth driver for loongson
 *
 * @revision
 * Date         Author          Notes
 * 2020-11-17   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#ifndef __LS2K_STMMAC_H__
#define __LS2K_STMMAC_H__

#include <os_types.h>
#include <os_mutex.h>
#include <device.h>
#include <os_list.h>
#include <net/net_dev.h>

#define cpu_to_le32(x) (x)
#define le32_to_cpu(x) (x)

#ifndef min
#define min(a, b) ((a) < (b) ? (a) : (b))
#endif

#ifndef offsetof
#define offsetof(TYPE, MEMBER) ((size_t) & ((TYPE *)0)->MEMBER)
#endif

/* Synopsys Core versions */
#define DWMAC_CORE_3_40   0x34
#define DWMAC_CORE_3_50   0x35
#define DWMAC_CORE_4_00   0x40
#define DWMAC_CORE_4_10   0x41
#define DWMAC_CORE_5_00   0x50
#define DWMAC_CORE_5_10   0x51
#define DWXGMAC_CORE_2_10 0x21

#define STMMAC_CHAIN_MODE 0x1
#define STMMAC_RING_MODE  0x2

#define BUF_SIZE_16KiB 16368
#define BUF_SIZE_8KiB  8188
#define BUF_SIZE_4KiB  4096
#define BUF_SIZE_2KiB  2048

#define JUMBO_LEN     9000
#define STMMAC_CH_MAX 8

#define BIT(n)        (1 << (n))
#define GENMASK(h, l) (((~0) - (1 << (l)) + 1) & (~0 >> (32 - 1 - (h))))

#define LS2K_GMAC0_BASE (0x800000001f020000)
#define LS2K_GMAC1_BASE (0x800000001f030000)

#define LS2K_GMAC0_DMA_BASE (0x800000001f021000)
#define LS2K_GMAC1_DMA_BASE (0x800000001f031000)

#define LS2K_GMAC0_IRQ (LIOINT_IRQ(12))
#define LS2K_GMAC1_IRQ (LIOINT_IRQ(14))

#define MAC_CSR_H_FRQ_MASK 0x20

/* MDC Clock Selection define*/
#define STMMAC_CSR_60_100M  0x0 /* MDC = clk_scr_i/42 */
#define STMMAC_CSR_100_150M 0x1 /* MDC = clk_scr_i/62 */
#define STMMAC_CSR_20_35M   0x2 /* MDC = clk_scr_i/16 */
#define STMMAC_CSR_35_60M   0x3 /* MDC = clk_scr_i/26 */
#define STMMAC_CSR_150_250M 0x4 /* MDC = clk_scr_i/102 */
#define STMMAC_CSR_250_300M 0x5 /* MDC = clk_scr_i/122 */

/* CSR Frequency Access Defines*/
#define CSR_F_35M  35000000
#define CSR_F_60M  60000000
#define CSR_F_100M 100000000
#define CSR_F_150M 150000000
#define CSR_F_250M 250000000
#define CSR_F_300M 300000000

#define GMAC_VERSION  0x00000020 /* GMAC CORE Version */
#define GMAC4_VERSION 0x00000110 /* GMAC4+ CORE Version */

#define PTP_XGMAC_OFFSET   0xd00
#define PTP_GMAC4_OFFSET   0xb00
#define PTP_GMAC3_X_OFFSET 0x700

#define MMC_GMAC4_OFFSET   0x700
#define MMC_GMAC3_X_OFFSET 0x100

/* Common MAC defines */
#define MAC_CTRL_REG  0x00000000 /* MAC Control */
#define MAC_ENABLE_TX 0x00000008 /* Transmitter Enable */
#define MAC_ENABLE_RX 0x00000004 /* Receiver Enable */

#define MTL_MAX_RX_QUEUES 8
#define MTL_MAX_TX_QUEUES 8
#define STMMAC_CH_MAX     8

#define STMMAC_RX_COE_NONE  0
#define STMMAC_RX_COE_TYPE1 1
#define STMMAC_RX_COE_TYPE2 2

#define TX_CIC_FULL 3 /* Include IP header and pseudoheader */

/* Packets types */
enum packets_types
{
    PACKET_AVCPQ  = 0x1, /* AV Untagged Control packets */
    PACKET_PTPQ   = 0x2, /* PTP Packets */
    PACKET_DCBCPQ = 0x3, /* DCB Control Packets */
    PACKET_UPQ    = 0x4, /* Untagged Packets */
    PACKET_MCBCQ  = 0x5, /* Multicast & Broadcast Packets */
};

/* Rx IPC status */
enum rx_frame_status
{
    good_frame    = 0x0,
    discard_frame = 0x1,
    csum_none     = 0x2,
    llc_snap      = 0x4,
    dma_own       = 0x8,
    rx_not_ls     = 0x10,
};

/* Tx status */
enum tx_frame_status
{
    tx_done    = 0x0,
    tx_not_ls  = 0x1,
    tx_err     = 0x2,
    tx_dma_own = 0x4,
};

enum dma_irq_status
{
    tx_hard_error         = 0x1,
    tx_hard_error_bump_tc = 0x2,
    handle_rx             = 0x4,
    handle_tx             = 0x8,
};

typedef enum
{
    PHY_INTERFACE_MODE_NA,
    PHY_INTERFACE_MODE_INTERNAL,
    PHY_INTERFACE_MODE_MII,
    PHY_INTERFACE_MODE_GMII,
    PHY_INTERFACE_MODE_SGMII,
    PHY_INTERFACE_MODE_TBI,
    PHY_INTERFACE_MODE_REVMII,
    PHY_INTERFACE_MODE_RMII,
    PHY_INTERFACE_MODE_RGMII,
    PHY_INTERFACE_MODE_RGMII_ID,
    PHY_INTERFACE_MODE_RGMII_RXID,
    PHY_INTERFACE_MODE_RGMII_TXID,
    PHY_INTERFACE_MODE_RTBI,
    PHY_INTERFACE_MODE_SMII,
    PHY_INTERFACE_MODE_XGMII,
    PHY_INTERFACE_MODE_MOCA,
    PHY_INTERFACE_MODE_QSGMII,
    PHY_INTERFACE_MODE_TRGMII,
    PHY_INTERFACE_MODE_1000BASEX,
    PHY_INTERFACE_MODE_2500BASEX,
    PHY_INTERFACE_MODE_RXAUI,
    PHY_INTERFACE_MODE_XAUI,
    /* 10GBASE-KR, XFI, SFI - single lane 10G Serdes */
    PHY_INTERFACE_MODE_10GKR,
    PHY_INTERFACE_MODE_MAX,
} phy_interface_t;

enum phy_state
{
    PHY_DOWN = 0,
    PHY_STARTING,
    PHY_READY,
    PHY_PENDING,
    PHY_UP,
    PHY_AN,
    PHY_RUNNING,
    PHY_NOLINK,
    PHY_FORCING,
    PHY_CHANGELINK,
    PHY_HALTED,
    PHY_RESUMING
};

/* These need to be power of two, and >= 4 */
#define DMA_TX_SIZE               (512)
#define DMA_RX_SIZE               (512)
#define STMMAC_GET_ENTRY(x, size) ((x + 1) & (size - 1))
#define STMMAC_ALIGN_SIZE         (0x4000)
#define STMMAC_DMA_ALIGN_SIZE     (0x4000)
#define STMMAC_BUFF_ALIGN_SIZE    (0x40)

#define MAX_DMA_RIWT 0xff
#define MIN_DMA_RIWT 0x20

typedef unsigned long dma_addr_t;

struct stmmac_dma_cfg
{
    int          pbl;
    int          txpbl;
    int          rxpbl;
    unsigned int pblx8;
    int          fixed_burst;
    int          mixed_burst;
    unsigned int aal;
};

struct dma_desc
{
    unsigned int des0;
    unsigned int des1;
    unsigned int des2;
    unsigned int des3;
};

struct dma_extended_desc
{
    struct dma_desc basic; /* Basic descriptors */
    unsigned int    des4;  /* Extended Status */
    unsigned int    des5;  /* Reserved */
    unsigned int    des6;  /* Tx/Rx Timestamp Low */
    unsigned int    des7;  /* Tx/Rx Timestamp High */
};

struct stmmac_channel
{
    struct os_loongson_eth *priv_data;
    unsigned int            index;
    int                     has_rx;
    int                     has_tx;
    os_semaphore_id         rx_sem;
    os_task_id              rx_task;
};

struct stmmac_regs_off
{
    unsigned int ptp_off;
    unsigned int mmc_off;
};

struct stmmac_hwif_entry
{
    unsigned int                 gmac;
    unsigned int                 gmac4;
    unsigned int                 xgmac;
    unsigned int                 min_id;
    const struct stmmac_regs_off regs;
    const void                  *desc;
    const void                  *dma;
    const void                  *mac;
    const void                  *hwtimestamp;
    const void                  *mode;
    const void                  *tc;
    int (*setup)(struct os_loongson_eth *device);
    int (*quirks)(struct os_loongson_eth *device);
};

struct mac_link
{
    unsigned int speed_mask;
    unsigned int speed10;
    unsigned int speed100;
    unsigned int speed1000;
    unsigned int speed2500;
    unsigned int speed10000;
    unsigned int duplex;
};

struct mii_regs
{
    unsigned int addr;       /* MII Address */
    unsigned int data;       /* MII Data */
    unsigned int addr_shift; /* MII address shift */
    unsigned int reg_shift;  /* MII reg shift */
    unsigned int addr_mask;  /* MII address mask */
    unsigned int reg_mask;   /* MII reg mask */
    unsigned int clk_csr_shift;
    unsigned int clk_csr_mask;
};

struct rgmii_adv
{
    unsigned int pause;
    unsigned int duplex;
    unsigned int lp_pause;
    unsigned int lp_duplex;
};

struct mac_device_info
{
    const struct stmmac_ops         *mac;
    const struct stmmac_desc_ops    *desc;
    const struct stmmac_dma_ops     *dma;
    const struct stmmac_mode_ops    *mode;
    const struct stmmac_hwtimestamp *ptp;
    const struct stmmac_tc_ops      *tc;
    struct mii_regs                  mii; /* MII register Addresses */
    struct mac_link                  link;
    void                            *pcsr; /* vpointer to device CSRs */
    int                              multicast_filter_bins;
    int                              unicast_filter_entries;
    int                              mcast_bits_log2;
    unsigned int                     rx_csum;
    unsigned int                     pcs;
    unsigned int                     pmt;
    unsigned int                     ps;
};

struct dma_features
{
    unsigned int mbps_10_100;
    unsigned int mbps_1000;
    unsigned int half_duplex;
    unsigned int hash_filter;
    unsigned int multi_addr;
    unsigned int pcs;
    unsigned int sma_mdio;
    unsigned int pmt_remote_wake_up;
    unsigned int pmt_magic_frame;
    unsigned int rmon;
    /* IEEE 1588-2002 */
    unsigned int time_stamp;
    /* IEEE 1588-2008 */
    unsigned int atime_stamp;
    /* 802.3az - Energy-Efficient Ethernet (EEE) */
    unsigned int eee;
    unsigned int av;
    unsigned int tsoen;
    /* TX and RX csum */
    unsigned int tx_coe;
    unsigned int rx_coe;
    unsigned int rx_coe_type1;
    unsigned int rx_coe_type2;
    unsigned int rxfifo_over_2048;
    /* TX and RX number of channels */
    unsigned int number_rx_channel;
    unsigned int number_tx_channel;
    /* TX and RX number of queues */
    unsigned int number_rx_queues;
    unsigned int number_tx_queues;
    /* PPS output */
    unsigned int pps_out_num;
    /* Alternate (enhanced) DESC mode */
    unsigned int enh_desc;
    /* TX and RX FIFO sizes */
    unsigned int tx_fifo_size;
    unsigned int rx_fifo_size;
    /* Automotive Safety Package */
    unsigned int asp;
    /* RX Parser */
    unsigned int frpsel;
    unsigned int frpbs;
    unsigned int frpes;
};

struct plat_stmmacenet_data
{
    int                   bus_id;
    int                   phy_addr;
    int                   interface;
    struct stmmac_dma_cfg dma_cfg;
    int                   clk_csr;
    int                   has_gmac;
    int                   enh_desc;
    int                   extend_desc;
    int                   tx_coe;
    int                   rx_coe;
    int                   bugged_jumbo;
    int                   pmt;
    int                   force_sf_dma_mode;
    int                   force_thresh_dma_mode;
    int                   riwt_off;
    int                   max_speed;
    int                   maxmtu;
    int                   multicast_filter_bins;
    int                   unicast_filter_entries;
    int                   tx_fifo_size;
    int                   rx_fifo_size;
    unsigned int          rx_queues_to_use;
    unsigned int          tx_queues_to_use;
    unsigned char         rx_sched_algorithm;
    unsigned char         tx_sched_algorithm;
    void                 *bsp_priv;
    struct clk           *stmmac_clk;
    struct clk           *pclk;
    struct clk           *clk_ptp_ref;
    unsigned int          clk_ptp_rate;
    unsigned int          clk_ref_rate;
    int                   has_gmac4;
    unsigned int          has_sun8i;
    unsigned int          tso_en;
    int                   mac_port_sel_speed;
    unsigned int          en_tx_lpi_clockgating;
    int                   has_xgmac;
    void                 *addr;
    const char           *mac;
    int                   wol_irq;
    int                   lpi_irq;
    int                   irq;
    unsigned int          chain_mode;
    int                   synopsys_id;
    void                 *ptpaddr;
    void                 *mmcaddr;
    int (*hwif_quirks)(struct os_loongson_eth *device);
    unsigned int        mode;
    int                 hw_cap_support;
    struct dma_features dma_cap;
    int                 use_riwt;
    unsigned int        rx_riwt;
};

struct loongson_eth_info
{
    void         *baseaddr;
    uint32_t   irqno;
    int           plb;
    unsigned char mac[6];
};

struct mii_bus
{
    struct os_loongson_eth *device;
    int (*read)(struct mii_bus *bus, int addr, int regnum);
    int (*write)(struct mii_bus *bus, int addr, int regnum, unsigned short val);
    int (*reset)(struct mii_bus *bus);
    enum
    {
        MDIOBUS_ALLOCATED = 1,
        MDIOBUS_REGISTERED,
        MDIOBUS_UNREGISTERED,
        MDIOBUS_RELEASED,
    } state;
    unsigned int    phy_mask;
    unsigned int    phy_ignore_ta_mask;
    os_mutex_id     mdio_lock;
};

struct phy_device
{

    unsigned int  phy_id;
    unsigned char autoneg;
    /* The most recently read link state */
    unsigned char   link;
    unsigned char   oldlink;
    unsigned int    oldduplex;
    int             oldspeed;
    unsigned char   autoneg_complete;
    enum phy_state  state;
    unsigned int    dev_flags;
    phy_interface_t interface;
    /*
     * forced speed & duplex (no autoneg)
     * partner speed & duplex & pause (autoneg)
     */
    int speed;
    int duplex;
    int pause;
    int asym_pause;
    /* Enabled Interrupts */
    unsigned int interrupts;

    /* Union of PHY and Attached devices' supported modes */
    /* See mii.h for more info */
    unsigned int supported;
    unsigned int advertising;
    unsigned int lp_advertising;
    /* Energy efficient ethernet modes which should be prohibited */
    unsigned int eee_broken_modes;
    int          link_timeout;
    /*
     * Interrupt number for this PHY
     * -1 means no interrupt
     */
    int irq;
    /* private data pointer */
    /* For use by PHYs to maintain extra state */
    struct os_loongson_eth *device;
    os_mutex_id             lock;
    unsigned char           mdix;
    unsigned char           mdix_ctrl;
    void (*phy_link_change)(struct phy_device *phydev, unsigned int up, unsigned int do_carrier);
    void (*adjust_link)(struct os_loongson_eth *device);
    os_task_id task;
    os_semaphore_id    sem;
};

struct stmmac_rx_queue
{
    unsigned int              queue_index;
    struct os_loongson_eth   *priv_data;
    struct dma_extended_desc *dma_erx;
    struct dma_desc          *dma_rx;
    unsigned char           **rx_buff;
    dma_addr_t               *rx_buff_dma;
    unsigned int              cur_rx;
    unsigned int              dirty_rx;
    unsigned int              rx_zeroc_thresh;
    dma_addr_t                dma_rx_phy;
    unsigned int              rx_tail_addr;
};

struct stmmac_tx_info
{
    dma_addr_t   buf;
    unsigned int map_as_page;
    unsigned     len;
    unsigned int last_segment;
    unsigned int is_jumbo;
};

struct stmmac_tx_queue
{
    unsigned int              tx_count_frames;
    unsigned int              queue_index;
    struct os_loongson_eth   *priv_data;
    struct dma_extended_desc *dma_etx;
    struct dma_desc          *dma_tx;
    unsigned char           **tx_buff;
    struct stmmac_tx_info    *tx_buff_dma;
    unsigned int              cur_tx;
    unsigned int              dirty_tx;
    dma_addr_t                dma_tx_phy;
    unsigned int              tx_tail_addr;
    unsigned int              mss;
};

struct rx_buf_list_node
{
    unsigned char *buf;
    unsigned int   size;
    os_list_node_t list;
};

struct os_loongson_eth
{
    struct os_net_device            net_dev;
    const struct loongson_eth_info *eth_info;
    struct plat_stmmacenet_data     plat_dat;
    unsigned char                   dev_addr[6];
    struct mac_device_info         *hw;
    struct stmmac_channel           channel[STMMAC_CH_MAX];
    struct mii_bus                  mii_bus;
    unsigned int                    phy_id;
    unsigned int                    phy_addr;
    struct phy_device               phy_dev;
    unsigned int                    dma_buf_sz;
    unsigned int                    rx_copybreak;
    struct stmmac_rx_queue          rx_queue[MTL_MAX_RX_QUEUES];
    struct stmmac_tx_queue          tx_queue[MTL_MAX_TX_QUEUES];
    unsigned int                    mtu;
};

struct stmmac_ops
{
    /* MAC core initialization */
    void (*core_init)(struct mac_device_info *hw, struct os_loongson_eth *device);
    /* Enable the MAC RX/TX */
    void (*set_mac)(void *ioaddr, unsigned int enable);
    /* Enable and verify that the IPC module is supported */
    int (*rx_ipc)(struct mac_device_info *hw);
    /* Enable RX Queues */
    void (*rx_queue_enable)(struct mac_device_info *hw, unsigned char mode, unsigned int queue);
    /* RX Queues Priority */
    void (*rx_queue_prio)(struct mac_device_info *hw, unsigned int prio, unsigned int queue);
    /* TX Queues Priority */
    void (*tx_queue_prio)(struct mac_device_info *hw, unsigned int prio, unsigned int queue);
    /* RX Queues Routing */
    void (*rx_queue_routing)(struct mac_device_info *hw, unsigned char packet, unsigned int queue);
    /* Program RX Algorithms */
    void (*prog_mtl_rx_algorithms)(struct mac_device_info *hw, unsigned int rx_alg);
    /* Program TX Algorithms */
    void (*prog_mtl_tx_algorithms)(struct mac_device_info *hw, unsigned int tx_alg);
    /* Set MTL TX queues weight */
    void (*set_mtl_tx_queue_weight)(struct mac_device_info *hw, unsigned int weight, unsigned int queue);
    /* RX MTL queue to RX dma mapping */
    void (*map_mtl_to_dma)(struct mac_device_info *hw, unsigned int queue, unsigned int chan);
    /* Configure AV Algorithm */
    void (*config_cbs)(struct mac_device_info *hw,
                       unsigned int            send_slope,
                       unsigned int            idle_slope,
                       unsigned int            high_credit,
                       unsigned int            low_credit,
                       unsigned int            queue);
    /* Dump MAC registers */
    void (*dump_regs)(struct mac_device_info *hw, unsigned int *reg_space);
    /* Handle extra events on specific interrupts hw dependent */
    int (*host_irq_status)(struct mac_device_info *hw);
    /* Handle MTL interrupts */
    int (*host_mtl_irq_status)(struct mac_device_info *hw, unsigned int chan);
    /* Multicast filter setting */
    void (*set_filter)(struct mac_device_info *hw);
    /* Flow control setting */
    void (*flow_ctrl)(struct mac_device_info *hw,
                      unsigned int            duplex,
                      unsigned int            fc,
                      unsigned int            pause_time,
                      unsigned int            tx_cnt);
    /* Set power management mode (e.g. magic frame) */
    void (*pmt)(struct mac_device_info *hw, unsigned long mode);
    /* Set/Get Unicast MAC addresses */
    void (*set_umac_addr)(struct mac_device_info *hw, unsigned char *addr, unsigned int reg_n);
    void (*get_umac_addr)(struct mac_device_info *hw, unsigned char *addr, unsigned int reg_n);
    void (*set_eee_mode)(struct mac_device_info *hw, unsigned int en_tx_lpi_clockgating);
    void (*reset_eee_mode)(struct mac_device_info *hw);
    void (*set_eee_timer)(struct mac_device_info *hw, int ls, int tw);
    void (*set_eee_pls)(struct mac_device_info *hw, int link);
    void (*debug)(void *ioaddr, unsigned int rx_queues, unsigned int tx_queues);
    /* PCS calls */
    void (*pcs_ctrl_ane)(void *ioaddr, unsigned int ane, unsigned int srgmi_ral, unsigned int loopback);
    void (*pcs_rane)(void *ioaddr, unsigned int restart);
    void (*pcs_get_adv_lp)(void *ioaddr, struct rgmii_adv *adv);
    /* Safety Features */
    int (*safety_feat_config)(void *ioaddr, unsigned int asp);
    int (*safety_feat_irq_status)(void *ioaddr, unsigned int asp);
    int (*safety_feat_dump)(int index, unsigned long *count, const char **desc);
    /* Flexible RX Parser */
    int (*rxp_config)(void *ioaddr, unsigned int count);
    /* Flexible PPS */
    int (*flex_pps_config)(void        *ioaddr,
                           int          index,
                           unsigned int enable,
                           unsigned int sub_second_inc,
                           unsigned int systime_flags);
};

/* DMA CRS Control and Status Register Mapping */
#define DMA_BUS_MODE         0x00001000 /* Bus Mode */
#define DMA_XMT_POLL_DEMAND  0x00001004 /* Transmit Poll Demand */
#define DMA_RCV_POLL_DEMAND  0x00001008 /* Received Poll Demand */
#define DMA_RCV_BASE_ADDR    0x0000100c /* Receive List Base */
#define DMA_TX_BASE_ADDR     0x00001010 /* Transmit List Base */
#define DMA_STATUS           0x00001014 /* Status Register */
#define DMA_CONTROL          0x00001018 /* Ctrl (Operational Mode) */
#define DMA_INTR_ENA         0x0000101c /* Interrupt Enable */
#define DMA_MISSED_FRAME_CTR 0x00001020 /* Missed Frame Counter */

/* SW Reset */
#define DMA_BUS_MODE_SFT_RESET 0x00000001 /* Software Reset */

/* Rx watchdog register */
#define DMA_RX_WATCHDOG 0x00001024

/* AXI Master Bus Mode */
#define DMA_AXI_BUS_MODE 0x00001028

#define DMA_AXI_EN_LPI           BIT(31)
#define DMA_AXI_LPI_XIT_FRM      BIT(30)
#define DMA_AXI_WR_OSR_LMT       GENMASK(23, 20)
#define DMA_AXI_WR_OSR_LMT_SHIFT 20
#define DMA_AXI_WR_OSR_LMT_MASK  0xf
#define DMA_AXI_RD_OSR_LMT       GENMASK(19, 16)
#define DMA_AXI_RD_OSR_LMT_SHIFT 16
#define DMA_AXI_RD_OSR_LMT_MASK  0xf

#define DMA_AXI_OSR_MAX 0xf
#define DMA_AXI_MAX_OSR_LIMIT                                                                                          \
    ((DMA_AXI_OSR_MAX << DMA_AXI_WR_OSR_LMT_SHIFT) | (DMA_AXI_OSR_MAX << DMA_AXI_RD_OSR_LMT_SHIFT))
#define DMA_AXI_1KBBE   BIT(13)
#define DMA_AXI_AAL     BIT(12)
#define DMA_AXI_BLEN256 BIT(7)
#define DMA_AXI_BLEN128 BIT(6)
#define DMA_AXI_BLEN64  BIT(5)
#define DMA_AXI_BLEN32  BIT(4)
#define DMA_AXI_BLEN16  BIT(3)
#define DMA_AXI_BLEN8   BIT(2)
#define DMA_AXI_BLEN4   BIT(1)
#define DMA_BURST_LEN_DEFAULT                                                                                          \
    (DMA_AXI_BLEN256 | DMA_AXI_BLEN128 | DMA_AXI_BLEN64 | DMA_AXI_BLEN32 | DMA_AXI_BLEN16 | DMA_AXI_BLEN8 |            \
     DMA_AXI_BLEN4)

#define DMA_AXI_UNDEF BIT(0)

#define DMA_AXI_BURST_LEN_MASK 0x000000FE

#define DMA_CUR_TX_BUF_ADDR 0x00001050 /* Current Host Tx Buffer */
#define DMA_CUR_RX_BUF_ADDR 0x00001054 /* Current Host Rx Buffer */
#define DMA_HW_FEATURE      0x00001058 /* HW Feature Register */

/* DMA Control register defines */
#define DMA_CONTROL_ST 0x00002000 /* Start/Stop Transmission */
#define DMA_CONTROL_SR 0x00000002 /* Start/Stop Receive */

/* DMA Normal interrupt */
#define DMA_INTR_ENA_NIE 0x00010000 /* Normal Summary */
#define DMA_INTR_ENA_TIE 0x00000001 /* Transmit Interrupt */
#define DMA_INTR_ENA_TUE 0x00000004 /* Transmit Buffer Unavailable */
#define DMA_INTR_ENA_RIE 0x00000040 /* Receive Interrupt */
#define DMA_INTR_ENA_ERE 0x00004000 /* Early Receive */

#define DMA_INTR_NORMAL (DMA_INTR_ENA_NIE | DMA_INTR_ENA_RIE | DMA_INTR_ENA_TIE)

/* DMA Abnormal interrupt */
#define DMA_INTR_ENA_AIE 0x00008000 /* Abnormal Summary */
#define DMA_INTR_ENA_FBE 0x00002000 /* Fatal Bus Error */
#define DMA_INTR_ENA_ETE 0x00000400 /* Early Transmit */
#define DMA_INTR_ENA_RWE 0x00000200 /* Receive Watchdog */
#define DMA_INTR_ENA_RSE 0x00000100 /* Receive Stopped */
#define DMA_INTR_ENA_RUE 0x00000080 /* Receive Buffer Unavailable */
#define DMA_INTR_ENA_UNE 0x00000020 /* Tx Underflow */
#define DMA_INTR_ENA_OVE 0x00000010 /* Receive Overflow */
#define DMA_INTR_ENA_TJE 0x00000008 /* Transmit Jabber */
#define DMA_INTR_ENA_TSE 0x00000002 /* Transmit Stopped */

#define DMA_INTR_ABNORMAL (DMA_INTR_ENA_AIE | DMA_INTR_ENA_FBE | DMA_INTR_ENA_UNE)

/* DMA default interrupt mask */
#define DMA_INTR_DEFAULT_MASK (DMA_INTR_NORMAL | DMA_INTR_ABNORMAL)

/* DMA Status register defines */
#define DMA_STATUS_GLPII       0x40000000 /* GMAC LPI interrupt */
#define DMA_STATUS_GPI         0x10000000 /* PMT interrupt */
#define DMA_STATUS_GMI         0x08000000 /* MMC interrupt */
#define DMA_STATUS_GLI         0x04000000 /* GMAC Line interface int */
#define DMA_STATUS_EB_MASK     0x00380000 /* Error Bits Mask */
#define DMA_STATUS_EB_TX_ABORT 0x00080000 /* Error Bits - TX Abort */
#define DMA_STATUS_EB_RX_ABORT 0x00100000 /* Error Bits - RX Abort */
#define DMA_STATUS_TS_MASK     0x00700000 /* Transmit Process State */
#define DMA_STATUS_TS_SHIFT    20
#define DMA_STATUS_RS_MASK     0x000e0000 /* Receive Process State */
#define DMA_STATUS_RS_SHIFT    17
#define DMA_STATUS_NIS         0x00010000 /* Normal Interrupt Summary */
#define DMA_STATUS_AIS         0x00008000 /* Abnormal Interrupt Summary */
#define DMA_STATUS_ERI         0x00004000 /* Early Receive Interrupt */
#define DMA_STATUS_FBI         0x00002000 /* Fatal Bus Error Interrupt */
#define DMA_STATUS_ETI         0x00000400 /* Early Transmit Interrupt */
#define DMA_STATUS_RWT         0x00000200 /* Receive Watchdog Timeout */
#define DMA_STATUS_RPS         0x00000100 /* Receive Process Stopped */
#define DMA_STATUS_RU          0x00000080 /* Receive Buffer Unavailable */
#define DMA_STATUS_RI          0x00000040 /* Receive Interrupt */
#define DMA_STATUS_UNF         0x00000020 /* Transmit Underflow */
#define DMA_STATUS_OVF         0x00000010 /* Receive Overflow */
#define DMA_STATUS_TJT         0x00000008 /* Transmit Jabber Timeout */
#define DMA_STATUS_TU          0x00000004 /* Transmit Buffer Unavailable */
#define DMA_STATUS_TPS         0x00000002 /* Transmit Process Stopped */
#define DMA_STATUS_TI          0x00000001 /* Transmit Interrupt */
#define DMA_CONTROL_FTF        0x00100000 /* Flush transmit FIFO */

#define NUM_DWMAC100_DMA_REGS  9
#define NUM_DWMAC1000_DMA_REGS 23

#define SPEED_10      10
#define SPEED_100     100
#define SPEED_1000    1000
#define SPEED_UNKNOWN -1

#define HALF_DUPLEX 1
#define FULL_DUPLEX 2

#define DMA_BUS_MODE_FB         0x00010000 /* Fixed burst */
#define DMA_BUS_MODE_MB         0x04000000 /* Mixed burst */
#define DMA_BUS_MODE_RPBL_MASK  0x007e0000 /* Rx-Programmable Burst Len */
#define DMA_BUS_MODE_RPBL_SHIFT 17
#define DMA_BUS_MODE_USP        0x00800000
#define DMA_BUS_MODE_MAXPBL     0x01000000
#define DMA_BUS_MODE_AAL        0x02000000

/* DMA CRS Control and Status Register Mapping */
#define DMA_HOST_TX_DESC 0x00001048 /* Current Host Tx descriptor */
#define DMA_HOST_RX_DESC 0x0000104c /* Current Host Rx descriptor */
/*  DMA Bus Mode register defines */
#define DMA_BUS_PR_RATIO_MASK  0x0000c000 /* Rx/Tx priority ratio */
#define DMA_BUS_PR_RATIO_SHIFT 14
#define DMA_BUS_FB             0x00010000 /* Fixed Burst */

/* DMA operation mode defines (start/stop tx/rx are placed in common header)*/
/* Disable Drop TCP/IP csum error */
#define DMA_CONTROL_DT  0x04000000
#define DMA_CONTROL_RSF 0x02000000 /* Receive Store and Forward */
#define DMA_CONTROL_DFF 0x01000000 /* Disaable flushing */

/* DAM HW feature register fields */
#define DMA_HW_FEAT_MIISEL     0x00000001 /* 10/100 Mbps Support */
#define DMA_HW_FEAT_GMIISEL    0x00000002 /* 1000 Mbps Support */
#define DMA_HW_FEAT_HDSEL      0x00000004 /* Half-Duplex Support */
#define DMA_HW_FEAT_EXTHASHEN  0x00000008 /* Expanded DA Hash Filter */
#define DMA_HW_FEAT_HASHSEL    0x00000010 /* HASH Filter */
#define DMA_HW_FEAT_ADDMAC     0x00000020 /* Multiple MAC Addr Reg */
#define DMA_HW_FEAT_PCSSEL     0x00000040 /* PCS registers */
#define DMA_HW_FEAT_L3L4FLTREN 0x00000080 /* Layer 3 & Layer 4 Feature */
#define DMA_HW_FEAT_SMASEL     0x00000100 /* SMA(MDIO) Interface */
#define DMA_HW_FEAT_RWKSEL     0x00000200 /* PMT Remote Wakeup */
#define DMA_HW_FEAT_MGKSEL     0x00000400 /* PMT Magic Packet */
#define DMA_HW_FEAT_MMCSEL     0x00000800 /* RMON Module */
#define DMA_HW_FEAT_TSVER1SEL  0x00001000 /* Only IEEE 1588-2002 */
#define DMA_HW_FEAT_TSVER2SEL  0x00002000 /* IEEE 1588-2008 PTPv2 */
#define DMA_HW_FEAT_EEESEL     0x00004000 /* Energy Efficient Ethernet */
#define DMA_HW_FEAT_AVSEL      0x00008000 /* AV Feature */
#define DMA_HW_FEAT_TXCOESEL   0x00010000 /* Checksum Offload in Tx */
#define DMA_HW_FEAT_RXTYP1COE  0x00020000 /* IP COE (Type 1) in Rx */
#define DMA_HW_FEAT_RXTYP2COE  0x00040000 /* IP COE (Type 2) in Rx */
#define DMA_HW_FEAT_RXFIFOSIZE 0x00080000 /* Rx FIFO > 2048 Bytes */
#define DMA_HW_FEAT_RXCHCNT    0x00300000 /* No. additional Rx Channels */
#define DMA_HW_FEAT_TXCHCNT    0x00c00000 /* No. additional Tx Channels */
#define DMA_HW_FEAT_ENHDESSEL  0x01000000 /* Alternate Descriptor */
/* Timestamping with Internal System Time */
#define DMA_HW_FEAT_INTTSEN    0x02000000
#define DMA_HW_FEAT_FLEXIPPSEN 0x04000000 /* Flexible PPS Output */
#define DMA_HW_FEAT_SAVLANINS  0x08000000 /* Source Addr or VLAN */
#define DMA_HW_FEAT_ACTPHYIF   0x70000000 /* Active/selected PHY iface */
#define DEFAULT_DMA_PBL        8

/*--- DMA BLOCK defines ---*/
/* DMA Bus Mode register defines */
#define DMA_BUS_MODE_DA        0x00000002 /* Arbitration scheme */
#define DMA_BUS_MODE_DSL_MASK  0x0000007c /* Descriptor Skip Length */
#define DMA_BUS_MODE_DSL_SHIFT 2          /*   (in DWORDS)      */
/* Programmable burst length (passed thorugh platform)*/
#define DMA_BUS_MODE_PBL_MASK  0x00003f00 /* Programmable Burst Len */
#define DMA_BUS_MODE_PBL_SHIFT 8
#define DMA_BUS_MODE_ATDS      0x00000080 /* Alternate Descriptor Size */

enum rtc_control
{
    DMA_CONTROL_RTC_64  = 0x00000000,
    DMA_CONTROL_RTC_32  = 0x00000008,
    DMA_CONTROL_RTC_96  = 0x00000010,
    DMA_CONTROL_RTC_128 = 0x00000018,
};

#define DMA_CONTROL_TC_RX_MASK 0xffffffe7
#define DMA_CONTROL_OSF        0x00000004 /* Operate on second frame */
#define DMA_CONTROL_TSF        0x00200000 /* Transmit  Store and Forward */

enum ttc_control
{
    DMA_CONTROL_TTC_64  = 0x00000000,
    DMA_CONTROL_TTC_128 = 0x00004000,
    DMA_CONTROL_TTC_192 = 0x00008000,
    DMA_CONTROL_TTC_256 = 0x0000c000,
    DMA_CONTROL_TTC_40  = 0x00010000,
    DMA_CONTROL_TTC_32  = 0x00014000,
    DMA_CONTROL_TTC_24  = 0x00018000,
    DMA_CONTROL_TTC_16  = 0x0001c000,
};

#define DMA_CONTROL_TC_TX_MASK 0xfffe3fff
#define DMA_CONTROL_EFC        0x00000100
#define DMA_CONTROL_FEF        0x00000080
#define DMA_CONTROL_FUF        0x00000040
#define DMA_CONTROL_RFA_MASK   0x00800600

/* Receive flow control deactivation field
 * RFD field in DMA control register, bits 22,12:11
 */
#define DMA_CONTROL_RFD_MASK 0x00401800

#define SF_DMA_MODE 1 /* DMA STORE-AND-FORWARD Operation Mode */

#define RFA_FULL_MINUS_1K 0x00000000
#define RFA_FULL_MINUS_2K 0x00000200
#define RFA_FULL_MINUS_3K 0x00000400
#define RFA_FULL_MINUS_4K 0x00000600
#define RFA_FULL_MINUS_5K 0x00800000
#define RFA_FULL_MINUS_6K 0x00800200
#define RFA_FULL_MINUS_7K 0x00800400

#define RFD_FULL_MINUS_1K 0x00000000
#define RFD_FULL_MINUS_2K 0x00000800
#define RFD_FULL_MINUS_3K 0x00001000
#define RFD_FULL_MINUS_4K 0x00001800
#define RFD_FULL_MINUS_5K 0x00400000
#define RFD_FULL_MINUS_6K 0x00400800
#define RFD_FULL_MINUS_7K 0x00401000

/* GMAC Configuration defines */
#define GMAC_CONTROL_2K 0x08000000 /* IEEE 802.3as 2K packets */
#define GMAC_CONTROL_TC 0x01000000 /* Transmit Conf. in RGMII/SGMII */
#define GMAC_CONTROL_WD 0x00800000 /* Disable Watchdog on receive */
#define GMAC_CONTROL_JD 0x00400000 /* Jabber disable */
#define GMAC_CONTROL_BE 0x00200000 /* Frame Burst Enable */
#define GMAC_CONTROL_JE 0x00100000 /* Jumbo frame */

#define GMAC_CONTROL_DCRS 0x00010000 /* Disable carrier sense */
#define GMAC_CONTROL_PS   0x00008000 /* Port Select 0:GMI 1:MII */
#define GMAC_CONTROL_FES  0x00004000 /* Speed 0:10 1:100 */
#define GMAC_CONTROL_DO   0x00002000 /* Disable Rx Own */
#define GMAC_CONTROL_LM   0x00001000 /* Loop-back mode */
#define GMAC_CONTROL_DM   0x00000800 /* Duplex Mode */
#define GMAC_CONTROL_IPC  0x00000400 /* Checksum Offload */
#define GMAC_CONTROL_DR   0x00000200 /* Disable Retry */
#define GMAC_CONTROL_LUD  0x00000100 /* Link up/down */
#define GMAC_CONTROL_ACS  0x00000080 /* Auto Pad/FCS Stripping */
#define GMAC_CONTROL_DC   0x00000010 /* Deferral Check */
#define GMAC_CONTROL_TE   0x00000008 /* Transmitter Enable */
#define GMAC_CONTROL_RE   0x00000004 /* Receiver Enable */

#define GMAC_CORE_INIT (GMAC_CONTROL_JD | GMAC_CONTROL_PS | GMAC_CONTROL_ACS | GMAC_CONTROL_BE | GMAC_CONTROL_DCRS)

#define GMAC_CONTROL       0x00000000 /* Configuration */
#define GMAC_FRAME_FILTER  0x00000004 /* Frame Filter */
#define GMAC_HASH_HIGH     0x00000008 /* Multicast Hash Table High */
#define GMAC_HASH_LOW      0x0000000c /* Multicast Hash Table Low */
#define GMAC_MII_ADDR      0x00000010 /* MII Address */
#define GMAC_MII_DATA      0x00000014 /* MII Data */
#define GMAC_FLOW_CTRL     0x00000018 /* Flow Control */
#define GMAC_VLAN_TAG      0x0000001c /* VLAN Tag */
#define GMAC_DEBUG         0x00000024 /* GMAC debug register */
#define GMAC_WAKEUP_FILTER 0x00000028 /* Wake-up Frame Filter */

#define GMAC_INT_STATUS         0x00000038 /* interrupt status register */
#define GMAC_INT_STATUS_PMT     BIT(3)
#define GMAC_INT_STATUS_MMCIS   BIT(4)
#define GMAC_INT_STATUS_MMCRIS  BIT(5)
#define GMAC_INT_STATUS_MMCTIS  BIT(6)
#define GMAC_INT_STATUS_MMCCSUM BIT(7)
#define GMAC_INT_STATUS_TSTAMP  BIT(9)
#define GMAC_INT_STATUS_LPIIS   BIT(10)

/* interrupt mask register */
#define GMAC_INT_MASK              0x0000003c
#define GMAC_INT_DISABLE_RGMII     BIT(0)
#define GMAC_INT_DISABLE_PCSLINK   BIT(1)
#define GMAC_INT_DISABLE_PCSAN     BIT(2)
#define GMAC_INT_DISABLE_PMT       BIT(3)
#define GMAC_INT_DISABLE_TIMESTAMP BIT(9)
#define GMAC_INT_DISABLE_PCS       (GMAC_INT_DISABLE_RGMII | GMAC_INT_DISABLE_PCSLINK | GMAC_INT_DISABLE_PCSAN)
#define GMAC_INT_DEFAULT_MASK      (GMAC_INT_DISABLE_TIMESTAMP | GMAC_INT_DISABLE_PCS)

/* PMT Control and Status */
#define GMAC_PMT 0x0000002c

/* GMAC HW ADDR regs */
#define GMAC_ADDR_HIGH(reg)        (((reg > 15) ? 0x00000800 : 0x00000040) + (reg * 8))
#define GMAC_ADDR_LOW(reg)         (((reg > 15) ? 0x00000804 : 0x00000044) + (reg * 8))
#define GMAC_MAX_PERFECT_ADDRESSES 1

#define GMAC_PCS_BASE 0x000000c0 /* PCS register base */
#define GMAC_RGSMIIIS 0x000000d8 /* RGMII/SMII status */

/* MMC registers offset */
#define GMAC_MMC_CTRL            0x100
#define GMAC_MMC_RX_INTR         0x104
#define GMAC_MMC_TX_INTR         0x108
#define GMAC_MMC_RX_CSUM_OFFLOAD 0x208
#define GMAC_EXTHASH_BASE        0x500

/* GMII ADDR  defines */
#define GMAC_MII_ADDR_WRITE 0x00000002 /* MII Write */
#define GMAC_MII_ADDR_BUSY  0x00000001 /* MII Busy */
/* GMAC FLOW CTRL defines */
#define GMAC_FLOW_CTRL_PT_MASK  0xffff0000 /* Pause Time Mask */
#define GMAC_FLOW_CTRL_PT_SHIFT 16
#define GMAC_FLOW_CTRL_UP       0x00000008 /* Unicast pause frame enable */
#define GMAC_FLOW_CTRL_RFE      0x00000004 /* Rx Flow Control Enable */
#define GMAC_FLOW_CTRL_TFE      0x00000002 /* Tx Flow Control Enable */
#define GMAC_FLOW_CTRL_FCB_BPA  0x00000001 /* Flow Control Busy ... */

/* Flow Control defines */
#define FLOW_OFF  0
#define FLOW_RX   1
#define FLOW_TX   2
#define FLOW_AUTO (FLOW_TX | FLOW_RX)

/* Energy Efficient Ethernet (EEE)
 *
 * LPI status, timer and control register offset
 */
#define LPI_CTRL_STATUS 0x0030
#define LPI_TIMER_CTRL  0x0034

/* LPI control and status defines */
#define LPI_CTRL_STATUS_LPITXA 0x00080000 /* Enable LPI TX Automate */
#define LPI_CTRL_STATUS_PLSEN  0x00040000 /* Enable PHY Link Status */
#define LPI_CTRL_STATUS_PLS    0x00020000 /* PHY Link Status */
#define LPI_CTRL_STATUS_LPIEN  0x00010000 /* LPI Enable */
#define LPI_CTRL_STATUS_RLPIST 0x00000200 /* Receive LPI state */
#define LPI_CTRL_STATUS_TLPIST 0x00000100 /* Transmit LPI state */
#define LPI_CTRL_STATUS_RLPIEX 0x00000008 /* Receive LPI Exit */
#define LPI_CTRL_STATUS_RLPIEN 0x00000004 /* Receive LPI Entry */
#define LPI_CTRL_STATUS_TLPIEX 0x00000002 /* Transmit LPI Exit */
#define LPI_CTRL_STATUS_TLPIEN 0x00000001 /* Transmit LPI Entry */

/* PCS registers (AN/TBI/SGMII/RGMII) offsets */
#define GMAC_AN_CTRL(x)   (x)        /* AN control */
#define GMAC_AN_STATUS(x) (x + 0x4)  /* AN status */
#define GMAC_ANE_ADV(x)   (x + 0x8)  /* ANE Advertisement */
#define GMAC_ANE_LPA(x)   (x + 0xc)  /* ANE link partener ability */
#define GMAC_ANE_EXP(x)   (x + 0x10) /* ANE expansion */
#define GMAC_TBI(x)       (x + 0x14) /* TBI extend status */

/* AN Configuration defines */
#define GMAC_AN_CTRL_RAN    (1 << 9)  /* Restart Auto-Negotiation */
#define GMAC_AN_CTRL_ANE    (1 << 12) /* Auto-Negotiation Enable */
#define GMAC_AN_CTRL_ELE    (1 << 14) /* External Loopback Enable */
#define GMAC_AN_CTRL_ECD    (1 << 16) /* Enable Comma Detect */
#define GMAC_AN_CTRL_LR     (1 << 17) /* Lock to Reference */
#define GMAC_AN_CTRL_SGMRAL (1 << 18) /* SGMII RAL Control */

/* AN Status defines */
#define GMAC_AN_STATUS_LS  BIT(2) /* Link Status 0:down 1:up */
#define GMAC_AN_STATUS_ANA BIT(3) /* Auto-Negotiation Ability */
#define GMAC_AN_STATUS_ANC BIT(5) /* Auto-Negotiation Complete */
#define GMAC_AN_STATUS_ES  BIT(8) /* Extended Status */

/* ADV and LPA defines */
#define GMAC_ANE_FD        BIT(5)
#define GMAC_ANE_HD        BIT(6)
#define GMAC_ANE_PSE       GENMASK(8, 7)
#define GMAC_ANE_PSE_SHIFT 7
#define GMAC_ANE_RFE       GENMASK(13, 12)
#define GMAC_ANE_RFE_SHIFT 12
#define GMAC_ANE_ACK       BIT(14)

/* Duplex, half or full. */
#define DUPLEX_HALF    0x00
#define DUPLEX_FULL    0x01
#define DUPLEX_UNKNOWN 0xff

#define PTP_XGMAC_OFFSET   0xd00
#define PTP_GMAC4_OFFSET   0xb00
#define PTP_GMAC3_X_OFFSET 0x700

/* IEEE 1588 PTP register offsets */
#define PTP_TCR    0x00 /* Timestamp Control Reg */
#define PTP_SSIR   0x04 /* Sub-Second Increment Reg */
#define PTP_STSR   0x08 /* System Time �C Seconds Regr */
#define PTP_STNSR  0x0c /* System Time �C Nanoseconds Reg */
#define PTP_STSUR  0x10 /* System Time �C Seconds Update Reg */
#define PTP_STNSUR 0x14 /* System Time �C Nanoseconds Update Reg */
#define PTP_TAR    0x18 /* Timestamp Addend Reg */

#define PTP_STNSUR_ADDSUB_SHIFT   31
#define PTP_DIGITAL_ROLLOVER_MODE 0x3B9ACA00 /* 10e9-1 ns */
#define PTP_BINARY_ROLLOVER_MODE  0x80000000 /* ~0.466 ns */

/* PTP Timestamp control register defines */
#define PTP_TCR_TSENA     BIT(0) /* Timestamp Enable */
#define PTP_TCR_TSCFUPDT  BIT(1) /* Timestamp Fine/Coarse Update */
#define PTP_TCR_TSINIT    BIT(2) /* Timestamp Initialize */
#define PTP_TCR_TSUPDT    BIT(3) /* Timestamp Update */
#define PTP_TCR_TSTRIG    BIT(4) /* Timestamp Interrupt Trigger Enable */
#define PTP_TCR_TSADDREG  BIT(5) /* Addend Reg Update */
#define PTP_TCR_TSENALL   BIT(8) /* Enable Timestamp for All Frames */
#define PTP_TCR_TSCTRLSSR BIT(9) /* Digital or Binary Rollover Control */
/* Enable PTP packet Processing for Version 2 Format */
#define PTP_TCR_TSVER2ENA BIT(10)
/* Enable Processing of PTP over Ethernet Frames */
#define PTP_TCR_TSIPENA BIT(11)
/* Enable Processing of PTP Frames Sent over IPv6-UDP */
#define PTP_TCR_TSIPV6ENA BIT(12)
/* Enable Processing of PTP Frames Sent over IPv4-UDP */
#define PTP_TCR_TSIPV4ENA BIT(13)
/* Enable Timestamp Snapshot for Event Messages */
#define PTP_TCR_TSEVNTENA BIT(14)
/* Enable Snapshot for Messages Relevant to Master */
#define PTP_TCR_TSMSTRENA BIT(15)
/* Select PTP packets for Taking Snapshots */
#define PTP_TCR_SNAPTYPSEL_1       BIT(16)
#define PTP_GMAC4_TCR_SNAPTYPSEL_1 GENMASK(17, 16)
/* Enable MAC address for PTP Frame Filtering */
#define PTP_TCR_TSENMACADDR BIT(18)

/* SSIR defines */
#define PTP_SSIR_SSINC_MASK        0xff
#define GMAC4_PTP_SSIR_SSINC_SHIFT 16

/* Normal receive descriptor defines */

/* RDES0 */
#define RDES0_PAYLOAD_CSUM_ERR BIT(0)
#define RDES0_CRC_ERROR        BIT(1)
#define RDES0_DRIBBLING        BIT(2)
#define RDES0_MII_ERROR        BIT(3)
#define RDES0_RECEIVE_WATCHDOG BIT(4)
#define RDES0_FRAME_TYPE       BIT(5)
#define RDES0_COLLISION        BIT(6)
#define RDES0_IPC_CSUM_ERROR   BIT(7)
#define RDES0_LAST_DESCRIPTOR  BIT(8)
#define RDES0_FIRST_DESCRIPTOR BIT(9)
#define RDES0_VLAN_TAG         BIT(10)
#define RDES0_OVERFLOW_ERROR   BIT(11)
#define RDES0_LENGTH_ERROR     BIT(12)
#define RDES0_SA_FILTER_FAIL   BIT(13)
#define RDES0_DESCRIPTOR_ERROR BIT(14)
#define RDES0_ERROR_SUMMARY    BIT(15)
#define RDES0_FRAME_LEN_MASK   GENMASK(29, 16)
#define RDES0_FRAME_LEN_SHIFT  16
#define RDES0_DA_FILTER_FAIL   BIT(30)
#define RDES0_OWN              BIT(31)
/* RDES1 */
#define RDES1_BUFFER1_SIZE_MASK      GENMASK(10, 0)
#define RDES1_BUFFER2_SIZE_MASK      GENMASK(21, 11)
#define RDES1_BUFFER2_SIZE_SHIFT     11
#define RDES1_SECOND_ADDRESS_CHAINED BIT(24)
#define RDES1_END_RING               BIT(25)
#define RDES1_DISABLE_IC             BIT(31)

/* Enhanced receive descriptor defines */

/* RDES0 (similar to normal RDES) */
#define ERDES0_RX_MAC_ADDR BIT(0)

/* RDES1: completely differ from normal desc definitions */
#define ERDES1_BUFFER1_SIZE_MASK      GENMASK(12, 0)
#define ERDES1_SECOND_ADDRESS_CHAINED BIT(14)
#define ERDES1_END_RING               BIT(15)
#define ERDES1_BUFFER2_SIZE_MASK      GENMASK(28, 16)
#define ERDES1_BUFFER2_SIZE_SHIFT     16
#define ERDES1_DISABLE_IC             BIT(31)

/* Normal transmit descriptor defines */
/* TDES0 */
#define TDES0_DEFERRED             BIT(0)
#define TDES0_UNDERFLOW_ERROR      BIT(1)
#define TDES0_EXCESSIVE_DEFERRAL   BIT(2)
#define TDES0_COLLISION_COUNT_MASK GENMASK(6, 3)
#define TDES0_VLAN_FRAME           BIT(7)
#define TDES0_EXCESSIVE_COLLISIONS BIT(8)
#define TDES0_LATE_COLLISION       BIT(9)
#define TDES0_NO_CARRIER           BIT(10)
#define TDES0_LOSS_CARRIER         BIT(11)
#define TDES0_PAYLOAD_ERROR        BIT(12)
#define TDES0_FRAME_FLUSHED        BIT(13)
#define TDES0_JABBER_TIMEOUT       BIT(14)
#define TDES0_ERROR_SUMMARY        BIT(15)
#define TDES0_IP_HEADER_ERROR      BIT(16)
#define TDES0_TIME_STAMP_STATUS    BIT(17)
#define TDES0_OWN                  ((unsigned int)BIT(31)) /* silence sparse */
/* TDES1 */
#define TDES1_BUFFER1_SIZE_MASK        GENMASK(10, 0)
#define TDES1_BUFFER2_SIZE_MASK        GENMASK(21, 11)
#define TDES1_BUFFER2_SIZE_SHIFT       11
#define TDES1_TIME_STAMP_ENABLE        BIT(22)
#define TDES1_DISABLE_PADDING          BIT(23)
#define TDES1_SECOND_ADDRESS_CHAINED   BIT(24)
#define TDES1_END_RING                 BIT(25)
#define TDES1_CRC_DISABLE              BIT(26)
#define TDES1_CHECKSUM_INSERTION_MASK  GENMASK(28, 27)
#define TDES1_CHECKSUM_INSERTION_SHIFT 27
#define TDES1_FIRST_SEGMENT            BIT(29)
#define TDES1_LAST_SEGMENT             BIT(30)
#define TDES1_INTERRUPT                BIT(31)

/* Enhanced transmit descriptor defines */
/* TDES0 */
#define ETDES0_DEFERRED                 BIT(0)
#define ETDES0_UNDERFLOW_ERROR          BIT(1)
#define ETDES0_EXCESSIVE_DEFERRAL       BIT(2)
#define ETDES0_COLLISION_COUNT_MASK     GENMASK(6, 3)
#define ETDES0_VLAN_FRAME               BIT(7)
#define ETDES0_EXCESSIVE_COLLISIONS     BIT(8)
#define ETDES0_LATE_COLLISION           BIT(9)
#define ETDES0_NO_CARRIER               BIT(10)
#define ETDES0_LOSS_CARRIER             BIT(11)
#define ETDES0_PAYLOAD_ERROR            BIT(12)
#define ETDES0_FRAME_FLUSHED            BIT(13)
#define ETDES0_JABBER_TIMEOUT           BIT(14)
#define ETDES0_ERROR_SUMMARY            BIT(15)
#define ETDES0_IP_HEADER_ERROR          BIT(16)
#define ETDES0_TIME_STAMP_STATUS        BIT(17)
#define ETDES0_SECOND_ADDRESS_CHAINED   BIT(20)
#define ETDES0_END_RING                 BIT(21)
#define ETDES0_CHECKSUM_INSERTION_MASK  GENMASK(23, 22)
#define ETDES0_CHECKSUM_INSERTION_SHIFT 22
#define ETDES0_TIME_STAMP_ENABLE        BIT(25)
#define ETDES0_DISABLE_PADDING          BIT(26)
#define ETDES0_CRC_DISABLE              BIT(27)
#define ETDES0_FIRST_SEGMENT            BIT(28)
#define ETDES0_LAST_SEGMENT             BIT(29)
#define ETDES0_INTERRUPT                BIT(30)
#define ETDES0_OWN                      ((unsigned int)BIT(31)) /* silence sparse */
/* TDES1 */
#define ETDES1_BUFFER1_SIZE_MASK  GENMASK(12, 0)
#define ETDES1_BUFFER2_SIZE_MASK  GENMASK(28, 16)
#define ETDES1_BUFFER2_SIZE_SHIFT 16

/* Extended Receive descriptor definitions */
#define ERDES4_IP_PAYLOAD_TYPE_MASK     (2, 6)
#define ERDES4_IP_HDR_ERR               BIT(3)
#define ERDES4_IP_PAYLOAD_ERR           BIT(4)
#define ERDES4_IP_CSUM_BYPASSED         BIT(5)
#define ERDES4_IPV4_PKT_RCVD            BIT(6)
#define ERDES4_IPV6_PKT_RCVD            BIT(7)
#define ERDES4_MSG_TYPE_MASK            GENMASK(11, 8)
#define ERDES4_PTP_FRAME_TYPE           BIT(12)
#define ERDES4_PTP_VER                  BIT(13)
#define ERDES4_TIMESTAMP_DROPPED        BIT(14)
#define ERDES4_AV_PKT_RCVD              BIT(16)
#define ERDES4_AV_TAGGED_PKT_RCVD       BIT(17)
#define ERDES4_VLAN_TAG_PRI_VAL_MASK    GENMASK(20, 18)
#define ERDES4_L3_FILTER_MATCH          BIT(24)
#define ERDES4_L4_FILTER_MATCH          BIT(25)
#define ERDES4_L3_L4_FILT_NO_MATCH_MASK GENMASK(27, 26)

/* Extended RDES4 message type definitions */
#define RDES_EXT_NO_PTP            0x0
#define RDES_EXT_SYNC              0x1
#define RDES_EXT_FOLLOW_UP         0x2
#define RDES_EXT_DELAY_REQ         0x3
#define RDES_EXT_DELAY_RESP        0x4
#define RDES_EXT_PDELAY_REQ        0x5
#define RDES_EXT_PDELAY_RESP       0x6
#define RDES_EXT_PDELAY_FOLLOW_UP  0x7
#define RDES_PTP_ANNOUNCE          0x8
#define RDES_PTP_MANAGEMENT        0x9
#define RDES_PTP_SIGNALING         0xa
#define RDES_PTP_PKT_RESERVED_TYPE 0xf

#define MII_BUSY          0x00000001
#define MII_WRITE         0x00000002
#define PHY_MAX_ADDR      32
#define PHY_INIT_TIMEOUT  100000
#define PHY_STATE_TIME    1
#define PHY_FORCE_TIMEOUT 10
#define PHY_AN_TIMEOUT    10

/* Generic MII registers. */
#define MII_BMCR        0x00 /* Basic mode control register */
#define MII_BMSR        0x01 /* Basic mode status register  */
#define MII_PHYSID1     0x02 /* PHYS ID 1                   */
#define MII_PHYSID2     0x03 /* PHYS ID 2                   */
#define MII_ADVERTISE   0x04 /* Advertisement control reg   */
#define MII_LPA         0x05 /* Link partner ability reg    */
#define MII_EXPANSION   0x06 /* Expansion register          */
#define MII_CTRL1000    0x09 /* 1000BASE-T control          */
#define MII_STAT1000    0x0a /* 1000BASE-T status           */
#define MII_MMD_CTRL    0x0d /* MMD Access Control Register */
#define MII_MMD_DATA    0x0e /* MMD Access Data Register */
#define MII_ESTATUS     0x0f /* Extended Status             */
#define MII_DCOUNTER    0x12 /* Disconnect counter          */
#define MII_FCSCOUNTER  0x13 /* False carrier counter       */
#define MII_NWAYTEST    0x14 /* N-way auto-neg test reg     */
#define MII_RERRCOUNTER 0x15 /* Receive error counter       */
#define MII_SREVISION   0x16 /* Silicon revision            */
#define MII_RESV1       0x17 /* Reserved...                 */
#define MII_LBRERROR    0x18 /* Lpback, rx, bypass error    */
#define MII_PHYADDR     0x19 /* PHY address                 */
#define MII_RESV2       0x1a /* Reserved...                 */
#define MII_TPISTATUS   0x1b /* TPI status for 10mbps       */
#define MII_NCONFIG     0x1c /* Network interface config    */

/* Basic mode control register. */
#define BMCR_RESV      0x003f /* Unused...                   */
#define BMCR_SPEED1000 0x0040 /* MSB of Speed (1000)         */
#define BMCR_CTST      0x0080 /* Collision test              */
#define BMCR_FULLDPLX  0x0100 /* Full duplex                 */
#define BMCR_ANRESTART 0x0200 /* Auto negotiation restart    */
#define BMCR_ISOLATE   0x0400 /* Isolate data paths from MII */
#define BMCR_PDOWN     0x0800 /* Enable low power state      */
#define BMCR_ANENABLE  0x1000 /* Enable auto negotiation     */
#define BMCR_SPEED100  0x2000 /* Select 100Mbps              */
#define BMCR_LOOPBACK  0x4000 /* TXD loopback bits           */
#define BMCR_RESET     0x8000 /* Reset to default state      */
#define BMCR_SPEED10   0x0000 /* Select 10Mbps               */

/* Basic mode status register. */
#define BMSR_ERCAP        0x0001 /* Ext-reg capability          */
#define BMSR_JCD          0x0002 /* Jabber detected             */
#define BMSR_LSTATUS      0x0004 /* Link status                 */
#define BMSR_ANEGCAPABLE  0x0008 /* Able to do auto-negotiation */
#define BMSR_RFAULT       0x0010 /* Remote fault detected       */
#define BMSR_ANEGCOMPLETE 0x0020 /* Auto-negotiation complete   */
#define BMSR_RESV         0x00c0 /* Unused...                   */
#define BMSR_ESTATEN      0x0100 /* Extended Status in R15      */
#define BMSR_100HALF2     0x0200 /* Can do 100BASE-T2 HDX       */
#define BMSR_100FULL2     0x0400 /* Can do 100BASE-T2 FDX       */
#define BMSR_10HALF       0x0800 /* Can do 10mbps, half-duplex  */
#define BMSR_10FULL       0x1000 /* Can do 10mbps, full-duplex  */
#define BMSR_100HALF      0x2000 /* Can do 100mbps, half-duplex */
#define BMSR_100FULL      0x4000 /* Can do 100mbps, full-duplex */
#define BMSR_100BASE4     0x8000 /* Can do 100mbps, 4k packets  */

/* Advertisement control register. */
#define ADVERTISE_SLCT          0x001f /* Selector bits               */
#define ADVERTISE_CSMA          0x0001 /* Only selector supported     */
#define ADVERTISE_10HALF        0x0020 /* Try for 10mbps half-duplex  */
#define ADVERTISE_1000XFULL     0x0020 /* Try for 1000BASE-X full-duplex */
#define ADVERTISE_10FULL        0x0040 /* Try for 10mbps full-duplex  */
#define ADVERTISE_1000XHALF     0x0040 /* Try for 1000BASE-X half-duplex */
#define ADVERTISE_100HALF       0x0080 /* Try for 100mbps half-duplex */
#define ADVERTISE_1000XPAUSE    0x0080 /* Try for 1000BASE-X pause    */
#define ADVERTISE_100FULL       0x0100 /* Try for 100mbps full-duplex */
#define ADVERTISE_1000XPSE_ASYM 0x0100 /* Try for 1000BASE-X asym pause */
#define ADVERTISE_100BASE4      0x0200 /* Try for 100mbps 4k packets  */
#define ADVERTISE_PAUSE_CAP     0x0400 /* Try for pause               */
#define ADVERTISE_PAUSE_ASYM    0x0800 /* Try for asymetric pause     */
#define ADVERTISE_RESV          0x1000 /* Unused...                   */
#define ADVERTISE_RFAULT        0x2000 /* Say we can detect faults    */
#define ADVERTISE_LPACK         0x4000 /* Ack link partners response  */
#define ADVERTISE_NPAGE         0x8000 /* Next page bit               */

#define ADVERTISE_FULL (ADVERTISE_100FULL | ADVERTISE_10FULL | ADVERTISE_CSMA)
#define ADVERTISE_ALL  (ADVERTISE_10HALF | ADVERTISE_10FULL | ADVERTISE_100HALF | ADVERTISE_100FULL)

/* Link partner ability register. */
#define LPA_SLCT            0x001f /* Same as advertise selector  */
#define LPA_10HALF          0x0020 /* Can do 10mbps half-duplex   */
#define LPA_1000XFULL       0x0020 /* Can do 1000BASE-X full-duplex */
#define LPA_10FULL          0x0040 /* Can do 10mbps full-duplex   */
#define LPA_1000XHALF       0x0040 /* Can do 1000BASE-X half-duplex */
#define LPA_100HALF         0x0080 /* Can do 100mbps half-duplex  */
#define LPA_1000XPAUSE      0x0080 /* Can do 1000BASE-X pause     */
#define LPA_100FULL         0x0100 /* Can do 100mbps full-duplex  */
#define LPA_1000XPAUSE_ASYM 0x0100 /* Can do 1000BASE-X pause asym*/
#define LPA_100BASE4        0x0200 /* Can do 100mbps 4k packets   */
#define LPA_PAUSE_CAP       0x0400 /* Can pause                   */
#define LPA_PAUSE_ASYM      0x0800 /* Can pause asymetrically     */
#define LPA_RESV            0x1000 /* Unused...                   */
#define LPA_RFAULT          0x2000 /* Link partner faulted        */
#define LPA_LPACK           0x4000 /* Link partner acked us       */
#define LPA_NPAGE           0x8000 /* Next page bit               */

#define LPA_DUPLEX (LPA_10FULL | LPA_100FULL)
#define LPA_100    (LPA_100FULL | LPA_100HALF | LPA_100BASE4)

/* Expansion register for auto-negotiation. */
#define EXPANSION_NWAY        0x0001 /* Can do N-way auto-nego      */
#define EXPANSION_LCWP        0x0002 /* Got new RX page code word   */
#define EXPANSION_ENABLENPAGE 0x0004 /* This enables npage words    */
#define EXPANSION_NPCAPABLE   0x0008 /* Link partner supports npage */
#define EXPANSION_MFAULTS     0x0010 /* Multiple faults detected    */
#define EXPANSION_RESV        0xffe0 /* Unused...                   */

#define ESTATUS_1000_TFULL 0x2000 /* Can do 1000BT Full          */
#define ESTATUS_1000_THALF 0x1000 /* Can do 1000BT Half          */

/* N-way test register. */
#define NWAYTEST_RESV1    0x00ff /* Unused...                   */
#define NWAYTEST_LOOPBACK 0x0100 /* Enable loopback for N-way   */
#define NWAYTEST_RESV2    0xfe00 /* Unused...                   */

/* 1000BASE-T Control register */
#define ADVERTISE_1000FULL    0x0200 /* Advertise 1000BASE-T full duplex */
#define ADVERTISE_1000HALF    0x0100 /* Advertise 1000BASE-T half duplex */
#define CTL1000_AS_MASTER     0x0800
#define CTL1000_ENABLE_MASTER 0x1000

/* 1000BASE-T Status register */
#define LPA_1000MSFAIL    0x8000 /* Master/Slave resolution failure */
#define LPA_1000LOCALRXOK 0x2000 /* Link partner local receiver status */
#define LPA_1000REMRXOK   0x1000 /* Link partner remote receiver status */
#define LPA_1000FULL      0x0800 /* Link partner 1000BASE-T full duplex */
#define LPA_1000HALF      0x0400 /* Link partner 1000BASE-T half duplex */

/* Flow control flags */
#define FLOW_CTRL_TX 0x01
#define FLOW_CTRL_RX 0x02

/* MMD Access Control register fields */
#define MII_MMD_CTRL_DEVAD_MASK 0x1f   /* Mask MMD DEVAD*/
#define MII_MMD_CTRL_ADDR       0x0000 /* Address */
#define MII_MMD_CTRL_NOINCR     0x4000 /* no post increment */
#define MII_MMD_CTRL_INCR_RDWT  0x8000 /* post increment on reads & writes */
#define MII_MMD_CTRL_INCR_ON_WT 0xC000 /* post increment on writes only */

/* MDIO Manageable Devices (MMDs). */
#define MDIO_MMD_PMAPMD                                                                                                \
    1                      /* Physical Medium Attachment/                                                              \
                            * Physical Medium Dependent */
#define MDIO_MMD_WIS    2  /* WAN Interface Sublayer */
#define MDIO_MMD_PCS    3  /* Physical Coding Sublayer */
#define MDIO_MMD_PHYXS  4  /* PHY Extender Sublayer */
#define MDIO_MMD_DTEXS  5  /* DTE Extender Sublayer */
#define MDIO_MMD_TC     6  /* Transmission Convergence */
#define MDIO_MMD_AN     7  /* Auto-Negotiation */
#define MDIO_MMD_C22EXT 29 /* Clause 22 extension */
#define MDIO_MMD_VEND1  30 /* Vendor specific 1 */
#define MDIO_MMD_VEND2  31 /* Vendor specific 2 */

/* Generic MDIO registers. */
#define MDIO_CTRL1          MII_BMCR
#define MDIO_STAT1          MII_BMSR
#define MDIO_DEVID1         MII_PHYSID1
#define MDIO_DEVID2         MII_PHYSID2
#define MDIO_SPEED          4 /* Speed ability */
#define MDIO_DEVS1          5 /* Devices in package */
#define MDIO_DEVS2          6
#define MDIO_CTRL2          7  /* 10G control 2 */
#define MDIO_STAT2          8  /* 10G status 2 */
#define MDIO_PMA_TXDIS      9  /* 10G PMA/PMD transmit disable */
#define MDIO_PMA_RXDET      10 /* 10G PMA/PMD receive signal detect */
#define MDIO_PMA_EXTABLE    11 /* 10G PMA/PMD extended ability */
#define MDIO_PKGID1         14 /* Package identifier */
#define MDIO_PKGID2         15
#define MDIO_AN_ADVERTISE   16 /* AN advertising (base page) */
#define MDIO_AN_LPA         19 /* AN LP abilities (base page) */
#define MDIO_PCS_EEE_ABLE   20 /* EEE Capability register */
#define MDIO_PCS_EEE_WK_ERR 22 /* EEE wake error counter */
#define MDIO_PHYXS_LNSTAT   24 /* PHY XGXS lane state */
#define MDIO_AN_EEE_ADV     60 /* EEE advertisement */
#define MDIO_AN_EEE_LPABLE  61 /* EEE link partner ability */

enum ethtool_link_mode_bit_indices
{
    ETHTOOL_LINK_MODE_10baseT_Half_BIT           = 0,
    ETHTOOL_LINK_MODE_10baseT_Full_BIT           = 1,
    ETHTOOL_LINK_MODE_100baseT_Half_BIT          = 2,
    ETHTOOL_LINK_MODE_100baseT_Full_BIT          = 3,
    ETHTOOL_LINK_MODE_1000baseT_Half_BIT         = 4,
    ETHTOOL_LINK_MODE_1000baseT_Full_BIT         = 5,
    ETHTOOL_LINK_MODE_Autoneg_BIT                = 6,
    ETHTOOL_LINK_MODE_TP_BIT                     = 7,
    ETHTOOL_LINK_MODE_AUI_BIT                    = 8,
    ETHTOOL_LINK_MODE_MII_BIT                    = 9,
    ETHTOOL_LINK_MODE_FIBRE_BIT                  = 10,
    ETHTOOL_LINK_MODE_BNC_BIT                    = 11,
    ETHTOOL_LINK_MODE_10000baseT_Full_BIT        = 12,
    ETHTOOL_LINK_MODE_Pause_BIT                  = 13,
    ETHTOOL_LINK_MODE_Asym_Pause_BIT             = 14,
    ETHTOOL_LINK_MODE_2500baseX_Full_BIT         = 15,
    ETHTOOL_LINK_MODE_Backplane_BIT              = 16,
    ETHTOOL_LINK_MODE_1000baseKX_Full_BIT        = 17,
    ETHTOOL_LINK_MODE_10000baseKX4_Full_BIT      = 18,
    ETHTOOL_LINK_MODE_10000baseKR_Full_BIT       = 19,
    ETHTOOL_LINK_MODE_10000baseR_FEC_BIT         = 20,
    ETHTOOL_LINK_MODE_20000baseMLD2_Full_BIT     = 21,
    ETHTOOL_LINK_MODE_20000baseKR2_Full_BIT      = 22,
    ETHTOOL_LINK_MODE_40000baseKR4_Full_BIT      = 23,
    ETHTOOL_LINK_MODE_40000baseCR4_Full_BIT      = 24,
    ETHTOOL_LINK_MODE_40000baseSR4_Full_BIT      = 25,
    ETHTOOL_LINK_MODE_40000baseLR4_Full_BIT      = 26,
    ETHTOOL_LINK_MODE_56000baseKR4_Full_BIT      = 27,
    ETHTOOL_LINK_MODE_56000baseCR4_Full_BIT      = 28,
    ETHTOOL_LINK_MODE_56000baseSR4_Full_BIT      = 29,
    ETHTOOL_LINK_MODE_56000baseLR4_Full_BIT      = 30,
    ETHTOOL_LINK_MODE_25000baseCR_Full_BIT       = 31,
    ETHTOOL_LINK_MODE_25000baseKR_Full_BIT       = 32,
    ETHTOOL_LINK_MODE_25000baseSR_Full_BIT       = 33,
    ETHTOOL_LINK_MODE_50000baseCR2_Full_BIT      = 34,
    ETHTOOL_LINK_MODE_50000baseKR2_Full_BIT      = 35,
    ETHTOOL_LINK_MODE_100000baseKR4_Full_BIT     = 36,
    ETHTOOL_LINK_MODE_100000baseSR4_Full_BIT     = 37,
    ETHTOOL_LINK_MODE_100000baseCR4_Full_BIT     = 38,
    ETHTOOL_LINK_MODE_100000baseLR4_ER4_Full_BIT = 39,
    ETHTOOL_LINK_MODE_50000baseSR2_Full_BIT      = 40,
    ETHTOOL_LINK_MODE_1000baseX_Full_BIT         = 41,
    ETHTOOL_LINK_MODE_10000baseCR_Full_BIT       = 42,
    ETHTOOL_LINK_MODE_10000baseSR_Full_BIT       = 43,
    ETHTOOL_LINK_MODE_10000baseLR_Full_BIT       = 44,
    ETHTOOL_LINK_MODE_10000baseLRM_Full_BIT      = 45,
    ETHTOOL_LINK_MODE_10000baseER_Full_BIT       = 46,
    ETHTOOL_LINK_MODE_2500baseT_Full_BIT         = 47,
    ETHTOOL_LINK_MODE_5000baseT_Full_BIT         = 48,

    ETHTOOL_LINK_MODE_FEC_NONE_BIT  = 49,
    ETHTOOL_LINK_MODE_FEC_RS_BIT    = 50,
    ETHTOOL_LINK_MODE_FEC_BASER_BIT = 51,

    /* Last allowed bit for __ETHTOOL_LINK_MODE_LEGACY_MASK is bit
     * 31. Please do NOT define any SUPPORTED_* or ADVERTISED_*
     * macro for bits > 31. The only way to use indices > 31 is to
     * use the new ETHTOOL_GLINKSETTINGS/ETHTOOL_SLINKSETTINGS API.
     */

    __ETHTOOL_LINK_MODE_LAST = ETHTOOL_LINK_MODE_FEC_BASER_BIT,
};

#define __ETHTOOL_LINK_MODE_LEGACY_MASK(base_name) (1UL << (ETHTOOL_LINK_MODE_##base_name##_BIT))

#define SUPPORTED_10baseT_Half       __ETHTOOL_LINK_MODE_LEGACY_MASK(10baseT_Half)
#define SUPPORTED_10baseT_Full       __ETHTOOL_LINK_MODE_LEGACY_MASK(10baseT_Full)
#define SUPPORTED_100baseT_Half      __ETHTOOL_LINK_MODE_LEGACY_MASK(100baseT_Half)
#define SUPPORTED_100baseT_Full      __ETHTOOL_LINK_MODE_LEGACY_MASK(100baseT_Full)
#define SUPPORTED_1000baseT_Half     __ETHTOOL_LINK_MODE_LEGACY_MASK(1000baseT_Half)
#define SUPPORTED_1000baseT_Full     __ETHTOOL_LINK_MODE_LEGACY_MASK(1000baseT_Full)
#define SUPPORTED_Autoneg            __ETHTOOL_LINK_MODE_LEGACY_MASK(Autoneg)
#define SUPPORTED_TP                 __ETHTOOL_LINK_MODE_LEGACY_MASK(TP)
#define SUPPORTED_AUI                __ETHTOOL_LINK_MODE_LEGACY_MASK(AUI)
#define SUPPORTED_MII                __ETHTOOL_LINK_MODE_LEGACY_MASK(MII)
#define SUPPORTED_FIBRE              __ETHTOOL_LINK_MODE_LEGACY_MASK(FIBRE)
#define SUPPORTED_BNC                __ETHTOOL_LINK_MODE_LEGACY_MASK(BNC)
#define SUPPORTED_10000baseT_Full    __ETHTOOL_LINK_MODE_LEGACY_MASK(10000baseT_Full)
#define SUPPORTED_Pause              __ETHTOOL_LINK_MODE_LEGACY_MASK(Pause)
#define SUPPORTED_Asym_Pause         __ETHTOOL_LINK_MODE_LEGACY_MASK(Asym_Pause)
#define SUPPORTED_2500baseX_Full     __ETHTOOL_LINK_MODE_LEGACY_MASK(2500baseX_Full)
#define SUPPORTED_Backplane          __ETHTOOL_LINK_MODE_LEGACY_MASK(Backplane)
#define SUPPORTED_1000baseKX_Full    __ETHTOOL_LINK_MODE_LEGACY_MASK(1000baseKX_Full)
#define SUPPORTED_10000baseKX4_Full  __ETHTOOL_LINK_MODE_LEGACY_MASK(10000baseKX4_Full)
#define SUPPORTED_10000baseKR_Full   __ETHTOOL_LINK_MODE_LEGACY_MASK(10000baseKR_Full)
#define SUPPORTED_10000baseR_FEC     __ETHTOOL_LINK_MODE_LEGACY_MASK(10000baseR_FEC)
#define SUPPORTED_20000baseMLD2_Full __ETHTOOL_LINK_MODE_LEGACY_MASK(20000baseMLD2_Full)
#define SUPPORTED_20000baseKR2_Full  __ETHTOOL_LINK_MODE_LEGACY_MASK(20000baseKR2_Full)
#define SUPPORTED_40000baseKR4_Full  __ETHTOOL_LINK_MODE_LEGACY_MASK(40000baseKR4_Full)
#define SUPPORTED_40000baseCR4_Full  __ETHTOOL_LINK_MODE_LEGACY_MASK(40000baseCR4_Full)
#define SUPPORTED_40000baseSR4_Full  __ETHTOOL_LINK_MODE_LEGACY_MASK(40000baseSR4_Full)
#define SUPPORTED_40000baseLR4_Full  __ETHTOOL_LINK_MODE_LEGACY_MASK(40000baseLR4_Full)
#define SUPPORTED_56000baseKR4_Full  __ETHTOOL_LINK_MODE_LEGACY_MASK(56000baseKR4_Full)
#define SUPPORTED_56000baseCR4_Full  __ETHTOOL_LINK_MODE_LEGACY_MASK(56000baseCR4_Full)
#define SUPPORTED_56000baseSR4_Full  __ETHTOOL_LINK_MODE_LEGACY_MASK(56000baseSR4_Full)
#define SUPPORTED_56000baseLR4_Full  __ETHTOOL_LINK_MODE_LEGACY_MASK(56000baseLR4_Full)

#define ADVERTISED_10baseT_Half       __ETHTOOL_LINK_MODE_LEGACY_MASK(10baseT_Half)
#define ADVERTISED_10baseT_Full       __ETHTOOL_LINK_MODE_LEGACY_MASK(10baseT_Full)
#define ADVERTISED_100baseT_Half      __ETHTOOL_LINK_MODE_LEGACY_MASK(100baseT_Half)
#define ADVERTISED_100baseT_Full      __ETHTOOL_LINK_MODE_LEGACY_MASK(100baseT_Full)
#define ADVERTISED_1000baseT_Half     __ETHTOOL_LINK_MODE_LEGACY_MASK(1000baseT_Half)
#define ADVERTISED_1000baseT_Full     __ETHTOOL_LINK_MODE_LEGACY_MASK(1000baseT_Full)
#define ADVERTISED_Autoneg            __ETHTOOL_LINK_MODE_LEGACY_MASK(Autoneg)
#define ADVERTISED_TP                 __ETHTOOL_LINK_MODE_LEGACY_MASK(TP)
#define ADVERTISED_AUI                __ETHTOOL_LINK_MODE_LEGACY_MASK(AUI)
#define ADVERTISED_MII                __ETHTOOL_LINK_MODE_LEGACY_MASK(MII)
#define ADVERTISED_FIBRE              __ETHTOOL_LINK_MODE_LEGACY_MASK(FIBRE)
#define ADVERTISED_BNC                __ETHTOOL_LINK_MODE_LEGACY_MASK(BNC)
#define ADVERTISED_10000baseT_Full    __ETHTOOL_LINK_MODE_LEGACY_MASK(10000baseT_Full)
#define ADVERTISED_Pause              __ETHTOOL_LINK_MODE_LEGACY_MASK(Pause)
#define ADVERTISED_Asym_Pause         __ETHTOOL_LINK_MODE_LEGACY_MASK(Asym_Pause)
#define ADVERTISED_2500baseX_Full     __ETHTOOL_LINK_MODE_LEGACY_MASK(2500baseX_Full)
#define ADVERTISED_Backplane          __ETHTOOL_LINK_MODE_LEGACY_MASK(Backplane)
#define ADVERTISED_1000baseKX_Full    __ETHTOOL_LINK_MODE_LEGACY_MASK(1000baseKX_Full)
#define ADVERTISED_10000baseKX4_Full  __ETHTOOL_LINK_MODE_LEGACY_MASK(10000baseKX4_Full)
#define ADVERTISED_10000baseKR_Full   __ETHTOOL_LINK_MODE_LEGACY_MASK(10000baseKR_Full)
#define ADVERTISED_10000baseR_FEC     __ETHTOOL_LINK_MODE_LEGACY_MASK(10000baseR_FEC)
#define ADVERTISED_20000baseMLD2_Full __ETHTOOL_LINK_MODE_LEGACY_MASK(20000baseMLD2_Full)
#define ADVERTISED_20000baseKR2_Full  __ETHTOOL_LINK_MODE_LEGACY_MASK(20000baseKR2_Full)
#define ADVERTISED_40000baseKR4_Full  __ETHTOOL_LINK_MODE_LEGACY_MASK(40000baseKR4_Full)
#define ADVERTISED_40000baseCR4_Full  __ETHTOOL_LINK_MODE_LEGACY_MASK(40000baseCR4_Full)
#define ADVERTISED_40000baseSR4_Full  __ETHTOOL_LINK_MODE_LEGACY_MASK(40000baseSR4_Full)
#define ADVERTISED_40000baseLR4_Full  __ETHTOOL_LINK_MODE_LEGACY_MASK(40000baseLR4_Full)
#define ADVERTISED_56000baseKR4_Full  __ETHTOOL_LINK_MODE_LEGACY_MASK(56000baseKR4_Full)
#define ADVERTISED_56000baseCR4_Full  __ETHTOOL_LINK_MODE_LEGACY_MASK(56000baseCR4_Full)
#define ADVERTISED_56000baseSR4_Full  __ETHTOOL_LINK_MODE_LEGACY_MASK(56000baseSR4_Full)
#define ADVERTISED_56000baseLR4_Full  __ETHTOOL_LINK_MODE_LEGACY_MASK(56000baseLR4_Full)

/* Enable or disable autonegotiation. */
#define AUTONEG_DISABLE 0x00
#define AUTONEG_ENABLE  0x01

int          stmmac_mdio_read(struct mii_bus *bus, int phyaddr, int phyreg);
int          stmmac_mdio_write(struct mii_bus *bus, int phyaddr, int phyreg, unsigned short phydata);
int          mdiobus_scan(struct mii_bus *bus, int addr, unsigned int *phy_id);
int          phy_resume(struct phy_device *phydev);
int          phy_init_hw(struct phy_device *phydev);
int          __phy_resume(struct phy_device *phydev);
int          phy_aneg_done(struct phy_device *phydev);
int          phy_read_status(struct phy_device *phydev);
void         phy_trigger_machine(struct phy_device *phydev);
int          phy_start_aneg_priv(struct phy_device *phydev, unsigned int sync);
int          genphy_update_link(struct phy_device *phydev);
void         stmmac_clk_csr_set(int *clk_csr);
unsigned int stmmac_get_id(void *ioaddr, unsigned int id_reg);
void         dwmac_enable_dma_transmission(void *ioaddr);
void         dwmac_enable_dma_irq(void *ioaddr, unsigned int chan);
void         dwmac_disable_dma_irq(void *ioaddr, unsigned int chan);
void         dwmac_dma_start_tx(void *ioaddr, unsigned int chan);
void         dwmac_dma_stop_tx(void *ioaddr, unsigned int chan);
void         dwmac_dma_start_rx(void *ioaddr, unsigned int chan);
void         dwmac_dma_stop_rx(void *ioaddr, unsigned int chan);
int          dwmac_dma_interrupt(void *ioaddr, unsigned int chan);
int          dwmac_dma_reset(void *ioaddr);

#endif
