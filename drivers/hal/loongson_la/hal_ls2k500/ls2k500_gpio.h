#ifndef __LS2K_500_GPIO_H__

#define __LS2K_500_GPIO_H__

#define LS2k500_GPIO_NUM     (155)
#define LS2k500_GPIO_IRQ_NUM (123)

#define GPIO_MUX_CONFIG_BASE         (0x800000001fe10490)
#define GPIO_MUX_CONFIG_PINS_PER_REG (8)
#define GPIO_MUX_CONFIG_MASK         (0x7)
#define GPIO_MUX_CONFIG_BITS         (4)

#define GPIO_OEN_BASE_0       (0x800000001fe10430)
#define GPIO_OEN_BASE_1       (0x800000001fe10434)
#define GPIO_OEN_BASE_2       (0x800000001fe10450)
#define GPIO_OEN_BASE_3       (0x800000001fe10454)
#define GPIO_OEN_BASE_4       (0x800000001fe10470)
#define GPIO_OEN_BASE_5       (0x800000001fe10474)
#define GPIO_OEN_PINS_PER_REG (32)
#define GPIO_OEN_MASK         (0x1)
#define GPIO_OEN_BITS         (1)

#define GPIO_OUT_BASE_0       (0x800000001fe10440)
#define GPIO_OUT_BASE_1       (0x800000001fe10444)
#define GPIO_OUT_BASE_2       (0x800000001fe10460)
#define GPIO_OUT_BASE_3       (0x800000001fe10464)
#define GPIO_OUT_BASE_4       (0x800000001fe10480)
#define GPIO_OUT_BASE_5       (0x800000001fe10484)
#define GPIO_OUT_PINS_PER_REG (32)
#define GPIO_OUT_MASK         (0x1)
#define GPIO_OUT_BITS         (1)

#define GPIO_IN_BASE_0       (0x800000001fe10438)
#define GPIO_IN_BASE_1       (0x800000001fe1043c)
#define GPIO_IN_BASE_2       (0x800000001fe10458)
#define GPIO_IN_BASE_3       (0x800000001fe1045c)
#define GPIO_IN_BASE_4       (0x800000001fe10478)
#define GPIO_IN_BASE_5       (0x800000001fe1047c)
#define GPIO_IN_PINS_PER_REG (32)
#define GPIO_IN_MASK         (0x1)
#define GPIO_IN_BITS         (1)

#define GPIO_INTEN_BASE         (0x800000001fe104e0)
#define GPIO_INTEN_PINS_PER_REG (32)
#define GPIO_INTEN_MASK         (0x1)
#define GPIO_INTEN_BITS         (1)

enum gpio_function
{
    GPIO_FUNC_GPIO = 0x00,
    GPIO_FUNC_1    = 0x01,
    GPIO_FUNC_2    = 0x02,
    GPIO_FUNC_3    = 0x03,
    GPIO_FUNC_4    = 0x04,
    GPIO_FUNC_MAIN = 0x05,
};

enum gpio_direction
{
    GPIO_DIRECTION_OUT = 0,
    GPIO_DIRECTION_IN  = 1,
};

#define LS2K_GPIO0_3_IRQ     (87)
#define LS2K_GPIO124_127_IRQ (117)

void gpio_set_func(enum gpio_function func, unsigned int pin);
void gpio_set_direction(enum gpio_direction dir, unsigned int pin);
void gpio_port_set_value(int value, unsigned int pin);
int  gpio_port_get_value(unsigned int pin);
void gpio_set_interrupt_enable(unsigned int pin);
void gpio_set_interrupt_disable(unsigned int pin);

#endif
