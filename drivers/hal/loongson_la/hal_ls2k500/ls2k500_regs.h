/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        ls2k500_regs.h
 *
 * @brief       This file provides ostick function.
 *
 * @revision
 * Date         Author          Notes
 * 2020-11-17   OneOS Team      First version.
 ***********************************************************************************************************************
 */
#ifndef LS2K500_REGS_H__
#define LS2K500_REGS_H__

#include <os_types.h>

#define KUSEG_ADDR      0x0
#define CACHED_MEMORY_ADDR  0x8000000000000000
#define UNCACHED_MEMORY_ADDR    0x8000000000000000
#define MAX_MEM_ADDR        PHYS_TO_UNCACHED(0x1e000000)
#define RESERVED_ADDR       PHYS_TO_UNCACHED(0x1fc80000)
#define IS_CACHED_ADDR(x)   (!!(((x) & 0xff00000000000000ULL) == CACHED_MEMORY_ADDR))

#define CACHED_TO_PHYS(x)   VA_TO_PHYS(x)
#define UNCACHED_TO_PHYS(x) VA_TO_PHYS(x)

#define __le16_to_cpu(x) ((unsigned short)(x))

typedef unsigned long   vm_offset_t;
#define VA_TO_PA(x)     UNCACHED_TO_PHYS(x)
#define PA_TO_VA(x)     PHYS_TO_CACHED(x)
#define _pci_dmamap(va, len) (VA_TO_PA(va))
#define vtophys(p)          _pci_dmamap((vm_offset_t)p, 1)

#define PHYS_TO_CACHED(x)   ((unsigned long long)(x) | CACHED_MEMORY_ADDR)
#define PHYS_TO_UNCACHED(x)     ((unsigned long long)(x) | UNCACHED_MEMORY_ADDR)
#define CACHED_TO_UNCACHED(x)   (PHYS_TO_UNCACHED(VA_TO_PHYS(x)))
#define UNCACHED_TO_CACHED(x)   (PHYS_TO_CACHED(VA_TO_PHYS(x)))
#define VA_TO_PHYS(x)       ((unsigned long)(x) & 0xffffffffffffUL)

/* ACPI regs */
#define LS2K500_ACPI_REG_BASE            PHYS_TO_UNCACHED(0x1ff6c000)
#define LS2K500_PM_SOC_REG              (LS2K500_ACPI_REG_BASE + 0x0000)
#define LS2K500_PM_RESUME_REG           (LS2K500_ACPI_REG_BASE + 0x0004)
#define LS2K500_PM_RTC_REG              (LS2K500_ACPI_REG_BASE + 0x0008)
#define LS2K500_PM1_STS_REG             (LS2K500_ACPI_REG_BASE + 0x000c)
#define LS2K500_PM1_EN_REG              (LS2K500_ACPI_REG_BASE + 0x0010)
#define LS2K500_PM1_CNT_REG             (LS2K500_ACPI_REG_BASE + 0x0014)
#define LS2K500_PM1_TMR_REG             (LS2K500_ACPI_REG_BASE + 0x0018)
#define LS2K500_P_CNT_REG               (LS2K500_ACPI_REG_BASE + 0x001c)
#define LS2K500_P_LVL2_REG              (LS2K500_ACPI_REG_BASE + 0x0020)
#define LS2K500_P_LVL3_REG              (LS2K500_ACPI_REG_BASE + 0x0024)
#define LS2K500_GPE0_STS_REG            (LS2K500_ACPI_REG_BASE + 0x0028)
#define LS2K500_GPE0_EN_REG             (LS2K500_ACPI_REG_BASE + 0x002c)
#define LS2K500_RST_CNT_REG             (LS2K500_ACPI_REG_BASE + 0x0030)
#define LS2K500_WD_SET_REG              (LS2K500_ACPI_REG_BASE + 0x0034)
#define LS2K500_WD_TIMER_REG            (LS2K500_ACPI_REG_BASE + 0x0038)
#define LS2K500_DVFS_CNT_REG            (LS2K500_ACPI_REG_BASE + 0x003c)
#define LS2K500_DVFS_STS_REG            (LS2K500_ACPI_REG_BASE + 0x0040)
#define LS2K500_MS_CNT_REG              (LS2K500_ACPI_REG_BASE + 0x0044)
#define LS2K500_MS_THT_REG              (LS2K500_ACPI_REG_BASE + 0x0048)
#define LS2K500_THSENS_CNT_REG          (LS2K500_ACPI_REG_BASE + 0x004c)
#define LS2K500_GEN_RTC1_REG            (LS2K500_ACPI_REG_BASE + 0x0050)
#define LS2K500_GEN_RTC2_REG            (LS2K500_ACPI_REG_BASE + 0x0054)

/* Timer registers */
#define CSR_TCFG_VAL_SHIFT    2UL
#define CSR_TCFG_VAL          (0x3fffffffffffUL << CSR_TCFG_VAL_SHIFT)
#define CSR_TCFG_PERIOD_SHIFT 1UL
#define CSR_TCFG_PERIOD       (0x1UL << CSR_TCFG_PERIOD_SHIFT)
#define CSR_TCFG_EN           (0x1UL)

#define CSR_TINTCLR_TI_SHIFT 0UL
#define CSR_TINTCLR_TI       (1UL << CSR_TINTCLR_TI_SHIFT)

#define LOONGARCH_CPUCFG2 0x2
#define LOONGARCH_CPUCFG4 0x4
#define LOONGARCH_CPUCFG5 0x5

#define CPUCFG2_LLFTP (1UL << 14)

#define CSR_ECFG_LIE_MASK   (0x1FFF)
#define CSR_ECFG_LIE_OFFSET (0)
#define CSR_ECFG_VS_MASK    (0x7)
#define CSR_ECFG_VS_OFFSET  (16)

#define CAUSEF_TI  ((unsigned int)(1) << 11)
#define CAUSEF_IP2 ((unsigned int)(1) << 2)
#define CAUSEF_IP3 ((unsigned int)(1) << 3)
#define CAUSEF_IP4 ((unsigned int)(1) << 4)
#define CAUSEF_IP5 ((unsigned int)(1) << 5)

#define readb(reg) (*((volatile unsigned char *)(reg)))
#define readw(reg) (*((volatile unsigned short *)(reg)))
#define readl(reg) (*((volatile unsigned int *)(reg)))
#define readq(reg) (*((volatile unsigned long *)(reg)))

#define writeb(data, reg) ((*((volatile unsigned char *)(reg))) = (unsigned char)(data))
#define writew(data, reg) ((*((volatile unsigned short *)(reg))) = (unsigned short)(data))
#define writel(data, reg) ((*((volatile unsigned int *)(reg))) = (unsigned int)(data))
#define writeq(data, reg) ((*((volatile unsigned long *)(reg))) = (unsigned long)(data))

os_ubase_t    ecfg_csrxchg(os_ubase_t val, os_ubase_t mask);
os_ubase_t    ecfg_csrrd(void);
void          ecfg_csrwr(os_ubase_t val);
os_ubase_t    estate_csrrd(void);
void          estate_csrwr(os_ubase_t val);
os_ubase_t    eentry_csrxchg(os_ubase_t val, os_ubase_t mask);
os_ubase_t    eentry_csrrd(void);
void          eentry_csrwr(os_ubase_t val);
unsigned long tcfg_csrrd(void);
void          tcfg_csrwr(unsigned long val);
void          ticlr_csrwr(unsigned long val);
unsigned int  read_cfg(unsigned int reg);
unsigned long drdtime(void);

#endif
