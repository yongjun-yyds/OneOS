/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        ls2k_clk.c
 *
 * @brief       This file implements clock driver for loongson ls2k500
 *
 * @revision
 * Date         Author          Notes
 * 2020-11-17   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include <stdio.h>
#include "ls2k_clk.h"
#include <ls2k500_regs.h>

int clk_get_cpu_freq(void)
{
    unsigned int reg_value = 0;
    unsigned int odiv = 0, div_loopc = 0, div_refc = 0;

    reg_value = readl(NODE_PLL_CONF_REG0);

    odiv      = (reg_value >> NODE_PLL_ODIV_OFFSET) & NODE_PLL_ODIV_MASK;
    div_loopc = (reg_value >> NODE_PLL_DIV_LOOPC_OFFSET) & NODE_PLL_DIV_LOOPC_MASK;
    div_refc  = (reg_value >> NODE_PLL_DIV_REFC_OFFSET) & NODE_PLL_DIV_REFC_MASK;

    return (XTL_FREQ / div_refc * div_loopc / odiv);
}

int clk_get_sb_freq(void)
{
    unsigned int reg_value = 0;
    unsigned int odiv = 0, div_loopc = 0, div_refc = 0;

    reg_value = readl(SOC_PLL_CONF_REG0);
    div_loopc = (reg_value >> SOC_PLL_DIV_LOOPC_OFFSET) & SOC_PLL_DIV_LOOPC_MASK;
    div_refc  = (reg_value >> SOC_PLL_DIV_REFC_OFFSET) & SOC_PLL_DIV_REFC_MASK;

    reg_value = readl(SOC_PLL_CONF_REG1);
    odiv      = (reg_value >> SOC_PLL_ODIV_SB_OFFSET) & SOC_PLL_ODIV_SB_MASK;

    return (XTL_FREQ / div_refc * div_loopc / odiv);
}

int clk_get_gmac_freq(void)
{
    unsigned int reg_value = 0;
    unsigned int odiv = 0, div_loopc = 0, div_refc = 0;

    reg_value = readl(SOC_PLL_CONF_REG0);
    div_loopc = (reg_value >> SOC_PLL_DIV_LOOPC_OFFSET) & SOC_PLL_DIV_LOOPC_MASK;
    div_refc  = (reg_value >> SOC_PLL_DIV_REFC_OFFSET) & SOC_PLL_DIV_REFC_MASK;

    reg_value = readl(SOC_PLL_CONF_REG1);
    odiv      = (reg_value >> SOC_PLL_ODIV_GMAC_OFFSET) & SOC_PLL_ODIV_GMAC_MASK;

    return (XTL_FREQ / div_refc * div_loopc / odiv);
}

int clk_get_apb_freq(void)
{
    int          sb_freq       = 0;
    unsigned int reg_value     = 0;
    unsigned int sb_freq_scale = 0, apb_freq_scale = 0;

    sb_freq        = clk_get_sb_freq();
    reg_value      = readl(SB_FREQ_SCALE_REG);
    sb_freq_scale  = ((reg_value >> SB_FREQ_SCALE_OFFSET) & SB_FREQ_SCALE_MASK);
    sb_freq        = sb_freq * (sb_freq_scale + 1) / 8;
    apb_freq_scale = ((reg_value >> APB_FREQ_SCALE_OFFSET) & APB_FREQ_SCALE_MASK);

    return sb_freq * (apb_freq_scale + 1) / 8;
}
