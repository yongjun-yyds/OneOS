/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_common.c
 *
 * @brief       This file provides common functions.
 *
 * @revision
 * Date         Author          Notes
 * 2020-11-17   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include <os_types.h>
#include <os_memory.h>
#include <drv_common.h>
#include <string.h>
#include <os_stddef.h>
#include <os_task.h>
#include <board.h>
#include <arch_interrupt.h>
#include <os_stddef.h>
#include <os_util.h>
#include <interrupt.h>

os_err_t os_hw_board_init(void)
{
    os_hw_exception_init();
    os_hw_interrupt_init();

#if defined(OS_USING_HEAP)
    os_default_heap_init();
    os_default_heap_add(OS_HW_HEAP_BEGIN, OS_HW_HEAP_END - OS_HW_HEAP_BEGIN, OS_MEM_ALG_DEFAULT);
#endif

    os_irq_enable();
    return OS_SUCCESS;
}

OS_INIT_CALL(os_hw_board_init, OS_INIT_LEVEL_PRE_KERNEL_1, OS_INIT_SUBLEVEL_HIGH);

void os_hw_cpu_reset(void)
{
    readl(LS2K500_RST_CNT_REG) = 1;
}

