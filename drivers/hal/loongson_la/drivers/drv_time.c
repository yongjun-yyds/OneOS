/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_time.c
 *
 * @brief       This file provides function for timer.
 *
 * @revision
 * Date         Author          Notes
 * 2020-11-17   OneOS Team      First version.
 ***********************************************************************************************************************
 */

#include <board.h>
#include <os_clock.h>
#include <timer/timer.h>
#include <timer/clocksource.h>
#include <timer/clockevent.h>
#include <ls2k500_regs.h>

static unsigned int calc_const_freq(void)
{
    unsigned int res;
    unsigned int base_freq;
    unsigned int cfm, cfd;

    res = read_cfg(LOONGARCH_CPUCFG2);
    if (!(res & CPUCFG2_LLFTP))
        return 0;

    base_freq = read_cfg(LOONGARCH_CPUCFG4);
    res       = read_cfg(LOONGARCH_CPUCFG5);
    cfm       = res & 0xffff;
    cfd       = (res >> 16) & 0xffff;

    if (!base_freq || !cfm || !cfd)
        return 0;
    else
        return (base_freq * cfm / cfd);
}

static uint64_t loongson_timer_read(void *clock)
{
    return 0;
}

static void loongson_timer_start(os_clockevent_t *ce, uint32_t prescaler, uint64_t count)
{
    unsigned long timer_config;

    timer_config = count & CSR_TCFG_VAL;
    timer_config |= (CSR_TCFG_PERIOD | CSR_TCFG_EN);
    tcfg_csrwr(timer_config);
}

static void loongson_timer_stop(os_clockevent_t *ce)
{
    unsigned long timer_config;

    timer_config = tcfg_csrrd();
    timer_config &=  ~CSR_TCFG_EN;
    tcfg_csrwr(timer_config);
}

static const struct os_clockevent_ops loongson_ce_ops = {
    .start = loongson_timer_start,
    .stop  = loongson_timer_stop,
    .read  = loongson_timer_read,
};

static os_clocksource_t loongson_cs;
static os_clockevent_t loongson_ce;

void os_tick_handler(void)
{
    os_tick_increase();
    os_clocksource_update();
}

void os_hw_ost_handler(void)
{
    ticlr_csrwr(CSR_TINTCLR_TI);
    os_clockevent_isr(&loongson_ce);
}

static uint64_t loongson_cs_read(void *clock)
{
    return drdtime();
}

static void loongson_cs_init(void)
{
    os_clocksource_t *cs = &loongson_cs;

    cs->rating = 640;
    cs->freq   = calc_const_freq();
    cs->mask   = 0xffffffffffffffffull;
    cs->read   = loongson_cs_read;

    os_clocksource_register("lcs", cs);
}

static void loongson_ce_init(void)
{
    os_clockevent_t *ce = &loongson_ce;

    ce->rating = 640;
    ce->freq   = calc_const_freq();
    ce->mask   = 0xffffffffffffffffull;

    ce->prescaler_mask = 0x0ul;
    ce->prescaler_bits = 0;

    ce->count_mask = 0xffffffffffffffffull;
    ce->count_bits = 64;

    ce->feature = OS_CLOCKEVENT_FEATURE_PERIOD;

    ce->min_nsec = NSEC_PER_SEC / ce->freq;

    ce->ops = &loongson_ce_ops;
    
    os_clockevent_register("lce", ce);
}

static os_err_t ostick_init(void)
{
    loongson_cs_init();
    loongson_ce_init();

    return 0;
}

OS_INIT_CALL(ostick_init, OS_INIT_LEVEL_PRE_DEVICE, OS_INIT_SUBLEVEL_MIDDLE);

