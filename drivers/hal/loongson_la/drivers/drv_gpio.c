/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_gpio.c
 *
 * @brief       This file implements gpio driver for loongson.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <arch_interrupt.h>
#include "drv_gpio.h"
#include <interrupt.h>
#include <arch_misc.h>
#include <os_list.h>

#define GPIO_PER_INTERRUPT_LINE (4)

static os_list_node_t gpio_isr_list = OS_LIST_INIT(gpio_isr_list);

struct gpio_isr_info
{
    uint32_t index;
    int32_t  pin;
    uint32_t mode;
    void (*hdr)(void *args);
    void          *args;
    os_list_node_t list;
};

static void loongson_pin_write(struct os_device *dev, os_base_t pin, os_base_t value)
{
    gpio_port_set_value((int)value, pin);
}

static int loongson_pin_read(struct os_device *dev, os_base_t pin)
{
    int value = PIN_LOW;

    value = gpio_port_get_value(pin);

    return value;
}

static void loongson_pin_mode(struct os_device *dev, os_base_t pin, os_base_t mode)
{
    enum gpio_direction dir = GPIO_DIRECTION_OUT;

    switch (mode)
    {
    case PIN_MODE_OUTPUT:
        dir = GPIO_DIRECTION_OUT;
        break;
    case PIN_MODE_INPUT:
    case PIN_MODE_INPUT_PULLUP:
    case PIN_MODE_INPUT_PULLDOWN:
        dir = GPIO_DIRECTION_IN;
        break;
    default:
        break;
    }
    gpio_set_func(GPIO_FUNC_GPIO, pin);
    gpio_set_direction(dir, pin);
}

/* clang-format off */
static os_err_t loongson_pin_attach_irq(struct os_device *device, int32_t pin, uint32_t mode, void (*hdr)(void *args), void *args)
{
    uint32_t           index = 0;
    os_ubase_t             level;
    struct gpio_isr_info *isr_info;

    if (pin >= LS2k500_GPIO_IRQ_NUM)
    {
        return OS_NOSYS;
    }

    if (mode != PIN_IRQ_MODE_RISING)
    {
        return OS_NOSYS;
    }

    if ((pin - 3) % 4 == 0)
    {
        return OS_NOSYS;
    }

    index = pin / GPIO_PER_INTERRUPT_LINE;

    os_spin_lock_irqsave(&gs_device_lock, &level);

    os_list_for_each_entry(isr_info, &gpio_isr_list, struct gpio_isr_info, list)
    {
        if (isr_info->index == index)
        {
            os_spin_unlock_irqrestore(&gs_device_lock, level);
            return OS_BUSY;
        }
    }

    isr_info = os_malloc(sizeof(struct gpio_isr_info));
    if (isr_info == OS_NULL)
    {
        os_spin_unlock_irqrestore(&gs_device_lock, level);
        return OS_NOMEM;
    }

    isr_info->index = index;
    isr_info->pin   = pin;
    isr_info->hdr   = hdr;
    isr_info->args  = args;
    os_list_add_tail(&gpio_isr_list, &isr_info->list);

    os_spin_unlock_irqrestore(&gs_device_lock, level);

    return OS_SUCCESS;
}
/* clang-format on */

static os_err_t loongson_pin_dettach_irq(struct os_device *device, int32_t pin)
{
    uint32_t           index = 0;
    os_ubase_t             level;
    struct gpio_isr_info *isr_info;

    if (pin >= LS2k500_GPIO_IRQ_NUM)
    {
        return OS_NOSYS;
    }

    if ((pin - 3) % 4 == 0)
    {
        return OS_NOSYS;
    }

    index = pin / GPIO_PER_INTERRUPT_LINE;

    os_spin_lock_irqsave(&gs_device_lock, &level);

    os_list_for_each_entry(isr_info, &gpio_isr_list, struct gpio_isr_info, list)
    {
        if ((isr_info->index == index) && (pin == (isr_info->pin)))
        {
            os_list_del_init(&isr_info->list);
            os_free(isr_info);
            os_spin_unlock_irqrestore(&gs_device_lock, level);
            return OS_SUCCESS;
        }
    }

    os_spin_unlock_irqrestore(&gs_device_lock, level);

    return OS_EMPTY;
}

static os_err_t loongson_pin_irq_enable(struct os_device *device, os_base_t pin, uint32_t enabled)
{
    os_ubase_t          level;
    enum gpio_function func = GPIO_FUNC_GPIO;

    if (enabled == PIN_IRQ_ENABLE)
    {
        os_spin_lock_irqsave(&gs_device_lock, &level);
        gpio_set_func(func, pin);
        gpio_set_direction(GPIO_DIRECTION_IN, pin);
        gpio_set_interrupt_enable(pin);

        os_spin_unlock_irqrestore(&gs_device_lock, level);
    }
    else if (enabled == PIN_IRQ_DISABLE)
    {
        os_spin_lock_irqsave(&gs_device_lock, &level);
        gpio_set_interrupt_disable(pin);
        os_spin_unlock_irqrestore(&gs_device_lock, level);
    }
    else
    {
        return OS_NOSYS;
    }
    return OS_SUCCESS;
}

void gpio_irq_handler(int irq, void *param)
{
    uint32_t           index = 0;
    os_ubase_t             level;
    struct gpio_isr_info *isr_info;

    index = (irq - LS2K_GPIO0_3_IRQ);

    os_spin_lock_irqsave(&gs_device_lock, &level);

    os_list_for_each_entry(isr_info, &gpio_isr_list, struct gpio_isr_info, list)
    {
        if (isr_info->index == index)
        {
            isr_info->hdr(isr_info->args);
            os_spin_unlock_irqrestore(&gs_device_lock, level);
            return;
        }
    }

    os_spin_unlock_irqrestore(&gs_device_lock, level);
}

const static struct os_pin_ops loongson_la_pin_ops = {
    loongson_pin_mode,
    loongson_pin_write,
    loongson_pin_read,
    loongson_pin_attach_irq,
    loongson_pin_dettach_irq,
    loongson_pin_irq_enable,
};

static os_err_t os_hw_pin_init(void)
{
    int          irqno = 0;
    unsigned int pin   = 0;

    for (pin = 0; pin < LS2k500_GPIO_IRQ_NUM; pin++)
    {
        gpio_set_interrupt_disable(pin);
    }

    for (irqno = LS2K_GPIO0_3_IRQ; irqno <= LS2K_GPIO124_127_IRQ; irqno++)
    {
        os_hw_interrupt_install(irqno, gpio_irq_handler, OS_NULL, OS_NULL);
        os_hw_interrupt_umask(irqno);
    }

    return os_device_pin_register(0, &loongson_la_pin_ops, OS_NULL);
}

OS_INIT_CALL(os_hw_pin_init, OS_INIT_LEVEL_PRE_DEVICE, OS_INIT_SUBLEVEL_HIGH);
