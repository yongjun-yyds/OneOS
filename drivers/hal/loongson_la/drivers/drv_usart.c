/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_usart.c
 *
 * @brief       This file implements usart driver for loongson
 *
 * @revision
 * Date         Author          Notes
 * 2020-11-17   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <arch_interrupt.h>
#include <drv_usart.h>
#include <os_errno.h>
#include <drv_common.h>
#include <arch_interrupt.h>
#include <os_memory.h>
#include <string.h>
#include <interrupt.h>
#include <drv_gpio.h>

static os_err_t loongson_configure(struct os_serial_device *serial, struct serial_configure *cfg)
{
    os_ubase_t            level = 0;
    struct loongson_uart *uart;
    ls2k_uart_info_t      uart_info = {0};

    uart = serial->parent.user_data;
    OS_ASSERT(uart != OS_NULL);

    uart_info.UARTx     = uart->usart_info->uart_base;
    uart_info.baudrate  = cfg->baud_rate;
    uart_info.databits  = cfg->data_bits;
    uart_info.stopbits  = cfg->stop_bits;
    uart_info.parity    = cfg->parity;
    uart_info.rx_enable = 1;
    os_spin_lock_irqsave(&gs_device_lock, &level);

    if (uart_init(&uart_info))
    {
        os_spin_unlock_irqrestore(&gs_device_lock, level);
        return OS_NOSYS;
    }
    os_spin_unlock_irqrestore(&gs_device_lock, level);
    return OS_SUCCESS;
}

void __os_hw_console_output(char *str)
{
    unsigned int length = 0;

    length = strlen(str);

    while (length--)
    {
        uart_putc((void *)(LS2K_UART2_BASE), *(str++));
    }
}

static void uart_irq_handler(int irqno, void *param)
{
    struct os_serial_device *serial     = (struct os_serial_device *)param;
    struct loongson_uart    *uart       = serial->parent.user_data;
    unsigned int             recv_value = 0;
    ls2k_uart_info_t         uart_info  = {0};

    unsigned char iir = readb(uart->usart_info->uart_base + LS2K_UART_IIR_OFFSET);

    if (((IIR_RXRDY & iir) == IIR_RXRDY) || ((IIR_RXTOUT & iir) == IIR_RXTOUT))
    {
        while (1)
        {
            recv_value = ls2k_uart_getc(uart->usart_info->uart_base);

            if (recv_value == (unsigned int)(-1))
            {
                break;
            }
            uart->rx_buff[uart->rx_index++] = (unsigned char)(recv_value & 0xFF);
        }

        if (uart->rx_index == (uart->rx_size / 2))
        {
            soft_dma_half_irq(&uart->sdma);
        }

        if (uart->rx_index == uart->rx_size)
        {
            uart->rx_index = 0;
            soft_dma_full_irq(&uart->sdma);
        }
    }
    else if (IIR_TXRDY & iir)
    {
        uart_tx_interrupt_disable(&uart_info);
        uart->tx_count++;

        if (uart->tx_count == uart->tx_size)
        {
            uart->tx_size  = 0;
            uart->tx_count = 0;

            uart_info.UARTx = uart->usart_info->uart_base;

            os_hw_serial_isr_txdone(serial);
            return;
        }
        uart_tx_interrupt_enable(&uart_info);
        uart_putc(uart->usart_info->uart_base, uart->tx_buff[uart->tx_count]);
    }

    return;
}

static void loongson_usart_sdma_callback(soft_dma_t *dma)
{
    struct loongson_uart *uart = os_container_of(dma, struct loongson_uart, sdma);

    os_hw_serial_isr_rxdone((struct os_serial_device *)uart);
}

static uint32_t loongson_sdma_int_get_index(soft_dma_t *dma)
{
    struct loongson_uart *uart = os_container_of(dma, struct loongson_uart, sdma);

    return uart->rx_index;
}

static os_err_t loongson_sdma_int_start(soft_dma_t *dma, void *buff, uint32_t size)
{
    struct loongson_uart *uart = os_container_of(dma, struct loongson_uart, sdma);

    uart->rx_buff  = buff;
    uart->rx_index = 0;
    uart->rx_size  = size;

    os_hw_interrupt_umask(uart->usart_info->irqno);

    return OS_SUCCESS;
}

static uint32_t loongson_sdma_int_stop(soft_dma_t *dma)
{
    struct loongson_uart *uart = os_container_of(dma, struct loongson_uart, sdma);

    os_hw_interrupt_mask(uart->usart_info->irqno);

    return loongson_sdma_int_get_index(dma);
}

static void loongson_usart_sdma_init(struct loongson_uart *uart, dma_ring_t *ring)
{
    soft_dma_t *dma = &uart->sdma;

    soft_dma_stop(dma);

    memset(&dma->hard_info, 0, sizeof(dma->hard_info));

    dma->hard_info.mode         = HARD_DMA_MODE_NORMAL;
    dma->hard_info.max_size     = 64 * 1024;
    dma->hard_info.data_timeout = uart_calc_byte_timeout_us(uart->serial_dev.config.baud_rate);

    dma->hard_info.flag = HARD_DMA_FLAG_HALF_IRQ | HARD_DMA_FLAG_FULL_IRQ;
    dma->ops.get_index  = loongson_sdma_int_get_index;
    dma->ops.dma_init   = OS_NULL;
    dma->ops.dma_start  = loongson_sdma_int_start;
    dma->ops.dma_stop   = loongson_sdma_int_stop;

    dma->cbs.dma_half_callback    = loongson_usart_sdma_callback;
    dma->cbs.dma_full_callback    = loongson_usart_sdma_callback;
    dma->cbs.dma_timeout_callback = loongson_usart_sdma_callback;

    soft_dma_init(dma);
    soft_dma_start(dma, ring);
    soft_dma_irq_enable(&uart->sdma, OS_TRUE);
}

static os_err_t loongson_usart_init(struct os_serial_device *serial, struct serial_configure *cfg)
{
    struct loongson_uart *uart;

    OS_ASSERT(serial != OS_NULL);
    OS_ASSERT(cfg != OS_NULL);

    uart = (struct loongson_uart *)serial->parent.user_data;

    loongson_configure(serial, cfg);
    loongson_usart_sdma_init(uart, &serial->rx_fifo->ring);

    return OS_SUCCESS;
}

static os_err_t loongson_usart_deinit(struct os_serial_device *serial)
{
    return 0;
}

static int loongson_uart_poll_send(struct os_serial_device *serial, const uint8_t *buff, os_size_t size)
{
    os_ubase_t             level;
    uint32_t           i = 0;
    struct loongson_uart *uart;

    OS_ASSERT(serial != OS_NULL);

    uart = serial->parent.user_data;
    OS_ASSERT(uart != OS_NULL);

    for (i = 0; i < size; i++)
    {
        os_spin_lock_irqsave(&gs_device_lock, &level);
        uart_putc(uart->usart_info->uart_base, *(uint8_t *)(buff + i));
        os_spin_unlock_irqrestore(&gs_device_lock, level);
    }
    os_device_send_notify(&serial->parent);
    return size;
}

static const struct os_uart_ops loongson_uart_ops = {
    .init       = loongson_usart_init,
    .deinit     = loongson_usart_deinit,
    .start_send = OS_NULL,
    .poll_send  = loongson_uart_poll_send,
};

void uart_gpio_func_init(void *uart_base)
{
    if ((unsigned long)uart_base == LS2K_UART2_BASE)
    {
        gpio_set_func(GPIO_FUNC_MAIN, 60);
        gpio_set_func(GPIO_FUNC_MAIN, 61);
    }
    else if ((unsigned long)uart_base == LS2K_UART3_BASE)
    {
        gpio_set_func(GPIO_FUNC_MAIN, 62);
        gpio_set_func(GPIO_FUNC_MAIN, 63);
    }
    else
    {
        return;
    }
    return;
}

static void __os_hw_usart_init(struct loongson_uart *uart, const os_device_info_t *dev)
{
    uart_gpio_func_init(uart->usart_info->uart_base);
    os_hw_interrupt_install(uart->usart_info->irqno, uart_irq_handler, &(uart->serial_dev), dev->name);
    os_hw_interrupt_umask(uart->usart_info->irqno);
    return;
}

static int loongson_usart_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    struct serial_configure config = OS_SERIAL_CONFIG_DEFAULT;

    os_err_t result = 0;

    struct loongson_uart *uart = os_calloc(1, sizeof(struct loongson_uart));

    OS_ASSERT(uart);

    uart->usart_info = (struct loongson_usart_info *)dev->info;

    struct os_serial_device *serial = &(uart->serial_dev);

    __os_hw_usart_init(uart, dev);

    serial->ops    = &loongson_uart_ops;
    serial->config = config;
    result         = os_hw_serial_register(serial, dev->name, uart);

    OS_ASSERT(result == OS_SUCCESS);

    return result;
}

OS_DRIVER_INFO loongson_usart_driver = {
    .name  = "UART_HandleTypeDef",
    .probe = loongson_usart_probe,
};

OS_DRIVER_DEFINE(loongson_usart_driver, OS_INIT_LEVEL_PRE_DEVICE, OS_INIT_SUBLEVEL_HIGH);

