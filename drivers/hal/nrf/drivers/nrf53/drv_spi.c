/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_spi.c
 *
 * @brief       This file provides operation functions declaration for adc.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <stdint.h>
#include <string.h>
#include "board.h"
#include "drv_spi.h"
#include <os_memory.h>

#define DRV_EXT_LVL DBG_EXT_INFO
#define LOG_TAG     "drv.spi"
#include <drv_log.h>
#include <shell.h>

struct nrf5_spi
{
    struct os_spi_bus spi_bus;

    struct nrf5_spi_info        *spi_info;
    struct nrf5_spi_config      *config;
    struct os_spi_configuration *cfg;

    uint8_t     spi_dma_flag;
    os_list_node_t list;
};

static os_list_node_t nrf5_spi_list = OS_LIST_INIT(nrf5_spi_list);

struct nrfx_drv_spi_config
{
    char       *bus_name;
    nrfx_spim_t spi;
};

struct nrfx_drv_spi
{
    nrfx_spim_t                  spi;        /* nrfx spi driver instance. */
    nrfx_spim_config_t           spi_config; /* nrfx spi config Configuration */
    struct os_spi_configuration *cfg;
    struct os_spi_bus            spi_bus;
};

/**
 * spi event handler function
 */
static void spi0_handler(const nrfx_spim_evt_t *p_event, void *p_context)
{
    return;
}

static void spi1_handler(const nrfx_spim_evt_t *p_event, void *p_context)
{
    return;
}

static void spi2_handler(const nrfx_spim_evt_t *p_event, void *p_context)
{
    return;
}
nrfx_spim_evt_handler_t spi_handler[] = {spi0_handler, spi1_handler, spi2_handler};

/**
 * @brief  This function config spi bus
 * @param  device
 * @param  configuration
 * @retval OS_SUCCESS / OS_FAILURE
 */
static os_err_t spi_configure(struct os_spi_device *device, struct os_spi_configuration *configuration)
{
    OS_ASSERT(device != OS_NULL);
    OS_ASSERT(device->bus != OS_NULL);
    OS_ASSERT(configuration != OS_NULL);

    struct nrf5_spi *spi_drv = os_container_of(device->bus, struct nrf5_spi, spi_bus);
    spi_drv->cfg             = configuration;
    struct nrf5_spi_info *spi_info;
    spi_info = spi_drv->spi_info;

    nrfx_spim_t        spi = spi_info->spi_dev;
    nrfx_spim_config_t config;
    config.sck_pin  = spi_info->sck_pin;
    config.mosi_pin = spi_info->mosi_pin;
    config.miso_pin = spi_info->miso_pin;
    config.ss_pin   = NRFX_SPIM_PIN_NOT_USED;

    /* spi config bit order */
    if (configuration->mode & OS_SPI_MSB)
    {
        config.bit_order = NRF_SPIM_BIT_ORDER_MSB_FIRST;
    }
    else
    {
        config.bit_order = NRF_SPIM_BIT_ORDER_LSB_FIRST;
    }

    /* spi mode config */
    switch (configuration->mode & OS_SPI_MODE_3)
    {
    case OS_SPI_MODE_0 /* OS_SPI_CPOL:0 , OS_SPI_CPHA:0 */:
        config.mode = NRF_SPIM_MODE_0;
        break;
    case OS_SPI_MODE_1 /* OS_SPI_CPOL:0 , OS_SPI_CPHA:1 */:
        config.mode = NRF_SPIM_MODE_1;
        break;
    case OS_SPI_MODE_2 /* OS_SPI_CPOL:1 , OS_SPI_CPHA:0 */:
        config.mode = NRF_SPIM_MODE_2;
        break;
    case OS_SPI_MODE_3 /* OS_SPI_CPOL:1 , OS_SPI_CPHA:1 */:
        config.mode = NRF_SPIM_MODE_3;
        break;
    default:
        os_kprintf("spi_configure mode error %x\n", configuration->mode);
        return OS_FAILURE;
    }
    /* spi frequency config */
    switch (configuration->max_hz / 1000)
    {
    case 125:
        config.frequency = NRF_SPIM_FREQ_125K;
        break;
    case 250:
        config.frequency = NRF_SPIM_FREQ_250K;
        break;
    case 500:
        config.frequency = NRF_SPIM_FREQ_500K;
        break;
    case 1000:
        config.frequency = NRF_SPIM_FREQ_1M;
        break;
    case 2000:
        config.frequency = NRF_SPIM_FREQ_2M;
        break;
    case 4000:
        config.frequency = NRF_SPIM_FREQ_4M;
        break;
    case 8000:
        config.frequency = NRF_SPIM_FREQ_8M;
        break;
    default:
        config.frequency = NRF_SPIM_FREQ_4M;
        break;
    }

    nrfx_spim_evt_handler_t handler = OS_NULL;    // spi send callback handler ,default NULL
    void                   *context = OS_NULL;
    nrfx_err_t              nrf_ret = nrfx_spim_init(&spi, &config, handler, context);
    if (NRFX_SUCCESS == nrf_ret)
        return OS_SUCCESS;

    return OS_FAILURE;
}

static uint32_t spixfer(struct os_spi_device *device, struct os_spi_message *message)
{
    OS_ASSERT(device != OS_NULL);
    OS_ASSERT(device->bus != OS_NULL);

    nrfx_err_t nrf_ret;

    struct nrf5_spi      *spi_drv = os_container_of(device->bus, struct nrf5_spi, spi_bus);
    struct nrf5_spi_info *spi_info;
    spi_info = spi_drv->spi_info;

    nrfx_spim_t          *p_instance = &spi_info->spi_dev;
    nrfx_spim_xfer_desc_t p_xfer_desc;

    if (message->cs_take)
    {
        os_pin_write(device->cs_pin, PIN_LOW);
    }

    p_xfer_desc.p_rx_buffer = message->recv_buf;
    p_xfer_desc.rx_length   = message->length;
    p_xfer_desc.p_tx_buffer = message->send_buf;
    p_xfer_desc.tx_length   = message->length;

    if (message->send_buf == OS_NULL)
    {
        p_xfer_desc.tx_length = 0;
    }

    if (message->recv_buf == OS_NULL)
    {
        p_xfer_desc.rx_length = 0;
    }

    nrf_ret = nrfx_spim_xfer(p_instance, &p_xfer_desc, 0);

    if (message->cs_release)
    {
        os_pin_write(device->cs_pin, PIN_HIGH);
    }

    if (NRFX_SUCCESS != nrf_ret)
    {
        return 0;
    }
    else
    {
        return message->length;
    }
}

/* spi bus callback function  */
static const struct os_spi_ops nrf5_spi_ops = {
    .configure = spi_configure,
    .xfer      = spixfer,
};

static int nrf5_spi_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    os_err_t  result = 0;
    os_ubase_t level;

    struct nrf5_spi *nrf_spi = os_calloc(1, sizeof(struct nrf5_spi));

    OS_ASSERT(nrf_spi);

    nrf_spi->spi_info = (struct nrf5_spi_info *)dev->info;

    struct os_spi_bus *spi_bus = &nrf_spi->spi_bus;

    os_spin_lock_irqsave(&gs_device_lock, &level);
    os_list_add_tail(&nrf5_spi_list, &nrf_spi->list);
    os_spin_unlock_irqrestore(&gs_device_lock, level);

    result = os_spi_bus_register(spi_bus, dev->name, &nrf5_spi_ops);
    OS_ASSERT(result == OS_SUCCESS);

    return result;
}

OS_DRIVER_INFO nrf5_spi_driver = {
    .name  = "SPI_HandleTypeDef",
    .probe = nrf5_spi_probe,
};

OS_DRIVER_DEFINE(nrf5_spi_driver, OS_INIT_LEVEL_PRE_DEVICE, OS_INIT_SUBLEVEL_LOW);
#define AT45DBXX_BUS_NAME "at45dbxx_spi"
#define W25Q64XX_BUS_NAME "w25q64xx_spi"

#define AT45DB_CMD_JEDEC_ID 0x9F
#define W25Q64_CMD_JEDEC_ID 0x9F

struct JEDEC_ID
{
    uint8_t manufacturer_id;  /* Manufacturer ID */
    uint8_t density_code : 5; /* Density Code */
    uint8_t family_code : 3;  /* Family Code */
    uint8_t version_code : 5; /* Product Version Code */
    uint8_t mlc_code : 3;     /* MLC Code */
    uint8_t byte_count;       /* Byte Count */
};

#define BSP_AT45DBXX_SPI_CS (10)
#define BSP_W25Q64XX_SPI_CS GET_PIN(A, 4)

static int spi_test(int argc, char *argv[])
{
    const char *spi_device_name = "spi1";

    struct os_spi_device *os_spi_device;
    struct JEDEC_ID      *JEDEC_ID;

    os_hw_spi_device_attach(spi_device_name, AT45DBXX_BUS_NAME, BSP_AT45DBXX_SPI_CS);

    os_spi_device = (struct os_spi_device *)os_device_find(AT45DBXX_BUS_NAME);
    if (os_spi_device == OS_NULL)
    {
        os_kprintf("spi device %s not found!\r\n", AT45DBXX_BUS_NAME);
        return 0;
    }

    /* config spi */
    {
        struct os_spi_configuration cfg;
        cfg.data_width = 8;
        cfg.mode       = OS_SPI_MODE_0 | OS_SPI_MSB; /* SPI Compatible Modes 0 and 3 */
        cfg.max_hz     = 6000000;                    /* Atmel RapidS Serial Interface: 66MHz Maximum Clock Frequency */
        os_spi_configure(os_spi_device, &cfg);
    }

    /* read JEDEC ID */
    uint8_t cmd;
    uint8_t id_recv[6];
    JEDEC_ID = (struct JEDEC_ID *)id_recv;

    cmd = AT45DB_CMD_JEDEC_ID;
    os_spi_send_then_recv(os_spi_device, &cmd, 1, id_recv, 6);

    os_kprintf("JEDEC Read-ID Data : %02X %02X %02X\r\n", id_recv[0], id_recv[1], id_recv[2]);
    return 0;
}

SH_CMD_EXPORT(spi_test, spi_test, "spi_test");
