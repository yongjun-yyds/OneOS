#ifndef __DRV_JPEG_H__
#define __DRV_JPEG_H__

#include <board.h>
#include <drv_common.h>
#include <shell.h>
#include <bus.h>
struct stm32_jpeg_info
{
    os_device_t         jpeg_dev;
    JPEG_HandleTypeDef *hjpeg;
};

void jpeg_start_decode(JPEG_HandleTypeDef *hjpeg,
                       uint8_t         *source,
                       uint32_t         source_length,
                       uint8_t         *yuv_addr,
                       uint32_t         yuv_length);

#endif
