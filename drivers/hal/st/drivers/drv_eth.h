/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_eth.h
 *
 * @brief       This file provides macro declaration for eth driver.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __DRV_ETH_H__
#define __DRV_ETH_H__

#include <os_task.h>
#include <arch_interrupt.h>
#include <device.h>
#include <board.h>

#ifdef PHY_USING_LAN8720A

#define PHY_BASIC_CONTROL_REG               0x00U
#define PHY_BCR_RESET                       (1 << 15)
#define PHY_BCR_SPEED_SELECT                (1 << 13)
#define PHY_BCR_AUTO_NEGOTIATION_EN         (1 << 12)
#define PHY_BCR_RESET_AUTO_NEGOTIATION      (1 << 9)
#define PHY_BCR_DUPLEX_MODE                 (1 << 8)

#define PHY_BASIC_STATUS_REG                0x01U
#define PHY_BSR_AUTO_NEGOTIATE_COMPLETE     (1 << 5)
#define PHY_BSR_LINK_STATUS                 (1 << 2)

#define PHY_ID1_REG                         0x02U
#define PHY_ID2_REG                         0x03U

#define PHY_STATUS_REG                      0x1FU
#define PHY_SR_AUTODONE                     (1 << 12)
#define PHY_SR_SPEED_INDICATION_MASK        (0x07 << 2)
#define PHY_SR_10M_HALF_DUPLEX              (0x01 << 2)
#define PHY_SR_10M_FULL_DUPLEX              (0x05 << 2)
#define PHY_SR_100M_HALF_DUPLEX             (0x02 << 2)
#define PHY_SR_100M_FULL_DUPLEX             (0x06 << 2)

#endif /* PHY_USING_LAN8720A */

#ifdef PHY_USING_DM9161CEP

#define PHY_BASIC_CONTROL_REG               0x00U
#define PHY_BCR_RESET                       (1 << 15)
#define PHY_BCR_SPEED_SELECT                (1 << 13)
#define PHY_BCR_AUTO_NEGOTIATION_EN         (1 << 12)
#define PHY_BCR_RESET_AUTO_NEGOTIATION      (1 << 9)
#define PHY_BCR_DUPLEX_MODE                 (1 << 8)

#define PHY_BASIC_STATUS_REG                0x01U
#define PHY_BSR_AUTO_NEGOTIATE_COMPLETE     (1 << 5)
#define PHY_BSR_LINK_STATUS                 (1 << 2)

#define PHY_ID1_REG                         0x02U
#define PHY_ID2_REG                         0x03U

#define PHY_STATUS_REG                      0x11U
#define PHY_SR_AUTODONE                     (1 << 3)
#define PHY_SR_SPEED_INDICATION_MASK        (0x0F << 12)
#define PHY_SR_10M_HALF_DUPLEX              (0x01 << 12)
#define PHY_SR_10M_FULL_DUPLEX              (0x02 << 12)
#define PHY_SR_100M_HALF_DUPLEX             (0x04 << 12)
#define PHY_SR_100M_FULL_DUPLEX             (0x08 << 12)

#endif /* PHY_USING_DM9161CEP */

#ifdef PHY_USING_DP83848C

#define PHY_BASIC_CONTROL_REG               0x00U
#define PHY_BCR_RESET                       (1 << 15)
#define PHY_BCR_SPEED_SELECT                (1 << 13)
#define PHY_BCR_AUTO_NEGOTIATION_EN         (1 << 12)
#define PHY_BCR_RESET_AUTO_NEGOTIATION      (1 << 9)
#define PHY_BCR_DUPLEX_MODE                 (1 << 8)

#define PHY_BASIC_STATUS_REG                0x01U
#define PHY_BSR_AUTO_NEGOTIATE_COMPLETE     (1 << 5)
#define PHY_BSR_LINK_STATUS                 (1 << 2)

#define PHY_ID1_REG                         0x02U
#define PHY_ID2_REG                         0x03U

#define PHY_STATUS_REG                      0x10U
#define PHY_SR_AUTODONE                     (1 << 4)
#define PHY_SR_SPEED_INDICATION_MASK        (0x06 << 1)
#define PHY_SR_10M_HALF_DUPLEX              (0x01 << 1)
#define PHY_SR_10M_FULL_DUPLEX              (0x03 << 1)
#define PHY_SR_100M_HALF_DUPLEX             (0x00 << 1)
#define PHY_SR_100M_FULL_DUPLEX             (0x02 << 1)

#endif /* PHY_USING_DP83848C */

#define  PHY_STATUS_LINK_DOWN             ((int32_t) 1)
#define  PHY_STATUS_100MBITS_FULLDUPLEX   ((int32_t) 2)
#define  PHY_STATUS_100MBITS_HALFDUPLEX   ((int32_t) 3)
#define  PHY_STATUS_10MBITS_FULLDUPLEX    ((int32_t) 4)
#define  PHY_STATUS_10MBITS_HALFDUPLEX    ((int32_t) 5)


#endif /* __DRV_ETH_H__ */
