/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with 
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_flash_l4.c
 *
 * @brief       This file provides flash read/write/erase functions for l4.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include "board.h"
#include "drv_flash.h"

#define DRV_EXT_LVL DBG_EXT_INFO
#define DBG_TAG     "drv.flash"
#include <drv_log.h>

/**
 ***********************************************************************************************************************
 * @brief           GetPage:Gets the page of a given address
 *
 * @param[in]       Addr            Address of the FLASH Memory
 *
 * @return          Return the page number.
 * @retval          page            The page of a given address
 ***********************************************************************************************************************
 */
static uint32_t GetPage(uint32_t Addr)
{
  uint32_t page = 0;

  if (Addr < (FLASH_BASE + FLASH_BANK_SIZE))
  {
    /* Bank 1 */
    page = (Addr - FLASH_BASE) / FLASH_PAGE_SIZE;
  }
  else
  {
    /* Bank 2 */
    page = (Addr - (FLASH_BASE + FLASH_BANK_SIZE)) / FLASH_PAGE_SIZE;
  }

  return page;
}
/**
 ***********************************************************************************************************************
 * @brief            Gets the bank of a given address
 *
 * @param[in]       Addr            Address of the FLASH Memory
 *
 * @return          [Return the bank of a given address.
 ***********************************************************************************************************************
 */
static uint32_t GetBank(uint32_t Addr)
{
    uint32_t bank = 0;
#ifndef FLASH_BANK_2
    bank = FLASH_BANK_1;
#else
    /* No Bank swap */
    if (Addr < (FLASH_BASE + FLASH_BANK_SIZE))
    {
        bank = FLASH_BANK_1;
    }
    else
    {
        bank = FLASH_BANK_2;
    }
#endif
    return bank;
}

/**
 ***********************************************************************************************************************
 * @brief           stm32_flash_read:Read data from flash,and this operation's units is word.
 *
 * @param[in]       addr            flash address.
 * @param[out]      buf             buffer to store read data.
 * @param[in]       size            read bytes size.
 *
 * @return          Return read size or status.
 * @retval          size            read bytes size.
 * @retval          Others          read failed.
 ***********************************************************************************************************************
 */
int stm32_flash_read(uint32_t addr, uint8_t *buf, size_t size)
{
    size_t   i;

    if ((addr + size) > STM32_FLASH_END_ADDRESS)
    {
        LOG_E(DBG_TAG, "read outrange flash size! addr is (0x%p)", (void *)(addr + size));
        return OS_INVAL;
    }

    for (i = 0; i < size; i++, buf++, addr++)
    {
        *buf = *(uint8_t *)addr;
    }

    return size;
}

/**
 ***********************************************************************************************************************
 * @brief           Write data to flash.This operation's units is word.
 *
 * @attention       This operation must after erase.
 *
 * @param[in]       addr            flash address.
 * @param[in]       buf             the write data buffer.
 * @param[in]       size            write bytes size.
 *
 * @return          Return write size or status.
 * @retval          size            write bytes size.
 * @retval          Others          write failed.
 ***********************************************************************************************************************
 */
int stm32_flash_write(uint32_t addr, const uint8_t *buf, size_t size)
{
    os_err_t 		  result = 0;
	size_t			  remain = size;
	uint8_t 		  FlashWord[16];
	os_ubase_t        level;
	HAL_StatusTypeDef status;

    if ((addr + size) > STM32_FLASH_END_ADDRESS)
    {
        LOG_E(DBG_TAG, "ERROR: write outrange flash size! addr is (0x%p)\r\n", (void *)(addr + size));
        return OS_INVAL;
    }

    if (addr % 16 != 0)
    {
        LOG_E(DBG_TAG, "write addr must be 8-byte alignment");
        return OS_INVAL;
    }

    if (size == 0)
    {
        LOG_E(DBG_TAG, "write size 0");
        return OS_FAILURE;
    }

	/* Disable instruction cache prior to internal cacheable memory update */
	if (HAL_ICACHE_Disable() != HAL_OK)
	{
	  os_kprintf("stm32_flash_write: ICACHE_Disable ERROR\n");
	}
    HAL_FLASH_Unlock();

	while (remain > 0)
	{
		if (remain >= 16)
		{
			memcpy(FlashWord, buf, 16);
			buf += 16;
			remain -= 16;
		}
		else
		{
			memcpy(FlashWord, buf, remain);
			memset(FlashWord + remain, 0, 16 - remain);
			buf += remain;
			remain = 0;
		}


        os_spin_lock_irqsave(&gs_device_lock, &level);

        /* write data */
        status = HAL_FLASH_Program(FLASH_TYPEPROGRAM_QUADWORD, addr, (uint32_t)FlashWord);
		os_spin_unlock_irqrestore(&gs_device_lock, level);

        if (status == HAL_OK)
        {
            /* Check the written value */
            if (memcmp((void *)addr, FlashWord, 16))
            {
                LOG_E(DBG_TAG, "ERROR: write data != read data\r\n");
                result = OS_FAILURE;
                goto __exit;
            }
        }
        else
        {
            result = OS_FAILURE;
            goto __exit;
        }

        addr += 16;
    }

__exit:
    HAL_FLASH_Lock();
	/* Re-enable instruction cache */
	if (HAL_ICACHE_Enable() != HAL_OK)
	{
		os_kprintf("stm32_flash_write: ICACHE_Disable ERROR\n");
	}
    if (result != 0)
    {
        return result;
    }

    return size;
}

/**
 ***********************************************************************************************************************
 * @brief           Erase data on flash.This operation is irreversible and it's units is different which on many chips.
 *
 * @param[in]       addr            Flash address.
 * @param[in]       size            Erase bytes size.
 *
 * @return          Return erase result or status.
 * @retval          size            Erase bytes size.
 * @retval          Others          Erase failed.
 ***********************************************************************************************************************
 */
int stm32_flash_erase(uint32_t addr, size_t size)
{
    os_err_t result    = OS_SUCCESS;
    uint32_t FirstPage = 0, NbOfPages = 0, BankNumber = 0;
    uint32_t PageError    = 0;

    if ((addr + size) > STM32_FLASH_END_ADDRESS)
    {
        LOG_E(DBG_TAG, "ERROR: erase outrange flash size! addr is (0x%p)\r\n", (void *)(addr + size));
        return OS_INVAL;
    }

	/*Variable used for Erase procedure*/
	static FLASH_EraseInitTypeDef EraseInitStruct;
	
	/* Disable instruction cache prior to internal cacheable memory update */
	if (HAL_ICACHE_Disable() != HAL_OK)
	{
		os_kprintf("stm32_flash_write: ICACHE_Disable ERROR\n");
	}
	
    /* Unlock the Flash to enable the flash control register access *************/
	HAL_FLASH_Unlock();
	
	/* Get the 1st page to erase */
	FirstPage = GetPage(addr);
	/* Get the number of pages to erase from 1st page */
	NbOfPages = GetPage(addr + size - 1) - FirstPage + 1;
    /* Get the bank */
    BankNumber = GetBank(addr);

    /* Fill EraseInit structure*/
	EraseInitStruct.TypeErase   = FLASH_TYPEERASE_PAGES;
	EraseInitStruct.Banks       = BankNumber;
	EraseInitStruct.Page        = FirstPage;
	EraseInitStruct.NbPages     = NbOfPages;
	
    if (HAL_FLASHEx_Erase(&EraseInitStruct, &PageError) != HAL_OK)
	{
		result = OS_FAILURE;
		goto __exit;
	}

__exit:
    HAL_FLASH_Lock();
	
	/* Re-enable instruction cache */
	if (HAL_ICACHE_Enable() != HAL_OK)
	{
		os_kprintf("stm32_flash_write: ICACHE_Disable ERROR\n");
	}

    if (result != OS_SUCCESS)
    {
        return result;
    }

    LOG_D(DBG_TAG, "erase done: addr (0x%p), size %d", (void *)addr, size);
    return size;
}


#define STM32_FLASH_BLOCK_SIZE (FLASH_PAGE_SIZE)
#define STM32_FLASH_PAGE_SIZE  (8)

#include "fal_drv_flash.c"

