/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_lcd.c
 *
 * @brief       This file implements lcd driver for stm32.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <board.h>
#include <drv_cfg.h>
#include <string.h>
#include <os_memory.h>
#include <os_event.h>
#include <graphic/graphic.h>
#include <graphic/dsi.h>

#define LOG_TAG "drv.lcd"
#include <drv_log.h>

#define LCD_EVETN_LCD_FLUSH   (0x0001)
#define LCD_EVETN_DMA2D_FINSH (0x0001 << 1)

struct stm32_lcd
{
    os_graphic_t graphic;
    os_event_id  lcd_event;
};

extern void stm32_lcd_inst_init(os_graphic_t *graphic);
extern void stm32_lcd_inst_flush(os_graphic_t *graphic);

struct stm32_lcd *st_lcd = OS_NULL;

#ifdef OS_GRAPHIC_GPU_ENABLE

extern DMA2D_HandleTypeDef hdma2d;

__WEAK void dma2d_trans_complete(void)
{
}

static void stm32_dma2d_trans_complete(DMA2D_HandleTypeDef *Dma2dHandle)
{
    UNUSED(Dma2dHandle);
    dma2d_trans_complete();
    os_event_send(st_lcd->lcd_event, LCD_EVETN_DMA2D_FINSH);
}

static void stm32_dma2d_trans_error(DMA2D_HandleTypeDef *Dma2dHandle)
{
    UNUSED(Dma2dHandle);
    os_event_send(st_lcd->lcd_event, LCD_EVETN_DMA2D_FINSH);
}

static void stm32_dma2d_wait_finish(struct os_device *dev)
{
    os_event_recv(st_lcd->lcd_event, LCD_EVETN_DMA2D_FINSH, OS_EVENT_OPTION_OR, OS_WAIT_FOREVER, OS_NULL);
}

static os_err_t stm32_dma2d_init(void)
{
    hdma2d.Init.ColorMode = DMA2D_OUTPUT_RGB565;

    if (OS_GRAPHIC_LCD_FORMAT == OS_GRAPHIC_PIXEL_FORMAT_ARGB8888)
        hdma2d.Init.ColorMode = DMA2D_OUTPUT_ARGB8888;
    else if (OS_GRAPHIC_LCD_FORMAT == OS_GRAPHIC_PIXEL_FORMAT_RGB888)
        hdma2d.Init.ColorMode = DMA2D_OUTPUT_RGB888;

    hdma2d.Instance          = DMA2D;
    hdma2d.XferCpltCallback  = stm32_dma2d_trans_complete;
    hdma2d.XferErrorCallback = stm32_dma2d_trans_error;

    if (HAL_DMA2D_Init(&hdma2d) != HAL_OK)
    {
        os_kprintf("stm32_gpu_init error\r\n");
        return OS_FAILURE;
    }

    return OS_SUCCESS;
}

void stm32_dma2d_blit(struct os_device *dev, struct os_device_gpu_info *gpu_info)
{
    os_graphic_t      *graphic = (os_graphic_t *)dev;
    os_graphic_info_t *info    = &graphic->info;

    char   *destPixels = gpu_info->dest;
    int     dbpl       = info->bytes_per_line;
    uint8_t dbpp       = info->bits_per_pixel / 8;
    int     w          = gpu_info->width;
    int     h          = gpu_info->height;
    int     color      = gpu_info->color;

    hdma2d.Init.Mode         = DMA2D_R2M;
    hdma2d.Init.OutputOffset = dbpl / dbpp - w;
    hdma2d.XferCpltCallback  = stm32_dma2d_trans_complete;
    hdma2d.XferErrorCallback = stm32_dma2d_trans_error;

    os_event_recv(st_lcd->lcd_event,
                  LCD_EVETN_DMA2D_FINSH,
                  OS_EVENT_OPTION_OR | OS_EVENT_OPTION_CLEAR,
                  OS_WAIT_FOREVER,
                  OS_NULL);

    if (HAL_DMA2D_Init(&hdma2d) != HAL_OK)
    {
        os_kprintf("stm32_gpu_blit error\r\n");
        return;
    }

    HAL_DMA2D_Start_IT(&hdma2d, color, (uint32_t)destPixels, w, h);
}

void stm32_dma2d_blend(struct os_device *dev, struct os_device_gpu_info *gpu_info)
{
    os_graphic_t      *graphic = (os_graphic_t *)dev;
    os_graphic_info_t *info    = &graphic->info;

    char    *destPixels = gpu_info->dest;
    int      dbpl       = info->bytes_per_line;
    uint8_t  dbpp       = info->bits_per_pixel / 8;
    char    *srcPixels  = gpu_info->src;
    int      sbpl       = gpu_info->sbpl;
    uint8_t  sbpp;
    uint8_t  srcContainsAlpha;
    int      w           = gpu_info->width;
    int      h           = gpu_info->height;
    int      const_alpha = gpu_info->alpha;
    uint32_t cssMode     = DMA2D_CSS_420;

    switch (gpu_info->src_format)
    {
    case OS_GRAPHIC_PIXEL_FORMAT_ARGB8888:
        sbpp             = 4;
        srcContainsAlpha = 1;
        break;
    case OS_GRAPHIC_PIXEL_FORMAT_RGB888:
        sbpp             = 3;
        srcContainsAlpha = 0;
        break;
    case OS_GRAPHIC_PIXEL_FORMAT_RGB565:
        sbpp             = 2;
        srcContainsAlpha = 0;
        break;
    case OS_GRAPHIC_PIXEL_FORMAT_YUV:
        if (gpu_info->yuv_sampling_mode == YUV_SAMPLING_MODE_420)
        {
            cssMode = DMA2D_CSS_420;
            sbpp    = 16;
        }
        else if (gpu_info->yuv_sampling_mode == YUV_SAMPLING_MODE_444)
        {
            cssMode = DMA2D_NO_CSS;
            sbpp    = 8;
        }
        else if (gpu_info->yuv_sampling_mode == YUV_SAMPLING_MODE_422)
        {
            cssMode = DMA2D_CSS_422;
            sbpp    = 16;
        }
        srcContainsAlpha = 0;
        break;
    default:
        os_kprintf("Unsupport dma2d format\r\n");
        break;
    }

    const_alpha = (const_alpha * 255) >> 8;

    int needsBlend = const_alpha != 255 || srcContainsAlpha;

    if (needsBlend)
        hdma2d.Init.Mode = DMA2D_M2M_BLEND;
    else if (dbpp != sbpp)
        hdma2d.Init.Mode = DMA2D_M2M_PFC;
    else
        hdma2d.Init.Mode = DMA2D_M2M;

    hdma2d.Init.OutputOffset = dbpl / dbpp - w;

    hdma2d.LayerCfg[1].AlphaMode =
        const_alpha == 255 ? DMA2D_NO_MODIF_ALPHA : (srcContainsAlpha ? DMA2D_COMBINE_ALPHA : DMA2D_REPLACE_ALPHA);
    hdma2d.LayerCfg[1].InputAlpha = const_alpha;
    if (sbpp == 2)
    {
        hdma2d.LayerCfg[1].InputColorMode = srcContainsAlpha ? DMA2D_INPUT_ARGB4444 : DMA2D_INPUT_RGB565;
    }
    else if (sbpp == 3)
    {
        hdma2d.LayerCfg[1].InputColorMode = DMA2D_INPUT_RGB888;
    }
    else if (sbpp == 4)
    {
        hdma2d.LayerCfg[1].InputColorMode = DMA2D_INPUT_ARGB8888;
    }
    else
    {
        hdma2d.LayerCfg[1].InputColorMode = DMA2D_INPUT_YCBCR;
    }
    if (OS_GRAPHIC_PIXEL_FORMAT_YUV == gpu_info->src_format)
    {
        hdma2d.LayerCfg[1].ChromaSubSampling = cssMode;
        hdma2d.LayerCfg[1].InputOffset       = w % sbpp == 0 ? 0 : (sbpp - w % sbpp);
    }
    else
        hdma2d.LayerCfg[1].InputOffset = sbpl / sbpp - w;

    hdma2d.XferCpltCallback  = stm32_dma2d_trans_complete;
    hdma2d.XferErrorCallback = stm32_dma2d_trans_error;

    os_event_recv(st_lcd->lcd_event,
                  LCD_EVETN_DMA2D_FINSH,
                  OS_EVENT_OPTION_OR | OS_EVENT_OPTION_CLEAR,
                  OS_WAIT_FOREVER,
                  OS_NULL);
    if (HAL_DMA2D_Init(&hdma2d) != HAL_OK)
    {
        os_kprintf("stm32_gpu_blend error\r\n");
        return;
    }

    if (needsBlend)
    {
        hdma2d.LayerCfg[0].AlphaMode  = DMA2D_REPLACE_ALPHA;
        hdma2d.LayerCfg[0].InputAlpha = 0xff;
        if (dbpp == 2)
            hdma2d.LayerCfg[0].InputColorMode = DMA2D_INPUT_RGB565;
        else if (dbpp == 3)
            hdma2d.LayerCfg[0].InputColorMode = DMA2D_INPUT_RGB888;
        else if (sbpp == 4)
        {
            hdma2d.LayerCfg[0].InputColorMode = DMA2D_INPUT_ARGB8888;
        }
        else
        {
            hdma2d.LayerCfg[0].InputColorMode = DMA2D_INPUT_YCBCR;
        }

        hdma2d.LayerCfg[0].InputOffset = dbpl / dbpp - w;

        HAL_DMA2D_ConfigLayer(&hdma2d, 0);
    }

    HAL_DMA2D_ConfigLayer(&hdma2d, 1);

    if (needsBlend)
    {
        HAL_DMA2D_BlendingStart_IT(&hdma2d, (uint32_t)srcPixels, (uint32_t)destPixels, (uint32_t)destPixels, w, h);
    }
    else
    {
        HAL_DMA2D_Start_IT(&hdma2d, (uint32_t)srcPixels, (uint32_t)destPixels, w, h);
    }

}

void stm32_dma2d_alphamapblend(struct os_device *dev, struct os_device_gpu_info *gpu_info)
{
    os_graphic_t      *graphic = (os_graphic_t *)dev;
    os_graphic_info_t *info    = &graphic->info;

    char    *destPixels = gpu_info->dest;
    int      dbpl       = info->bytes_per_line;
    uint8_t  dbpp       = info->bits_per_pixel / 8;
    uint32_t color      = gpu_info->color;
    char    *map        = gpu_info->src;
    int      mapWidth   = gpu_info->width;
    int      mapHeight  = gpu_info->height;
    int      mapStride  = gpu_info->sbpl;

    hdma2d.Init.Mode         = DMA2D_M2M_BLEND;
    hdma2d.Init.OutputOffset = dbpl / dbpp - mapWidth;

    hdma2d.LayerCfg[1].AlphaMode      = DMA2D_COMBINE_ALPHA;
    hdma2d.LayerCfg[1].InputAlpha     = color;
    hdma2d.LayerCfg[1].InputColorMode = DMA2D_INPUT_A8;
    hdma2d.LayerCfg[1].InputOffset    = mapStride - mapWidth;

    hdma2d.LayerCfg[0].AlphaMode  = DMA2D_NO_MODIF_ALPHA;
    hdma2d.LayerCfg[0].InputAlpha = 0xff;
    if (dbpp == 2)
        hdma2d.LayerCfg[0].InputColorMode = DMA2D_INPUT_RGB565;
    else if (dbpp == 3)
        hdma2d.LayerCfg[0].InputColorMode = DMA2D_INPUT_RGB888;
    else
        hdma2d.LayerCfg[0].InputColorMode = DMA2D_INPUT_ARGB8888;

    hdma2d.LayerCfg[0].InputOffset = dbpl / dbpp - mapWidth;
    hdma2d.XferCpltCallback        = stm32_dma2d_trans_complete;
    hdma2d.XferErrorCallback       = stm32_dma2d_trans_error;

    os_event_recv(st_lcd->lcd_event,
                  LCD_EVETN_DMA2D_FINSH,
                  OS_EVENT_OPTION_OR | OS_EVENT_OPTION_CLEAR,
                  OS_WAIT_FOREVER,
                  OS_NULL);

    if (HAL_DMA2D_Init(&hdma2d) != HAL_OK)
    {
        os_kprintf("stm32_gpu_alphamapblend error\r\n");
        return;
    }

    HAL_DMA2D_ConfigLayer(&hdma2d, 0);
    HAL_DMA2D_ConfigLayer(&hdma2d, 1);

    HAL_DMA2D_BlendingStart_IT(&hdma2d, (uint32_t)map, (uint32_t)destPixels, (uint32_t)destPixels, mapWidth, mapHeight);
}

#endif

static void stm32_lcd_flush(struct os_device *dev)
{
    os_graphic_t *graphic = (os_graphic_t *)dev;

    stm32_lcd_inst_flush(graphic);
}

__WEAK void stm32_lcd_display_on(struct os_device *dev, os_bool_t on_off)
{
    return;
}

const static struct os_graphic_ops ops = {
    .display_on   = stm32_lcd_display_on,
    .display_area = OS_NULL,
    .frame_flush  = stm32_lcd_flush,

#ifdef OS_GRAPHIC_GPU_ENABLE
    .gpu_blend          = stm32_dma2d_blend,
    .gpu_alphamap_blend = stm32_dma2d_alphamapblend,
    .gpu_blit           = stm32_dma2d_blit,
    .gpu_wait_finish    = stm32_dma2d_wait_finish,
#endif
};

static int stm32_lcd_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    uint8_t          *fb;
    uint32_t          size;
    st_lcd = os_calloc(1, sizeof(struct stm32_lcd));
    OS_ASSERT(st_lcd);

    st_lcd->graphic.ops = &ops;

    os_graphic_register("lcd", &st_lcd->graphic);

    size = OS_GRAPHIC_LCD_WIDTH * OS_GRAPHIC_LCD_HEIGHT * OS_GRAPHIC_LCD_DEPTH / 8;

#ifdef LCD_FRAME_BUFFER_1
    fb = (uint8_t *)LCD_FRAME_BUFFER_1;
#else
    fb = (uint8_t *)os_dma_malloc_align(size, BSP_CACHE_LINE_SIZE);
#endif
    OS_ASSERT(fb != OS_NULL);
    os_graphic_add_framebuffer(&st_lcd->graphic.parent, fb, size);

#ifdef LCD_USE_DOUBLE_BUFFER
#ifdef LCD_FRAME_BUFFER_2
    fb = (uint8_t *)LCD_FRAME_BUFFER_2;
#else
    fb = (uint8_t *)os_dma_malloc_align(size, BSP_CACHE_LINE_SIZE);
#endif
    OS_ASSERT(fb != OS_NULL);
    os_graphic_add_framebuffer(&st_lcd->graphic.parent, fb, size);
#endif

    st_lcd->lcd_event = os_event_create(OS_NULL, "lcd_event");
    if (st_lcd->lcd_event == OS_NULL)
    {
        goto __err_exit;
    }
    os_event_send(st_lcd->lcd_event, LCD_EVETN_LCD_FLUSH | LCD_EVETN_DMA2D_FINSH);
    stm32_lcd_inst_init(&st_lcd->graphic);
#ifdef OS_GRAPHIC_GPU_ENABLE
    if (stm32_dma2d_init() != OS_SUCCESS)
    {
        goto __err_exit;
    }
#endif

    return OS_SUCCESS;
__err_exit:
    if (st_lcd)
    {
        if (st_lcd->lcd_event)
        {
            os_event_destroy(st_lcd->lcd_event);
        }

        os_free(st_lcd);
    }

    return OS_FAILURE;
}

OS_DRIVER_INFO stm32_lcd_driver = {
    .name  = "LTDC_HandleTypeDef",
    .probe = stm32_lcd_probe,
};

OS_DRIVER_DEFINE(stm32_lcd_driver, OS_INIT_LEVEL_DEVICE, OS_INIT_SUBLEVEL_MIDDLE);

