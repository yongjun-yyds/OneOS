/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_lcd_instance.c
 *
 * @brief       This file implements lcd driver instance for stm32.
 *
 * @revision
 * Date         Author          Notes
 * 2020-09-30   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <board.h>
#include <drv_cfg.h>
#include <graphic/graphic.h>
#include <graphic/dsi.h>

#if (defined STM32_USE_LCD_DSI_VEDIO)

extern LTDC_HandleTypeDef hltdc;

static volatile int32_t g_st_frame_done = 0;

void HAL_LTDC_LineEventCallback(LTDC_HandleTypeDef *hltdc)
{
    HAL_LTDC_ProgramLineEvent(hltdc, OS_GRAPHIC_LCD_HEIGHT - 1);

    g_st_frame_done = 1;
}

void stm32_lcd_inst_init(os_graphic_t *graphic)
{
    os_device_dsi_t *dsi;

    /* Set framebuffer for layer */
    LTDC_LayerCfgTypeDef layer_cfg = hltdc.LayerCfg[0];
    layer_cfg.FBStartAdress        = (uint32_t)graphic->info.framebuffer_curr;
    HAL_LTDC_ConfigLayer(&hltdc, &layer_cfg, 0);

    HAL_LTDC_ProgramLineEvent(&hltdc, OS_GRAPHIC_LCD_HEIGHT - 1);

    dsi = (os_device_dsi_t *)os_device_find("dsi");
    if (dsi != OS_NULL)
    {
        os_dsi_start(dsi);
    }
}

void stm32_lcd_inst_flush(os_graphic_t *graphic)
{
    OS_ASSERT(graphic->info.framebuffer_curr);

    g_st_frame_done = 0;
    while (!g_st_frame_done)
    {
    }

    LTDC_LAYER(&hltdc, 0)->CFBAR = (uint32_t)(graphic->info.framebuffer_curr);
    __HAL_LTDC_RELOAD_IMMEDIATE_CONFIG(&hltdc);
}

#elif (defined STM32_USE_LCD_DSI_COMMAND)

extern LTDC_HandleTypeDef hltdc;
extern DSI_HandleTypeDef  hdsi;

static volatile int32_t g_st_frame_done = 0;

void HAL_DSI_TearingEffectCallback(DSI_HandleTypeDef *hdsi)
{
    g_st_frame_done = 1;
}

void HAL_DSI_EndOfRefreshCallback(DSI_HandleTypeDef *hdsi)
{
    g_st_frame_done = 1;
}

void stm32_lcd_inst_init(os_graphic_t *graphic)
{
    os_device_dsi_t *dsi;

    /* Set framebuffer for layer */
    LTDC_LayerCfgTypeDef layer_cfg = hltdc.LayerCfg[0];
    layer_cfg.FBStartAdress        = (uint32_t)(graphic->info.framebuffer_curr);
    HAL_LTDC_ConfigLayer(&hltdc, &layer_cfg, 0);

    dsi = (os_device_dsi_t *)os_device_find("dsi");
    if (dsi != OS_NULL)
    {
        os_dsi_start(dsi);
    }
}

void stm32_lcd_inst_flush(os_graphic_t *graphic)
{
    g_st_frame_done = 0;

    HAL_DSI_ShortWrite(&hdsi, 0, DSI_DCS_SHORT_PKT_WRITE_P1, DSI_SET_TEAR_ON, 0x00);
    HAL_DSI_Refresh(&hdsi);

    while (!g_st_frame_done)
    {
    }
}

#elif (defined STM32_USE_LCD_RGB)

extern LTDC_HandleTypeDef hltdc;

void stm32_lcd_inst_init(os_graphic_t *graphic)
{
    /* Set framebuffer for layer */
    LTDC_LayerCfgTypeDef layer_cfg = hltdc.LayerCfg[0];
    layer_cfg.FBStartAdress        = (uint32_t)(graphic->info.framebuffer_curr);

    HAL_LTDC_ConfigLayer(&hltdc, &layer_cfg, 0);
    HAL_LTDC_SetWindowPosition(&hltdc, 0, 0, 0);
    HAL_LTDC_SetWindowSize(&hltdc, OS_GRAPHIC_LCD_WIDTH, OS_GRAPHIC_LCD_HEIGHT, 0);
}

void stm32_lcd_inst_flush(os_graphic_t *graphic)
{   
#ifdef OS_GRAPHIC_DRAW_ENABLE
    os_clean_dcache(graphic->info.framebuffer_curr, graphic->info.framebuffer_size);
#endif
    return;
}

#endif
