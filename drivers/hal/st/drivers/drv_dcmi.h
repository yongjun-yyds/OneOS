#ifndef __DRV_DCMI_H__
#define __DRV_DCMI_H__

#include <board.h>
#include <os_mq.h>
#include <stdint.h>

os_err_t cam_dcmi_irq_init(void);
void     dcmi_start(void);
void     dcmi_stop(void);
bool     ll_cam_dma_sizes(cam_obj_t *cam);
os_err_t ll_cam_dma_start(cam_obj_t *cam);

#endif
