#include <drv_jpeg.h>

extern JPEG_HandleTypeDef hjpeg;

typedef struct
{
    uint8_t *Buffer;
    uint32_t BufferSize;
    uint32_t Count;
} JPEG_Data_BufferTypeDef;

static JPEG_Data_BufferTypeDef In_Buff;
static JPEG_Data_BufferTypeDef Out_Buff;
#define MDMA_DATA_LENGTHIN  (64 * 1024)
#define MDMA_DATA_LENGTHOUT (64 * 1024)

static void jpeg_data_in_config(uint8_t *source, uint32_t source_length)
{
    In_Buff.Buffer     = source;
    In_Buff.BufferSize = source_length + 32;
    In_Buff.Count      = 0;
}

static void jpeg_data_out_config(uint8_t *dest_addr, uint32_t dest_area_length)
{
    Out_Buff.Buffer     = dest_addr;
    Out_Buff.BufferSize = dest_area_length;
    Out_Buff.Count      = 0;
}

/* start jpeg decode */
static void jpeg_start_mdma_decode(JPEG_HandleTypeDef *hjpeg)
{
    if (In_Buff.BufferSize <= MDMA_DATA_LENGTHIN && Out_Buff.BufferSize <= MDMA_DATA_LENGTHOUT)
    {
        HAL_JPEG_Decode_DMA(hjpeg, In_Buff.Buffer, In_Buff.BufferSize, Out_Buff.Buffer, Out_Buff.BufferSize);
    }
    else if (In_Buff.BufferSize > MDMA_DATA_LENGTHIN && Out_Buff.BufferSize <= MDMA_DATA_LENGTHOUT)
    {
        HAL_JPEG_Decode_DMA(hjpeg, In_Buff.Buffer, MDMA_DATA_LENGTHIN, Out_Buff.Buffer, Out_Buff.BufferSize);
    }
    else if (In_Buff.BufferSize <= MDMA_DATA_LENGTHIN && Out_Buff.BufferSize > MDMA_DATA_LENGTHOUT)
    {
        HAL_JPEG_Decode_DMA(hjpeg, In_Buff.Buffer, In_Buff.BufferSize, Out_Buff.Buffer, MDMA_DATA_LENGTHOUT);
    }
    else
    {
        HAL_JPEG_Decode_DMA(hjpeg, In_Buff.Buffer, MDMA_DATA_LENGTHIN, Out_Buff.Buffer, MDMA_DATA_LENGTHOUT);
    }
}

void jpeg_start_decode(JPEG_HandleTypeDef *hjpeg,
                       uint8_t         *source,
                       uint32_t         source_length,
                       uint8_t         *yuv_addr,
                       uint32_t         yuv_length)
{
    jpeg_data_in_config(source, source_length);
    jpeg_data_out_config(yuv_addr, yuv_length);
    jpeg_start_mdma_decode(hjpeg);
}

/* JPEG Get Data callback */
void HAL_JPEG_GetDataCallback(JPEG_HandleTypeDef *hjpeg, uint32_t NbDecodedData)
{
    uint32_t inDataLength;
    In_Buff.Count += NbDecodedData;
    In_Buff.Buffer += NbDecodedData;

    if (In_Buff.Count >= In_Buff.BufferSize)
    {
        inDataLength = 0;
        HAL_JPEG_Pause(hjpeg, JPEG_PAUSE_RESUME_INPUT);
    }
    else
    {
        if (In_Buff.BufferSize - In_Buff.Count > MDMA_DATA_LENGTHIN)
        {
            inDataLength = MDMA_DATA_LENGTHIN;
        }
        else
            inDataLength = In_Buff.BufferSize - In_Buff.Count;
    }
    HAL_JPEG_ConfigInputBuffer(hjpeg, In_Buff.Buffer, inDataLength);
}

/* JPEG out Data or end decode callback */
void HAL_JPEG_DataReadyCallback(JPEG_HandleTypeDef *hjpeg, uint8_t *pDataOut, uint32_t OutDataLength)
{
    /* Update JPEG encoder output buffer address*/
    Out_Buff.Count += OutDataLength;
    Out_Buff.Buffer += OutDataLength;

    if (Out_Buff.BufferSize - Out_Buff.Count > MDMA_DATA_LENGTHOUT)
        HAL_JPEG_ConfigOutputBuffer(hjpeg, (uint8_t *)Out_Buff.Buffer, MDMA_DATA_LENGTHOUT);
    else
        HAL_JPEG_ConfigOutputBuffer(hjpeg, (uint8_t *)Out_Buff.Buffer, (Out_Buff.BufferSize - Out_Buff.Count));
}

void HAL_JPEG_InfoReadyCallback(JPEG_HandleTypeDef *hjpeg, JPEG_ConfTypeDef *pInfo)
{
}

__WEAK void Jpeg_Decode_CpltCallback(void)
{
    os_kprintf("jpeg decode complete\r\n");
}

void HAL_JPEG_DecodeCpltCallback(JPEG_HandleTypeDef *hjpeg)
{
    HAL_JPEG_Abort(hjpeg);
    Jpeg_Decode_CpltCallback();
}

void HAL_JPEG_ErrorCallback(JPEG_HandleTypeDef *hjpeg)
{
    os_kprintf("jpeg decode error\r\n");
}

static int stm32_jpeg_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    os_err_t result = 0;

    struct stm32_jpeg_info *st_jpeg = os_calloc(1, sizeof(struct stm32_jpeg_info));

    OS_ASSERT(st_jpeg);

    st_jpeg->hjpeg = (JPEG_HandleTypeDef *)dev->info;

    st_jpeg->jpeg_dev.type = OS_DEVICE_TYPE_MISCELLANEOUS;
    st_jpeg->jpeg_dev.ops  = OS_NULL;

    result = os_device_register(&st_jpeg->jpeg_dev, "jpeg");
    OS_ASSERT(result == OS_SUCCESS);

    return result;
}

OS_DRIVER_INFO stm32_jpeg_driver = {
    .name  = "JPEG_HandleTypeDef",
    .probe = stm32_jpeg_probe,
};

OS_DRIVER_DEFINE(stm32_jpeg_driver, OS_INIT_LEVEL_DEVICE, OS_INIT_SUBLEVEL_LOW);
