/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_dcmi.c
 *
 * @brief       This file implements dcmi driver for stm32
 *
 * @revision
 * Date         Author          Notes
 * 2022-07-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <stdio.h>
#include <string.h>
#include "ll_cam.h"
#include <board.h>
#include <os_memory.h>
#include <bus/bus.h>
#include <os_stddef.h>
#include <string.h>
#define DRV_EXT_TAG "drv.dcmi"
#include <drv_log.h>
#include <drv_dcmi.h>
#include <stdlib.h>

extern DCMI_HandleTypeDef hdcmi;
extern DMA_HandleTypeDef  hdma_dcmi;

struct stm32_dcmi
{
    DCMI_HandleTypeDef *hdcmi;
    DMA_HandleTypeDef  *hdma_dcmi;
    cam_obj_t          *cam;
};
static struct stm32_dcmi *dcmi_cam;

void HAL_DCMI_FrameEventCallback(DCMI_HandleTypeDef *hdcmi)
{
    DMA_Stream_TypeDef *dma_stream = (DMA_Stream_TypeDef *)hdma_dcmi.Instance;
    ll_cam_dma_stop(dcmi_cam->cam);

    __HAL_DCMI_ENABLE_IT(hdcmi, DCMI_IT_FRAME);
    if (dcmi_cam->cam->jpeg_mode)
    {
        if (dma_stream->CR & (1 << 19))    // dual buff use the second buf
        {
            os_invalid_dcache((dcmi_cam->cam->dma_buffer + dcmi_cam->cam->dma_half_buffer_size),
                              dcmi_cam->cam->dma_half_buffer_size);
            ll_cam_send_event(dcmi_cam->cam, CAM_SEM_VSYNC_BUF1);
        }
        else
        {
            os_invalid_dcache((dcmi_cam->cam->dma_buffer), dcmi_cam->cam->dma_half_buffer_size);
            ll_cam_send_event(dcmi_cam->cam, CAM_SEM_VSYNC_BUF0);
        }
    }
    else
    {
        ll_cam_send_event(dcmi_cam->cam, CAM_SEM_VSYNC_END);
    }
}

static void XferCpltCallback_M0(struct __DMA_HandleTypeDef *hdma)
{
    os_invalid_dcache((dcmi_cam->cam->dma_buffer), dcmi_cam->cam->dma_half_buffer_size);
    ll_cam_send_event(dcmi_cam->cam, CAM_SEM_DMA_BUF0);
}
static void XferCpltCallback_M1(struct __DMA_HandleTypeDef *hdma)
{
    os_invalid_dcache((dcmi_cam->cam->dma_buffer + dcmi_cam->cam->dma_half_buffer_size),
                      dcmi_cam->cam->dma_half_buffer_size);
    ll_cam_send_event(dcmi_cam->cam, CAM_SEM_DMA_BUF1);
}

os_err_t ll_cam_dma_start(cam_obj_t *cam)
{
    __HAL_UNLOCK(&hdma_dcmi);

    HAL_DMAEx_MultiBufferStart(&hdma_dcmi,
                               (uint32_t)&DCMI->DR,
                               (uint32_t)cam->dma_buffer,
                               (uint32_t)(cam->dma_buffer + cam->dma_half_buffer_size),
                               cam->dma_half_buffer_size / 4);
    __HAL_DMA_ENABLE_IT(&hdma_dcmi, DMA_IT_TC);
    return OS_SUCCESS;
}

os_err_t ll_cam_dma_stop(cam_obj_t *cam)
{
    __HAL_DMA_DISABLE_IT(&hdma_dcmi, DMA_IT_TC);
    __HAL_DMA_DISABLE(&hdma_dcmi);    // disable dma
    hdma_dcmi.State = HAL_DMA_STATE_READY;
    return OS_SUCCESS;
}

/* init some hardware, stm32 has been initialized */
os_err_t ll_cam_config(cam_obj_t *cam, const camera_config_t *config)
{
    dcmi_cam = os_calloc(1, sizeof(struct stm32_dcmi));
    OS_ASSERT(dcmi_cam);

    hdma_dcmi.XferCpltCallback   = XferCpltCallback_M0;
    hdma_dcmi.XferM1CpltCallback = XferCpltCallback_M1;

    dcmi_cam->cam       = cam;
    dcmi_cam->hdcmi     = &hdcmi;
    dcmi_cam->hdma_dcmi = &hdma_dcmi;

    return OS_SUCCESS;
}

os_err_t ll_cam_deinit(cam_obj_t *cam)
{
    if (dcmi_cam != OS_NULL)
    {
        os_free(dcmi_cam);
    }
    return OS_SUCCESS;
}

/* init some pin, stm32 has been initialized */
os_err_t ll_cam_set_pin(cam_obj_t *cam, const camera_config_t *config)
{
    return OS_SUCCESS;
}

os_err_t cam_dcmi_irq_init(void)
{
    DCMI->IER = 0x0;
    __HAL_DCMI_ENABLE_IT(&hdcmi, DCMI_IT_FRAME);    // enable frame irq
    __HAL_DCMI_ENABLE(&hdcmi);                      // enable DCMI
    return OS_SUCCESS;
}

void dcmi_start(void)
{
    DCMI->CR &= ~DCMI_CR_CAPTURE;    // disable capture
    while (DCMI->CR & 0X01)
        ;
    __HAL_DCMI_DISABLE(&hdcmi);
    __HAL_DCMI_ENABLE(&hdcmi);
    DCMI->CR |= DCMI_CR_CAPTURE;
}

void dcmi_stop(void)
{
    DCMI->CR &= ~DCMI_CR_CAPTURE;    // disable capture
    while (DCMI->CR & 0X01)
        ;
    __HAL_DCMI_DISABLE(&hdcmi);
}

uint8_t ll_cam_get_dma_align(cam_obj_t *cam)
{
    return 32;
}

static bool ll_cam_calc_rgb_dma(cam_obj_t *cam)
{
    size_t node_max       = LCD_CAM_DMA_NODE_BUFFER_MAX_SIZE / cam->dma_bytes_per_item;
    size_t line_width     = cam->width * cam->in_bytes_per_pixel;
    size_t node_size      = node_max;
    size_t lines_per_node = 1;

    // Calculate DMA Node Size so that it's divisable by or divisor of the line width
    if (line_width >= node_max)
    {
        // One or more nodes will be requied for one line
        for (size_t i = node_max; i > 0; i = i - 1)
        {
            if ((line_width % i) == 0)
            {
                node_size = i;
                break;
            }
        }
    }
    else
    {
        // One or more lines can fit into one node
        for (size_t i = node_max; i > 0; i = i - 1)
        {
            if ((i % line_width) == 0)
            {
                node_size      = i;
                lines_per_node = node_size / line_width;
                while ((cam->height % lines_per_node) != 0)
                {
                    lines_per_node = lines_per_node - 1;
                    node_size      = lines_per_node * line_width;
                }
                break;
            }
        }
    }

    LOG_I(DRV_EXT_TAG, "node_size: %4u, lines_per_node: %u", node_size * cam->dma_bytes_per_item, lines_per_node);

    cam->dma_node_buffer_size = node_size * cam->dma_bytes_per_item;

    cam->dma_buffer_size      = 2 * cam->dma_node_buffer_size;
    cam->dma_half_buffer_size = cam->dma_node_buffer_size;
    cam->dma_half_buffer_cnt  = line_width * cam->height * cam->in_bytes_per_pixel / cam->dma_half_buffer_size;

    LOG_I(DRV_EXT_TAG,
          "dma_buffer_size: %5u, dma_half_buffer_cnt: %2u",
          cam->dma_buffer_size,
          cam->dma_half_buffer_cnt);

    return 1;
}

bool ll_cam_dma_sizes(cam_obj_t *cam)
{
    cam->dma_bytes_per_item = 1;
    if (cam->jpeg_mode)
    {
        cam->dma_half_buffer_cnt  = 2;
        cam->dma_buffer_size      = cam->dma_half_buffer_cnt * 2048;
        cam->dma_half_buffer_size = cam->dma_buffer_size / cam->dma_half_buffer_cnt;
        cam->dma_node_buffer_size = cam->dma_half_buffer_size;
    }
    else
    {
        return ll_cam_calc_rgb_dma(cam);
    }
    return 1;
}

size_t ll_cam_memcpy(cam_obj_t *cam, uint8_t *out, const uint8_t *in, size_t len)
{
    // YUV to Grayscale
    if (cam->in_bytes_per_pixel == 2 && cam->fb_bytes_per_pixel == 1)
    {
        size_t end = len / 8;
        for (size_t i = 0; i < end; ++i)
        {
            out[0] = in[0];
            out[1] = in[2];
            out[2] = in[4];
            out[3] = in[6];
            out += 4;
            in += 8;
        }
        return len / 2;
    }

    memcpy(out, in, len);
    os_clean_dcache(out,len);
    return len;
}

os_err_t ll_cam_set_sample_mode(cam_obj_t *cam, pixformat_t pix_format, uint32_t xclk_freq_hz, uint16_t sensor_pid)
{
    if (pix_format == PIXFORMAT_GRAYSCALE)
    {
        if (sensor_pid == OV3660_PID || sensor_pid == OV5640_PID || sensor_pid == NT99141_PID)
        {
            cam->in_bytes_per_pixel = 1;    // camera sends Y8
        }
        else
        {
            cam->in_bytes_per_pixel = 2;    // camera sends YU/YV
        }
        cam->fb_bytes_per_pixel = 1;    // frame buffer stores Y8
    }
    else if (pix_format == PIXFORMAT_YUV422 || pix_format == PIXFORMAT_RGB565)
    {
        cam->in_bytes_per_pixel = 2;    // camera sends YU/YV
        cam->fb_bytes_per_pixel = 2;    // frame buffer stores YU/YV/RGB565
    }
    else if (pix_format == PIXFORMAT_JPEG)
    {
        cam->in_bytes_per_pixel = 1;
        cam->fb_bytes_per_pixel = 1;
    }
    else
    {
        LOG_E(DRV_EXT_TAG, "Requested format is not supported");
        return OS_FAILURE;
    }
    return OS_SUCCESS;
}
