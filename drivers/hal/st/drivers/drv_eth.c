/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_eth.c
 *
 * @brief       This file implements eth driver.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include "board.h"
#include <os_memory.h>
#include <dma.h>
#include <string.h>
#include <bus/bus.h>
#include <net_dev.h>
#include <timer/clocksource.h>
#include "drv_eth.h"

/*
 * Emac driver uses CubeMX tool to generate emac and phy's configuration,
 * the configuration files can be found in CubeMX_Config folder.
 */

#define DRV_EXT_LVL DBG_EXT_INFO
#define DRV_EXT_TAG "drv.eth"
#include <dlog.h>

#ifdef SERIES_STM32H7
#define ETH_DMA_TRANSMIT_TIMEOUT (2000U)

extern ETH_TxPacketConfig TxConfig;
extern ETH_DMADescTypeDef DMARxDscrTab[ETH_RX_DESC_CNT];
extern ETH_DMADescTypeDef DMATxDscrTab[ETH_TX_DESC_CNT];
extern uint8_t            Rx_Buff[ETH_RX_DESC_CNT][ETH_MAX_PACKET_SIZE];

ETH_BufferTypeDef *rxbuff_dscrtab = OS_NULL;
ETH_BufferTypeDef *txbuff_dscrtab = OS_NULL;
#endif

struct os_stm32_eth
{
    struct os_net_device net_dev;
    struct napi_struct napi;

    ETH_HandleTypeDef *EthHandle;

    uint8_t dev_addr[OS_NET_MAC_LENGTH];
    uint32_t   eth_speed;
    uint32_t   eth_mode;

    ETH_DMADescTypeDef *DMARxDscrTab;
    ETH_DMADescTypeDef *DMATxDscrTab;

    uint8_t *rxbuff;
    uint8_t *txbuff;
    
    int phy_addr;
    int link;

    os_base_t reset_pin;
    os_base_t reset_value;

    os_timer_id timer;

    os_list_node_t list;
};

static os_list_node_t stm32_eth_list = OS_LIST_INIT(stm32_eth_list);

void HAL_ETH_RxCpltCallback(ETH_HandleTypeDef *heth)
{
    struct os_stm32_eth *eth_dev = OS_NULL;

    os_list_for_each_entry(eth_dev, &stm32_eth_list, struct os_stm32_eth, list)
    {
        if (eth_dev->EthHandle == heth)
        {
            napi_schedule(&eth_dev->napi);
            break;
        }
    }
}

void HAL_ETH_ErrorCallback(ETH_HandleTypeDef *heth)
{
    LOG_E(DRV_EXT_TAG, "eth err");
}

OS_WEAK os_err_t phy_reset(struct os_stm32_eth *eth_dev)
{
    os_task_msleep(100);
    os_pin_write(eth_dev->reset_pin, eth_dev->reset_value);
    os_task_msleep(100);
    os_pin_write(eth_dev->reset_pin, !eth_dev->reset_value);
    os_task_msleep(100);

    return OS_SUCCESS;
}

OS_WEAK os_err_t phy_gpio_init(struct os_stm32_eth *eth_dev)
{
    if (ETH_RESET_PIN < 0)
        return OS_INVAL;

    eth_dev->reset_pin = ETH_RESET_PIN;

#ifdef ETH_RESET_PIN_ACTIVE_HIGH
    eth_dev->reset_value = PIN_HIGH;
#else
    eth_dev->reset_value = PIN_LOW;
#endif

    os_pin_mode(eth_dev->reset_pin, PIN_MODE_OUTPUT);

    return OS_SUCCESS;
}

static os_err_t os_stm32_eth_init(struct os_net_device *net_dev)
{
    struct os_stm32_eth *eth_dev = (struct os_stm32_eth *)net_dev;

    if (phy_gpio_init(eth_dev))
    {
        LOG_E(DRV_EXT_TAG, "phy_gpio_init failed");
    }
    
    phy_reset(eth_dev);

#ifndef SERIES_STM32H7
    eth_dev->EthHandle->Init.MACAddr = (uint8_t *)&eth_dev->dev_addr[0];
    eth_dev->EthHandle->Init.RxMode = ETH_RXINTERRUPT_MODE;
#endif
    
    if (HAL_ETH_Init(eth_dev->EthHandle) != HAL_OK)
    {
        LOG_E(DRV_EXT_TAG, "eth hardware init failed");
        return OS_FAILURE;
    }
    
#ifndef SERIES_STM32H7
#define MACFFR_FILTER_MODE_MASK                                                                                        \
    ETH_MULTICASTFRAMESFILTER_PERFECTHASHTABLE | ETH_MULTICASTFRAMESFILTER_HASHTABLE |                                 \
        ETH_MULTICASTFRAMESFILTER_PERFECT | ETH_MULTICASTFRAMESFILTER_NONE |                                           \
        ETH_UNICASTFRAMESFILTER_PERFECTHASHTABLE | ETH_UNICASTFRAMESFILTER_HASHTABLE | ETH_UNICASTFRAMESFILTER_PERFECT
    uint32_t tmpreg;
    /*config frame filter : group use hashtable; idividual use perfect*/
    tmpreg = eth_dev->EthHandle->Instance->MACFFR;
    tmpreg &= ~MACFFR_FILTER_MODE_MASK;
    tmpreg |= ETH_MULTICASTFRAMESFILTER_PERFECTHASHTABLE;

    eth_dev->EthHandle->Instance->MACFFR = tmpreg;
    tmpreg                               = eth_dev->EthHandle->Instance->MACFFR;
    HAL_Delay(ETH_REG_WRITE_DELAY);
    eth_dev->EthHandle->Instance->MACFFR = tmpreg;

    HAL_ETH_DMATxDescListInit(eth_dev->EthHandle, eth_dev->DMATxDscrTab, eth_dev->txbuff, ETH_TXBUFNB);
    HAL_ETH_DMARxDescListInit(eth_dev->EthHandle, eth_dev->DMARxDscrTab, eth_dev->rxbuff, ETH_RXBUFNB);

    if (HAL_ETH_Stop(eth_dev->EthHandle) != HAL_OK)
    {
        LOG_E(DRV_EXT_TAG, "emac hardware stop faild");
        return OS_FAILURE;
    }
#else
    uint8_t                 i = 0;
    ETH_MACFilterConfigTypeDef FilterConfig;
    memset(&FilterConfig, 0, sizeof(ETH_MACFilterConfigTypeDef));

    HAL_ETH_SetMDIOClockRange(eth_dev->EthHandle);

#ifdef NET_USING_HGN     
    FilterConfig.ReceiveAllMode      = ENABLE;
#else
    FilterConfig.HashMulticast       = ENABLE;
    FilterConfig.HachOrPerfectFilter = ENABLE;
    FilterConfig.BroadcastFilter     = ENABLE;
#endif
    HAL_ETH_SetMACFilterConfig(eth_dev->EthHandle, &FilterConfig);
    
    for (i = 0; i < ETH_RX_DESC_CNT; i++)
    {
        HAL_ETH_DescAssignMemory(eth_dev->EthHandle, i, Rx_Buff[i], NULL);
    }
#endif

    return OS_SUCCESS;
}

static os_err_t os_stm32_eth_deinit(struct os_net_device *net_dev)
{
    struct os_stm32_eth *eth_dev = (struct os_stm32_eth *)net_dev;

    HAL_ETH_DeInit(eth_dev->EthHandle);

    phy_reset(eth_dev);

    return OS_SUCCESS;
}

#ifndef SERIES_STM32H7
#define ETH_MACADDR_LBASE ETH_MAC_ADDR_LBASE
#define ETH_MACADDR_HBASE ETH_MAC_ADDR_HBASE
#else
#define ETH_MACADDR_LBASE (uint32_t)(heth->Instance->MACA0HR)
#define ETH_MACADDR_HBASE (uint32_t)(heth->Instance->MACA1HR)
#endif
static os_err_t os_stm32_eth_set_perfect_filter(ETH_HandleTypeDef *heth, uint8_t *addr, os_bool_t enable)
{
    uint8_t  i        = 0;
    uint32_t tmpreg_h = 0;
    uint32_t tmpreg_l = 0;
    uint32_t addr_h   = 0;
    uint32_t addr_l   = 0;

    if (enable)
    {
        for (i = 1; i < 4; i++)
        {
            tmpreg_h = (*(__IO uint32_t *)((uint32_t)(ETH_MACADDR_HBASE + i * 8)));
            tmpreg_l = (*(__IO uint32_t *)((uint32_t)(ETH_MACADDR_LBASE + i * 8)));
            if (tmpreg_h || tmpreg_l)
            {
                break;
            }
        }

        if (i == 4)
        {
            LOG_E(DRV_EXT_TAG, "perfect filter is full!");
            return OS_FULL;
        }

        addr_h = ((uint32_t)addr[5] << 8) | (uint32_t)addr[4] | 0x80000000;
        (*(__IO uint32_t *)((uint32_t)(ETH_MACADDR_HBASE + i * 8))) = addr_h;

        addr_l = ((uint32_t)addr[3] << 24) | ((uint32_t)addr[2] << 16) | ((uint32_t)addr[1] << 8) | addr[0];
        (*(__IO uint32_t *)((uint32_t)(ETH_MACADDR_LBASE + i * 8))) = addr_l;
    }
    else
    {
        addr_h = ((uint32_t)addr[5] << 8) | (uint32_t)addr[4] | 0x80000000;
        addr_l = ((uint32_t)addr[3] << 24) | ((uint32_t)addr[2] << 16) | ((uint32_t)addr[1] << 8) | addr[0];

        for (i = 1; i < 4; i++)
        {
            tmpreg_h = (*(__IO uint32_t *)((uint32_t)(ETH_MACADDR_HBASE + i * 8)));
            tmpreg_l = (*(__IO uint32_t *)((uint32_t)(ETH_MACADDR_LBASE + i * 8)));
            if ((tmpreg_h == addr_h) && (tmpreg_l == addr_l))
            {
                (*(__IO uint32_t *)((uint32_t)(ETH_MACADDR_HBASE + i * 8))) = 0;
                (*(__IO uint32_t *)((uint32_t)(ETH_MACADDR_LBASE + i * 8))) = 0;
            }
        }
    }

    return OS_SUCCESS;
}

static uint32_t _calc_filter_hash(uint8_t *addr)
{
    uint32_t crc    = 0xFFFFFFFFU;
    uint32_t count1 = 0;
    uint32_t count2 = 0;

    for (count1 = 0; count1 < OS_NET_MAC_LENGTH; count1++)
    {
        crc = crc ^ addr[count1];
        for (count2 = 0; count2 < 0x08U; count2++)
        {
            if (0U != (crc & 1U))
            {
                crc >>= 1U;
                crc ^= 0xEDB88320U;
            }
            else
            {
                crc >>= 1U;
            }
        }
    }

    count1 = 0;
    count2 = ~crc;
    crc    = 0;

    for (count1 = 0; count1 < 32; count1++)
    {
        if (count2 & (1 << count1))
        {
            crc |= 1 << (31 - count1);
        }
    }

    crc = crc >> 26U;

    return crc;
}

#ifndef SERIES_STM32H7
#define ETH_HASHTABLE_L eth_dev->EthHandle->Instance->MACHTLR
#define ETH_HASHTABLE_H eth_dev->EthHandle->Instance->MACHTHR
#else
#define ETH_HASHTABLE_L eth_dev->EthHandle->Instance->MACHT0R
#define ETH_HASHTABLE_H eth_dev->EthHandle->Instance->MACHT1R
#endif
static os_err_t os_stm32_eth_set_filter(struct os_net_device *net_dev, uint8_t *addr, os_bool_t enable)
{
    uint32_t crc       = 0;
    uint32_t configVal = 0;

    struct os_stm32_eth *eth_dev = (struct os_stm32_eth *)net_dev;

    if (*addr && 0x01)
    {
        crc = _calc_filter_hash(addr);

        configVal = ((uint32_t)1U << (crc & 0x1FU));

        if (enable)
        {
            if (0U != (crc & 0x20U))
            {
                ETH_HASHTABLE_H |= configVal;
            }
            else
            {
                ETH_HASHTABLE_L |= configVal;
            }
        }
        else
        {
            if (0U != (crc & 0x20U))
            {
                ETH_HASHTABLE_H &= ~configVal;
            }
            else
            {
                ETH_HASHTABLE_L &= ~configVal;
            }
        }

        return OS_SUCCESS;
    }
    else
    {
        return os_stm32_eth_set_perfect_filter(eth_dev->EthHandle, addr, enable);
    }
}

static os_err_t os_stm32_eth_get_macaddr(struct os_net_device *net_dev, uint8_t *addr)
{
    struct os_stm32_eth *eth_dev = (struct os_stm32_eth *)net_dev;

    memcpy(addr, eth_dev->dev_addr, OS_NET_MAC_LENGTH);

    return OS_SUCCESS;
}

static os_err_t os_stm32_eth_send(struct os_net_device *net_dev, uint8_t *data, int len)
{
    HAL_StatusTypeDef state;

    struct os_stm32_eth *eth_dev = (struct os_stm32_eth *)net_dev;

    if (len > ETH_MAX_PACKET_SIZE)
    {
        LOG_E(DRV_EXT_TAG, "eth send pack over %d", ETH_MAX_PACKET_SIZE);
        return OS_FAILURE;
    }

#ifndef SERIES_STM32H7
    __IO ETH_DMADescTypeDef *dmatxdesc;

    dmatxdesc = eth_dev->EthHandle->TxDesc;

    while ((dmatxdesc->Status & ETH_DMATXDESC_OWN) != (uint32_t)RESET)
    {
        if ((eth_dev->EthHandle->Instance->DMASR & ETH_DMASR_TUS) != (uint32_t)RESET)
        {
            eth_dev->EthHandle->Instance->DMASR   = ETH_DMASR_TUS;
            eth_dev->EthHandle->Instance->DMATPDR = 0;
        }
    }

    memcpy((uint8_t *)dmatxdesc->Buffer1Addr, data, len);
    
    state = HAL_ETH_TransmitFrame(eth_dev->EthHandle, len);
#else
    memset(txbuff_dscrtab, 0, ETH_TX_DESC_CNT * sizeof(ETH_BufferTypeDef));

    TxConfig.Length             = len;
    TxConfig.TxBuffer           = txbuff_dscrtab;
    
    txbuff_dscrtab[0].buffer    = eth_dev->txbuff;
    txbuff_dscrtab[0].len       = len;
    
    memcpy(eth_dev->txbuff, data, len);

    os_clean_dcache(eth_dev->txbuff, len);
    
    state = HAL_ETH_Transmit(eth_dev->EthHandle, &TxConfig, ETH_DMA_TRANSMIT_TIMEOUT);
#endif
    if (state != HAL_OK)
    {
        LOG_E(DRV_EXT_TAG, "eth transmit frame faild: %d", state);
        return OS_FAILURE;
    }

    return OS_SUCCESS;
}

#ifndef SERIES_STM32H7
static void stm32_eth_recv_reset(struct os_stm32_eth *eth_dev)
{
    uint8_t        i = 0;
    __IO ETH_DMADescTypeDef *dmarxdesc;
    
    dmarxdesc = eth_dev->EthHandle->RxFrameInfos.FSRxDesc;
    for (i = 0; i < eth_dev->EthHandle->RxFrameInfos.SegCount; i++)
    {
        dmarxdesc->Status |= ETH_DMARXDESC_OWN;
        dmarxdesc = (ETH_DMADescTypeDef *)(dmarxdesc->Buffer2NextDescAddr);
    }
    
    eth_dev->EthHandle->RxFrameInfos.SegCount = 0;
    
    if ((eth_dev->EthHandle->Instance->DMASR & ETH_DMASR_RBUS) != (uint32_t)RESET)
    {
        eth_dev->EthHandle->Instance->DMASR   = ETH_DMASR_RBUS;
        eth_dev->EthHandle->Instance->DMARPDR = 0;
    }
}
#endif
static int os_stm32_eth_poll(struct napi_struct *napi, int budget)
{
    int i;
    int length;
    struct os_stm32_eth *eth_dev = (struct os_stm32_eth *)napi->dev;

#ifndef SERIES_STM32H7
    for (i = 0; i < budget; i++)
    {
        HAL_StatusTypeDef state;
    
        state = HAL_ETH_GetReceivedFrame_IT(eth_dev->EthHandle);

        length = eth_dev->EthHandle->RxFrameInfos.length;

        if (state != HAL_OK)
        {
            stm32_eth_recv_reset(eth_dev);
            break;
        }

        if (length <= 0)
        {
            napi_complete_done(napi, i);
            break;
        }

        else if (length > ETH_MAX_PACKET_SIZE)
        {
            os_kprintf("pack size over range, length:%d, MRU:%d\r\n",length,ETH_MAX_PACKET_SIZE);
            break;
        }

        os_net_rx_data(napi->dev, (uint8_t *)eth_dev->EthHandle->RxFrameInfos.buffer, length);
        
        stm32_eth_recv_reset(eth_dev);
    }
    
#else
    for (i = 0; i < budget; i++)
    {
        int j;
        
        if (HAL_ETH_IsRxDataAvailable(eth_dev->EthHandle) == 0)
        {
            break;
        }
        
        memset(rxbuff_dscrtab, 0, ETH_RX_DESC_CNT * sizeof(ETH_BufferTypeDef));
        for (j = 0; j < ETH_RX_DESC_CNT - 1; j++)
        {
            rxbuff_dscrtab[j].next = &rxbuff_dscrtab[j + 1];
        }
        
        HAL_ETH_GetRxDataBuffer(eth_dev->EthHandle, rxbuff_dscrtab);
        HAL_ETH_GetRxDataLength(eth_dev->EthHandle, (uint32_t *)&length);
        HAL_ETH_BuildRxDescriptors(eth_dev->EthHandle);
        
        if (length <= 0)
        {
            napi_complete_done(napi, i);
            break;
        }

        else if (length > ETH_MAX_PACKET_SIZE)
        {
            os_kprintf("pack size over range, length:%d, MRU:%d\r\n",length,ETH_MAX_PACKET_SIZE);
            break;
        }


        os_net_rx_data(napi->dev, rxbuff_dscrtab->buffer, length);
    }
#endif

    /* enable irq */
    
    return i;
}

/* clang-format off */
static HAL_StatusTypeDef os_eth_phyreg_write(ETH_HandleTypeDef *heth, uint32_t PHYAddr, uint32_t PHYReg, uint32_t pRegValue)
{
#ifndef SERIES_STM32H7
    return HAL_ETH_WritePHYRegister(heth, PHYReg, pRegValue);
#else
    return HAL_ETH_WritePHYRegister(heth, PHYAddr, PHYReg, pRegValue);
#endif
}

static HAL_StatusTypeDef os_eth_phyreg_read(ETH_HandleTypeDef *heth, uint32_t PHYAddr, uint32_t PHYReg, uint32_t *pRegValue)
{
#ifndef SERIES_STM32H7
    return HAL_ETH_ReadPHYRegister(heth, PHYReg, pRegValue);
#else
    return HAL_ETH_ReadPHYRegister(heth, PHYAddr, PHYReg, pRegValue);
#endif
}
/* clang-format on */

static void stm32_phy_init(struct os_stm32_eth *eth_dev)
{    
    uint8_t  i = 0;
    uint32_t reg = 0;

    /* phy probe */
    eth_dev->phy_addr = 0xff;
    for (i = 0; i <= 0x1F; i++)
    {
#ifndef SERIES_STM32H7
        eth_dev->EthHandle->Init.PhyAddress = i;
#endif
        os_eth_phyreg_read(eth_dev->EthHandle, i, PHY_ID1_REG, (uint32_t *)&reg);

        if (reg != 0xFFFF && reg != 0x00)
        {
            eth_dev->phy_addr = i;
            os_kprintf("phy addr %d\r\n", eth_dev->phy_addr);
            break;
        }
    }

    if (eth_dev->phy_addr == 0xFF)
    {
        os_kprintf("invalid phy\r\n");
        return;
    }

    /* phy reset */
    os_eth_phyreg_read(eth_dev->EthHandle, eth_dev->phy_addr, PHY_BASIC_CONTROL_REG, (uint32_t *)&reg);
    reg = reg | PHY_BCR_RESET;
    os_eth_phyreg_write(eth_dev->EthHandle, eth_dev->phy_addr, PHY_BASIC_CONTROL_REG, reg);
    
    for (i = 0; i < 50; i++)
    {
        os_eth_phyreg_read(eth_dev->EthHandle, eth_dev->phy_addr, PHY_BASIC_CONTROL_REG, (uint32_t *)&reg);
        if ((reg & PHY_BCR_RESET) == 0)
            break;
        os_task_msleep(20);
    }

    if ((reg & PHY_BCR_RESET) == 0)
    {
        os_kprintf("reset success\r\n");
    }
    else
    {
        os_kprintf("reset failed\r\n");
        return;
    }

    /* phy config */
    os_eth_phyreg_read(eth_dev->EthHandle, eth_dev->phy_addr, PHY_BASIC_CONTROL_REG, (uint32_t *)&reg);
    reg = reg | PHY_BCR_AUTO_NEGOTIATION_EN;
    os_eth_phyreg_write(eth_dev->EthHandle, eth_dev->phy_addr, PHY_BASIC_CONTROL_REG, reg);

    eth_dev->link = PHY_STATUS_LINK_DOWN;
}

static void stm32_phy_event(void *parameter)
{
    int link = PHY_STATUS_LINK_DOWN;
    uint32_t reg = 0;

    struct os_stm32_eth *eth_dev = (struct os_stm32_eth *)parameter;

    os_eth_phyreg_read(eth_dev->EthHandle, eth_dev->phy_addr, PHY_BASIC_STATUS_REG, (uint32_t *)&reg);
    if ((reg & PHY_BSR_LINK_STATUS) == 0)
    {
        link = PHY_STATUS_LINK_DOWN;
    }
    else
    {
        os_eth_phyreg_read(eth_dev->EthHandle, eth_dev->phy_addr, PHY_BASIC_CONTROL_REG, (uint32_t *)&reg);
        if (reg & PHY_BCR_AUTO_NEGOTIATION_EN)
        {
            os_eth_phyreg_read(eth_dev->EthHandle, eth_dev->phy_addr, PHY_STATUS_REG, (uint32_t *)&reg);
            
            if ((reg & PHY_SR_AUTODONE) == 0)
            {
                link = PHY_STATUS_LINK_DOWN;
            }
            else
            {
                reg = reg & PHY_SR_SPEED_INDICATION_MASK;
                if (reg == PHY_SR_100M_FULL_DUPLEX)
                    link = PHY_STATUS_100MBITS_FULLDUPLEX;
                else if (reg == PHY_SR_100M_HALF_DUPLEX)
                    link = PHY_STATUS_100MBITS_HALFDUPLEX;
                else if (reg == PHY_SR_10M_FULL_DUPLEX)
                    link = PHY_STATUS_10MBITS_FULLDUPLEX;
                else
                    link = PHY_STATUS_10MBITS_HALFDUPLEX;
            }
        }
        else
        {
            if ((reg & PHY_BCR_SPEED_SELECT) && (reg & PHY_BCR_DUPLEX_MODE))
                link = PHY_STATUS_100MBITS_FULLDUPLEX;
            else if (reg & PHY_BCR_SPEED_SELECT)
                link = PHY_STATUS_100MBITS_HALFDUPLEX;
            else if (reg & PHY_BCR_DUPLEX_MODE)
                link = PHY_STATUS_10MBITS_FULLDUPLEX;
            else
                link = PHY_STATUS_10MBITS_HALFDUPLEX;
        }
    }
    
    if (eth_dev->link != link)
    {
        if (link == PHY_STATUS_LINK_DOWN)
        {
            os_net_linkchange(&eth_dev->net_dev, OS_FALSE);
#ifndef SERIES_STM32H7
            HAL_ETH_Stop(eth_dev->EthHandle);
#else
            HAL_ETH_Stop_IT(eth_dev->EthHandle);
#endif
        }
        else
        {
#ifndef SERIES_STM32H7
            HAL_ETH_Start(eth_dev->EthHandle);
#else
            ETH_MACConfigTypeDef        MACConf;
            
            HAL_ETH_GetMACConfig(eth_dev->EthHandle, &MACConf);
            if (link == PHY_STATUS_100MBITS_FULLDUPLEX)
            {
                MACConf.DuplexMode  = ETH_FULLDUPLEX_MODE;
                MACConf.Speed       = ETH_SPEED_100M;
            }
            else if (link == PHY_STATUS_100MBITS_HALFDUPLEX)
            {
                MACConf.DuplexMode  = ETH_HALFDUPLEX_MODE;
                MACConf.Speed       = ETH_SPEED_100M;
            }
            else if (link == PHY_STATUS_10MBITS_FULLDUPLEX)
            {
                MACConf.DuplexMode  = ETH_FULLDUPLEX_MODE;
                MACConf.Speed       = ETH_SPEED_10M;
            }
            else
            {
                MACConf.DuplexMode  = ETH_HALFDUPLEX_MODE;
                MACConf.Speed       = ETH_SPEED_10M;
            }
            HAL_ETH_SetMACConfig(eth_dev->EthHandle, &MACConf);
            HAL_ETH_Start_IT(eth_dev->EthHandle);
#endif
            os_net_linkchange(&eth_dev->net_dev, OS_TRUE);
        }
        
        eth_dev->link = link;
    }
}

const static struct os_net_device_ops stm32_net_dev_ops = {
    .init       = os_stm32_eth_init,
    .deinit     = os_stm32_eth_deinit,
    .send       = os_stm32_eth_send,
    .get_mac    = os_stm32_eth_get_macaddr,
    .set_filter = os_stm32_eth_set_filter,
};

static int stm32_eth_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    uint8_t eth_align = BSP_CACHE_LINE_SIZE;

    struct os_stm32_eth *eth_dev = os_calloc(1, sizeof(struct os_stm32_eth));
    OS_ASSERT(eth_dev != OS_NULL);

    eth_dev->EthHandle = (ETH_HandleTypeDef *)dev->info;

#ifndef SERIES_STM32H7
    eth_dev->rxbuff = (uint8_t *)os_dma_malloc_align(ETH_RXBUFNB * ETH_MAX_PACKET_SIZE, eth_align);
    OS_ASSERT(eth_dev->rxbuff != OS_NULL);

    eth_dev->txbuff = (uint8_t *)os_dma_malloc_align(ETH_TXBUFNB * ETH_MAX_PACKET_SIZE, eth_align);
    OS_ASSERT(eth_dev->txbuff != OS_NULL);

    eth_dev->DMARxDscrTab =
        (ETH_DMADescTypeDef *)os_dma_malloc_align(ETH_RXBUFNB * sizeof(ETH_DMADescTypeDef), eth_align);
    OS_ASSERT(eth_dev->DMARxDscrTab != OS_NULL);

    eth_dev->DMATxDscrTab =
        (ETH_DMADescTypeDef *)os_dma_malloc_align(ETH_TXBUFNB * sizeof(ETH_DMADescTypeDef), eth_align);
    OS_ASSERT(eth_dev->DMATxDscrTab != OS_NULL);

    /* OUI 00-80-E1 STMICROELECTRONICS. */
    eth_dev->dev_addr[0] = 0x00;
    eth_dev->dev_addr[1] = 0x80;
    eth_dev->dev_addr[2] = 0xE1;
    /* generate MAC addr from 96bit unique ID (only for test). */
    eth_dev->dev_addr[3] = *(uint8_t *)(UID_BASE + 4);
    eth_dev->dev_addr[4] = *(uint8_t *)(UID_BASE + 2);
    eth_dev->dev_addr[5] = *(uint8_t *)(UID_BASE + 0);
#else
    eth_dev->txbuff = (uint8_t *)os_dma_malloc_align(ETH_TX_DESC_CNT * ETH_MAX_PACKET_SIZE, eth_align);
    OS_ASSERT(eth_dev->txbuff != OS_NULL);

    txbuff_dscrtab = (ETH_BufferTypeDef *)os_dma_malloc_align(ETH_TX_DESC_CNT * sizeof(ETH_BufferTypeDef), eth_align);
    rxbuff_dscrtab = (ETH_BufferTypeDef *)os_dma_malloc_align(ETH_RX_DESC_CNT * sizeof(ETH_BufferTypeDef), eth_align);
    OS_ASSERT(txbuff_dscrtab != OS_NULL);
    OS_ASSERT(rxbuff_dscrtab != OS_NULL);

    // memset(eth_dev->dev_addr, 0, OS_NET_MAC_LENGTH);

    // memcpy(eth_dev->dev_addr, eth_dev->EthHandle->Init.MACAddr, 1);
    // memcpy(eth_dev->dev_addr, eth_dev->EthHandle->Init.MACAddr, 2);
    // memcpy(eth_dev->dev_addr, eth_dev->EthHandle->Init.MACAddr, 3);
    // memcpy(eth_dev->dev_addr, eth_dev->EthHandle->Init.MACAddr, 4);

    // memcpy(eth_dev->dev_addr, eth_dev->EthHandle->Init.MACAddr, OS_NET_MAC_LENGTH);

    /* OUI 00-80-E1 STMICROELECTRONICS. */
    eth_dev->dev_addr[0] = 0x00;
    eth_dev->dev_addr[1] = 0x80;
    eth_dev->dev_addr[2] = 0xE1;
    /* generate MAC addr from 96bit unique ID (only for test). */
    eth_dev->dev_addr[3] = *(uint8_t *)(UID_BASE + 4);
    eth_dev->dev_addr[4] = *(uint8_t *)(UID_BASE + 2);
    eth_dev->dev_addr[5] = *(uint8_t *)(UID_BASE + 0);
    memcpy(eth_dev->EthHandle->Init.MACAddr, eth_dev->dev_addr, OS_NET_MAC_LENGTH);

    eth_dev->DMATxDscrTab = DMATxDscrTab;
    eth_dev->DMARxDscrTab = DMARxDscrTab;
    eth_dev->rxbuff       = (uint8_t *)Rx_Buff;
#endif

    eth_dev->net_dev.info.MTU       = ETH_MAX_PACKET_SIZE;
    eth_dev->net_dev.info.MRU       = ETH_MAX_PACKET_SIZE;
    eth_dev->net_dev.info.mode      = net_dev_mode_sta;
    eth_dev->net_dev.info.intf_type = net_dev_intf_ether;
    eth_dev->net_dev.ops            = &stm32_net_dev_ops;

    os_net_device_register(&eth_dev->net_dev, dev->name);

    /* napi */
    netif_napi_add(&eth_dev->net_dev, &eth_dev->napi, os_stm32_eth_poll, NAPI_POLL_WEIGHT);

#if OS_TIMER_TASK_STACK_SIZE < 1024
#warning: timer stack too small OS_TIMER_TASK_STACK_SIZE
#endif

    stm32_phy_init(eth_dev);

    eth_dev->timer = os_timer_create(OS_NULL,
                                     dev->name,
                                     stm32_phy_event,
                                     eth_dev,
                                     OS_TICK_PER_SECOND,
                                     OS_TIMER_FLAG_PERIODIC);
    os_timer_start(eth_dev->timer);

    os_list_add(&stm32_eth_list, &eth_dev->list);

    return OS_SUCCESS;
}

OS_DRIVER_INFO stm32_eth_driver = {
    .name  = "ETH_HandleTypeDef",
    .probe = stm32_eth_probe,
};

OS_DRIVER_DEFINE(stm32_eth_driver, OS_INIT_LEVEL_COMPONENT, OS_INIT_SUBLEVEL_LOW);

