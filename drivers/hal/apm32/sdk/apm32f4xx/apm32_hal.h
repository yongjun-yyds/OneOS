#ifndef __APM32_HAL_H_
#define __APM32_HAL_H_

#include "apm32f4xx.h"

#include "apm32f4xx_adc.h"
#include "apm32f4xx_can.h"
#include "apm32f4xx_crc.h"
#include "apm32f4xx_cryp.h"
#include "apm32f4xx_dac.h"
#include "apm32f4xx_dbgmcu.h"
#include "apm32f4xx_dci.h"
#include "apm32f4xx_dma.h"
#include "apm32f4xx_dmc.h"
#include "apm32f4xx_eint.h"
#include "apm32f4xx_fmc.h"
#include "apm32f4xx_gpio.h"
#include "apm32f4xx_hash.h"
#include "apm32f4xx_i2c.h"
#include "apm32f4xx_iwdt.h"
#include "apm32f4xx_misc.h"
#include "apm32f4xx_pmu.h"
#include "apm32f4xx_rcm.h"
#include "apm32f4xx_rng.h"
#include "apm32f4xx_rtc.h"
#include "apm32f4xx_sdio.h"
#include "apm32f4xx_smc.h"
#include "apm32f4xx_spi.h"
#include "apm32f4xx_syscfg.h"
#include "apm32f4xx_tmr.h"
#include "apm32f4xx_usart.h"
#include "apm32f4xx_wwdt.h"
#include "apm32f4xx_eth.h"

#endif

