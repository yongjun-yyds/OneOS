/*!
 * @file       apm32f4xx_pmu.c
 *
 * @brief      This file provides all the PMU firmware functions.
 *
 * @version    V1.0.0
 *
 * @date       2021-09-08
 *
*/

#include "apm32f4xx_pmu.h"
#include "apm32f4xx_rcm.h"

/** @addtogroup Peripherals_Library Standard Peripheral Library
  @{
*/

/** @addtogroup PMU_Driver PMU Driver
  @{
*/

/** @addtogroup PMU_Fuctions Fuctions
  @{
*/

/*!
 * @brief     Reset the PMU peripheral register.
 *
 * @param     None
 *
 * @retval    None
 */
void PMU_Reset(void)
{
    RCM_EnableAPB1PeriphReset(RCM_APB1_PERIPH_PMU);
    RCM_DisableAPB1PeriphReset(RCM_APB1_PERIPH_PMU);
}

/*!
 * @brief     Enables access to the RTC and backup registers.
 *
 * @param     None
 *
 * @retval    None
 */
void PMU_EnableBackupAccess(void)
{
    PMU->CTRL_B.BPWEN = ENABLE;
}

/*!
 * @brief     Disables access to the RTC and backup registers.
 *
 * @param     None
 *
 * @retval    None
 */
void PMU_DisableBackupAccess(void)
{
    PMU->CTRL_B.BPWEN = DISABLE;
}

/*!
 * @brief     Enables the Power Voltage Detector(PVD).
 *
 * @param     None
 *
 * @retval    None
 */
void PMU_EnablePVD(void)
{
    PMU->CTRL_B.PVDEN = ENABLE;
}

/*!
 * @brief     Disables the Power Voltage Detector(PVD).
 *
 * @param     None
 *
 * @retval    None
 */
void PMU_DisablePVD(void)
{
    PMU->CTRL_B.PVDEN = DISABLE;
}

/*!
 * @brief    Configures the voltage threshold detected by the Power Voltage Detector(PVD).
 *
 * @param    level: specifies the PVD detection level
 *           This parameter can be one of the following values:
 *             @arg PMU_PVD_LEVEL_0
 *             @arg PMU_PVD_LEVEL_1
 *             @arg PMU_PVD_LEVEL_2
 *             @arg PMU_PVD_LEVEL_3
 *             @arg PMU_PVD_LEVEL_4
 *             @arg PMU_PVD_LEVEL_5
 *             @arg PMU_PVD_LEVEL_6
 *             @arg PMU_PVD_LEVEL_7
 *
 * @retval   None
 */
void PMU_ConfigPVDLevel(PMU_PVD_LEVEL_T level)
{
    PMU->CTRL_B.PLSEL = 0;
    PMU->CTRL_B.PLSEL = level;
}

/*!
 * @brief     Enables the WakeUp Pin functionality.
 *
 * @param     None
 *
 * @retval    None
 */
void PMU_EnableWakeUpPin(void)
{
    PMU->CSTS_B.WKUPCFG = ENABLE;
}

/*!
 * @brief     Diaables the WakeUp Pin functionality.
 *
 * @param     None
 *
 * @retval    None
 */
void PMU_DisableWakeUpPin(void)
{
    PMU->CSTS_B.WKUPCFG = DISABLE;
}

/*!
 * @brief     Enables the Backup Regulator.
 *
 * @param     None
 *
 * @retval    None
 */
void PMU_EnableBackupRegulator(void)
{
    PMU->CSTS_B.BKPREN = ENABLE;
}

/*!
 * @brief     Diaables the Backup Regulator.
 *
 * @param     None
 *
 * @retval    None
 */
void PMU_DisableBackupRegulator(void)
{
    PMU->CSTS_B.BKPREN = DISABLE;
}

/*!
 * @brief     Configures the main internal regulator output voltage.
 *
 * @param     scale: This parameter can be one of the following values:
 *               @arg PMU_REGULATOR_VOLTAGE_SCALE1
 *               @arg PMU_REGULATOR_VOLTAGE_SCALE2
 *
 * @retval    None
 */
void PMU_ConfigMainRegulatorMode(PMU_REGULATOR_VOLTAGE_SCALE_T scale)
{
    PMU->CTRL_B.VOSSEL = 0;
    PMU->CTRL_B.VOSSEL = scale;
}

/*!
 * @brief     Enables the Flash Power Down in STOP mode.
 *
 * @param     None
 *
 * @retval    None
 */
void PMU_EnableFlashPowerDown(void)
{
    PMU->CTRL_B.FPDSM = ENABLE ;
}

/*!
 * @brief     Diaables the Flash Power Down in STOP mode.
 *
 * @param     None
 *
 * @retval    None
 */
void PMU_DisableFlashPowerDown(void)
{
    PMU->CTRL_B.FPDSM = DISABLE ;
}

/*!
 * @brief     Enters STOP mode.
 *
 * @param     regulator: specifies the regulator state in STOP mode.
 *            This parameter can be one of the following values:
 *              @arg PMU_MAIN_REGULATOR      : STOP mode with regulator ON
 *              @arg PMU_LOWPOWER_REGULATOR  : STOP mode with regulator in low power mode
 *
 * @param     entry: specifies if STOP mode in entered with WFI or WFE instruction.
 *            This parameter can be one of the following values:
 *              @arg PMU_STOP_ENTRY_WFI: Enter STOP mode with WFI instruction
 *              @arg PMU_STOP_ENTRY_WFE: Enter STOP mode with WFE instruction
 *
 * @retval    None
 */
void PMU_EnterSTOPMode(PMU_REGULATOR_T regulator, PMU_STOP_ENTRY_T entry)
{
    /** Clear PDDSCFG and LPDSCFG bits */
    PMU->CTRL_B.PDDSCFG = 0x00;
    PMU->CTRL_B.LPDSCFG = 0x00;
    /** Set LPSM bit according to PWR_Regulator value */
    PMU->CTRL_B.LPDSCFG = regulator;

    /* Set SLEEPDEEP bit of Cortex System Control Register */
    SCB->SCR |= (uint32_t )0x04;

    /** Select STOP mode entry*/
    if (entry == PMU_STOP_ENTRY_WFI)
    {
        /** Request Wait For Interrupt */
        __WFI();
    }
    else
    {
        /** Request Wait For Event */
        __WFE();
    }

    /** Reset SLEEPDEEP bit of Cortex System Control Register */
    SCB->SCR &= (uint32_t)~((uint32_t)0x04);
}

/*!
 * @brief     Enters STANDBY mode.
 *
 * @param     None
 *
 * @retval    None
 */
void PMU_EnterSTANDBYMode(void)
{
    /* Select STANDBY mode */
    PMU->CTRL_B.PDDSCFG = BIT_SET;
    /* Set SLEEPDEEP bit of Cortex System Control Register */
    SCB->SCR |= (uint32_t )0x04;
    #if defined ( __CC_ARM   )
    __force_stores();
    #endif
    /* Request Wait For Interrupt */
    __WFI();

}

/*!
 * @brief     Read the specified PMU flag is set or not.
 *
 * @param     flag: Reads the status of specifies the flag.
 *                  This parameter can be one of the following values:
 *                    @arg PMU_FLAG_WUEFLG: Wake Up flag.
 *                    @arg PMU_FLAG_SBFLG: StandBy flag.
 *                    @arg PMU_FLAG_PVDOFLG: PVD Output.
 *                    @arg PMU_FLAG_BKPRFLG: Backup regulator ready flag.
 *                    @arg PMU_FLAG_VOSRFLG: This flag indicates that the Regulator voltage
 *                         scaling output selection is ready.
 *
 * @retval    The new state of PMU_FLAG (SET or RESET).
 */
uint8_t PMU_ReadStatusFlag(PMU_FLAG_T flag)
{
    uint8_t BitStatus = BIT_RESET;

    if (flag == PMU_FLAG_WUEFLG)
    {
        BitStatus = PMU->CSTS_B.WUEFLG;
    }
    else if (flag == PMU_FLAG_SBFLG)
    {
        BitStatus = PMU->CSTS_B.SBFLG;
    }
    else if (flag == PMU_FLAG_PVDOFLG)
    {
        BitStatus = PMU->CSTS_B.PVDOFLG;
    }
    else if (flag == PMU_FLAG_BKPRFLG)
    {
        BitStatus = PMU->CSTS_B.BKPRFLG;
    }
    else if (flag == PMU_FLAG_VOSRFLG)
    {
        BitStatus = PMU->CSTS_B.VOSRFLG;
    }

    return BitStatus;
}

/*!
 * @brief     Clears the PMU's pending flags.
 *
 * @param     flag: specifies the flag to clear.
 *            This parameter can be one of the following values:
 *              @arg PMU_FLAG_WUEFLG : Wake Up flag.
 *              @arg PMU_FLAG_SBFLG  : StandBy flag.
 *
 * @retval    None
 */
void PMU_ClearStatusFlag(PMU_FLAG_T flag)
{
    if (flag == PMU_FLAG_WUEFLG)
    {
        PMU->CTRL_B.WUFLGCLR = BIT_SET;
    }
    else if (flag == PMU_FLAG_SBFLG)
    {
        PMU->CTRL_B.SBFLGCLR = BIT_SET;
    }
}

/**@} end of group PMU_Fuctions*/
/**@} end of group PMU_Driver*/
/**@} end of group Peripherals_Library*/
