#ifndef __APM32_HAL_H_
#define __APM32_HAL_H_

#include "apm32f10x.h"

#include "apm32f10x_adc.h"
#include "apm32f10x_bakpr.h"
#include "apm32f10x_can.h"
#include "apm32f10x_crc.h"
#include "apm32f10x_dac.h"
#include "apm32f10x_dbgmcu.h"
#include "apm32f10x_dma.h"
#include "apm32f10x_dmc.h"
#include "apm32f10x_eint.h"
#include "apm32f10x_emmc.h"
#include "apm32f10x_fmc.h"
#include "apm32f10x_gpio.h"
#include "apm32f10x_i2c.h"
#include "apm32f10x_iwdt.h"
#include "apm32f10x_misc.h"
#include "apm32f10x_pmu.h"
#include "apm32f10x_qspi.h"
#include "apm32f10x_rcm.h"
#include "apm32f10x_rtc.h"
#include "apm32f10x_sci2c.h"
#include "apm32f10x_sdio.h"
#include "apm32f10x_spi.h"
#include "apm32f10x_tmr.h"
#include "apm32f10x_usart.h"
#include "apm32f10x_usb.h"
#include "apm32f10x_wwdt.h"

#endif

