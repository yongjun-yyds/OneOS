/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_gpio.c
 *
 * @brief       This file implements gpio driver for apm32.
 *
 * @revision
 * Date         Author          Notes
 * 2022-01-17   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <board.h>

#define DRV_EXT_LVL DBG_EXT_INFO
#define DRV_EXT_TAG "drv.gpio"
#include <drv_log.h>

#include "drv_gpio.h"

#define GPIO_ITEM_NUM(items) sizeof(items) / sizeof(items[0])

#ifdef SERIES_APM32F10X
#define GPIO_PORT_INFO_MAP(port)                                                                                       \
    {                                                                                                                  \
        RCM_APB2_PERIPH_GPIO##port, GPIO_PORT_SOURCE_##port                                                            \
    }
#else
#define GPIO_PORT_INFO_MAP(port)                                                                                       \
    {                                                                                                                  \
        RCM_AHB1_PERIPH_GPIO##port, SYSCFG_PORT_GPIO##port                                                             \
    }
#endif

struct gpio_port_info
{
    uint32_t Periph;
#ifdef SERIES_APM32F10X
    GPIO_PORT_SOURCE_T portSource;
#else
    SYSCFG_PORT_T portSource;
#endif
};

struct pin_irq_map
{
    uint16_t          pinbit;
    EINT_LINE_T       line;
    GPIO_PIN_SOURCE_T pinSource;
#ifndef SERIES_APM32F10X
    SYSCFG_PIN_T irq_pinSource;
#endif
    IRQn_Type irqno;
};

struct gpio_info
{
    GPIO_T  *port;
    uint16_t port_index;
    uint16_t pin;
    uint16_t pin_index;
};

struct apm32_pin_info
{
    uint32_t      irq_enable_mask;
    GPIO_Config_T default_cfg;
    uint8_t       irq_PrePri;
    uint8_t       irq_SubPri;

    uint16_t port_info_size;

    void (*PeriphClock)(uint32_t Periph);
};

static const struct gpio_port_info port_info_table[] = {
#ifdef GPIOA
    GPIO_PORT_INFO_MAP(A),
#endif
#ifdef GPIOB
    GPIO_PORT_INFO_MAP(B),
#endif
#ifdef GPIOC
    GPIO_PORT_INFO_MAP(C),
#endif
#ifdef GPIOD
    GPIO_PORT_INFO_MAP(D),
#endif
#ifdef GPIOE
    GPIO_PORT_INFO_MAP(E),
#endif
#ifdef GPIOF
    GPIO_PORT_INFO_MAP(F),
#endif
#ifdef GPIOG
    GPIO_PORT_INFO_MAP(G),
#endif
#ifdef GPIOH
    GPIO_PORT_INFO_MAP(H),
#endif
};

static const struct pin_irq_map pin_irq_map[] = {
#ifdef SERIES_APM32F10X
    {GPIO_PIN_0, EINT_LINE_0, GPIO_PIN_SOURCE_0, EINT0_IRQn},
    {GPIO_PIN_1, EINT_LINE_1, GPIO_PIN_SOURCE_1, EINT1_IRQn},
    {GPIO_PIN_2, EINT_LINE_2, GPIO_PIN_SOURCE_2, EINT2_IRQn},
    {GPIO_PIN_3, EINT_LINE_3, GPIO_PIN_SOURCE_3, EINT3_IRQn},
    {GPIO_PIN_4, EINT_LINE_4, GPIO_PIN_SOURCE_4, EINT4_IRQn},
    {GPIO_PIN_5, EINT_LINE_5, GPIO_PIN_SOURCE_5, EINT9_5_IRQn},
    {GPIO_PIN_6, EINT_LINE_6, GPIO_PIN_SOURCE_6, EINT9_5_IRQn},
    {GPIO_PIN_7, EINT_LINE_7, GPIO_PIN_SOURCE_7, EINT9_5_IRQn},
    {GPIO_PIN_8, EINT_LINE_8, GPIO_PIN_SOURCE_8, EINT9_5_IRQn},
    {GPIO_PIN_9, EINT_LINE_9, GPIO_PIN_SOURCE_9, EINT9_5_IRQn},
    {GPIO_PIN_10, EINT_LINE_10, GPIO_PIN_SOURCE_10, EINT15_10_IRQn},
    {GPIO_PIN_11, EINT_LINE_11, GPIO_PIN_SOURCE_11, EINT15_10_IRQn},
    {GPIO_PIN_12, EINT_LINE_12, GPIO_PIN_SOURCE_12, EINT15_10_IRQn},
    {GPIO_PIN_13, EINT_LINE_13, GPIO_PIN_SOURCE_13, EINT15_10_IRQn},
    {GPIO_PIN_14, EINT_LINE_14, GPIO_PIN_SOURCE_14, EINT15_10_IRQn},
    {GPIO_PIN_15, EINT_LINE_15, GPIO_PIN_SOURCE_15, EINT15_10_IRQn},
#else
    {GPIO_PIN_0, EINT_LINE_0, GPIO_PIN_SOURCE_0, SYSCFG_PIN_0, EINT0_IRQn},
    {GPIO_PIN_1, EINT_LINE_1, GPIO_PIN_SOURCE_1, SYSCFG_PIN_1, EINT1_IRQn},
    {GPIO_PIN_2, EINT_LINE_2, GPIO_PIN_SOURCE_2, SYSCFG_PIN_2, EINT2_IRQn},
    {GPIO_PIN_3, EINT_LINE_3, GPIO_PIN_SOURCE_3, SYSCFG_PIN_3, EINT3_IRQn},
    {GPIO_PIN_4, EINT_LINE_4, GPIO_PIN_SOURCE_4, SYSCFG_PIN_4, EINT4_IRQn},
    {GPIO_PIN_5, EINT_LINE_5, GPIO_PIN_SOURCE_5, SYSCFG_PIN_5, EINT9_5_IRQn},
    {GPIO_PIN_6, EINT_LINE_6, GPIO_PIN_SOURCE_6, SYSCFG_PIN_6, EINT9_5_IRQn},
    {GPIO_PIN_7, EINT_LINE_7, GPIO_PIN_SOURCE_7, SYSCFG_PIN_7, EINT9_5_IRQn},
    {GPIO_PIN_8, EINT_LINE_8, GPIO_PIN_SOURCE_8, SYSCFG_PIN_8, EINT9_5_IRQn},
    {GPIO_PIN_9, EINT_LINE_9, GPIO_PIN_SOURCE_9, SYSCFG_PIN_9, EINT9_5_IRQn},
    {GPIO_PIN_10, EINT_LINE_10, GPIO_PIN_SOURCE_10, SYSCFG_PIN_10, EINT15_10_IRQn},
    {GPIO_PIN_11, EINT_LINE_11, GPIO_PIN_SOURCE_11, SYSCFG_PIN_11, EINT15_10_IRQn},
    {GPIO_PIN_12, EINT_LINE_12, GPIO_PIN_SOURCE_12, SYSCFG_PIN_12, EINT15_10_IRQn},
    {GPIO_PIN_13, EINT_LINE_13, GPIO_PIN_SOURCE_13, SYSCFG_PIN_13, EINT15_10_IRQn},
    {GPIO_PIN_14, EINT_LINE_14, GPIO_PIN_SOURCE_14, SYSCFG_PIN_14, EINT15_10_IRQn},
    {GPIO_PIN_15, EINT_LINE_15, GPIO_PIN_SOURCE_15, SYSCFG_PIN_15, EINT15_10_IRQn},
#endif
};

static struct os_pin_irq_hdr pin_irq_hdr_tab[] = {
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
};

static struct apm32_pin_info pin_info = {
#ifdef SERIES_APM32F10X
    .default_cfg = {0, GPIO_SPEED_50MHz, GPIO_MODE_OUT_PP},
    .PeriphClock = RCM_EnableAPB2PeriphClock,
#else
    .default_cfg = {0, GPIO_MODE_OUT, GPIO_SPEED_50MHz, GPIO_OTYPE_PP, GPIO_PUPD_NOPULL},
    .PeriphClock = RCM_EnableAHB1PeriphClock,
#endif

    .irq_PrePri = 5,
    .irq_SubPri = 0,

};

static os_err_t _get_gpio_info(struct gpio_info *pinfo, os_base_t pin)
{
    pinfo->port_index = (uint16_t)((pin >> 4) & 0x0F);
    pinfo->port       = (GPIO_T *)(GPIOA_BASE + 0x400u * pinfo->port_index);
    pinfo->pin        = (uint16_t)(1 << (pin & 0x0F));
    pinfo->pin_index  = (uint16_t)(pin & 0x0F);

    return OS_SUCCESS;
}

static void _gpio_init(void)
{
    uint8_t i = 0;

    pin_info.port_info_size = GPIO_ITEM_NUM(port_info_table);

    for (i = 0; i < pin_info.port_info_size; i++)
    {
        pin_info.PeriphClock(port_info_table[i].Periph);
    }

#ifdef SERIES_APM32F10X
    RCM_EnableAPB2PeriphClock(RCM_APB2_PERIPH_AFIO);
#endif
}

static void _pin_write(os_device_t *dev, os_base_t pin, os_base_t value)
{
    struct gpio_info info = {0};

    if (_get_gpio_info(&info, pin) == OS_SUCCESS)
    {
        GPIO_WriteBitValue(info.port, info.pin, (uint8_t)value);
    }
}

static int _pin_read(os_device_t *dev, os_base_t pin)
{
    int value = PIN_LOW;

    struct gpio_info info = {0};

    if (_get_gpio_info(&info, pin) == OS_SUCCESS)
    {
        value = (int)GPIO_ReadInputBit(info.port, info.pin);
    }

    return value;
}

static os_err_t _pin_detach_irq(struct os_device *device, int32_t pin);
static void     _pin_mode(os_device_t *dev, os_base_t pin, os_base_t mode)
{
    struct gpio_info info = {0};

    if (_get_gpio_info(&info, pin) != OS_SUCCESS)
    {
        return;
    }
#ifdef SERIES_APM32F10X
    GPIO_Config_T gpioConfig = pin_info.default_cfg;
    gpioConfig.pin           = info.pin;

    if (mode == PIN_MODE_OUTPUT)
    {
        gpioConfig.mode = GPIO_MODE_OUT_PP;
    }
    else if (mode == PIN_MODE_INPUT)
    {
        gpioConfig.mode = GPIO_MODE_IN_FLOATING;
    }
    else if (mode == PIN_MODE_INPUT_PULLUP)
    {
        gpioConfig.mode = GPIO_MODE_IN_PU;
    }
    else if (mode == PIN_MODE_INPUT_PULLDOWN)
    {
        gpioConfig.mode = GPIO_MODE_IN_PD;
    }
    else if (mode == PIN_MODE_OUTPUT_OD)
    {
        gpioConfig.mode = GPIO_MODE_OUT_OD;
    }
    else if (mode == PIN_MODE_DISABLE)
    {
        os_pin_irq_enable(pin, PIN_IRQ_DISABLE);
        _pin_detach_irq(dev, pin);

        gpioConfig.mode = GPIO_MODE_IN_FLOATING;
    }
#else
    GPIO_Config_T gpioConfig = pin_info.default_cfg;
    gpioConfig.pin = info.pin;

    if (mode == PIN_MODE_OUTPUT)
    {
        gpioConfig.mode = GPIO_MODE_OUT;
        gpioConfig.otype = GPIO_OTYPE_PP;
        gpioConfig.pupd = GPIO_PUPD_NOPULL;
    }
    else if (mode == PIN_MODE_INPUT)
    {
        gpioConfig.mode = GPIO_MODE_IN;
        gpioConfig.pupd = GPIO_PUPD_NOPULL;
    }
    else if (mode == PIN_MODE_INPUT_PULLUP)
    {
        gpioConfig.mode = GPIO_MODE_IN;
        gpioConfig.pupd = GPIO_PUPD_UP;
    }
    else if (mode == PIN_MODE_INPUT_PULLDOWN)
    {
        gpioConfig.mode = GPIO_MODE_IN;
        gpioConfig.pupd = GPIO_PUPD_DOWN;
    }
    else if (mode == PIN_MODE_OUTPUT_OD)
    {
        gpioConfig.mode = GPIO_MODE_OUT;
        gpioConfig.otype = GPIO_OTYPE_OD;
        gpioConfig.pupd = GPIO_PUPD_UP;
    }
    else if (mode == PIN_MODE_DISABLE)
    {
        os_pin_irq_enable(pin, PIN_IRQ_DISABLE);
        _pin_detach_irq(dev, pin);

        gpioConfig.mode = GPIO_MODE_IN;
        gpioConfig.pupd = GPIO_PUPD_NOPULL;
    }
#endif

    GPIO_Config(info.port, &gpioConfig);
}

/* clang-format off */
static os_err_t _pin_attach_irq(struct os_device *device, int32_t pin, uint32_t mode, void (*hdr)(void *args), void *args)
{
    os_ubase_t level;

    struct gpio_info info = {0};

    if (_get_gpio_info(&info, pin) != OS_SUCCESS)
    {
        return OS_FAILURE;
    }

    os_spin_lock_irqsave(&gs_device_lock, &level);
    if (pin_irq_hdr_tab[info.pin_index].pin == pin && pin_irq_hdr_tab[info.pin_index].hdr == hdr &&
        pin_irq_hdr_tab[info.pin_index].mode == mode && pin_irq_hdr_tab[info.pin_index].args == args)
    {
        os_spin_unlock_irqrestore(&gs_device_lock, level);
        return OS_SUCCESS;
    }
    if (pin_irq_hdr_tab[info.pin_index].pin != -1)
    {
        os_spin_unlock_irqrestore(&gs_device_lock, level);
        return OS_BUSY;
    }
    pin_irq_hdr_tab[info.pin_index].pin  = pin;
    pin_irq_hdr_tab[info.pin_index].hdr  = hdr;
    pin_irq_hdr_tab[info.pin_index].mode = mode;
    pin_irq_hdr_tab[info.pin_index].args = args;
    os_spin_unlock_irqrestore(&gs_device_lock, level);

    return OS_SUCCESS;
}
/* clang-format on */

static os_err_t _pin_detach_irq(struct os_device *device, int32_t pin)
{
    os_ubase_t level;

    struct gpio_info info = {0};

    if (_get_gpio_info(&info, pin) != OS_SUCCESS)
    {
        return OS_FAILURE;
    }

    os_spin_lock_irqsave(&gs_device_lock, &level);
    if (pin_irq_hdr_tab[info.pin_index].pin == -1)
    {
        os_spin_unlock_irqrestore(&gs_device_lock, level);
        return OS_SUCCESS;
    }
    pin_irq_hdr_tab[info.pin_index].pin  = -1;
    pin_irq_hdr_tab[info.pin_index].hdr  = OS_NULL;
    pin_irq_hdr_tab[info.pin_index].mode = 0;
    pin_irq_hdr_tab[info.pin_index].args = OS_NULL;
    os_spin_unlock_irqrestore(&gs_device_lock, level);

    return OS_SUCCESS;
}

static os_err_t _pin_irq_enable(struct os_device *device, os_base_t pin, uint32_t enabled)
{
    os_ubase_t                 level;
    const struct pin_irq_map *irqmap = OS_NULL;

    EINT_Config_T EINT_configStruct;

    struct gpio_info info = {0};

    if (_get_gpio_info(&info, pin) != OS_SUCCESS)
    {
        return OS_FAILURE;
    }

    irqmap = &pin_irq_map[info.pin_index];
    os_spin_lock_irqsave(&gs_device_lock, &level);

    if (enabled == PIN_IRQ_ENABLE)
    {
        if (pin_irq_hdr_tab[info.pin_index].pin == -1)
        {
            os_spin_unlock_irqrestore(&gs_device_lock, level);
            return OS_NOSYS;
        }

        EINT_configStruct.line    = irqmap->line;
        EINT_configStruct.mode    = EINT_MODE_INTERRUPT;
        EINT_configStruct.lineCmd = DISABLE;
        EINT_Config(&EINT_configStruct);
        EINT_ClearIntFlag(irqmap->line);

#ifdef SERIES_APM32F10X
        GPIO_ConfigEINTLine(port_info_table[info.port_index].portSource, irqmap->pinSource);
#else
        RCM_EnableAPB2PeriphClock(RCM_APB2_PERIPH_SYSCFG);
        SYSCFG_ConfigEINTLine(port_info_table[info.port_index].portSource, irqmap->irq_pinSource);
#endif
        switch (pin_irq_hdr_tab[info.pin_index].mode)
        {
        case PIN_IRQ_MODE_RISING:
            EINT_configStruct.trigger = EINT_TRIGGER_RISING;
            break;
        case PIN_IRQ_MODE_FALLING:
            EINT_configStruct.trigger = EINT_TRIGGER_FALLING;
            break;
        case PIN_IRQ_MODE_RISING_FALLING:
            EINT_configStruct.trigger = EINT_TRIGGER_RISING_FALLING;
            break;
        default:
            LOG_E(DRV_EXT_TAG, "not support pin mode!");
            return OS_FAILURE;
        }

        EINT_configStruct.line    = irqmap->line;
        EINT_configStruct.mode    = EINT_MODE_INTERRUPT;
        EINT_configStruct.lineCmd = ENABLE;
        EINT_Config(&EINT_configStruct);

        NVIC_EnableIRQRequest(irqmap->irqno, pin_info.irq_PrePri, pin_info.irq_SubPri);

        pin_info.irq_enable_mask |= irqmap->pinbit;
    }
    else if (enabled == PIN_IRQ_DISABLE)
    {
        pin_info.irq_enable_mask &= ~irqmap->pinbit;

        if ((irqmap->pinbit >= GPIO_PIN_5) && (irqmap->pinbit <= GPIO_PIN_9))
        {
            if (!(pin_info.irq_enable_mask & (GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7 | GPIO_PIN_8 | GPIO_PIN_9)))
            {
                NVIC_DisableIRQRequest(irqmap->irqno);
            }
        }
        else if ((irqmap->pinbit >= GPIO_PIN_10) && (irqmap->pinbit <= GPIO_PIN_15))
        {
            if (!(pin_info.irq_enable_mask &
                  (GPIO_PIN_10 | GPIO_PIN_11 | GPIO_PIN_12 | GPIO_PIN_13 | GPIO_PIN_14 | GPIO_PIN_15)))
            {
                NVIC_DisableIRQRequest(irqmap->irqno);
            }
        }
        else
        {
            NVIC_DisableIRQRequest(irqmap->irqno);
        }
    }

    os_spin_unlock_irqrestore(&gs_device_lock, level);

    return OS_SUCCESS;
}

void GPIO_EXTI_IRQHandler(EINT_LINE_T line, uint16_t index)
{
    if (EINT_ReadIntFlag(line))
    {
        EINT_ClearIntFlag(line);

        if (pin_irq_hdr_tab[index].hdr)
        {
            pin_irq_hdr_tab[index].hdr(pin_irq_hdr_tab[index].args);
        }
    }
}

const static struct os_pin_ops apm32_pin_ops = {
    .pin_mode       = _pin_mode,
    .pin_write      = _pin_write,
    .pin_read       = _pin_read,
    .pin_attach_irq = _pin_attach_irq,
    .pin_detach_irq = _pin_detach_irq,
    .pin_irq_enable = _pin_irq_enable,
};

int os_hw_pin_init(void)
{
    _gpio_init();

    return os_device_pin_register(0, &apm32_pin_ops, OS_NULL);
}
