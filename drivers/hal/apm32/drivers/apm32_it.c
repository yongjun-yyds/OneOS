/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        apm32_it.c
 *
 * @brief       This file provides systick time init/IRQ functions.
 *
 * @revision
 * Date         Author          Notes
 * 2022-01-10   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include "os_clock.h"
#include "os_stddef.h"
#include "oneos_config.h"

#ifdef OS_USING_CLOCKSOURCE
#include <timer/clocksource.h>
#include <timer/clocksource_cortexm.h>
#endif

#define DRV_EXT_LVL DBG_EXT_INFO
#define DRV_EXT_TAG "drv.irq"
#include <drv_log.h>

#include "apm32_it.h"
#include "drv_gpio.h"
#include "drv_usart.h"

void SysTick_Handler(void)
{
    os_tick_increase();

#ifdef OS_USING_CLOCKSOURCE
    os_clocksource_update();
#endif
}

OS_WEAK void GPIO_EXTI_IRQHandler(EINT_LINE_T line, uint16_t index)
{
}

#define EXIT_IRQ_HANDLER(index) GPIO_EXTI_IRQHandler(EINT_LINE_##index, index)

void EINT0_IRQHandler(void)
{
    EXIT_IRQ_HANDLER(0);
}
void EINT1_IRQHandler(void)
{
    EXIT_IRQ_HANDLER(1);
}
void EINT2_IRQHandler(void)
{
    EXIT_IRQ_HANDLER(2);
}
void EINT3_IRQHandler(void)
{
    EXIT_IRQ_HANDLER(3);
}
void EINT4_IRQHandler(void)
{
    EXIT_IRQ_HANDLER(4);
}
void EINT9_5_IRQHandler(void)
{
    EXIT_IRQ_HANDLER(5);
    EXIT_IRQ_HANDLER(6);
    EXIT_IRQ_HANDLER(7);
    EXIT_IRQ_HANDLER(8);
    EXIT_IRQ_HANDLER(9);
}
void EINT15_10_IRQHandler(void)
{
    EXIT_IRQ_HANDLER(10);
    EXIT_IRQ_HANDLER(11);
    EXIT_IRQ_HANDLER(12);
    EXIT_IRQ_HANDLER(13);
    EXIT_IRQ_HANDLER(14);
    EXIT_IRQ_HANDLER(15);
}

OS_WEAK void USART_IRQHandler(USART_T *husart, uint16_t index)
{
}

#define USART_IRQ_HANDLER(index) USART_IRQHandler(USART##index, index)

void USART1_IRQHandler(void)
{
    USART_IRQ_HANDLER(1);
}
void USART2_IRQHandler(void)
{
    USART_IRQ_HANDLER(2);
}
void USART3_IRQHandler(void)
{
    USART_IRQ_HANDLER(3);
}
