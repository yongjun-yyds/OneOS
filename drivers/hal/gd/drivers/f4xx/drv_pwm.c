/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_pwm.c
 *
 * @brief       This file implements PWM driver for stm32.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <board.h>
#include <drv_hwtimer.h>
#include <string.h>
#include <os_memory.h>
#include "drv_pwm.h"

#define DRV_TAG "drv.pwm"
#include <drv_log.h>

static os_err_t get_pwm_channel(uint16_t channel, uint32_t *gd32_channel)
{
    switch (channel)
    {
    case 1:
        *gd32_channel = TIMER_CH_0;
        break;
    case 2:
        *gd32_channel = TIMER_CH_1;
        break;
    case 3:
        *gd32_channel = TIMER_CH_2;
        break;
    case 4:
        *gd32_channel = TIMER_CH_3;
        break;
    default:
        return OS_NOSYS;
    }
    return OS_SUCCESS;
}

static os_err_t gd32_pwm_enabled(struct os_pwm_device *dev, uint32_t channel, os_bool_t enable)
{

    struct gd32_pwm_info *pwm_info;
    struct gd32_pwm      *gd_pwm;
    uint32_t           gd_timer_channel = 0;
    uint32_t           timer_periph;

    if (get_pwm_channel(channel, &gd_timer_channel) != OS_SUCCESS)
    {
        LOG_E(DRV_TAG, "pwm channel %d is illegal!\r\n", channel);
        return OS_NOSYS;
    }

    gd_pwm       = os_container_of(dev, struct gd32_pwm, pwm);
    pwm_info     = &gd_pwm->tim->info->pwm_info;
    timer_periph = gd_pwm->tim->info->timer_periph;
    if (enable)
    {
        timer_channel_output_config(timer_periph, gd_timer_channel, &pwm_info->timer_ocintpara);
        timer_channel_output_mode_config(timer_periph, gd_timer_channel, pwm_info->ocmode);
        timer_channel_output_shadow_config(timer_periph, gd_timer_channel, TIMER_OC_SHADOW_DISABLE);
        timer_auto_reload_shadow_enable(timer_periph);

        timer_enable(timer_periph);
    }
    else
    {
        timer_disable(timer_periph);
    }
    return OS_SUCCESS;
}

static os_err_t gd32_pwm_set_period(struct os_pwm_device *dev, uint32_t channel, uint32_t nsec)
{
    uint64_t os_tim_tick = 0;
    uint64_t prescaler   = 0;
    uint64_t Period      = 0;

    struct gd32_pwm        *gd_pwm;
    timer_parameter_struct *timer_para;

    gd_pwm     = os_container_of(dev, struct gd32_pwm, pwm);
    timer_para = &gd_pwm->tim->info->timer_initpara;

    os_tim_tick = (uint64_t)nsec * gd_pwm->tim_mult >> gd_pwm->tim_shift;
    prescaler   = os_tim_tick / gd_pwm->pwm.max_value;
    Period      = os_tim_tick / (prescaler + 1) - 1;

    if ((timer_para->prescaler != prescaler) || (timer_para->period != Period))
    {
        timer_para->prescaler = prescaler;
        timer_para->period    = os_tim_tick / (timer_para->prescaler + 1) - 1;
        gd_pwm->freq          = gd_pwm->tim->freq / (timer_para->prescaler + 1);
        // os_kprintf("pwm:prescaler:%d,period:%d\r\n",timer_para->prescaler,timer_para->period);

        timer_init(gd_pwm->tim->info->timer_periph, timer_para);
    }

    return OS_SUCCESS;
}

static os_err_t gd32_pwm_set_pulse(struct os_pwm_device *dev, uint32_t channel, uint32_t buffer)
{
    uint32_t      pwm_pulse        = 0;
    uint32_t      gd_timer_channel = 0;
    struct gd32_pwm *gd_pwm;

    if (get_pwm_channel(channel, &gd_timer_channel) != OS_SUCCESS)
    {
        LOG_E(DRV_TAG, "pwm channel %d is illegal!\r\n", channel);
        return OS_NOSYS;
    }

    gd_pwm = os_container_of(dev, struct gd32_pwm, pwm);

    if (buffer > dev->period)
    {
        LOG_E(DRV_TAG, "pwm pulse value over range!\r\n");
        return OS_FAILURE;
    }
    else
    {
        pwm_pulse = ((uint64_t)buffer * gd_pwm->tim_mult >> gd_pwm->tim_shift) /
                    (gd_pwm->tim->info->timer_initpara.prescaler + 1);
    }

    if (pwm_pulse > dev->max_value)
    {
        LOG_E(DRV_TAG, "pwm pulse value over range!");
        return OS_FULL;
    }
    // os_kprintf("pwm_pulse:%d\r\n",pwm_pulse);

    timer_channel_output_pulse_value_config(gd_pwm->tim->info->timer_periph, gd_timer_channel, pwm_pulse);

    return OS_SUCCESS;
}

static const struct os_pwm_ops gd32_pwm_ops = {
    .enabled    = gd32_pwm_enabled,
    .set_period = gd32_pwm_set_period,
    .set_pulse  = gd32_pwm_set_pulse,
    .control    = OS_NULL,
};

static uint8_t os_hw_pwm_init(struct gd32_pwm_info *pwm_info)
{
    struct gd_pwm_pin_info *pin_info;

    pin_info = &pwm_info->pin_info;

    /*pwm gpio init*/
    rcu_periph_clock_enable(pin_info->periph);
    gpio_mode_set(pin_info->pwm_port, pin_info->mode, GPIO_PUPD_NONE, pin_info->pwm_pin);
    gpio_output_options_set(pin_info->pwm_port, GPIO_OTYPE_PP, pin_info->speed, pin_info->pwm_pin);
    gpio_af_set(pin_info->pwm_port, pin_info->alt_func_num, pin_info->pwm_pin);

    return OS_SUCCESS;
}

os_err_t gd32_pwm_register(const char *name, struct gd32_timer *tim)
{
    char *pwm_name = os_calloc(1, sizeof(name) + 14);

    strcpy(pwm_name, "pwm_");

    pwm_name = strcat(pwm_name, name);

    struct gd32_pwm *gd_pwm = os_calloc(1, sizeof(struct gd32_pwm));

    OS_ASSERT(gd32_pwm);

    gd_pwm->tim = tim;

    gd_pwm->freq = gd_pwm->tim->freq / (gd_pwm->tim->info->timer_initpara.prescaler + 1);

    if (gd32_timer_is_32b(gd_pwm->tim->info->timer_periph))
    {
        gd_pwm->pwm.max_value = 0xFFFFFFFF;
    }
    else
    {
        gd_pwm->pwm.max_value = 0xFFFF;
    }

    calc_mult_shift(&gd_pwm->tim_mult,
                    &gd_pwm->tim_shift,
                    NSEC_PER_SEC,
                    gd_pwm->tim->freq,
                    gd_pwm->pwm.max_value / gd_pwm->tim->freq);

    gd_pwm->pwm.ops = &gd32_pwm_ops;

    os_hw_pwm_init(&tim->info->pwm_info);

    os_device_pwm_register(&gd_pwm->pwm, pwm_name);
    return OS_SUCCESS;
}
