/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_qspi.c
 *
 * @brief       This file implements QSPI driver for stm32.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <os_memory.h>
#include <string.h>
#include "board.h"
#include "drv_qspi.h"
#include <bus/bus.h>
#include <sfbus.h>
#include <drv_qspi.h>

#define DRV_EXT_TAG "drv.qspi"
#include <drv_log.h>

#define DUMMY_BYTE 0xA5

struct gd32_qspi
{
    struct os_sfbus sfbus;

    struct gd32_qspi_info *qspi_info;

    os_list_node_t list;
};

static os_list_node_t gd32_qspi_list = OS_LIST_INIT(gd32_qspi_list);

static uint32_t spi_send_buf(struct gd32_qspi *qspi_dev, uint8_t *buf, uint32_t len)
{
    uint32_t count = 0;

    uint32_t qspi_periph = qspi_dev->qspi_info->hqspi;

    for (count = 0; count < len; count++)
    {
        /* clang-format off */
        /* loop while data register in not emplty */
        while (RESET == spi_i2s_flag_get(qspi_periph, SPI_FLAG_TBE));
        /* clang-format on */

        if (qspi_dev->qspi_info->qspi_init_struct.frame_size == SPI_FRAMESIZE_16BIT)
        {
            /* send byte through the SPI peripheral */
            spi_i2s_data_transmit(qspi_periph, *(uint16_t *)(buf + count * sizeof(uint16_t)));
        }
        else
        {
            /* send byte through the SPI peripheral */
            spi_i2s_data_transmit(qspi_periph, *(uint8_t *)(buf + count * sizeof(uint8_t)));
        }

        /* clang-format off */
        /* wait to receive a byte */
        while (RESET == spi_i2s_flag_get(qspi_periph, SPI_FLAG_RBNE));
        /* clang-format on */

        spi_i2s_data_receive(qspi_periph);
    }
    /* return the byte read from the SPI bus */
    return OS_SUCCESS;
}

static uint32_t spi_receive_buf(struct gd32_qspi *qspi_dev, uint8_t *buf, uint32_t len)
{
    uint32_t count = 0;

    uint32_t qspi_periph = qspi_dev->qspi_info->hqspi;

    for (count = 0; count < len; count++)
    {
        /* clang-format off */
        /* loop while data register in not emplty */
        while (RESET == spi_i2s_flag_get(qspi_periph, SPI_FLAG_TBE));
        /* clang-format on */

        /* send byte through the SPI peripheral */
        spi_i2s_data_transmit(qspi_periph, DUMMY_BYTE);

        /* clang-format off */
        /* wait to receive a byte */
        while (RESET == spi_i2s_flag_get(qspi_periph, SPI_FLAG_RBNE));
        /* clang-format on */

        if (qspi_dev->qspi_info->qspi_init_struct.frame_size == SPI_FRAMESIZE_16BIT)
        {
            *(uint16_t *)(buf + count * sizeof(uint16_t)) = (uint16_t)spi_i2s_data_receive(qspi_periph);
        }
        else
        {
            *(uint8_t *)(buf + count * sizeof(uint8_t)) = (uint8_t)spi_i2s_data_receive(qspi_periph);
        }
    }
    /* return the byte read from the SPI bus */
    return OS_SUCCESS;
}

static uint32_t qspixfer(struct gd32_qspi *qspi_dev, struct os_spi_message *message)
{
    os_size_t         message_length, already_send_length;
    uint16_t       send_length;
    uint8_t       *recv_buf;
    const uint8_t *send_buf;
    os_err_t          state;

    OS_ASSERT(qspi_dev != OS_NULL);
    OS_ASSERT(message != OS_NULL);

    message_length = message->length;
    recv_buf       = message->recv_buf;
    send_buf       = message->send_buf;
    while (message_length)
    {
        /* the HAL library use uint16 to save the data length */
        if (message_length > 65535)
        {
            send_length    = 65535;
            message_length = message_length - 65535;
        }
        else
        {
            send_length    = message_length;
            message_length = 0;
        }

        /* calculate the start address */
        already_send_length = message->length - send_length - message_length;
        send_buf            = (uint8_t *)message->send_buf + already_send_length;
        recv_buf            = (uint8_t *)message->recv_buf + already_send_length;

        if (message->send_buf)
        {
            state = spi_send_buf(qspi_dev, (uint8_t *)send_buf, send_length);
        }
        else
        {
            memset((uint8_t *)recv_buf, 0xff, send_length);
            state = spi_receive_buf(qspi_dev, (uint8_t *)recv_buf, send_length);
        }

        if (state != OS_SUCCESS)
        {
            LOG_I(DRV_EXT_TAG, "spi transfer error : %d", state);
            message->length = 0;
        }
        else
        {
            LOG_D(DRV_EXT_TAG, "transfer done");
        }
    }

    return message->length;
}

static struct os_spi_message *qspi_transfer_message(struct gd32_qspi *qspi_dev, struct os_spi_message *message)
{
    os_err_t               result;
    struct os_spi_message *index;

    OS_ASSERT(qspi_dev != OS_NULL);

    /* Get first message */
    index = message;
    if (index == OS_NULL)
        return index;

    /* Transmit each SPI message */
    while (index != OS_NULL)
    {
        /* Transmit SPI message */
        result = qspixfer(qspi_dev, index);
        if (result == 0)
        {
            os_set_errno(OS_EIO);
            break;
        }

        index = index->next;
    }

    return index;
}

static int gd32_qspi_transfer(struct os_sfbus *sfbus, struct os_xspi_message *xmessage)
{
    struct os_spi_message *msg_list = os_calloc(5, sizeof(struct os_spi_message));
    struct os_spi_message *msg_head = msg_list;
    struct os_spi_message *msg_tail = msg_list;
    struct gd32_qspi      *qspi_dev = os_container_of(sfbus, struct gd32_qspi, sfbus);
    int                    i;

    uint8_t *dummy = OS_NULL;
    uint8_t  addrs[5];

    if (msg_list == OS_NULL)
    {
        os_kprintf("gd32_qspi_transfer msg list no memory.\r\n");
        return OS_NOMEM;
    }

    /* prepare 5 step xspi message */
    if (xmessage_instruction_lines(xmessage) != 0)
    {
        OS_ASSERT(xmessage_instruction_lines(xmessage) == 1);

        msg_head->length   = 1;
        msg_head->send_buf = &xmessage_instruction(xmessage);
        msg_head->recv_buf = OS_NULL;
        msg_head++;
    }

    if (xmessage_address_lines(xmessage) != 0)
    {
        OS_ASSERT(xmessage_address_lines(xmessage) == 1);
        OS_ASSERT(xmessage_address_size(xmessage) == 3 || xmessage_address_size(xmessage) == 4);

        msg_head->length = xmessage_address_size(xmessage);

        /* swap addr bytes(msb) */
        for (i = 0; i < xmessage_address_size(xmessage); i++)
            addrs[i] = xmessage_address(xmessage) >> ((xmessage_address_size(xmessage) - 1 - i) * 8);

        msg_head->send_buf = addrs;
        msg_head->recv_buf = OS_NULL;
        msg_head++;
    }

    if (xmessage_alternate_lines(xmessage) != 0)
    {
        OS_ASSERT(xmessage_alternate_lines(xmessage) == 1);

        msg_head->length   = xmessage_alternate_size(xmessage);
        msg_head->send_buf = &xmessage_alternate(xmessage);
        msg_head->recv_buf = OS_NULL;
        msg_head++;
    }

    if (xmessage_dummy_cycles(xmessage) != 0)
    {
        OS_ASSERT(xmessage_dummy_cycles(xmessage) % 8 == 0);

        dummy = os_calloc(1, xmessage_dummy_cycles(xmessage) / 8);

        if (dummy == OS_NULL)
        {
            os_free(msg_list);
            os_kprintf("gd32_qspi_transfer dummy no memory.\r\n");
            return OS_NOMEM;
        }

        msg_head->length   = xmessage_dummy_cycles(xmessage) / 8;
        msg_head->send_buf = dummy;
        msg_head->recv_buf = OS_NULL;
        msg_head++;
    }

    if (xmessage_data_lines(xmessage) != 0)
    {
        OS_ASSERT(xmessage_data_lines(xmessage) == 1);

        msg_head->length = xmessage_data_size(xmessage);

        if (xmessage_data_dir(xmessage) == OS_XSPI_DATA_DIR_HOST_TO_DEVICE)
        {
            msg_head->send_buf = xmessage_data(xmessage);
        }
        else if (xmessage_data_dir(xmessage) == OS_XSPI_DATA_DIR_DEVICE_TO_HOST)
        {
            msg_head->recv_buf = xmessage_data(xmessage);
        }
        else
        {
            OS_ASSERT(OS_FALSE);
        }

        msg_head++;
    }

    /* transfer */
    msg_tail->cs_take = OS_TRUE;

    while (msg_tail + 1 != msg_head)
    {
        msg_tail->next = msg_tail + 1;
        msg_tail++;
    }

    msg_tail->cs_release = OS_TRUE;

    qspi_transfer_message(qspi_dev, msg_list);

    if (dummy != OS_NULL)
    {
        os_free(dummy);
    }

    os_free(msg_list);

    return OS_SUCCESS;
}

static int gd32_qspi_configure(struct os_sfbus *sfbus, struct os_spi_configuration *cfg)
{
    uint32_t            qspi_periph;
    spi_parameter_struct   qspi_init_struct;
    struct gd32_qspi_info *qspi_info;

    struct gd32_qspi *qspi = os_container_of(sfbus, struct gd32_qspi, sfbus);

    qspi_info = qspi->qspi_info;

    qspi_init_struct = qspi_info->qspi_init_struct;

    qspi_periph = (uint32_t)qspi_info->hqspi;

    if (cfg->data_width <= 8)
    {
        qspi_init_struct.frame_size = SPI_FRAMESIZE_8BIT;
    }
    else if (cfg->data_width <= 16)
    {
        qspi_init_struct.frame_size = SPI_FRAMESIZE_16BIT;
    }
    else
    {
        return OS_EIO;
    }

    {
        rcu_clock_freq_enum spi_src;
        uint32_t         spi_apb_clock;
        uint32_t         max_hz;

        max_hz = cfg->max_hz;

        LOG_D(DRV_EXT_TAG, "sys   freq: %d\n", rcu_clock_freq_get(CK_SYS));
        LOG_D(DRV_EXT_TAG, "CK_APB2 freq: %d\n", rcu_clock_freq_get(CK_APB2));
        LOG_D(DRV_EXT_TAG, "max   freq: %d\n", max_hz);

        if (qspi_periph == SPI1 || qspi_periph == SPI2)
        {
            spi_src = CK_APB1;
        }
        else
        {
            spi_src = CK_APB2;
        }

        spi_apb_clock = rcu_clock_freq_get(spi_src);

        if (max_hz >= spi_apb_clock / 2)
        {
            qspi_init_struct.prescale = SPI_PSC_2;
        }
        else if (max_hz >= spi_apb_clock / 4)
        {
            qspi_init_struct.prescale = SPI_PSC_4;
        }
        else if (max_hz >= spi_apb_clock / 8)
        {
            qspi_init_struct.prescale = SPI_PSC_8;
        }
        else if (max_hz >= spi_apb_clock / 16)
        {
            qspi_init_struct.prescale = SPI_PSC_16;
        }
        else if (max_hz >= spi_apb_clock / 32)
        {
            qspi_init_struct.prescale = SPI_PSC_32;
        }
        else if (max_hz >= spi_apb_clock / 64)
        {
            qspi_init_struct.prescale = SPI_PSC_64;
        }
        else if (max_hz >= spi_apb_clock / 128)
        {
            qspi_init_struct.prescale = SPI_PSC_128;
        }
        else
        {
            /*  min prescaler 256 */
            qspi_init_struct.prescale = SPI_PSC_256;
        }
    } /* baudrate */

    switch (cfg->mode & OS_SPI_MODE_3)
    {
    case OS_SPI_MODE_0:
        qspi_init_struct.clock_polarity_phase = SPI_CK_PL_LOW_PH_1EDGE;
        break;
    case OS_SPI_MODE_1:
        qspi_init_struct.clock_polarity_phase = SPI_CK_PL_LOW_PH_2EDGE;
        break;
    case OS_SPI_MODE_2:
        qspi_init_struct.clock_polarity_phase = SPI_CK_PL_HIGH_PH_1EDGE;
        break;
    case OS_SPI_MODE_3:
        qspi_init_struct.clock_polarity_phase = SPI_CK_PL_HIGH_PH_2EDGE;
        break;
    }

    /* MSB or LSB */
    if (cfg->mode & OS_SPI_MSB)
    {
        qspi_init_struct.endian = SPI_ENDIAN_MSB;
    }
    else
    {
        qspi_init_struct.endian = SPI_ENDIAN_LSB;
    }

    qspi_info->hw_init(&qspi_init_struct);

    return OS_SUCCESS;
}

static const struct os_sfbus_ops gd32_qspi_ops = {
    .configure = gd32_qspi_configure,
    .transfer  = gd32_qspi_transfer,
};

static int gd32_qspi_bus_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    struct gd32_qspi *qspi = os_calloc(1, sizeof(struct gd32_qspi));

    OS_ASSERT(qspi != OS_NULL);

    qspi->qspi_info = (struct gd32_qspi_info *)dev->info;

    qspi->sfbus.support_1_line = OS_TRUE;

    return os_sfbus_xspi_register(&qspi->sfbus, dev->name, &gd32_qspi_ops);
}

OS_DRIVER_INFO gd32_qspi_driver = {
    .name  = "QSPI_HandleTypeDef",
    .probe = gd32_qspi_bus_probe,
};

OS_DRIVER_DEFINE(gd32_qspi_driver, OS_INIT_LEVEL_PRE_DEVICE, OS_INIT_SUBLEVEL_LOW);

