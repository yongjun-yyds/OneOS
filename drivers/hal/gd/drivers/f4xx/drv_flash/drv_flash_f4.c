/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_flash_f4.c
 *
 * @brief        This file provides flash read/write/erase functions for gd32f4xx.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include "board.h"
#include "drv_flash.h"
#include <drv_log.h>

#define LOG_TAG "drv.flash"

/* sector size */
#define SIZE_16KB  ((uint32_t)0x00004000U) /*!< size of 16KB*/
#define SIZE_64KB  ((uint32_t)0x00010000U) /*!< size of 64KB*/
#define SIZE_128KB ((uint32_t)0x00020000U) /*!< size of 128KB*/
#define SIZE_256KB ((uint32_t)0x00040000U) /*!< size of 256KB*/

/* FMC BANK address */
#define FMC_START_ADDRESS       FLASH_BASE                           /*!< FMC start address */
#define FMC_BANK0_START_ADDRESS FMC_START_ADDRESS                    /*!< FMC BANK0 start address */
#define FMC_BANK1_START_ADDRESS ((uint32_t)0x08100000U)              /*!< FMC BANK1 start address */
#define FMC_SIZE                (*(uint16_t *)0x1FFF7A22U)           /*!< FMC SIZE */
#define FMC_END_ADDRESS         (FLASH_BASE + (FMC_SIZE * 1024) - 1) /*!< FMC end address */
#define FMC_MAX_END_ADDRESS     ((uint32_t)0x08300000U)              /*!< FMC maximum end address */

/* FMC error message */
#define FMC_WRONG_SECTOR_NAME ((uint32_t)0xFFFFFFFFU) /*!< wrong sector name*/
#define FMC_WRONG_SECTOR_NUM  ((uint32_t)0xFFFFFFFFU) /*!< wrong sector number*/
#define FMC_INVALID_SIZE      ((uint32_t)0xFFFFFFFFU) /*!< invalid sector size*/
#define FMC_INVALID_ADDR      ((uint32_t)0xFFFFFFFFU) /*!< invalid sector address*/

/* FMC sector information */
typedef struct
{
    uint32_t sector_name;       /*!< the name of the sector */
    uint32_t sector_num;        /*!< the number of the sector */
    uint32_t sector_size;       /*!< the size of the sector */
    uint32_t sector_start_addr; /*!< the start address of the sector */
    uint32_t sector_end_addr;   /*!< the end address of the sector */
} fmc_sector_info_struct;

fmc_sector_info_struct fmc_sector_info_get(uint32_t addr)
{
    fmc_sector_info_struct sector_info;
    uint32_t               temp = 0x00000000U;
    if ((FMC_START_ADDRESS <= addr) && (FMC_END_ADDRESS >= addr))
    {
        if ((FMC_BANK1_START_ADDRESS > addr))
        {
            /* bank0 area */
            temp = (addr - FMC_BANK0_START_ADDRESS) / SIZE_16KB;
            if (4U > temp)
            {
                sector_info.sector_name       = (uint32_t)temp;
                sector_info.sector_num        = CTL_SN(temp);
                sector_info.sector_size       = SIZE_16KB;
                sector_info.sector_start_addr = FMC_BANK0_START_ADDRESS + (SIZE_16KB * temp);
                sector_info.sector_end_addr   = sector_info.sector_start_addr + SIZE_16KB - 1;
            }
            else if (8U > temp)
            {
                sector_info.sector_name       = 0x00000004U;
                sector_info.sector_num        = CTL_SN(4);
                sector_info.sector_size       = SIZE_64KB;
                sector_info.sector_start_addr = 0x08010000U;
                sector_info.sector_end_addr   = 0x0801FFFFU;
            }
            else
            {
                temp                          = (addr - FMC_BANK0_START_ADDRESS) / SIZE_128KB;
                sector_info.sector_name       = (uint32_t)(temp + 4);
                sector_info.sector_num        = CTL_SN(temp + 4);
                sector_info.sector_size       = SIZE_128KB;
                sector_info.sector_start_addr = FMC_BANK0_START_ADDRESS + (SIZE_128KB * temp);
                sector_info.sector_end_addr   = sector_info.sector_start_addr + SIZE_128KB - 1;
            }
        }
        else
        {
            /* bank1 area */
            temp = (addr - FMC_BANK1_START_ADDRESS) / SIZE_16KB;
            if (4U > temp)
            {
                sector_info.sector_name       = (uint32_t)(temp + 12);
                sector_info.sector_num        = CTL_SN(temp + 16);
                sector_info.sector_size       = SIZE_16KB;
                sector_info.sector_start_addr = FMC_BANK0_START_ADDRESS + (SIZE_16KB * temp);
                sector_info.sector_end_addr   = sector_info.sector_start_addr + SIZE_16KB - 1;
            }
            else if (8U > temp)
            {
                sector_info.sector_name       = 0x00000010;
                sector_info.sector_num        = CTL_SN(20);
                sector_info.sector_size       = SIZE_64KB;
                sector_info.sector_start_addr = 0x08110000U;
                sector_info.sector_end_addr   = 0x0811FFFFU;
            }
            else if (64U > temp)
            {
                temp                          = (addr - FMC_BANK1_START_ADDRESS) / SIZE_128KB;
                sector_info.sector_name       = (uint32_t)(temp + 16);
                sector_info.sector_num        = CTL_SN(temp + 20);
                sector_info.sector_size       = SIZE_128KB;
                sector_info.sector_start_addr = FMC_BANK1_START_ADDRESS + (SIZE_128KB * temp);
                sector_info.sector_end_addr   = sector_info.sector_start_addr + SIZE_128KB - 1;
            }
            else
            {
                temp                          = (addr - FMC_BANK1_START_ADDRESS) / SIZE_256KB;
                sector_info.sector_name       = (uint32_t)(temp + 20);
                sector_info.sector_num        = CTL_SN(temp + 8);
                sector_info.sector_size       = SIZE_256KB;
                sector_info.sector_start_addr = FMC_BANK1_START_ADDRESS + (SIZE_256KB * temp);
                sector_info.sector_end_addr   = sector_info.sector_start_addr + SIZE_256KB - 1;
            }
        }
    }
    else
    {
        /* invalid address */
        sector_info.sector_name       = FMC_WRONG_SECTOR_NAME;
        sector_info.sector_num        = FMC_WRONG_SECTOR_NUM;
        sector_info.sector_size       = FMC_INVALID_SIZE;
        sector_info.sector_start_addr = FMC_INVALID_ADDR;
        sector_info.sector_end_addr   = FMC_INVALID_ADDR;
    }
    return sector_info;
}

/*!
    \brief      get the sector number by a given sector name
    \param[in]  address: a given sector name
    \param[out] none
    \retval     uint32_t: sector number
*/
uint32_t sector_name_to_number(uint32_t sector_name)
{
    if (11 >= sector_name)
    {
        return CTL_SN(sector_name);
    }
    else if (23 >= sector_name)
    {
        return CTL_SN(sector_name + 4);
    }
    else if (27 >= sector_name)
    {
        return CTL_SN(sector_name - 12);
    }
    else
    {
        os_kprintf("sector_name error!\r\n");
    }
    return 0;
}

#if 0
/*!
    \brief      erases the sector of a given address
    \param[in]  address: a given address
    \param[out] none
    \retval     none
*/
/* clang-format off */
void fmc_erase_sector_by_address(uint32_t address)
{
    fmc_sector_info_struct sector_info;
    
    os_kprintf("FMC erase operation:\r\n");
    /* get information about the sector in which the specified address is located */
    sector_info = fmc_sector_info_get(address);
    if (FMC_WRONG_SECTOR_NAME == sector_info.sector_name)
    {
        os_kprintf("Wrong address!\r\n");
        return;
    }
    else
    {
        os_kprintf("Erase start ......\r\n");
        /* unlock the flash program erase controller */
        fmc_unlock();
        /* clear pending flags */
        fmc_flag_clear(FMC_FLAG_END | FMC_FLAG_OPERR | FMC_FLAG_WPERR | FMC_FLAG_PGMERR | FMC_FLAG_PGSERR);
        /* wait the erase operation complete*/
        if (FMC_READY != fmc_sector_erase(sector_info.sector_num))
        {
            os_kprintf("Erase fail!\r\n");
            fmc_lock();
            return;
        }
        /* lock the flash program erase controller */
        fmc_lock();
        os_kprintf("Address 0x%08X is located in the : SECTOR_NUMBER_%d !\r\n", address, sector_info.sector_name);
        os_kprintf("Sector range: 0x%08X to 0x%08X\r\n", sector_info.sector_start_addr, sector_info.sector_end_addr);
        os_kprintf("Sector size: %d KB\r\n", (sector_info.sector_size / 1024));
        os_kprintf("Erase success!\r\n");
        os_kprintf("\r\n");
    }
}
/* clang-format on */
#endif

/*!
    \brief      erase fmc sectors
    \param[in]  none
    \param[out] none
    \retval     none
*/
int gd32_flash_erase(uint32_t addr, size_t size)
{
    uint32_t               sector_num;
    uint32_t               i;
    fmc_sector_info_struct start_sector_info;
    fmc_sector_info_struct end_sector_info;

    start_sector_info = fmc_sector_info_get(addr);
    end_sector_info   = fmc_sector_info_get(addr + size - 1);

    /* unlock the flash program erase controller */
    fmc_unlock();
    /* clear pending flags */
    fmc_flag_clear(FMC_FLAG_END | FMC_FLAG_OPERR | FMC_FLAG_WPERR | FMC_FLAG_PGMERR | FMC_FLAG_PGSERR);
    /* erase the flash pages */
    for (i = start_sector_info.sector_name; i <= end_sector_info.sector_name; i++)
    {
        sector_num = sector_name_to_number(i);
        if (FMC_READY != fmc_sector_erase(sector_num))
        {
            os_kprintf("Erase fail!\r\n");
            /* lock the flash program erase controller */
            fmc_lock();
            return 0;
        }
    }

    /* lock the flash program erase controller */
    fmc_lock();

    return size;
}

/**
 ***********************************************************************************************************************
 * @brief           gd32_flash_read: Read data from flash.
 *
 * @param[in]       addr            flash address.
 * @param[out]      buf             buffer to store read data.
 * @param[in]       size            read bytes size.
 *
 * @return          Return read size or status.
 * @retval          size            read bytes size.
 * @retval          Others          read failed.
 ***********************************************************************************************************************
 */
int gd32_flash_read(uint32_t addr, uint8_t *buf, size_t size)
{
    size_t i;

    if ((addr + size) > GD32_FLASH_END_ADDRESS)
    {
        LOG_E(LOG_TAG, "read outrange flash size! addr is (0x%p)", (void *)(addr + size));
        return OS_INVAL;
    }

    for (i = 0; i < size; i++, buf++, addr++)
    {
        *buf = *(uint8_t *)addr;
    }

    return size;
}

/*!
    \brief      program fmc byte by byte
    \param[in]  none
    \param[out] none
    \retval     none
*/
int gd32_flash_write(uint32_t addr, const uint8_t *buf, size_t size)
{
    uint32_t i;

    /* unlock the flash program erase controller */
    fmc_unlock();
    /* clear pending flags */
    fmc_flag_clear(FMC_FLAG_END | FMC_FLAG_OPERR | FMC_FLAG_WPERR | FMC_FLAG_PGMERR | FMC_FLAG_PGSERR);

    /* write data_32 to the corresponding address */
    for (i = 0; i < size; i++)
    {
        if (FMC_READY == fmc_byte_program(addr, buf[i]))
        {
            addr++;
        }
        else
        {
            os_kprintf("Write faild!\r\n");
            /* lock the flash program erase controller */
            fmc_lock();
            return 0;
        }
    }
    /* lock the flash program erase controller */
    fmc_lock();

    return size;
}

#include "fal_drv_flash.c"
