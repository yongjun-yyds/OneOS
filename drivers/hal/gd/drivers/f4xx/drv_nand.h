/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_nand.h
 *
 * @brief       This file provides macro declaration for nand driver.
 *
 * @revision
 * Date         Author          Notes
 * 2022-04-28   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __DRV_NAND_H__
#define __DRV_NAND_H__

#include "gd32f4xx.h"
#include "gd32f4xx_exmc.h"
#include "gd32f4xx_gpio.h"

#include "os_types.h"

/* clang-format off */
/* NAND area definition */
/* A16 = CLE high command area */
#define EXMC_CMD_AREA              (uint32_t)(1<<16)
/* A17 = ALE high address area */
#define EXMC_ADDR_AREA             (uint32_t)(1<<17)
/* data area */
#define EXMC_DATA_AREA             ((uint32_t)0x00000000)

/* NAND memory command (GD9FU1G8F2AMG\hynix HY27UF081G2A) */
#define NAND_CMD_READ1_1ST         ((uint8_t)0x00)
#define NAND_CMD_READ1_2ND         ((uint8_t)0x30)
#define NAND_CMD_WRITE_1ST         ((uint8_t)0x80)
#define NAND_CMD_WRITE_2ND         ((uint8_t)0x10)
#define NAND_CMD_ERASE_1ST         ((uint8_t)0x60)
#define NAND_CMD_ERASE_2ND         ((uint8_t)0xD0)
#define NAND_CMD_READID            ((uint8_t)0x90)
#define NAND_CMD_STATUS            ((uint8_t)0x70)
#define NAND_CMD_LOCK_STATUS       ((uint8_t)0x7A)
#define NAND_CMD_RESET             ((uint8_t)0xFF)

/* NAND memory status */
#define NAND_BUSY                  ((uint8_t)0x00)
#define NAND_ERROR                 ((uint8_t)0x01)
#define NAND_READY                 ((uint8_t)0x40)
#define NAND_TIMEOUT_ERROR         ((uint8_t)0x80)

/* NAND memory parameters */
/* NAND zone count */
#define NAND_ZONE_COUNT            ((uint16_t)0x0001)

/* 1024 block per zone */
#define NAND_ZONE_SIZE             ((uint16_t)0x0400)

/* 64 pages per block */
#define NAND_BLOCK_SIZE            ((uint16_t)0x0040)

/* 2 * 1024 bytes per page */
#define NAND_PAGE_SIZE             ((uint16_t)0x0800)

/* last 128 bytes as spare area */
#define NAND_SPARE_AREA_SIZE       ((uint16_t)0x0080)

/* total page size = page size + spare are size */
#define NAND_PAGE_TOTAL_SIZE       (NAND_PAGE_SIZE + NAND_SPARE_AREA_SIZE)

/* max read and write address */
#define NAND_MAX_ADDRESS           (((NAND_ZONE_COUNT*NAND_ZONE_SIZE)*NAND_BLOCK_SIZE)*NAND_PAGE_SIZE)

/* block count */
#define NAND_BLOCK_COUNT            1024

/* NAND memory address computation */
#define ADDR_1ST_CYCLE(ADDR)       (uint8_t)((ADDR)& 0xFF)
#define ADDR_2ND_CYCLE(ADDR)       (uint8_t)(((ADDR)& 0xFF00) >> 8)
#define ADDR_3RD_CYCLE(ADDR)       (uint8_t)(((ADDR)& 0xFF0000) >> 16)
#define ADDR_4TH_CYCLE(ADDR)       (uint8_t)(((ADDR)& 0xFF000000) >> 24)

/* define return value of functions */
#define NAND_OK                    0
#define NAND_FAIL                  1

/* defines the physical address of nand flash, and it is determined by the hardware */
#define BANK1_NAND_ADDR     ((uint32_t)0x70000000)
#define BANK2_NAND_ADDR     ((uint32_t)0x80000000)
/* clang-format on */

typedef struct
{
    uint8_t maker_id;
    uint8_t device_id;
    uint8_t third_id;
    uint8_t fourth_id;
} nand_id_struct;

struct gd32_exmc_nand_info
{
    uint32_t bank_base;
    os_bool_t   is_x8;
    os_err_t (*hard_init)(void);
};

#endif /* __DRV_ETH_H__ */
