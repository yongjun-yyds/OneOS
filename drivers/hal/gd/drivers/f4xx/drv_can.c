/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        st_can.c
 *
 * @brief       This file implements can driver for stm32.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <board.h>
#include <os_errno.h>
#include <os_assert.h>
#include <os_memory.h>
#include <string.h>
#include "drv_can.h"
#define DRV_EXT_TAG "drv.can"
#include <drv_log.h>

typedef struct gd32_can_baud
{
    uint32_t baud;

    uint16_t prescale;
    uint8_t  sjw;
    uint8_t  bs1;
    uint8_t  bs2;
} gd32_can_baud_t;

struct gd32_can
{
    struct os_can_device can;

    gd32_can_info_t *can_info;
    gd32_can_baud_t  baud;

    struct os_can_msg *rx_msg;
    os_list_node_t     list;
};

static os_list_node_t gs_gd32_can_list = OS_LIST_INIT(gs_gd32_can_list);

static struct gd32_can *find_gd32_can(uint32_t can_periph)
{
    struct gd32_can *gd_can;

    os_list_for_each_entry(gd_can, &gs_gd32_can_list, struct gd32_can, list)
    {
        if (gd_can->can_info->can_periph == can_periph)
        {
            return gd_can;
        }
    }

    OS_ASSERT(OS_FALSE);

    return OS_NULL;
}

static const uint32_t sjw_table[] = {
    CAN_BT_SJW_1TQ,
    CAN_BT_SJW_2TQ,
    CAN_BT_SJW_3TQ,
    CAN_BT_SJW_4TQ,
};

static uint32_t gd32_can_sjw(uint8_t sjw)
{
    return sjw_table[sjw - 1];
}

static const uint32_t bs1_table[] = {
    CAN_BT_BS1_1TQ,
    CAN_BT_BS1_2TQ,
    CAN_BT_BS1_3TQ,
    CAN_BT_BS1_4TQ,
    CAN_BT_BS1_5TQ,
    CAN_BT_BS1_6TQ,
    CAN_BT_BS1_7TQ,
    CAN_BT_BS1_8TQ,
    CAN_BT_BS1_9TQ,
    CAN_BT_BS1_10TQ,
    CAN_BT_BS1_11TQ,
    CAN_BT_BS1_12TQ,
    CAN_BT_BS1_13TQ,
    CAN_BT_BS1_14TQ,
    CAN_BT_BS1_15TQ,
    CAN_BT_BS1_16TQ,
};

static uint32_t gd32_can_bs1(uint8_t bs1)
{
    return bs1_table[bs1 - 1];
}

const uint32_t bs2_table[] = {
    CAN_BT_BS2_1TQ,
    CAN_BT_BS2_2TQ,
    CAN_BT_BS2_3TQ,
    CAN_BT_BS2_4TQ,
    CAN_BT_BS2_5TQ,
    CAN_BT_BS2_6TQ,
    CAN_BT_BS2_7TQ,
    CAN_BT_BS2_8TQ,
};

static uint32_t gd32_can_bs2(uint8_t bs2)
{
    return bs2_table[bs2 - 1];
}

static int gd32_can_calc_baud(gd32_can_baud_t *baud)
{
    uint32_t pclk1;
    uint16_t prescale;
    uint8_t  sjw;
    uint8_t  bs1;
    uint8_t  bs2;

    uint64_t tmp;
    uint32_t min_delta = (uint32_t)-1;

    uint16_t best_prescale = (uint16_t)-1;
    uint8_t  best_sjw      = (uint8_t)-1;
    uint8_t  best_bs1      = (uint8_t)-1;
    uint8_t  best_bs2      = (uint8_t)-1;

    pclk1 = rcu_clock_freq_get(CK_APB1);

    for (sjw = 1; sjw < 3; sjw++)
    {
        for (bs2 = 2; bs2 < 6; bs2++)
        {
            for (bs1 = 4; bs1 < 12; bs1++)
            {
                for (prescale = 1; prescale < 0x3FF; prescale++)
                {
                    tmp = (uint64_t)(sjw + bs1 + bs2) * prescale * baud->baud;

                    if (tmp == pclk1)
                    {
                        best_prescale = prescale;
                        best_sjw      = sjw;
                        best_bs1      = bs1;
                        best_bs2      = bs2;
                        min_delta     = 0;
                        goto end;
                    }

                    if (tmp - pclk1 < min_delta || pclk1 - tmp < min_delta)
                    {
                        min_delta     = min(tmp - pclk1, pclk1 - tmp);
                        best_prescale = prescale;
                        best_sjw      = sjw;
                        best_bs1      = bs1;
                        best_bs2      = bs2;
                    }
                }
            }
        }
    }

end:
    if (best_prescale == (uint16_t)-1)
        return -1;

    baud->prescale = best_prescale;
    baud->sjw      = best_sjw;
    baud->bs1      = best_bs1;
    baud->bs2      = best_bs2;

    os_kprintf("can baud:%d, clk:%d, rate:0.%03d\r\n", baud->baud, pclk1, (int)((uint64_t)min_delta * 1000 / pclk1));

    return 0;
}

static os_err_t gd32_can_config(struct os_can_device *can, struct can_configure *cfg)
{
    struct gd32_can      *gd_can;
    can_parameter_struct *can_parameter;
    gd32_can_info_t      *can_info;

    OS_ASSERT(can);
    OS_ASSERT(cfg);

    gd_can = (struct gd32_can *)can;

    can_info      = gd_can->can_info;
    can_parameter = &gd_can->can_info->can_parameter_init;

    switch (cfg->mode)
    {
    case OS_CAN_MODE_NORMAL:
        can_parameter->working_mode = CAN_NORMAL_MODE;
        break;
    case OS_CAN_MODE_LISEN:
        can_parameter->working_mode = CAN_SILENT_MODE;
        break;
    case OS_CAN_MODE_LOOPBACK:
        can_parameter->working_mode = CAN_LOOPBACK_MODE;
        break;
    case OS_CAN_MODE_LOOPBACKANLISEN:
        can_parameter->working_mode = CAN_SILENT_LOOPBACK_MODE;
        break;
    }

    gd32_can_baud_t baud;
    baud.baud = cfg->baud_rate;

    if (gd32_can_calc_baud(&baud) != 0)
    {
        os_kprintf("%s calc baud failed %u.\r\n", device_name(&can->parent), baud.baud);
        return OS_FAILURE;
    }

    gd_can->baud = baud;

    can_parameter->resync_jump_width = gd32_can_sjw(baud.sjw);
    can_parameter->time_segment_1    = gd32_can_bs1(baud.bs1);
    can_parameter->time_segment_2    = gd32_can_bs2(baud.bs2);
    can_parameter->prescaler         = baud.prescale;

    if (can_init(can_info->can_periph, can_parameter) == 0)
    {
        os_kprintf("%s init failed.\r\n", device_name(&can->parent));
        return OS_FAILURE;
    }
    can_filter_init(&can_info->can_filter_parameter_init);

    return OS_SUCCESS;
}

static os_err_t gd32_can_control(struct os_can_device *can, int cmd, void *arg)
{
    uint32_t                  argval;
    struct gd32_can             *gd_can;
    struct os_can_filter_config *filter_cfg;
    gd32_can_info_t             *can_info;
    can_filter_parameter_struct *can_filter;

    OS_ASSERT(can != OS_NULL);
    gd_can   = (struct gd32_can *)can;
    can_info = gd_can->can_info;

    switch (cmd)
    {
    case OS_CAN_CMD_SET_FILTER:
        can_filter = &can_info->can_filter_parameter_init;
        if (OS_NULL == arg)
        {
            can_filter_init(can_filter);    // CAN_FCTL
        }
        else
        {
            filter_cfg = (struct os_can_filter_config *)arg;

            for (int i = 0; i < filter_cfg->count; i++)
            {

                can_filter->filter_number = filter_cfg->items[i].hdr;
                /* initialize filter */
                can_filter->filter_mode = filter_cfg->items[i].mode;
                can_filter->filter_bits = CAN_FILTERBITS_32BIT;
                /* clang-format off */
                can_filter->filter_list_high   = ((filter_cfg->items[i].id & 0x7ff) << 5) | (filter_cfg->items[i].id >> 24);
                /* clang-format on */
                can_filter->filter_list_low    = ((filter_cfg->items[i].id >> 11) | (filter_cfg->items[i].ide << 2) |
                                               (filter_cfg->items[i].rtr << 1) & 0xffff);
                can_filter->filter_mask_high   = (filter_cfg->items[i].mask >> 16) & 0xFFFF;
                can_filter->filter_mask_low    = filter_cfg->items[i].mask & 0xFFFF;
                can_filter->filter_fifo_number = CAN_FIFO0;
                can_filter->filter_enable      = ENABLE;

                can_filter_init(can_filter);    // CAN_FCTL
            }
        }
        break;
    case OS_CAN_CMD_SET_MODE:
        argval = (uint32_t)arg;
        if (argval != OS_CAN_MODE_NORMAL && argval != OS_CAN_MODE_LISEN && argval != OS_CAN_MODE_LOOPBACK &&
            argval != OS_CAN_MODE_LOOPBACKANLISEN)
        {
            return OS_FAILURE;
        }
        if (argval != gd_can->can.config.mode)
        {
            can->config.mode = argval;
            return gd32_can_config(can, &can->config);
        }
        break;
    case OS_CAN_CMD_SET_BAUD:
        argval = (uint32_t)arg;
        if (argval != can->config.baud_rate)
        {
            can->config.baud_rate = argval;
            return gd32_can_config(can, &can->config);
        }
        break;
    case OS_CAN_CMD_SET_PRIV:
        argval = (uint32_t)arg;
        if (argval != OS_CAN_MODE_PRIV && argval != OS_CAN_MODE_NOPRIV)
        {
            return OS_FAILURE;
        }
        if (argval != can->config.privmode)
        {
            can->config.privmode = argval;
            return gd32_can_config(can, &can->config);
        }
        break;
    case OS_CAN_CMD_GET_STATUS:
    {
        uint32_t errtype;
        errtype                 = CAN_ERR(can_info->can_periph);
        can->status.rcverrcnt   = errtype >> 24;
        can->status.snderrcnt   = (errtype >> 16) & 0xFF;
        can->status.lasterrtype = errtype & 0x70;
        can->status.errcode     = errtype & 0x07;

        memcpy(arg, &can->status, sizeof(can->status));
    }
    break;
    }

    return OS_SUCCESS;
}

static int gd32_can_recvmsg(struct os_can_device *can, struct os_can_msg *pmsg, uint32_t fifo)
{
    struct gd32_can *gd_can;
    gd32_can_info_t *can_info;

    can_receive_message_struct rx_message = {0};

    OS_ASSERT(can);
    gd_can   = (struct gd32_can *)can;
    can_info = gd_can->can_info;

    /* get data */
    can_message_receive(can_info->can_periph, fifo, &rx_message);

    /* get id */
    if (CAN_FF_STANDARD == rx_message.rx_ff)
    {
        pmsg->ide = OS_CAN_STDID;
        pmsg->id  = rx_message.rx_sfid;
    }
    else
    {
        pmsg->ide = OS_CAN_EXTID;
        pmsg->id  = rx_message.rx_efid;
    }
    /* get type */
    if (CAN_FT_DATA == rx_message.rx_ft)
    {
        pmsg->rtr = OS_CAN_DTR;
    }
    else
    {
        pmsg->rtr = OS_CAN_RTR;
    }
    /* get len */
    pmsg->len = rx_message.rx_dlen;
    /* get hdr */
    pmsg->hdr = rx_message.rx_ft;
    memcpy(pmsg->data, rx_message.rx_data, rx_message.rx_dlen);

    return OS_SUCCESS;
}

static int gd32_can_start_send(struct os_can_device *can, const struct os_can_msg *pmsg)
{
    struct gd32_can *gd_can;
    gd32_can_info_t *can_info;

    can_trasnmit_message_struct transmit_message = {0};

    OS_ASSERT(can);
    gd_can   = (struct gd32_can *)can;
    can_info = gd_can->can_info;

    if (OS_CAN_STDID == pmsg->ide)
    {
        transmit_message.tx_ff   = CAN_FF_STANDARD;
        transmit_message.tx_sfid = pmsg->id;
    }
    else
    {
        transmit_message.tx_ff   = CAN_FF_EXTENDED;
        transmit_message.tx_efid = pmsg->id;
    }

    if (OS_CAN_DTR == pmsg->rtr)
    {
        transmit_message.tx_ft = CAN_FT_DATA;
    }
    else
    {
        transmit_message.tx_ft = CAN_FT_REMOTE;
    }

    transmit_message.tx_dlen = pmsg->len;

    memcpy(transmit_message.tx_data, pmsg->data, pmsg->len);

    uint8_t ret = can_message_transmit(can_info->can_periph, &transmit_message);
    if (ret == CAN_NOMAILBOX)
    {
        os_kprintf("%s hal send failed.\r\n", device_name(&can->parent), ret);
        return -1;
    }
    can_interrupt_enable(can_info->can_periph, CAN_INT_TME);
    can_interrupt_enable(can_info->can_periph, CAN_INT_ERR);

    return 0;
}

static int gd32_can_stop_send(struct os_can_device *can)
{
    struct gd32_can *gd_can;
    gd32_can_info_t *can_info;

    OS_ASSERT(can);
    gd_can   = (struct gd32_can *)can;
    can_info = gd_can->can_info;

    can_interrupt_disable(can_info->can_periph, CAN_INT_TME);
    can_interrupt_disable(can_info->can_periph, CAN_INT_ERR);

    return 0;
}

static int gd32_can_start_recv(struct os_can_device *can, struct os_can_msg *msg)
{
    struct gd32_can *gd_can;
    gd32_can_info_t *can_info;

    OS_ASSERT(can);
    gd_can   = (struct gd32_can *)can;
    can_info = gd_can->can_info;

    gd_can->rx_msg = msg;

    can_interrupt_enable(can_info->can_periph, CAN_INT_RFNE0 | CAN_INT_RFF0 | CAN_INT_RFO0);
    can_interrupt_enable(can_info->can_periph, CAN_INT_RFNE1 | CAN_INT_RFF1 | CAN_INT_RFO1);

    return 0;
}

static int gd32_can_stop_recv(struct os_can_device *can)
{
    struct gd32_can *gd_can;
    gd32_can_info_t *can_info;

    OS_ASSERT(can);
    gd_can   = (struct gd32_can *)can;
    can_info = gd_can->can_info;

    can_interrupt_disable(can_info->can_periph, CAN_INT_RFNE0 | CAN_INT_RFF0 | CAN_INT_RFO0);
    can_interrupt_disable(can_info->can_periph, CAN_INT_RFNE1 | CAN_INT_RFF1 | CAN_INT_RFO1);

    return 0;
}

static int gd32_can_recv_state(struct os_can_device *can)
{
    return 0;
}

static const struct os_can_ops gd32_can_ops = {
    .configure  = gd32_can_config,
    .control    = gd32_can_control,
    .start_send = gd32_can_start_send,
    .stop_send  = gd32_can_stop_send,
    .start_recv = gd32_can_start_recv,
    .stop_recv  = gd32_can_stop_recv,
    .recv_state = gd32_can_recv_state,
};

void CAN_Tx_Callback(uint32_t can_periph)
{
    can_interrupt_disable(can_periph, CAN_INT_TME);
    /* clang-format off */
    if (!(can_flag_get(can_periph, CAN_FLAG_MTE0) || 
          can_flag_get(can_periph, CAN_FLAG_MTE1) ||
          can_flag_get(can_periph, CAN_FLAG_MTE2)))
    /* clang-format on */
    {
        os_hw_can_isr_txdone((struct os_can_device *)find_gd32_can(can_periph), OS_CAN_EVENT_TX_DONE);
    }
    else
    {
        os_hw_can_isr_txdone((struct os_can_device *)find_gd32_can(can_periph), OS_CAN_EVENT_TX_FAIL);
    }
    can_interrupt_flag_clear(can_periph, CAN_INT_FLAG_MTF0);
    can_interrupt_flag_clear(can_periph, CAN_INT_FLAG_MTF1);
    can_interrupt_flag_clear(can_periph, CAN_INT_FLAG_MTF2);
}

void CAN_Rx0_Callback(uint32_t can_periph)
{
    struct gd32_can *gd_can = find_gd32_can(can_periph);

    can_interrupt_flag_clear(can_periph, CAN_INT_FLAG_RFO0);
    can_interrupt_flag_clear(can_periph, CAN_INT_FLAG_RFF0);
    can_interrupt_flag_clear(can_periph, CAN_INT_FLAG_RFL0);

    can_interrupt_disable(can_periph, CAN_INT_RFNE0 | CAN_INT_RFF0 | CAN_INT_RFO0);

    if (gd_can->rx_msg == OS_NULL)
        return;

    gd32_can_recvmsg(&gd_can->can, gd_can->rx_msg, CAN_FIFO0);
    gd_can->rx_msg = OS_NULL;

    os_hw_can_isr_rxdone((struct os_can_device *)gd_can);
}

void CAN_Rx1_Callback(uint32_t can_periph)
{
    struct gd32_can *gd_can = find_gd32_can(can_periph);

    can_interrupt_flag_clear(can_periph, CAN_INT_FLAG_RFO1);
    can_interrupt_flag_clear(can_periph, CAN_INT_FLAG_RFF1);
    can_interrupt_flag_clear(can_periph, CAN_INT_FLAG_RFL1);

    can_interrupt_disable(can_periph, CAN_INT_RFNE1 | CAN_INT_RFF1 | CAN_INT_RFO1);

    if (gd_can->rx_msg == OS_NULL)
        return;

    gd32_can_recvmsg(&gd_can->can, gd_can->rx_msg, CAN_FIFO1);
    gd_can->rx_msg = OS_NULL;

    os_hw_can_isr_rxdone((struct os_can_device *)gd_can);
}

void CAN0_TX_IRQHandler(void)
{
    CAN_Tx_Callback(CAN0);
}

void CAN0_RX0_IRQHandler(void)
{
    CAN_Rx0_Callback(CAN0);
}

void CAN0_RX1_IRQHandler(void)
{
    CAN_Rx1_Callback(CAN0);
}

void CAN0_EWMC_IRQHandler(void)
{
    if (can_flag_get(CAN0, CAN_FLAG_ERRIF))
    {
        can_flag_clear(CAN0, CAN_FLAG_ERRIF);
    }
}

void CAN1_TX_IRQHandler(void)
{
    CAN_Tx_Callback(CAN1);
}

void CAN1_RX0_IRQHandler(void)
{
    CAN_Rx0_Callback(CAN1);
}

void CAN1_RX1_IRQHandler(void)
{
    CAN_Rx1_Callback(CAN1);
}

void CAN1_EWMC_IRQHandler(void)
{
    if (can_flag_get(CAN1, CAN_FLAG_ERRIF))
    {
        can_flag_clear(CAN1, CAN_FLAG_ERRIF);
    }
}

static void gd32_can_gpio_init(struct gd32_can_pin_info *info)
{
    rcu_periph_clock_enable(info->periph);

    /* configure CAN0 GPIO, CAN0_TX(PB9) and CAN0_RX(PB8) */
    gpio_output_options_set(info->can_tx_port, GPIO_OTYPE_PP, GPIO_OSPEED_50MHZ, info->can_tx_pin);
    gpio_mode_set(info->can_tx_port, GPIO_MODE_AF, GPIO_PUPD_NONE, info->can_tx_pin);
    gpio_af_set(info->can_tx_port, info->alt_func_num, info->can_tx_pin);

    gpio_output_options_set(info->can_rx_port, GPIO_OTYPE_PP, GPIO_OSPEED_50MHZ, info->can_rx_pin);
    gpio_mode_set(info->can_rx_port, GPIO_MODE_AF, GPIO_PUPD_NONE, info->can_rx_pin);
    gpio_af_set(info->can_rx_port, info->alt_func_num, info->can_rx_pin);
}

static int gd32_can_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    struct can_configure      config = CANDEFAULTCONFIG;
    struct gd32_can_pin_info *pin_info;

    os_ubase_t level;

    struct gd32_can *gd_can = os_calloc(1, sizeof(struct gd32_can));

    OS_ASSERT(gd_can);

    gd_can->can_info = (gd32_can_info_t *)dev->info;
    pin_info         = &gd_can->can_info->can_pin_info;

    rcu_periph_clock_enable(gd_can->can_info->rcu_periph);
    gd32_can_gpio_init(pin_info);
    if (gd_can->can_info->can_periph == CAN0)
    {
        nvic_irq_enable(CAN0_TX_IRQn, 0, 0);
        nvic_irq_enable(CAN0_RX0_IRQn, 0, 1);
        nvic_irq_enable(CAN0_RX1_IRQn, 1, 0);
        nvic_irq_enable(CAN0_EWMC_IRQn, 1, 1);
    }
    else
    {
        nvic_irq_enable(CAN1_TX_IRQn, 0, 0);
        nvic_irq_enable(CAN1_RX0_IRQn, 0, 0);
        nvic_irq_enable(CAN1_RX1_IRQn, 0, 0);
        nvic_irq_enable(CAN1_EWMC_IRQn, 0, 0);
    }
    config.privmode = OS_CAN_MODE_NOPRIV;
    config.ticks    = 50;
#ifdef OS_CAN_USING_HDR
    config.maxhdr = 14;
#ifdef CAN2
    config.maxhdr = 28;
#endif
#endif

    gd_can->can.config = config;

    os_spin_lock_irqsave(&gs_device_lock, &level);
    os_list_add_tail(&gs_gd32_can_list, &gd_can->list);
    os_spin_unlock_irqrestore(&gs_device_lock, level);

    os_hw_can_register(&gd_can->can, dev->name, &gd32_can_ops, gd_can);

    return OS_SUCCESS;
}

OS_DRIVER_INFO gd32_can_driver = {
    .name  = "CAN_HandleTypeDef",
    .probe = gd32_can_probe,
};

OS_DRIVER_DEFINE(gd32_can_driver, OS_INIT_LEVEL_PRE_DEVICE, OS_INIT_SUBLEVEL_LOW);

