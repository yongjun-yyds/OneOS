/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_hwtimer.h
 *
 * @brief       This file implements hwtimer driver for gd32.
 *
 * @revision
 * Date         Author          Notes
 * 2020-09-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __DRV_HWTIMER_H__
#define __DRV_HWTIMER_H__

#include <board.h>
#include <os_task.h>
#include <timer/clocksource.h>
#include <timer/clockevent.h>
#include "gd32f4xx_rcu.h"

#ifdef OS_USING_PWM
#include "drv_pwm.h"
#endif

#define TIMER_MODE_TIM 0x00
#define TIMER_MODE_PWM 0x01

struct gd32_timer_info
{
    uint32_t        timer_periph;
    rcu_periph_enum periph;
    uint8_t         nvic_irq;
    uint8_t         mode;
    
    timer_parameter_struct timer_initpara;
    
#ifdef OS_USING_PWM
    struct gd32_pwm_info pwm_info;
#endif
};

struct gd32_timer
{
    union _clock
    {
        os_clocksource_t cs;
        os_clockevent_t  ce;
    } clock;

    struct gd32_timer_info *info;
    uint32_t                freq;
    os_list_node_t          list;
};

os_bool_t gd32_timer_is_32b(uint32_t timer);

#endif
