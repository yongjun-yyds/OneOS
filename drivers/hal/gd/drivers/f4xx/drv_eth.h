/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_eth.h
 *
 * @brief       This file provides macro declaration for eth driver.
 *
 * @revision
 * Date         Author          Notes
 * 2022-04-28   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __DRV_ETH_H__
#define __DRV_ETH_H__

#include "gd32f4xx.h"
#include "gd32f4xx_enet.h"
#include "gd32f4xx_gpio.h"

#include "os_types.h"

struct gd32_enet_info
{
    os_err_t (*hard_init)(void);
};

#endif /* __DRV_ETH_H__ */
