/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_nand.c
 *
 * @details
 *
 * @revision
 * Date          Author          Notes
 * 2020-02-20    OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include "gd32f4xx_exmc.h"
#include "drv_nand.h"

#include <board.h>
#include <os_memory.h>
#include <bus/bus.h>

#define DRV_EXT_TAG "drv.nand"
#include <dlog.h>

/* clang-format off */
struct gd32_exmc_nand
{
    os_nand_device_t            nand;

    os_bool_t                   is_x8;

    struct gd32_exmc_nand_info *info;

    os_list_node_t              list;
};
/* clang-format on */
static os_list_node_t gd32_exmc_nand_list = OS_LIST_INIT(gd32_exmc_nand_list);

static uint8_t exmc_nand_readstatus(struct gd32_exmc_nand *exmc_nand)
{
    uint8_t data;
    uint8_t status = NAND_BUSY;

    /* send read status command to the command area */
    *(__IO uint8_t *)(exmc_nand->info->bank_base | EXMC_CMD_AREA) = NAND_CMD_STATUS;

    data = *(__IO uint32_t *)(exmc_nand->info->bank_base | EXMC_DATA_AREA);

    if ((data & NAND_ERROR) == NAND_ERROR)
    {
        status = NAND_ERROR;
    }
    else if ((data & NAND_READY) == NAND_READY)
    {
        status = NAND_READY;
    }
    else
    {
        status = NAND_BUSY;
    }

    return (status);
}

static uint8_t exmc_nand_getstatus(struct gd32_exmc_nand *exmc_nand)
{
    uint32_t timeout = 0x10000;
    uint8_t  status  = NAND_READY;

    status = exmc_nand_readstatus(exmc_nand);

    /* waiting for NAND operation over, it will exit after a timeout. */
    while ((status != NAND_READY) && (timeout != 0x00))
    {
        status = exmc_nand_readstatus(exmc_nand);
        timeout--;
    }

    if (timeout == 0x00)
    {
        status = NAND_TIMEOUT_ERROR;
    }

    return (status);
}

static os_err_t
gd32_exmc_nand_read_page(os_nand_device_t *nand, uint32_t page_addr, uint8_t *buff, uint32_t page_nr)
{
    uint32_t i = 0;
    uint32_t j = 0;

    uint32_t cur_page_addr = page_addr;

    struct gd32_exmc_nand *exmc_nand = (struct gd32_exmc_nand *)nand;

    for (i = 0; i < page_nr; i++)
    {
        /* send 1st cycle page programming command to the command area */
        *(__IO uint8_t *)(exmc_nand->info->bank_base | EXMC_CMD_AREA) = NAND_CMD_READ1_1ST;

        /* send address to the address area */
        *(__IO uint8_t *)(exmc_nand->info->bank_base | EXMC_ADDR_AREA) = 0;
        *(__IO uint8_t *)(exmc_nand->info->bank_base | EXMC_ADDR_AREA) = 0;
        *(__IO uint8_t *)(exmc_nand->info->bank_base | EXMC_ADDR_AREA) = (uint8_t)(cur_page_addr & 0xFF);
        *(__IO uint8_t *)(exmc_nand->info->bank_base | EXMC_ADDR_AREA) = (uint8_t)((cur_page_addr >> 8) & 0xFF);

        /* send 2nd cycle page programming command to the command area */
        *(__IO uint8_t *)(exmc_nand->info->bank_base | EXMC_CMD_AREA) = NAND_CMD_READ1_2ND;

        /* write data to data area */
        for (j = 0; j < nand->cfg.info.page_size; j++)
        {
            *buff = *(__IO uint8_t *)(exmc_nand->info->bank_base | EXMC_DATA_AREA);
            buff++;
        }

        /* check operation status */
        if (NAND_READY != exmc_nand_getstatus(exmc_nand))
        {
            return OS_FAILURE;
        }

        cur_page_addr++;
    }

    return OS_SUCCESS;
}

static os_err_t
gd32_exmc_nand_write_page(os_nand_device_t *nand, uint32_t page_addr, const uint8_t *buff, uint32_t page_nr)
{
    uint32_t i = 0;
    uint32_t j = 0;

    uint32_t cur_page_addr = page_addr;

    struct gd32_exmc_nand *exmc_nand = (struct gd32_exmc_nand *)nand;

    for (i = 0; i < page_nr; i++)
    {
        /* send 1st cycle page programming command to the command area */
        *(__IO uint8_t *)(exmc_nand->info->bank_base | EXMC_CMD_AREA) = NAND_CMD_WRITE_1ST;

        /* send address to the address area */
        *(__IO uint8_t *)(exmc_nand->info->bank_base | EXMC_ADDR_AREA) = 0;
        *(__IO uint8_t *)(exmc_nand->info->bank_base | EXMC_ADDR_AREA) = 0;
        *(__IO uint8_t *)(exmc_nand->info->bank_base | EXMC_ADDR_AREA) = (uint8_t)(cur_page_addr & 0xFF);
        *(__IO uint8_t *)(exmc_nand->info->bank_base | EXMC_ADDR_AREA) = (uint8_t)((cur_page_addr >> 8) & 0xFF);

        /* write data to data area */
        for (j = 0; j < nand->cfg.info.page_size; j++)
        {
            *(__IO uint8_t *)(exmc_nand->info->bank_base | EXMC_DATA_AREA) = *buff;
            buff++;
        }

        /* send 2nd cycle page programming command to the command area */
        *(__IO uint8_t *)(exmc_nand->info->bank_base | EXMC_CMD_AREA) = NAND_CMD_WRITE_2ND;

        /* check operation status */
        if (NAND_READY != exmc_nand_getstatus(exmc_nand))
        {
            return OS_FAILURE;
        }

        cur_page_addr++;
    }

    return OS_SUCCESS;
}

static os_err_t
gd32_exmc_nand_read_spare(os_nand_device_t *nand, uint32_t page_addr, uint8_t *buff, uint32_t spare_nr)
{
    uint32_t i = 0;
    uint32_t j = 0;

    uint32_t cur_page_addr = page_addr;

    struct gd32_exmc_nand *exmc_nand = (struct gd32_exmc_nand *)nand;

    for (i = 0; i < spare_nr; i++)
    {
        /* send 1st cycle page programming command to the command area */
        *(__IO uint8_t *)(exmc_nand->info->bank_base | EXMC_CMD_AREA) = NAND_CMD_READ1_1ST;

        /* send address to the address area */
        *(__IO uint8_t *)(exmc_nand->info->bank_base | EXMC_ADDR_AREA) = (uint8_t)(nand->cfg.info.page_size & 0xFF);
        *(__IO uint8_t *)(exmc_nand->info->bank_base | EXMC_ADDR_AREA) =
            (uint8_t)((nand->cfg.info.page_size >> 8) & 0xFF);
        *(__IO uint8_t *)(exmc_nand->info->bank_base | EXMC_ADDR_AREA) = (uint8_t)(cur_page_addr & 0xFF);
        *(__IO uint8_t *)(exmc_nand->info->bank_base | EXMC_ADDR_AREA) = (uint8_t)((cur_page_addr >> 8) & 0xFF);

        /* send 2nd cycle page programming command to the command area */
        *(__IO uint8_t *)(exmc_nand->info->bank_base | EXMC_CMD_AREA) = NAND_CMD_READ1_2ND;

        /* write data to data area */
        for (j = 0; j < nand->cfg.info.spare_size; j++)
        {
            *buff = *(__IO uint8_t *)(exmc_nand->info->bank_base | EXMC_DATA_AREA);
            buff++;
        }

        /* check operation status */
        if (NAND_READY != exmc_nand_getstatus(exmc_nand))
        {
            return OS_FAILURE;
        }

        cur_page_addr++;
    }

    return OS_SUCCESS;
}

static os_err_t
gd32_exmc_nand_write_spare(os_nand_device_t *nand, uint32_t page_addr, const uint8_t *buff, uint32_t spare_nr)
{
    uint32_t i = 0;
    uint32_t j = 0;

    uint32_t cur_page_addr = page_addr;

    struct gd32_exmc_nand *exmc_nand = (struct gd32_exmc_nand *)nand;

    for (i = 0; i < spare_nr; i++)
    {
        /* send 1st cycle page programming command to the command area */
        *(__IO uint8_t *)(exmc_nand->info->bank_base | EXMC_CMD_AREA) = NAND_CMD_WRITE_1ST;

        /* send address to the address area */
        *(__IO uint8_t *)(exmc_nand->info->bank_base | EXMC_ADDR_AREA) = (uint8_t)(nand->cfg.info.page_size & 0xFF);
        *(__IO uint8_t *)(exmc_nand->info->bank_base | EXMC_ADDR_AREA) =
            (uint8_t)((nand->cfg.info.page_size >> 8) & 0xFF);
        *(__IO uint8_t *)(exmc_nand->info->bank_base | EXMC_ADDR_AREA) = (uint8_t)(cur_page_addr & 0xFF);
        *(__IO uint8_t *)(exmc_nand->info->bank_base | EXMC_ADDR_AREA) = (uint8_t)((cur_page_addr >> 8) & 0xFF);

        /* write data to data area */
        for (j = 0; j < nand->cfg.info.spare_size; j++)
        {
            *(__IO uint8_t *)(exmc_nand->info->bank_base | EXMC_DATA_AREA) = *buff;
            buff++;
        }

        /* send 2nd cycle page programming command to the command area */
        *(__IO uint8_t *)(exmc_nand->info->bank_base | EXMC_CMD_AREA) = NAND_CMD_WRITE_2ND;

        /* check operation status */
        if (NAND_READY != exmc_nand_getstatus(exmc_nand))
        {
            return OS_FAILURE;
        }

        cur_page_addr++;
    }

    return OS_SUCCESS;
}
/* clang-format on */

static os_err_t gd32_exmc_nand_erase_block(os_nand_device_t *nand, uint32_t page_addr)
{
    struct gd32_exmc_nand *exmc_nand = (struct gd32_exmc_nand *)nand;

    /* send 1st cycle page programming command to the command area */
    *(__IO uint8_t *)(exmc_nand->info->bank_base | EXMC_CMD_AREA) = NAND_CMD_ERASE_1ST;

    *(__IO uint8_t *)(exmc_nand->info->bank_base | EXMC_ADDR_AREA) = ADDR_1ST_CYCLE(page_addr);
    *(__IO uint8_t *)(exmc_nand->info->bank_base | EXMC_ADDR_AREA) = ADDR_2ND_CYCLE(page_addr);

    /* send 2nd cycle erase command to command area */
    *(__IO uint8_t *)(exmc_nand->info->bank_base | EXMC_CMD_AREA) = NAND_CMD_ERASE_2ND;

    /* check operation status */
    if (NAND_READY != exmc_nand_getstatus(exmc_nand))
    {
        return OS_FAILURE;
    }

    return OS_SUCCESS;
}

static os_err_t gd32_exmc_nand_get_status(os_nand_device_t *nand)
{
    struct gd32_exmc_nand *exmc_nand = (struct gd32_exmc_nand *)nand;

    /* check operation status */
    if (NAND_READY != exmc_nand_getstatus(exmc_nand))
    {
        return OS_FAILURE;
    }

    return OS_SUCCESS;
}

static const struct os_nand_ops gd32_exmc_nand_ops = {
    .read_page       = gd32_exmc_nand_read_page,
    .write_page      = gd32_exmc_nand_write_page,
    .read_spare      = gd32_exmc_nand_read_spare,
    .write_spare     = gd32_exmc_nand_write_spare,
    .erase_block     = gd32_exmc_nand_erase_block,
    .config_hardecc  = OS_NULL,
    .control_hardecc = OS_NULL,
    .get_hardecc     = OS_NULL,
    .get_status      = gd32_exmc_nand_get_status,
};

static os_err_t exmc_nand_reset(struct gd32_exmc_nand *exmc_nand)
{
    /* send command to the command area */
    *(__IO uint8_t *)(exmc_nand->info->bank_base | EXMC_CMD_AREA) = NAND_CMD_RESET;

    /* check operation status */
    if (NAND_READY != exmc_nand_getstatus(exmc_nand))
    {
        return OS_FAILURE;
    }

    return OS_SUCCESS;
}

static void exmc_nand_read_id(struct gd32_exmc_nand *exmc_nand, nand_id_struct *nand_id)
{
    uint32_t data = 0;

    /* send command to the command area */
    *(__IO uint8_t *)(exmc_nand->info->bank_base | EXMC_CMD_AREA) = NAND_CMD_READID;

    /* send address to the address area */
    *(__IO uint8_t *)(exmc_nand->info->bank_base | EXMC_ADDR_AREA) = 0x00;

    /* read id from NAND flash */
    data = *(__IO uint32_t *)(exmc_nand->info->bank_base | EXMC_DATA_AREA);

    nand_id->maker_id  = ADDR_1ST_CYCLE(data);
    nand_id->device_id = ADDR_2ND_CYCLE(data);
    nand_id->third_id  = ADDR_3RD_CYCLE(data);
    nand_id->fourth_id = ADDR_4TH_CYCLE(data);
}

static int gd32_exmc_nand_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    os_ubase_t level;

    nand_id_struct id;
    uint32_t    nand_id = 0;

    struct gd32_exmc_nand *exmc_nand = os_calloc(1, sizeof(struct gd32_exmc_nand));
    if (exmc_nand == OS_NULL)
    {
        LOG_E(DRV_EXT_TAG, "no memory");
        return OS_SUCCESS;
    }

    exmc_nand->info = (struct gd32_exmc_nand_info *)dev->info;

    if (exmc_nand->info->hard_init == OS_NULL)
    {
        LOG_E(DRV_EXT_TAG, "no init function");
        goto __exit;
    }

    if (exmc_nand->info->hard_init() != OS_SUCCESS)
    {
        LOG_E(DRV_EXT_TAG, "hard init failed");
        goto __exit;
    }

    if (exmc_nand_reset(exmc_nand) != OS_SUCCESS)
    {
        LOG_E(DRV_EXT_TAG, "nand reset failed");
        goto __exit;
    }

    exmc_nand->nand.ops = &gd32_exmc_nand_ops;

    exmc_nand_read_id(exmc_nand, &id);

    nand_id = (id.maker_id << 24) | (id.device_id << 16) | (id.third_id << 8) | id.fourth_id;

    os_kprintf("nand id: 0x%08x\r\n", nand_id);

    if (os_nand_device_register(&exmc_nand->nand, dev->name, nand_id) != OS_SUCCESS)
    {
        LOG_E(DRV_EXT_TAG, "nand register failed ");
        goto __exit;
    }

    if ((exmc_nand->nand.cfg.info.bus_width == 8) && exmc_nand->info->is_x8)
    {
        exmc_nand->is_x8 = OS_TRUE;
    }
    else if ((exmc_nand->nand.cfg.info.bus_width != 8) && !exmc_nand->info->is_x8)
    {
        exmc_nand->is_x8 = OS_FALSE;
    }
    else
    {
        LOG_E(DRV_EXT_TAG, "nand chip bus wigth not fit hard ");
        return OS_FAILURE;
    }

    os_spin_lock_irqsave(&gs_device_lock, &level);
    os_list_add_tail(&gd32_exmc_nand_list, &exmc_nand->list);
    os_spin_unlock_irqrestore(&gs_device_lock, level);

    return OS_SUCCESS;

__exit:
    if (exmc_nand)
    {
        os_free(exmc_nand);
    }

    return OS_SUCCESS;
}

OS_DRIVER_INFO gd32_exmc_nand_driver = {
    .name  = "NAND_Type",
    .probe = gd32_exmc_nand_probe,
};

OS_DRIVER_DEFINE(gd32_exmc_nand_driver, OS_INIT_LEVEL_DEVICE, OS_INIT_SUBLEVEL_HIGH);

