/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_eth.c
 *
 * @brief       This file implements eth driver.
 *
 * @revision
 * Date         Author          Notes
 * 2022-04-28   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include "gd32f4xx.h"
#include "gd32f4xx_enet.h"
#include "gd32f4xx_gpio.h"

#include "board.h"
#include <os_memory.h>
#include <string.h>
#include <bus/bus.h>

#include <net_dev.h>
#include "drv_eth.h"

#define DRV_EXT_LVL DBG_EXT_INFO
#define DRV_EXT_TAG "drv.eth"
#include <dlog.h>

/* ENET RxDMA/TxDMA descriptor */
extern enet_descriptors_struct rxdesc_tab[ENET_RXBUF_NUM], txdesc_tab[ENET_TXBUF_NUM];
/*global transmit and receive descriptors pointers */
extern enet_descriptors_struct *dma_current_txdesc;
extern enet_descriptors_struct *dma_current_rxdesc;

struct gd32_enet_filter
{
    enet_macaddress_enum mac_addr;
    os_bool_t            status;
    uint8_t           addr[OS_NET_MAC_LENGTH];
};

struct os_gd32_eth
{
    struct os_net_device net_dev;
    struct napi_struct napi;

    uint8_t dev_addr[OS_NET_MAC_LENGTH];

    struct gd32_enet_info *info;

    int link;
    os_timer_id timer;
};

struct os_gd32_eth *g_eth_dev = OS_NULL;

void ENET_IRQHandler(void)
{
    enet_interrupt_flag_clear(ENET_DMA_INT_FLAG_RS_CLR);
    enet_interrupt_flag_clear(ENET_DMA_INT_FLAG_NI_CLR);
    napi_schedule(&g_eth_dev->napi);
}

void enet_setup(struct os_gd32_eth *eth_dev)
{
    uint8_t i = 0;

    //#ifdef USE_ENET_INTERRUPT
    enet_interrupt_enable(ENET_DMA_INT_NIE);
    enet_interrupt_enable(ENET_DMA_INT_RIE);
    //#endif /* USE_ENET_INTERRUPT */

    /* initialize MAC address in ethernet MAC */
    enet_mac_address_set(ENET_MAC_ADDRESS0, eth_dev->dev_addr);

    /* initialize descriptors list: chain/ring mode */
#ifdef SELECT_DESCRIPTORS_ENHANCED_MODE
    enet_ptp_enhanced_descriptors_chain_init(ENET_DMA_TX);
    enet_ptp_enhanced_descriptors_chain_init(ENET_DMA_RX);
#else

    enet_descriptors_chain_init(ENET_DMA_TX);
    enet_descriptors_chain_init(ENET_DMA_RX);

    //    enet_descriptors_ring_init(ENET_DMA_TX);
    //    enet_descriptors_ring_init(ENET_DMA_RX);

#endif /* SELECT_DESCRIPTORS_ENHANCED_MODE */

    /* enable ethernet Rx interrrupt */
    for (i = 0; i < ENET_RXBUF_NUM; i++)
    {
        enet_rx_desc_immediate_receive_complete_interrupt(&rxdesc_tab[i]);
    }

    //#ifdef CHECKSUM_BY_HARDWARE
    /* enable the TCP, UDP and ICMP checksum insertion for the Tx frames */
    //    for(i = 0;i < ENET_TXBUF_NUM;i++)
    //    {
    //        enet_transmit_checksum_config(&txdesc_tab[i], ENET_CHECKSUM_TCPUDPICMP_FULL);
    //    }
    //#endif /* CHECKSUM_BY_HARDWARE */

    /* note: TCP, UDP, ICMP checksum checking for received frame are enabled in DMA config */

    /* enable MAC and DMA transmission and reception */
    enet_enable();

    enet_fliter_feature_enable(ENET_FILTER_MODE_EITHER);
}

static os_err_t os_gd32_eth_deinit(struct os_net_device *net_dev)
{
    enet_deinit();

    return OS_SUCCESS;
}

#define GD32_FILTER_NUM_MAX 3
/* clang-format off */
#define GD32_MACADDR_MASK   ENET_ADDRESS_MASK_BYTE0 | ENET_ADDRESS_MASK_BYTE1 | ENET_ADDRESS_MASK_BYTE2 | \
                            ENET_ADDRESS_MASK_BYTE3 | ENET_ADDRESS_MASK_BYTE4 | ENET_ADDRESS_MASK_BYTE5
/* clang-format on */

struct gd32_enet_filter enet_filter[GD32_FILTER_NUM_MAX] = {
    {ENET_MAC_ADDRESS1, OS_FALSE, {0}},
    {ENET_MAC_ADDRESS2, OS_FALSE, {0}},
    {ENET_MAC_ADDRESS3, OS_FALSE, {0}},
};
static os_err_t os_gd32_eth_set_filter(struct os_net_device *net_dev, uint8_t *addr, os_bool_t enable)
{
    uint32_t i = 0;

    if (enable)
    {
        for (i = 0; i < GD32_FILTER_NUM_MAX; i++)
        {
            if (enet_filter[i].status == OS_FALSE)
            {
                break;
            }
        }

        if (i == GD32_FILTER_NUM_MAX)
        {
            return OS_FAILURE;
        }

        enet_mac_address_set(enet_filter[i].mac_addr, addr);
        enet_address_filter_config(enet_filter[i].mac_addr, GD32_MACADDR_MASK, ENET_ADDRESS_FILTER_DA);
        enet_address_filter_enable(enet_filter[i].mac_addr);

        memcpy(enet_filter[i].addr, addr, OS_NET_MAC_LENGTH);
        enet_filter[i].status = OS_TRUE;
    }
    else
    {
        for (i = 0; i < GD32_FILTER_NUM_MAX; i++)
        {
            if ((memcmp(enet_filter[i].addr, addr, OS_NET_MAC_LENGTH) == 0) && (enet_filter[i].status == OS_TRUE))
            {
                break;
            }
        }

        if (i == GD32_FILTER_NUM_MAX)
        {
            return OS_FAILURE;
        }

        enet_address_filter_disable(enet_filter[i].mac_addr);
        memset(enet_filter[i].addr, 0, OS_NET_MAC_LENGTH);
        enet_filter[i].status = OS_FALSE;
    }

    return OS_SUCCESS;
}

static os_err_t os_gd32_eth_get_macaddr(struct os_net_device *net_dev, uint8_t *addr)
{
    struct os_gd32_eth *eth_dev = (struct os_gd32_eth *)net_dev;

    memcpy(addr, eth_dev->dev_addr, OS_NET_MAC_LENGTH);

    return OS_SUCCESS;
}

static os_err_t os_gd32_eth_send(struct os_net_device *net_dev, uint8_t *data, int len)
{
    uint8_t *buffer = OS_NULL;

    buffer = (uint8_t *)(enet_desc_information_get(dma_current_txdesc, TXDESC_BUFFER_1_ADDR));

    memcpy(buffer, data, len);

    /* note: padding and CRC for transmitted frame
       are automatically inserted by DMA */

    /* transmit descriptors to give to DMA */
#ifdef SELECT_DESCRIPTORS_ENHANCED_MODE
    ENET_NOCOPY_PTPFRAME_TRANSMIT_ENHANCED_MODE(len, NULL);
#else
    if (ENET_NOCOPY_FRAME_TRANSMIT(len) != SUCCESS)
    {
        LOG_E(DRV_EXT_TAG, "eth send error");
        return OS_FAILURE;
    }
#endif /* SELECT_DESCRIPTORS_ENHANCED_MODE */

    return OS_SUCCESS;
}

static int os_gd32_eth_poll(struct napi_struct *napi, int budget)
{
    int i, length;
    uint8_t *buffer = OS_NULL;

    for (i = 0; i < budget; i++)
    {
        if (enet_rxframe_size_get() == 0)
        {
            LOG_E(DRV_EXT_TAG, "no data");
            break;
        }

        /* obtain the size of the packet and put it into the "len" variable. */
        length = enet_desc_information_get(dma_current_rxdesc, RXDESC_FRAME_LENGTH);
        
        if (length <= 0)
        {
            napi_complete_done(napi, i);
            break;
        }
        
        buffer = (uint8_t *)(enet_desc_information_get(dma_current_rxdesc, RXDESC_BUFFER_1_ADDR));
        os_net_rx_data(napi->dev, buffer, length);

#ifdef SELECT_DESCRIPTORS_ENHANCED_MODE
        ENET_NOCOPY_PTPFRAME_RECEIVE_ENHANCED_MODE(NULL);
#else
        if (ENET_NOCOPY_FRAME_RECEIVE() != SUCCESS)
        {
            LOG_E(DRV_EXT_TAG, "eth recv error");
            break;
        }
#endif /* SELECT_DESCRIPTORS_ENHANCED_MODE */
    }

    return i;
}

static void gd32_phy_init(struct os_gd32_eth *eth_dev)
{
    int16_t  phy_init_count;

    /* enable ethernet clock  */
    rcu_periph_clock_enable(RCU_ENET);
    rcu_periph_clock_enable(RCU_ENETTX);
    rcu_periph_clock_enable(RCU_ENETRX);

    /* reset ethernet on AHB bus */
    enet_deinit();

    phy_init_count = 0;
    while (enet_software_reset() != SUCCESS)
    {
        if (phy_init_count++ >= 2)
        {
            return;
        }

        os_task_msleep(20);
    }

    phy_init_count = 0;
#ifdef CHECKSUM_BY_HARDWARE
    while (enet_init(ENET_AUTO_NEGOTIATION, ENET_AUTOCHECKSUM_DROP_FAILFRAMES, ENET_BROADCAST_FRAMES_PASS) != SUCCESS)
#else
    while (enet_init(ENET_AUTO_NEGOTIATION, ENET_NO_AUTOCHECKSUM, ENET_BROADCAST_FRAMES_PASS) != SUCCESS)
#endif
    {
        if (phy_init_count++ >= 1)
        {
            LOG_E(DRV_EXT_TAG, "eth connet failed");
            return;
        }

        LOG_W(DRV_EXT_TAG, "eth try connet");

        os_task_msleep(20);
    }

    nvic_vector_table_set(NVIC_VECTTAB_FLASH, 0x0);
    nvic_priority_group_set(NVIC_PRIGROUP_PRE2_SUB2);
    nvic_irq_enable(ENET_IRQn, 0, 0);

    enet_setup(eth_dev);
    eth_dev->link = OS_FALSE;
}

static void gd32_phy_event(void *parameter)
{
    os_bool_t link = OS_FALSE;
    uint16_t phy_value = 0;

    struct os_gd32_eth *eth_dev = (struct os_gd32_eth *)parameter;

    enet_phy_write_read(ENET_PHY_READ, PHY_ADDRESS, PHY_REG_BSR, &phy_value);
    
    if ((phy_value & PHY_AUTONEGO_COMPLETE) != RESET)
    {
        link = OS_TRUE;
    }
    else
    {
        link = OS_FALSE;
    }

    if (eth_dev->link != link)
    {
        os_net_linkchange(&eth_dev->net_dev, link);
    }

    eth_dev->link = link;
}

const static struct os_net_device_ops gd32f4xx_net_dev_ops = {
    .init       = OS_NULL,    // os_gd32_eth_init,
    .deinit     = os_gd32_eth_deinit,
    .send       = os_gd32_eth_send,
    .get_mac    = os_gd32_eth_get_macaddr,
    .set_filter = os_gd32_eth_set_filter,
};

static int gd32_eth_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    g_eth_dev = os_calloc(1, sizeof(struct os_gd32_eth));
    if (g_eth_dev == OS_NULL)
    {
        LOG_E(DRV_EXT_TAG, "no memory");
        return OS_SUCCESS;
    }

    g_eth_dev->info = (struct gd32_enet_info *)dev->info;
    if (g_eth_dev->info->hard_init == OS_NULL)
    {
        LOG_E(DRV_EXT_TAG, "no init function");
        return OS_SUCCESS;
    }

    g_eth_dev->info->hard_init();

    uint32_t *UID_BASE = (uint32_t *)(0x1FFF7A10);

    g_eth_dev->dev_addr[0] = 0x00;
    g_eth_dev->dev_addr[1] = 0x01;
    g_eth_dev->dev_addr[2] = *(uint8_t *)(UID_BASE + 3);
    g_eth_dev->dev_addr[3] = *(uint8_t *)(UID_BASE + 2);
    g_eth_dev->dev_addr[4] = *(uint8_t *)(UID_BASE + 1);
    g_eth_dev->dev_addr[5] = *(uint8_t *)(UID_BASE + 0);

    g_eth_dev->net_dev.info.MTU       = 1500;
    g_eth_dev->net_dev.info.MRU       = 1500;
    g_eth_dev->net_dev.info.mode      = net_dev_mode_sta;
    g_eth_dev->net_dev.info.intf_type = net_dev_intf_ether;
    g_eth_dev->net_dev.ops            = &gd32f4xx_net_dev_ops;

    os_net_device_register(&g_eth_dev->net_dev, dev->name);

    /* napi */
    netif_napi_add(&g_eth_dev->net_dev, &g_eth_dev->napi, os_gd32_eth_poll, NAPI_POLL_WEIGHT);

#if OS_TIMER_TASK_STACK_SIZE < 1024
#warning: timer stack too small OS_TIMER_TASK_STACK_SIZE
#endif

    gd32_phy_init(g_eth_dev);

    g_eth_dev->timer = os_timer_create(OS_NULL,
                                     dev->name,
                                     gd32_phy_event,
                                     g_eth_dev,
                                     OS_TICK_PER_SECOND,
                                     OS_TIMER_FLAG_PERIODIC);
    os_timer_start(g_eth_dev->timer);

    return OS_SUCCESS;
}

OS_DRIVER_INFO gd32_eth_driver = {
    .name  = "ETH_Type",
    .probe = gd32_eth_probe,
};

OS_DRIVER_DEFINE(gd32_eth_driver, OS_INIT_LEVEL_COMPONENT, OS_INIT_SUBLEVEL_LOW);

