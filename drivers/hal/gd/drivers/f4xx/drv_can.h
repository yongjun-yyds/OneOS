/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_can.h
 *
 * @brief        This file provides functions declaration for can driver.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __DRV_CAN_H__
#define __DRV_CAN_H__

#include <os_task.h>
#include <device.h>
#include <drv_cfg.h>

struct gd32_can_pin_info
{
    rcu_periph_enum periph;
    uint32_t     can_tx_port;
    uint32_t     can_tx_pin;
    uint32_t     can_rx_port;
    uint32_t     can_rx_pin;
    uint32_t     alt_func_num;
};

typedef struct gd32_can_info
{
    uint32_t                 can_periph;
    rcu_periph_enum             rcu_periph;
    can_parameter_struct        can_parameter_init;
    can_filter_parameter_struct can_filter_parameter_init;
    struct gd32_can_pin_info    can_pin_info;
} gd32_can_info_t;

#endif /* __DRV_CAN_H__ */

/******************* end of file *******************/
