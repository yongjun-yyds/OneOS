/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        gd_devicess.c
 *
 * @brief       This file implements driver cfg
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include "board.h"
#include "drv_cfg.h"
#include "drv_common.h"

#ifdef BSP_USING_I2C
#include "dev_i2c.c"
#endif

#ifdef BSP_USING_USART
#include "dev_usart.c"
#endif

#ifdef BSP_USING_RTC
#include "dev_rtc.c"
#endif

#ifdef BSP_USING_SPI
#include "dev_spi.c"
#endif

#ifdef BSP_USING_FLASH
#include "ports/flash_info.c"
#endif

#ifdef BSP_USING_ADC
#include "dev_adc.c"
#endif

#ifdef BSP_USING_DAC
#include "dev_dac.c"
#endif

#ifdef BSP_USING_TIMER
#include "dev_timer.c"
#endif

#ifdef BSP_USING_ETH
    #include "dev_eth.c"
#endif
#ifdef BSP_USING_CAN
    #include "dev_can.c"
#endif
#ifdef BSP_USING_NAND_FLASH
    #include "dev_nand.c"
#endif
#ifdef BSP_USING_QSPI
    #include "dev_qspi.c"
#endif

