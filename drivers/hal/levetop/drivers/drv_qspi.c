/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_qspi.c
 *
 * @brief       This file implements QSPI driver for lt776.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <board.h>
#include <drv_qspi.h>

static os_list_node_t lt776_qspi_list = OS_LIST_INIT(lt776_qspi_list);

static void QSPI_Mode_Init(uint8_t mode, struct os_xspi_message *xmessage)
{
    SSI_InitTypeDef SPI5;

    if (mode == STD_mode)
    {
        SPI5.CLK_STRETCH_EN = CLK_STRETCH_DISABLE;
        SPI5.TMOD = TX_AND_RX;
        SPI5.SSI_STD_BaudRatePrescaler = 2; // SSI2_STD_BaudRatePrescaler; //���2��Ƶ��200M SysCLK�£�100 Max��
        SPI5.Transfer_start_FIFO_level = 0;
        SPI5.Transmit_FIFO_Threshold   = 0;
        SPI5.Receive_FIFO_Threshold    = 0;
        SPI5.WAIT_CYCLES = 0x0;
        SPI5.NDF = 0;
        SPI5.Trans_Type = TT0; // TTO:ָ��͵�ַʹ�õ���; TT1:ָ��ʹ�õ��ߣ���ַ���������;TT2:ָ��͵�ַ��ʹ���������
    }
    else if (mode == QUAD_mode)
    {
        SPI5.CLK_STRETCH_EN = CLK_STRETCH_ENABLE;
        SPI5.SSI_QUAD_BaudRatePrescaler = 6;
        SPI5.WAIT_CYCLES = 6;
        SPI5.NDF = xmessage_data_size(xmessage) - 1;
        if (xmessage_data_dir(xmessage) == OS_XSPI_DATA_DIR_DEVICE_TO_HOST)
        {
            SPI5.TMOD = RX_ONLY;
            SPI5.Transfer_start_FIFO_level = 1;
            SPI5.Transmit_FIFO_Threshold   = 0;
            SPI5.Receive_FIFO_Threshold    = 7;
        }
        else if (xmessage_data_dir(xmessage) == OS_XSPI_DATA_DIR_HOST_TO_DEVICE)
        {
            SPI5.TMOD = TX_ONLY;
            SPI5.Transfer_start_FIFO_level = 2;
            SPI5.Transmit_FIFO_Threshold   = 0;
            SPI5.Receive_FIFO_Threshold    = 0;
        }
        
        if (xmessage_instruction(xmessage) == 1 && xmessage_address_lines(xmessage) == 1)
            SPI5.Trans_Type = TT0;
        else if(xmessage_instruction(xmessage) == 1 && xmessage_address_lines(xmessage) == 4)
            SPI5.Trans_Type = TT1;
    }

    SPI5.Format = mode;
    SPI5.DFS    = DFS_08_BIT;       //����֡λ����ʽ
    SPI5.FRF    = FRF_SPI;          //
    SPI5.SCPH   = SCPH_MIDDLE_BIT; //
    SPI5.SCPOL  = INACTIVE_HIGH;  //
    SPI5.SRL    = NORMAL_MODE;      //����ģʽ
    SPI5.SSTE   = TOGGLE_DISABLE;
    SPI5.CFS    = CFS_01_BIT; //����֡λ����ʽ
    SPI5.Rx_Sample_Delay = SAMPLE_DELAY_2_0;

    SPI5.Address_Length     = ADDR_L24;
    SPI5.Instruction_length = INST_L8;

    DRV_SSI_Init(SSI2, &SPI5, 0xFFFFFF);
}

uint16_t SSI2_ReadWriteByte(uint16_t TxData)
{

    uint32_t time;
    time = 50000;
    while (time && ((SSI2->SR & 0x04) == 0)) //�ȴ�TX FIFO��
    {
        __nop();
        time--;
    }
    SSI2->DR = TxData; //����һ��byte
    time = 50000;
    while (time && ((SSI2->SR & 0x08) == 0)) //��RX FIFO�ǿգ���������һ��byte
    {
        __nop();
        time--;
    }
    return SSI2->DR; //�����յ�������
}

static void HAL_SPI_Transmit(uint8_t *send_buf, uint16_t length)
{
    uint16_t i;

    for (i = 0; i < length; i++) //ѭ��д����
    {
        if ((SSI2->SR & 0x02) == 0x02)              // TX FIFO û��������д��
            SSI2->DR = ((uint8_t *)send_buf)[i]; //ѭ��д����
        else
        {
            while ((SSI2->SR & 0x04) == 0)
            {
                ;
            } // TX FIFO �������ȴ�FIFO��

            SSI2->DR = ((uint8_t *)send_buf)[i];
        }
    }
}

static void HAL_SPI_Receive(uint8_t *recv_buf, uint16_t length)
{
    uint16_t i;

    for (i = 0; i < length; i++)
    {
        while ((SSI2->SR & 0x08) == 0)
        {
            __nop();
        }

        ((uint8_t *)recv_buf)[i] = SSI2->DR;  //ѭ��������
    }
}

static int lt776_qspi_configure(struct os_sfbus *sfbus, struct os_spi_configuration *configuration)
{
    QSPI_Mode_Init(STD_mode, OS_NULL);

    return 0;
}

static int lt776_qspi_transfer(struct os_sfbus *sfbus, struct os_xspi_message *xmessage)
{
    uint32_t i;

    if (xmessage_data_lines(xmessage) == 4)
    {
        QSPI_Mode_Init(QUAD_mode, xmessage);
        
        SSI2->DR = xmessage_instruction(xmessage);
        SSI2->DR = xmessage_address(xmessage);

        if (xmessage_data_dir(xmessage) == OS_XSPI_DATA_DIR_HOST_TO_DEVICE)
        {
            HAL_SPI_Transmit((uint8_t *)xmessage_data(xmessage), xmessage_data_size(xmessage));
        }
        else if (xmessage_data_dir(xmessage) == OS_XSPI_DATA_DIR_DEVICE_TO_HOST)
        {
            HAL_SPI_Receive((uint8_t *)xmessage_data(xmessage), xmessage_data_size(xmessage));
        }
    }
    else
    {
        QSPI_Mode_Init(STD_mode, xmessage);

        SSI2_ReadWriteByte(xmessage_instruction(xmessage));

        if (xmessage_address_lines(xmessage))
        {
            switch (xmessage_address_size(xmessage))
            {
            case 4:
                SSI2_ReadWriteByte(xmessage_address(xmessage) >> 24);
            case 3:
                SSI2_ReadWriteByte(xmessage_address(xmessage) >> 16);
            case 2:
                SSI2_ReadWriteByte(xmessage_address(xmessage) >> 8);
            case 1:
                SSI2_ReadWriteByte(xmessage_address(xmessage));
                break;
            default:
                break;
            }
        }

        if (xmessage_data_lines(xmessage) == 0)
        {
            return 0;
        }

        if (xmessage_data_dir(xmessage) == OS_XSPI_DATA_DIR_HOST_TO_DEVICE)
        {
            for (i = 0; i < xmessage_data_size(xmessage); i++)
            {
                SSI2_ReadWriteByte((xmessage_data(xmessage))[i]);
            }
        }
        else if (xmessage_data_dir(xmessage) == OS_XSPI_DATA_DIR_DEVICE_TO_HOST)
        {
            for (i = 0; i < xmessage_data_size(xmessage); i++)
            {
                (xmessage_data(xmessage))[i] = SSI2_ReadWriteByte(DUMMY_BYTE); //ѭ������
            }
        }
    }

    return 0;
}

static const struct os_sfbus_ops lt776_qspi_ops = {
    .configure = lt776_qspi_configure,
    .transfer  = lt776_qspi_transfer,
};

static int lt776_qspi_bus_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    struct lt776_qspi *qspi = os_calloc(1, sizeof(struct lt776_qspi));

    OS_ASSERT(qspi != OS_NULL);

    qspi->QSPI_Handler = (SSI_TypeDef *)dev->info;

    qspi->sfbus.support_1_line = OS_TRUE;

    return os_sfbus_xspi_register(&qspi->sfbus, dev->name, &lt776_qspi_ops);
}

OS_DRIVER_INFO lt776_qspi_driver = {
    .name  = "SSI_TypeDef",
    .probe = lt776_qspi_bus_probe,
};

OS_DRIVER_DEFINE(lt776_qspi_driver, OS_INIT_LEVEL_PRE_DEVICE, OS_INIT_SUBLEVEL_LOW);

