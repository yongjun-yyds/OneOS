/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_can.h
 *
 * @brief       This file provides functions declaration for lt776 can driver.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __DRV_CAN_H__
#define __DRV_CAN_H__

#include <board.h>
#include <can_drv.h>
#include <sys.h>

/**
 * @brief UART Error Codes
 *
 */
#define CAN_ERROR_NONE ((u32)0x00) /*!< No error            */
#define CAN_ERROR_PE   ((u32)0x01)   /*!< Parity error        */
#define CAN_ERROR_NE   ((u32)0x02)   /*!< Noise error         */
#define CAN_ERROR_FE   ((u32)0x04)   /*!< frame error         */
#define CAN_ERROR_ORE  ((u32)0x08)  /*!< Overrun error       */

/**
 * @brief HAL UART State structures definition
 */
typedef enum
{
    CAN_STATE_RESET   = 0x00U,      /*!< CAN not yet initialized or disabled */
    CAN_STATE_READY   = 0x01U,      /*!< CAN initialized and ready for use   */
    CAN_STATE_BUSY    = 0x02U,       /*!< CAN process is ongoing              */
    CAN_STATE_BUSY_TX = 0x12U,    /*!< CAN process is ongoing              */
    CAN_STATE_BUSY_RX = 0x22U,    /*!< CAN process is ongoing              */
    CAN_STATE_BUSY_TX_RX = 0x32U, /*!< CAN process is ongoing              */
    CAN_STATE_TIMEOUT = 0x03U,    /*!< Timeout state                       */
    CAN_STATE_ERROR   = 0x04U       /*!< CAN error state                     */
} CAN_StateTypeDef;

typedef void (*CAN_CALLBACK)(void *);
/**
 *@brief CAN 句柄结构体定义
 */
typedef struct
{
    CAN_TypeDef     *Instance;       /*!< Register base address          */
    CAN_InitTypeDef  Init;        /*!< CAN required parameters        */
    CanTxMsgTypeDef *pTxMsg;     /*!< Pointer to transmit structure  */
    u32 TxSize;                  /**< 待接收数据长度   */
    u32 TxCnt;                   /**< 待接收数据计数   */
    CanRxMsgTypeDef *pRxMsg;     /*!< Pointer to reception structure */
    u32 RxSize;                  /**< 待发送数据长度  */
    u32 RxCnt;                   /**< 待发送数据计数  */
    __IO CAN_StateTypeDef State; /*!< CAN communication state        */
    //  LockTypeDef             Lock;           /*!< CAN locking object             */
    __IO u32 ErrorCode;         /*!< CAN Error code                 */
    CAN_CALLBACK BOFFCallback;  /**< Bus Off回调函数 */
    CAN_CALLBACK ErrCallback;   /**< 错误处理回调函数 */
    CAN_CALLBACK RwrnCallback;  /**< RX Warning处理回调函数 */
    CAN_CALLBACK TwrnCallback;  /**< TX Warning处理回调函数 */
    CAN_CALLBACK MBIsrCallback; /**< Message Buffer中断回调函数 */
} CAN_HandleTypeDef, *pCAN_HandleTypeDef;

struct lt776_can
{
    struct os_can_device can;
    CAN_TypeDef *hcan;
    uint32_t  baud;
    struct os_can_msg *can_rx_msg;
    os_list_node_t list;
};
#endif
