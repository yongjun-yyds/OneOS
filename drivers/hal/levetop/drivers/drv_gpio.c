/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_gpio.c
 *
 * @brief       This file implements gpio driver for lt776.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <drv_gpio.h>
#include <eport_drv.h>
#include <ioctrl_drv.h>
#include <sys.h>

#define DBG_TAG "drv_gpio"

#define ITEM_NUM(items) sizeof(items) / sizeof(items[0])

#define __LT776_IRQHandler(eport, pin)                                                                                 \
    EPORT##eport##_##pin##_IRQHandler(void)                                                                            \
    {                                                                                                                  \
        lt776_GPIO_EXTI_IRQHandler(EPORT##eport##_##pin##_IRQn, EPORT_PIN##pin);                                       \
    }

#define __LT776_PIN(index, eport, pin, irqn)                                                                           \
    {                                                                                                                  \
        index, EPORT##eport, EPORT_PIN##pin, EPORT##irqn##_IRQn                                                        \
    }

static os_list_node_t pin_irq_hdr_list = OS_LIST_INIT(pin_irq_hdr_list);

struct pin_irq_hdr
{
    struct os_pin_irq_hdr pin_hdr;
    os_list_node_t        list;
};

typedef struct
{
    IOCTRL_GINTIDTypeDef id;
    IOCTRL_DSTypeDef     ds;
    IOCTRL_SRTypeDef     sr;
    IOCTRL_ISTypeDef     is;
    IOCTRL_PSTypeDef     ps;
    IOCTRL_IETypeDef     ie;
    uint8_t           pins;
} IOCTRL_GINTInitTypeDef;

OS_INLINE void pin_irq_handle(os_base_t pin)
{
    struct pin_irq_hdr *pin_irq_hdr;

    os_list_for_each_entry(pin_irq_hdr, &pin_irq_hdr_list, struct pin_irq_hdr, list)
    {
        if ((pin_irq_hdr->pin_hdr.pin == pin) && (pin_irq_hdr->pin_hdr.hdr))
        {
            pin_irq_hdr->pin_hdr.hdr(pin_irq_hdr->pin_hdr.args);
        }
    }
}

static const struct pin_index pins[] = {
    __LT776_PIN(1, 10, 81, 10_1),   __LT776_PIN(2, 10, 80, 10_0),   __LT776_PIN(3, 10, 84, 10_4),
    __LT776_PIN(4, 10, 85, 10_5),   __LT776_PIN(5, 10, 83, 10_3),   __LT776_PIN(6, 10, 82, 10_2),
    __LT776_PIN(8, 1, 14, 1_6),     __LT776_PIN(11, 13, 111, 13_7), __LT776_PIN(12, 13, 110, 13_6),
    __LT776_PIN(13, 13, 109, 13_5), __LT776_PIN(14, 14, 114, 14_2), __LT776_PIN(15, 14, 112, 14_0),
    __LT776_PIN(16, 13, 108, 13_4), __LT776_PIN(17, 13, 107, 13_3), __LT776_PIN(18, 13, 106, 13_2),
    __LT776_PIN(19, 14, 113, 14_1), __LT776_PIN(20, 13, 105, 13_1), __LT776_PIN(21, 13, 104, 13_0),
    __LT776_PIN(22, 14, 115, 14_3), __LT776_PIN(24, 15, 126, 15_6), __LT776_PIN(25, 15, 125, 15_5),
    __LT776_PIN(26, 15, 124, 15_4), __LT776_PIN(27, 15, 123, 15_3), __LT776_PIN(28, 3, 26, 3_2),
    __LT776_PIN(29, 3, 27, 3_3),    __LT776_PIN(30, 15, 127, 15_7), __LT776_PIN(31, 3, 28, 3_4),
    __LT776_PIN(34, 3, 31, 3_7),    __LT776_PIN(35, 3, 30, 3_6),    __LT776_PIN(36, 3, 29, 3_5),
    __LT776_PIN(37, 3, 25, 3_1),    __LT776_PIN(40, 3, 24, 3_0),    __LT776_PIN(45, 1, 13, 1_5),
    __LT776_PIN(46, 1, 12, 1_4),    __LT776_PIN(47, 12, 98, 12_2),  __LT776_PIN(48, 12, 100, 12_4),
    __LT776_PIN(49, 12, 99, 12_3),  __LT776_PIN(50, 12, 96, 12_0),  __LT776_PIN(51, 12, 97, 12_1),
    __LT776_PIN(52, 12, 101, 12_5), __LT776_PIN(53, 9, 72, 9_0),    __LT776_PIN(54, 9, 75, 9_3),
    __LT776_PIN(55, 9, 74, 9_2),    __LT776_PIN(56, 9, 73, 9_1),    __LT776_PIN(57, 16, 132, 15_4),
    __LT776_PIN(58, 16, 128, 15_0), __LT776_PIN(59, 16, 131, 15_3), __LT776_PIN(60, 16, 133, 15_5),
    __LT776_PIN(61, 16, 134, 15_6), __LT776_PIN(62, 14, 119, 14_7), __LT776_PIN(63, , 3, 0_3),
    __LT776_PIN(64, , 2, 0_2),      __LT776_PIN(65, , 5, 0_5),      __LT776_PIN(66, , 4, 0_4),
    __LT776_PIN(67, , 1, 0_1),      __LT776_PIN(69, 2, 18, 2_2),    __LT776_PIN(70, 2, 19, 2_3),
    __LT776_PIN(71, , 0, 0_0),      __LT776_PIN(72, 2, 16, 2_0),    __LT776_PIN(73, 2, 17, 2_1),
    __LT776_PIN(88, 1, 11, 1_3),    __LT776_PIN(89, 1, 10, 1_2),    __LT776_PIN(90, 1, 9, 1_1),
    __LT776_PIN(91, 1, 8, 1_0),
};

void lt776_GPIO_EXTI_IRQHandler(IRQn_Type irqn, uint8_t pinx)
{
    uint8_t              i;
    os_bool_t               find = OS_FALSE;
    struct pin_irq_hdr     *pin_irq_hdr;
    const struct pin_index *pin_index = OS_NULL;

    for (i = 0; ((i < ITEM_NUM(pins)) && (find == OS_FALSE)); i++)
    {
        if (irqn == pins[i].irqn)
        {
            pin_index = &pins[i];
            os_list_for_each_entry(pin_irq_hdr, &pin_irq_hdr_list, struct pin_irq_hdr, list)
            {
                if (pin_index->index == pin_irq_hdr->pin_hdr.pin)
                {
                    find = OS_TRUE;
                    break;
                }
            }
        }
    }

    if (find == OS_FALSE)
    {
        return;
    }

    os_bool_t status = _eport_epfr_pins_flag_get(pin_index->eport, pinx);
    if (status)
    {
        _eport_epfr_pins_flag_clr(pin_index->eport, pinx);
    }
    else
    {
        DRV_EPORT_SetIT(pin_index->eport, pinx, DISABLE);
    }

    pin_irq_handle(pin_index->index);
}

void __LT776_IRQHandler(0, 0);
void __LT776_IRQHandler(0, 1);
void __LT776_IRQHandler(0, 2);
void __LT776_IRQHandler(0, 3);
void __LT776_IRQHandler(0, 4);
void __LT776_IRQHandler(0, 5);
void __LT776_IRQHandler(0, 6);
void __LT776_IRQHandler(0, 7);
void __LT776_IRQHandler(1, 0);
void __LT776_IRQHandler(1, 1);
void __LT776_IRQHandler(1, 2);
void __LT776_IRQHandler(1, 3);
void __LT776_IRQHandler(1, 4);
void __LT776_IRQHandler(1, 5);
void __LT776_IRQHandler(1, 6);
void __LT776_IRQHandler(1, 7);
void __LT776_IRQHandler(2, 0);
void __LT776_IRQHandler(2, 1);
void __LT776_IRQHandler(2, 2);
void __LT776_IRQHandler(2, 3);
void __LT776_IRQHandler(2, 4);
void __LT776_IRQHandler(2, 5);
void __LT776_IRQHandler(2, 6);
void __LT776_IRQHandler(2, 7);
void __LT776_IRQHandler(3, 0);
void __LT776_IRQHandler(3, 1);
void __LT776_IRQHandler(3, 2);
void __LT776_IRQHandler(3, 3);
void __LT776_IRQHandler(3, 4);
void __LT776_IRQHandler(3, 5);
void __LT776_IRQHandler(3, 6);
void __LT776_IRQHandler(3, 7);
void __LT776_IRQHandler(4, 0);
void __LT776_IRQHandler(4, 1);
void __LT776_IRQHandler(4, 2);
void __LT776_IRQHandler(4, 3);
void __LT776_IRQHandler(4, 4);
void __LT776_IRQHandler(4, 5);
void __LT776_IRQHandler(4, 6);
void __LT776_IRQHandler(4, 7);
void __LT776_IRQHandler(5, 0);
void __LT776_IRQHandler(5, 1);
void __LT776_IRQHandler(5, 2);
void __LT776_IRQHandler(5, 3);
void __LT776_IRQHandler(5, 4);
void __LT776_IRQHandler(5, 5);
void __LT776_IRQHandler(5, 6);
void __LT776_IRQHandler(5, 7);
void __LT776_IRQHandler(6, 0);
void __LT776_IRQHandler(6, 1);
void __LT776_IRQHandler(6, 2);
void __LT776_IRQHandler(6, 3);
void __LT776_IRQHandler(6, 4);
void __LT776_IRQHandler(6, 5);
void __LT776_IRQHandler(6, 6);
void __LT776_IRQHandler(6, 7);
void __LT776_IRQHandler(7, 0);
void __LT776_IRQHandler(7, 1);
void __LT776_IRQHandler(7, 2);
void __LT776_IRQHandler(7, 3);
void __LT776_IRQHandler(7, 4);
void __LT776_IRQHandler(7, 5);
void __LT776_IRQHandler(7, 6);
void __LT776_IRQHandler(7, 7);
void __LT776_IRQHandler(8, 0);
void __LT776_IRQHandler(8, 1);
void __LT776_IRQHandler(8, 2);
void __LT776_IRQHandler(8, 3);
void __LT776_IRQHandler(8, 4);
void __LT776_IRQHandler(8, 5);
void __LT776_IRQHandler(8, 6);
void __LT776_IRQHandler(8, 7);
void __LT776_IRQHandler(9, 0);
void __LT776_IRQHandler(9, 1);
void __LT776_IRQHandler(9, 2);
void __LT776_IRQHandler(9, 3);
void __LT776_IRQHandler(9, 4);
void __LT776_IRQHandler(9, 5);
void __LT776_IRQHandler(9, 6);
void __LT776_IRQHandler(9, 7);
void __LT776_IRQHandler(10, 0);
void __LT776_IRQHandler(10, 1);
void __LT776_IRQHandler(10, 2);
void __LT776_IRQHandler(10, 3);
void __LT776_IRQHandler(10, 4);
void __LT776_IRQHandler(10, 5);
void __LT776_IRQHandler(10, 6);
void __LT776_IRQHandler(10, 7);
void __LT776_IRQHandler(11, 0);
void __LT776_IRQHandler(11, 1);
void __LT776_IRQHandler(11, 2);
void __LT776_IRQHandler(11, 3);
void __LT776_IRQHandler(11, 4);
void __LT776_IRQHandler(11, 5);
void __LT776_IRQHandler(11, 6);
void __LT776_IRQHandler(11, 7);
void __LT776_IRQHandler(12, 0);
void __LT776_IRQHandler(12, 1);
void __LT776_IRQHandler(12, 2);
void __LT776_IRQHandler(12, 3);
void __LT776_IRQHandler(12, 4);
void __LT776_IRQHandler(12, 5);
void __LT776_IRQHandler(12, 6);
void __LT776_IRQHandler(12, 7);
void __LT776_IRQHandler(13, 0);
void __LT776_IRQHandler(13, 1);
void __LT776_IRQHandler(13, 2);
void __LT776_IRQHandler(13, 3);
void __LT776_IRQHandler(13, 4);
void __LT776_IRQHandler(13, 5);
void __LT776_IRQHandler(13, 6);
void __LT776_IRQHandler(13, 7);
void __LT776_IRQHandler(14, 0);
void __LT776_IRQHandler(14, 1);
void __LT776_IRQHandler(14, 2);
void __LT776_IRQHandler(14, 3);
void __LT776_IRQHandler(14, 4);
void __LT776_IRQHandler(14, 5);
void __LT776_IRQHandler(14, 6);
void __LT776_IRQHandler(14, 7);
void __LT776_IRQHandler(15, 0);
void __LT776_IRQHandler(15, 1);
void __LT776_IRQHandler(15, 2);
void __LT776_IRQHandler(15, 3);
void __LT776_IRQHandler(15, 4);
void __LT776_IRQHandler(15, 5);
void __LT776_IRQHandler(15, 6);
void __LT776_IRQHandler(15, 7);

const struct pin_index *get_pin(uint8_t pin)
{
    const struct pin_index *index = OS_NULL;

    uint16_t i;
    for (i = 0; i < ITEM_NUM(pins); i++)
    {
        if (pin == pins[i].index)
        {
            index = &pins[i];
            break;
        }
    }

    return index;
};

/**
 * @brief �л����ùܽ�ΪGINT(EPORT)
 *
 * @@param[in] pEport��ָ��eport���ָ��
 * @@param[in] Pins��eport pin��
 * @retval NONE
 */
void GintSwitch(EPORT_TypeDef *pEport, uint8_t Pins)
{
    uint8_t i;
    if (EPORT == pEport)
    {
    }
    else if (EPORT1 == pEport)
    {
        for (i = 0; i < 8; i++)
        {
            if (Pins & (0x1 << i))
            {
                switch (i)
                {
                case 0:
                    DRV_IOCTRL_SwapintEn((IOCTRL_SwapTypeDef)8);    // PIN8   SWAPINTCR 0x5C
                    break;
                case 1:
                    DRV_IOCTRL_SwapintEn((IOCTRL_SwapTypeDef)9);    // PIN9   SWAPINTCR 0x5C
                    break;
                case 2:
                    DRV_IOCTRL_SwapintEn((IOCTRL_SwapTypeDef)10);    // PIN10   SWAPINTCR 0x5C
                    break;
                case 3:
                    DRV_IOCTRL_SwapintEn((IOCTRL_SwapTypeDef)11);    // PIN11    SWAPINTCR 0x5C
                    break;
                case 4:
                    // PIN12
                    break;
                case 5:
                    DRV_IOCTRL_SwapDis((IOCTRL_SwapTypeDef)0);    // PIN13    SWAPCR 0x1c
                    break;
                case 6:
                    DRV_IOCTRL_SwapDis((IOCTRL_SwapTypeDef)1);    // PIN14    SWAPCR 0x1c
                    break;
                case 7:
                    DRV_IOCTRL_SwapDis((IOCTRL_SwapTypeDef)1);    // PIN15     SWAPCR 0x1c
                    break;
                default:
                    break;
                }
            }
        }
    }
    else if (EPORT2 == pEport)
    {
        for (i = 0; i < 8; i++)
        {
            if (Pins & (0x1 << i))
            {
                switch (i)
                {
                case 0:
                    DRV_IOCTRL_SwapintEn((IOCTRL_SwapTypeDef)(IOCTRL_SwapTypeDef)0);    // PIN16   SWAPINTCR 0x5C
                    break;
                case 1:
                    DRV_IOCTRL_SwapintEn((IOCTRL_SwapTypeDef)(IOCTRL_SwapTypeDef)1);    // PIN17   SWAPINTCR 0x5C
                    break;
                case 2:
                    DRV_IOCTRL_SwapintEn((IOCTRL_SwapTypeDef)(IOCTRL_SwapTypeDef)2);    // PIN18   SWAPINTCR 0x5C
                    break;
                case 3:
                    DRV_IOCTRL_SwapintEn((IOCTRL_SwapTypeDef)(IOCTRL_SwapTypeDef)3);    // PIN19    SWAPINTCR 0x5C
                    break;
                case 4:
                    DRV_IOCTRL_SwapintEn((IOCTRL_SwapTypeDef)(IOCTRL_SwapTypeDef)4);    // PIN20    SWAPINTCR 0x5C
                    break;
                case 5:
                    // PIN21
                    break;
                case 6:
                    // PIN22
                    break;
                case 7:
                    // PIN23
                    break;
                default:
                    break;
                }
            }
        }
    }
    else if (EPORT3 == pEport)
    {
        for (i = 0; i < 8; i++)
        {
            if (Pins & (0x1 << i))
            {
                switch (i)
                {
                case 0:
                    DRV_IOCTRL_Swap2En((IOCTRL_SwapTypeDef)8);    // PIN24   SWAP2CR  0x60
                    break;
                case 1:
                    DRV_IOCTRL_Swap2En((IOCTRL_SwapTypeDef)9);    // PIN25   SWAP2CR  0x60
                    break;
                case 2:
                    DRV_IOCTRL_Swap2En((IOCTRL_SwapTypeDef)10);    // PIN26   SWAP2CR  0x60
                    break;
                case 3:
                    DRV_IOCTRL_Swap2En((IOCTRL_SwapTypeDef)11);    // PIN27   SWAP2CR  0x60
                    break;
                case 4:
                    DRV_IOCTRL_Swap2En((IOCTRL_SwapTypeDef)12);    // PIN28   SWAP2CR  0x60
                    break;
                case 5:
                    DRV_IOCTRL_Swap2En((IOCTRL_SwapTypeDef)13);    // PIN29   SWAP2CR  0x60
                    break;
                case 6:
                    DRV_IOCTRL_Swap2En((IOCTRL_SwapTypeDef)14);    // PIN30   SWAP2CR  0x60
                    break;
                case 7:
                    DRV_IOCTRL_Swap2En((IOCTRL_SwapTypeDef)15);    // PIN31    SWAP2CR  0x60
                    break;
                default:
                    break;
                }
            }
        }
    }
    else if (EPORT4 == pEport)
    {
        for (i = 0; i < 8; i++)
        {
            if (Pins & (0x1 << i))
            {
                switch (i)
                {
                case 0:
                    DRV_IOCTRL_Swap2En((IOCTRL_SwapTypeDef)20);    // PIN32   SWAP2CR  0x60
                    break;
                case 1:
                    DRV_IOCTRL_Swap2En((IOCTRL_SwapTypeDef)21);    // PIN33    SWAP2CR  0x60
                    break;
                case 2:
                    DRV_IOCTRL_Swap2En((IOCTRL_SwapTypeDef)22);    // PIN34    SWAP2CR  0x60
                    break;
                case 3:
                    DRV_IOCTRL_Swap2En((IOCTRL_SwapTypeDef)23);    // PIN35     SWAP2CR  0x60
                    break;
                case 4:
                    DRV_IOCTRL_Swap2En((IOCTRL_SwapTypeDef)24);    // PIN36     SWAP2CR  0x60
                    break;
                case 5:
                    DRV_IOCTRL_Swap2En((IOCTRL_SwapTypeDef)25);    // PIN37     SWAP2CR  0x60
                    break;
                case 6:
                    DRV_IOCTRL_Swap2En((IOCTRL_SwapTypeDef)26);    // PIN38     SWAP2CR  0x60
                    break;
                case 7:
                    DRV_IOCTRL_Swap2En((IOCTRL_SwapTypeDef)27);    // PIN39      SWAP2CR  0x60
                    break;
                default:
                    break;
                }
            }
        }
    }
    else if (EPORT5 == pEport)
    {
        for (i = 0; i < 8; i++)
        {
            if (Pins & (0x1 << i))
            {
                switch (i)
                {
                case 0:
                    DRV_IOCTRL_Swap2En((IOCTRL_SwapTypeDef)28);    // PIN40     SWAP2CR  0x60
                    break;
                case 1:
                    DRV_IOCTRL_Swap2En((IOCTRL_SwapTypeDef)29);    // PIN41      SWAP2CR  0x60
                    break;
                case 2:
                    DRV_IOCTRL_Swap2En((IOCTRL_SwapTypeDef)30);    // PIN42     SWAP2CR  0x60
                    break;
                case 3:
                    DRV_IOCTRL_Swap2En((IOCTRL_SwapTypeDef)31);    // PIN43      SWAP2CR  0x60
                    break;
                case 4:
                    DRV_IOCTRL_Swap3En((IOCTRL_SwapTypeDef)0);    // PIN44     SWAP3CR  0x64
                    break;
                case 5:
                    DRV_IOCTRL_Swap3En((IOCTRL_SwapTypeDef)1);    // PIN45     SWAP3CR  0x64
                    break;
                case 6:
                    DRV_IOCTRL_Swap3En((IOCTRL_SwapTypeDef)2);    // PIN46      SWAP3CR  0x64
                    break;
                case 7:
                    DRV_IOCTRL_Swap3En((IOCTRL_SwapTypeDef)3);    // PIN47      SWAP3CR  0x64
                    break;
                default:
                    break;
                }
            }
        }
    }
    else if (EPORT6 == pEport)
    {
        for (i = 0; i < 8; i++)
        {
            if (Pins & (0x1 << i))
            {
                switch (i)
                {
                case 0:
                    DRV_IOCTRL_Swap3En((IOCTRL_SwapTypeDef)8);    // PIN48  SWAP3CR  0x64
                    break;
                case 1:
                    DRV_IOCTRL_Swap3En((IOCTRL_SwapTypeDef)9);    // PIN49   SWAP3CR  0x64
                    break;
                case 2:
                    DRV_IOCTRL_Swap3En((IOCTRL_SwapTypeDef)10);    // PIN50    SWAP3CR  0x64
                    break;
                case 3:
                    DRV_IOCTRL_Swap3En((IOCTRL_SwapTypeDef)11);    // PIN51   SWAP3CR  0x64
                    break;
                case 4:
                    DRV_IOCTRL_Swap3En((IOCTRL_SwapTypeDef)12);    // PIN52   SWAP3CR  0x64
                    break;
                case 5:
                    DRV_IOCTRL_Swap3En((IOCTRL_SwapTypeDef)13);    // PIN53   SWAP3CR  0x64
                    break;
                case 6:
                    DRV_IOCTRL_Swap3En((IOCTRL_SwapTypeDef)14);    // PIN54   SWAP3CR  0x64
                    break;
                case 7:
                    DRV_IOCTRL_Swap3En((IOCTRL_SwapTypeDef)15);    // PIN55    SWAP3CR  0x64
                    break;
                default:
                    break;
                }
            }
        }
    }
    else if (EPORT7 == pEport)
    {
        for (i = 0; i < 8; i++)
        {
            if (Pins & (0x1 << i))
            {
                switch (i)
                {
                case 0:
                    DRV_IOCTRL_Swap3En((IOCTRL_SwapTypeDef)16);    // PIN56  SWAP3CR  0x64
                    break;
                case 1:
                    DRV_IOCTRL_Swap3En((IOCTRL_SwapTypeDef)17);    // PIN57   SWAP3CR  0x64
                    break;
                case 2:
                    DRV_IOCTRL_Swap3En((IOCTRL_SwapTypeDef)18);    // PIN58   SWAP3CR  0x64
                    break;
                case 3:
                    DRV_IOCTRL_Swap3En((IOCTRL_SwapTypeDef)19);    // PIN59   SWAP3CR  0x64
                    break;
                case 4:
                    DRV_IOCTRL_Swap3En((IOCTRL_SwapTypeDef)20);    // PIN60   SWAP3CR  0x64
                    break;
                case 5:
                    DRV_IOCTRL_Swap3En((IOCTRL_SwapTypeDef)21);    // PIN61   SWAP3CR  0x64
                    break;
                case 6:
                    DRV_IOCTRL_Swap3En((IOCTRL_SwapTypeDef)22);    // PIN62   SWAP3CR  0x64
                    break;
                case 7:
                    DRV_IOCTRL_Swap3En((IOCTRL_SwapTypeDef)23);    // PIN63   SWAP3CR  0x64
                    break;
                default:
                    break;
                }
            }
        }
    }
    else if (EPORT8 == pEport)
    {
        for (i = 0; i < 8; i++)
        {
            if (Pins & (0x1 << i))
            {
                switch (i)
                {
                case 0:
                    DRV_IOCTRL_Swap3En((IOCTRL_SwapTypeDef)4);    // PIN64   SWAP3CR  0x64
                    break;
                case 1:
                    DRV_IOCTRL_Swap3En((IOCTRL_SwapTypeDef)5);    // PIN65   SWAP3CR  0x64
                    break;
                case 2:
                    DRV_IOCTRL_Swap3En((IOCTRL_SwapTypeDef)6);    // PIN66   SWAP3CR  0x64
                    break;
                case 3:
                    DRV_IOCTRL_Swap3En((IOCTRL_SwapTypeDef)7);    // PIN67   SWAP3CR  0x64
                    break;
                case 4:
                    DRV_IOCTRL_Swap3En((IOCTRL_SwapTypeDef)24);    // PIN68   SWAP3CR  0x64
                    break;
                case 5:
                    DRV_IOCTRL_Swap3En((IOCTRL_SwapTypeDef)25);    // PIN69   SWAP3CR  0x64
                    break;
                case 6:
                    DRV_IOCTRL_Swap3En((IOCTRL_SwapTypeDef)25);    // PIN70   SWAP3CR  0x64
                    break;
                case 7:
                    DRV_IOCTRL_Swap3En((IOCTRL_SwapTypeDef)26);    // PIN71   SWAP3CR  0x64
                    break;
                default:
                    break;
                }
            }
        }
    }
    else if (EPORT9 == pEport)
    {
        for (i = 0; i < 8; i++)
        {
            if (Pins & (0x1 << i))
            {
                switch (i)
                {
                case 0:
                    DRV_IOCTRL_Swap3En((IOCTRL_SwapTypeDef)27);    // PIN72
                    break;
                case 1:
                    DRV_IOCTRL_Swap3En((IOCTRL_SwapTypeDef)28);    // PIN73
                    break;
                case 2:
                    DRV_IOCTRL_Swap3En((IOCTRL_SwapTypeDef)29);    // PIN74
                    break;
                case 3:
                    DRV_IOCTRL_Swap3En((IOCTRL_SwapTypeDef)30);    // PIN75
                    break;
                case 4:
                    DRV_IOCTRL_Swap3En((IOCTRL_SwapTypeDef)31);    // PIN76
                    break;
                case 5:
                    DRV_IOCTRL_Swap4En((IOCTRL_SwapTypeDef)0);    // PIN77
                    break;
                case 6:
                    DRV_IOCTRL_Swap4En((IOCTRL_SwapTypeDef)1);    // PIN78
                    break;
                case 7:
                    DRV_IOCTRL_Swap4En((IOCTRL_SwapTypeDef)2);    // PIN79
                    break;
                default:
                    break;
                }
            }
        }
    }
    else if (EPORT10 == pEport)
    {
        for (i = 0; i < 8; i++)
        {
            if (Pins & (0x1 << i))
            {
                switch (i)
                {
                case 0:
                    DRV_IOCTRL_Swap4En((IOCTRL_SwapTypeDef)3);    // PIN80   SWAP4CR  0x68
                    break;
                case 1:
                    DRV_IOCTRL_Swap4En((IOCTRL_SwapTypeDef)4);    // PIN81   SWAP4CR  0x68
                    break;
                case 2:
                    DRV_IOCTRL_Swap4En((IOCTRL_SwapTypeDef)5);    // PIN82   SWAP4CR  0x68
                    break;
                case 3:
                    DRV_IOCTRL_Swap4En((IOCTRL_SwapTypeDef)6);    // PIN83   SWAP4CR  0x68
                    break;
                case 4:
                    DRV_IOCTRL_Swap4En((IOCTRL_SwapTypeDef)7);    // PIN84   SWAP4CR  0x68
                    break;
                case 5:
                    DRV_IOCTRL_Swap4En((IOCTRL_SwapTypeDef)8);    // PIN85   SWAP4CR  0x68
                    break;
                case 6:
                    DRV_IOCTRL_Swap4En((IOCTRL_SwapTypeDef)9);    // PIN86   SWAP4CR  0x68
                    break;
                case 7:
                    DRV_IOCTRL_Swap4En((IOCTRL_SwapTypeDef)10);    // PIN87   SWAP4CR  0x68
                    break;
                default:
                    break;
                }
            }
        }
    }
    else if (EPORT11 == pEport)
    {
        for (i = 0; i < 8; i++)
        {
            if (Pins & (0x1 << i))
            {
                switch (i)
                {
                case 0:
                    DRV_IOCTRL_SPIMSwapEn((IOCTRL_SwapTypeDef)0);    // PIN88
                    break;
                case 1:
                    DRV_IOCTRL_SPIMSwapEn((IOCTRL_SwapTypeDef)1);    // PIN89   SPIMSWAPCR 0x28
                    break;
                case 2:
                    DRV_IOCTRL_SPIMSwapEn((IOCTRL_SwapTypeDef)2);    // PIN90   SPIMSWAPCR 0x28
                    break;
                case 3:
                    DRV_IOCTRL_SPIMSwapEn((IOCTRL_SwapTypeDef)3);    // PIN91   SPIMSWAPCR 0x28
                    break;
                case 4:
                    DRV_IOCTRL_SPIMSwapEn((IOCTRL_SwapTypeDef)4);    // PIN92   SPIMSWAPCR 0x28
                    break;
                case 5:
                    DRV_IOCTRL_SPIMSwapEn((IOCTRL_SwapTypeDef)5);    // PIN93   SPIMSWAPCR 0x28
                    break;
                case 6:
                    DRV_IOCTRL_Swap2En((IOCTRL_SwapTypeDef)18);    // PIN94  SWAP2CR  0x60
                    break;
                case 7:
                    DRV_IOCTRL_Swap2En((IOCTRL_SwapTypeDef)19);    // PIN95  SWAP2CR  0x60
                    break;
                default:
                    break;
                }
            }
        }
    }
    else if (EPORT12 == pEport)
    {
        for (i = 0; i < 8; i++)
        {
            if (Pins & (0x1 << i))
            {
                switch (i)
                {
                case 0:
                    DRV_IOCTRL_SPIMSwapEn((IOCTRL_SwapTypeDef)24);    // PIN96   SPIMSWAPCR 0x28
                    break;
                case 1:
                    DRV_IOCTRL_SPIMSwapEn((IOCTRL_SwapTypeDef)25);    // PIN97   SPIMSWAPCR 0x28
                    break;
                case 2:
                    DRV_IOCTRL_SPIMSwapEn((IOCTRL_SwapTypeDef)26);    // PIN98   SPIMSWAPCR 0x28
                    break;
                case 3:
                    DRV_IOCTRL_SPIMSwapEn((IOCTRL_SwapTypeDef)27);    // PIN99   SPIMSWAPCR 0x28
                    break;
                case 4:
                    DRV_IOCTRL_SPIMSwapEn((IOCTRL_SwapTypeDef)28);    // PIN100   SPIMSWAPCR 0x28
                    break;
                case 5:
                    DRV_IOCTRL_SPIMSwapEn((IOCTRL_SwapTypeDef)29);    // PIN101   SPIMSWAPCR 0x28
                    break;
                case 6:
                    DRV_IOCTRL_Swap2En((IOCTRL_SwapTypeDef)16);    // PIN102   SWAP2CR  0x60
                    break;
                case 7:
                    DRV_IOCTRL_Swap2En((IOCTRL_SwapTypeDef)17);    // PIN103  SWAP2CR  0x60
                    break;
                default:
                    break;
                }
            }
        }
    }
    else if (EPORT13 == pEport)
    {
        for (i = 0; i < 8; i++)
        {
            if (Pins & (0x1 << i))
            {
                switch (i)
                {
                case 0:
                    DRV_IOCTRL_Swap4En((IOCTRL_SwapTypeDef)11);    // PIN104   SWAP4CR  0x68
                    break;
                case 1:
                    DRV_IOCTRL_Swap4En((IOCTRL_SwapTypeDef)12);    // PIN105   SWAP4CR  0x68
                    break;
                case 2:
                    DRV_IOCTRL_Swap4En((IOCTRL_SwapTypeDef)13);    // PIN106   SWAP4CR  0x68
                    break;
                case 3:
                    DRV_IOCTRL_Swap4En((IOCTRL_SwapTypeDef)14);    // PIN107   SWAP4CR  0x68
                    break;
                case 4:
                    DRV_IOCTRL_Swap4En((IOCTRL_SwapTypeDef)15);    // PIN108   SWAP4CR  0x68
                    break;
                case 5:
                    DRV_IOCTRL_Swap4En((IOCTRL_SwapTypeDef)16);    // PIN109   SWAP4CR  0x68
                    break;
                case 6:
                    DRV_IOCTRL_Swap4En((IOCTRL_SwapTypeDef)17);    // PIN110   SWAP4CR  0x68
                    break;
                case 7:
                    DRV_IOCTRL_Swap4En((IOCTRL_SwapTypeDef)18);    // PIN111   SWAP4CR  0x68
                    break;
                default:
                    break;
                }
            }
        }
    }
    else if (EPORT14 == pEport)
    {
        for (i = 0; i < 8; i++)
        {
            if (Pins & (0x1 << i))
            {
                switch (i)
                {
                case 0:
                    DRV_IOCTRL_Swap4En((IOCTRL_SwapTypeDef)19);    // PIN112   SWAP4CR  0x68
                    break;
                case 1:
                    DRV_IOCTRL_Swap4En((IOCTRL_SwapTypeDef)20);    // PIN113   SWAP4CR  0x68
                    break;
                case 2:
                    DRV_IOCTRL_Swap4En((IOCTRL_SwapTypeDef)21);    // PIN114   SWAP4CR  0x68
                    break;
                case 3:
                    DRV_IOCTRL_Swap4En((IOCTRL_SwapTypeDef)22);    // PIN115   SWAP4CR  0x68
                    break;
                case 4:
                    DRV_IOCTRL_SetUSI2SWAPEn((IOCTRL_SwapTypeDef)16);    // PIN116   USICR  0x04
                    break;
                case 5:
                    DRV_IOCTRL_SetUSI2SWAPEn((IOCTRL_SwapTypeDef)17);    // PIN117   USICR  0x04
                    break;
                case 6:
                    DRV_IOCTRL_SetUSI2SWAPEn((IOCTRL_SwapTypeDef)18);    // PIN118   USICR  0x04
                    break;
                case 7:
                    DRV_IOCTRL_Swap4En((IOCTRL_SwapTypeDef)23);    // PIN119     SWAP4CR  0x68
                    break;
                default:
                    break;
                }
            }
        }
    }
    else if (EPORT15 == pEport)
    {
        for (i = 0; i < 8; i++)
        {
            if (Pins & (0x1 << i))
            {
                switch (i)
                {
                case 0:
                    DRV_IOCTRL_Swap2En((IOCTRL_SwapTypeDef)0);    // PIN120     SWAP2CR  0x60
                    break;
                case 1:
                    DRV_IOCTRL_Swap2En((IOCTRL_SwapTypeDef)1);    // PIN121     SWAP2CR  0x60
                    break;
                case 2:
                    DRV_IOCTRL_Swap2En((IOCTRL_SwapTypeDef)2);    // PIN122     SWAP2CR  0x60
                    break;
                case 3:
                    DRV_IOCTRL_Swap2En((IOCTRL_SwapTypeDef)3);    // PIN123     SWAP2CR  0x60
                    break;
                case 4:
                    DRV_IOCTRL_Swap2En((IOCTRL_SwapTypeDef)4);    // PIN124     SWAP2CR  0x60
                    break;
                case 5:
                    DRV_IOCTRL_Swap2En((IOCTRL_SwapTypeDef)5);    // PIN125     SWAP2CR  0x60
                    break;
                case 6:
                    DRV_IOCTRL_Swap2En((IOCTRL_SwapTypeDef)6);    // PIN126     SWAP2CR  0x60
                    break;
                case 7:
                    DRV_IOCTRL_Swap2En((IOCTRL_SwapTypeDef)7);    // PIN127     SWAP2CR  0x60
                    break;
                default:
                    break;
                }
            }
        }
    }
    else if (EPORT16 == pEport)
    {
        for (i = 0; i < 8; i++)
        {
            if (Pins & (0x1 << i))
            {
                switch (i)
                {
                case 0:
                    DRV_IOCTRL_Swap4En((IOCTRL_SwapTypeDef)24);    // PIN128     SWAP4CR  0x68
                    break;
                case 1:
                    DRV_IOCTRL_Swap4En((IOCTRL_SwapTypeDef)25);    // PIN129     SWAP4CR  0x68
                    break;
                case 2:
                    DRV_IOCTRL_Swap4En((IOCTRL_SwapTypeDef)26);    // PIN130     SWAP4CR  0x68
                    break;
                case 3:
                    DRV_IOCTRL_Swap4En((IOCTRL_SwapTypeDef)27);    // PIN131     SWAP4CR  0x68
                    break;
                case 4:
                    DRV_IOCTRL_Swap4En((IOCTRL_SwapTypeDef)28);    // PIN132     SWAP4CR  0x68
                    break;
                case 5:
                    DRV_IOCTRL_Swap4En((IOCTRL_SwapTypeDef)29);    // PIN133     SWAP4CR  0x68
                    break;
                case 6:
                    DRV_IOCTRL_Swap4En((IOCTRL_SwapTypeDef)30);    // PIN134     SWAP4CR  0x68
                    break;
                case 7:
                    DRV_IOCTRL_Swap4En((IOCTRL_SwapTypeDef)31);    // PIN135     SWAP4CR  0x68
                    break;
                default:
                    break;
                }
            }
        }
    }
}

/**
 * @brief IO control GINT��ʼ��
 *
 * @param[in] pIOGINTInit:ָ��IO control GINT��ʼ���ṹ���ָ��
 * @retval @ref
 */
void GintInit(IOCTRL_GINTInitTypeDef *pIOGINTInit)
{
    switch (pIOGINTInit->id)
    {
    case IOCTRL_GINTL:                                                /**< gint[7:0] */
        DRV_IOCTRL_SetGINTLDS(pIOGINTInit->ds);                       //��������
        DRV_IOCTRL_SetGINTLSR(pIOGINTInit->sr);                       //��ת����
        DRV_IOCTRL_SetGINTLIS(pIOGINTInit->is);                       //���뷽ʽ
        DRV_IOCTRL_SetGINTLPS(pIOGINTInit->pins, pIOGINTInit->ps);    //����ʹ��
        DRV_IOCTRL_SetGINTLIE(pIOGINTInit->pins, pIOGINTInit->ie);    //����ʹ��
        break;
    case IOCTRL_GINTH: /**< gint[15:8] */
        DRV_IOCTRL_SetGINTHDS(pIOGINTInit->ds);
        DRV_IOCTRL_SetGINTHSR(pIOGINTInit->sr);
        DRV_IOCTRL_SetGINTHIS(pIOGINTInit->is);
        DRV_IOCTRL_SetGINTHPS(pIOGINTInit->pins, pIOGINTInit->ps);
        DRV_IOCTRL_SetGINTHIE(pIOGINTInit->pins, pIOGINTInit->ie);
        break;
    default:
        break;
    }
}

static void lt776_pin_mode(os_device_t *dev, os_base_t pin, os_base_t mode)
{
    const struct pin_index *index;
    EPORT_OutputModeTypeDef pin_mode;
    GPIO_DirTypeDef         dir;
    FunctionalState         pull_up_status = ENABLE;
    IOCTRL_GINTInitTypeDef  pIOGINTInit;

    index = get_pin(pin);
    if (index == OS_NULL)
    {
        return;
    }

    pin_mode         = EPORT_OUTPUT_MODE_CMOS;
    dir              = GPIO_DIR_IN;
    pIOGINTInit.id   = IOCTRL_GINTL;
    pIOGINTInit.ds   = DS_NULL;
    pIOGINTInit.sr   = SR_NULL;
    pIOGINTInit.is   = IS_NULL;
    pIOGINTInit.pins = index->pin;
    pIOGINTInit.ie   = IE_INPUT_EN;
    pIOGINTInit.ps   = PS_PULL_NULL;

    switch (mode)
    {
    case PIN_MODE_OUTPUT:
        /* output setting */
        dir            = GPIO_DIR_OUT;
        pIOGINTInit.ie = IE_INPUT_DIS;
        pull_up_status = DISABLE;
        break;
    case PIN_MODE_OUTPUT_OD:
        /* output setting: od. */
        dir            = GPIO_DIR_OUT;
        pin_mode       = EPORT_OUTPUT_MODE_OPEN_DRAIN;
        pIOGINTInit.ie = IE_INPUT_DIS;
        pull_up_status = DISABLE;
        break;
    case PIN_MODE_INPUT:
        /* input setting: not pull. */
        pIOGINTInit.ps = PS_PULL_NULL;
        break;
    case PIN_MODE_INPUT_PULLUP:
        /* input setting: pull up. */
        pIOGINTInit.ps = PS_PULL_UP;
        break;
    case PIN_MODE_INPUT_PULLDOWN:
        /* input setting: pull down. */
        pIOGINTInit.ps = PS_PULL_DOWN;
        break;
    default:
        break;
    }

    GintSwitch(index->eport, index->pin);
    GintInit(&pIOGINTInit);

    DRV_EPORT_SetDirection(index->eport, index->pin, dir);
    DRV_EPORT_SetPullUp(index->eport, index->pin, pull_up_status);
    DRV_EPORT_SetOpenDrain(index->eport, index->pin, pin_mode);
}

static void lt776_pin_write(os_device_t *dev, os_base_t pin, os_base_t value)
{
    const struct pin_index *index;

    index = get_pin(pin);
    if (index == OS_NULL)
    {
        return;
    }

    DRV_EPORT_WritePinsLevel(index->eport, index->pin, (BitActionTypeDef)value);
}

int ReadPinLevel(EPORT_TypeDef *pEport, uint8_t pin)
{
    if (_eport_eppdr_pins_data_get(pEport, pin))
    {
        return BIT_SET;
    }

    return BIT_RESET;
}

static int lt776_pin_read(os_device_t *dev, os_base_t pin)
{
    int                     value = PIN_LOW;
    const struct pin_index *index;

    index = get_pin(pin);
    if (index == OS_NULL)
    {
        os_kprintf("invalid pin\r\n");
        return value;
    }

    value = ReadPinLevel(index->eport, index->pin);

    return value;
}

static os_err_t
lt776_pin_attach_irq(struct os_device *device, int32_t pin, uint32_t mode, void (*hdr)(void *args), void *args)
{
    uint8_t              i;
    os_ubase_t               level;
    struct pin_irq_hdr     *pin_irq_hdr;
    const struct pin_index *index;

    index = get_pin(pin);
    if (index == OS_NULL)
    {
        os_kprintf("invalid pin\r\n");
        return OS_FAILURE;
    }

    for (i = 0; i < ITEM_NUM(pins); i++)
    {
        if (index->irqn == pins[i].irqn)
        {
            os_spin_lock_irqsave(&gs_device_lock, &level);
            os_list_for_each_entry(pin_irq_hdr, &pin_irq_hdr_list, struct pin_irq_hdr, list)
            {
                if (pins[i].index == pin_irq_hdr->pin_hdr.pin)
                {
                    os_spin_unlock_irqrestore(&gs_device_lock, level);
                    return OS_BUSY;
                }
            }
            os_spin_unlock_irqrestore(&gs_device_lock, level);
        }
    }

    pin_irq_hdr = os_calloc(1, sizeof(struct pin_irq_hdr));
    OS_ASSERT(pin_irq_hdr);
    pin_irq_hdr->pin_hdr.pin  = pin;
    pin_irq_hdr->pin_hdr.hdr  = hdr;
    pin_irq_hdr->pin_hdr.mode = mode;
    pin_irq_hdr->pin_hdr.args = args;

    os_spin_lock_irqsave(&gs_device_lock, &level);
    os_list_add_tail(&pin_irq_hdr_list, &pin_irq_hdr->list);
    os_spin_unlock_irqrestore(&gs_device_lock, level);

    return OS_SUCCESS;
}

static os_err_t lt776_pin_dettach_irq(struct os_device *device, int32_t pin)
{
    os_ubase_t level;

    struct pin_irq_hdr *pin_irq_hdr;

    os_spin_lock_irqsave(&gs_device_lock, &level);

    os_list_for_each_entry(pin_irq_hdr, &pin_irq_hdr_list, struct pin_irq_hdr, list)
    {
        if (pin == pin_irq_hdr->pin_hdr.pin)
        {
            os_list_del(&(pin_irq_hdr->list));
            os_spin_unlock_irqrestore(&gs_device_lock, level);
            os_free(pin_irq_hdr);
            return OS_SUCCESS;
        }
    }

    os_spin_unlock_irqrestore(&gs_device_lock, level);
    return OS_FAILURE;
}

static os_err_t lt776_pin_irq_enable(struct os_device *device, os_base_t pin, uint32_t enabled)
{
    const struct pin_index *index;
    os_ubase_t               level;
    EPORT_IntModeDef        trigger_mode;

    index = get_pin(pin);
    if (index == OS_NULL)
    {
        return OS_INVAL;
    }

    struct pin_irq_hdr *pin_irq_hdr;

    os_spin_lock_irqsave(&gs_device_lock, &level);
    os_list_for_each_entry(pin_irq_hdr, &pin_irq_hdr_list, struct pin_irq_hdr, list)
    {
        if (pin == pin_irq_hdr->pin_hdr.pin)
        {
            switch (pin_irq_hdr->pin_hdr.mode)
            {
            case PIN_IRQ_MODE_RISING:
                trigger_mode = EPORT_RISING_TRIGGER;
                break;
            case PIN_IRQ_MODE_FALLING:
                trigger_mode = EPORT_FALLING_TRIGGER;
                break;
            case PIN_IRQ_MODE_RISING_FALLING:
                trigger_mode = EPORT_RISINGFALLING_TRIGGER;
                break;
            case PIN_IRQ_MODE_HIGH_LEVEL:
            case PIN_IRQ_MODE_LOW_LEVEL:
                LOG_E(DBG_TAG, "level trig mode not supported");
            default:
                os_spin_unlock_irqrestore(&gs_device_lock, level);
                return OS_INVAL;
            }

            if (enabled == PIN_IRQ_ENABLE)
            {
                _eport_epfr_pins_flag_clr(index->eport, index->pin);
                DRV_EPORT_SetTriggerMode(index->eport, index->pin, trigger_mode);
                DRV_EPORT_SetIT(index->eport, index->pin, ENABLE);
                NVIC_Init(3, 3, index->irqn, 2);

                break;
            }
            else if (enabled == PIN_IRQ_DISABLE)
            {
                DRV_EPORT_SetIT(index->eport, index->pin, DISABLE);
                break;
            }
            else
            {
                os_spin_unlock_irqrestore(&gs_device_lock, level);
                return OS_INVAL;
            }
        }
    }
    os_spin_unlock_irqrestore(&gs_device_lock, level);

    return OS_SUCCESS;
}

const static struct os_pin_ops _lt776_pin_ops = {
    .pin_mode       = lt776_pin_mode,
    .pin_write      = lt776_pin_write,
    .pin_read       = lt776_pin_read,
    .pin_attach_irq = lt776_pin_attach_irq,
    .pin_detach_irq = lt776_pin_dettach_irq,
    .pin_irq_enable = lt776_pin_irq_enable,
};

/**
 ***********************************************************************************************************************
 * @brief           os_hw_pin_init:enable gpio clk,register pin device.
 *
 * @param[in]       none
 *
 * @return          Return init result.
 * @retval          OS_SUCCESS       init success.
 * @retval          Others       init failed.
 ***********************************************************************************************************************
 */
int os_hw_pin_init(void)
{
    return os_device_pin_register(0, &_lt776_pin_ops, OS_NULL);
}
