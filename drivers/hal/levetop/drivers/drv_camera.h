/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_camera.h
 *
 * @brief       This file provides functions declaration for lt776 camera driver.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __DRV_CAMERA_H__
#define __DRV_CAMERA_H__

#include <board.h>

typedef struct room_rate 
{
    uint8_t numerator;
    uint8_t denominator;
} room_rate_t;

static room_rate_t room_rate_table[] = {
    {.numerator = 1, .denominator = 1},
    {.numerator = 1, .denominator = 2},
    {.numerator = 1, .denominator = 3},
    {.numerator = 1, .denominator = 4},
    {.numerator = 1, .denominator = 5},
    {.numerator = 1, .denominator = 6},
    {.numerator = 1, .denominator = 7},
    {.numerator = 2, .denominator = 3},
    {.numerator = 2, .denominator = 5},
    {.numerator = 2, .denominator = 7},
    {.numerator = 3, .denominator = 5},
    {.numerator = 3, .denominator = 7},
    {.numerator = 4, .denominator = 7},
};

/* os_device_control() cmd param */
typedef enum
{
    OS_CAMERA_CMD_RESOLUTION     = 0x00,
    OS_CAMERA_CMD_TRANSFORMATION = 0x01,
} OS_CAMERA_CMD_E;

/* OS_CAMERA_CMD_TRANSFORMATION param */
typedef enum
{
    OS_CAMERA_DISP_ROTATE_180      = 0,
    OS_CAMERA_DISP_FLIP_UP_DOWN    = 1,
    OS_CAMERA_DISP_FLIP_LEFT_RIGHT = 2,
    OS_CAMERA_DISP_NORMAL          = 3,
} OS_CAMERA_TRANSFORMATION_TYPE_E;

#endif
