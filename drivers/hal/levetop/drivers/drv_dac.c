/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_dac.c
 *
 * @brief       This file implements dac driver for lt776.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <board.h>
#include <dac_drv.h>
#include <sys.h>

#define DBG_TAG "drv.dac"
#include <dlog.h>

struct lt776_dac
{
    os_dac_device_t dac;
    DAC_TypeDef    *inst;
};

void InitDacTimer(unsigned int rate)
{
    PIT2->PCSR &= (~PIT_PCSR_EN);
    PIT2->PCSR = PIT_PCSR_PRESCALER_1 | PIT_PCSR_OVW | PIT_PCSR_PIE | PIT_PCSR_RLD | PIT_PCSR_PDBG;
#if (FREQUENCY_MHZ == 500)
    PIT2->PMR = (g_ips_clk / 1250000 * (1250000 / (float)rate));
#else
    pPit2->PMR = (g_ips_clk / 1000000 * (1000000 / (float)rate));
#endif

    PIT2->PCSR |= PIT_PCSR_EN;
}

static os_err_t lt776_dac_enabled(os_dac_device_t *dac, uint32_t channel, os_bool_t enabled)
{
    OS_ASSERT(dac != OS_NULL);

    if (enabled)
    {
        DAC_InitTypeDef hdac;
        DRV_DAC_Deinit();
        hdac.DataFomat      = RIGHTALIGNED_12BITS;
        hdac.ExtTriggerMode = DET_ON_RISING;
        hdac.TriggerMode    = TRIGGER_PIT;
        hdac.boDmaEnable    = DISABLE;
        DRV_DAC_Init(hdac);

        InitDacTimer(22050);
    }
    else
    {
        PIT2->PCSR &= (~PIT_PCSR_EN);
        DRV_DAC_Deinit();
    }

    return OS_SUCCESS;
}

static os_err_t lt776_dac_write(os_dac_device_t *dac, uint32_t channel, uint32_t value)
{
    OS_ASSERT(dac != OS_NULL);

    uint8_t       i;
    const uint8_t fifo_size = 2;

    /* set dac val */
    for (i = 0; i < fifo_size; i++)
    {
        DAC->DAC_DR = value;
    }

    return OS_SUCCESS;
}

static const struct os_dac_ops lt776_dac_ops = {
    .enabled = lt776_dac_enabled,
    .write   = lt776_dac_write,
};

static int lt776_dac_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    struct lt776_dac *dac;

    dac = os_calloc(1, sizeof(struct lt776_dac));

    OS_ASSERT(dac);

    dac->inst = (DAC_TypeDef *)dev->info;

    dac->dac.max_value = (1UL << 12) - 1; /* 12bit */
    dac->dac.ref_low   = 0;               /* ref 0 - 3.3v */
    dac->dac.ref_hight = 3300;
    dac->dac.ops       = &lt776_dac_ops;

    if (os_dac_register(&dac->dac, dev->name, OS_NULL) != OS_SUCCESS)
    {
        LOG_E(DBG_TAG, "dac device register failed.");
        return OS_FAILURE;
    }
    LOG_D(DBG_TAG, "dac device register success.");

    return OS_SUCCESS;
}

OS_DRIVER_INFO lt776_dac_driver = {
    .name  = "DAC_TypeDef",
    .probe = lt776_dac_probe,
};

OS_DRIVER_DEFINE(lt776_dac_driver, OS_INIT_LEVEL_PRE_DEVICE, OS_INIT_SUBLEVEL_LOW);

