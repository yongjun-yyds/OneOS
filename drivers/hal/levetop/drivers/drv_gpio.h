/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_gpio.h
 *
 * @brief       This file provides struct/macro declaration and functions declaration for lt776 gpio driver.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __DRV_GPIO_H__
#define __DRV_GPIO_H__

#include "board.h"
#include "lt776_reg.h"
#include "lt776.h"

#define PIN(index) (os_base_t)(index)

/* LT766 GPIO driver */
struct pin_index
{
    uint8_t     index;
    EPORT_TypeDef *eport;
    uint8_t     pin;
    IRQn_Type      irqn;
};

const struct pin_index *get_pin(uint8_t pin);
int                     os_hw_pin_init(void);

#endif /* __DRV_GPIO_H__ */
