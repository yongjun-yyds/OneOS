/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_can.c
 *
 * @brief       This file implements can driver for lt776.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <drv_can.h>

#define DRV_EXT_LVL DBG_EXT_INFO
#define DRV_EXT_TAG "drv.can"
#include <drv_log.h>

static os_list_node_t gs_lt776_can_list = OS_LIST_INIT(gs_lt776_can_list);
static CAN_HandleTypeDef hcan;

static struct lt776_can *find_lt776_can(CAN_TypeDef *hcan)
{
    struct lt776_can *lt_can;

    os_list_for_each_entry(lt_can, &gs_lt776_can_list, struct lt776_can, list)
    {
        if (lt_can->hcan == hcan)
        {
            return lt_can;
        }
    }

    return OS_NULL;
}

/**
 * @brief  Initializes the CAN peripheral according to the specified
 *         parameters in the CAN_InitStruct.
 * @param  hcan: pointer to a CAN_HandleTypeDef structure that contains
 *         the configuration information for the specified CAN.
 * @retval HAL status
 */
static os_err_t CAN_Init(CAN_HandleTypeDef *hcan)
{
    /* Check CAN handle */
    if (hcan == NULL)
    {
        return ERROR;
    }

    /* Initialize the CAN state*/
    hcan->State = CAN_STATE_BUSY;

    if (DRV_CAN_Init(hcan->Instance, &hcan->Init) != OK)
    {
        return ERROR;
    }

    if (hcan->Instance != CAN1)
    {
        return ERROR;
    }
    if ((hcan->Init.TransmitInterrupt == ENABLE) || (hcan->Init.ReceiveInterrupt == ENABLE))
    {
        NVIC_Init(3, 3, CAN1_BUF32_63_IRQn, 3);
        NVIC_Init(3, 3, CAN1_BUF16_31_IRQn, 3);
        NVIC_Init(3, 3, CAN1_BUF12_15_IRQn, 3);
        NVIC_Init(3, 3, CAN1_BUF08_11_IRQn, 3);
        NVIC_Init(3, 3, CAN1_BUF04_07_IRQn, 3);
        NVIC_Init(3, 3, CAN1_BUF00_03_IRQn, 3);
        NVIC_Init(3, 3, CAN1_ERR_IRQn,      3);
    }
    hcan->ErrorCode = CAN_ERROR_NONE;
    hcan->State = CAN_STATE_READY;

    return (OK);
}

static os_err_t CAN_ReceiveIT(CAN_HandleTypeDef *hcan, uint32_t Timeout)
{
    static os_err_t st;

    if (hcan->State != CAN_STATE_READY)
    {
        return (DRV_CAN_ERROR);
    }

    hcan->State = CAN_STATE_BUSY_RX;
    hcan->ErrorCode = CAN_ERROR_NONE;

    if (hcan->Init.FEN == ENABLE)
    {
        st = DRV_CAN_FIFO_ReceiveBytesIT(hcan->Instance, hcan->pRxMsg, hcan->RxSize, Timeout);
    }
    else
    {
        st = DRV_CAN_ReceiveBytesIT(hcan->Instance, hcan->pRxMsg, hcan->RxSize, Timeout);
    }

    if (st != DRV_CAN_OK)
    {
        return st;
    }

    hcan->State = CAN_STATE_READY;

    return (DRV_CAN_OK);
}


void CANERR_IRQHandler(void)
{
    if(_can_get_err_int_flag(CAN1) == 0x02)
    {
        os_hw_can_isr_txdone((struct os_can_device *)find_lt776_can(CAN1), OS_CAN_EVENT_TX_FAIL);
    }

    NVIC_DisableIRQ(CAN1_ERR_IRQn);
    _can_clr_err_int_flag(CAN1);
}

//  SEND IRQ
void CAN32_63_IRQHandler(void)
{
    volatile uint32_t status;
    volatile uint32_t im_flg;
    volatile uint32_t if_flag;
    volatile uint8_t  i;
 
    status = CAN1->CAN_IF2R;

    im_flg = CAN1->CAN_IM2R;
    for (i = 0; i <= 31; i++)
    {
        if (im_flg & (1 << i))
        {
            if (status & (1 << i))
                if_flag |= (1 << i);
        }
    }

    CAN1->CAN_IF2R = if_flag;

    os_hw_can_isr_txdone((struct os_can_device *)find_lt776_can(CAN1), OS_CAN_EVENT_TX_DONE);
}

//  RECV IRQ
void CAN4_7_IRQHandler(void)
{
    volatile uint32_t status1;
    volatile uint32_t im_flg;
    volatile uint32_t if_flag;
    volatile uint8_t i;
    uint8_t canDataBuf[8];

    struct lt776_can *lt_can = find_lt776_can(CAN1);

    if (lt_can->can_rx_msg == OS_NULL)
        return;

    status1 = CAN1->CAN_IF1R;

    im_flg = CAN1->CAN_IM1R;

    for (i = 4; i <= 7; i++)
    {
        if (im_flg & (1 << i))
        {
            if (status1 & (1 << i))
            {
                if_flag |= (1 << i);
                if (i == 5)
                {
                    lt_can->can_rx_msg->ide = (DRV_CAN_IDETypeDef)((CAN1->CAN_MB[0].MB_CS >> 21) & 0x1);
                    if (lt_can->can_rx_msg->ide == DRV_CAN_ID_STD)
                    {
                        lt_can->can_rx_msg->id = CAN1->CAN_MB[0].MB_ID >> 18;
                    }
                    else
                    {
                        lt_can->can_rx_msg->id = CAN1->CAN_MB[0].MB_ID;
                    }

                    lt_can->can_rx_msg->len = (CAN1->CAN_MB[0].MB_CS >> 16) & 0x0f;
                    /*read data in the MB*/
                    for (uint8_t ucIndex = 0; ucIndex < lt_can->can_rx_msg->len; ucIndex++)
                    {
                        canDataBuf[ucIndex] = CAN1->CAN_MB[0].MB_DATA[ucIndex];
                    }
                    if (lt_can->can_rx_msg->len == 8)
                    {
                        uint8_t ii;
                        for (ii = 0; ii < 4; ii++)
                        {
                            lt_can->can_rx_msg->data[ii] = canDataBuf[3 - ii];
                        }
                        for (ii = 4; ii < 8; ii++)
                        {
                            lt_can->can_rx_msg->data[ii] = canDataBuf[11 - ii];
                        }
                    }
                }
            }
        }
    }
    CAN1->CAN_IF1R = (if_flag & 0x000000f0);
    
    lt_can->can_rx_msg = OS_NULL;
    os_hw_can_isr_rxdone((struct os_can_device *)lt_can);
}

static os_err_t lt776_can_config(struct os_can_device *can, struct can_configure *cfg)
{
    CanTxMsgTypeDef tx_msg = {0};
    CanRxMsgTypeDef rx_msg = {0};

    if (cfg->baud_rate > 1000000)
    {
        os_kprintf("Bit rate is out of range!!!\r\n");
        return OS_FAILURE;
    }

    DRV_CAN_DeInit(CAN1);
    hcan.RxSize   = 8;
    hcan.Instance = CAN1;
    hcan.pRxMsg   = &rx_msg;
    hcan.pTxMsg   = &tx_msg;
    hcan.Init.BitRate = cfg->baud_rate;
    hcan.Init.IPSFreq = g_ips_clk;
    hcan.Init.FEN               = ENABLE;
    hcan.Init.TransmitInterrupt = ENABLE;
    hcan.Init.ReceiveInterrupt  = ENABLE;
    CAN_Init(&hcan);

    CAN_ReceiveIT(&hcan, 0);
    _bit_set(hcan.Instance->CAN_CR, CAN_CR_ERR_MSK);

    switch (cfg->mode)
    {
    case OS_CAN_MODE_NORMAL:
        _can_select_normal_mode(CAN1);
        _bit_set(hcan.Instance->CAN_MCR, CAN_MCR_SRX_DIS);
        break;
    case OS_CAN_MODE_LISEN:
        _can_en_listen_only_mode(CAN1);
        break;
    case OS_CAN_MODE_LOOPBACK:
        _can_select_loop_back_mode(CAN1);
        break;
    case OS_CAN_MODE_LOOPBACKANLISEN:
        os_kprintf("not support loopbackanlisen\r\n");
        break;
    }

    return OS_SUCCESS;
}

static os_err_t lt776_can_control(struct os_can_device *can, int cmd, void *arg)
{
    uint32_t argval;
    struct lt776_can *lt_can;

    OS_ASSERT(can != OS_NULL);
    lt_can = (struct lt776_can *)can;

    switch (cmd)
    {
    case OS_CAN_CMD_SET_FILTER:
        break;
    case OS_CAN_CMD_SET_MODE:
        argval = (uint32_t)arg;
        if (argval != OS_CAN_MODE_NORMAL &&
            argval != OS_CAN_MODE_LISEN &&
            argval != OS_CAN_MODE_LOOPBACK &&
            argval != OS_CAN_MODE_LOOPBACKANLISEN)
        {
            return OS_FAILURE;
        }
        if (argval != lt_can->can.config.mode)
        {
            can->config.mode = argval;
            return lt776_can_config(can, &can->config);
        }
        break;
    case OS_CAN_CMD_SET_BAUD:
        argval = (uint32_t)arg;
        if (argval != can->config.baud_rate)
        {
            can->config.baud_rate = argval;
            return lt776_can_config(can, &can->config);
        }
        break;
    case OS_CAN_CMD_SET_PRIV:
        argval = (uint32_t)arg;
        if (argval != OS_CAN_MODE_PRIV && argval != OS_CAN_MODE_NOPRIV)
        {
            return OS_FAILURE;
        }
        if (argval != can->config.privmode)
        {
            can->config.privmode = argval;
            return lt776_can_config(can, &can->config);
        }
        break;
    case OS_CAN_CMD_GET_STATUS:
        break;
    }

    return OS_SUCCESS;
}

static os_err_t CAN_TransmitIT(CAN_HandleTypeDef *hcan, CanTxMsgTypeDef *pTxmsg, uint32_t Timeout) 
{
    if (hcan->State != CAN_STATE_READY)
    {
        return (DRV_CAN_ERROR);
    }

    hcan->State     = CAN_STATE_BUSY_TX;
    hcan->ErrorCode = CAN_ERROR_NONE;

    if (DRV_CAN_TransmitBytesIT(hcan->Instance, pTxmsg, Timeout) != DRV_CAN_OK)
    {
        return DRV_CAN_ERROR;
    }

    hcan->State = CAN_STATE_READY;

    return (DRV_CAN_OK);
}

static int lt776_can_start_send(struct os_can_device *can, const struct os_can_msg *pmsg)
{
    OS_ASSERT(can);
    uint32_t i, j, k = 0;
    CanTxMsgTypeDef tx_msg;

    if ((CAN1->CAN_CR & CAN_CR_LOM) == CAN_CR_LOM)
    {
        return OS_EPERM;
    }

    if (pmsg->ide == OS_CAN_EXTID)
    {
        tx_msg.IDE   = DRV_CAN_ID_EXT;
        tx_msg.ExtId = pmsg->id;
    }
    else if (pmsg->ide == OS_CAN_STDID)
    {
        tx_msg.IDE   = DRV_CAN_ID_STD;
        tx_msg.StdId = pmsg->id;
    }

    if (pmsg->rtr == OS_CAN_RTR)
    {
        tx_msg.RTR = DRV_CAN_RTR_REMOTE;
    }
    else if (pmsg->rtr == OS_CAN_DTR)
    {
        tx_msg.RTR = DRV_CAN_RTR_DATA;
    }

    NVIC_EnableIRQ(CAN1_BUF32_63_IRQn);
    NVIC_EnableIRQ(CAN1_ERR_IRQn);

    k = 0;
    for (j = 0; j < pmsg->len / 8; j++, k++)
    {
        tx_msg.DLC = 8;

        for (i = 0; i < 4; i++)
            tx_msg.Data[3 - i]  = pmsg->data[8 * k + i];
        for (i = 4; i < 8; i++)
            tx_msg.Data[11 - i] = pmsg->data[8 * k + i];

        CAN_TransmitIT(&hcan, &tx_msg, 0);
    }

    if (pmsg->len % 8)
    {
        tx_msg.DLC = 8;
        for (i = 0; i < 8; i++)
            tx_msg.Data[i] = 0;

        for (i = 0; i < (pmsg->len % 8); i++)
            if (i < 4)
                tx_msg.Data[3 - i]  = pmsg->data[8 * k + i];
            else
                tx_msg.Data[11 - i] = pmsg->data[8 * k + i];

        CAN_TransmitIT(&hcan, &tx_msg, 0);
    }

    return 0;
}

static int lt776_can_stop_send(struct os_can_device *can)
{
    NVIC_DisableIRQ(CAN1_BUF32_63_IRQn);
    NVIC_DisableIRQ(CAN1_ERR_IRQn);

    return 0;
}

static int lt776_can_start_recv(struct os_can_device *can, struct os_can_msg *msg)
{
    OS_ASSERT(can);
    struct lt776_can *lt_can = (struct lt776_can *)can;
    lt_can->can_rx_msg = msg;

    NVIC_EnableIRQ(CAN1_BUF04_07_IRQn);
    
    return 0;
}

static int lt776_can_stop_recv(struct os_can_device *can)
{
    NVIC_DisableIRQ(CAN1_BUF04_07_IRQn);

    return 0;
}

static int lt776_can_recv_state(struct os_can_device *can)
{
    return 0;
}

static const struct os_can_ops lt776_can_ops = {
    .configure  = lt776_can_config,
    .control    = lt776_can_control,
    .start_send = lt776_can_start_send,
    .stop_send  = lt776_can_stop_send,
    .start_recv = lt776_can_start_recv,
    .stop_recv  = lt776_can_stop_recv,
    .recv_state = lt776_can_recv_state,
};

static int lt776_can_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    struct can_configure config = CANDEFAULTCONFIG;

    os_ubase_t level;

    struct lt776_can *lt_can = os_calloc(1, sizeof(struct lt776_can));

    OS_ASSERT(lt_can);

    lt_can->hcan = CAN1;

    config.privmode = OS_CAN_MODE_NOPRIV;
    config.ticks = 50;
    lt_can->can.config = config;

    os_spin_lock_irqsave(&gs_device_lock, &level);
    os_list_add_tail(&gs_lt776_can_list, &lt_can->list);
    os_spin_unlock_irqrestore(&gs_device_lock, level);

    os_hw_can_register(&lt_can->can, dev->name, &lt776_can_ops, lt_can);

    return OS_SUCCESS;
}

OS_DRIVER_INFO lt776_can_driver = {
    .name = "CAN_TypeDef",
    .probe = lt776_can_probe,
};

OS_DRIVER_DEFINE(lt776_can_driver, OS_INIT_LEVEL_PRE_DEVICE, OS_INIT_SUBLEVEL_LOW);

