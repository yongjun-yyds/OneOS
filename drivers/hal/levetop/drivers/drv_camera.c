/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\ you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an \"AS IS\" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * \@file        drv_camera.c
 *
 * \@brief       This file implements camera driver for lt776.
 *
 * \@revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <sys.h>
#include <dcmi_drv.h>
#include <eflash_drv.h>
#include <ioctrl_drv.h>
#include <drv_graphic.h>
#include <drv_camera.h>
#include <drv_i2c.h>

#define IMAGE_W 640
#define IMAGE_H 480
#define PIN_RSTN PIN(90)
#define PIN_PWDN PIN(91)

#define THREAD_PRIORITY_CAMERA   20
#define THREAD_STACK_SIZE_CAMERA 256

static os_task_id camera_task = OS_NULL;
static os_semaphore_id camera_sem  = OS_NULL;
static DCMI_HandleTypeDef DCMI_Handle;

static OS_CAMERA_TRANSFORMATION_TYPE_E g_transforme_type = OS_CAMERA_DISP_NORMAL;
static room_rate_t g_room_rate = 
{
    .numerator   = 2,
    .denominator = 3,
};

const unsigned char gc0308_vga[][2] =
{
    {0xfe, 0x80},
    {0xfe, 0x00}, // set page0

    {0xd2, 0x10}, // close AEC
    {0x22, 0x55}, // close AWB

    {0x5a, 0x56},
    {0x5b, 0x40},
    {0x5c, 0x4a},

    {0x22, 0x57}, // Open AWB

    //{0x01 , 0x0A},//Horizontal blanking, unit pixel clock
    {0x01, 0xff},
    {0x02, 0x0C}, // Vertical blanking,
    //{0x02 , 0x07},
    {0x0f, 0x01},

    {0x03, 0x01}, // 0x01
    {0x04, 0x2c}, // 0x2c

    {0xe2, 0x00}, // anti-flicker step [11:8]
    {0xe3, 0x64}, // anti-flicker step [7:0]

    {0xe4, 0x02}, // exp level 1  16.67fps
    {0xe5, 0x58},
    {0xe6, 0x02}, // exp level 2  12.5fps //03
    {0xe7, 0x58}, // 20
    {0xe8, 0x02}, // exp level 3  8.33fps //04
    {0xe9, 0x58}, // b0
    {0xea, 0x09}, // exp level 4  4.00fps
    {0xeb, 0xc4},

    {0x05, 0x00},
    {0x06, 0x00},
    {0x07, 0x00},
    {0x08, 0x00},

    {0x09, 0x01}, // 0x1e8=488
    {0x0a, 0xe8},

    {0x0b, 0x02}, // 0x288=648
    {0x0c, 0x88},

    {0x0d, 0x02},
    {0x0e, 0x02},
    {0x10, 0x22},
    {0x11, 0xfd},
    {0x12, 0x2a},
    {0x13, 0x00},
    //{0x14 , 0x10},
    {0x15, 0x0a},
    {0x16, 0x05},
    {0x17, 0x01},
    {0x18, 0x44},
    {0x19, 0x44},
    {0x1a, 0x1e},
    {0x1b, 0x00},
    {0x1c, 0xc1},
    {0x1d, 0x08},
    {0x1e, 0x60},
    {0x1f, 0x16},

    {0x20, 0xff},
    {0x21, 0xf8},
    {0x22, 0x57},

    {0x24, 0xa6}, // rgb565
    //{0x24 , 0xb2},
    //{0x24 , 0xa1},

    {0x25, 0x0f},

    // output sync_mode
    {0x26, 0x02},
    {0x2f, 0x01},
    {0x30, 0xf7},
    {0x31, 0x50},
    {0x32, 0x00},
    {0x39, 0x04},
    {0x3a, 0x18},
    {0x3b, 0x20},
    {0x3c, 0x00},
    {0x3d, 0x00},
    {0x3e, 0x00},
    {0x3f, 0x00},
    {0x50, 0x10},
    {0x53, 0x82},
    {0x54, 0x80},
    {0x55, 0x80},
    {0x56, 0x82},
    {0x8b, 0x40},
    {0x8c, 0x40},
    {0x8d, 0x40},
    {0x8e, 0x2e},
    {0x8f, 0x2e},
    {0x90, 0x2e},
    {0x91, 0x3c},
    {0x92, 0x50},
    {0x5d, 0x12},
    {0x5e, 0x1a},
    {0x5f, 0x24},
    {0x60, 0x07},
    {0x61, 0x15},
    {0x62, 0x08},
    {0x64, 0x03},
    {0x66, 0xe8},
    {0x67, 0x86},
    {0x68, 0xa2},
    {0x69, 0x18},
    {0x6a, 0x0f},
    {0x6b, 0x00},
    {0x6c, 0x5f},
    {0x6d, 0x8f},
    {0x6e, 0x55},
    {0x6f, 0x38},
    {0x70, 0x15},
    {0x71, 0x33},
    {0x72, 0xdc},
    {0x73, 0x80},
    {0x74, 0x02},
    {0x75, 0x3f},
    {0x76, 0x02},
    {0x77, 0x36},
    {0x78, 0x88},
    {0x79, 0x81},
    {0x7a, 0x81},
    {0x7b, 0x22},
    {0x7c, 0xff},
    {0x93, 0x48},
    {0x94, 0x00},
    {0x95, 0x05},
    {0x96, 0xe8},
    {0x97, 0x40},
    {0x98, 0xf0},
    {0xb1, 0x38},
    {0xb2, 0x38},
    {0xbd, 0x38},
    {0xbe, 0x36},
    {0xd0, 0xc9},
    {0xd1, 0x10},
    //{0xd2 , 0x90},
    {0xd3, 0x80},
    {0xd5, 0xf2},
    {0xd6, 0x16},
    {0xdb, 0x92},
    {0xdc, 0xa5},
    {0xdf, 0x23},
    {0xd9, 0x00},
    {0xda, 0x00},
    {0xe0, 0x09},
    {0xec, 0x20},
    {0xed, 0x04},
    {0xee, 0xa0},
    {0xef, 0x40},
    {0x80, 0x03},
    {0x80, 0x03},
    // gamma
    {0x9F, 0x10},
    {0xA0, 0x20},
    {0xA1, 0x38},
    {0xA2, 0x4E},
    {0xA3, 0x63},
    {0xA4, 0x76},
    {0xA5, 0x87},
    {0xA6, 0xA2},
    {0xA7, 0xB8},
    {0xA8, 0xCA},
    {0xA9, 0xD8},
    {0xAA, 0xE3},
    {0xAB, 0xEB},
    {0xAC, 0xF0},
    {0xAD, 0xF8},
    {0xAE, 0xFD},
    {0xAF, 0xFF},
    // gamma end
    {0xc0, 0x00},
    {0xc1, 0x10},
    {0xc2, 0x1C},
    {0xc3, 0x30},
    {0xc4, 0x43},
    {0xc5, 0x54},
    {0xc6, 0x65},
    {0xc7, 0x75},
    {0xc8, 0x93},
    {0xc9, 0xB0},
    {0xca, 0xCB},
    {0xcb, 0xE6},
    {0xcc, 0xFF},
    {0xf0, 0x02},
    {0xf1, 0x01},
    {0xf2, 0x01},
    {0xf3, 0x30},
    {0xf9, 0x9f},
    {0xfa, 0x78},

    //---------------------------------------------------------------
    {0xfe, 0x01},

    {0x00, 0xf5},
    {0x02, 0x1a}, ////
    {0x0a, 0xa0},
    {0x0b, 0x60},
    {0x0c, 0x08},
    {0x0e, 0x4c},
    {0x0f, 0x39},
    {0x11, 0x3f},
    {0x12, 0x72},
    {0x13, 0x13},
    {0x14, 0x42},
    {0x15, 0x43},
    {0x16, 0xc2},
    {0x17, 0xa8},
    {0x18, 0x18},
    {0x19, 0x40},
    {0x1a, 0xd0},
    {0x1b, 0xf5},
    {0x70, 0x40},
    {0x71, 0x58},
    {0x72, 0x30},
    {0x73, 0x48},
    {0x74, 0x20},
    {0x75, 0x60},
    {0x77, 0x20},
    {0x78, 0x32},
    {0x30, 0x03},
    {0x31, 0x40},
    {0x32, 0xe0},
    {0x33, 0xe0},
    {0x34, 0xe0},
    {0x35, 0xb0},
    {0x36, 0xc0},
    {0x37, 0xc0},
    {0x38, 0x04},
    {0x39, 0x09},
    {0x3a, 0x12},
    {0x3b, 0x1C},
    {0x3c, 0x28},
    {0x3d, 0x31},
    {0x3e, 0x44},
    {0x3f, 0x57},
    {0x40, 0x6C},
    {0x41, 0x81},
    {0x42, 0x94},
    {0x43, 0xA7},
    {0x44, 0xB8},
    {0x45, 0xD6},
    {0x46, 0xEE},
    {0x47, 0x0d},

    {0xfe, 0x00},

    {0xd2, 0x90}, // Open AEC at last.

    //  TODO: FAE Modify the Init Regs here!!!
    {0x10, 0x26},
    {0x11, 0x0d}, // fd,modified by mormo 2010/07/06
    {0x1a, 0x2a}, // 1e,modified by mormo 2010/07/06
    {0x1c, 0x49}, // c1,modified by mormo 2010/07/06
    {0x1d, 0x9a}, // 08,modified by mormo 2010/07/06
    {0x1e, 0x61}, // 60,modified by mormo 2010/07/06

    {0x3a, 0x20},

    {0x50, 0x14}, // 10,modified by mormo 2010/07/06
    {0x53, 0x80},
    {0x56, 0x80},

    {0x8b, 0x20}, // LSC
    {0x8c, 0x20},
    {0x8d, 0x20},
    {0x8e, 0x14},
    {0x8f, 0x10},
    {0x90, 0x14},

    {0x94, 0x02},
    {0x95, 0x07},
    {0x96, 0xe0},

    {0xb1, 0x40}, // YCPT
    {0xb2, 0x40},
    {0xb3, 0x40},
    {0xb6, 0xe0},

    {0xd0, 0xcb}, // AECT	c9,modifed by mormo 2010/07/06
    {0xd3, 0x48}, // 80,modified by mormor 2010/07/06

    {0xf2, 0x02},
    {0xf7, 0x12},
    {0xf8, 0x0a},

    // Registers of Page1
    {0xfe, 0x01},

    {0x02, 0x20}, //////
    //  {0x02 , 0x00},
    {0x04, 0x10},
    {0x05, 0x08},
    {0x06, 0x20},
    {0x08, 0x0a},

    {0x0e, 0x44},
    {0x0f, 0x32},
    {0x10, 0x41},
    {0x11, 0x37},
    {0x12, 0x22},
    {0x13, 0x19},
    {0x14, 0x44},
    {0x15, 0x44},

    {0x19, 0x50},
    {0x1a, 0xd8},

    {0x32, 0x10},
};

void BoardDCMIInit(DCMI_TypeDef *pdcmi)
{
    DCMI_Handle.Instance           = pdcmi;
    DCMI_Handle.Init.InputMode     = INPUT_FORMAT_RGB565;
    DCMI_Handle.Init.OutputMode    = OUTPUT_FORMAT_RGB565;
    DCMI_Handle.Init.isCaptureMode = ENABLE;
    DCMI_Handle.Init.isRgbiDmaMode = ENABLE;
    DCMI_Handle.Init.eDmaDst       = (uint32_t)LCDC_ADDR0 + 3 * SCREEN_SIZE;
    DCMI_Handle.Init.iDmaDst       = (uint32_t)LCDC_ADDR0 + 2 * SCREEN_SIZE; // layer 3
    DCMI_Handle.Init.Input_height  = (g_room_rate.numerator * IMAGE_H - 1) / g_room_rate.denominator + 1;
    DCMI_Handle.Init.Input_width   = (g_room_rate.numerator * IMAGE_W - 1) / g_room_rate.denominator + 1;
    if (g_room_rate.numerator  == 2 && g_room_rate.denominator == 7)
        DCMI_Handle.Init.Input_width += 1;

    DCMI_Handle.Init.EnableIE      = ENABLE;
    DCMI_Handle.Init.IEMask        = 0x01;
    DCMI_Handle.Init.Rgb2Bgr       = ENABLE;
    DRV_DCMI_Init(DCMI, &DCMI_Handle.Init);
}

void DCMI_IRQHandler(void)
{
    uint32_t ris_bak;
    uint32_t mis_bak;

    ris_bak = readl(0x40053000 + 0x0008); // RIS
    mis_bak = readl(0x40053000 + 0x0010); // MIS

    if (ris_bak & mis_bak)
    {
        writel(0x40053000 + 0x0014, mis_bak);

        if (mis_bak & 0x10) // line interrupt.
        {
            // os_kprintf("line_interrupt\n");
        }

        if (mis_bak & 0x80) // gray interrupt.
        {
            // os_kprintf("Gray_interrupt\n");
        }

        if (mis_bak & 0x400) // specific line interrupt.
        {
            // os_kprintf("specific line interrupt!\n");
        }

        if (mis_bak & 0x01) // frame interrupt.
        {
            // os_kprintf("frame\r\n" );
            if (camera_sem)
            {
                os_semaphore_post(camera_sem);
            }
        }
    }
}

static void I2C_IO_Write(struct os_i2c_bus_device *bus, uint16_t addr, uint8_t reg, uint8_t value)
{
    uint8_t buff[2] = {reg, value};
    os_i2c_master_send(bus, addr, 0, buff, 2);
}

void gc0308_init(void)
{
    static I2C_InitTypeDef I2c_Init;
    struct os_i2c_bus_device *camera_i2c = os_i2c_bus_device_find(OS_CAMERA_I2C_BUS_NAME);
    OS_ASSERT(camera_i2c);
    
    uint32_t i;
    uint8_t room_rate = 0x00;

    if (g_room_rate.numerator == 3)
    {
        room_rate = 0x04;
    }
    else if (g_room_rate.numerator == 4)
    {
        room_rate = 0x46;    
    }

    I2c_Init.Mode      = I2C_MODE_MASTER;
    I2c_Init.AddBits   = 0x00;
    I2c_Init.HighSpeed = ENABLE;
    I2c_Init.ClockMode = I2C_CLOCK_MODE_NORMAL;
    I2c_Init.Prescaler = 28;
    DRV_I2C_Init(I2C1, &I2c_Init);

    os_pin_mode(PIN_PWDN, PIN_MODE_OUTPUT);
    os_pin_mode(PIN_RSTN, PIN_MODE_OUTPUT);

    // pwdn = 1; rstn = 1
    os_pin_write(PIN_PWDN, PIN_HIGH);
    os_pin_write(PIN_RSTN, PIN_HIGH);

    // pwdn = 1; rstn = 0
    os_pin_write(PIN_PWDN, PIN_HIGH);
    os_pin_write(PIN_RSTN, PIN_LOW);
    delay(1000000);
    
    // pwdn = 0; rstn = 0
    os_pin_write(PIN_PWDN, PIN_LOW);
    os_pin_write(PIN_RSTN, PIN_LOW);
    delay(400000);

    // pwdn = 0; rstn = 1
    os_pin_write(PIN_PWDN, PIN_LOW);
    os_pin_write(PIN_RSTN, PIN_HIGH);
    delay(10000000);

    for (i = 0; i < ((sizeof(gc0308_vga) >> 1)); i++)
    {
        I2C_IO_Write(camera_i2c, OS_CAMERA_I2C_ADDR, gc0308_vga[i][0], gc0308_vga[i][1]);
    }

    I2C_IO_Write(camera_i2c, OS_CAMERA_I2C_ADDR, 0x53, 0x82);
    I2C_IO_Write(camera_i2c, OS_CAMERA_I2C_ADDR, 0x54, 0x11 * g_room_rate.denominator);
    I2C_IO_Write(camera_i2c, OS_CAMERA_I2C_ADDR, 0x55, 0x03);
    I2C_IO_Write(camera_i2c, OS_CAMERA_I2C_ADDR, 0x56, g_room_rate.numerator > 1 ? 0x02 : 0x00);
    I2C_IO_Write(camera_i2c, OS_CAMERA_I2C_ADDR, 0x57, room_rate);
    I2C_IO_Write(camera_i2c, OS_CAMERA_I2C_ADDR, 0x58, g_room_rate.numerator > 1 ? 0x02 : 0x00);
    I2C_IO_Write(camera_i2c, OS_CAMERA_I2C_ADDR, 0x59, room_rate);

    I2C_IO_Write(camera_i2c, OS_CAMERA_I2C_ADDR, 0xfe, 0x00);
    I2C_IO_Write(camera_i2c, OS_CAMERA_I2C_ADDR, 0x14, 0x10 + g_transforme_type);    
}

void lt776_camera_clear_screen(void)
{
    uint32_t i;
    uint32_t width  = (g_room_rate.numerator * IMAGE_W - 1) / g_room_rate.denominator + 1;    
    uint32_t height = (g_room_rate.numerator * IMAGE_H - 1) / g_room_rate.denominator;

    if (g_room_rate.numerator  == 2 && g_room_rate.denominator == 7)
    {
        ++width;
    }

    for (i = 0; i < height; i++)
    {
        memset((void *)(LCDC_ADDR0 + (OS_GRAPHIC_LCD_WIDTH * i << 1)), 0, width << 1);
        memset((void *)(LCDC_ADDR1 + (OS_GRAPHIC_LCD_WIDTH * i << 1)), 0, width << 1);
    }
}

static void camera_entry(void *parameter)
{
    uint32_t i;
    uint32_t disAddr;
    uint32_t width  = (g_room_rate.numerator * IMAGE_W - 1) / g_room_rate.denominator + 1;
    uint32_t height = (g_room_rate.numerator * IMAGE_H - 1) / g_room_rate.denominator;

    if (g_room_rate.numerator  == 2 && g_room_rate.denominator == 7)
    {
        ++width;
    }

    while (1)
    {     
        if (camera_sem)
        {
            os_semaphore_wait(camera_sem, OS_WAIT_FOREVER);
        }

        disAddr = GetSdramAddr(0, 0, CLCD);

        for (i = 0; i < height; i++)
        {
            memcpy((void *)(disAddr + (OS_GRAPHIC_LCD_WIDTH * i << 1)), (void *)(DCMI_Handle.Init.iDmaDst + (width * i << 1)), width << 1);
        }

        DRV_CLCD_UpdateBaseAddr(disAddr);
    }
}

static os_err_t os_camera_init(struct os_device *dev)
{
    OS_ASSERT(dev);

    _ioctrl_swapintcr_swap_dis(26);

    gc0308_init();

    BoardDCMIInit(DCMI);

    if (camera_sem == OS_NULL)
    {
        camera_sem = os_semaphore_create(OS_NULL, "camera_sem", 0, 1);
        if (camera_sem == OS_NULL)
        {
            os_kprintf("create dynamic semaphore failed.\r\n");
            return OS_FAILURE;
        }
    }

    if (camera_task == OS_NULL)
    {
        camera_task = os_task_create(OS_NULL, OS_NULL, THREAD_STACK_SIZE_CAMERA, "camera", camera_entry, OS_NULL, THREAD_PRIORITY_CAMERA);
        if (camera_task != OS_NULL)
        {
            os_task_startup(camera_task);
        }
    }

    return OS_SUCCESS;
}

static os_err_t os_camera_deinit(struct os_device *dev)
{
    OS_ASSERT(dev);

    (*(volatile unsigned int *)(0x40053000)) &= ~0x00004000; // close DCMI

    if (camera_task != OS_NULL)
    {
        os_task_destroy(camera_task);
        camera_task = OS_NULL;
    }

    if (camera_sem != OS_NULL)
    {
        os_sem_destroy(camera_sem);
        camera_sem = OS_NULL; 
    }

    lt776_camera_clear_screen();

    return OS_SUCCESS;
}

static os_err_t os_camera_control(struct os_device *dev, int cmd, void *args)
{
    uint8_t i;
    room_rate_t room_rate;
    os_bool_t find_in_table = OS_FALSE;
    OS_CAMERA_TRANSFORMATION_TYPE_E trans_type;

    if (!args)
    {
        return OS_INVAL;
    }  

    switch (cmd)
    {
    case OS_CAMERA_CMD_RESOLUTION:
        room_rate = *(room_rate_t *)args;
        if ((room_rate.numerator == g_room_rate.numerator) && (room_rate.denominator == g_room_rate.denominator))
        {
            os_kprintf("repeat set\r\n");
            return OS_INVAL;
        }

        for(i = 0; i < sizeof(room_rate_table)/sizeof(room_rate_table[0]); i++)
        {
            if ((room_rate.numerator == room_rate_table[i].numerator) && (room_rate.denominator == room_rate_table[i].denominator))
            {
                find_in_table = OS_TRUE;
                break;
            }
        }

        if (find_in_table == OS_FALSE)
        {
            os_kprintf("not support %d:%d zoom\r\n", room_rate.numerator, room_rate.denominator);
            return OS_INVAL;
        }

        if (dev->ref_count)
        {
            os_camera_deinit(dev);
            g_room_rate = room_rate;
            return os_camera_init(dev);
        }
        break;
    case OS_CAMERA_CMD_TRANSFORMATION:
        trans_type = *(OS_CAMERA_TRANSFORMATION_TYPE_E *)args;
        if (trans_type == g_transforme_type)
        {
            os_kprintf("repeat set\r\n");
            return OS_INVAL;
        }    
        if (trans_type > OS_CAMERA_DISP_NORMAL)
        {
            os_kprintf("display parameter illegal: %d\r\n", trans_type);
            return OS_INVAL;
        }

        if (dev->ref_count)
        {
            os_camera_deinit(dev);
            g_transforme_type = trans_type;        
            return os_camera_init(dev);
        }    
        break;
    default:
        return OS_FAILURE;
    }

    return OS_SUCCESS;
}

const static struct os_device_ops camera_ops =
{
    .init    = os_camera_init,
    .deinit  = os_camera_deinit,
    .control = os_camera_control,
};

static int os_hw_camera_init(void)
{
    os_device_t *dev_camera = os_calloc(1, sizeof(os_device_t));
    OS_ASSERT(dev_camera);

    dev_camera->ops = &camera_ops;

    return os_device_register(dev_camera, "camera");
}

OS_INIT_CALL(os_hw_camera_init, OS_INIT_LEVEL_DEVICE, OS_INIT_SUBLEVEL_LOW);
