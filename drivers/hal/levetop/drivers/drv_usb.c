/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\ you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an \"AS IS\" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * \@file        drv_usb.c
 *
 * \@brief       This file implements usb driver for lt776.
 *
 * \@revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <drv_usb.h>

#if (defined BSP_ENABLE_USBD_HID_CLASS)
#include <usb_hid_protocol.h>
#elif (defined BSP_ENABLE_USBD_CDC_CLASS)
#include <usb_cdc_protocol.h>
#endif

static os_err_t lt776_usb_init(struct os_device *dev)
{
    OS_ASSERT(dev);

    USBC_HandleTypeDef hUSB;

    hUSB.Ep0DataStage = 0;
    hUSB.osc = 0;

#if (defined BSP_ENABLE_USBD_HID_CLASS)
    hUSB.version = USB_VERSION_11;
#elif (defined BSP_ENABLE_USBD_CDC_CLASS)
    hUSB.version = USB_VERSION_20;
#endif

    USBC_Init(&hUSB);

    return OS_SUCCESS;
}

static os_err_t lt776_usb_deinit(struct os_device *dev)
{
    OS_ASSERT(dev);

    _cpm_set_usbphy_power_switch_dis;

    return OS_SUCCESS;
}

static os_ssize_t lt776_usb_read(struct os_device *dev, os_off_t pos, void *buffer, os_size_t size)
{
    OS_ASSERT(dev);

    uint16_t recv_len = 0;

    if ((gUSBC_RxINT_Flag & (1 << BULKOUT_EP)))
    {
#if (defined BSP_ENABLE_USBD_HID_CLASS)
        recv_len = USB_HID_Receive(BULKOUT_EP, buffer);
#elif (defined BSP_ENABLE_USBD_CDC_CLASS)
        recv_len = USB_CDC_Receive(BULKOUT_EP, buffer);
#endif
        gUSBC_RxINT_Flag &= ~(1 << BULKOUT_EP); //�����־λ
    }

    return recv_len;
}

static os_ssize_t lt776_usb_write(struct os_device *dev, os_off_t pos, const void *buffer, os_size_t size)
{
    OS_ASSERT(dev);

#if (defined BSP_ENABLE_USBD_HID_CLASS)
    USB_HID_Send(BULKIN_EP, (uint8_t *)buffer, size);
#elif (defined BSP_ENABLE_USBD_CDC_CLASS)
    USB_CDC_Send(BULKIN_EP, (uint8_t *)buffer, size);
#endif
    return size;
}

const static struct os_device_ops usb_ops =
{
    .init   = lt776_usb_init,
    .deinit = lt776_usb_deinit,
    .read   = lt776_usb_read,
    .write  = lt776_usb_write,
};

static int os_hw_usb_init(void)
{
    os_device_t *dev_usb = os_calloc(1, sizeof(os_device_t));
    OS_ASSERT(dev_usb);

    dev_usb->type = OS_DEVICE_TYPE_USBDEVICE;
    dev_usb->ops = &usb_ops;

    return os_device_register(dev_usb, "usbd");
}

OS_INIT_CALL(os_hw_usb_init, OS_INIT_LEVEL_DEVICE, OS_INIT_SUBLEVEL_LOW);
