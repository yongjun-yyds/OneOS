/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_common.c
 *
 * @brief       This file provides systick time init/IRQ and board init functions.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <board.h>
#include <os_clock.h>
#include <timer/clocksource.h>
#include <cpm_drv.h>

#ifdef OS_USING_PIN
#include <drv_gpio.h>
#endif

#ifdef OS_USING_FAL
#include <eflash_drv.h>
#endif

void SysTick_Handler(void)
{
    os_tick_increase();

#ifdef OS_USING_CLOCKSOURCE
    os_clocksource_update();
#endif
}

void systick_config(void)
{
    if (SysTick_Config(g_core_clk / OS_TICK_PER_SECOND))
    {
        /* capture error */
        while (1)
        {
        }
    }

    /* configure the systick handler priority */
    NVIC_SetPriority(SysTick_IRQn, 0x00U);
}

/**
 ***********************************************************************************************************************
 * @brief           This function will initial STM32 board.
 *
 * @param[in]       none
 *
 * @return          none
 ***********************************************************************************************************************
 */
static os_err_t os_hw_board_init(void)
{
    InitChip();

    systick_config();

#if defined(OS_USING_HEAP)
    if ((os_size_t)HEAP_END > (os_size_t)HEAP_BEGIN)
    {
        os_default_heap_init();
        os_default_heap_add((void *)HEAP_BEGIN, (os_size_t)HEAP_END - (os_size_t)HEAP_BEGIN, OS_MEM_ALG_DEFAULT);
    }
#endif

    return OS_SUCCESS;
}
OS_INIT_CALL(os_hw_board_init, OS_INIT_LEVEL_PRE_KERNEL_1, OS_INIT_SUBLEVEL_MIDDLE);

static os_err_t board_post_init(void)
{
#ifdef OS_USING_PIN
    os_hw_pin_init();
#endif

#ifdef OS_USING_FAL
    DRV_EFM_Init(g_sys_clk / 1000);
    DRV_EFLASH_SetWritePermission();
#endif

#if defined(OS_USING_DWT_FOR_CLOCKSOURCE) && defined(DWT)
    cortexm_dwt_init();
#endif
    return OS_SUCCESS;
}
OS_INIT_CALL(board_post_init, OS_INIT_LEVEL_POST_KERNEL, OS_INIT_SUBLEVEL_HIGH);

void os_hw_cpu_reset(void)
{
    NVIC_SystemReset();
}
