/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at

 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_adc.c
 *
 * @brief       This file implements adc driver for lt776.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <drv_adc.h>

#include <dlog.h>
#define DBG_TAG "drv.adc"

uint16_t ADC_GetConversionValue(void)
{
    uint32_t tmp;
    uint8_t  i;

    DRV_ADC_ModuleEn();
    for (i = 0; i < 9; i++)
    {
        DRV_ADC_ConvStart();

        while (_adc_chk_end_sequence_flag != ADC_END_SEQUENCE_FLAG)
        {
            ;
        }

        _adc_clr_end_sequence_flag;
        DRV_ADC_ConvStop();

        //������һ������ֵ
        if (i == 0)
        {
            tmp = _adc_get_convert_data;
            tmp = 0;
            continue;
        }
        tmp += _adc_get_convert_data;
    }

    DRV_ADC_ModuleDis();

    return ((tmp >> 3) & 0xFFFF);
}

static uint32_t lt776_adc_read_channel(uint32_t channel)
{
    return 3300 * ADC_GetConversionValue() / 4095;
}

void ADC_Init(ADC_InitTypeDef *adc)
{
    _adc_set_prescaler_clk_div(adc->clk_div);
    _adc_set_startup_counter(adc->stab_time);
    _adc_set_sampling_time(adc->smp_time);
    _adc_set_data_resolution(adc->resolution);

    _adc_set_sequence_length(0);
    _adc_set_number0_channel_name(adc->channel);

    if (adc->vref == ADC_EXTERNAL_VREF)
    {
        _adc_sel_external_vret;
    }
    else
    {
        _adc_sel_internal_vref;
    }

    if (adc->conv == ADC_CONTINUOUS_CONV_MODE)
    {
        _adc_set_continuous_conversion;
    }
    else
    {
        _adc_set_single_conversion;
    }

    if (adc->overrun == ADC_OVERRUN_MODE_LAST)
    {
        _adc_set_overrun_mode_last;
    }
    else
    {
        _adc_set_overrun_mode_old;
    }

    if (adc->align == ADC_RIGHT_ALIGN)
    {
        _adc_set_data_right_alignment;
    }
    else
    {
        _adc_set_data_left_alignment;
    }

    DRV_ADC_ChannelSel(adc->channel);

    if (adc->int_kind != 0)
    {
        NVIC_Init(3, 3, QADC_IRQn, 2);
        _adc_set_int_en(adc->int_kind);
    }
}

static os_err_t lt776_adc_channel_init(struct lt776_adc *dev, uint32_t channel)
{
    ADC_InitTypeDef adc;

    adc.align      = ADC_RIGHT_ALIGN;
    adc.clk_div    = ADC_CLK_DIV_4;
    adc.conv       = ADC_CONTINUOUS_CONV_MODE;
    adc.overrun    = ADC_OVERRUN_MODE_LAST;
    adc.resolution = ADC_RESOLUTION_12BIT;
    adc.smp_time   = 0x20;
    adc.stab_time  = 0x20;
    adc.vref       = ADC_INTERNAL_VREF;
    adc.int_kind   = 0;

    switch (channel)
    {
    case 0:
        adc.channel = ADC_CHANNEL_0;    // AIN0
        break;
    case 1:
        adc.channel = ADC_CHANNEL_1;    // AIN1
        break;
    case 2:
        adc.channel = ADC_CHANNEL_2;    // AIN2
        break;
    case 3:
        adc.channel = ADC_CHANNEL_3;    // AIN3
        break;
    default:
        os_kprintf("illegal channel: %d\r\n", channel);
        return OS_FAILURE;
    }

    ADC_Init(&adc);

    return OS_SUCCESS;
}

/**
 ***********************************************************************************************************************
 * @brief           lt776_adc_poll_convert_then_read: start adc convert in poll
 *
 * @details
 *
 * @attention       Attention_description_Optional
 *
 ***********************************************************************************************************************
 */
static os_err_t lt776_adc_read(struct os_adc_device *dev, uint32_t channel, int32_t *buff)
{
    struct lt776_adc *dev_adc;

    OS_ASSERT(dev != OS_NULL);
    dev_adc = os_container_of(dev, struct lt776_adc, adc);

    if (dev_adc->status != OS_ADC_ENABLE)
    {
        LOG_E(DBG_TAG, "adc disabled! please enable adc first!");
        return OS_FAILURE;
    }

    if (lt776_adc_channel_init(dev_adc, channel) != OS_SUCCESS)
    {
        return OS_FAILURE;
    }

    *buff = (int32_t)lt776_adc_read_channel(channel);

    return OS_SUCCESS;
}

static os_err_t lt776_adc_enabled(struct os_adc_device *dev, os_bool_t enable)
{
    struct lt776_adc *dev_adc;

    dev_adc = os_container_of(dev, struct lt776_adc, adc);

    if (!enable)
        dev_adc->status = OS_ADC_DISABLE;
    else
        dev_adc->status = OS_ADC_ENABLE;

    return OS_SUCCESS;
}

static os_err_t lt776_adc_control(struct os_adc_device *dev, int cmd, void *arg)
{
    return OS_SUCCESS;
}

static const struct os_adc_ops lt776_adc_ops = {
    .adc_enabled = lt776_adc_enabled,
    .adc_control = lt776_adc_control,
    .adc_read    = lt776_adc_read,
};

static int lt776_adc_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    os_err_t result = 0;

    struct lt776_adc *lt776_adc = OS_NULL;
    lt776_adc                   = os_calloc(1, sizeof(struct lt776_adc));
    OS_ASSERT(lt776_adc);

    lt776_adc->status = OS_ADC_DISABLE;

    struct os_adc_device *dev_adc = &lt776_adc->adc;
    dev_adc->ops                  = &lt776_adc_ops;
    dev_adc->max_value            = 0;
    dev_adc->ref_low              = 0; /* ref 0 - 3.3v */
    dev_adc->ref_hight            = 3300;

    result = os_hw_adc_register(dev_adc, dev->name, NULL);
    if (result != OS_SUCCESS)
    {
        LOG_E(DBG_TAG, "%s register fialed!\r\n", dev->name);
        return OS_FAILURE;
    }
    return OS_SUCCESS;
}

OS_DRIVER_INFO lt776_adc_driver = {
    .name  = "ADC_TypeDef",
    .probe = lt776_adc_probe,
};

OS_DRIVER_DEFINE(lt776_adc_driver, OS_INIT_LEVEL_PRE_DEVICE, OS_INIT_SUBLEVEL_HIGH);

