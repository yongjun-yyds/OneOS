/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with 
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_qspi.h
 *
 * @brief       This file implements QSPI driver for lt776.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __DRV_QSPI_H_
#define __DRV_QSPI_H_

#include <sfbus.h>
#include <ssi_drv.h>

struct lt776_qspi
{
    struct os_sfbus sfbus;
    SSI_TypeDef *QSPI_Handler;
    os_list_node_t list;
};

#endif /* __DRV_QSPI_H_ */
