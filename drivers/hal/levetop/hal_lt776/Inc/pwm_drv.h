/**
    ******************************************************************************
             Copyright(c) 2020 Levetop Semiconductor Co., Ltd.
                      All Rights Reserved
  ******************************************************************************
  * @file    pwm_drv.h
  * @author  Product application department
  * @version V1.0
  * @date    2020.02.25
  * @brief   Header file of PWM HAL module.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __PWM_DRV_H
#define __PWM_DRV_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "lt776_reg.h"

#ifdef FILE_OUT
/**@defgroup DRV 3 HAL driver
 *
 *@{
 */

/** @defgroup DRV_PWM PWM
 *
 *@{
 */
#endif
/*** 宏定义 *******************************************************************/
#ifdef FILE_OUT
/** @defgroup DRV_PWM_Exported_Macros Exported Macros
 *
 * @{
 */
#endif

// PPR
#define _pwm_set_ch01_perscaler(pwm, value)                                                                            \
    _reg_modify(pwm->PPR, ~(0xff << PWM_PPR_CP01_SHIFT), value << PWM_PPR_CP01_SHIFT) /**<                             \
                                                                                         PWM:设置CH0/1的时钟预分频值*/
#define _pwm_set_ch23_perscaler(pwm, value)                                                                            \
    _reg_modify(pwm->PPR, ~(0xff << PWM_PPR_CP23_SHIFT), value << PWM_PPR_CP23_SHIFT) /**<                             \
                                                                                         PWM:设置CH2/3的时钟预分频值*/
#define _pwm_set_ch45_perscaler(pwm, value)                                                                            \
    _reg_modify(pwm->PPR, ~(0xff << PWM_PPR_CP45_SHIFT), value << PWM_PPR_CP45_SHIFT) /**<                             \
                                                                                         PWM:设置CH4/5的时钟预分频值*/
#define _pwm_set_ch67_perscaler(pwm, value)                                                                            \
    _reg_modify(pwm->PPR, ~((u32)0xff << PWM_PPR_CP67_SHIFT), (u32)value << PWM_PPR_CP67_SHIFT) /**<                             \
                                                                                         PWM:设置CH6/7的时钟预分频值*/
#define _pwm_get_ppr(pwm) _reg_read(pwm->PPR)

#define _pwm_set_ch01_deadzoneinterva(pwm, value)                                                                      \
    _reg_modify(pwm->DZ, ~(0xff << PWM_DZ_DZ01_SHIFT), value << PWM_DZ_DZ01_SHIFT) /**< PWM:设置CH0/1死区翻转*/
#define _pwm_set_ch23_deadzoneinterva(pwm, value)                                                                      \
    _reg_modify(pwm->DZ, ~(0xff << PWM_DZ_DZ23_SHIFT), value << PWM_DZ_DZ23_SHIFT) /**< PWM:设置CH2/3死区翻转*/
#define _pwm_set_ch45_deadzoneinterva(pwm, value)                                                                      \
    _reg_modify(pwm->DZ, ~(0xff << PWM_DZ_DZ45_SHIFT), value << PWM_DZ_DZ45_SHIFT) /**< PWM:设置CH4/5死区翻转*/
#define _pwm_set_ch67_deadzoneinterva(pwm, value)                                                                      \
    _reg_modify(pwm->DZ, ~((u32)0xff << PWM_DZ_DZ67_SHIFT), (u32)value << PWM_DZ_DZ67_SHIFT) /**< PWM:设置CH6/7死区翻转*/

#define _pwm_set_ch0_clkdiv(pwm, value)                                                                                \
    _reg_modify(pwm->PCSR,                                                                                             \
                ~(0x07 << PWM_PCSR_CSR0_SHIFT),                                                                        \
                ((value & 0x07) << PWM_PCSR_CSR0_SHIFT)) /**< PWM:设置CH0的时钟分频值*/
#define _pwm_set_ch1_clkdiv(pwm, value)                                                                                \
    _reg_modify(pwm->PCSR,                                                                                             \
                ~(0x07 << PWM_PCSR_CSR1_SHIFT),                                                                        \
                ((value & 0x07) << PWM_PCSR_CSR1_SHIFT)) /**< PWM:设置CH1的时钟分频值*/
#define _pwm_set_ch2_clkdiv(pwm, value)                                                                                \
    _reg_modify(pwm->PCSR,                                                                                             \
                ~(0x07 << PWM_PCSR_CSR2_SHIFT),                                                                        \
                ((value & 0x07) << PWM_PCSR_CSR2_SHIFT)) /**< PWM:设置CH2的时钟分频值*/
#define _pwm_set_ch3_clkdiv(pwm, value)                                                                                \
    _reg_modify(pwm->PCSR,                                                                                             \
                ~(0x07 << PWM_PCSR_CSR3_SHIFT),                                                                        \
                ((value & 0x07) << PWM_PCSR_CSR3_SHIFT)) /**< PWM:设置CH3的时钟分频值*/
#define _pwm_set_ch4_clkdiv(pwm, value)                                                                                \
    _reg_modify(pwm->PCSR,                                                                                             \
                ~(0x07 << PWM_PCSR_CSR4_SHIFT),                                                                        \
                ((value & 0x07) << PWM_PCSR_CSR4_SHIFT)) /**< PWM:设置CH4的时钟分频值*/
#define _pwm_set_ch5_clkdiv(pwm, value)                                                                                \
    _reg_modify(pwm->PCSR,                                                                                             \
                ~(0x07 << PWM_PCSR_CSR5_SHIFT),                                                                        \
                ((value & 0x07) << PWM_PCSR_CSR5_SHIFT)) /**< PWM:设置CH5的时钟分频值*/
#define _pwm_set_ch6_clkdiv(pwm, value)                                                                                \
    _reg_modify(pwm->PCSR,                                                                                             \
                ~(0x07 << PWM_PCSR_CSR6_SHIFT),                                                                        \
                ((value & 0x07) << PWM_PCSR_CSR6_SHIFT)) /**< PWM:设置CH6的时钟分频值*/
#define _pwm_set_ch7_clkdiv(pwm, value)                                                                                \
    _reg_modify(pwm->PCSR,                                                                                             \
                ~(0x07 << PWM_PCSR_CSR7_SHIFT),                                                                        \
                ((value & 0x07) << PWM_PCSR_CSR7_SHIFT)) /**< PWM:设置CH7的时钟分频值*/
#define _pwm_get_pcsr(pwm) _reg_read(pwm->PCSR)

#define _pwm_en_ch0(pwm)               _bit_set(pwm->PCR, PWM_PCR_CH0EN)  /**< PWM:CH0使能*/
#define _pwm_dis_ch0(pwm)              _bit_clr(pwm->PCR, PWM_PCR_CH0EN)  /**< PWM:CH0禁用*/
#define _pwm_en_ch4(pwm)               _bit_set(pwm->PCR, PWM_PCR_CH4EN)  /**< PWM:CH4使能*/
#define _pwm_dis_ch4(pwm)              _bit_clr(pwm->PCR, PWM_PCR_CH4EN)  /**< PWM:CH4禁用*/
#define _pwm_en_ch0_timerinverte(pwm)  _bit_set(pwm->PCR, PWM_PCR_CH0INV) /**< PWM:CH0使能时钟翻转*/
#define _pwm_dis_ch0_timerinverte(pwm) _bit_clr(pwm->PCR, PWM_PCR_CH0INV) /**< PWM:CH0禁用时钟翻转*/
#define _pwm_set_ch0_autoloadmode(pwm) _bit_set(pwm->PCR, PWM_PCR_CH0MOD) /**< PWM:CH0配置自送加载模式*/
#define _pwm_set_ch0_oneshotmode(pwm)  _bit_clr(pwm->PCR, PWM_PCR_CH0MOD) /**< PWM:CH0配置单次加载模式*/
#define _pwm_en_deadzone0(pwm)         _bit_set(pwm->PCR, PWM_PCR_DZ0EN)  /**< PWM:使能死区0*/
#define _pwm_dis_deadzone0(pwm)        _bit_clr(pwm->PCR, PWM_PCR_DZ0EN)  /**< PWM:禁用死区0*/
#define _pwm_en_deadzone1(pwm)         _bit_set(pwm->PCR, PWM_PCR_DZ1EN)  /**< PWM:使能死区1*/
#define _pwm_dis_deadzone1(pwm)        _bit_clr(pwm->PCR, PWM_PCR_DZ1EN)  /**< PWM:禁用死区1*/
#define _pwm_en_ch1(pwm)               _bit_set(pwm->PCR, PWM_PCR_CH1EN)  /**< PWM:CH1使能*/
#define _pwm_dis_ch1(pwm)              _bit_clr(pwm->PCR, PWM_PCR_CH1EN)  /**< PWM:CH1禁用*/
#define _pwm_en_ch5(pwm)               _bit_set(pwm->PCR, PWM_PCR_CH5EN)  /**< PWM:CH5使能*/
#define _pwm_dis_ch5(pwm)              _bit_clr(pwm->PCR, PWM_PCR_CH5EN)  /**< PWM:CH5禁用*/
#define _pwm_en_ch1_timerinverte(pwm)  _bit_set(pwm->PCR, PWM_PCR_CH1INV) /**< PWM:CH1使能时钟翻转*/
#define _pwm_dis_ch1_timerinverte(pwm) _bit_clr(pwm->PCR, PWM_PCR_CH1INV) /**< PWM:CH1禁用时钟翻转*/
#define _pwm_set_ch1_autoloadmode(pwm) _bit_set(pwm->PCR, PWM_PCR_CH1MOD) /**< PWM:CH1配置自送加载模式*/
#define _pwm_set_ch1_oneshotmode(pwm)  _bit_clr(pwm->PCR, PWM_PCR_CH1MOD) /**< PWM:CH1配置单次加载模式*/
#define _pwm_en_ch2(pwm)               _bit_set(pwm->PCR, PWM_PCR_CH2EN)  /**< PWM:CH2使能*/
#define _pwm_dis_ch2(pwm)              _bit_clr(pwm->PCR, PWM_PCR_CH2EN)  /**< PWM:CH2禁用*/
#define _pwm_en_ch6(pwm)               _bit_set(pwm->PCR, PWM_PCR_CH6EN)  /**< PWM:CH6使能*/
#define _pwm_dis_ch6(pwm)              _bit_clr(pwm->PCR, PWM_PCR_CH6EN)  /**< PWM:CH6禁用*/
#define _pwm_en_ch2_timerinverte(pwm)  _bit_set(pwm->PCR, PWM_PCR_CH2INV) /**< PWM:CH2使能时钟翻转*/
#define _pwm_dis_ch2_timerinverte(pwm) _bit_clr(pwm->PCR, PWM_PCR_CH2INV) /**< PWM:CH2禁用时钟翻转*/
#define _pwm_set_ch2_autoloadmode(pwm) _bit_set(pwm->PCR, PWM_PCR_CH2MOD) /**< PWM:CH2配置自送加载模式*/
#define _pwm_set_ch2_oneshotmode(pwm)  _bit_clr(pwm->PCR, PWM_PCR_CH2MOD) /**< PWM:CH2配置单次加载模式*/
#define _pwm_en_ch3(pwm)               _bit_set(pwm->PCR, PWM_PCR_CH3EN)  /**< PWM:CH3使能*/
#define _pwm_dis_ch3(pwm)              _bit_clr(pwm->PCR, PWM_PCR_CH3EN)  /**< PWM:CH3禁用*/
#define _pwm_en_ch7(pwm)               _bit_set(pwm->PCR, PWM_PCR_CH7EN)  /**< PWM:CH7使能*/
#define _pwm_dis_ch7(pwm)              _bit_clr(pwm->PCR, PWM_PCR_CH7EN)  /**< PWM:CH7禁用*/
#define _pwm_en_ch3_timerinverte(pwm)  _bit_set(pwm->PCR, PWM_PCR_CH3INV) /**< PWM:CH3使能时钟翻转*/
#define _pwm_dis_ch3_timerinverte(pwm) _bit_clr(pwm->PCR, PWM_PCR_CH3INV) /**< PWM:CH3禁用时钟翻转*/
#define _pwm_set_ch3_autoloadmode(pwm) _bit_set(pwm->PCR, PWM_PCR_CH3MOD) /**< PWM:CH3配置自送加载模式*/
#define _pwm_set_ch3_oneshotmode(pwm)  _bit_clr(pwm->PCR, PWM_PCR_CH3MOD) /**< PWM:CH3配置单次加载模式*/
#define _pwm_get_pcr(pwm)              _reg_read(pwm->PCR)                /**< PWM:获取PCR寄存器值*/

#define _pwm_en_ch4_timerinverte(pwm)  _bit_set(pwm->PCR1, PWM_PCR1_CH4INV) /**< PWM:CH4使能时钟翻转*/
#define _pwm_dis_ch4_timerinverte(pwm) _bit_clr(pwm->PCR1, PWM_PCR1_CH4INV) /**< PWM:CH4禁用时钟翻转*/
#define _pwm_set_ch4_autoloadmode(pwm) _bit_set(pwm->PCR1, PWM_PCR1_CH4MOD) /**< PWM:CH4配置自送加载模式*/
#define _pwm_set_ch4_oneshotmode(pwm)  _bit_clr(pwm->PCR1, PWM_PCR1_CH4MOD) /**< PWM:CH4配置单次加载模式*/
#define _pwm_en_deadzone2(pwm)         _bit_set(pwm->PCR1, PWM_PCR1_DZ2EN)  /**< PWM:使能死区2*/
#define _pwm_dis_deadzone2(pwm)        _bit_clr(pwm->PCR1, PWM_PCR1_DZ2EN)  /**< PWM:禁用死区2*/
#define _pwm_en_deadzone3(pwm)         _bit_set(pwm->PCR1, PWM_PCR1_DZ3EN)  /**< PWM:使能死区3*/
#define _pwm_dis_deadzone3(pwm)        _bit_clr(pwm->PCR1, PWM_PCR1_DZ3EN)  /**< PWM:禁用死区3*/
#define _pwm_en_ch5_timerinverte(pwm)  _bit_set(pwm->PCR1, PWM_PCR1_CH5INV) /**< PWM:CH5使能时钟翻转*/
#define _pwm_dis_ch5_timerinverte(pwm) _bit_clr(pwm->PCR1, PWM_PCR1_CH5INV) /**< PWM:CH5禁用时钟翻转*/
#define _pwm_set_ch5_autoloadmode(pwm) _bit_set(pwm->PCR1, PWM_PCR1_CH5MOD) /**< PWM:CH5配置自送加载模式*/
#define _pwm_set_ch5_oneshotmode(pwm)  _bit_clr(pwm->PCR1, PWM_PCR1_CH5MOD) /**< PWM:CH5配置单次加载模式*/
#define _pwm_en_ch6_timerinverte(pwm)  _bit_set(pwm->PCR1, PWM_PCR1_CH6INV) /**< PWM:CH6使能时钟翻转*/
#define _pwm_dis_ch6_timerinverte(pwm) _bit_clr(pwm->PCR1, PWM_PCR1_CH6INV) /**< PWM:CH6禁用时钟翻转*/
#define _pwm_set_ch6_autoloadmode(pwm) _bit_set(pwm->PCR1, PWM_PCR1_CH6MOD) /**< PWM:CH6配置自送加载模式*/
#define _pwm_set_ch6_oneshotmode(pwm)  _bit_clr(pwm->PCR1, PWM_PCR1_CH6MOD) /**< PWM:CH6配置单次加载模式*/
#define _pwm_en_ch7_timerinverte(pwm)  _bit_set(pwm->PCR1, PWM_PCR1_CH7INV) /**< PWM:CH7使能时钟翻转*/
#define _pwm_dis_ch7_timerinverte(pwm) _bit_clr(pwm->PCR1, PWM_PCR1_CH7INV) /**< PWM:CH7禁用时钟翻转*/
#define _pwm_set_ch7_autoloadmode(pwm) _bit_set(pwm->PCR1, PWM_PCR1_CH7MOD) /**< PWM:CH7配置自送加载模式*/
#define _pwm_set_ch7_oneshotmode(pwm)  _bit_clr(pwm->PCR1, PWM_PCR1_CH7MOD) /**< PWM:CH7配置单次加载模式*/
#define _pwm_get_pcr1(pwm)             _reg_read(pwm->PCR1)                 /**< PWM:获取PCR1寄存器值*/

#define _pwm_set_ch0_cnt(pwm, value)                                                                                   \
    _reg_write(pwm->PCNR0, ((value & 0x0000ffff) | (pwm->PCNR0 & 0xffff0000))) /**< PWM:CH0设置周期计数器值*/
#define _pwm_set_ch1_cnt(pwm, value)                                                                                   \
    _reg_write(pwm->PCNR1, ((value & 0x0000ffff) | (pwm->PCNR1 & 0xffff0000))) /**< PWM:CH1设置周期计数器值*/
#define _pwm_set_ch2_cnt(pwm, value)                                                                                   \
    _reg_write(pwm->PCNR2, ((value & 0x0000ffff) | (pwm->PCNR2 & 0xffff0000))) /**< PWM:CH2设置周期计数器值*/
#define _pwm_set_ch3_cnt(pwm, value)                                                                                   \
    _reg_write(pwm->PCNR3, ((value & 0x0000ffff) | (pwm->PCNR3 & 0xffff0000))) /**< PWM:CH3设置周期计数器值*/
#define _pwm_set_ch4_cnt(pwm, value)                                                                                   \
    _reg_write(pwm->PCNR0, (((value << 16) & 0xffff0000) | (pwm->PCNR0 & 0x0000ffff))) /**< PWM:CH4设置周期计数器值*/
#define _pwm_set_ch5_cnt(pwm, value)                                                                                   \
    _reg_write(pwm->PCNR1, (((value << 16) & 0xffff0000) | (pwm->PCNR1 & 0x0000ffff))) /**< PWM:CH5设置周期计数器值*/
#define _pwm_set_ch6_cnt(pwm, value)                                                                                   \
    _reg_write(pwm->PCNR2, (((value << 16) & 0xffff0000) | (pwm->PCNR2 & 0x0000ffff))) /**< PWM:CH6设置周期计数器值*/
#define _pwm_set_ch7_cnt(pwm, value)                                                                                   \
    _reg_write(pwm->PCNR3, (((value << 16) & 0xffff0000) | (pwm->PCNR3 & 0x0000ffff))) /**< PWM:CH7设置周期计数器值*/

#define _pwm_get_pcnr0(pwm) (_reg_read(pwm->PCNR0) & 0xffff)       /**< PWM:获取CH0寄存器值*/
#define _pwm_get_pcnr1(pwm) (_reg_read(pwm->PCNR1) & 0xffff)       /**< PWM:获取CH1寄存器值*/
#define _pwm_get_pcnr2(pwm) (_reg_read(pwm->PCNR2) & 0xffff)       /**< PWM:获取CH2寄存器值*/
#define _pwm_get_pcnr3(pwm) (_reg_read(pwm->PCNR3) & 0xffff)       /**< PWM:获取CH3寄存器值*/
#define _pwm_get_pcnr4(pwm) (_reg_read(pwm->PCNR0 >> 16) & 0xffff) /**< PWM:获取CH4寄存器值*/
#define _pwm_get_pcnr5(pwm) (_reg_read(pwm->PCNR1 >> 16) & 0xffff) /**< PWM:获取CH5寄存器值*/
#define _pwm_get_pcnr6(pwm) (_reg_read(pwm->PCNR2 >> 16) & 0xffff) /**< PWM:获取CH6寄存器值*/
#define _pwm_get_pcnr7(pwm) (_reg_read(pwm->PCNR3 >> 16) & 0xffff) /**< PWM:获取CH7寄存器值*/

#define _pwm_set_ch0_Comparator(pwm, value)                                                                            \
    _reg_write(pwm->PCMR0, ((value & 0x0000ffff) | (pwm->PCMR0 & 0xffff0000))) /**< PWM:CH0设置脉冲值*/
#define _pwm_set_ch1_Comparator(pwm, value)                                                                            \
    _reg_write(pwm->PCMR1, ((value & 0x0000ffff) | (pwm->PCMR1 & 0xffff0000))) /**< PWM:CH1设置脉冲值*/
#define _pwm_set_ch2_Comparator(pwm, value)                                                                            \
    _reg_write(pwm->PCMR2, ((value & 0x0000ffff) | (pwm->PCMR2 & 0xffff0000))) /**< PWM:CH2设置脉冲值*/
#define _pwm_set_ch3_Comparator(pwm, value)                                                                            \
    _reg_write(pwm->PCMR3, ((value & 0x0000ffff) | (pwm->PCMR3 & 0xffff0000))) /**< PWM:CH3设置脉冲值*/
#define _pwm_set_ch4_Comparator(pwm, value)                                                                            \
    _reg_write(pwm->PCMR0, (((value << 16) & 0xffff0000) | (pwm->PCMR0 & 0x0000ffff))) /**< PWM:CH4设置脉冲值*/
#define _pwm_set_ch5_Comparator(pwm, value)                                                                            \
    _reg_write(pwm->PCMR1, (((value << 16) & 0xffff0000) | (pwm->PCMR1 & 0x0000ffff))) /**< PWM:CH5设置脉冲值*/
#define _pwm_set_ch6_Comparator(pwm, value)                                                                            \
    _reg_write(pwm->PCMR2, (((value << 16) & 0xffff0000) | (pwm->PCMR2 & 0x0000ffff))) /**< PWM:CH6设置脉冲值*/
#define _pwm_set_ch7_Comparator(pwm, value)                                                                            \
    _reg_write(pwm->PCMR3, (((value << 16) & 0xffff0000) | (pwm->PCMR3 & 0x0000ffff))) /**< PWM:CH7设置脉冲值*/

#define _pwm_get_Comparator0(pwm) (_reg_read(pwm->PCMR0) & 0xffff)       /**< PWM:获取CH0寄存器值*/
#define _pwm_get_Comparator1(pwm) (_reg_read(pwm->PCMR1) & 0xffff)       /**< PWM:获取CH1寄存器值*/
#define _pwm_get_Comparator2(pwm) (_reg_read(pwm->PCMR2) & 0xffff)       /**< PWM:获取CH2寄存器值*/
#define _pwm_get_Comparator3(pwm) (_reg_read(pwm->PCMR3) & 0xffff)       /**< PWM:获取CH3寄存器值*/
#define _pwm_get_Comparator4(pwm) (_reg_read(pwm->PCMR0 >> 16) & 0xffff) /**< PWM:获取CH4寄存器值*/
#define _pwm_get_Comparator5(pwm) (_reg_read(pwm->PCMR1 >> 16) & 0xffff) /**< PWM:获取CH5寄存器值*/
#define _pwm_get_Comparator6(pwm) (_reg_read(pwm->PCMR2 >> 16) & 0xffff) /**< PWM:获取CH6寄存器值*/
#define _pwm_get_Comparator7(pwm) (_reg_read(pwm->PCMR3 >> 16) & 0xffff) /**< PWM:获取CH7寄存器值*/

#define _pwm_get_ch0_timercnt(pwm) (_reg_read(pwm->PTR0) & 0xffff)       /**< PWM:CH0获取时钟计数器*/
#define _pwm_get_ch1_timercnt(pwm) (_reg_read(pwm->PTR1) & 0xffff)       /**< PWM:CH1获取时钟计数器*/
#define _pwm_get_ch2_timercnt(pwm) (_reg_read(pwm->PTR2) & 0xffff)       /**< PWM:CH2获取时钟计数器*/
#define _pwm_get_ch3_timercnt(pwm) (_reg_read(pwm->PTR3) & 0xffff)       /**< PWM:CH3获取时钟计数器*/
#define _pwm_get_ch4_timercnt(pwm) (_reg_read(pwm->PTR0 >> 16) & 0xffff) /**< PWM:CH4获取时钟计数器*/
#define _pwm_get_ch5_timercnt(pwm) (_reg_read(pwm->PTR1 >> 16) & 0xffff) /**< PWM:CH5获取时钟计数器*/
#define _pwm_get_ch6_timercnt(pwm) (_reg_read(pwm->PTR2 >> 16) & 0xffff) /**< PWM:CH6获取时钟计数器*/
#define _pwm_get_ch7_timercnt(pwm) (_reg_read(pwm->PTR3 >> 16) & 0xffff) /**< PWM:CH7获取时钟计数器*/

#define _pwm_en_ch0_timerint(pwm)  _bit_set(pwm->PIER, PWM_PIER_CH0INT_EN) /**< PWM:CH0启用时钟中断*/
#define _pwm_dis_ch0_timerint(pwm) _bit_clr(pwm->PIER, PWM_PIER_CH0INT_EN) /**< PWM:CH0禁用时钟中断*/
#define _pwm_en_ch1_timerint(pwm)  _bit_set(pwm->PIER, PWM_PIER_CH1INT_EN) /**< PWM:CH1启用时钟中断*/
#define _pwm_dis_ch1_timerint(pwm) _bit_clr(pwm->PIER, PWM_PIER_CH1INT_EN) /**< PWM:CH1禁用时钟中断*/
#define _pwm_en_ch2_timerint(pwm)  _bit_set(pwm->PIER, PWM_PIER_CH2INT_EN) /**< PWM:CH2启用时钟中断*/
#define _pwm_dis_ch2_timerint(pwm) _bit_clr(pwm->PIER, PWM_PIER_CH2INT_EN) /**< PWM:CH2禁用时钟中断*/
#define _pwm_en_ch3_timerint(pwm)  _bit_set(pwm->PIER, PWM_PIER_CH3INT_EN) /**< PWM:CH3启用时钟中断*/
#define _pwm_dis_ch3_timerint(pwm) _bit_clr(pwm->PIER, PWM_PIER_CH3INT_EN) /**< PWM:CH3禁用时钟中断*/
#define _pwm_en_ch4_timerint(pwm)  _bit_set(pwm->PIER, PWM_PIER_CH4INT_EN) /**< PWM:CH4启用时钟中断*/
#define _pwm_dis_ch4_timerint(pwm) _bit_clr(pwm->PIER, PWM_PIER_CH4INT_EN) /**< PWM:CH4禁用时钟中断*/
#define _pwm_en_ch5_timerint(pwm)  _bit_set(pwm->PIER, PWM_PIER_CH5INT_EN) /**< PWM:CH5启用时钟中断*/
#define _pwm_dis_ch5_timerint(pwm) _bit_clr(pwm->PIER, PWM_PIER_CH5INT_EN) /**< PWM:CH5禁用时钟中断*/
#define _pwm_en_ch6_timerint(pwm)  _bit_set(pwm->PIER, PWM_PIER_CH6INT_EN) /**< PWM:CH6启用时钟中断*/
#define _pwm_dis_ch6_timerint(pwm) _bit_clr(pwm->PIER, PWM_PIER_CH6INT_EN) /**< PWM:CH6禁用时钟中断*/
#define _pwm_en_ch7_timerint(pwm)  _bit_set(pwm->PIER, PWM_PIER_CH7INT_EN) /**< PWM:CH7启用时钟中断*/
#define _pwm_dis_ch7_timerint(pwm) _bit_clr(pwm->PIER, PWM_PIER_CH7INT_EN) /**< PWM:CH7禁用时钟中断*/
#define _pwm_get_pier(pwm)         _reg_read(pwm->PIER)                    /**< PWM:获取PIER寄存器值*/

#define _pwm_get_ch0_timer_intflag(pwm) _reg_chk(pwm->PIFR, PWM_PIFR_CH0INT) /**< PWM:CH0获取中断标志位*/
#define _pwm_get_ch1_timer_intflag(pwm) _reg_chk(pwm->PIFR, PWM_PIFR_CH1INT) /**< PWM:CH1获取中断标志位*/
#define _pwm_get_ch2_timer_intflag(pwm) _reg_chk(pwm->PIFR, PWM_PIFR_CH2INT) /**< PWM:CH2获取中断标志位*/
#define _pwm_get_ch3_timer_intflag(pwm) _reg_chk(pwm->PIFR, PWM_PIFR_CH3INT) /**< PWM:CH3获取中断标志位*/
#define _pwm_get_ch4_timer_intflag(pwm) _reg_chk(pwm->PIFR, PWM_PIFR_CH4INT) /**< PWM:CH4获取中断标志位*/
#define _pwm_get_ch5_timer_intflag(pwm) _reg_chk(pwm->PIFR, PWM_PIFR_CH5INT) /**< PWM:CH5获取中断标志位*/
#define _pwm_get_ch6_timer_intflag(pwm) _reg_chk(pwm->PIFR, PWM_PIFR_CH6INT) /**< PWM:CH6获取中断标志位*/
#define _pwm_get_ch7_timer_intflag(pwm) _reg_chk(pwm->PIFR, PWM_PIFR_CH7INT) /**< PWM:CH7获取中断标志位*/
#define _pwm_get_pifr(pwm)              _reg_read(pwm->PIFR)                 /**< PWM:获取PIFR寄存器值*/

#define _pwm_clr_ch0_timer_intflag(pwm) _reg_write(pwm->PIFR, PWM_PIFR_CH0INT) /**< PWM:CH0清除中断标志位*/
#define _pwm_clr_ch1_timer_intflag(pwm) _reg_write(pwm->PIFR, PWM_PIFR_CH1INT) /**< PWM:CH1清除中断标志位*/
#define _pwm_clr_ch2_timer_intflag(pwm) _reg_write(pwm->PIFR, PWM_PIFR_CH2INT) /**< PWM:CH2清除中断标志位*/
#define _pwm_clr_ch3_timer_intflag(pwm) _reg_write(pwm->PIFR, PWM_PIFR_CH3INT) /**< PWM:CH3清除中断标志位*/
#define _pwm_clr_ch4_timer_intflag(pwm) _reg_write(pwm->PIFR, PWM_PIFR_CH4INT) /**< PWM:CH4清除中断标志位*/
#define _pwm_clr_ch5_timer_intflag(pwm) _reg_write(pwm->PIFR, PWM_PIFR_CH5INT) /**< PWM:CH5清除中断标志位*/
#define _pwm_clr_ch6_timer_intflag(pwm) _reg_write(pwm->PIFR, PWM_PIFR_CH6INT) /**< PWM:CH6清除中断标志位*/
#define _pwm_clr_ch7_timer_intflag(pwm) _reg_write(pwm->PIFR, PWM_PIFR_CH7INT) /**< PWM:CH7清除中断标志位*/

#define _pwm_en_ch0_inv(pwm)          _bit_set(pwm->PCCR0, PWM_PCCR0_CH0_INV)   /**< PWM:CH0通道翻转使能*/
#define _pwm_dis_ch0_inv(pwm)         _bit_clr(pwm->PCCR0, PWM_PCCR0_CH0_INV)   /**< PWM:CH0通道翻转禁用*/
#define _pwm_en_ch0_rlint(pwm)        _bit_set(pwm->PCCR0, PWM_PCCR0_CH0_RLINT) /**< PWM:CH0上升沿中断使能*/
#define _pwm_dis_ch0_rlint(pwm)       _bit_clr(pwm->PCCR0, PWM_PCCR0_CH0_RLINT) /**< PWM:CH0上升沿中断禁用*/
#define _pwm_en_ch0_flint(pwm)        _bit_set(pwm->PCCR0, PWM_PCCR0_CH0_FLINT) /**< PWM:CH0下升沿中断使能*/
#define _pwm_dis_ch0_flint(pwm)       _bit_clr(pwm->PCCR0, PWM_PCCR0_CH0_FLINT) /**< PWM:CH0下升沿中断禁用*/
#define _pwm_en_ch0_capture(pwm)      _bit_set(pwm->PCCR0, PWM_PCCR0_CH0_CAPEN) /**< PWM:CH0捕获启用*/
#define _pwm_dis_ch0_capture(pwm)     _bit_clr(pwm->PCCR0, PWM_PCCR0_CH0_CAPEN) /**< PWM:CH0捕获禁用*/
#define _pwm_get_ch0_captureflag(pwm) _reg_chk(pwm->PCCR0, PWM_PCCR0_CH0_CAPIF) /**< PWM:CH0获取捕获标志位*/
#define _pwm_clr_ch0_captureflag(pwm) _bit_set(pwm->PCCR0, PWM_PCCR0_CH0_CAPIF) /**< PWM:CH0清除捕获标志位*/
#define _pwm_get_ch0_Risingflag(pwm)  _reg_chk(pwm->PCCR0, PWM_PCCR0_CH0_CRLRD) /**< PWM:CH0获取上升沿标志位*/
#define _pwm_clr_ch0_Risingflag(pwm)  _bit_set(pwm->PCCR0, PWM_PCCR0_CH0_CRLRD) /**< PWM:CH0清除上升沿标志位*/
#define _pwm_get_ch0_Fallingflag(pwm) _reg_chk(pwm->PCCR0, PWM_PCCR0_CH0_CFLRD) /**< PWM:CH0获取下降沿标志位*/
#define _pwm_clr_ch0_Fallingflag(pwm) _bit_set(pwm->PCCR0, PWM_PCCR0_CH0_CFLRD) /**< PWM:CH0清除下降沿标志位*/
#define _pwm_set_ch0_edgeinterrupt(pwm, value)                                                                         \
    _reg_modify(pwm->PCCR0,                                                                                            \
                ~(PWM_PCCR0_CH0_RLINT | PWM_PCCR0_CH0_FLINT | PWM_PCCR0_CH0_CAPEN),                                    \
                value)                                                          /**< PWM:CH0配置边沿中断*/
#define _pwm_en_ch1_inv(pwm)          _bit_set(pwm->PCCR0, PWM_PCCR0_CH1_INV)   /**< PWM:CH1通道翻转使能*/
#define _pwm_dis_ch1_inv(pwm)         _bit_clr(pwm->PCCR0, PWM_PCCR0_CH1_INV)   /**< PWM:CH1通道翻转禁用*/
#define _pwm_en_ch1_rlint(pwm)        _bit_set(pwm->PCCR0, PWM_PCCR0_CH1_RLINT) /**< PWM:CH1上升沿中断使能*/
#define _pwm_dis_ch1_rlint(pwm)       _bit_clr(pwm->PCCR0, PWM_PCCR0_CH1_RLINT) /**< PWM:CH1上升沿中断禁用*/
#define _pwm_en_ch1_flint(pwm)        _bit_set(pwm->PCCR0, PWM_PCCR0_CH1_FLINT) /**< PWM:CH1下升沿中断使能*/
#define _pwm_dis_ch1_flint(pwm)       _bit_clr(pwm->PCCR0, PWM_PCCR0_CH1_FLINT) /**< PWM:CH1下升沿中断禁用*/
#define _pwm_en_ch1_capture(pwm)      _bit_set(pwm->PCCR0, PWM_PCCR0_CH1_CAPEN) /**< PWM:CH1捕获启用*/
#define _pwm_dis_ch1_capture(pwm)     _bit_clr(pwm->PCCR0, PWM_PCCR0_CH1_CAPEN) /**< PWM:CH1捕获禁用*/
#define _pwm_get_ch1_captureflag(pwm) _reg_chk(pwm->PCCR0, PWM_PCCR0_CH1_CAPIF) /**< PWM:CH1获取捕获标志位*/
#define _pwm_clr_ch1_captureflag(pwm) _bit_set(pwm->PCCR0, PWM_PCCR0_CH1_CAPIF) /**< PWM:CH1清除捕获标志位*/
#define _pwm_get_ch1_Risingflag(pwm)  _reg_chk(pwm->PCCR0, PWM_PCCR0_CH1_CRLRD) /**< PWM:CH1获取上升沿标志位*/
#define _pwm_clr_ch1_Risingflag(pwm)  _bit_set(pwm->PCCR0, PWM_PCCR0_CH1_CRLRD) /**< PWM:CH1清除上升沿标志位*/
#define _pwm_get_ch1_Fallingflag(pwm) _reg_chk(pwm->PCCR0, PWM_PCCR0_CH1_CFLRD) /**< PWM:CH1获取下降沿标志位*/
#define _pwm_clr_ch1_Fallingflag(pwm) _bit_set(pwm->PCCR0, PWM_PCCR0_CH1_CFLRD) /**< PWM:CH1清除下降沿标志位*/
#define _pwm_set_ch1_edgeinterrupt(pwm, value)                                                                         \
    _reg_modify(pwm->PCCR0,                                                                                            \
                ~(PWM_PCCR0_CH1_RLINT | PWM_PCCR0_CH1_FLINT | PWM_PCCR0_CH1_CAPEN),                                    \
                value)                            /**< PWM:CH1配置边沿中断*/
#define _pwm_get_pccr0(pwm) _reg_read(pwm->PCCR0) /**< PWM:获取PCCR0寄存器值*/

#define _pwm_en_ch2_inv(pwm)          _bit_set(pwm->PCCR1, PWM_PCCR1_CH2_INV)   /**< PWM:CH2通道翻转使能*/
#define _pwm_dis_ch2_inv(pwm)         _bit_clr(pwm->PCCR1, PWM_PCCR1_CH2_INV)   /**< PWM:CH2通道翻转禁用*/
#define _pwm_en_ch2_rlint(pwm)        _bit_set(pwm->PCCR1, PWM_PCCR1_CH2_RLINT) /**< PWM:CH2上升沿中断使能*/
#define _pwm_dis_ch2_rlint(pwm)       _bit_clr(pwm->PCCR1, PWM_PCCR1_CH2_RLINT) /**< PWM:CH2上升沿中断禁用*/
#define _pwm_en_ch2_flint(pwm)        _bit_set(pwm->PCCR1, PWM_PCCR1_CH2_FLINT) /**< PWM:CH2下升沿中断使能*/
#define _pwm_dis_ch2_flint(pwm)       _bit_clr(pwm->PCCR1, PWM_PCCR1_CH2_FLINT) /**< PWM:CH2下升沿中断禁用*/
#define _pwm_en_ch2_capture(pwm)      _bit_set(pwm->PCCR1, PWM_PCCR1_CH2_CAPEN) /**< PWM:CH2捕获启用*/
#define _pwm_dis_ch2_capture(pwm)     _bit_clr(pwm->PCCR1, PWM_PCCR1_CH2_CAPEN) /**< PWM:CH2捕获禁用*/
#define _pwm_get_ch2_captureflag(pwm) _reg_chk(pwm->PCCR1, PWM_PCCR1_CH2_CAPIF) /**< PWM:CH2获取捕获标志位*/
#define _pwm_clr_ch2_captureflag(pwm) _bit_set(pwm->PCCR1, PWM_PCCR1_CH2_CAPIF) /**< PWM:CH2清除捕获标志位*/
#define _pwm_get_ch2_Risingflag(pwm)  _reg_chk(pwm->PCCR1, PWM_PCCR1_CH2_CRLRD) /**< PWM:CH2获取上升沿标志位*/
#define _pwm_clr_ch2_Risingflag(pwm)  _bit_set(pwm->PCCR1, PWM_PCCR1_CH2_CRLRD) /**< PWM:CH2清除上升沿标志位*/
#define _pwm_get_ch2_Fallingflag(pwm) _reg_chk(pwm->PCCR1, PWM_PCCR1_CH2_CFLRD) /**< PWM:CH2获取下降沿标志位*/
#define _pwm_clr_ch2_Fallingflag(pwm) _bit_set(pwm->PCCR1, PWM_PCCR1_CH2_CFLRD) /**< PWM:CH2清除下降沿标志位*/
#define _pwm_set_ch2_edgeinterrupt(pwm, value)                                                                         \
    _reg_modify(pwm->PCCR1,                                                                                            \
                ~(PWM_PCCR1_CH2_RLINT | PWM_PCCR1_CH2_FLINT | PWM_PCCR1_CH2_CAPEN),                                    \
                value)                                                          /**< PWM:CH2配置边沿中断*/
#define _pwm_en_ch3_inv(pwm)          _bit_set(pwm->PCCR1, PWM_PCCR1_CH3_INV)   /**< PWM:CH3通道翻转使能*/
#define _pwm_dis_ch3_inv(pwm)         _bit_clr(pwm->PCCR1, PWM_PCCR1_CH3_INV)   /**< PWM:CH3通道翻转禁用*/
#define _pwm_en_ch3_rlint(pwm)        _bit_set(pwm->PCCR1, PWM_PCCR1_CH3_RLINT) /**< PWM:CH3上升沿中断使能*/
#define _pwm_dis_ch3_rlint(pwm)       _bit_clr(pwm->PCCR1, PWM_PCCR1_CH3_RLINT) /**< PWM:CH3上升沿中断禁用*/
#define _pwm_en_ch3_flint(pwm)        _bit_set(pwm->PCCR1, PWM_PCCR1_CH3_FLINT) /**< PWM:CH3下升沿中断使能*/
#define _pwm_dis_ch3_flint(pwm)       _bit_clr(pwm->PCCR1, PWM_PCCR1_CH3_FLINT) /**< PWM:CH3下升沿中断禁用*/
#define _pwm_en_ch3_capture(pwm)      _bit_set(pwm->PCCR1, PWM_PCCR1_CH3_CAPEN) /**< PWM:CH3捕获启用*/
#define _pwm_dis_ch3_capture(pwm)     _bit_clr(pwm->PCCR1, PWM_PCCR1_CH3_CAPEN) /**< PWM:CH3捕获禁用*/
#define _pwm_get_ch3_captureflag(pwm) _reg_chk(pwm->PCCR1, PWM_PCCR1_CH3_CAPIF) /**< PWM:CH3获取捕获标志位*/
#define _pwm_clr_ch3_captureflag(pwm) _bit_set(pwm->PCCR1, PWM_PCCR1_CH3_CAPIF) /**< PWM:CH3清除捕获标志位*/
#define _pwm_get_ch3_Risingflag(pwm)  _reg_chk(pwm->PCCR1, PWM_PCCR1_CH3_CRLRD) /**< PWM:CH3获取上升沿标志位*/
#define _pwm_clr_ch3_Risingflag(pwm)  _bit_set(pwm->PCCR1, PWM_PCCR1_CH3_CRLRD) /**< PWM:CH3清除上升沿标志位*/
#define _pwm_get_ch3_Fallingflag(pwm) _reg_chk(pwm->PCCR1, PWM_PCCR1_CH3_CFLRD) /**< PWM:CH3获取下降沿标志位*/
#define _pwm_clr_ch3_Fallingflag(pwm) _bit_set(pwm->PCCR1, PWM_PCCR1_CH3_CFLRD) /**< PWM:CH3清除下降沿标志位*/
#define _pwm_set_ch3_edgeinterrupt(pwm, value)                                                                         \
    _reg_modify(pwm->PCCR1,                                                                                            \
                ~(PWM_PCCR1_CH3_RLINT | PWM_PCCR1_CH3_FLINT | PWM_PCCR1_CH3_CAPEN),                                    \
                value)                            /**< PWM:CH3配置边沿中断*/
#define _pwm_get_pccr1(pwm) _reg_read(pwm->PCCR1) /**< PWM:获取PCCR1寄存器值*/

#define _pwm_en_ch4_inv(pwm)          _bit_set(pwm->PCCR2, PWM_PCCR2_CH4_INV)   /**< PWM:CH4通道翻转使能*/
#define _pwm_dis_ch4_inv(pwm)         _bit_clr(pwm->PCCR2, PWM_PCCR2_CH4_INV)   /**< PWM:CH4通道翻转禁用*/
#define _pwm_en_ch4_rlint(pwm)        _bit_set(pwm->PCCR2, PWM_PCCR2_CH4_RLINT) /**< PWM:CH4上升沿中断使能*/
#define _pwm_dis_ch4_rlint(pwm)       _bit_clr(pwm->PCCR2, PWM_PCCR2_CH4_RLINT) /**< PWM:CH4上升沿中断禁用*/
#define _pwm_en_ch4_flint(pwm)        _bit_set(pwm->PCCR2, PWM_PCCR2_CH4_FLINT) /**< PWM:CH4下升沿中断使能*/
#define _pwm_dis_ch4_flint(pwm)       _bit_clr(pwm->PCCR2, PWM_PCCR2_CH4_FLINT) /**< PWM:CH4下升沿中断禁用*/
#define _pwm_en_ch4_capture(pwm)      _bit_set(pwm->PCCR2, PWM_PCCR2_CH4_CAPEN) /**< PWM:CH4捕获启用*/
#define _pwm_dis_ch4_capture(pwm)     _bit_clr(pwm->PCCR2, PWM_PCCR2_CH4_CAPEN) /**< PWM:CH4捕获禁用*/
#define _pwm_get_ch4_captureflag(pwm) _reg_chk(pwm->PCCR2, PWM_PCCR2_CH4_CAPIF) /**< PWM:CH4获取捕获标志位*/
#define _pwm_clr_ch4_captureflag(pwm) _bit_set(pwm->PCCR2, PWM_PCCR2_CH4_CAPIF) /**< PWM:CH4清除捕获标志位*/
#define _pwm_get_ch4_Risingflag(pwm)  _reg_chk(pwm->PCCR2, PWM_PCCR2_CH4_CRLRD) /**< PWM:CH4获取上升沿标志位*/
#define _pwm_clr_ch4_Risingflag(pwm)  _bit_set(pwm->PCCR2, PWM_PCCR2_CH4_CRLRD) /**< PWM:CH4清除上升沿标志位*/
#define _pwm_get_ch4_Fallingflag(pwm) _reg_chk(pwm->PCCR2, PWM_PCCR2_CH4_CFLRD) /**< PWM:CH4获取下降沿标志位*/
#define _pwm_clr_ch4_Fallingflag(pwm) _bit_set(pwm->PCCR2, PWM_PCCR2_CH4_CFLRD) /**< PWM:CH4清除下降沿标志位*/
#define _pwm_set_ch4_edgeinterrupt(pwm, value)                                                                         \
    _reg_modify(pwm->PCCR2,                                                                                            \
                ~(PWM_PCCR2_CH4_RLINT | PWM_PCCR2_CH4_FLINT | PWM_PCCR2_CH4_CAPEN),                                    \
                value)                                                          /**< PWM:CH4配置边沿中断*/
#define _pwm_en_ch5_inv(pwm)          _bit_set(pwm->PCCR2, PWM_PCCR2_CH5_INV)   /**< PWM:CH5通道翻转使能*/
#define _pwm_dis_ch5_inv(pwm)         _bit_clr(pwm->PCCR2, PWM_PCCR2_CH5_INV)   /**< PWM:CH5通道翻转禁用*/
#define _pwm_en_ch5_rlint(pwm)        _bit_set(pwm->PCCR2, PWM_PCCR2_CH5_RLINT) /**< PWM:CH5上升沿中断使能*/
#define _pwm_dis_ch5_rlint(pwm)       _bit_clr(pwm->PCCR2, PWM_PCCR2_CH5_RLINT) /**< PWM:CH5上升沿中断禁用*/
#define _pwm_en_ch5_flint(pwm)        _bit_set(pwm->PCCR2, PWM_PCCR2_CH5_FLINT) /**< PWM:CH5下升沿中断使能*/
#define _pwm_dis_ch5_flint(pwm)       _bit_clr(pwm->PCCR2, PWM_PCCR2_CH5_FLINT) /**< PWM:CH5下升沿中断禁用*/
#define _pwm_en_ch5_capture(pwm)      _bit_set(pwm->PCCR2, PWM_PCCR2_CH5_CAPEN) /**< PWM:CH5捕获启用*/
#define _pwm_dis_ch5_capture(pwm)     _bit_clr(pwm->PCCR2, PWM_PCCR2_CH5_CAPEN) /**< PWM:CH5捕获禁用*/
#define _pwm_get_ch5_captureflag(pwm) _reg_chk(pwm->PCCR2, PWM_PCCR2_CH5_CAPIF) /**< PWM:CH5获取捕获标志位*/
#define _pwm_clr_ch5_captureflag(pwm) _bit_set(pwm->PCCR2, PWM_PCCR2_CH5_CAPIF) /**< PWM:CH5清除捕获标志位*/
#define _pwm_get_ch5_Risingflag(pwm)  _reg_chk(pwm->PCCR2, PWM_PCCR2_CH5_CRLRD) /**< PWM:CH5获取上升沿标志位*/
#define _pwm_clr_ch5_Risingflag(pwm)  _bit_set(pwm->PCCR2, PWM_PCCR2_CH5_CRLRD) /**< PWM:CH5清除上升沿标志位*/
#define _pwm_get_ch5_Fallingflag(pwm) _reg_chk(pwm->PCCR2, PWM_PCCR2_CH5_CFLRD) /**< PWM:CH5获取下降沿标志位*/
#define _pwm_clr_ch5_Fallingflag(pwm) _bit_set(pwm->PCCR2, PWM_PCCR2_CH5_CFLRD) /**< PWM:CH5清除下降沿标志位*/
#define _pwm_set_ch5_edgeinterrupt(pwm, value)                                                                         \
    _reg_modify(pwm->PCCR2,                                                                                            \
                ~(PWM_PCCR2_CH5_RLINT | PWM_PCCR2_CH5_FLINT | PWM_PCCR2_CH5_CAPEN),                                    \
                value)                            /**< PWM:CH5配置边沿中断*/
#define _pwm_get_pccr2(pwm) _reg_read(pwm->PCCR2) /**< PWM:获取PCCR2寄存器值*/

#define _pwm_en_ch6_inv(pwm)          _bit_set(pwm->PCCR3, PWM_PCCR3_CH6_INV)   /**< PWM:CH6通道翻转使能*/
#define _pwm_dis_ch6_inv(pwm)         _bit_clr(pwm->PCCR3, PWM_PCCR3_CH6_INV)   /**< PWM:CH6通道翻转禁用*/
#define _pwm_en_ch6_rlint(pwm)        _bit_set(pwm->PCCR3, PWM_PCCR3_CH6_RLINT) /**< PWM:CH6上升沿中断使能*/
#define _pwm_dis_ch6_rlint(pwm)       _bit_clr(pwm->PCCR3, PWM_PCCR3_CH6_RLINT) /**< PWM:CH6上升沿中断禁用*/
#define _pwm_en_ch6_flint(pwm)        _bit_set(pwm->PCCR3, PWM_PCCR3_CH6_FLINT) /**< PWM:CH6下升沿中断使能*/
#define _pwm_dis_ch6_flint(pwm)       _bit_clr(pwm->PCCR3, PWM_PCCR3_CH6_FLINT) /**< PWM:CH6下升沿中断禁用*/
#define _pwm_en_ch6_capture(pwm)      _bit_set(pwm->PCCR3, PWM_PCCR3_CH6_CAPEN) /**< PWM:CH6捕获启用*/
#define _pwm_dis_ch6_capture(pwm)     _bit_clr(pwm->PCCR3, PWM_PCCR3_CH6_CAPEN) /**< PWM:CH6捕获禁用*/
#define _pwm_get_ch6_captureflag(pwm) _reg_chk(pwm->PCCR3, PWM_PCCR3_CH6_CAPIF) /**< PWM:CH6获取捕获标志位*/
#define _pwm_clr_ch6_captureflag(pwm) _bit_set(pwm->PCCR3, PWM_PCCR3_CH6_CAPIF) /**< PWM:CH6清除捕获标志位*/
#define _pwm_get_ch6_Risingflag(pwm)  _reg_chk(pwm->PCCR3, PWM_PCCR3_CH6_CRLRD) /**< PWM:CH6获取上升沿标志位*/
#define _pwm_clr_ch6_Risingflag(pwm)  _bit_set(pwm->PCCR3, PWM_PCCR3_CH6_CRLRD) /**< PWM:CH6清除上升沿标志位*/
#define _pwm_get_ch6_Fallingflag(pwm) _reg_chk(pwm->PCCR3, PWM_PCCR3_CH6_CFLRD) /**< PWM:CH6获取下降沿标志位*/
#define _pwm_clr_ch6_Fallingflag(pwm) _bit_set(pwm->PCCR3, PWM_PCCR3_CH6_CFLRD) /**< PWM:CH6清除下降沿标志位*/
#define _pwm_set_ch6_edgeinterrupt(pwm, value)                                                                         \
    _reg_modify(pwm->PCCR3,                                                                                            \
                ~(PWM_PCCR3_CH6_RLINT | PWM_PCCR3_CH6_FLINT | PWM_PCCR3_CH6_CAPEN),                                    \
                value)                                                          /**< PWM:CH6配置边沿中断*/
#define _pwm_en_ch7_inv(pwm)          _bit_set(pwm->PCCR3, PWM_PCCR3_CH7_INV)   /**< PWM:CH7通道翻转使能*/
#define _pwm_dis_ch7_inv(pwm)         _bit_clr(pwm->PCCR3, PWM_PCCR3_CH7_INV)   /**< PWM:CH7通道翻转禁用*/
#define _pwm_en_ch7_rlint(pwm)        _bit_set(pwm->PCCR3, PWM_PCCR3_CH7_RLINT) /**< PWM:CH7上升沿中断使能*/
#define _pwm_dis_ch7_rlint(pwm)       _bit_clr(pwm->PCCR3, PWM_PCCR3_CH7_RLINT) /**< PWM:CH7上升沿中断禁用*/
#define _pwm_en_ch7_flint(pwm)        _bit_set(pwm->PCCR3, PWM_PCCR3_CH7_FLINT) /**< PWM:CH7下升沿中断使能*/
#define _pwm_dis_ch7_flint(pwm)       _bit_clr(pwm->PCCR3, PWM_PCCR3_CH7_FLINT) /**< PWM:CH7下升沿中断禁用*/
#define _pwm_en_ch7_capture(pwm)      _bit_set(pwm->PCCR3, PWM_PCCR3_CH7_CAPEN) /**< PWM:CH7捕获启用*/
#define _pwm_dis_ch7_capture(pwm)     _bit_clr(pwm->PCCR3, PWM_PCCR3_CH7_CAPEN) /**< PWM:CH7捕获禁用*/
#define _pwm_get_ch7_captureflag(pwm) _reg_chk(pwm->PCCR3, PWM_PCCR3_CH7_CAPIF) /**< PWM:CH7获取捕获标志位*/
#define _pwm_clr_ch7_captureflag(pwm) _bit_set(pwm->PCCR3, PWM_PCCR3_CH7_CAPIF) /**< PWM:CH7清除捕获标志位*/
#define _pwm_get_ch7_Risingflag(pwm)  _reg_chk(pwm->PCCR3, PWM_PCCR3_CH7_CRLRD) /**< PWM:CH7获取上升沿标志位*/
#define _pwm_clr_ch7_Risingflag(pwm)  _bit_set(pwm->PCCR3, PWM_PCCR3_CH7_CRLRD) /**< PWM:CH7清除上升沿标志位*/
#define _pwm_get_ch7_Fallingflag(pwm) _reg_chk(pwm->PCCR3, PWM_PCCR3_CH7_CFLRD) /**< PWM:CH7获取下降沿标志位*/
#define _pwm_clr_ch7_Fallingflag(pwm) _bit_set(pwm->PCCR3, PWM_PCCR3_CH7_CFLRD) /**< PWM:CH7清除下降沿标志位*/
#define _pwm_set_ch7_edgeinterrupt(pwm, value)                                                                         \
    _reg_modify(pwm->PCCR3,                                                                                            \
                ~(PWM_PCCR3_CH7_RLINT | PWM_PCCR3_CH7_FLINT | PWM_PCCR3_CH7_CAPEN),                                    \
                value)                            /**< PWM:CH7配置边沿中断*/
#define _pwm_get_pccr3(pwm) _reg_read(pwm->PCCR3) /**< PWM:获取PCCR3寄存器值*/

#define _pwm_get_pcrlr0(pwm) _reg_read(pwm->PCRLR0) /**< PWM:获取PCRLR0寄存器值*/
#define _pwm_get_pcrlr1(pwm) _reg_read(pwm->PCRLR1) /**< PWM:获取PCRLR1寄存器值*/
#define _pwm_get_pcrlr2(pwm) _reg_read(pwm->PCRLR2) /**< PWM:获取PCRLR2寄存器值*/
#define _pwm_get_pcrlr3(pwm) _reg_read(pwm->PCRLR3) /**< PWM:获取PCRLR3寄存器值*/
#define _pwm_get_pcrlr4(pwm) _reg_read(pwm->PCRLR4) /**< PWM:获取PCRLR4寄存器值*/
#define _pwm_get_pcrlr5(pwm) _reg_read(pwm->PCRLR5) /**< PWM:获取PCRLR5寄存器值*/
#define _pwm_get_pcrlr6(pwm) _reg_read(pwm->PCRLR6) /**< PWM:获取PCRLR6寄存器值*/
#define _pwm_get_pcrlr7(pwm) _reg_read(pwm->PCRLR7) /**< PWM:获取PCRLR7寄存器值*/

#define _pwm_get_pcflr0(pwm) _reg_read(pwm->PCFLR0) /**< PWM:获取PCFLR0寄存器值*/
#define _pwm_get_pcflr1(pwm) _reg_read(pwm->PCFLR1) /**< PWM:获取PCFLR1寄存器值*/
#define _pwm_get_pcflr2(pwm) _reg_read(pwm->PCFLR2) /**< PWM:获取PCFLR2寄存器值*/
#define _pwm_get_pcflr3(pwm) _reg_read(pwm->PCFLR3) /**< PWM:获取PCFLR3寄存器值*/
#define _pwm_get_pcflr4(pwm) _reg_read(pwm->PCFLR4) /**< PWM:获取PCFLR4寄存器值*/
#define _pwm_get_pcflr5(pwm) _reg_read(pwm->PCFLR5) /**< PWM:获取PCFLR5寄存器值*/
#define _pwm_get_pcflr6(pwm) _reg_read(pwm->PCFLR6) /**< PWM:获取PCFLR6寄存器值*/
#define _pwm_get_pcflr7(pwm) _reg_read(pwm->PCFLR7) /**< PWM:获取PCFLR7寄存器值*/

#define _pwm_set_ch0_output(pwm) _bit_set(pwm->PPCR, PWM_PPCR_CH0_OUTPUT) /**< PWM:CH0配置方向属性为输出*/
#define _pwm_set_ch0_input(pwm)  _bit_clr(pwm->PPCR, PWM_PPCR_CH0_OUTPUT) /**< PWM:CH0配置方向属性为输入*/
#define _pwm_set_ch1_output(pwm) _bit_set(pwm->PPCR, PWM_PPCR_CH1_OUTPUT) /**< PWM:CH1配置方向属性为输出*/
#define _pwm_set_ch1_input(pwm)  _bit_clr(pwm->PPCR, PWM_PPCR_CH1_OUTPUT) /**< PWM:CH1配置方向属性为输入*/
#define _pwm_set_ch2_output(pwm) _bit_set(pwm->PPCR, PWM_PPCR_CH2_OUTPUT) /**< PWM:CH2配置方向属性为输出*/
#define _pwm_set_ch2_input(pwm)  _bit_clr(pwm->PPCR, PWM_PPCR_CH2_OUTPUT) /**< PWM:CH2配置方向属性为输入*/
#define _pwm_set_ch3_output(pwm) _bit_set(pwm->PPCR, PWM_PPCR_CH3_OUTPUT) /**< PWM:CH3配置方向属性为输出*/
#define _pwm_set_ch3_input(pwm)  _bit_clr(pwm->PPCR, PWM_PPCR_CH3_OUTPUT) /**< PWM:CH3配置方向属性为输入*/
#define _pwm_set_ch4_output(pwm) _bit_set(pwm->PPCR, PWM_PPCR_CH4_OUTPUT) /**< PWM:CH4配置方向属性为输出*/
#define _pwm_set_ch4_input(pwm)  _bit_clr(pwm->PPCR, PWM_PPCR_CH4_OUTPUT) /**< PWM:CH4配置方向属性为输入*/
#define _pwm_set_ch5_output(pwm) _bit_set(pwm->PPCR, PWM_PPCR_CH5_OUTPUT) /**< PWM:CH5配置方向属性为输出*/
#define _pwm_set_ch5_input(pwm)  _bit_clr(pwm->PPCR, PWM_PPCR_CH5_OUTPUT) /**< PWM:CH5配置方向属性为输入*/
#define _pwm_set_ch6_output(pwm) _bit_set(pwm->PPCR, PWM_PPCR_CH6_OUTPUT) /**< PWM:CH6配置方向属性为输出*/
#define _pwm_set_ch6_input(pwm)  _bit_clr(pwm->PPCR, PWM_PPCR_CH6_OUTPUT) /**< PWM:CH6配置方向属性为输入*/
#define _pwm_set_ch7_output(pwm) _bit_set(pwm->PPCR, PWM_PPCR_CH7_OUTPUT) /**< PWM:CH7配置方向属性为输出*/
#define _pwm_set_ch7_input(pwm)  _bit_clr(pwm->PPCR, PWM_PPCR_CH7_OUTPUT) /**< PWM:CH7配置方向属性为输入*/
#define _pwm_en_ch0_pullup(pwm)  _bit_set(pwm->PPCR, PWM_PPCR_CH0_PULLUP) /**< PWM:CH0配置拉高使能*/
#define _pwm_dis_ch0_pullup(pwm) _bit_clr(pwm->PPCR, PWM_PPCR_CH0_PULLUP) /**< PWM:CH0配置拉高禁用*/
#define _pwm_en_ch1_pullup(pwm)  _bit_set(pwm->PPCR, PWM_PPCR_CH1_PULLUP) /**< PWM:CH1配置拉高使能*/
#define _pwm_dis_ch1_pullup(pwm) _bit_clr(pwm->PPCR, PWM_PPCR_CH1_PULLUP) /**< PWM:CH1配置拉高禁用*/
#define _pwm_en_ch2_pullup(pwm)  _bit_set(pwm->PPCR, PWM_PPCR_CH2_PULLUP) /**< PWM:CH2配置拉高使能*/
#define _pwm_dis_ch2_pullup(pwm) _bit_clr(pwm->PPCR, PWM_PPCR_CH2_PULLUP) /**< PWM:CH2配置拉高禁用*/
#define _pwm_en_ch3_pullup(pwm)  _bit_set(pwm->PPCR, PWM_PPCR_CH3_PULLUP) /**< PWM:CH3配置拉高使能*/
#define _pwm_dis_ch3_pullup(pwm) _bit_clr(pwm->PPCR, PWM_PPCR_CH3_PULLUP) /**< PWM:CH3配置拉高禁用*/
#define _pwm_en_ch4_pullup(pwm)  _bit_set(pwm->PPCR, PWM_PPCR_CH4_PULLUP) /**< PWM:CH4配置拉高使能*/
#define _pwm_dis_ch4_pullup(pwm) _bit_clr(pwm->PPCR, PWM_PPCR_CH4_PULLUP) /**< PWM:CH4配置拉高禁用*/
#define _pwm_en_ch5_pullup(pwm)  _bit_set(pwm->PPCR, PWM_PPCR_CH5_PULLUP) /**< PWM:CH5配置拉高使能*/
#define _pwm_dis_ch5_pullup(pwm) _bit_clr(pwm->PPCR, PWM_PPCR_CH5_PULLUP) /**< PWM:CH5配置拉高禁用*/
#define _pwm_en_ch6_pullup(pwm)  _bit_set(pwm->PPCR, PWM_PPCR_CH6_PULLUP) /**< PWM:CH6配置拉高使能*/
#define _pwm_dis_ch6_pullup(pwm) _bit_clr(pwm->PPCR, PWM_PPCR_CH6_PULLUP) /**< PWM:CH6配置拉高禁用*/
#define _pwm_en_ch7_pullup(pwm)  _bit_set(pwm->PPCR, PWM_PPCR_CH7_PULLUP) /**< PWM:CH7配置拉高使能*/
#define _pwm_dis_ch7_pullup(pwm) _bit_clr(pwm->PPCR, PWM_PPCR_CH7_PULLUP) /**< PWM:CH7配置拉高禁用*/
#define _pwm_set_ch0_bit(pwm)    _bit_set(pwm->PPCR, PWM_PPCR_CH0_BITSET) /**< PWM:CH0配置为高电平*/
#define _pwm_clr_ch0_bit(pwm)    _bit_clr(pwm->PPCR, PWM_PPCR_CH0_BITSET) /**< PWM:CH0配置为低电平*/
#define _pwm_set_ch1_bit(pwm)    _bit_set(pwm->PPCR, PWM_PPCR_CH1_BITSET) /**< PWM:CH1配置为高电平*/
#define _pwm_clr_ch1_bit(pwm)    _bit_clr(pwm->PPCR, PWM_PPCR_CH1_BITSET) /**< PWM:CH1配置为低电平*/
#define _pwm_set_ch2_bit(pwm)    _bit_set(pwm->PPCR, PWM_PPCR_CH2_BITSET) /**< PWM:CH2配置为高电平*/
#define _pwm_clr_ch2_bit(pwm)    _bit_clr(pwm->PPCR, PWM_PPCR_CH2_BITSET) /**< PWM:CH2配置为低电平*/
#define _pwm_set_ch3_bit(pwm)    _bit_set(pwm->PPCR, PWM_PPCR_CH3_BITSET) /**< PWM:CH3配置为高电平*/
#define _pwm_clr_ch3_bit(pwm)    _bit_clr(pwm->PPCR, PWM_PPCR_CH3_BITSET) /**< PWM:CH3配置为低电平*/
#define _pwm_set_ch4_bit(pwm)    _bit_set(pwm->PPCR, PWM_PPCR_CH4_BITSET) /**< PWM:CH4配置为高电平*/
#define _pwm_clr_ch4_bit(pwm)    _bit_clr(pwm->PPCR, PWM_PPCR_CH4_BITSET) /**< PWM:CH4配置为低电平*/
#define _pwm_set_ch5_bit(pwm)    _bit_set(pwm->PPCR, PWM_PPCR_CH5_BITSET) /**< PWM:CH5配置为高电平*/
#define _pwm_clr_ch5_bit(pwm)    _bit_clr(pwm->PPCR, PWM_PPCR_CH5_BITSET) /**< PWM:CH5配置为低电平*/
#define _pwm_set_ch6_bit(pwm)    _bit_set(pwm->PPCR, PWM_PPCR_CH6_BITSET) /**< PWM:CH6配置为高电平*/
#define _pwm_clr_ch6_bit(pwm)    _bit_clr(pwm->PPCR, PWM_PPCR_CH6_BITSET) /**< PWM:CH6配置为低电平*/
#define _pwm_set_ch7_bit(pwm)    _bit_set(pwm->PPCR, PWM_PPCR_CH7_BITSET) /**< PWM:CH7配置为高电平*/
#define _pwm_clr_ch7_bit(pwm)    _bit_clr(pwm->PPCR, PWM_PPCR_CH7_BITSET) /**< PWM:CH7配置为低电平*/
#define _pwm_chk_ch0_bit(pwm)    _reg_chk(pwm->PPCR, PWM_PPCR_CH0_BITSET) /**< PWM:CH0检测为高电平*/
#define _pwm_chk_ch1_bit(pwm)    _reg_chk(pwm->PPCR, PWM_PPCR_CH1_BITSET) /**< PWM:CH1检测为低电平*/
#define _pwm_chk_ch2_bit(pwm)    _reg_chk(pwm->PPCR, PWM_PPCR_CH2_BITSET) /**< PWM:CH2检测为高电平*/
#define _pwm_chk_ch3_bit(pwm)    _reg_chk(pwm->PPCR, PWM_PPCR_CH3_BITSET) /**< PWM:CH3检测为低电平*/
#define _pwm_chk_ch4_bit(pwm)    _reg_chk(pwm->PPCR, PWM_PPCR_CH4_BITSET) /**< PWM:CH4检测为高电平*/
#define _pwm_chk_ch5_bit(pwm)    _reg_chk(pwm->PPCR, PWM_PPCR_CH5_BITSET) /**< PWM:CH5检测为低电平*/
#define _pwm_chk_ch6_bit(pwm)    _reg_chk(pwm->PPCR, PWM_PPCR_CH6_BITSET) /**< PWM:CH6检测为高电平*/
#define _pwm_chk_ch7_bit(pwm)    _reg_chk(pwm->PPCR, PWM_PPCR_CH7_BITSET) /**< PWM:CH7检测为低电平*/
#define _pwm_get_ppcr(pwm)       _reg_read(pwm->PPCR)                     /**< PWM:获取PPCR寄存器值*/

/**
 * @}
 */

/*** 结构体、枚举变量定义 *****************************************************/
#ifdef FILE_OUT
/** @defgroup DRV_PWM_Exported_Types Exported Types
 *
 * @{
 */
#endif

/**
 * @brief PWM分频系数定义
 */
typedef enum
{
    PWM_CLK_DIV_2 = 0,
    PWM_CLK_DIV_4,
    PWM_CLK_DIV_8,
    PWM_CLK_DIV_16,
    PWM_CLK_DIV_1,
} PWM_CLKDIV;

/**
 * @}
 */

/*** 全局变量声明 **************************************************************/
#ifdef FILE_OUT
/** @defgroup DRV_PWM_Exported_Variables Exported Variables
 *
 * @{
 */
#endif

/**
 * @}
 */

/*** 函数声明 ******************************************************************/
#ifdef FILE_OUT
/** @defgroup DRV_PWM_Exported_Functions Exported Functions
 * @{
 */
#endif
extern uint8_t DRV_PWM_CHxCmd(PWM_TypeDef *ppwm, uint8_t chx, FunctionalStateTypeDef status);

extern uint8_t DRV_PWM_CHxTimerInverteCmd(PWM_TypeDef *ppwm, uint8_t chx, FunctionalStateTypeDef status);

extern uint8_t DRV_PWM_CHxDeadZoneCmd(PWM_TypeDef *ppwm, uint8_t chx, FunctionalStateTypeDef status);

extern uint8_t DRV_PWM_ConfigCHxDeadZone(PWM_TypeDef *ppwm, uint8_t chx, uint8_t value);

extern uint8_t DRV_PWM_CHxAutoLoadMode(PWM_TypeDef *ppwm, uint8_t chx, uint8_t mode);

extern uint8_t DRV_PWM_ConfigCHxPeriod(PWM_TypeDef *ppwm, uint8_t chx, uint16_t cnt);

extern uint16_t DRV_PWM_GetCHxPeriod(PWM_TypeDef *ppwm, uint8_t chx);

extern uint8_t DRV_PWM_ConfigCHxWidth(PWM_TypeDef *ppwm, uint8_t chx, uint16_t cnt);

extern uint16_t DRV_PWM_GetCHxWidth(PWM_TypeDef *ppwm, uint8_t chx);

extern uint16_t DRV_PWM_GetCHxTimerCounter(PWM_TypeDef *ppwm, uint8_t chx);

extern uint8_t DRV_PWM_CHxTimerInterruptCmd(PWM_TypeDef *ppwm, uint8_t chx, FunctionalStateTypeDef status);

extern uint8_t DRV_PWM_GetCHxTimerInterrupt(PWM_TypeDef *ppwm, uint8_t chx);

extern uint8_t DRV_PWM_CHxInverteCmd(PWM_TypeDef *ppwm, uint8_t chx, FunctionalStateTypeDef status);

extern uint8_t DRV_PWM_CHxRisingInterruptCmd(PWM_TypeDef *ppwm, uint8_t chx, FunctionalStateTypeDef status);

extern uint8_t DRV_PWM_CHxFallinginterruptCmd(PWM_TypeDef *ppwm, uint8_t chx, FunctionalStateTypeDef status);

extern uint8_t DRV_PWM_CHxCaptureCmd(PWM_TypeDef *ppwm, uint8_t chx, FunctionalStateTypeDef status);

extern uint8_t DRV_PWM_ConfigCHxEdgeinterrupt(PWM_TypeDef *ppwm, uint8_t chx, uint32_t edge);

extern uint8_t DRV_PWM_SetCHxDirection(PWM_TypeDef *ppwm, uint8_t chx, uint8_t dir);

extern uint8_t DRV_PWM_CHxPullupCmd(PWM_TypeDef *ppwm, uint8_t chx, FunctionalStateTypeDef status);

extern uint8_t DRV_PWM_SetCHxLevel(PWM_TypeDef *ppwm, uint8_t chx, uint8_t level);

extern uint8_t DRV_PWM_GetCHxLevel(PWM_TypeDef *ppwm, uint8_t chx);

extern uint8_t DRV_PWM_ConfigCHxPerscaler(PWM_TypeDef *ppwm, uint8_t chx, uint8_t value);

extern uint8_t DRV_PWM_ConfigCHxClkDiv(PWM_TypeDef *ppwm, uint8_t chx, PWM_CLKDIV div);
/**
 * @}
 */

/**
 *@}
 */

/**
 *@}
 */

#ifdef __cplusplus
}
#endif

#endif /* __PWM_DRV_H */
