/**********************************************************
 * @Copyright (c) 2021   Levetop Semiconductor Co., Ltd.
 * @File			dcmi_drv.h
 * @Author		Howard
 * @Date			2021/12/30
 * @Time			10:04:32
 * @Version
 * @Brief
 **********************************************************
 * Modification History
 *
 *
 **********************************************************/
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __DCMI_DRV_H
#define __DCMI_DRV_H

#ifdef __cplusplus
extern "C"
{
#endif

/* Includes ------------------------------------------------------------------*/
#include "lt776_reg.h"
//#include "drv_gpio_def.h"
#include "type.h"
#ifdef FILE_OUT
/**@defgroup DRV 3 HAL driver
 *
 *@{
 */

/** @defgroup DRV_DCMI DCMI
 *
 *@{
 */
#endif
/*** 宏定义 *******************************************************************/
#ifdef FILE_OUT
/** @defgroup DRV_DCMI_Exported_Macros Exported Macros
 *
 * @{
 */
#endif

/**
 * @brief  DCMI 常用宏常量
 *
 */
#define DCMI_ENABLE (1 << 14)
#define CAPTURE (1 << 0)

#define EDMA_ENABLE ((uint32_t)1 << 31)
#define IDMA_ENABLE (1 << 0)

#define RGB_BGR_1 (1 << 12)
#define DVP_1 (1 << 11)
#define RAW_PROC_EN (1 << 3)
#define RGB_PROC_EN (1 << 2)
#define YUV_PROC_EN (1 << 1)
#define Y2EXTDMA_EN (1 << 0)

#define IDMA_FIXED_ADDR (1 << 30)

    //#define SNSR_DATA_FORMAT_YUV422     (5<<4)
    //#define SNSR_DATA_FORMAT_YUV420     (7<<4)
    //#define SNSR_DATA_FORMAT_RGB565     (2<<4)
    //#define SNSR_DATA_FORMAT_RGB888     (4<<4)

    //#define DISP_DATA_FORMAT_RGB565 (0<<9)
    //#define DISP_DATA_FORMAT_OTHER  (7<<9)

#define SNSR_DATA_FORMAT_YUV422 (5 << 4)
#define SNSR_DATA_FORMAT_YUV420 (7 << 4)
#define SNSR_DATA_FORMAT_RGB565 (2 << 4)
#define SNSR_DATA_FORMAT_RGB888 (4 << 4)
#define SNSR_DATA_FORMAT_OTHER (0x10 << 4)

#define DISP_DATA_FORMAT_YUV422 (3 << 9)
#define DISP_DATA_FORMAT_YUV420 (4 << 9)
#define DISP_DATA_FORMAT_RGB565 (0 << 9)
#define DISP_DATA_FORMAT_RGB565 (0 << 9)
#define DISP_DATA_FORMAT_RGB888 (2 << 9)
#define DISP_DATA_FORMAT_OTHER (7 << 9)

#define CM_SNAP (1 << 1)

#define DCMI_ERROR_NONE (0x00000000U)    /**< No error              */
#define DCMI_ERROR_BERR (0x00000001U)    /**< BERR error            */
#define DCMI_ERROR_ARLO (0x00000002U)    /**< ARLO error            */
#define DCMI_ERROR_AF (0x00000004U)      /**< ACKF error            */
#define DCMI_ERROR_OVR (0x00000008U)     /**< OVR error             */
#define DCMI_ERROR_DMA (0x00000010U)     /**< DMA transfer error    */
#define DCMI_ERROR_TIMEOUT (0x00000020U) /**< Timeout error         */
#define DCMI_ERROR_SIZE (0x00000040U)    /**< Size Management error */

    /**
     * @brief DCMI状态定义
     */
    typedef enum
    {
        DRV_DCMI_OK = 0x00,
        DRV_DCMI_ERROR = 0x01,
        DRV_DCMI_BUSY = 0x02,
        DRV_DCMI_TIMEOUT = 0x03,
    } DRV_DCMI_StatusTypeDef;
    /**
     * @brief DCMI输入格式定义
     */
    typedef enum
    {
        INPUT_FORMAT_RGB565, // 0
        INPUT_FORMAT_RGB666,
        INPUT_FORMAT_RGB888,
        INPUT_FORMAT_YUV422,
        INPUT_FORMAT_YUV420,
        INPUT_FORMAT_MO1NO,
        INPUT_FORMAT_RAWBAYER,
        INPUT_FORMAT_JPEG,
    } DCMI_InputModeTypeDef;

    /**
     * @brief DCMI输出格式定义
     */
    typedef enum
    {
        OUTPUT_FORMAT_RGB444,
        OUTPUT_FORMAT_RGB555,
        OUTPUT_FORMAT_RGB565,
        OUTPUT_FORMAT_YUV444,
        OUTPUT_FORMAT_RGB888,
        OUTPUT_FORMAT_YUV422,
        OUTPUT_FORMAT_420LEGACY,
        OUTPUT_FORMAT_YUV420,
        OUTPUT_FORMAT_RAW6,
        OUTPUT_FORMAT_RAW7,
        OUTPUT_FORMAT_RAW8,
        OUTPUT_FORMAT_RAW10,
        OUTPUT_FORMAT_RAW12,
        OUTPUT_FORMAT_RAW14,
        OUTPUT_FORMAT_MONO,

    } DCMI_OutputModeTypeDef;
    /**
     * @}
     */
    /**
     * @brief dcmi初始定义
     */
    typedef struct
    {
        uint8_t InputMode;  /**<输入图像格式*/
        uint8_t OutputMode; /**<输出图像格式*/

        uint8_t isCaptureMode; /**<0:single  1:continuous*/
        uint8_t isRgbiDmaMode; /**<0:dma->lcd  1:idma->lcd*/

        // uint8_t iDmaMode;           /**<图像传输是否采用内部DMA*/

        uint32_t iDmaDst; /**<内部DMA传输的目的地址*/
        uint32_t eDmaDst; /**<外部DMA传输的目的地址*/

        uint16_t Input_width;  /**<输入图像宽度*/
        uint16_t Input_height; /**<输入图像高度*/

        uint8_t EnableIE;
        uint8_t IEMask;

        uint8_t Rgb2Bgr;

    } DCMI_InitTypeDef;

    /**
     *@brief DCMI通信状态枚举
     */
    typedef enum
    {
        DCMI_STATE_RESET = 0x00, /**< Peripheral is not yet Initialized          */
        DCMI_STATE_READY = 0x20, /**< Peripheral Initialized and ready for use  */
        DCMI_STATE_BUSY = 0x24,  /**< An internal process is ongoing             */

        DCMI_STATE_ABORT = 0x60,   /**< Abort user request ongoing                 */
        DCMI_STATE_TIMEOUT = 0xA0, /**< Timeout state                              */
        DCMI_STATE_ERROR = 0xE0,   /**< Error                                     */

    } DCMI_StateTypeDef;

    /**
     *@brief  回调函数类型定义
     *
     */
    typedef void (*dcmi_callback_t)(void *);

    /**
     *@brief DCMI句柄定义
     */
    typedef struct __DCMI_HandleTypeDef
    {
        DCMI_TypeDef *Instance; /**< DCMI 寄存器基址       */
        DCMI_InitTypeDef Init;  /**< 初始化结构体         */

        __IO DCMI_StateTypeDef State; /**< 状态                 */
        __IO uint32_t ErrorCode;      /**< 错误代码             */

        dcmi_callback_t callback; /**< 回调函数*/

    } DCMI_HandleTypeDef;

    /**
     *@}
     */
    extern DRV_DCMI_StatusTypeDef DRV_DCMI_Getgray(DCMI_TypeDef *pdcmi, DCMI_InitTypeDef *pinit);
    extern DRV_DCMI_StatusTypeDef DRV_DCMI_Init(DCMI_TypeDef *pdcmi, DCMI_InitTypeDef *pinit);

    extern DRV_DCMI_StatusTypeDef DRV_DCMI_Getgray_Idma(DCMI_TypeDef *pdcmi, DCMI_InitTypeDef *pinit);
    extern DRV_DCMI_StatusTypeDef DRV_DCMI_Getgray_Edma(DCMI_TypeDef *pdcmi, DCMI_InitTypeDef *pinit);

    extern void DCMI_Init(DCMI_HandleTypeDef *hdcmi);
#ifdef __cplusplus
}

#endif

#endif /* __DCMI_DRV_H */
/************************ (C) COPYRIGHT LEVETOP *****END OF FILE**********************/
