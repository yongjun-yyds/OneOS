/**********************************************************
 * @Copyright (c) 2021   Levetop Semiconductor Co., Ltd.
 * @File			wdt_drv.h
 * @Author		Jason Zhao
 * @Date			2021/12/5
 * @Time			8:42:6
 * @Version
 * @Brief
 **********************************************************
 * Modification History
 *
 *
 **********************************************************/
#ifndef __WDT_DRV_H__
#define __WDT_DRV_H__

#include "lt776_reg.h"

extern void DRV_WDT_Init(uint16_t WMRCounterVal);

extern void DRV_WDT_FeedDog(void);

extern void DRV_WDT_Open(void);

extern void DRV_WDT_Close(void);

extern uint32_t DRV_WDT_GetCount(void);

#endif /* __WDT_DRV_H__ */
