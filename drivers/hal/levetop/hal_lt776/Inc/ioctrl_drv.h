/**
  ******************************************************************************
             Copyright(c) 2021 Levetop Semiconductor Co. Ltd.
                      All Rights Reserved
  ******************************************************************************
  * @file    ioctrl_drv.h
  * @author  Product application department
  * @version V1.0
  * @date    2021.11.01
  * @brief   Header file of IO Control module.
  *
  ******************************************************************************
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __IOCTRL_DRV_H
#define __IOCTRL_DRV_H

#ifdef __cplusplus
extern "C"
{
#endif

/* Includes ------------------------------------------------------------------*/
#include "lt776_reg.h"

#ifdef FILE_OUT
/**@defgroup DRV 3 HAL driver
  *
  *@{
  */

/** @defgroup DRV_IOCTRL IOCTRL
  *
  *@{
  */
#endif
/*** 宏定义 *******************************************************************/
#ifdef FILE_OUT
/** @defgroup DRV_IOCTRL_Exported_Macros Exported Macros
  *
  * @{
  */
#endif
#define IOCTRL_BIT(bits) (0x01 << bits)
/* SPICR  */
#define _ioctrl_spi_ds_2ma _reg_modify(IOCTRL->SPICR, ~IOCTRL_SPICR_DS1_DS0, 0x00)
#define _ioctrl_spi_ds_4ma _reg_modify(IOCTRL->SPICR, ~IOCTRL_SPICR_DS1_DS0, 0x01)
#define _ioctrl_spi_ds_8ma _reg_modify(IOCTRL->SPICR, ~IOCTRL_SPICR_DS1_DS0, 0x02)
#define _ioctrl_spi_ds_12ma _reg_modify(IOCTRL->SPICR, ~IOCTRL_SPICR_DS1_DS0, 0x03)
#define _ioctrl_spi_slew_fast _bit_clr(IOCTRL->SPICR, IOCTRL_SPICR_SR)
#define _ioctrl_spi_slew_slow _bit_set(IOCTRL->SPICR, IOCTRL_SPICR_SR)
#define _ioctrl_spi_input_cmos _bit_clr(IOCTRL->SPICR, IOCTRL_SPICR_IS)
#define _ioctrl_spi_input_schmitt _bit_set(IOCTRL->SPICR, IOCTRL_SPICR_IS)
#define _ioctrl_spi1_ss_pull_down _bit_clr(IOCTRL->SPICR, IOCTRL_SPICR_SS1_PS)
#define _ioctrl_spi1_ss_pull_up _bit_set(IOCTRL->SPICR, IOCTRL_SPICR_SS1_PS)
#define _ioctrl_spi1_sck_pull_down _bit_clr(IOCTRL->SPICR, IOCTRL_SPICR_SCK1_PS)
#define _ioctrl_spi1_sck_pull_up _bit_set(IOCTRL->SPICR, IOCTRL_SPICR_SCK1_PS)
#define _ioctrl_spi1_miso_pull_down _bit_clr(IOCTRL->SPICR, IOCTRL_SPICR_MISO1_PS)
#define _ioctrl_spi1_miso_pull_up _bit_set(IOCTRL->SPICR, IOCTRL_SPICR_MISO1_PS)
#define _ioctrl_spi1_mosi_pull_down _bit_clr(IOCTRL->SPICR, IOCTRL_SPICR_MOSI1_PS)
#define _ioctrl_spi1_mosi_pull_up _bit_set(IOCTRL->SPICR, IOCTRL_SPICR_MOSI1_PS)
#define _ioctrl_spi1_ss_input_dis _bit_clr(IOCTRL->SPICR, IOCTRL_SPICR_SS1_IE)
#define _ioctrl_spi1_ss_input_en _bit_set(IOCTRL->SPICR, IOCTRL_SPICR_SS1_IE)
#define _ioctrl_spi1_sck_input_dis _bit_clr(IOCTRL->SPICR, IOCTRL_SPICR_SCK1_IE)
#define _ioctrl_spi1_sck_input_en _bit_set(IOCTRL->SPICR, IOCTRL_SPICR_SCK1_IE)
#define _ioctrl_spi1_miso_input_dis _bit_clr(IOCTRL->SPICR, IOCTRL_SPICR_MISO1_IE)
#define _ioctrl_spi1_miso_input_en _bit_set(IOCTRL->SPICR, IOCTRL_SPICR_MISO1_IE)
#define _ioctrl_spi1_mosi_input_dis _bit_clr(IOCTRL->SPICR, IOCTRL_SPICR_MOSI1_IE)
#define _ioctrl_spi1_mosi_input_en _bit_set(IOCTRL->SPICR, IOCTRL_SPICR_MOSI1_IE)
#define _ioctrl_spi2_ss_pull_down _bit_clr(IOCTRL->SPICR, IOCTRL_SPICR_SS2_PS)
#define _ioctrl_spi2_ss_pull_up _bit_set(IOCTRL->SPICR, IOCTRL_SPICR_SS2_PS)
#define _ioctrl_spi2_sck_pull_down _bit_clr(IOCTRL->SPICR, IOCTRL_SPICR_SCK2_PS)
#define _ioctrl_spi2_sck_pull_up _bit_set(IOCTRL->SPICR, IOCTRL_SPICR_SCK2_PS)
#define _ioctrl_spi2_miso_pull_down _bit_clr(IOCTRL->SPICR, IOCTRL_SPICR_MISO2_PS)
#define _ioctrl_spi2_miso_pull_up _bit_set(IOCTRL->SPICR, IOCTRL_SPICR_MISO2_PS)
#define _ioctrl_spi2_mosi_pull_down _bit_clr(IOCTRL->SPICR, IOCTRL_SPICR_MOSI2_PS)
#define _ioctrl_spi2_mosi_pull_up _bit_set(IOCTRL->SPICR, IOCTRL_SPICR_MOSI2_PS)
#define _ioctrl_spi2_ss_input_dis _bit_clr(IOCTRL->SPICR, IOCTRL_SPICR_SS2_IE)
#define _ioctrl_spi2_ss_input_en _bit_set(IOCTRL->SPICR, IOCTRL_SPICR_SS2_IE)
#define _ioctrl_spi2_sck_input_dis _bit_clr(IOCTRL->SPICR, IOCTRL_SPICR_SCK2_IE)
#define _ioctrl_spi2_sck_input_en _bit_set(IOCTRL->SPICR, IOCTRL_SPICR_SCK2_IE)
#define _ioctrl_spi2_miso_input_dis _bit_clr(IOCTRL->SPICR, IOCTRL_SPICR_MISO2_IE)
#define _ioctrl_spi2_miso_input_en _bit_set(IOCTRL->SPICR, IOCTRL_SPICR_MISO2_IE)
#define _ioctrl_spi2_mosi_input_dis _bit_clr(IOCTRL->SPICR, IOCTRL_SPICR_MOSI2_IE)
#define _ioctrl_spi2_mosi_input_en _bit_set(IOCTRL->SPICR, IOCTRL_SPICR_MOSI2_IE)
#define _ioctrl_spi3_ss_pull_down _bit_clr(IOCTRL->SPICR, IOCTRL_SPICR_SS3_PS)
#define _ioctrl_spi3_ss_pull_up _bit_set(IOCTRL->SPICR, IOCTRL_SPICR_SS3_PS)
#define _ioctrl_spi3_sck_pull_down _bit_clr(IOCTRL->SPICR, IOCTRL_SPICR_SCK3_PS)
#define _ioctrl_spi3_sck_pull_up _bit_set(IOCTRL->SPICR, IOCTRL_SPICR_SCK3_PS)
#define _ioctrl_spi3_miso_pull_down _bit_clr(IOCTRL->SPICR, IOCTRL_SPICR_MISO3_PS)
#define _ioctrl_spi3_miso_pull_up _bit_set(IOCTRL->SPICR, IOCTRL_SPICR_MISO3_PS)
#define _ioctrl_spi3_mosi_pull_down _bit_clr(IOCTRL->SPICR, IOCTRL_SPICR_MOSI3_PS)
#define _ioctrl_spi3_mosi_pull_up _bit_set(IOCTRL->SPICR, IOCTRL_SPICR_MOSI3_PS)
#define _ioctrl_spi3_ss_input_dis _bit_clr(IOCTRL->SPICR, IOCTRL_SPICR_SS3_IE)
#define _ioctrl_spi3_ss_input_en _bit_set(IOCTRL->SPICR, IOCTRL_SPICR_SS3_IE)
#define _ioctrl_spi3_sck_input_dis _bit_clr(IOCTRL->SPICR, IOCTRL_SPICR_SCK3_IE)
#define _ioctrl_spi3_sck_input_en _bit_set(IOCTRL->SPICR, IOCTRL_SPICR_SCK3_IE)
#define _ioctrl_spi3_miso_input_dis _bit_clr(IOCTRL->SPICR, IOCTRL_SPICR_MISO3_IE)
#define _ioctrl_spi3_miso_input_en _bit_set(IOCTRL->SPICR, IOCTRL_SPICR_MISO3_IE)
#define _ioctrl_spi3_mosi_input_dis _bit_clr(IOCTRL->SPICR, IOCTRL_SPICR_MOSI3_IE)
#define _ioctrl_spi3_mosi_input_en _bit_set(IOCTRL->SPICR, IOCTRL_SPICR_MOSI3_IE)
#define _ioctrl_spi1_input_dis(bits) _bit_clr(IOCTRL->SPICR, (bits << IOCTRL_SPICR_SPI1_IE))
#define _ioctrl_spi1_input_en(bits) _bit_set(IOCTRL->SPICR, (bits << IOCTRL_SPICR_SPI1_IE))
#define _ioctrl_spi1_pull_down(bits) _bit_clr(IOCTRL->SPICR, (bits << IOCTRL_SPICR_SPI1_PS))
#define _ioctrl_spi1_pull_up(bits) _bit_set(IOCTRL->SPICR, (bits << IOCTRL_SPICR_SPI1_PS))
#define _ioctrl_spi2_input_dis(bits) _bit_clr(IOCTRL->SPICR, (bits << IOCTRL_SPICR_SPI2_IE))
#define _ioctrl_spi2_input_en(bits) _bit_set(IOCTRL->SPICR, (bits << IOCTRL_SPICR_SPI2_IE))
#define _ioctrl_spi2_pull_down(bits) _bit_clr(IOCTRL->SPICR, (bits << IOCTRL_SPICR_SPI2_PS))
#define _ioctrl_spi2_pull_up(bits) _bit_set(IOCTRL->SPICR, (bits << IOCTRL_SPICR_SPI2_PS))
#define _ioctrl_spi3_input_dis(bits) _bit_clr(IOCTRL->SPICR, (bits << IOCTRL_SPICR_SPI3_IE))
#define _ioctrl_spi3_input_en(bits) _bit_set(IOCTRL->SPICR, (bits << IOCTRL_SPICR_SPI3_IE))
#define _ioctrl_spi3_pull_down(bits) _bit_clr(IOCTRL->SPICR, (bits << IOCTRL_SPICR_SPI3_PS))
#define _ioctrl_spi3_pull_up(bits) _bit_set(IOCTRL->SPICR, (bits << IOCTRL_SPICR_SPI3_PS))

/* USICR  */
#define _ioctrl_usi_ds_2ma _reg_modify(IOCTRL->USICR, ~IOCTRL_USICR_DS1_DS0, 0x00)
#define _ioctrl_usi_ds_4ma _reg_modify(IOCTRL->USICR, ~IOCTRL_USICR_DS1_DS0, 0x01)
#define _ioctrl_usi_ds_8ma _reg_modify(IOCTRL->USICR, ~IOCTRL_USICR_DS1_DS0, 0x02)
#define _ioctrl_usi_ds_12ma _reg_modify(IOCTRL->USICR, ~IOCTRL_USICR_DS1_DS0, 0x03)
#define _ioctrl_usi_slew_fast _bit_clr(IOCTRL->USICR, IOCTRL_USICR_SR)
#define _ioctrl_usi_slew_slow _bit_set(IOCTRL->USICR, IOCTRL_USICR_SR)
#define _ioctrl_usi_input_cmos _bit_clr(IOCTRL->USICR, IOCTRL_USICR_IS)
#define _ioctrl_usi_input_schmitt _bit_set(IOCTRL->USICR, IOCTRL_USICR_IS)
#define _ioctrl_usi2_dren_dis _bit_clr(IOCTRL->USICR, IOCTRL_USICR_USI2_DREN)
#define _ioctrl_usi2_dren_en _bit_set(IOCTRL->USICR, IOCTRL_USICR_USI2_DREN)
#define _ioctrl_usi3_dren_dis _bit_clr(IOCTRL->USICR, IOCTRL_USICR_USI3_DREN)
#define _ioctrl_usi3_dren_en _bit_set(IOCTRL->USICR, IOCTRL_USICR_USI3_DREN)
#define _ioctrl_usi1_isorst_pull_down _bit_clr(IOCTRL->USICR, IOCTRL_USICR_ISORST1_PS)
#define _ioctrl_usi1_isorst_pull_up _bit_set(IOCTRL->USICR, IOCTRL_USICR_ISORST1_PS)
#define _ioctrl_usi1_isodat_pull_down _bit_clr(IOCTRL->USICR, IOCTRL_USICR_ISODAT1_PS)
#define _ioctrl_usi1_isodat_pull_up _bit_set(IOCTRL->USICR, IOCTRL_USICR_ISODAT1_PS)
#define _ioctrl_usi1_isoclk_pull_down _bit_clr(IOCTRL->USICR, IOCTRL_USICR_ISOCLK1_PS)
#define _ioctrl_usi1_isoclk_pull_up _bit_set(IOCTRL->USICR, IOCTRL_USICR_ISOCLK1_PS)
#define _ioctrl_usi2_puen_isorst_dis _bit_clr(IOCTRL->USICR, IOCTRL_USICR_ISORST2_PUEN)
#define _ioctrl_usi2_puen_isorst_en _bit_set(IOCTRL->USICR, IOCTRL_USICR_ISORST2_PUEN)
#define _ioctrl_usi2_puen_isodat_dis _bit_clr(IOCTRL->USICR, IOCTRL_USICR_ISODAT2_PUEN)
#define _ioctrl_usi2_puen_isodat_en _bit_set(IOCTRL->USICR, IOCTRL_USICR_ISODAT2_PUEN)
#define _ioctrl_usi2_puen_isoclk_dis _bit_clr(IOCTRL->USICR, IOCTRL_USICR_ISOCLK2_PUEN)
#define _ioctrl_usi2_puen_isoclk_en _bit_set(IOCTRL->USICR, IOCTRL_USICR_ISOCLK2_PUEN)
#define _ioctrl_usi2_dien_isorst_dis _bit_clr(IOCTRL->USICR, IOCTRL_USICR_ISORST2_DIEN)
#define _ioctrl_usi2_dien_isorst_en _bit_set(IOCTRL->USICR, IOCTRL_USICR_ISORST2_DIEN)
#define _ioctrl_usi2_dien_isodat_dis _bit_clr(IOCTRL->USICR, IOCTRL_USICR_ISODAT2_DIEN)
#define _ioctrl_usi2_dien_isodat_en _bit_set(IOCTRL->USICR, IOCTRL_USICR_ISODAT2_DIEN)
#define _ioctrl_usi2_dien_isoclk_dis _bit_clr(IOCTRL->USICR, IOCTRL_USICR_ISOCLK2_DIEN)
#define _ioctrl_usi2_dien_isoclk_en _bit_set(IOCTRL->USICR, IOCTRL_USICR_ISOCLK2_DIEN)
#define _ioctrl_usi1_pull_down(bits) _bit_clr(IOCTRL->USICR, (bits << IOCTRL_USICR_USI1_PS))
#define _ioctrl_usi1_pull_up(bits) _bit_set(IOCTRL->USICR, (bits << IOCTRL_USICR_USI1_PS))
#define _ioctrl_usi2_swap_dis(bits) _bit_clr(IOCTRL->USICR, (bits << IOCTRL_USICR_USI2_SWAP))
#define _ioctrl_usi2_swap_en(bits) _bit_set(IOCTRL->USICR, (bits << IOCTRL_USICR_USI2_SWAP))
#define _ioctrl_usi2_dien_dis(bits) _bit_clr(IOCTRL->USICR, (bits << IOCTRL_USICR_USI2_DIEN))
#define _ioctrl_usi2_dien_en(bits) _bit_set(IOCTRL->USICR, (bits << IOCTRL_USICR_USI2_DIEN))
#define _ioctrl_usi3_puen_dis(bits) _bit_clr(IOCTRL->USICR, (bits << IOCTRL_USICR_USI3_PUEN))
#define _ioctrl_usi3_puen_en(bits) _bit_set(IOCTRL->USICR, (bits << IOCTRL_USICR_USI3_PUEN))
#define _ioctrl_usi3_dien_dis(bits) _bit_clr(IOCTRL->USICR, (bits << IOCTRL_USICR_USI3_DIEN))
#define _ioctrl_usi3_dien_en(bits) _bit_set(IOCTRL->USICR, (bits << IOCTRL_USICR_USI3_DIEN))
#define _ioctrl_usi2_swap_pins_en(pins) _bit_set(IOCTRL->USICR, (0x1 << pins))
/* I2CCR  */
#define _ioctrl_i2c_ds_2ma _reg_modify(IOCTRL->I2CCR, ~IOCTRL_I2CCR_DS1_DS0, 0x00)
#define _ioctrl_i2c_ds_4ma _reg_modify(IOCTRL->I2CCR, ~IOCTRL_I2CCR_DS1_DS0, 0x01)
#define _ioctrl_i2c_ds_8ma _reg_modify(IOCTRL->I2CCR, ~IOCTRL_I2CCR_DS1_DS0, 0x02)
#define _ioctrl_i2c_ds_12ma _reg_modify(IOCTRL->I2CCR, ~IOCTRL_I2CCR_DS1_DS0, 0x03)
#define _ioctrl_i2c_slew_fast _bit_clr(IOCTRL->I2CCR, IOCTRL_I2CCR_SR)
#define _ioctrl_i2c_slew_slow _bit_set(IOCTRL->I2CCR, IOCTRL_I2CCR_SR)
#define _ioctrl_i2c_input_cmos _bit_clr(IOCTRL->I2CCR, IOCTRL_I2CCR_IS)
#define _ioctrl_i2c_input_schmitt _bit_set(IOCTRL->I2CCR, IOCTRL_I2CCR_IS)
#define _ioctrl_i2c1_scl_pull_down _bit_clr(IOCTRL->I2CCR, IOCTRL_I2CCR_SCL1_PS)
#define _ioctrl_i2c1_scl_pull_up _bit_set(IOCTRL->I2CCR, IOCTRL_I2CCR_SCL1_PS)
#define _ioctrl_i2c1_sda_pull_down _bit_clr(IOCTRL->I2CCR, IOCTRL_I2CCR_SDA1_PS)
#define _ioctrl_i2c1_sda_pull_up _bit_set(IOCTRL->I2CCR, IOCTRL_I2CCR_SDA1_PS)
#define _ioctrl_i2c1_scl_input_dis _bit_clr(IOCTRL->I2CCR, IOCTRL_I2CCR_SCL1_IE)
#define _ioctrl_i2c1_scl_input_en _bit_set(IOCTRL->I2CCR, IOCTRL_I2CCR_SCL1_IE)
#define _ioctrl_i2c1_sda_input_dis _bit_clr(IOCTRL->I2CCR, IOCTRL_I2CCR_SDA1_IE)
#define _ioctrl_i2c1_sda_input_en _bit_set(IOCTRL->I2CCR, IOCTRL_I2CCR_SDA1_IE)
#define _ioctrl_i2c2_scl_pull_down _bit_clr(IOCTRL->I2CCR, IOCTRL_I2CCR_SCL2_PS)
#define _ioctrl_i2c2_scl_pull_up _bit_set(IOCTRL->I2CCR, IOCTRL_I2CCR_SCL2_PS)
#define _ioctrl_i2c2_sda_pull_down _bit_clr(IOCTRL->I2CCR, IOCTRL_I2CCR_SDA2_PS)
#define _ioctrl_i2c2_sda_pull_up _bit_set(IOCTRL->I2CCR, IOCTRL_I2CCR_SDA2_PS)
#define _ioctrl_i2c2_scl_input_dis _bit_clr(IOCTRL->I2CCR, IOCTRL_I2CCR_SCL2_IE)
#define _ioctrl_i2c2_scl_input_en _bit_set(IOCTRL->I2CCR, IOCTRL_I2CCR_SCL2_IE)
#define _ioctrl_i2c2_sda_input_dis _bit_clr(IOCTRL->I2CCR, IOCTRL_I2CCR_SDA2_IE)
#define _ioctrl_i2c2_sda_input_en _bit_set(IOCTRL->I2CCR, IOCTRL_I2CCR_SDA2_IE)
#define _ioctrl_i2c3_scl_pull_down _bit_clr(IOCTRL->I2CCR, IOCTRL_I2CCR_SCL3_PS)
#define _ioctrl_i2c3_scl_pull_up _bit_set(IOCTRL->I2CCR, IOCTRL_I2CCR_SCL3_PS)
#define _ioctrl_i2c3_sda_pull_down _bit_clr(IOCTRL->I2CCR, IOCTRL_I2CCR_SDA3_PS)
#define _ioctrl_i2c3_sda_pull_up _bit_set(IOCTRL->I2CCR, IOCTRL_I2CCR_SDA3_PS)
#define _ioctrl_i2c3_scl_input_dis _bit_clr(IOCTRL->I2CCR, IOCTRL_I2CCR_SCL3_IE)
#define _ioctrl_i2c3_scl_input_en _bit_set(IOCTRL->I2CCR, IOCTRL_I2CCR_SCL3_IE)
#define _ioctrl_i2c3_sda_input_dis _bit_clr(IOCTRL->I2CCR, IOCTRL_I2CCR_SDA3_IE)
#define _ioctrl_i2c3_sda_input_en _bit_set(IOCTRL->I2CCR, IOCTRL_I2CCR_SDA3_IE)
#define _ioctrl_i2c1_input_dis(bits) _bit_clr(IOCTRL->I2CCR, (bits << IOCTRL_I2CCR_I2C1_IE))
#define _ioctrl_i2c1_input_en(bits) _bit_set(IOCTRL->I2CCR, (bits << IOCTRL_I2CCR_I2C1_IE))
#define _ioctrl_i2c1_pull_down(bits) _bit_clr(IOCTRL->I2CCR, (bits << IOCTRL_I2CCR_I2C1_PS))
#define _ioctrl_i2c1_pull_up(bits) _bit_set(IOCTRL->I2CCR, (bits << IOCTRL_I2CCR_I2C1_PS))
#define _ioctrl_i2c2_input_dis(bits) _bit_clr(IOCTRL->I2CCR, (bits << IOCTRL_I2CCR_I2C2_IE))
#define _ioctrl_i2c2_input_en(bits) _bit_set(IOCTRL->I2CCR, (bits << IOCTRL_I2CCR_I2C2_IE))
#define _ioctrl_i2c2_pull_down(bits) _bit_clr(IOCTRL->I2CCR, (bits << IOCTRL_I2CCR_I2C2_PS))
#define _ioctrl_i2c2_pull_up(bits) _bit_set(IOCTRL->I2CCR, (bits << IOCTRL_I2CCR_I2C2_PS))
#define _ioctrl_i2c3_input_dis(bits) _bit_clr(IOCTRL->I2CCR, (bits << IOCTRL_I2CCR_I2C3_IE))
#define _ioctrl_i2c3_input_en(bits) _bit_set(IOCTRL->I2CCR, (bits << IOCTRL_I2CCR_I2C3_IE))
#define _ioctrl_i2c3_pull_down(bits) _bit_clr(IOCTRL->I2CCR, (bits << IOCTRL_I2CCR_I2C3_PS))
#define _ioctrl_i2c3_pull_up(bits) _bit_set(IOCTRL->I2CCR, (bits << IOCTRL_I2CCR_I2C3_PS))

/* UARTCR  */
#define _ioctrl_uart_ds_2ma _reg_modify(IOCTRL->UARTCR, ~IOCTRL_UARTCR_DS1_DS0, 0x00)
#define _ioctrl_uart_ds_4ma _reg_modify(IOCTRL->UARTCR, ~IOCTRL_UARTCR_DS1_DS0, 0x01)
#define _ioctrl_uart_ds_8ma _reg_modify(IOCTRL->UARTCR, ~IOCTRL_UARTCR_DS1_DS0, 0x02)
#define _ioctrl_uart_ds_12ma _reg_modify(IOCTRL->UARTCR, ~IOCTRL_UARTCR_DS1_DS0, 0x03)
#define _ioctrl_uart_slew_fast _bit_clr(IOCTRL->UARTCR, IOCTRL_UARTCR_SR)
#define _ioctrl_uart_slew_slow _bit_set(IOCTRL->UARTCR, IOCTRL_UARTCR_SR)
#define _ioctrl_uart_input_cmos _bit_clr(IOCTRL->UARTCR, IOCTRL_UARTCR_IS)
#define _ioctrl_uart_input_schmitt _bit_set(IOCTRL->UARTCR, IOCTRL_UARTCR_IS)
#define _ioctrl_uart1_txd_pull_down _bit_clr(IOCTRL->UARTCR, IOCTRL_UARTCR_TXD1_PS)
#define _ioctrl_uart1_txd_pull_up _bit_set(IOCTRL->UARTCR, IOCTRL_UARTCR_TXD1_PS)
#define _ioctrl_uart1_rxd_pull_down _bit_clr(IOCTRL->UARTCR, IOCTRL_UARTCR_RXD1_PS)
#define _ioctrl_uart1_rxd_pull_up _bit_set(IOCTRL->UARTCR, IOCTRL_UARTCR_RXD1_PS)
#define _ioctrl_uart1_cts_pull_down _bit_clr(IOCTRL->UARTCR, IOCTRL_UARTCR_CTS1_PUE)
#define _ioctrl_uart1_cts_pull_up _bit_set(IOCTRL->UARTCR, IOCTRL_UARTCR_CTS1_PUE)
#define _ioctrl_uart2_txd_pull_down _bit_clr(IOCTRL->UARTCR, IOCTRL_UARTCR_TXD2_PS)
#define _ioctrl_uart2_txd_pull_up _bit_set(IOCTRL->UARTCR, IOCTRL_UARTCR_TXD2_PS)
#define _ioctrl_uart2_rxd_pull_down _bit_clr(IOCTRL->UARTCR, IOCTRL_UARTCR_RXD2_PS)
#define _ioctrl_uart2_rxd_pull_up _bit_set(IOCTRL->UARTCR, IOCTRL_UARTCR_RXD2_PS)
#define _ioctrl_uart2_cts_pull_down _bit_clr(IOCTRL->UARTCR, IOCTRL_UARTCR_CTS2_PUE)
#define _ioctrl_uart2_cts_pull_up _bit_set(IOCTRL->UARTCR, IOCTRL_UARTCR_CTS2_PUE)
#define _ioctrl_uart1_pull_down(bits) _bit_clr(IOCTRL->UARTCR, (bits << IOCTRL_UARTCR_UART1_PS))
#define _ioctrl_uart1_pull_up(bits) _bit_set(IOCTRL->UARTCR, (bits << IOCTRL_UARTCR_UART1_PS))
#define _ioctrl_uart2_pull_down(bits) _bit_clr(IOCTRL->UARTCR, (bits << IOCTRL_UARTCR_UART2_PS))
#define _ioctrl_uart2_pull_up(bits) _bit_set(IOCTRL->UARTCR, (bits << IOCTRL_UARTCR_UART2_PS))

/* GINTLCR  */
#define _ioctrl_gintl_ds_2ma _reg_modify(IOCTRL->GINTLCR, ~IOCTRL_GINTLCR_DS1_DS0, 0x00)
#define _ioctrl_gintl_ds_4ma _reg_modify(IOCTRL->GINTLCR, ~IOCTRL_GINTLCR_DS1_DS0, 0x01)
#define _ioctrl_gintl_ds_8ma _reg_modify(IOCTRL->GINTLCR, ~IOCTRL_GINTLCR_DS1_DS0, 0x02)
#define _ioctrl_gintl_ds_12ma _reg_modify(IOCTRL->GINTLCR, ~IOCTRL_GINTLCR_DS1_DS0, 0x03)
#define _ioctrl_gintl_slew_fast _bit_clr(IOCTRL->GINTLCR, IOCTRL_GINTLCR_SR)
#define _ioctrl_gintl_slew_slow _bit_set(IOCTRL->GINTLCR, IOCTRL_GINTLCR_SR)
#define _ioctrl_gintl_input_cmos _bit_clr(IOCTRL->GINTLCR, IOCTRL_GINTLCR_IS)
#define _ioctrl_gintl_input_schmitt _bit_set(IOCTRL->GINTLCR, IOCTRL_GINTLCR_IS)
#define _ioctrl_gintl_gint0_input_dis _bit_clr(IOCTRL->GINTLCR, IOCTRL_GINTLCR_GINT0_IE)
#define _ioctrl_gintl_gint0_input_en _bit_set(IOCTRL->GINTLCR, IOCTRL_GINTLCR_GINT0_IE)
#define _ioctrl_gintl_gint1_input_dis _bit_clr(IOCTRL->GINTLCR, IOCTRL_GINTLCR_GINT1_IE)
#define _ioctrl_gintl_gint1_input_en _bit_set(IOCTRL->GINTLCR, IOCTRL_GINTLCR_GINT1_IE)
#define _ioctrl_gintl_gint2_input_dis _bit_clr(IOCTRL->GINTLCR, IOCTRL_GINTLCR_GINT2_IE)
#define _ioctrl_gintl_gint2_input_en _bit_set(IOCTRL->GINTLCR, IOCTRL_GINTLCR_GINT2_IE)
#define _ioctrl_gintl_gint3_input_dis _bit_clr(IOCTRL->GINTLCR, IOCTRL_GINTLCR_GINT3_IE)
#define _ioctrl_gintl_gint3_input_en _bit_set(IOCTRL->GINTLCR, IOCTRL_GINTLCR_GINT3_IE)
#define _ioctrl_gintl_gint4_input_dis _bit_clr(IOCTRL->GINTLCR, IOCTRL_GINTLCR_GINT4_IE)
#define _ioctrl_gintl_gint4_input_en _bit_set(IOCTRL->GINTLCR, IOCTRL_GINTLCR_GINT4_IE)
#define _ioctrl_gintl_gint5_input_dis _bit_clr(IOCTRL->GINTLCR, IOCTRL_GINTLCR_GINT5_IE)
#define _ioctrl_gintl_gint5_input_en _bit_set(IOCTRL->GINTLCR, IOCTRL_GINTLCR_GINT5_IE)
#define _ioctrl_gintl_gint6_input_dis _bit_clr(IOCTRL->GINTLCR, IOCTRL_GINTLCR_GINT6_IE)
#define _ioctrl_gintl_gint6_input_en _bit_set(IOCTRL->GINTLCR, IOCTRL_GINTLCR_GINT6_IE)
#define _ioctrl_gintl_gint7_input_dis _bit_clr(IOCTRL->GINTLCR, IOCTRL_GINTLCR_GINT7_IE)
#define _ioctrl_gintl_gint7_input_en _bit_set(IOCTRL->GINTLCR, IOCTRL_GINTLCR_GINT7_IE)
#define _ioctrl_gintl_gint0_pull_down _bit_clr(IOCTRL->GINTLCR, IOCTRL_GINTLCR_GINT0_PS)
#define _ioctrl_gintl_gint0_pull_up _bit_set(IOCTRL->GINTLCR, IOCTRL_GINTLCR_GINT0_PS)
#define _ioctrl_gintl_gint1_pull_down _bit_clr(IOCTRL->GINTLCR, IOCTRL_GINTLCR_GINT1_PS)
#define _ioctrl_gintl_gint1_pull_up _bit_set(IOCTRL->GINTLCR, IOCTRL_GINTLCR_GINT1_PS)
#define _ioctrl_gintl_gint2_pull_down _bit_clr(IOCTRL->GINTLCR, IOCTRL_GINTLCR_GINT2_PS)
#define _ioctrl_gintl_gint2_pull_up _bit_set(IOCTRL->GINTLCR, IOCTRL_GINTLCR_GINT2_PS)
#define _ioctrl_gintl_gint3_pull_down _bit_clr(IOCTRL->GINTLCR, IOCTRL_GINTLCR_GINT3_PS)
#define _ioctrl_gintl_gint3_pull_up _bit_set(IOCTRL->GINTLCR, IOCTRL_GINTLCR_GINT3_PS)
#define _ioctrl_gintl_gint4_pull_down _bit_clr(IOCTRL->GINTLCR, IOCTRL_GINTLCR_GINT4_PS)
#define _ioctrl_gintl_gint4_pull_up _bit_set(IOCTRL->GINTLCR, IOCTRL_GINTLCR_GINT4_PS)
#define _ioctrl_gintl_gint5_pull_down _bit_clr(IOCTRL->GINTLCR, IOCTRL_GINTLCR_GINT5_PS)
#define _ioctrl_gintl_gint5_pull_up _bit_set(IOCTRL->GINTLCR, IOCTRL_GINTLCR_GINT5_PS)
#define _ioctrl_gintl_gint6_pull_down _bit_clr(IOCTRL->GINTLCR, IOCTRL_GINTLCR_GINT6_PS)
#define _ioctrl_gintl_gint6_pull_up _bit_set(IOCTRL->GINTLCR, IOCTRL_GINTLCR_GINT6_PS)
#define _ioctrl_gintl_gint7_pull_down _bit_clr(IOCTRL->GINTLCR, IOCTRL_GINTLCR_GINT7_PS)
#define _ioctrl_gintl_gint7_pull_up _bit_set(IOCTRL->GINTLCR, IOCTRL_GINTLCR_GINT7_PS)
#define _ioctrl_gintl_gints_input_dis(bits) _bit_clr(IOCTRL->GINTLCR, (bits << IOCTRL_GINTLCR_IE))
#define _ioctrl_gintl_gints_input_en(bits) _bit_set(IOCTRL->GINTLCR, (bits << IOCTRL_GINTLCR_IE))
#define _ioctrl_gintl_gints_pull_down(bits) _bit_clr(IOCTRL->GINTLCR, (bits << IOCTRL_GINTLCR_PS))
#define _ioctrl_gintl_gints_pull_up(bits) _bit_set(IOCTRL->GINTLCR, (bits << IOCTRL_GINTLCR_PS))

/* GINTHCR  */
#define _ioctrl_ginth_ds_2ma _reg_modify(IOCTRL->GINTHCR, ~IOCTRL_GINTHCR_DS1_DS0, 0x00)
#define _ioctrl_ginth_ds_4ma _reg_modify(IOCTRL->GINTHCR, ~IOCTRL_GINTHCR_DS1_DS0, 0x01)
#define _ioctrl_ginth_ds_8ma _reg_modify(IOCTRL->GINTHCR, ~IOCTRL_GINTHCR_DS1_DS0, 0x02)
#define _ioctrl_ginth_ds_12ma _reg_modify(IOCTRL->GINTHCR, ~IOCTRL_GINTHCR_DS1_DS0, 0x03)
#define _ioctrl_ginth_slew_fast _bit_clr(IOCTRL->GINTHCR, IOCTRL_GINTHCR_SR)
#define _ioctrl_ginth_slew_slow _bit_set(IOCTRL->GINTHCR, IOCTRL_GINTHCR_SR)
#define _ioctrl_ginth_input_cmos _bit_clr(IOCTRL->GINTHCR, IOCTRL_GINTHCR_IS)
#define _ioctrl_ginth_input_schmitt _bit_set(IOCTRL->GINTHCR, IOCTRL_GINTHCR_IS)
#define _ioctrl_ginth_gint8_input_dis _bit_clr(IOCTRL->GINTHCR, IOCTRL_GINTHCR_GINT8_IE)
#define _ioctrl_ginth_gint8_input_en _bit_set(IOCTRL->GINTHCR, IOCTRL_GINTHCR_GINT8_IE)
#define _ioctrl_ginth_gint9_input_dis _bit_clr(IOCTRL->GINTHCR, IOCTRL_GINTHCR_GINT9_IE)
#define _ioctrl_ginth_gint9_input_en _bit_set(IOCTRL->GINTHCR, IOCTRL_GINTHCR_GINT9_IE)
#define _ioctrl_ginth_gint10_input_dis _bit_clr(IOCTRL->GINTHCR, IOCTRL_GINTHCR_GINT10_IE)
#define _ioctrl_ginth_gint10_input_en _bit_set(IOCTRL->GINTHCR, IOCTRL_GINTHCR_GINT10_IE)
#define _ioctrl_ginth_gint11_input_dis _bit_clr(IOCTRL->GINTHCR, IOCTRL_GINTHCR_GINT11_IE)
#define _ioctrl_ginth_gint11_input_en _bit_set(IOCTRL->GINTHCR, IOCTRL_GINTHCR_GINT11_IE)
#define _ioctrl_ginth_gint12_input_dis _bit_clr(IOCTRL->GINTHCR, IOCTRL_GINTHCR_GINT12_IE)
#define _ioctrl_ginth_gint12_input_en _bit_set(IOCTRL->GINTHCR, IOCTRL_GINTHCR_GINT12_IE)
#define _ioctrl_ginth_gint13_input_dis _bit_clr(IOCTRL->GINTHCR, IOCTRL_GINTHCR_GINT13_IE)
#define _ioctrl_ginth_gint13_input_en _bit_set(IOCTRL->GINTHCR, IOCTRL_GINTHCR_GINT13_IE)
#define _ioctrl_ginth_gint14_input_dis _bit_clr(IOCTRL->GINTHCR, IOCTRL_GINTHCR_GINT14_IE)
#define _ioctrl_ginth_gint14_input_en _bit_set(IOCTRL->GINTHCR, IOCTRL_GINTHCR_GINT14_IE)
#define _ioctrl_ginth_gint15_input_dis _bit_clr(IOCTRL->GINTHCR, IOCTRL_GINTHCR_GINT15_IE)
#define _ioctrl_ginth_gint15_input_en _bit_set(IOCTRL->GINTHCR, IOCTRL_GINTHCR_GINT15_IE)
#define _ioctrl_ginth_gint8_pull_down _bit_clr(IOCTRL->GINTHCR, IOCTRL_GINTHCR_GINT8_PS)
#define _ioctrl_ginth_gint8_pull_up _bit_set(IOCTRL->GINTHCR, IOCTRL_GINTHCR_GINT8_PS)
#define _ioctrl_ginth_gint9_pull_down _bit_clr(IOCTRL->GINTHCR, IOCTRL_GINTHCR_GINT9_PS)
#define _ioctrl_ginth_gint9_pull_up _bit_set(IOCTRL->GINTHCR, IOCTRL_GINTHCR_GINT9_PS)
#define _ioctrl_ginth_gint10_pull_down _bit_clr(IOCTRL->GINTHCR, IOCTRL_GINTHCR_GINT10_PS)
#define _ioctrl_ginth_gint10_pull_up _bit_set(IOCTRL->GINTHCR, IOCTRL_GINTHCR_GINT10_PS)
#define _ioctrl_ginth_gint11_pull_down _bit_clr(IOCTRL->GINTHCR, IOCTRL_GINTHCR_GINT11_PS)
#define _ioctrl_ginth_gint11_pull_up _bit_set(IOCTRL->GINTHCR, IOCTRL_GINTHCR_GINT11_PS)
#define _ioctrl_ginth_gint12_pull_down _bit_clr(IOCTRL->GINTHCR, IOCTRL_GINTHCR_GINT12_PS)
#define _ioctrl_ginth_gint12_pull_up _bit_set(IOCTRL->GINTHCR, IOCTRL_GINTHCR_GINT12_PS)
#define _ioctrl_ginth_gint13_pull_down _bit_clr(IOCTRL->GINTHCR, IOCTRL_GINTHCR_GINT13_PS)
#define _ioctrl_ginth_gint13_pull_up _bit_set(IOCTRL->GINTHCR, IOCTRL_GINTHCR_GINT13_PS)
#define _ioctrl_ginth_gint14_pull_down _bit_clr(IOCTRL->GINTHCR, IOCTRL_GINTHCR_GINT14_PS)
#define _ioctrl_ginth_gint14_pull_up _bit_set(IOCTRL->GINTHCR, IOCTRL_GINTHCR_GINT14_PS)
#define _ioctrl_ginth_gint15_pull_down _bit_clr(IOCTRL->GINTHCR, IOCTRL_GINTHCR_GINT15_PS)
#define _ioctrl_ginth_gint15_pull_up _bit_set(IOCTRL->GINTHCR, IOCTRL_GINTHCR_GINT15_PS)
#define _ioctrl_ginth_gints_input_dis(bits) _bit_clr(IOCTRL->GINTHCR, (bits << IOCTRL_GINTHCR_IE))
#define _ioctrl_ginth_gints_input_en(bits) _bit_set(IOCTRL->GINTHCR, (bits << IOCTRL_GINTHCR_IE))
#define _ioctrl_ginth_gints_pull_down(bits) _bit_clr(IOCTRL->GINTHCR, (bits << IOCTRL_GINTHCR_PS))
#define _ioctrl_ginth_gints_pull_up(bits) _bit_set(IOCTRL->GINTHCR, (bits << IOCTRL_GINTHCR_PS))

/* SWAPCR  */
#define _ioctrl_swapcr_swap_dis(pins) _bit_clr(IOCTRL->SWAPCR, (0x01 << pins)) //OX1C
#define _ioctrl_swapcr_swap_en(pins) _bit_set(IOCTRL->SWAPCR, (0x01 << pins))

/* SSI1CR(SPIM1CR)  */
#define _ioctrl_ssi1_ds_2ma _reg_modify(IOCTRL->SSI1CR, ~IOCTRL_SSI1CR_DS1_DS0, 0x00)
#define _ioctrl_ssi1_ds_4ma _reg_modify(IOCTRL->SSI1CR, ~IOCTRL_SSI1CR_DS1_DS0, 0x01)
#define _ioctrl_ssi1_ds_8ma _reg_modify(IOCTRL->SSI1CR, ~IOCTRL_SSI1CR_DS1_DS0, 0x02)
#define _ioctrl_ssi1_ds_12ma _reg_modify(IOCTRL->SSI1CR, ~IOCTRL_SSI1CR_DS1_DS0, 0x03)
#define _ioctrl_ssi1_slew_fast _bit_clr(IOCTRL->SSI1CR, IOCTRL_SSI1CR_SR)
#define _ioctrl_ssi1_slew_slow _bit_set(IOCTRL->SSI1CR, IOCTRL_SSI1CR_SR)
#define _ioctrl_ssi1_input_cmos _bit_clr(IOCTRL->SSI1CR, IOCTRL_SSI1CR_IS)
#define _ioctrl_ssi1_input_schmitt _bit_set(IOCTRL->SSI1CR, IOCTRL_SSI1CR_IS)
#define _ioctrl_ssi1_pull_up_down_dis _bit_clr(IOCTRL->SSI1CR, IOCTRL_SSI1CR_PUE)
#define _ioctrl_ssi1_pull_up_down_en _bit_set(IOCTRL->SSI1CR, IOCTRL_SSI1CR_PUE)
#define _ioctrl_ssi1_output_cmos _bit_clr(IOCTRL->SSI1CR, IOCTRL_SSI1CR_ODE)
#define _ioctrl_ssi1_output_open_d _bit_set(IOCTRL->SSI1CR, IOCTRL_SSI1CR_ODE)
#define _ioctrl_ssi1_ss_pull_down _bit_clr(IOCTRL->SSI1CR, IOCTRL_SSI1CR_SS_PS)
#define _ioctrl_ssi1_ss_pull_up _bit_set(IOCTRL->SSI1CR, IOCTRL_SSI1CR_SS_PS)
#define _ioctrl_ssi1_sck_pull_down _bit_clr(IOCTRL->SSI1CR, IOCTRL_SSI1CR_SCK_PS)
#define _ioctrl_ssi1_sck_pull_up _bit_set(IOCTRL->SSI1CR, IOCTRL_SSI1CR_SCK_PS)
#define _ioctrl_ssi1_d0_pull_down _bit_clr(IOCTRL->SSI1CR, IOCTRL_SSI1CR_D0_PS)
#define _ioctrl_ssi1_d0_pull_up _bit_set(IOCTRL->SSI1CR, IOCTRL_SSI1CR_D0_PS)
#define _ioctrl_ssi1_d1_pull_down _bit_clr(IOCTRL->SSI1CR, IOCTRL_SSI1CR_D1_PS)
#define _ioctrl_ssi1_d1_pull_up _bit_set(IOCTRL->SSI1CR, IOCTRL_SSI1CR_D1_PS)
#define _ioctrl_ssi1_d2_pull_down _bit_clr(IOCTRL->SSI1CR, IOCTRL_SSI1CR_D2_PS)
#define _ioctrl_ssi1_d2_pull_up _bit_set(IOCTRL->SSI1CR, IOCTRL_SSI1CR_D2_PS)
#define _ioctrl_ssi1_d3_pull_down _bit_clr(IOCTRL->SSI1CR, IOCTRL_SSI1CR_D3_PS)
#define _ioctrl_ssi1_d3_pull_up _bit_set(IOCTRL->SSI1CR, IOCTRL_SSI1CR_D3_PS)
#define _ioctrl_ssi1_ss_input_dis _bit_clr(IOCTRL->SSI1CR, IOCTRL_SSI1CR_SS_IE)
#define _ioctrl_ssi1_ss_input_en _bit_set(IOCTRL->SSI1CR, IOCTRL_SSI1CR_SS_IE)
#define _ioctrl_ssi1_sck_input_dis _bit_clr(IOCTRL->SSI1CR, IOCTRL_SSI1CR_SCK_IE)
#define _ioctrl_ssi1_sck_input_en _bit_set(IOCTRL->SSI1CR, IOCTRL_SSI1CR_SCK_IE)
#define _ioctrl_ssi1_d0_input_dis _bit_clr(IOCTRL->SSI1CR, IOCTRL_SSI1CR_D0_IE)
#define _ioctrl_ssi1_d0_input_en _bit_set(IOCTRL->SSI1CR, IOCTRL_SSI1CR_D0_IE)
#define _ioctrl_ssi1_d1_input_dis _bit_clr(IOCTRL->SSI1CR, IOCTRL_SSI1CR_D1_IE)
#define _ioctrl_ssi1_d1_input_en _bit_set(IOCTRL->SSI1CR, IOCTRL_SSI1CR_D1_IE)
#define _ioctrl_ssi1_d2_input_dis _bit_clr(IOCTRL->SSI1CR, IOCTRL_SSI1CR_D2_IE)
#define _ioctrl_ssi1_d2_input_en _bit_set(IOCTRL->SSI1CR, IOCTRL_SSI1CR_D2_IE)
#define _ioctrl_ssi1_d3_input_dis _bit_clr(IOCTRL->SSI1CR, IOCTRL_SSI1CR_D3_IE)
#define _ioctrl_ssi1_d3_input_en _bit_set(IOCTRL->SSI1CR, IOCTRL_SSI1CR_D3_IE)
#define _ioctrl_ssi1_input_dis(bits) _bit_clr(IOCTRL->SSI1CR, (bits << IOCTRL_SSI1CR_IE))
#define _ioctrl_ssi1_input_en(bits) _bit_set(IOCTRL->SSI1CR, (bits << IOCTRL_SSI1CR_IE))
#define _ioctrl_ssi1_pull_down(bits) _bit_clr(IOCTRL->SSI1CR, (bits << IOCTRL_SSI1CR_PS))
#define _ioctrl_ssi1_pull_up(bits) _bit_set(IOCTRL->SSI1CR, (bits << IOCTRL_SSI1CR_PS))

/* SSI2CR(SPIM2CR)  */
#define _ioctrl_ssi2_ds_2ma _reg_modify(IOCTRL->SSI2CR, ~IOCTRL_SSI2CR_DS1_DS0, 0x00)
#define _ioctrl_ssi2_ds_4ma _reg_modify(IOCTRL->SSI2CR, ~IOCTRL_SSI2CR_DS1_DS0, 0x01)
#define _ioctrl_ssi2_ds_8ma _reg_modify(IOCTRL->SSI2CR, ~IOCTRL_SSI2CR_DS1_DS0, 0x02)
#define _ioctrl_ssi2_ds_12ma _reg_modify(IOCTRL->SSI2CR, ~IOCTRL_SSI2CR_DS1_DS0, 0x03)
#define _ioctrl_ssi2_slew_fast _bit_clr(IOCTRL->SSI2CR, IOCTRL_SSI2CR_SR)
#define _ioctrl_ssi2_slew_slow _bit_set(IOCTRL->SSI2CR, IOCTRL_SSI2CR_SR)
#define _ioctrl_ssi2_input_cmos _bit_clr(IOCTRL->SSI2CR, IOCTRL_SSI2CR_IS)
#define _ioctrl_ssi2_input_schmitt _bit_set(IOCTRL->SSI2CR, IOCTRL_SSI2CR_IS)
#define _ioctrl_ssi2_pull_up_down_dis _bit_clr(IOCTRL->SSI2CR, IOCTRL_SSI2CR_PUE)
#define _ioctrl_ssi2_pull_up_down_en _bit_set(IOCTRL->SSI2CR, IOCTRL_SSI2CR_PUE)
#define _ioctrl_ssi2_output_cmos _bit_clr(IOCTRL->SSI2CR, IOCTRL_SSI2CR_ODE)
#define _ioctrl_ssi2_output_open_d _bit_set(IOCTRL->SSI2CR, IOCTRL_SSI2CR_ODE)
#define _ioctrl_ssi2_ss_pull_down _bit_clr(IOCTRL->SSI2CR, IOCTRL_SSI2CR_SS_PS)
#define _ioctrl_ssi2_ss_pull_up _bit_set(IOCTRL->SSI2CR, IOCTRL_SSI2CR_SS_PS)
#define _ioctrl_ssi2_sck_pull_down _bit_clr(IOCTRL->SSI2CR, IOCTRL_SSI2CR_SCK_PS)
#define _ioctrl_ssi2_sck_pull_up _bit_set(IOCTRL->SSI2CR, IOCTRL_SSI2CR_SCK_PS)
#define _ioctrl_ssi2_d0_pull_down _bit_clr(IOCTRL->SSI2CR, IOCTRL_SSI2CR_D0_PS)
#define _ioctrl_ssi2_d0_pull_up _bit_set(IOCTRL->SSI2CR, IOCTRL_SSI2CR_D0_PS)
#define _ioctrl_ssi2_d1_pull_down _bit_clr(IOCTRL->SSI2CR, IOCTRL_SSI2CR_D1_PS)
#define _ioctrl_ssi2_d1_pull_up _bit_set(IOCTRL->SSI2CR, IOCTRL_SSI2CR_D1_PS)
#define _ioctrl_ssi2_d2_pull_down _bit_clr(IOCTRL->SSI2CR, IOCTRL_SSI2CR_D2_PS)
#define _ioctrl_ssi2_d2_pull_up _bit_set(IOCTRL->SSI2CR, IOCTRL_SSI2CR_D2_PS)
#define _ioctrl_ssi2_d3_pull_down _bit_clr(IOCTRL->SSI2CR, IOCTRL_SSI2CR_D3_PS)
#define _ioctrl_ssi2_d3_pull_up _bit_set(IOCTRL->SSI2CR, IOCTRL_SSI2CR_D3_PS)
#define _ioctrl_ssi2_ss_input_dis _bit_clr(IOCTRL->SSI2CR, IOCTRL_SSI2CR_SS_IE)
#define _ioctrl_ssi2_ss_input_en _bit_set(IOCTRL->SSI2CR, IOCTRL_SSI2CR_SS_IE)
#define _ioctrl_ssi2_sck_input_dis _bit_clr(IOCTRL->SSI2CR, IOCTRL_SSI2CR_SCK_IE)
#define _ioctrl_ssi2_sck_input_en _bit_set(IOCTRL->SSI2CR, IOCTRL_SSI2CR_SCK_IE)
#define _ioctrl_ssi2_d0_input_dis _bit_clr(IOCTRL->SSI2CR, IOCTRL_SSI2CR_D0_IE)
#define _ioctrl_ssi2_d0_input_en _bit_set(IOCTRL->SSI2CR, IOCTRL_SSI2CR_D0_IE)
#define _ioctrl_ssi2_d1_input_dis _bit_clr(IOCTRL->SSI2CR, IOCTRL_SSI2CR_D1_IE)
#define _ioctrl_ssi2_d1_input_en _bit_set(IOCTRL->SSI2CR, IOCTRL_SSI2CR_D1_IE)
#define _ioctrl_ssi2_d2_input_dis _bit_clr(IOCTRL->SSI2CR, IOCTRL_SSI2CR_D2_IE)
#define _ioctrl_ssi2_d2_input_en _bit_set(IOCTRL->SSI2CR, IOCTRL_SSI2CR_D2_IE)
#define _ioctrl_ssi2_d3_input_dis _bit_clr(IOCTRL->SSI2CR, IOCTRL_SSI2CR_D3_IE)
#define _ioctrl_ssi2_d3_input_en _bit_set(IOCTRL->SSI2CR, IOCTRL_SSI2CR_D3_IE)
#define _ioctrl_ssi2_input_dis(bits) _bit_clr(IOCTRL->SSI1CR, (bits << IOCTRL_SSI2CR_IE))
#define _ioctrl_ssi2_input_en(bits) _bit_set(IOCTRL->SSI1CR, (bits << IOCTRL_SSI2CR_IE))
#define _ioctrl_ssi2_pull_down(bits) _bit_clr(IOCTRL->SSI1CR, (bits << IOCTRL_SSI2CR_PS))
#define _ioctrl_ssi2_pull_up(bits) _bit_set(IOCTRL->SSI1CR, (bits << IOCTRL_SSI2CR_PS))

/* SPIMSWAPCR  */
#define _ioctrl_spimswapcr_swap_dis(pins) _bit_clr(IOCTRL->SPIMSWAPCR, (0x01 << pins)) //OX28
#define _ioctrl_spimswapcr_swap_en(pins) _bit_set(IOCTRL->SPIMSWAPCR, (0x01 << pins))

/* CLCDCR  */
#define _ioctrl_clcd_ds_2ma _reg_modify(IOCTRL->CLCDCR, ~IOCTRL_CLCDCR_DS1_DS0, 0x00)
#define _ioctrl_clcd_ds_4ma _reg_modify(IOCTRL->CLCDCR, ~IOCTRL_CLCDCR_DS1_DS0, 0x01)
#define _ioctrl_clcd_ds_8ma _reg_modify(IOCTRL->CLCDCR, ~IOCTRL_CLCDCR_DS1_DS0, 0x02)
#define _ioctrl_clcd_ds_12ma _reg_modify(IOCTRL->CLCDCR, ~IOCTRL_CLCDCR_DS1_DS0, 0x03)
#define _ioctrl_clcd_slew_fast _bit_clr(IOCTRL->CLCDCR, IOCTRL_CLCDCR_SR)
#define _ioctrl_clcd_slew_slow _bit_set(IOCTRL->CLCDCR, IOCTRL_CLCDCR_SR)
#define _ioctrl_clcd_input_cmos _bit_clr(IOCTRL->CLCDCR, IOCTRL_CLCDCR_IS)
#define _ioctrl_clcd_input_schmitt _bit_set(IOCTRL->CLCDCR, IOCTRL_CLCDCR_IS)
#define _ioctrl_clcd_pull_up_down_dis _bit_clr(IOCTRL->CLCDCR, IOCTRL_CLCDCR_PUE)
#define _ioctrl_clcd_pull_up_down_en _bit_set(IOCTRL->CLCDCR, IOCTRL_CLCDCR_PUE)
#define _ioctrl_clcd_output_cmos _bit_clr(IOCTRL->CLCDCR, IOCTRL_CLCDCR_ODE)
#define _ioctrl_clcd_output_open_d _bit_set(IOCTRL->CLCDCR, IOCTRL_CLCDCR_ODE)
#define _ioctrl_clcd_pull_down(bits) _bit_clr(IOCTRL->CLCDCR, (bits << IOCTRL_CLCDCR_PS))
#define _ioctrl_clcd_pull_up(bits) _bit_set(IOCTRL->CLCDCR, (bits << IOCTRL_CLCDCR_PS))
#define _ioctrl_clcd_input_dis(bits) _bit_clr(IOCTRL->CLCDCR, (bits << IOCTRL_CLCDCR_IE))
#define _ioctrl_clcd_input_en(bits) _bit_set(IOCTRL->CLCDCR, (bits << IOCTRL_CLCDCR_IE))
/* CLCDDIECR  */
#define _ioctrl_clcd_data_input_dis(bits) _bit_clr(IOCTRL->CLCD_DIECR, (bits << IOCTRL_CLCDDIECR_IE))
#define _ioctrl_clcd_data_input_en(bits) _bit_set(IOCTRL->CLCD_DIECR, (bits << IOCTRL_CLCDDIECR_IE))
/* CLCDDPSCR  */
#define _ioctrl_clcd_data_pull_down(bits) _bit_clr(IOCTRL->CLCD_DPSCR, (bits << IOCTRL_CLCDDPSCR_PS))
#define _ioctrl_clcd_data_pull_up(bits) _bit_set(IOCTRL->CLCD_DPSCR, (bits << IOCTRL_CLCDDPSCR_PS))
/* DCMICR  */
#define _ioctrl_dcmi_ds_2ma _reg_modify(IOCTRL->DCMICR, ~IOCTRL_DCMICR_DS1_DS0, 0x00)
#define _ioctrl_dcmi_ds_4ma _reg_modify(IOCTRL->DCMICR, ~IOCTRL_DCMICR_DS1_DS0, 0x01)
#define _ioctrl_dcmi_ds_8ma _reg_modify(IOCTRL->DCMICR, ~IOCTRL_DCMICR_DS1_DS0, 0x02)
#define _ioctrl_dcmi_ds_12ma _reg_modify(IOCTRL->DCMICR, ~IOCTRL_DCMICR_DS1_DS0, 0x03)
#define _ioctrl_dcmi_slew_fast _bit_clr(IOCTRL->DCMICR, IOCTRL_DCMICR_SR)
#define _ioctrl_dcmi_slew_slow _bit_set(IOCTRL->DCMICR, IOCTRL_DCMICR_SR)
#define _ioctrl_dcmi_input_cmos _bit_clr(IOCTRL->DCMICR, IOCTRL_DCMICR_IS)
#define _ioctrl_dcmi_input_schmitt _bit_set(IOCTRL->DCMICR, IOCTRL_DCMICR_IS)
#define _ioctrl_dcmi_pull_up_down_dis _bit_clr(IOCTRL->DCMICR, IOCTRL_DCMICR_PUE)
#define _ioctrl_dcmi_pull_up_down_en _bit_set(IOCTRL->DCMICR, IOCTRL_DCMICR_PUE)
#define _ioctrl_dcmi_output_cmos _bit_clr(IOCTRL->DCMICR, IOCTRL_DCMICR_ODE)
#define _ioctrl_dcmi_output_open_d _bit_set(IOCTRL->DCMICR, IOCTRL_DCMICR_ODE)
#define _ioctrl_dcmi_pull_down(bits) _bit_clr(IOCTRL->DCMICR, (bits << IOCTRL_DCMICR_PS))
#define _ioctrl_dcmi_pull_up(bits) _bit_set(IOCTRL->DCMICR, (bits << IOCTRL_DCMICR_PS))
#define _ioctrl_dcmi_blank_pull_down _bit_clr(IOCTRL->DCMICR, IOCTRL_DCMICR_BLANK_PS)
#define _ioctrl_dcmi_blank_pull_up _bit_set(IOCTRL->DCMICR, IOCTRL_DCMICR_BLANK_PS)
#define _ioctrl_dcmi_hsync_pull_down _bit_clr(IOCTRL->DCMICR, IOCTRL_DCMICR_HSYNC_PS)
#define _ioctrl_dcmi_hsync_pull_up _bit_set(IOCTRL->DCMICR, IOCTRL_DCMICR_HSYNC_PS)
#define _ioctrl_dcmi_vsync_pull_down _bit_clr(IOCTRL->DCMICR, IOCTRL_DCMICR_VSYNC_PS)
#define _ioctrl_dcmi_vsync_pull_up _bit_set(IOCTRL->DCMICR, IOCTRL_DCMICR_VSYNC_PS)
#define _ioctrl_dcmi_enb_pull_down _bit_clr(IOCTRL->DCMICR, IOCTRL_DCMICR_ENB_PS)
#define _ioctrl_dcmi_enb_pull_up _bit_set(IOCTRL->DCMICR, IOCTRL_DCMICR_ENB_PS)
#define _ioctrl_dcmi_pwdn_pull_down _bit_clr(IOCTRL->DCMICR, IOCTRL_DCMICR_PWDN_PS)
#define _ioctrl_dcmi_pwdn_pull_up _bit_set(IOCTRL->DCMICR, IOCTRL_DCMICR_PWDN_PS)
#define _ioctrl_dcmi_resetn_pull_down _bit_clr(IOCTRL->DCMICR, IOCTRL_DCMICR_RESETN_PS)
#define _ioctrl_dcmi_resetn_pull_up _bit_set(IOCTRL->DCMICR, IOCTRL_DCMICR_RESETN_PS)
#define _ioctrl_dcmi_clkin_pull_down _bit_clr(IOCTRL->DCMICR, IOCTRL_DCMICR_CLKIN_PS)
#define _ioctrl_dcmi_clkin_pull_up _bit_set(IOCTRL->DCMICR, IOCTRL_DCMICR_CLKIN_PS)

#define _ioctrl_dcmi_input_dis(bits) _bit_clr(IOCTRL->DCMICR, (bits << IOCTRL_DCMICR_IE))
#define _ioctrl_dcmi_input_en(bits) _bit_set(IOCTRL->DCMICR, (bits << IOCTRL_DCMICR_IE))
#define _ioctrl_dcmi_blank_input_dis _bit_clr(IOCTRL->DCMICR, IOCTRL_DCMICR_BLANK_IE)
#define _ioctrl_dcmi_blank_input_en _bit_set(IOCTRL->DCMICR, IOCTRL_DCMICR_BLANK_IE)
#define _ioctrl_dcmi_hsync_input_dis _bit_clr(IOCTRL->DCMICR, IOCTRL_DCMICR_HSYNC_IE)
#define _ioctrl_dcmi_hsync_input_en _bit_set(IOCTRL->DCMICR, IOCTRL_DCMICR_HSYNC_IE)
#define _ioctrl_dcmi_vsync_input_dis _bit_clr(IOCTRL->DCMICR, IOCTRL_DCMICR_VSYNC_IE)
#define _ioctrl_dcmi_vsync_input_en _bit_set(IOCTRL->DCMICR, IOCTRL_DCMICR_VSYNC_IE)
#define _ioctrl_dcmi_data_input_dis _bit_clr(IOCTRL->DCMICR, IOCTRL_DCMICR_DATA_IE)
#define _ioctrl_dcmi_data_input_en _bit_set(IOCTRL->DCMICR, IOCTRL_DCMICR_DATA_IE)
#define _ioctrl_dcmi_enb_input_dis _bit_clr(IOCTRL->DCMICR, IOCTRL_DCMICR_ENB_IE)
#define _ioctrl_dcmi_enb_input_en _bit_set(IOCTRL->DCMICR, IOCTRL_DCMICR_ENB_IE)
#define _ioctrl_dcmi_pwdn_input_dis _bit_clr(IOCTRL->DCMICR, IOCTRL_DCMICR_PWDN_IE)
#define _ioctrl_dcmi_pwdn_input_en _bit_set(IOCTRL->DCMICR, IOCTRL_DCMICR_PWDN_IE)
#define _ioctrl_dcmi_resetn_input_dis _bit_clr(IOCTRL->DCMICR, IOCTRL_DCMICR_RESETN_IE)
#define _ioctrl_dcmi_resetn_input_en _bit_set(IOCTRL->DCMICR, IOCTRL_DCMICR_RESETN_IE)
#define _ioctrl_dcmi_clkin_input_dis _bit_clr(IOCTRL->DCMICR, IOCTRL_DCMICR_CLKIN_IE)
#define _ioctrl_dcmi_clkin_input_en _bit_set(IOCTRL->DCMICR, IOCTRL_DCMICR_CLKIN_IE)

/* SDRCR  */
#define _ioctrl_sdrcr_dc_10pf _reg_modify(IOCTRL->SDRCR, ~IOCTRL_SDRCR_DR1_DR0, 0x00)
#define _ioctrl_sdrcr_dc_20pf _reg_modify(IOCTRL->SDRCR, ~IOCTRL_SDRCR_DR1_DR0, 0x01)
#define _ioctrl_sdrcr_dc_30pf _reg_modify(IOCTRL->SDRCR, ~IOCTRL_SDRCR_DR1_DR0, 0x02)
#define _ioctrl_sdrcr_dc_50pf _reg_modify(IOCTRL->SDRCR, ~IOCTRL_SDRCR_DR1_DR0, 0x03)
#define _ioctrl_sdrcr_speed_up_dis _bit_clr(IOCTRL->SDRCR, IOCTRL_SDRCR_HX)
#define _ioctrl_sdrcr_speed_up_en _bit_set(IOCTRL->SDRCR, IOCTRL_SDRCR_HX)
#define _ioctrl_sdrcr_output_cmos _bit_clr(IOCTRL->SDRCR, IOCTRL_SDRCR_ODE)
#define _ioctrl_sdrcr_output_open_d _bit_set(IOCTRL->SDRCR, IOCTRL_SDRCR_ODE)
#define _ioctrl_sdrcr_pull_down_dis(bits) _bit_clr(IOCTRL->SDRCR, (bits << IOCTRL_SDRCR_PD))
#define _ioctrl_sdrcr_pull_down_en(bits) _bit_set(IOCTRL->SDRCR, (bits << IOCTRL_SDRCR_PD))
#define _ioctrl_sdrcr_pull_up_dis(bits) _bit_clr(IOCTRL->SDRCR, (bits << IOCTRL_SDRCR_PU))
#define _ioctrl_sdrcr_pull_up_en(bits) _bit_set(IOCTRL->SDRCR, (bits << IOCTRL_SDRCR_PU))
#define _ioctrl_sdrcr_input_dis(bits) _bit_clr(IOCTRL->SDRCR, (bits << IOCTRL_SDRCR_IE))
#define _ioctrl_sdrcr_input_en(bits) _bit_set(IOCTRL->SDRCR, (bits << IOCTRL_SDRCR_IE))

/* SMCR  */
#define _ioctrl_smcr_dc_10pf _reg_modify(IOCTRL->SMCR, ~IOCTRL_SMCR_DR1_DR0, 0x00)
#define _ioctrl_smcr_dc_20pf _reg_modify(IOCTRL->SMCR, ~IOCTRL_SMCR_DR1_DR0, 0x01)
#define _ioctrl_smcr_dc_30pf _reg_modify(IOCTRL->SMCR, ~IOCTRL_SMCR_DR1_DR0, 0x02)
#define _ioctrl_smcr_dc_50pf _reg_modify(IOCTRL->SMCR, ~IOCTRL_SMCR_DR1_DR0, 0x03)
#define _ioctrl_smcr_speed_up_dis _bit_clr(IOCTRL->SMCR, IOCTRL_SMCR_HX)
#define _ioctrl_smcr_speed_up_en _bit_set(IOCTRL->SMCR, IOCTRL_SMCR_HX)
#define _ioctrl_smcr_output_cmos _bit_clr(IOCTRL->SMCR, IOCTRL_SMCR_ODE)
#define _ioctrl_smcr_output_open_d _bit_set(IOCTRL->SMCR, IOCTRL_SMCR_ODE)
#define _ioctrl_smcr_pull_down_dis(bits) _bit_clr(IOCTRL->SMCR, (bits << IOCTRL_SMCR_PD))
#define _ioctrl_smcr_pull_down_en(bits) _bit_set(IOCTRL->SMCR, (bits << IOCTRL_SMCR_PD))
#define _ioctrl_smcr_pull_up_dis(bits) _bit_clr(IOCTRL->SMCR, (bits << IOCTRL_SMCR_PU))
#define _ioctrl_smcr_pull_up_en(bits) _bit_set(IOCTRL->SMCR, (bits << IOCTRL_SMCR_PU))
#define _ioctrl_smcr_input_dis(bits) _bit_clr(IOCTRL->SMCR, (bits << IOCTRL_SMCR_IE))
#define _ioctrl_smcr_input_en(bits) _bit_set(IOCTRL->SMCR, (bits << IOCTRL_SMCR_IE))

/* SDRAMDCR  */
#define _ioctrl_sdram_data_dc_10pf _reg_modify(IOCTRL->SDRAMDCR, ~IOCTRL_SDRAMDCR_DR1_DR0, 0x00)
#define _ioctrl_sdram_data_dc_20pf _reg_modify(IOCTRL->SDRAMDCR, ~IOCTRL_SDRAMDCR_DR1_DR0, 0x01)
#define _ioctrl_sdram_data_dc_30pf _reg_modify(IOCTRL->SDRAMDCR, ~IOCTRL_SDRAMDCR_DR1_DR0, 0x02)
#define _ioctrl_sdram_data_dc_50pf _reg_modify(IOCTRL->SDRAMDCR, ~IOCTRL_SDRAMDCR_DR1_DR0, 0x03)
#define _ioctrl_sdram_data_speed_up_dis _bit_clr(IOCTRL->SDRAMDCR, IOCTRL_SDRAMDCR_HX)
#define _ioctrl_sdram_data_speed_up_en _bit_set(IOCTRL->SDRAMDCR, IOCTRL_SDRAMDCR_HX)
#define _ioctrl_sdram_data_output_cmos _bit_clr(IOCTRL->SDRAMDCR, IOCTRL_SDRAMDCR_ODE)
#define _ioctrl_sdram_data_output_open_d _bit_set(IOCTRL->SDRAMDCR, IOCTRL_SDRAMDCR_ODE)
#define _ioctrl_sdram_data_pull_down_dis(bits) _bit_clr(IOCTRL->SDRAMDCR, (bits << IOCTRL_SDRAMDCR_PD))
#define _ioctrl_sdram_data_pull_down_en(bits) _bit_set(IOCTRL->SDRAMDCR, (bits << IOCTRL_SDRAMDCR_PD))
#define _ioctrl_sdram_data_pull_up_dis(bits) _bit_clr(IOCTRL->SDRAMDCR, (bits << IOCTRL_SDRAMDCR_PU))
#define _ioctrl_sdram_data_pull_up_en(bits) _bit_set(IOCTRL->SDRAMDCR, (bits << IOCTRL_SDRAMDCR_PU))
#define _ioctrl_sdram_data_input_dis(bits) _bit_clr(IOCTRL->SDRAMDCR, (bits << IOCTRL_SDRAMDCR_IE))
#define _ioctrl_sdram_data_input_en(bits) _bit_set(IOCTRL->SDRAMDCR, (bits << IOCTRL_SDRAMDCR_IE))

/* MADDRIECR  */
#define _ioctrl_sdram_addr_input_dis(pins) _bit_clr(IOCTRL->M_ADDRIECR, (0x01 << pins))
#define _ioctrl_sdram_addr_input_en(pins) _bit_set(IOCTRL->M_ADDRIECR, (0x01 << pins))
/* MADDRPUCR  */
#define _ioctrl_sdram_addr_pull_up_dis(pins) _bit_clr(IOCTRL->M_ADDRPUCR, (0x01 << pins))
#define _ioctrl_sdram_addr_pull_up_en(pins) _bit_set(IOCTRL->M_ADDRPUCR, (0x01 << pins))
/* MADDRPDCR  */
#define _ioctrl_sdram_addr_pull_down_dis(pins) _bit_clr(IOCTRL->M_ADDRPDCR, (0x01 << pins))
#define _ioctrl_sdram_addr_pull_down_en(pins) _bit_set(IOCTRL->M_ADDRPDCR, (0x01 << pins))
/* SWAPHCR  */
#define _ioctrl_swaphcr_swap_dis(pins) _bit_clr(IOCTRL->SWAPHCR, (0x01 << pins))
#define _ioctrl_swaphcr_swap_en(pins) _bit_set(IOCTRL->SWAPHCR, (0x01 << pins))
/* I2SCR  */
#define _ioctrl_i2s_ds_2ma _reg_modify(IOCTRL->I2SCR, ~IOCTRL_I2SCR_DS1_DS0, 0x00)
#define _ioctrl_i2s_ds_4ma _reg_modify(IOCTRL->I2SCR, ~IOCTRL_I2SCR_DS1_DS0, 0x01)
#define _ioctrl_i2s_ds_8ma _reg_modify(IOCTRL->I2SCR, ~IOCTRL_I2SCR_DS1_DS0, 0x02)
#define _ioctrl_i2s_ds_12ma _reg_modify(IOCTRL->I2SCR, ~IOCTRL_I2SCR_DS1_DS0, 0x03)
#define _ioctrl_i2s_slew_fast _bit_clr(IOCTRL->I2SCR, IOCTRL_I2SCR_SR)
#define _ioctrl_i2s_slew_slow _bit_set(IOCTRL->I2SCR, IOCTRL_I2SCR_SR)
#define _ioctrl_i2s_input_cmos _bit_clr(IOCTRL->I2SCR, IOCTRL_I2SCR_IS)
#define _ioctrl_i2s_input_schmitt _bit_set(IOCTRL->I2SCR, IOCTRL_I2SCR_IS)
#define _ioctrl_i2s_sd_pull_up_down_dis _bit_clr(IOCTRL->I2SCR, IOCTRL_I2SCR_SD_PUE)
#define _ioctrl_i2s_sd_pull_up_down_en _bit_set(IOCTRL->I2SCR, IOCTRL_I2SCR_SD_PUE)
#define _ioctrl_i2s_lrck_pull_up_down_dis _bit_clr(IOCTRL->I2SCR, IOCTRL_I2SCR_LRCK_PUE)
#define _ioctrl_i2s_lrck_pull_up_down_en _bit_set(IOCTRL->I2SCR, IOCTRL_I2SCR_LRCK_PUE)
#define _ioctrl_i2s_sclk_pull_up_down_dis _bit_clr(IOCTRL->I2SCR, IOCTRL_I2SCR_SCLK_PS)
#define _ioctrl_i2s_sclk_pull_up_down_en _bit_set(IOCTRL->I2SCR, IOCTRL_I2SCR_SCLK_PUE)
#define _ioctrl_i2s_audrstn_pull_up_down_dis _bit_clr(IOCTRL->I2SCR, IOCTRL_I2SCR_AUDRSTN_PUE)
#define _ioctrl_i2s_audrstn_pull_up_down_en _bit_set(IOCTRL->I2SCR, IOCTRL_I2SCR_AUDRSTN_PUE)
#define _ioctrl_i2s_sd_pull_down _bit_clr(IOCTRL->I2SCR, IOCTRL_I2SCR_SD_PS)
#define _ioctrl_i2s_sd_pull_up _bit_set(IOCTRL->I2SCR, IOCTRL_I2SCR_SD_PS)
#define _ioctrl_i2s_lrck_pull_down _bit_clr(IOCTRL->I2SCR, IOCTRL_I2SCR_LRCK_PS)
#define _ioctrl_i2s_lrck_pull_up _bit_set(IOCTRL->I2SCR, IOCTRL_I2SCR_LRCK_PS)
#define _ioctrl_i2s_sclk_pull_down _bit_clr(IOCTRL->I2SCR, IOCTRL_I2SCR_SCLK_PS)
#define _ioctrl_i2s_sclk_pull_up _bit_set(IOCTRL->I2SCR, IOCTRL_I2SCR_SCLK_PS)
#define _ioctrl_i2s_audrstn_pull_down _bit_clr(IOCTRL->I2SCR, IOCTRL_I2SCR_AUDRSTN_PS)
#define _ioctrl_i2s_audrstn_pull_up _bit_set(IOCTRL->I2SCR, IOCTRL_I2SCR_AUDRSTN_PS)
#define _ioctrl_i2s_sd_input_dis _bit_clr(IOCTRL->I2SCR, IOCTRL_I2SCR_SD_IE)
#define _ioctrl_i2s_sd_input_en _bit_set(IOCTRL->I2SCR, IOCTRL_I2SCR_SD_IE)
#define _ioctrl_i2s_lrck_input_dis _bit_clr(IOCTRL->I2SCR, IOCTRL_I2SCR_LRCK_IE)
#define _ioctrl_i2s_lrck_input_en _bit_set(IOCTRL->I2SCR, IOCTRL_I2SCR_LRCK_IE)
#define _ioctrl_i2s_sclk_input_dis _bit_clr(IOCTRL->I2SCR, IOCTRL_I2SCR_SCLK_IE)
#define _ioctrl_i2s_sclk_input_en _bit_set(IOCTRL->I2SCR, IOCTRL_I2SCR_SCLK_IE)
#define _ioctrl_i2s_audrstn_input_dis _bit_clr(IOCTRL->I2SCR, IOCTRL_I2SCR_AUDRSTN_IE)
#define _ioctrl_i2s_audrstn_input_en _bit_set(IOCTRL->I2SCR, IOCTRL_I2SCR_AUDRSTN_IE)
#define _ioctrl_i2s_input_dis(bits) _bit_clr(IOCTRL->I2SCR, (bits << IOCTRL_I2SCR_IE))
#define _ioctrl_i2s_input_en(bits) _bit_set(IOCTRL->I2SCR, (bits << IOCTRL_I2SCR_IE))
#define _ioctrl_i2s_pull_down(bits) _bit_clr(IOCTRL->I2SCR, (bits << IOCTRL_I2SCR_PS))
#define _ioctrl_i2s_pull_up(bits) _bit_set(IOCTRL->I2SCR, (bits << IOCTRL_I2SCR_PS))
#define _ioctrl_i2s_pull_up_down_dis(bits) _bit_clr(IOCTRL->I2SCR, (bits << IOCTRL_I2SCR_PUE))
#define _ioctrl_i2s_pull_up_down_en(bits) _bit_set(IOCTRL->I2SCR, (bits << IOCTRL_I2SCR_PUE))
/* SWAPINTCR  */
#define _ioctrl_swapintcr_swap_dis(pins) _bit_clr(IOCTRL->SWAPINTCR, (0x01 << pins))
#define _ioctrl_swapintcr_swap_en(pins) _bit_set(IOCTRL->SWAPINTCR, (0x01 << pins))
/* SWAP2CR  */
#define _ioctrl_swap2cr_swap_dis(pins) _bit_clr(IOCTRL->SWAP2CR, (0x01 << pins))
#define _ioctrl_swap2cr_swap_en(pins) _bit_set(IOCTRL->SWAP2CR, (0x01 << pins))
/* SWAP3CR  */
#define _ioctrl_swap3cr_swap_dis(pins) _bit_clr(IOCTRL->SWAP3CR, (0x01 << pins))
#define _ioctrl_swap3cr_swap_en(pins) _bit_set(IOCTRL->SWAP3CR, (0x01 << pins))
/* SWAP4CR  */
#define _ioctrl_swap4cr_swap_dis(pins) _bit_clr(IOCTRL->SWAP4CR, (0x01 << pins))
#define _ioctrl_swap4cr_swap_en(pins) _bit_set(IOCTRL->SWAP4CR, (0x01 << pins))
/* SWAP5CR  */
#define _ioctrl_swap5cr_swap_dis(pins) _bit_clr(IOCTRL->SWAP5CR, (0x01 << pins))
#define _ioctrl_swap5cr_swap_en(pins) _bit_set(IOCTRL->SWAP5CR, (0x01 << pins))
/* SDRAMSWAPCR  */
#define _ioctrl_sdramswap_swap_dis(pins) _bit_clr(IOCTRL->SDRAMSWAPCR, (0x01 << pins))
#define _ioctrl_sdramswap_swap_en(pins) _bit_set(IOCTRL->SDRAMSWAPCR, (0x01 << pins))

/**
  * @}
  */

/*** 结构体、枚举变量定义 *****************************************************/
#ifdef FILE_OUT
/** @defgroup DRV_IOCTRL_Exported_Types Exported Types
  *
  * @{
  */
#endif

  typedef enum
  {
    DC_NULL = 0,
    DC_10PF,
    DC_20PF,
    DC_30PF,
    DC_50PF,
  } IOCTRL_DCTypeDef;
  typedef enum
  {
    DS_NULL = 0,
    DS_2MA,
    DS_4MA,
    DS_8MA,
    DS_12MA,
  } IOCTRL_DSTypeDef;

  typedef enum
  {
    SR_NULL = 0,
    SR_SLEW_FAST,
    SR_SLEW_SLOW,
  } IOCTRL_SRTypeDef;

  typedef enum
  {
    IS_NULL = 0,
    IS_INPUT_CMOS,
    IS_INPUT_SCHMITT,
  } IOCTRL_ISTypeDef;

  typedef enum
  {
    PS_PULL_NULL = 0,
    PS_PULL_DOWN,
    PS_PULL_UP,
  } IOCTRL_PSTypeDef;

  typedef enum
  {
    HX_SPEED_UP_NULL = 0,
    HX_SPEED_UP_DIS,
    HX_SPEED_UP_EN,
  } IOCTRL_HXTypeDef;

  typedef enum
  {
    IE_INPUT_NULL = 0,
    IE_INPUT_DIS,
    IE_INPUT_EN,
  } IOCTRL_IETypeDef;

  typedef enum
  {
    PUE_PULL_NULL = 0,
    PUE_PULL_DIS,
    PUE_PULL_EN,
  } IOCTRL_PUETypeDef;

  typedef enum
  {
    PUDE_PULL_DOWN_NULL = 0,
    PUDE_PULL_DOWN_DIS,
    PUDE_PULL_DOWN_EN,
  } IOCTRL_PDETypeDef;

  typedef enum
  {
    ODE_OUTPUT_NULL = 0,
    ODE_OUTPUT_CMOS,
    ODE_OUTPUT_OPEN_DRAIN,
  } IOCTRL_ODETypeDef;

  typedef enum
  {
    IOCTRL_SPI1 = 0,
    IOCTRL_SPI2,
    IOCTRL_SPI3,
  } IOCTRL_SPIIDTypeDef;

  typedef enum
  {
    IOCTRL_USI1 = 0,
    IOCTRL_USI2,
    IOCTRL_USI3,
  } IOCTRL_USIIDTypeDef;

  typedef enum
  {
    IOCTRL_I2C1 = 0,
    IOCTRL_I2C2,
    IOCTRL_I2C3,
  } IOCTRL_I2CIDTypeDef;

  typedef enum
  {
    IOCTRL_UART1 = 0,
    IOCTRL_UART2,
    IOCTRL_UART3,
  } IOCTRL_UARTIDTypeDef;

  typedef enum
  {
    IOCTRL_SSI1 = 0,
    IOCTRL_SSI2,
  } IOCTRL_SSIIDTypeDef;

  typedef enum
  {
    IOCTRL_GINTL = 0,
    IOCTRL_GINTH,
  } IOCTRL_GINTIDTypeDef;

  typedef enum
  {
    IOCTRL_SPI_SS = 0x01,
    IOCTRL_SPI_SCK = 0x02,
    IOCTRL_SPI_MISO = 0x04,
    IOCTRL_SPI_MOSI = 0x08,
  } IOCTRL_SPIPinTypeDef;

  typedef enum
  {
    IOCTRL_USI_ISORST = 0x01,
    IOCTRL_USI_ISODAT = 0x02,
    IOCTRL_USI_ISOCLK = 0x04,
  } IOCTRL_USIPinTypeDef;

  typedef enum
  {
    IOCTRL_I2C_SCL = 0x01,
    IOCTRL_I2C_SDA = 0x02,
  } IOCTRL_I2CPinTypeDef;

  typedef enum
  {
    IOCTRL_UART_TXD = 0x01,
    IOCTRL_UART_RXD = 0x02,
    IOCTRL_UART_CTS = 0x08,
  } IOCTRL_UARTPinTypeDef;

  typedef enum
  {
    IOCTRL_PIN_BIT0 = 0x01,
    IOCTRL_PIN_BIT1 = 0x02,
    IOCTRL_PIN_BIT2 = 0x04,
    IOCTRL_PIN_BIT3 = 0x08,
    IOCTRL_PIN_BIT4 = 0x10,
    IOCTRL_PIN_BIT5 = 0x20,
    IOCTRL_PIN_BIT6 = 0x40,
    IOCTRL_PIN_BIT7 = 0x80,
  } IOCTRL_PinBitTypeDef;

  typedef enum
  {
    IOCTRL_SSI_SS = 0x01,
    IOCTRL_SSI_SCK = 0x02,
    IOCTRL_SSI_D0 = 0x04,
    IOCTRL_SSI_D1 = 0x08,
    IOCTRL_SSI_D2 = 0x10,
    IOCTRL_SSI_D3 = 0x20,
  } IOCTRL_SSIPinTypeDef;

  typedef enum
  {
    TX1_GINT4 = 0x10,
    RX1_GINT0 = 0x01,
    TX2_GINT5 = 0x20,
    RX2_GINT3 = 0x08,
    TX3_GINT1 = 0x02,
    RX3_GINT2 = 0x04,
  } IOCTRL_UARTSwapGintTypeDef;

  typedef enum
  {
    GINT_EN = 0,
    UART_EN,

  } IOCTRL_UARTSwapGintENTypeDef;

  typedef enum
  {
    IOCTRL_SWAP0 = 0,
    IOCTRL_SWAP1,
    IOCTRL_SWAP2,
    IOCTRL_SWAP3,
    IOCTRL_SWAP4,
    IOCTRL_SWAP5,
    IOCTRL_SWAP6,
    IOCTRL_SWAP7,
    IOCTRL_SWAP8,
    IOCTRL_SWAP9,
    IOCTRL_SWAP10,
    IOCTRL_SWAP11,
    IOCTRL_SWAP12,
    IOCTRL_SWAP13,
    IOCTRL_SWAP14,
    IOCTRL_SWAP15,
    IOCTRL_SWAP16,
    IOCTRL_SWAP17,
    IOCTRL_SWAP18,
    IOCTRL_SWAP19,
    IOCTRL_SWAP20,
    IOCTRL_SWAP21,
    IOCTRL_SWAP22,
    IOCTRL_SWAP23,
    IOCTRL_SWAP24,
    IOCTRL_SWAP25,
    IOCTRL_SWAP26,
    IOCTRL_SWAP27,
    IOCTRL_SWAP28,
    IOCTRL_SWAP29,
    IOCTRL_SWAP30,
    IOCTRL_SWAP31,
  } IOCTRL_SwapTypeDef;

/**
  * @}
  */

/*** 全局变量声明 **************************************************************/
#ifdef FILE_OUT
/** @defgroup DRV_IOCTRL_Exported_Variables Exported Variables
  *
  * @{
  */
#endif

/**
  * @}
  */

/*** 函数声明 ******************************************************************/
#ifdef FILE_OUT
/** @defgroup DRV_IOCTRL_Exported_Functions Exported Functions
  * @{
  */
#endif
  extern void DRV_IOCTRL_SetSPIDS(IOCTRL_DSTypeDef ds);
  extern void DRV_IOCTRL_SetSPISR(IOCTRL_SRTypeDef sr);
  extern void DRV_IOCTRL_SetSPIIS(IOCTRL_ISTypeDef is);
  extern void DRV_IOCTRL_SetSPI1PS(uint8_t pins, IOCTRL_PSTypeDef ps);
  extern void DRV_IOCTRL_SetSPI2PS(uint8_t pins, IOCTRL_PSTypeDef ps);
  extern void DRV_IOCTRL_SetSPI3PS(uint8_t pins, IOCTRL_PSTypeDef ps);
  extern void DRV_IOCTRL_SetSPI1IE(uint8_t pins, IOCTRL_IETypeDef ie);
  extern void DRV_IOCTRL_SetSPI2IE(uint8_t pins, IOCTRL_IETypeDef ie);
  extern void DRV_IOCTRL_SetSPI3IE(uint8_t pins, IOCTRL_IETypeDef ie);
  extern void DRV_IOCTRL_SetUSIDS(IOCTRL_DSTypeDef ds);
  extern void DRV_IOCTRL_SetUSISR(IOCTRL_SRTypeDef sr);
  extern void DRV_IOCTRL_SetUSIIS(IOCTRL_ISTypeDef is);
  extern void DRV_IOCTRL_SetUSI1PS(uint8_t pins, IOCTRL_PSTypeDef ps);
  extern void DRV_IOCTRL_SetUSI2DREN(FunctionalState status);
  extern void DRV_IOCTRL_SetUSI2SWAPDis(uint8_t pins);
  extern void DRV_IOCTRL_SetUSI2SWAPEn(uint8_t pins);
  extern void DRV_IOCTRL_SetUSI2DIEN(uint8_t pins, FunctionalState status);
  extern void DRV_IOCTRL_SetUSI3DREN(FunctionalState status);
  extern void DRV_IOCTRL_SetUSI3PUEN(uint8_t pins, FunctionalState status);
  extern void DRV_IOCTRL_SetUSI3DIEN(uint8_t pins, FunctionalState status);
  extern void DRV_IOCTRL_SetI2CDS(IOCTRL_DSTypeDef ds);
  extern void DRV_IOCTRL_SetI2CSR(IOCTRL_SRTypeDef sr);
  extern void DRV_IOCTRL_SetI2CIS(IOCTRL_ISTypeDef is);
  extern void DRV_IOCTRL_SetI2C1PS(uint8_t pins, IOCTRL_PSTypeDef ps);
  extern void DRV_IOCTRL_SetI2C2PS(uint8_t pins, IOCTRL_PSTypeDef ps);
  extern void DRV_IOCTRL_SetI2C3PS(uint8_t pins, IOCTRL_PSTypeDef ps);
  extern void DRV_IOCTRL_SetI2C1IE(uint8_t pins, IOCTRL_IETypeDef ie);
  extern void DRV_IOCTRL_SetI2C2IE(uint8_t pins, IOCTRL_IETypeDef ie);
  extern void DRV_IOCTRL_SetI2C3IE(uint8_t pins, IOCTRL_IETypeDef ie);
  extern void DRV_IOCTRL_SetUARTDS(IOCTRL_DSTypeDef ds);
  extern void DRV_IOCTRL_SetUARTSR(IOCTRL_SRTypeDef sr);
  extern void DRV_IOCTRL_SetUARTIS(IOCTRL_ISTypeDef is);
  extern void DRV_IOCTRL_SetUART1PS(uint8_t pins, IOCTRL_PSTypeDef ps);
  extern void DRV_IOCTRL_SetUART2PS(uint8_t pins, IOCTRL_PSTypeDef ps);
  extern void DRV_IOCTRL_SetUART3PS(uint8_t pins, IOCTRL_PSTypeDef ps);
  extern void DRV_IOCTRL_UARTSwapGint(uint8_t pins, IOCTRL_UARTSwapGintENTypeDef swap);
  extern void DRV_IOCTRL_SetGINTLDS(IOCTRL_DSTypeDef ds);
  extern void DRV_IOCTRL_SetGINTLSR(IOCTRL_SRTypeDef sr);
  extern void DRV_IOCTRL_SetGINTLIS(IOCTRL_ISTypeDef is);
  extern void DRV_IOCTRL_SetGINTLIE(uint8_t pins, IOCTRL_IETypeDef ie);
  extern void DRV_IOCTRL_SetGINTLPS(uint8_t pins, IOCTRL_PSTypeDef ps);
  extern void DRV_IOCTRL_SetGINTHDS(IOCTRL_DSTypeDef ds);
  extern void DRV_IOCTRL_SetGINTHSR(IOCTRL_SRTypeDef sr);
  extern void DRV_IOCTRL_SetGINTHIS(IOCTRL_ISTypeDef is);
  extern void DRV_IOCTRL_SetGINTHIE(uint8_t pins, IOCTRL_IETypeDef ie);
  extern void DRV_IOCTRL_SetGINTHPS(uint8_t pins, IOCTRL_PSTypeDef ps);
  extern void DRV_IOCTRL_SwapEn(IOCTRL_SwapTypeDef pSwap);
  extern void DRV_IOCTRL_SwapDis(IOCTRL_SwapTypeDef pSwap);
  extern void DRV_IOCTRL_SetGINT3130DS(IOCTRL_DSTypeDef ds);
  extern void DRV_IOCTRL_SetGINT3130SR(IOCTRL_SRTypeDef sr);
  extern void DRV_IOCTRL_SetGINT3130IS(IOCTRL_ISTypeDef is);
  extern void DRV_IOCTRL_SetGINT3932DS(IOCTRL_DSTypeDef ds);
  extern void DRV_IOCTRL_SetGINT3932SR(IOCTRL_SRTypeDef sr);
  extern void DRV_IOCTRL_SetGINT3932IS(IOCTRL_ISTypeDef is);
  extern void DRV_IOCTRL_SetGINT2922DS(IOCTRL_DSTypeDef ds);
  extern void DRV_IOCTRL_SetGINT2922SR(IOCTRL_SRTypeDef sr);
  extern void DRV_IOCTRL_SetGINT2922IS(IOCTRL_ISTypeDef is);
  extern void DRV_IOCTRL_SetGINT2922PS(uint8_t pins, IOCTRL_PSTypeDef ps);
  extern void DRV_IOCTRL_SetSSI1DS(IOCTRL_DSTypeDef ds);
  extern void DRV_IOCTRL_SetSSI1SR(IOCTRL_SRTypeDef sr);
  extern void DRV_IOCTRL_SetSSI1IS(IOCTRL_ISTypeDef is);
  extern void DRV_IOCTRL_SetSSI1PUE(IOCTRL_PUETypeDef pue);
  extern void DRV_IOCTRL_SetSSI1ODE(IOCTRL_ODETypeDef ode);
  extern void DRV_IOCTRL_SetSSI1IE(uint8_t pins, IOCTRL_IETypeDef ie);
  extern void DRV_IOCTRL_SetSSI1PS(uint8_t pins, IOCTRL_PSTypeDef ps);
  extern void DRV_IOCTRL_SetSSI2DS(IOCTRL_DSTypeDef ds);
  extern void DRV_IOCTRL_SetSSI2SR(IOCTRL_SRTypeDef sr);
  extern void DRV_IOCTRL_SetSSI2IS(IOCTRL_ISTypeDef is);
  extern void DRV_IOCTRL_SetSSI2PUE(IOCTRL_PUETypeDef pue);
  extern void DRV_IOCTRL_SetSSI2ODE(IOCTRL_ODETypeDef ode);
  extern void DRV_IOCTRL_SetSSI2IE(uint8_t pins, IOCTRL_IETypeDef ie);
  extern void DRV_IOCTRL_SetSSI2PS(uint8_t pins, IOCTRL_PSTypeDef ps);
  extern void DRV_IOCTRL_SPIMSwapEn(IOCTRL_SwapTypeDef pSwap);
  extern void DRV_IOCTRL_SPIMSwapEn(IOCTRL_SwapTypeDef pSwap);
  extern void DRV_IOCTRL_SetCLCDDS(IOCTRL_DSTypeDef ds);
  extern void DRV_IOCTRL_SetCLCDSR(IOCTRL_SRTypeDef sr);
  extern void DRV_IOCTRL_SetCLCDIS(IOCTRL_ISTypeDef is);
  extern void DRV_IOCTRL_SetCLCDPUE(IOCTRL_PUETypeDef pue);
  extern void DRV_IOCTRL_SetCLCDIE(uint8_t pins, IOCTRL_IETypeDef ie);
  extern void DRV_IOCTRL_SetCLCDPS(uint8_t pins, IOCTRL_PSTypeDef ie);
  extern void DRV_IOCTRL_SetCLCDDATAIE(uint32_t pins, IOCTRL_IETypeDef ie);
  extern void DRV_IOCTRL_SetCLCDDATAPS(uint32_t pins, IOCTRL_PSTypeDef ps);
  extern void DRV_IOCTRL_SetCLCDDATAPS(uint32_t pins, IOCTRL_PSTypeDef ps);
  extern void DRV_IOCTRL_SetDCMISR(IOCTRL_SRTypeDef sr);
  extern void DRV_IOCTRL_SetDCMIIS(IOCTRL_ISTypeDef is);
  extern void DRV_IOCTRL_SetDCMIPUE(IOCTRL_PUETypeDef pue);
  extern void DRV_IOCTRL_SetDCMIIE(uint8_t pins, IOCTRL_IETypeDef ie);
  extern void DRV_IOCTRL_SetDCMIPS(uint8_t pins, IOCTRL_PSTypeDef ie);
  extern void DRV_IOCTRL_SetSDRIE(uint8_t pins, IOCTRL_IETypeDef ie);
  extern void DRV_IOCTRL_SetSDRPDE(uint8_t pins, IOCTRL_PDETypeDef pde);
  extern void DRV_IOCTRL_SetSDRPUE(uint8_t pins, IOCTRL_PUETypeDef pue);
  extern void DRV_IOCTRL_SetSDRPUE(uint8_t pins, IOCTRL_PUETypeDef pue);
  extern void DRV_IOCTRL_SetSDRHX(IOCTRL_HXTypeDef hx);
  extern void DRV_IOCTRL_SetSDRDC(IOCTRL_DCTypeDef dc);
  extern void DRV_IOCTRL_SetSMIE(uint8_t pins, IOCTRL_IETypeDef ie);
  extern void DRV_IOCTRL_SetSMPDE(uint8_t pins, IOCTRL_PDETypeDef pde);
  extern void DRV_IOCTRL_SetSMPUE(uint8_t pins, IOCTRL_PUETypeDef pue);
  extern void DRV_IOCTRL_SetSMPUE(uint8_t pins, IOCTRL_PUETypeDef pue);
  extern void DRV_IOCTRL_SetSMHX(IOCTRL_HXTypeDef hx);
  extern void DRV_IOCTRL_SetSMDC(IOCTRL_DCTypeDef dc);
  extern void DRV_IOCTRL_SetSDRAMDATAIE(uint8_t pins, IOCTRL_IETypeDef ie);
  extern void DRV_IOCTRL_SetSDRAMDATAPDE(uint8_t pins, IOCTRL_PDETypeDef pde);
  extern void DRV_IOCTRL_SetSDRAMDATAPUE(uint8_t pins, IOCTRL_PUETypeDef pue);
  extern void DRV_IOCTRL_SetSDRAMDATAODE(IOCTRL_ODETypeDef ode);
  extern void DRV_IOCTRL_SetSDRAMDATAHX(IOCTRL_HXTypeDef hx);
  extern void DRV_IOCTRL_SetSDRAMDATADC(IOCTRL_DCTypeDef dc);
  extern void DRV_IOCTRL_SetSDRAMADDRIE(uint32_t pins, IOCTRL_IETypeDef ie);
  extern void DRV_IOCTRL_SetSDRAMADDRPDE(uint32_t pins, IOCTRL_PDETypeDef pde);
  extern void DRV_IOCTRL_SetSDRAMADDRPUE(uint32_t pins, IOCTRL_PUETypeDef pue);
  extern void DRV_IOCTRL_SwaphEn(IOCTRL_SwapTypeDef pSwap);
  extern void DRV_IOCTRL_SwaphDisable(IOCTRL_SwapTypeDef pSwap);
  extern void DRV_IOCTRL_SetI2SDS(IOCTRL_DSTypeDef ds);
  extern void DRV_IOCTRL_SetI2SSR(IOCTRL_SRTypeDef sr);
  extern void DRV_IOCTRL_SetI2SIS(IOCTRL_ISTypeDef is);
  extern void DRV_IOCTRL_SetI2SIE(uint8_t pins, IOCTRL_IETypeDef ie);
  extern void DRV_IOCTRL_SetI2SPS(uint8_t pins, IOCTRL_PSTypeDef ps);
  extern void DRV_IOCTRL_SetI2SPUE(uint8_t pins, IOCTRL_PUETypeDef pue);
  extern void DRV_IOCTRL_SwapintEn(IOCTRL_SwapTypeDef pSwap);
  extern void DRV_IOCTRL_SwapintDisable(IOCTRL_SwapTypeDef pSwap);
  extern void DRV_IOCTRL_Swap2En(IOCTRL_SwapTypeDef pSwap);
  extern void DRV_IOCTRL_Swap2Disable(IOCTRL_SwapTypeDef pSwap);
  extern void DRV_IOCTRL_Swap3En(IOCTRL_SwapTypeDef pSwap);
  extern void DRV_IOCTRL_Swap3Disable(IOCTRL_SwapTypeDef pSwap);
  extern void DRV_IOCTRL_Swap4En(IOCTRL_SwapTypeDef pSwap);
  extern void DRV_IOCTRL_Swap4Disable(IOCTRL_SwapTypeDef pSwap);
  extern void DRV_IOCTRL_Swap4Disable(IOCTRL_SwapTypeDef pSwap);
  extern void DRV_IOCTRL_Swap5Disable(IOCTRL_SwapTypeDef pSwap);
  extern void DRV_IOCTRL_SdramswapEn(IOCTRL_SwapTypeDef pSwap);
  extern void DRV_IOCTRL_SdramswapDisable(IOCTRL_SwapTypeDef pSwap);

  /**
  * @}
  */

  /**
  *@}
  */

  /**
  *@}
  */

#ifdef __cplusplus
}
#endif

#endif /* __IOCTRL_DRV_H */

/************************ (C) COPYRIGHT LEVETOP *****END OF FILE*************/
