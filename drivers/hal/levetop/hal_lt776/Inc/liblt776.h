/**
	******************************************************************************
             Copyright(c) 2021 Levetop Semiconductor Co. Ltd.
                      All Rights Reserved
  ******************************************************************************
  * @file    liblt776.h
  * @author  Product application department
  * @version V1.0
  * @date    2021.11.01
  * @brief   Header file of lt776 库函数接口.
  * @note 库接口包括
  *   - eflash 接口
  *   - get sn 接口
  *   - clk switch 接口
  *   - lock jtag 接口
  *   - reg option 接口
  * 
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __LIBlt776_H
#define __LIBlt776_H

#ifdef __cplusplus
extern "C"
{
#endif

/* Includes ------------------------------------------------------------------*/
#include "type.h"

#ifdef FILE_OUT
/**@defgroup DRV 3 HAL driver
  *
  *@{
  */

/** @defgroup DRV_LIB LIB lt776
  *
  *@{
  */
#endif
/*** 宏定义 *******************************************************************/
#ifdef FILE_OUT
/** @defgroup DRV_LIB_Exported_Macros Exported Macros
  *
  * @{
  */
#endif
#define LOCK_JTAG_KEY 0xA5A5A5A5 /**<  */

#define EFLASH_BOOT (2)
#define SPIFLASH_BOOT (3)

/**
  * @}
  */

/*** 结构体、枚举变量定义 *****************************************************/
#ifdef FILE_OUT
/** @defgroup DRV_LIB_Exported_Types Exported Types
  *
  * @{
  */
#endif

/**
  * @}
  */

/*** 全局变量声明 **************************************************************/
#ifdef FILE_OUT
/** @defgroup DRV_LIB_Exported_Variables Exported Variables
  *
  * @{
  */
#endif

/**
  * @}
  */

/*** 函数声明 ******************************************************************/
#ifdef FILE_OUT
/** @defgroup DRV_LIB_Exported_Functions Exported Functions
  * @{
  */
#endif

  /* 获取库的版本 */
  extern uint32_t Lib_GetVersion(void);

  /* EFLASH 函数接口 */
  extern void LIB_EFLASH_Init(uint32_t f_eflash_khz);

  extern uint8_t LIB_EFLASH_Program(uint32_t addr, uint32_t data);

  extern uint8_t LIB_EFLASH_PageErase(uint32_t addr);

  extern uint8_t LIB_EFLASH_BulkProgram(uint32_t addr, uint32_t num_words, uint32_t *data);

  extern uint8_t LIB_EFLASH_UpdateWord(uint32_t addr, uint32_t data);

  //extern uint8_t LIB_EFLASH_Disboot(void);
  extern uint8_t LIB_EFlash_Chip_Mode(uint8_t choose);

  extern uint8_t LIB_EFLASH_RecoveryToBoot(void);

  extern void LIB_EFLASH_SetMainPermission(uint8_t writeable, uint8_t readable);

  extern void LIB_EFLASH_SetWritePermission(void);

  extern void LIB_EFLASH_ClrWritePermission(void);

  /* 禁止JTAG函数接口，执行后芯片将不能仿真 */
  extern void LIB_LockDisJtag(uint32_t key);

  /* 获取芯片SN号函数接口，长度8字节 */
  extern void LIB_GetSerialNumber(uint8_t *buff);

  /* CPM PWRCR 寄存器特殊操作接口 */
  extern uint8_t LIB_CPM_OptBitsToPWRCR(uint32_t bits, uint8_t opt);

  /* 时钟源切换接口 */
  extern void LIB_CPM_OscSwitch(int32_t osc_sel);

  /**
  * @}
  */

  /**
  *@}
  */

  /**
  *@}
  */

#ifdef __cplusplus
}
#endif

#endif /* __LIBlt776_H */

/************************ (C) COPYRIGHT LEVETOP *****END OF FILE****/
