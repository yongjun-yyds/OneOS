/**
  **********************************************************************************
             Copyright(c) 2021 Levetop Semiconductor Co. Ltd.
                      All Rights Reserved
  **********************************************************************************
  * @file    eflash_drv.c
  * @author  Product application department
  * @version V1.0
  * @date    2021.11.01
  * @brief   eflash模块DRIVATE层驱动.
  * @note 这个文件为应用层提供了操作eflash的接口:
  *
  @verbatim
  ===================================================================================
                                  ##### 更新 #####
  ===================================================================================
  [..] 更新时间:
  [..] 更新人:
  [..] 更新内容:
        1. nothing.
  @endverbatim
  */

#include "eflash_drv.h"

/*** 全局变量定义 ***************************************************************/
/*** volatile **********/

/*** @ static **********/

/*** 常量定义 *******************************************************************/

/*** 函数定义 *******************************************************************/
extern FunctionalState DRV_CACHE_GetStatus(void);
extern void            DRV_CACHE_Invalidate(uint32_t Addr, uint32_t Size);
/**
 *@brief eflash:写使能.
 *
 *@param[in] pefm :eflash实例
 *@param[in] status
 *- ENABLE
 *- DISABLE
 *@return NONE
 */
void DRV_EFM_WriteCmd(EFLASH_TypeDef *pefm, FunctionalStateTypeDef status)
{
    if (status == ENABLE)
    {
        _eflash_en_write_access(pefm);
    }
    else
    {
        _eflash_dis_write_access(pefm);
    }
}

/**
 *@brief eflash:读使能.
 *
 *@param[in] pefm :eflash实例
 *@param[in] status
 *- ENABLE
 *- DISABLE
 *@return NONE
 */
void DRV_EFM_ReadCmd(EFLASH_TypeDef *pefm, FunctionalStateTypeDef status)
{
    if (status == ENABLE)
    {
        _eflash_en_read_access(pefm);
    }
    else
    {
        _eflash_dis_read_access(pefm);
    }
}

/**
 *@brief eflash:读写使能.
 *
 *@param[in] pefm :eflash实例
 *@param[in] status
 *- ENABLE
 *- DISABLE
 *@return NONE
 */
void DRV_EFM_MainCmd(EFLASH_TypeDef *pefm, FunctionalStateTypeDef status)
{
    if (status == ENABLE)
    {
        _eflash_en_main_access(pefm);
    }
    else
    {
        _eflash_dis_main_access(pefm);
    }
}

/**
 *@brief eflash:初始化
 *
 *@param[in] pefm :eflash实例
 *@param[in] fclk:EFLASH时钟，单位KHz
 *@return NONE
 */
void DRV_EFM_Init(uint32_t fclk)
{
    LIB_EFLASH_Init(fclk);
}

void DRV_EFLASH_SetWritePermission(void)
{
    LIB_EFLASH_SetWritePermission();
}
/**
 *@brief eflash:页擦
 *
 *@param[in] address :eflash擦除地址
 *@return
 *-0:成功
 *-1:失败
 */
uint8_t DRV_EFM_EreasePage(uint32_t address)
{
    return LIB_EFLASH_PageErase(address);
}

/**
 *@brief eflash:按字写
 *
 *@param[in] address :eflash写入地址
 *@param[in] data:被写入的字
 *@return
 *-0:成功
 *-1:失败
 */
uint8_t DRV_EFM_ProgramWord(uint32_t address, uint32_t data)
{
    return LIB_EFLASH_BulkProgram((address), 1, &data);
}

/**
 *@brief eflash:按块编程
 *
 *@param[in] address :eflash写入地址
 *@param[in] buffer:写入数据指针
 *@param[in] length:写入数据字长度
 *@return
 *-0:成功
 *-1:失败
 */
uint8_t DRV_EFM_ProgramBulk(uint32_t address, uint8_t *buffer, uint32_t length)
{
    return LIB_EFLASH_BulkProgram((address), length, (uint32_t *)buffer);
}

/**
 *@brief eflash:读取EFLASH
 *
 *@param[in] address :eflash地址
 *@param[in] length：读取数据的长度
 *@param[out] buffer:读取数据buffer
 *@return NONE
 */
void DRV_EFM_Read(uint32_t address, uint8_t *buffer, uint32_t length)
{
    uint32_t i;
    for (i = 0; i < length; i++)
    {
        buffer[i] = (*(volatile uint8_t *)(address + i));
    }
}

/************************ (C) COPYRIGHT LEVETOP *****END OF FILE**********************/
