/**********************************************************
 * @Copyright (c) 2021   Levetop Semiconductor Co., Ltd.
 * @File			clcd_drv.c
 * @Author		Jason Zhao
 * @Date			2021/12/4
 * @Time			23:56:44
 * @Version
 * @Brief
 **********************************************************/

#include "type.h"
#include "clcd_drv.h"

static FunctionalStateTypeDef enableIE = DISABLE;

#define PWMT_BADDR (0x4001f000)
#define PWMT_PUE (*(volatile unsigned int *)(PWMT_BADDR + 0x0064))

/**
 *@brief CLCD模块是否中断
 *
 * @param[in] clcd 初始化结构体
 * @param[in] status 功能状态结构体
 *@return NONE
 */
void DRV_CLCD_InterruptCmd(CLCD_TypeDef *clcd, uint8_t int_flg, FunctionalStateTypeDef status)
{
    if (status == ENABLE)
    {
        _clcd_enable_int(clcd, int_flg);
    }
    else
    {
        _clcd_disable_int(clcd, int_flg);
    }
}

/**
 *@brief 使能CLCD.
 *
 *@param[in] clcd 指向CLCD_TypeDef的指针;
 *@return NONE;
 */
void DRV_CLCD_En(CLCD_TypeDef *clcd)
{
    _clcd_en(clcd);
}
/**
 *@brief 禁止CLCD.
 *
 *@param[in] clcd 指向CLCD_TypeDef的指针;
 *@return NONE;
 */
void DRV_CLCD_Dis(CLCD_TypeDef *clcd)
{
    _clcd_dis(clcd);
}
/**
 *@brief 使能或禁止CLCD.
 *
 *@param[in] clcd 指向CLCD_TypeDef的指针;
 *@param[in] NewState 新的状态
 * -ENABLE;
 * -DISABLE;
 *@return 无;
 */
void DRV_CLCD_Cmd(CLCD_TypeDef *clcd, FunctionalStateTypeDef NewState)
{
    if (NewState == ENABLE)
    {
        _clcd_en(clcd);
        if (enableIE == ENABLE)
        {
            _clcd_enable_int(clcd, VCOMP);
        }
    }
    else
        _clcd_dis(clcd);
}

/**
 *@brief CLCD时钟分频设置.
 *
 *@param[in] clcd 指向CLCD_TypeDef的指针;
 *@param[in] div  CLCD模块时钟分频值;
 *@return  NONE
 */
void DRV_CLCD_SetClkDiv(CLCD_TypeDef *clcd, uint32_t div)
{
    if (div > 256)
    {
        div = 256;
    }
    div = (div - 1) & 0xff;

    CPM->PCDIVR2 = (CPM->PCDIVR2 & (~0x00ff0000)) | (div << 16);
    CPM->CDIVUPDR = 0x00000003;
    // clcd->LCD_TIMING2 &=~ (PCD_HI_MASK | BCD | PCD_LO_MASK);
    clcd->LCD_TIMING2 |= BCD;
}

/**
 *@brief CLCDcontrol寄存器设置.
 *
 *@param[in] clcd 指向CLCD_TypeDef的指针;
 *@param[in] config  CLCD模块control寄存器配置值;
 *@return  NONE
 */
void DRV_CLCD_SetControlRegister(CLCD_TypeDef *clcd, uint32_t config)
{
    clcd->LCD_CONTROL = 0x00000000;

    clcd->LCD_CONTROL = (config & LCD_FMT_MASK_ALL) |
                        (WATERMARK | LCDTFT);
}

/**
 *@brief 恢复CLCD为默认设置.
 *
 *@param[in] clcd 指向CLCD_TypeDef的指针;
 *@return  NONE
 */
void DRV_CLCD_Deinit(CLCD_TypeDef *clcd)
{
    clcd->LCD_CONTROL = 0x00000000;
    clcd->LCD_IMSC = 0x00000000;                  // disable all lcdc interrupt.
    clcd->LCD_ICR = (MBERR | VCOMP | LNBU | FUF); // clear all interrupt status.
    clcd->LCD_TIMING0 = 0x00000000;
    clcd->LCD_TIMING1 = 0x00000000;
    clcd->LCD_TIMING2 = 0x00000000;
    clcd->LCD_TIMING3 = 0x00000000;
    clcd->LCD_UPBASE = 0x00000000;
    clcd->LCD_LPBASE = 0x00000000;
}

/**
 *@brief CLCD的Palette设置.
 *
 *@param[in] clcd  指向CLCD_TypeDef的指针;
 *@param[in] paddr palette值指针;
 *@param[in] palette_num 要设置palette的数量;
 *@return  NONE
 */
void DRV_CLCD_PaletteConfig(CLCD_TypeDef *clcd, uint32_t *paddr, uint32_t palette_num)
{
    uint32_t i;

    for (i = 0; i < (palette_num / 2); i++)
    {
        clcd->LCD_PALETTE[i] = paddr[i];
    }
}

/**
 *@brief CLCD的gamma设置.
 *
 *@param[in] clcd  指向CLCD_TypeDef的指针;
 *@param[in] paddr gamma值指针;
 *@param[in] gamma_num 要设置gamma的数量;
 *@return  NONE
 */
void DRV_CLCD_GammaConfig(CLCD_TypeDef *clcd, uint32_t *paddr, uint32_t gamma_num)
{
    uint32_t i;

    for (i = 0; i < (gamma_num); i++)
    {
        clcd->LCD_GAMMA_CORR[i] = paddr[i];
    }
}

/**
 *@brief CLCD的Timing0设置.
 *
 *@param[in] clcd  指向CLCD_TypeDef的指针;
 *@param[in] hbp ;
 *@param[in] hfp ;
 *@param[in] hsw ;
 *@param[in] ppl ;
 *@return  NONE
 */
void DRV_CLCD_SetTiming0(CLCD_TypeDef *clcd, uint8_t hbp, uint8_t hfp, uint8_t hsw, uint8_t ppl)
{
    clcd->LCD_TIMING0 = (((hbp - 1) & 0xff) << HBP_OFF) |
                        (((hfp - 1) & 0xff) << HFP_OFF) |
                        (((hsw - 1) & 0xff) << HSW_OFF) |
                        (((ppl - 1) & 0x3f) << PPL_OFF);
}

/**
 *@brief CLCD的Timing1设置.
 *
 *@param[in] clcd  指向CLCD_TypeDef的指针;
 *@param[in] vbp ;
 *@param[in] vfp ;
 *@param[in] vsw ;
 *@param[in] lpp ;
 *@return  NONE
 */
void DRV_CLCD_SetTiming1(CLCD_TypeDef *clcd, uint8_t vbp, uint8_t vfp, uint8_t vsw, uint16_t lpp)
{
    clcd->LCD_TIMING1 = ((vbp & 0xff) << VBP_OFF) |
                        ((vfp & 0xff) << VFP_OFF) |
                        (((vsw - 1) & 0x3f) << VSW_OFF) |
                        (((lpp - 1) & 0x3ff) << LPP_OFF);
}

/**
 *@brief CLCD的Timing2设置.
 *
 *@param[in] clcd  指向CLCD_TypeDef的指针;
 *@param[in] cpl ;
 *@param[in] flg ;
 *@return  NONE
 */
void DRV_CLCD_SetTiming2(CLCD_TypeDef *clcd, uint16_t cpl, uint32_t flg)
{
    clcd->LCD_TIMING2 &= ~(CPL_MASK | IEO | IPC | IHS | IVS | ACB_MASK);
    clcd->LCD_TIMING2 |= (((cpl - 1) & 0x3ff) << CPL_OFF) |
                         (flg & (IEO | IPC | IHS | IVS));
}

/**
 *@brief CLCD的Timing3设置.
 *
 *@param[in] clcd  指向CLCD_TypeDef的指针;
 *@param[in] led ;
 *@param[in] lee ;
 *@return  NONE
 */
void DRV_CLCD_SetTiming3(CLCD_TypeDef *clcd, uint8_t led, boolean lee)
{
    clcd->LCD_TIMING3 = 0x00000000;
    if (lee == TRUE)
    {
        clcd->LCD_TIMING3 = (led << 0) | (1 << 16);
    }
    else
        clcd->LCD_TIMING3 = (led << 0);
}
extern void NVIC_Init(uint8_t NVIC_PreemptionPriority, uint8_t NVIC_SubPriority, uint8_t NVIC_Channel, uint8_t NVIC_Group);

void DRV_CLCD_SetMisc(CLCD_TypeDef *clcd, FunctionalStateTypeDef NewState)
{
    (*(volatile unsigned char *)(0x40019005)) |= 0x02;
    (*(volatile unsigned char *)(0x40019003)) |= 0x02;
    PWMT_PUE &= ~0x000f0000;

    enableIE = NewState;
    if (NewState == ENABLE)
    {
        _clcd_disable_int(clcd, VCOMP);
        NVIC_Init(3, 3, 57, 2);
    }
}

/************************ (C) COPYRIGHT C*Core *****END OF FILE**********************/
