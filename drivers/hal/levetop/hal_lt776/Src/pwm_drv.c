/**
  **********************************************************************************
             Copyright(c) 2020 Levetop Semiconductor Co., Ltd.
                      All Rights Reserved
  **********************************************************************************
  * @file    pwm_drv.c
  * @author  Product application department
  * @version V1.0
  * @date    2020.02.25
  * @brief   pwm模块driver层驱动.
  *
  @verbatim
  ===================================================================================
                                  ##### 更新 #####
  ===================================================================================
  [..] 更新时间:
  [..] 更新人:
  [..] 更新内容:
        1. nothing.
  @endverbatim
  */

#include "pwm_drv.h"

/*** 全局变量定义 ***************************************************************/

/*** volatile **********/

/*** @ static **********/

/*** 常量定义 *******************************************************************/

/*** 函数定义 *******************************************************************/

/**
 * @brief pwm通道使能
 *
 * @param[in] ppwm pwm实例
 * @param[in] chx  pwm通道号
 * @param[in] status 使能状态
 * -ENABLE
 * -DISBALE
 * @return
 * -0:成功
 * -1:失败
 */
uint8_t DRV_PWM_CHxCmd(PWM_TypeDef *ppwm, uint8_t chx, FunctionalStateTypeDef status)
{
    if (status == ENABLE)
    {
        switch (chx)
        {
        case 0:
            _pwm_en_ch0(ppwm);
            break;
        case 1:
            _pwm_en_ch1(ppwm);
            break;
        case 2:
            _pwm_en_ch2(ppwm);
            break;
        case 3:
            _pwm_en_ch3(ppwm);
            break;
        case 4:
            _pwm_en_ch4(ppwm);
            break;
        case 5:
            _pwm_en_ch5(ppwm);
            break;
        case 6:
            _pwm_en_ch6(ppwm);
            break;
        case 7:
            _pwm_en_ch7(ppwm);
            break;
        default:
            return 1;
        }
    }
    else
    {
        switch (chx)
        {
        case 0:
            _pwm_dis_ch0(ppwm);
            break;
        case 1:
            _pwm_dis_ch1(ppwm);
            break;
        case 2:
            _pwm_dis_ch2(ppwm);
            break;
        case 3:
            _pwm_dis_ch3(ppwm);
            break;
        case 4:
            _pwm_dis_ch4(ppwm);
            break;
        case 5:
            _pwm_dis_ch5(ppwm);
            break;
        case 6:
            _pwm_dis_ch6(ppwm);
            break;
        case 7:
            _pwm_dis_ch7(ppwm);
            break;
        default:
            return 1;
        }
    }
    return 0;
}

/**
 * @brief pwm通道时钟翻转使能
 *
 * @param[in] ppwm pwm实例
 * @param[in] chx  pwm通道号
 * @param[in] status 使能状态
 * -ENABLE
 * -DISBALE
 * @return
 * -0:成功
 * -1:失败
 */
uint8_t DRV_PWM_CHxTimerInverteCmd(PWM_TypeDef *ppwm, uint8_t chx, FunctionalStateTypeDef status)
{
    if (status == ENABLE)
    {
        switch (chx)
        {
        case 0:
            _pwm_en_ch0_timerinverte(ppwm);
            break;
        case 1:
            _pwm_en_ch1_timerinverte(ppwm);
            break;
        case 2:
            _pwm_en_ch2_timerinverte(ppwm);
            break;
        case 3:
            _pwm_en_ch3_timerinverte(ppwm);
            break;
        case 4:
            _pwm_en_ch4_timerinverte(ppwm);
            break;
        case 5:
            _pwm_en_ch5_timerinverte(ppwm);
            break;
        case 6:
            _pwm_en_ch6_timerinverte(ppwm);
            break;
        case 7:
            _pwm_en_ch7_timerinverte(ppwm);
            break;
        default:
            return 1;
        }
    }
    else
    {
        switch (chx)
        {
        case 0:
            _pwm_dis_ch0_timerinverte(ppwm);
            break;
        case 1:
            _pwm_dis_ch1_timerinverte(ppwm);
            break;
        case 2:
            _pwm_dis_ch2_timerinverte(ppwm);
            break;
        case 3:
            _pwm_dis_ch3_timerinverte(ppwm);
            break;
        case 4:
            _pwm_dis_ch4_timerinverte(ppwm);
            break;
        case 5:
            _pwm_dis_ch5_timerinverte(ppwm);
            break;
        case 6:
            _pwm_dis_ch6_timerinverte(ppwm);
            break;
        case 7:
            _pwm_dis_ch7_timerinverte(ppwm);
            break;
        default:
            return 1;
        }
    }
    return 0;
}

/**
 * @brief pwm通道死区使能
 *
 * @param[in] ppwm pwm实例
 * @param[in] chx  pwm通道号
 * @param[in] status 使能状态
 * -ENABLE
 * -DISBALE
 * @return
 * -0:成功
 * -1:失败
 */
uint8_t DRV_PWM_CHxDeadZoneCmd(PWM_TypeDef *ppwm, uint8_t chx, FunctionalStateTypeDef status)
{
    if (status == ENABLE)
    {
        switch (chx)
        {
        case 0:
        case 1:
            _pwm_en_deadzone0(ppwm);
            break;
        case 2:
        case 3:
            _pwm_en_deadzone1(ppwm);
            break;
        case 4:
        case 5:
            _pwm_en_deadzone2(ppwm);
            break;
        case 6:
        case 7:
            _pwm_en_deadzone3(ppwm);
            break;
        default:
            return 1;
        }
    }
    else
    {
        switch (chx)
        {
        case 0:
        case 1:
            _pwm_dis_deadzone0(ppwm);
            break;
        case 2:
        case 3:
            _pwm_dis_deadzone1(ppwm);
            break;
        case 4:
        case 5:
            _pwm_dis_deadzone2(ppwm);
            break;
        case 6:
        case 7:
            _pwm_dis_deadzone3(ppwm);
            break;
        default:
            return 1;
        }
    }
    return 0;
}

/**
 * @brief pwm通道设置死区翻转参数
 *
 * @param[in] ppwm pwm实例
 * @param[in] chx  pwm通道号
 * @param[in] value 死区翻转值
 * @return
 * -0:成功
 * -1:失败
 */
uint8_t DRV_PWM_ConfigCHxDeadZone(PWM_TypeDef *ppwm, uint8_t chx, uint8_t value)
{
    switch (chx)
    {
    case 0:
    case 1:
        _pwm_set_ch01_deadzoneinterva(ppwm, value);
        break;
    case 2:
    case 3:
        _pwm_set_ch23_deadzoneinterva(ppwm, value);
        break;
    case 4:
    case 5:
        _pwm_set_ch45_deadzoneinterva(ppwm, value);
        break;
    case 6:
    case 7:
        _pwm_set_ch67_deadzoneinterva(ppwm, value);
        break;
    default:
        return 1;
    }
    return 0;
}

/**
 * @brief pwm通道设置预分频值
 *
 * @param[in] ppwm pwm实例
 * @param[in] chx  pwm通道号
 * @param[in] value 预分频值
 * @return
 * -0:成功
 * -1:失败
 */
uint8_t DRV_PWM_ConfigCHxPerscaler(PWM_TypeDef *ppwm, uint8_t chx, uint8_t value)
{
    switch (chx)
    {
    case 0:
    case 1:
        _pwm_set_ch01_perscaler(ppwm, value);
        break;
    case 2:
    case 3:
        _pwm_set_ch23_perscaler(ppwm, value);
        break;
    case 4:
    case 5:
        _pwm_set_ch45_perscaler(ppwm, value);
        break;
    case 6:
    case 7:
        _pwm_set_ch67_perscaler(ppwm, value);
        break;
    default:
        return 1;
    }
    return 0;
}

/**
 * @brief pwm通道设置分频系数
 *
 * @param[in] ppwm pwm实例
 * @param[in] chx  pwm通道号
 * @param[in] div 分频系数
 * @return
 * -0:成功
 * -1:失败
 */
uint8_t DRV_PWM_ConfigCHxClkDiv(PWM_TypeDef *ppwm, uint8_t chx, PWM_CLKDIV div)
{
    switch (chx)
    {
    case 0:
        _pwm_set_ch0_clkdiv(ppwm, div);
        break;
    case 1:
        _pwm_set_ch1_clkdiv(ppwm, div);
        break;
    case 2:
        _pwm_set_ch2_clkdiv(ppwm, div);
        break;
    case 3:
        _pwm_set_ch3_clkdiv(ppwm, div);
        break;
    case 4:
        _pwm_set_ch4_clkdiv(ppwm, div);
        break;
    case 5:
        _pwm_set_ch5_clkdiv(ppwm, div);
        break;
    case 6:
        _pwm_set_ch6_clkdiv(ppwm, div);
        break;
    case 7:
        _pwm_set_ch7_clkdiv(ppwm, div);
        break;
    default:
        return 1;
    }
    _pwm_set_ch0_clkdiv(ppwm, div);
    return 0;
}

/**
 * @brief pwm通道模式配置
 *
 * @param[in] ppwm pwm实例
 * @param[in] chx  pwm通道号
 * @param[in] status 计数器auto load模式
 * -ENABLE
 * -DISABLE
 * @return
 * -0:成功
 * -1:失败
 */

uint8_t DRV_PWM_CHxAutoLoadMode(PWM_TypeDef *ppwm, uint8_t chx, FunctionalStateTypeDef status)
{
    if (status == ENABLE)
    {
        switch (chx)
        {
        case 0:
            _pwm_set_ch0_autoloadmode(ppwm);
            break;
        case 1:
            _pwm_set_ch1_autoloadmode(ppwm);
            break;
        case 2:
            _pwm_set_ch2_autoloadmode(ppwm);
            break;
        case 3:
            _pwm_set_ch3_autoloadmode(ppwm);
            break;
        case 4:
            _pwm_set_ch4_autoloadmode(ppwm);
            break;
        case 5:
            _pwm_set_ch5_autoloadmode(ppwm);
            break;
        case 6:
            _pwm_set_ch6_autoloadmode(ppwm);
            break;
        case 7:
            _pwm_set_ch7_autoloadmode(ppwm);
            break;
        default:
            return 1;
        }
    }
    else
    {
        switch (chx)
        {
        case 0:
            _pwm_set_ch0_oneshotmode(ppwm);
            break;
        case 1:
            _pwm_set_ch1_oneshotmode(ppwm);
            break;
        case 2:
            _pwm_set_ch2_oneshotmode(ppwm);
            break;
        case 3:
            _pwm_set_ch3_oneshotmode(ppwm);
            break;
        case 4:
            _pwm_set_ch4_oneshotmode(ppwm);
            break;
        case 5:
            _pwm_set_ch5_oneshotmode(ppwm);
            break;
        case 6:
            _pwm_set_ch6_oneshotmode(ppwm);
            break;
        case 7:
            _pwm_set_ch7_oneshotmode(ppwm);
            break;
        default:
            return 1;
        }
    }
    return 0;
}

/**
 * @brief 设置pwm周期
 *
 * @param[in] ppwm pwm实例
 * @param[in] chx  pwm通道号
 * @param[in] cnt 一个timer周期内产生的PWM波形个数
 * @return
 * -0:成功
 * -1:失败
 */

uint8_t DRV_PWM_ConfigCHxPeriod(PWM_TypeDef *ppwm, uint8_t chx, uint16_t cnt)
{
    switch (chx)
    {
    case 0:
        _pwm_set_ch0_cnt(ppwm, cnt);
        break;
    case 1:
        _pwm_set_ch1_cnt(ppwm, cnt);
        break;
    case 2:
        _pwm_set_ch2_cnt(ppwm, cnt);
        break;
    case 3:
        _pwm_set_ch3_cnt(ppwm, cnt);
        break;
    case 4:
        _pwm_set_ch4_cnt(ppwm, cnt);
        break;
    case 5:
        _pwm_set_ch5_cnt(ppwm, cnt);
        break;
    case 6:
        _pwm_set_ch6_cnt(ppwm, cnt);
        break;
    case 7:
        _pwm_set_ch7_cnt(ppwm, cnt);
        break;
    default:
        return 1;
    }
    return 0;
}

/**
 * @brief 获取pwm周期
 *
 * @param[in] ppwm pwm实例
 * @param[in] chx  pwm通道号
 * @retval 周期值
 */

uint16_t DRV_PWM_GetCHxPeriod(PWM_TypeDef *ppwm, uint8_t chx)
{
    switch (chx)
    {
    case 0:
        return _pwm_get_pcnr0(ppwm);
    case 1:
        return _pwm_get_pcnr1(ppwm);
    case 2:
        return _pwm_get_pcnr2(ppwm);
    case 3:
        return _pwm_get_pcnr3(ppwm);
    case 4:
        return _pwm_get_pcnr4(ppwm);
    case 5:
        return _pwm_get_pcnr5(ppwm);
    case 6:
        return _pwm_get_pcnr6(ppwm);
    case 7:
        return _pwm_get_pcnr7(ppwm);
    default:
        return 0;
    }
}

/**
 * @brief 设置pwm脉冲宽度
 *
 * @param[in] ppwm pwm实例
 * @param[in] chx  pwm通道号
 * @param[in] cnt 计数器值
 * @return
 * -0:成功
 * -1:失败

 */
uint8_t DRV_PWM_ConfigCHxWidth(PWM_TypeDef *ppwm, uint8_t chx, uint16_t cnt)
{
    switch (chx)
    {
    case 0:
        _pwm_set_ch0_Comparator(ppwm, cnt);
        break;
    case 1:
        _pwm_set_ch1_Comparator(ppwm, cnt);
        break;
    case 2:
        _pwm_set_ch2_Comparator(ppwm, cnt);
        break;
    case 3:
        _pwm_set_ch3_Comparator(ppwm, cnt);
        break;
    case 4:
        _pwm_set_ch4_Comparator(ppwm, cnt);
        break;
    case 5:
        _pwm_set_ch5_Comparator(ppwm, cnt);
        break;
    case 6:
        _pwm_set_ch6_Comparator(ppwm, cnt);
        break;
    case 7:
        _pwm_set_ch7_Comparator(ppwm, cnt);
        break;
    default:
        return 1;
    }
    return 0;
}

/**
 * @brief 获取pwm脉冲宽度
 *
 * @param[in] ppwm pwm实例
 * @param[in] chx  pwm通道号
 * @retval 脉冲宽度
 */

uint16_t DRV_PWM_GetCHxWidth(PWM_TypeDef *ppwm, uint8_t chx)
{
    switch (chx)
    {
    case 0:
        return _pwm_get_Comparator0(ppwm);
    case 1:
        return _pwm_get_Comparator1(ppwm);
    case 2:
        return _pwm_get_Comparator2(ppwm);
    case 3:
        return _pwm_get_Comparator3(ppwm);
    case 4:
        return _pwm_get_Comparator4(ppwm);
    case 5:
        return _pwm_get_Comparator5(ppwm);
    case 6:
        return _pwm_get_Comparator6(ppwm);
    case 7:
        return _pwm_get_Comparator7(ppwm);
    default:
        return 0;
    }
}

/**
 * @brief 获取pwm通道时钟计数器值
 *
 * @param[in] ppwm pwm实例
 * @param[in] chx  pwm通道号
 * @retval 通道时钟计数器值
 */

uint16_t DRV_PWM_GetCHxTimerCounter(PWM_TypeDef *ppwm, uint8_t chx)
{
    switch (chx)
    {
    case 0:
        return _pwm_get_ch0_timercnt(ppwm);
    case 1:
        return _pwm_get_ch1_timercnt(ppwm);
    case 2:
        return _pwm_get_ch2_timercnt(ppwm);
    case 3:
        return _pwm_get_ch3_timercnt(ppwm);
    case 4:
        return _pwm_get_ch4_timercnt(ppwm);
    case 5:
        return _pwm_get_ch5_timercnt(ppwm);
    case 6:
        return _pwm_get_ch6_timercnt(ppwm);
    case 7:
        return _pwm_get_ch7_timercnt(ppwm);
    default:
        return 0;
    }
}

/**
 * @brief pwm通道中断使能
 *
 * @param[in] ppwm pwm实例
 * @param[in] chx  pwm通道号
 * @param[in] status 使能状态
 * -ENABLE
 * -DISBALE
 * @return
 * -0:成功
 * -1:失败

 */

uint8_t DRV_PWM_CHxTimerInterruptCmd(PWM_TypeDef *ppwm, uint8_t chx, FunctionalStateTypeDef status)
{
    if (status == ENABLE)
    {
        switch (chx)
        {
        case 0:
            _pwm_en_ch0_timerint(ppwm);
            break;
        case 1:
            _pwm_en_ch1_timerint(ppwm);
            break;
        case 2:
            _pwm_en_ch2_timerint(ppwm);
            break;
        case 3:
            _pwm_en_ch3_timerint(ppwm);
            break;
        case 4:
            _pwm_en_ch4_timerint(ppwm);
            break;
        case 5:
            _pwm_en_ch5_timerint(ppwm);
            break;
        case 6:
            _pwm_en_ch6_timerint(ppwm);
            break;
        case 7:
            _pwm_en_ch7_timerint(ppwm);
            break;
        default:
            return 1;
        }
    }
    else
    {
        switch (chx)
        {
        case 0:
            _pwm_dis_ch0_timerint(ppwm);
            break;
        case 1:
            _pwm_dis_ch1_timerint(ppwm);
            break;
        case 2:
            _pwm_dis_ch2_timerint(ppwm);
            break;
        case 3:
            _pwm_dis_ch3_timerint(ppwm);
            break;
        case 4:
            _pwm_dis_ch4_timerint(ppwm);
            break;
        case 5:
            _pwm_dis_ch5_timerint(ppwm);
            break;
        case 6:
            _pwm_dis_ch6_timerint(ppwm);
            break;
        case 7:
            _pwm_dis_ch7_timerint(ppwm);
            break;
        default:
            return 1;
        }
    }
    return 0;
}

/**
 * @brief 获取pwm通道中断状态
 *
 * @param[in] ppwm pwm实例
 * @param[in] chx  pwm通道号
 * @return
 *-0:未产生中断
 *-1:产生中断
 */

uint8_t DRV_PWM_GetCHxTimerInterrupt(PWM_TypeDef *ppwm, uint8_t chx)
{
    switch (chx)
    {
    case 0:
        if (_pwm_get_ch0_timer_intflag(ppwm))
            return 1;
        break;
    case 1:
        if (_pwm_get_ch1_timer_intflag(ppwm))
            return 1;
        break;
    case 2:
        if (_pwm_get_ch2_timer_intflag(ppwm))
            return 1;
        break;
    case 3:
        if (_pwm_get_ch3_timer_intflag(ppwm))
            return 1;
        break;
    case 4:
        if (_pwm_get_ch4_timer_intflag(ppwm))
            return 1;
        break;
    case 5:
        if (_pwm_get_ch5_timer_intflag(ppwm))
            return 1;
        break;
    case 6:
        if (_pwm_get_ch6_timer_intflag(ppwm))
            return 1;
        break;
    case 7:
        if (_pwm_get_ch7_timer_intflag(ppwm))
            return 1;
        break;
    default:
        break;
    }
    return 0;
}

/**
 * @brief pwm通道翻转使能
 *
 * @param[in] ppwm pwm实例
 * @param[in] chx  pwm通道号
 * @param[in] status 使能状态
 * -ENABLE
 * -DISBALE
 * @return
 * -0:成功
 * -1:失败

 */

uint8_t DRV_PWM_CHxInverteCmd(PWM_TypeDef *ppwm, uint8_t chx, FunctionalStateTypeDef status)
{
    if (status == ENABLE)
    {
        switch (chx)
        {
        case 0:
            _pwm_en_ch0_inv(ppwm);
            break;
        case 1:
            _pwm_en_ch1_inv(ppwm);
            break;
        case 2:
            _pwm_en_ch2_inv(ppwm);
            break;
        case 3:
            _pwm_en_ch3_inv(ppwm);
            break;
        case 4:
            _pwm_en_ch4_inv(ppwm);
            break;
        case 5:
            _pwm_en_ch5_inv(ppwm);
            break;
        case 6:
            _pwm_en_ch6_inv(ppwm);
            break;
        case 7:
            _pwm_en_ch7_inv(ppwm);
            break;
        default:
            return 1;
        }
    }
    else
    {
        switch (chx)
        {
        case 0:
            _pwm_dis_ch0_inv(ppwm);
            break;
        case 1:
            _pwm_dis_ch1_inv(ppwm);
            break;
        case 2:
            _pwm_dis_ch2_inv(ppwm);
            break;
        case 3:
            _pwm_dis_ch3_inv(ppwm);
            break;
        case 4:
            _pwm_dis_ch4_inv(ppwm);
            break;
        case 5:
            _pwm_dis_ch5_inv(ppwm);
            break;
        case 6:
            _pwm_dis_ch6_inv(ppwm);
            break;
        case 7:
            _pwm_dis_ch7_inv(ppwm);
            break;
        default:
            return 1;
        }
    }
    return 0;
}

/**
 * @brief pwm通道上升沿中断使能
 *
 * @param[in] ppwm pwm实例
 * @param[in] chx  pwm通道号
 * @param[in] status 使能状态
 * -ENABLE
 * -DISBALE
 * @return
 * -0:成功
 * -1:失败

 */

uint8_t DRV_PWM_CHxRisingInterruptCmd(PWM_TypeDef *ppwm, uint8_t chx, FunctionalStateTypeDef status)
{
    if (status == ENABLE)
    {
        switch (chx)
        {
        case 0:
            _pwm_en_ch0_rlint(ppwm);
            break;
        case 1:
            _pwm_en_ch1_rlint(ppwm);
            break;
        case 2:
            _pwm_en_ch2_rlint(ppwm);
            break;
        case 3:
            _pwm_en_ch3_rlint(ppwm);
            break;
        case 4:
            _pwm_en_ch4_rlint(ppwm);
            break;
        case 5:
            _pwm_en_ch5_rlint(ppwm);
            break;
        case 6:
            _pwm_en_ch6_rlint(ppwm);
            break;
        case 7:
            _pwm_en_ch7_rlint(ppwm);
            break;
        default:
            return 1;
        }
    }
    else
    {
        switch (chx)
        {
        case 0:
            _pwm_dis_ch0_rlint(ppwm);
            break;
        case 1:
            _pwm_dis_ch1_rlint(ppwm);
            break;
        case 2:
            _pwm_dis_ch2_rlint(ppwm);
            break;
        case 3:
            _pwm_dis_ch3_rlint(ppwm);
            break;
        case 4:
            _pwm_dis_ch4_rlint(ppwm);
            break;
        case 5:
            _pwm_dis_ch5_rlint(ppwm);
            break;
        case 6:
            _pwm_dis_ch6_rlint(ppwm);
            break;
        case 7:
            _pwm_dis_ch7_rlint(ppwm);
            break;
        default:
            return 1;
        }
    }
    return 0;
}

/**
 * @brief pwm通道下降沿中断使能
 *
 * @param[in] ppwm pwm实例
 * @param[in] chx  pwm通道号
 * @param[in] status 使能状态
 * -ENABLE
 * -DISBALE
 * @return
 * -0:成功
 * -1:失败

 */

uint8_t DRV_PWM_CHxFallinginterruptCmd(PWM_TypeDef *ppwm, uint8_t chx, FunctionalStateTypeDef status)
{
    if (status == ENABLE)
    {
        switch (chx)
        {
        case 0:
            _pwm_en_ch0_flint(ppwm);
            break;
        case 1:
            _pwm_en_ch1_flint(ppwm);
            break;
        case 2:
            _pwm_en_ch2_flint(ppwm);
            break;
        case 3:
            _pwm_en_ch3_flint(ppwm);
            break;
        case 4:
            _pwm_en_ch4_flint(ppwm);
            break;
        case 5:
            _pwm_en_ch5_flint(ppwm);
            break;
        case 6:
            _pwm_en_ch6_flint(ppwm);
            break;
        case 7:
            _pwm_en_ch7_flint(ppwm);
            break;
        default:
            return 1;
        }
    }
    else
    {
        switch (chx)
        {
        case 0:
            _pwm_dis_ch0_flint(ppwm);
            break;
        case 1:
            _pwm_dis_ch1_flint(ppwm);
            break;
        case 2:
            _pwm_dis_ch2_flint(ppwm);
            break;
        case 3:
            _pwm_dis_ch3_flint(ppwm);
            break;
        case 4:
            _pwm_dis_ch4_flint(ppwm);
            break;
        case 5:
            _pwm_dis_ch5_flint(ppwm);
            break;
        case 6:
            _pwm_dis_ch6_flint(ppwm);
            break;
        case 7:
            _pwm_dis_ch7_flint(ppwm);
            break;
        default:
            return 1;
        }
    }
    return 0;
}

/**
 * @brief pwm通道捕获中断使能
 *
 * @param[in] ppwm pwm实例
 * @param[in] chx  pwm通道号
 * @param[in] status 使能状态
 * -ENABLE
 * -DISBALE
 * @return
 * -0:成功
 * -1:失败

 */

uint8_t DRV_PWM_CHxCaptureCmd(PWM_TypeDef *ppwm, uint8_t chx, FunctionalStateTypeDef status)
{
    if (status == ENABLE)
    {
        switch (chx)
        {
        case 0:
            _pwm_en_ch0_capture(ppwm);
            break;
        case 1:
            _pwm_en_ch1_capture(ppwm);
            break;
        case 2:
            _pwm_en_ch2_capture(ppwm);
            break;
        case 3:
            _pwm_en_ch3_capture(ppwm);
            break;
        case 4:
            _pwm_en_ch4_capture(ppwm);
            break;
        case 5:
            _pwm_en_ch5_capture(ppwm);
            break;
        case 6:
            _pwm_en_ch6_capture(ppwm);
            break;
        case 7:
            _pwm_en_ch7_capture(ppwm);
            break;
        default:
            return 1;
        }
    }
    else
    {
        switch (chx)
        {
        case 0:
            _pwm_dis_ch0_capture(ppwm);
            break;
        case 1:
            _pwm_dis_ch1_capture(ppwm);
            break;
        case 2:
            _pwm_dis_ch2_capture(ppwm);
            break;
        case 3:
            _pwm_dis_ch3_capture(ppwm);
            break;
        case 4:
            _pwm_dis_ch4_capture(ppwm);
            break;
        case 5:
            _pwm_dis_ch5_capture(ppwm);
            break;
        case 6:
            _pwm_dis_ch6_capture(ppwm);
            break;
        case 7:
            _pwm_dis_ch7_capture(ppwm);
            break;
        default:
            return 1;
        }
    }
    return 0;
}

/**
 * @brief pwm通道设置边沿触发
 *
 * @param[in] ppwm pwm实例
 * @param[in] chx  pwm通道号
 * @param[in] value 边沿触发标志
 * -PWM_PCCRX_CHX_RLINT
 * -PWM_PCCRX_CHX_FLINT
 * -PWM_PCCRX_CHX_CAPEN
 * @return
 * -0:成功
 * -1:失败

 */
uint8_t DRV_PWM_ConfigCHxEdgeinterrupt(PWM_TypeDef *ppwm, uint8_t chx, uint32_t edge)
{
    switch (chx)
    {
    case 0:
        _pwm_set_ch0_edgeinterrupt(ppwm, edge);
        break;
    case 1:
        _pwm_set_ch1_edgeinterrupt(ppwm, edge);
        break;
    case 2:
        _pwm_set_ch2_edgeinterrupt(ppwm, edge);
        break;
    case 3:
        _pwm_set_ch3_edgeinterrupt(ppwm, edge);
        break;
    case 4:
        _pwm_set_ch4_edgeinterrupt(ppwm, edge);
        break;
    case 5:
        _pwm_set_ch5_edgeinterrupt(ppwm, edge);
        break;
    case 6:
        _pwm_set_ch6_edgeinterrupt(ppwm, edge);
        break;
    case 7:
        _pwm_set_ch7_edgeinterrupt(ppwm, edge);
        break;
    default:
        return 1;
    }
    return 0;
}

/**
 * @brief pwm通道设置方向属性
 *
 * @param[in] ppwm pwm实例
 * @param[in] chx  pwm通道号
 * @param[in] dir  方向
 * -0:输入
 * -1:输出
 * @return
 * -0:成功
 * -1:失败

 */

uint8_t DRV_PWM_SetCHxDirection(PWM_TypeDef *ppwm, uint8_t chx, uint8_t dir)
{
    if (dir == 0)
    {
        switch (chx)
        {
        case 0:
            _pwm_set_ch0_input(ppwm);
            break;
        case 1:
            _pwm_set_ch1_input(ppwm);
            break;
        case 2:
            _pwm_set_ch2_input(ppwm);
            break;
        case 3:
            _pwm_set_ch3_input(ppwm);
            break;
        case 4:
            _pwm_set_ch4_input(ppwm);
            break;
        case 5:
            _pwm_set_ch5_input(ppwm);
            break;
        case 6:
            _pwm_set_ch6_input(ppwm);
            break;
        case 7:
            _pwm_set_ch7_input(ppwm);
            break;
        default:
            return 1;
        }
    }
    else
    {
        switch (chx)
        {
        case 0:
            _pwm_set_ch0_output(ppwm);
            break;
        case 1:
            _pwm_set_ch1_output(ppwm);
            break;
        case 2:
            _pwm_set_ch2_output(ppwm);
            break;
        case 3:
            _pwm_set_ch3_output(ppwm);
            break;
        case 4:
            _pwm_set_ch4_output(ppwm);
            break;
        case 5:
            _pwm_set_ch5_output(ppwm);
            break;
        case 6:
            _pwm_set_ch6_output(ppwm);
            break;
        case 7:
            _pwm_set_ch7_output(ppwm);
            break;
        default:
            return 1;
        }
    }
    return 0;
}

/**
 * @brief pwm通道拉高使能
 *
 * @param[in] ppwm pwm实例
 * @param[in] chx  pwm通道号
 * @param[in] status 使能状态
 * -ENABLE
 * -DISBALE
 * @return
 * -0:成功
 * -1:失败
 */

uint8_t DRV_PWM_CHxPullupCmd(PWM_TypeDef *ppwm, uint8_t chx, FunctionalStateTypeDef status)
{
    if (status == ENABLE)
    {
        switch (chx)
        {
        case 0:
            _pwm_en_ch0_pullup(ppwm);
            break;
        case 1:
            _pwm_en_ch1_pullup(ppwm);
            break;
        case 2:
            _pwm_en_ch2_pullup(ppwm);
            break;
        case 3:
            _pwm_en_ch3_pullup(ppwm);
            break;
        case 4:
            _pwm_en_ch4_pullup(ppwm);
            break;
        case 5:
            _pwm_en_ch5_pullup(ppwm);
            break;
        case 6:
            _pwm_en_ch6_pullup(ppwm);
            break;
        case 7:
            _pwm_en_ch7_pullup(ppwm);
            break;
        default:
            return 1;
        }
    }
    else
    {
        switch (chx)
        {
        case 0:
            _pwm_dis_ch0_pullup(ppwm);
            break;
        case 1:
            _pwm_dis_ch1_pullup(ppwm);
            break;
        case 2:
            _pwm_dis_ch2_pullup(ppwm);
            break;
        case 3:
            _pwm_dis_ch3_pullup(ppwm);
            break;
        case 4:
            _pwm_dis_ch4_pullup(ppwm);
            break;
        case 5:
            _pwm_dis_ch5_pullup(ppwm);
            break;
        case 6:
            _pwm_dis_ch6_pullup(ppwm);
            break;
        case 7:
            _pwm_dis_ch7_pullup(ppwm);
            break;
        default:
            return 1;
        }
    }
    return 0;
}

/**
 * @brief pwm通道设置电平级别
 *
 * @param[in] ppwm pwm实例
 * @param[in] chx  pwm通道号
 * @param[in] level:电平状态
 * -0:reset
 * -1:set
 * @return
 * -0:成功
 * -1:失败
 */
uint8_t DRV_PWM_SetCHxLevel(PWM_TypeDef *ppwm, uint8_t chx, uint8_t level)
{
    if (level == 0)
    {
        switch (chx)
        {
        case 0:
            _pwm_clr_ch0_bit(ppwm);
            break;
        case 1:
            _pwm_clr_ch1_bit(ppwm);
            break;
        case 2:
            _pwm_clr_ch2_bit(ppwm);
            break;
        case 3:
            _pwm_clr_ch3_bit(ppwm);
            break;
        case 4:
            _pwm_clr_ch4_bit(ppwm);
            break;
        case 5:
            _pwm_clr_ch5_bit(ppwm);
            break;
        case 6:
            _pwm_clr_ch6_bit(ppwm);
            break;
        case 7:
            _pwm_clr_ch7_bit(ppwm);
            break;
        default:
            return 1;
        }
    }
    else
    {
        switch (chx)
        {
        case 0:
            _pwm_set_ch0_bit(ppwm);
            break;
        case 1:
            _pwm_set_ch1_bit(ppwm);
            break;
        case 2:
            _pwm_set_ch2_bit(ppwm);
            break;
        case 3:
            _pwm_set_ch3_bit(ppwm);
            break;
        case 4:
            _pwm_set_ch4_bit(ppwm);
            break;
        case 5:
            _pwm_set_ch5_bit(ppwm);
            break;
        case 6:
            _pwm_set_ch6_bit(ppwm);
            break;
        case 7:
            _pwm_set_ch7_bit(ppwm);
            break;
        default:
            return 1;
        }
    }
    return 0;
}

/**
 * @brief pwm通道设置电平级别
 *
 * @param[in] ppwm pwm实例
 * @param[in] chx  pwm通道号
 * @return
 * -0:低电平
 * -1:高电平
 */
uint8_t DRV_PWM_GetCHxLevel(PWM_TypeDef *ppwm, uint8_t chx)
{
    switch (chx)
    {
    case 0:
        if (_pwm_chk_ch0_bit(ppwm))
            return 1;
        break;
    case 1:
        if (_pwm_chk_ch1_bit(ppwm))
            return 1;
        break;
    case 2:
        if (_pwm_chk_ch2_bit(ppwm))
            return 1;
        break;
    case 3:
        if (_pwm_chk_ch3_bit(ppwm))
            return 1;
        break;
    case 4:
        if (_pwm_chk_ch4_bit(ppwm))
            return 1;
        break;
    case 5:
        if (_pwm_chk_ch5_bit(ppwm))
            return 1;
        break;
    case 6:
        if (_pwm_chk_ch6_bit(ppwm))
            return 1;
        break;
    case 7:
        if (_pwm_chk_ch7_bit(ppwm))
            return 1;
        break;
    default:
        break;
    }
    return 0;
}
