/**********************************************************
 * @Copyright (c) 2021   Levetop Semiconductor Co., Ltd.
 * @File			dcmi_drv.c
 * @Author		Howard
 * @Date			2021/12/30
 * @Time			10:04:32
 * @Version
 * @Brief
 **********************************************************
 * Modification History
 *
 *
 **********************************************************/
#include "dcmi_drv.h"
#include "dma_drv.h"
#include "sys.h"
//#include "delay.h"
/*** 全局变量定义 ***************************************************************/
/**@ volatile
 *@{
 */

/**@
 *@}
 */
/**@ static
 *@{
 */

/**@
 *@}
 */
/*** 常量定义 *******************************************************************/
/**@ const
 *@{
 */

/**@
 *@}
 */
//#define DMA_MAX_LLI_CNT					(38)
//#define DMA_MAX_SIZE_PER_XFER		(20480)
#define DMA_MAX_LLI_CNT (64)
#define DMA_MAX_SIZE_PER_XFER (4088)
#define HS_SEL_SRC_HARD (0x0 << 11)
#define DMA_DCMI_CHN (0)

typedef struct _DMA_CONTROL_T40_REG
{
    volatile unsigned int DMA_RAWTFR;
    volatile unsigned int DMA_RAWTFR_H;
    volatile unsigned int DMA_RAWBLOCK;
    volatile unsigned int DMA_RAWBLOCK_H;
    volatile unsigned int DMA_RAWSRCTRAN;
    volatile unsigned int DMA_RAWSRCTRAN_H;
    volatile unsigned int DMA_RAWDSTTRAN;
    volatile unsigned int DMA_RAWDSTTRAN_H;
    volatile unsigned int DMA_RAWERR;
    volatile unsigned int DMA_RAWERR_H;
    volatile unsigned int DMA_STATTFR;
    volatile unsigned int DMA_STATTFR_H;
    volatile unsigned int DMA_STATBLOCK;
    volatile unsigned int DMA_STATBLOCK_H;
    volatile unsigned int DMA_STATSRC;
    volatile unsigned int DMA_STATSRC_H;
    volatile unsigned int DMA_STATDST;
    volatile unsigned int DMA_STATDST_H;
    volatile unsigned int DMA_STATERR;
    volatile unsigned int DMA_STATERR_H;
    volatile unsigned int DMA_MASKTFR;
    volatile unsigned int DMA_MASKTFR_H;
    volatile unsigned int DMA_MASKBLOCK;
    volatile unsigned int DMA_MASKBLOCK_H;
    volatile unsigned int DMA_MASKSRC;
    volatile unsigned int DMA_MASKSRC_H;
    volatile unsigned int DMA_MASKDST;
    volatile unsigned int DMA_MASKDST_H;
    volatile unsigned int DMA_MASKERR;
    volatile unsigned int DMA_MASKERR_H;
    volatile unsigned int DMA_CLRTFR;
    volatile unsigned int DMA_CLRTFR_H;
    volatile unsigned int DMA_CLRBLOCK;
    volatile unsigned int DMA_CLRBLOCK_H;
    volatile unsigned int DMA_CLRSRC;
    volatile unsigned int DMA_CLRSRC_H;
    volatile unsigned int DMA_CLRDST;
    volatile unsigned int DMA_CLRDST_H;
    volatile unsigned int DMA_CLRERR;
    volatile unsigned int DMA_CLRERR_H;
    volatile unsigned int DMA_STATUSINT;
    volatile unsigned int DMA_STATUSINT_H;
    volatile unsigned int DMA_SRCREQ;
    volatile unsigned int DMA_SRCREQ_H;
    volatile unsigned int DMA_DSTREQ;
    volatile unsigned int DMA_DSTREQ_H;
    volatile unsigned int DMA_SINGLESRC;
    volatile unsigned int DMA_SINGLESRC_H;
    volatile unsigned int DMA_SINGLEDST;
    volatile unsigned int DMA_SINGLEDST_H;
    volatile unsigned int DMA_LASTSRC;
    volatile unsigned int DMA_LASTSRC_H;
    volatile unsigned int DMA_LASTDST;
    volatile unsigned int DMA_LASTDST_H;
    volatile unsigned int DMA_CONFIG;
    volatile unsigned int DMA_CONFIG_H;
    volatile unsigned int DMA_CHEN;
    volatile unsigned int DMA_CHEN_H;

} DMA_CONTROL_REG_T40;

///****************DMA CONTROL Register define *************************/
// typedef struct _DMA_CHANNEL_REG
//{
//	volatile unsigned int DMACC0SADDR;
//	volatile unsigned int DMACC0SADDR_H;
//	volatile unsigned int DMACC0DADDR;
//	volatile unsigned int DMACC0DADDR_H;
//	volatile unsigned int DMACC0LLI;
//	volatile unsigned int DMACC0LLI_H;
//	volatile unsigned int DMACC0CTRL;
//	volatile unsigned int DMACC0CTRL_H;
//	volatile unsigned int reserved4[8];
//	volatile unsigned int DMACC0CONFIG;
//	volatile unsigned int DMACC0CONFIG_H;
//
// }DMA_CHANNEL_REG;

static DMAC_LLITypeDef g_dma_lli[DMA_MAX_LLI_CNT];

//static DMA_CHANNEL_REG *m_dma_channel[4]; // global struct variable for for Channel registers
// static DMAC_TypeDef         *m_dma_control;//global struct variable for for DMAC registers
static DMA_CONTROL_REG_T40 *m_dma_control; // global struct variable for for DMAC registers

static DCMI_HandleTypeDef *g_hdcmi;

/*
 * dma_register_init
 * input：
 * output:
 * description：init 8 channel base address for Channel registers
 */
void dma_register_init(unsigned int dmac_base_addr)
{
    // int n =0;
    m_dma_control = (DMA_CONTROL_REG_T40 *)(dmac_base_addr + 0x2c0);
    // m_dma_control    = (DMAC_TypeDef*)(dmac_base_addr);
    m_dma_channel[0] = (DMA_CHANNEL_REG *)(dmac_base_addr);
    m_dma_channel[1] = (DMA_CHANNEL_REG *)(dmac_base_addr + 0x58);
    m_dma_channel[2] = (DMA_CHANNEL_REG *)(dmac_base_addr + 0xB0);
    m_dma_channel[3] = (DMA_CHANNEL_REG *)(dmac_base_addr + 0x108);

    m_dma_control->DMA_MASKBLOCK = 0xf0f;
    // interrupt_setup(0x24, isr_dma);
    m_dma_control->DMA_CONFIG = 0x01;
}

void dma_dcmi_xfer_mcu_lcd(u32 sourAddr, u32 destAddr)
{
    g_dma_lli[0].src_addr = sourAddr;
    g_dma_lli[0].dst_addr = destAddr;

    g_dma_lli[0].next_lli = (u32)&g_dma_lli[0];
    g_dma_lli[0].len = DMA_MAX_SIZE_PER_XFER;
    // g_dma_lli[0].control0 = DBSIZE_8|SBSIZE_8|DNOCHG|P2M_DMA|SNOCHG|DWIDTH_W|SWIDTH_W|LLP_DST_EN|LLP_SRC_EN;
    g_dma_lli[0].control = DBSIZE_8 | SBSIZE_8 | P2M_DMA | SNOCHG | DWIDTH_W | SWIDTH_W | LLP_DST_EN | LLP_SRC_EN;

    if (!(destAddr & 0x20000000))
        g_dma_lli[0].control |= DNOCHG;
    else
        g_dma_lli[0].control |= DI;

    m_dma_channel[0]->DMA_SADDR = g_dma_lli[0].src_addr;
    m_dma_channel[0]->DMA_DADDR = g_dma_lli[0].dst_addr;
    m_dma_channel[0]->DMA_CTRL = g_dma_lli[0].control;
    m_dma_channel[0]->DMA_CTRL_HIGH = g_dma_lli[0].len;
    m_dma_channel[0]->DMA_LLP = (u32)&g_dma_lli[0];

    m_dma_channel[0]->DMA_CFG = HS_SEL_SRC_HARD;

    if ((sourAddr & 0xFFFFF000) == 0x40053000)
        m_dma_channel[0]->DMA_CFG_HIGH = 0x9 << 7;
    else
        m_dma_channel[0]->DMA_CFG_HIGH = 13 << 7;

    m_dma_control->DMA_MASKTFR = (0x01 << 0) | ((0x100) << 0); // interrupt
    m_dma_control->DMA_CHEN = (0x01 << 0) | ((0x100) << 0);
}

void dma_dcmi_xfer(u8 n, u32 sourAddr, u32 destAddr, unsigned int len)
{
    u32 mod;
    u32 loop;
    u32 i;
    u32 word_len;

    word_len = len / 4;

    loop = word_len / DMA_MAX_SIZE_PER_XFER;
    mod = word_len % DMA_MAX_SIZE_PER_XFER;

    if (mod != 0)
        loop++;

    for (i = 0; i < loop; i++)
    {
        g_dma_lli[i].src_addr = sourAddr;
        g_dma_lli[i].dst_addr = destAddr + DMA_MAX_SIZE_PER_XFER * i * 4;

        if (i == (loop - 1))
        {
            g_dma_lli[i].next_lli = 0;
            g_dma_lli[i].len = mod;
            // g_dma_lli[i].control0 = DI|P2M_DMA|SNOCHG|DWIDTH_W|SWIDTH_W;
            g_dma_lli[i].control = DBSIZE_8 | SBSIZE_8 | DI | P2M_DMA | SNOCHG | DWIDTH_W | SWIDTH_W;
        }
        else
        {
            g_dma_lli[i].next_lli = (u32)&g_dma_lli[i + 1];
            g_dma_lli[i].len = DMA_MAX_SIZE_PER_XFER;
            g_dma_lli[i].control = DBSIZE_8 | SBSIZE_8 | DI | P2M_DMA | SNOCHG | DWIDTH_W | SWIDTH_W | LLP_DST_EN | LLP_SRC_EN;
            // g_dma_lli[i].control0 = DI|P2M_DMA|SNOCHG|DWIDTH_W|SWIDTH_W|LLP_DST_EN|LLP_SRC_EN;
        }
    }

    m_dma_channel[n]->DMA_SADDR = g_dma_lli[0].src_addr;
    m_dma_channel[n]->DMA_DADDR = g_dma_lli[0].dst_addr;
    m_dma_channel[n]->DMA_CTRL = g_dma_lli[0].control;
    // BY PYIN
    //	m_dma_channel[n]->DMACC0CTRL |= (0x2 << 11) | (0x2 << 14);

    m_dma_channel[n]->DMA_CTRL_HIGH = g_dma_lli[0].len;
    m_dma_channel[n]->DMA_LLP = (u32)&g_dma_lli[0];

    m_dma_channel[n]->DMA_CFG = HS_SEL_SRC_HARD;
    if ((sourAddr & 0xFFFFF000) == 0x40053000)
        m_dma_channel[0]->DMA_CFG_HIGH = 0x9 << 7;
    else
        m_dma_channel[0]->DMA_CFG_HIGH = 13 << 7;
    //	m_dma_channel[n]->DMACC0LLI = dma_lli->next_lli;
    // m_dma_channel[n]->DMACC0CONFIG_H = 0x01;
    m_dma_control->DMA_MASKTFR = (0x01 << n) | ((0x100) << n); // interrupt
    m_dma_control->DMA_CHEN = (0x01 << n) | ((0x100) << n);
}

void dma_wait_for_done(u8 n)
{
    while (!(m_dma_control->DMA_RAWTFR & (1 << n)))
        ;
    m_dma_control->DMA_CLRTFR = (1 << n);
}

/**
 *@brief 初始化DCMI模块的设置.
 *
 *@param[in] pdcmi  指DCMI_TypeDef结构体的指针;
 *@param[in] pinit  指DCMI_InitTypeDef结构体的指针;
 *@return DRV_DCMI_Init @ref DRV_DCMI_StatusTypeDef
 */
DRV_DCMI_StatusTypeDef DRV_DCMI_Init(DCMI_TypeDef *pdcmi, DCMI_InitTypeDef *pinit)
{
    pdcmi->DMAEN = IDMA_ENABLE | EDMA_ENABLE; // OK

    if (pinit->InputMode == INPUT_FORMAT_YUV422)
    {
        pdcmi->PROC = (SNSR_DATA_FORMAT_YUV422 | YUV_PROC_EN | RGB_PROC_EN);
        pdcmi->EXTRA_DATA_FORM = 0x00000002;
    }
    else if (pinit->InputMode == INPUT_FORMAT_YUV420)
    {
        pdcmi->PROC = (SNSR_DATA_FORMAT_YUV420 | DISP_DATA_FORMAT_OTHER | YUV_PROC_EN | RGB_PROC_EN);
        pdcmi->EXTRA_DATA_FORM = 0x00000001;
    }
    else if (pinit->InputMode == INPUT_FORMAT_RGB565)
    {
        if (pinit->Rgb2Bgr == ENABLE)
        {
            pdcmi->PROC = (SNSR_DATA_FORMAT_RGB565 | YUV_PROC_EN | RGB_PROC_EN | RGB_BGR_1); // ok
        }
        else
        {
            pdcmi->PROC = (SNSR_DATA_FORMAT_RGB565 | YUV_PROC_EN | RGB_PROC_EN);
        }
        pdcmi->EXTRA_DATA_FORM = 0x00000000; // ok
    }
    else if (pinit->InputMode == INPUT_FORMAT_RGB888)
    {
        pdcmi->PROC = (SNSR_DATA_FORMAT_RGB888 | DISP_DATA_FORMAT_RGB565 | YUV_PROC_EN | RGB_PROC_EN);
        pdcmi->EXTRA_DATA_FORM = 0x00000000;
    }
    else
    {
        return (DRV_DCMI_ERROR);
    }
    pdcmi->PROC |= (1 << 25) | (0x7 << 16); // ok

    pdcmi->IDMA_CFG = 0x81e05f00; // ok

    if (pinit->isRgbiDmaMode == ENABLE)
    {
        // pdcmi->IDMA_CFG |=IDMA_FIXED_ADDR;
        pdcmi->PROC |= Y2EXTDMA_EN; // Y走外部dma通路
        pdcmi->IDMA_DST = pinit->iDmaDst;
    }
    else
    {
        //外部DMA
        dma_register_init(DMAC2_BASE_ADDR);
        dma_dcmi_xfer_mcu_lcd((u32)&pdcmi->DR, pinit->eDmaDst);
        // pdcmi->IDMA_DST = pinit->iDmaDst;
    }

    pdcmi->PIC_SIZE = ((pinit->Input_height << 16) | (pinit->Input_width));  // ok
    pdcmi->GRAY_SIZE = ((pinit->Input_height << 16) | (pinit->Input_width)); // 0k
    pdcmi->CWSIZE = ((pinit->Input_height << 16) | (pinit->Input_width));    // ok

    pdcmi->GRAY_COORD = ((0 << 16) | 0); // ok

    // pdcmi->THROTTLE = CAPTURE | (1 << 8);//ok
    pdcmi->THROTTLE = CAPTURE; // ok
    if (pinit->EnableIE == ENABLE)
    {
        pdcmi->IER = 0x00000000;
        pdcmi->ICR = 0x000000FF;
        pdcmi->IER = pinit->IEMask;
        NVIC_Init(0, 0, DCMI_IRQn, 2);
    }

    if (pinit->isCaptureMode == ENABLE)
    {
        pdcmi->CR |= DCMI_ENABLE | CAPTURE; // ok
    }
    else
    {
        pdcmi->CR |= DCMI_ENABLE;
    }

    return (DRV_DCMI_OK);
}

/**
 *@brief 查询方式从设备读取数据.
 *
 *@param[in] pi2c      指向I2C_TypeDef结构体的指针;
 *@param[in] SlaveAdd  从机地址;
 *@param[in] Highspeed 是否高速模式;
 *- ENABLE;
 *- DISABLE;
 *@param[in] pBuf      待接收数据指针;
 *@param[in] Size      待接收数据长度;
 *@return 操作结果;@ref DRV_I2C_StatusTypeDef
 *@warning ccm33xx_it.c实现的中断函数为HAL层的，如客户不使用HAL层则需实现自已的服务函数;
 */
DRV_DCMI_StatusTypeDef DRV_DCMI_Getgray(DCMI_TypeDef *pdcmi, DCMI_InitTypeDef *pinit)
{

    if (pinit->isRgbiDmaMode == ENABLE)
    {
        DRV_DCMI_Getgray_Edma(pdcmi, pinit);
    }
    else
    {

        DRV_DCMI_Getgray_Idma(pdcmi, pinit);
    }
    
    return DRV_DCMI_OK;
}

DRV_DCMI_StatusTypeDef DRV_DCMI_Getgray_Idma(DCMI_TypeDef *pdcmi, DCMI_InitTypeDef *pinit)
{

    pdcmi->IDMA_DST = pinit->iDmaDst;
    pdcmi->ICR = 0x80;

    while ((pdcmi->RIS) & 0x80)
    {
        pdcmi->ICR = 0x80;
    }

    pdcmi->THROTTLE = (CAPTURE | CM_SNAP);
    while (!((pdcmi->RIS) & 0x80))
        ;
    pdcmi->ICR = 0x80;
    
    return DRV_DCMI_OK;
}

DRV_DCMI_StatusTypeDef DRV_DCMI_Getgray_Edma(DCMI_TypeDef *pdcmi, DCMI_InitTypeDef *pinit)
{
    dma_register_init(DMAC2_BASE_ADDR);
    dma_dcmi_xfer(DMA_DCMI_CHN, (u32)&pdcmi->DR, pinit->eDmaDst, pinit->Input_height * pinit->Input_width);

    pdcmi->ICR = 0x80;
    while (pdcmi->RIS & 0x80)
    {
        pdcmi->ICR = 0x80;
    }

    pdcmi->THROTTLE = (CAPTURE | CM_SNAP);

    while (!(pdcmi->RIS & 0x80))
        ;
    pdcmi->ICR = 0x80;

    delay(0x10000);
    dma_wait_for_done(DMA_DCMI_CHN);
    
    return DRV_DCMI_OK;
}

/**
 *@brief DCMI:注册寄存器的回调函数
 *
 *@param[in] hdcmi DCMI句柄
 *@param[in] callback:回调函数实例
 *@param[in] index:回调函数类别
 *@return
 *- 0 :成功;
 *- 1 :失败;
 */
static uint8_t DRV_DCMI_ConfigCallbackRegistry(DCMI_HandleTypeDef *hdcmi,
                                               dcmi_callback_t callback,
                                               uint8_t index)
{
    (void)index;
    hdcmi->callback = callback;
    return 0;
}

/**
 *@brief DCMI:中断处理函数
 *
 *@param[in]
 *- NONE
 *@param[out]
 *- NONE
 *@return NONE
 */
void DRV_DCMI_IRQHandler(void)
{

    if (g_hdcmi->callback)
    {
        g_hdcmi->callback((void *)g_hdcmi);
        return;
    }
}

#define readl(addr) (*(volatile unsigned long *)(addr))
#define writel(addr, value) (*(volatile unsigned long *)(addr)) = ((unsigned long)(value))

/**
 *@brief DCMI:中断回调处理函数
 *
 *@param[in]
 *- NONE
 *@param[out]
 *- NONE
 *@return NONE
 */
void DRV_DCMI_Callback(void)
{
    uint32_t ris_bak;
    uint32_t mis_bak;

    ris_bak = readl(0x40053000 + 0x0008); // RIS
    mis_bak = readl(0x40053000 + 0x0010); // MIS

    if (ris_bak & mis_bak)
    {
        writel(0x40053000 + 0x0014, mis_bak);

        if (mis_bak & 0x10) // line interrupt.
        {
            // printf("line_interrupt\n");
        }

        if (mis_bak & 0x80) // gray interrupt.
        {

            // printf("Gray_interrupt\n");
        }

        if (mis_bak & 0x400) // specific line interrupt.
        {
            // printf("specific line interrupt!\n");
        }
        if (mis_bak & 0x01) // frame interrupt.
        {
            // printf("frame interrupt\n");
        }
    }
}

/**
 *@brief 根据DCMI_HandleTypeDef指定的参数初始化DCMI模块,并初始化相关handle.
 *
 *@param[in] hdcmi 指向DCMI_HandleTypeDef结构体的指针,此结构体包含了DCMI模块的配置信息;
 *@retval @ref HAL_StatusTypeDef
 */
void DCMI_Init(DCMI_HandleTypeDef *hdcmi)
{
    if (hdcmi->State == DCMI_STATE_RESET)
    {
        // HAL_I2C_MspInit(hi2c);
    }

    hdcmi->State = DCMI_STATE_BUSY;

    if (hdcmi->Init.EnableIE == ENABLE)
    {
        g_hdcmi = hdcmi;
        DRV_DCMI_ConfigCallbackRegistry(hdcmi, (dcmi_callback_t)DRV_DCMI_Callback, 0);
    }

    /*初始化硬件层相关*/
    DRV_DCMI_Init(hdcmi->Instance, &hdcmi->Init);

    hdcmi->ErrorCode = DCMI_ERROR_NONE;

    /*状态更改为READY*/
    hdcmi->State = DCMI_STATE_READY;
}
