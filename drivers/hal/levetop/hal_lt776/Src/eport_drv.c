/**
    **********************************************************************************
             Copyright(c) 2021 Levetop Semiconductor Co. Ltd.
                      All Rights Reserved
  **********************************************************************************
  * @file    eport_drv.c
  * @author  Product application department
  * @version V1.0
  * @date    2021.11.01
  * @brief   EPORT模块DRV层驱动.
  * @note 操作EPORT的接口:
  *  - DRV_EPORT_IRQHandler       中断服务函数
  *  - DRV_EPORT_SetTriggerMode   中断触发模式设置
  *  - DRV_EPORT_SetDirection     pin脚方向设置
  *  - DRV_EPORT_SetIT            中断设置
  *  - DRV_EPORT_WritePinsLevel   设置pin脚状态
  *  - DRV_EPORT_ReadPinsLevel    读取pin脚状态
  *  - DRV_EPORT_ReadITFlag       读取中断标志，边沿触发
  *  - DRV_EPORT_SetPullUp        设置pin脚上拉状态
  *  - DRV_EPORT_SetOpenDrain     设置pin脚属性
  *
  @verbatim
  ===================================================================================
                                  ##### 更新 #####
  ===================================================================================
  [..] 更新时间:
  [..] 更新人:
  [..] 更新内容:
        1. nothing.
  @endverbatim
  */
#include "eport_drv.h"

/*** 全局变量定义 ***************************************************************/
/*** volatile **********/

/*** @ static **********/

/*** 常量定义 *******************************************************************/

/*** 函数定义 *******************************************************************/

/**
 * @brief EPORT 中断服务函数
 *
 * @param[in] pEport: EPORT实例化
 * @param[in] Pinx：EPORT引脚
 * @return NONE
 */

void DRV_EPORT_IRQHandler(EPORT_TypeDef *pEport, uint8_t Pinx)
{
    uint8_t status;

    status = _eport_epfr_pins_flag_get(pEport, Pinx);
    if (status)
    {
        _eport_epfr_pins_flag_clr(pEport, Pinx);
        ////printf("边沿促发\r\n");
    }
    else
    {
        // DRV_EPORT_SetIT(pEport, Pinx, DISABLE);
        ////printf("电平促发\r\n");
    }

    if (EPORT == pEport)
    {
        // printf("Enter EPORT%d interrupt.PIN = %d\r\n", 0, PIN_NUM_IS(Pinx));
    }
    else if (EPORT1 == pEport)
    {
        // printf("Enter EPORT%d interrupt.PIN = %d\r\n", 1, PIN_NUM_IS(Pinx));
    }
    else if (EPORT2 == pEport)
    {
        // printf("Enter EPORT%d interrupt.PIN = %d\r\n", 2, PIN_NUM_IS(Pinx));
    }
    else if (EPORT3 == pEport)
    {
        // printf("Enter EPORT%d interrupt.PIN = %d\r\n", 3, PIN_NUM_IS(Pinx));
    }
    else if (EPORT4 == pEport)
    {
        // printf("Enter EPORT%d interrupt.PIN = %d\r\n", 4, PIN_NUM_IS(Pinx));
    }
    else if (EPORT5 == pEport)
    {
        // printf("Enter EPORT%d interrupt.PIN = %d\r\n", 5, PIN_NUM_IS(Pinx));
    }
    else if (EPORT6 == pEport)
    {
        // printf("Enter EPORT%d interrupt.PIN = %d\r\n", 6, PIN_NUM_IS(Pinx));
    }
    else if (EPORT7 == pEport)
    {
        // printf("Enter EPORT%d interrupt.PIN = %d\r\n", 7, PIN_NUM_IS(Pinx));
    }
    else if (EPORT8 == pEport)
    {
        // printf("Enter EPORT%d interrupt.PIN = %d\r\n", 8, PIN_NUM_IS(Pinx));
    }
    else if (EPORT9 == pEport)
    {
        // printf("Enter EPORT%d interrupt.PIN = %d\r\n", 9, PIN_NUM_IS(Pinx));
    }
    else if (EPORT10 == pEport)
    {
        // printf("Enter EPORT%d interrupt.PIN = %d\r\n", 10, PIN_NUM_IS(Pinx));
    }
    else if (EPORT11 == pEport)
    {
        // printf("Enter EPORT%d interrupt.PIN = %d\r\n", 11, PIN_NUM_IS(Pinx));
    }
    else if (EPORT12 == pEport)
    {
        // printf("Enter EPORT%d interrupt.PIN = %d\r\n", 12, PIN_NUM_IS(Pinx));
    }
    else if (EPORT13 == pEport)
    {
        // printf("Enter EPORT%d interrupt.PIN = %d\r\n", 13, PIN_NUM_IS(Pinx));
    }
    else if (EPORT14 == pEport)
    {
        // printf("Enter EPORT%d interrupt.PIN = %d\r\n", 14, PIN_NUM_IS(Pinx));
    }
    else if (EPORT15 == pEport)
    {
        // printf("Enter EPORT%d interrupt.PIN = %d\r\n", 15, PIN_NUM_IS(Pinx));
    }
}

/**
 * @brief EPORT 设置引脚中断触发模式
 *
 * @param[in] pEport:EPORT实例化
 * @param[in] Pins: EPORT引脚
 * @param[in] TriggerMode 中断触发模式
 *  - EPORT_HIGH_LEVEL_TRIGGER    高电平触发
 *  - EPORT_LOW_LEVEL_TRIGGER     低电平触发
 *  - EPORT_RISING_TRIGGER        上升沿触发
 *  - EPORT_FALLING_TRIGGER       下降沿触发
 *  - EPORT_RISINGFALLING_TRIGGER 上升和下降沿触发
 * @return NONE
 */
void DRV_EPORT_SetTriggerMode(EPORT_TypeDef *pEport, uint8_t Pins, EPORT_IntModeDef TriggerMode)
{
    uint8_t i;
    //    uint16_t ports_val;

    for (i = 0; i < 8; i++)
    {
        if (Pins & (0x01 << i))
        {
            switch (TriggerMode)
            {
            case EPORT_HIGH_LEVEL_TRIGGER:
                _eport_eppar_trigger_level_set(pEport, i);
                _eport_eplpr_pins_high_level_set(pEport, Pins);
                break;
            case EPORT_LOW_LEVEL_TRIGGER:
                _eport_eppar_trigger_level_set(pEport, i);
                _eport_eplpr_pins_low_level_set(pEport, Pins);
                break;
            case EPORT_RISING_TRIGGER:
                _eport_eppar_trigger_rising_set(pEport, i);
                break;
            case EPORT_FALLING_TRIGGER:
                _eport_eppar_trigger_falling_set(pEport, i);
                break;
            case EPORT_RISINGFALLING_TRIGGER:
                _eport_eppar_trigger_falling_rising_set(pEport, i);
                break;
            default:
                break;
            }
        }
    }
}

/**
 * @brief EPORT 设置引脚输入输出
 *
 * @param[in] pEport:EPORT实例化
 * @param[in] Pins: EPORT引脚
 * @param[in] Dir:
 *  - GPIO_DIR_OUT
 *  - GPIO_DIR_IN
 * @return NONE
 */
void DRV_EPORT_SetDirection(EPORT_TypeDef *pEport, uint8_t Pins, EPORT_DirTypeDef Dir)
{
    switch (Dir)
    {
    case GPIO_DIR_IN:
        _eport_epddr_pins_input_set(pEport, Pins);
        break;
    case GPIO_DIR_OUT:
        _eport_epddr_pins_output_set(pEport, Pins);
        break;
    default:
        break;
    }
}

/**
 * @brief EPORT 设置中断
 *
 * @param[in] pEport:EPORT实例化
 * @param[in] Pins EPORT引脚
 * @param[in] Status
 *  - DISABLE
 *  - ENABLE
 * @return NONE
 */
void DRV_EPORT_SetIT(EPORT_TypeDef *pEport, uint8_t Pins, FunctionalState Status)
{
    switch (Status)
    {
    case ENABLE:
        _eport_epier_pins_it_en(pEport, Pins);
        break;
    case DISABLE:
        _eport_epier_pins_it_dis(pEport, Pins);
        break;
    default:
        break;
    }
}

/**
 * @brief EPORT 设置引脚电平
 *
 * @param[in] pEport:EPORT实例化
 * @param[in] Pins: EPORT引脚
 * @param[in] Level:
 *  - BIT_RESET
 *  - BIT_SET
 * @return NONE
 */
void DRV_EPORT_WritePinsLevel(EPORT_TypeDef *pEport, uint8_t Pins, BitActionTypeDef Level)
{
    switch (Level)
    {
    case BIT_SET:
        _eport_epdr_pins_load_set(pEport, Pins);
        break;
    case BIT_RESET:
        _eport_epdr_pins_load_clr(pEport, Pins);
        break;
    default:
        break;
    }
}

/**
 * @brief EPORT 读取引脚电平
 *
 * @param[in] pEport:EPORT实例化
 * @param[in] Pins: EPORT引脚
 * @return 引脚状态，返回bits
 */
uint8_t DRV_EPORT_ReadPinsLevel(EPORT_TypeDef *pEport, uint8_t pins)
{
    return _eport_eppdr_pins_data_get(pEport, pins);
}

/**
 * @brief EPORT 读取引脚中断标志
 *
 * @param[in] pEport:EPORT实例化
 * @param[in] Pins: EPORT引脚
 * @note 返回边沿触发的状态
 * @return 引脚中断状态，返回bits
 */
uint8_t DRV_EPORT_ReadITFlag(EPORT_TypeDef *pEport, uint8_t pins)
{
    return _eport_epfr_pins_flag_get(pEport, pins);
}

/**
 * @brief EPORT 配置引脚上下拉使能
 *
 * @param[in] pEport:EPORT实例化
 * @param[in] Pins: EPORT引脚
 * @param[in] Status
 *  - ENABLE
 *  - DISABLE
 * @return NONE
 */
void DRV_EPORT_SetPullUp(EPORT_TypeDef *pEport, uint8_t Pins, FunctionalState Status)
{
    switch (Status)
    {
    case ENABLE:
        _eport_eppuer_pins_pull_up_en(pEport, Pins);
        break;
    case DISABLE:
        _eport_eppuer_pins_pull_up_dis(pEport, Pins);
        break;
    default:
        break;
    }
}

/**
 * @brief EPORT 配置引脚输出属性
 *
 * @param[in] pEport:EPORT实例化
 * @param[in] Pins: EPORT引脚
 * @param[in] Mode
 *  - EPORT_OUTPUT_MODE_OPEN_DRAIN
 *  - EPORT_OUTPUT_MODE_CMOS
 * @return NONE
 */
void DRV_EPORT_SetOpenDrain(EPORT_TypeDef *pEport, uint8_t Pins, EPORT_OutputModeTypeDef Mode)
{
    switch (Mode)
    {
    case EPORT_OUTPUT_MODE_CMOS:
        _eport_epoder_pins_output_cmos_set(pEport, Pins);
        break;
    case EPORT_OUTPUT_MODE_OPEN_DRAIN:
        _eport_epoder_pins_output_od_set(pEport, Pins);
        break;
    default:
        break;
    }
}

/************************ (C) COPYRIGHT LEVETOP *****END OF FILE**********************/
