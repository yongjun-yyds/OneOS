/**********************************************************
 * @Copyright (c) 2021   Levetop Semiconductor Co., Ltd.
 * @File			misc.c
 * @Author		Jason Zhao
 * @Date			2021/12/2
 * @Time			14:42:0
 * @Version
 * @Brief
 **********************************************************/
#include "misc.h"
#include "liblt776.h"
#include "libSwithOSC500MHz.h"
#include "oneos_config.h"
#include "cache_drv.h"
#include "os_task.h"

#ifdef OS_USING_FAL
#include "eflash_drv.h"
#endif

static uint8_t g_OSC500M_Flg = FALSE;

/**
 * @brief 设置系统时钟
 *
 * @param[in] CPM_SYSCLKSource 配置的系统时钟源
 * @param[in] sys_clk_trim 系统时钟源Trim,注意只能Trim内部400M晶振
 * @param[in] core_div 系统时钟源分频系数，取值范围【0x0~0xf】
 * @param[in] sys_div 系统时钟源分频系数，取值范围【0x00~0xff】
 * @param[in] ips_div IPS时钟源分频系数，取值范围【0x00~0x0f】
 * @return @ref CPM_ErrCodeTypeDef
 */
void ConfigSysClk(CPM_ClkInitTypeDef *pClkInit)
{
    // uint32_t efcr_reg;

    //配置系统时钟
    if (CPM_SYSCLK_OSC8M == pClkInit->SysClkSource)
    {
        DRV_CPM_SystemClkOSC8MSelect();
    }
    else if (CPM_SYSCLK_OSC400M == pClkInit->SysClkSource)
    {
        DRV_CPM_SystemClkOSC400MSelect();
    }
#if (FREQUENCY_MHZ == 500)
    else if (CPM_SYSCLK_OSC500M == pClkInit->SysClkSource)
    {
        HAL_StatusTypeDef st;

        st = HAL_CPM_SwitchOSC500MHz();
        if (st == HAL_ERROR) //芯片不支持跑500M，恢复到400M
        {
            pClkInit->SysClkSource = CPM_SYSCLK_OSC400M;
        }
        DRV_CPM_SystemClkOSC400MSelect();
    }
#endif
    else if (CPM_SYSCLK_USBPHY480M == pClkInit->SysClkSource)
    {
        DRV_CPM_SystemClkOSC480MSelect();
    }
    else if (CPM_SYSCLK_OSCEXT == pClkInit->SysClkSource)
    {
        DRV_CPM_SystemClkOSCEXTSelect();
    }

    //配置IPS时钟（分频系数不可以为0）
    if (CLK_DIV_1 == pClkInit->IpsClkDiv)
    {
        DRV_CPM_SetIpsClkDiv(pClkInit->IpsClkDiv + 1);
    }
    else
    {
        DRV_CPM_SetIpsClkDiv(pClkInit->IpsClkDiv);
    }

    if ((CLK_DIV_1 == pClkInit->CoreClkDiv) && (CLK_DIV_1 == pClkInit->SysClkDiv))
    {
        DRV_CPM_SetSystemClkDiv(pClkInit->SysClkDiv + 1);
    }
    else
    {
        DRV_CPM_SetSystemClkDiv(pClkInit->SysClkDiv);
    }

    DRV_CPM_SetCoreClkDiv(pClkInit->CoreClkDiv);

    //获取CPU核时钟
    if (pClkInit->SysClkSource == CPM_SYSCLK_OSC500M)
    {
        g_OSC500M_Flg = TRUE;
        g_core_clk = DRV_CPM_GetCOREClHz(TRUE);
    }
    else
    {
        g_OSC500M_Flg = FALSE;
        //获取CPU核时钟
        g_core_clk = DRV_CPM_GetCOREClHz(FALSE);
    }

    //获取系统时钟
    g_sys_clk = DRV_CPM_GetSYSClHz();

    //获取ips时钟
    g_ips_clk = DRV_CPM_GetIPSClHz();

    if ((g_sys_clk >= DRV_SYS_OSC_CLK_200M) && (CLK_DIV_1 == pClkInit->SdramClkDiv))
    {

        DRV_CPM_SetSdramClkDiv(pClkInit->SdramClkDiv + 1);
    }
    else
    {

        DRV_CPM_SetSdramClkDiv(pClkInit->SdramClkDiv);
    }
    g_sdram_clk = DRV_CPM_GetSDRAMClHz();
}
/**
 * @brief 设置系统时钟
 *
 * @param[in] CPM_SYSCLKSource 配置的系统时钟源
 * @param[in] sys_clk_trim 系统时钟源Trim,注意只能Trim内部400M晶振
 * @param[in] core_div 系统时钟源分频系数，取值范围【0x0~0xf】
 * @param[in] sys_div 系统时钟源分频系数，取值范围【0x00~0xff】
 * @param[in] ips_div IPS时钟源分频系数，取值范围【0x00~0x0f】
 * @return @ref CPM_ErrCodeTypeDef
 */
const unsigned char updata_bin_data[216] = {
    0xf8, 0xb5, 0x04, 0x46, 0x0e, 0x46, 0xdd, 0xe9, 0x06, 0x75, 0x72, 0xb6, 0x01, 0x2c, 0x0c, 0xd1, 0x2f, 0x48,
    0xc0, 0x6b, 0x20, 0xf0, 0x09, 0x00, 0x40, 0xf0, 0x08, 0x00, 0xdf, 0xf8, 0xb0, 0xc0, 0xcc, 0xf8, 0x3c, 0x00,
    0x4f, 0xf0, 0xc0, 0x41, 0x11, 0xe0, 0x02, 0x2c, 0x0c, 0xd1, 0x28, 0x48, 0xc0, 0x6b, 0x20, 0xf4, 0x10, 0x20,
    0x40, 0xf4, 0x00, 0x20, 0xdf, 0xf8, 0x94, 0xc0, 0xcc, 0xf8, 0x3c, 0x00, 0x4f, 0xf0, 0xe0, 0x41, 0x02, 0xe0,
    0x4f, 0xf0, 0xff, 0x30, 0xf8, 0xbd, 0x88, 0x6a, 0x00, 0x90, 0x00, 0x20, 0x88, 0x60, 0x4f, 0xf0, 0x40, 0x20,
    0x80, 0x68, 0xdf, 0xf8, 0x74, 0xc0, 0x00, 0xea, 0x0c, 0x00, 0xa6, 0xf1, 0x01, 0x0c, 0x40, 0xea, 0x0c, 0x70,
    0xa2, 0xf1, 0x01, 0x0c, 0x0c, 0xf0, 0xff, 0x0c, 0x40, 0xea, 0x0c, 0x00, 0x4f, 0xf0, 0x40, 0x2c, 0xcc, 0xf8,
    0x08, 0x00, 0x60, 0x46, 0x80, 0x69, 0x40, 0xf0, 0x02, 0x00, 0xcc, 0xf8, 0x18, 0x00, 0x4b, 0x61, 0xc1, 0xf8,
    0xf0, 0x70, 0x01, 0x20, 0x88, 0x60, 0x01, 0x2c, 0x07, 0xd1, 0x0d, 0x48, 0xc0, 0x6b, 0x40, 0xf0, 0x09, 0x00,
    0xdf, 0xf8, 0x2c, 0xc0, 0xcc, 0xf8, 0x3c, 0x00, 0x02, 0x2c, 0x07, 0xd1, 0x08, 0x48, 0xc0, 0x6b, 0x40, 0xf4,
    0x10, 0x20, 0xdf, 0xf8, 0x18, 0xc0, 0xcc, 0xf8, 0x3c, 0x00, 0x62, 0xb6, 0x25, 0xb1, 0x00, 0xbf, 0x28, 0x1e,
    0xa5, 0xf1, 0x01, 0x05, 0xfb, 0xd1, 0x00, 0x20, 0xbd, 0xe7, 0x00, 0x10, 0x00, 0x40, 0x00, 0xff, 0xff, 0x0f,
};
void ConfigSysClk1(CPM_ClkInitTypeDef *pClkInit)
{
    memcpy((void *)0x2001f000, updata_bin_data, sizeof(updata_bin_data));
    //配置系统时钟
    if (CPM_SYSCLK_OSC8M == pClkInit->SysClkSource)
    {
        DRV_CPM_SystemClkOSC8MSelect();
    }
    else if (CPM_SYSCLK_OSC400M == pClkInit->SysClkSource)
    {
        DRV_CPM_SystemClkOSC400MSelect();
    }
#if (FREQUENCY_MHZ == 500)
    else if (CPM_SYSCLK_OSC500M == pClkInit->SysClkSource)
    {
        HAL_StatusTypeDef st;

        st = HAL_CPM_SwitchOSC500MHz();
        if (st == HAL_ERROR) //芯片不支持跑500M，恢复到400M
        {
            pClkInit->SysClkSource = CPM_SYSCLK_OSC400M;
        }
        DRV_CPM_SystemClkOSC400MSelect();
    }
#endif
    else if (CPM_SYSCLK_USBPHY480M == pClkInit->SysClkSource)
    {
        DRV_CPM_SystemClkOSC480MSelect();
    }
    else if (CPM_SYSCLK_OSCEXT == pClkInit->SysClkSource)
    {
        DRV_CPM_SystemClkOSCEXTSelect();
    }

    //配置IPS时钟（分频系数不可以为0）
    if (CLK_DIV_1 == pClkInit->IpsClkDiv)
    {
        DRV_CPM_SetIpsClkDiv(pClkInit->IpsClkDiv + 1);
    }
    else
    {
        DRV_CPM_SetIpsClkDiv(pClkInit->IpsClkDiv);
    }

    //	if((CLK_DIV_1 == pClkInit->CoreClkDiv)&&(CLK_DIV_1 == pClkInit->SysClkDiv))
    //	{
    //
    //		SysSsiConfigure(1, pClkInit->CoreClkDiv+1, pClkInit->SysClkDiv+2, 2, 2, 2);
    //	}
    //	else
    //    {
    //		SysSsiConfigure(1,pClkInit->CoreClkDiv+1,pClkInit->SysClkDiv+1,2,2,2);
    //	}

    //获取CPU核时钟
    if (pClkInit->SysClkSource == CPM_SYSCLK_OSC500M)
    {
        g_OSC500M_Flg = TRUE;
        g_core_clk = DRV_CPM_GetCOREClHz(TRUE);
    }
    else
    {
        g_OSC500M_Flg = FALSE;
        //获取CPU核时钟
        g_core_clk = DRV_CPM_GetCOREClHz(FALSE);
    }

    //获取系统时钟
    g_sys_clk = DRV_CPM_GetSYSClHz();

    //获取ips时钟
    g_ips_clk = DRV_CPM_GetIPSClHz();

    if ((g_sys_clk >= DRV_SYS_OSC_CLK_200M) && (CLK_DIV_1 == pClkInit->SdramClkDiv))
    {

        DRV_CPM_SetSdramClkDiv(pClkInit->SdramClkDiv + 1);
    }
    else
    {

        DRV_CPM_SetSdramClkDiv(pClkInit->SdramClkDiv);
    }
    g_sdram_clk = DRV_CPM_GetSDRAMClHz();
}

void GetSysClk(void)
{
    //获取CPU核时钟
    if (g_OSC500M_Flg == TRUE)
    {
        g_core_clk = DRV_CPM_GetCOREClHz(TRUE);
    }
    else
    {
        //获取CPU核时钟
        g_core_clk = DRV_CPM_GetCOREClHz(FALSE);
    }

    //获取系统时钟
    g_sys_clk = DRV_CPM_GetSYSClHz();

    //获取ips时钟
    g_ips_clk = DRV_CPM_GetIPSClHz();

    //获取sdram时钟
    g_sdram_clk = DRV_CPM_GetSDRAMClHz();
}
/**
 * @brief 设置IPS、EPT、CLKOUT、EFM、SYSTEM的时钟分频
 *
 * @param[in] CLKSourceFlg: 时钟源标志。
 * @param[in] Div:分频系数,分频值等于(Div+1)
 * @retval NONE
 */
void SetClkDiv(CPM_ClkSourceIndexTypeDef ClkSourceIndex, uint8_t Div)
{
    switch (ClkSourceIndex)
    {
    case HAL_CHIP_SYS_CLK_INDEX_CLKOUT:
    {
        /*CLKOUT时钟分频*/
        DRV_CPM_SetClkoutClkDiv(Div);
        break;
    }
    case HAL_CHIP_SYS_CLK_INDEX_TRACE:
    {
        /*TRACE时钟分频*/
        DRV_CPM_SetTraceClkDiv(Div);
        break;
    }
    case HAL_CHIP_SYS_CLK_INDEX_SYS:
    {
        /*系统时钟分频 */
        DRV_CPM_SetSystemClkDiv(Div);
        break;
    }
    case HAL_CHIP_CORE_CLK_INDEX_SYS:
    {
        /*内核时钟分频 */
        DRV_CPM_SetCoreClkDiv(Div);
        break;
    }
    case HAL_CHIP_PERIPHERAL_CLK_INDEX_ARITH:
    {
        /*ARITH时钟分频*/
        DRV_CPM_SetArithClkDiv(Div);
        break;
    }
    case HAL_CHIP_PERIPHERAL_CLK_INDEX_AHB3:
    {
        /*AHB3输出时钟分频*/
        DRV_CPM_SetAhb3ClkDiv(Div);
        break;
    }
    case HAL_CHIP_PERIPHERAL_CLK_INDEX_IPS:
    {
        /*IPS时钟分频*/
        DRV_CPM_SetIpsClkDiv(Div);
        break;
    }
    case HAL_CHIP_PERIPHERAL_CLK_INDEX_TC:
    {
        /*TC时钟分频*/
        DRV_CPM_SetTcClkDiv(Div);
        break;
    }
    case HAL_CHIP_PERIPHERAL_CLK_INDEX_MESH:
    {
        /*MESH时钟分频*/
        DRV_CPM_SetMeshClkDiv(Div);
        break;
    }
    case HAL_CHIP_PERIPHERAL_CLK_INDEX_ADC:
    {
        /*ADC时钟分频*/
        DRV_CPM_SetAdcClkDiv(Div);
        break;
    }
    case HAL_CHIP_PERIPHERAL_CLK_INDEX_MCC_ADR:
    {
        /*MCC_ADR时钟分频*/
        DRV_CPM_SetMccAdrClkDiv(Div);
        break;
    }
    case HAL_CHIP_PERIPHERAL_CLK_INDEX_MCC:
    {
        /*MCC时钟分频*/
        DRV_CPM_SetMccClkDiv(Div);
        break;
    }
    case HAL_CHIP_PERIPHERAL_CLK_INDEX_SDRAM2LCD:
    {
        /*系统时钟分频 */
        DRV_CPM_SetSdram2lcdClkDiv(Div);
        break;
    }
    case HAL_CHIP_PERIPHERAL_CLK_INDEX_SDRAM_SM:
    {
        /*ARITH时钟分频*/
        DRV_CPM_SetSdram_smClkDiv(Div);
        break;
    }
    case HAL_CHIP_PERIPHERAL_CLK_INDEX_SDRAM:
    {
        /*AHB3输出时钟分频*/
        DRV_CPM_SetSdramClkDiv(Div);
        break;
    }
    case HAL_CHIP_PERIPHERAL_CLK_INDEX_CLCD:
    {
        /*IPS时钟分频*/
        DRV_CPM_SetClcdClkDiv(Div);
        break;
    }
    case HAL_CHIP_PERIPHERAL_CLK_INDEX_SD_HOST:
    {
        /*TC时钟分频*/
        DRV_CPM_SetSd_hostClkDiv(Div);
        break;
    }
    case HAL_CHIP_PERIPHERAL_CLK_INDEX_DCMI_SENSOR:
    {
        /*MESH时钟分频*/
        DRV_CPM_SetDcmi_sensorClkDiv(Div);
        break;
    }
    case HAL_CHIP_PERIPHERAL_CLK_INDEX_DCMI_PIX:
    {
        /*ADC时钟分频*/
        DRV_CPM_SetDcmi_pixClkDiv(Div);
        break;
    }
    case HAL_CHIP_PERIPHERAL_CLK_INDEX_MIPI_SAMPLE:
    {
        /*MCC_ADR时钟分频*/
        DRV_CPM_SetMipi_sampleClkDiv(Div);
        break;
    }
    case HAL_CHIP_PERIPHERAL_CLK_INDEX_DMA2D_SRAM:
    {
        /*MCC时钟分频*/
        DRV_CPM_SetDma2d_sramClkDiv(Div);
        break;
    }
    default:
    {
        break;
    }
    }
}

/**
 * @brief 获取芯片唯一SN号.
 * @param[out] pSN：pSN：芯片SN号缓存，长度8字节，未CP的芯片返回值为全0xFF。.
 */
void GetSN(uint8_t *pSN)
{
    //  LIB_GetSerialNumber(pSN);
}

/**
 * @brief 将芯片恢复rom boot状态，芯片重新上电从rom启动.
 * @note 执行函数前请先初始化EFLASH
 * @retval NONE
 */
void RebackBoot(void)
{
#ifdef OS_USING_FAL
    DRV_EFLASH_SetWritePermission();
    LIB_EFLASH_RecoveryToBoot();
#endif
}

/**
 * @brief 将芯片禁止rom boot状态，芯片重新上电或复位从EFlash启动或SPIFLASH启动.
 * @note 执行函数前请先初始化EFLASH
 * @retval NONE
 */
void DisBoot(void)
{
#ifdef OS_USING_FAL
    LIB_EFlash_Chip_Mode(EFLASH_BOOT);
    ;
#endif
}

/**
 * @brief 系统执行软件复位.
 * @note 执行函数前请先初始化EFLASH
 * @retval NONE
 */
void HAL_LockDisJtag(void)
{
    // LIB_LockDisJtag(LOCK_JTAG_KEY);	//执行此接口，芯片将不能仿真，执行前请慎重考虑！！！
}

void InitChip(void)
{
    CPM_ClkInitTypeDef clk_init;
    uint8_t sdDiv;

    DeInitCache();
    /*调试时钟建议打开，预防修改时钟失败导致无法仿真*/
    // delay(250000);

#if (FREQUENCY_MHZ == 500)
    clk_init.SysClkSource = CPM_SYSCLK_OSC500M;
    sdDiv = CLK_DIV_4;
#else
    /*系统时钟源选择内部8M、内部400M和外部12M，推荐内部400M*/
    clk_init.SysClkSource = CPM_SYSCLK_OSC400M;
    sdDiv = CLK_DIV_3;
#endif

    /* 最小1分频，最大16分频  */
    clk_init.CoreClkDiv = CLK_DIV_1;
    /*最小1分频，最大256分频，当 CoreClkDiv=CLK_DIV_1时 SysClkDiv最小为2分频！，既是系统时钟最大为200MHZ*/
    clk_init.SysClkDiv = CLK_DIV_2;
    /*最小1分频，最大16分频， 当 CoreClkDiv=CLK_DIV_1同时 SysClkDiv = CLK_DIV_2时
     * SdramClkDiv最小为2分频！既是SDRAM时钟最大为100MHZ。*/
    clk_init.SdramClkDiv = CLK_DIV_2;
    /*最小2分频，最大16分频， 当 CoreClkDiv=CLK_DIV_1同时 SysClkDiv = CLK_DIV_2时
     * IpsClkDiv最小为2分频！既是IPS时钟最大为100MHZ。  */
    clk_init.IpsClkDiv = CLK_DIV_2;

#if defined(__CC_ARM) || defined(__CLANG_ARM)
    extern unsigned int __Vectors;
    if ((unsigned int)(&__Vectors) == 0x10000000)
#elif defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050)
    if ((unsigned int)(&__Vectors) == 0x10000000)
#elif defined(__GNUC__)
    extern unsigned int g_pfnVectors;
    if ((unsigned int)(&g_pfnVectors) == 0x10000000)
#endif
    {
        /*初始化时钟*/
        ConfigSysClk1(&clk_init);
    }
    else
    {
        /*初始化时钟*/
        ConfigSysClk(&clk_init);
    }

    /*初始化CACHE*/
    InitCache();

    SetClkDiv(HAL_CHIP_PERIPHERAL_CLK_INDEX_SD_HOST, sdDiv);
}

void delay(volatile uint32_t time)
{
    while (time--)
    {
        ;
    }
}
