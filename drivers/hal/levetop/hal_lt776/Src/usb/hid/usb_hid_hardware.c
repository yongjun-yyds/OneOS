#include "usb_drv.h"
#include "usb_hid_protocol.h"
#include "stdlib.h"

/*************************************************
Function: USB_HID_Receive
Description: USB端点接收一包数据
Input:  -usbEpx :USB接收端口号
        -buf    :接收数据的起始地址
Output: 无
Return: 接收数据的长度
Others: 无
*************************************************/
uint16_t USB_HID_Receive(uint8_t usbEpx, uint8_t *buf)
{
    return USBC_ReceiveData(usbEpx, buf);
}

/*************************************************
Function: USB_HID_Send
Description: USB端点发送一包数据
Input:--usbEpx :USB发送端口号
      -buf:发送数据的起始地址
      -len:发送数据的长度
Output: 无
Return: 无
Others: 无
*************************************************/
void USB_HID_Send(uint8_t usbEpx, uint8_t *buf, uint16_t len)
{
    uint16_t n, m;
    uint16_t i;

    n = len / gUSBC_PacketSize;
    m = len % gUSBC_PacketSize;

    for (i = 0; i < n; i++)
    {
        USBC_SendData(usbEpx, buf + gUSBC_PacketSize * i, gUSBC_PacketSize);
    }
    if (m)
    {
        uint8_t *temp = (uint8_t *)malloc(gUSBC_PacketSize);
        if (!temp)
        {
            return;
        }

        memset(temp, 0, gUSBC_PacketSize);
        memcpy(temp, buf + gUSBC_PacketSize * n, m);
        
        USBC_SendData(usbEpx, temp, gUSBC_PacketSize);

        free(temp);
    }
}
