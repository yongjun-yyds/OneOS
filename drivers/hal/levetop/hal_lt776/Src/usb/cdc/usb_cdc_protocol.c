#include "usb_drv.h"
#include "usb_protocol.h"
#include "usb_cdc_protocol.h"

/*************************************************
Function: USBC_VarInit
Description: USB变量初始化
Output: -None
Return: -None
*************************************************/
void USBC_VarInit(void)
{
}

/*************************************************
Function: USB_CDC_GetDescriptor
Description: USB standard command GetDescriptor
Input: -None
Output: -None
Return: -None
*************************************************/
static void USB_CDC_GetDescriptor(USB_REQ_TypeDef *usb_request)
{
    uint8_t USBDEV_Configuration_Descriptor[USB_CDC_CONFIG_LEN];

    _memcpy(USBDEV_Configuration_Descriptor, (uint8_t *)usb_cdc_Configuration_Descriptor, USB_CDC_CONFIG_LEN);

    switch ((usb_request->Value >> 8) & 0xFF)
    {
    case DEVICE_TYPE:
        USBC_WriteEP0Data((uint8_t *)usb_cdc_Device_Descriptor, usb_request->Length > usb_cdc_Device_Descriptor[0] ? 8 : usb_request->Length, PACKET_END);
        break;
    case CONFIG_TYPE:
        USBC_EP0SendData(usb_request->Length, (uint8_t *)usb_cdc_Configuration_Descriptor, USB_CDC_CONFIG_LEN);
        break;
    case STRING_TYPE:
        switch (usb_request->Value & 0xFF)
        {
        case 0:
            USBC_WriteEP0Data((uint8_t *)usb_cdc_LanguageID, usb_cdc_LanguageID[0], PACKET_END);
            break;
        case 1:
            USBC_WriteEP0Data((uint8_t *)usb_cdc_StrDescManufacturer, usb_request->Length > usb_cdc_StrDescManufacturer[0] ? usb_cdc_StrDescManufacturer[0] : usb_request->Length, PACKET_END);
            break;
        case 2:
            USBC_WriteEP0Data((uint8_t *)usb_cdc_StrDescProduct, usb_request->Length > usb_cdc_StrDescProduct[0] ? usb_cdc_StrDescProduct[0] : usb_request->Length, PACKET_END);
            break;
        case 3:
            USBC_WriteEP0Data((uint8_t *)usb_cdc_StrDescSerialNumber, usb_request->Length > usb_cdc_StrDescSerialNumber[0] ? usb_cdc_StrDescSerialNumber[0] : usb_request->Length, PACKET_END);
            break;
        default:
            break;
        }
        break;
    case DEVICE_QUALIFIER:
        USBC_WriteEP0Data((uint8_t *)usb_cdc_Device_Qualifier_Descriptor, usb_request->Length > usb_cdc_Device_Qualifier_Descriptor[0] ? 8 : usb_request->Length, PACKET_END);
        break;
    case OTHER_SPEED:
        if (usb_request->Length < 10)
        {
            USBDEV_Configuration_Descriptor[1] = DT_OTHER_SPEED_CONDIGURATION;
            USBC_WriteEP0Data((uint8_t *)USBDEV_Configuration_Descriptor, usb_request->Length, PACKET_END);
        }
        break;
    default:
        USBC_EP0SendStall();
        break;
    }
}

/*************************************************
Function: USBC_Ep0Handler
Description: USB CDC协议下EP0的中断处理
Input: -None
Output: -None
Return: -None
*************************************************/
void USBC_Ep0Handler(void)
{
    USB_REQ_TypeDef usb_request;

    //设置串口属性，非setup包，而是out包
    if (gUSBC_IndexReg->E0COUNTR_L != DEV_CSR0_DATAEND)
    {
        USBC_WriteEPxData(CONTROL_EP, usb_cdc_LineCoding, 7);
        gUSBC_IndexReg->E0CSR_L = DEV_CSR0_SERVICE_RXPKTRDY;
        return;
    }

    /*获取USB命令请求数据*/
    USBC_ReadEPxData(CONTROL_EP, (uint8_t *)&usb_request, 8);

    if ((usb_request.RequestType & USB_REQUEST_MASK) == USB_REQUEST_CLASS)
    {
        gUSBC_IndexReg->E0CSR_L = DEV_CSR0_SERVICE_RXPKTRDY;
        switch (usb_request.Request)
        {
        case SET_LINE_CODING:
        case SET_CONTROL_LINR_STATE:
        case SEND_BREAK:
            break;
        case GET_LINE_CODING:
            USBC_WriteEP0Data((uint8_t *)usb_cdc_LineCoding, 7, PACKET_END);
            break;
        default:
            USBC_EP0SendStall();
        }
    }
    else
    {
        switch (usb_request.Request)
        {
        case GET_STATUS:
            // set ServiceRxPktRdy to clear RxPktRdy and set DataEnd
            gUSBC_IndexReg->E0CSR_L = DEV_CSR0_SERVICE_RXPKTRDY | DEV_CSR0_DATAEND;
            USB_GetStatus(usb_request.RequestType);
            break;
        case CLEAR_FEATURE:
            // set ServiceRxPktRdy to clear RxPktRdy and set DataEnd
            gUSBC_IndexReg->E0CSR_L = DEV_CSR0_SERVICE_RXPKTRDY | DEV_CSR0_DATAEND;
            break;
        case SET_FEATURE:
            // set ServiceRxPktRdy to clear RxPktRdy and set DataEnd
            gUSBC_IndexReg->E0CSR_L = DEV_CSR0_SERVICE_RXPKTRDY | DEV_CSR0_DATAEND;
            if (usb_request.RequestType == 0)
            {
                USB_TestMode(usb_request.Index);
            }
            break;
        case SET_ADDRESS:
            // set ServiceRxPktRdy to clear RxPktRdy and set DataEnd
            gUSBC_IndexReg->E0CSR_L = DEV_CSR0_SERVICE_RXPKTRDY | DEV_CSR0_DATAEND;
            gUSBC_NewAddress = usb_request.Value & 0xFF;
            break;
        case GET_DESCRIPTOR:
            // set ServiceRxPktRdy to clear RxPktRdy
            gUSBC_IndexReg->E0CSR_L = DEV_CSR0_SERVICE_RXPKTRDY;
            USB_CDC_GetDescriptor(&usb_request);
            break;
        case GET_CONFIGURATION:
            // set ServiceRxPktRdy to clear RxPktRdy and set DataEnd
            gUSBC_IndexReg->E0CSR_L = DEV_CSR0_SERVICE_RXPKTRDY | DEV_CSR0_DATAEND;
            USBC_WriteEP0Data((uint8_t *)&usb_request.Request, 1, PACKET_END);
            break;
        case SET_CONFIGURATION:
            // set ServiceRxPktRdy to clear RxPktRdy and set DataEnd
            gUSBC_IndexReg->E0CSR_L = DEV_CSR0_SERVICE_RXPKTRDY | DEV_CSR0_DATAEND;
            break;
        case GET_INTERFACE:
            // set ServiceRxPktRdy to clear RxPktRdy and set DataEnd
            gUSBC_IndexReg->E0CSR_L = DEV_CSR0_SERVICE_RXPKTRDY;
            USBC_WriteEP0Data((uint8_t *)&usb_request.Request, 1, PACKET_END);
            break;
        case SET_INTERFACE:
            gUSBC_IndexReg->E0CSR_L = DEV_CSR0_SERVICE_RXPKTRDY | DEV_CSR0_DATAEND;
            break;
        default:
            USBC_EP0SendStall();
        }
    }
}
