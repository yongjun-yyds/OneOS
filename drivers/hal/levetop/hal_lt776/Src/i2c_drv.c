/**
  **********************************************************************************
             Copyright(c) 2020 Levetop Semiconductor Co., Ltd.
                      All Rights Reserved
  **********************************************************************************
  *@file    i2c_drv.c
  *@author  Product application department
  *@version V1.0
  *@date    2020.02.13
  *@brief   I2C模块DRV层驱动.
  *
  @verbatim
  ===================================================================================
                                  ##### 更新 #####
  ===================================================================================
  [..] 更新时间:
  [..] 更新人:
  [..] 更新内容:
        1. nothing.
  @endverbatim
*/
#include "i2c_drv.h"
//#include "delay.h"
/*** 全局变量定义 ***************************************************************/
/**@ volatile
 *@{
 */

/**@
 *@}
 */
/**@ static
 *@{
 */

/**@
 *@}
 */
/*** 常量定义 *******************************************************************/
/**@ const
 *@{
 */

/**@
 *@}
 */
void DRV_I2C_Stop(I2C_TypeDef *pi2c)
{
    /*SDL 高 SDA LOW--HIGH*/
    _i2c_set_scl_bit(pi2c);
    _i2c_reset_sda_bit(pi2c);
    _i2c_set_sda_bit(pi2c);
}
/**
 *@brief 恢复I2C模块的设置.
 *
 *@param[in] pi2c 指向I2C_TypeDef结构体的指针;
 *@return DRV_I2C_StatusTypeDef @ref DRV_I2C_StatusTypeDef
 */
DRV_I2C_StatusTypeDef DRV_I2C_DeInit(I2C_TypeDef *pi2c)
{
    pi2c->CCR = 0x00;

    return (DRV_I2C_OK);
}
/**
 *@brief 初始化I2C模块的设置.
 *
 *@param[in] pi2c  指向I2C_TypeDef结构体的指针;
 *@param[in] pinit 指向I2C_InitTypeDef结构体的指针;
 *@return DRV_I2C_StatusTypeDef @ref DRV_I2C_StatusTypeDef
 */
DRV_I2C_StatusTypeDef DRV_I2C_Init(I2C_TypeDef *pi2c, I2C_InitTypeDef *pinit)
{
    _i2c_dis(pi2c);
    _i2c_en(pi2c);

    /*管脚配置为主功能*/
    //_i2c_sda_configure_as_primary(pi2c);
    //_i2c_scl_configure_as_primary(pi2c);

    if (pinit->Mode == I2C_MODE_MASTER)
    {
        _i2c_nack(pi2c);
        /*主模式初始化以下:
         *- 时钟频
         */
        if (pinit->ClockMode == I2C_CLOCK_MODE_TEST)
        {
            _i2c_en_clock_test_mode(pi2c);
        }
        else if (pinit->ClockMode == (uint8_t)I2C_CLOCK_MODE_NORMAL)
        {
            _i2c_en_clock_normal_mode(pi2c);
        }

        /*分频系数*/
        pi2c->PR |= pinit->Prescaler & 0x3f;
    }
    else if (pinit->Mode == I2C_MODE_SLAVE)
    {
        /*从机地址*/
        if (pinit->AddBits == I2C_SLAVE_ADD_7BITS)
        {
#if 0
            pi2c->SARH = 0;
            pi2c->SARL = pinit->Add;
#else
            pi2c->SAR = pinit->Add;
            _i2c_ack(pi2c);
#endif
        }
        else if (pinit->AddBits == I2C_SLAVE_ADD_10BITS)
        {
#if 0
            /*MSB 2bits放在SAR的高8bits掩码11110XX0*/
            pi2c->SARH = (uint8_t)(((pinit->Add >> 8) << 1) | 0XF0);
            /*低8位*/
            pi2c->SARL = (pinit->Add) & 0XFF;
#else

#endif
        }
    }
    else
    {
        _i2c_dis(pi2c);
        return (DRV_I2C_ERROR);
    }

    return (DRV_I2C_OK);
}
/**
 *@brief 使能I2C模块.
 *
 *@param[in] pi2c  指向I2C_TypeDef结构体的指针;
 *@return 无
 */
inline void DRV_I2C_En(I2C_TypeDef *pi2c)
{
    _i2c_en(pi2c);
}
/**
 *@brief 禁止I2C模块.
 *
 *@param[in] pi2c  指向I2C_TypeDef结构体的指针;
 *@return 无
 */
inline void DRV_I2C_Dis(I2C_TypeDef *pi2c)
{
    _i2c_dis(pi2c);
}
/**
 *@brief I2C模块使能或禁止.
 *
 *@param[in] pi2c  指向I2C_TypeDef结构体的指针;
 *@param[in] NewState 新的状态;
 *- ENABLE;
 *- DISABLE;
 *@return 无
 */
void DRV_I2C_Cmd(I2C_TypeDef *pi2c, FunctionalStateTypeDef NewState)
{
    if (NewState == ENABLE)
    {
        _i2c_en(pi2c);
    }
    else
    {
        _i2c_dis(pi2c);
    }
}
/**
 *@brief I2C模块中断使能.
 *
 *@param[in] pi2c  指向I2C_TypeDef结构体的指针;
 *@param[in] index 中断索引;
 *- I2C_IT_INDEX_IEN;
 *- I2C_IT_INDEX_AMIE;
 *- I2C_IT_INDEX_SLV_HSIE;
 *@return 无
 */
inline void DRV_I2C_EnIt(I2C_TypeDef *pi2c, I2C_ItIndexTypeDef index)
{
    pi2c->CCR |= index;
}
/**
 *@brief I2C模块中断禁止.
 *
 *@param[in] pi2c  指向I2C_TypeDef结构体的指针;
 *@param[in] index 中断索引;
 *- I2C_IT_INDEX_IEN;
 *- I2C_IT_INDEX_AMIE;
 *- I2C_IT_INDEX_SLV_HSIE;
 *@return 无
 */
inline void DRV_I2C_DisIt(I2C_TypeDef *pi2c, I2C_ItIndexTypeDef index)
{
    pi2c->CCR &= ~index;
}
/**
 *@brief I2C模块中断使能禁止.
 *
 *@param[in] pi2c  指向I2C_TypeDef结构体的指针;
 *@param[in] index 中断索引;
 *- I2C_IT_INDEX_IEN;
 *- I2C_IT_INDEX_AMIE;
 *- I2C_IT_INDEX_SLV_HSIE;
 *@param[in] NewState 新的状态;
 *- ENABLE;
 *- DISABLE;
 *@return 无
 */
void DRV_I2C_ItCmd(I2C_TypeDef *pi2c, I2C_ItIndexTypeDef index, FunctionalStateTypeDef NewState)
{
    if (NewState == ENABLE)
    {
        DRV_I2C_EnIt(pi2c, index);
    }
    else
    {
        DRV_I2C_DisIt(pi2c, index);
    }
}
/**
 *@brief 获取I2C模块标志状态.
 *
 *@param[in] pi2c  指向I2C_TypeDef结构体的指针;
 *@param[in] index 标志索引;
 *- I2C_FLAG_INDEX_TF;
 *- I2C_FLAG_INDEX_RC;
 *- I2C_FLAG_INDEX_ASLV;
 *- I2C_FLAG_INDEX_BUSY;
 *- I2C_FLAG_INDEX_ARBL;
 *- I2C_FLAG_INDEX_RXTX;
 *- I2C_FLAG_INDEX_DACK;
 *- I2C_FLAG_INDEX_AACK;
 *@param[out] flag 状态值指针
 *@return 操作结果 @ref DRV_I2C_StatusTypeDef
 */
DRV_I2C_StatusTypeDef DRV_I2C_GetFlag(I2C_TypeDef *pi2c, I2C_FlagIndexTypeDef index, FlagStatusTypeDef *pFlag)
{
    *pFlag = (pi2c->SR & (1 << index)) ? SET : RESET;

    return (DRV_I2C_OK);
}
/**
 *@brief 获取I2C模块标志状态.
 *
 *@param[in] pi2c  指向I2C_TypeDef结构体的指针;
 *@param[in] index 标志索引;
 *- I2C_FLAG_INDEX_TF;
 *- I2C_FLAG_INDEX_RC;
 *- I2C_FLAG_INDEX_ASLV;
 *- I2C_FLAG_INDEX_BUSY;
 *- I2C_FLAG_INDEX_ARBL;
 *- I2C_FLAG_INDEX_RXTX;
 *- I2C_FLAG_INDEX_DACK;
 *- I2C_FLAG_INDEX_AACK;
 *@param[in] status 状态值
 *- SET;
 *- RESET;
 *@param[in] timeou 超时时间;
 *@return 操作结果 @ref DRV_I2C_StatusTypeDef
 */
DRV_I2C_StatusTypeDef
DRV_I2C_WaitonFlagTimeout(I2C_TypeDef *pi2c, I2C_FlagIndexTypeDef index, FlagStatusTypeDef status, uint32_t timeout)
{
#if 1
    FlagStatusTypeDef tmp_flag = RESET;

    if (timeout == 0)
    {
        do
        {
            delay(10);
            DRV_I2C_GetFlag(pi2c, index, &tmp_flag);
        } while (tmp_flag != status);
    }
    else
    {
        if (status == SET)
        {
            DRV_I2C_GetFlag(pi2c, index, &tmp_flag);

            while (tmp_flag != SET)
            {
                timeout--;

                if (timeout == 0)
                {
                    return (DRV_I2C_TIMEOUT);
                }
                delay(10000);
                DRV_I2C_GetFlag(pi2c, index, &tmp_flag);
            }
        }
        else if (status == RESET)
        {
            DRV_I2C_GetFlag(pi2c, index, &tmp_flag);

            while (tmp_flag != RESET)
            {

                timeout--;

                if (timeout == 0)
                {
                    return (DRV_I2C_TIMEOUT);
                }
                delay(10000);
                DRV_I2C_GetFlag(pi2c, index, &tmp_flag);
            }
        }
    }

#endif
    return DRV_I2C_OK;
}
/**
 *@brief 获取I2C模块高速模式状态标志.
 *
 *@param[in] pi2c  指向I2C_TypeDef结构体的指针;
 *@param[in] status 状态值
 *- SET;
 *- RESET;
 *@param[in] timeou 超时时间;
 *@return 操作结果 @ref DRV_I2C_StatusTypeDef
 */
DRV_I2C_StatusTypeDef DRV_I2C_WaitonSlvHsFlagTimeout(I2C_TypeDef *pi2c, FlagStatusTypeDef status, uint32_t timeout)
{
    FlagStatusTypeDef tmp_flag = RESET;

    if (timeout == 0)
    {
        do
        {
            if (_i2c_get_flag_slave_high_speed(pi2c))
                tmp_flag = SET;
            else
                tmp_flag = RESET;
        } while (tmp_flag != SET);
    }
    else
    {
        if (status == SET)
        {
            if (_i2c_get_flag_slave_high_speed(pi2c))
                tmp_flag = SET;
            else
                tmp_flag = RESET;

            while (tmp_flag != SET)
            {
                timeout--;

                if (timeout == 0)
                {
                    return (DRV_I2C_TIMEOUT);
                }

                if (_i2c_get_flag_slave_high_speed(pi2c))
                    tmp_flag = SET;
                else
                    tmp_flag = RESET;
            }
        }
        else if (status == RESET)
        {
            if (_i2c_get_flag_slave_high_speed(pi2c))
                tmp_flag = SET;
            else
                tmp_flag = RESET;

            while (tmp_flag != RESET)
            {
                timeout--;

                if (timeout == 0)
                {
                    return (DRV_I2C_TIMEOUT);
                }

                if (_i2c_get_flag_slave_high_speed(pi2c))
                    tmp_flag = SET;
                else
                    tmp_flag = RESET;
            }
        }
    }

    return DRV_I2C_OK;
}
/**
 *@brief I2C模块主模式写操作请求.
 *
 *@param[in] pi2c     指向I2C_TypeDef结构体的指针;
 *@param[in] SlaveAdd 从机地址;
 *@param[in] timeou   超时时间;
 *@return 操作结果;@ref DRV_I2C_StatusTypeDef
 */
DRV_I2C_StatusTypeDef DRV_I2C_MasterWriteRequest(I2C_TypeDef *pi2c, uint8_t SlaveAdd, uint32_t timeout)
{
    DRV_I2C_StatusTypeDef status;
    FlagStatusTypeDef     tmp_flag;

    /*产生起始条件:读请求最低位为0*/
    SlaveAdd &= ~0x01;
    pi2c->DR = SlaveAdd;
    _i2c_generate_start(pi2c);

    /*查TF标志*/
    status = DRV_I2C_WaitonFlagTimeout(pi2c, I2C_FLAG_INDEX_TF, SET, timeout);

    DRV_I2C_GetFlag(pi2c, I2C_FLAG_INDEX_AACK, &tmp_flag);

    if (tmp_flag == SET)
    {
        return (DRV_I2C_ERROR);
        ;
    }

    return (status);
}
/**
 *@brief I2C模块主模式读操作请求.
 *
 *@param[in] pi2c     指向I2C_TypeDef结构体的指针;
 *@param[in] SlaveAdd 从机地址;
 *@param[in] timeou   超时时间;
 *@return 操作结果;@ref DRV_I2C_StatusTypeDef
 */
DRV_I2C_StatusTypeDef DRV_I2C_MasterReadRequest(I2C_TypeDef *pi2c, uint8_t SlaveAdd, uint32_t timeout)
{
    DRV_I2C_StatusTypeDef status;
    FlagStatusTypeDef     tmp_flag;

    /*产生起始条件:写请求最低位为1*/
    SlaveAdd |= 0x01;
    pi2c->DR = SlaveAdd;
    _i2c_generate_start(pi2c);

    /*查询RC标志*/
    status = DRV_I2C_WaitonFlagTimeout(pi2c, I2C_FLAG_INDEX_RC, SET, timeout);
    DRV_I2C_GetFlag(pi2c, I2C_FLAG_INDEX_AACK, &tmp_flag);

    if (tmp_flag == SET)
    {
        return (DRV_I2C_ERROR);
    }
    return (status);
}
/**
 *@brief I2C模块主模式生成停止条件.
 *
 *@param[in] pi2c     指向I2C_TypeDef结构体的指针;
 *@param[in] NewState 高速使能;
 *- ENABLE;
 *- DISABLE;
 *@param[in] timeou   超时时间;
 *@return 操作结果;@ref DRV_I2C_StatusTypeDef
 */
DRV_I2C_StatusTypeDef DRV_I2C_MasterGenerateStop(I2C_TypeDef *pi2c, FunctionalStateTypeDef NewState, uint32_t timeout)
{
    DRV_I2C_StatusTypeDef status = DRV_I2C_OK;
    _i2c_generate_stop(pi2c);

    if (NewState == ENABLE)
    {
        _i2c_dis_highspeed(pi2c);
    }

    /*查询BUSY标志*/
    status = DRV_I2C_WaitonFlagTimeout(pi2c, I2C_FLAG_INDEX_BUSY, RESET, timeout);

    return (status);
}
DRV_I2C_StatusTypeDef DRV_I2C_MasterGenerateStopV1(I2C_TypeDef *pi2c, FunctionalStateTypeDef NewState, uint32_t timeout)
{
    DRV_I2C_StatusTypeDef status = DRV_I2C_OK;
    FlagStatusTypeDef     tmp_flag;

    tmp_flag = SET;
    if (timeout == 0)
    {
        while (tmp_flag != RESET)
        {
            // delay(1000000);
            // DRV_I2C_Stop(pi2c);
            _i2c_generate_stop(pi2c);
            DRV_I2C_GetFlag(pi2c, I2C_FLAG_INDEX_BUSY, &tmp_flag);
        }
    }
    else
    {
        while (tmp_flag != RESET)
        {
            delay(1000000);
            // DRV_I2C_Stop(pi2c);
            _i2c_generate_stop(pi2c);
            DRV_I2C_GetFlag(pi2c, I2C_FLAG_INDEX_BUSY, &tmp_flag);
            timeout--;
            if (timeout == 0)
            {
                status = DRV_I2C_TIMEOUT;
            }
        }
    }
    if (NewState == ENABLE)
    {
        _i2c_dis_highspeed(pi2c);
    }

    return (status);
}
DRV_I2C_StatusTypeDef DRV_I2C_MasterGenerateStopV2(I2C_TypeDef *pi2c, FunctionalStateTypeDef NewState, uint32_t timeout)
{
    DRV_I2C_StatusTypeDef status = DRV_I2C_OK;
    FlagStatusTypeDef     tmp_flag;

    tmp_flag = SET;
    _i2c_generate_stop(pi2c);
    if (timeout == 0)
    {
        while (tmp_flag != RESET)
        {
            delay(1000000);
            DRV_I2C_GetFlag(pi2c, I2C_FLAG_INDEX_BUSY, &tmp_flag);
        }
    }
    else
    {
        _i2c_generate_stop(pi2c);
        while (tmp_flag != RESET)
        {
            delay(1000000);

            DRV_I2C_GetFlag(pi2c, I2C_FLAG_INDEX_BUSY, &tmp_flag);
            timeout--;
            if (timeout == 0)
            {
                status = DRV_I2C_TIMEOUT;
            }
        }
    }
    if (NewState == ENABLE)
    {
        _i2c_dis_highspeed(pi2c);
    }

    return (status);
}
/**
 *@brief I2C模块主模式生成重复起始读条件.
 *
 *@param[in] pi2c     指向I2C_TypeDef结构体的指针;
 *@param[in] SlaveAdd 从机地址;
 *@param[in] timeou   超时时间;
 *@return 操作结果;@ref DRV_I2C_StatusTypeDef
 */
DRV_I2C_StatusTypeDef DRV_I2C_MasterRestartReadRequest(I2C_TypeDef *pi2c, uint8_t SlaveAdd, uint32_t timeout)
{
    DRV_I2C_StatusTypeDef status;

    /*产生起始条件:写请求最低位为1*/
    _i2c_repeat_start(pi2c);
    SlaveAdd |= 0x01;
    pi2c->DR = SlaveAdd;
    _i2c_reset_repeat_start(pi2c);

    /*查询标志*/
    status = DRV_I2C_WaitonFlagTimeout(pi2c, I2C_FLAG_INDEX_RC, SET, timeout);

    return (status);
}
/**
 *@brief I2C模块主模式高速写操作请求.
 *
 *@param[in] pi2c     指向I2C_TypeDef结构体的指针;
 *@param[in] SlaveAdd 从机地址;
 *@param[in] timeou   超时时间;
 *@return 操作结果;@ref DRV_I2C_StatusTypeDef
 */
DRV_I2C_StatusTypeDef DRV_I2C_MasterHighspeedWriteRequest(I2C_TypeDef *pi2c, uint8_t SLaveAdd, uint32_t timeout)
{
    DRV_I2C_StatusTypeDef status = DRV_I2C_OK;

    /**************************************************************/
    /*禁止高速模式*/
    _i2c_dis_highspeed(pi2c);
    _i2c_dis_clock_test_mode(pi2c);
    /**************************************************************/

    /**************************************************************/
    /*常速模式下发送主机码*/
    pi2c->DR = I2C_MASTER_CODE;
    _i2c_generate_start(pi2c);

    /*polling tf falg*/
    status = DRV_I2C_WaitonFlagTimeout(pi2c, I2C_FLAG_INDEX_TF, SET, timeout);

    if (status != DRV_I2C_OK)
    {
        return status;
    }
    else
    {
        ;
    }

    /**************************************************************/
    /*高速使能:发送写请求*/
    _i2c_en_clock_test_mode(pi2c);
    _i2c_en_highspeed(pi2c);

    _i2c_repeat_start(pi2c);
    SLaveAdd &= 0xfe;
    pi2c->DR = SLaveAdd;
    _i2c_reset_repeat_start(pi2c);

    if (DRV_I2C_WaitonFlagTimeout(pi2c, I2C_FLAG_INDEX_TF, SET, timeout) != DRV_I2C_OK)
    {
        return DRV_I2C_TIMEOUT;
    }
    else
    {
        ;
    }

    if (pi2c->SR & (1 << I2C_FLAG_INDEX_AACK))
    {
        return DRV_I2C_TIMEOUT;
    }
    else
    {
        ;
    }

    /**************************************************************/
    return DRV_I2C_OK;
}
/**
 *@brief 发送数据到总线.
 *
 *@param[in] pi2c     指向I2C_TypeDef结构体的指针;
 *@param[in] tosend   待发送数据;
 *@param[in] timeou   超时时间;
 *@return 操作结果;@ref DRV_I2C_StatusTypeDef
 */
DRV_I2C_StatusTypeDef DRV_I2C_TransmitByte(I2C_TypeDef *pi2c, uint8_t tosend, uint32_t timeout)
{
    DRV_I2C_StatusTypeDef status;

    pi2c->DR = tosend;
    status   = DRV_I2C_WaitonFlagTimeout(pi2c, I2C_FLAG_INDEX_TF, SET, timeout);

    return (status);
}
/**
 *@brief 读取总线数据.
 *
 *@param[in] pi2c     指向I2C_TypeDef结构体的指针;
 *@param[in] *Rx      接收数据指针;
 *@param[in] timeou   超时时间;
 *@return 操作结果;@ref DRV_I2C_StatusTypeDef
 */
DRV_I2C_StatusTypeDef DRV_I2C_ReceiveByte(I2C_TypeDef *pi2c, uint8_t *Rx, uint32_t timeout)
{
    DRV_I2C_StatusTypeDef status;

    status = DRV_I2C_WaitonFlagTimeout(pi2c, I2C_FLAG_INDEX_RC, SET, timeout);

    if (status == DRV_I2C_OK)
    {
        *Rx = pi2c->DR;
    }

    return (status);
}
/**
 *@brief 查询方式发送数据到设备.
 *
 *@param[in] pi2c      指向I2C_TypeDef结构体的指针;
 *@param[in] SlaveAdd  从机地址;
 *@param[in] Highspeed 是否高速模式;
 *- ENABLE;
 *- DISABLE;
 *@param[in] pBuf      待发送数据指针;
 *@param[in] Size      待发送数据长度;
 *@param[in] timeou    超时时间;
 *@return 操作结果;@ref DRV_I2C_StatusTypeDef
 */
DRV_I2C_StatusTypeDef DRV_I2C_Transmit(I2C_TypeDef           *pi2c,
                                       uint8_t                SlaveAdd,
                                       FunctionalStateTypeDef Highspeed,
                                       uint8_t               *pBuf,
                                       uint32_t               Size,
                                       uint32_t               timeout)
{
    uint32_t              loop = 0;
    DRV_I2C_StatusTypeDef status;

    /*是否是高速模式   */
    if (Highspeed == ENABLE)
    {
        if (DRV_I2C_MasterHighspeedWriteRequest(pi2c, SlaveAdd, timeout) != DRV_I2C_OK)
            return (status);
    }
    else
    {
        /*发送起始条件写操作*/
        if (DRV_I2C_MasterWriteRequest(pi2c, SlaveAdd, timeout) != DRV_I2C_OK)
            return (status);
    }

    /**************************************************************/
    /*发送数据*/
    for (loop = 0; loop < Size; loop++)
    {
        status = DRV_I2C_TransmitByte(pi2c, *(pBuf + loop), timeout);

        if (status != DRV_I2C_OK)
        {
            return (status);
        }
    }

/*发送停止条件*/
#if 1
    status = DRV_I2C_MasterGenerateStop(pi2c, Highspeed, timeout);
#else
    delay(1000000);
    _i2c_generate_stop(pi2c);
#endif
    return (status);
}
/**
 *@brief 查询方式从设备读取数据.
 *
 *@param[in] pi2c      指向I2C_TypeDef结构体的指针;
 *@param[in] SlaveAdd  从机地址;
 *@param[in] Highspeed 是否高速模式;
 *- ENABLE;
 *- DISABLE;
 *@param[in] pBuf      待接收数据指针;
 *@param[in] Size      待接收数据长度;
 *@param[in] timeout   超时时间;
 *@return 操作结果;@ref DRV_I2C_StatusTypeDef
 */
DRV_I2C_StatusTypeDef DRV_I2C_Receive(I2C_TypeDef           *pi2c,
                                      uint8_t                SlaveAdd,
                                      FunctionalStateTypeDef Highspeed,
                                      uint8_t               *pBuf,
                                      uint32_t               Size,
                                      uint32_t               timeout)
{
    uint32_t              loop = 0;
    DRV_I2C_StatusTypeDef status;

    /*发送起始条件读操作*/
    status = DRV_I2C_MasterReadRequest(pi2c, SlaveAdd, timeout);

    if (status != DRV_I2C_OK)
    {
        return (status);
    }

    /**************************************************************/
    /*读取数据*/
    if (Size == 1)
    {
        _i2c_nack(pi2c);

        status = DRV_I2C_WaitonFlagTimeout(pi2c, I2C_FLAG_INDEX_RC, SET, timeout);

        if (status != DRV_I2C_OK)
        {
            return (status);
        }

        *(pBuf) = pi2c->DR;
    }
    else
    {
        for (loop = 0; loop < Size - 1; loop++)
        {
            _i2c_ack(pi2c);
            status = DRV_I2C_WaitonFlagTimeout(pi2c, I2C_FLAG_INDEX_RC, SET, timeout);

            if (status != DRV_I2C_OK)
            {
                return (status);
            }

            *(pBuf + loop) = pi2c->DR;
        }

        /**************************************************************/
        /*最后一字节数据NACK*/
        _i2c_nack(pi2c);
        status = DRV_I2C_WaitonFlagTimeout(pi2c, I2C_FLAG_INDEX_RC, SET, timeout);

        if (status != DRV_I2C_OK)
        {
            return (status);
        }

        *(pBuf + loop) = pi2c->DR;
    }

    /*发送停止条件*/
    // status = DRV_I2C_MasterGenerateStop(pi2c, Highspeed, timeout);
    delay(1000000);
    _i2c_generate_stop(pi2c);
    return (status);
}
/**
 *@brief 中断方式发送数据到设备.
 *
 *@param[in] pi2c      指向I2C_TypeDef结构体的指针;
 *@param[in] SlaveAdd  从机地址;
 *@param[in] Highspeed 是否高速模式;
 *- ENABLE;
 *- DISABLE;
 *@param[in] pBuf      待发送数据指针;
 *@param[in] Size      待发送数据长度;
 *@return 操作结果;@ref DRV_I2C_StatusTypeDef
 *@warning ccm33xx_it.c实现的中断函数为HAL层的，如客户不使用HAL层则需实现自已的服务函数;
 */
DRV_I2C_StatusTypeDef
DRV_I2C_TransmitIT(I2C_TypeDef *pi2c, uint8_t SlaveAdd, FunctionalStateTypeDef Highspeed, uint8_t *pBuf, uint32_t Size)
{
    /*高速模式还是普通模式*/
    if (Highspeed == ENABLE)
    {
        /*先禁止高速模式以普通模式发送高速起始信号*/
        _i2c_dis_highspeed(pi2c);
        /*使能相应的中断*/
        _i2c_en_it(pi2c);
        pi2c->DR = I2C_MASTER_CODE;
        _i2c_generate_start(pi2c);
        /**/
    }

    else
    {
        /*使能相应的中断*/
        _i2c_en_it(pi2c);
        pi2c->DR = SlaveAdd;
        _i2c_generate_start(pi2c);
    }

    /**/
    return (DRV_I2C_OK);
}
/**
 *@brief 查询方式从设备读取数据.
 *
 *@param[in] pi2c      指向I2C_TypeDef结构体的指针;
 *@param[in] SlaveAdd  从机地址;
 *@param[in] Highspeed 是否高速模式;
 *- ENABLE;
 *- DISABLE;
 *@param[in] pBuf      待接收数据指针;
 *@param[in] Size      待接收数据长度;
 *@return 操作结果;@ref DRV_I2C_StatusTypeDef
 *@warning ccm33xx_it.c实现的中断函数为HAL层的，如客户不使用HAL层则需实现自已的服务函数;
 */
DRV_I2C_StatusTypeDef
DRV_I2C_ReceiveIT(I2C_TypeDef *pi2c, uint8_t SlaveAdd, FunctionalStateTypeDef Highspeed, uint8_t *pBuf, uint32_t Size)
{
    /*高速模式还是普通模式*/
    if (Highspeed == ENABLE)
    {
        /*先禁止高速模式以普通模式发送高速起始信号*/
        _i2c_dis_highspeed(pi2c);
        /*使能相应的中断*/
        _i2c_en_it(pi2c);
        pi2c->DR = I2C_MASTER_CODE;
        _i2c_generate_start(pi2c);
        /**/
    }

    else
    {
        /*使能相应的中断*/
        _i2c_en_it(pi2c);
        pi2c->DR = SlaveAdd | 0x01;
        _i2c_generate_start(pi2c);
    }

    /**/
    return (DRV_I2C_OK);
}
/**
 *@brief 查询方式写数据到存储器.
 *
 *@param[in] pi2c        指向I2C_TypeDef结构体的指针;
 *@param[in] SlaveAdd    从机地址;
 *@param[in] MemAdd      存储器地址;
 *@param[in] MemAddBytes 存储器地址长度，单位字节;
 *@param[in] Highspeed   是否高速模式;
 *- ENABLE;
 *- DISABLE;
 *@param[in] pBuf        待发送数据指针;
 *@param[in] Size        待发送数据长度;
 *@param[in] timeout     超时时间;
 *@return 操作结果;@ref DRV_I2C_StatusTypeDef
 *@warning 某些存储器可能不支持高速模式，此时Highspeed设为DISABLE;
 */
DRV_I2C_StatusTypeDef DRV_I2C_MemWrite(I2C_TypeDef           *pi2c,
                                       uint8_t                SlaveAdd,
                                       uint32_t               MemAdd,
                                       uint8_t                MemAddBytes,
                                       FunctionalStateTypeDef Highspeed,
                                       uint8_t               *pBuf,
                                       uint32_t               Size,
                                       uint32_t               timeout)
{
    uint32_t              loop = 0;
    DRV_I2C_StatusTypeDef status;
    uint8_t               AddBuf[4];

#if 1
    /*等到不忙*/
    status = DRV_I2C_WaitonFlagTimeout(pi2c, I2C_FLAG_INDEX_BUSY, RESET, timeout);

    if (status != DRV_I2C_OK)
    {
        return (status);
    }
#endif
    /*是否是高速模式   */
    if (Highspeed == ENABLE)
    {
        if (DRV_I2C_MasterHighspeedWriteRequest(pi2c, SlaveAdd, timeout) != DRV_I2C_OK)
            return (status);
    }
    else
    {
        /*发送起始条件写操作*/
        if (DRV_I2C_MasterWriteRequest(pi2c, SlaveAdd, timeout) != DRV_I2C_OK)
            return (status);
    }

    /*发送子地址*/
    AddBuf[0] = (uint8_t)(MemAdd);
    AddBuf[1] = (MemAdd >> 8) & 0XFF;
    AddBuf[2] = (MemAdd >> 16) & 0XFF;
    AddBuf[3] = (MemAdd >> 24) & 0XFF;

    if (MemAddBytes > 4)
        MemAddBytes = 4;

    for (loop = 0; loop < MemAddBytes; loop++)
    {
        status = DRV_I2C_TransmitByte(pi2c, *(AddBuf + loop), timeout);

        if (status != DRV_I2C_OK)
        {
            return (status);
        }
    }

    /**************************************************************/
    /*发送数据*/
    for (loop = 0; loop < Size; loop++)
    {
        status = DRV_I2C_TransmitByte(pi2c, *(pBuf + loop), timeout);

        if (status != DRV_I2C_OK)
        {
            return (status);
        }
    }

    /*发送停止条件*/
    status = DRV_I2C_MasterGenerateStop(pi2c, Highspeed, timeout);

    return (status);
}
/**
 *@brief 查询方式读取存储器数据.
 *
 *@param[in] pi2c        指向I2C_TypeDef结构体的指针;
 *@param[in] SlaveAdd    从机地址;
 *@param[in] MemAdd      存储器地址;
 *@param[in] MemAddBytes 存储器地址长度，单位字节;
 *@param[in] Highspeed   是否高速模式;
 *- ENABLE;
 *- DISABLE;
 *@param[in] pBuf        待接收数据指针;
 *@param[in] Size        待接收数据长度;
 *@param[in] timeout     超时时间;
 *@return 操作结果;@ref DRV_I2C_StatusTypeDef
 *@warning 某些存储器可能不支持高速模式，此时Highspeed设为DISABLE;
 */
DRV_I2C_StatusTypeDef DRV_I2C_MemRead(I2C_TypeDef           *pi2c,
                                      uint8_t                SlaveAdd,
                                      uint32_t               MemAdd,
                                      uint8_t                MemAddBytes,
                                      FunctionalStateTypeDef Highspeed,
                                      uint8_t               *pBuf,
                                      uint32_t               Size,
                                      uint32_t               timeout)
{
    uint32_t              loop = 0;
    uint8_t               AddBuf[4];
    DRV_I2C_StatusTypeDef status;

#if 1
    /*等到不忙*/
    status = DRV_I2C_WaitonFlagTimeout(pi2c, I2C_FLAG_INDEX_BUSY, RESET, timeout);

    if (status != DRV_I2C_OK)
    {
        return (status);
    }
#endif
    /*是否是高速模式   */
    if (Highspeed == ENABLE)
    {
        status = DRV_I2C_MasterHighspeedWriteRequest(pi2c, SlaveAdd, timeout);

        if (status != DRV_I2C_OK)
        {
            return (status);
        }
    }
    else
    {
        /*发送起始条件写操作*/
        status = DRV_I2C_MasterWriteRequest(pi2c, SlaveAdd, timeout);

        if (status != DRV_I2C_OK)
        {
            return (status);
        }
    }

    /*发送子地址*/
    AddBuf[0] = (uint8_t)(MemAdd);
    AddBuf[1] = (MemAdd >> 8) & 0XFF;
    AddBuf[2] = (MemAdd >> 8) & 0XFF;
    AddBuf[3] = (MemAdd >> 8) & 0XFF;

    if (MemAddBytes > 4)
        MemAddBytes = 4;

    for (loop = 0; loop < MemAddBytes; loop++)
    {
        status = DRV_I2C_TransmitByte(pi2c, *(AddBuf + loop), timeout);

        if (status != DRV_I2C_OK)
        {
            return (status);
        }
    }

/**************************************************************/
#if 0

    /*发送重复条件读请求*/
    if(iic_master_restart_read_request(hiic, slave_add, timeout) != HAL_OK)
    {
        hiic->lock = HAL_UNLOCKED;
        return HAL_TIMEOUT;
    }

#else

    /*发送停止条件*/
    status = DRV_I2C_MasterGenerateStop(pi2c, Highspeed, timeout);

    if (status != DRV_I2C_OK)
    {
        return (status);
    }

    status = DRV_I2C_MasterReadRequest(pi2c, SlaveAdd, timeout);

    if (status != DRV_I2C_OK)
    {
        return (status);
    }

#endif

    /**************************************************************/
    /*读取数据*/
    if (Size == 1)
    {
        _i2c_nack(pi2c);

        status = DRV_I2C_WaitonFlagTimeout(pi2c, I2C_FLAG_INDEX_RC, SET, timeout);

        if (status != DRV_I2C_OK)
        {
            return (status);
        }

        *(pBuf) = pi2c->DR;
    }
    else
    {
        /**************************************************************/
        for (loop = 0; loop < Size - 1; loop++)
        {
            _i2c_ack(pi2c);
            status = DRV_I2C_WaitonFlagTimeout(pi2c, I2C_FLAG_INDEX_RC, SET, timeout);

            if (status != DRV_I2C_OK)
            {
                return (status);
            }

            *(pBuf + loop) = pi2c->DR;
        }

        /**************************************************************/
        /*最后一字节数据NACK*/
        _i2c_nack(pi2c);
        status = DRV_I2C_WaitonFlagTimeout(pi2c, I2C_FLAG_INDEX_RC, SET, timeout);

        if (status != DRV_I2C_OK)
        {
            return (status);
        }

        *(pBuf + loop) = pi2c->DR;
    }

    /*发送停止条件*/
    status = DRV_I2C_MasterGenerateStop(pi2c, Highspeed, timeout);

    return (status);
}

DRV_I2C_StatusTypeDef DRV_I2C_SlaveTransmitReceive(I2C_TypeDef *pi2c, uint8_t *pBuf, uint32_t Size, uint32_t timeout)
{
    uint32_t         loop = 0;
    uint8_t          I2CStatus;
    uint8_t          I2CHighspeedStatus;
    volatile uint8_t tmp = 0;

    /**************************************************************/
    do
    {
        /*获取状态*/
        I2CStatus          = pi2c->SR;
        I2CHighspeedStatus = pi2c->SHIR;

        /**/
        if (I2CHighspeedStatus & I2C_FLAG_SLAVE_HIGH_SPEED)
        {
            /*write 1 to clear the flag*/
            _i2c_clr_flag_slave_high_speed(pi2c);
        }

        if (I2CStatus & (I2C_FLAG_RC | I2C_FLAG_TF))
        {
            if (I2CStatus & I2C_FLAG_AASLV)
            {
                /*地址匹配了*/
                if ((I2CStatus & I2C_FLAG_RXTX) != I2C_FLAG_RXTX)
                {
                    /**************************************************************/
                    /*处于接收模式:接收数据或者地址*/
                    if ((I2CStatus & I2C_FLAG_TF) != I2C_FLAG_TF)
                    {
                        /*接收到的是地址*/
                        /*应答主机同时清除RC标志*/
                        _i2c_ack(pi2c);
                    }
                    /**************************************************************/

                    /**************************************************************/
                    else
                    {
                        /*接收到的是数据*/
                        pBuf[loop] = pi2c->DR;
                        loop++;
                        /*应答主机同时清除RC标志*/
                        _i2c_ack(pi2c);
                    }

                    /**************************************************************/
                } /*接收模式*/

                /**************************************************************/
                else
                {
                    /*发送模式*/
                    pi2c->DR = pBuf[loop];
                    loop++;
                } /*发送模式*/

                /**************************************************************/
            } /*地址匹配*/
        }     /*RC/TF置位*/
    } while (loop < Size);

    return (DRV_I2C_OK);
}
/**
 *@brief 从机查询方式发送数据.
 *
 *@param[in] pi2c      指向I2C_TypeDef结构体的指针;
 *@param[in] pBuf      待发送数据指针;
 *@param[in] Size      待发送数据长度;
 *@param[in] timeou    超时时间;
 *@return 操作结果;@ref DRV_I2C_StatusTypeDef
 */
DRV_I2C_StatusTypeDef DRV_I2C_SlaveTransmit(I2C_TypeDef *pi2c, uint8_t *pBuf, uint32_t Size, uint32_t timeout)
{
    uint32_t         loop       = 0;
    uint32_t         TmpTimeout = timeout;
    uint8_t          I2CStatus;
    uint8_t          I2CHighspeedStatus;
    volatile uint8_t tmp = 0;

    /**************************************************************/
    do
    {
        /*获取中断状态*/
        I2CStatus          = pi2c->SR;
        I2CHighspeedStatus = pi2c->SHIR;

        /**/
        if (I2CHighspeedStatus & I2C_FLAG_SLAVE_HIGH_SPEED)
        {
            /*write 1 to clear the flag*/
            _i2c_clr_flag_slave_high_speed(pi2c);
        }

        if (I2CStatus & (I2C_FLAG_RC | I2C_FLAG_TF))
        {
            if (I2CStatus & I2C_FLAG_AASLV)
            {
                /*地址匹配了*/
                if ((I2CStatus & I2C_FLAG_RXTX) != I2C_FLAG_RXTX)
                {
                    /**************************************************************/
                    /*处于接收模式:接收数据或者地址*/
                    if ((I2CStatus & I2C_FLAG_TF) != I2C_FLAG_TF)
                    {
                        /*接收到的是地址*/
                        /*应答主机同时清除RC标志*/
                        _i2c_ack(pi2c);
                    }
                } /*接收模式*/
                /**************************************************************/
                else
                {
                    /*发送模式*/
                    pi2c->DR = pBuf[loop];
                    loop++;
                } /*发送模式*/

                /**************************************************************/
            } /*地址匹配*/
        }
        else
        {
            /*超时处理*/
            if (timeout)
            {
                TmpTimeout--;

                if (TmpTimeout == 0)
                {
                    return (DRV_I2C_TIMEOUT);
                }
            }
        }
    } while (loop < Size);

    return (DRV_I2C_OK);
}
/**
 *@brief 从机查询方式接收数据.
 *
 *@param[in] pi2c      指向I2C_TypeDef结构体的指针;
 *@param[in] pBuf      待接收数据指针;
 *@param[in] Size      待接收数据长度;
 *@param[in] timeou    超时时间;
 *@return 操作结果;@ref DRV_I2C_StatusTypeDef
 */
DRV_I2C_StatusTypeDef DRV_I2C_SlaveReceive(I2C_TypeDef *pi2c, uint8_t *pBuf, uint32_t Size, uint32_t timeout)
{
    uint32_t         loop = 0;
    uint8_t          I2CStatus;
    volatile uint8_t tmp         = 0;
    uint32_t         TempTimeout = timeout;
    uint8_t          I2CHighspeedStatus;

    /**************************************************************/
    do
    {
        /*获取中断状态*/
        I2CStatus          = pi2c->SR;
        I2CHighspeedStatus = pi2c->SHIR;

        if (I2CHighspeedStatus & I2C_FLAG_SLAVE_HIGH_SPEED)
        {
            /*write 1 to clear the flag*/
            _i2c_clr_flag_slave_high_speed(pi2c);
        }

        if (I2CStatus & (I2C_FLAG_RC | I2C_FLAG_TF))
        {
            TempTimeout = timeout;

            if (I2CStatus & I2C_FLAG_AASLV)
            {
                /*地址匹配了*/
                if ((I2CStatus & I2C_FLAG_RXTX) != I2C_FLAG_RXTX)
                {
                    /**************************************************************/
                    /*处于接收模式:接收数据或者地址*/
                    if ((I2CStatus & I2C_FLAG_TF) != I2C_FLAG_TF)
                    {
                        /*接收到的是地址*/
                        /*应答主机同时清除RC标志*/
                        _i2c_ack(pi2c);
                    }
                    /**************************************************************/

                    /**************************************************************/
                    else
                    {
                        /*接收到的是数据*/
                        pBuf[loop] = pi2c->DR;
                        loop++;
                        /*应答主机同时清除RC标志*/
                        _i2c_ack(pi2c);
                    }

                    /**************************************************************/
                } /*接收模式*/
            }     /*地址匹配*/
        }
        else
        {
            if (timeout)
            {
                TempTimeout--;

                if (TempTimeout == 0)
                {
                    return (DRV_I2C_TIMEOUT);
                }
            }
        }
    } while (loop < Size);

    return (DRV_I2C_OK);
}
/**
 *@brief 获取使能的中断源.
 *
 *@param[in] pi2c       指向I2C_TypeDef结构体的指针;
 *@param[out] pItSource 中断源指针;

 *@return 操作结果;@ref DRV_I2C_StatusTypeDef
 */
DRV_I2C_StatusTypeDef DRV_I2C_GetItSource(I2C_TypeDef *pi2c, uint8_t *pItSource)
{
    *pItSource = 0;

    if (_bit_get(pi2c->CCR, I2C_IT_INDEX_IEN))
    {
        *pItSource |= I2C_IT_INDEX_IEN;
    }

    if (_bit_get(pi2c->CCR, I2C_IT_INDEX_AMIE))
    {
        *pItSource |= I2C_IT_INDEX_AMIE;
    }

    if (_bit_get(pi2c->CCR, I2C_IT_INDEX_SLV_HSIE))
    {
        *pItSource |= I2C_IT_INDEX_SLV_HSIE;
    }

    return (DRV_I2C_OK);
}
/**
 *@brief 获取标志状态.
 *
 *@param[in] pi2c                指向I2C_TypeDef结构体的指针;
 *@param[out] pBasicFlags        基本状态指针;
 *@param[out] pSlvHighspeedFlags 高速状态指针;
 *@return 操作结果;@ref DRV_I2C_StatusTypeDef
 */
DRV_I2C_StatusTypeDef DRV_I2C_GetItFlags(I2C_TypeDef *pi2c, uint8_t *pBasicFlags, uint8_t *pSlvHighspeedFlags)
{
    *pBasicFlags        = pi2c->SR;
    *pSlvHighspeedFlags = pi2c->SHIR;

    return (DRV_I2C_OK);
}
/**
 *@brief 获取当前数据寄存器内的数据.
 *
 *@param[in] pi2c 指向I2C_TypeDef结构体的指针;
 *@return 无
 */
void DRV_I2C_GetDR(I2C_TypeDef *pi2c, uint8_t *pData)
{
    *pData = pi2c->DR;
}
/**
 *@brief 写数据寄存器数据.
 *
 *@param[in] pi2c 指向I2C_TypeDef结构体的指针;
 *@return 无
 */
void DRV_I2C_SetDR(I2C_TypeDef *pi2c, uint8_t Data)
{
    pi2c->DR = Data;
}
/**************************************************************/
/*GPIO*/
/**
 *@brief I2C管脚用作GPIO初始化.
 *
 *@param pi2c[in] pi2c 指向I2C_TypeDef结构体的指针;
 *@param Init[in] Init 初始化结构体指针;
 *@return DRV_I2C_StatusTypeDef @ref DRV_I2C_StatusTypeDef
 */
DRV_I2C_StatusTypeDef DRV_I2C_GpioInit(I2C_TypeDef *pi2c, GPIO_InitTypeDef *Init)
{
    /*用作GPIO功能*/
    _i2c_pin_gpio(pi2c, Init->Pin);

    /*方向*/
    if (Init->Dir == GPIO_DIR_OUT)
    {
        _i2c_pin_dir_output(pi2c, Init->Pin);

        if (Init->OutputMode == GPIO_OUTPUT_MODE_CMOS)
        {
            _i2c_pin_output_cmos(pi2c, Init->Pin);
        }
        else if (Init->OutputMode == GPIO_OUTPUT_MODE_CMOS)
        {
            _i2c_pin_output_opendrain(pi2c, Init->Pin);
        }
        else
        {
            return (DRV_I2C_ERROR);
        }
    }
    else if (Init->Dir == GPIO_DIR_IN)
    {
        _i2c_pin_dir_input(pi2c, Init->Pin);
    }
    else if (Init->Dir == GPIo_DIR_TRIGATE)
    {
        /*用作输主之后IOCONTROL部分禁止输入*/
        _i2c_pin_dir_input(pi2c, Init->Pin);
        /**/
    }

    /*上下拉*/
    if (Init->PullMode == GPIO_PULL_MODE_UP)
    {
        _i2c_pin_pullup_en(pi2c, Init->Pin);
        _i2c_pin_pulldown_dis(pi2c, Init->Pin);
    }
    else if (Init->PullMode == GPIO_PULL_MODE_UP)
    {
        _i2c_pin_pulldown_en(pi2c, Init->Pin);
        _i2c_pin_pullup_dis(pi2c, Init->Pin);
    }
    else
    {
        return (DRV_I2C_ERROR);
    }

    /**/
    return (DRV_I2C_OK);
}
/*GPIO*/
/**
 *@brief I2C管脚恢复初始设置.
 *
 *@param pi2c[in] pi2c 指向I2C_TypeDef结构体的指针;
 *@param Init[in] pin  I2C的管脚;
 *- I2C_PIN_SDA;
 *- I2C_PIN_SCL;
 *@return DRV_I2C_StatusTypeDef @ref DRV_I2C_StatusTypeDef
 */
DRV_I2C_StatusTypeDef DRV_I2C_GpioDeInit(I2C_TypeDef *pi2c, uint8_t pin)
{
    /*用作FUN功能*/
    _i2c_pin_primary_fun(pi2c, pin);
    _i2c_pin_dir_input(pi2c, pin);

    return (DRV_I2C_OK);
}
/**
 *@brief I2C管脚输出指定电平.
 *
 *@param pi2c[in] pi2c 指向I2C_TypeDef结构体的指针;
 *@param Init[in] pin  I2C的管脚;
 *- I2C_PIN_SDA;
 *- I2C_PIN_SCL;
 *@return 无
 */
void DRV_I2C_WritePin(I2C_TypeDef *pi2c, uint8_t pin, GPIO_PinStateTypeDef state)
{
    if (state == RESET)
    {
        _i2c_reset_pin_bit(pi2c, pin);
    }
    else
    {
        _i2c_set_pin_bit(pi2c, pin);
    }
}
/**
 *@brief 获取I2C管脚电平.
 *
 *@param pi2c[in] pi2c 指向I2C_TypeDef结构体的指针;
 *@param Init[in] pin  I2C的管脚;
 *- I2C_PIN_SDA;
 *- I2C_PIN_SCL;
 *@return 管脚电平状态 @ref @BitActionTypeDef
 */
BitActionTypeDef DRV_I2C_ReadPin(I2C_TypeDef *pi2c, uint8_t pin)
{
    if (_i2c_get_pin_bit(pi2c, pin))
    {
        return (BIT_SET);
    }
    else
    {
        return (BIT_RESET);
    }
}
/**
 *@brief I2C管脚电平翻转.
 *
 *@param pi2c[in] pi2c 指向I2C_TypeDef结构体的指针;
 *@param Init[in] pin  I2C的管脚;
 *- I2C_PIN_SDA;
 *- I2C_PIN_SCL;
 *@return 无
 */
void DRV_I2C_TogglePin(I2C_TypeDef *pi2c, uint8_t pin)
{
    pi2c->DDR &= ~(1 << pin);
}
