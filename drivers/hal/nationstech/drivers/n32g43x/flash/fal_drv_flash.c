#include "string.h"
#include <os_memory.h>
#include <oneos_config.h>
#include "dlog.h"

#include "drv_flash.h"

#if defined(OS_USING_FAL)
#include "fal.h"
#include "ports/flash_info.c"
#endif

static int n32_fal_flash_read(fal_flash_t *flash, uint32_t page_addr, uint8_t *buff, uint32_t page_nr)
{
    int count = n32_flash_read(N32_FLASH_START_ADDR + page_addr * N32_FLASH_PAGE_SIZE,
                                 buff,
                                 page_nr * N32_FLASH_PAGE_SIZE);

    return (count == page_nr * N32_FLASH_PAGE_SIZE) ? 0 : -1;
}

static int n32_fal_flash_write(fal_flash_t *flash, uint32_t page_addr, const uint8_t *buff, uint32_t page_nr)
{
    int count = n32_flash_write(N32_FLASH_START_ADDR + page_addr * N32_FLASH_PAGE_SIZE,
                                  buff,
                                  page_nr * N32_FLASH_PAGE_SIZE);

    return (count == page_nr * N32_FLASH_PAGE_SIZE) ? 0 : -1;
}

static int n32_fal_flash_erase(fal_flash_t *flash, uint32_t page_addr, uint32_t page_nr)
{
    int count = n32_flash_erase(N32_FLASH_START_ADDR + page_addr * N32_FLASH_PAGE_SIZE,
                                  page_nr * N32_FLASH_PAGE_SIZE);

    return (count == page_nr * N32_FLASH_PAGE_SIZE) ? 0 : -1;
}

static int n32_flash_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    fal_flash_t *fal_flash = os_calloc(1, sizeof(fal_flash_t));

    if (fal_flash == OS_NULL)
    {
        os_kprintf("fal flash mem leak %s.\r\n", dev->name);
        return -1;
    }

    struct onchip_flash_info *flash_info = (struct onchip_flash_info *)dev->info;

    memcpy(fal_flash->name, dev->name, min(FAL_DEV_NAME_MAX - 1, strlen(dev->name)));

    fal_flash->name[min(FAL_DEV_NAME_MAX - 1, strlen(dev->name))] = 0;

    fal_flash->capacity   = flash_info->capacity;
    fal_flash->block_size = flash_info->block_size;
    fal_flash->page_size  = flash_info->page_size;

    fal_flash->ops.read_page   = n32_fal_flash_read;
    fal_flash->ops.write_page  = n32_fal_flash_write;
    fal_flash->ops.erase_block = n32_fal_flash_erase;

    fal_flash->priv = flash_info;

    return fal_flash_register(fal_flash);
}

OS_DRIVER_INFO n32_flash_driver = {
    .name  = "N32G43X_Onchip_Flash",
    .probe = n32_flash_probe,
};

OS_DRIVER_DEFINE(n32_flash_driver, OS_INIT_LEVEL_PRE_DEVICE, OS_INIT_SUBLEVEL_HIGH);
