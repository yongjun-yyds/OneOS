/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_usart.c
 *
 * @brief       This file implements usart driver for apm32
 *
 * @revision
 * Date         Author          Notes
 * 2022-01-10   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <os_memory.h>
#include <bus/bus.h>
#include <dma/dma.h>
#include <board.h>
#include <string.h>
#include <os_list.h>

#define DRV_EXT_LVL DBG_EXT_INFO
#define DRV_EXT_TAG "drv.usart"
#include <drv_log.h>

#include "drv_usart.h"

typedef struct n32_usart
{
    struct os_serial_device  serial;
    
    struct n32_usart_info *info;

    soft_dma_t  sdma;
    uint32_t sdma_hard_size;

    DMA_InitType DMA_InitStructure;

    uint8_t *rx_buff;
    uint32_t rx_index;
    uint32_t rx_size;

    const uint8_t *tx_buff;
    uint32_t       tx_count;
    uint32_t       tx_size;
	
    os_list_node_t list;
} n32_usart_t;

static os_list_node_t n32_usart_list = OS_LIST_INIT(n32_usart_list);

static const struct n32_usart_info *console_uart = OS_NULL;

static void n32_usart_interrupt_rx(n32_usart_t *uart)
{
    if (USART_GetIntStatus(uart->info->huart, USART_INT_RXDNE) != RESET)
    {
        OS_ASSERT(uart->rx_buff != OS_NULL);
        OS_ASSERT(uart->rx_index < uart->rx_size);

        USART_ClrIntPendingBit(uart->info->huart, USART_INT_RXDNE);
        USART_ConfigInt(uart->info->huart, USART_INT_IDLEF, ENABLE);

        uart->rx_buff[uart->rx_index++] = USART_ReceiveData(uart->info->huart);
    }

    if (USART_GetIntStatus(uart->info->huart, USART_INT_IDLEF) != RESET)
    {
        USART_ClrIntPendingBit(uart->info->huart, USART_INT_IDLEF);
        USART_ReceiveData(uart->info->huart);

        if (uart->rx_index > 0)
        {
            soft_dma_timeout_irq(&uart->sdma);
        }
    }

    if (uart->rx_index == (uart->rx_size / 2))
    {
        soft_dma_half_irq(&uart->sdma);
    }

    if (uart->rx_index == uart->rx_size)
    {
        uart->rx_index = 0;
        soft_dma_full_irq(&uart->sdma);
    }
}


static void n32_usart_dma_rx(n32_usart_t *uart)
{
    if (USART_GetIntStatus(uart->info->huart, USART_INT_RXDNE) != RESET)
    {
        USART_ClrIntPendingBit(uart->info->huart, USART_INT_RXDNE);
        USART_ReceiveData(uart->info->huart);
    }

    if (USART_GetIntStatus(uart->info->huart, USART_INT_IDLEF) != RESET)
    {
        USART_ClrIntPendingBit(uart->info->huart, USART_INT_IDLEF);
        USART_ReceiveData(uart->info->huart);

        soft_dma_timeout_irq(&uart->sdma);
    }
}

static void n32_usart_irq_callback(n32_usart_t *uart)
{
    /* rx */
    if (uart->info->dma_channel == OS_NULL)
    {
        n32_usart_interrupt_rx(uart);
    }
    else
    {
        n32_usart_dma_rx(uart);
    }

    /* tx */
    if (USART_GetIntStatus(uart->info->huart, USART_INT_TXDE) != RESET)
    {
        USART_ClrIntPendingBit(uart->info->huart, USART_INT_TXDE);
		
        if (uart->tx_size > 0)
        {
			if (uart->tx_count == 0)
			{
				if (uart->info->send_start_hook)
				{
					uart->info->send_start_hook();
				}
			}
			
            if (uart->tx_count < uart->tx_size)
                USART_SendData(uart->info->huart, uart->tx_buff[uart->tx_count++]);

            if (uart->tx_count >= uart->tx_size)
            {
                uart->tx_size = 0;
                USART_ConfigInt(uart->info->huart, USART_INT_TXDE, DISABLE);
                os_hw_serial_isr_txdone((struct os_serial_device *)uart);
				
				USART_ConfigInt(uart->info->huart, USART_INT_TXC, ENABLE);
            }
        }
    }
	
	if (USART_GetIntStatus(uart->info->huart, USART_INT_TXC) != RESET)
	{
		USART_ClrIntPendingBit(uart->info->huart, USART_INT_TXC);
		USART_ConfigInt(uart->info->huart, USART_INT_TXC, DISABLE);
		
		if (uart->info->send_end_hook)
		{
			uart->info->send_end_hook();
		}
	}
}

static void usart_irqhandler(USART_Module *huart)
{
    n32_usart_t *uart;

    os_list_for_each_entry(uart, &n32_usart_list, n32_usart_t, list)
    {
        if (uart->info->huart == huart)
        {
            n32_usart_irq_callback(uart);
            return;
        }
    }
}

static void usart_dma_irqhandler(USART_Module *huart)
{
    n32_usart_t *uart;

    os_list_for_each_entry(uart, &n32_usart_list, n32_usart_t, list)
    {
        if (uart->info->huart == huart)
        {
            soft_dma_half_irq(&uart->sdma);
            return;
        }
    }
}

void USART1_IRQHandler(void)
{
#ifdef USART1
    usart_irqhandler(USART1);
#endif
}

void USART2_IRQHandler(void)
{
#ifdef USART2
    usart_irqhandler(USART2);
#endif
}

void USART3_IRQHandler(void)
{
#ifdef USART3
    usart_irqhandler(USART3);
#endif
}

void DMA_Channel5_IRQHandler(void)
{
    DMA_ClrIntPendingBit(DMA_INT_HTX5, DMA);
    DMA_ClrIntPendingBit(DMA_INT_TXC5, DMA);

#ifdef USART1
    usart_dma_irqhandler(USART1);
#endif
}

void DMA_Channel6_IRQHandler(void)
{
    DMA_ClrIntPendingBit(DMA_INT_HTX6, DMA);
    DMA_ClrIntPendingBit(DMA_INT_TXC6, DMA);

#ifdef USART2
    usart_dma_irqhandler(USART2);
#endif
}

void DMA_Channel3_IRQHandler(void)
{
    DMA_ClrIntPendingBit(DMA_INT_HTX3, DMA);
    DMA_ClrIntPendingBit(DMA_INT_TXC3, DMA);

#ifdef USART3
    usart_dma_irqhandler(USART3);
#endif
}

/* interrupt rx mode */
static uint32_t n32_sdma_int_get_index(soft_dma_t *dma)
{
    n32_usart_t *uart = os_container_of(dma, n32_usart_t, sdma);

    return uart->rx_index;
}

static os_err_t n32_sdma_int_start(soft_dma_t *dma, void *buff, uint32_t size)
{
    n32_usart_t *uart = os_container_of(dma, n32_usart_t, sdma);

    uart->rx_buff  = buff;
    uart->rx_index = 0;
    uart->rx_size  = size;

    USART_ConfigInt(uart->info->huart, USART_INT_RXDNE, ENABLE);

    return OS_SUCCESS;
}

static uint32_t n32_sdma_int_stop(soft_dma_t *dma)
{
    n32_usart_t *uart = os_container_of(dma, n32_usart_t, sdma);

    USART_ConfigInt(uart->info->huart, USART_INT_RXDNE, DISABLE);
    USART_ConfigInt(uart->info->huart, USART_INT_IDLEF, DISABLE);

    return n32_sdma_int_get_index(dma);
}

/* dma rx mode */
static uint32_t n32_sdma_dma_get_index(soft_dma_t *dma)
{
    n32_usart_t *uart = os_container_of(dma, n32_usart_t, sdma);

    return uart->sdma_hard_size - DMA_GetCurrDataCounter(uart->info->dma_channel);
}

static os_err_t n32_sdma_dma_init(soft_dma_t *dma)
{
    n32_usart_t *uart = os_container_of(dma, n32_usart_t, sdma);

    RCC_EnableAHBPeriphClk(uart->info->dma_rcc, ENABLE);

    uart->DMA_InitStructure.PeriphAddr     = (uint32_t)&uart->info->huart->DAT;
    uart->DMA_InitStructure.Direction      = DMA_DIR_PERIPH_SRC;
    uart->DMA_InitStructure.PeriphInc      = DMA_PERIPH_INC_DISABLE;
    uart->DMA_InitStructure.DMA_MemoryInc  = DMA_MEM_INC_ENABLE;
    uart->DMA_InitStructure.PeriphDataSize = DMA_PERIPH_DATA_SIZE_BYTE;
    uart->DMA_InitStructure.MemDataSize    = DMA_MemoryDataSize_Byte;
    uart->DMA_InitStructure.CircularMode   = DMA_MODE_CIRCULAR;
    uart->DMA_InitStructure.Priority       = DMA_PRIORITY_HIGH;
    uart->DMA_InitStructure.Mem2Mem        = DMA_M2M_DISABLE;

    NVIC_InitType NVIC_InitStructure;
    NVIC_InitStructure.NVIC_IRQChannel                   = uart->info->dma_irq;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 3;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority        = 3;
    NVIC_InitStructure.NVIC_IRQChannelCmd                = ENABLE;
    NVIC_Init(&NVIC_InitStructure);

    return OS_SUCCESS;
}

static os_err_t n32_sdma_dma_start(soft_dma_t *dma, void *buff, uint32_t size)
{
    n32_usart_t *uart = os_container_of(dma, n32_usart_t, sdma);

    uart->sdma_hard_size = size;

    uart->DMA_InitStructure.MemAddr = (uint32_t)buff;
    uart->DMA_InitStructure.BufSize = size;

    DMA_Init(uart->info->dma_channel, &uart->DMA_InitStructure);
    DMA_EnableChannel(uart->info->dma_channel, ENABLE);
    USART_EnableDMA(uart->info->huart, USART_DMAREQ_RX, ENABLE);

    USART_ConfigInt(uart->info->huart, USART_INT_IDLEF, ENABLE);
    USART_ConfigInt(uart->info->huart, USART_INT_RXDNE, ENABLE);

    DMA_ConfigInt(uart->info->dma_channel, DMA_INT_TXC | DMA_INT_HTX, ENABLE);

    return OS_SUCCESS;
}

static uint32_t n32_sdma_dma_stop(soft_dma_t *dma)
{
    n32_usart_t *uart = os_container_of(dma, n32_usart_t, sdma);

    DMA_EnableChannel(uart->info->dma_channel, DISABLE);

    return n32_sdma_dma_get_index(dma);
}

/* sdma callback */
static void n32_usart_sdma_callback(soft_dma_t *dma)
{
    n32_usart_t *uart = os_container_of(dma, n32_usart_t, sdma);

    os_hw_serial_isr_rxdone((struct os_serial_device *)uart);
}

static void n32_usart_sdma_init(struct n32_usart *uart, dma_ring_t *ring)
{
    soft_dma_t *dma = &uart->sdma;

    soft_dma_stop(dma);

    memset(&dma->hard_info, 0, sizeof(dma->hard_info));

    dma->hard_info.mode         = HARD_DMA_MODE_CIRCULAR;
    dma->hard_info.max_size     = 64 * 1024;
    dma->hard_info.flag         = HARD_DMA_FLAG_HALF_IRQ | HARD_DMA_FLAG_FULL_IRQ | HARD_DMA_FLAG_TIMEOUT_IRQ;
    dma->hard_info.data_timeout = uart_calc_byte_timeout_us(uart->serial.config.baud_rate);

    if (uart->info->dma_channel == OS_NULL)
    {
        dma->ops.get_index = n32_sdma_int_get_index;
        dma->ops.dma_init  = OS_NULL;
        dma->ops.dma_start = n32_sdma_int_start;
        dma->ops.dma_stop  = n32_sdma_int_stop;
    }
    else
    {
        dma->ops.get_index = n32_sdma_dma_get_index;
        dma->ops.dma_init  = n32_sdma_dma_init;
        dma->ops.dma_start = n32_sdma_dma_start;
        dma->ops.dma_stop  = n32_sdma_dma_stop;
    }

    dma->cbs.dma_half_callback    = n32_usart_sdma_callback;
    dma->cbs.dma_full_callback    = n32_usart_sdma_callback;
    dma->cbs.dma_timeout_callback = n32_usart_sdma_callback;

    soft_dma_init(dma);
    soft_dma_start(dma, ring);
    soft_dma_irq_enable(&uart->sdma, OS_TRUE);
}

static int __n32_usart_init(const struct n32_usart_info *uart_info, struct serial_configure *cfg)
{
    GPIO_InitType  GPIO_InitStructure;
    USART_InitType USART_InitStructure;
    uint32_t    data_bits;

    RCC_EnableAPB2PeriphClk(uart_info->tx_rcc, ENABLE);
    RCC_EnableAPB2PeriphClk(uart_info->rx_rcc, ENABLE);

    GPIO_InitStructure.GPIO_Slew_Rate = GPIO_Slew_Rate_High;
    GPIO_InitStructure.Pin        = uart_info->tx_pin;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF_PP;
    GPIO_InitPeripheral(uart_info->tx_port, &GPIO_InitStructure);

    GPIO_InitStructure.Pin  = uart_info->rx_pin;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Input;
    GPIO_InitPeripheral(uart_info->rx_port, &GPIO_InitStructure);
	
    USART_InitStructure.BaudRate            = cfg->baud_rate;
    USART_InitStructure.HardwareFlowControl = USART_HFCTRL_NONE;
    USART_InitStructure.Mode                = USART_MODE_RX | USART_MODE_TX;
	
    switch (cfg->stop_bits)
    {
    case STOP_BITS_1:
        USART_InitStructure.StopBits = USART_STPB_1;
        break;
    case STOP_BITS_2:
        USART_InitStructure.StopBits = USART_STPB_2;
        break;
    default:
        return OS_INVAL;
    }
    switch (cfg->parity)
    {
    case PARITY_NONE:
        USART_InitStructure.Parity = USART_PE_NO;
        data_bits                        = cfg->data_bits;
        break;
    case PARITY_ODD:
        USART_InitStructure.Parity = USART_PE_ODD;
        data_bits                        = cfg->data_bits + 1;
        break;
    case PARITY_EVEN:
        USART_InitStructure.Parity = USART_PE_EVEN;
        data_bits                        = cfg->data_bits + 1;
        break;
    default:
        return OS_INVAL;
    }

    switch (data_bits)
    {
    case DATA_BITS_8:
        USART_InitStructure.WordLength = USART_WL_8B;
        break;
    case DATA_BITS_9:
        USART_InitStructure.WordLength = USART_WL_9B;
        break;
    default:
        return OS_INVAL;
    }

    switch(uart_info->rcc_type)
    {
    case 1:
        RCC_EnableAPB1PeriphClk(uart_info->rcc, ENABLE);
        break;
    case 2:
        RCC_EnableAPB2PeriphClk(uart_info->rcc, ENABLE);
        break;
    }   

    USART_DeInit(uart_info->huart);
    USART_Init(uart_info->huart, &USART_InitStructure);
    USART_ConfigInt(uart_info->huart, USART_INT_IDLEF, DISABLE);
    USART_ConfigInt(uart_info->huart, USART_INT_RXDNE, DISABLE);
    USART_Enable(uart_info->huart, ENABLE);

    return OS_SUCCESS;
}

static os_err_t n32_usart_init(struct os_serial_device *serial, struct serial_configure *cfg)
{
    struct n32_usart *uart;

    OS_ASSERT(serial != OS_NULL);
    OS_ASSERT(cfg != OS_NULL);

    uart = os_container_of(serial, struct n32_usart, serial);

    __n32_usart_init(uart->info, cfg);

    NVIC_InitType NVIC_InitStructure;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority        = 3;
    NVIC_InitStructure.NVIC_IRQChannel                   = uart->info->irq;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 3;
    NVIC_InitStructure.NVIC_IRQChannelCmd                = ENABLE;
    NVIC_Init(&NVIC_InitStructure);

    n32_usart_sdma_init(uart, &serial->rx_fifo->ring);

    return OS_SUCCESS;
}

static os_err_t n32_usart_deinit(struct os_serial_device *serial)
{
    struct n32_usart *uart;

    OS_ASSERT(serial != OS_NULL);

    uart = os_container_of(serial, struct n32_usart, serial);

    /* rx */
    USART_ConfigInt(uart->info->huart, USART_INT_IDLEF, DISABLE);
    USART_ConfigInt(uart->info->huart, USART_INT_RXDNE, DISABLE);

    if (uart->info->dma_channel != OS_NULL)
    {
        soft_dma_stop(&uart->sdma);
        DMA_DeInit(uart->info->dma_channel);
    }

    /* tx */
    USART_ConfigInt(uart->info->huart, USART_INT_TXDE, DISABLE);

    uart->tx_buff  = OS_NULL;
    uart->tx_count = 0;
    uart->tx_size  = 0;

    return 0;
}

static int n32_uart_start_send(struct os_serial_device *serial, const uint8_t *buff, os_size_t size)
{
    struct n32_usart *uart;

    OS_ASSERT(serial != OS_NULL);

    uart = os_container_of(serial, struct n32_usart, serial);

    uart->tx_buff  = buff;
    uart->tx_count = 0;
    uart->tx_size  = size;

    USART_ConfigInt(uart->info->huart, USART_INT_TXDE, ENABLE);

    return size;
}

/* clang-format off */
static int n32_usart_poll_send(struct os_serial_device *serial, const uint8_t *buff, os_size_t size)
{
    int                i;
    struct n32_usart *uart;

    OS_ASSERT(serial != OS_NULL);

    uart = os_container_of(serial, struct n32_usart, serial);

    for (i = 0; i < size; i++)
    {
        while (USART_GetFlagStatus(uart->info->huart, USART_FLAG_TXC) == RESET);
        USART_SendData(uart->info->huart, buff[i]);
    }

    return size;
}
/* clang-format on */

static const struct os_uart_ops n32_usart_ops = {
    .init   = n32_usart_init,
    .deinit = n32_usart_deinit,

    .start_send = n32_uart_start_send,
    .poll_send  = n32_usart_poll_send,
};

static int n32_usart_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    struct serial_configure config = OS_SERIAL_CONFIG_DEFAULT;

    os_err_t  result = 0;
    os_base_t level;

    struct n32_usart *uart = os_calloc(1, sizeof(struct n32_usart));

    OS_ASSERT(uart);

    uart->info = (void*)dev->info;

    struct os_serial_device *serial = &uart->serial;

    serial->ops    = &n32_usart_ops;
    serial->config = config;

    level = os_irq_lock();
    os_list_add_tail(&n32_usart_list, &uart->list);
    os_irq_unlock(level);

    result = os_hw_serial_register(serial, dev->name, NULL);

    OS_ASSERT(result == OS_EOK);
	
	os_kprintf(DRV_EXT_TAG ": %s init done\r\n", dev->name);

    return result;
}

/* clang-format off */
void __os_hw_console_output(char *str)
{
    int i;

    if (console_uart == OS_NULL)
        return;

    for (i = 0; i < strlen(str); i++)
    {
        while (USART_GetFlagStatus(console_uart->huart, USART_FLAG_TXC) == RESET);
        USART_SendData(console_uart->huart, str[i]);
    }
}
/* clang-format on */

OS_DRIVER_INFO n32_usart_driver = {
    .name  = "USART_Module",
    .probe = n32_usart_probe,
};

OS_DRIVER_DEFINE(n32_usart_driver, OS_INIT_LEVEL_PRE_DEVICE, OS_INIT_SUBLEVEL_HIGH);

static int n32_usart_early_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    if (strcmp(dev->name, OS_CONSOLE_DEVICE_NAME))
        return OS_SUCCESS;

    console_uart = (const struct n32_usart_info *)dev->info;

    struct serial_configure config = OS_SERIAL_CONFIG_DEFAULT;

    __n32_usart_init(console_uart, &config);

    return OS_SUCCESS;
}

OS_DRIVER_INFO n32_usart_early_driver = {
    .name  = "USART_Module",
    .probe = n32_usart_early_probe,
};

OS_DRIVER_DEFINE(n32_usart_early_driver, OS_INIT_LEVEL_PRE_KERNEL_1, OS_INIT_SUBLEVEL_LOW);
