/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_gpio.c
 *
 * @brief       This file implements gpio driver for apm32.
 *
 * @revision
 * Date         Author          Notes
 * 2022-01-17   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <board.h>

#define DRV_EXT_LVL DBG_EXT_INFO
#define DRV_EXT_TAG "drv.gpio"
#include <drv_log.h>

#include "drv_gpio.h"

#ifdef SERIES_N32G43X
#include "n32g43x_conf.h"
#endif

#ifdef SERIES_N32G45X
#include "n32g45x_hal.h"
#endif

#ifdef SERIES_N32L40X
#include "n32l40x_hal.h"
#endif

#define N32_PORT(pin) ((pin) >> 4)
#define N32_PIN(pin)  ((pin) & 0x0F)

struct n32_pin_irq
{
    uint16_t pinbit;
    IRQn_Type   irqno;
};

struct n32_gpio
{
    GPIO_Module *base;
    uint32_t     rcc;
};

static const struct n32_gpio n32_gpio_table[] =
{
#ifdef GPIOA
    {GPIOA, RCC_APB2_PERIPH_GPIOA},
#ifdef GPIOB
    {GPIOB, RCC_APB2_PERIPH_GPIOB},
#ifdef GPIOC
    {GPIOC, RCC_APB2_PERIPH_GPIOC},
#ifdef GPIOD
    {GPIOD, RCC_APB2_PERIPH_GPIOD},
#ifdef GPIOE
    {GPIOE, RCC_APB2_PERIPH_GPIOE},
#ifdef GPIOF
    {GPIOF, RCC_APB2_PERIPH_GPIOF},
#ifdef GPIOG
    {GPIOG, RCC_APB2_PERIPH_GPIOG},
#endif /* GPIOG */
#endif /* GPIOF */
#endif /* GPIOE */
#endif /* GPIOD */
#endif /* GPIOC */
#endif /* GPIOB */
#endif /* GPIOA */
};

static const struct n32_pin_irq n32_pin_irq[] =
{
    {GPIO_PIN_0,  EXTI0_IRQn},
    {GPIO_PIN_1,  EXTI1_IRQn},
    {GPIO_PIN_2,  EXTI2_IRQn},
    {GPIO_PIN_3,  EXTI3_IRQn},
    {GPIO_PIN_4,  EXTI4_IRQn},
    {GPIO_PIN_5,  EXTI9_5_IRQn},
    {GPIO_PIN_6,  EXTI9_5_IRQn},
    {GPIO_PIN_7,  EXTI9_5_IRQn},
    {GPIO_PIN_8,  EXTI9_5_IRQn},
    {GPIO_PIN_9,  EXTI9_5_IRQn},
    {GPIO_PIN_10, EXTI15_10_IRQn},
    {GPIO_PIN_11, EXTI15_10_IRQn},
    {GPIO_PIN_12, EXTI15_10_IRQn},
    {GPIO_PIN_13, EXTI15_10_IRQn},
    {GPIO_PIN_14, EXTI15_10_IRQn},
    {GPIO_PIN_15, EXTI15_10_IRQn},
};

static struct os_pin_irq_hdr n32_pin_irq_hdr_tab[] = {
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
};

static void n32_pin_write(os_device_t *dev, os_base_t pin, os_base_t value)
{
    int Port = N32_PORT(pin);
    int Pin  = N32_PIN(pin);

    OS_ASSERT(Port < ARRAY_SIZE(n32_gpio_table));

    const struct n32_gpio *gpio = &n32_gpio_table[Port];

    if (value)
    {   // set
        gpio->base->PBSC = (1<<Pin);
    }
    else
    {   // reset
        gpio->base->PBC = (1<<Pin);
    }
}

static int n32_pin_read(os_device_t *dev, os_base_t pin)
{
    int Port = N32_PORT(pin);
    int Pin  = N32_PIN(pin);

    OS_ASSERT(Port < ARRAY_SIZE(n32_gpio_table));

    const struct n32_gpio *gpio = &n32_gpio_table[Port];
    const uint32_t mask = 1 << Pin;

    return (gpio->base->PID & mask) == mask;
}

static void n32_pin_mode(os_device_t *dev, os_base_t pin, os_base_t mode)
{
    int Port = N32_PORT(pin);
    int Pin  = N32_PIN(pin);

    OS_ASSERT(Port < ARRAY_SIZE(n32_gpio_table));

    const struct n32_gpio *gpio = &n32_gpio_table[Port];

    GPIO_InitType init;
    init.Pin = (1<<Pin);
    init.GPIO_Mode = GPIO_Mode_Input;
    
	switch(mode)
	{
    case PIN_MODE_OUTPUT:
        init.GPIO_Mode = GPIO_Mode_Out_PP;
		init.GPIO_Slew_Rate = GPIO_Slew_Rate_High;
		init.GPIO_Current = GPIO_DC_12mA;
        break;

    case PIN_MODE_INPUT:
        init.GPIO_Mode = GPIO_Mode_Input;
		init.GPIO_Pull = GPIO_No_Pull;
        break;

    case PIN_MODE_INPUT_PULLUP:
        init.GPIO_Mode = GPIO_Mode_Input;
		init.GPIO_Pull = GPIO_Pull_Up;
        break;

    case PIN_MODE_INPUT_PULLDOWN:
        init.GPIO_Mode = GPIO_Mode_Input;
		init.GPIO_Pull = GPIO_Pull_Down;
        break;

    case PIN_MODE_OUTPUT_OD:
        init.GPIO_Mode = GPIO_Mode_Out_OD;
		init.GPIO_Slew_Rate = GPIO_Slew_Rate_High;
		init.GPIO_Current = GPIO_DC_12mA;
        break;

    default:
        init.GPIO_Mode = GPIO_Mode_Input;
        break;
	}

    RCC_EnableAPB2PeriphClk(gpio->rcc, ENABLE);
    GPIO_InitPeripheral(gpio->base, &init);
}

static os_err_t n32_pin_attach_irq(struct os_device *device, int32_t pin, uint32_t mode, void (*hdr)(void *args), void *args)
{
    os_base_t level;

    int Pin = N32_PIN(pin);
    OS_ASSERT(Pin < ARRAY_SIZE(n32_pin_irq));

    level = os_irq_lock();

    if (n32_pin_irq_hdr_tab[Pin].pin == pin
        && n32_pin_irq_hdr_tab[Pin].hdr == hdr
        && n32_pin_irq_hdr_tab[Pin].mode == mode
        && n32_pin_irq_hdr_tab[Pin].args == args)
    {
        os_irq_unlock(level);
        return OS_SUCCESS;
    }

    if (n32_pin_irq_hdr_tab[Pin].pin != -1)
    {
        os_irq_unlock(level);
        return OS_BUSY;
    }

    n32_pin_irq_hdr_tab[Pin].pin  = pin;
    n32_pin_irq_hdr_tab[Pin].hdr  = hdr;
    n32_pin_irq_hdr_tab[Pin].mode = mode;
    n32_pin_irq_hdr_tab[Pin].args = args;

    os_irq_unlock(level);

    return OS_SUCCESS;
}

static os_err_t n32_pin_detach_irq(struct os_device *device, int32_t pin)
{
    os_base_t level;

    int Pin = N32_PIN(pin);

    OS_ASSERT(Pin < ARRAY_SIZE(n32_pin_irq));

    level = os_irq_lock();

    if (n32_pin_irq_hdr_tab[Pin].pin == -1)
    {
        os_irq_unlock(level);
        return OS_INVAL;
    }

    n32_pin_irq_hdr_tab[Pin].pin  = -1;
    n32_pin_irq_hdr_tab[Pin].hdr  = OS_NULL;
    n32_pin_irq_hdr_tab[Pin].mode = 0;
    n32_pin_irq_hdr_tab[Pin].args = OS_NULL;

    os_irq_unlock(level);

    return OS_SUCCESS;
}

static os_err_t n32_pin_irq_enable(struct os_device *device, os_base_t pin, uint32_t enabled)
{
    os_base_t                  level;
    const struct n32_pin_irq *irqmap;

    int Port = N32_PORT(pin);
    int Pin  = N32_PIN(pin);

    OS_ASSERT(Port < ARRAY_SIZE(n32_gpio_table));
    OS_ASSERT(Pin < ARRAY_SIZE(n32_pin_irq));

    if (enabled == PIN_IRQ_ENABLE)
    {
        level = os_irq_lock();

        if (n32_pin_irq_hdr_tab[Pin].pin == -1)
        {
            os_irq_unlock(level);
            return OS_ENOSYS;
        }

        irqmap = &n32_pin_irq[Pin];

        EXTI_InitType EXTI_InitStructure;
        EXTI_InitStructure.EXTI_Line    = irqmap->pinbit;
        EXTI_InitStructure.EXTI_Mode    = EXTI_Mode_Interrupt;
        EXTI_InitStructure.EXTI_LineCmd = ENABLE;

        switch (n32_pin_irq_hdr_tab[Pin].mode)
        {
        case PIN_IRQ_MODE_RISING:
            EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
            break;
        case PIN_IRQ_MODE_FALLING:
            EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
            break;
        case PIN_IRQ_MODE_RISING_FALLING:
            EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;
            break;
        case PIN_IRQ_MODE_HIGH_LEVEL:
        case PIN_IRQ_MODE_LOW_LEVEL:
        default:
            os_irq_unlock(level);
            return OS_INVAL;
        }

        GPIO_ConfigEXTILine(GPIOA_PORT_SOURCE + Port, GPIO_PIN_SOURCE0 + Pin);

        EXTI_InitPeripheral(&EXTI_InitStructure);

        NVIC_InitType NVIC_InitStructure;
        NVIC_InitStructure.NVIC_IRQChannel                   = irqmap->irqno;
        NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
        NVIC_InitStructure.NVIC_IRQChannelSubPriority        = 0;
        NVIC_InitStructure.NVIC_IRQChannelCmd                = ENABLE;
        NVIC_Init(&NVIC_InitStructure);

        os_irq_unlock(level);
    }
    else if (enabled == PIN_IRQ_DISABLE)
    {
        level = os_irq_lock();

        EXTI_InitType EXTI_InitStructure;
        EXTI_InitStructure.EXTI_Line    = irqmap->pinbit;
        EXTI_InitStructure.EXTI_Mode    = EXTI_Mode_Interrupt;
        EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
        EXTI_InitStructure.EXTI_LineCmd = DISABLE;
        EXTI_InitPeripheral(&EXTI_InitStructure);

        os_irq_unlock(level);
    }
    else
    {
        return OS_ENOSYS;
    }

    return OS_SUCCESS;
}

const static struct os_pin_ops n32_pin_ops = {
    .pin_mode       = n32_pin_mode,
    .pin_write      = n32_pin_write,
    .pin_read       = n32_pin_read,
    .pin_attach_irq = n32_pin_attach_irq,
    .pin_detach_irq = n32_pin_detach_irq,
    .pin_irq_enable = n32_pin_irq_enable,
};

int os_hw_pin_init(void)
{
    RCC_EnableAPB2PeriphClk(RCC_APB2_PERIPH_AFIO, ENABLE);
	
	RCC_EnableAPB2PeriphClk(RCC_APB2_PERIPH_GPIOA, ENABLE);
	RCC_EnableAPB2PeriphClk(RCC_APB2_PERIPH_GPIOB, ENABLE);
	RCC_EnableAPB2PeriphClk(RCC_APB2_PERIPH_GPIOC, ENABLE);
	RCC_EnableAPB2PeriphClk(RCC_APB2_PERIPH_GPIOD, ENABLE);

    return os_device_pin_register(0, &n32_pin_ops, OS_NULL);
}

static void n32_pin_irq_hdr(int irqno)
{
    if (n32_pin_irq_hdr_tab[irqno].hdr)
    {
        n32_pin_irq_hdr_tab[irqno].hdr(n32_pin_irq_hdr_tab[irqno].args);
    }
}


static void HAL_GPIO_EXTI_IRQHandler(uint16_t EXTI_Line)
{
    if ((EXTI->IMASK & EXTI_Line)
        && (EXTI->PEND & EXTI_Line))
    {
        EXTI->PEND = EXTI_Line;

        n32_pin_irq_hdr(os_ffs(EXTI_Line) - 1);
    }
}

void EXTI0_IRQHandler(void)
{
    HAL_GPIO_EXTI_IRQHandler(EXTI_LINE0);
}

void EXTI1_IRQHandler(void)
{
    HAL_GPIO_EXTI_IRQHandler(EXTI_LINE1);
}

void EXTI2_IRQHandler(void)
{
    HAL_GPIO_EXTI_IRQHandler(EXTI_LINE2);
}

void EXTI3_IRQHandler(void)
{
    HAL_GPIO_EXTI_IRQHandler(EXTI_LINE3);
}

void EXTI4_IRQHandler(void)
{
    HAL_GPIO_EXTI_IRQHandler(EXTI_LINE4);
}

void EXTI9_5_IRQHandler(void)
{
    HAL_GPIO_EXTI_IRQHandler(EXTI_LINE5);
    HAL_GPIO_EXTI_IRQHandler(EXTI_LINE6);
    HAL_GPIO_EXTI_IRQHandler(EXTI_LINE7);
    HAL_GPIO_EXTI_IRQHandler(EXTI_LINE8);
    HAL_GPIO_EXTI_IRQHandler(EXTI_LINE9);
}

void EXTI15_10_IRQHandler(void)
{
    HAL_GPIO_EXTI_IRQHandler(EXTI_LINE10);
    HAL_GPIO_EXTI_IRQHandler(EXTI_LINE11);
    HAL_GPIO_EXTI_IRQHandler(EXTI_LINE12);
    HAL_GPIO_EXTI_IRQHandler(EXTI_LINE13);
    HAL_GPIO_EXTI_IRQHandler(EXTI_LINE14);
    HAL_GPIO_EXTI_IRQHandler(EXTI_LINE15);
}
