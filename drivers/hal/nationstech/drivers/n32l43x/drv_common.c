/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_common.c
 *
 * @brief       This file provides systick time init/IRQ and board init functions.
 *
 * @revision
 * Date         Author          Notes
 * 2022-01-10   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include <string.h>
#include <os_types.h>
#include <os_stddef.h>

#include <arch_interrupt.h>
#include <os_clock.h>
#include <os_memory.h>
#include <oneos_config.h>

#include <drv_common.h>
#include "n32_it.h"
#include "board.h"
#include "drv_gpio.h"

#ifdef OS_USING_DMA
#include <dma.h>
#endif

#ifdef OS_USING_CLOCKSOURCE
#include <timer/clocksource.h>
#include <timer/clocksource_cortexm.h>
#endif

#include <timer/hrtimer.h>

static volatile os_bool_t hardware_init_done = OS_FALSE;

void HAL_SuspendTick(void)
{
    /* Disable SysTick Interrupt */
    SysTick->CTRL &= ~SysTick_CTRL_TICKINT_Msk;
}

void os_tick_handler(void)
{
    os_tick_increase();

#ifdef OS_USING_CLOCKSOURCE
    os_clocksource_update();
#endif
}

#ifdef OS_USING_SYSTICK_FOR_KERNEL_TICK

void SysTick_Handler(void)
{
    os_tick_increase();

#ifdef OS_USING_CLOCKSOURCE
    os_clocksource_update();
#endif
}

static void cortexm_systick_kernel_tick_init(void)
{
    SysTick_Config(SystemCoreClock / OS_TICK_PER_SECOND);
    SysTick->CTRL |= 0x00000004UL;

    NVIC_SetPriority(SysTick_IRQn, 0xFF);
}

#endif

#if defined(OS_USING_SYSTICK_FOR_CLOCKEVENT)
void SysTick_Handler(void)
{
    if (hardware_init_done)
    {
        cortexm_systick_clockevent_isr();
    }
    else
    {
        os_tick_handler();
    }
}
#endif

void hardware_init(void)
{
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);  // 不能是NVIC_PriorityGroup_0, 否则SVC指令会产生硬件错误(Group0所有位为响应优先级,没有抢占)
	
	
	SysTick_Config(SystemCoreClock / OS_TICK_PER_SECOND);
    SysTick->CTRL = 0;
	NVIC_SetPriority(SysTick_IRQn, 0xFF);
	NVIC_EnableIRQ(SysTick_IRQn);
	
	SysTick->LOAD = 10000; /* set reload register */
    SysTick->VAL  = 0UL;   /* Load the systick Counter Value */
    SysTick->CTRL |= SysTick_CTRL_CLKSOURCE_Msk |
                   SysTick_CTRL_TICKINT_Msk   |
                   SysTick_CTRL_ENABLE_Msk;;

    
}

void os_hw_cpu_reset(void)
{
    NVIC_SystemReset();
}

/**
 ***********************************************************************************************************************
 * @brief           This function will initial STM32 board.
 *
 * @param[in]       none
 *
 * @return          none
 ***********************************************************************************************************************
 */
static os_err_t os_hw_board_init(void)
{
    hardware_init();
    os_irq_enable();
    HAL_SuspendTick();

#ifdef HAL_SDRAM_MODULE_ENABLED
    SDRAM_Init();
#endif

    /* Heap initialization */
#if defined(OS_USING_HEAP)
    if ((os_size_t)HEAP_END > (os_size_t)HEAP_BEGIN)
    {
        os_default_heap_init();
        os_default_heap_add((void *)HEAP_BEGIN, (os_size_t)HEAP_END - (os_size_t)HEAP_BEGIN, OS_MEM_ALG_DEFAULT);
    }
#endif

#ifdef OS_USING_DMA_RAM
    os_dma_mem_init();
#endif

    return OS_SUCCESS;
}
OS_INIT_CALL(os_hw_board_init, OS_INIT_LEVEL_PRE_KERNEL_1, OS_INIT_SUBLEVEL_MIDDLE);

void cortexm_systick_init(void)
{
#ifdef OS_USING_SYSTICK_FOR_KERNEL_TICK
    cortexm_systick_kernel_tick_init();
#elif defined(OS_USING_SYSTICK_FOR_CLOCKSOURCE)
    cortexm_systick_clocksource_init();
#elif defined(OS_USING_SYSTICK_FOR_CLOCKEVENT)
    cortexm_systick_clockevent_init();
#endif
}

static os_err_t board_post_init(void)
{
#ifdef OS_USING_PIN
    os_hw_pin_init();
#endif

#if defined(OS_USING_DWT_FOR_CLOCKSOURCE) && defined(DWT)
    cortexm_dwt_init();
#endif

    // calc_mult_shift(&mult_systick2msec, &shift_systick2msec, OS_TICK_PER_SECOND, 1000, 1);

    cortexm_systick_init();
	
	hardware_init_done = OS_TRUE;

    return OS_SUCCESS;
}

OS_INIT_CALL(board_post_init, OS_INIT_LEVEL_POST_KERNEL, OS_INIT_SUBLEVEL_LOW);
