/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_spi.c
 *
 * @brief       This file implements usart driver for apm32
 *
 * @revision
 * Date         Author          Notes
 * 2022-01-10   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <os_memory.h>
#include <bus/bus.h>
#include <dma/dma.h>
#include <board.h>
#include <os_task.h>
#include <string.h>
#include <os_list.h>
#include <spi/spi.h>
#include <os_task.h>

#define DRV_EXT_LVL DBG_EXT_INFO
#define DRV_EXT_TAG "drv.spi"

#include "drv_spi.h"

#define SPI_USING_RX_DMA_FLAG (1 << 0)
#define SPI_USING_TX_DMA_FLAG (1 << 1)

struct n32_spi_bus
{
    struct os_spi_bus 				spi_bus;

    SPI_Module 						*hspi;
	const struct n32_spi_info 		*info;

    struct os_spi_configuration 	*cfg;

    uint8_t spi_dma_flag;

    os_list_node_t list;
};

static os_list_node_t n32_spi_list = OS_LIST_INIT(n32_spi_list);

static os_err_t n32_spi_init(struct n32_spi_bus *spi_bus, struct os_spi_configuration *cfg)
{
    OS_ASSERT(spi_drv != OS_NULL);
    OS_ASSERT(cfg != OS_NULL);
	
	GPIO_InitType ioinit;
	GPIO_InitStruct(&ioinit);
    
	ioinit.Pin = spi_bus->info->sck_pin;
    ioinit.GPIO_Speed = GPIO_Speed_50MHz;
    ioinit.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitPeripheral(spi_bus->info->sck_port, &ioinit);
	
	ioinit.Pin = spi_bus->info->mosi_pin;
	GPIO_InitPeripheral(spi_bus->info->mosi_port, &ioinit);
	
	ioinit.Pin = spi_bus->info->miso_pin;
	ioinit.GPIO_Mode = GPIO_Mode_IPD;
	GPIO_InitPeripheral(spi_bus->info->miso_port, &ioinit);
	
	
	switch(spi_bus->info->rcc_type)
	{
	case 1:
		RCC_EnableAPB1PeriphClk(spi_bus->info->rcc, ENABLE);
		break;
	case 2:
		RCC_EnableAPB2PeriphClk(spi_bus->info->rcc, ENABLE);
		break;
	default:
		OS_ASSERT(false);
		break;
	}
	
    SPI_InitType init;
    SPI_InitStruct(&init);
	init.NSS = SPI_NSS_SOFT;

    SPI_Module *hspi = spi_bus->hspi;

    if (cfg->mode & OS_SPI_SLAVE)
    {
        init.SpiMode = SPI_MODE_SLAVE;
    }
    else
    {
        init.SpiMode = SPI_MODE_MASTER;
    }

    if (cfg->mode & OS_SPI_3WIRE)
    {
        init.DataDirection = SPI_DIR_DOUBLELINE_FULLDUPLEX;
    }
    else
    {
        init.DataDirection = SPI_DIR_DOUBLELINE_FULLDUPLEX;
    }

    if (cfg->data_width == 8)
    {
        init.DataLen = SPI_DATA_SIZE_8BITS;
    }
    else if (cfg->data_width == 16)
    {
        init.DataLen = SPI_DATA_SIZE_16BITS;
    }
    else
    {
        return OS_EIO;
    }

    if (cfg->mode & OS_SPI_CPHA)
    {
        init.CLKPHA = SPI_CLKPHA_SECOND_EDGE;
    }
    else
    {
        init.CLKPHA = SPI_CLKPHA_FIRST_EDGE;
    }

    if (cfg->mode & OS_SPI_CPOL)
    {
        init.CLKPOL = SPI_CLKPOL_HIGH;
    }
    else
    {
        init.CLKPOL = SPI_CLKPOL_LOW;
    }

    if (cfg->mode & OS_SPI_MSB)
    {
        init.FirstBit = SPI_FB_MSB;
    }
    else
    {
        init.FirstBit = SPI_FB_LSB;
    }

	// HCLK:100MHz, APB2 CLK: 50MHz 100MHz / 2
    init.BaudRatePres = SPI_BR_PRESCALER_32;//cfg->max_hz;

    SPI_Init(hspi, &init);
	
	SPI_Enable(hspi, ENABLE);

    os_kprintf(DRV_EXT_TAG ": %s init done\r\n", spi_bus->spi_bus.parent.name);
    return OS_SUCCESS;
}

static os_err_t spi_configure(struct os_spi_device *device, struct os_spi_configuration *configuration)
{
    OS_ASSERT(device != OS_NULL);
    OS_ASSERT(configuration != OS_NULL);

    struct n32_spi_bus *spi_bus = os_container_of(device->bus, struct n32_spi_bus, spi_bus);
    spi_bus->cfg              = configuration;

    return n32_spi_init(spi_bus, configuration);
}

/* clang-format off */
static uint32_t spixfer(struct os_spi_device *device, struct os_spi_message *message)
{
    OS_ASSERT(device != OS_NULL);
    OS_ASSERT(device->bus != OS_NULL);
    OS_ASSERT(message != OS_NULL);

    struct n32_spi_bus *spi_bus = os_container_of(device->bus, struct n32_spi_bus, spi_bus);
    SPI_Module *hspi = spi_bus->hspi;

    if (message->cs_take
		&& device->cs_pin >= 0)
    {
        os_pin_write(device->cs_pin, PIN_LOW);
    }
	
	SPI_I2S_ReceiveData(hspi);

    // LOG_D(DBG_TAG, "%s transfer prepare and start", spi_drv->config->bus_name);
    // LOG_D(DBG_TAG,
    //       "%s sendbuf: %X, recvbuf: %X, length: %d",
    //       spi_drv->config->bus_name,
    //       (uint32_t)message->send_buf,
    //       (uint32_t)message->recv_buf,
    //       message->length);

	/* start once data exchange in DMA mode */
	if (message->send_buf && message->recv_buf)
	{
		if ((spi_bus->spi_dma_flag & SPI_USING_TX_DMA_FLAG) && (spi_bus->spi_dma_flag & SPI_USING_RX_DMA_FLAG))
		{
			//ret =  SPI_TransmitReceive_DMA SPI_TransmitReceive_DMA(spi_handle, (uint8_t *)send_buf, (uint8_t *)recv_buf, send_length);
		}
		else
		{
			int step = 1;
			if (spi_bus->cfg->data_width > 8)
			{
				step = 2;
			}

			if (step == 1)
            {
                const uint8_t *const send_buf = message->send_buf;
                uint8_t *const recv_buf = message->recv_buf;

                for(int i=0; i<message->length; i+=step)
                {
                    while(!SPI_I2S_GetStatus(hspi, SPI_I2S_TE_FLAG));
                    SPI_I2S_TransmitData(hspi, send_buf[i]);
                    while(!SPI_I2S_GetStatus(hspi, SPI_I2S_RNE_FLAG));
                    recv_buf[i] = SPI_I2S_ReceiveData(hspi);
                }
            }
            else
            {
                const uint16_t *const send_buf = message->send_buf;
                uint16_t *const recv_buf = message->recv_buf;

                for(int i=0; i<message->length; i+=step)
                {
                    const int p = i >> 1;

                    while(!SPI_I2S_GetStatus(hspi, SPI_I2S_TE_FLAG));
                    SPI_I2S_TransmitData(hspi, send_buf[p]);
                    while(!SPI_I2S_GetStatus(hspi, SPI_I2S_RNE_FLAG));
                    recv_buf[p] = SPI_I2S_ReceiveData(hspi);
                }
            }
		}
	}
	else if (message->send_buf)
	{
		if (spi_bus->spi_dma_flag & SPI_USING_TX_DMA_FLAG)
		{
			//ret = HAL_SPI_Transmit_DMA(spi_handle, (uint8_t *)send_buf, send_length);
		}
		else
		{
			int step = 1;
			if (spi_bus->cfg->data_width > 8)
			{
				step = 2;
			}

			if (step == 1)
            {
                const uint8_t *const send_buf = message->send_buf;

                for(int i=0; i<message->length; i+=step)
                {
                    while(!SPI_I2S_GetStatus(hspi, SPI_I2S_TE_FLAG));
                    SPI_I2S_TransmitData(hspi, send_buf[i]);
                }
            }
            else
            {
                const uint16_t *const send_buf = message->send_buf;

                for(int i=0; i<message->length; i+=step)
                {
                    const int p = i >> 1;

                    while(!SPI_I2S_GetStatus(hspi, SPI_I2S_TE_FLAG));
                    SPI_I2S_TransmitData(hspi, send_buf[p]);
                }
            }
			
			while(!SPI_I2S_GetStatus(hspi, SPI_I2S_RNE_FLAG));
		}
	}
	else if (message->recv_buf)
	{
		if (spi_bus->spi_dma_flag & SPI_USING_RX_DMA_FLAG)
		{
			//ret = HAL_SPI_Receive_DMA(spi_handle, (uint8_t *)recv_buf, send_length);
		}
		else
		{
            int step = 1;
			if (spi_bus->cfg->data_width > 8)
			{
				step = 2;
			}

			if (step == 1)
            {
                uint8_t *const recv_buf = message->recv_buf;

                for(int i=0; i<message->length; i+=step)
                {
                    while(!SPI_I2S_GetStatus(hspi, SPI_I2S_TE_FLAG));
                    SPI_I2S_TransmitData(hspi, recv_buf[i]);
                    while(!SPI_I2S_GetStatus(hspi, SPI_I2S_RNE_FLAG));
                    recv_buf[i] = SPI_I2S_ReceiveData(hspi);
                }
            }
            else
            {
                uint16_t *const recv_buf = message->recv_buf;

                for(int i=0; i<message->length; i+=step)
                {
                    const int p = i >> 1;

                    while(!SPI_I2S_GetStatus(hspi, SPI_I2S_TE_FLAG));
                    SPI_I2S_TransmitData(hspi, recv_buf[i]);
                    while(!SPI_I2S_GetStatus(hspi, SPI_I2S_RNE_FLAG));
                    recv_buf[p] = SPI_I2S_ReceiveData(hspi);
                }
            }
		}
	}
    else
    {	// 错误 message->send_buf、message->recv_buf 都是NULL
        return 0;
    }

    if (message->cs_release
		&& device->cs_pin >= 0)
    {
        os_pin_write(device->cs_pin, PIN_HIGH);
    }

    return message->length;
}
/* clang-format on */

static const struct os_spi_ops n32_spi_bus_ops = {
    .configure = spi_configure,
    .xfer      = spixfer,
};

static int n32_spi_bus_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    os_err_t  result = 0;
    os_base_t level;

    struct n32_spi_bus *spi_bus = os_calloc(1, sizeof(struct n32_spi_bus));

    OS_ASSERT(nt_spi);
	
	spi_bus->info = dev->info;

    spi_bus->hspi = spi_bus->info->hspi;

    level = os_irq_lock();
    os_list_add_tail(&n32_spi_list, &spi_bus->list);
    os_irq_unlock(level);

    result = os_spi_bus_register(&spi_bus->spi_bus, dev->name, &n32_spi_bus_ops);
    OS_ASSERT(result == OS_EOK);

    os_kprintf(DRV_EXT_TAG ": %s bus init done\r\n", dev->name);

    return result;
}

OS_DRIVER_INFO n32_spi_driver = {
    .name  = "SPI_Module",
    .probe = n32_spi_bus_probe,
};

OS_DRIVER_DEFINE(n32_spi_driver, OS_INIT_LEVEL_PRE_DEVICE, OS_INIT_SUBLEVEL_LOW);
