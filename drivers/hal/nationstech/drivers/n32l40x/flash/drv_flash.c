/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_flash_common.c
 *
 * @brief       This file provides flash read/write/erase functions for N32.
 *
 * @revision
 * Date         Author          Notes
 * 2022-01-11   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include <os_memory.h>

#define DRV_EXT_LVL DBG_EXT_INFO
#define DRV_EXT_TAG "drv.flash"
#include <drv_log.h>

#include "drv_flash.h"

#define N32_FLASH_BLOCK_SIZE 	(2 * 1024)
#define N32_FLASH_PAGE_SIZE     (2 * 1024)

int n32_flash_read(uint32_t addr, uint8_t *buf, size_t size)
{
    size_t i;

    if ((addr + size) > N32_FLASH_END_ADDR)
    {
        LOG_E(DRV_EXT_TAG, "read outrange flash size! addr is (0x%p)", (void *)(addr + size));
        return OS_INVAL;
    }

    for (i = 0; i < size; i++, buf++, addr++)
    {
        *buf = *(uint8_t *)addr;
    }

    return size;
}

int n32_flash_write(uint32_t addr, const uint8_t *buf, size_t size)
{
    size_t      i, j;
    os_err_t    result     = 0;
    uint32_t write_data = 0, temp_data = 0;

    if ((addr + size) > N32_FLASH_END_ADDR)
    {
        LOG_E(DRV_EXT_TAG, "ERROR: write outrange flash size! addr is (0x%p)", (void *)(addr + size));
        return OS_INVAL;
    }

    if (addr % 4 != 0)
    {
        LOG_E(DRV_EXT_TAG, "write addr must be 4-byte alignment");
        return OS_INVAL;
    }

    FLASH_Unlock();

    if (size < 1)
    {
        return OS_INVAL;
    }

    for (i = 0; i < size;)
    {
        if ((size - i) < 4)
        {
            for (j = 0; (size - i) > 0; i++, j++)
            {
                temp_data  = *buf;
                write_data = (write_data) | (temp_data << 8 * j);
                buf++;
            }
        }
        else
        {
            for (j = 0; j < 4; j++, i++)
            {
                temp_data  = *buf;
                write_data = (write_data) | (temp_data << 8 * j);
                buf++;
            }
        }

        /* write data */
        if (FLASH_ProgramWord(addr, write_data) == FLASH_COMPL)
        {
            /* Check the written value */
            if (*(uint32_t *)addr != write_data)
            {
                LOG_E(DRV_EXT_TAG, "ERROR: write data != read data");
                result = OS_FAILURE;
                goto __exit;
            }
        }
        else
        {
            result = OS_FAILURE;
            goto __exit;
        }

        temp_data  = 0;
        write_data = 0;

        addr += 4;
    }

__exit:
    FLASH_Lock();
    if (result != 0)
    {
        return result;
    }

    return size;
}

int n32_flash_erase(uint32_t addr, size_t size)
{
    os_err_t    result      = OS_SUCCESS;

    const uint32_t page_addr_mask = ~(N32_FLASH_PAGE_SIZE - 1);
    uint32_t end_addr = addr + size;

    if (end_addr > N32_FLASH_END_ADDR)
    {
        LOG_E(DRV_EXT_TAG, "ERROR: erase outrange flash size! addr is (0x%p)", (void *)(addr + size));
        return OS_INVAL;
    }

    FLASH_Unlock();

    for (uint32_t page = addr & page_addr_mask; page < end_addr; page+=N32_FLASH_PAGE_SIZE)
    {
        if (FLASH_EraseOnePage(page) != FLASH_COMPL)
        {
            result = OS_FAILURE;
            goto __exit;
        }
    }

__exit:
    FLASH_Lock();

    if (result != OS_SUCCESS)
    {
        return result;
    }

    LOG_D(DRV_EXT_TAG, "erase done: addr (0x%p), size %d", (void *)addr, size);
    return size;
}

#include "fal_drv_flash.c"
