/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        n32_it.c
 *
 * @brief       This file provides systick time init/IRQ functions.
 *
 * @revision
 * Date         Author          Notes
 * 2022-01-10   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include "os_clock.h"
#include "os_stddef.h"
#include "oneos_config.h"

#ifdef OS_USING_CLOCKSOURCE
#include <timer/clocksource.h>
#include <timer/clocksource_cortexm.h>
#endif

#define DRV_EXT_LVL DBG_EXT_INFO
#define DRV_EXT_TAG "drv.irq"
#include <drv_log.h>

#include "n32_it.h"
#include "drv_gpio.h"
#include "drv_usart.h"

void NMI_Handler(void)
{
	if (CoreDebug->DHCSR & 1) { // check C_DEBUGEN == 1-> Debugger Connected
		__BKPT(0); // halt program execution here
	}
	while (1);
}

void MemManage_Handler(void)
{
	if (CoreDebug->DHCSR & 1) { // check C_DEBUGEN == 1-> Debugger Connected
		__BKPT(0); // halt program execution here
	}
	/* Go to infinite loop when Memory Manage exception occurs */
	while (1);
}

void BusFault_Handler(void)
{
	if (CoreDebug->DHCSR & 1) { // check C_DEBUGEN == 1-> Debugger Connected
		__BKPT(0); // halt program execution here
	}
	/* Go to infinite loop when Bus Fault exception occurs */
	while (1);
}

void UsageFault_Handler(void)
{
	if (CoreDebug->DHCSR & 1) { // check C_DEBUGEN == 1-> Debugger Connected
		__BKPT(0); // halt program execution here
	}
	/* Go to infinite loop when Usage Fault exception occurs */
	while (1);
}
