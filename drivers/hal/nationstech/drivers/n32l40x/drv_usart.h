/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_usart.h
 *
 * @brief        This file provides functions declaration for usart driver.
 *
 * @revision
 * Date         Author          Notes
 * 2022-01-10   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#ifndef __DRV_USART_H__
#define __DRV_USART_H__

#include <os_types.h>

#ifdef SERIES_N32G43X
#include "n32g43x_conf.h"
#endif

#ifdef SERIES_N32G45X
#include "n32g45x_hal.h"
#endif

#ifdef SERIES_N32L40X
#include "n32l40x_hal.h"
#endif

struct n32_usart_info
{
    USART_Module *huart;
    int rcc_type;
    int rcc;
    IRQn_Type irq;

    DMA_ChannelType *dma_channel;
    int dma_rcc;
    IRQn_Type dma_irq;

    GPIO_Module *tx_port;
    int tx_pin;
    int tx_rcc;

    GPIO_Module *rx_port;
    int rx_pin;
    int rx_rcc;
	
	void (*init_hook)();
	void (*send_start_hook)(void);
	void (*send_end_hook)(void);
};

#endif /* __DRV_USART_H__ */
