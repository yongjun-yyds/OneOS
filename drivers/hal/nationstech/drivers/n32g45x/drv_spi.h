/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_spi.h
 *
 * @brief        This file provides functions declaration for usart driver.
 *
 * @revision
 * Date         Author          Notes
 * 2022-01-10   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#ifndef __DRV_SPI_H__
#define __DRV_SPI_H__

#include <os_types.h>

#include "n32g45x.h"

struct n32_spi_info
{
    SPI_Module *hspi;
    int rcc_type;
    int rcc;
    IRQn_Type irq;

	struct
    {
		DMA_ChannelType *channel;
		int rcc;
		IRQn_Type irq;
	}dma_rx, dma_tx;

    GPIO_Module *nss_port;
    int nss_pin;
    int nss_rcc;

    GPIO_Module *sck_port;
    int sck_pin;
    int sck_rcc;

    GPIO_Module *miso_port;
    int miso_pin;
    int miso_rcc;

    GPIO_Module *mosi_port;
    int mosi_pin;
    int mosi_rcc;
};

#endif /* __DRV_SPI_H__ */
