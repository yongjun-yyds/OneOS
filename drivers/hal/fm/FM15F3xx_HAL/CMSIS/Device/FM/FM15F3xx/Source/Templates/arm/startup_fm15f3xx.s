;//-------- <<< Use Configuration Wizard in Context Menu >>> ------------------
;*/


; <h> Stack Configuration
;   <o> Stack Size (in Bytes) <0x0-0xFFFFFFFF:8>
; </h>

Stack_Size      EQU     0x00001000; 4K

                AREA    STACK, NOINIT, READWRITE, ALIGN=3
Stack_Mem       SPACE   Stack_Size
__initial_sp


; <h> Heap Configuration
;   <o>  Heap Size (in Bytes) <0x0-0xFFFFFFFF:8>
; </h>

Heap_Size       EQU     0x00000400; 1K

                AREA    HEAP, NOINIT, READWRITE, ALIGN=3
__heap_base
Heap_Mem        SPACE   Heap_Size
__heap_limit


                PRESERVE8
                THUMB
 

; Vector Table Mapped to Address 0 at Reset

                AREA    RESET, DATA, READONLY
                EXPORT  __Vectors
                EXPORT  __Vectors_End
                EXPORT  __Vectors_Size

__Vectors       DCD     __initial_sp              ; Top of Stack
                DCD     Reset_Handler             ; Reset Handler
                DCD     NMI_Handler               ; NMI Handler
                DCD     HardFault_Handler         ; Hard Fault Handler
                DCD     MemManage_Handler         ; MPU Fault Handler
                DCD     BusFault_Handler          ; Bus Fault Handler
                DCD     UsageFault_Handler        ; Usage Fault Handler
                DCD     0                         ; Reserved 
                DCD     0                         ; Reserved
                DCD     0                         ; Reserved
                DCD     0                         ; Reserved
                DCD     SVC_Handler               ; SVCall Handler
                DCD     DebugMon_Handler          ; Debug Monitor Handler
                DCD     0                         ; Reserved
                DCD     PendSV_Handler            ; PendSV Handler
                DCD     SysTick_Handler           ; SysTick Handler

                ; External Interrupts	

				DCD			DMA_Channel0_Handler         ; DMA channel 0 transfer complete  
				DCD			DMA_Channel1_Handler         ; DMA channel 1 transfer complete  
				DCD			DMA_Channel2_Handler         ; DMA channel 2 transfer complete  
				DCD			DMA_Channel3_Handler         ; DMA channel 3 transfer complete  
				DCD			DMA_Channel4_Handler         ; DMA channel 4 transfer complete  
				DCD			DMA_Channel5_Handler         ; DMA channel 5 transfer complete  
				DCD			DMA_Channel6_Handler         ; DMA channel 6 transfer complete  
				DCD			DMA_Channel7_Handler         ; DMA channel 7 transfer complete  
				DCD			DMA_Error_Handler            ; DMA channel 0 - 7 error          
				DCD			PAE_Handler                  ; PAE interrupt                    
				DCD			HASH_Handler                 ; HASH interrupt                   
				DCD			BCA_Handler                  ; BCA interrupt                    
				DCD			DSP_Handler                  ; DSP interrupt                    
				DCD			DSP_Error_Handler            ; Dsp error interrupt              
				DCD			FLASH_Int_Handler            ; Flash         interrupt          
				DCD			FLASH_RdcolInt_Handler       ; Flash read collision interrupt   
				DCD			FLASH_LvtInt_Handler         ; Flash low V interrupt            
				DCD			PM_Int_Handler               ; PM interrupt                     
				DCD			RANDOM_Handler               ; Random  interrupt                
				DCD			0                            ; Reserved interrupt               
				DCD			SPI0_Handler                 ; SPI0 interrupt                   
				DCD			SPI1_Handler                 ; SPI1 interrupt                   
				DCD			SPI2_Handler                 ; SPI2 interrupt                   
				DCD			SPI3_Handler                 ; SPI3 interrupt                   
				DCD			UART0_Handler                ; UART0 interrupt                  
				DCD			UART1_Handler                ; UART1 interrupt                  
				DCD			UART2_Handler                ; UART2 interrupt                  
				DCD			0                            ; Reserved interrupt               
				DCD			I2C0_Handler                 ; I2C0 interrupt                   
				DCD			I2C1_Handler                 ; I2C1 interrupt                   
				DCD			0                            ; Reserved interrupt               
				DCD			0                            ; Reserved interrupt               
				DCD			CT0_Handler                  ; CT0 interrupt                    
				DCD			CT1_Handler                  ; CT1 interrupt                    
				DCD			FSMC_Handler                 ; FSMC interrupt                   
				DCD			QSPI_Handler                 ; QSPI interrupt                   
				DCD			USB_Handler                  ; USB interrupt                    
				DCD			DCMI_Handler                 ; DCMI interrupt                   
				DCD			DCMI_DMA_Handler             ; DCMI_DMA interrupt               
				DCD			0                            ; Reserved interrupt               
				DCD			STIMER0_Handler              ; Stimer0 interrupt                
				DCD			STIMER1_Handler              ; Stimer1 interrupt                
				DCD			STIMER2_Handler              ; Stimer2 interrupt                
				DCD			STIMER3_Handler              ; Stimer3 interrupt                
				DCD			STIMER4_Handler              ; Stimer4 interrupt                
				DCD			STIMER5_Handler              ; Stimer5 interrupt                
				DCD			LPTIMER0_Handler             ; LPtimer0 interrupt               
				DCD			0                            ; Reserved interrupt               
				DCD			GPIOA_Handler                ; GPIO A interrupt                 
				DCD			GPIOB_Handler                ; GPIO B interrupt                 
				DCD			GPIOC_Handler                ; GPIO C interrupt                 
				DCD			GPIOD_Handler                ; GPIO D interrupt                 
				DCD			GPIOE_Handler                ; GPIO E interrupt                 
				DCD			GPIOF_Handler                ; GPIO F interrupt                 
				DCD			RTC_Checkerr_Handler         ; RTC check error interrupt        
				DCD			BKP_Tamper_Handler           ; RTC tamper interrupt             
				DCD			RTC_Alarm_Handler            ; RTC alarm interrupt               
				DCD			RTC_1S_Handler               ; RTC 1S interrupt             
				DCD		    0                            ; Reserved interrupt               
				DCD			ADC0_Handler                 ; ADC0 interrupt                   
				DCD			CMP0_Handler                 ; CMP0 interrupt                   
				DCD			DAC_Watermark_Handler        ; DAC watermark interrupt          
				DCD			DAC_Top_Handler              ; DAC top interrupt                
				DCD			DAC_Bottom_Handler           ; DAC bottom interrupt             
__Vectors_End

__Vectors_Size  EQU     __Vectors_End - __Vectors

                AREA    |.text|, CODE, READONLY


; Reset Handler

Reset_Handler   PROC
                EXPORT  Reset_Handler             [WEAK]
                IMPORT  SystemInit
                IMPORT  __main
	;						  B       .
                LDR     R0, =SystemInit
                BLX     R0
                LDR     R0, =__main
                BX      R0
                ENDP 


; Dummy Exception Handlers (infinite loops which can be modified)

NMI_Handler     PROC
                EXPORT  NMI_Handler               [WEAK]
                B       .
                ENDP
HardFault_Handler\
                PROC
                EXPORT  HardFault_Handler         [WEAK]
                B       .
                ENDP
MemManage_Handler\
                PROC
                EXPORT  MemManage_Handler         [WEAK]
                B       .
                ENDP
BusFault_Handler\
                PROC
                EXPORT  BusFault_Handler          [WEAK]
                B       .
                ENDP
UsageFault_Handler\
                PROC
                EXPORT  UsageFault_Handler        [WEAK]
                B       .
                ENDP
SVC_Handler     PROC
                EXPORT  SVC_Handler               [WEAK]
                B       .
                ENDP
DebugMon_Handler\
                PROC
                EXPORT  DebugMon_Handler          [WEAK]
                B       .
                ENDP				
PendSV_Handler  PROC
                EXPORT  PendSV_Handler            [WEAK]
                B       .
                ENDP
SysTick_Handler PROC
                EXPORT  SysTick_Handler           [WEAK]
                B       .
                ENDP

Default_Handler PROC

;                EXPORT  DMA_Channel0_Handler      [WEAK]
;                EXPORT  DMA_Channel1_Handler      [WEAK]
;                EXPORT  DMA_Channel2_Handler      [WEAK]
;                EXPORT  DMA_Channel3_Handler      [WEAK]
;                EXPORT  DMA_Channel4_Handler      [WEAK]
;                EXPORT  DMA_Channel5_Handler      [WEAK]
;                EXPORT  DMA_Channel6_Handler      [WEAK]
;                EXPORT  DMA_Channel7_Handler      [WEAK]
;                EXPORT  DMA_Error_Handler         [WEAK]
;                EXPORT  PAE_Handler           	   [WEAK]
;                EXPORT  HASH_Handler              [WEAK]
;                EXPORT  BCA_Handler               [WEAK]
;                EXPORT  DSP_Handler               [WEAK]
;                EXPORT  DSP_Error_Handler         [WEAK]
;                EXPORT  FLASH_Int_Handler         [WEAK]
;                EXPORT  FLASH_RdcolInt_Handler    [WEAK]
;                EXPORT  PM_Int_Handler            [WEAK]
;                EXPORT  MONITOR_Handler           [WEAK]
;                EXPORT  RANDOM_Handler            [WEAK]
;                EXPORT  I2C0_Handler              [WEAK]
;                EXPORT  I2C1_Handler              [WEAK]
;                EXPORT  SPI0_Handler              [WEAK]
;                EXPORT  SPI1_Handler              [WEAK]
;                EXPORT  UART0_Handler             [WEAK]
;                EXPORT  UART1_Handler             [WEAK]
;                EXPORT  UART2_Handler             [WEAK]
;                EXPORT  UART3_Handler             [WEAK]
;                EXPORT  CT0_Handler               [WEAK]
;                EXPORT  CT1_Handler               [WEAK]
;                EXPORT  ADC0_Handler              [WEAK]
;                EXPORT  CMP0_Handler              [WEAK]
;                EXPORT  RTC_Checkerr_Handler      [WEAK]
;                EXPORT  BKP_Tamper_Handler        [WEAK]
;                EXPORT  RTC_Alarm_Handler         [WEAK]
;                EXPORT  RTC_1S_Handler            [WEAK]
;                EXPORT  STIMER0_Handler           [WEAK]
;                EXPORT  STIMER1_Handler           [WEAK]
;                EXPORT  STIMER2_Handler           [WEAK]
;                EXPORT  STIMER3_Handler           [WEAK]
;                EXPORT  USB_Handler               [WEAK]
;                EXPORT  DAC_Watermark_Handler     [WEAK]
;                EXPORT  DAC_Top_Handler           [WEAK]
;                EXPORT  DAC_Bottom_Handler        [WEAK]
;                EXPORT  LPTIMER0_Handler          [WEAK]
;                EXPORT  LPTIMER1_Handler          [WEAK]
;                EXPORT  GPIOA_Handler             [WEAK]
;                EXPORT  GPIOB_Handler             [WEAK]
;                EXPORT  GPIOC_Handler             [WEAK]
;                EXPORT  GPIOD_Handler             [WEAK]
;                EXPORT  GPIOE_Handler             [WEAK]
;                EXPORT  GPIOF_Handler             [WEAK]
;                EXPORT  SPI2_Handler              [WEAK]
;                EXPORT  SPI3_Handler              [WEAK]
;                EXPORT  FSMC_Handler              [WEAK]
;                EXPORT  I2C2_Handler              [WEAK]
;                EXPORT  I2C3_Handler              [WEAK]
;                EXPORT  ATIMER_Fault_Handler      [WEAK]
;                EXPORT  ATIMER_Ch0_Handler        [WEAK]
;                EXPORT  ATIMER_Ch1_Handler        [WEAK]
;                EXPORT  ATIMER_Ch2_Handler        [WEAK]
;                EXPORT  ATIMER_Ch3_Handler        [WEAK]
;                EXPORT  ATIMER_Count_Handler      [WEAK]
;                EXPORT  GTIMER0_Fault_Handler     [WEAK]
;                EXPORT  GTIMER0_Ch0_Handler       [WEAK]
;                EXPORT  GTIMER0_Ch1_Handler       [WEAK]
;                EXPORT  GTIMER0_Ch2_Handler       [WEAK]
;                EXPORT  GTIMER0_Ch3_Handler       [WEAK]
;                EXPORT  GTIMER0_Count_Handler     [WEAK]
;                EXPORT  GTIMER1_Fault_Handler     [WEAK]
;                EXPORT  GTIMER1_Ch0_Handler       [WEAK]
;                EXPORT  GTIMER1_Ch1_Handler       [WEAK]
;                EXPORT  GTIMER1_Ch2_Handler       [WEAK]
;                EXPORT  GTIMER1_Ch3_Handler       [WEAK]
;                EXPORT  GTIMER1_Count_Handler     [WEAK]
;                EXPORT  QSPI_Handler              [WEAK]

                 EXPORT  DMA_Channel0_Handler         [WEAK]
                 EXPORT  DMA_Channel1_Handler         [WEAK]
                 EXPORT  DMA_Channel2_Handler         [WEAK]
                 EXPORT  DMA_Channel3_Handler         [WEAK]
                 EXPORT  DMA_Channel4_Handler         [WEAK]
                 EXPORT  DMA_Channel5_Handler         [WEAK]
                 EXPORT  DMA_Channel6_Handler         [WEAK]
                 EXPORT  DMA_Channel7_Handler         [WEAK]
                 EXPORT  DMA_Error_Handler            [WEAK]
                 EXPORT  PAE_Handler                  [WEAK]
                 EXPORT  HASH_Handler                 [WEAK]
                 EXPORT  BCA_Handler                  [WEAK]
                 EXPORT  DSP_Handler                  [WEAK]
                 EXPORT  DSP_Error_Handler            [WEAK]
                 EXPORT  FLASH_Int_Handler            [WEAK]
                 EXPORT  FLASH_RdcolInt_Handler       [WEAK]
                 EXPORT  FLASH_LvtInt_Handler         [WEAK]
                 EXPORT  PM_Int_Handler               [WEAK]
                 EXPORT  RANDOM_Handler               [WEAK]
                 EXPORT  SPI0_Handler                 [WEAK]
                 EXPORT  SPI1_Handler                 [WEAK]
                 EXPORT  SPI2_Handler                 [WEAK]
                 EXPORT  SPI3_Handler                 [WEAK]
                 EXPORT  UART0_Handler                [WEAK]
                 EXPORT  UART1_Handler                [WEAK]
                 EXPORT  UART2_Handler                [WEAK]
                 EXPORT  I2C0_Handler                 [WEAK]
                 EXPORT  I2C1_Handler                 [WEAK]
                 EXPORT  CT0_Handler                  [WEAK]
                 EXPORT  CT1_Handler                  [WEAK]
                 EXPORT  FSMC_Handler                 [WEAK]
                 EXPORT  QSPI_Handler                 [WEAK]
                 EXPORT  USB_Handler                  [WEAK]
                 EXPORT  DCMI_Handler                 [WEAK]
								 EXPORT	 DCMI_DMA_Handler							[WEAK]
                 EXPORT  STIMER0_Handler              [WEAK]
                 EXPORT  STIMER1_Handler              [WEAK]
                 EXPORT  STIMER2_Handler              [WEAK]
                 EXPORT  STIMER3_Handler              [WEAK]
                 EXPORT  STIMER4_Handler              [WEAK]
                 EXPORT  STIMER5_Handler              [WEAK]
                 EXPORT  LPTIMER0_Handler             [WEAK]
                 EXPORT  GPIOA_Handler                [WEAK]
                 EXPORT  GPIOB_Handler                [WEAK]
                 EXPORT  GPIOC_Handler                [WEAK]
                 EXPORT  GPIOD_Handler                [WEAK]
                 EXPORT  GPIOE_Handler                [WEAK]
                 EXPORT  GPIOF_Handler                [WEAK]
                 EXPORT  RTC_Checkerr_Handler         [WEAK]
                 EXPORT  BKP_Tamper_Handler           [WEAK]
                 EXPORT  RTC_Alarm_Handler            [WEAK]
                 EXPORT  RTC_1S_Handler               [WEAK]
                 EXPORT  ADC0_Handler                 [WEAK]
                 EXPORT  CMP0_Handler                 [WEAK]
                 EXPORT  DAC_Watermark_Handler        [WEAK]
                 EXPORT  DAC_Top_Handler              [WEAK]
                 EXPORT  DAC_Bottom_Handler           [WEAK]
                 

DMA_Channel0_Handler  
DMA_Channel1_Handler  
DMA_Channel2_Handler  
DMA_Channel3_Handler  
DMA_Channel4_Handler  
DMA_Channel5_Handler  
DMA_Channel6_Handler  
DMA_Channel7_Handler  
DMA_Error_Handler     
PAE_Handler           
HASH_Handler          
BCA_Handler           
DSP_Handler           
DSP_Error_Handler     
FLASH_Int_Handler     
FLASH_RdcolInt_Handler 
FLASH_LvtInt_Handler  
PM_Int_Handler        
RANDOM_Handler        
SPI0_Handler          
SPI1_Handler          
SPI2_Handler          
SPI3_Handler          
UART0_Handler         
UART1_Handler         
UART2_Handler         
I2C0_Handler          
I2C1_Handler          
CT0_Handler           
CT1_Handler           
FSMC_Handler          
QSPI_Handler          
USB_Handler           
DCMI_Handler
DCMI_DMA_Handler
STIMER0_Handler       
STIMER1_Handler       
STIMER2_Handler       
STIMER3_Handler       
STIMER4_Handler       
STIMER5_Handler       
LPTIMER0_Handler      
GPIOA_Handler         
GPIOB_Handler         
GPIOC_Handler         
GPIOD_Handler         
GPIOE_Handler         
GPIOF_Handler         
RTC_Checkerr_Handler  
BKP_Tamper_Handler    
RTC_Alarm_Handler      
RTC_1S_Handler    
ADC0_Handler          
CMP0_Handler          
DAC_Watermark_Handler 
DAC_Top_Handler       
DAC_Bottom_Handler    

                B       .
                ENDP


                ALIGN


; User Initial Stack & Heap

                IF      :DEF:__MICROLIB

                EXPORT  __initial_sp
                EXPORT  __heap_base
                EXPORT  __heap_limit

                ELSE

                IMPORT  __use_two_region_memory
                EXPORT  __user_initial_stackheap
__user_initial_stackheap

                LDR     R0, =  Heap_Mem
                LDR     R1, =(Stack_Mem + Stack_Size)
                LDR     R2, = (Heap_Mem +  Heap_Size)
                LDR     R3, = Stack_Mem
                BX      LR

                ALIGN

                ENDIF


                END
