/**
 ******************************************************************************
 * @file    fm15f3xx_ll_stimer.c
 * @author  SRG
 * @version V1.0.0
 * @date    2020-03-17
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */
#include "fm15f3xx_ll_stimer.h"


/**
 * @brief  Configure the TIMx time base unit.
 * @param  TIMx Timer Instance
 * @param  TIM_InitStruct pointer to a @ref LL_TIM_InitTypeDef structure (TIMx time base unit configuration data structure)
 * @retval An ErrorStatus enumeration value:
 *          - SUCCESS: TIMx registers are de-initialized
 *          - ERROR: not applicable
 */
ErrorStatus LL_TIM_Init(ST_TypeDef *STIMx, LL_TIM_InitTypeDef *TIM_InitStruct)
{
    ErrorStatus result = SUCCESS;
    /* Check the parameters */
    if (TIM_InitStruct->Stx <= ST5) {
        if (TIM_InitStruct->Prescaler == 0)
            LL_STIM_DisablePrescaler(TIM_InitStruct->Stx);
        else
            LL_STIM_SetPrescaler(TIM_InitStruct->Stx, TIM_InitStruct->Prescaler);
        LL_STIM_SetRunMode(TIM_InitStruct->Stx, TIM_InitStruct->CounterMode);
        LL_STIM_SetMod0Value(TIM_InitStruct->Stx, TIM_InitStruct->Autoreload);
    } else {
        result = ERROR;
    }
    return (result);
}


/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/

