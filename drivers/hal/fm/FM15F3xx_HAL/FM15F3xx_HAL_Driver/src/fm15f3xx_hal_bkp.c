/**
 ******************************************************************************
 * @file    fm15f3xx_hal_bkp.c
 * @author  CLF
 * @brief   BKP HAL module driver.
 *          This file provides firmware functions to manage the following
 *          functionalities of the BKP :
 *           + Initialization and de-initialization functions
 *           + Regfile write,read and clear functions
 *           + BKP Tamper Init and Self Detect Init functions
 *           + BKP Gpio Init function
 *           + BKP Tamper Int function
 *           + Peripheral State function
 *
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "fm15f3xx_hal.h"
#include "fm15f3xx_ll_rtc.h"


/** @addtogroup FM15F3xx_HAL_Driver
 * @{
 */


/** @defgroup BKP
 * @brief BKP HAL module driver
 * @{
 */
#ifdef BKP_MODULE_ENABLED

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/


/** @addtogroup BKP_Private_Constants BKP Private Constants
 * @{
 */


/**
 * @}
 */
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
/* Exported functions --------------------------------------------------------*/


/** @defgroup BKP_Exported_Functions_Group BKP Exported Functions
 * @{
 */


/** @defgroup BKP_Exported_Functions_Group1 Initialization and de-initialization functions
 *  @brief    Initialization and Configuration functions
 *
   @verbatim
   ===============================================================================
 ##### Initialization and de-initialization functions #####
   ===============================================================================
   @endverbatim
 * @{
 */


/**
 * @brief  Initializes the BKP peripheral
 * @param  hbkp pointer to a BKP_HandleTypeDef structure that contains
 *                the configuration information for BKP.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_BKP_Init(BKP_HandleTypeDef *hbkp)
{
    /* Check the BKP peripheral state */
    if (hbkp == NULL)
        return (HAL_ERROR);

    /* Check the parameters */
    assert_param(IS_BKP_REGFILE_TYPE(hbkp->RegfileType));

    __HAL_BKP_RESET_HANDLE_STATE(hbkp);

    if (hbkp->RegfileType == BKP_REGFILE_SECURE)
        LL_BKP_OpenNormalRegfileAuth();

    /* Initialize BKP MSP */
    HAL_BKP_MspInit(hbkp);

    /* Set BKP state */
    hbkp->State = HAL_BKP_STATE_READY;

    return (HAL_OK);
}


/**
 * @brief  DeInitializes the BKP peripheral
 * @param  hbkp pointer to a BKP_HandleTypeDef structure that contains
 *                the configuration information for BKP.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_BKP_DeInit(BKP_HandleTypeDef *hbkp)
{
    /* Check the RTC peripheral state */
    if (hbkp == NULL)
        return (HAL_ERROR);

    /* De-Initialize RTC MSP */
    HAL_BKP_MspDeInit(hbkp);

    /* Set hbkp State */
    __HAL_BKP_RESET_HANDLE_STATE(hbkp);

    return (HAL_OK);
}


/**
 * @brief  Initializes the BKP MSP.
 * @param  hbkp pointer to a BKP_HandleTypeDef structure that contains
 *                the configuration information for BKP.
 * @retval None
 */
__weak void HAL_BKP_MspInit(BKP_HandleTypeDef* hbkp)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(hbkp);


    /* NOTE : This function Should not be modified, when the callback is needed,
              the HAL_RTC_MspInit could be implemented in the user file
     */
}


/**
 * @brief  DeInitializes the BKP MSP.
 * @param  hbkp pointer to a BKP_HandleTypeDef structure that contains
 *                the configuration information for BKP.
 * @retval None
 */
__weak void HAL_BKP_MspDeInit(BKP_HandleTypeDef* hbkp)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(hbkp);


    /* NOTE : This function Should not be modified, when the callback is needed,
              the HAL_RTC_MspInit could be implemented in the user file
     */
}


/**
 * @}
 */


/** @defgroup BKP_Exported_Functions_Group2 Regfile write,read and clear functions
 *  @brief    Regfile write,read and clear
 *
   @verbatim
   ===============================================================================
 ##### Regfile write,read and clear functions #####
   ===============================================================================
   @endverbatim
 * @{
 */


/**
 * @brief  Regfile write.
 * @param  hbkp pointer to a BKP_HandleTypeDef structure that contains
 *                the configuration information for BKP.
 * @param  buff Pointer to data to be written.
 * @param  len specifices the length of the written data.
 * @param  mode specifices the BKP Regfile Type,contains BKP_REGFILE_NORMAL and BKP_REGFILE_SECURE.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_BKP_RegfileWrite(BKP_HandleTypeDef *hbkp, uint8_t *buff, uint8_t len, uint8_t mode)
{
    /* Check the parameters */
    assert_param(IS_BKP_REGFILE_TYPE(mode));

    /* Set BKP state */
    hbkp->State = HAL_BKP_STATE_BUSY;

    uint8_t     i;
    uint32_t    *p;
    uint32_t    *index = (uint32_t *) buff;
    switch (mode) {
    case BKP_REGFILE_SECURE: {
        p = (uint32_t *)(hbkp->Instance->KEYRF);
        if (len > 32)
            len = 32;
        for (i = 0; i < (len >> 2); i++) {
            WRITE_REG(*p, *index);
            p++;
            index++;
        }
        break;
    }
    case BKP_REGFILE_NORMAL: {
        p = (uint32_t *)(hbkp->Instance->NORMRF);
        if (len > 128)
            len = 128;
        for (i = 0; i < (len >> 2); i++) {
            WRITE_REG(*p, *index);
            p++;
            index++;
        }
        break;
    }
    default:
        break;
    }
    /* Set BKP state  */
    hbkp->State = HAL_BKP_STATE_READY;

    return (HAL_OK);
}


/**
 * @brief  Regfile read.
 * @param  hbkp pointer to a BKP_HandleTypeDef structure that contains
 *                the configuration information for BKP.
 * @param  buff Pointer to data to be put in.
 * @param  len specifices the length of the data to be put in.
 * @param  mode specifices the BKP Regfile Type,contains BKP_REGFILE_NORMAL and BKP_REGFILE_SECURE.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_BKP_RegfileRead(BKP_HandleTypeDef *hbkp, uint8_t *buff, uint8_t len, uint8_t mode)
{
    /* Check the parameters */
    assert_param(IS_BKP_REGFILE_TYPE(mode));

    /* Set BKP state */
    hbkp->State = HAL_BKP_STATE_BUSY;

    uint8_t     i;
    uint32_t    *p;
    uint32_t    *index = (uint32_t *) buff;
    switch (mode) {
    case BKP_REGFILE_SECURE: {
        p = (uint32_t *)(hbkp->Instance->KEYRF);
        if (len > 32)
            len = 32;
        for (i = 0; i < (len >> 2); i++) {
            *index = READ_REG(*p);
            p++;
            index++;
        }
        break;
    }
    case BKP_REGFILE_NORMAL: {
        p = (uint32_t *)(hbkp->Instance->NORMRF);
        if (len > 128)
            len = 128;
        for (i = 0; i < (len >> 2); i++) {
            *index = READ_REG(*p);
            p++;
            index++;
        }
        break;
    }
    default:
        break;
    }
    /* Set BKP state  */
    hbkp->State = HAL_BKP_STATE_READY;

    return (HAL_OK);
}


/**
 * @brief  KeyRegfile clear.
 * @param  hbkp pointer to a BKP_HandleTypeDef structure that contains
 *                the configuration information for BKP.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_BKP_KeyRegfileClear(BKP_HandleTypeDef *hbkp)
{
    /* Set BKP state */
    hbkp->State = HAL_BKP_STATE_BUSY;

    uint8_t     i;
    uint32_t    *p = (uint32_t *)(hbkp->Instance->KEYRF);

    for (i = 0; i < 8; i++) {
        CLEAR_REG(*p);
        p++;
    }

    /* Set BKP state */
    hbkp->State = HAL_BKP_STATE_READY;

    return (HAL_OK);
}


/**
 * @brief  NormalRegfile clear.
 * @param  hbkp pointer to a BKP_HandleTypeDef structure that contains
 *                the configuration information for BKP.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_BKP_NormalRegfileClear(BKP_HandleTypeDef *hbkp)
{
    /* Set BKP state */
    hbkp->State = HAL_BKP_STATE_BUSY;

    uint8_t     i;
    uint32_t    *p = (uint32_t *)(hbkp->Instance->NORMRF);

    for (i = 0; i < 32; i++) {
        CLEAR_REG(*p);
        p++;
    }

    /* Set BKP state */
    hbkp->State = HAL_BKP_STATE_READY;

    return (HAL_OK);
}


/**
 * @}
 */


/** @defgroup BKP_Exported_Functions_Group3 BKP Tamper Init and Self Detect Init
 *  @brief   Tamper Init and Self Detect Init functions
 *
   @verbatim
   ===============================================================================
 ##### Tamper Init and Self Detect Init functions #####
   ===============================================================================
   @endverbatim
 * @{
 */


/**
 * @brief  BKP Tamper Init.
 * @param  hbkp pointer to a BKP_HandleTypeDef structure that contains
 *                the configuration information for BKP.
 * @param  sTamper pointer to a BKP_TamperDetectionTypeDef structure
 *                that contains the configuration information for Tamper.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_BKP_TamperInit(BKP_HandleTypeDef *hbkp, BKP_TamperDetectionTypeDef * sTamper)
{
    /* Check the parameters */
    assert_param(IS_BKP_TAMPER_MODE(sTamper->TamperDetectMode));

    /* Set BKP state */
    hbkp->State = HAL_BKP_STATE_BUSY;

    /* Close isolation switch */
    LL_BKP_EnableVmain2Vbat(BKP_VMAIN);
    while (1) {
        if (LL_BKP_IsReady(BKP_VMAIN))
            break;
    }

    /* Open 32K */
    LL_RTC_Enable32kXtalClk();
    LL_BKP_ClearFlag_Tamper(LL_BKP_FLAG_All);

    if (sTamper->TamperDetectMode == BKP_TAMPER_STATIC) {
        /* Check the parameters */
        assert_param(IS_BKP_TAMPER_PIN(sTamper->Pin));
        assert_param(IS_BKP_TAMPER_GLITCH_FILTER_CLK(sTamper->FilterClockSource));
        assert_param(IS_BKP_TAMPER_GLITCH_FILTER_WIDTH(sTamper->FilterWidth));
        assert_param(IS_BKP_TAMPER_GLITCH_FILTER_EN(sTamper->FilterEn));
        assert_param(IS_BKP_TAMPER_PIN_SAMPLE_FREQ(sTamper->PinSampleFreq));
        assert_param(IS_BKP_TAMPER_PIN_SAMPLE_WIDTH(sTamper->PinSampleWidth));
        assert_param(IS_BKP_TAMPER_PIN_PULL_SEL(sTamper->PinPullSelect));
        assert_param(IS_BKP_TAMPER_PIN_PULL_EN(sTamper->PinPullEn));
        assert_param(IS_BKP_TAMPER_PIN_EXPECTDATA(sTamper->PinExpectData));
        assert_param(IS_BKP_TAMPER_PIN_POLARITY(sTamper->PinPolarity));
        assert_param(IS_BKP_TAMPER_PIN_HYSTER_SEL(sTamper->PinHysterSel));
        assert_param(IS_BKP_TAMPER_PIN_DIRECTION(sTamper->PinDirection));
        assert_param(IS_BKP_TAMPER_PIN_PASSFILTER_EN(sTamper->PinPassfilterEn));
        assert_param(IS_BKP_TAMPER_PIN_DRISTRENGTH_EN(sTamper->PinDriveStrength));
        assert_param(IS_BKP_TAMPER_PIN_SLEWRATE(sTamper->PinSlewRate));
        assert_param(IS_BKP_TAMPER_PIN_EN(sTamper->TamperPinEn));
        assert_param(IS_BKP_TAMEPER_INT(sTamper->TamperIntEn));

        /* Tamper soft reset */
        LL_BKP_SoftResetTamper();

        MODIFY_REG(hbkp->Instance->PCFG, BKP_ALLBIT_MSK, sTamper->PinHysterSel | sTamper->PinPassfilterEn \
                   | sTamper->PinDriveStrength | sTamper->PinSlewRate);

        uint32_t pin = sTamper->Pin;
        for (int i = 0; i < 8; i++) {
            if (pin & 0x01) {
                MODIFY_REG(hbkp->Instance->PxGF[i], BKP_ALLBIT_MSK, sTamper->FilterClockSource | sTamper->FilterWidth \
                           | sTamper->FilterEn | sTamper->PinSampleFreq | sTamper->PinSampleWidth | sTamper->PinPullSelect \
                           | sTamper->PinPullEn | sTamper->PinExpectData);
            }
            pin >>= 1;
        }
        WRITE_REG(hbkp->Instance->PLRTY, sTamper->PinPolarity);
        WRITE_REG(hbkp->Instance->PDIR, sTamper->PinDirection);

        __HAL_BKP_TAMPER_ENABLE(hbkp, (sTamper->TamperPinEn | BKP_TAMPER_EN_TDETCTE));
        __HAL_BKP_TAMPER_ENABLE_IT(hbkp, (sTamper->TamperIntEn | BKP_TAMPER_INTEN));
    } else if (sTamper->TamperDetectMode == BKP_TAMPER_DYNAMIC) {
        /* Check the parameters */
        assert_param(IS_BKP_TAMPER_PIN(sTamper->Pin));
        assert_param(IS_BKP_TAMPER_GLITCH_FILTER_CLK(sTamper->FilterClockSource));
        assert_param(IS_BKP_TAMPER_GLITCH_FILTER_WIDTH(sTamper->FilterWidth));
        assert_param(IS_BKP_TAMPER_GLITCH_FILTER_EN(sTamper->FilterEn));
        assert_param(IS_BKP_TAMPER_PIN_SAMPLE_FREQ(sTamper->PinSampleFreq));
        assert_param(IS_BKP_TAMPER_PIN_SAMPLE_WIDTH(sTamper->PinSampleWidth));
        assert_param(IS_BKP_TAMPER_PIN_PULL_SEL(sTamper->PinPullSelect));
        assert_param(IS_BKP_TAMPER_PIN_PULL_EN(sTamper->PinPullEn));
        assert_param(IS_BKP_TAMPER_PIN_EXPECTDATA(sTamper->PinExpectData));
        assert_param(IS_BKP_TAMPER_PIN_POLARITY(sTamper->PinPolarity));
        assert_param(IS_BKP_TAMPER_PIN_HYSTER_SEL(sTamper->PinHysterSel));
        assert_param(IS_BKP_TAMPER_PIN_DIRECTION(sTamper->PinDirection));
        assert_param(IS_BKP_TAMPER_PIN_PASSFILTER_EN(sTamper->PinPassfilterEn));
        assert_param(IS_BKP_TAMPER_PIN_DRISTRENGTH_EN(sTamper->PinDriveStrength));
        assert_param(IS_BKP_TAMPER_PIN_SLEWRATE(sTamper->PinSlewRate));
        assert_param(IS_BKP_TAMPER_PIN_EN(sTamper->TamperPinEn));
        assert_param(IS_BKP_TAMEPER_INT(sTamper->TamperIntEn));
        assert_param(IS_BKP_LFSR_INIT_VAL(sTamper->LFSRIntialValue));
        assert_param(IS_BKP_TAMPER_LFSR_TAP(sTamper->LFSRTap));
        assert_param(IS_TAMPER_LFSR_TYPE(sTamper->TamperActiveLFSRx));

        /* Tamper soft reset */
        LL_BKP_SoftResetTamper();

        MODIFY_REG(hbkp->Instance->PCFG, BKP_ALLBIT_MSK, sTamper->PinHysterSel | sTamper->PinPassfilterEn \
                   | sTamper->PinDriveStrength | sTamper->PinSlewRate);

        uint32_t pin = sTamper->Pin;
        for (int i = 0; i < 8; i++) {
            if (pin & 0x01) {
                MODIFY_REG(hbkp->Instance->PxGF[i], BKP_ALLBIT_MSK, sTamper->FilterClockSource | sTamper->FilterWidth \
                           | sTamper->FilterEn | sTamper->PinSampleFreq | sTamper->PinSampleWidth | sTamper->PinPullSelect \
                           | sTamper->PinPullEn | sTamper->PinExpectData);
            }
            pin >>= 1;
        }
        WRITE_REG(hbkp->Instance->PLRTY, sTamper->PinPolarity);
        WRITE_REG(hbkp->Instance->PDIR, sTamper->PinDirection);

        if (sTamper->TamperActiveLFSRx == BKP_TAMPER_ACTIVE_LFSR0) {
            CLEAR_BIT(hbkp->Instance->TAMLFSRDATA, BKP_TAMPER_LFSR_INIT_SEL);
            LL_BKP_EnableTamperActiveLfsr0();
            LL_BKP_SetTamperLfsrInitData(sTamper->LFSRIntialValue);
            LL_BKP_SetTamperActiveLfsr0Tap(sTamper->LFSRTap);
            LL_BKP_StartTamperLfsrSequence();
        } else if (sTamper->TamperActiveLFSRx == BKP_TAMPER_ACTIVE_LFSR1) {
            SET_BIT(hbkp->Instance->TAMLFSRDATA, BKP_TAMPER_LFSR_INIT_SEL);
            LL_BKP_EnableTamperActiveLfsr1();
            LL_BKP_SetTamperLfsrInitData(sTamper->LFSRIntialValue);
            LL_BKP_SetTamperActiveLfsr1Tap(sTamper->LFSRTap);
            LL_BKP_StartTamperLfsrSequence();
        } else if (sTamper->TamperActiveLFSRx == BKP_TAMPER_ACTIVE_LFSR01) {
            /* Selected by the user. CLEAR_BIT:LFSR0,SET_BIT:LFSR1.Default:LFSR0. */
//              CLEAR_BIT(hbkp->Instance->TAMLFSRDATA,BKP_TAMPER_LFSR_INIT_SEL);
//              SET_BIT(hbkp->Instance->TAMLFSRDATA,BKP_TAMPER_LFSR_INIT_SEL);

            LL_BKP_EnableTamperActiveLfsr0();
            LL_BKP_EnableTamperActiveLfsr1();
            LL_BKP_SetTamperLfsrInitData(sTamper->LFSRIntialValue);
            LL_BKP_SetTamperActiveLfsr0Tap(sTamper->LFSRTap);
            LL_BKP_StartTamperLfsrSequence();
        }

        __HAL_BKP_TAMPER_ENABLE(hbkp, (sTamper->TamperPinEn | BKP_TAMPER_EN_TDETCTE));
        __HAL_BKP_TAMPER_ENABLE_IT(hbkp, (sTamper->TamperIntEn | BKP_TAMPER_INTEN));
    }
    /* Set BKP state */
    hbkp->State = HAL_BKP_STATE_READY;

    return (HAL_OK);
}


/**
 * @brief  BKP Selt detect Init.
 * @param  hbkp pointer to a BKP_HandleTypeDef structure that contains
 *                the configuration information for BKP.
 * @param  sSelf pointer to a BKP_SelfDetectionTypeDef structure
 *                that contains the configuration information for Tamper.
 * @param  trim specifices the BKP Trim value.if Set trim=0, this register will hold default value.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_BKP_SelfDetectInit(BKP_HandleTypeDef *hbkp, BKP_SelfDetectionTypeDef *sSelf, uint32_t trim)
{
    /* Check the parameters */
    assert_param(IS_BKP_SELF_DET_MODE(sSelf->SelfDetectMode));
    assert_param(IS_BKP_SDET_HIGHFREQ_EN(sSelf->HighFreqEn));

    /* Set BKP state */
    hbkp->State = HAL_BKP_STATE_BUSY;

    /* Close isolation switch */
    LL_BKP_EnableVmain2Vbat(BKP_VMAIN);
    while (1) {
        if (LL_BKP_IsReady(BKP_VMAIN))
            break;
    }

    /* Open 32K */
    LL_RTC_Enable32kXtalClk();

    /* Tamper soft reset */
    LL_BKP_SoftResetTamper();

    if (sSelf->HighFreqEn == BKP_SDET_HIGHFREQ_ENABLE) {
        SET_BIT(BKP_VMAIN->TSTCLKEN, BKP_VMAIN_TSTCLKEN);
    }

    if (trim == 0) {
        sSelf->GlitchTrim       = (0x0U);
        sSelf->TemperHighTrim   = (0x10U);
        sSelf->TemperLowTrim    = (0x10U);
        sSelf->VolHighTrim      = (0x3U);
        sSelf->VolLowTrim       = (0x3U);
        sSelf->FreqHighTrim     = (0x3U);
        sSelf->FreqLowTrim      = (0x3U);

        MODIFY_REG(hbkp->Instance->TRIM, BKP_ALLBIT_MSK, ((sSelf->GlitchTrim << BKP_GLITCH_TRIM_POS) \
                   | (sSelf->TemperHighTrim << BKP_TEMPER_HIGH_TRIM_POS) | (sSelf->TemperLowTrim << BKP_TEMPER_LOW_TRIM_POS) \
                   | (sSelf->VolHighTrim << BKP_VOL_HIGH_TRIM_POS) | (sSelf->VolLowTrim << BKP_VOL_LOW_TRIM_POS) \
                   | (sSelf->FreqHighTrim << BKP_FREQ_HIGH_TRIM_POS) | (sSelf->FreqLowTrim)));
    } else {
        sSelf->GlitchTrim       = ((trim >> BKP_GLITCH_TRIM_POS) & (0x1U));
        sSelf->TemperHighTrim   = ((trim >> BKP_TEMPER_HIGH_TRIM_POS) & (0x1FU));
        sSelf->TemperLowTrim    = ((trim >> BKP_TEMPER_LOW_TRIM_POS) & (0x1FU));
        sSelf->VolHighTrim      = ((trim >> BKP_VOL_HIGH_TRIM_POS) & (0x3U));
        sSelf->VolLowTrim       = ((trim >> BKP_VOL_LOW_TRIM_POS) & (0x3U));
        sSelf->FreqHighTrim     = ((trim >> BKP_FREQ_HIGH_TRIM_POS) & (0x3U));
        sSelf->FreqLowTrim      = ((trim) & (0x3U));

        MODIFY_REG(hbkp->Instance->TRIM, BKP_ALLBIT_MSK, ((sSelf->GlitchTrim << BKP_GLITCH_TRIM_POS) \
                   | (sSelf->TemperHighTrim << BKP_TEMPER_HIGH_TRIM_POS) | (sSelf->TemperLowTrim << BKP_TEMPER_LOW_TRIM_POS) \
                   | (sSelf->VolHighTrim << BKP_VOL_HIGH_TRIM_POS) | (sSelf->VolLowTrim << BKP_VOL_LOW_TRIM_POS) \
                   | (sSelf->FreqHighTrim << BKP_FREQ_HIGH_TRIM_POS) | (sSelf->FreqLowTrim)));
    }

    if (sSelf->SelfDetectMode == BKP_SELF_DET_VOL) {
        /* Check the parameters */
        assert_param(IS_VOL_HIGH_TRIM(sSelf->VolHighTrim));
        assert_param(IS_VOL_LOW_TRIM(sSelf->VolLowTrim));
        assert_param(IS_BKP_VOL_DETECT_TIME(sSelf->VolDetectTime));
        assert_param(IS_BKP_VOL_DETECT_TIME_INTER(sSelf->VolDetectTimeInterval));
        assert_param(IS_BKP_VDET_AUTO_EN(sSelf->VolDetectAutoEn));
        assert_param(IS_TEMPER_HIGH_TRIM(sSelf->TemperHighTrim));
        assert_param(IS_TEMPER_LOW_TRIM(sSelf->TemperLowTrim));
        assert_param(IS_BKP_BGTEMPER_TYPE(sSelf->BGTemperMode));

        /* vol detection related regsisters config */
        hbkp->Instance->APPCFG  |= (sSelf->VolDetectTime << BKP_VOL_DET_TIME_POS);
        hbkp->Instance->APPCFG  |= (sSelf->VolDetectTimeInterval << BKP_VOL_DET_TIME_INTER_POS);
        hbkp->Instance->APPCFG  |= (BKP_VOLDET_AUTOEN);

        SET_BIT(hbkp->Instance->AUTOEN, sSelf->VolDetectAutoEn);

        if (sSelf->BGTemperMode == BKP_BGTEMPER_INTER)
            SET_BIT(hbkp->Instance->EN, BKP_TAMPER_EN_BGSWME);

        __HAL_BKP_TAMPER_ENABLE(hbkp, (BKP_TAMPER_EN_BGBUFE | BKP_TAMPER_EN_TDETCTE | BKP_TAMPER_EN_VDETCTE));
        __HAL_BKP_TAMPER_ENABLE_IT(hbkp, (BKP_ST_VOL_INTEN | BKP_TAMPER_INTEN | BKP_ST_TEMP_INTEN));
    } else if (sSelf->SelfDetectMode == BKP_SELF_DET_FREQ) {
        /* Check the parameters */
        assert_param(IS_FREQ_LOW_TRIM(sSelf->FreqLowTrim));
        assert_param(IS_FREQ_HIGH_TRIM(sSelf->FreqHighTrim));
        assert_param(IS_TEMPER_HIGH_TRIM(sSelf->TemperHighTrim));
        assert_param(IS_TEMPER_LOW_TRIM(sSelf->TemperLowTrim));
        assert_param(IS_BKP_BGTEMPER_TYPE(sSelf->BGTemperMode));

        if (sSelf->BGTemperMode == BKP_BGTEMPER_INTER)
            SET_BIT(hbkp->Instance->EN, BKP_TAMPER_EN_BGSWME);

        __HAL_BKP_TAMPER_ENABLE(hbkp, BKP_TAMPER_EN_TDETCTE | BKP_TAMPER_EN_FDETCTE);
        __HAL_BKP_TAMPER_ENABLE_IT(hbkp, (BKP_ST_FREQ_INTEN | BKP_TAMPER_INTEN | BKP_ST_TEMP_INTEN));
    } else if (sSelf->SelfDetectMode == BKP_SELF_DET_GLITCH) {
        /* Check the parameters */
        assert_param(IS_GLITCH_TRIM(sSelf->GlitchTrim));
        assert_param(IS_TEMPER_HIGH_TRIM(sSelf->TemperHighTrim));
        assert_param(IS_TEMPER_LOW_TRIM(sSelf->TemperLowTrim));
        assert_param(IS_BKP_BGTEMPER_TYPE(sSelf->BGTemperMode));

        if (sSelf->BGTemperMode == BKP_BGTEMPER_INTER)
            SET_BIT(hbkp->Instance->EN, BKP_TAMPER_EN_BGSWME);

        __HAL_BKP_TAMPER_ENABLE(hbkp, BKP_TAMPER_EN_TDETCTE | BKP_TAMPER_EN_GDETCTE);
        __HAL_BKP_TAMPER_ENABLE_IT(hbkp, (BKP_ST_GLITCH_INTEN | BKP_TAMPER_INTEN | BKP_ST_TEMP_INTEN));
    }

    /* Set BKP state */
    hbkp->State = HAL_BKP_STATE_READY;

    return (HAL_OK);
}


/**
 * @}
 */


/** @defgroup BKP_Exported_Functions_Group4 BKP Gpio function
 *  @brief   BKP Gpio Init,Set, clear and toggle pin function
 *
   @verbatim
   ===============================================================================
 ##### BKP Gpio function  #####
   ===============================================================================
   @endverbatim
 * @{
 */


/**
 * @brief  BKP Gpio Init.
 * @param  hbkp pointer to a BKP_HandleTypeDef structure that contains
 *              the configuration information for BKP.
 * @param  sGpio pointer to a BKP_GPIOTypeDef structure that contains
 *              the configuration information for GPIO.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_BKP_GPIOInit(BKP_HandleTypeDef *hbkp, BKP_GPIOTypeDef * sGpio)
{
    /* Check the parameters */
    assert_param(IS_BKP_GPIO_PIN_MODE(sGpio->GpioMode));

    /* Set BKP state */
    hbkp->State = HAL_BKP_STATE_BUSY;

    /* Close isolation switch */
    LL_BKP_EnableVmain2Vbat(BKP_VMAIN);
    while (1) {
        if (LL_BKP_IsReady(BKP_VMAIN))
            break;
    }

    LL_RTC_Enable32kXtalClk();
    LL_BKP_GPIO_Enable();

    if (sGpio->GpioMode == BKP_GPIO_PIN_GENERIC) {
        //assert_param(IS_BKP_GPIO_PIN(sGpio->Pin));
        assert_param(IS_BKP_GPIO_DIR(sGpio->Direction));

        uint32_t pin = sGpio->Pin;

        for (int i = 0; i < 8; i++) {
            if (pin & 0x1)
                SET_BIT(hbkp->Instance->GPIODIR, (sGpio->Direction << i));
            pin >>= 1;
        }
    } else if (sGpio->GpioMode == BKP_GPIO_PIN_RTC_STATUS) {
        assert_param(IS_BKP_RTC_STATUS_OUT(sGpio->Pin));
        assert_param(IS_PIN_INV_EN(sGpio->PinInvEn));
        assert_param(IS_PIN_OD_EN(sGpio->PinODEn));
        assert_param(IS_WAKUP_OUTPUT_TYPE(sGpio->WakupType));
        CLEAR_REG(RTC_TIME->WAKUPEN);

        SET_BIT(RTC_TIME->WAKUPCFG, RTC_TIME_WAKUPCFG_PINON | sGpio->PinInvEn | sGpio->PinODEn);
        SET_BIT(RTC_TIME->WAKUPEN, sGpio->WakupType);
    } else if (sGpio->GpioMode == BKP_GPIO_RTC_CLK) {
        assert_param(IS_BKP_RTC_CLK_OUT(sGpio->Pin));

        SET_BIT(RTC_TIME->CLKCFG, RTC_TIME_CLKCFG_32XTALEN | RTC_TIME_CLKCFG_CLKOUTPUT);
    } else if (sGpio->GpioMode == BKP_GPIO_DIG_SIG_OUT) {
        assert_param(IS_BKP_DIGSIG_OUT(sGpio->Pin));
        assert_param(IS_BKP_DIG_SIGNAL_TYPE(sGpio->DigSignalOutputType));

        SET_BIT(hbkp->Instance->PCFG, BKP_DIG_SIGNAL_OUT_EN | sGpio->DigSignalOutputType);
    }

    /* Set BKP state */
    hbkp->State = HAL_BKP_STATE_READY;

    return (HAL_OK);
}


/**
 * @brief  Sets BKP Gpio selected data port bit.
 * @param  hbkp pointer to a BKP_HandleTypeDef structure that contains
 *              the configuration information for BKP.
 * @param  sGpio pointer to a BKP_GPIOTypeDef structure that contains
 *              the configuration information for GPIO.
 * @note   sGpio->GpioMode must be BKP_GPIO_PIN_GENERIC
 * @retval None
 */
void HAL_BKP_GPIOSetPin(BKP_HandleTypeDef *hbkp, BKP_GPIOTypeDef * sGpio)
{
    /* Check the parameters */
    assert_param(IS_BKP_GPIO_PIN_MODE(sGpio->GpioMode));

    /* Set BKP state */
    hbkp->State = HAL_BKP_STATE_BUSY;

    if (sGpio->GpioMode == BKP_GPIO_PIN_GENERIC) {
        //assert_param(IS_BKP_GPIO_PIN(sGpio->Pin));
        LL_BKP_GPIO_SetOutputPin(sGpio->Pin);
    }

    /* Set BKP state */
    hbkp->State = HAL_BKP_STATE_READY;
}


/**
 * @brief  Clears BKP Gpio selected data port bit.
 * @param  hbkp pointer to a BKP_HandleTypeDef structure that contains
 *              the configuration information for BKP.
 * @param  sGpio pointer to a BKP_GPIOTypeDef structure that contains
 *              the configuration information for GPIO.
 * @note   sGpio->GpioMode must be BKP_GPIO_PIN_GENERIC
 * @retval None
 */
void HAL_BKP_GPIOClearPin(BKP_HandleTypeDef *hbkp, BKP_GPIOTypeDef * sGpio)
{
    /* Check the parameters */
    assert_param(IS_BKP_GPIO_PIN_MODE(sGpio->GpioMode));

    /* Set BKP state */
    hbkp->State = HAL_BKP_STATE_BUSY;

    if (sGpio->GpioMode == BKP_GPIO_PIN_GENERIC) {
        //assert_param(IS_BKP_GPIO_PIN(sGpio->Pin));
        LL_BKP_GPIO_ResetOutputPin(sGpio->Pin);
    }

    /* Set BKP state */
    hbkp->State = HAL_BKP_STATE_READY;
}


/**
 * @brief  Toggles BKP Gpio selected data port bit.
 * @param  hbkp pointer to a BKP_HandleTypeDef structure that contains
 *              the configuration information for BKP.
 * @param  sGpio pointer to a BKP_GPIOTypeDef structure that contains
 *              the configuration information for GPIO.
 * @note   sGpio->GpioMode must be BKP_GPIO_PIN_GENERIC
 * @retval None
 */
void HAL_BKP_GPIOTogglePin(BKP_HandleTypeDef *hbkp, BKP_GPIOTypeDef * sGpio)
{
    /* Check the parameters */
    assert_param(IS_BKP_GPIO_PIN_MODE(sGpio->GpioMode));

    /* Set BKP state */
    hbkp->State = HAL_BKP_STATE_BUSY;

    if (sGpio->GpioMode == BKP_GPIO_PIN_GENERIC) {
        //assert_param(IS_BKP_GPIO_PIN(sGpio->Pin));
        LL_BKP_GPIO_TogglePin(sGpio->Pin);
    }

    /* Set BKP state */
    hbkp->State = HAL_BKP_STATE_READY;
}


/**
 * @}
 */


/** @defgroup BKP_Exported_Functions_Group5 BKP Tamper Int function
 *  @brief   BKP Tamper IRQ handler and callback function
 *
   @verbatim
   ===============================================================================
 ##### BKP Tamper Int function #####
   ===============================================================================
   @endverbatim
 * @{
 */


/**
 * @brief  This function handles tamper interrupt request.
 * @param  hbkp pointer to a BKP_HandleTypeDef structure that contains
 *              the configuration information for BKP.
 * @retval None
 */
__weak void HAL_BKP_TamperIRQHandler(BKP_HandleTypeDef *hbkp)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(hbkp);


    /* NOTE : This function Should not be modified, when the callback is needed,
              the HAL_RTC_MspInit could be implemented in the user file
     */
}


/**
 * @brief  tamper interrupt callback.
 * @param  hbkp pointer to a BKP_HandleTypeDef structure that contains
 *              the configuration information for BKP.
 * @retval None
 */
__weak void HAL_BKP_TamperEventCallback(BKP_HandleTypeDef *hbkp)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(hbkp);


    /* NOTE : This function Should not be modified, when the callback is needed,
              the HAL_RTC_MspInit could be implemented in the user file
     */
}


/**
 * @}
 */


/** @defgroup BKP_Exported_Functions_Group6 Peripheral State function
 *  @brief   Peripheral State function
 *
   @verbatim
   ===============================================================================
 ##### Peripheral State function #####
   ===============================================================================
   @endverbatim
 * @{
 */


/**
 * @brief  Peripheral State function.
 * @param  hbkp pointer to a BKP_HandleTypeDef structure that contains
 *              the configuration information for BKP.
 * @retval None
 */
HAL_BKP_StateTypeDef HAL_BKP_GetState(BKP_HandleTypeDef *hbkp)
{
    return (hbkp->State);
}


/**
 * @}
 */


/**
 * @}
 */


/**
 * @}
 */

#endif /* BKP_MODULE_ENABLED */


/**
 * @}
 */

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/

