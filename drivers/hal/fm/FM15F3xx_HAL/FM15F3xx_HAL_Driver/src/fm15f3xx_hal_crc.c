/**
 ******************************************************************************
 * @file    fm15f3xx_hal_crc.c
 * @author  WYL
 * @brief   CRC HAL module driver.
 *          This file provides firmware functions to manage the following
 *          functionalities of the Cyclic Redundancy Check (CRC) peripheral:
 *           + Initialization and de-initialization functions
 *           + Peripheral Control functions
 *           + Peripheral State functions
 *
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "fm15f3xx_hal.h"


/** @addtogroup FM15F3xx_HAL_Driver
 * @{
 */


/** @addtogroup CRC
 * @{
 */

#ifdef CRC_MODULE_ENABLED

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
static uint16_t __CRC16_Calculation(LL_CRC_InitTypeDef* CRC_InitStrcuct, uint8_t *pData, uint32_t DataLength);


/* Private functions ---------------------------------------------------------*/
/* Exported functions --------------------------------------------------------*/


/** @addtogroup CRC_Exported_Functions
 * @{
 */


/** @addtogroup CRC_Exported_Functions_Group1
 *  @brief   Initialization and de-initialization functions
 *
   @verbatim
   ==============================================================================
 ##### Initialization and de-initialization functions #####
   ==============================================================================
   @endverbatim
 * @{
 */


/**
 * @brief  Initializes the CRC16 according to the specified
 *         parameters in the CRC_InitTypeDef and creates the associated handle.
 * @param  hcrc pointer to a CRC_HandleTypeDef structure that contains
 *         the configuration information for CRC
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_CRC_Init(CRC_HandleTypeDef *hcrc)
{
    /* Check the CRC handle allocation */
    if (hcrc == NULL) {
        return (HAL_ERROR);
    }

    /* Check the parameters */
    assert_param(IS_CRC_INSTANCE(hcrc->Instance));

    if (hcrc->State == HAL_CRC_STATE_RESET) {
        /* Init the low level hardware */
        HAL_CRC_MspInit(hcrc);
    }

    /* Change CRC peripheral state */
    hcrc->State = HAL_CRC_STATE_READY;

    /* Return function status */
    return (HAL_OK);
}


/**
 * @brief  DeInitializes the CRC peripheral.
 * @param  hcrc pointer to a CRC_HandleTypeDef structure that contains
 *         the configuration information for CRC
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_CRC_DeInit(CRC_HandleTypeDef *hcrc)
{
    /* Check the CRC handle allocation */
    if (hcrc == NULL) {
        return (HAL_ERROR);
    }

    /* Check the parameters */
    assert_param(IS_CRC_INSTANCE(hcrc->Instance));

    /* Change CRC peripheral state */
    hcrc->State = HAL_CRC_STATE_BUSY;

    /* DeInit the low level hardware */
    HAL_CRC_MspDeInit(hcrc);

    /* Change CRC peripheral state */
    hcrc->State = HAL_CRC_STATE_RESET;

    /* Return function status */
    return (HAL_OK);
}


/**
 * @brief  Initializes the CRC MSP.
 * @param  hcrc pointer to a CRC_HandleTypeDef structure that contains
 *         the configuration information for CRC
 * @retval None
 */
__weak void HAL_CRC_MspInit(CRC_HandleTypeDef *hcrc)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(hcrc);


    /* NOTE : This function Should not be modified, when the callback is needed,
              the HAL_CRC_MspInit could be implemented in the user file
     */
}


/**
 * @brief  DeInitializes the CRC MSP.
 * @param  hcrc pointer to a CRC_HandleTypeDef structure that contains
 *         the configuration information for CRC
 * @retval None
 */
__weak void HAL_CRC_MspDeInit(CRC_HandleTypeDef *hcrc)
{
    CRC->CFG = 0x00;
}


/**
 * @}
 */


/** @addtogroup CRC_Exported_Functions_Group2
 *  @brief   Peripheral Control functions
 *
   @verbatim
   ==============================================================================
 ##### Peripheral Control functions #####
   ==============================================================================
   @endverbatim
 * @{
 */


/**
 * @brief  Computes the 16-bit CRC of 32-bit data buffer independently
 *         of the previous CRC value.
 * @param  hcrc pointer to a CRC_HandleTypeDef structure that contains
 *         the configuration information for CRC
 * @param  pBuffer Pointer to the buffer containing the data to be computed
 * @param  BufferLength Length of the buffer to be computed
 * @retval 16-bit CRC
 */
uint16_t HAL_CRC16_Calculate(CRC_HandleTypeDef *hcrc, uint8_t* pBuffer, uint32_t BufferLength)
{
    uint16_t crc16 = 0U;

    /* Change CRC peripheral state */
    hcrc->State = HAL_CRC_STATE_BUSY;

    /* Enter Data to the CRC calculator */
    crc16 = __CRC16_Calculation(&hcrc->Init, pBuffer, BufferLength);

    /* Change CRC peripheral state */
    hcrc->State = HAL_CRC_STATE_READY;

    /* Return the CRC computed value */
    return (crc16);
}


/**
 * @}
 */


/** @addtogroup CRC_Exported_Functions_Group3
 *  @brief   Peripheral State functions
 *
   @verbatim
   ==============================================================================
 ##### Peripheral State functions #####
   ==============================================================================
    [..]
    This subsection permits to get in run-time the status of the peripheral
    and the data flow.

   @endverbatim
 * @{
 */


/**
 * @brief  Returns the CRC state.
 * @param  hcrc pointer to a CRC_HandleTypeDef structure that contains
 *         the configuration information for CRC
 * @retval HAL state
 */
HAL_CRC_StateTypeDef HAL_CRC_GetState(CRC_HandleTypeDef *hcrc)
{
    return (hcrc->State);
}


/**
 * @brief Deinitialize the CRC configuration register to their default reset values.
 * @param None
 * @retval None
 */
static void __CRC_DeInit(void)
{
    CRC->CFG = 0x00;
}


/**
 * @brief Get the CRC checksum from the given data
 * @param CRC_InitTypeDef: CRC parameter struct pointer
 * @retval The checksum value
 */
static uint16_t __CRC16_Process(LL_CRC_InitTypeDef* CRC_IntiStrcuct, uint8_t *pData, uint32_t DataLength)
{
    uint8_t *data = pData;

    uint16_t    crc_data        = 0;
    uint32_t    word_cnt        = DataLength >> 2;
    uint8_t     last_byte_cnt   = DataLength & 0x03;

    __CRC_DeInit();

    CRC->DATA = CRC_IntiStrcuct->Init;

    if (FALSE == CRC_IntiStrcuct->RefIn)
        LL_CRC_SetInputReverse(CRC, LL_CRC_REVERSE_BYTE);
    else
        LL_CRC_SetInputReverse(CRC, LL_CRC_REVERSE_BITinWORD);

    LL_CRC_SetInputDataWidth(CRC, LL_CRC_DATAWIDTH_32);

    LL_CRC_Start(CRC);

    while (word_cnt--) {
        CRC->DATA_IN    = *(unsigned int *) data;
        data            += 4;
    }

    if (FALSE == CRC_IntiStrcuct->RefIn)
        LL_CRC_SetInputReverse(CRC, LL_CRC_REVERSE_NO);
    else
        LL_CRC_SetInputReverse(CRC, LL_CRC_REVERSE_BITinBYTE);

    LL_CRC_SetInputDataWidth(CRC, LL_CRC_DATAWIDTH_8);

    while (last_byte_cnt--) {
        CRC->DATA_IN = *(data++);
    }

    switch (CRC_IntiStrcuct->RefOut) {
    case FALSE:
        crc_data = CRC->DATA0;
        break;
    default:
        crc_data = CRC->DATA2;
        break;
    }

    crc_data = crc_data ^ CRC_IntiStrcuct->XorOut;

    LL_CRC_Stop(CRC);

    return (crc_data);
}


/**
 * @brief Get the CRC16 from the given data
 * @param CRC_InitTypeDef: CRC parameter struct pointer
 * @retval The checksum value
 */
static uint16_t __CRC16_Calculation(LL_CRC_InitTypeDef* CRC_InitStrcuct, uint8_t *pData, uint32_t DataLength)
{
    return (__CRC16_Process(CRC_InitStrcuct, pData, DataLength));
}


/**
 * @}
 */


/**
 * @}
 */

#endif /* CRC_MODULE_ENABLED */


/**
 * @}
 */


/**
 * @}
 */

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
