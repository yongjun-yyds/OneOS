/**
 ******************************************************************************
 * @file    fm15f3xx_ll_ct.c
 * @author  WYL
 * @version V1.0.0
 * @date    2020-03-17
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */
/* Includes ------------------------------------------------------------------*/
#include "fm15f3xx_ll_ct.h"


/** @addtogroup FM15F3XX_LL_Driver
 * @{
 */

#if defined (CT0) || defined (CT1)


/** @addtogroup CT_LL
 * @{
 */


/**
 * @brief  Set each @ref LL_CT_StructInit field to default value.
 * @param  CT_StructInit pointer to a @ref LL_CT_StructInit structure
 * whose fields will be set to default values.
 * @retval None
 */
void LL_CT_StructInit(CT_InitTypeDef *CT_InitStruct)
{
    CT_InitStruct->IoModeSel            = LL_CT_7816;
    CT_InitStruct->OneShotEn            = LL_CT_ONESHOT_ENABLE;
    CT_InitStruct->ClkMode              = LL_CT_CLK_MASTER;
    CT_InitStruct->RstMode              = LL_CT_RST_MASTER;
    CT_InitStruct->FreqA                = 2;
    CT_InitStruct->FreqB                = 4; //若CT_CLK=12MHZ，12M/(2+(4+1)/16+1)=3.62MHz
    CT_InitStruct->DataOrder            = LL_CT_ORDER_LSB;
    CT_InitStruct->TxRepeatNum          = 5;
    CT_InitStruct->RxRepeatNum          = 5;
    CT_InitStruct->RxGuardTimeNum       = 1;
    CT_InitStruct->TxGuardTimeNum       = 2;
    CT_InitStruct->ParityMode           = LL_CT_PARITY_EVEN;
    CT_InitStruct->RxCheckParity        = LL_CT_RxPARCHECK_DISABLE;
    CT_InitStruct->IoCollerrNum         = 0;
    CT_InitStruct->Fi                   = 1;
    CT_InitStruct->Di                   = 1;
    CT_InitStruct->ExtraGuardTimeNum    = 0;
    CT_InitStruct->ClkOutput            = LL_CT_CLKOUT_CLK0;
    CT_InitStruct->RstCfgReg            = LL_CT_CFGREG_RST;
    CT_InitStruct->RstStateReg          = LL_CT_STATEREG_RST;
    CT_InitStruct->TxFlagMode           = LL_CT_TxFIFOEMPTY;
    CT_InitStruct->TxRxMode             = LL_CT_TxRxMODE_SEQ;
}


/**
 * @brief  Initialize the CT according to the specified
 *         parameters in the CT_InitTypeDef
 * @param  Device Pointer to CT instance
 * @param  CT_InitTypeDef structure
 * @retval HAL status
 */
void LL_CT_Init(CT_TypeDef *CTx, CT_InitTypeDef *CT_InitStruct)
{
    //CGF_IO
    MODIFY_REG(CTx->IO_CFG, 0x0F1F00FF,
               CT_InitStruct->IoModeSel |
               CT_InitStruct->OneShotEn |
               CT_InitStruct->ClkMode |
               CT_InitStruct->RstMode |
               (CT_InitStruct->FreqA << CT_CFGIO_CLKFRQCFGA_Pos) |
               (CT_InitStruct->FreqB << CT_CFGIO_CLKFRQCFGB_Pos));
    //CFG_TRANS
    MODIFY_REG(CTx->CFG, 0xFFFFFFFF,
               CT_InitStruct->DataOrder |
               (CT_InitStruct->TxRepeatNum << CT_CFGTRNS_TXREPCFG_Pos) |
               (CT_InitStruct->RxRepeatNum << CT_CFGTRNS_RXREPCFG_Pos) |
               (CT_InitStruct->RxGuardTimeNum << CT_CFGTRNS_RXGTCFG_Pos) |
               (CT_InitStruct->TxGuardTimeNum << CT_CFGTRNS_TXGTCFG_Pos) |
               CT_InitStruct->ParityMode |
               CT_InitStruct->RxCheckParity |
               (CT_InitStruct->IoCollerrNum << CT_CFGTRNS_IOCLCFG_Pos) |
               (CT_InitStruct->Fi << CT_CFGTRNS_FI_Pos) |
               (CT_InitStruct->Di << CT_CFGTRNS_DI_Pos) |
               (CT_InitStruct->ExtraGuardTimeNum << CT_CFGTRNS_TXEGT_Pos));
    //CTRL_IO
    MODIFY_REG(CTx->IO_CTRL, 0x2F,
               CT_InitStruct->ClkOutput |
               CT_InitStruct->RstCfgReg |
               CT_InitStruct->RstStateReg);
    //CTRL_TRANS
    MODIFY_REG(CTx->CTRL, 0xF,
               CT_InitStruct->TxFlagMode |
               CT_InitStruct->TxRxMode);
}


/**
 * @}CT_LL
 */

#endif /* CT */


/**
 * @}FM15F3XX_LL_Driver
 */

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
