/**
 ******************************************************************************
 * @file    fm15f3xx_ll_wdt.c
 * @author  SRG
 * @version V1.0.0
 * @date    2020-03-17
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */
#include "fm15f3xx_ll_wdt.h"


/**
 * @brief  Enable WDT Reset.
 * @retval Null
 */
void LL_WDT_EnableReset(void)
{
    MODIFY_REG(ALM_MONITOR->GROUPC2, ALM_GROUPC2_C11, (uint32_t)(0x90) << ALM_GROUPC2_C11_Pos);     //Enable reset
}


/**
 * @brief  Enable WDT Interrupt.
 * @retval Null
 */
void LL_WDT_EnableInt(void)
{
    MODIFY_REG(ALM_MONITOR->GROUPC2, ALM_GROUPC2_C11, (uint32_t)(0x88) << ALM_GROUPC2_C11_Pos);     //Enable Int
}


/**
 * @brief  Initializes the WDT according to the specified parameters in the HAL_ALM_SensorsInit.
 * @param  WDTx  pointer to a WDT structure that contains
 *                the configuration information for the specified WDT module.
 * @param  Period WDT Count Period : 2^Period/Count_Clk;
            This parameter can be combination of the following values:
 *         @arg @ref LL_WDT_PERIOD_8
 *         @arg @ref LL_WDT_PERIOD_12
 *         @arg @ref LL_WDT_PERIOD_15
 *         @arg @ref LL_WDT_PERIOD_18
 *         @arg @ref LL_WDT_PERIOD_21
 *         @arg @ref LL_WDT_PERIOD_24
 *         @arg @ref LL_WDT_PERIOD_27
 *         @arg @ref LL_WDT_PERIOD_30

 * @retval
 */
void LL_WDT_Init(WDT_TypeDef *WDTx, uint32_t Period)
{
    LL_WDT_EnableReset();
    LL_WDT_SetPeriod(WDTx, Period);
}


/**
 * @brief  Start the WDT.
 * @param  hiwdg  pointer to a WDT_HandleTypeDef structure that contains
 *                the configuration information for the specified WDT module.
 * @retval Null
 */
void LL_WDT_Start(WDT_TypeDef *WDTx)
{
    LL_WDT_Enable(WDTx);
}


/**
 * @brief  Refresh the WDT.
 * @param  WDTx  pointer to a WDT structure that contains
 *                the configuration information for the specified WDT module.
 * @retval Null
 */
void LL_WDT_FeedDog(WDT_TypeDef *WDTx)
{
    LL_WDT_Reset(WDTx);
}


/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
