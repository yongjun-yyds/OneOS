/**
 ******************************************************************************
 * @file    fm15f3xx_ll_pmu.c
 * @author  SRG
 * @version V1.0.0
 * @date    2020-03-17
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */
#include "fm15f3xx_ll_pmu.h"
#include "fm15f3xx_ll_flash.h"

static uint32_t LP_mode = LL_PMU_LP_POWERDOWN;

void LL_PMU_Init(LL_PMU_InitTypeDef *PMU_InitStruct)
{
    LP_mode = PMU_InitStruct->LP_mode;

    LL_LLWU_ConfigWakeupPad(PMU_InitStruct->LLWU.PADSource, PMU_InitStruct->LLWU.PADEdgeSel);
    LL_LLWU_EnablePad(PMU_InitStruct->LLWU.PADSource);

    LL_LLWU_ConfigWakeupIntf(PMU_InitStruct->LLWU.INTFSource, PMU_InitStruct->LLWU.INTFEdgeSel);
    LL_LLWU_EnableIntf(PMU_InitStruct->LLWU.INTFSource);

    if (LL_PMU_LP_STANDBY == LP_mode) {
        //STBY CFG
        if (PMU_InitStruct->STBY.PADHoldEn)
            LL_PMU_EnablePadHold();
        else
            LL_PMU_ReleasePadHold();

        if (LL_PMU_STBY_LDO16_CLOSE == PMU_InitStruct->STBY.LDO16CFG)
            LL_PSTBY_DisableLDO16();
        else
            LL_PSTBY_EnableLDO16();
    } else if (LL_PMU_LP_STOP == LP_mode) {
        //STOP CFG
        if (LL_PSTOP_LDO12ENLP_NORMAL == PMU_InitStruct->STOP.LDO12En)
            LL_PSTOP_DisableLDO12LP();
        else
            LL_PSTOP_EnableLDO12LP();

        LL_PSTOP_SetLDO12(PMU_InitStruct->STOP.LDO12CFG);

        if (LL_PSTOP_LDO16ENLP_NORMAL == PMU_InitStruct->STOP.LDO16En)
            LL_PSTOP_DisableLDO16LP();
        else
            LL_PSTOP_EnableLDO16LP();
    }
    LL_WEC_EnableWakeupA(PMU_InitStruct->WEC.ModuleASource);
    LL_WEC_EnableWakeupB(PMU_InitStruct->WEC.ModuleBSource);
}


void LL_PMU_EnterLowpowerMode(void)
{
    LL_LLWU_Enable();
    switch (LP_mode) {
    case LL_PMU_LP_POWERDOWN:
        LL_PMU_SetDeepSleepMode(LL_PMU_DPSLP_PWRDOWN);
        LL_LPM_EnableDeepSleep();
        LL_PMU_EnableiLoadAutoCTL();
        break;
    case LL_PMU_LP_STANDBY:
        LL_PMU_SetDeepSleepMode(LL_PMU_DPSLP_STDBY);
        LL_LPM_EnableDeepSleep();
        LL_PMU_DisableiLoad();
        break;
    case LL_PMU_LP_STOP:
        LL_PMU_SetSleepMode(LL_PMU_SLP_STOP);
        LL_LPM_EnableSleep();
        LL_PMU_DisableiLoad();
        break;
    case LL_PMU_LP_WAIT:
        LL_PMU_SetSleepMode(LL_PMU_SLP_WAIT);
        LL_LPM_EnableSleep();
        break;
    default:
        return;
    }
    LL_PMU_SetLDOReadyCnt();
    NVIC_EnableIRQ(PM_Int_IRQn);
    __WFI();
}


/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/

