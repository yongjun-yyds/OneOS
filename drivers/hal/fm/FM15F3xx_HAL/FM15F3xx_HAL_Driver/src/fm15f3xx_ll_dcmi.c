/**
 ******************************************************************************
 * @file    fm15f3xx_ll_dcmi.c
 * @author  WYL
 * @version V1.0.0
 * @date    2020-03-17
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */
/* Define to prevent recursive inclusion -------------------------------------*/

/* Includes ------------------------------------------------------------------*/
#include "fm15f3xx_ll_dcmi.h"


/** @addtogroup FM15F3XX_LL_Driver
 * @{
 */


/** @defgroup DCMI
 * @brief DCMI driver modules
 * @{
 */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/


/** @defgroup DCMI_Private_Functions
 * @{
 */


/** @defgroup DCMI_Group1 Initialization and Configuration functions
 *  @brief   Initialization and Configuration functions
 *
   @verbatim
   ===============================================================================
 ##### Initialization and Configuration functions #####
   ===============================================================================

   @endverbatim
 * @{
 */


/**
 * @brief  Deinitializes the DCMI registers to their default reset values.
 * @param  None
 * @retval None
 */
void DCMI_DeInit(void)
{
    DCMI->CFG       = 0x0;
    DCMI->INTEN     = 0x0;
    DCMI->ICL       = 0x1F;
    DCMI->ESC       = 0x0;
    DCMI->ESUC      = 0x0;
    DCMI->CWSTRT    = 0x0;
    DCMI->CWSIZE    = 0x0;
}


/**
 * @brief  Initializes the DCMI according to the specified parameters in the DCMI_InitStruct.
 * @param  DCMI_InitStruct: pointer to a DCMI_InitTypeDef structure that contains
 *         the configuration information for the DCMI.
 * @retval None
 */
void DCMI_Init(DCMI_InitTypeDef* DCMI_InitStruct)
{
    uint32_t temp = 0x0;


    /* The DCMI configuration registers should be programmed correctly before
       enabling the CFG_ENABLE Bit and the CFG_CAPTURE Bit */
    DCMI->CFG &= ~(DCMI_CFG_ENABLE | DCMI_CFG_CAPTURE | DCMI_CFG_MAPEN);

    /* Reset the old DCMI configuration */
    temp = DCMI->CFG;

    temp &= ~((uint32_t) DCMI_CFG_CM | DCMI_CFG_ESS | DCMI_CFG_PCKOR |
              DCMI_CFG_HSPOL | DCMI_CFG_VSPOL | DCMI_CFG_FCRC |
              DCMI_CFG_DWM | DCMI_CFG_BSELMODE | DCMI_CFG_BSEL |
              DCMI_CFG_LSELMODE | DCMI_CFG_LSEL | DCMI_CFG_SHIFTNUM |
              DCMI_CFG_SYNCL | DCMI_CFG_SYNCEDGESEL);

    /* Sets the new configuration of the DCMI peripheral */
    temp |= ((uint32_t) DCMI_InitStruct->DCMI_CaptureMode |
             DCMI_InitStruct->DCMI_SynchroMode |
             DCMI_InitStruct->DCMI_PCKPolarity |
             DCMI_InitStruct->DCMI_VSPolarity |
             DCMI_InitStruct->DCMI_HSPolarity |
             DCMI_InitStruct->DCMI_CaptureRate |
             DCMI_InitStruct->DCMI_DataWidthMode |
             DCMI_InitStruct->DCMI_ByteSelMode |
             DCMI_InitStruct->DCMI_ByteSel |
             DCMI_InitStruct->DCMI_LineSelMode |
             DCMI_InitStruct->DCMI_LineSel |
             DCMI_InitStruct->DCMI_SyncEdgeSel
            );

    DCMI->CFG = temp;
}


/**
 * @brief  Fills each DCMI_InitStruct member with its default value.
 * @param  DCMI_InitStruct : pointer to a DCMI_InitTypeDef structure which will
 *         be initialized.
 * @retval None
 */
void DCMI_StructInit(DCMI_InitTypeDef* DCMI_InitStruct)
{
    /* Set the default configuration */
    DCMI_InitStruct->DCMI_CaptureMode   = DCMI_CAPTUREMODE_CONTINUOUS;
    DCMI_InitStruct->DCMI_SynchroMode   = DCMI_SYNCHROMODE_HARDWARE;
    DCMI_InitStruct->DCMI_PCKPolarity   = DCMI_PCKPOLARITY_FALLING;
    DCMI_InitStruct->DCMI_VSPolarity    = DCMI_VSPOLARITY_LOW;
    DCMI_InitStruct->DCMI_HSPolarity    = DCMI_HSPOLARITY_LOW;
    DCMI_InitStruct->DCMI_CaptureRate   = DCMI_CAPTURERATE_ALL_FRAME;
    DCMI_InitStruct->DCMI_DataWidthMode = DCMI_DATAWIDTH_8B;

    DCMI_InitStruct->DCMI_ByteSelMode   = DCMI_BYTESELMODE_ALL;
    DCMI_InitStruct->DCMI_ByteSel       = DCMI_BYTESEL_FIRST;
    DCMI_InitStruct->DCMI_LineSelMode   = DCMI_LINESELMODE_ALL;
    DCMI_InitStruct->DCMI_LineSel       = DCMI_LINESEL_FIRST;
    DCMI_InitStruct->DCMI_SyncEdgeSel   = DCMI_SYNCEDGESEL_NEGEDGE;
}


/**
 * @brief  Initializes the DCMI sync limit according to the specified
 * @note   This function should be called before to enable and start the DCMI interface.
 * @param  DCMI_SyncLimit:  Hardware sample the dcmi_d when the sample_cnt count to csr_sync_limit.
 * @retval None
 */
void DCMI_SyncLimitConfig(uint8_t DCMI_SyncLimit)
{
    /* Sets the sync limit */
    DCMI->CFG = (uint32_t)((uint32_t) DCMI_SyncLimit << DCMI_CFG_SYNCL_Pos);
}


/**
 * @brief  Initializes the DCMI sync limit according to the specified
 * @note   This function should be called before to enable and start the DCMI interface.
 * @param  DCMI_ShiftNum:
 * @retval None
 */
void DCMI_ShiftNumConfig(uint8_t DCMI_ShiftNum)
{
    /* Sets the shift num */
    DCMI->CFG = (uint32_t)((uint32_t) DCMI_ShiftNum << DCMI_CFG_SHIFTNUM_Pos);
}


/**
 * @brief  Initializes the DCMI peripheral CROP mode according to the specified
 *         parameters in the DCMI_CROPInitStruct.
 * @note   This function should be called before to enable and start the DCMI interface.
 * @param  DCMI_CROPInitStruct:  pointer to a DCMI_CROPInitTypeDef structure that
 *         contains the configuration information for the DCMI peripheral CROP mode.
 * @retval None
 */
void DCMI_CROPConfig(DCMI_CROPInitTypeDef* DCMI_CROPInitStruct)
{
    /* Sets the CROP window coordinates */
    DCMI->CWSTRT = (uint32_t)((uint32_t) DCMI_CROPInitStruct->DCMI_HorizontalOffsetCount |
                              ((uint32_t) DCMI_CROPInitStruct->DCMI_VerticalStartLine << 16));

    /* Sets the CROP window size */
    DCMI->CWSIZE = (uint32_t)(DCMI_CROPInitStruct->DCMI_CaptureCount |
                              ((uint32_t) DCMI_CROPInitStruct->DCMI_VerticalLineCount << 16));
}


/**
 * @brief  Enables or disables the DCMI Crop feature.
 * @note   This function should be called before to enable and start the DCMI interface.
 * @param  NewState: new state of the DCMI Crop feature.
 *          This parameter can be: ENABLE or DISABLE.
 * @retval None
 */
void DCMI_CROPCmd(FunctionalState NewState)
{
    if (NewState != DISABLE) {
        /* Enable the DCMI Crop feature */
        DCMI->CFG |= (uint32_t) DCMI_CFG_CROP;
    } else {
        /* Disable the DCMI Crop feature */
        DCMI->CFG &= ~(uint32_t) DCMI_CFG_CROP;
    }
}


/**
 * @brief  Sets the embedded synchronization codes
 * @param  DCMI_CodesInitTypeDef: pointer to a DCMI_CodesInitTypeDef structure that
 *         contains the embedded synchronization codes for the DCMI peripheral.
 * @retval None
 */
void DCMI_SetEmbeddedSynchroCodes(DCMI_CodesInitTypeDef* DCMI_CodesInitStruct)
{
    DCMI->ESC = (uint32_t)(DCMI_CodesInitStruct->DCMI_FrameStartCode |
                           ((uint32_t) DCMI_CodesInitStruct->DCMI_LineStartCode << 8) |
                           ((uint32_t) DCMI_CodesInitStruct->DCMI_LineEndCode << 16) |
                           ((uint32_t) DCMI_CodesInitStruct->DCMI_FrameEndCode << 24));
}


/**
 * @brief  Enables or disables the DCMI JPEG format.
 * @note   The Crop and Embedded Synchronization features cannot be used in this mode.
 * @param  NewState: new state of the DCMI JPEG format.
 *          This parameter can be: ENABLE or DISABLE.
 * @retval None
 */
void DCMI_JPEGCmd(FunctionalState NewState)
{
    if (NewState != DISABLE) {
        /* Enable the DCMI JPEG format */
        DCMI->CFG |= (uint32_t) DCMI_CFG_JPEG;
    } else {
        /* Disable the DCMI JPEG format */
        DCMI->CFG &= ~(uint32_t) DCMI_CFG_JPEG;
    }
}


/**
 * @}
 */


/** @defgroup DCMI_Group2 Image capture functions
 *  @brief   Image capture functions
 *
   @verbatim
   ===============================================================================
 ##### Image capture functions #####
   ===============================================================================

   @endverbatim
 * @{
 */


/**
 * @brief  Enables or disables the DCMI interface.
 * @param  NewState: new state of the DCMI interface.
 *          This parameter can be: ENABLE or DISABLE.
 * @retval None
 */
void DCMI_Cmd(FunctionalState NewState)
{
    if (NewState != DISABLE) {
        /* Enable the DCMI by setting ENABLE bit */
        DCMI->CFG |= (uint32_t) DCMI_CFG_ENABLE;
    } else {
        /* Disable the DCMI by clearing ENABLE bit */
        DCMI->CFG &= ~(uint32_t) DCMI_CFG_ENABLE;
    }
}


/**
 * @brief  Enables or disables the DCMI Capture.
 * @param  NewState: new state of the DCMI capture.
 *          This parameter can be: ENABLE or DISABLE.
 * @retval None
 */
void DCMI_CaptureCmd(FunctionalState NewState)
{
    if (NewState != DISABLE) {
        /* Enable the DCMI Capture */
        DCMI->CFG |= (uint32_t) DCMI_CFG_CAPTURE;
    } else {
        /* Disable the DCMI Capture */
        DCMI->CFG &= ~(uint32_t) DCMI_CFG_CAPTURE;
    }
}


/**
 * @brief  Enables or disables the DCMI Map.
 * @param  NewState: new state of the DCMI map.
 *          This parameter can be: ENABLE or DISABLE.
 * @retval None
 */
void DCMI_MapCmd(FunctionalState NewState)
{
    if (NewState != DISABLE) {
        /* Enable the DCMI Map */
        DCMI->CFG |= (uint32_t) DCMI_CFG_MAPEN;
    } else {
        /* Disable the DCMI Map */
        DCMI->CFG &= ~(uint32_t) DCMI_CFG_MAPEN;
    }
}


/**
 * @brief  Reads the data stored in the DR register.
 * @param  None
 * @retval Data register value
 */
uint32_t DCMI_ReadData(void)
{
    return (DCMI->FIFOD);
}


/**
 * @}
 */


/** @defgroup DCMI_Group3 Interrupts and flags management functions
 *  @brief   Interrupts and flags management functions
 *
   @verbatim
   ===============================================================================
 ##### Interrupts and flags management functions #####
   ===============================================================================

   @endverbatim
 * @{
 */


/**
 * @brief  Enables or disables the DCMI interface interrupts.
 * @param  DCMI_IT: specifies the DCMI interrupt sources to be enabled or disabled.
 *          This parameter can be any combination of the following values:
 *            @arg DCMI_IT_FRAME: Frame capture complete interrupt mask
 *            @arg DCMI_IT_OVF: Overflow interrupt mask
 *            @arg DCMI_IT_ERR: Synchronization error interrupt mask
 *            @arg DCMI_IT_VSYNC: VSYNC interrupt mask
 *            @arg DCMI_IT_LINE: Line interrupt mask
 * @param  NewState: new state of the specified DCMI interrupts.
 *          This parameter can be: ENABLE or DISABLE.
 * @retval None
 */
void DCMI_ITConfig(uint16_t DCMI_IT, FunctionalState NewState)
{
    if (NewState != DISABLE) {
        /* Enable the Interrupt sources */
        DCMI->INTEN |= DCMI_IT;
    } else {
        /* Disable the Interrupt sources */
        DCMI->INTEN &= (uint16_t)(~DCMI_IT);
    }
}


/**
 * @brief  Checks whether the  DCMI interface flag is set or not.
 * @param  DCMI_FLAG: specifies the flag to check.
 *          This parameter can be one of the following values:
 *            @arg DCMI_FLAG_FRAMERI: Frame capture complete Raw flag mask
 *            @arg DCMI_FLAG_OVFRI: Overflow Raw flag mask
 *            @arg DCMI_FLAG_ERRRI: Synchronization error Raw flag mask
 *            @arg DCMI_FLAG_VSYNCRI: VSYNC Raw flag mask
 *            @arg DCMI_FLAG_LINERI: Line Raw flag mask
 *            @arg DCMI_FLAG_FRAMEMI: Frame capture complete Masked flag mask
 *            @arg DCMI_FLAG_OVFMI: Overflow Masked flag mask
 *            @arg DCMI_FLAG_ERRMI: Synchronization error Masked flag mask
 *            @arg DCMI_FLAG_VSYNCMI: VSYNC Masked flag mask
 *            @arg DCMI_FLAG_LINEMI: Line Masked flag mask
 *            @arg DCMI_FLAG_HSYNC: HSYNC flag mask
 *            @arg DCMI_FLAG_VSYNC: VSYNC flag mask
 *            @arg DCMI_FLAG_FNE: Fifo not empty flag mask
 * @retval The new state of DCMI_FLAG (SET or RESET).
 */
FlagStatus DCMI_GetFlagStatus(uint16_t DCMI_FLAG)
{
    FlagStatus  bitstatus = RESET;
    uint32_t    dcmireg, tempreg = 0;

    /* Get the DCMI register index */
    dcmireg = (((uint16_t) DCMI_FLAG) >> 12);

    if (dcmireg == 0x00) {      /* The FLAG is in RISR register */
        tempreg = DCMI->RISTATUS;
    } else if (dcmireg == 0x02) { /* The FLAG is in SR register */
        tempreg = DCMI->STATUS;
    } else { /* The FLAG is in MISR register */
        tempreg = DCMI->MISTATUS;
    }

    if ((tempreg & DCMI_FLAG) != (uint16_t) RESET) {
        bitstatus = SET;
    } else {
        bitstatus = RESET;
    }
    /* Return the DCMI_FLAG status */
    return (bitstatus);
}


/**
 * @brief  Clears the DCMI's pending flags.
 * @param  DCMI_FLAG: specifies the flag to clear.
 *          This parameter can be any combination of the following values:
 *            @arg DCMI_FLAG_FRAMERI: Frame capture complete Raw flag mask
 *            @arg DCMI_FLAG_OVFRI: Overflow Raw flag mask
 *            @arg DCMI_FLAG_ERRRI: Synchronization error Raw flag mask
 *            @arg DCMI_FLAG_VSYNCRI: VSYNC Raw flag mask
 *            @arg DCMI_FLAG_LINERI: Line Raw flag mask
 * @retval None
 */
void DCMI_ClearFlag(uint16_t DCMI_FLAG)
{
    /* Clear the flag by writing in the ICR register 1 in the corresponding
       Flag position*/

    DCMI->ICL = DCMI_FLAG;
}


/**
 * @brief  Checks whether the DCMI interrupt has occurred or not.
 * @param  DCMI_IT: specifies the DCMI interrupt source to check.
 *          This parameter can be one of the following values:
 *            @arg DCMI_IT_FRAME: Frame capture complete interrupt mask
 *            @arg DCMI_IT_OVF: Overflow interrupt mask
 *            @arg DCMI_IT_ERR: Synchronization error interrupt mask
 *            @arg DCMI_IT_VSYNC: VSYNC interrupt mask
 *            @arg DCMI_IT_LINE: Line interrupt mask
 * @retval The new state of DCMI_IT (SET or RESET).
 */
ITStatus DCMI_GetITStatus(uint16_t DCMI_IT)
{
    ITStatus    bitstatus   = RESET;
    uint32_t    itstatus    = 0;


    itstatus = DCMI->MISTATUS & DCMI_IT; /* Only masked interrupts are checked */

    if ((itstatus != (uint16_t) RESET)) {
        bitstatus = SET;
    } else {
        bitstatus = RESET;
    }
    return (bitstatus);
}


/**
 * @brief  Clears the DCMI's interrupt pending bits.
 * @param  DCMI_IT: specifies the DCMI interrupt pending bit to clear.
 *          This parameter can be any combination of the following values:
 *            @arg DCMI_IT_FRAME: Frame capture complete interrupt mask
 *            @arg DCMI_IT_OVF: Overflow interrupt mask
 *            @arg DCMI_IT_ERR: Synchronization error interrupt mask
 *            @arg DCMI_IT_VSYNC: VSYNC interrupt mask
 *            @arg DCMI_IT_LINE: Line interrupt mask
 * @retval None
 */
void DCMI_ClearITPendingBit(uint16_t DCMI_IT)
{
    /* Clear the interrupt pending Bit by writing in the ICR register 1 in the
       corresponding pending Bit position*/

    DCMI->ICL = DCMI_IT;
}


/**
 * @}
 */


/**
 * @}
 */


/**
 * @}
 */


/**
 * @}
 */

/************************ (C) COPYRIGHT Fudan Microelectronics *****END OF FILE****/

