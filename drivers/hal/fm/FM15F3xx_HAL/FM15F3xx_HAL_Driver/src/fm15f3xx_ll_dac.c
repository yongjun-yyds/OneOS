/**
 ******************************************************************************
 * @file    fm15f3xx_ll_dac.c
 * @author  SRG
 * @version V1.0.0
 * @date    2020-03-17
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */
#include "fm15f3xx_ll_dac.h"

#if defined (DAC0)


/** @addtogroup DAC_LL
 * @{
 */

/* Private types -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private constants ---------------------------------------------------------*/

/* Private macros ------------------------------------------------------------*/


/** @addtogroup DAC_LL_Private_Macros
 * @{
 */


/**
 * @}
 */


/* Private function prototypes -----------------------------------------------*/


/** @defgroup DAC_LL_Private_Functions DAC Private Functions
 * @{
 */
//static uint32_t BistAt0Reg_Rst(uint32_t RegPos);


/**
 * @}
 */

/* Exported functions --------------------------------------------------------*/


/** @addtogroup ATIM_LL_Exported_Functions
 * @{
 */


/**
 * @brief  Set each @ref LL_DAC_InitTypeDef field to default value.
 * @param  DAC_InitStruct pointer to a @ref LL_DAC_InitTypeDef structure
 * whose fields will be set to default values.
 * @retval None
 */
void LL_DAC_StructInit(LL_DAC_InitTypeDef *DAC_InitStruct)
{
    /* Set DAC_InitStruct fields to default values */
    DAC_InitStruct->DACEN           = LL_DAC_DISABLE;
    DAC_InitStruct->VrefSel         = LL_DAC_VREFSEL_VDDA;
    DAC_InitStruct->HighPowerEn     = LL_DAC_HIGHPOWER_ENABLE;
    DAC_InitStruct->PointerMode     = LL_DAC_POINTERMODE_FIX;
    DAC_InitStruct->ScanEn          = LL_DAC_SCAN_TRIG;
    DAC_InitStruct->ScanMode        = LL_DAC_SCANMODE_CONTINUE;
    DAC_InitStruct->ScanInterval    = 0x0U;
    DAC_InitStruct->TrigSource      = LL_DAC_TRIGSRC_SOFTWARE;
    DAC_InitStruct->TrigInvEn       = LL_DAC_TRIGINV_DISABLE;
    DAC_InitStruct->TrigEdge        = LL_DAC_TRIGEDGE_HIGH;
    DAC_InitStruct->TrigFilter      = LL_DAC_TRIGFILTER_NOFILTER;
}


void LL_DAC_Init(DAC_TypeDef *DACx, LL_DAC_InitTypeDef *DAC_InitStruct)
{
    CLEAR_BIT(DACx->CTRL, DAC_CTRL_DACEN);  //disable DAC defore config
    MODIFY_REG(DACx->CFG, DAC_CFG_VSEL | DAC_CFG_HPEN, DAC_InitStruct->VrefSel | DAC_InitStruct->HighPowerEn);
    MODIFY_REG(DACx->WORK,
               DAC_WORK_PTRM | DAC_WORK_SCANME | DAC_WORK_SCANOS | DAC_WORK_SCANINVL,
               DAC_InitStruct->PointerMode | DAC_InitStruct->ScanEn | DAC_InitStruct->ScanMode | (DAC_InitStruct->ScanInterval << DAC_WORK_SCANINVL_Pos));
    //dac_work Configuration: PointerMode,ScanModeEn,ScanOneShot,ScanInterval
    MODIFY_REG(DACx->TRIG,
               DAC_TRIG_TSC | DAC_TRIG_TINV | DAC_TRIG_TEDGE | DAC_TRIG_TFILT,
               DAC_InitStruct->TrigSource | DAC_InitStruct->TrigInvEn | DAC_InitStruct->TrigEdge | DAC_InitStruct->TrigFilter);
    //dac_work Configuration: TrigSource,TrigInvEn,TrigEdge,TrigFilt

    MODIFY_REG(DACx->CTRL, DAC_CTRL_DACEN, DAC_InitStruct->DACEN);   //dac_ctrl Configuration:dac_en
}


#endif /* DAC0 */

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/

