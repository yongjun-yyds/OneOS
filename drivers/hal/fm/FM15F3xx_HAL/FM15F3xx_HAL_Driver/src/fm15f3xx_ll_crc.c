/**
 ******************************************************************************
 * @file    fm15f3xx_ll_crc.c
 * @author  ZJH
 * @version V1.0.0
 * @date    2020-03-17
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */
#include "fm15f3xx_ll_crc.h"

static uint16_t CRC16_Process(LL_CRC_InitTypeDef* CRC_IntiStrcuct, uint8_t *pData, uint32_t DataLength);


/**
 * @brief Deinitialize the CRC configuration register to their default reset values.
 * @param None
 * @retval None
 */
void LL_CRC_DeInit(void)
{
    CRC->CFG = 0x00;
}


/**
 * @brief Get the CRC16 from the given data
 * @param CRC_InitTypeDef: CRC parameter struct pointer
 * @retval The checksum value
 */
uint16_t LL_CRC16_Calculation(LL_CRC_InitTypeDef* CRC_InitStrcuct, uint8_t *pData, uint32_t DataLength)
{
    return (CRC16_Process(CRC_InitStrcuct, pData, DataLength));
}


/**
 * @brief Get the CRC checksum from the given data
 * @param CRC_InitTypeDef: CRC parameter struct pointer
 * @retval The checksum value
 */
static uint16_t CRC16_Process(LL_CRC_InitTypeDef* CRC_IntiStrcuct, uint8_t *pData, uint32_t DataLength)
{
    uint8_t *data = pData;

    uint16_t    crc_data        = 0;
    uint32_t    word_cnt        = DataLength >> 2;
    uint8_t     last_byte_cnt   = DataLength & 0x03;

    LL_CRC_DeInit();

    CRC->DATA = CRC_IntiStrcuct->Init;

    if (FALSE == CRC_IntiStrcuct->RefIn)
        LL_CRC_SetInputReverse(CRC, LL_CRC_REVERSE_BYTE);
    else
        LL_CRC_SetInputReverse(CRC, LL_CRC_REVERSE_BITinWORD);

    LL_CRC_SetInputDataWidth(CRC, LL_CRC_DATAWIDTH_32);

    LL_CRC_Start(CRC);

    while (word_cnt--) {
        CRC->DATA_IN    = *(unsigned int *) data;
        data            += 4;
    }

    if (FALSE == CRC_IntiStrcuct->RefIn)
        LL_CRC_SetInputReverse(CRC, LL_CRC_REVERSE_NO);
    else
        LL_CRC_SetInputReverse(CRC, LL_CRC_REVERSE_BITinBYTE);

    LL_CRC_SetInputDataWidth(CRC, LL_CRC_DATAWIDTH_8);

    while (last_byte_cnt--) {
        CRC->DATA_IN = *(data++);
    }

    switch (CRC_IntiStrcuct->RefOut) {
    case FALSE:
        crc_data = CRC->DATA0;
        break;
    default:
        crc_data = CRC->DATA2;
        break;
    }

    crc_data = crc_data ^ CRC_IntiStrcuct->XorOut;

    LL_CRC_Stop(CRC);

    return (crc_data);
}


/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
