/**
 ******************************************************************************
 * @file    fm15f3xx_ll_systick.c
 * @author  SRG
 * @version V1.0.0
 * @date    2020-04-21
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */
#include "fm15f3xx_ll_systick.h"
#include "fm15f3xx_ll_cmu.h"

#define SYSTICK_COUNT_MAX 0xFFFFFFU

static uint32_t SystickClock;


/*
 */
static uint32_t UsMax, MsMax;
static uint32_t ValueUs, ValueMs;
ErrorStatus LL_SYSTICK_Init(LL_SYSTICK_InitTypeDef *InitStruct)
{
    if (LL_SYSTICKCLK_SYSCLK == InitStruct->TickClkSrcSel) { //1:core clock 0:external clock
        SysTick->CTRL   |= SysTick_CTRL_CLKSOURCE_Msk;
        SystickClock    = LL_CMU_SystemCoreClockUpdate();
    } else {
        switch (InitStruct->TickRefClkSel) {
        case LL_CMU_SYSTICKREFCLK_IRC4M:
            SystickClock = IRC4M_VALUE;
            break;
        case LL_CMU_SYSTICKREFCLK_IRC16M:
            SystickClock = IRC16M_VALUE;
            break;
        case LL_CMU_SYSTICKREFCLK_OSCCLK:
            SystickClock = InitStruct->OscValueHz;
            break;
        case LL_CMU_SYSTICKREFCLK_GPIOCLK:
            //TODO
            return (ERROR);
        default:
            return (ERROR);
        }
        SystickClock /= (0x01U << (InitStruct->TickRefClkDiv + 1));
//      SystickClock/=8;
        LL_CMU_ConfigSysTickRefClk(InitStruct->TickRefClkSel, InitStruct->TickRefClkDiv);
        SysTick->CTRL &= (~SysTick_CTRL_CLKSOURCE_Msk);
    }
    SysTick->CTRL   &= ((~SysTick_CTRL_TICKINT_Msk) & (~SysTick_CTRL_ENABLE_Msk));
    ValueUs         = SystickClock / 1000000;
    ValueMs         = SystickClock / 1000;
    return (SUCCESS);
}


void init_systick(void)
{
    LL_SYSTICK_InitTypeDef InitStruct;
    InitStruct.OscValueHz         = 12000000;
    InitStruct.TickClkSrcSel    = LL_SYSTICKCLK_SYSCLK;//LL_SYSTICKCLK_SYSCLK;
    InitStruct.TickRefClkSel    = LL_CMU_SYSTICKREFCLK_OSCCLK;
    InitStruct.TickRefClkDiv    = LL_CMU_REUSEDIV2_DIV2;
    if (SUCCESS == LL_SYSTICK_Init(&InitStruct)) {
        UsMax   = SYSTICK_COUNT_MAX / ValueUs;
        MsMax   = SYSTICK_COUNT_MAX / ValueMs;
    }
}


void systick_delay_us(uint32_t us)
{
    uint32_t    load    = us;
    uint32_t    i       = 0;
    if (load > UsMax) {
        i               = load / UsMax;
        load            = load % UsMax;
        SysTick->LOAD   = UsMax * ValueUs - 2 * 32 / i;
    }
    SysTick->VAL    = 0;
    SysTick->CTRL   |= SysTick_CTRL_ENABLE_Msk;
    while (i--) {
        while (!(SysTick->CTRL & SysTick_CTRL_COUNTFLAG_Msk))
            ;
    }
    SysTick->LOAD   = load * ValueUs;
    if (SysTick->LOAD > 4 * 33) {
        SysTick->LOAD -= 4 * 33;
    } else
        SysTick->LOAD = 1;
    SysTick->VAL    = 0;
    SysTick->CTRL   |= SysTick_CTRL_ENABLE_Msk;
    while (!(SysTick->CTRL & SysTick_CTRL_COUNTFLAG_Msk))
        ;
    SysTick->VAL    = 0;
    SysTick->CTRL   &= (~SysTick_CTRL_ENABLE_Msk);

}


void systick_delay_ms(uint32_t ms)
{
    uint32_t    load    = ms;
    uint32_t    i       = 0;
    if (load > MsMax) {
        i               = load / MsMax;
        load            = load % MsMax;
        SysTick->LOAD   = MsMax * ValueMs - 2 * 32 / i;
    }
    SysTick->VAL    = 0;
    SysTick->CTRL   |= SysTick_CTRL_ENABLE_Msk;
    while (i--) {
        while (!(SysTick->CTRL & SysTick_CTRL_COUNTFLAG_Msk))
            ;
    }
    SysTick->LOAD   = load * ValueMs;
    if (SysTick->LOAD > 4 * 33) {
        SysTick->LOAD -= 4 * 33;
    } else
        SysTick->LOAD = 1;
    SysTick->VAL    = 0;
    SysTick->CTRL   |= SysTick_CTRL_ENABLE_Msk;
    while (!(SysTick->CTRL & SysTick_CTRL_COUNTFLAG_Msk))
        ;
    SysTick->VAL    = 0;
    SysTick->CTRL   &= (~SysTick_CTRL_ENABLE_Msk);

}


/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/

