/**
 ******************************************************************************
 * @file    fm15f3xx_ll_gpio.c
 * @author  SRG
 * @version V1.0.1
 * @date    2020-06-04
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */
#include "fm15f3xx_ll_gpio.h"

void LL_GPIO_StructInitParam(LL_GPIO_InitTypeDef *GPIO_InitStruct)
{
    GPIO_InitStruct->Pin            = GPIO_PIN_0 | GPIO_PIN_1;
    GPIO_InitStruct->Alt            = GPIO_AF_GPIO;
    GPIO_InitStruct->Dir            = LL_GPIO_DIRECTION_OUT;
    GPIO_InitStruct->Speed          = LL_GPIO_SLEWRATE_HIGH;
    GPIO_InitStruct->OType          = LL_GPIO_OUTPUT_NOOPENDRAIN;
    GPIO_InitStruct->PuPd           = LL_GPIO_PULL_UP;
    GPIO_InitStruct->DigitFiltEn    = LL_GPIO_DIGITFILTE_NO;
    GPIO_InitStruct->DriveStrength  = LL_GPIO_DRIVES_STRONG;
    GPIO_InitStruct->Lock           = LL_GPIO_LK_UNLOCK;
    GPIO_InitStruct->Irq            = LL_GPIO_INTorDMA_DISABLE;
    GPIO_InitStruct->WECconfig      = LL_GPIO_WKUP_CLOSED;
}


void LL_GPIO_Init(GPIO_TypeDef *GPIOx, LL_GPIO_InitTypeDef *GPIO_InitStruct)
{
    uint32_t Pin = GPIO_InitStruct->Pin;
    for (int i = 0; i < 16; i++) {
        if (Pin & 0x01) {
            MODIFY_REG(LL_GPIO_GetCFG(GPIOx)->PCR[i], GPIO_CFG_PCR_MUX_Msk, GPIO_InitStruct->Alt);
            MODIFY_REG(LL_GPIO_GetCFG(GPIOx)->PCR[i],
                       GPIO_CFG_PCR_SRE_Msk |
                       GPIO_CFG_PCR_ODE_Msk |
                       GPIO_CFG_PCR_PS_Msk | GPIO_CFG_PCR_PE_Msk |
                       GPIO_CFG_PCR_DSE_Msk |
                       GPIO_CFG_PCR_LK_Msk |
                       GPIO_CFG_PCR_IRQC_Msk |
                       GPIO_CFG_PCR_WKUPCFG_Msk,
                       GPIO_InitStruct->Speed |
                       GPIO_InitStruct->OType |
                       GPIO_InitStruct->PuPd |
                       GPIO_InitStruct->DriveStrength |
                       GPIO_InitStruct->Lock |
                       GPIO_InitStruct->Irq |
                       GPIO_InitStruct->WECconfig);
        }
        Pin >>= 1;
    }
    LL_GPIO_SetPinDir(GPIOx, GPIO_InitStruct->Pin, GPIO_InitStruct->Dir);
    MODIFY_REG(LL_GPIO_GetCFG(GPIOx)->DFER, GPIO_InitStruct->Pin, GPIO_InitStruct->DigitFiltEn);
}


/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
