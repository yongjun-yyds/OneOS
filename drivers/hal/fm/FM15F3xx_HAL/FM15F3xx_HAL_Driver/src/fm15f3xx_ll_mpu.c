/**
 ******************************************************************************
 * @file    fm15f3xx_ll_mpu.c
 * @author  SRG
 * @version V1.0.0
 * @date    2020-03-17
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */
#include "fm15f3xx_ll_mpu.h"


/** @addtogroup FM15f3XX_LL_Driver
 * @{
 */


/** @addtogroup ATIM_LL
 * @{
 */

/* Private types -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private constants ---------------------------------------------------------*/
/* Private macros ------------------------------------------------------------*/


/**
 * @brief  Set each @ref MPU_SetRegionConfig field to default value.
 * @param  MPU_TypeDef pointer to a @ref regionConfig structure
 * whose fields will be set to default values.
 * @retval None
 */
void LL_MPU_SetRegionConfig(MPU_TypeDef *base, LL_MPU_RegionTypeDef *regionConfig)
{
    uint8_t regNumber = regionConfig->regionNum;

    base->CSR &= ~MPU_CSR_EN;
    /* The start and end address of the region descriptor. */
    base->REGION[regNumber].SADDR   = regionConfig->startAddress;
    base->REGION[regNumber].EADDR   = regionConfig->endAddress;
    base->REGION[regNumber].AUTHA   = regionConfig->accessRights1;
    base->REGION[regNumber].AUTHB   = regionConfig->accessRights2;
    base->REGION[regNumber].AUTHC   = regionConfig->accessRights3;
    base->REGION[regNumber].CTRL    = regionConfig->setCtrl;
    base->REGION[regNumber].SWCTL   = regionConfig->setSwCtl;
    base->CSR                       |= MPU_CSR_EN;
}


/**
 * @brief  set MPU region address.
 * @param  None
 * @retval None
 */
void LL_MPU_SetRegionAddr(MPU_TypeDef *base, uint32_t regionNum, uint32_t startAddr, uint32_t endAddr)
{
    base->REGION[regionNum].SADDR   = startAddr;
    base->REGION[regionNum].EADDR   = endAddr;
}


/**
 * @brief  set MPU region access right.
 * @param  None
 * @retval None
 */
void LL_MPU_SetRegionAccessRights(MPU_TypeDef *base, LL_MPU_RightTypeDef *accessRights)
{
    uint8_t regionNum = accessRights->regionNum;

    /* Set low master region access rights. */
    base->REGION[regionNum].AUTHA   = accessRights->accessRights1;
    base->REGION[regionNum].AUTHB   = accessRights->accessRights2;
    base->REGION[regionNum].AUTHC   = accessRights->accessRights3;
    base->REGION[regionNum].CTRL    = accessRights->setCtrl;
    base->REGION[regionNum].SWCTL   = accessRights->setSwCtl;
}


/**
 * @brief  get MPU error access info.
 * @param  None
 * @retval None
 */
void LL_MPU_GetErrorAccessInfo(MPU_TypeDef *base, uint32_t slaveNum, LL_MPU_AccessErrTypeDef *errInform)
{
    errInform->status = base->CSR;
    /* Error address. */
    errInform->address = base->SP[slaveNum].EAR;
    /* Error detail information. */
    errInform->details = base->SP[slaveNum].EDR;
}


/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/

