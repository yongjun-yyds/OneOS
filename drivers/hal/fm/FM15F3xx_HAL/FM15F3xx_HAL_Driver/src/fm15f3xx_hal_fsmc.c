/**
 ******************************************************************************
 * @file    fm15f3xx_hal_fsmc.c
 * @author  WYL
 * @version V1.0.0
 * @date    2020-03-17
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 *//* Includes ------------------------------------------------------------------*/
#include "fm15f3xx_hal.h"


/** @addtogroup FM15f3XX_HAL_Driver
 * @{
 */

#ifdef FSMC_MODULE_ENABLED


/** @addtogroup FSMC_HAL
 * @{
 */
/* Private function prototypes -----------------------------------------------*/


/** @addtogroup ADC_Private_Functions
 * @{
 */
static void __FSMC_NORSRAMConfig(FSMC_HandleTypeDef *hfsmc);


static void __FSMC_LCDConfig(FSMC_HandleTypeDef *hfsmc);


/**
 * @}
 */

/* Exported functions --------------------------------------------------------*/


/**
 * @brief  Initialize the FSMC_NORSRAM device according to the specified
 *         control parameters in the FSMC_NORSRAM_InitTypeDef
 * @param  Device Pointer to FSMC instance
 * @param  Init Pointer to NORSRAM Initialization structure
 * @retval HAl Status
 */
HAL_StatusTypeDef HAL_FSMC_NORSRAMInit(FSMC_HandleTypeDef* hfsmc)
{
    /* Check FSMC handle */
    if (hfsmc == NULL) {
        return (HAL_ERROR);
    }
    /* Check the parameters */
    assert_param(IS_FSMC_INSTANCE(hfsmc->Instance));

    if (hfsmc->State == HAL_FSMC_STATE_RESET) {
        /* Init the low level hardware */
        HAL_FSMC_MspInit(hfsmc);
    }

    /* Set FSMC parameters */
    __FSMC_NORSRAMConfig(hfsmc);

    /* Initialize the FSMC state*/
    hfsmc->State = HAL_FSMC_STATE_READY;

    /* Return function status */
    return (HAL_OK);
}


/**
 * @brief  Initialize the FSMC_LCD device according to the specified
 *         control parameters in the FSMC_LCD_InitTypeDef
 * @param  Device Pointer to FSMC instance
 * @param  Init Pointer to LCD Initialization structure
 * @retval null
 */
HAL_StatusTypeDef HAL_FSMC_LCDInit(FSMC_HandleTypeDef* hfsmc)
{
    /* Check FSMC handle */
    if (hfsmc == NULL) {
        return (HAL_ERROR);
    }
    /* Check the parameters */
    assert_param(IS_FSMC_INSTANCE(hfsmc->Instance));

    if (hfsmc->State == HAL_FSMC_STATE_RESET) {
        /* Init the low level hardware */
        HAL_FSMC_MspInit(hfsmc);
    }

    /* Set FSMC parameters */
    __FSMC_LCDConfig(hfsmc);

    /* Initialize the FSMC state*/
    hfsmc->State = HAL_FSMC_STATE_READY;

    /* Return function status */
    return (HAL_OK);
}


/**
 * @brief  Initializes the FSMC MSP.
 * @param  hfsmc pointer to a FSMC_HandleTypeDef structure that contains
 *         the configuration information for the specified FSMC.
 * @retval None
 */
__weak void HAL_FSMC_MspInit(FSMC_HandleTypeDef* hfsmc)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(hfsmc);


    /* NOTE : This function Should not be modified, when the callback is needed,
              the HAL_FSMC_MspInit could be implemented in the user file
     */
}


/**
 * @brief  Initialize the Write Timing according to the specified
 *         parameters in the FSMC_Timing_TypeDef
 * @param  Device Pointer to FSMC instance
 * @param  Timing Pointer to Timing structure
 * @retval HAL status
 */
void HAL_FSMC_WriteTimingConfig(FSMC_HandleTypeDef *hfsmc, FSMC_TimingConfigTypeDef *FSMC_TimingtStruct)
{
    /* Check the parameters */
    assert_param(IS_FSMC_INSTANCE(hfsmc->Instance));
    assert_param(IS_FSMC_ADDRESSSETUPTIME(FSMC_TimingtStruct->AddressSetupTime));
    assert_param(IS_FSMC_ADDRESSHOLDTIME(FSMC_TimingtStruct->AddressHoldTime));
    assert_param(IS_FSMC_DATASETUPTIME(FSMC_TimingtStruct->DataSetupTime));
    assert_param(IS_FSMC_DATAHOLDTIME(FSMC_TimingtStruct->DataHoldTime));
    assert_param(IS_FSMC_BUSREADYTIME(FSMC_TimingtStruct->BusReadyTime));

    /* Config AWRT Reg */
    WRITE_REG(hfsmc->Instance->AWRT,
              (FSMC_TimingtStruct->AddressSetupTime << FSMC_AWRT_WRADDSET_Pos) |
              (FSMC_TimingtStruct->AddressHoldTime << FSMC_AWRT_WRADDHLD_Pos) |
              (FSMC_TimingtStruct->DataSetupTime << FSMC_AWRT_WRDATAST_Pos) |
              (FSMC_TimingtStruct->DataHoldTime << FSMC_AWRT_WRDATAHLD_Pos) |
              (FSMC_TimingtStruct->BusReadyTime << FSMC_AWRT_WRBUSRDY_Pos));
}

/**
 * @brief  Initialize the Read Timing according to the specified
 *         parameters in the FSMC_Timing_TypeDef
 * @param  Device Pointer to FSMC instance
 * @param  Timing Pointer to Timing structure
 * @retval HAL status
 */
void HAL_FSMC_ReadTimingConfig(FSMC_HandleTypeDef *hfsmc, FSMC_TimingConfigTypeDef *FSMC_TimingtStruct)
{
    /* Check the parameters */
    assert_param(IS_FSMC_INSTANCE(hfsmc->Instance));
    assert_param(IS_FSMC_ADDRESSSETUPTIME(FSMC_TimingtStruct->AddressSetupTime));
    assert_param(IS_FSMC_ADDRESSHOLDTIME(FSMC_TimingtStruct->AddressHoldTime));
    assert_param(IS_FSMC_DATASETUPTIME(FSMC_TimingtStruct->DataSetupTime));
    assert_param(IS_FSMC_DATAHOLDTIME(FSMC_TimingtStruct->DataHoldTime));
    assert_param(IS_FSMC_BUSREADYTIME(FSMC_TimingtStruct->BusReadyTime));

    /* Config ARDT Reg */
    WRITE_REG(hfsmc->Instance->ARDT,
              (FSMC_TimingtStruct->AddressSetupTime << FSMC_ARDT_RDADDSET_Pos) |
              (FSMC_TimingtStruct->AddressHoldTime << FSMC_ARDT_RDADDHLD_Pos) |
              (FSMC_TimingtStruct->DataSetupTime << FSMC_ARDT_RDDATAST_Pos) |
              (FSMC_TimingtStruct->DataHoldTime << FSMC_ARDT_RDDATAHLD_Pos) |
              (FSMC_TimingtStruct->BusReadyTime << FSMC_ARDT_RDBUSRDY_Pos));
}

/**
 * @brief  Enables FSMC.
 * @param  hfsmc pointer to a FSMC_HandleTypeDef structure that contains
 *         the configuration information for the specified FSMC.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_FSMC_START(FSMC_HandleTypeDef *hfsmc)
{
    /* Check the parameters */
    assert_param(IS_FSMC_INSTANCE(hfsmc->Instance));

    /* Enable the Peripheral */
    __HAL_FSMC_ENABLE(hfsmc);

    /* Initialize the FSMC state*/
    hfsmc->State = HAL_FSMC_STATE_READY;

    return (HAL_OK);
}


/**
 * @brief  Disables FSMC.
 * @param  hfsmc pointer to a FSMC_HandleTypeDef structure that contains
 *         the configuration information for the specified FSMC.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_FSMC_Stop(FSMC_HandleTypeDef *hfsmc)
{
    /* Check the parameters */
    assert_param(IS_FSMC_INSTANCE(hfsmc->Instance));

    /* Enable the Peripheral */
    __HAL_FSMC_DISABLE(hfsmc);

    /* Initialize the FSMC state*/
    hfsmc->State = HAL_FSMC_STATE_RESET;

    return (HAL_OK);
}


/**
 * @brief  Enables FSMC EXTM Mode and INT.
 * @param  hfsmc pointer to a FSMC_HandleTypeDef structure that contains
 *         the configuration information for the specified FSMC.
 * @param  Interrupt flag can be combination of the following values:
 *         @arg @ref FSMC_INT_EORD
 *         @arg @ref FSMC_INT_EOWR
 *         @arg @ref FSMC_INT_ERR
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_FSMC_EXTM_IT(FSMC_HandleTypeDef *hfsmc, uint32_t IT)
{
    /* Check the parameters */
    assert_param(IS_FSMC_INSTANCE(hfsmc->Instance));

    /* Check EXTM Mode */
    if (!__HAL_FSMC_IS_EXTMMODE(hfsmc, FSMC_ACCESS_EXTM)) {
        /* Update FSMC State to error */
        SET_BIT(hfsmc->State, HAL_FSMC_STATE_ERROR);

        return (HAL_ERROR);
    }

    /* Config INTE Reg */
    __HAL_FSMC_ENABLE_INT(hfsmc, IT);

    /* Enable the Peripheral */
    __HAL_FSMC_ENABLE(hfsmc);

    /* Initialize the FSMC state*/
    hfsmc->State = HAL_FSMC_STATE_READY;

    return (HAL_OK);
}


/**
 * @brief  Enables FSMC EXTM FIFO Mode and INT.
 * @param  hfsmc pointer to a FSMC_HandleTypeDef structure that contains
 *         the configuration information for the specified FSMC.
 * @param  Interrupt flag can be combination of the following values:
 *         @arg @ref FSMC_INT_EORD
 *         @arg @ref FSMC_INT_EOWR
 *         @arg @ref FSMC_INT_ERR
 *         @arg @ref FSMC_INT_WMRD
 *         @arg @ref FSMC_INT_RDFIFOOV
 *         @arg @ref FSMC_INT_WMWR
 *         @arg @ref FSMC_INT_WRFIFOOV
 * @param  RDFIFOWaterMark from 0 to 4.
 * @param  WRFIFOWaterMark from 0 to 4.
 * @retval HAL status
 */

HAL_StatusTypeDef HAL_FSMC_EXTMFIFO_IT(FSMC_HandleTypeDef *hfsmc, uint32_t IT, uint8_t RDFIFOWaterMark, uint8_t WRFIFOWaterMark)
{
    /* Check the parameters */
    assert_param(IS_FSMC_INSTANCE(hfsmc->Instance));

    /* Check EXTM FIFO Mode */
    if (!__HAL_FSMC_IS_EXTMMODE(hfsmc, FSMC_ACCESS_EXTMFIFO)) {
        /* Update FSMC State to error */
        SET_BIT(hfsmc->State, HAL_FSMC_STATE_ERROR);

        return (HAL_ERROR);
    }

    /* Config INTE Reg */
    __HAL_FSMC_ENABLE_INT(hfsmc, IT);

    /* Config FCFG Reg */
    __HAL_FSMC_SET_WRFIFOWATERMARK(hfsmc, WRFIFOWaterMark);
    __HAL_FSMC_SET_RDFIFOWATERMARK(hfsmc, RDFIFOWaterMark);

    /* Enable the Peripheral */
    __HAL_FSMC_ENABLE(hfsmc);

    /* Initialize the FSMC state*/
    hfsmc->State = HAL_FSMC_STATE_READY;

    return (HAL_OK);
}


/**
 * @brief  Disables FSMC.
 * @param  hfsmc pointer to a FSMC_HandleTypeDef structure that contains
 *         the configuration information for the specified FSMC.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_FSMC_Stop_IT(FSMC_HandleTypeDef *hfsmc)
{
    /* Check the parameters */
    assert_param(IS_FSMC_INSTANCE(hfsmc->Instance));

    /* Disable the Interrupt */
    __HAL_FSMC_DISABLE_INT(hfsmc, FSMC_INT_ALL);

    /* Enable the Peripheral */
    __HAL_FSMC_DISABLE(hfsmc);

    /* Initialize the FSMC state*/
    hfsmc->State = HAL_FSMC_STATE_RESET;

    return (HAL_OK);
}


/**
 * @brief  Handles FSMC interrupt request
 * @param  hadc pointer to a ADC_HandleTypeDef structure that contains
 *         the configuration information for the specified ADC.
 * @retval None
 */
void HAL_FSMC_IRQHandler(FSMC_HandleTypeDef* hfsmc)
{
    /* Check read or write complete flag */
    if (__HAL_FSMC_IS_ACTIVEINTSTATUS(hfsmc, FSMC_FLAG_RDINT) || __HAL_FSMC_IS_ACTIVEINTSTATUS(hfsmc, FSMC_FLAG_WRINT)) {
        HAL_FSMC_RdWrCpltCallback(hfsmc);

        /* Clear conversion flag */
        __HAL_FSMC_CLEAR_INTSTATUS(hfsmc, FSMC_FLAG_RDINT | FSMC_FLAG_WRINT);
    }

    /* Check read or write fifo flag */
    if (__HAL_FSMC_IS_ACTIVEINTSTATUS(hfsmc, FSMC_FLAG_RDFIFOINT) || __HAL_FSMC_IS_ACTIVEINTSTATUS(hfsmc, FSMC_FLAG_WRFIFOINT)) {
        HAL_FSMC_RdWrFifoCallback(hfsmc);

        /* Clear conversion flag */
        __HAL_FSMC_CLEAR_INTSTATUS(hfsmc, FSMC_FLAG_RDFIFOINT | FSMC_FLAG_WRFIFOINT);
    }

    /* Check error flag */
    if (__HAL_FSMC_IS_ACTIVEINTSTATUS(hfsmc, FSMC_FLAG_WRINT)) {
        HAL_FSMC_ErrorCallback(hfsmc);

        /* Clear conversion flag */
        __HAL_FSMC_CLEAR_INTSTATUS(hfsmc, FSMC_FLAG_WRINT);
    }

    /* Check read or write fifo over run flag */
    if (__HAL_FSMC_IS_ACTIVEINTSTATUS(hfsmc, FSMC_FLAG_RDFIFOINT) || __HAL_FSMC_IS_ACTIVEINTSTATUS(hfsmc, FSMC_FLAG_WRFIFOINT)) {
        HAL_FSMC_RdWrFifoOvCallback(hfsmc);

        /* Clear conversion flag */
        __HAL_FSMC_CLEAR_INTSTATUS(hfsmc, FSMC_FLAG_RDFIFOINT | FSMC_FLAG_WRFIFOINT);
    }
}


/**
 * @brief  read or write complete callback in non blocking mode
 * @param  hfsmc pointer to a FSMC_HandleTypeDef structure that contains
 *         the configuration information for the specified FSMC.
 * @retval None
 */
__weak void HAL_FSMC_RdWrCpltCallback(FSMC_HandleTypeDef* hfsmc)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(hfsmc);


    /* NOTE : This function Should not be modified, when the callback is needed,
              the HAL_FSMC_ReadCpltCallback could be implemented in the user file
     */
}


/**
 * @brief  read or write fifo callback in non blocking mode
 * @param  hfsmc pointer to a FSMC_HandleTypeDef structure that contains
 *         the configuration information for the specified FSMC.
 * @retval None
 */
__weak void HAL_FSMC_RdWrFifoCallback(FSMC_HandleTypeDef* hfsmc)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(hfsmc);


    /* NOTE : This function Should not be modified, when the callback is needed,
              the HAL_FSMC_WriteCpltCallback could be implemented in the user file
     */
}


/**
 * @brief  read or write fifo Over run callback in non blocking mode
 * @param  hfsmc pointer to a FSMC_HandleTypeDef structure that contains
 *         the configuration information for the specified FSMC.
 * @retval None
 */
__weak void HAL_FSMC_RdWrFifoOvCallback(FSMC_HandleTypeDef* hfsmc)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(hfsmc);


    /* NOTE : This function Should not be modified, when the callback is needed,
              the HAL_FSMC_WriteCpltCallback could be implemented in the user file
     */
}


/**
 * @brief  Error callback in non blocking mode
 * @param  hfsmc pointer to a FSMC_HandleTypeDef structure that contains
 *         the configuration information for the specified FSMC.
 * @retval None
 */
__weak void HAL_FSMC_ErrorCallback(FSMC_HandleTypeDef* hfsmc)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(hfsmc);


    /* NOTE : This function Should not be modified, when the callback is needed,
              the HAL_FSMC_WriteCpltCallback could be implemented in the user file
     */
}


/**
 * @brief  FSMC EXTM Mode Write Data
 * @param  hfsmc pointer to a FSMC_HandleTypeDef structure that contains
 *         the configuration information for the specified FSMC.
 * @param  Address
 * @param  *Buffer Write data buffer data
 * @param  Num Number words to write
 * @param  Timeout
 * @retval HAl Status
 */
HAL_StatusTypeDef HAL_FSMC_EXTM_Write(FSMC_HandleTypeDef *hfsmc, uint32_t Address, uint32_t* Buffer, uint8_t Num, uint32_t Timeout)
{
    uint32_t tickstart = 0U, index = 0U;

    if (!__HAL_FSMC_IS_ACTIVESTATUS(hfsmc, FSMC_FLAG_DONE)) {
        /* Update FSMC State to error */
        SET_BIT(hfsmc->State, HAL_FSMC_STATE_BUSY);

        return (HAL_ERROR);
    }
    /* Clear flag */
    __HAL_FSMC_CLEAR_STATUS(hfsmc, FSMC_FLAG_DONE | FSMC_FLAG_ERR);

    /* Set Wrtie Word Number */
    __HAL_FSMC_SET_RDWRNUM(hfsmc, 1);

    for (index = 0; index < Num; index++) {
        /* Set Address */
        __HAL_FSMC_SET_ADDRESS(hfsmc, Address++);

        /* Write Data */
        __HAL_FSMC_WRITE_DATA(hfsmc, *Buffer++);

        /* Start Write */
        __HAL_FSMC_START_WRITE(hfsmc);

        /* Get tick */
        tickstart = HAL_GetTick();

        /* Check End of Access flag */
        while (!__HAL_FSMC_IS_ACTIVESTATUS(hfsmc, FSMC_FLAG_DONE)) {
            /* Check if timeout is disabled (set to infinite wait) */
            if (Timeout != HAL_MAX_DELAY) {
                if ((Timeout == 0U) || ((HAL_GetTick() - tickstart) > Timeout)) {
                    /* Update FSMC state machine to timeout */
                    SET_BIT(hfsmc->State, HAL_FSMC_STATE_TIMEOUT);
                    return (HAL_TIMEOUT);
                }
                /* Check FSMC done error */
                if (__HAL_FSMC_IS_ACTIVESTATUS(hfsmc, FSMC_FLAG_ERR)) {
                    /* Update FSMC State to error */
                    SET_BIT(hfsmc->State, HAL_FSMC_STATE_ERROR);
                    return (HAL_ERROR);
                }
            }
        }
        /* Clear all flag */
        __HAL_FSMC_CLEAR_STATUS(hfsmc, FSMC_FLAG_DONE);
    }

    return (HAL_OK);
}


/**
 * @brief  FSMC EXTM Mode Read Data
 * @param  hfsmc pointer to a FSMC_HandleTypeDef structure that contains
 *         the configuration information for the specified FSMC.
 * @param  Address
 * @param  *Buffer Write data buffer data
 * @param  Num Number words to read
 * @param  Timeout
 * @retval HAl Status
 */
HAL_StatusTypeDef HAL_FSMC_EXTM_Read(FSMC_HandleTypeDef *hfsmc, uint32_t Address, uint32_t* Buffer, uint8_t Num, uint32_t Timeout)
{
    uint32_t tickstart = 0U, index = 0U;

    if (!__HAL_FSMC_IS_ACTIVESTATUS(hfsmc, FSMC_FLAG_DONE)) {
        /* Update FSMC State to error */
        SET_BIT(hfsmc->State, HAL_FSMC_STATE_BUSY);
        return (HAL_ERROR);
    }
    /* Clear flag */
    __HAL_FSMC_CLEAR_STATUS(hfsmc, FSMC_FLAG_DONE | FSMC_FLAG_ERR);

    /* Set Read Word Number */
    __HAL_FSMC_SET_RDWRNUM(hfsmc, 1);

    for (index = 0; index < Num; index++) {
        /* Set Address */
        __HAL_FSMC_SET_ADDRESS(hfsmc, Address++);

        /* Start Read */
        __HAL_FSMC_START_READ(hfsmc);

        /* Get tick */
        tickstart = HAL_GetTick();

        /* Check End of Access flag */
        while (!__HAL_FSMC_IS_ACTIVESTATUS(hfsmc, FSMC_FLAG_DONE)) {
            /* Check if timeout is disabled (set to infinite wait) */
            if (Timeout != HAL_MAX_DELAY) {
                if ((Timeout == 0U) || ((HAL_GetTick() - tickstart) > Timeout)) {
                    /* Update FSMC state machine to timeout */
                    SET_BIT(hfsmc->State, HAL_FSMC_STATE_TIMEOUT);
                    return (HAL_TIMEOUT);
                }
                /* Check FSMC done error */
                if (__HAL_FSMC_IS_ACTIVESTATUS(hfsmc, FSMC_FLAG_ERR)) {
                    /* Update FSMC State to error */
                    SET_BIT(hfsmc->State, HAL_FSMC_STATE_ERROR);
                    return (HAL_ERROR);
                }
            }
        }

        /* Read Data */
        __HAL_FSMC_READ_DATA(hfsmc, *Buffer++);
        /* Clear all flag */
        __HAL_FSMC_CLEAR_STATUS(hfsmc, FSMC_FLAG_DONE);
    }

    return (HAL_OK);
}


/**
 * @brief  FSMC EXTM FIFO Mode Write Data
 * @param  hfsmc pointer to a FSMC_HandleTypeDef structure that contains
 *        the configuration information for the specified FSMC.
 * @param  Address
 * @param  *Buffer Write data buffer data
 * @param  Num Number word to write
 * @param  Timeout
 * @retval HAl Status
 */
HAL_StatusTypeDef HAL_FSMC_EXTMFIFO_Write(FSMC_HandleTypeDef *hfsmc, uint32_t Address, uint32_t* Buffer, uint8_t Num, uint32_t Timeout)
{
    uint32_t tickstart = 0U, index = 0U, count = 0U;

    if (!__HAL_FSMC_IS_ACTIVESTATUS(hfsmc, FSMC_FLAG_DONE)) {
        /* Update FSMC State to error */
        SET_BIT(hfsmc->State, HAL_FSMC_STATE_BUSY);

        return (HAL_ERROR);
    }

    /* Clear flag */
    __HAL_FSMC_CLEAR_STATUS(hfsmc, FSMC_FLAG_RSTFIFO | FSMC_FLAG_DONE | FSMC_FLAG_ERR);
    LL_FSMC_EnableWrFifo(hfsmc->Instance);

    while (Num) {
        /* Set Address */
        __HAL_FSMC_SET_ADDRESS(hfsmc, Address + count);

        if (Num >= 255) {
            count = 255;
            /* Set Wrtie Word Number */
            __HAL_FSMC_SET_RDWRNUM(hfsmc, 255);
        } else {
            count = Num;
            /* Set Wrtie Word Number */
            __HAL_FSMC_SET_RDWRNUM(hfsmc, Num);
        }

        /* Start Write */
        __HAL_FSMC_START_WRITE(hfsmc);
        for (index = 0; index < count; index++, Num--) {
            /* Write Data */
            __HAL_FSMC_WRITE_DATA(hfsmc, *Buffer++);
        }

        /* Get tick */
        tickstart = HAL_GetTick();
        /* Check End of Access flag */
        while (!__HAL_FSMC_IS_ACTIVESTATUS(hfsmc, FSMC_FLAG_DONE)) {
            /* Check if timeout is disabled (set to infinite wait) */
            if (Timeout != HAL_MAX_DELAY) {
                if ((Timeout == 0U) || ((HAL_GetTick() - tickstart) > Timeout)) {
                    /* Update FSMC state machine to timeout */
                    SET_BIT(hfsmc->State, HAL_FSMC_STATE_TIMEOUT);
                    return (HAL_TIMEOUT);
                }
                /* Check FSMC done error */
                if (__HAL_FSMC_IS_ACTIVESTATUS(hfsmc, FSMC_FLAG_ERR)) {
                    /* Update FSMC State to error */
                    SET_BIT(hfsmc->State, HAL_FSMC_STATE_ERROR);
                    return (HAL_ERROR);
                }
            }
        }
        /* Clear all flag */
        __HAL_FSMC_CLEAR_STATUS(hfsmc, FSMC_FLAG_DONE);
    }

    return (HAL_OK);
}


/**
 * @brief  FSMC EXTM FIFO Mode Write Data
 * @param  hfsmc pointer to a FSMC_HandleTypeDef structure that contains
 *        the configuration information for the specified FSMC.
 * @param  Address
 * @param  *Buffer Write data buffer data
 * @param  Num Number word to write
 * @param  Timeout
 * @retval HAl Status
 */
HAL_StatusTypeDef HAL_FSMC_EXTMFIFO_Read(FSMC_HandleTypeDef *hfsmc, uint32_t Address, uint32_t* Buffer, uint8_t Num, uint32_t Timeout)
{
    uint32_t tickstart = 0U, index = 0U, count = 0U;

    if (!__HAL_FSMC_IS_ACTIVESTATUS(hfsmc, FSMC_FLAG_DONE)) {
        /* Update FSMC State to error */
        SET_BIT(hfsmc->State, HAL_FSMC_STATE_BUSY);
        return (HAL_ERROR);
    }

    /* Clear flag */
    __HAL_FSMC_CLEAR_STATUS(hfsmc, FSMC_FLAG_RSTFIFO | FSMC_FLAG_DONE | FSMC_FLAG_ERR);
    LL_FSMC_EnableRdFifo(hfsmc->Instance);

    while (Num) {
        /* Set Address */
        __HAL_FSMC_SET_ADDRESS(hfsmc, Address + count);

        if (Num >= 255) {
            count = 255;
            /* Set Wrtie Word Number */
            __HAL_FSMC_SET_RDWRNUM(hfsmc, 255);
        } else {
            count = Num;
            /* Set Wrtie Word Number */
            __HAL_FSMC_SET_RDWRNUM(hfsmc, Num);
        }

        /* Start Read Data */
        __HAL_FSMC_START_READ(hfsmc);
        for (index = 0; index < count; index++, Num--) {
            /* Read Data */
            __HAL_FSMC_READ_DATA(hfsmc, *Buffer++);
        }
        /* Get tick */
        tickstart = HAL_GetTick();
        /* Check End of Access flag */
        while (!__HAL_FSMC_IS_ACTIVESTATUS(hfsmc, FSMC_FLAG_DONE)) {
            /* Check if timeout is disabled (set to infinite wait) */
            if (Timeout != HAL_MAX_DELAY) {
                if ((Timeout == 0U) || ((HAL_GetTick() - tickstart) > Timeout)) {
                    /* Update FSMC state machine to timeout */
                    SET_BIT(hfsmc->State, HAL_FSMC_STATE_TIMEOUT);
                    return (HAL_TIMEOUT);
                }
                /* Check FSMC done error */
                if (__HAL_FSMC_IS_ACTIVESTATUS(hfsmc, FSMC_FLAG_ERR)) {
                    /* Update FSMC State to error */
                    SET_BIT(hfsmc->State, HAL_FSMC_STATE_ERROR);
                    return (HAL_ERROR);
                }
            }
        }
        /* Clear all flag */
        __HAL_FSMC_CLEAR_STATUS(hfsmc, FSMC_FLAG_DONE);

    }

    return (HAL_OK);
}


/** @defgroup FSMC_Exported_Functions_Group4 Peripheral State and Errors functions
 *  @brief   Peripheral State and Errors functions
 *
   @verbatim
   ==============================================================================
 ##### Peripheral State and Errors functions #####
   ==============================================================================
   @endverbatim
 * @{
 */


/**
 * @brief  return the FSMC state
 * @param  hfsmc pointer to a FSMC_HandleTypeDef structure that contains
 *         the configuration information for the specified FSMC.
 * @retval HAL state
 */
uint32_t HAL_FSMC_GetState(FSMC_HandleTypeDef* hfsmc)
{
    /* Return FSMC state */
    return (hfsmc->State);
}


/**
 * @}
 */

/* Private functions ---------------------------------------------------------*/


/**
 * @brief  Configures the FSMC NORSRAM.
 * @param  hfsmc pointer to a FSMC_HandleTypeDef structure that contains
 *         the configuration information for the specified FSMC NORSRAM.
 * @retval
 */
static void __FSMC_NORSRAMConfig(FSMC_HandleTypeDef *hfsmc)
{
    /* Check the parameters */
    assert_param(IS_FSMC_MEMORYWIDTH(hfsmc->NORSRAMInit.MemoryWidth));
    assert_param(IS_FSMC_NWAITEN(hfsmc->NORSRAMInit.NWaitEn));
    assert_param(IS_FSMC_NWAITPOL(hfsmc->NORSRAMInit.NWaitPol));
    assert_param(IS_FSMC_CLKEN(hfsmc->NORSRAMInit.ClkEn));
    assert_param(IS_FSMC_ACCESSMODE(hfsmc->NORSRAMInit.AccessMode));

    /* Disable the Peripheral */
    __HAL_FSMC_DISABLE(hfsmc);

    /* Config CTRL Reg */
    MODIFY_REG(hfsmc->Instance->CTRL, 0x0701FF,
               FSMC_NORSRAM_MODE |
               hfsmc->NORSRAMInit.MemoryWidth |
               hfsmc->NORSRAMInit.NWaitEn |
               hfsmc->NORSRAMInit.NWaitPol |
               hfsmc->NORSRAMInit.ClkEn);

    /* Config SYNCT Reg */
    if (FSMC_CLK_ENABLE == hfsmc->NORSRAMInit.ClkEn) {
        assert_param(IS_FSMC_CLKDIV(hfsmc->NORSRAMInit.ClkDiv));

        LL_FSMC_SetClkDiv(hfsmc->Instance, hfsmc->NORSRAMInit.ClkDiv);
    }

    /* Config EXTM Reg */
    MODIFY_REG(hfsmc->Instance->EXTM, 0x0F, hfsmc->NORSRAMInit.AccessMode);
}


/**
 * @brief  Configures the FSMC LCD.
 * @param  hfsmc pointer to a FSMC_HandleTypeDef structure that contains
 *         the configuration information for the specified FSMC LCD.
 * @retval
 */
static void __FSMC_LCDConfig(FSMC_HandleTypeDef *hfsmc)
{
    /* Check the parameters */
    assert_param(IS_FSMC_LCDWIDTH(hfsmc->LCDInit.LcdWidth));
    assert_param(IS_FSMC_LCDTYPE(hfsmc->LCDInit.LcdType));
    assert_param(IS_FSMC_ENORRDPOL(hfsmc->LCDInit.EnOrRdPol));
    assert_param(IS_FSMC_RWORWRPOL(hfsmc->LCDInit.RwOrWrPol));
    assert_param(IS_FSMC_ACCESSMODE(hfsmc->LCDInit.AccessMode));

    /* Disable the Peripheral */
    __HAL_FSMC_DISABLE(hfsmc);

    /* Config CTRL Reg */
    MODIFY_REG(hfsmc->Instance->CTRL, 0x0701FF,
               FSMC_LCD_MODE |
               hfsmc->LCDInit.LcdWidth |
               hfsmc->LCDInit.LcdType |
               hfsmc->LCDInit.EnOrRdPol |
               hfsmc->LCDInit.RwOrWrPol |
               FSMC_CLK_DISABLE);

    /* Config EXTM Reg */
    MODIFY_REG(hfsmc->Instance->EXTM, 0x0F, hfsmc->LCDInit.AccessMode);
}


/**
 * @}FSMC_HAL
 */

#endif /* FSMC_MODULE_ENABLED */


/**
 * @}FM15F3XX_HAL_Driver
 */

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
