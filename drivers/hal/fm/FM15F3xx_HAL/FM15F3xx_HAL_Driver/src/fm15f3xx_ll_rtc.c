/**
 ******************************************************************************
 * @file    fm15f3xx_ll_rtc.c
 * @author  SRG
 * @version V1.0.0
 * @date    2020-03-17
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */
#include "fm15f3xx_ll_rtc.h"


void LL_RTC_Init(LL_UtcTimer_InitTypeDef *RTC_InitStructure)
{
    LL_RTC_DisableAllTimer();

    if (RTC_InitStructure->ClkOuputEn)
        LL_RTC_Enable32kOutput();
    else
        LL_RTC_Disable32kOutput();


    if (RTC_InitStructure->WakeUpOpenDrain)
        LL_RTC_EnableWakupPinOD();
    else
        LL_RTC_DisableWakupPinOD();

    if (RTC_InitStructure->WakeUpMode)
        LL_RTC_EnableWakeupPin();
    else
        LL_RTC_DisableWakeupPin();

    if (RTC_InitStructure->WakeUpPolarity)
        LL_RTC_EnableWakupPinInv();
    else
        LL_RTC_DisableWakupPinInv();

    LL_RTC_LockReg(RTC_InitStructure->LockMode);

    LL_RTC_SetSecondTimerValue(RTC_InitStructure->TimeSeconds);
    LL_RTC_SetCycleTimerValue(RTC_InitStructure->TimeCycle);
    LL_RTC_SetTimerPrescaler(RTC_InitStructure->TimePrescaler);
    LL_RTC_SetSecondTimerAlarm(RTC_InitStructure->SecondsAlarm);
    LL_RTC_SetCycleTimerAlarm(RTC_InitStructure->CycleAlarm);
    if (RTC_InitStructure->CompensationDirection)
        LL_RTC_EnableFallCompensation();
    else
        LL_RTC_EnableRaiseCompensation();

    LL_RTC_SetCompensationInterval(RTC_InitStructure->CompensationInterVal);
    LL_RTC_SetCompensationValue(RTC_InitStructure->CompensationValue);

    LL_RTC_EnableUtc();
}


/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/

