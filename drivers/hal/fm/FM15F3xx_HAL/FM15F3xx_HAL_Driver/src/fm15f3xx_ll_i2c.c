/**
 ******************************************************************************
 * @file    fm15f3xx_ll_i2c.c
 * @author  TYW
 * @version V1.0.0
 * @date    2020-03-17
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */
/* Define to prevent recursive inclusion -------------------------------------*/


#include "fm15f3xx.h"
#include "fm15f3xx_ll_i2c.h"
#include "fm15f3xx_ll_cmu.h"


/** @addtogroup FM15F3XX_LL_Driver
 * @{
 */

#if defined (I2C0) || defined (I2C1)


/** @defgroup I2C_Private_Functions
 * @{
 */


/** @defgroup I2C address in slave mode,should be changed according to devices
 * @{
 */
#define SLAVE_7ADDR     (0x10)
#define SLAVE_10ADDR    (0x0110)


/**
 * @}
 */


/** @defgroup I2C address in master mode,should be changed according to devices
 * @{
 */
#define MASTER_7ADDR_W  (0x22)
#define MASTER_7ADDR_R  (0x23)
#define MASTER_10ADDR_W (0xF011)
#define MASTER_10ADDR_R (0xF111)


/**
 * @}
 */


/**
 * @brief Set each @ref LL_I2C_StructInit field to default value.
 * @param  UART_InitStruct pointer to a @ref LL_I2C_StructInit structure
 *                          whose fields will be set to default values.
 * @retval None
 */
void LL_I2C_StructInit(LL_I2C_InitTypeDef *I2C_InitStruct)
{
    I2C_InitStruct->Mode        = LL_I2C_MASTER_MODE;
    I2C_InitStruct->ClockSpeed  = LL_I2C_MAX_SPEED_FAST;
    I2C_InitStruct->OwnAddress  = SLAVE_7ADDR;
    I2C_InitStruct->OwnAddrSize = LL_I2C_SLAVESSPCON_A7;
    I2C_InitStruct->ClkHoldEn   = LL_I2C_SLAVESSPCON_CKSEN;
    I2C_InitStruct->MasterIrq   = LL_I2C_MASTERSSPIR_IE;
    I2C_InitStruct->SlaveIrq    = LL_I2C_SLAVESSPIR_IE;
}


/**
 * @brief  Cfg the I2C BAUD
 * @rmtoll MSSPBRG    I2C_MASTERSSPBRG       LL_I2C_ConfigSpeed
 * @param  I2Cx I2C Instance
 * @param  I2C_BRG value can be one of the following values:
 *         @arg @ref LL_I2C_MAX_SPEED_STANDARD
 *         @arg @ref LL_I2C_MAX_SPEED_FAST
 */
void LL_I2C_ConfigSpeed(I2C_TypeDef *I2Cx, uint32_t I2C_BRG)
{
    uint32_t SysClk = 0, BusDiv = 0, BrgCfg = 0;
    BusDiv  = LL_CMU_GetBusClkPrescaler() + 1;
    SysClk  = LL_CMU_SystemCoreClockUpdate();

    BrgCfg = (SysClk / BusDiv / I2C_BRG / 4) - 1;

    LL_I2C_SetMasterBaud(I2Cx, BrgCfg);
}


/**
 * @brief  Initialize the I2C registers according to the specified parameters in I2C_InitStruct.
 * @param  I2Cx I2C Instance.
 * @param  I2C_InitStruct pointer to a @ref LL_I2C_InitTypeDef structure.
 */
void LL_I2C_Init(I2C_TypeDef *I2Cx, LL_I2C_InitTypeDef *I2C_InitStruct)
{
    /* Disable the selected I2Cx Peripheral */
    LL_I2C_Disable(I2Cx);

    I2Cx->MSSPSTAT  &= 0;
    I2Cx->SSSPSTAT  &= 0;
    LL_I2C_ClearMFlag_IT(I2Cx);
    LL_I2C_ClearSFlag_IT(I2Cx);

    if (I2C_InitStruct->Mode == LL_I2C_MASTER_MODE) {
        /* Retrieve Clock frequencies */
        LL_I2C_ConfigSpeed(I2Cx, I2C_InitStruct->ClockSpeed);
        if (I2C_InitStruct->MasterIrq == LL_I2C_MASTERSSPIR_IE)
            LL_I2C_EnableMasterINT(I2Cx);
        else
            LL_I2C_DisableMasterINT(I2Cx);

        LL_I2C_DisableSlaveINT(I2Cx);
        LL_I2C_EnableMasterMode(I2Cx);
    } else {
        /*set slave AddressSize and address */
        LL_I2C_SetSlaveAddress(I2Cx, I2C_InitStruct->OwnAddress, I2C_InitStruct->OwnAddrSize);
        if (I2C_InitStruct->ClkHoldEn == LL_I2C_SLAVESSPCON_CKSEN)
            LL_I2C_EnableClockStretch(I2Cx);
        else
            LL_I2C_DisableClockStretch(I2Cx);

        if (I2C_InitStruct->SlaveIrq == LL_I2C_SLAVESSPIR_IE)
            LL_I2C_EnableSlaveINT(I2Cx);
        else
            LL_I2C_DisableSlaveINT(I2Cx);

        LL_I2C_DisableMasterINT(I2Cx);
        LL_I2C_EnableSlaveACK(I2Cx);
        LL_I2C_EnableSlave(I2Cx);
    }
}


/**
 * @}
 */


/**
 * @}
 */

#endif /* I2C0 || I2C1 */


/**
 * @}
 */

/************************ (C) COPYRIGHT Fudan Microelectronics *****END OF FILE****/
