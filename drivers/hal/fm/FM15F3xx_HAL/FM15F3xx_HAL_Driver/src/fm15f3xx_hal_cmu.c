/**
 ******************************************************************************
 * @file    fm15f3xx_hal_cmu.c
 * @author  WYL
 * @brief   CMU HAL module driver.
 *          This file provides firmware functions to manage the following
 *          functionalities of the Reset and Clock Control (CMU) peripheral:
 *           + Initialization and de-initialization functions
 *           + Peripheral Control functions
 *
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */


/* Includes ------------------------------------------------------------------*/
#include "fm15f3xx_hal.h"
#include "fm15f3xx_ll_pmu.h"

/** @addtogroup FM15F3xx_HAL_Driver
 * @{
 */


/** @defgroup CMU CMU
 * @brief CMU HAL module driver
 * @{
 */

#ifdef CMU_MODULE_ENABLED

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/


/** @addtogroup CMU_Private_Constants
 * @{
 */

/* Private macro -------------------------------------------------------------*/


/**
 * @}
 */

/* Private variables ---------------------------------------------------------*/


/** @defgroup CMU_Private_Variables CMU Private Variables
 * @{
 */


/*----------------------------------------------------------------------------
   Clock Variable definitions
 *----------------------------------------------------------------------------*/


/* ToDo: initialize SystemCoreClock with the system core clock frequency value
         achieved after system intitialization.
         This means system core clock frequency after call to SystemInit()    */
uint32_t    SystemCoreClock = IRC16M_VALUE;
uint32_t    OscClock        = 12000000; //12MHz


static CMU_SysClk_CFG const SysClkCfg[40] = {
    { 1,  0x00 }, { 2,  0x01 }, { 3,  0x02 }, { 4,  0x03 }, { 5,  0x04 }, { 6,  0x05 }, { 7,   0x06 }, { 8,   0x07 }, { 9,   0x08 }, { 10,  0x09 },
    { 11, 0x0A }, { 12, 0x0B }, { 13, 0x0C }, { 14, 0x0D }, { 15, 0x0E }, { 16, 0x0F }, { 18,  0x18 }, { 20,  0x19 }, { 22,  0x1A }, { 24,  0x1B },
    { 26, 0x1C }, { 28, 0x1D }, { 30, 0x1E }, { 32, 0x1F }, { 36, 0x28 }, { 40, 0x29 }, { 44,  0x2A }, { 48,  0x2B }, { 52,  0x2C }, { 56,  0x2D },
    { 60, 0x2E }, { 64, 0x2F }, { 72, 0x38 }, { 80, 0x39 }, { 88, 0x3A }, { 96, 0x3B }, { 104, 0x3C }, { 112, 0x3D }, { 120, 0x3E }, { 128, 0x3F }
};


/**
 * @}
 */
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/


/** @defgroup CMU_Exported_Functions CMU Exported Functions
 *  @{
 */
static HAL_StatusTypeDef __CMU_ClockConfig(CMU_InitTypeDef *CMU_InitStruct);


/** @defgroup Initialization and de-initialization functions
 *  @brief    Initialization and Configuration functions
 *
   @verbatim
   ===============================================================================
 ##### Initialization and de-initialization functions #####
   ===============================================================================
   @endverbatim
 * @{
 */


/**
 * @brief  Resets the MCU clock configuration to the default reset state.
 * @retval HAL status
 */
__weak HAL_StatusTypeDef HAL_CMU_DeInit(void)
{
    return (HAL_OK);
}


/**
 * @brief  Initializes the CPU, SYS and BUS clocks according to the specified
 *         parameters in the CMU_ClkInitStruct.
 * @param  CMU_ClkInitStruct pointer to an CMU_InitTypeDef structure that
 *         contains the configuration information for the CMU peripheral.
 * @retval None
 */
HAL_StatusTypeDef HAL_CMU_ClockInit(CMU_InitTypeDef *CMU_InitStruct)
{
    /* Check Null pointer */
    if (CMU_InitStruct == NULL) {
        return (HAL_ERROR);
    }
    /* Check the parameters */
    assert_param(IS_CMU_OSCTYPE(CMU_InitStruct->OscType));
    assert_param(IS_CMU_OSCVALUE(CMU_InitStruct->XtalHz));
    assert_param(IS_FUNCTIONAL_STATE(CMU_InitStruct->PLL.PLLState));
    assert_param(IS_CMU_MAINCLKSRC(CMU_InitStruct->MainClkSrc));
    assert_param(IS_CMU_MAINCLKDIV(CMU_InitStruct->MainClkDiv));
    assert_param(IS_CMU_MAINCLKDIV(CMU_InitStruct->BusClkDiv));

    if (CMU_InitStruct->PLL.PLLState == ENABLE) {
        assert_param(IS_CMU_PLLCLKSRC(CMU_InitStruct->PLL.PLLSource));
        assert_param(IS_CMU_PLLCLK(CMU_InitStruct->PLL.PLLFreqHz));
    }


    if (HAL_OK != __CMU_ClockConfig(CMU_InitStruct)) {
        return (HAL_ERROR);
    }

    /* Update the SystemCoreClock*/
    HAL_CMU_SystemCoreClockUpdate();

    /* if Enable SysTick Configure ,Updata the source of time base considering new system clocks settings */
    if ((SysTick->CTRL & SysTick_CTRL_ENABLE_Msk) == SysTick_CTRL_ENABLE_Msk) {
        HAL_InitTick(TICK_INT_PRIORITY);
    }

    return (HAL_OK);
}


/**
 * @brief  Config the Security Clock.
 * @param  CFG This parameter can be one of the following values:
 *         @arg CMU_SECCLK_SYNSYSCLK
 *         @arg CMU_SECCLK_RNDSYSCLK
 * @retval None
 */
void HAL_CMU_SECConfig(uint8_t CFG)
{
    /* Check the parameters */
    assert_param(IS_CMU_SECCLKCONFIG(CFG));
    LL_CMU_SetSecuClkSrc(CFG);
}

/**
 * @brief  Restore the Clock to PLLCLK.
 * @param  sysclkfreq This parameter can be one of the following values:
 *         @arg CMU_SECCLK_SYNSYSCLK
 * @retval None
 */
/*
   * 时钟恢复启动配置的PLL clk
   * sysclkfreq为用户设置的PLL时钟频率
 */
HAL_StatusTypeDef HAL_CMU_SysClkRestoreToPLL(uint32_t sysclkfreq)
{
    uint32_t    div_v;
    uint32_t    i = 0;
    //waiting pll ready
    while (!LL_CMU_IsLock_PLL()) {
        if (++i > 1000 * 1000)
            return (HAL_ERROR);
    }
    i = 0;
    while (!LL_CMU_IsReady_PLL()) {
        if (++i > 1000 * 100)
            return (HAL_ERROR);
    }
    div_v   = LL_CMU_GET_SYSCLKDIV();
    i       = sysclkfreq / (8 * 1000000);
    if (sysclkfreq > 48 * 1000000) {                        //sysclk>48MHz open iload
        LL_PMU_EnableiLoad();
        LL_CMU_SetSysClkPrescaler(LL_CMU_REUSEDIV3_DIV2);   //must set
    }
    LL_CMU_SetMCGMainClkSrc(LL_CMU_MCGMAINCLK_PLLCLK);
    //software dely
    while (i--)
        ;
    LL_CMU_SetSysClkPrescaler(div_v - 1);                   //切到目标频率
    return (HAL_OK);
}


/**
 * @}
 */


/** @defgroupPeripheral Control functions
 *  @brief   CMU clocks control functions
 *
   @verbatim
   ===============================================================================
 ##### Peripheral Control functions #####
   ===============================================================================
   @endverbatim
 * @{
 */


/**
 * @brief  Returns the SYSCLK frequency
 * @retval SYSCLK frequency
 */
__weak uint32_t HAL_CMU_SystemCoreClockUpdate(void)
{
    uint32_t    pllfb = 0U, pllp = 0U, div = 0U;
    uint32_t    sysclockfreq = 0U;

    /* Get SYSCLK source -------------------------------------------------------*/
    switch (LL_CMU_GetMCGMainClkSrc()) {
    case LL_CMU_MCGMAINCLK_IRC4M: { /* IRC4M used as system clock source */
        sysclockfreq = IRC4M_VALUE;
        break;
    }
    case LL_CMU_MCGMAINCLK_IRC16M: { /* IRC16M used as system clock  source */
        sysclockfreq = IRC16M_VALUE;
        break;
    }
    case LL_CMU_MCGMAINCLK_OSCCLK: { /* OSC used as system clock  source */
        sysclockfreq = OscClock;
        break;
    }
    case LL_CMU_MCGMAINCLK_PLLCLK: { /* PLL used as system clock  source */
        /* SYSCLK = (HSE_VALUE or IRC16M_VALUE) * (PLLFB + 16) / (2 * (PLLN+1) ) */

        pllfb   = LL_CMU_GetPLLFB();
        pllp    = LL_CMU_GetPLLPRE();

        if (LL_CMU_GetPLLClkSrc() != LL_CMU_PLLCLK_IRC16M) {
            /* HSE used as PLL clock source */
            sysclockfreq = (uint32_t)(OscClock * (pllfb + 16) / (2 * (pllp + 1)));
        } else {
            /* IRC16M used as PLL clock source */
            sysclockfreq = (uint32_t)((IRC16M_VALUE * (pllfb + 16)) / (2 * (pllp + 1)));
        }
        break;
    }
    default: {
        sysclockfreq = SystemCoreClock;
        break;
    }
    }
    div             = LL_CMU_GET_SYSCLKDIV();
    sysclockfreq    /= div;
    SystemCoreClock = sysclockfreq;
    return (sysclockfreq);
}


/**
 * @brief  Returns the Sys CLK frequency
 * @retval BUS CLK frequency
 */
uint32_t HAL_CMU_GetSysClock(void)
{
    HAL_CMU_SystemCoreClockUpdate();
    return (SystemCoreClock);
}


/**
 * @brief  Returns the Osc CLK frequency
 * @retval BUS CLK frequency
 */
uint32_t HAL_CMU_GetOscClock(void)
{
    return (OscClock);
}


/**
 * @brief  Returns the BUS CLK frequency
 * @retval BUS CLK frequency
 */
uint32_t HAL_CMU_GetBusClock(void)
{
    return (HAL_CMU_GetSysClock() / (LL_CMU_GetBusClkPrescaler() + 1));
}


/**
 * @brief  CLK Source and Div Config of Moudel in MCU
 * @param  Module of the CMU peripheral.This parameter can be one of the following values:
 *         @arg CMU_MODULE_TRNG
 *         @arg CMU_MODULE_CT
 *         @arg CMU_MODULE_UART
 *         @arg CMU_MODULE_USBFS
 *         @arg CMU_MODULE_SHIELD
 *         @arg CMU_MODULE_ALWAYSON
 *         @arg CMU_MODULE_RTCMAINBUS
 *         @arg CMU_MODULE_WDT
 *         @arg CMU_MODULE_ADC
 *         @arg CMU_MODULE_CLKOUT0
 *         @arg CMU_MODULE_CLKOUT1
 *         @arg CMU_MODULE_CLKOUT2
 *         @arg CMU_MODULE_BKPSELFTEST
 *         @arg CMU_MODULE_STIM0
 *         @arg CMU_MODULE_STIM1
 *         @arg CMU_MODULE_STIM2
 *         @arg CMU_MODULE_STIM3
 *         @arg CMU_MODULE_STIM4
 *         @arg CMU_MODULE_STIM5
 *         @arg CMU_MODULE_USBPHY
 *         @arg CMU_MODULE_SYSTICK
 *         @arg CMU_MODULE_SYSTEST
 *         @arg CMU_MODULE_PLLEXT
 * @param  clock of the Module peripheral.
 * @param  prescaler of the Module peripheral.
 * @retval None
 */
void HAL_CMU_ModuleClockConfig(uint8_t Module, uint32_t ClockSrc, uint32_t ClockDiv)
{
    /* Check the parameters */
    assert_param(IS_CMU_MODULESEL(Module));

    switch (Module) {
    case CMU_MODULE_TRNG: {
        assert_param(IS_CMU_TRNGCLKDIV(ClockDiv));
        LL_CMU_SetTRngClkPrescaler(ClockDiv);
        break;
    }
    case CMU_MODULE_CT: {
        assert_param(IS_CMU_CTCLKSEL(ClockSrc));
        assert_param(IS_CMU_CTCLKDIV(ClockDiv));
        LL_CMU_ConfigCtClk(ClockSrc, ClockDiv);
        break;
    }
    case CMU_MODULE_UART: {
        assert_param(IS_CMU_UARTCLKSEL(ClockSrc));
        assert_param(IS_CMU_UARTCLKDIV(ClockDiv));
        LL_CMU_ConfigUARTClk(ClockSrc, ClockDiv);
        break;
    }
    case CMU_MODULE_USBFS: {
        assert_param(IS_CMU_USBFSCLKSEL(ClockSrc));
        LL_CMU_SetUSBFSClkSrc(ClockSrc);
        break;
    }
    case CMU_MODULE_SHIELD: {
        assert_param(IS_CMU_SHIELDCLKSEL(ClockSrc));
        assert_param(IS_CMU_SHIELDCLKDIV(ClockDiv));
        LL_CMU_ConfigShieldClk(ClockSrc, ClockDiv);
        break;
    }
    case CMU_MODULE_ALWAYSON: {
        assert_param(IS_CMU_ALWAYSONCLKSEL(ClockSrc));
        LL_CMU_SetAonBusClkSrc(ClockSrc);
        break;
    }
    case CMU_MODULE_RTCMAINBUS: {
        assert_param(IS_CMU_RTCMAINBUSCLKSEL(ClockDiv));
        LL_CMU_SetRtcMainBusClkPrescaler(ClockDiv);
        break;
    }
    case CMU_MODULE_WDT: {
        assert_param(IS_CMU_WTDCLKSEL(ClockSrc));
        LL_CMU_SetWDTClkSrc(ClockSrc);
        break;
    }
    case CMU_MODULE_ADC: {
        assert_param(IS_CMU_ADCCLKSEL(ClockSrc));
        assert_param(IS_CMU_ADCCLKDIV(ClockDiv));
        LL_CMU_ConfigADCClk(ClockSrc, ClockDiv);
        break;
    }
    case CMU_MODULE_CLKOUT0: {
        assert_param(IS_CMU_CLKOUTCLKSEL(ClockSrc));
        LL_CMU_SetCout0ClkSrc(ClockSrc);
        break;
    }
    case CMU_MODULE_CLKOUT1: {
        assert_param(IS_CMU_CLKOUTCLKSEL(ClockSrc));
        LL_CMU_SetCout1ClkSrc(ClockSrc);
        break;
    }
    case CMU_MODULE_CLKOUT2: {
        assert_param(IS_CMU_CLKOUTCLKSEL(ClockSrc));
        LL_CMU_SetCout2ClkSrc(ClockSrc);
        break;
    }
    case CMU_MODULE_BKPSELFTEST: {
        assert_param(IS_CMU_BKPSELFTESTCLKDIV(ClockDiv));
        LL_CMU_SetBkpTestClkPrescaler(ClockDiv);
        break;
    }
    case CMU_MODULE_STIM0: {
        assert_param(IS_CMU_STIMCLKSEL(ClockSrc));
        LL_CMU_SetStimerClkSrc(0, ClockSrc);
        break;
    }
    case CMU_MODULE_STIM1: {
        assert_param(IS_CMU_STIMCLKSEL(ClockSrc));
        LL_CMU_SetStimerClkSrc(1, ClockSrc);
        break;
    }
    case CMU_MODULE_STIM2: {
        assert_param(IS_CMU_STIMCLKSEL(ClockSrc));
        LL_CMU_SetStimerClkSrc(2, ClockSrc);
        break;
    }
    case CMU_MODULE_STIM3: {
        assert_param(IS_CMU_STIMCLKSEL(ClockSrc));
        LL_CMU_SetStimerClkSrc(3, ClockSrc);
        break;
    }
    case CMU_MODULE_STIM4: {
        assert_param(IS_CMU_STIMCLKSEL(ClockSrc));
        LL_CMU_SetStimerClkSrc(4, ClockSrc);
        break;
    }
    case CMU_MODULE_STIM5: {
        assert_param(IS_CMU_STIMCLKSEL(ClockSrc));
        LL_CMU_SetStimerClkSrc(5, ClockSrc);
        break;
    }
    case CMU_MODULE_USBPHY: {
        assert_param(IS_CMU_USBPHYCLKSEL(ClockSrc));
        assert_param(IS_CMU_USBPHYCLKDIV(ClockDiv));
        LL_CMU_ConfigUSBPhyClk(ClockSrc, ClockDiv);
        break;
    }
    case CMU_MODULE_SYSTICK: {
        assert_param(IS_CMU_SYSTICKCLKSEL(ClockSrc));
        assert_param(IS_CMU_SYSTICKCLKDIV(ClockDiv));
        LL_CMU_ConfigSysTickRefClk(ClockSrc, ClockDiv);
        break;
    }
    case CMU_MODULE_SYSTEST: {
        assert_param(IS_CMU_SYSTESTCLKDIV(ClockDiv));
        LL_CMU_SetSysTestClkPrescaler(ClockDiv);
        break;
    }
    case CMU_MODULE_PLLEXT: {
        assert_param(IS_CMU_PLLEXTCLKSEL(ClockSrc));
        assert_param(IS_CMU_PLLEXTCLKDIV(ClockDiv));
        LL_CMU_ConfigPLLExtClk(ClockSrc, ClockDiv);
        break;
    }
    }
}


/**
 * @brief  When MCU In Low Power Mode Clock Source Config
 * @param  Clk This parameter can be a combination of the following values:
 *         @arg @ref CMU_POWERDOWN_POR_ENABLE
 *         @arg @ref CMU_POWERDOWN_LPO_ENABLE
 *         @arg @ref CMU_STANDBY_IRC4M_ENABLE
 *         @arg @ref CMU_STOP_IRC4M_ENABLE
 * @param  State ENABLE or DISABLE
 * @retval None
 */
void HAL_CMU_LowPowerModeClockSourceConfig(uint32_t ClkSrc, uint32_t State)
{
    assert_param(IS_CMU_LPMODECLKCFG(ClkSrc));
    assert_param(IS_FUNCTIONAL_STATE(State));

    if (ENABLE == State) {
        LL_CMU_LP_EnableClkSrc(ClkSrc);
    } else {
        LL_CMU_LP_DisableClkSrc(ClkSrc);
    }
}


/**
 * @brief  When MCU In Idle Mode Root Clock Source Config
 * @param  Clk This parameter can be a combination of the following values:
 *         @arg @ref CMU_IDLE_FLL_ENABLE
 *         @arg @ref CMU_IDLE_PLL_ENABLE
 *         @arg @ref CMU_IDLE_OSC_ENABLE
 *         @arg @ref CMU_IDLE_IRC16M_ENABLE
 * @param  State ENABLE or DISABLE
 * @retval None
 */
void HAL_CMU_IdleModeRootClockSourceConfig(uint32_t ClkSrc, uint32_t State)
{
    assert_param(IS_CMU_IDLEMODECLKCFG(ClkSrc));
    assert_param(IS_FUNCTIONAL_STATE(State));
    if (ENABLE == State) {
        LL_CMU_IDLE_EnableRootClkSrc(ClkSrc);
    } else {
        LL_CMU_IDLE_DisableRootClkSrc(ClkSrc);
    }
}


/**
 * @brief  When MCU In Stop Mode Clock Source Config
 * @param  Clk This parameter can be a combination of the following values:
 *         @arg @ref CMU_STOP_FLL_ENABLE
 *         @arg @ref CMU_STOP_PLL_ENABLE
 *         @arg @ref CMU_STOP_OSC_ENABLE
 *         @arg @ref CMU_STOP_IRC16M_ENABLE
 * @param  State ENABLE or DISABLE
 * @retval None
 */
void HAL_CMU_StopModeClockSourceConfig(uint32_t ClkSrc, uint32_t State)
{
    assert_param(IS_CMU_STOPMODECLKCFG(ClkSrc));
    assert_param(IS_FUNCTIONAL_STATE(State));
    if (ENABLE == State) {
        LL_CMU_STOP_EnableClkSrc(ClkSrc);
    } else {
        LL_CMU_STOP_DisableClkSrc(ClkSrc);
    }
}


/**
 * @brief  When MCU In Idle Mode Module Clock Config
 * @param  MdlClkSel This parameter can be a combination of the following values:
 *         @arg CMU_IDLE_MODULECLK_SHIELD
 *         @arg CMU_IDLE_MODULECLK_WDT
 *         @arg CMU_IDLE_MODULECLK_ADC
 *         @arg CMU_IDLE_MODULECLK_TRNG
 *         @arg CMU_IDLE_MODULECLK_CT
 *         @arg CMU_IDLE_MODULECLK_UART
 *         @arg CMU_IDLE_MODULECLK_USBFS
 *         @arg CMU_IDLE_MODULECLK_COUT0
 *         @arg CMU_IDLE_MODULECLK_COUT1
 *         @arg CMU_IDLE_MODULECLK_COUT2
 *         @arg CMU_IDLE_MODULECLK_STIMER0
 *         @arg CMU_IDLE_MODULECLK_STIMER1
 *         @arg CMU_IDLE_MODULECLK_STIMER2
 *         @arg CMU_IDLE_MODULECLK_STIMER3
 *         @arg CMU_IDLE_MODULECLK_STIMER4
 *         @arg CMU_IDLE_MODULECLK_STIMER5
 *         @arg CMU_IDLE_MODULECLK_USBPHY
 *         @arg CMU_IDLE_MODULECLK_BKPTEST
 *         @arg CMU_IDLE_MODULECLK_SYSTICK
 *         @arg CMU_IDLE_MODULECLK_SYSTEST
 *         @arg CMU_IDLE_MODULECLK_PLLEXT
 * @param  Gate  ENABLE or DISABLE
 * @retval None
 */
void HAL_CMU_IdleModeModuleClockConfig(uint32_t MdlClkSel, uint32_t Gate)
{
    assert_param(IS_CMU_IDLEMODEMDLCLKCFG(MdlClkSel));
    assert_param(IS_FUNCTIONAL_STATE(Gate));
    if (ENABLE == Gate) {
        LL_CMU_IDLE_OpenModuleClk(Gate);
    } else {
        LL_CMU_IDLE_CloseModuleClk(Gate);
    }
}


/**
 * @brief  When MCU In Stop Mode Module Clock Config
 * @param  MdlClkSel This parameter can be a combination of the following values:
 *         @arg CMU_STOP_MODULECLK_SHIELD
 *         @arg CMU_STOP_MODULECLK_ADC
 *         @arg CMU_STOP_MODULECLK_TRNG
 *         @arg CMU_STOP_MODULECLK_CT
 *         @arg CMU_STOP_MODULECLK_UART
 *         @arg CMU_STOP_MODULECLK_USBFS
 *         @arg CMU_STOP_MODULECLK_COUT0
 *         @arg CMU_STOP_MODULECLK_COUT1
 *         @arg CMU_STOP_MODULECLK_COUT2
 *         @arg CMU_STOP_MODULECLK_STIMER0
 *         @arg CMU_STOP_MODULECLK_STIMER1
 *         @arg CMU_STOP_MODULECLK_STIMER2
 *         @arg CMU_STOP_MODULECLK_STIMER3
 *         @arg CMU_STOP_MODULECLK_STIMER4
 *         @arg CMU_STOP_MODULECLK_STIMER5
 *         @arg CMU_STOP_MODULECLK_USBPHY
 *         @arg CMU_STOP_MODULECLK_BKPTEST
 *         @arg CMU_STOP_MODULECLK_SYSTICK
 *         @arg CMU_STOP_MODULECLK_SYSTEST
 *         @arg CMU_STOP_MODULECLK_PLLEXT
 * @param  Gate  ENABLE or DISABLE
 * @retval None
 */
void HAL_CMU_StopModeModuleClockConfig(uint32_t MdlClkSel, uint32_t Gate)
{
    assert_param(IS_CMU_STOPMODEMDLCLKCFG(MdlClkSel));
    assert_param(IS_FUNCTIONAL_STATE(Gate));
    if (ENABLE == Gate) {
        LL_CMU_STOP_OpenModuleClk(Gate);
    } else {
        LL_CMU_STOP_CloseModuleClk(Gate);
    }
}


/**
 * @brief  Config PLL parameters
 * @param  source clock
 * @param  goal clock
 * @param  pre point
 * @param  fb point
 * @retval HAL_Status
 * @note   PLL CLK Range(MHz)
            90 ,92 ,93 ,94 ,96 ,99 ,102 ,105 ,108 ,111 ,114 ,117 ,120 ,123 ,126 ,129 ,
            132 ,135 ,138 ,141 ,144 ,150 ,156 ,162 ,168 ,174 ,180
 */
static HAL_StatusTypeDef __CMU_SetPLL(uint32_t src_clk, uint32_t goal_clk, uint8_t *o_pre, uint8_t *o_fb)
{
    uint8_t pre = 0, fb = 0, div = 0;

    for (pre = 0; pre < 8; pre++) {
        div = goal_clk * 2 * (1 + pre) / src_clk;
        if (div >= 16 && div < 48) {
            if (goal_clk * 2 * (1 + pre) == (src_clk * div)) {
                fb = div - 16;
                //PLL clk=fosc*(16+fb)/(1+pre)/2; pre:0~7 fb: 0~31
                *o_pre  = pre;
                *o_fb   = fb;
                return (HAL_OK);
            }
        }
    }
    return (HAL_ERROR);
}


/**
 * @brief  Initializes the CPU, SYS and BUS clocks according to the specified
 *         parameters in the CMU_ClkInitStruct.
 * @param  CMU_ClkInitStruct pointer to an CMU_InitTypeDef structure that
 *         contains the configuration information for the CMU peripheral.
 * @retval None
 */
static HAL_StatusTypeDef __CMU_ClockConfig(CMU_InitTypeDef *CMU_InitStruct)
{
    uint8_t         osc_en = 0, pll_en = 0;
    uint32_t        src_hz = IRC16M_VALUE, pll_hz = 0, main_hz, real_clk = 0;
    uint8_t         pre = 0, fb = 0;
    uint8_t         falsh_latency = 0;
    uint8_t         div_v;
    __IO uint32_t   i;

    if (CMU_InitStruct->OscType & LL_CMU_OSCTYPE_XTAL) { //osc
        LL_CMU_IDLE_EnableOSC();
        while (!LL_CMU_IsReady_OSC())
            ;
        osc_en      = 1;
        OscClock    = CMU_InitStruct->XtalHz;
    }
    LL_CMU_SetIRC16MReadyCnt(200);
    if (LL_CMU_PLL_ON == CMU_InitStruct->PLL.PLLState) { //pll
        pll_hz = CMU_InitStruct->PLL.PLLFreqHz;
        if ((LL_CMU_PLLCLK_XTAL == CMU_InitStruct->PLL.PLLSource) && osc_en) {
            src_hz = CMU_InitStruct->XtalHz;
            LL_CMU_SetPLLClkSrc(LL_CMU_PLLCLK_XTAL);
        } else if (LL_CMU_PLLCLK_IRC16M == CMU_InitStruct->PLL.PLLSource)
            LL_CMU_SetPLLClkSrc(LL_CMU_PLLCLK_IRC16M);
        else
            return (HAL_ERROR);
        if (pll_hz >= src_hz) {
            LL_CMU_SetPLLReadyCnt(6900);  //waiting 1.8ms
            if (HAL_OK == __CMU_SetPLL(src_hz, pll_hz, &pre, &fb)) {
                LL_CMU_ConfigPLL(pre, fb);
                MODIFY_REG(CMU->CFG_CLKS1, CMU_CFGCLKS1_PLLLOCKCFG, 1 << CMU_CFGCLKS1_PLLLOCKCFG_Pos);
                MODIFY_REG(CMU->CFG_CLKS1, CMU_CFGCLKS1_PLLTRIM, 1 << CMU_CFGCLKS1_PLLTRIM_Pos);
                MODIFY_REG(CMU->CFG_CLKS1, CMU_CFGCLKS1_PLLCPICTRL, 0x7 << CMU_CFGCLKS1_PLLCPICTRL_Pos);

                LL_CMU_IDLE_EnablePLL();
                LL_CMU_EnablePLL();
                pll_en = 1;
            }
        }
    }
    switch (CMU_InitStruct->MainClkSrc) {
    case LL_CMU_MCGMAINCLK_IRC4M:
        main_hz = IRC4M_VALUE;
        break;
    case LL_CMU_MCGMAINCLK_IRC16M:
        main_hz = IRC16M_VALUE;
        break;
    case LL_CMU_MCGMAINCLK_PLLCLK:
        if (!pll_en)
            return (HAL_ERROR);
        main_hz = pll_hz;
        break;
    case LL_CMU_MCGMAINCLK_OSCCLK:
        if (!osc_en)
            return (HAL_ERROR);
        main_hz = src_hz;
        break;
    default:
        return (HAL_ERROR);
    }
    if (CMU_InitStruct->MainClkDiv > LL_CMU_MAINCLK_DIV128)
        return (HAL_ERROR);

    SystemCoreClock = main_hz / SysClkCfg[CMU_InitStruct->MainClkDiv].Div;

    div_v = SysClkCfg[CMU_InitStruct->MainClkDiv].Cfg;

    LL_CMU_SetBusClkPrescaler(CMU_InitStruct->BusClkDiv);

    if (main_hz > 27000000) {
        //falsh_latency config
        real_clk        = main_hz / 1000000;
        falsh_latency   = (real_clk + 26) / 27;
        MODIFY_REG(FLASH->RDT, FLASH_RDT_TACCN, falsh_latency << FLASH_RDT_TACCN_Pos);

        if (real_clk >= 100)
            MODIFY_REG(FLASH->RDT, FLASH_RDT_TCKH, 1 << FLASH_RDT_TCKH_Pos);
        else
            MODIFY_REG(FLASH->RDT, FLASH_RDT_TCKH, 0 << FLASH_RDT_TCKH_Pos);
    }

    if (pll_en) {
        if (SystemCoreClock > 48000000) {                       //sysclk>48MHz open iload
            LL_PMU_EnableiLoad();
            LL_CMU_SetSysClkPrescaler(LL_CMU_REUSEDIV3_DIV2);   //must set
        }
        //waiting pll ready
        while (!LL_CMU_IsLock_PLL())
            ;
        while (!LL_CMU_IsReady_PLL())
            ;
    }
    LL_CMU_SetMCGMainClkPrescaler((div_v & 0x70) >> 4);
    LL_CMU_SetMCGMainClkSrc(CMU_InitStruct->MainClkSrc);
#if (CACHE_ENABLE != 0)
    __HAL_CACHE_ENABLE();
#endif /* DATA and CODE CACHE_ENABLE */
    if (SystemCoreClock > 96000000) { //sysclk>96MHz add delay
        for (i = 0; i < 10; i++)
            ;            //software delay
    }
    LL_CMU_SetSysClkPrescaler(div_v & 0x0F);
    return (HAL_OK);
}


/**
 * @}
 */


/**
 * @}
 */

#endif /* CMU_MODULE_ENABLED */


/**
 * @}
 */


/**
 * @}
 */

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
