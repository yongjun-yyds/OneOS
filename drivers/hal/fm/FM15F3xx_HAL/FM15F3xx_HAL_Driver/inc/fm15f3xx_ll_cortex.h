/**
 ******************************************************************************
 * @file    fm15f3xx_cortex.h
 * @author
 * @version V1.0.0
 * @date    2020-3-17
 * @brief   Header file of CORTEX LL module.
   @verbatim
   ==============================================================================
 ##### How to use this driver #####
   ==============================================================================
    [..]
    The LL CORTEX driver contains a set of generic APIs that can be
    used by user:
      (+) SYSTICK configuration used by @ref LL_mDelay and @ref LL_Init1msTick
          functions
      (+) Low power mode configuration (SCB register of Cortex-MCU)
      (+) API to access to MCU info (CPUID register)
      (+) API to enable fault handler (SHCSR accesses)

   @endverbatim
 **/

/*- Define to prevent recursive inclusion -------------------------------------*/
#ifndef __FM15F3XX_LL_CORTEX_H
#define __FM15F3XX_LL_CORTEX_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "fm15f3xx.h"


/** @addtogroup FM15F3XX_LL_Driver
 * @{
 */


/** @defgroup CORTEX_LL CORTEX
 * @{
 */

/* Private types -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

/* Private constants ---------------------------------------------------------*/

/* Private macros ------------------------------------------------------------*/

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/


/** @defgroup CORTEX_LL_Exported_Constants CORTEX Exported Constants
 * @{
 */


/** @defgroup CORTEX_LL_EC_CLKSOURCE_HCLK SYSTICK Clock Source
 * @{
 */
#define LL_SYSTICK_CLKSOURCE_HCLK_DIV8  0x00000000U                     /*!< AHB clock divided by 8 selected as SysTick clock source.*/
#define LL_SYSTICK_CLKSOURCE_HCLK       SysTick_CTRL_CLKSOURCE_Msk      /*!< AHB clock selected as SysTick clock source. */


/**
 * @}
 */


/** @defgroup CORTEX_LL_EC_FAULT Handler Fault type
 * @{
 */
#define LL_HANDLER_FAULT_USG    SCB_SHCSR_USGFAULTENA_Msk               /*!< Usage fault */
#define LL_HANDLER_FAULT_BUS    SCB_SHCSR_BUSFAULTENA_Msk               /*!< Bus fault */
#define LL_HANDLER_FAULT_MEM    SCB_SHCSR_MEMFAULTENA_Msk               /*!< Memory management fault */


/**
 * @}
 */


/**
 * @}
 */

/* Exported macro ------------------------------------------------------------*/
#define NVIC_PRIORITYGROUP_0    ( (uint32_t) 0x00000007)            /*!< 0 bits for pre-emption priority
                                                                       4 bits for subpriority */
#define NVIC_PRIORITYGROUP_1    ( (uint32_t) 0x00000006)            /*!< 1 bits for pre-emption priority
                                                                       3 bits for subpriority */
#define NVIC_PRIORITYGROUP_2    ( (uint32_t) 0x00000005)            /*!< 2 bits for pre-emption priority
                                                                       2 bits for subpriority */
#define NVIC_PRIORITYGROUP_3    ( (uint32_t) 0x00000004)            /*!< 3 bits for pre-emption priority
                                                                       1 bits for subpriority */
#define NVIC_PRIORITYGROUP_4    ( (uint32_t) 0x00000003)            /*!< 4 bits for pre-emption priority
                                                                       0 bits for subpriority */
/* Exported functions --------------------------------------------------------*/


/** @defgroup CORTEX_LL_Exported_Functions CORTEX Exported Functions
 * @{
 */


/** @defgroup CORTEX_LL_EF_SYSTICK SYSTICK
 * @{
 */


/**
 * @brief  This function checks if the Systick counter flag is active or not.
 * @note   It can be used in timeout function on application side.
 * @rmtoll STK_CTRL     COUNTFLAG     LL_SYSTICK_IsActiveCounterFlag
 * @retval State of bit (1 or 0).
 */
__STATIC_INLINE uint32_t LL_SYSTICK_IsActiveCounterFlag(void)
{
    return ((SysTick->CTRL & SysTick_CTRL_COUNTFLAG_Msk) == (SysTick_CTRL_COUNTFLAG_Msk));
}


/**
 * @brief  Configures the SysTick clock source
 * @rmtoll STK_CTRL     CLKSOURCE     LL_SYSTICK_SetClkSource
 * @param  Source This parameter can be one of the following values:
 *         @arg @ref LL_SYSTICK_CLKSOURCE_HCLK_DIV8
 *         @arg @ref LL_SYSTICK_CLKSOURCE_HCLK
 * @retval None
 */
__STATIC_INLINE void LL_SYSTICK_SetClkSource(uint32_t Source)
{
    if (Source == LL_SYSTICK_CLKSOURCE_HCLK) {
        SET_BIT(SysTick->CTRL, LL_SYSTICK_CLKSOURCE_HCLK);
    } else {
        CLEAR_BIT(SysTick->CTRL, LL_SYSTICK_CLKSOURCE_HCLK);
    }
}


/**
 * @brief  Get the SysTick clock source
 * @rmtoll STK_CTRL     CLKSOURCE     LL_SYSTICK_GetClkSource
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_SYSTICK_CLKSOURCE_HCLK_DIV8
 *         @arg @ref LL_SYSTICK_CLKSOURCE_HCLK
 */
__STATIC_INLINE uint32_t LL_SYSTICK_GetClkSource(void)
{
    return (READ_BIT(SysTick->CTRL, LL_SYSTICK_CLKSOURCE_HCLK));
}


/**
 * @brief  Enable SysTick exception request
 * @rmtoll STK_CTRL     TICKINT       LL_SYSTICK_EnableIT
 * @retval None
 */
__STATIC_INLINE void LL_SYSTICK_EnableIT(void)
{
    SET_BIT(SysTick->CTRL, SysTick_CTRL_TICKINT_Msk);
}


/**
 * @brief  Disable SysTick exception request
 * @rmtoll STK_CTRL     TICKINT       LL_SYSTICK_DisableIT
 * @retval None
 */
__STATIC_INLINE void LL_SYSTICK_DisableIT(void)
{
    CLEAR_BIT(SysTick->CTRL, SysTick_CTRL_TICKINT_Msk);
}


/**
 * @brief  Checks if the SYSTICK interrupt is enabled or disabled.
 * @rmtoll STK_CTRL     TICKINT       LL_SYSTICK_IsEnabledIT
 * @retval State of bit (1 or 0).
 */
__STATIC_INLINE uint32_t LL_SYSTICK_IsEnabledIT(void)
{
    return (READ_BIT(SysTick->CTRL, SysTick_CTRL_TICKINT_Msk) == (SysTick_CTRL_TICKINT_Msk));
}


/**
 * @}
 */


/** @defgroup CORTEX_LL_EF_LOW_POWER_MODE LOW POWER MODE
 * @{
 */


/**
 * @brief  Processor uses sleep as its low power mode
 * @rmtoll SCB_SCR      SLEEPDEEP     LL_LPM_EnableSleep
 * @retval None
 */
__STATIC_INLINE void LL_LPM_EnableSleep(void)
{
    /* Clear SLEEPDEEP bit of Cortex System Control Register */
    CLEAR_BIT(SCB->SCR, ((uint32_t) SCB_SCR_SLEEPDEEP_Msk));
}


/**
 * @brief  Processor uses deep sleep as its low power mode
 * @rmtoll SCB_SCR      SLEEPDEEP     LL_LPM_EnableDeepSleep
 * @retval None
 */
__STATIC_INLINE void LL_LPM_EnableDeepSleep(void)
{
    /* Set SLEEPDEEP bit of Cortex System Control Register */
    SET_BIT(SCB->SCR, ((uint32_t) SCB_SCR_SLEEPDEEP_Msk));
}


/**
 * @brief  Configures sleep-on-exit when returning from Handler mode to Thread mode.
 * @note   Setting this bit to 1 enables an interrupt-driven application to avoid returning to an
 *         empty main application.
 * @rmtoll SCB_SCR      SLEEPONEXIT   LL_LPM_EnableSleepOnExit
 * @retval None
 */
__STATIC_INLINE void LL_LPM_EnableSleepOnExit(void)
{
    /* Set SLEEPONEXIT bit of Cortex System Control Register */
    SET_BIT(SCB->SCR, ((uint32_t) SCB_SCR_SLEEPONEXIT_Msk));
}


/**
 * @brief  Do not sleep when returning to Thread mode.
 * @rmtoll SCB_SCR      SLEEPONEXIT   LL_LPM_DisableSleepOnExit
 * @retval None
 */
__STATIC_INLINE void LL_LPM_DisableSleepOnExit(void)
{
    /* Clear SLEEPONEXIT bit of Cortex System Control Register */
    CLEAR_BIT(SCB->SCR, ((uint32_t) SCB_SCR_SLEEPONEXIT_Msk));
}


/**
 * @brief  Enabled events and all interrupts, including disabled interrupts, can wakeup the
 *         processor.
 * @rmtoll SCB_SCR      SEVEONPEND    LL_LPM_EnableEventOnPend
 * @retval None
 */
__STATIC_INLINE void LL_LPM_EnableEventOnPend(void)
{
    /* Set SEVEONPEND bit of Cortex System Control Register */
    SET_BIT(SCB->SCR, ((uint32_t) SCB_SCR_SEVONPEND_Msk));
}


/**
 * @brief  Only enabled interrupts or events can wakeup the processor, disabled interrupts are
 *         excluded
 * @rmtoll SCB_SCR      SEVEONPEND    LL_LPM_DisableEventOnPend
 * @retval None
 */
__STATIC_INLINE void LL_LPM_DisableEventOnPend(void)
{
    /* Clear SEVEONPEND bit of Cortex System Control Register */
    CLEAR_BIT(SCB->SCR, ((uint32_t) SCB_SCR_SEVONPEND_Msk));
}


/**
 * @}
 */


/** @defgroup CORTEX_LL_EF_HANDLER HANDLER
 * @{
 */


/**
 * @brief  Enable a fault in System handler control register (SHCSR)
 * @rmtoll SCB_SHCSR    MEMFAULTENA   LL_HANDLER_EnableFault
 * @param  Fault This parameter can be a combination of the following values:
 *         @arg @ref LL_HANDLER_FAULT_USG
 *         @arg @ref LL_HANDLER_FAULT_BUS
 *         @arg @ref LL_HANDLER_FAULT_MEM
 * @retval None
 */
__STATIC_INLINE void LL_HANDLER_EnableFault(uint32_t Fault)
{
    /* Enable the system handler fault */
    SET_BIT(SCB->SHCSR, Fault);
}


/**
 * @brief  Disable a fault in System handler control register (SHCSR)
 * @rmtoll SCB_SHCSR    MEMFAULTENA   LL_HANDLER_DisableFault
 * @param  Fault This parameter can be a combination of the following values:
 *         @arg @ref LL_HANDLER_FAULT_USG
 *         @arg @ref LL_HANDLER_FAULT_BUS
 *         @arg @ref LL_HANDLER_FAULT_MEM
 * @retval None
 */
__STATIC_INLINE void LL_HANDLER_DisableFault(uint32_t Fault)
{
    /* Disable the system handler fault */
    CLEAR_BIT(SCB->SHCSR, Fault);
}


/**
 * @}
 */


/** @defgroup CORTEX_LL_EF_MCU_INFO MCU INFO
 * @{
 */


/**
 * @brief  Get Implementer code
 * @rmtoll SCB_CPUID    IMPLEMENTER   LL_CPUID_GetImplementer
 * @retval Value should be equal to 0x41 for ARM
 */
__STATIC_INLINE uint32_t LL_CPUID_GetImplementer(void)
{
    return ((uint32_t)(READ_BIT(SCB->CPUID, SCB_CPUID_IMPLEMENTER_Msk) >> SCB_CPUID_IMPLEMENTER_Pos));
}


/**
 * @brief  Get Variant number (The r value in the rnpn product revision identifier)
 * @rmtoll SCB_CPUID    VARIANT       LL_CPUID_GetVariant
 * @retval Value between 0 and 255 (0x1: revision 1, 0x2: revision 2)
 */
__STATIC_INLINE uint32_t LL_CPUID_GetVariant(void)
{
    return ((uint32_t)(READ_BIT(SCB->CPUID, SCB_CPUID_VARIANT_Msk) >> SCB_CPUID_VARIANT_Pos));
}


/**
 * @brief  Get Constant number
 * @rmtoll SCB_CPUID    ARCHITECTURE  LL_CPUID_GetConstant
 * @retval Value should be equal to 0xF for Cortex-M3 devices
 */
__STATIC_INLINE uint32_t LL_CPUID_GetConstant(void)
{
    return ((uint32_t)(READ_BIT(SCB->CPUID, SCB_CPUID_ARCHITECTURE_Msk) >> SCB_CPUID_ARCHITECTURE_Pos));
}


/**
 * @brief  Get Part number
 * @rmtoll SCB_CPUID    PARTNO        LL_CPUID_GetParNo
 * @retval Value should be equal to 0xC23 for Cortex-M3
 */
__STATIC_INLINE uint32_t LL_CPUID_GetParNo(void)
{
    return ((uint32_t)(READ_BIT(SCB->CPUID, SCB_CPUID_PARTNO_Msk) >> SCB_CPUID_PARTNO_Pos));
}


/**
 * @brief  Get Revision number (The p value in the rnpn product revision identifier, indicates patch release)
 * @rmtoll SCB_CPUID    REVISION      LL_CPUID_GetRevision
 * @retval Value between 0 and 255 (0x0: patch 0, 0x1: patch 1)
 */
__STATIC_INLINE uint32_t LL_CPUID_GetRevision(void)
{
    return ((uint32_t)(READ_BIT(SCB->CPUID, SCB_CPUID_REVISION_Msk) >> SCB_CPUID_REVISION_Pos));
}


/**
 * @}
 */


/**
 * @}
 */


/**
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif /* __FM15F3XX_LL_CORTEX_H */

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/

