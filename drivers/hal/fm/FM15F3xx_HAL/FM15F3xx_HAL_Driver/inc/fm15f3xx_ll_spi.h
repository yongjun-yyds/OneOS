/**
 ******************************************************************************
 * @file    fm15f3xx_ll_spi.h
 * @author  SRG
 * @version V1.0.0
 * @date    2020-03-17
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */

#ifndef FM15F3XX_LL_SPI_H
#define FM15F3XX_LL_SPI_H
#ifdef __cplusplus
extern "C" {
#endif

#include "fm15f3xx.h"


/** @addtogroup FM15F366_LL_Driver
 * @{
 */

#if defined (SPI0) || defined (SPI1) || defined (SPI2) || defined (SPI3)


/** @defgroup SPI_LL SPI
 * @{
 */

/* Private types -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private constants ---------------------------------------------------------*/
/* Private macros ------------------------------------------------------------*/


/** @defgroup SPI_LL_EM_WRITE_READ Common Write and read registers Macros
 * @{
 */


/**
 * @brief  Write a value in SPI register
 * @param  __INSTANCE__ SPI Instance
 * @param  __REG__ Register to be written
 * @param  __VALUE__ Value to be written in the register
 * @retval None
 */
#define LL_SPI_WriteReg( __INSTANCE__, __REG__, __VALUE__ ) WRITE_REG( __INSTANCE__->__REG__, (__VALUE__) )


/**
 * @brief  Read a value in SPI register
 * @param  __INSTANCE__ SPI Instance
 * @param  __REG__ Register to be read
 * @retval Register value
 */
#define LL_SPI_ReadReg( __INSTANCE__, __REG__ ) READ_REG( __INSTANCE__->__REG__ )


/**
 * @}
 */

/* ExSPIed functions --------------------------------------------------------*/


/** @defgroup SPI_LL_ExSPIed_Functions SPI ExSPIed Functions
 * @{
 */


/** @defgroup GPIO_LL_EF_GPIO_Configuration GPIO Configuration
 * @{
 */


/**
 * @brief  SPI Init structures definition
 */
typedef struct {
//////SPCR1_reg
    uint32_t ClockPhase;                /* Specifies the clock active edge for the bit capture.
                                           This parameter can be a value of @ref SPI_LL_CPHA.

                                           This feature can be modified afterwards using unitary function @ref LL_SPI_SetClockPhase().*/

    uint32_t ClockPolarity;             /* Specifies the serial clock steady state.
                                           This parameter can be a value of @ref SPI_LL_CPOL.

                                           This feature can be modified afterwards using unitary function @ref LL_SPI_SetClockPolarity().*/

    uint32_t Mode;                      /* MSTR:Specifies the SPI mode (Master/Slave).
                                           This parameter can be a value of @ref SPI_LL_MODE.

                                           This feature can be modified afterwards using unitary function @ref LL_SPI_SetMode().*/

    uint32_t BitOrder;                  /* LSBFIRST:Specifies the SPI transfer format (LsbFirst/MsbFirst).
                                           This parameter can be a value of @ref SPI_LL_BIT_Order.

                                           This feature can be modified afterwards using unitary function @ref LL_SPI_SetTransferBitOrder().*/

    uint32_t SSNMode;                   /* Specifies the SPI SSN signal when completed 8bit transfered.
                                           This parameter can be a value of @ref SPI_LL_SSN_MODE.

                                           This feature can be modified afterwards using unitary function @ref LL_SPI_SetSSNMode().*/

    uint32_t BaudRate;                  /* Specifies the BaudRate prescaler value which will be used to configure the transmit and receive SCK clock.
                                           This parameter can be a value of @ref SPI_LL_BAUDRATEPRESCALER.
                                           @note The communication clock is derived from the master clock. The slave clock does not need to be set.

                                           This feature can be modified afterwards using unitary function @ref LL_SPI_SetBaudRatePrescaler().*/

    uint32_t T_BYTE;                    /* Specifies the MCU write to TXFIFO byte number .
                                           This parameter can be a value of @ref SPI_LL_TBYTE.
                                           @note The communication clock is derived from the master clock. The slave clock does not need to be set.

                                           This feature can be modified afterwards using unitary function @ref LL_SPI_SetBaudRatePrescaler().*/

    uint32_t WAIT_CNT;                  /* Specifies the SSN will pulluped up 1+WAIT_CNT SCK cycle when SPI is work in MSTR mode and SSNMODE=1.
                                           This parameter can be a value of @ref SPI_LL_WAITCNT.
                                           @note The communication clock is derived from the master clock. The slave clock does not need to be set.

                                           This feature can be modified afterwards using unitary function @ref LL_SPI_SetBaudRatePrescaler().*/

    uint32_t SSN_MCU_EN;                /* MSTR mode,Specifies whether the SSN signal is managed by hardware (SSN pin) or by software using the SSN_MCU bit(SPCR2 reg.).
                                           This parameter can be a value of @ref SPI_LL_SSN_MCU_EN.

                                           This feature can be modified afterwards using unitary function @ref LL_SPI_SetNSSMode().*/

    uint32_t SPI_EN;                    /* Specifies SPI module enable or disable.
                                           This parameter can be a value of @ref SPI_LL_SPI_EN.

                                           This feature can be modified afterwards using unitary function @ref LL_SPI_SetNSSMode().*/

    uint32_t TXONLY;                    /* if TX_ONLY_EN bit =1,this bit Specifies SPI work in TX mode only(recieve nothing);
                                           this bit will clear automatically while transfer is completed.
                                           This parameter can be a value of @ref SPI_LL_TXONLY.

                                           This feature can be modified afterwards using unitary function @ref LL_SPI_SetNSSMode().*/

    uint32_t SamplePosition;            /* if MSTR bit =1,Specifies MISO signal sample position;
                                            This parameter can be a value of @ref SPI_LL_SamplePosition.

                                           This feature can be modified afterwards using unitary function @ref LL_SPI_SetNSSMode().*/

    uint32_t SlaveDataSendMode;         /* Specifies MISO signal send position;
                                           This parameter can be a value of @ref SPI_LL_SlaveDataSendMode.

                                           This feature can be modified afterwards using unitary function @ref LL_SPI_SetNSSMode().*/

    uint32_t SignalFilterSSN;           /* Specifies if use the SSN signal filter;
                                           This parameter can be a value of @ref SPI_LL_SignalFilterSSN.

                                           This feature can be modified afterwards using unitary function @ref LL_SPI_SetNSSMode().*/

    uint32_t SignalFilterSCK;           /* Specifies if use the SCK signal filter;
                                           This parameter can be a value of @ref SPI_LL_SignalFilterSCK.

                                           This feature can be modified afterwards using unitary function @ref LL_SPI_SetNSSMode().*/

    uint32_t SignalFilterMOSI;          /* Specifies if use the MOSI signal filter;
                                           This parameter can be a value of @ref SPI_LL_SignalFilterMOSI.

                                           This feature can be modified afterwards using unitary function @ref LL_SPI_SetNSSMode().*/
    uint32_t SSNODEN;                   /* Specifies ssn pin OD;
                                           This parameter can be a value of @ref SPI_LL_SsnOdEn.

                                           This feature can be modified afterwards using unitary function @ref LL_SPI_SetNSSMode().*/
/////SPCR2_reg
    uint32_t SSNMCU;                    /* MSTR mode,Specifies whether the SSN signal is H or L by software when SSN_MCU_EN = 1.
                                           This parameter can be a value of @ref SPI_SPCR2_SSNMCU.

                                           This feature can be modified afterwards using unitary function @ref LL_SPI_SetSSNMCU().*/

/////SPCR3_reg
    uint32_t TXONLY_EN;                 /* .
                                           This parameter can be a value of @ref SPI_SPCR3_TXONLYEN.

                                           This feature can be modified afterwards using unitary function @ref LL_SPI_SetTXONLYEN().*/
/////SPIIE_reg
    uint32_t RXNEIE;                    /* RXFIFO not empty interrupt enable.
                                           This parameter can be a value of @ref SPI_SPIIE_RXNEIE.

                                           This feature can be modified afterwards using unitary function @ref LL_SPI_SetRXNEIE().*/

    uint32_t TXEIE;                     /* TXFIFO empty interrupt enable.
                                           This parameter can be a value of @ref SPI_SPIIE_TXEIE.

                                           This feature can be modified afterwards using unitary function @ref LL_SPI_SetTXEIE().*/

    uint32_t RXFHALFIE;                 /* RX number >= 1/2 RXFIFO number interruput enable.
                                           This parameter can be a value of @ref SPI_SPIIE_RXFHALFIE.

                                           This feature can be modified afterwards using unitary function @ref LL_SPI_SetRXFHALFIE().*/

    uint32_t TXEHalfIE;                 /* TX data number less then 1/2 TXFIFO number interruput enable.
                                           This parameter can be a value of @ref SPI_SPIIE_TXEHalfIE.

                                           This feature can be modified afterwards using unitary function @ref LL_SPI_SetTXEHalfIE().*/

    uint32_t ErrorIE;                   /* error occur interruput enable.
                                           This parameter can be a value of @ref SPI_SPIIE_ERRIE.

                                           This feature can be modified afterwards using unitary function @ref LL_SPI_SetErrorIE().*/
} LL_SPI_InitParamTypeDef;


/**SPCR1****************************
 * @}
 */


/** @defgroup SPI_LL_CPHA Clock Phase
 * @{
 */
#define LL_SPI_CPHA_1EDGE   0x00000000U                                 /* First clock transition is the first data capture edge(default)  */
#define LL_SPI_CPHA_2EDGE   (SPI_SPCR1_CPHA)                            /* Second clock transition is the first data capture edge */


/**
 * @}
 */


/** @defgroup SPI_LL_CPOL Clock Polarity
 * @{
 */
#define LL_SPI_CPOL_LOW     0x00000000U                                 /* Clock to 0 when idle */
#define LL_SPI_CPOL_HIGH    (SPI_SPCR1_CPOL)                            /* Clock to 1 when idle */


/**
 * @}
 */


/** @defgroup SPI_LL_MODE Mastr/Slave Mode
 * @{
 */
#define LL_SPI_MODE_SLAVE   0x00000000U                                 /* Slave config   */
#define LL_SPI_MODE_MASTER  (SPI_SPCR1_MSTR)                            /* Master config  */


/**
 * @}
 */


/** @defgroup SPI_LL_BIT_Order transfer format (LsbFirst/MsbFirst)
 * @{
 */
#define LL_SPI_MSB_FIRST    0x00000000U                                 /* MSBFIRST config   */
#define LL_SPI_LSB_FIRST    (SPI_SPCR1_LSBFIRST)                        /* LSBFIRST config  */


/**
 * @}
 */


/** @defgroup SPI_LL_SSN_MODE SSN_Mode
 * @{
 */
#define LL_SPI_SSNMODE_LOW  0x00000000U                                 /* after transfer 8bit SSN keep to low if Txffio is not empty  */
#define LL_SPI_SSNMODE_HIGH (SPI_SPCR1_SSNMODE)                         /* at master mode,after transfer 8bit SSN will pulled up 1+WAIT_CNT SCK Cycle  */


/**
 * @}
 */


/** @defgroup SPI_LL_BAUDRATEPRESCALER Baud Rate Prescaler
 * @{
 */
#define LL_SPI_BAUDRATE_DIV2    0x00000000U                             /* BaudRate = fPCLK/2   */
#define LL_SPI_BAUDRATE_DIV4    (0x1U << SPI_SPCR1_BR_Pos)              /* BaudRate = fPCLK/4   */
#define LL_SPI_BAUDRATE_DIV8    (0x2U << SPI_SPCR1_BR_Pos)              /* BaudRate = fPCLK/8   */
#define LL_SPI_BAUDRATE_DIV16   (0x3U << SPI_SPCR1_BR_Pos)              /* BaudRate = fPCLK/16  */
#define LL_SPI_BAUDRATE_DIV32   (0x4U << SPI_SPCR1_BR_Pos)              /* BaudRate = fPCLK/32  */
#define LL_SPI_BAUDRATE_DIV64   (0x5U << SPI_SPCR1_BR_Pos)              /* BaudRate = fPCLK/64  */
#define LL_SPI_BAUDRATE_DIV128  (0x6U << SPI_SPCR1_BR_Pos)              /* BaudRate = fPCLK/128 */
#define LL_SPI_BAUDRATE_DIV256  (0x7U << SPI_SPCR1_BR_Pos)              /* BaudRate = fPCLK/256 */


/**
 * @}
 */


/** @defgroup SPI_LL_TBYTE write to txfifo number
 * @{
 */
#define LL_SPI_TBYTE_1BYTE  0x00000000U                                 /* 1byte   */
#define LL_SPI_TBYTE_3BYTE  (0x2U << SPI_SPCR1_TBYTE_Pos)               /* 3byte   */
#define LL_SPI_TBYTE_2BYTE  (0x1U << SPI_SPCR1_TBYTE_Pos)               /* 2byte   */
#define LL_SPI_TBYTE_4BYTE  (0x3U << SPI_SPCR1_TBYTE_Pos)               /* 4byte   */


/**
 * @}
 */


/** @defgroup SPI_LL_WAITCNT wait cnt
 * @{
 */
#define LL_SPI_WAITCNT_0CYCLE   0x00000000U                             /* SSN total High level = 1+0 SCK cycle   */
#define LL_SPI_WAITCNT_1CYCLE   (0x1U << SPI_SPCR1_WAITCNT_Pos)         /* SSN total High level = 1+1 SCK cycle   */
#define LL_SPI_WAITCNT_2CYCLE   (0x2U << SPI_SPCR1_WAITCNT_Pos)         /* SSN total High level = 1+2 SCK cycle   */
#define LL_SPI_WAITCNT_3CYCLE   (0x3U << SPI_SPCR1_WAITCNT_Pos)         /* SSN total High level = 1+3 SCK cycle   */


/**
 * @}
 */


/** @defgroup SPI_LL_SSN_MCU_EN SSN drived by hardware or MCU
 * @{
 */
#define LL_SPI_SSNMCUEN_HARDWARE    0x00000000U                         /* SSN modified by harware  */
#define LL_SPI_SSNMCUEN_MCU         (SPI_SPCR1_SSNMCUEN)                /* SSN modified by SSN_MCU bit(SPCR2)  */


/**
 * @}
 */


/** @defgroup SPI_LL_SPI_EN Enable or Disable SPI
 * @{
 */
#define LL_SPI_SPIEN_DISABLE    0x00000000U                             /* disable SPI module  */
#define LL_SPI_SPIEN_ENABLE     (SPI_SPCR1_SPIEN)                       /* enable SPI module  */


/**
 * @}
 */


/** @defgroup SPI_LL_TXONLY set TXONLY
 * @{
 */
#define LL_SPI_TXONLY_DISABLE   0x00000000U                             /* disable SPI TXONLY mode  */
#define LL_SPI_TXONLY_ENABLE    (SPI_SPCR1_TXONLY)                      /* enable SPI TXONLY mode  */


/**
 * @}
 */


/** @defgroup SPI_LL_SamplePosition modify sample position
 * @{
 */
#define LL_SPI_SAMPLEPOSITION_NORMAL            0x00000000U             /* normal sample  */
#define LL_SPI_SAMPLEPOSITION_DELAYHALFCYCLE    (SPI_SPCR1_SAMPLEP)     /* sample position delay 1/2cycle  */


/**
 * @}
 */


/** @defgroup SPI_LL_SlaveDataSendMode modify Slave data send position
 * @{
 */
#define LL_SPI_SLAVEDATASENDMODE_NORMAL         0x00000000U             /* normal send  */
#define LL_SPI_SLAVEDATASENDMODE_AHEADHALFCYCLE (SPI_SPCR1_SDOUTSEND)   /* slave data sended ahead of SCK 1/2cycle  */


/**
 * @}
 */


/** @defgroup SPI_LL_SignalFilterSSN signal SSN filter
 * @{
 */
#define LL_SPI_SIGNALFILTERSSN_DISABLE  0x00000000U                     /* disable SSN filter  */
#define LL_SPI_SIGNALFILTERSSN_ENABLE   (SPI_SPCR1_SSNFILT)             /* enable SSN filter  */


/**
 * @}
 */


/** @defgroup SPI_LL_SignalFilterSCK signal SCK filter
 * @{
 */
#define LL_SPI_SIGNALFILTERSCK_DISABLE  0x00000000U                     /* disable SCK filter  */
#define LL_SPI_SIGNALFILTERSCK_ENABLE   (SPI_SPCR1_SCKFILT)             /* enable SCK filter  */


/**
 * @}
 */


/** @defgroup SPI_LL_SignalFilterMOSI signal MOSI filter
 * @{
 */
#define LL_SPI_SIGNALFILTERMOSI_DISABLE 0x00000000U                     /* disable MOSI filter  */
#define LL_SPI_SIGNALFILTERMOSI_ENABLE  (SPI_SPCR1_MOSIFILT)            /* enable MOSI filter  */


/**
 * @}
 */


/** @defgroup SPI_LL_SsnOdEn signal SSN OD
 * @{
 */
#define LL_SPI_SSNODEN_DISABLE  0x00000000U                             /* disable OD output  */
#define LL_SPI_SSNODEN_ENABLE   (SPI_SPCR1_SSNODEN)                     /* SSN OD output when master mode  */


/**
 * @}
 */


/**SPCR2****************************
 * @}
 */


/** @defgroup SPI_LL_SSNMCU SSN drived by  MCU
 * @{
 */
#define LL_SPI_SSNMCU_LOW   0x00000000U                             /* SSN output 0  */
#define LL_SPI_SSNMCU_HIGH  (SPI_SPCR2_SSNMCU)                      /* SSN output 1  */


/**
 * @}
 */


/**SPCR3****************************
 * @}
 */


/** @defgroup SPI_LL_TXONLYEN if enable TXONLYEN,TX_ONLY bit manage the TXONLY mode
 * @{
 */
#define LL_SPI_TXONLYEN_DISABLE 0x00000000U                         /* TXONLY bit no use   */
#define LL_SPI_TXONLYEN_ENABLE  (SPI_SPCR3_TXONLYEN)                /* TXONLY bit active  */


/**
 * @}
 */


/**SPCR4****************************
 * @}
 */


/** @defgroup SPI_LL_CLRTXFIFOWCOL bit write 1 for clear TXFIFO_WCOL
 * @{
 */
#define LL_SPI_CLRTXFIFOWCOL (SPI_SPCR4_CTXWCOL)                    /* clear TXFIFO_WCOL  */


/**
 * @}
 */


/** @defgroup SPI_LL_CLRRXFIFOWCOL bit write 1 for clear RXFIFO_WCOL
 * @{
 */
#define LL_SPI_CLRRXFIFOWCOL (SPI_SPCR4_CRXWCOL)                    /* clear RXFIFO_WCOL  */


/**
 * @}
 */


/** @defgroup SPI_LL_CLRSLAVEERROR bit write 1 for clear SLAVE_ERROR
 * @{
 */
#define LL_SPI_CLRSLAVEERROR (SPI_SPCR4_CSLAVEER)                   /* clear SLAVE_ERROR  */


/**
 * @}
 */


/** @defgroup SPI_LL_CLRMASTERERROR bit write 1 for clear MASTER_ERROR
 * @{
 */
#define LL_SPI_CLRMASTERERROR (SPI_SPCR4_CMASTERER)                 /* clear MASTER_ERROR  */


/**
 * @}
 */


/** @defgroup SPI_LL_CLRRXFIFO bit write 1 for clear all of RX FIFO
 * @{
 */
#define LL_SPI_CLRRXFIFO (SPI_SPCR4_CRXFIFO)                        /* clear RX_FIFO  */


/**
 * @}
 */


/** @defgroup SPI_LL_CLRTXFIFO bit write 1 for clear all of TX FIFO
 * @{
 */
#define LL_SPI_CLRTXFIFO (SPI_SPCR4_CTXFIFO)                        /* clear TX_FIFO  */


/**
 * @}
 */


/**SPIIE****************************
 * @}
 */


/** @defgroup SPI_LL_RXNEIE RXFIFO not empty interrupt
 * @{
 */
#define LL_SPI_RXNEIE_DISABLE   0x00000000U                                 /* disable RXFIFO not empty Interrupt  */
#define LL_SPI_RXNEIE_ENABLE    (SPI_SPIIE_RXNEIE)                          /* enable RXFIFO not empty Interrupt */


/**
 * @}
 */


/** @defgroup SPI_LL_TXEIE TXFIFO empty interrupt
 * @{
 */
#define LL_SPI_TXEIE_DISABLE    0x00000000U                                 /* disable TXFIFO empty Interrupt  */
#define LL_SPI_TXEIE_ENABLE     (SPI_SPIIE_TXEIE)                           /* enable TXFIFO empty Interrupt */


/**
 * @}
 */


/** @defgroup SPI_LL_RXFHALFIE RX number >= 1/2 RXFIFO number interruput
 * @{
 */
#define LL_SPI_RXFHALFIE_DISABLE    0x00000000U                             /* disable Interrupt  */
#define LL_SPI_RXFHALFIE_ENABLE     (SPI_SPIIE_RXHFIE)                      /* enable RX number >=1/2 FIFO number Interrupt */


/**
 * @}
 */


/** @defgroup SPI_LL_TXEHALFIE TX data number less then 1/2 TXFIFO number interruput
 * @{
 */
#define LL_SPI_TXEHALFIE_DISABLE    0x00000000U                             /* disable Interrupt  */
#define LL_SPI_TXEHALFIE_ENABLE     (SPI_SPIIE_TXHEIE)                      /* enable TX data number less then 1/2 TXFIFO number interruput */


/**
 * @}
 */


/** @defgroup SPI_LL_ERRORIE error occur interruput
 * @{
 */
#define LL_SPI_ERRORIE_DISABLE  0x00000000U                                 /* disable Interrupt  */
#define LL_SPI_ERRORIE_ENABLE   (SPI_SPIIE_ERRIE)                           /* enable error occur interruput */


/**
 * @}
 */


/**SPDF********read only********************
 * @}
 */


/** @defgroup RXFIFO_NE RXFIFO not empty flag
 * @{
 */
#define LL_SPI_RXFIFONE_EMPTY       0x00000000U                             /* RXFIFO empty   */
#define LL_SPI_RXFIFONE_NOTEMPTY    (SPI_SPDF_RXFIFONE)                     /* RXFIFO not empty */


/**
 * @}
 */


/** @defgroup RXFIFO_S Recieve Number
 * @{
 */
#define LL_SPI_RXFIFOS_0BYTE    0x00000000U                                 /* RXFIFO empty   */
#define LL_SPI_RXFIFOS_1BYTE    (0x1U << SPI_SPDF_RXFIFOS_Pos)              /* RXFIFO recieve 1byte */
#define LL_SPI_RXFIFOS_2BYTE    (0x2U << SPI_SPDF_RXFIFOS_Pos)              /* RXFIFO recieve 2byte */
#define LL_SPI_RXFIFOS_3BYTE    (0x3U << SPI_SPDF_RXFIFOS_Pos)              /* RXFIFO recieve 3byte */
#define LL_SPI_RXFIFOS_4BYTE    (0x4U << SPI_SPDF_RXFIFOS_Pos)              /* RXFIFO recieve 4byte */
#define LL_SPI_RXFIFOS_5BYTE    (0x5U << SPI_SPDF_RXFIFOS_Pos)              /* RXFIFO recieve 5byte */
#define LL_SPI_RXFIFOS_6BYTE    (0x6U << SPI_SPDF_RXFIFOS_Pos)              /* RXFIFO recieve 6byte */
#define LL_SPI_RXFIFOS_7BYTE    (0x7U << SPI_SPDF_RXFIFOS_Pos)              /* RXFIFO recieve 7byte */
#define LL_SPI_RXFIFOS_8BYTE    (0x8U << SPI_SPDF_RXFIFOS_Pos)              /* RXFIFO recieve 8byte (full) */


/**
 * @}
 */


/** @defgroup TXFIFO_E TXFIFO empty flag
 * @{
 */
#define LL_SPI_TXFIFOE_NOTEMPTY 0x00000000U                                 /* TXFIFO not empty   */
#define LL_SPI_TXFIFOE_EMPTY    (SPI_SPDF_TXFIFOE)                          /* TXFIFO empty(transfer complete) */


/**
 * @}
 */


/** @defgroup TXFIFO_S TXFIFO data Number
 * @{
 */
#define LL_SPI_TXFIFOS_0BYTE    0x00000000U                                 /* TXFIFO empty   */
#define LL_SPI_TXFIFOS_1BYTE    (0x1U << SPI_SPDF_TXFIFOS_Pos)              /* TXFIFO data 1byte */
#define LL_SPI_TXFIFOS_2BYTE    (0x2U << SPI_SPDF_TXFIFOS_Pos)              /* TXFIFO data 2byte */
#define LL_SPI_TXFIFOS_3BYTE    (0x3U << SPI_SPDF_TXFIFOS_Pos)              /* TXFIFO data 3byte */
#define LL_SPI_TXFIFOS_4BYTE    (0x4U << SPI_SPDF_TXFIFOS_Pos)              /* TXFIFO data 4byte */
#define LL_SPI_TXFIFOS_5BYTE    (0x5U << SPI_SPDF_TXFIFOS_Pos)              /* TXFIFO data 5byte */
#define LL_SPI_TXFIFOS_6BYTE    (0x6U << SPI_SPDF_TXFIFOS_Pos)              /* TXFIFO data 6byte */
#define LL_SPI_TXFIFOS_7BYTE    (0x7U << SPI_SPDF_TXFIFOS_Pos)              /* TXFIFO data 7byte */
#define LL_SPI_TXFIFOS_8BYTE    (0x8U << SPI_SPDF_TXFIFOS_Pos)              /* TXFIFO data 8byte (full) */


/**
 * @}
 */


/**SPSR************************************
 * @}
 */


/** @defgroup SPI_LL_STATE select
 * @{
 */
#define LL_SPI_FLAG_BUSY        SPI_SPSR_BUSY
#define LL_SPI_FLAG_TXWCOL      SPI_SPSR_TXWCOL
#define LL_SPI_FLAG_RXWCOL      SPI_SPSR_RXWCOL
#define LL_SPI_FLAG_SLAVEERROR  SPI_SPSR_SLAVEERROR
#define LL_SPI_FLAG_MASTERERROR SPI_SPSR_MASTERERROR


/** @defgroup BUSY: TXFIFO not empty or SPI in translate
 * @{
 */
#define LL_SPI_BUSY_NOT 0x00000000U                                         /* TXFIFO empty   */
#define LL_SPI_BUSY_YES (SPI_SPSR_BUSY)                                     /* TXFIFO not empty(or SPI in translete) */


/**
 * @}
 */


/** @defgroup TXFIFO_WCOL  MCU write TXFIFO when it is full
 * @{
 */
#define LL_SPI_TXFIFONOWCOL 0x00000000U                                     /* TXFIFO not wcol   */
#define LL_SPI_TXFIFOWCOL   (SPI_SPSR_TXWCOL)                               /* TXFIFO WCOL */


/**
 * @}
 */


/** @defgroup RXFIFO_WCOL  RX when RXFIFO is full
 * @{
 */
#define LL_SPI_RXFIFONOWCOL 0x00000000U                                     /* RXFIFO not wcol   */
#define LL_SPI_RXFIFOWCOL   (SPI_SPSR_RXWCOL)                               /* RXFIFO WCOL */


/**
 * @}
 */


/** @defgroup SLAVE_ERROR SSN change to 1 during transfer 1byte
 * @{
 */
#define LL_SPI_NOSLAVEERROR 0x00000000U                                     /*    */
#define LL_SPI_SLAVEERROR   (SPI_SPSR_SLAVEERROR)                           /* SLAVE_ERROR */


/**
 * @}
 */


/** @defgroup MASTER_ERROR SSN change to 1 during transfer 1byte
 * @{
 */
#define LL_SPI_NOMASTERERROR    0x00000000U                                 /*    */
#define LL_SPI_MASTERERROR      (SPI_SPSR_MASTERERROR)                      /* MASTER_ERROR */


/**
 * @}
 */

/* functions --------------------------------------------------------*/


/** @defgroup SPI_LL_Functions SPI Functions
 * @{
 */


/** @defgroup SPI_LL_Configuration Configuration
 * @{
 */


/**
 * @brief  Set clock phase
 * @note   This bit should not be changed when communication is on going.
 * @rmtoll SPCR1          CPHA          LL_SPI_SetClockPhase
 * @param  SPIx SPI Instance
 * @param  ClockPhase This parameter can be one of the following values:
 *         @arg @ref LL_SPI_CPHA_1EDGE
 *         @arg @ref LL_SPI_CPHA_2EDGE
 * @retval None
 */
__STATIC_INLINE void LL_SPI_SetClockPhase(SPI_TypeDef *SPIx, uint32_t ClockPhase)
{
    MODIFY_REG(SPIx->SPCR1, SPI_SPCR1_CPHA, ClockPhase);
}


/**
 * @brief  Get clock phase
 * @rmtoll SPCR1          CPHA          LL_SPI_GetClockPhase
 * @param  SPIx SPI Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_SPI_CPHA_1EDGE
 *         @arg @ref LL_SPI_CPHA_2EDGE
 */
__STATIC_INLINE uint32_t LL_SPI_GetClockPhase(SPI_TypeDef *SPIx)
{
    return ((uint32_t)(READ_BIT(SPIx->SPCR1, SPI_SPCR1_CPHA)));
}


/**
 * @brief  Set clock polarity
 * @note   This bit should not be changed when communication is ongoing.
 *         This bit is not used in SPI TI mode.
 * @rmtoll SPCR1          CPOL          LL_SPI_SetClockPolarity
 * @param  SPIx SPI Instance
 * @param  ClockPolarity This parameter can be one of the following values:
 *         @arg @ref LL_SPI_CPOL_LOW
 *         @arg @ref LL_SPI_CPOL_HIGH
 * @retval None
 */
__STATIC_INLINE void LL_SPI_SetClockPol(SPI_TypeDef *SPIx, uint32_t ClockPolarity)
{
    MODIFY_REG(SPIx->SPCR1, SPI_SPCR1_CPOL, ClockPolarity);
}


/**
 * @brief  Get clock polarity
 * @rmtoll SPCR1          CPOL          LL_SPI_GetClockPolarity
 * @param  SPIx SPI Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_SPI_CPOL_LOW
 *         @arg @ref LL_SPI_CPOL_HIGH
 */
__STATIC_INLINE uint32_t LL_SPI_GetClockPolarity(SPI_TypeDef *SPIx)
{
    return ((uint32_t)(READ_BIT(SPIx->SPCR1, SPI_SPCR1_CPOL)));
}


/**
 * @brief  Set SPI operation mode to Master or Slave
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll SPCR1          MSTR          LL_SPI_SetMode\n
 *         CR1          SSI           LL_SPI_SetMode
 * @param  SPIx SPI Instance
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_SPI_MODE_MASTER
 *         @arg @ref LL_SPI_MODE_SLAVE
 * @retval None
 */
__STATIC_INLINE void LL_SPI_SetMode(SPI_TypeDef *SPIx, uint32_t Mode)
{
    MODIFY_REG(SPIx->SPCR1, SPI_SPCR1_MSTR, Mode);
}


/**
 * @brief  Get SPI operation mode (Master or Slave)
 * @rmtoll SPCR1          MSTR          LL_SPI_IsMasterMode
 * @param  SPIx SPI Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_SPI_MODE_MASTER
 *         @arg @ref LL_SPI_MODE_SLAVE
 */
__STATIC_INLINE uint32_t LL_SPI_IsEnableMasterMode(SPI_TypeDef *SPIx)
{
    return ((uint32_t)(READ_BIT(SPIx->SPCR1, SPI_SPCR1_MSTR) == SPI_SPCR1_MSTR));
}


/**
 * @brief  Set transfer bit order
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll SPCR1          LSBFIRST      LL_SPI_SetTransferBitOrder
 * @param  SPIx SPI Instance
 * @param  BitOrder This parameter can be one of the following values:
 *         @arg @ref LL_SPI_LSB_FIRST
 *         @arg @ref LL_SPI_MSB_FIRST
 * @retval None
 */
__STATIC_INLINE void LL_SPI_SetTransferBitOrder(SPI_TypeDef *SPIx, uint32_t BitOrder)
{
    MODIFY_REG(SPIx->SPCR1, SPI_SPCR1_LSBFIRST, BitOrder);
}


/**
 * @brief  Get transfer bit order
 * @rmtoll SPCR1          LSBFIRST      LL_SPI_IsEnableLsbFirst
 * @param  SPIx SPI Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_SPI_LSB_FIRST
 *         @arg @ref LL_SPI_MSB_FIRST
 */
__STATIC_INLINE uint32_t LL_SPI_IsEnableLsbFirst(SPI_TypeDef *SPIx)
{
    return ((uint32_t)(READ_BIT(SPIx->SPCR1, SPI_SPCR1_LSBFIRST) == SPI_SPCR1_LSBFIRST));
}


/**
 * @brief  Set SSNMODE bit for SSN signal
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll SPCR1          SSNMode      LL_SPI_SetSSNMode
 * @param  SPIx SPI Instance
 * @param  SSNMode This parameter can be one of the following values:
 *         @arg @ref LL_SPI_SSNMODE_LOW        ; after transfer 8bit SSN keep to low if Txffio is not empty
 *         @arg @ref LL_SPI_SSNMODE_HIGH       ; at master mode,after transfer 8bit SSN will pulled up 1+WAIT_CNT SCK Cycle
 * @retval None
 */
__STATIC_INLINE void LL_SPI_SetSSNMode(SPI_TypeDef *SPIx, uint32_t SSNMode)
{
    MODIFY_REG(SPIx->SPCR1, SPI_SPCR1_SSNMODE, SSNMode);
}


/**
 * @brief  Get SSNMODE
 * @rmtoll SPCR1          SSNMode      LL_SPI_GetSSNMode
 * @param  SPIx SPI Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_SPI_SSNMODE_LOW        ; after transfer 8bit SSN keep to low if Txffio is not empty
 *         @arg @ref LL_SPI_SSNMODE_HIGH             ; at master mode,after transfer 8bit SSN will pulled up 1+WAIT_CNT SCK Cycle
 */
__STATIC_INLINE uint32_t LL_SPI_GetSSNMode(SPI_TypeDef *SPIx)
{
    return ((uint32_t) READ_BIT(SPIx->SPCR1, SPI_SPCR1_SSNMODE));
}


/**
 * @brief  Set baud rate prescaler
 * @note   These bits should not be changed when communication is ongoing. SPI BaudRate = fPCLK/Prescaler.
 * @rmtoll SPCR1          BR            LL_SPI_SetBaudRatePrescaler
 * @param  SPIx SPI Instance
 * @param  BaudRate This parameter can be one of the following values:
 *         @arg @ref LL_SPI_BaudRatePrescaler_DIV2
 *         @arg @ref LL_SPI_BaudRatePrescaler_DIV4
 *         @arg @ref LL_SPI_BaudRatePrescaler_DIV8
 *         @arg @ref LL_SPI_BaudRatePrescaler_DIV16
 *         @arg @ref LL_SPI_BaudRatePrescaler_DIV32
 *         @arg @ref LL_SPI_BaudRatePrescaler_DIV64
 *         @arg @ref LL_SPI_BaudRatePrescaler_DIV128
 *         @arg @ref LL_SPI_BaudRatePrescaler_DIV256
 * @retval None
 */
__STATIC_INLINE void LL_SPI_SetBaudRatePrescaler(SPI_TypeDef *SPIx, uint32_t BaudRate)
{
    MODIFY_REG(SPIx->SPCR1, SPI_SPCR1_BR, BaudRate);
}


/**
 * @brief  Get baud rate prescaler
 * @rmtoll SPCR1          BR            LL_SPI_GetBaudRatePrescaler
 * @param  SPIx SPI Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_SPI_BaudRatePrescaler_DIV2
 *         @arg @ref LL_SPI_BaudRatePrescaler_DIV4
 *         @arg @ref LL_SPI_BaudRatePrescaler_DIV8
 *         @arg @ref LL_SPI_BaudRatePrescaler_DIV16
 *         @arg @ref LL_SPI_BaudRatePrescaler_DIV32
 *         @arg @ref LL_SPI_BaudRatePrescaler_DIV64
 *         @arg @ref LL_SPI_BaudRatePrescaler_DIV128
 *         @arg @ref LL_SPI_BaudRatePrescaler_DIV256
 */
__STATIC_INLINE uint32_t LL_SPI_GetBaudRatePrescaler(SPI_TypeDef *SPIx)
{
    return ((uint32_t)(READ_BIT(SPIx->SPCR1, SPI_SPCR1_BR)));
}


/**
 * @brief  Set MCU write to TXFIFO byte number
 * @rmtoll SPCR1          T_BYTE           LL_SPI_SetTBYTE
 * @param  SPIx SPI Instance
 * @param  DataWidth This parameter can be one of the following values:
 *         @arg @ref LL_SPI_TBYTE_1BYTE
 *         @arg @ref LL_SPI_TBYTE_2BYTE
 *         @arg @ref LL_SPI_TBYTE_3BYTE
 *         @arg @ref LL_SPI_TBYTE_4BYTE
 * @retval None
 */
__STATIC_INLINE void LL_SPI_SetTBYTE(SPI_TypeDef *SPIx, uint32_t T_BYTE)
{
    MODIFY_REG(SPIx->SPCR1, SPI_SPCR1_TBYTE, T_BYTE);
}


/**
 * @brief  Get T_BYTE config
 * @rmtoll SPCR1          T_BYTE           LL_SPI_GetTBYTE
 * @param  SPIx SPI Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_SPI_TBYTE_1BYTE
 *         @arg @ref LL_SPI_TBYTE_2BYTE
 *         @arg @ref LL_SPI_TBYTE_3BYTE
 *         @arg @ref LL_SPI_TBYTE_4BYTE
 */
__STATIC_INLINE uint32_t LL_SPI_GetTBYTE(SPI_TypeDef *SPIx)
{
    return ((uint32_t)(READ_BIT(SPIx->SPCR1, SPI_SPCR1_TBYTE)));
}


/**
 * @brief  Set WAIT_CNT(SSN pullup SCK cycle)
 * @rmtoll SPCR1          WAIT_CNT           LL_SPI_SetWaitCnt
 * @param  SPIx SPI Instance
 * @param  DataWidth This parameter can be one of the following values:
 *         @arg @ref LL_SPI_WAITCNT_0CYCLE
 *         @arg @ref LL_SPI_WAITCNT_1CYCLE
 *         @arg @ref LL_SPI_WAITCNT_2CYCLE
 *         @arg @ref LL_SPI_WAITCNT_3CYCLE
 * @retval None
 */
__STATIC_INLINE void LL_SPI_SetWaitCnt(SPI_TypeDef *SPIx, uint32_t Wait_Cnt)
{
    MODIFY_REG(SPIx->SPCR1, SPI_SPCR1_WAITCNT, Wait_Cnt);
}


/**
 * @brief  Get WAIT_CNT config
 * @rmtoll SPCR1          WAIT_CNT           LL_SPI_GetWaitCnt
 * @param  SPIx SPI Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_SPI_WAITCNT_0CYCLE
 *         @arg @ref LL_SPI_WAITCNT_1CYCLE
 *         @arg @ref LL_SPI_WAITCNT_2CYCLE
 *         @arg @ref LL_SPI_WAITCNT_3CYCLE
 */
__STATIC_INLINE uint32_t LL_SPI_GetWaitCnt(SPI_TypeDef *SPIx)
{
    return ((uint32_t)(READ_BIT(SPIx->SPCR1, SPI_SPCR1_WAITCNT)));
}


/**
 * @brief  Set SSN_MCU_EN:Modify SSN managed mode Hardware/MCU
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll SPCR1          SSN_MCU_EN      LL_SPI_SetSSNMCUEN
 * @param  SPIx SPI Instance
 * @param  SSN_MCU_EN This parameter can be one of the following values:
 *         @arg @ref LL_SPI_SSNMCUEN_HARDWARE
 *         @arg @ref LL_SPI_SSNMCUEN_MCU
 * @retval None
 */
__STATIC_INLINE void LL_SPI_SetSSNMCUEn(SPI_TypeDef *SPIx, uint32_t SSN_MCU_EN)
{
    MODIFY_REG(SPIx->SPCR1, SPI_SPCR1_SSNMCUEN, SSN_MCU_EN);
}


/**
 * @brief  Get SSN_MCU_EN
 * @rmtoll SPCR1          SSN_MCU_EN      LL_SPI_IsEnableSSNMCU
 * @param  SPIx SPI Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_SPI_SSNMCUEN_HARDWARE
 *         @arg @ref LL_SPI_SSNMCUEN_MCU
 */
__STATIC_INLINE uint32_t LL_SPI_IsEnableSSNMCU(SPI_TypeDef *SPIx)
{
    return ((uint32_t)(READ_BIT(SPIx->SPCR1, SPI_SPCR1_SSNMCUEN) == SPI_SPCR1_SSNMCUEN));
}


/**
 * @brief  Set SPI_EN:Enable/Disable SPI Module
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll SPCR1                LL_SPI_Enable
 * @param  SPIx SPI Instance
 * @param  SPI_EN This parameter can be one of the following values:
 *         @arg @ref LL_SPI_SPIEN_DISABLE
 *         @arg @ref LL_SPI_SPIEN_ENABLE
 * @retval None
 */
__STATIC_INLINE void LL_SPI_Enable(SPI_TypeDef *SPIx)
{
    SET_BIT(SPIx->SPCR1, SPI_SPCR1_SPIEN);
}


/**
 * @brief  Set SPI_EN:Enable/Disable SPI Module
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll SPCR1                LL_SPI_Disable
 * @param  SPIx SPI Instance
 * @retval None
 */
__STATIC_INLINE void LL_SPI_Disable(SPI_TypeDef *SPIx)
{
    CLEAR_BIT(SPIx->SPCR1, SPI_SPCR1_SPIEN);
}


/**
 * @brief  Get SPI_EN
 * @rmtoll SPCR1               LL_SPI_IsEnable
 * @param  SPIx SPI Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_SPI_SPIEN_DISABLE
 *         @arg @ref LL_SPI_SPIEN_ENABLE
 */
__STATIC_INLINE uint32_t LL_SPI_IsEnable(SPI_TypeDef *SPIx)
{
    return ((uint32_t)(READ_BIT(SPIx->SPCR1, SPI_SPCR1_SPIEN) == SPI_SPCR1_SPIEN));
}


/**
 * @brief  Set TXONLY mode
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll SPCR1          TXONLY      LL_SPI_SetTXonlyEn
 * @param  SPIx SPI Instance
 * @param  SPI_EN This parameter can be one of the following values:
 *         @arg @ref LL_SPI_TXONLY_DISABLE
 *         @arg @ref LL_SPI_TXONLY_ENABLE
 * @retval None
 */
__STATIC_INLINE void LL_SPI_SetTxOnlyMode(SPI_TypeDef *SPIx, uint32_t TXONLY)
{
    MODIFY_REG(SPIx->SPCR1, SPI_SPCR1_TXONLY, TXONLY);
}


/**
 * @brief  Get TXONLY config
 * @rmtoll SPCR1          TXONLY      LL_SPI_GetTxOnlyMode
 * @param  SPIx SPI Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_SPI_TXONLY_DISABLE
 *         @arg @ref LL_SPI_TXONLY_ENABLE
 */
__STATIC_INLINE uint32_t LL_SPI_GetTxOnlyMode(SPI_TypeDef *SPIx)
{
    return ((uint32_t) READ_BIT(SPIx->SPCR1, SPI_SPCR1_TXONLY));
}


/**
 * @brief  Set MSTR SPI Sample MISO signal position
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll SPCR1          SamplePosition      LL_SPI_SetSamplePosition
 * @param  SPIx SPI Instance
 * @param  SPI_EN This parameter can be one of the following values:
 *         @arg @ref LL_SPI_SamplePosition_Normal
 *         @arg @ref LL_SPI_SamplePosition_DelayHalfCycle
 * @retval None
 */
__STATIC_INLINE void LL_SPI_SetSamplePosition(SPI_TypeDef *SPIx, uint32_t SamplePosition)
{
    MODIFY_REG(SPIx->SPCR1, SPI_SPCR1_SAMPLEP, SamplePosition);
}


/**
 * @brief  Get Sample Position
 * @rmtoll SPCR1          SamplePosition      LL_SPI_IsEnableSamplePositionDelay
 * @param  SPIx SPI Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_SPI_SamplePosition_Normal
 *         @arg @ref LL_SPI_SamplePosition_DelayHalfCycle
 */
__STATIC_INLINE uint32_t LL_SPI_IsEnableSamplePositionDelay(SPI_TypeDef *SPIx)
{
    return ((uint32_t)(READ_BIT(SPIx->SPCR1, SPI_SPCR1_SAMPLEP) == SPI_SPCR1_SAMPLEP));
}


/**
 * @brief  Set Slave Data Send Mode
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll SPCR1          SlaveDataSendMode      LL_SPI_SetSlaveDataSendMode
 * @param  SPIx SPI Instance
 * @param  SPI_EN This parameter can be one of the following values:
 *         @arg @ref LL_SPI_SlaveDataSendMode_Normal
 *         @arg @ref LL_SPI_SlaveDataSendMode_AheadHalfCycle
 * @retval None
 */
__STATIC_INLINE void LL_SPI_SetSlaveDataSendMode(SPI_TypeDef *SPIx, uint32_t SlaveDataSendMode)
{
    MODIFY_REG(SPIx->SPCR1, SPI_SPCR1_SDOUTSEND, SlaveDataSendMode);
}


/**
 * @brief  Get Slave Data Send Mode
 * @rmtoll SPCR1          SlaveDataSendMode      LL_SPI_IsEnableSlaveDataSendAhead
 * @param  SPIx SPI Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_SPI_SlaveDataSendMode_Normal
 *         @arg @ref LL_SPI_SlaveDataSendMode_AheadHalfCycle
 */
__STATIC_INLINE uint32_t LL_SPI_IsEnableSlaveDataSendAhead(SPI_TypeDef *SPIx)
{
    return ((uint32_t)(READ_BIT(SPIx->SPCR1, SPI_SPCR1_SDOUTSEND) == SPI_SPCR1_SDOUTSEND));
}


/**
 * @brief  Config Filter for SSN pin
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll SPCR1          SignalFilterSSN      LL_SPI_SetPinFilterSSN
 * @param  SPIx SPI Instance
 * @param  SPI_EN This parameter can be one of the following values:
 *         @arg @ref LL_SPI_SignalFilterSSN_DISABLE
 *         @arg @ref LL_SPI_SignalFilterSSN_ENABLE
 * @retval None
 */
__STATIC_INLINE void LL_SPI_SetPinFilterSSNEn(SPI_TypeDef *SPIx, uint32_t SignalFilterSSN)
{
    MODIFY_REG(SPIx->SPCR1, SPI_SPCR1_SSNFILT, SignalFilterSSN);
}


/**
 * @brief  Get SignalFilterSSN
 * @rmtoll SPCR1          SignalFilterSSN      LL_SPI_IsEnablePinFilterSSN
 * @param  SPIx SPI Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_SPI_SignalFilterSSN_DISABLE
 *         @arg @ref LL_SPI_SignalFilterSSN_ENABLE
 */
__STATIC_INLINE uint32_t LL_SPI_IsEnablePinFilterSSN(SPI_TypeDef *SPIx)
{
    return ((uint32_t)(READ_BIT(SPIx->SPCR1, SPI_SPCR1_SSNFILT) == SPI_SPCR1_SSNFILT));
}


/**
 * @brief  Config Filter for SCK pin
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll SPCR1          SignalFilterSCK      LL_SPI_SetPinFilterSCK
 * @param  SPIx SPI Instance
 * @param  SPI_EN This parameter can be one of the following values:
 *         @arg @ref LL_SPI_SignalFilterSCK_DISABLE
 *         @arg @ref LL_SPI_SignalFilterSCK_ENABLE
 * @retval None
 */
__STATIC_INLINE void LL_SPI_SetPinFilterSCKEn(SPI_TypeDef *SPIx, uint32_t SignalFilterSCK)
{
    MODIFY_REG(SPIx->SPCR1, SPI_SPCR1_SCKFILT, SignalFilterSCK);
}


/**
 * @brief  Get SignalFilterSCK
 * @rmtoll SPCR1          SignalFilterSCK      LL_SPI_IsEnablePinFilterSCK
 * @param  SPIx SPI Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_SPI_SignalFilterSCK_DISABLE
 *         @arg @ref LL_SPI_SignalFilterSCK_ENABLE
 */
__STATIC_INLINE uint32_t LL_SPI_IsEnablePinFilterSCK(SPI_TypeDef *SPIx)
{
    return ((uint32_t)(READ_BIT(SPIx->SPCR1, SPI_SPCR1_SCKFILT) == SPI_SPCR1_SCKFILT));
}


/**
 * @brief  Config Filter for MOSI pin
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll SPCR1          SignalFilterMOSI      LL_SPI_SetPinFilterMOSI
 * @param  SPIx SPI Instance
 * @param  SPI_EN This parameter can be one of the following values:
 *         @arg @ref LL_SPI_SignalFilterMOSI_DISABLE
 *         @arg @ref LL_SPI_SignalFilterMOSI_ENABLE
 * @retval None
 */
__STATIC_INLINE void LL_SPI_SetPinFilterMOSIEn(SPI_TypeDef *SPIx, uint32_t SignalFilterMOSI)
{
    MODIFY_REG(SPIx->SPCR1, SPI_SPCR1_MOSIFILT, SignalFilterMOSI);
}


/**
 * @brief  Get SignalFilterMOSI
 * @rmtoll SPCR1          SignalFilterMOSI      LL_SPI_IsEnablePinFilterMOSI
 * @param  SPIx SPI Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_SPI_SignalFilterMOSI_DISABLE
 *         @arg @ref LL_SPI_SignalFilterMOSI_ENABLE
 */
__STATIC_INLINE uint32_t LL_SPI_IsEnablePinFilterMOSI(SPI_TypeDef *SPIx)
{
    return ((uint32_t)(READ_BIT(SPIx->SPCR1, SPI_SPCR1_MOSIFILT) == SPI_SPCR1_MOSIFILT));
}


/**
 * @brief  Config Filter for SPI pin
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll SPCR1          SignalFilterPin      LL_SPI_EnablePinFilter
 * @param  SPIx SPI Instance
 * @param  SPI_EN This parameter can be one of the following values:
 *         @arg @ref LL_SPI_SignalFilterSSN_ENABLE
 *         @arg @ref LL_SPI_SignalFilterSCK_ENABLE
 *         @arg @ref LL_SPI_SignalFilterMOSI_ENABLE
 * @retval None
 */
__STATIC_INLINE void LL_SPI_EnablePinFilter(SPI_TypeDef *SPIx, uint32_t SignalFilterPin)
{
    MODIFY_REG(SPIx->SPCR1, SPI_SPCR1_SSNFILT | SPI_SPCR1_SCKFILT | SPI_SPCR1_MOSIFILT, SignalFilterPin);
}


/**
 * @brief  Config SSN pin OD output
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll SPCR1          SPI_SsnOdEn      LL_SPI_SetSPI_SsnOdEn
 * @param  SPIx SPI Instance
 * @param  SPI_EN This parameter can be one of the following values:
 *         @arg @ref LL_SPI_SSNODEN_DISABLE
 *         @arg @ref LL_SPI_SSNODEN_ENABLE
 * @retval None
 */
__STATIC_INLINE void LL_SPI_SetSSNODEn(SPI_TypeDef *SPIx, uint32_t SPI_SsnOdEn)
{
    MODIFY_REG(SPIx->SPCR1, SPI_SPCR1_SSNODEN, SPI_SsnOdEn);
}


/**
 * @brief  Get SPI_SsnOdEn
 * @rmtoll SPCR1          SPI_SsnOdEn      LL_SPI_IsEnableSSNOD
 * @param  SPIx SPI Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_SPI_SSNODEN_DISABLE
 *         @arg @ref LL_SPI_SSNODEN_ENABLE
 */
__STATIC_INLINE uint32_t LL_SPI_IsEnableSSNOD(SPI_TypeDef *SPIx)
{
    return ((uint32_t)(READ_BIT(SPIx->SPCR1, SPI_SPCR1_SSNODEN) == SPI_SPCR1_SSNODEN));
}


/**
 * @brief  Set SSN if modified by MCU
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll SPCR2          SSN_MCU      LL_SPI_SetSSNMCUOutput
 * @param  SPIx SPI Instance
 * @param  SSN_MCU_EN This parameter can be one of the following values:
 *         @arg @ref LL_SPI_SSNMCU_LOW
 *         @arg @ref LL_SPI_SSNMCU_HIGH
 * @retval None
 */
__STATIC_INLINE void LL_SPI_SetSSNMCUOutput(SPI_TypeDef *SPIx, uint32_t SSN_MCU)
{
    MODIFY_REG(SPIx->SPCR2, SPI_SPCR2_SSNMCU, SSN_MCU);
}


/**
 * @brief  Get SSN_MCU
 * @rmtoll SPCR2          SSN_MCU      LL_SPI_IsHigh_SSNMCU
 * @param  SPIx SPI Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_SPI_SSNMCU_LOW
 *         @arg @ref LL_SPI_SSNMCU_HIGH
 */
__STATIC_INLINE uint32_t LL_SPI_IsHigh_SSNMCU(SPI_TypeDef *SPIx)
{
    return ((uint32_t)(READ_BIT(SPIx->SPCR2, SPI_SPCR2_SSNMCU) == SPI_SPCR2_SSNMCU));
}


/**
 * @brief  Set TX_ONLY_EN
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll SPCR3          TX_ONLY_EN      LL_SPI_SetTXonlyEn
 * @param  SPIx SPI Instance
 * @param  TX_ONLY_EN This parameter can be one of the following values:
 *         @arg @ref LL_SPI_TXONLYEN_DISABLE
 *         @arg @ref LL_SPI_TXONLYEN_ENABLE
 * @retval None
 */
__STATIC_INLINE void LL_SPI_SetTxOnlyEn(SPI_TypeDef *SPIx, uint32_t TX_ONLY_EN)
{
    MODIFY_REG(SPIx->SPCR3, SPI_SPCR3_TXONLYEN, TX_ONLY_EN);
}


/**
 * @brief  Get TX_ONLY_EN
 * @rmtoll SPCR3          TX_ONLY_EN      LL_SPI_IsEnableTXonly
 * @param  SPIx SPI Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_SPI_TXONLYEN_DISABLE
 *         @arg @ref LL_SPI_TXONLYEN_ENABLE
 */
__STATIC_INLINE uint32_t LL_SPI_IsEnableTxOnly(SPI_TypeDef *SPIx)
{
    return ((uint32_t)(READ_BIT(SPIx->SPCR3, SPI_SPCR3_TXONLYEN) == SPI_SPCR3_TXONLYEN));
}


/**SPCR4******************
 *
 */


/**
 * @brief  Set SPCR4 Multi Bit for clear some status
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll SPCR4          ClearBits      LL_SPI_ClearMultiBits
 * @param  SPIx SPI Instance
 * @param  ClearBits This parameter can be one of the following values:
 *         @arg @ref part of  LL_SPI_CLRTXFIFOWCOL | LL_SPI_CLRRXFIFOWCOL |  LL_SPI_CLRSLAVEERROR | LL_SPI_CLRMASTERERROR | LL_SPI_CLRRXFIFO | LL_SPI_CLRTXFIFO
 * @retval None
 */
__STATIC_INLINE void LL_SPI_ClearMultiBits(SPI_TypeDef *SPIx, uint32_t ClearBits)
{
    SET_BIT(SPIx->SPCR4, ClearBits);
}


/**
 * @brief  Set CLR_TXFIFO_WCOL Bit for clear TXFIFO_WCOL
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll SPCR4          CLR_TXFIFO_WCOL      LL_SPI_ClearFlag_TxFifoWcol
 * @param  SPIx SPI Instance
 * @param  CLR_TXFIFO_WCOL This parameter can be one of the following values:
 *         @arg @ref LL_SPI_CLRTXFIFOWCOL
 * @retval None
 */
__STATIC_INLINE void LL_SPI_ClearFlag_TxFifoWcol(SPI_TypeDef *SPIx)
{
    SET_BIT(SPIx->SPCR4, LL_SPI_CLRTXFIFOWCOL);
}


/**
 * @brief  Set CLR_RXFIFO_WCOL Bit for clear RXFIFO_WCOL
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll SPCR4          CLR_RXFIFO_WCOL      LL_SPI_SetClearRxfifoWcol
 * @param  SPIx SPI Instance
 * @param  CLR_TXFIFO_WCOL This parameter can be one of the following values:
 *         @arg @ref LL_CLRSPI_RXFIFOWCOL
 * @retval None
 */
__STATIC_INLINE void LL_SPI_ClearFlag_RxFifoWcol(SPI_TypeDef *SPIx)
{
    SET_BIT(SPIx->SPCR4, LL_SPI_CLRRXFIFOWCOL);
}


/**
 * @brief  Set CLR_SLAVE_ERROR Bit for clear SLAVE_ERROR
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll SPCR4          CLR_SLAVE_ERROR      LL_SPI_SetClearSlaveError
 * @param  SPIx SPI Instance
 * @param  CLR_SLAVE_ERROR This parameter can be one of the following values:
 *         @arg @ref LL_SPI_CLRSLAVEERROR
 * @retval None
 */
__STATIC_INLINE void LL_SPI_ClearSlaveError(SPI_TypeDef *SPIx)
{
    SET_BIT(SPIx->SPCR4, LL_SPI_CLRSLAVEERROR);
}


/**
 * @brief  Set CLR_MASTER_ERROR Bit for clear MASTER_ERROR
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll SPCR4          CLR_MASTER_ERROR      LL_SPI_SetClearMasterError
 * @param  SPIx SPI Instance
 * @param  CLR_MASTER_ERROR This parameter can be one of the following values:
 *         @arg @ref LL_SPI_CLRMASTERERROR
 * @retval None
 */
__STATIC_INLINE void LL_SPI_ClearMasterError(SPI_TypeDef *SPIx)
{
    SET_BIT(SPIx->SPCR4, LL_SPI_CLRMASTERERROR);
}


/**
 * @brief  Set CLR_RXFIFO Bit for clear RX_FIFO
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll SPCR4          CLR_RX_FIFO      LL_SPI_SetClearRxFifo
 * @param  SPIx SPI Instance
 * @param  CLR_RX_FIFO This parameter can be one of the following values:
 *         @arg @ref LL_SPI_CLRRXFIFO
 * @retval None
 */
__STATIC_INLINE void LL_SPI_ClearRxFifo(SPI_TypeDef *SPIx)
{
    SET_BIT(SPIx->SPCR4, LL_SPI_CLRRXFIFO);
}


/**
 * @brief  Set CLR_TXFIFO Bit for clear TX_FIFO
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll SPCR4          CLR_TX_FIFO      LL_SPI_SetClearTxFifo
 * @param  SPIx SPI Instance
 * @param  CLR_TX_FIFO This parameter can be one of the following values:
 *         @arg @ref LL_SPI_CLRTXFIFO
 * @retval None
 */
__STATIC_INLINE void LL_SPI_ClearTxFifo(SPI_TypeDef *SPIx)
{
    SET_BIT(SPIx->SPCR4, LL_SPI_CLRTXFIFO);
}


/**SPIIE******************
 *
 */


/**
 * @brief  Enable RX FIFO not empty interrupt
 * @rmtoll SPIIE          RX_NE_IE         LL_SPI_SetINT_RXNE
 * @param  SPIx SPI Instance
 * @param  RX_NE_IE This parameter can be one of the following values:
 *         @arg @ref LL_SPI_RXNEIE_DISABLE
 *         @arg @ref LL_SPI_RXNEIE_ENABLE
 * @retval None
 */
__STATIC_INLINE void LL_SPI_SetINT_RXNE(SPI_TypeDef *SPIx, uint32_t RX_NE_IE)
{
    MODIFY_REG(SPIx->SPIIE, SPI_SPIIE_RXNEIE, RX_NE_IE);
}


/**
 * @brief  Get RX_NE_IE
 * @rmtoll SPIIE          RX_NE_IE      LL_SPI_IsEnableINT_RXNE
 * @param  SPIx SPI Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_SPI_RXNEIE_DISABLE
 *         @arg @ref LL_SPI_RXNEIE_ENABLE
 */
__STATIC_INLINE uint32_t LL_SPI_IsEnableINT_RXNE(SPI_TypeDef *SPIx)
{
    return ((uint32_t)(READ_BIT(SPIx->SPIIE, SPI_SPIIE_RXNEIE) == SPI_SPIIE_RXNEIE));
}


/**
 * @brief  Enable TX FIFO empty interrupt
 * @rmtoll SPIIE          TX_E_IE         LL_SPI_SetINT_TXE
 * @param  SPIx SPI Instance
 * @param  TX_E_IE This parameter can be one of the following values:
 *         @arg @ref LL_SPI_TXEIE_DISABLE
 *         @arg @ref LL_SPI_TXEIE_ENABLE
 * @retval None
 */
__STATIC_INLINE void LL_SPI_SetINT_TXE(SPI_TypeDef *SPIx, uint32_t TX_E_IE)
{
    MODIFY_REG(SPIx->SPIIE, SPI_SPIIE_TXEIE, TX_E_IE);
}


/**
 * @brief  Get TX_E_IE
 * @rmtoll SPIIE          TX_E_IE      LL_SPI_IsEnableINT_TXE
 * @param  SPIx SPI Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_SPI_TXEIE_DISABLE
 *         @arg @ref LL_SPI_TXEIE_ENABLE
 */
__STATIC_INLINE uint32_t LL_SPI_IsEnableINT_TXE(SPI_TypeDef *SPIx)
{
    return ((uint32_t)(READ_BIT(SPIx->SPIIE, SPI_SPIIE_TXEIE) == SPI_SPIIE_TXEIE));
}


/**
 * @brief  Enable RX_F_HALF_IE interrupt
 * @rmtoll SPIIE          RX_F_HALF_IE         LL_SPI_SetINT_RXFHalf
 * @param  SPIx SPI Instance
 * @param  RX_F_HALF_IE This parameter can be one of the following values:
 *         @arg @ref LL_SPI_RXFHALFIE_DISABLE
 *         @arg @ref LL_SPI_RXFHALFIE_ENABLE
 * @retval None
 */
__STATIC_INLINE void LL_SPI_SetINT_RXFHalf(SPI_TypeDef *SPIx, uint32_t RX_F_HALF_IE)
{
    MODIFY_REG(SPIx->SPIIE, SPI_SPIIE_RXHFIE, RX_F_HALF_IE);
}


/**
 * @brief  Get RX_F_HALF_IE
 * @rmtoll SPIIE          RX_F_HALF_IE      LL_SPI_IsEnableINT_RXFHalf
 * @param  SPIx SPI Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_SPI_RXFHALFIE_DISABLE
 *         @arg @ref LL_SPI_RXFHALFIE_ENABLE
 */
__STATIC_INLINE uint32_t LL_SPI_IsEnableINT_RXFHalf(SPI_TypeDef *SPIx)
{
    return ((uint32_t)(READ_BIT(SPIx->SPIIE, SPI_SPIIE_RXHFIE) == SPI_SPIIE_RXHFIE));
}


/**
 * @brief  Enable TX_E_HALF_IE interrupt
 * @rmtoll SPIIE          TX_E_HALF_IE         LL_SPI_SetINT_TXEHalf
 * @param  SPIx SPI Instance
 * @param  TX_E_HALF_IE This parameter can be one of the following values:
 *         @arg @ref LL_SPI_TXEHALFIE_DISABLE
 *         @arg @ref LL_SPI_TXEHALFIE_ENABLE
 * @retval None
 */
__STATIC_INLINE void LL_SPI_SetINT_TXEHalf(SPI_TypeDef *SPIx, uint32_t TX_E_HALF_IE)
{
    MODIFY_REG(SPIx->SPIIE, SPI_SPIIE_TXHEIE, TX_E_HALF_IE);
}


/**
 * @brief  Get TX_E_HALF_IE
 * @rmtoll SPIIE          TX_E_HALF_IE      LL_SPI_IsEnableINT_TXEHalf
 * @param  SPIx SPI Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_SPI_TXEHALFIE_DISABLE
 *         @arg @ref LL_SPI_TXEHALFIE_ENABLE
 */
__STATIC_INLINE uint32_t LL_SPI_IsEnableINT_TXEHalf(SPI_TypeDef *SPIx)
{
    return ((uint32_t)(READ_BIT(SPIx->SPIIE, SPI_SPIIE_TXHEIE) == SPI_SPIIE_TXHEIE));
}


/**
 * @brief  Enable ERROR_IE interrupt
 * @rmtoll SPIIE          ERROR_IE         LL_SPI_SetINT_Error
 * @param  SPIx SPI Instance
 * @param  ERROR_IE This parameter can be one of the following values:
 *         @arg @ref LL_SPI_ERRORIE_DISABLE
 *         @arg @ref LL_SPI_ERRORIE_ENABLE
 * @retval None
 */
__STATIC_INLINE void LL_SPI_SetINT_Error(SPI_TypeDef *SPIx, uint32_t ERROR_IE)
{
    MODIFY_REG(SPIx->SPIIE, SPI_SPIIE_ERRIE, ERROR_IE);
}


/**
 * @brief  Get ERROR_IE
 * @rmtoll SPIIE          ERROR_IE      LL_SPI_IsEnableINT_Error
 * @param  SPIx SPI Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_SPI_ERRORIE_DISABLE
 *         @arg @ref LL_SPI_ERRORIE_ENABLE
 */
__STATIC_INLINE uint32_t LL_SPI_IsEnableINT_Error(SPI_TypeDef *SPIx)
{
    return ((uint32_t)(READ_BIT(SPIx->SPIIE, SPI_SPIIE_ERRIE) == SPI_SPIIE_ERRIE));
}


/**SPDF******************
 *
 */


/**
 * @brief  Get RX_FIFO_NE
 * @rmtoll SPDF          RX_FIFO_NE      LL_SPI_IsEmpty_RxFifo
 * @param  SPIx SPI Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref TRUE
 *         @arg @ref FALSE
 */
__STATIC_INLINE uint32_t LL_SPI_IsEmpty_RxFifo(SPI_TypeDef *SPIx)
{
    return ((uint32_t)(READ_BIT(SPIx->SPDF, SPI_SPDF_RXFIFONE) != SPI_SPDF_RXFIFONE));
}


/**
 * @brief  Get RXFIFO_S Recieve Number
 * @rmtoll SPDF          RXFIFO_S      LL_SPI_GetLengthRxFifo
 * @param  SPIx SPI Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_SPI_RXFIFOS_0BYTE
 *         @arg @ref LL_SPI_RXFIFOS_1BYTE
 *         @arg @ref LL_SPI_RXFIFOS_2BYTE
 *         @arg @ref LL_SPI_RXFIFOS_3BYTE
 *         @arg @ref LL_SPI_RXFIFOS_4BYTE
 *         @arg @ref LL_SPI_RXFIFOS_5BYTE
 *         @arg @ref LL_SPI_RXFIFOS_6BYTE
 *         @arg @ref LL_SPI_RXFIFOS_7BYTE
 *         @arg @ref LL_SPI_RXFIFOS_8BYTE
 */
__STATIC_INLINE uint32_t LL_SPI_GetLengthRxFifo(SPI_TypeDef *SPIx)
{
    return ((uint32_t)(READ_BIT(SPIx->SPDF, SPI_SPDF_RXFIFOS_Pos) >> SPI_SPDF_RXFIFOS_Pos));
}


/**
 * @brief  Get TX_FIFO_E
 * @rmtoll SPDF          TX_FIFO_E      LL_SPI_IsNotEmpty_TxFifo
 * @param  SPIx SPI Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_SPI_TXFIFOE_NOTEMPTY
 *         @arg @ref LL_SPI_TXFIFOE_EMPTY
 */
__STATIC_INLINE uint32_t LL_SPI_IsNotEmpty_TxFifo(SPI_TypeDef *SPIx)
{
    return ((uint32_t)(READ_BIT(SPIx->SPDF, SPI_SPDF_TXFIFOE) != SPI_SPDF_TXFIFOE));
}


/**
 * @brief  Get TXFIFO_S TXFIFO data Number
 * @rmtoll SPDF          TXFIFO_S      LL_SPI_GetLengthTXFifo
 * @param  SPIx SPI Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_SPI_TXFIFOS_0BYTE
 *         @arg @ref LL_SPI_TXFIFOS_1BYTE
 *         @arg @ref LL_SPI_TXFIFOS_2BYTE
 *         @arg @ref LL_SPI_TXFIFOS_3BYTE
 *         @arg @ref LL_SPI_TXFIFOS_4BYTE
 *         @arg @ref LL_SPI_TXFIFOS_5BYTE
 *         @arg @ref LL_SPI_TXFIFOS_6BYTE
 *         @arg @ref LL_SPI_TXFIFOS_7BYTE
 *         @arg @ref LL_SPI_TXFIFOS_8BYTE
 */
__STATIC_INLINE uint32_t LL_SPI_GetLengthTXFifo(SPI_TypeDef *SPIx)
{
    return ((uint32_t)(READ_BIT(SPIx->SPDF, SPI_SPDF_TXFIFOS_Pos) >> SPI_SPDF_TXFIFOS_Pos));
}


/**SPSR************************************
 * @}
 */


/**
 * @brief  Check if the SPI Flag is set or not
 * @rmtoll STATUS               LL_SPI_GetFlag
 * @param  SPIx SPI Instance
 * @param  flag
 *         @arg @ref LL_SPI_FLAG_BUSY
 *         @arg @ref LL_SPI_FLAG_TXWCOL
 *         @arg @ref LL_SPI_FLAG_RXWCOL
 *         @arg @ref LL_SPI_FLAG_SLAVEERROR
 *         @arg @ref LL_SPI_FLAG_MASTERERROR
 * @retval State of bit (1 or 0).
 */
__STATIC_INLINE uint32_t LL_SPI_GetFlag(SPI_TypeDef *SPIx, uint32_t flag)
{
    return (READ_BIT(SPIx->SPSR, flag) == (flag));
}


/**
 * @brief  Get BUSY: TXFIFO not empty or SPI in translate
 * @rmtoll SPSR          BUSY      LL_SPI_IsBusy
 * @param  SPIx SPI Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref TRUE
 *         @arg @ref FLASE
 */
__STATIC_INLINE uint32_t LL_SPI_IsBusy(SPI_TypeDef *SPIx)
{
    return ((uint32_t)(READ_BIT(SPIx->SPSR, SPI_SPSR_BUSY) == SPI_SPSR_BUSY));
}


/**
 * @brief  Get TXFIFO_WCOL MCU write TXFIFO when it is full
 * @rmtoll SPSR          TXFifoWcol      LL_SPI_IsFull_TxFifo
 * @param  SPIx SPI Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_SPI_TXFIFONOWCOL
 *         @arg @ref LL_SPI_TXFIFOWCOL
 */
__STATIC_INLINE uint32_t LL_SPI_IsFull_TxFifo(SPI_TypeDef *SPIx)
{
    return ((uint32_t)(READ_BIT(SPIx->SPSR, SPI_SPSR_TXWCOL) == SPI_SPSR_TXWCOL));
}


/**
 * @brief  Get RXFIFO_WCOL RX when RXFIFO is full
 * @rmtoll SPSR          RXFifoWcol      LL_SPI_IsFull_RxFifo
 * @param  SPIx SPI Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_SPI_TXFIFONOWCOL
 *         @arg @ref LL_SPI_TXFIFOWCOL
 */
__STATIC_INLINE uint32_t LL_SPI_IsFull_RxFifo(SPI_TypeDef *SPIx)
{
    return ((uint32_t)(READ_BIT(SPIx->SPSR, SPI_SPSR_RXWCOL) == SPI_SPSR_RXWCOL));
}


/**
 * @brief  Get SLAVE_ERROR SSN change to 1 during transfer 1byte
 * @rmtoll SPSR          SlaveError      LL_SPI_GetSlaveError
 * @param  SPIx SPI Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_SPI_NOSLAVEERROR
 *         @arg @ref LL_SPI_SLAVEERROR
 */
__STATIC_INLINE uint32_t LL_SPI_IsActiveFlag_SlaveError(SPI_TypeDef *SPIx)
{
    return ((uint32_t)(READ_BIT(SPIx->SPSR, SPI_SPSR_SLAVEERROR) == SPI_SPSR_SLAVEERROR));
}


/**
 * @brief  Get MASTER_ERROR SSN change to 1 during transfer 1byte
 * @rmtoll SPSR          MasterError      LL_SPI_IsActiveMasterErrorFlag
 * @param  SPIx SPI Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_SPI_NoMasterError
 *         @arg @ref LL_SPI_MasterError
 */
__STATIC_INLINE uint32_t LL_SPI_IsActiveFlag_MasterError(SPI_TypeDef *SPIx)
{
    return ((uint32_t)(READ_BIT(SPIx->SPSR, SPI_SPSR_MASTERERROR) == SPI_SPSR_MASTERERROR));
}


/**TXFIFO******************
 *
 */


/**
 * @brief  Set TXFIFO DATA for translate
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll TXFIFO          WRDATA      LL_SPI_SetWrFifoData
 * @param  SPIx SPI Instance
 * @param  ClearBits This parameter can be one of the following values:
 *         @arg @ref part of  0x0~0xFFFFFFFF
 * @retval None
 */
__STATIC_INLINE void LL_SPI_TransmitData(SPI_TypeDef *SPIx, uint32_t WrData)
{
    WRITE_REG(SPIx->TXFIFO, WrData);
}


/**RXFIFO************************************
 * @}
 */


/**
 * @brief  Get RXFIFO data
 * @rmtoll RXFIFO          RXDATA      LL_SPI_ReceiveData
 * @param  SPIx SPI Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref
 */
__STATIC_INLINE uint8_t LL_SPI_ReceiveData(SPI_TypeDef *SPIx)
{
    return ((uint8_t)(READ_BIT(SPIx->RXFIFO, SPI_RXFIFO_RXDATA)));
}


void LL_SPI_StructInitParam(LL_SPI_InitParamTypeDef *SPI_InitParamStruct);


void LL_SPI_StructInit(LL_SPI_InitParamTypeDef *SPI_InitParamStruct);


void LL_SPIx_Init(SPI_TypeDef *SPIx, LL_SPI_InitParamTypeDef *SPI_InitParamStruct);


#endif /* defined (SPI0) || defined (SPI1) || defined (SPI2) || defined (SPI3) */


/**
 * @}
 */


#ifdef __cplusplus
}
#endif
#endif
/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/

