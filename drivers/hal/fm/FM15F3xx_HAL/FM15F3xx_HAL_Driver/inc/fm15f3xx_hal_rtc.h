/**
 ******************************************************************************
 * @file    fm15f3xx_hal_rtc.h
 * @author  CLF
 * @version V1.0.0
 * @brief   Header file of RTC HAL module.
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */

/* Define to prevent recursive inclusion ------------------------------------*/
#ifndef __FM15F3xx_HAL_RTC_H
#define __FM15F3xx_HAL_RTC_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes -----------------------------------------------------------------*/
#include "fm15f3xx_hal_def.h"
#include "fm15f3xx_ll_rtc.h"
#include <time.h>


/** @addtogroup FM15F3xx_HAL_Driver
 * @{
 */


/** @addtogroup RTC
 * @{
 */

/* Exported types -----------------------------------------------------------*/


/** @defgroup RTC_Exported_Types RTC Exported Types
 *@{
 */


/**
 * @brief HAL RTC State structures definition
 */
typedef enum {
    HAL_RTC_STATE_RESET = 0x00U,            /*! < RTC not yet initialized or disabled */

    HAL_RTC_STATE_READY = 0x01U,            /*! < RTC initialized and ready for use   */

    HAL_RTC_STATE_BUSY = 0x02U,             /*! < RTC process is ongoing              */

    HAL_RTC_STATE_ERROR = 0x03U,            /*! < RTC error state                     */
} HAL_RTCStateTypeDef;


/**
 * @brief RTC Digital adjustment compensation Structure definition
 */
typedef struct {
    uint8_t CompensationDirection;          /*! < Specifies the digital adjustment direction.
                                               0 - Increase the count value,1 - Decrease the count value.*/

    uint8_t CompensationInterVal;           /*! < Specifies the digital adjustment interval.
                                               The parameter must be a number between Min_Data = 1 and Max-Data = 256.*/

    uint8_t CompensationValue;              /*! < Specifies the digital adjustment value.
                                               The parameter must be a number between Min_Data = 0x00 and Max-Data = 0x7F.*/
} RTC_CompensationDef;


/**
 * @brief RTC Time Structure definition
 */
typedef struct {
    uint32_t Hours;                         /*! < Specifies the RTC Time Hours.
                                               The parameter must be a number between Min_Data = 0 and Max-Data = 23.*/

    uint32_t Minutes;                       /*! < Specifies the RTC Time Minutes.
                                               The parameter must be a number between Min_Data = 0 and Max-Data = 59.*/

    uint32_t Seconds;                       /*! < Specifies the RTC Time Seconds.
                                               The parameter must be a number between Min_Data = 0 and Max-Data = 59.*/

    uint32_t Date;                          /*! < Specifies the RTC Time Date.
                                               The parameter must be a number between Min_Data = 0 and Max-Data = 31.*/

    uint32_t Month;                         /*! < Specifies the RTC Time Month.
                                               The parameter must be a number between Min_Data = 0 and Max-Data = 12.*/

    uint32_t Year;                          /*! < Specifies the RTC Time Year. The parameter equals set year minus base year.
                                               The parameter must be a number between Min_Data = 0 and Max-Data = 136.*/
    char *Weekday;                          /*! < Specifies the RTC Time Weekday.*/
} RTC_TimeTypeDef;


/**
 * @brief RTC Alarm Structure definition
 */
typedef struct {
    RTC_TimeTypeDef AlarmTime;              /*! < Specifies the RTC Alarm Time members. */

    uint32_t AlarmType;                     /*! < Specifies the RTC Alarm Type.
                                               The parameter can be a value of @ref RTC_Alarm_Type_Definitions.*/
} RTC_AlarmTypeDef;


/**
 * @brief RTC Handle Structure definition
 */
typedef struct {
    RTC_TIME_TypeDef *Instance;             /*! < Register base address */

    RTC_CompensationDef Compen;             /*! < RTC Compensation required parameters */

    uint32_t TimePrescaler;                 /*! < Specifies the RTC Time Prescaler.
                                               The parameter must be a number between Min_Data = 0X0000 and Max-Data = 0XFFFF.*/

    __IO HAL_RTCStateTypeDef State;         /*! < RTC Time communication state */
} RTC_HandleTypeDef;


/**
 *@}
 */

/* Exported constants -----------------------------------------------------------*/


/** @defgroup RTC_Exported_Constants RTC Exported Constants
 *@{
 */


/** @defgroup RTC_Alarm_Type_Definitions RTC Alarm Type Definitions
 * @{
 */
#define  RTC_ALARM_SECONDS  (0x00000000U)
#define  RTC_ALARM_CYCLE    (0x00000001U)


/**
 *@}
 */


/** @defgroup RTC_Interrupts_Definitions RTC Interrupts Definitions
 * @{
 */
#define  HAL_RTC_ENABLE_INVLDIE     RTC_TIME_TIMEIE_INVLDIE_Msk
#define  HAL_RTC_ENABLE_OVIE        RTC_TIME_TIMEIE_OVIE_Msk
#define  HAL_RTC_ENABLE_SECONDIE    RTC_TIME_TIMEIE_SECONDIE_Msk
#define  HAL_RTC_ENABLE_1SIE        RTC_TIME_TIMEIE_1SIE_Msk
#define  HAL_RTC_ENABLE_CYCLEIE     RTC_TIME_TIMEIE_CYCLEIE_Msk
#define  HAL_RTC_ENABLE_1SFLAGIE    RTC_TIME_TIMEIE_1SFLAGIE_Msk
#define  HAL_RTC_DISABLE_ALLIE      (0x00000000U)
#define  HAL_RTC_ENABLE_ALLIE       (0x81FU)


/**
 *@}
 */


/** @defgroup RTC_Status_Flags_Definitions RTC Status Flags Definitions
 * @{
 */
#define  RTC_UTC_CYCLE_RECORD       RTC_TIME_STATUS_CYCRD_Msk
#define  RTC_UTC_1S_RECORD          RTC_TIME_STATUS_1SRD_Msk
#define  RTC_UTC_ALARM_RECORD       RTC_TIME_STATUS_SECONDRD_Msk
#define  RTC_UTC_OVERFLOW_RECORD    RTC_TIME_STATUS_OVRD_Msk
#define  RTC_UTC_INVALID_RECORD     RTC_TIME_STATUS_INVLDRD_Msk
#define  RTC_UTC_CYCLE_FLAG         RTC_TIME_STATUS_CYCFG_Msk
#define  RTC_UTC_1S_FLAG            RTC_TIME_STATUS_1SFG_Msk
#define  RTC_UTC_ALARM_FLAG         RTC_TIME_STATUS_SECONDFG_Msk
#define  RTC_UTC_OVERFLOW_FLAG      RTC_TIME_STATUS_OVFG_Msk
#define  RTC_UTC_INVALID_FLAG       RTC_TIME_STATUS_INVLDFG_Msk


/**
 * @}
 */


/** @defgroup RTC_Record_Clear_Definitions RTC Record Clear Definitions
 * @{
 */
#define  RTC_CLR_CYCLE_RECORD       RTC_TIME_RECORDCLR_CYCCLR
#define  RTC_CLR_1S_RECORD          RTC_TIME_RECORDCLR_1SCLR
#define  RTC_CLR_ALARM_RECORD       RTC_TIME_RECORDCLR_SECONDCLR
#define  RTC_CLR_OVERFLOW_RECORD    RTC_TIME_RECORDCLR_OVCLR
#define  RTC_CLR_INVALID_RECORD     RTC_TIME_RECORDCLR_INVLDCLR


/**
 *@}
 */


/**
 *@}
 */


/** @addtogroup RTC_Exported_Functions
 *@{
 */


/** @addtogroup RTC_Exported_Functions_Group1
 *@{
 */
/* Initialization and de-initialization functions -------------------------------*/
HAL_StatusTypeDef HAL_RTC_Init(RTC_HandleTypeDef *hrtc);


HAL_StatusTypeDef HAL_RTC_DeInit(RTC_HandleTypeDef *hrtc);


void HAL_RTC_MspInit(RTC_HandleTypeDef *hrtc);


void HAL_RTC_MspDeInit(RTC_HandleTypeDef *hrtc);


/**
 *@}
 */


/** @addtogroup RTC_Exported_Functions_Group2
 *@{
 */
/* RTC Time and Time compensation functions -------------------------------------*/
HAL_StatusTypeDef HAL_RTC_SetTime(RTC_HandleTypeDef *hrtc, RTC_TimeTypeDef *sTime);


HAL_StatusTypeDef HAL_RTC_SetTimeCompensation(RTC_HandleTypeDef *hrtc, RTC_CompensationDef *Compen);


RTC_TimeTypeDef HAL_RTC_GetTime(RTC_HandleTypeDef *hrtc);


/**
 *@}
 */


/** @addtogroup RTC_Exported_Functions_Group3
 *@{
 */
/* RTC Alarm functions -------------------------------------------------------------*/
HAL_StatusTypeDef HAL_RTC_SetAlarmTime(RTC_HandleTypeDef *hrtc, RTC_AlarmTypeDef *sAlarm);


RTC_AlarmTypeDef HAL_RTC_GetAlarmTime(RTC_HandleTypeDef *hrtc, uint32_t Alarm);


HAL_StatusTypeDef HAL_RTC_DeactivateAlarm(RTC_HandleTypeDef *hrtc, uint32_t Alarm);


/**
 *@}
 */


/** @addtogroup RTC_Exported_Functions_Group4
 *@{
 */
/* RTC IRQ functions --------------------------------------------------------__-----*/
HAL_StatusTypeDef HAL_RTC_SetAlarmTime_Int(RTC_HandleTypeDef *hrtc, RTC_AlarmTypeDef *sAlarm);


HAL_StatusTypeDef HAL_RTC_1S_Int(RTC_HandleTypeDef *hrtc, uint32_t RTC_Int);


void HAL_RTC_AlarmIRQHandler(RTC_HandleTypeDef *hrtc);


void HAL_RTC_AlarmEventCallback(RTC_HandleTypeDef *hrtc);


void HAL_RTC_1S_IRQHandler(RTC_HandleTypeDef *hrtc);


void HAL_RTC_1S_EventCallback(RTC_HandleTypeDef *hrtc);


/**
 *@}
 */


/** @addtogroup RTC_Exported_Functions_Group5
 *@{
 */
/* RTC Time transform functions -----------------------------------------------------*/
uint8_t IsLeapYear(uint32_t year);


uint32_t RTC_TimetoTimestamp(RTC_TimeTypeDef *sTime);


/**
 *@}
 */


/** @addtogroup RTC_Exported_Functions_Group6
 *@{
 */
/* Peripheral Control functions-----------------------------------------------------*/
HAL_RTCStateTypeDef HAL_RTC_GetState(RTC_HandleTypeDef *hrtc);


/**
 *@}
 */


/**
 *@}
 */

/* Private macros ------------------------------------------------------------------*/


/** @addtogroup RTC_IS_RTC_Definitions RTC Private macros to check input parameters
 *@{
 */
#define IS_RTC_OUTPUT( OUTPUT )         ( ( (OUTPUT) == RTC_OUTPUT_DISABLE) ||  \
                                          ( (OUTPUT) == RTC_OUTPUT_XTAL32K) )
#define IS_RTC_OUTPUT_POLARITY( POL )   ( ( (POL) == RTC_OUTPUT_POLARITY_NOINV) ||  \
                                          ( (POL) == RTC_OUTPUT_POLARITY_INV) )
#define IS_RTC_OUTPUT_OD( TYPE )        ( ( (TYPE) == RTC_OUTPUT_OD_DISABLE) ||  \
                                          ( (TYPE) == RTC_OUTPUT_OD_ENABLE) )
#define IS_RTC_PSC( PSC )               ( (PSC) <= 0xFFFFU)
#define IS_RTC_HOUR( HOUR )             ( (HOUR) <= 23U)
#define IS_RTC_MINUTES( MIN )           ( (MIN) <= 59U)
#define IS_RTC_SECONDS( SEC )           ( (SEC) <= 59U)
#define IS_RTC_YEAR( YEAR )             ( (YEAR) <= 2136U)
#define IS_RTC_MONTH( MONTH )           ( (MONTH) >= 1U || (MONTH) <= 12U)
#define IS_RTC_DATE( DATE )             ( (DATE) >= 1U || (DATE) <= 31U)
#define IS_RTC_ALARM( ALARM )           ( ( (ALARM) == RTC_ALARM_SECONDS) || ( (ALARM) == RTC_ALARM_CYCLE) )
#define IS_RTC_INT( INT )               ( ( ( (INT) == HAL_RTC_ENABLE_INVLDIE) || (INT) == HAL_RTC_ENABLE_OVIE) || \
                                          ( (INT) == HAL_RTC_ENABLE_SECONDIE) || ( (INT) == HAL_RTC_ENABLE_1SIE) || \
                                          ( (INT) == HAL_RTC_ENABLE_CYCLEIE) || ( (INT) == HAL_RTC_ENABLE_1SFLAGIE) )


/**
 *@}
 */


/**
 *@}
 */


/**
 *@}
 */

#ifdef __cplusplus
}
#endif

#endif
