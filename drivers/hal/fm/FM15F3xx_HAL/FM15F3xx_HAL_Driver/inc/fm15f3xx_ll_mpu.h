/**
 ******************************************************************************
 * @file    fm15f3xx_ll_mpu.h
 * @author  SRG
 * @version V1.0.0
 * @date    2020-03-17
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */

#ifndef FM15F3XX_LL_MPU_H
#define FM15F3XX_LL_MPU_H
#ifdef __cplusplus
extern "C" {
#endif

#include "fm15f3xx.h"


/** @addtogroup FM15F366_LL_Driver
 * @{
 */

/* Private types -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private constants ---------------------------------------------------------*/
/* Private macros ------------------------------------------------------------*/
/* ExSPIed functions --------------------------------------------------------*/
/*! @brief MPU region config. */
typedef struct {
    uint32_t    regionNum;
    uint32_t    startAddress;
    uint32_t    endAddress;
    uint32_t    accessRights1;
    uint32_t    accessRights2;
    uint32_t    accessRights3;
    uint32_t    setCtrl;
    uint32_t    setSwCtl;
} LL_MPU_RegionTypeDef;

/*! @brief MPU region right config. */
typedef struct {
    uint32_t    regionNum;
    uint32_t    accessRights1;
    uint32_t    accessRights2;
    uint32_t    accessRights3;
    uint32_t    setCtrl;
    uint32_t    setSwCtl;
} LL_MPU_RightTypeDef;

/*! @brief MPU detail error access information. */
typedef struct {
    uint32_t    status;
    uint32_t    address;
    uint32_t    details;
} LL_MPU_AccessErrTypeDef;


/** @defgroup MPU_LL_Configuration Configuration
 * @{
 */

#define LL_MPU_REGION_COUNT     (8U)
#define LL_MPU_ACCESS_RIGHT_N   0x00

#define LL_MPU_REGION_NUM_0 (0x00)
#define LL_MPU_REGION_NUM_1 (0x01)
#define LL_MPU_REGION_NUM_2 (0x02)
#define LL_MPU_REGION_NUM_3 (0x03)
#define LL_MPU_REGION_NUM_4 (0x04)
#define LL_MPU_REGION_NUM_5 (0x05)
#define LL_MPU_REGION_NUM_6 (0x06)
#define LL_MPU_REGION_NUM_7 (0x07)

#define LL_MPU_REGION_DMA_DSP_N 0x00
#define LL_MPU_REGION_SJUMPE_N  0x00

#define LL_MPU_REGIONE_ENABLE   MPU_CTRL_REGIONE
#define LL_MPU_REGIONE_DISABLE  0

#define LL_MPU_AUTHA_DSPWA  MPU_AUTHA_DSPWA
#define LL_MPU_AUTHA_DSPRA  MPU_AUTHA_DSPRA
#define LL_MPU_AUTHA_DMAWA  MPU_AUTHA_DMAWA
#define LL_MPU_AUTHA_DMARA  MPU_AUTHA_DMARA
#define LL_MPU_AUTHB_R0UPX  MPU_AUTHB_R0UPX
#define LL_MPU_AUTHB_R0UPW  MPU_AUTHB_R0UPW
#define LL_MPU_AUTHB_R0UPR  MPU_AUTHB_R0UPR
#define LL_MPU_AUTHB_R0PX   MPU_AUTHB_R0PX
#define LL_MPU_AUTHB_R0PW   MPU_AUTHB_R0PW
#define LL_MPU_AUTHB_R0PR   MPU_AUTHB_R0PR
#define LL_MPU_AUTHB_R1UPX  MPU_AUTHB_R1UPX
#define LL_MPU_AUTHB_R1UPW  MPU_AUTHB_R1UPW
#define LL_MPU_AUTHB_R1UPR  MPU_AUTHB_R1UPR
#define LL_MPU_AUTHB_R1PX   MPU_AUTHB_R1PX
#define LL_MPU_AUTHB_R1PW   MPU_AUTHB_R1PW
#define LL_MPU_AUTHB_R1PR   MPU_AUTHB_R1PR
#define LL_MPU_AUTHB_R2UPX  MPU_AUTHB_R2UPX
#define LL_MPU_AUTHB_R2UPW  MPU_AUTHB_R2UPW
#define LL_MPU_AUTHB_R2UPR  MPU_AUTHB_R2UPR
#define LL_MPU_AUTHB_R2PX   MPU_AUTHB_R2PX
#define LL_MPU_AUTHB_R2PW   MPU_AUTHB_R2PW
#define LL_MPU_AUTHB_R2PR   MPU_AUTHB_R2PR
#define LL_MPU_AUTHB_R3UPX  MPU_AUTHB_R3UPX
#define LL_MPU_AUTHB_R3UPW  MPU_AUTHB_R3UPW
#define LL_MPU_AUTHB_R3UPR  MPU_AUTHB_R3UPR
#define LL_MPU_AUTHB_R3PX   MPU_AUTHB_R3PX
#define LL_MPU_AUTHB_R3PW   MPU_AUTHB_R3PW
#define LL_MPU_AUTHB_R3PR   MPU_AUTHB_R3PR
#define LL_MPU_AUTHC_R4UPX  MPU_AUTHC_R4UPX
#define LL_MPU_AUTHC_R4UPW  MPU_AUTHC_R4UPW
#define LL_MPU_AUTHC_R4UPR  MPU_AUTHC_R4UPR
#define LL_MPU_AUTHC_R4PX   MPU_AUTHC_R4PX
#define LL_MPU_AUTHC_R4PW   MPU_AUTHC_R4PW
#define LL_MPU_AUTHC_R4PR   MPU_AUTHC_R4PR
#define LL_MPU_AUTHC_R5UPX  MPU_AUTHC_R5UPX
#define LL_MPU_AUTHC_R5UPW  MPU_AUTHC_R5UPW
#define LL_MPU_AUTHC_R5UPR  MPU_AUTHC_R5UPR
#define LL_MPU_AUTHC_R5PX   MPU_AUTHC_R5PX
#define LL_MPU_AUTHC_R5PW   MPU_AUTHC_R5PW
#define LL_MPU_AUTHC_R5PR   MPU_AUTHC_R5PR
#define LL_MPU_AUTHC_R6UPX  MPU_AUTHC_R6UPX
#define LL_MPU_AUTHC_R6UPW  MPU_AUTHC_R6UPW
#define LL_MPU_AUTHC_R6UPR  MPU_AUTHC_R6UPR
#define LL_MPU_AUTHC_R6PX   MPU_AUTHC_R6PX
#define LL_MPU_AUTHC_R6PW   MPU_AUTHC_R6PW
#define LL_MPU_AUTHC_R6PR   MPU_AUTHC_R6PR
#define LL_MPU_AUTHC_R7UPX  MPU_AUTHC_R7UPX
#define LL_MPU_AUTHC_R7UPW  MPU_AUTHC_R7UPW
#define LL_MPU_AUTHC_R7UPR  MPU_AUTHC_R7UPR
#define LL_MPU_AUTHC_R7PX   MPU_AUTHC_R7PX
#define LL_MPU_AUTHC_R7PW   MPU_AUTHC_R7PW
#define LL_MPU_AUTHC_R7PR   MPU_AUTHC_R7PR


/**
 * @brief  LL_MPU_Get_CSR
 * @param  None
 * @retval RegValue
 */
__STATIC_INLINE uint32_t LL_MPU_Get_CSR(MPU_TypeDef *MPUx)
{
    return (READ_REG(MPUx->CSR));
}


/**
 * @brief  LL_MPU_Set_CSR
 * @param  None
 * @retval None
 */
__STATIC_INLINE void LL_MPU_Set_CSR(MPU_TypeDef *MPUx, uint32_t val)
{
    WRITE_REG(MPUx->CSR, val);
}


/**
 * @brief  LL_MPU_Get_EAR
 * @param  None
 * @retval RegValue
 */
__STATIC_INLINE uint32_t LL_MPU_Get_EAR(MPU_TypeDef *MPUx, uint8_t num)
{
    return ((uint32_t)(READ_REG(MPUx->SP[num].EAR)));
}


/**
 * @brief  LL_MPU_Get_EDR
 * @param  None
 * @retval RegValue
 */
__STATIC_INLINE uint32_t LL_MPU_Get_EDR(MPU_TypeDef *MPUx, uint8_t num)
{
    return ((uint32_t)(READ_REG(MPUx->SP[num].EDR)));
}


/**
 * @brief  LL_MPU_Set_SADDR
 * @param  None
 * @retval None
 */
__STATIC_INLINE void LL_MPU_Set_SADDR(MPU_TypeDef *MPUx, uint8_t num, uint32_t val)
{
    WRITE_REG(MPUx->REGION[num].SADDR, val);
}


/**
 * @brief  LL_MPU_Set_EADDR
 * @param  None
 * @retval None
 */
__STATIC_INLINE void LL_MPU_Set_EADDR(MPU_TypeDef *MPUx, uint8_t num, uint32_t val)
{
    WRITE_REG(MPUx->REGION[num].EADDR, val);
}


/**
 * @brief  LL_MPU_Set_AUTHA
 * @param  None
 * @retval None
 */
__STATIC_INLINE void LL_MPU_Set_AUTHA(MPU_TypeDef *MPUx, uint8_t num, uint32_t val)
{
    WRITE_REG(MPUx->REGION[num].EADDR, val);
}


/**
 * @brief  LL_MPU_Get_AUTHA
 * @param  None
 * @retval RegValue
 */
__STATIC_INLINE uint32_t LL_MPU_Get_AUTHA(MPU_TypeDef *MPUx, uint8_t num)
{
    return ((uint32_t)(READ_REG(MPUx->REGION[num].EADDR)));
}


/**
 * @brief  LL_MPU_Set_AUTHB
 * @param  None
 * @retval None
 */
__STATIC_INLINE void LL_MPU_Set_AUTHB(MPU_TypeDef *MPUx, uint8_t num, uint32_t val)
{
    WRITE_REG(MPUx->REGION[num].EADDR, val);
}


/**
 * @brief  LL_MPU_Get_AUTHB
 * @param  None
 * @retval RegValue
 */
__STATIC_INLINE uint32_t LL_MPU_Get_AUTHB(MPU_TypeDef *MPUx, uint8_t num)
{
    return ((uint32_t)(READ_REG(MPUx->REGION[num].EADDR)));
}


/**
 * @brief  LL_MPU_Set_AUTHC
 * @param  None
 * @retval None
 */
__STATIC_INLINE void LL_MPU_Set_AUTHC(MPU_TypeDef *MPUx, uint8_t num, uint32_t val)
{
    WRITE_REG(MPUx->REGION[num].EADDR, val);
}


/**
 * @brief  LL_MPU_Get_AUTHC
 * @param  None
 * @retval RegValue
 */
__STATIC_INLINE uint32_t LL_MPU_Get_AUTHC(MPU_TypeDef *MPUx, uint8_t num)
{
    return ((uint32_t)(READ_REG(MPUx->REGION[num].EADDR)));
}


/**
 * @brief  LL_MPU_Set_CTRL
 * @param  None
 * @retval None
 */
__STATIC_INLINE void LL_MPU_Set_CTRL(MPU_TypeDef *MPUx, uint8_t num, uint32_t val)
{
    WRITE_REG(MPUx->REGION[num].EADDR, val);
}


/**
 * @brief  LL_MPU_Get_CTRL
 * @param  None
 * @retval RegValue
 */
__STATIC_INLINE uint32_t LL_MPU_Get_CTRL(MPU_TypeDef *MPUx, uint8_t num)
{
    return ((uint32_t)(READ_REG(MPUx->REGION[num].EADDR)));
}


/**
 * @brief  LL_MPU_Set_SWCTL
 * @param  None
 * @retval None
 */
__STATIC_INLINE void LL_MPU_Set_SWCTL(MPU_TypeDef *MPUx, uint8_t num, uint32_t val)
{
    WRITE_REG(MPUx->REGION[num].EADDR, val);
}


/**
 * @brief  LL_MPU_Get_SWCTL
 * @param  None
 * @retval RegValue
 */
__STATIC_INLINE uint32_t LL_MPU_Get_SWCTL(MPU_TypeDef *MPUx, uint8_t num)
{
    return ((uint32_t)(READ_BIT(MPUx->REGION[num].EADDR, SPI_SPSR_RXWCOL) == SPI_SPSR_RXWCOL));
}


/**
 * @brief  MPU_Enable
 * @param  base MPUx
 * @param  enable TURE-MPU enable,FALSE-MPU disable
 * @retval None
 */
__STATIC_INLINE void MPU_Enable(MPU_TypeDef *base, uint8_t enable)
{
    if (enable) {
        /* Enable the MPU globally. */
        base->CSR |= MPU_CSR_EN;
    } else {
        /* Disable the MPU globally. */
        base->CSR &= ~MPU_CSR_EN;
    }
}


void LL_MPU_SetRegionConfig(MPU_TypeDef *base, LL_MPU_RegionTypeDef *regionConfig);


void LL_MPU_SetRegionAddr(MPU_TypeDef *base, uint32_t regionNum, uint32_t startAddr, uint32_t endAddr);


void LL_MPU_SetRegionAccessRights(MPU_TypeDef *base, LL_MPU_RightTypeDef *accessRights);


void LL_MPU_GetErrorAccessInfo(MPU_TypeDef *base, uint32_t slaveNum, LL_MPU_AccessErrTypeDef *errInform);


#ifdef __cplusplus
}
#endif
#endif
/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/

