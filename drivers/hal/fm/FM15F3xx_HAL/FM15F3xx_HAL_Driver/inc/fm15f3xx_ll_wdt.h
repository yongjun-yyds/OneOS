/**
 ******************************************************************************
 * @file    fm15f3xx_ll_wdt.h
 * @author  SRG
 * @version V1.0.0
 * @date    2020-03-17
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */

#ifndef FM15F3XX_LL_WDT_H
#define FM15F3XX_LL_WDT_H
#ifdef __cplusplus
extern "C" {
#endif

#include "fm15f3xx.h"


#if defined (WDT0) || defined (WDT1)


/** @defgroup WDT_LL WDT
 * @{
 */

/* Private types -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/


/** @defgroup WDT_LL_Private_Variables WDT Private Variables
 * @{
 */


/**
 * @}
 */
/* Private constants ---------------------------------------------------------*/
/* Private macros ------------------------------------------------------------*/


/** @defgroup WDT_LL_ES_INIT WDT ExWDTed Init structures
 * @{
 */


/**
 * @brief LL WDT Init Structure definition
 */


/**
 * @brief  WDT  configuration structure definition.
 */
//typedef struct
//{
//  uint32_t Period;
//} LL_WDT_InitTypeDef;


/**
 * @}
 */


/* ExWDTed constants --------------------------------------------------------*/


/** @defgroup WDT_LL_ExWDTed_Constants WDT ExWDTed Constants
 * @{
 */


/** @defgroup WDT_LL_EC_WDT_PERIOD Select period of watchdog
 * @{
 */
#define LL_WDT_PERIOD_8     (0x0U << WDT_CFG_PERIOD_Pos)
#define LL_WDT_PERIOD_12    (0x1U << WDT_CFG_PERIOD_Pos)
#define LL_WDT_PERIOD_15    (0x2U << WDT_CFG_PERIOD_Pos)
#define LL_WDT_PERIOD_18    (0x3U << WDT_CFG_PERIOD_Pos)
#define LL_WDT_PERIOD_21    (0x4U << WDT_CFG_PERIOD_Pos)
#define LL_WDT_PERIOD_24    (0x5U << WDT_CFG_PERIOD_Pos)
#define LL_WDT_PERIOD_27    (0x6U << WDT_CFG_PERIOD_Pos)
#define LL_WDT_PERIOD_30    (0x7U << WDT_CFG_PERIOD_Pos)


/**
 * @}
 */

/* ExWDTed macro ------------------------------------------------------------*/


/** @defgroup WDT_LL_ExWDTed_Macros WDT ExWDTed Macros
 * @{
 */


/**
 * @brief  Write a value in WDT register
 * @param  __INSTANCE__ WDT Instance
 * @param  __REG__ Register to be written
 * @param  __VALUE__ Value to be written in the register
 * @retval None
 */
#define LL_WDT_WriteReg( __INSTANCE__, __REG__, __VALUE__ ) WRITE_REG( __INSTANCE__->__REG__, (__VALUE__) )


/**
 * @brief  Read a value in WDT register
 * @param  __INSTANCE__ WDT Instance
 * @param  __REG__ Register to be read
 * @retval Register value
 */
#define LL_WDT_ReadReg( __INSTANCE__, __REG__ ) READ_REG( __INSTANCE__->__REG__ )


/**
 * @}
 */

/* ExWDTed functions --------------------------------------------------------*/


/** @defgroup WDT_LL_ExWDTed_Functions WDT ExWDTed Functions
 * @{
 */


/** @defgroup WDT_LL_EF_WDT_Configuration WDT Configuration
 * @{
 */


/**
 * @brief  Enable wdt counter.
 * @rmtoll WRCTRL       WRCTRL       LL_WDT_Enable
 * @rmtoll CFG          EN           LL_WDT_Enable
 * @param  WDTx Timer instance
 * @retval None
 */
__STATIC_INLINE void LL_WDT_Enable(WDT_TypeDef *WDTx)
{
    WRITE_REG(WDTx->WRCTRL, 0xAAAAAAAA);
    WRITE_REG(WDTx->WRCTRL, 0x55555555);   //frist write 0xAAAAAAAA,next write 0x55555555
    SET_BIT(WDTx->CFG, WDT_CFG_EN);
}


/**
 * @brief  Disable wdt counter.
 * @rmtoll CFG          EN           LL_WDT_Disable
 * @param  WDTx Timer instance
 * @retval None
 */
__STATIC_INLINE void LL_WDT_Disable(WDT_TypeDef *WDTx)
{
    WRITE_REG(WDTx->WRCTRL, 0xAAAAAAAA);
    WRITE_REG(WDTx->WRCTRL, 0x55555555);   //frist write 0xAAAAAAAA,next write 0x55555555
    CLEAR_BIT(WDTx->CFG, WDT_CFG_EN);
}


/**
 * @brief  Reset wdt counter.
 * @rmtoll WRCTRL       WRCTRL           LL_WDT_Reset
 * @rmtoll CFG          CNTRST           LL_WDT_Reset
 * @param  WDTx Timer instance
 * @retval None
 */
__STATIC_INLINE void LL_WDT_Reset(WDT_TypeDef *WDTx)
{
    WRITE_REG(WDTx->WRCTRL, 0xAAAAAAAA);
    WRITE_REG(WDTx->WRCTRL, 0x55555555);   //frist write 0xAAAAAAAA,next write 0x55555555
    SET_BIT(WDTx->CFG, WDT_CFG_CNTRST);
}


/**
 * @brief  Configure wdt period.
 * @rmtoll WRCTRL       WRCTRL           LL_WDT_PeriodConfig
 * @rmtoll CFG          PERIOD           LL_WDT_PeriodConfig
 * @param  WDTx Timer instance
 * @param  Period This parameter can be one of the following values:
 *         @arg @ref LL_WDT_PERIOD_8
 *         @arg @ref LL_WDT_PERIOD_12
 *         @arg @ref LL_WDT_PERIOD_15
 *         @arg @ref LL_WDT_PERIOD_18
 *         @arg @ref LL_WDT_PERIOD_21
 *         @arg @ref LL_WDT_PERIOD_24
 *         @arg @ref LL_WDT_PERIOD_27
 *         @arg @ref LL_WDT_PERIOD_30
 * @retval None
 */
__STATIC_INLINE void LL_WDT_SetPeriod(WDT_TypeDef *WDTx, uint32_t Period)
{
    WRITE_REG(WDTx->WRCTRL, 0xAAAAAAAA);
    WRITE_REG(WDTx->WRCTRL, 0x55555555);   //frist write 0xAAAAAAAA,next write 0x55555555
    MODIFY_REG(WDTx->CFG, WDT_CFG_PERIOD, Period);
}


/**
 * @brief  Get wdt counter value.
 * @rmtoll CNT          CNT           LL_WDT_GetCounter
 * @param  WDTx Timer instance
 * @retval None
 */
__STATIC_INLINE uint32_t LL_WDT_GetCurrentValue(WDT_TypeDef *WDTx)
{
    return ((uint32_t)(READ_REG(WDTx->CNT)));
}


/* Exported functions --------------------------------------------------------*/
void LL_WDT_Init(WDT_TypeDef *WDTx, uint32_t Period);


void LL_WDT_Start(WDT_TypeDef *WDTx);


void LL_WDT_FeedDog(WDT_TypeDef *WDTx);


void LL_WDT_EnableReset(void);


void LL_WDT_EnableInt(void);


#endif /* defined (WDT0 | WDT1) */


/**
 * @}
 */

#ifdef __cplusplus
}
#endif
#endif
/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
