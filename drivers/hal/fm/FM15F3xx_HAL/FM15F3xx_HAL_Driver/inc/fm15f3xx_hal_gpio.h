/**
 ******************************************************************************
 * @file    fm15f3xx_hal_gpio.h
 * @author  WYL
 * @brief   Header file of GPIO HAL module.
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 ******************************************************************************
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __FM15F3xx_HAL_GPIO_H
#define __FM15F3xx_HAL_GPIO_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "fm15f3xx_hal_def.h"
#include "fm15f3xx_ll_gpio.h"


/** @addtogroup FM15F3xx_HAL_Driver
 * @{
 */


/** @addtogroup GPIO
 * @{
 */

/* Exported types ------------------------------------------------------------*/


/** @defgroup GPIO_Exported_Types GPIO Exported Types
 * @{
 */


/**
 * @brief  GPIO Init structure definition
 */
#define GPIO_InitTypeDef LL_GPIO_InitTypeDef


/**
 * @}
 */


/* Exported constants --------------------------------------------------------*/


/** @defgroup GPIO_Exported_Constants GPIO Exported Constants
 * @{
 */


/** @defgroup GPIO_pins_define GPIO pins define
 * @{
 */
#define GPIO_PIN_MASK 0x0000FFFFU                   /* PIN mask for assert test */


/**
 * @}
 */


/**
 * @brief   AF 0 selection
 */
#define GPIO_AF0_HIZ    LL_GPIO_ALT_0               /* HIZ Alternate Function mapping */
#define GPIO_AF0_ADC    LL_GPIO_ALT_0               /* ADC Alternate Function mapping */
#define GPIO_AF0_CMP    LL_GPIO_ALT_0               /* CMP Alternate Function mapping */


/**
 * @brief   AF 1 selection
 */
#define GPIO_AF1_GPIO LL_GPIO_ALT_1                 /* GPIO Alternate Function mapping */


/**
 * @brief   AF 2 selection
 */
#define GPIO_AF2_SPI0   LL_GPIO_ALT_2               /* SPI0 Alternate Function mapping */
#define GPIO_AF2_SPI1   LL_GPIO_ALT_2               /* SPI1 Alternate Function mapping */
#define GPIO_AF2_SPI2   LL_GPIO_ALT_2               /* SPI2 Alternate Function mapping */
#define GPIO_AF2_SPI3   LL_GPIO_ALT_2               /* SPI3 Alternate Function mapping */
#define GPIO_AF2_QSPI   LL_GPIO_ALT_2               /* QSPI Alternate Function mapping */


/**
 * @brief   AF 3 selection
 */
#define GPIO_AF3_UART0  LL_GPIO_ALT_3               /* UART0 Alternate Function mapping */
#define GPIO_AF3_UART1  LL_GPIO_ALT_3               /* UART1 Alternate Function mapping */
#define GPIO_AF3_UART2  LL_GPIO_ALT_3               /* UART2 Alternate Function mapping */
#define GPIO_AF3_QSPI   LL_GPIO_ALT_3               /* QSPI Alternate Function mapping */
#define GPIO_AF3_SWJ    LL_GPIO_ALT_3               /* SWJ (SWD and JTAG) Alternate Function mapping */
#define GPIO_AF3_CT1    LL_GPIO_ALT_3               /* CT1 Alternate Function mapping */


/**
 * @brief   AF 4 selection
 */
#define GPIO_AF4_I2C0   LL_GPIO_ALT_4               /* I2C0 Alternate Function mapping */
#define GPIO_AF4_I2C1   LL_GPIO_ALT_4               /* I2C1 Alternate Function mapping */
#define GPIO_AF4_STIM5  LL_GPIO_ALT_4               /* TIM5 Alternate Function mapping */
#define GPIO_AF4_MCO    LL_GPIO_ALT_4               /* MCO (MCO1 and MCO2) Alternate Function mapping        */
#define GPIO_AF4_SWJ    LL_GPIO_ALT_4               /* SWJ (SWD and JTAG)  Alternate Function mapping */
#define GPIO_AF4_NMI    LL_GPIO_ALT_4               /* NMI Alternate Function mapping */
#define GPIO_AF4_TRACE  LL_GPIO_ALT_4               /* TRACE Alternate Function mapping */
#define GPIO_AF4_ADC    LL_GPIO_ALT_4               /* ADC Alternate Function mapping */
#define GPIO_AF4_DAC    LL_GPIO_ALT_4               /* DAC Alternate Function mapping */
#define GPIO_AF4_CMP    LL_GPIO_ALT_4               /* CMP Alternate Function mapping */
#define GPIO_AF4_CT0    LL_GPIO_ALT_4               /* CT0 Alternate Function mapping */


/**
 * @brief   AF 5 selection
 */
#define GPIO_AF5_CT0    LL_GPIO_ALT_5               /* CT0 Alternate Function mapping        */
#define GPIO_AF5_CT1    LL_GPIO_ALT_5               /* CT0 Alternate Function mapping        */
#define GPIO_AF5_QSPI   LL_GPIO_ALT_5               /* QSPI Alternate Function mapping        */
#define GPIO_AF5_MCO    LL_GPIO_ALT_5               /* MCO (MCO1 and MCO2) Alternate Function mapping        */
#define GPIO_AF5_ADC    LL_GPIO_ALT_5               /* ADC Alternate Function mapping        */
#define GPIO_AF5_DAC    LL_GPIO_ALT_5               /* DAC Alternate Function mapping        */
#define GPIO_AF5_CMP    LL_GPIO_ALT_5               /* CMP Alternate Function mapping        */


/**
 * @brief   AF 6 selection
 */
#define GPIO_AF6_DCMI   LL_GPIO_ALT_6               /* DCMI Alternate Function mapping  */
#define GPIO_AF6_STIM0  LL_GPIO_ALT_6               /* STIM0 Alternate Function mapping */
#define GPIO_AF6_STIM1  LL_GPIO_ALT_6               /* STIM1 Alternate Function mapping */
#define GPIO_AF6_STIM2  LL_GPIO_ALT_6               /* STIM2 Alternate Function mapping */
#define GPIO_AF6_STIM3  LL_GPIO_ALT_6               /* STIM4 Alternate Function mapping */
#define GPIO_AF6_STIM4  LL_GPIO_ALT_6               /* STIM5 Alternate Function mapping */
#define GPIO_AF6_STIM5  LL_GPIO_ALT_6               /* STIM6 Alternate Function mapping */


/**
 * @brief   AF 7 selection
 */
#define GPIO_AF7_FSMC LL_GPIO_ALT_7                 /* USART1 Alternate Function mapping     */


/** @defgroup GPIO Direction data direction
 * @{
 */
#define GPIO_DIRECTION_IN   LL_GPIO_DIRECTION_IN    /*!< data input */
#define GPIO_DIRECTION_OUT  LL_GPIO_DIRECTION_OUT   /*!< data output */


/**
 * @}
 */


/** @defgroup  GPIO Slew speed define
 * @brief GPIO Slew speed
 * @{
 */
#define GPIO_SLEW_SPEED_LOW     LL_GPIO_SLEWRATE_LOW
#define GPIO_SLEW_SPEED_HIGH    LL_GPIO_SLEWRATE_HIGH


/**
 * @}
 */


/** @defgroup GPIO OUTPUT Output Type
 * @{
 */
#define GPIO_OUTPUT_NOOPENDRAIN LL_GPIO_OUTPUT_NOOPENDRAIN  /*!< Disable open-drain */
#define GPIO_OUTPUT_OPENDRAIN   LL_GPIO_OUTPUT_OPENDRAIN    /*!< Select open-drain as output type */


/**
 * @}
 */


/** @defgroup GPIO DIGITFILTE digit filter enable
 * @{
 */
#define GPIO_DIGITFILTE_OFF LL_GPIO_DIGITFILTE_NO           /*!< Disable passive filter enable */
#define GPIO_DIGITFILTE_ON  LL_GPIO_DIGITFILTE_EN           /*!< Enable passive filter enable */


/**
 * @}
 */


/** @defgroup GPIO_pull_define GPIO pull define
 * @brief GPIO Pull-Up or Pull-Down Activation
 * @{
 */
#define GPIO_NOPULL     0x00000000U                         /*!< No Pull-up or Pull-down activation  */
#define GPIO_PULLUP     (GPIO_CFG_PCR_PE | GPIO_CFG_PCR_PS) /*!< Pull-up activation                  */
#define GPIO_PULLDOWN   GPIO_CFG_PCR_PE                     /*!< Pull-down activation                */


/**
 * @}
 */


/** @defgroup GPIO DRIVES Output Type
 * @{
 */
#define GPIO_DRIVES_WEEK    LL_GPIO_DRIVES_WEEK             /*!< Week drive strength */
#define GPIO_DRIVES_STRONG  LL_GPIO_DRIVES_STRONG           /*!< Strong drive */


/**
 * @}
 */


/** @defgroup GPIO Lock Register
 * @{
 */
#define GPIO_UNLOCK LL_GPIO_LK_UNLOCK                       /*!< Pin control register not lock */
#define GPIO_LOCK   LL_GPIO_LK_LOCK                         /*!< Pin control register locked */


/**
 * @}
 */


/** @defgroup GPIO IRQ Interrupt Configuration
 * @{
 */
#define GPIO_IRQorDMA_DISABLE   LL_GPIO_INTorDMA_DISABLE    /*!< Diable interrupt or DMA */
#define GPIO_DMA_RISE           LL_GPIO_DMAonRISE           /*!< DMA request on rising edge */
#define GPIO_DMA_FALL           LL_GPIO_DMAonFALL           /*!< DMA request on falling edge */
#define GPIO_DMA_BOTH           LL_GPIO_DMAonBOTHEDGE       /*!< DMA request on either edge */
#define GPIO_IRQ_ZERO           LL_GPIO_INTonZERO           /*!< Interrupt when logic zero */
#define GPIO_IRQ_RISE           LL_GPIO_INTonRISE           /*!< Interrupt on rising edge */
#define GPIO_IRQ_FALL           LL_GPIO_INTonFALL           /*!< Interrupt on falling edge */
#define GPIO_IRQ_BOTH           LL_GPIO_INTonBOTHEDGE       /*!< Interrupt on either edge */
#define GPIO_IRQ_ONE            LL_GPIO_INTonHIGH           /*!< Interrupt when logic one */


/**
 * @}
 */


/** @defgroup GPIO_LL_EC_WKUP Alternate Function
 * @{
 */
#define GPIO_WKUP_CLOSED    LL_GPIO_WKUP_CLOSED             /*!< Wakeup closed */
#define GPIO_WKUP_RISE      LL_GPIO_WKUP_RISE               /*!< Wakeup rising edge */
#define GPIO_WKUP_FALL      LL_GPIO_WKUP_FALL               /*!< Wakeup falling edge */
#define GPIO_WKUP_BOTH      LL_GPIO_WKUP_BOTH               /*!< Wakeup both edge */


/**
 * @}
 */


/**
 * @}
 */

/* Exported macro ------------------------------------------------------------*/


/** @defgroup GPIO_Exported_Macros GPIO Exported Macros
 * @{
 */

/* Exported functions --------------------------------------------------------*/


/** @addtogroup GPIO_Exported_Functions
 * @{
 */


/** @addtogroup GPIO_Exported_Functions_Group1
 * @{
 */
/* Initialization and de-initialization functions *****************************/
void HAL_GPIO_Init(GPIO_TypeDef  *GPIOx, GPIO_InitTypeDef *GPIO_Init);


void HAL_GPIO_DeInit(GPIO_TypeDef  *GPIOx, uint32_t GPIO_Pin);


/**
 * @}
 */


/** @addtogroup GPIO_Exported_Functions_Group2
 * @{
 */
/* IO operation functions *****************************************************/
GPIO_PinState HAL_GPIO_ReadPin(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin);


void HAL_GPIO_WritePin(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin, GPIO_PinState PinState);


void HAL_GPIO_TogglePin(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin);


void HAL_GPIO_IRQHandler(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin);


void HAL_GPIO_IRQCallback(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin);


/**
 * @}
 */


/**
 * @}
 */
/* Private types -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private constants ---------------------------------------------------------*/


/** @defgroup GPIO_Private_Constants GPIO Private Constants
 * @{
 */


/**
 * @}
 */

/* Private macros ------------------------------------------------------------*/


/** @defgroup GPIO_Private_Macros GPIO Private Macros
 * @{
 */
#define IS_GPIO_ALL_INSTANCE( INSTANCE )    ( ( (INSTANCE) == GPIOA) || \
                                              ( (INSTANCE) == GPIOB) || \
                                              ( (INSTANCE) == GPIOC) || \
                                              ( (INSTANCE) == GPIOD) || \
                                              ( (INSTANCE) == GPIOE) || \
                                              ( (INSTANCE) == GPIOF) )
#define IS_GPIO_AF( AF )                    ( ( (AF) == GPIO_AF0_HIZ) || ( (AF) == GPIO_AF0_ADC) || \
                                              ( (AF) == GPIO_AF0_CMP) || ( (AF) == GPIO_AF1_GPIO) || \
                                              ( (AF) == GPIO_AF2_SPI0) || ( (AF) == GPIO_AF2_SPI1) || \
                                              ( (AF) == GPIO_AF2_SPI2) || ( (AF) == GPIO_AF2_SPI3) || \
                                              ( (AF) == GPIO_AF2_QSPI) || ( (AF) == GPIO_AF3_UART0) || \
                                              ( (AF) == GPIO_AF3_UART1) || ( (AF) == GPIO_AF3_UART2) || \
                                              ( (AF) == GPIO_AF3_QSPI) || ( (AF) == GPIO_AF3_SWJ) || \
                                              ( (AF) == GPIO_AF3_CT1) || ( (AF) == GPIO_AF4_I2C0) || \
                                              ( (AF) == GPIO_AF4_I2C1) || ( (AF) == GPIO_AF4_STIM5) || \
                                              ( (AF) == GPIO_AF4_MCO) || ( (AF) == GPIO_AF4_SWJ) || \
                                              ( (AF) == GPIO_AF4_NMI) || ( (AF) == GPIO_AF4_TRACE) || \
                                              ( (AF) == GPIO_AF4_ADC) || ( (AF) == GPIO_AF4_DAC) || \
                                              ( (AF) == GPIO_AF4_CMP) || ( (AF) == GPIO_AF4_CT0) || \
                                              ( (AF) == GPIO_AF5_CT0) || ( (AF) == GPIO_AF5_CT1) || \
                                              ( (AF) == GPIO_AF5_QSPI) || ( (AF) == GPIO_AF5_MCO) || \
                                              ( (AF) == GPIO_AF5_ADC) || ( (AF) == GPIO_AF5_DAC) || \
                                              ( (AF) == GPIO_AF5_CMP) || ( (AF) == GPIO_AF6_DCMI) || \
                                              ( (AF) == GPIO_AF6_STIM0) || ( (AF) == GPIO_AF6_STIM1) || \
                                              ( (AF) == GPIO_AF6_STIM2) || ( (AF) == GPIO_AF6_STIM3) || \
                                              ( (AF) == GPIO_AF6_STIM4) || ( (AF) == GPIO_AF6_STIM5) || \
                                              ( (AF) == GPIO_AF7_FSMC) )
#define IS_GPIO_PIN_ACTION( ACTION )        ( ( (ACTION) == GPIO_PIN_RESET) || ( (ACTION) == GPIO_PIN_SET) )
#define IS_GPIO_DIRECTION( DIR )            ( ( (DIR) == GPIO_DIRECTION_IN) || ( (DIR) == GPIO_DIRECTION_OUT) )
#define IS_GPIO_PIN( PIN )                  ( ( ( (PIN) &GPIO_PIN_MASK) != 0x00U) && ( ( (PIN) &~GPIO_PIN_MASK) == 0x00U) )
#define IS_GPIO_SLEWSPEED( SPEED )          ( ( (SPEED) == GPIO_SLEW_SPEED_LOW) || ( (SPEED) == GPIO_SLEW_SPEED_HIGH) )
#define IS_GPIO_OUTPUT( MODE )              ( ( (MODE) == GPIO_OUTPUT_NOOPENDRAIN) || ( (MODE) == GPIO_OUTPUT_OPENDRAIN) )
#define IS_GPIO_PULL( PULL )                ( ( (PULL) == GPIO_NOPULL) || \
                                              ( (PULL) == GPIO_PULLUP) || \
                                              ( (PULL) == GPIO_PULLDOWN) )
#define IS_GPIO_DIGITFILT( CGF )            ( ( (CGF) == GPIO_DIGITFILTE_ON) || ( (CGF) == GPIO_DIGITFILTE_OFF) )
#define IS_GPIO_DRIVE( CGF )                ( ( (CGF) == GPIO_DRIVES_WEEK) || ( (CGF) == GPIO_DRIVES_STRONG) )
#define IS_GPIO_LOCK( CGF )                 ( ( (CGF) == GPIO_UNLOCK) || ( (CGF) == GPIO_LOCK) )
#define IS_GPIO_IRQorDMA( CFG )             ( ( (CFG) == GPIO_IRQorDMA_DISABLE) || \
                                              ( (CFG) == GPIO_DMA_RISE) || \
                                              ( (CFG) == GPIO_DMA_FALL) || \
                                              ( (CFG) == GPIO_DMA_BOTH) || \
                                              ( (CFG) == GPIO_IRQ_ZERO) || \
                                              ( (CFG) == GPIO_IRQ_RISE) || \
                                              ( (CFG) == GPIO_IRQ_FALL) || \
                                              ( (CFG) == GPIO_IRQ_BOTH) || \
                                              ( (CFG) == GPIO_IRQ_ONE) )
#define IS_GPIO_WAKEUP( CFG )               ( ( (CFG) == GPIO_WKUP_CLOSED) || \
                                              ( (CFG) == GPIO_WKUP_FALL) || \
                                              ( (CFG) == GPIO_WKUP_RISE) || \
                                              ( (CFG) == GPIO_WKUP_BOTH) )


/**
 * @}
 */

/* Private functions ---------------------------------------------------------*/


/** @defgroup GPIO_Private_Functions GPIO Private Functions
 * @{
 */


/**
 * @}
 */


/**
 * @}
 */


/**
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif /* __FM15F3xx_HAL_GPIO_H */

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
