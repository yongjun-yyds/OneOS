/**
 ******************************************************************************
 * @file    fm15f3xx_hal_i2c.h
 * @author  MCD Application Team
 * @brief   Header file of I2C HAL module.
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __FM15F3xx_HAL_I2C_H
#define __FM15F3xx_HAL_I2C_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "fm15f3xx_hal_def.h"
#include "fm15f3xx_ll_i2c.h"


/** @addtogroup FM15F3xx_HAL_Driver
 * @{
 */


/** @addtogroup I2C
 * @{
 */

/* Exported types ------------------------------------------------------------*/


/** @defgroup I2C_Exported_Types I2C Exported Types
 * @{
 */


/**
 * @brief  I2C Configuration Structure definition
 */
typedef struct {
    uint32_t Mode;                              /*!< Specifies the I2C mode is master or slave*/

    uint32_t ClockSpeed;                        /*!< Specifies the clock frequency.*/

    uint32_t ExtAddress;                        /*!< Specifies if 7-bit or 10-bit addressing in master mode is selected. */

    uint32_t ExtAddrSize;                       /*!< Specifies if 7-bit or 10-bit addressing Size in master mode is selected. */

    uint32_t OwnAddress;                        /*!< Specifies the device own address .*/

    uint32_t OwnAddrSize;                       /*!< Specifies the device own address 1 size (7-bit or 10-bit)*/

    uint32_t StretchEn;                         /*!< clock stretching enable */
} I2C_InitTypeDef;


/**
 * @brief  HAL State structure definition
 * @note  HAL I2C State value coding follow below described bitmap :
 *          b7-b6  Error information
 *             00 : No Error
 *             01 : Abort (Abort user request on going)
 *             10 : Timeout
 *             11 : Error
 *          b5     IP initilisation status
 *             0  : Reset (IP not initialized)
 *             1  : Init done (IP initialized and ready to use. HAL I2C Init function called)
 *          b4     (not used)
 *             x  : Should be set to 0
 *          b3
 *             0  : Ready or Busy (No Listen mode ongoing)
 *             1  : Listen (IP in Address Listen Mode)
 *          b2     Intrinsic process state
 *             0  : Ready
 *             1  : Busy (IP busy with some configuration or internal operations)
 *          b1     Rx state
 *             0  : Ready (no Rx operation ongoing)
 *             1  : Busy (Rx operation ongoing)
 *          b0     Tx state
 *             0  : Ready (no Tx operation ongoing)
 *             1  : Busy (Tx operation ongoing)
 */
typedef enum {
    HAL_I2C_STATE_RESET             = 0x00U,    /*!< Peripheral is not yet Initialized         */
    HAL_I2C_STATE_READY             = 0x20U,    /*!< Peripheral Initialized and ready for use  */
    HAL_I2C_STATE_BUSY              = 0x24U,    /*!< An internal process is ongoing            */
    HAL_I2C_STATE_BUSY_TX           = 0x21U,    /*!< Data Transmission process is ongoing      */
    HAL_I2C_STATE_BUSY_RX           = 0x22U,    /*!< Data Reception process is ongoing         */
    HAL_I2C_STATE_LISTEN            = 0x28U,    /*!< Address Listen Mode is ongoing            */
    HAL_I2C_STATE_BUSY_TX_LISTEN    = 0x29U,    /*!< Address Listen Mode and Data Transmission
                                                    process is ongoing                         */
    HAL_I2C_STATE_BUSY_RX_LISTEN    = 0x2AU,    /*!< Address Listen Mode and Data Reception
                                                    process is ongoing                         */
    HAL_I2C_STATE_ABORT             = 0x60U,    /*!< Abort user request ongoing                */
    HAL_I2C_STATE_TIMEOUT           = 0xA0U,    /*!< Timeout state                             */
    HAL_I2C_STATE_ERROR             = 0xE0U     /*!< Error                                     */
} HAL_I2C_StateTypeDef;


/**
 * @brief  I2C handle Structure definition
 */
typedef struct {
    I2C_TypeDef *Instance;                      /*!< I2C registers base address               */

    I2C_InitTypeDef Init;                       /*!< I2C communication parameters             */

//  DMA_HandleTypeDef          *hdmatx;        /*!< I2C Tx DMA handle parameters             */

//  DMA_HandleTypeDef          *hdmarx;        /*!< I2C Rx DMA handle parameters             */

    __IO HAL_I2C_StateTypeDef State;            /*!< I2C communication state                  */

    __IO uint32_t ErrorCode;                    /*!< I2C Error code                           */
} I2C_HandleTypeDef;


/**
 * @}
 */

/* Exported constants --------------------------------------------------------*/


/** @defgroup I2C_Exported_Constants I2C Exported Constants
 * @{
 */


/** @defgroup I2C_Error_Code I2C Error Code
 * @brief    I2C Error Code
 * @{
 */
#define HAL_I2C_ERROR_NONE      0x00000000U             /*!< No error           */
#define HAL_I2C_ERROR_BERR      0x00000001U             /*!< BERR error         */
#define HAL_I2C_ERROR_ARLO      0x00000002U             /*!< ARLO error         */
#define HAL_I2C_ERROR_AF        0x00000004U             /*!< AF error           */
#define HAL_I2C_ERROR_OVR       0x00000008U             /*!< OVR error          */
#define HAL_I2C_ERROR_DMA       0x00000010U             /*!< DMA transfer error */
#define HAL_I2C_ERROR_TIMEOUT   0x00000020U             /*!< Timeout Error      */


/**
 * @}
 */


/** @defgroup I2C_Mode  I2C mode
 * @{
 */
#define I2C_MODE_SLAVE  LL_I2C_SLAVE_MODE
#define I2C_MODE_MASTER LL_I2C_MASTER_MODE


/**
 * @}
 */


/** @defgroup I2C_addressing_mode I2C addressing mode
 * @{
 */
#define I2C_OWN_ADDRESS_7BIT    LL_I2C_SLAVESSPCON_A7
#define I2C_OWN_ADDRESS_10BIT   LL_I2C_SLAVESSPCON_A10


/**
 * @}
 */


/** @defgroup I2C_addressing_mode I2C addressing mode
 * @{
 */
#define I2C_ADDRESS_7BIT    (0x0U)
#define I2C_ADDRESS_10BIT   (0x1U)


/**
 * @}
 */


/** @defgroup I2C_general_call_addressing_mode I2C general call addressing mode
 * @{
 */
#define I2C_MASTER_DISABLE  LL_I2C_MASTER_DISABLE
#define I2C_MASTER_ENABLE   LL_I2C_MASTER_ENABLE


/**
 * @}
 */


/** @defgroup I2C_MASTERSSPCON_STARTEN config start bit as enable or disable
 * @{
 */
#define I2C_MASTER_START    LL_I2C_MASTERSSPCON_STARTEN                     /*!< enable generate START bit   */
#define I2C_MASTER_RESTART  LL_I2C_MASTERSSPCON_RESTARTEN                   /*!< enable generate RESTART bit */
#define I2C_MASTER_STOP     LL_I2C_MASTERSSPCON_STOPEN                      /*!< enable generate STOP bit    */
#define I2C_MASTER_RCVEN    LL_I2C_MASTERSSPCON_RCVEN                       /*!< enable master receive       */
#define I2C_MASTER_ACK      LL_I2C_MASTERSSPCON_ACK                         /*!< enable ACK bit              */


/**
 * @}
 */


/** @defgroup I2C_MASTERSSPCON_STARTEN config start bit as enable or disable
 * @{
 */
#define I2C_MASTERS_RECEIVE_ENABLE LL_I2C_MASTERSSPCON_RCVEN                /*!< enable master receive  */


/**
 * @}
 */


/** @defgroup I2C_stretch_mode I2C stretch mode
 * @{
 */
#define I2C_SLAVE_STRETCH_DISABLE   LL_I2C_SLAVESSPCON_NCKS
#define I2C_SLAVE_STRETCH_ENABLE    LL_I2C_SLAVESSPCON_CKSEN


/**
 * @}
 */


/** @defgroup I2C_XferDirection_definition I2C XferDirection definition
 * @{
 */
#define I2C_DIRECTION_RECEIVE   0x00000000U
#define I2C_DIRECTION_TRANSMIT  0x00000001U


/**
 * @}
 */


/** @defgroup I2C_Interrupt_configuration_definition I2C Interrupt configuration definition
 * @{
 */
#define I2C_MASTER_INT_FLAG     (0x100 | LL_I2C_MASTERSSPIR_IF)         /*!< set interrupt flag */
#define I2C_MASTER_INT_ENABLE   (0x100 | LL_I2C_MASTERSSPIR_IE)         /*!< interrupt enable   */
#define I2C_MASTER_INT_ALL      (0x100 | 0xFFU)                         /*!< interrupt enable   */

#define I2C_SLAVE_INT_FLAG      LL_I2C_SLAVESSPIR_IF                    /*!< set interrupt flag */
#define I2C_SLAVE_INT_ENABLE    LL_I2C_SLAVESSPIR_IE                    /*!< interrupt enable   */
#define I2C_SLAVE_INT_ALL       (0xFFU)                                 /*!< interrupt enable   */

#define I2C_INT_MASK (0x00000FFU)


/**
 * @}
 */


/** @defgroup I2C_Flag_definition I2C Flag definition
 * @{
 */
#define I2C_MASTER_SEND_ACK LL_I2C_FLAG_MASTERNACKDATA

#define I2C_MASTER_FLAG_START   (0x00010000 | LL_I2C_FLAG_MASTERSTART)
#define I2C_MASTER_FLAG_STOP    (0x00010000 | LL_I2C_FLAG_MASTERSTOP)
#define I2C_MASTER_FLAG_BUSY    (0x00010000 | LL_I2C_FLAG_MASTERBUSY)
#define I2C_MASTER_FLAG_WCOL    (0x00010000 | LL_I2C_FLAG_MASTERWCOL)
#define I2C_MASTER_FLAG_FULL    (0x00010000 | LL_I2C_FLAG_MASTERBUFFULL)
#define I2C_MASTER_FLAG_NACK    (0x00010000 | LL_I2C_FLAG_MASTERNACK)
#define I2C_MASTER_FLAG_ALL     (0x00010000 | 0x83U)


#define I2C_SLAVE_FLAG_START    LL_I2C_FLAG_SLAVESTART
#define I2C_SLAVE_FLAG_STOP     LL_I2C_FLAG_SLAVESTOP
#define I2C_SLAVE_FLAG_WCOL     LL_I2C_FLAG_SLAVEWCOL
#define I2C_SLAVE_FLAG_OV       LL_I2C_FLAG_SLAVEOV
#define I2C_SLAVE_FLAG_WRITE    LL_I2C_FLAG_SLAVEWRITE
#define I2C_SLAVE_FLAG_DorA     LL_I2C_FLAG_SLAVEDA
#define I2C_SLAVE_FLAG_FULL     LL_I2C_FLAG_SLAVEBF
#define I2C_SLAVE_FLAG_ALL      (0x78U)

#define I2C_FLAG_MASK (0x000000FFU)


/**
 * @}
 */


/**
 * @}
 */

/* Exported macro ------------------------------------------------------------*/


/** @defgroup I2C_Exported_Macros I2C Exported Macros
 * @{
 */


/** @brief Reset I2C handle state
 * @param  __HANDLE__ specifies the I2C Handle.
 *         This parameter can be I2C where x: 0 or 1 to select the I2C peripheral.
 * @retval None
 */
#define __HAL_I2C_RESET_HANDLE_STATE( __HANDLE__ ) ( (__HANDLE__)->State = HAL_I2C_STATE_RESET)


/** @brief  Enable or disable the specified I2C interrupts.
 * @param  __HANDLE__ specifies the I2C Handle.
 *         This parameter can be I2C where x: 0 or 1 to select the I2C peripheral.
 * @param  __INTERRUPT__ specifies the interrupt source to enable or disable.
 * @retval None
 */
#define __HAL_I2C_ENABLE_IT( __HANDLE__, __INTERRUPT__ ) ( ( ( (uint8_t) ( (__INTERRUPT__) >> 8U) ) == 0x01U) ? ( ( (__HANDLE__)->Instance->MSSPIR) |= ( (__INTERRUPT__) &I2C_INT_MASK) ) : \
                                                           ( ( (__HANDLE__)->Instance->SSSPIR) |= ( (__INTERRUPT__) &I2C_INT_MASK) ) )

#define __HAL_I2C_CLEAR_IT( __HANDLE__, __INTERRUPT__ ) ( ( ( (uint8_t) ( (__INTERRUPT__) >> 8U) ) == 0x01U) ? ( ( (__HANDLE__)->Instance->MSSPIR) &= ~( (__INTERRUPT__) &I2C_INT_MASK) ) : \
                                                          ( ( (__HANDLE__)->Instance->SSSPIR) &= ~( (__INTERRUPT__) &I2C_INT_MASK) ) )


/** @brief  Checks Whether the specified I2C flag is set or not.
 * @param  __HANDLE__ specifies the I2C Handle.
 *         This parameter can be I2C where x: 0 or 1 to select the I2C peripheral.
 * @param  __FLAG__ specifies the flag to check.
 * @retval The new state of __FLAG__ (TRUE or FALSE).
 */
#define __HAL_I2C_GET_FLAG( __STATUS__, __FLAG__ ) ( ( ( (uint8_t) ( (__FLAG__) >> 16U) ) == 0x01U) ? ( ( (__STATUS__) &( (__FLAG__) &I2C_FLAG_MASK) ) == ( (__FLAG__) &I2C_FLAG_MASK) ) : \
                                                     ( ( (__STATUS__) &( (__FLAG__) &I2C_FLAG_MASK) ) == ( (__FLAG__) &I2C_FLAG_MASK) ) )


/** @brief  Clears the I2C pending flags which are cleared by writing 0 in a specific bit.
 * @param  __HANDLE__ specifies the I2C Handle.
 *         This parameter can be I2C where x: 0 or 1 to select the I2C peripheral.
 * @param  __FLAG__ specifies the flag to clear.
 * @retval None
 */
#define __HAL_I2C_CLEAR_FLAG( __HANDLE__, __FLAG__ ) ( ( ( (uint8_t) ( (__FLAG__) >> 16U) ) == 0x01U) ? ( ( (__HANDLE__)->Instance->MSSPSTAT) = ~( (__FLAG__) &I2C_FLAG_MASK) ) : \
                                                       ( ( (__HANDLE__)->Instance->SSSPSTAT) = ~(__FLAG__) &I2C_FLAG_MASK) )


/** @brief  Checks whether the specified I2C flag is set or not.
 * @param  __HANDLE__ specifies the I2C Handle.
 *         This parameter can be I2C where x: 0 or 1 to select the I2C peripheral.
 * @param  __FLAG__ specifies the flag to check.
 * @retval The new state of __FLAG__ (TRUE or FALSE).
 */
#define __HAL_I2C_GET_IT_FLAG( __HANDLE__, __FLAG__ ) ( ( ( (uint8_t) ( (__FLAG__) >> 8U) ) == 0x01U) ? ( ( ( (__HANDLE__)->Instance->MSSPIR) & ( (__FLAG__) & 0x01) ) == ( (__FLAG__) & 0x01) ) : \
                                                        ( ( ( (__HANDLE__)->Instance->SSSPIR) & ( (__FLAG__) & 0x01) ) == ( (__FLAG__) & 0x01) ) )


/** @brief  Clears the I2C pending flags which are cleared by writing 0 in a specific bit.
 * @param  __HANDLE__ specifies the I2C Handle.
 *         This parameter can be I2C where x: 0 or 1 to select the I2C peripheral.
 * @param  __FLAG__ specifies the flag to clear.
 * @retval None
 */
#define __HAL_I2C_CLEAR_IT_FLAG( __HANDLE__, __FLAG__ ) ( ( ( (uint8_t) ( (__FLAG__) >> 8U) ) == 0x01U) ? ( ( (__HANDLE__)->Instance->MSSPIR) = ~( (__FLAG__) & 0x01) : \
                                                                                                                                                ( ( (__HANDLE__)->Instance->SSSPIR) = ~(__FLAG__) & 0x01) )


/** @brief  Disable the I2C peripheral.
 * @param  __HANDLE__ specifies the I2C Handle.
 *         This parameter can be I2Cx where x: 0 or 1 to select the I2C peripheral.
 * @retval None
 */
#define __HAL_I2C_DISABLE( __HANDLE__ ) ( (__HANDLE__)->Instance->MEN &= ~(I2C_MASTER_EN | I2C_MODE) )


/** @brief  Enable the I2C peripheral Master.
 * @param  __HANDLE__ specifies the I2C Handle.
 *         This parameter can be I2Cx where x: 0 or 1 to select the I2C peripheral.
 * @retval None
 */
#define __HAL_I2C_MASTER_ENABLE( __HANDLE__ ) ( (__HANDLE__)->Instance->MEN |= I2C_MASTER_EN | I2C_MODE)


/** @brief  Enable the I2C peripheral Slave.
 * @param  __HANDLE__ specifies the I2C Handle.
 *         This parameter can be I2Cx where x: 0 or 1 to select the I2C peripheral.
 * @retval None
 */
#define __HAL_I2C_SLAVE_ENABLE( __HANDLE__ ) ( (__HANDLE__)->Instance->MEN &= ~(I2C_MASTER_EN | I2C_MODE) )


/** @brief  the I2C peripheral send ack.
 * @param  __HANDLE__ specifies the I2C Handle.
 *         This parameter can be I2Cx where x: 0 or 1  to select the I2C peripheral.
 * @retval None
 */
#define __HAL_I2C_MASTER_SEND_ACK( __HANDLE__ ) (__HANDLE__)->Instance->MSSPSTAT    &= ~(I2C_MASTERSSPSTAUS_ACKDT); \
    (__HANDLE__)->Instance->MSSPCON                                                 |= I2C_MASTERSSPC0N_ACKEN

#define __HAL_I2C_SLAVE_SEND_ACK( __HANDLE__ ) (__HANDLE__)->Instance->SSSPCON |= I2C_SLAVESSPC0N_ACKEN


/** @brief  the I2C peripheral no ack.
 * @param  __HANDLE__ specifies the I2C Handle.
 *         This parameter can be I2Cx where x: 0 or 1  to select the I2C peripheral.
 * @retval None
 */
#define __HAL_I2C_MASTER_SEND_NACK( __HANDLE__ ) (__HANDLE__)->Instance->MSSPSTAT   |= I2C_MASTERSSPSTAUS_ACKDT; \
    (__HANDLE__)->Instance->MSSPCON                                                 |= I2C_MASTERSSPC0N_ACKEN


/**
 * @}
 */

/* Exported functions --------------------------------------------------------*/


/** @addtogroup I2C_Exported_Functions
 * @{
 */


/** @addtogroup I2C_Exported_Functions_Group1
 * @{
 */
/* Initialization/de-initialization functions  **********************************/
HAL_StatusTypeDef HAL_I2C_Init(I2C_HandleTypeDef *hi2c);


HAL_StatusTypeDef HAL_I2C_DeInit(I2C_HandleTypeDef *hi2c);


void HAL_I2C_MspInit(I2C_HandleTypeDef *hi2c);


void HAL_I2C_MspDeInit(I2C_HandleTypeDef *hi2c);


/**
 * @}
 */


/** @addtogroup I2C_Exported_Functions_Group2
 * @{
 */
HAL_StatusTypeDef HAL_I2C_START(I2C_HandleTypeDef *hi2c, uint32_t Timeout);


HAL_StatusTypeDef HAL_I2C_STOP(I2C_HandleTypeDef *hi2c, uint32_t Timeout);


HAL_StatusTypeDef HAL_I2C_Master_SendByte(I2C_HandleTypeDef *hi2c, uint8_t Data, uint32_t Timeout);


HAL_StatusTypeDef HAL_I2C_Master_RecvByte(I2C_HandleTypeDef *hi2c, uint8_t *pData, uint8_t IsSendACK, uint32_t Timeout);


HAL_StatusTypeDef HAL_I2C_Slave_SendByte(I2C_HandleTypeDef *hi2c, uint8_t Data, uint32_t Timeout);


HAL_StatusTypeDef HAL_I2C_Slave_RecvByte(I2C_HandleTypeDef *hi2c, uint8_t *pData, uint8_t IsSendACK, uint32_t Timeout);


HAL_StatusTypeDef HAL_I2C_Slave_WaitOnStartAndAddress(I2C_HandleTypeDef *hi2c, uint32_t *RW, uint32_t Timeout);


void HAL_I2C_Slave_Reset(I2C_HandleTypeDef *hi2c);


/**
 * @}
 */


/**
 * @}
 */
/* Private types -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private constants ---------------------------------------------------------*/
/* Private macros ------------------------------------------------------------*/


/** @defgroup I2C_Private_Macros I2C Private Macros
 * @{
 */


/** @defgroup I2C_IS_RTC_Definitions I2C Private macros to check input parameters
 * @{
 */
#define IS_I2C_ALL_INSTANCE( INSTANCE ) ( ( (INSTANCE) == I2C0) || \
                                          ( (INSTANCE) == I2C1) )
#define IS_I2C_MODE( MODE )             ( ( (MODE) == I2C_MODE_SLAVE) || \
                                          ( (MODE) == I2C_MODE_MASTER) )
#define IS_I2C_ADDRESS_SIZE( ADDRESS )  ( ( (ADDRESS) == I2C_ADDRESS_7BIT) || \
                                          ( (ADDRESS) == I2C_ADDRESS_10BIT) )
#define IS_I2C_ADDRESS( ADDRESS )       ( ( (ADDRESS) & 0xFFFFFC00U) == 0U)
#define IS_I2C_STRETCH( STRETCH )       ( ( (STRETCH) == I2C_SLAVE_STRETCH_DISABLE) || \
                                          ( (STRETCH) == I2C_SLAVE_STRETCH_ENABLE) )
#define IS_I2C_CLOCK_SPEED( SPEED )     ( ( (SPEED) > 0U) && ( (SPEED) <= 400000U) )


/**
 * @}
 */


/**
 * @}
 */

/* Private functions ---------------------------------------------------------*/


/** @defgroup I2C_Private_Functions I2C Private Functions
 * @{
 */


/**
 * @}
 */


/**
 * @}
 */


/**
 * @}
 */

#ifdef __cplusplus
}
#endif


#endif /* __FM15F3xx_HAL_I2C_H */

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
