/**
  ******************************************************************************
  * @file    fm15f3xx_hal_pmu.h
  * @author  MXB
  * @version V1.0.0
  * @date    2020-09-14
  * @brief
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
  * All rights reserved.</center></h2>
  *
  ******************************************************************************
  */
#ifndef FM15F3xx_HAL_PMU_H
#define FM15F3xx_HAL_PMU_H

#ifdef __cplusplus
extern "C" {
#endif

#include "fm15f3xx_hal_def.h"
#include "fm15f3xx_ll_pmu.h"

typedef struct {
    uint32_t PADHoldEn;/*@ref PMU_LL_PAD_HOLD*/
    uint32_t LDO16CFG;/*@ref PMU_LL_STBY_LDO16*/
} HAL_STBY_CFG_TypeDef;

typedef struct {
    uint32_t LDO12En;/*@ref PMU_LL_EC_PSTOP_LDO12*/
    uint32_t LDO12CFG;/*@ref PMU_LL_EC_PSTOP_LDO12CFG*/
    uint32_t LDO16En;/*@ref PMU_LL_EC_PSTOP_LDO16*/
} HAL_STOP_CFG_TypeDef;

typedef struct {
    uint32_t PADSource;/*@ref PMU_LL_WKUP*/
    uint32_t INTFSource;/*@ref PMU_LL_INTF*/
    uint32_t PADEdgeSel;/*@ref PMU_LL_EC_WKUP*/
    uint32_t INTFEdgeSel;/*@ref PMU_LL_EC_WKUP*/
} HAL_LLWU_TypeDef;

typedef struct {
    uint32_t ModuleASource;/*@ref PMU_LL_MODULE_WKUP_A*/
    uint32_t ModuleBSource;/*@ref PMU_LL_MODULE_WKUP_B*/
} HAL_WEC_TypeDef;

typedef struct {
    uint32_t LP_mode;/*@ref PMU_LL_EC_LP_MODE*/

    HAL_LLWU_TypeDef LLWU;/*@ref Low Leakage Wake Up Unit*/
    HAL_WEC_TypeDef WEC;/*@ref Wake Up Event Controller*/

    HAL_STBY_CFG_TypeDef STBY;/*@ref STBY MODE*/
    HAL_STOP_CFG_TypeDef STOP;/*@ref STOP MODE*/
} HAL_PMU_InitTypeDef;


/** @defgroup PMU_HAL_EC_LP_MODE
  * @{
  */
#define HAL_PMU_LP_POWERDOWN                   (0x00)
#define HAL_PMU_LP_STANDBY                     (0x01)
#define HAL_PMU_LP_STOP                        (0x02)
#define HAL_PMU_LP_WAIT                        (0x03)
/**
  * @}
  */

/** @defgroup PMU_HAL_EC_PSTOP_LDO12
  * @{
  */
#define HAL_PSTOP_LDO12ENLP_NORMAL             0x00000000U                    /*!< Enable ldo12 enter  normalmode  when the CPU enters Stop mode */
#define HAL_PSTOP_LDO12ENLP_LOWPOWER           (PMU_PSTOP_LDO12ENLP)          /*!< Enable ldo12 enter  lowpower mode  when the CPU enters Stop mode */
/**
  * @}
  */

/** @defgroup PMU_HAL_EC_PSTOP_LDO12CFG
  * @{
  */
#define HAL_PMU_LDO12CFG_1V0                   0x00000000U                    /*!< level 0 1.0V*/
#define HAL_PMU_LDO12CFG_1V1                   (0x1U)<<PMU_PSTOP_LDO12CFG_Pos)  /*!< level 1 1.1V*/
#define HAL_PMU_LDO12CFG_1V2                   (0x2U)<<PMU_PSTOP_LDO12CFG_Pos)  /*!< level 2 1.2V*/
#define HAL_PMU_LDO12CFG_1V3                   (0x3U)<<PMU_PSTOP_LDO12CFG_Pos)  /*!< level 3 1.3V*/
/**
  * @}
  */


/** @defgroup PMU_HAL_EC_PIDLE_BGPERCFG
  * @{
  */
#define HAL_PMU_BGPERIODCFG_0                  0x00000000U                    /*!< level 0 */
#define HAL_PMU_BGPERIODCFG_1                  (0x1U<<PMU_PIDLE_BGPERCFG_Pos)  /*!< level 1 */
#define HAL_PMU_BGPERIODCFG_2                  (0x2U<<PMU_PIDLE_BGPERCFG_Pos)  /*!< level 2 */
#define HAL_PMU_BGPERIODCFG_3                  (0x3U<<PMU_PIDLE_BGPERCFG_Pos)  /*!< level 3 */
/**
  * @}
  */
/** @defgroup PMU_LL_EC_PSTOP_LDO16
  * @{
  */
#define HAL_PSTOP_LDO16ENLP_NORMAL             0x00000000U                    /*!< Enable ldo16 enter normal mode  when the CPU enters Stop mode */
#define HAL_PSTOP_LDO16ENLP_LOWPOWER           (PMU_PSTOP_LDO16ENLP)          /*!< Enable ldo16 enter lowpower mode  when the CPU enters Stop mode */
/**
  * @}
  */

/**
  * @}
  */

/** @defgroup PMU_HAL_WKUP PIN SELECT
  * @{
  */
#define HAL_LLWU_WKUP_PD0                      (0x00000001U)                   /*!< PD0 */
#define HAL_LLWU_WKUP_PD1                      (0x00000002U)                   /*!< PD1 */
#define HAL_LLWU_WKUP_PD2                      (0x00000004U)                   /*!< PD2 */
#define HAL_LLWU_WKUP_PD3                      (0x00000008U)                   /*!< PD3 */
#define HAL_LLWU_WKUP_PD4                      (0x00000010U)                   /*!< PD4 */
#define HAL_LLWU_WKUP_PD5                      (0x00000020U)                   /*!< PD5 */
#define HAL_LLWU_WKUP_PD6                      (0x00000040U)                   /*!< PD6 */
#define HAL_LLWU_WKUP_PD7                      (0x00000080U)                   /*!< PD7 */
#define HAL_LLWU_WKUP_PC4                      (0x00000100U)                  /*!< PC4 */
#define HAL_LLWU_WKUP_PC5                      (0x00000200U)                  /*!< PC5 */
#define HAL_LLWU_WKUP_PC6                      (0x00000400U)                  /*!< PC6 */
#define HAL_LLWU_WKUP_PC7                      (0x00000800U)                  /*!< PC7 */
#define HAL_LLWU_WKUP_PF0                      (0x00001000U)                 /*!< PF0 */
#define HAL_LLWU_WKUP_PF1                      (0x00002000U)                 /*!< PF1 */
#define HAL_LLWU_WKUP_PF2                      (0x00004000U)                 /*!< PF2 */
#define HAL_LLWU_WKUP_PF3                      (0x00008000U)                 /*!< PF3 */
#define HAL_LLWU_WKUP_PF4                      (0x00010000U)                /*!< PF4 */
/**
  * @}
  */

/** @defgroup PMU_HAL_EC_WKUP
  * @{
  */
#define HAL_LLWU_WKUP_CLOSED                   (0x00000000U)                   /*!< Wakeup closed */
#define HAL_LLWU_WKUP_RISE                     (0x00000001U)                   /*!< Wakeup rising edge */
#define HAL_LLWU_WKUP_FALL                     (0x00000002U)                   /*!< Wakeup falling edge */
#define HAL_LLWU_WKUP_BOTH                     (0x00000003U)                   /*!< Wakeup both edge */
/**
  * @}
  */

/** @defgroup PMU_LL_INTF SELECT
  * @{
  */
#define HAL_LLWU_WKUP_LPTM                     (0x00000001U)                 /*!< v2m_pdm_lptmr_wakeup */
#define HAL_LLWU_WKUP_CMPOUT                   (0x00000002U)                 /*!< a2v_cmp_out */
#define HAL_LLWU_WKUP_USBDP                    (0x00000004U)                 /*!< a2v_phy_rx_dp */
#define HAL_LLWU_WKUP_USBDM                    (0x00000008U)                 /*!< a2v_phy_rx_dm */
#define HAL_LLWU_WKUP_VBUSDET                  (0x00000010U)                 /*!< p2v_vbus_det */
#define HAL_LLWU_WKUP_RTCALARM                 (0x00000020U)                 /*!< b2v_rtc_alarm */
#define HAL_LLWU_WKUP_RTCTAMPER                (0x00000040U)                 /*!< b2v_rtc_tamper */
#define HAL_LLWU_WKUP_RTC1S                    (0x00000080U)                 /*!< b2v_rtc_1s */
#define HAL_LLWU_WKUP_CHG90PER                 (0x00000100U)                 /*!< a2v_chg_90per */
#define HAL_LLWU_WKUP_CHGIEOC                  (0x00000200U)                 /*!< a2v_chg_ieoc_flg */
#define HAL_LLWU_WKUP_CHGPWOK                  (0x00000400U)                 /*!< a2v_chg_pwok */
#define HAL_LLWU_WKUP_CHG_MON_CHG              (0x00000800U)                 /*!< a2v_chg_vbat_mon_chg */
#define HAL_LLWU_WKUP_CHG_MON_RCH              (0x00001000U)                 /*!< a2v_chg_vbat_mon_rch */
#define HAL_LLWU_WKUP_JTAG_SELN                (0x00004000U)                 /*!< p2v_jtag_sel_n */
#define HAL_LLWU_WKUP_STBY_PAD_EVENT           (0x00008000U)                 /*!< m2v_pm_standby_pad_event */
/**
  * @}
  */

/** @defgroup PMU_HAL_PAD_HOLD
  * @{
  */
#define HAL_PMU_PAD_HOLD_DISABLE               0x00
#define HAL_PMU_PAD_HOLD_ENABLE                (0x01)
/**
  * @}
  */

/** @defgroup PMU_HAL_STBY_LDO16
  * @{
  */
#define HAL_PMU_STBY_LDO16_LOWPOWER            0x00
#define HAL_PMU_STBY_LDO16_CLOSE               (0x01)
/**
  * @}
  */

/** @defgroup PMU_HAL_PADFILTER
  * @{
  */
#define HAL_PMU_LLWU_PADFILTER_CH0             (0x01)
#define HAL_PMU_LLWU_PADFILTER_CH1             (0x02)
#define HAL_PMU_LLWU_PADFILTER_CH2             (0x04)
#define HAL_PMU_LLWU_PADFILTER_CH3             (0x08)

#define HAL_PMU_LLWU_PADFILTER_CH0SN           (0)
#define HAL_PMU_LLWU_PADFILTER_CH1SN           (1)
#define HAL_PMU_LLWU_PADFILTER_CH2SN           (2)
#define HAL_PMU_LLWU_PADFILTER_CH3SN           (3)
/**
  * @}
  */

/** @defgroup PMU_HAL_MODULE_WKUP_A
  * @{
  */
#define HAL_WEC_WKUPA_LPTIMER_INT              (0x00000001U)
#define HAL_WEC_WKUPA_STIMER4_INT              (0x00000004U)
#define HAL_WEC_WKUPA_STIMER5_INT              (0x00000008U)
#define HAL_WEC_WKUPA_STIMER0_INT              (0x00000010U)
#define HAL_WEC_WKUPA_STIMER1_INT              (0x00000020U)
#define HAL_WEC_WKUPA_STIMER2_INT              (0x00000040U)
#define HAL_WEC_WKUPA_STIMER3_INT              (0x00000080U)
#define HAL_WEC_WKUPA_LLWU                     (0x00000100U)
#define HAL_WEC_WKUPA_UART0_INT                (0x00010000U)
#define HAL_WEC_WKUPA_UART1_INT                (0x00020000U)
#define HAL_WEC_WKUPA_UART2_INT                (0x00040000U)
#define HAL_WEC_WKUPA_UART3_INT                (0x00080000U)
#define HAL_WEC_WKUPA_CT0_INT                  (0x01000000U)
#define HAL_WEC_WKUPA_CT1_INT                  (0x02000000U)
#define HAL_WEC_WKUPA_ALM_INT                  (0x10000000U)
#define HAL_WEC_WKUPA_TAMPER_ALM_INT           (0x20000000U)
#define HAL_WEC_WKUPA_RTC_ALM_INT              (0x40000000U)
#define HAL_WEC_WKUPA_RTC_1S_INT               (0x80000000U)
/**
  * @}
  */

/** @defgroup PMU_HAL_MODULE_WKUP_B
  * @{
  */
#define HAL_WEC_WKUPB_GPIOA                    (0x00000002U)
#define HAL_WEC_WKUPB_GPIOB                    (0x00000004U)
#define HAL_WEC_WKUPB_GPIOC                    (0x00000008U)
#define HAL_WEC_WKUPB_GPIOD                    (0x00000010U)
#define HAL_WEC_WKUPB_GPIOE                    (0x00000020U)
#define HAL_WEC_WKUPB_GPIOF                    (0x00000040U)
/**
  * @}
  */

/** @defgroup PMU_HAL_MODULE_WKUP_C
  * @{
  */
#define HAL_WEC_WKUPC_LLWU                     (0x00000001U)
#define HAL_WEC_WKUPC_CMP                      (0x00000002U)
#define HAL_WEC_WKUPC_ADC                      (0x00000004U)
#define HAL_WEC_WKUPC_DMA                      (0x00000008U)
/**
  * @}
  */


/**
  * @}
  */

void HAL_PMU_Init(HAL_PMU_InitTypeDef *PMU_InitStruct);
void HAL_PMU_EnterLowpowerMode(void);


#ifdef __cplusplus
}
#endif
#endif

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
