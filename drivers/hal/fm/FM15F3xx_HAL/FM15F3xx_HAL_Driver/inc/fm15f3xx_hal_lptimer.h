/**
 ******************************************************************************
 * @file    fm15f3xx_hal_lptimer.h
 * @author  MCD Application Team
 * @brief   Header file of LPTIMER HAL module.
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __FM15F3xx_HAL_LPTIM_H
#define __FM15F3xx_HAL_LPTIM_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "fm15f3xx_hal_def.h"
#include "fm15f3xx_ll_lptimer.h"


/** @addtogroup FM15F3xx_HAL_Driver
 * @{
 */


/** @defgroup LPTIM LPTIM
 * @brief LPTIM HAL module driver
 * @{
 */

/* Exported types ------------------------------------------------------------*/


/** @defgroup LPTIM_Exported_Types LPTIM Exported Types
 * @{
 */


/**
 * @brief  LPTIM Initialization Structure definition
 */
typedef struct {
    uint32_t InputInvert;

    uint32_t TrigSrc;
} LPTIM_TrigConfigTypeDef;


/**
 * @brief  LPTIM Initialization Structure definition
 */
typedef struct {
    uint32_t Pad;
} LPTIM_PadClkConfigTypeDef;


/**
 * @brief  LPTIM Initialization Structure definition
 */
typedef struct {
    uint8_t Clock;

    uint8_t Prescaler;

    uint8_t WorkMode;

    LPTIM_TrigConfigTypeDef Trigger;            /*!< Specifies the Trigger parameters */

    LPTIM_PadClkConfigTypeDef PadClock;         /*!< Specifies the PadClock parameters */
} LPTIM_InitTypeDef;


/**
 * @brief  HAL LPTIM State structure definition
 */
typedef enum __HAL_LPTIM_StateTypeDef {
    HAL_LPTIM_STATE_RESET   = 0x00U,            /*!< Peripheral not yet initialized or disabled  */
    HAL_LPTIM_STATE_READY   = 0x01U,            /*!< Peripheral Initialized and ready for use    */
    HAL_LPTIM_STATE_BUSY    = 0x02U,            /*!< An internal process is ongoing              */
    HAL_LPTIM_STATE_TIMEOUT = 0x03U,            /*!< Timeout state                               */
    HAL_LPTIM_STATE_ERROR   = 0x04U             /*!< Internal Process is ongoing                 */
} HAL_LPTIM_StateTypeDef;


/**
 * @brief  LPTIM handle Structure definition
 */
typedef struct {
    LPTIMER_TypeDef *Instance;                  /*!< Register base address     */

    LPTIM_InitTypeDef Init;                     /*!< LPTIM required parameters */

    __IO HAL_LPTIM_StateTypeDef State;          /*!< LPTIM peripheral state    */
} LPTIM_HandleTypeDef;


/**
 * @}
 */

/* Exported constants --------------------------------------------------------*/


/** @defgroup LPTIM_Exported_Constants LPTIM Exported Constants
 * @{
 */


/** @defgroup LPTIM_Clock_Source LPTIM Clock Source
 * @{
 */
#define LPTIM_CLKSRC_IRC4M  0x00000000U
#define LPTIM_CLKSRC_LPO1K  (0x1 << LPTIMER_CLKCTRL_CLKSEL_Pos)
#define LPTIM_CLKSRC_RTC1S  (0x2 << LPTIMER_CLKCTRL_CLKSEL_Pos)
#define LPTIM_CLKSRC_PAD    (0x3 << LPTIMER_CLKCTRL_CLKSEL_Pos)


/**
 * @}
 */


#define LPTIM_PAD_CLK_PD0   (0x0U << LPTIMER_CLKCTRL_PADCLKSEL_Pos)
#define LPTIM_PAD_CLK_PD1   (0x1U << LPTIMER_CLKCTRL_PADCLKSEL_Pos)
#define LPTIM_PAD_CLK_PD2   (0x2U << LPTIMER_CLKCTRL_PADCLKSEL_Pos)
#define LPTIM_PAD_CLK_PD3   (0x3U << LPTIMER_CLKCTRL_PADCLKSEL_Pos)
#define LPTIM_PAD_CLK_PD4   (0x4U << LPTIMER_CLKCTRL_PADCLKSEL_Pos)
#define LPTIM_PAD_CLK_PD5   (0x5U << LPTIMER_CLKCTRL_PADCLKSEL_Pos)
#define LPTIM_PAD_CLK_PD6   (0x6U << LPTIMER_CLKCTRL_PADCLKSEL_Pos)
#define LPTIM_PAD_CLK_PD7   (0x7U << LPTIMER_CLKCTRL_PADCLKSEL_Pos)
#define LPTIM_PAD_CLK_PC4   (0x8U << LPTIMER_CLKCTRL_PADCLKSEL_Pos)
#define LPTIM_PAD_CLK_PC5   (0x9U << LPTIMER_CLKCTRL_PADCLKSEL_Pos)
#define LPTIM_PAD_CLK_PC6   (0xAU << LPTIMER_CLKCTRL_PADCLKSEL_Pos)
#define LPTIM_PAD_CLK_PC7   (0xBU << LPTIMER_CLKCTRL_PADCLKSEL_Pos)
#define LPTIM_PAD_CLK_PF0   (0xCU << LPTIMER_CLKCTRL_PADCLKSEL_Pos)
#define LPTIM_PAD_CLK_PF1   (0xDU << LPTIMER_CLKCTRL_PADCLKSEL_Pos)
#define LPTIM_PAD_CLK_PF2   (0xEU << LPTIMER_CLKCTRL_PADCLKSEL_Pos)
#define LPTIM_PAD_CLK_PF3   (0xFU << LPTIMER_CLKCTRL_PADCLKSEL_Pos)


/** @defgroup LPTIM_Clock_Prescaler LPTIM Clock TIME Prescaler
 * @{
 */
#define LPTIM_PRESCALER_TIME_DIV2       ( (0x00U << LPTIMER_CLKCTRL_PRESCFG_Pos) | LPTIMER_CLKCTRL_PRESEN)
#define LPTIM_PRESCALER_TIME_DIV4       ( (0x01U << LPTIMER_CLKCTRL_PRESCFG_Pos) | LPTIMER_CLKCTRL_PRESEN)
#define LPTIM_PRESCALER_TIME_DIV8       ( (0x02U << LPTIMER_CLKCTRL_PRESCFG_Pos) | LPTIMER_CLKCTRL_PRESEN)
#define LPTIM_PRESCALER_TIME_DIV16      ( (0x03U << LPTIMER_CLKCTRL_PRESCFG_Pos) | LPTIMER_CLKCTRL_PRESEN)
#define LPTIM_PRESCALER_TIME_DIV32      ( (0x04U << LPTIMER_CLKCTRL_PRESCFG_Pos) | LPTIMER_CLKCTRL_PRESEN)
#define LPTIM_PRESCALER_TIME_DIV64      ( (0x05U << LPTIMER_CLKCTRL_PRESCFG_Pos) | LPTIMER_CLKCTRL_PRESEN)
#define LPTIM_PRESCALER_TIME_DIV128     ( (0x06U << LPTIMER_CLKCTRL_PRESCFG_Pos) | LPTIMER_CLKCTRL_PRESEN)
#define LPTIM_PRESCALER_TIME_DIV256     ( (0x07U << LPTIMER_CLKCTRL_PRESCFG_Pos) | LPTIMER_CLKCTRL_PRESEN)
#define LPTIM_PRESCALER_TIME_DIV512     ( (0x08U << LPTIMER_CLKCTRL_PRESCFG_Pos) | LPTIMER_CLKCTRL_PRESEN)
#define LPTIM_PRESCALER_TIME_DIV1024    ( (0x09U << LPTIMER_CLKCTRL_PRESCFG_Pos) | LPTIMER_CLKCTRL_PRESEN)
#define LPTIM_PRESCALER_TIME_DIV2048    ( (0x0AU << LPTIMER_CLKCTRL_PRESCFG_Pos) | LPTIMER_CLKCTRL_PRESEN)
#define LPTIM_PRESCALER_TIME_DIV4096    ( (0x0BU << LPTIMER_CLKCTRL_PRESCFG_Pos) | LPTIMER_CLKCTRL_PRESEN)
#define LPTIM_PRESCALER_TIME_DIV8192    ( (0x0CU << LPTIMER_CLKCTRL_PRESCFG_Pos) | LPTIMER_CLKCTRL_PRESEN)
#define LPTIM_PRESCALER_TIME_DIV16384   ( (0x0DU << LPTIMER_CLKCTRL_PRESCFG_Pos) | LPTIMER_CLKCTRL_PRESEN)
#define LPTIM_PRESCALER_TIME_DIV32768   ( (0x0EU << LPTIMER_CLKCTRL_PRESCFG_Pos) | LPTIMER_CLKCTRL_PRESEN)
#define LPTIM_PRESCALER_TIME_DIV65536   ( (0x0FU << LPTIMER_CLKCTRL_PRESCFG_Pos) | LPTIMER_CLKCTRL_PRESEN)


/**
 * @}
 */


/** @defgroup LPTIM_Clock_Prescaler LPTIM Clock PULSE Prescaler
 * @{
 */
#define LPTIM_PRESCALER_PULSE_NODIV ( (0x00U << LPTIMER_CLKCTRL_PRESCFG_Pos) | LPTIMER_CLKCTRL_PRESEN)
#define LPTIM_PRESCALER_PULSE_DIV2  ( (0x01U << LPTIMER_CLKCTRL_PRESCFG_Pos) | LPTIMER_CLKCTRL_PRESEN)


/**
 * @}
 */


/** @defgroup LPTIM_Clock_Prescaler LPTIM Clock Pulse Prescaler
 * @{
 */
#define LPTIM_PRESCALER_TRIG_NODIV  (0x00U | LPTIMER_CLKCTRL_PRESEN)
#define LPTIM_PRESCALER_TRIG_DIV2   ( (0x01U << LPTIMER_CLKCTRL_PRESCFG_Pos) | LPTIMER_CLKCTRL_PRESEN)


/**
 * @}
 */


/** @defgroup LPTIM_Work_Mode LPTIM Work Mode
 * @{
 */
#define LPTIM_WORKMODE_MODCNT   (0x00000000U)
#define LPTIM_WORKMODE_FREECNT  (0x02U << LPTIMER_CFG_RUNM_Pos)
#define LPTIM_WORKMODE_TRIG     (0x01U << LPTIMER_CFG_RUNM_Pos)


/**
 * @}
 */


/** @defgroup LPTIM_Trigger_Source LPTIM Trigger Source
 * @{
 */
#define LPTIM_TRIGSRC_PD0               0x00000000U
#define LPTIM_TRIGSRC_PD1               (0x1 << LPTIMER_CFG_INSEL_Pos)
#define LPTIM_TRIGSRC_PD2               (0x2 << LPTIMER_CFG_INSEL_Pos)
#define LPTIM_TRIGSRC_PD3               (0x3 << LPTIMER_CFG_INSEL_Pos)
#define LPTIM_TRIGSRC_PD4               (0x4 << LPTIMER_CFG_INSEL_Pos)
#define LPTIM_TRIGSRC_PD5               (0x5 << LPTIMER_CFG_INSEL_Pos)
#define LPTIM_TRIGSRC_PD6               (0x6 << LPTIMER_CFG_INSEL_Pos)
#define LPTIM_TRIGSRC_PD7               (0x7 << LPTIMER_CFG_INSEL_Pos)
#define LPTIM_TRIGSRC_PC4               (0x8 << LPTIMER_CFG_INSEL_Pos)
#define LPTIM_TRIGSRC_PC5               (0x9 << LPTIMER_CFG_INSEL_Pos)
#define LPTIM_TRIGSRC_PC6               (0xA << LPTIMER_CFG_INSEL_Pos)
#define LPTIM_TRIGSRC_PC7               (0xB << LPTIMER_CFG_INSEL_Pos)
#define LPTIM_TRIGSRC_PF0               (0xC << LPTIMER_CFG_INSEL_Pos)
#define LPTIM_TRIGSRC_PF1               (0xD << LPTIMER_CFG_INSEL_Pos)
#define LPTIM_TRIGSRC_PF2               (0xE << LPTIMER_CFG_INSEL_Pos)
#define LPTIM_TRIGSRC_PF3               (0xF << LPTIMER_CFG_INSEL_Pos)
#define LPTIM_TRIGSRC_LPTIM_WAKEUP      (0x10 << LPTIMER_CFG_INSEL_Pos)
#define LPTIM_TRIGSRC_CMP               (0x11 << LPTIMER_CFG_INSEL_Pos)
#define LPTIM_TRIGSRC_PHY_RXDP          (0x12 << LPTIMER_CFG_INSEL_Pos)
#define LPTIM_TRIGSRC_PHY_RXDM          (0x13 << LPTIMER_CFG_INSEL_Pos)
#define LPTIM_TRIGSRC_VBUS_DET          (0x14 << LPTIMER_CFG_INSEL_Pos)
#define LPTIM_TRIGSRC_RTC_ALARM         (0x15 << LPTIMER_CFG_INSEL_Pos)
#define LPTIM_TRIGSRC_BKP_TAMPER        (0x16 << LPTIMER_CFG_INSEL_Pos)
#define LPTIM_TRIGSRC_RTC_1S            (0x17 << LPTIMER_CFG_INSEL_Pos)
#define LPTIM_TRIGSRC_PMU_CHG90         (0x18 << LPTIMER_CFG_INSEL_Pos)
#define LPTIM_TRIGSRC_PMU_CHGEOC        (0x19 << LPTIMER_CFG_INSEL_Pos)
#define LPTIM_TRIGSRC_PF4               (0x1D << LPTIMER_CFG_INSEL_Pos)
#define LPTIM_TRIGSRC_JTAG_SEL          (0x1E << LPTIMER_CFG_INSEL_Pos)
#define LPTIM_TRIGSRC_STANDBY_WAKEUP    (0x1F << LPTIMER_CFG_INSEL_Pos)


/**
 * @}
 */


/** @defgroup LPTIM_Output_Polarity LPTIM Input Polarity
 * @{
 */
#define LPTIM_INPUT_NOINVERT    0x00000000U
#define LPTIM_INPUT_INVERT      (LPTIMER_CFG_INPOLAR)


/**
 * @}
 */


/** @defgroup LPTIM_Flag_Definition LPTIM Flag Definition
 * @{
 */

#define LPTIM_FLAG_OVER LPTIMER_FLAGSTATUS_OVF


/**
 * @}
 */


/** @defgroup LPTIM_Interrupts_Definition LPTIM Interrupts Definition
 * @{
 */

#define LPTIM_IT_OVER LPTIMER_CLKCTRL_INTEN


/**
 * @}
 */


/**
 * @}
 */

/* Exported macro ------------------------------------------------------------*/


/** @defgroup LPTIM_Exported_Macros LPTIM Exported Macros
 * @{
 */


/** @brief Reset LPTIM handle state
 * @param  __HANDLE__ LPTIM handle
 * @retval None
 */
#define __HAL_LPTIM_RESET_HANDLE_STATE( __HANDLE__ ) ( (__HANDLE__)->State = HAL_LPTIM_STATE_RESET)


/**
 * @brief  Enable/Disable the LPTIM peripheral.
 * @param  __HANDLE__ LPTIM handle
 * @retval None
 */
#define __HAL_LPTIM_ENABLE( __HANDLE__ )    ( (__HANDLE__)->Instance->CTRL |= (LPTIMER_CTRL_RUNEN) )
#define __HAL_LPTIM_DISABLE( __HANDLE__ )   ( (__HANDLE__)->Instance->CTRL &= ~(LPTIMER_CTRL_RUNEN) )


/**
 * @brief  Writes the passed parameter in the Autoreload register.
 * @param  __HANDLE__ LPTIM handle
 * @param  __VALUE__  Autoreload value
 * @retval None
 */
#define __HAL_LPTIM_AUTORELOAD_SET( __HANDLE__, __VALUE__ ) ( (__HANDLE__)->Instance->MOD = (__VALUE__) )


/**
 * @brief  Checks whether the specified LPTIM  OVER flag is set or not.
 * @param  __HANDLE__ LPTIM handle
 * @retval The state of the specified flag (SET or RESET).
 */
#define __HAL_LPTIM_GET_FLAG_OVER( __HANDLE__ ) ( ( (__HANDLE__)->Instance->FLAGSTATUS & (LPTIMER_FLAGSTATUS_OVF) ) == (LPTIMER_FLAGSTATUS_OVF) )


/**
 * @brief  Clears the specified LPTIM OVER flag.
 * @param  __HANDLE__ LPTIM handle.
 * @retval None.
 */
#define __HAL_LPTIM_CLEAR_FLAG_OVER( __HANDLE__ ) ( (__HANDLE__)->Instance->FLAGCLR |= (LPTIMER_IFCLR_OVF) )


/**
 * @brief  Enable the specified LPTIM interrupt.
 * @param  __HANDLE__     LPTIM handle.
 * @retval None.
 */
#define __HAL_LPTIM_ENABLE_IT( __HANDLE__ ) ( (__HANDLE__)->Instance->CLKCFG |= (LPTIMER_CLKCTRL_INTEN) )


/**
 * @brief  Disable the specified LPTIM interrupt.
 * @param  __HANDLE__     LPTIM handle.
 * @retval None.
 */
#define __HAL_LPTIM_DISABLE_IT( __HANDLE__ ) ( (__HANDLE__)->Instance->CLKCFG &= (~(LPTIMER_CLKCTRL_INTEN) ) )


/**
 * @brief  Checks whether the specified LPTIM interrupt is set or not.
 * @param  __HANDLE__     LPTIM handle.
 * @retval Interrupt status.
 */
#define __HAL_LPTIM_GET_IT_SOURCE( __HANDLE__ ) ( ( ( (__HANDLE__)->Instance->CLKCFG & (LPTIMER_CLKCTRL_INTEN) ) == (LPTIMER_CLKCTRL_INTEN) ) )


/**
 * @}
 */
/* Exported functions --------------------------------------------------------*/


/** @defgroup LPTIM_Exported_Functions LPTIM Exported Functions
 * @{
 */

/* Initialization/de-initialization functions  ********************************/
HAL_StatusTypeDef HAL_LPTIM_Init(LPTIM_HandleTypeDef *hlptim);


HAL_StatusTypeDef HAL_LPTIM_DeInit(LPTIM_HandleTypeDef *hlptim);


/* MSP functions  *************************************************************/
void HAL_LPTIM_MspInit(LPTIM_HandleTypeDef *hlptim);


void HAL_LPTIM_MspDeInit(LPTIM_HandleTypeDef *hlptim);


/* Start/Stop operation functions  *********************************************/
/* ############################# TRIG Mode ##############################*/
/* Blocking mode: Polling */
HAL_StatusTypeDef HAL_LPTIM_TRIG_Start(LPTIM_HandleTypeDef *hlptim, uint16_t Period);


HAL_StatusTypeDef HAL_LPTIM_TRIG_Stop(LPTIM_HandleTypeDef *hlptim);


/* Non-Blocking mode: Interrupt */
HAL_StatusTypeDef HAL_LPTIM_TRIG_Start_IT(LPTIM_HandleTypeDef *hlptim, uint16_t Period);


HAL_StatusTypeDef HAL_LPTIM_TRIG_Stop_IT(LPTIM_HandleTypeDef *hlptim);


/* ############################## Counter Mode ###############################*/
/* Blocking mode: Polling */
HAL_StatusTypeDef HAL_LPTIM_Counter_Start(LPTIM_HandleTypeDef *hlptim, uint16_t Period);


HAL_StatusTypeDef HAL_LPTIM_Counter_Stop(LPTIM_HandleTypeDef *hlptim);


/* Non-Blocking mode: Interrupt */
HAL_StatusTypeDef HAL_LPTIM_Counter_Start_IT(LPTIM_HandleTypeDef *hlptim, uint16_t Period);


HAL_StatusTypeDef HAL_LPTIM_Counter_Stop_IT(LPTIM_HandleTypeDef *hlptim);


/* Reading operation functions ************************************************/
uint32_t HAL_LPTIM_ReadCounter(LPTIM_HandleTypeDef *hlptim);


uint32_t HAL_LPTIM_ReadAutoReload(LPTIM_HandleTypeDef *hlptim);


/* LPTIM IRQ functions  *******************************************************/
void HAL_LPTIM_IRQHandler(LPTIM_HandleTypeDef *hlptim);


/* CallBack functions  ********************************************************/
void HAL_LPTIM_OverCallback(LPTIM_HandleTypeDef *hlptim);


/* Peripheral State functions  ************************************************/
HAL_LPTIM_StateTypeDef HAL_LPTIM_GetState(LPTIM_HandleTypeDef *hlptim);


/**
 * @}
 */

/* Private types -------------------------------------------------------------*/


/** @defgroup LPTIM_Private_Types LPTIM Private Types
 * @{
 */


/**
 * @}
 */

/* Private variables ---------------------------------------------------------*/


/** @defgroup LPTIM_Private_Variables LPTIM Private Variables
 * @{
 */


/**
 * @}
 */

/* Private constants ---------------------------------------------------------*/


/** @defgroup LPTIM_Private_Constants LPTIM Private Constants
 * @{
 */


/**
 * @}
 */

/* Private macros ------------------------------------------------------------*/


/** @defgroup LPTIM_Private_Macros LPTIM Private Macros
 * @{
 */

#define IS_LPTIM_INSTANCE( INSTANCE ) ( (INSTANCE) == LPTIMER0)

#define IS_LPTIM_CLOCK_SOURCE( SOURCE ) ( ( (SOURCE) == LPTIM_CLKSRC_IRC4M) || \
                                          ( (SOURCE) == LPTIM_CLKSRC_LPO1K) || \
                                          ( (SOURCE) == LPTIM_CLKSRC_RTC1S) || \
                                          ( (SOURCE) == LPTIM_CLKSRC_PAD) )

#define IS_LPTIM_CLOCK_PAD_CLK( CLK ) ( ( (CLK) == LPTIM_PAD_CLK_PD0) || \
                                        ( (CLK) == LPTIM_PAD_CLK_PD1) || \
                                        ( (CLK) == LPTIM_PAD_CLK_PD2) || \
                                        ( (CLK) == LPTIM_PAD_CLK_PD3) || \
                                        ( (CLK) == LPTIM_PAD_CLK_PD4) || \
                                        ( (CLK) == LPTIM_PAD_CLK_PD5) || \
                                        ( (CLK) == LPTIM_PAD_CLK_PD6) || \
                                        ( (CLK) == LPTIM_PAD_CLK_PD7) || \
                                        ( (CLK) == LPTIM_PAD_CLK_PC4) || \
                                        ( (CLK) == LPTIM_PAD_CLK_PC5) || \
                                        ( (CLK) == LPTIM_PAD_CLK_PC6) || \
                                        ( (CLK) == LPTIM_PAD_CLK_PC7) || \
                                        ( (CLK) == LPTIM_PAD_CLK_PF0) || \
                                        ( (CLK) == LPTIM_PAD_CLK_PF1) || \
                                        ( (CLK) == LPTIM_PAD_CLK_PF2) || \
                                        ( (CLK) == LPTIM_PAD_CLK_PF3) )

#define IS_LPTIM_CLOCK_PRESCALER_CNT( PRESCALER ) ( ( (PRESCALER) == LPTIM_PRESCALER_TIME_DIV2) || \
                                                    ( (PRESCALER) == LPTIM_PRESCALER_PULSE_NODIV) || \
                                                    ( (PRESCALER) == LPTIM_PRESCALER_TIME_DIV8) || \
                                                    ( (PRESCALER) == LPTIM_PRESCALER_PULSE_DIV2) || \
                                                    ( (PRESCALER) == LPTIM_PRESCALER_TIME_DIV8) || \
                                                    ( (PRESCALER) == LPTIM_PRESCALER_TIME_DIV16) || \
                                                    ( (PRESCALER) == LPTIM_PRESCALER_TIME_DIV32) || \
                                                    ( (PRESCALER) == LPTIM_PRESCALER_TIME_DIV64) || \
                                                    ( (PRESCALER) == LPTIM_PRESCALER_TIME_DIV128) || \
                                                    ( (PRESCALER) == LPTIM_PRESCALER_TIME_DIV256) || \
                                                    ( (PRESCALER) == LPTIM_PRESCALER_TIME_DIV512) || \
                                                    ( (PRESCALER) == LPTIM_PRESCALER_TIME_DIV1024) || \
                                                    ( (PRESCALER) == LPTIM_PRESCALER_TIME_DIV2048) || \
                                                    ( (PRESCALER) == LPTIM_PRESCALER_TIME_DIV4096) || \
                                                    ( (PRESCALER) == LPTIM_PRESCALER_TIME_DIV8192) || \
                                                    ( (PRESCALER) == LPTIM_PRESCALER_TIME_DIV16384) || \
                                                    ( (PRESCALER) == LPTIM_PRESCALER_TIME_DIV32768) || \
                                                    ( (PRESCALER) == LPTIM_PRESCALER_TIME_DIV65536) )

#define IS_LPTIM_CLOCK_PRESCALER_TRIG( PRESCALER ) ( ( (PRESCALER) == LPTIM_PRESCALER_TRIG_NODIV) || \
                                                     ( (PRESCALER) == LPTIM_PRESCALER_TRIG_DIV2) )

#define IS_LPTIM_WORK_MODE( MODE ) ( ( (MODE) == LPTIM_WORKMODE_MODCNT) || \
                                     ( (MODE) == LPTIM_WORKMODE_FREECNT) || \
                                     ( (MODE) == LPTIM_WORKMODE_TRIG) )

#define IS_LPTIM_TRIG_SOURCE( TRIG ) ( ( (TRIG) == LPTIM_TRIGSRC_PD0) || \
                                       ( (TRIG) == LPTIM_TRIGSRC_PD1) || \
                                       ( (TRIG) == LPTIM_TRIGSRC_PD2) || \
                                       ( (TRIG) == LPTIM_TRIGSRC_PD3) || \
                                       ( (TRIG) == LPTIM_TRIGSRC_PD4) || \
                                       ( (TRIG) == LPTIM_TRIGSRC_PD5) || \
                                       ( (TRIG) == LPTIM_TRIGSRC_PD6) || \
                                       ( (TRIG) == LPTIM_TRIGSRC_PD7) || \
                                       ( (TRIG) == LPTIM_TRIGSRC_PC4) || \
                                       ( (TRIG) == LPTIM_TRIGSRC_PC5) || \
                                       ( (TRIG) == LPTIM_TRIGSRC_PC6) || \
                                       ( (TRIG) == LPTIM_TRIGSRC_PC7) || \
                                       ( (TRIG) == LPTIM_TRIGSRC_PF0) || \
                                       ( (TRIG) == LPTIM_TRIGSRC_PF1) || \
                                       ( (TRIG) == LPTIM_TRIGSRC_PF2) || \
                                       ( (TRIG) == LPTIM_TRIGSRC_PF3) || \
                                       ( (TRIG) == LPTIM_TRIGSRC_PD7) || \
                                       ( (TRIG) == LPTIM_TRIGSRC_LPTIM_WAKEUP) || \
                                       ( (TRIG) == LPTIM_TRIGSRC_CMP) || \
                                       ( (TRIG) == LPTIM_TRIGSRC_PHY_RXDP) || \
                                       ( (TRIG) == LPTIM_TRIGSRC_PHY_RXDM) || \
                                       ( (TRIG) == LPTIM_TRIGSRC_VBUS_DET) || \
                                       ( (TRIG) == LPTIM_TRIGSRC_RTC_ALARM) || \
                                       ( (TRIG) == LPTIM_TRIGSRC_BKP_TAMPER) || \
                                       ( (TRIG) == LPTIM_TRIGSRC_RTC_1S) || \
                                       ( (TRIG) == LPTIM_TRIGSRC_PMU_CHG90) || \
                                       ( (TRIG) == LPTIM_TRIGSRC_PMU_CHGEOC) || \
                                       ( (TRIG) == LPTIM_TRIGSRC_PF4) || \
                                       ( (TRIG) == LPTIM_TRIGSRC_JTAG_SEL) || \
                                       ( (TRIG) == LPTIM_TRIGSRC_STANDBY_WAKEUP) )

#define IS_LPTIM_INPUT_INVERT( INVERT ) ( ( (INVERT) == LPTIM_INPUT_NOINVERT) || \
                                          ( (INVERT) == LPTIM_INPUT_INVERT) )

#define IS_LPTIM_AUTORELOAD( AUTORELOAD ) ( (AUTORELOAD) <= 0x0000FFFFU)


/**
 * @}
 */

/* Private functions ---------------------------------------------------------*/


/** @defgroup LPTIM_Private_Functions LPTIM Private Functions
 * @{
 */


/**
 * @}
 */


/**
 * @}
 */


/**
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif /* __FM15F3xx_HAL_LPTIM_H */

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
