/**
 ******************************************************************************
 * @file    fm15f3xx_ll_i2c.h
 * @author  TYW
 * @version V1.0.0
 * @date    2020-03-17
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */

/* Define to prevent recursive inclusion -------------------------------------*/

#ifndef __FM15F3XX_LL_I2C_H
#define __FM15F3XX_LL_I2C_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "fm15f3xx.h"


/** @addtogroup FM399_LL_Driver
 * @{
 */


/** @addtogroup I2C
 * @{
 */


/** @defgroup I2C_Exported_Types
 * @{
 */


/**
 * @brief LL_I2C_InitTypeDef I2C Exported Init structure definition
 */

typedef struct {
    uint32_t Mode;              /*!< Specifies the I2C mode is master or slave */

    uint32_t ClockSpeed;        /*!< Specifies the clock frequency.
                                     This parameter must be set to a value lower than 400kHz (in Hz)*/

    uint32_t OwnAddress;        /*!< Specifies the device own address .
                                     This parameter must be a value between Min_Data = 0x00 and Max_Data = 0x3FF*/

    uint32_t OwnAddrSize;       /*!< Specifies the device own address 1 size (7-bit or 10-bit)*/

    uint32_t ClkHoldEn;         /*! clock stretching enable */

    uint32_t MasterIrq;         /*!< Specifies Master IrqEnable or not */

    uint32_t SlaveIrq;          /*!< Specifies Slave IrqEnable or not */
} LL_I2C_InitTypeDef;


/**
 * @}
 */


/** @defgroup I2C_Exported_Constants
 * @{
 */


/** @defgroup Defines used to perform compute and check in the Macros
 * @{
 */

#define LL_I2C_MAX_SPEED_STANDARD   100000U
#define LL_I2C_MAX_SPEED_FAST       400000U


/**
 * @}
 */


/** @defgroup I2C_LL_MODE  config  I2C  module as master or slave
 * @{
 */

#define LL_I2C_SLAVE_MODE   (0x00000000U)               /*!< config I2C as slave */
#define LL_I2C_MASTER_MODE  I2C_MODE                    /*!< config I2C as master*/


/**
 * @}
 */


/** @defgroup I2C_LL_MASTER_EN   config MASTER as enable or disable
 * @{
 */

#define LL_I2C_MASTER_DISABLE   (0x00000000U)           /*!< config MASTER as disable    */
#define LL_I2C_MASTER_ENABLE    I2C_MASTER_EN           /*!< config MASTER as enable     */


/**
 * @}
 */


/** @defgroup I2C_LL_MASTERSSPCON_STARTEN config start bit as enable or disable
 * @{
 */

#define LL_I2C_MASTERSSPCON_NSTART  (0x00000000U)               /*!< disable generate START bit*/
#define LL_I2C_MASTERSSPCON_STARTEN I2C_MASTERSSPC0N_SEN        /*!< enable generate START bit */


/**
 * @}
 */


/** @defgroup I2C_LL_MASTERSSPCON_STARTEN config restart bit as enable or disable
 * @{
 */

#define LL_I2C_MASTERSSPCON_NRESTART    (0x00000000U)           /*!< disable generate RESTART bit*/
#define LL_I2C_MASTERSSPCON_RESTARTEN   I2C_MASTERSSPC0N_RSEN   /*!< enable generate RESTART bit */


/**
 * @}
 */


/** @defgroup I2C_LL_STOPBIT_EN config stop bit as enable or disable
 * @{
 */

#define LL_I2C_MASTERSSPCON_NSTOP   (0x00000000U)               /*!< disable generate STOP bit*/
#define LL_I2C_MASTERSSPCON_STOPEN  I2C_MASTERSSPC0N_PEN        /*!< enable generate STOP bit */


/**
 * @}
 */


/** @defgroup I2C_LL_MASTERSSPCON_RCVEN enable receive in master mode
 * @{
 */

#define LL_I2C_MASTERSSPCON_NRCV    (0x00000000U)               /*!< disable master receive */
#define LL_I2C_MASTERSSPCON_RCVEN   I2C_MASTERSSPC0N_RCEN       /*!< enable master receive  */


/**
 * @}
 */


/** @defgroup I2C_LL_MASTERSSPCON_ACKEN enable ACK in master receive mode
 * @{
 */

#define LL_I2C_MASTERSSPCON_NACK    (0x00000000U)                   /*!< disable ACK  bit*/
#define LL_I2C_MASTERSSPCON_ACK     I2C_MASTERSSPC0N_ACKEN          /*!< enable ACK bit */


/**
 * @}
 */


/** @defgroup I2C_LL_MASTERSSPSTATUS_ACKDT enable ACK in master receive mode
 * @{
 */

#define LL_I2C_MASTERSSPSTATUS_ACKDT    (0x00000000U)               /*!< master send ACK in master receive mode  */
#define LL_I2C_MASTERSSPSTATUS_NACDT    I2C_MASTERSSPSTAUS_ACKDT    /*!<  master send NACK in master receive mode*/


/**
 * @}
 */


/** @defgroup I2C_LL_MASTERSSPSTATUS_ACKSTAT enable ACK in master receive mode
 * @{
 */

#define LL_I2C_MASTERSSPSTATUS_ACK  (0x00000000U)                   /*!< slave send ACK in master receive mode  */
#define LL_I2C_MASTERSSPSTATUS_NACK I2C_MASTERSSPSTAUS_ACKSTAT      /*!< slave send NACK in master receive mode*/


/**
 * @}
 */


/** @defgroup I2C_LL_MASTERSSPSTATUS_BF indicate buffer
 * @{
 */
#define LL_I2C_MASTERSSPSTATUS_BUFEMPTY (0x00000000U)               /*!< data will going to send ,buffer is full */
#define LL_I2C_MASTERSSPSTATUS_BUFFULL  I2C_MASTERSSPSTAUS_BF       /*!< data sended finish ,buffer empty */


/** @defgroup I2C_LL_MasterSTATE select
 * @{
 */
#define LL_I2C_FLAG_MASTERSTART     I2C_MASTERSSPSTAUS_S
#define LL_I2C_FLAG_MASTERSTOP      I2C_MASTERSSPSTAUS_P
#define LL_I2C_FLAG_MASTERRW        I2C_MASTERSSPSTAUS_RW
#define LL_I2C_FLAG_MASTERBUSY      I2C_MASTERSSPSTAUS_RW
#define LL_I2C_FLAG_MASTERWCOL      I2C_MASTERSSPSTAUS_WCOL
#define LL_I2C_FLAG_MASTERBUFFULL   I2C_MASTERSSPSTAUS_BF
#define LL_I2C_FLAG_MASTERNACKDATA  I2C_MASTERSSPSTAUS_ACKDT
#define LL_I2C_FLAG_MASTERNACK      I2C_MASTERSSPSTAUS_ACKSTAT
#define LL_I2C_FLAG_MASTERALL       I2C_MASTERSSPSTAUS_ALL


/**
 * @}
 */


/** @defgroup I2C_LL_MASTERSSPSTATUS_S  indicate start bit
 * @{
 */

#define LL_I2C_MASTERSSPSTATUS_NSTART   (0x00000000U)           /*!< indicate deteced no start bit */
#define LL_I2C_MASTERSSPSTATUS_START    I2C_MASTERSSPSTAUS_S    /*!< indicate deteced  start bit */


/**
 * @}
 */


/** @defgroup I2C_LL_MASTERSSPSTATUS_P  indicate stop bit
 * @{
 */
#define LL_I2C_MASTERSSPSTATUS_NSTOP    (0x00000000U)           /*!< indicate deteced no stop bit */
#define LL_I2C_MASTERSSPSTATUS_STOP     I2C_MASTERSSPSTAUS_P    /*!< indicate deteced  stop bit */


/**
 * @}
 */


/** @defgroup I2C_LL_MASTERSSPSTATUS_RW  indicate read or write is finish or ongoning
 * @{
 */
#define LL_I2C_MASTERSSPSTATUS_NRW  (0x00000000U)               /*!< indicate read or write is finished */
#define LL_I2C_MASTERSSPSTATUS_RW   I2C_MASTERSSPSTAUS_RW       /*!< indicate read or write is ongonging */


/**
 * @}
 */


/** @defgroup I2C_LL_MASTERSSPSTATUS_WCOL  indicate write collision happened or not happened
 * @{
 */

#define LL_I2C_MASTERSSPSTATUS_NWCOL    (0x00000000U)               /*!< indicate no write collision */
#define LL_I2C_MASTERSSPSTATUS_WCOL     I2C_MASTERSSPSTAUS_WCOL     /*!< indicate write collision happened */


/**
 * @}
 */


/** @defgroup I2C_LL_MASTERSSPBRG_BRG  indicate SCL Baudrate
 * @{
 */

#define LL_I2C_MASTERSSPBRG_BRG I2C_MASTERSSPBRG                 /*!< config SCL Baudrate*/


/**
 * @}
 */


/** @defgroup I2C_LL_MASTERSSPBUF_DATA  master read write data
 * @{
 */

#define LL_I2C_MASTERSSPBUF_DATA I2C_MASTERSSPBUF                /*!< buffer data mask*/


/**
 * @}
 */


/** @defgroup I2C_LL_MASTERSSSPIR_IF  master interrupt flag
 * @{
 */

#define LL_I2C_MASTERSSPIR_NIF  (0x00000000U)                       /*!< no interrupt flag */
#define LL_I2C_MASTERSSPIR_IF   I2C_MASTERSSPIR_IF                  /*!< set interrupt flag */


/**
 * @}
 */


/** @defgroup I2C_LL_MASTERSSSPIR_IE  master interrupt en
 * @{
 */

#define LL_I2C_MASTERSSPIR_NIE  (0x00000000U)                   /*!< interrupt enable  disable*/
#define LL_I2C_MASTERSSPIR_IE   I2C_MASTERSSPIR_IE              /*!< interrupt enable  */


/**
 * @}
 */


/** @defgroup I2C_LL_SLAVESSPCON_A10EN config start bit as enable or disable
 * @{
 */

#define LL_I2C_SLAVESSPCON_A7   (0x00000000U)                   /*!< slave seven bit address */
#define LL_I2C_SLAVESSPCON_A10  I2C_SLAVESSPC0N_A10EN           /*!< slave ten bit address */


/**
 * @}
 */


/** @defgroup I2C_LL_SLAVESSPCON_CKP config restart bit as enable or disable
 * @{
 */

#define LL_I2C_SLAVESSPCON_CKP I2C_SLAVESSPC0N_CKP             /*!< write 1 to release scl bus,write 0 meaningless  */


/**
 * @}
 */


/** @defgroup I2C_LL_SLAVESSPCON_ACKEN  config slave send ACK or not after receive data
 * @{
 */

#define LL_I2C_SLAVESSPCON_NACK     (0x00000000U)               /*!< slave not send ack */
#define LL_I2C_SLAVESSPCON_ACKEN    I2C_SLAVESSPC0N_ACKEN       /*!< slave send ack */


/**
 * @}
 */


/** @defgroup I2C_LL_SLAVESSPCON_CKSEN  config clock stretching enable
 * @{
 */

#define LL_I2C_SLAVESSPCON_NCKS     (0x00000000U)               /*!< not support clock stretching function */
#define LL_I2C_SLAVESSPCON_CKSEN    I2C_SLAVESSPC0N_CKSEN       /*!< enable clk stretching function ,slave set scl "0" until write CKSEN */


/**
 * @}
 */


/** @defgroup I2C_LL_SLAVESSPCON_RST  rst all flag and state machine
 * @{
 */

#define LL_I2C_SLAVESSPCON_RST I2C_SLAVESSPC0N_RST             /*!< rst all flag and state machine */


/**
 * @}
 */


/** @defgroup I2C_LL_MasterSTATE select
 * @{
 */
#define LL_I2C_FLAG_SLAVESTART  I2C_SLAVESSPSTAUS_S
#define LL_I2C_FLAG_SLAVESTOP   I2C_SLAVESSPSTAUS_P
#define LL_I2C_FLAG_SLAVEWCOL   I2C_SLAVESSPSTAUS_WCOL
#define LL_I2C_FLAG_SLAVEOV     I2C_SLAVESSPSTAUS_SSPOV
#define LL_I2C_FLAG_SLAVEWRITE  I2C_SLAVESSPSTAUS_RW
#define LL_I2C_FLAG_SLAVERW     I2C_SLAVESSPSTAUS_RW
#define LL_I2C_FLAG_SLAVEDA     I2C_SLAVESSPSTAUS_DA
#define LL_I2C_FLAG_SLAVEBF     I2C_SLAVESSPSTAUS_BF
#define LL_I2C_FLAG_SLAVEALL    I2C_SLAVESSPSTAUS_ALL


/** @defgroup I2C_LL_SLAVESSPSTAT_BF  received one byte address or data  and rw is 0 ,
 * @{
 */

#define LL_I2C_SLAVESSPSTAT_BUFEMPTY    (0x00000000U)           /*!< slave reveive buffer empty   */
#define LL_I2C_SLAVESSPSTAT_BUFFULL     I2C_SLAVESSPSTAUS_BF    /*!< slave reveive buffer full flag   */


/**
 * @}
 */


/** @defgroup I2C_LL_SLAVESSPSTAT_DA  received one byte is address or data
 * @{
 */

#define LL_I2C_SLAVESSPSTAT_AD  (0x00000000U)                   /*!< received one byte is address */
#define LL_I2C_SLAVESSPSTAT_DA  I2C_SLAVESSPSTAUS_DA            /*!< received one byte is data   */


/**
 * @}
 */


/** @defgroup I2C_LL_SLAVESSPSTAT_RW  indicate slave is in reading mode or writting mode
 * @{
 */

#define LL_I2C_SLAVESSPSTAT_READ    (0x00000000U)               /*!< indicate slave is in reading mode */
#define LL_I2C_SLAVESSPSTAT_WRITE   I2C_SLAVESSPSTAUS_RW        /*!< indicate slave is in writting mode*/


/**
 * @}
 */


/** @defgroup LL_I2C_SLAVESSPSTAT_OVERFLOW  indicate slave receive not overflow
 * @{
 */

#define LL_I2C_SLAVESSPSTAT_NOVERFLOW   (0x00000000U)           /*!< indicate slave reveive overflow */
#define LL_I2C_SLAVESSPSTAT_OVERFLOW    I2C_SLAVESSPSTAUS_SSPOV /*!< indicate slave reveive not overflow */


/**
 * @}
 */


/** @defgroup LL_I2C_SLAVESSPSTAT_WCOL  indicate slave sendbuffer write collision
 * @{
 */

#define LL_I2C_SLAVESSPSTAT_NWCOL   (0x00000000U)                   /*!< indicate slave sendbuffer write no collision */
#define LL_I2C_SLAVESSPSTAT_WCOL    I2C_SLAVESSPSTAUS_WCOL          /*!< indicate slave sendbuffer write collision */


/**
 * @}
 */


/** @defgroup I2C_LL_SLAVESSPSTAT_P  indicate slave detect stop bit
 * @{
 */

#define LL_I2C_SLAVESSPSTAT_NSTOP   (0x00000000U)                   /*!< indicate slave not detect stop bit */
#define LL_I2C_SLAVESSPSTAT_STOP    I2C_SLAVESSPSTAUS_P             /*!< indicate slave detect stop bit */


/**
 * @}
 */


/** @defgroup I2C_LL_SLAVESSPSTAT_S  indicate slave detect stop bit
 * @{
 */

#define LL_I2C_SLAVESSPSTAT_NSTART  (0x00000000U)               /*!< indicate slave not detect start bit */
#define LL_I2C_SLAVESSPSTAT_START   I2C_SLAVESSPSTAUS_S         /*!< indicate slave detect start bit */


/**
 * @}
 */


/** @defgroup I2C_LL_SLAVESSPBUF_DATA slave read write data
 * @{
 */
#define LL_I2C_SLAVESSPBUF_DATA I2C_SLAVESSPBUF                 /*!< buffer data mask*/


/**
 * @}
 */


/** @defgroup I2C_LL_SLAVESSPBUF_SLAVEADDR slave address
 * @{
 */

#define LL_I2C_SLAVESSPADDR_ADDR I2C_SLAVESSPADDR            /*!< slave address*/


/**
 * @}
 */


/** @defgroup I2C_LL_SLAVESSPIR_IF  slave  interrupt flag
 * @{
 */

#define LL_I2C_SLAVESSPIR_NIF   (0x00000000U)               /*!< no interrupt flag */
#define LL_I2C_SLAVESSPIR_IF    I2C_SLAVESSPIR_IF           /*!< set interrupt flag */


/**
 * @}
 */


/** @defgroup I2C_LL_SLAVESSSPIR_IE  slave interrupt en
 * @{
 */

#define LL_I2C_SLAVESSPIR_NIE   (0x00000000U)               /*!< slave interrupt enable  */
#define LL_I2C_SLAVESSPIR_IE    I2C_SLAVESSPIR_IE           /*!< slave interrupt disable */


/**
 * @}
 */


/**
 * @}
 */


/** @defgroup I2C_Exported_Functions
 * @{
 */


/** @defgroup I2C_Exported_Master_Functions
 * @{
 */


/**
 * @brief  I2C  MasterModeEnable
 * @rmtoll MEN     I2C_MODE          LL_I2C_MasterModeEnable
 * @param  CMPx CMP Instance
 * @retval None
 */
__STATIC_INLINE void LL_I2C_EnableMasterMode(I2C_TypeDef *I2Cx)
{
    SET_BIT(I2Cx->MEN, I2C_MODE);
}


/**
 * @brief  I2C  LL_I2C_SlaveModeEnable
 * @rmtoll MEN     I2C_MODE          LL_I2C_EnableSlave
 * @param  CMPx CMP Instance
 * @retval None
 */
__STATIC_INLINE void LL_I2C_EnableSlave(I2C_TypeDef *I2Cx)
{
    CLEAR_BIT(I2Cx->MEN, I2C_MODE | I2C_MASTER_EN);
    //SET_BIT(I2Cx->MEN, I2C_MODE);
}


/**
 * @brief  I2C  Master Enable
 * @rmtoll MEN     I2C_MODEandI2C_MASTER_EN          LL_I2C_EnableMaster
 * @param  CMPx CMP Instance
 * @retval None
 */
__STATIC_INLINE void LL_I2C_EnableMaster(I2C_TypeDef *I2Cx)
{
    SET_BIT(I2Cx->MEN, I2C_MODE | I2C_MASTER_EN);
}


/**
 * @brief  I2C  Master Disable
 * @rmtoll MEN     I2C_MASTER_EN          LL_I2C_DisableMaster
 * @param  CMPx CMP Instance
 * @retval None
 */
__STATIC_INLINE void LL_I2C_DisableMaster(I2C_TypeDef *I2Cx)
{
    CLEAR_BIT(I2Cx->MEN, I2C_MASTER_EN);
}


/**
 * @brief  I2C  I2C_LL_Disable;
 * @rmtoll CTRL     MEN          LL_I2CDisable
 * @param  CMPx CMP Instance
 * @retval None
 */
__STATIC_INLINE void LL_I2C_Disable(I2C_TypeDef *I2Cx)
{
    //MODIFY_REG(I2Cx->MEN,I2C_MODE_Msk|I2C_MASTER_EN_Msk,LL_I2C_MASTER_MODE|LL_I2C_MASTER_DISABLE);
    CLEAR_BIT(I2Cx->MEN, I2C_MASTER_EN | I2C_MODE);
}


/**
 * @brief  Generate a START  condition (master mode).
 * @note   The START bit can be set when slave not hode.
 * @rmtoll MSSPCON          I2C_MASTERSSPC0N_SEN         LL_I2C_GenerateStart
 * @param  I2Cx I2C Instance.
 * @retval None
 */
__STATIC_INLINE void LL_I2C_GenerateStart(I2C_TypeDef *I2Cx)
{
    SET_BIT(I2Cx->MSSPCON, I2C_MASTERSSPC0N_SEN);
}


/**
 * @brief  Generate a ReSTART  condition (master mode).
 * @note   The START bit can be set when slave not hode.
 * @rmtoll MSSPCON          I2C_MASTERSSPC0N_RSEN         LL_I2C_GenerateReStart
 * @param  I2Cx I2C Instance.
 * @retval None
 */
__STATIC_INLINE void LL_I2C_GenerateReStart(I2C_TypeDef *I2Cx)
{
    SET_BIT(I2Cx->MSSPCON, I2C_MASTERSSPC0N_RSEN);
}


/**
 * @brief Generate a stop  condition (master mode).
 * @note
 * @rmtoll CR1              I2C_MASTERSSPC0N_PEN            LL_I2C_EnableBitPOS
 * @param  I2Cx I2C Instance.
 * @retval None
 */
__STATIC_INLINE void LL_I2C_GenerateStop(I2C_TypeDef *I2Cx)
{
    SET_BIT(I2Cx->MSSPCON, I2C_MASTERSSPC0N_PEN);
}


/**
 * @brief enable master receive  (master mode).
 * @note
 * @rmtoll MSSPCON          I2C_MASTERSSPC0N_RCEN           LL_I2C_EnableMasterRecv
 * @param  I2Cx I2C Instance.
 * @retval None
 */
__STATIC_INLINE void LL_I2C_EnableMasterRecv(I2C_TypeDef *I2Cx)
{
    SET_BIT(I2Cx->MSSPCON, I2C_MASTERSSPC0N_RCEN);
}


/**
 * @brief enable master send ACK function (master mode).
 * @note
 * @rmtoll MSSPCON          I2C_MASTERSSPC0N_ACKEN_Msk          LL_I2C_SetMasterACKEn
 * @param  I2Cx I2C Instance.
 * @param  acken
 *         @arg @ref LL_I2C_MASTERSSPCON_NACK
 *         @arg @ref LL_I2C_MASTERSSPCON_ACK
 * @retval None
 */
__STATIC_INLINE void LL_I2C_SetMasterACKEn(I2C_TypeDef *I2Cx, uint32_t acken)
{
    MODIFY_REG(I2Cx->MSSPCON, I2C_MASTERSSPC0N_ACKEN_Msk, acken);
}


/**
 * @brief  master send ACK function (master mode).
 * @note
 * @rmtoll MSSPSTAT         I2C_MASTERSSPSTAUS_ACKDT          LL_I2C_SendMasterACK
 * @rmtoll MSSPCON          I2C_MASTERSSPC0N_ACKEN
 * @param  I2Cx I2C Instance.
 * @retval None
 */
__STATIC_INLINE void LL_I2C_SendMasterACK(I2C_TypeDef *I2Cx)
{
    CLEAR_BIT(I2Cx->MSSPSTAT, I2C_MASTERSSPSTAUS_ACKDT);
    SET_BIT(I2Cx->MSSPCON, I2C_MASTERSSPC0N_ACKEN);
}


/**
 * @brief  master send ACK function (master mode).
 * @note
 * @rmtoll MSSPSTAT         I2C_MASTERSSPSTAUS_ACKDT          LL_I2C_SendMasterNACK
 * @rmtoll MSSPCON          I2C_MASTERSSPC0N_ACKEN
 * @param  I2Cx I2C Instance.
 * @retval None
 */
__STATIC_INLINE void LL_I2C_SendMasterNACK(I2C_TypeDef *I2Cx)
{
    SET_BIT(I2Cx->MSSPSTAT, I2C_MASTERSSPSTAUS_ACKDT);
    SET_BIT(I2Cx->MSSPCON, I2C_MASTERSSPC0N_ACKEN);
}


/**
 * @brief  get if the I2C master Flag is set or not
 * @rmtoll STATUS               LL_I2C_GetMFlag
 * @param  I2Cx I2C Instance
 * @param  flag
 *         @arg @ref LL_I2C_FLAG_MASTERSTART
 *         @arg @ref LL_I2C_FLAG_MASTERSTOP
 *         @arg @ref LL_I2C_FLAG_MASTERBUSY /  LL_I2C_FLAG_MASTERRW
 *         @arg @ref LL_I2C_FLAG_MASTERWCOL
 *         @arg @ref LL_I2C_FLAG_MASTERBUFFULL
 *         @arg @ref LL_I2C_FLAG_MASTERNACKDATA
 *         @arg @ref LL_I2C_FLAG_MASTERNACK
 *         @arg @ref LL_I2C_FLAG_MASTERALL
 * @retval State of bit (1 or 0).
 */
__STATIC_INLINE uint32_t LL_I2C_GetMFlag(I2C_TypeDef *I2Cx, uint32_t flag)
{
    return (READ_BIT(I2Cx->MSSPSTAT, flag));
}


/**
 * @brief  clear the I2C master Flag
 * @rmtoll STATUS               LL_I2C_ClearMFlag
 * @param  I2Cx I2C Instance
 * @param  flag
 *         @arg @ref LL_I2C_FLAG_MASTERWCOL
 *         @arg @ref LL_I2C_FLAG_MASTERNACK
 * @retval State of bit (1 or 0).
 */
__STATIC_INLINE void LL_I2C_ClearMFlag(I2C_TypeDef *I2Cx, uint32_t flag)
{
    CLEAR_BIT(I2Cx->MSSPSTAT, flag);
}


/**
 * @brief  clear the I2C master All Flag
 * @rmtoll STATUS               LL_I2C_ClearAllMFlag
 * @param  I2Cx I2C Instance
 * @retval State of bit (1 or 0).
 */
__STATIC_INLINE void LL_I2C_ClearAllMFlag(I2C_TypeDef *I2Cx)
{
    CLEAR_BIT(I2Cx->MSSPSTAT, LL_I2C_FLAG_MASTERALL);
}


/**
 * @brief  Indicate the status of Start Bit (master mode).
 *         SET: When Start condition is generated.
 * @rmtoll MSSPSTAT          LL_I2C_MASTERSSPSTATUS_START            LL_I2C_IsActiveMFlag_StartBit
 * @param  I2Cx I2C Instance.
 * @retval State of bit (1 or 0).1 is indicate receive start bit,0 is not received start bit
 */
__STATIC_INLINE uint32_t LL_I2C_IsActiveMFlag_StartBit(I2C_TypeDef *I2Cx)
{
    return (READ_BIT(I2Cx->MSSPSTAT, LL_I2C_MASTERSSPSTATUS_START) == (LL_I2C_MASTERSSPSTATUS_START));
}


/**
 * @brief  Indicate the status of Stop Bit (master mode).
 *         SET: When stop condition is generated.
 * @rmtoll MSSPSTAT          LL_I2C_MASTERSSPSTATUS_STOP            LL_I2C_IsActiveMFlag_StopBit
 * @param  I2Cx I2C Instance.
 * @retval State of bit (1 or 0).1 is indicate receive stop bit,0 is not received stopt bit
 */
__STATIC_INLINE uint32_t LL_I2C_IsActiveMFlag_StopBit(I2C_TypeDef *I2Cx)
{
    return (READ_BIT(I2Cx->MSSPSTAT, LL_I2C_MASTERSSPSTATUS_STOP) == (LL_I2C_MASTERSSPSTATUS_STOP));
}


/**
 * @brief  Indicate the status of buffer (master mode).
 *         SET: When buffer is full,data or address is in buffer.
 * @rmtoll MSSPSTAT          LL_I2C_MASTERSSPSTATUS_BUFFULL         LL_I2C_IsFull_MasterTxBuff
 * @param  I2Cx I2C Instance.
 * @retval State of bit (1 or 0).1 is indicate buffer is full bit,0 indicate buffer empty
 */
__STATIC_INLINE uint32_t LL_I2C_IsFull_MasterRxBuff(I2C_TypeDef *I2Cx)
{
    return (READ_BIT(I2Cx->MSSPSTAT, LL_I2C_MASTERSSPSTATUS_BUFFULL) == (LL_I2C_MASTERSSPSTATUS_BUFFULL));
}


/**
 * @brief  Indicate the slave send status of Acknowledge (master mode).
 *
 * @rmtoll MSSPSTAT          LL_I2C_MASTERSSPSTATUS_ACK         LL_I2C_IsActiveMFlag_ACK
 * @param  I2Cx I2C Instance.
 * @retval State of bit (1 or 0).1 is indicate master receive  ack,0 indicate reveive no ack
 */
__STATIC_INLINE uint32_t LL_I2C_IsActiveMFlag_ACK(I2C_TypeDef *I2Cx)
{
    return (READ_BIT(I2Cx->MSSPSTAT, I2C_MASTERSSPSTAUS_ACKSTAT_Msk) == (LL_I2C_MASTERSSPSTATUS_ACK));
}


/**
 * @brief  Indicate the  data transfer is finished or not (master mode).
 *
 * @rmtoll MSSPSTAT          I2C_MASTERSSPSTAUS_RW         LL_I2C_IsActiveMFlag_RW
 * @rmtoll MSSPSTAT          I2C_MASTERSSPSTAUS_RW         LL_I2C_IsActiveMFlag_Busy
 * @param  I2Cx I2C Instance.
 * @retval State of bit (1 or 0).1 is indicate data is communcating,0 indicate data is finished.
 */
__STATIC_INLINE uint32_t LL_I2C_IsActiveMFlag_RW(I2C_TypeDef *I2Cx)
{
    return (READ_BIT(I2Cx->MSSPSTAT, I2C_MASTERSSPSTAUS_RW) == (I2C_MASTERSSPSTAUS_RW));
}


__STATIC_INLINE uint32_t LL_I2C_IsActiveMFlag_Busy(I2C_TypeDef *I2Cx)
{
    return (READ_BIT(I2Cx->MSSPSTAT, I2C_MASTERSSPSTAUS_RW) == (I2C_MASTERSSPSTAUS_RW));
}


/**
 * @brief  Indicate the write collision is happened or not (master mode).
 *
 * @rmtoll MSSPSTAT          I2C_MASTERSSPSTAUS_WCOL          LL_I2C_IsActiveMFlag_WCOL
 * @param  I2Cx I2C Instance.
 * @retval State of bit (1 or 0).1 is indicate data is happened write collision,0 indicate not happened .
 */
__STATIC_INLINE uint32_t LL_I2C_IsActiveMFlag_Wcol(I2C_TypeDef *I2Cx)
{
    return (READ_BIT(I2Cx->MSSPSTAT, I2C_MASTERSSPSTAUS_WCOL) == (I2C_MASTERSSPSTAUS_WCOL));
}


/**
 * @brief  Set the master to send  ACK
 * @rmtoll MSSPSTAT          I2C_MASTERSSPSTAUS_ACKDT          LL_I2C_SetMasterACK
 * @param  I2Cx I2C Instance.
 * @retval None
 */
__STATIC_INLINE void LL_I2C_SetMasterACK(I2C_TypeDef *I2Cx)
{
    CLEAR_BIT(I2Cx->MSSPSTAT, I2C_MASTERSSPSTAUS_ACKDT);
}


/**
 * @brief  Set the master to send  NACK
 * @rmtoll MSSPSTAT          I2C_MASTERSSPSTAUS_ACKDT          LL_I2C_SetMasterNACK
 * @param  I2Cx I2C Instance.
 * @retval None
 */
__STATIC_INLINE void LL_I2C_SetMasterNACK(I2C_TypeDef *I2Cx)
{
    SET_BIT(I2Cx->MSSPSTAT, I2C_MASTERSSPSTAUS_ACKDT);
}


/**
 * @brief  cfg I2C transfer baud .
 * @rmtoll MSSPBRG           I2C_MASTERSSPBRG            LL_I2C_SetMasterBaud
 * @param  I2Cx I2C Instance.
 * @param  Data Value between Min_Data=0x0 and Max_Data=0xFF
 * @retval None
 */
__STATIC_INLINE void LL_I2C_SetMasterBaud(I2C_TypeDef *I2Cx, uint8_t Data)
{
    MODIFY_REG(I2Cx->MSSPBRG, I2C_MASTERSSPBRG, Data);
}


/**
 * @brief  Read Master  Receive Data register SSPBUF.
 * @rmtoll MSSPBUF           I2C_MASTERSSPBUF                  LL_I2C_ReceiveData8Master
 * @param  I2Cx I2C Instance.
 * @retval Value between Min_Data=0x0 and Max_Data=0xFF
 */
__STATIC_INLINE uint8_t LL_I2C_ReceiveData8Master(I2C_TypeDef *I2Cx)
{
    return ((uint8_t)(READ_BIT(I2Cx->MSSPBUF, I2C_MASTERSSPBUF)));
}


/**
 * @brief  Write in master Transmit Data Register SSPBUF .
 * @rmtoll MSSPBUF           I2C_MASTERSSPBUF            LL_I2C_TransmitData8Master
 * @param  I2Cx I2C Instance.
 * @param  Data Value between Min_Data=0x0 and Max_Data=0xFF
 * @retval None
 */
__STATIC_INLINE void LL_I2C_TransmitData8Master(I2C_TypeDef *I2Cx, uint8_t Data)
{
    MODIFY_REG(I2Cx->MSSPBUF, I2C_MASTERSSPBUF, Data);
}


/**
 * @brief  Enable Master interrupt.
 * @rmtoll MSSPIR          I2C_MASTERSSPIR_IE       LL_I2C_EnableMasterIT\n
 * @param  I2Cx I2C Instance.
 * @retval None
 */
__STATIC_INLINE void LL_I2C_EnableMasterINT(I2C_TypeDef *I2Cx)
{
    SET_BIT(I2Cx->MSSPIR, I2C_MASTERSSPIR_IE);
}


/**
 * @brief  Disable Master interrupt.
 * @rmtoll MSSPIR          I2C_MASTERSSPIR_IE       LL_I2C_DisableMasterIT\n
 * @param  I2Cx I2C Instance.
 * @retval None
 */
__STATIC_INLINE void LL_I2C_DisableMasterINT(I2C_TypeDef *I2Cx)
{
    CLEAR_BIT(I2Cx->MSSPIR, I2C_MASTERSSPIR_IE);
}


/**
 * @brief  Get  Master interrupt flag
 * @rmtoll MSSPIR          I2C_MASTERSSPIR_IF       LL_I2C_IsActiveMFlag_IT
 * @param  I2Cx I2C Instance.
 * @retval State of bit (1 or 0).1 is indicate interrupt flag is set ,0 indicate interrupt flag is not set
 */
__STATIC_INLINE uint32_t LL_I2C_IsActiveMFlag_IT(I2C_TypeDef *I2Cx)
{
    return (READ_BIT(I2Cx->MSSPIR, I2C_MASTERSSPIR_IF) == (I2C_MASTERSSPIR_IF));
}


/**
 * @brief  Get  Master interrupt flag
 * @rmtoll MSSPIR          I2C_MASTERSSPIR_IF       LL_I2C_ClearMFlag_IT
 * @param  I2Cx I2C Instance.
 * @retval None
 */
__STATIC_INLINE void LL_I2C_ClearMFlag_IT(I2C_TypeDef *I2Cx)
{
    CLEAR_BIT(I2Cx->MSSPIR, I2C_MASTERSSPIR_IF);
}


/** @defgroup I2C_Exported_Slave_Functions
 * @{
 */


/**
 * @brief  Set the Own Address1.
 * @rmtoll SSSPADDR         I2C_SLAVESSPADDR          LL_I2C_SetSlaveAddress
 * @rmtoll SSSPCON          I2C_SLAVESSPC0N_A10EN     LL_I2C_SetSlaveAddress
 * @param  I2Cx I2C Instance.
 * @param  OwnAddress This parameter must be a value between Min_Data=0 and Max_Data=0x3FF.
 * @param  OwnAddrSize This parameter can be one of the following values:
 *         @arg @ref LL_I2C_SLAVESSPCON_A7
 *         @arg @ref LL_I2C_SLAVESSPCON_A10
 * @retval None
 */
__STATIC_INLINE void LL_I2C_SetSlaveAddress(I2C_TypeDef *I2Cx, uint32_t OwnAddress, uint32_t OwnAddrSize)
{
    MODIFY_REG(I2Cx->SSSPADDR, I2C_SLAVESSPADDR, OwnAddress);
    MODIFY_REG(I2Cx->SSSPCON, I2C_SLAVESSPC0N_A10EN, OwnAddrSize);
}


/**
 * @brief  write 1 to release SCL.
 * @note   config clock stretching enable
 * @rmtoll SSSPCON          I2C_SLAVESSPC0N_CKP      LL_I2C_ReleaseHoldSlave
 * @param  I2Cx I2C Instance.
 * @retval None
 */
__STATIC_INLINE void LL_I2C_ReleaseSlaveHold(I2C_TypeDef *I2Cx)
{
    SET_BIT(I2Cx->SSSPCON, I2C_SLAVESSPC0N_CKP);
}


/**
 * @brief  Enable Slave Clock stretching.
 * @note   config clock stretching enable
 * @rmtoll SSSPCON          CKSEN      LL_I2C_EnableClockStretching
 * @param  I2Cx I2C Instance.
 * @retval None
 */
__STATIC_INLINE void LL_I2C_EnableClockStretch(I2C_TypeDef *I2Cx)
{
    SET_BIT(I2Cx->SSSPCON, LL_I2C_SLAVESSPCON_CKSEN);
}


/**
 * @brief  Disable Slave Clock stretching.
 * @note   config clock stretching disable
 * @rmtoll SSSPCON          CKSEN      LL_I2C_DisableClockStretch
 * @param  I2Cx I2C Instance.
 * @retval None
 */
__STATIC_INLINE void LL_I2C_DisableClockStretch(I2C_TypeDef *I2Cx)
{
    CLEAR_BIT(I2Cx->SSSPCON, LL_I2C_SLAVESSPCON_CKSEN);
}


/**
 * @brief  Check if Clock stretching is enabled or disabled.
 * @rmtoll  SSSPCON          CKSEN    LL_I2C_IsEnabledClockStretching
 * @param  I2Cx I2C Instance.
 * @retval State of bit (1 or 0). 0 is enable,1 is disable
 */
__STATIC_INLINE uint32_t LL_I2C_IsEnableClockStretch(I2C_TypeDef *I2Cx)
{
    return (READ_BIT(I2Cx->SSSPCON, LL_I2C_SLAVESSPCON_CKSEN) != (LL_I2C_SLAVESSPCON_CKSEN));
}


/**
 * @brief   software write 1 to Reset i2c ,reset statemachine and all flag
 * @rmtoll SSSPCON          I2C_SLAVESSPC0N_RST         LL_I2C_Reset
 * @param  I2Cx I2C Instance.
 * @retval None
 */
__STATIC_INLINE void LL_I2C_ResetSlave(I2C_TypeDef *I2Cx)
{
    SET_BIT(I2Cx->SSSPCON, I2C_SLAVESSPC0N_RST);
    CLEAR_BIT(I2Cx->SSSPIR, I2C_SLAVESSPIR_IF);
    CLEAR_BIT(I2Cx->SSSPSTAT, LL_I2C_FLAG_SLAVEALL);
    SET_BIT(I2Cx->SSSPCON, I2C_SLAVESSPC0N_CKP);
    CLEAR_BIT(I2Cx->SSSPCON, LL_I2C_SLAVESSPCON_ACKEN);
    READ_REG(I2Cx->SSSPBUF);  //flush buffer
}


/**
 * @brief enable slave send ACK function (slave mode).
 * @note
 * @rmtoll SSSPCON          LL_I2C_SLAVESSPCON_ACKEN          LL_I2C_EnableSlaveACK
 * @param  I2Cx I2C Instance.
 * @retval None
 */
__STATIC_INLINE void LL_I2C_EnableSlaveACK(I2C_TypeDef *I2Cx)
{
    SET_BIT(I2Cx->SSSPCON, LL_I2C_SLAVESSPCON_ACKEN);
}


/**
 * @brief disable slave send ACK function (slave mode).
 * @note
 * @rmtoll SSSPCON          LL_I2C_SLAVESSPCON_ACKEN          LL_I2C_EnableSlaveACK
 * @param  I2Cx I2C Instance.
 * @retval None
 */
__STATIC_INLINE void LL_I2C_DisableSlaveACK(I2C_TypeDef *I2Cx)
{
    CLEAR_BIT(I2Cx->SSSPCON, LL_I2C_SLAVESSPCON_ACKEN);
}


/**
 * @brief  Get if the I2C slave Flag is set or not
 * @rmtoll STATUS               LL_I2C_GetFlagSlave
 * @param  I2Cx I2C Instance
 * @param  flag
 *         @arg @ref LL_I2C_FLAG_SLAVESTART
 *         @arg @ref LL_I2C_FLAG_SLAVESTOP
 *         @arg @ref LL_I2C_FLAG_SLAVEWCOL
 *         @arg @ref LL_I2C_FLAG_SLAVEOV
 *         @arg @ref LL_I2C_FLAG_SLAVEBUSY /LL_I2C_FLAG_SLAVERW
 *         @arg @ref LL_I2C_FLAG_SLAVEDA
 *         @arg @ref LL_I2C_FLAG_SLAVEBF
 *         @arg @ref LL_I2C_FLAG_SLAVEALL
 * @retval State of bit (1 or 0).
 */
__STATIC_INLINE uint32_t LL_I2C_GetSFlag(I2C_TypeDef *I2Cx, uint32_t flag)
{
    return (READ_BIT(I2Cx->SSSPSTAT, flag));
}


/**
 * @brief  Clear the I2C slave Flag
 * @rmtoll STATUS               LL_I2C_ClearSFlag
 * @param  I2Cx I2C Instance
 * @param  flag
 *         @arg @ref LL_I2C_FLAG_SLAVESTART
 *         @arg @ref LL_I2C_FLAG_SLAVESTOP
 *         @arg @ref LL_I2C_FLAG_SLAVEWCOL
 *         @arg @ref LL_I2C_FLAG_SLAVEOV
 *         @arg @ref LL_I2C_FLAG_SLAVEALL
 * @retval None.
 */
__STATIC_INLINE void LL_I2C_ClearSFlag(I2C_TypeDef *I2Cx, uint32_t flag)
{
    CLEAR_BIT(I2Cx->SSSPSTAT, flag);
}


/**
 * @brief  Clear the I2C slave All Flag
 * @rmtoll STATUS               LL_I2C_ClearAllSFlag
 * @param  I2Cx I2C Instance
 * @retval None
 */
__STATIC_INLINE void LL_I2C_ClearAllSFlag(I2C_TypeDef *I2Cx)
{
    CLEAR_BIT(I2Cx->SSSPSTAT, LL_I2C_FLAG_SLAVEALL);
}


/**
 * @brief  Indicate the status of buffer (slave mode).
 *         SET: When buffer is full,data or address is in buffer.
 * @rmtoll SSSPSTAT          I2C_SLAVESSPSTAUS_BF         LL_I2C_IsFull_SlaveBuff
 * @param  I2Cx I2C Instance.
 * @retval State of bit (1 or 0).1 is indicate buffer is full bit,0 indicate buffer empty
 */
__STATIC_INLINE uint32_t LL_I2C_IsFull_SlaveBuff(I2C_TypeDef *I2Cx)
{
    return (READ_BIT(I2Cx->SSSPSTAT, I2C_SLAVESSPSTAUS_BF) == (I2C_SLAVESSPSTAUS_BF));
}


/**
 * @brief  Indicate the received is data or address (slave mode).
 *
 * @rmtoll SSSPSTAT          I2C_SLAVESSPSTAUS_DA            LL_I2C_IsActiveSFlag_DataAddress
 * @param  I2Cx I2C Instance.
 * @retval State of bit (1 or 0).1 indicates received is address,0 indicates received is data
 */
__STATIC_INLINE uint32_t LL_I2C_IsActiveSFlag_Address(I2C_TypeDef *I2Cx)
{
    return (READ_BIT(I2Cx->SSSPSTAT, I2C_SLAVESSPSTAUS_DA) != (I2C_SLAVESSPSTAUS_DA));
}


/**
 * @brief  Indicate the direction of communication  (slave mode).
 *
 * @rmtoll SSSPSTAT          I2C_SLAVESSPSTAUS_RW            LL_I2C_IsActiveSFlag_DirRW
 * @rmtoll SSSPSTAT          I2C_SLAVESSPSTAUS_RW            LL_I2C_IsActiveSFlag_Busy
 * @param  I2Cx I2C Instance.
 * @retval State of bit (1 or 0). 1 indicates slave send data to master,0 indicates master send data to slave
 */
__STATIC_INLINE uint32_t LL_I2C_IsActiveSFlag_DirRW(I2C_TypeDef *I2Cx)
{
    return (READ_BIT(I2Cx->SSSPSTAT, I2C_SLAVESSPSTAUS_RW) == (I2C_SLAVESSPSTAUS_RW));
}


__STATIC_INLINE uint32_t LL_I2C_IsActiveSFlag_Write(I2C_TypeDef *I2Cx)
{
    return (READ_BIT(I2Cx->SSSPSTAT, LL_I2C_FLAG_SLAVEWRITE) != (LL_I2C_FLAG_SLAVEWRITE));
}


/**
 * @brief  Indicate the slave receive overflow or not (slave mode).
 *
 * @rmtoll SSSPSTAT          I2C_SLAVESSPSTAUS_SSPOV         LL_I2C_IsActiveSFlag_RecvOverFlow
 * @param  I2Cx I2C Instance.
 * @retval State of bit (1 or 0).1 is indicate overflow,0 indicate not overflow
 */
__STATIC_INLINE uint32_t LL_I2C_IsActiveSFlag_RecvOverFlow(I2C_TypeDef *I2Cx)
{
    return (READ_BIT(I2Cx->SSSPSTAT, I2C_SLAVESSPSTAUS_SSPOV) == (I2C_SLAVESSPSTAUS_SSPOV));
}


/**
 * @brief clear Slave  receive overflow flag (slave mode).
 *
 * @rmtoll SSSPSTAT          I2C_SLAVESSPSTAUS_SSPOV         LL_I2C_ClearSFlag_RecvOverFlow
 * @param  I2Cx I2C Instance.
 * @retval State of bit (1 or 0).1 is indicate overflow,0 indicate not overflow
 */
__STATIC_INLINE void LL_I2C_ClearSFlag_RecvOverFlow(I2C_TypeDef *I2Cx)
{
    CLEAR_BIT(I2Cx->SSSPSTAT, I2C_SLAVESSPSTAUS_SSPOV);
}


/**
 * @brief  Indicate the write collision is happened or not (Slave mode).
 *
 * @rmtoll SSSPSTAT          I2C_SLAVESSPSTAUS_WCOL         LL_I2C_IsActiveSFlag_WCOL
 * @param  I2Cx I2C Instance.
 * @retval State of bit (1 or 0).1 is indicate data is happened write collision,0 indicate not happened .
 */
__STATIC_INLINE uint32_t LL_I2C_IsActiveSFlag_Wcol(I2C_TypeDef *I2Cx)
{
    return (READ_BIT(I2Cx->SSSPSTAT, I2C_SLAVESSPSTAUS_WCOL) == (I2C_SLAVESSPSTAUS_WCOL));
}


/**
 * @brief  clear Slave write collision flag (Slave mode).
 *
 * @rmtoll SSSPSTAT          I2C_SLAVESSPSTAUS_WCOL         LL_I2C_ClearSFlag_WCOL
 * @param  I2Cx I2C Instance.
 * @retval
 */
__STATIC_INLINE void LL_I2C_ClearSFlag_Wcol(I2C_TypeDef *I2Cx)
{
    CLEAR_BIT(I2Cx->SSSPSTAT, I2C_SLAVESSPSTAUS_WCOL);
}


/**
 * @brief  Indicate the receive Stop bit(Slave mode).
 * SSSPSTAT
 * @rmtoll SSSPSTAT          I2C_SLAVESSPSTAUS_P         LL_I2C_IsActiveSFlag_StopBit
 * @param  I2Cx I2C Instance.
 * @retval State of bit (1 or 0).1 is indicate slave receive stopbit ,0 indicate not receive .
 */
__STATIC_INLINE uint32_t LL_I2C_IsActiveSFlag_StopBit(I2C_TypeDef *I2Cx)
{
    return (READ_BIT(I2Cx->SSSPSTAT, I2C_SLAVESSPSTAUS_P) == (I2C_SLAVESSPSTAUS_P));
}


/**
 * @brief  clear  receive Stop bit(Slave mode).
 * SSSPSTAT
 * @rmtoll SSSPSTAT          I2C_SLAVESSPSTAUS_P         LL_I2C_ClearSFlag_StopBit
 * @param  I2Cx I2C Instance.
 * @retval
 */
__STATIC_INLINE void LL_I2C_ClearSFlag_StopBit(I2C_TypeDef *I2Cx)
{
    CLEAR_BIT(I2Cx->SSSPSTAT, I2C_SLAVESSPSTAUS_P);
}


/**
 * @brief  Indicate the receive start bit(Slave mode).
 *
 * @rmtoll SSSPSTAT          I2C_SLAVESSPSTAUS_S         LL_I2C_IsActiveSFlag_StartBit
 * @param  I2Cx I2C Instance.
 * @retval State of bit (1 or 0).1 is indicate slave receive startbit ,0 indicate not receive .
 */
__STATIC_INLINE uint32_t LL_I2C_IsActiveSFlag_StartBit(I2C_TypeDef *I2Cx)
{
    return (READ_BIT(I2Cx->SSSPSTAT, I2C_SLAVESSPSTAUS_S) == (I2C_SLAVESSPSTAUS_S));
}


/**
 * @brief  Indicate the receive LL_I2C_ClearSFlag_StartBit bit(Slave mode).
 *
 * @rmtoll SSSPSTAT          I2C_SLAVESSPSTAUS_S         LL_I2C_IsActiveSFlag_StartBit
 * @param  I2Cx I2C Instance.
 * @retval
 */
__STATIC_INLINE void LL_I2C_ClearSFlag_StartBit(I2C_TypeDef *I2Cx)
{
    CLEAR_BIT(I2Cx->SSSPSTAT, I2C_SLAVESSPSTAUS_S);
}


/**
 * @brief  Read Slave  Receive Data register SSPBUF.
 * @rmtoll SSSPBUF           I2C_SLAVESSPBUF                  LL_I2C_ReceiveData8Slave
 * @param  I2Cx I2C Instance.
 * @retval Value between Min_Data=0x0 and Max_Data=0xFF
 */
__STATIC_INLINE uint8_t LL_I2C_ReceiveData8Slave(I2C_TypeDef *I2Cx)
{
    return ((uint8_t)(READ_REG(I2Cx->SSSPBUF)));
}


/**
 * @brief  Write in Slave Transmit Data Register SSPBUF .
 * @rmtoll SSSPBUF           DR            LL_I2C_TransmitData8Slave
 * @param  I2Cx I2C Instance.
 * @param  Data Value between Min_Data=0x0 and Max_Data=0xFF
 * @retval None
 */
__STATIC_INLINE void LL_I2C_TransmitData8Slave(I2C_TypeDef *I2Cx, uint8_t Data)
{
    MODIFY_REG(I2Cx->SSSPBUF, I2C_SLAVESSPBUF, Data);
}


/**
 * @brief  Enable Slave interrupt.
 * @rmtoll SSSPIR          I2C_SLAVESSPIR_IE       LL_I2C_EnableSlaveIT
 * @param  I2Cx I2C Instance.
 * @retval None
 */
__STATIC_INLINE void LL_I2C_EnableSlaveINT(I2C_TypeDef *I2Cx)
{
    SET_BIT(I2Cx->SSSPIR, I2C_SLAVESSPIR_IE);
}


/**
 * @brief  Disable Slave interrupt.
 * @rmtoll SSSPIR          I2C_SLAVESSPIR_IE       LL_I2C_DisableSlaveIT
 * @param  I2Cx I2C Instance.
 * @retval None
 */
__STATIC_INLINE void LL_I2C_DisableSlaveINT(I2C_TypeDef *I2Cx)
{
    CLEAR_BIT(I2Cx->SSSPIR, I2C_SLAVESSPIR_IE);
}


/**
 * @brief  Get  Slave interrupt flag
 * @rmtoll SSSPIR          I2C_SLAVESSPIR_IF       LL_I2C_IsActiveSFlag_IT
 * @param  I2Cx I2C Instance.
 * @retval State of bit (1 or 0).1 is indicate interrupt flag is set ,0 indicate interrupt flag is not set
 */
__STATIC_INLINE uint32_t LL_I2C_IsActiveSFlag_IT(I2C_TypeDef *I2Cx)
{
    return (READ_BIT(I2Cx->SSSPIR, I2C_SLAVESSPIR_IF) == (I2C_SLAVESSPIR_IF));
}


/**
 * @brief  Clear  Slave interrupt flag
 * @rmtoll SSSPIR          I2C_SLAVESSPIR_IF       LL_I2C_ClearSFlag_IT
 * @param  I2Cx I2C Instance.
 * @retval None
 */
__STATIC_INLINE void LL_I2C_ClearSFlag_IT(I2C_TypeDef *I2Cx)
{
    CLEAR_BIT(I2Cx->SSSPIR, I2C_SLAVESSPIR_IF);
}


void LL_I2C_Init(I2C_TypeDef *I2Cx, LL_I2C_InitTypeDef *I2C_InitStruct);


/**
 * @}
 */


/**
 * @}
 */


/**
 * @}
 */


/**
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif /* __FM15F3XX_LL_I2C_H */


/************************ (C) COPYRIGHT Fudan Microelectronics *****END OF FILE****/

