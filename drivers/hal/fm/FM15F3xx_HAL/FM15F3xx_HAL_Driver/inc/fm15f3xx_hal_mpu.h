/**
 ******************************************************************************
 * @file    fm15f3xx_hal_mpu.h
 * @author  SRG
 * @version V1.0.0
 * @date    2020-03-17
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */

#ifndef FM15F3XX_HAL_MPU_H
#define FM15F3XX_HAL_MPU_H
#ifdef __cplusplus
extern "C" {
#endif


/** @addtogroup FM15F3xx_HAL_Driver
 * @{
 */


/** @addtogroup MPU
 * @{
 */

/* Exported types ------------------------------------------------------------*/


/** @defgroup MPU_Exported_Types MPU Exported Types
 * @{
 */

/* Includes ------------------------------------------------------------------*/
#include "fm15f3xx_conf.h"
#include "fm15f3xx_ll_mpu.h"

/* Private types -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private constants ---------------------------------------------------------*/
/* Private macros ------------------------------------------------------------*/
/* ExSPIed functions --------------------------------------------------------*/
/*! @brief MPU region config. */
typedef struct {
    uint32_t    RegionNum;                  /*!< Specifies the region number.*/
    uint32_t    StartAddress;               /*!< Specifies the start address.*/
    uint32_t    EndAddress;                 /*!< Specifies the start address.*/
    uint32_t    AccessRights1;              /*!< Specifies the access right1.*/
    uint32_t    AccessRights2;              /*!< Specifies the access right2.*/
    uint32_t    AccessRights3;              /*!< Specifies the access right3.*/
    uint32_t    SetCtrl;                    /*!< Specifies the set ctrl reg.*/
    uint32_t    SetSwCtl;                   /*!< Specifies the set swctl reg.*/
} HAL_MPU_RegionTypeDef;

/*! @brief MPU region right config. */
typedef struct {
    uint32_t    RegionNum;                  /*!< Specifies the region number.*/
    uint32_t    AccessRights1;              /*!< Specifies the access right1.*/
    uint32_t    AccessRights2;              /*!< Specifies the access right2.*/
    uint32_t    AccessRights3;              /*!< Specifies the access right3.*/
    uint32_t    SetCtrl;                    /*!< Specifies the set ctrl reg.*/
    uint32_t    SetSwCtl;                   /*!< Specifies the set swctl reg.*/
} HAL_MPU_RightTypeDef;

/*! @brief MPU detail error access information. */
typedef struct {
    uint32_t    Status;                     /*!< Specifies the status.*/
    uint32_t    Address;                    /*!< Specifies the address.*/
    uint32_t    Details;                    /*!< Specifies the details.*/
} HAL_MPU_AccessErrTypeDef;

/* Private macros ------------------------------------------------------------*/


/** @defgroup MPU_Private_Macros MPU Private Macros
 * @{
 */
#define HAL_MPU_REGION_COUNT    (8U)        /*!< MPU region count */
#define HAL_MPU_ACCESS_RIGHT_N  0x00        /*!< MPU access right none */

#define HAL_MPU_REGION_NUM_0    0x00        /*!< MPU region0 */
#define HAL_MPU_REGION_NUM_1    (0x01)      /*!< MPU region1 */
#define HAL_MPU_REGION_NUM_2    (0x02)      /*!< MPU region2 */
#define HAL_MPU_REGION_NUM_3    (0x03)      /*!< MPU region3 */
#define HAL_MPU_REGION_NUM_4    (0x04)      /*!< MPU region4 */
#define HAL_MPU_REGION_NUM_5    (0x05)      /*!< MPU region5 */
#define HAL_MPU_REGION_NUM_6    (0x06)      /*!< MPU region6 */
#define HAL_MPU_REGION_NUM_7    (0x07)      /*!< MPU region7 */

#define HAL_MPU_REGION_DMA_DSP_N    0x00    /*!< MPU region dma dsp right none */
#define HAL_MPU_REGION_SJUMPE_N     0x00    /*!< MPU region sjumpe right none */

#define HAL_MPU_REGIONE_ENABLE  LL_MPU_REGIONE_ENABLE
#define HAL_MPU_REGIONE_DISABLE 0

#define HAL_MPU_AUTHA_DSPWA LL_MPU_AUTHA_DSPWA
#define HAL_MPU_AUTHA_DSPRA LL_MPU_AUTHA_DSPRA
#define HAL_MPU_AUTHA_DMAWA LL_MPU_AUTHA_DMAWA
#define HAL_MPU_AUTHA_DMARA LL_MPU_AUTHA_DMARA
#define HAL_MPU_AUTHB_R0UPX LL_MPU_AUTHB_R0UPX
#define HAL_MPU_AUTHB_R0UPW LL_MPU_AUTHB_R0UPW
#define HAL_MPU_AUTHB_R0UPR LL_MPU_AUTHB_R0UPR
#define HAL_MPU_AUTHB_R0PX  LL_MPU_AUTHB_R0PX
#define HAL_MPU_AUTHB_R0PW  LL_MPU_AUTHB_R0PW
#define HAL_MPU_AUTHB_R0PR  LL_MPU_AUTHB_R0PR
#define HAL_MPU_AUTHB_R1UPX LL_MPU_AUTHB_R1UPX
#define HAL_MPU_AUTHB_R1UPW LL_MPU_AUTHB_R1UPW
#define HAL_MPU_AUTHB_R1UPR LL_MPU_AUTHB_R1UPR
#define HAL_MPU_AUTHB_R1PX  LL_MPU_AUTHB_R1PX
#define HAL_MPU_AUTHB_R1PW  LL_MPU_AUTHB_R1PW
#define HAL_MPU_AUTHB_R1PR  LL_MPU_AUTHB_R1PR
#define HAL_MPU_AUTHB_R2UPX LL_MPU_AUTHB_R2UPX
#define HAL_MPU_AUTHB_R2UPW LL_MPU_AUTHB_R2UPW
#define HAL_MPU_AUTHB_R2UPR LL_MPU_AUTHB_R2UPR
#define HAL_MPU_AUTHB_R2PX  LL_MPU_AUTHB_R2PX
#define HAL_MPU_AUTHB_R2PW  LL_MPU_AUTHB_R2PW
#define HAL_MPU_AUTHB_R2PR  LL_MPU_AUTHB_R2PR
#define HAL_MPU_AUTHB_R3UPX LL_MPU_AUTHB_R3UPX
#define HAL_MPU_AUTHB_R3UPW LL_MPU_AUTHB_R3UPW
#define HAL_MPU_AUTHB_R3UPR LL_MPU_AUTHB_R3UPR
#define HAL_MPU_AUTHB_R3PX  LL_MPU_AUTHB_R3PX
#define HAL_MPU_AUTHB_R3PW  LL_MPU_AUTHB_R3PW
#define HAL_MPU_AUTHB_R3PR  LL_MPU_AUTHB_R3PR
#define HAL_MPU_AUTHC_R4UPX LL_MPU_AUTHC_R4UPX
#define HAL_MPU_AUTHC_R4UPW LL_MPU_AUTHC_R4UPW
#define HAL_MPU_AUTHC_R4UPR LL_MPU_AUTHC_R4UPR
#define HAL_MPU_AUTHC_R4PX  LL_MPU_AUTHC_R4PX
#define HAL_MPU_AUTHC_R4PW  LL_MPU_AUTHC_R4PW
#define HAL_MPU_AUTHC_R4PR  LL_MPU_AUTHC_R4PR
#define HAL_MPU_AUTHC_R5UPX LL_MPU_AUTHC_R5UPX
#define HAL_MPU_AUTHC_R5UPW LL_MPU_AUTHC_R5UPW
#define HAL_MPU_AUTHC_R5UPR LL_MPU_AUTHC_R5UPR
#define HAL_MPU_AUTHC_R5PX  LL_MPU_AUTHC_R5PX
#define HAL_MPU_AUTHC_R5PW  LL_MPU_AUTHC_R5PW
#define HAL_MPU_AUTHC_R5PR  LL_MPU_AUTHC_R5PR
#define HAL_MPU_AUTHC_R6UPX LL_MPU_AUTHC_R6UPX
#define HAL_MPU_AUTHC_R6UPW LL_MPU_AUTHC_R6UPW
#define HAL_MPU_AUTHC_R6UPR LL_MPU_AUTHC_R6UPR
#define HAL_MPU_AUTHC_R6PX  LL_MPU_AUTHC_R6PX
#define HAL_MPU_AUTHC_R6PW  LL_MPU_AUTHC_R6PW
#define HAL_MPU_AUTHC_R6PR  LL_MPU_AUTHC_R6PR
#define HAL_MPU_AUTHC_R7UPX LL_MPU_AUTHC_R7UPX
#define HAL_MPU_AUTHC_R7UPW LL_MPU_AUTHC_R7UPW
#define HAL_MPU_AUTHC_R7UPR LL_MPU_AUTHC_R7UPR
#define HAL_MPU_AUTHC_R7PX  LL_MPU_AUTHC_R7PX
#define HAL_MPU_AUTHC_R7PW  LL_MPU_AUTHC_R7PW
#define HAL_MPU_AUTHC_R7PR  LL_MPU_AUTHC_R7PR


/** @defgroup MPU Private macros to check input parameters
 * @{
 */
#define IS_MPU_ALL_INSTANCE( INSTANCE ) ( (INSTANCE) == MPU)
#define IS_MPU_RIGHT( RIGHT )           ( ( (RIGHT) == HAL_MPU_AUTHA_DSPWA) || \
                                          ( (RIGHT) == HAL_MPU_AUTHA_DSPRA) || \
                                          ( (RIGHT) == HAL_MPU_AUTHA_DMAWA) || \
                                          ( (RIGHT) == HAL_MPU_AUTHA_DMARA) || \
                                          ( (RIGHT) == HAL_MPU_AUTHB_R0UPX) || \
                                          ( (RIGHT) == HAL_MPU_AUTHB_R0UPW) || \
                                          ( (RIGHT) == HAL_MPU_AUTHB_R0UPR) || \
                                          ( (RIGHT) == HAL_MPU_AUTHB_R0PX) || \
                                          ( (RIGHT) == HAL_MPU_AUTHB_R0PW) || \
                                          ( (RIGHT) == HAL_MPU_AUTHB_R0PR) || \
                                          ( (RIGHT) == HAL_MPU_AUTHB_R1UPX) || \
                                          ( (RIGHT) == HAL_MPU_AUTHB_R1UPW) || \
                                          ( (RIGHT) == HAL_MPU_AUTHB_R1UPR) || \
                                          ( (RIGHT) == HAL_MPU_AUTHB_R1PX) || \
                                          ( (RIGHT) == HAL_MPU_AUTHB_R1PW) || \
                                          ( (RIGHT) == HAL_MPU_AUTHB_R1PR) || \
                                          ( (RIGHT) == HAL_MPU_AUTHB_R2UPX) || \
                                          ( (RIGHT) == HAL_MPU_AUTHB_R2UPW) || \
                                          ( (RIGHT) == HAL_MPU_AUTHB_R2UPR) || \
                                          ( (RIGHT) == HAL_MPU_AUTHB_R2PX) || \
                                          ( (RIGHT) == HAL_MPU_AUTHB_R2PW) || \
                                          ( (RIGHT) == HAL_MPU_AUTHB_R2PR) || \
                                          ( (RIGHT) == HAL_MPU_AUTHB_R3UPX) || \
                                          ( (RIGHT) == HAL_MPU_AUTHB_R3UPW) || \
                                          ( (RIGHT) == HAL_MPU_AUTHB_R3UPR) || \
                                          ( (RIGHT) == HAL_MPU_AUTHB_R3PX) || \
                                          ( (RIGHT) == HAL_MPU_AUTHB_R3PW) || \
                                          ( (RIGHT) == HAL_MPU_AUTHB_R3PR) || \
                                          ( (RIGHT) == HAL_MPU_AUTHC_R4UPX) || \
                                          ( (RIGHT) == HAL_MPU_AUTHC_R4UPW) || \
                                          ( (RIGHT) == HAL_MPU_AUTHC_R4UPR) || \
                                          ( (RIGHT) == HAL_MPU_AUTHC_R4PX) || \
                                          ( (RIGHT) == HAL_MPU_AUTHC_R4PW) || \
                                          ( (RIGHT) == HAL_MPU_AUTHC_R4PR) || \
                                          ( (RIGHT) == HAL_MPU_AUTHC_R5UPX) || \
                                          ( (RIGHT) == HAL_MPU_AUTHC_R5UPW) || \
                                          ( (RIGHT) == HAL_MPU_AUTHC_R5UPR) || \
                                          ( (RIGHT) == HAL_MPU_AUTHC_R5PX) || \
                                          ( (RIGHT) == HAL_MPU_AUTHC_R5PW) || \
                                          ( (RIGHT) == HAL_MPU_AUTHC_R5PR) || \
                                          ( (RIGHT) == HAL_MPU_AUTHC_R6UPX) || \
                                          ( (RIGHT) == HAL_MPU_AUTHC_R6UPW) || \
                                          ( (RIGHT) == HAL_MPU_AUTHC_R6UPR) || \
                                          ( (RIGHT) == HAL_MPU_AUTHC_R6PX) || \
                                          ( (RIGHT) == HAL_MPU_AUTHC_R6PW) || \
                                          ( (RIGHT) == HAL_MPU_AUTHC_R6PR) || \
                                          ( (RIGHT) == HAL_MPU_AUTHC_R7UPX) || \
                                          ( (RIGHT) == HAL_MPU_AUTHC_R7UPW) || \
                                          ( (RIGHT) == HAL_MPU_AUTHC_R7UPR) || \
                                          ( (RIGHT) == HAL_MPU_AUTHC_R7PX) || \
                                          ( (RIGHT) == HAL_MPU_AUTHC_R7PW) || \
                                          ( (RIGHT) == HAL_MPU_AUTHC_R7PR) )


/**
 * @}
 */

/* Private functions ---------------------------------------------------------*/


/** @defgroup MPU_Private_Functions MPU Private Functions
 * @{
 */


/**
 * @}
 */


/**
 * @}
 */


/**
 * @}
 */


/** @addtogroup MPU_Exported_Functions_Group1
 * @{
 */
void HAL_MPU_SetRegionConfig(MPU_TypeDef *base, HAL_MPU_RegionTypeDef *regionConfig);


void HAL_MPU_SetRegionAddr(MPU_TypeDef *base, uint32_t regionNum, uint32_t startAddr, uint32_t endAddr);


void HAL_MPU_SetRegionAccessRights(MPU_TypeDef *base, HAL_MPU_RightTypeDef *accessRights);


void HAL_MPU_GetErrorAccessInfo(MPU_TypeDef *base, uint32_t slaveNum, HAL_MPU_AccessErrTypeDef *errInform);


#ifdef __cplusplus
}
#endif
#endif /* FM15F3XX_HAL_MPU_H */
/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
