/**
 ******************************************************************************
 * @file    fm15f3xx_ll_dcmi.h
 * @author  WYL
 * @version V1.0.0
 * @date    2020-03-17
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __FM15F3XX_LL_DCMI_H
#define __FM15F3XX_LL_DCMI_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "fm15f3xx.h"


/** @addtogroup FM15F3XX_LL_Driver
 * @{
 */


/** @addtogroup DCMI
 * @{
 */

/* Exported types ------------------------------------------------------------*/


/**
 * @brief   DCMI Init structure definition
 */
typedef struct {
    uint32_t DCMI_CaptureMode;              /*!< Specifies the Capture Mode: Continuous or Snapshot.
                                               This parameter can be a value of @ref DCMI_Capture_Mode */

    uint32_t DCMI_SynchroMode;              /*!< Specifies the Synchronization Mode: Hardware or Embedded.
                                               This parameter can be a value of @ref DCMI_Synchronization_Mode */

    uint32_t DCMI_PCKPolarity;              /*!< Specifies the Pixel clock polarity: Falling or Rising.
                                               This parameter can be a value of @ref DCMI_PIXCK_Polarity */

    uint32_t DCMI_VSPolarity;               /*!< Specifies the Vertical synchronization polarity: High or Low.
                                               This parameter can be a value of @ref DCMI_VSYNC_Polarity */

    uint32_t DCMI_HSPolarity;               /*!< Specifies the Horizontal synchronization polarity: High or Low.
                                               This parameter can be a value of @ref DCMI_HSYNC_Polarity */

    uint32_t DCMI_CaptureRate;              /*!< Specifies the frequency of frame capture: All, 1/3.
                                               This parameter can be a value of @ref DCMI_Capture_Rate */

    uint32_t DCMI_DataWidthMode;            /*!< Specifies the data width: 8-bit, 10-bit, 12-bit or 14-bit.
                                               This parameter can be a value of @ref DCMI_Data_Width_Mode */

    uint32_t DCMI_ByteSelMode;              /*!< Specifies the Byte Select Mode: all, every oher, 1/2 or 1/4.
                                               This parameter can be a value of @ref DCMI_Byte_Sel_Mode */

    uint32_t DCMI_ByteSel;                  /*!< Specifies the Byte Select: first, second.
                                               This parameter can be a value of @ref DCMI_Byte_Sel */

    uint32_t DCMI_LineSelMode;              /*!< Specifies the Line Select Mode: all,1/2.
                                               This parameter can be a value of @ref DCMI_Line_Sel_Mode */

    uint32_t DCMI_LineSel;                  /*!< Specifies the Line Select: first, second.
                                               This parameter can be a value of @ref DCMI_Line_Sel_Mode */

    uint32_t DCMI_SyncEdgeSel;              /*!< Specifies the sync edge Select: negedge, posedge.
                                               This parameter can be a value of @ref DCMI_Sync_Edge_Sel */
} DCMI_InitTypeDef;


/**
 * @brief   DCMI CROP Init structure definition
 */
typedef struct {
    uint16_t DCMI_VerticalStartLine;        /*!< Specifies the Vertical start line count from which the image capture
                                               will start. This parameter can be a value between 0x00 and 0x1FFF */

    uint16_t DCMI_HorizontalOffsetCount;    /*!< Specifies the number of pixel clocks to count before starting a capture.
                                               This parameter can be a value between 0x00 and 0x3FFF */

    uint16_t DCMI_VerticalLineCount;        /*!< Specifies the number of lines to be captured from the starting point.
                                               This parameter can be a value between 0x00 and 0x3FFF */

    uint16_t DCMI_CaptureCount;             /*!< Specifies the number of pixel clocks to be captured from the starting
                                               point on the same line.
                                               This parameter can be a value between 0x00 and 0x3FFF */
} DCMI_CROPInitTypeDef;


/**
 * @brief   DCMI Embedded Synchronisation CODE Init structure definition
 */
typedef struct {
    uint8_t DCMI_FrameStartCode;            /*!< Specifies the code of the frame start delimiter. */
    uint8_t DCMI_LineStartCode;             /*!< Specifies the code of the line start delimiter. */
    uint8_t DCMI_LineEndCode;               /*!< Specifies the code of the line end delimiter. */
    uint8_t DCMI_FrameEndCode;              /*!< Specifies the code of the frame end delimiter. */
} DCMI_CodesInitTypeDef;

/* Exported constants --------------------------------------------------------*/


/** @defgroup DCMI_Exported_Constants
 * @{
 */


/** @defgroup DCMI_Capture_Mode
 * @{
 */
#define DCMI_CAPTUREMODE_CONTINUOUS ( (uint32_t) 0x00000000)    /*!< The received data are transferred continuously
                                                                   into the destination memory through the DMA */
#define DCMI_CAPTUREMODE_SNAPSHOT   ( (uint32_t) 0x00000002)    /*!< Once activated, the interface waits for the start of
                                                                   frame and then transfers a single frame through the DMA */


/**
 * @}
 */


/** @defgroup DCMI_Synchronization_Mode
 * @{
 */
#define DCMI_SYNCHROMODE_HARDWARE   ( (uint32_t) 0x00000000)    /*!< Hardware synchronization data capture (frame/line start/stop)
                                                                   is synchronized with the HSYNC/VSYNC signals */
#define DCMI_SYNCHROMODE_EMBEDDED   ( (uint32_t) 0x00000010)    /*!< Embedded synchronization data capture is synchronized with
                                                                   synchronization codes embedded in the data flow */


/**
 * @}
 */


/** @defgroup DCMI_PIXCK_Polarity
 * @{
 */
#define DCMI_PCKPOLARITY_FALLING    ( (uint32_t) 0x00000000)    /*!< Pixel clock active on Falling edge */
#define DCMI_PCKPOLARITY_RISING     ( (uint32_t) 0x00000020)    /*!< Pixel clock active on Rising edge */


/**
 * @}
 */


/** @defgroup DCMI_VSYNC_Polarity
 * @{
 */
#define DCMI_VSPOLARITY_LOW     ( (uint32_t) 0x00000000)        /*!< Vertical synchronization active Low */
#define DCMI_VSPOLARITY_HIGH    ( (uint32_t) 0x00000080)        /*!< Vertical synchronization active High */


/**
 * @}
 */


/** @defgroup DCMI_HSYNC_Polarity
 * @{
 */
#define DCMI_HSPOLARITY_LOW     ( (uint32_t) 0x00000000)        /*!< Horizontal synchronization active Low */
#define DCMI_HSPOLARITY_HIGH    ( (uint32_t) 0x00000040)        /*!< Horizontal synchronization active High */


/**
 * @}
 */


/** @defgroup DCMI_Capture_Rate
 * @{
 */
#define DCMI_CAPTURERATE_ALL_FRAME  ( (uint32_t) 0x00000000)    /*!< All frames are captured */
#define DCMI_CAPTURERATE_1OF3_FRAME ( (uint32_t) 0x00000200)    /*!< One frame in 3 frames captured */


/**
 * @}
 */


/** @defgroup DCMI_Data_Width_Mode
 * @{
 */
#define DCMI_DATAWIDTH_8B   ( (uint32_t) 0x00000000)            /*!< Interface captures 8-bit data on every pixel clock */
#define DCMI_DATAWIDTH_10B  ( (uint32_t) 0x00000400)            /*!< Interface captures 10-bit data on every pixel clock */
#define DCMI_DATAWIDTH_12B  ( (uint32_t) 0x00000800)            /*!< Interface captures 12-bit data on every pixel clock */
#define DCMI_DATAWIDTH_14B  ( (uint32_t) 0x00000C00)            /*!< Interface captures 14-bit data on every pixel clock */


/**
 * @}
 */


/** @defgroup DCMI_Byte_Sel_Mode
 * @{
 */
#define DCMI_BYTESELMODE_ALL        ( (uint32_t) 0x00000000)    /*!< Interface captures all received data */
#define DCMI_BYTESELMODE_EVERYOTHER ( (uint32_t) 0x00010000)    /*!< Interface captures every other byte from the received data */
#define DCMI_BYTESELMODE_1OF4       ( (uint32_t) 0x00020000)    /*!< Interface captures one byte out of four */
#define DCMI_BYTESELMODE_2OF4       ( (uint32_t) 0x00030000)    /*!< Interface captures two bytes out of four */


/**
 * @}
 */


/** @defgroup DCMI_Byte_Sel
 * @{
 */
#define DCMI_BYTESEL_FIRST  ( (uint32_t) 0x00000000)            /*!< Interface captures first data(byte or double byte) from the frame/line start, second one being dropped */
#define DCMI_BYTESEL_SECOND ( (uint32_t) 0x00040000)            /*!< Interface captures second data(byte or double byte) from the frame/line start, first one being dropped */


/**
 * @}
 */


/** @defgroup DCMI_Line_Sel_Mode
 * @{
 */
#define DCMI_LINESELMODE_ALL    ( (uint32_t) 0x00000000)        /*!< Interface captures first data(byte or double byte) from the frame/line start, second one being dropped */
#define DCMI_LineSelMode_1of2   ( (uint32_t) 0x00080000)        /*!< Interface captures second data(byte or double byte) from the frame/line start, first one being dropped */


/**
 * @}
 */


/** @defgroup DCMI_Line_Sel
 * @{
 */
#define DCMI_LINESEL_FIRST  ( (uint32_t) 0x00000000)            /*!< interface captures first line after the frame start, second one being dropped */
#define DCMI_LINESEL_SECOND ( (uint32_t) 0x00100000)            /*!< Interface captures second line from the frame start, first one being dropped */


/**
 * @}
 */


/** @defgroup DCMI_Sync_Edge_Sel
 * @{
 */
#define DCMI_SYNCEDGESEL_NEGEDGE    ( (uint32_t) 0x00000000)    /*!< interface captures first line after the frame start, second one being dropped */
#define DCMI_SYNCEDGESEL_POSEDGE    ( (uint32_t) 0x80000000)    /*!< Interface captures second line from the frame start, first one being dropped */


/**
 * @}
 */


/** @defgroup DCMI_interrupt_sources
 * @{
 */
#define DCMI_IT_FRAME   ( (uint16_t) 0x0001)
#define DCMI_IT_OVF     ( (uint16_t) 0x0002)
#define DCMI_IT_ERR     ( (uint16_t) 0x0004)
#define DCMI_IT_VSYNC   ( (uint16_t) 0x0008)
#define DCMI_IT_LINE    ( (uint16_t) 0x0010)


/**
 * @}
 */


/** @defgroup DCMI_Flags
 * @{
 */


/**
 * @brief   DCMI STATUS register
 */
#define DCMI_FLAG_HSYNC ( (uint16_t) 0x2001)
#define DCMI_FLAG_VSYNC ( (uint16_t) 0x2002)
#define DCMI_FLAG_FNE   ( (uint16_t) 0x2004)


/**
 * @brief   DCMI RISTATUS register
 */
#define DCMI_FLAG_FRAMERI   ( (uint16_t) 0x0001)
#define DCMI_FLAG_OVFRI     ( (uint16_t) 0x0002)
#define DCMI_FLAG_ERRRI     ( (uint16_t) 0x0004)
#define DCMI_FLAG_VSYNCRI   ( (uint16_t) 0x0008)
#define DCMI_FLAG_LINERI    ( (uint16_t) 0x0010)


/**
 * @brief   DCMI MISTATUS register
 */
#define DCMI_FLAG_FRAMEMI   ( (uint16_t) 0x1001)
#define DCMI_FLAG_OVFMI     ( (uint16_t) 0x1002)
#define DCMI_FLAG_ERRMI     ( (uint16_t) 0x1004)
#define DCMI_FLAG_VSYNCMI   ( (uint16_t) 0x1008)
#define DCMI_FLAG_LINEMI    ( (uint16_t) 0x1010)


/**
 * @}
 */


/**
 * @}
 */

/* Exported macro ------------------------------------------------------------*/
/* Exported functions --------------------------------------------------------*/

/*  Function used to set the DCMI configuration to the default reset state ****/
void DCMI_DeInit(void);


/* Initialization and Configuration functions *********************************/
void DCMI_Init(DCMI_InitTypeDef* DCMI_InitStruct);


void DCMI_StructInit(DCMI_InitTypeDef* DCMI_InitStruct);


void DCMI_SyncLimitConfig(uint8_t DCMI_SyncLimit);


void DCMI_ShiftNumConfig(uint8_t DCMI_ShiftNum);


void DCMI_CROPConfig(DCMI_CROPInitTypeDef* DCMI_CROPInitStruct);


void DCMI_CROPCmd(FunctionalState NewState);


void DCMI_SetEmbeddedSynchroCodes(DCMI_CodesInitTypeDef* DCMI_CodesInitStruct);


void DCMI_JPEGCmd(FunctionalState NewState);


/* Image capture functions ****************************************************/
void DCMI_Cmd(FunctionalState NewState);


void DCMI_CaptureCmd(FunctionalState NewState);


void DCMI_MapCmd(FunctionalState NewState);


uint32_t DCMI_ReadData(void);


/* Interrupts and flags management functions **********************************/
void DCMI_ITConfig(uint16_t DCMI_IT, FunctionalState NewState);


FlagStatus DCMI_GetFlagStatus(uint16_t DCMI_FLAG);


void DCMI_ClearFlag(uint16_t DCMI_FLAG);


ITStatus DCMI_GetITStatus(uint16_t DCMI_IT);


void DCMI_ClearITPendingBit(uint16_t DCMI_IT);


#ifdef __cplusplus
}
#endif

#endif /*__FM15F3XX_LL_DCMI_H */


/**
 * @}
 */


/**
 * @}
 */

/************************ (C) COPYRIGHT Fudan Microelectronics *****END OF FILE****/
