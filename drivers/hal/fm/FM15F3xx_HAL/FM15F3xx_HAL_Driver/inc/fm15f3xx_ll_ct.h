/**
 ******************************************************************************
 * @file    fm15f3xx_ll_ct.h
 * @author  WYL
 * @version V1.0.0
 * @date    2020-03-17
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __FM15F3XX_LL_CT_H
#define __FM15F3XX_LL_CT_H

#ifdef __cplusplus
extern "C" {
#endif


/** @addtogroup FM15F3XX_LL_Driver
 * @{
 */

#if defined (CT0) || defined (CT1)


/** @defgroup CT_LL CT
 * @{
 */

#include "fm15f3xx.h"

typedef struct {
    uint32_t IoModeSel;

    uint32_t OneShotEn;

    uint32_t ClkMode;

    uint32_t RstMode;

    uint32_t FreqA;

    uint32_t FreqB;

    uint32_t DataOrder;

    uint32_t TxRepeatNum;

    uint32_t RxRepeatNum;

    uint32_t RxGuardTimeNum;

    uint32_t TxGuardTimeNum;

    uint32_t ParityMode;

    uint32_t RxCheckParity;

    uint32_t IoCollerrNum;

    uint32_t Fi;

    uint32_t Di;

    uint32_t ExtraGuardTimeNum;

    uint32_t ClkOutput;

    uint32_t RstCfgReg;

    uint32_t RstStateReg;

    uint32_t TxFlagMode;

    uint32_t TxRxMode;
} CT_InitTypeDef;

/*************************************************************CFG_IO REG*************************************************/


/** @defgroup LL_CT_Enable
 * @{
 */
#define LL_CT_DISABLE   0x00U
#define LL_CT_ENABLE    CT_CFGIO_MODLEN


/**
 * @}
 */


/** @defgroup LL_CT_IOMode
 * @{
 */
#define LL_CT_7816  0x00U
#define LL_CT_UART  CT_CFGIO_IOMDSEL


/**
 * @}
 */


/** @defgroup LL_CT_OneShoot
 * @{
 */
#define LL_CT_ONESHOT_DISABLE   0x00U
#define LL_CT_ONESHOT_ENABLE    CT_CFGIO_IOONSEN




/**
 * @}
 */


/** @defgroup LL_CT_CLKMode
 * @{
 */
#define LL_CT_CLK_MASTER    0x0U
#define LL_CT_CLK_SLAVE     CT_CFGIO_CLKONSEN


/**
 * @}
 */


/** @defgroup LL_CT_RSTMode
 * @{
 */
#define LL_CT_RST_MASTER    0x0U
//#define LL_CT_RST_SLAVE       (1 << CT_CFGIO_RSTRXPTCFG_Pos)//CT_CFGIO_RSTRXPTCFG
#define LL_CT_RST_SLAVE         CT_CFGIO_RSTRXPTCFG

/**
 * @}
 */

/******************************************************CFG_TRANS********************************************************/


/** @defgroup LL_CT_DataOrder
 * @{
 */
#define LL_CT_ORDER_LSB 0x0U
#define LL_CT_ORDER_MSB CT_CFGTRNS_DICONV


/**+
 * @}
 */


/** @defgroup LL_CT_SetParity
 * @{
 */
#define LL_CT_PARITY_EVEN   (0x0U)
#define LL_CT_PARITY_ODD    (0x1U)
#define LL_CT_PARITY_1      (0x2U)
#define LL_CT_PARITY_NO     (0x3U)


/**
 * @}
 */


/** @defgroup LL_CT_RxParityCheck
 * @{
 */
#define LL_CT_RxPARCHECK_ENABLE     CT_CFGTRNS_IECKPEN
#define LL_CT_RxPARCHECK_DISABLE    0x0U


/**
 * @}
 */
/******************************************************CTRL_IO********************************************************/


/** @defgroup LL_CT_SetCLK
 * @{
 */
#define LL_CT_CLKOUT_CLK0   (0x0U)
#define LL_CT_CLKOUT_CLK1   (0x1U)
#define LL_CT_CLKOUT_0      (0x2U)
#define LL_CT_CLKOUT_1      (0x3U)


/**
 * @}
 */


/** @defgroup LL_CT_CFGTransReg
 * @{
 */
#define LL_CT_CFGREG_NoRST  0x0U
#define LL_CT_CFGREG_RST    CT_CTRLIO_CFGRSTEN


/**
 * @}
 */


/** @defgroup LL_CT_RSTSampleMode
 * @{
 */
#define LL_CT_RSTSAMPLEMODE_DIRECT  0x0U
#define LL_CT_RSTSAMPLEMODE_SYSCLK  CT_CTRLIO_RSTEGDTMD


/**
 * @}
 */


/** @defgroup LL_CT_CFGStateReg
 * @{
 */
#define LL_CT_STATEREG_NoRST    0x0U
#define LL_CT_STATEREG_RST      CT_CTRLIO_STRSTCFG


/**
 * @}
 */


/** @defgroup LL_CT_CLKDetect
 * @{
 */
#define LL_CT_CLKDET_DISABLE    0x0U
#define LL_CT_CLKDET_ENABLE     CT_CTRLIO_CKDTEN


/**
 * @}
 */


/** @defgroup LL_CT_RSTEdgeDetect
 * @{
 */
#define LL_CT_RSTEDGEDET_DISABLE    0x0U
#define LL_CT_RSTEDGEDET_ENABLE     CT_CTRLIO_RSTEGDTEN


/**
 * @}
 */


/** @defgroup LL_CT_INTEN select
 * @{
 */
#define LL_CT_TXIE          CT_INTEN_TXINTEN
#define LL_CT_RXIE          CT_INTEN_RXINTEN
#define LL_CT_ERRIE         CT_INTEN_ERRINTEN
#define LL_CT_CLKSTOPIE     CT_INTEN_CLKSTPINTEN
#define LL_CT_CLKRESUMEIE   CT_INTEN_CLKRSMINTEN
#define LL_CT_RSTIE         CT_INTEN_RSTINTEN
#define LL_CT_CLKCNTIE      CT_INTEN_CLKCNTINTEN
#define LL_CT_ETUCNTIE      CT_INTEN_ETUCNTINTEN
#define LL_CT_RXF0ERRIE     (CT_INTEN_ERRINTEN | CT_INTEN_RXFOERINTEN)
#define LL_CT_TXF0ERRIE     (CT_INTEN_ERRINTEN | CT_INTEN_TXFOERINTEN)
#define LL_CT_RXFRMERRIE    (CT_INTEN_ERRINTEN | CT_INTEN_RXFRMERINTEN)
#define LL_CT_TXRSDERRIE    (CT_INTEN_ERRINTEN | CT_INTEN_TXRSDERINTEN)
#define LL_CT_RXPRTYERRIE   (CT_INTEN_ERRINTEN | CT_INTEN_RXPRTYERINTEN)
#define LL_CT_COLLEREIE     (CT_INTEN_ERRINTEN | CT_INTEN_COLLERINTEN)
#define LL_CT_RXFOPRYERRIE  (CT_INTEN_ERRINTEN | CT_INTEN_RXFOPRYERINTEN)
#define LL_CT_TXFOPRYERRIE  (CT_INTEN_ERRINTEN | CT_INTEN_TXFOPRYERINTEN)
#define LL_CT_TXDMAE        CT_INTEN_TXDMAEN
#define LL_CT_RXDMAE        CT_INTEN_RXDMAEN


/**
 * @}
 */


/** @defgroup LL_CT_CLEAR_STATE select
 * @{
 */
#define LL_CT_FLAG_CLEARERR         CT_STSCLRTRNS_CLRERR
#define LL_CT_FLAG_CLEARTXFIFO      CT_STSCLRTRNS_CLRTXFIFO
#define LL_CT_FLAG_CLEARRXFIFO      CT_STSCLRTRNS_CLRRXFIFO
#define LL_CT_FLAG_CLEARCLK         CT_STSCLRTRNS_CLRCLKFLG
#define LL_CT_FLAG_CLEARRST         CT_STSCLRTRNS_CLRRSTFLG
#define LL_CT_FLAG_CLEARCLKCNT      CT_STSCLRTRNS_CLRCTCNT
#define LL_CT_FLAG_CLEARETUCNT      CT_STSCLRTRNS_CLRETUCNT
#define LL_CT_FLAG_CLEARRXETUCNT    CT_STSCLRTRNS_CLRRXETUCNT
#define LL_CT_FLAG_CLEARTXETUCNT    CT_STSCLRTRNS_CLRTXETUCNT


/**
 * @}
 */


/** @defgroup LL_CT_IOSTATUS select
 * @{
 */

#define LL_CT_FLAG_CLKSTOP  CT_IOSTAT_CLKSTPFLG
#define LL_CT_FLAG_CLKRSM   CT_IOSTAT_CLKRSMFLG
#define LL_CT_FLAG_RSTNG    CT_IOSTAT_RSTNGFLG
#define LL_CT_FLAG_RSTPG    CT_IOSTAT_RSTPGFLG


/**
 * @}
 */


/** @defgroup LL_CT_STATUS select
 * @{
 */
#define LL_CT_FLAG_RX           CT_STATUS_RXFLAG
#define LL_CT_FLAG_TX           CT_STATUS_TXFLAG
#define LL_CT_FLAG_ERR          CT_STATUS_ERRFLAG
#define LL_CT_FLAG_RXBUSY       CT_STATUS_RXBUSY
#define LL_CT_FLAG_TXBUSY       CT_STATUS_TXBUSY
#define LL_CT_FLAG_RXFIFOERR    CT_STATUS_RXFIFOERR
#define LL_CT_FLAG_TXFIFOERR    CT_STATUS_TXFIFOERR
#define LL_CT_FLAG_RXFRMERR     CT_STATUS_RXFRMERR
#define LL_CT_FLAG_TXRSDERR     CT_STATUS_TXRSDERR
#define LL_CT_FLAG_RXPRYERR     CT_STATUS_RXPRYERR
#define LL_CT_FLAG_COLLERR      CT_STATUS_COLLERR
#define LL_CT_FLAG_RXFIFOPRYERR CT_STATUS_RXFIFOPRYERR
#define LL_CT_FLAG_EXTETUCNT    CT_STATUS_EXTETUCNT
#define LL_CT_FLAG_CLKCNT       CT_STATUS_CLKCNTFLG
#define LL_CT_FLAG_ETUCNT       CT_STATUS_ETUCNTFLG
#define LL_CT_FLAG_TXFIFOPRYERR CT_STATUS_TXFOPRYERR


/**
 * @}
 */


/** @defgroup LL_CT_FIFOSTATUS select
 * @{
 */
#define LL_CT_FLAG_TXFIFO       CT_FFSTATUS_TXFFSTATS
#define LL_CT_FLAG_TXFIFOEMPTY  CT_FFSTATUS_TXFFEMPTY
#define LL_CT_FLAG_TXFIFOFULL   CT_FFSTATUS_TXFFFULL
#define LL_CT_FLAG_RXFIFO       CT_FFSTATUS_RXFFSTATS
#define LL_CT_FLAG_RXFIFOEMPTY  CT_FFSTATUS_RXFFEMPTY
#define LL_CT_FLAG_RXFIFOFULL   CT_FFSTATUS_RXFFFULL


/**
 * @}
 */


/******************************************************CTRL_TRANS********************************************************/


/** @defgroup LL_CT_CFGTxFlag
 * @{
 */
#define LL_CT_TxFIFOEMPTY       0x0U
#define LL_CT_TxFIFOHALFEMPTY   CT_CTRLTRNS_TXFLAGCFG


/**
 * @}
 */


/** @defgroup LL_CT_TxRxWorkMode
 * @{
 */
#define LL_CT_TxRxMODE_SIMUL    0x0U
#define LL_CT_TxRxMODE_SEQ      CT_CTRLTRNS_TRMODE


/**
 * @}
 */

/*************************************************************CFG_IO REG*************************************************/

/* Private functions ---------------------------------------------------------*/


/** @defgroup CT_LL_Functions CT Functions
 * @{
 */


/** @defgroup CT_LL_Configuration Configuration
 * @{
 */


/**
 * @brief  Set CT module enable or disable
 * @param  CT Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref LL_CT_DISABLE
 *         @arg @ref LL_CT_ENABLE
 * @retval None
 */
__STATIC_INLINE void LL_CT_Enable(CT_TypeDef *CTx, uint32_t ct_en)
{
    SET_BIT(CTx->IO_CFG, (CT_CFGIO_MODLEN_Msk & ct_en));
}


/**
 * @brief  Set CT mode
 * @param  CT Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref LL_CT_7816
 *         @arg @ref LL_CT_UART
 * @retval None
 */
__STATIC_INLINE void LL_CT_SetIOMode(CT_TypeDef *CTx, uint32_t mode)
{
    MODIFY_REG(CTx->IO_CFG, CT_CFGIO_IOMDSEL, mode);
}


/**
 * @brief  Set CT IO ONE_SHOT enable or disable
 * @param  CT Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref LL_CT_ONESHOT_DISABLE
 *         @arg @ref LL_CT_ONESHOT_ENABLE
 * @retval None
 */
__STATIC_INLINE void LL_CT_SetIOOneShotEn(CT_TypeDef *CTx, uint32_t en)
{
    MODIFY_REG(CTx->IO_CFG, CT_CFGIO_IOONSEN, en);
}


/**
 * @brief  Set CT CLK Mode
 * @param  CT Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref LL_CT_CLK_MASTER
 *         @arg @ref LL_CT_CLK_SLAVE
 * @retval None
 */
__STATIC_INLINE void LL_CT_SetClkMode(CT_TypeDef *CTx, uint32_t mode)
{
    MODIFY_REG(CTx->IO_CFG, CT_CFGIO_CLKONSEN, mode);
}


/**
 * @brief  Set CT RST Mode
 * @param  CT Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref LL_CT_RST_MASTER
 *         @arg @ref LL_CT_RST_SLAVE
 * @retval None
 */
__STATIC_INLINE void LL_CT_SetResetMode(CT_TypeDef *CTx, uint32_t mode)
{
    MODIFY_REG(CTx->IO_CFG, CT_CFGIO_RSTRXPTCFG, mode);
}


/**
 * @brief  Set CT CLK frequency
 * @param  CT Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref a:0x0~0x1F
 * @param  This parameter can be one of the following values:
 *         @arg @ref b:0x0U~0xFU
 * @retval None               Freq= ct_clk/(a + (b+1)/16 + 1)
 */
__STATIC_INLINE void LL_CT_SetFreqAB(CT_TypeDef *CTx, uint32_t a, uint32_t b)
{
    MODIFY_REG(CTx->IO_CFG, CT_CFGIO_CLKFRQCFGA, a << CT_CFGIO_CLKFRQCFGA_Pos);
    MODIFY_REG(CTx->IO_CFG, CT_CFGIO_CLKFRQCFGB, b << CT_CFGIO_CLKFRQCFGB_Pos);
}


/**
 * @}
 */

/******************************************************CFG_TRANS********************************************************/


/**
 * @brief  Set trans data order
 * @param  CT Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref LL_CT_ORDER_LSB
 *         @arg @ref LL_CT_ORDER_MSB
 * @retval None
 */
__STATIC_INLINE void LL_CT_SetDataOrder(CT_TypeDef *CTx, uint32_t order)
{
    MODIFY_REG(CTx->CFG, CT_CFGTRNS_DICONV, order);
}


/**
 * @brief  Set CT Tx Repeat Num
 * @param  CT Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref num:0x0~0x7
 * @retval None
 */
__STATIC_INLINE void LL_CT_SetTxRepeatNum(CT_TypeDef *CTx, uint32_t num)
{
    MODIFY_REG(CTx->CFG, CT_CFGTRNS_TXREPCFG, num << CT_CFGTRNS_TXREPCFG_Pos);
}


/**
 * @brief  Set CT Rx Repeat Num
 * @param  CT Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref num:0x0~0x7
 * @retval None
 */
__STATIC_INLINE void LL_CT_SetRxRepeatNum(CT_TypeDef *CTx, uint32_t num)
{
    MODIFY_REG(CTx->CFG, CT_CFGTRNS_RXREPCFG, num << CT_CFGTRNS_RXREPCFG_Pos);
}


/**
 * @brief  Set rx guard time
 * @param  CT Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref num:0x0~0x1
 * @retval None
 */
__STATIC_INLINE void LL_CT_SetRxGuardTime(CT_TypeDef *CTx, uint32_t num)
{
    MODIFY_REG(CTx->CFG, CT_CFGTRNS_RXGTCFG, num << CT_CFGTRNS_RXGTCFG_Pos);
}


/**
 * @brief  Set tx guard time
 * @param  CT Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref num:0x0~0x3
 * @retval None
 */
__STATIC_INLINE void LL_CT_SetTxGuardTime(CT_TypeDef *CTx, uint32_t num)
{
    MODIFY_REG(CTx->CFG, CT_CFGTRNS_TXGTCFG, num << CT_CFGTRNS_TXGTCFG_Pos);
}


/**
 * @brief  Set Parity
 * @param  CT Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref LL_CT_PARITY_EVEN
 *         @arg @ref LL_CT_PARITY_ODD
 *         @arg @ref LL_CT_PARITY_1
 *         @arg @ref LL_CT_PARITY_NO
 * @retval None
 */
__STATIC_INLINE void LL_CT_SetParity(CT_TypeDef *CTx, uint32_t mode)
{
    MODIFY_REG(CTx->CFG, CT_CFGTRNS_PRTYCFG, mode << CT_CFGTRNS_PRTYCFG_Pos);
}


/**
 * @brief  Set tx guard time
 * @param  CT Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref LL_CT_RxPARCHECK_ENABLE
 *         @arg @ref LL_CT_RxPARCHECK_DISABLE
 * @retval None
 */
__STATIC_INLINE void LL_CT_SetRxParityCheckEn(CT_TypeDef *CTx, uint32_t en)
{
    MODIFY_REG(CTx->CFG, CT_CFGTRNS_IECKPEN, en);
}


/**
 * @brief  Set IO Collide alarm
 * @param  CT Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref num:0x0~0x7
 * @retval None
 */
__STATIC_INLINE void LL_CT_SetIOCollideAlarm(CT_TypeDef *CTx, uint32_t num)
{
    MODIFY_REG(CTx->CFG, CT_CFGTRNS_IOCLCFG, num << CT_CFGTRNS_IOCLCFG_Pos);
}


/**
 * @brief  Set Fi/Di
 * @param  CT Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref fi:0x0U~0x0DU except:0x7��0x8
 * @param  This parameter can be one of the following values:
 *         @arg @ref di:0x0~0x9
 * @retval None
 */
__STATIC_INLINE void LL_CT_SetFiDi(CT_TypeDef *CTx, uint32_t fi, uint32_t di)
{
    MODIFY_REG(CTx->CFG, CT_CFGTRNS_DI, di << CT_CFGTRNS_DI_Pos);
    MODIFY_REG(CTx->CFG, CT_CFGTRNS_FI, fi << CT_CFGTRNS_FI_Pos);
}


/**
 * @brief  Set trans extra guard time
 * @param  CT Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref num:0x0~0xFF
 * @retval None
 */
__STATIC_INLINE void LL_CT_SetTxExtraGuardTime(CT_TypeDef *CTx, uint32_t num)
{
    MODIFY_REG(CTx->CFG, CT_CFGTRNS_TXEGT, num << CT_CFGTRNS_TXEGT_Pos);
}


/******************************************************CTRL_IO********************************************************/


/**
 * @brief  Set CT Reset output
 * @param  CT Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref _VALUE_:0x00~0x1
 * @retval None
 */
__STATIC_INLINE void LL_CT_SetResetOutput(CT_TypeDef *CTx, uint32_t io)
{
    MODIFY_REG(CTx->IO_CTRL, CT_CTRLIO_RSTREG, io << CT_CTRLIO_RSTREG_Pos);
}


/**
 * @brief  Set CT Stop mode Clk Output
 * @param  CT Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref LL_CT_CLKOUT_CLK
 *         @arg @ref LL_CT_CLKOUT_CLK
 *         @arg @ref LL_CT_CLKOUT_0
 *         @arg @ref LL_CT_CLKOUT_1
 * @retval None
 */
__STATIC_INLINE void LL_CT_SetClkStopMode(CT_TypeDef *CTx, uint32_t clk)
{
    MODIFY_REG(CTx->IO_CTRL, CT_CTRLIO_CLKOUTSTP, clk << CT_CTRLIO_CLKOUTSTP_Pos);
}


/**
 * @brief  set ct_cfg reg is reset or not reset by CT_RST
 * @param  CT Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref LL_CT_CFGREG_NoRST
 *         @arg @ref LL_CT_CFGREG_RST
 * @retval None
 */
__STATIC_INLINE void LL_CT_SetCfgRegResetEn(CT_TypeDef *CTx, uint32_t rst)
{
    MODIFY_REG(CTx->IO_CTRL, CT_CTRLIO_CFGRSTEN, rst << CT_CTRLIO_CFGRSTEN);
}


/**
 * @brief  Set Reset sample mode
 * @param  CT Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref LL_CT_RSTSAMPLEMODE_DIRECT
 *         @arg @ref LL_CT_RSTSAMPLEMODE_SYSCLK
 * @retval None
 */
__STATIC_INLINE void LL_CT_SetResetSampleMode(CT_TypeDef *CTx, uint32_t mode)
{
    MODIFY_REG(CTx->IO_CTRL, CT_CTRLIO_RSTEGDTMD, mode);
}


/**
 * @brief  set ct_cfg_state reg is reset or not reset by CT_RST
 * @param  CT Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref LL_CT_STATEREG_NoRST
 *         @arg @ref LL_CT_STATEREG_RST
 * @retval None
 */
__STATIC_INLINE void LL_CT_SetStateRegResetEn(CT_TypeDef *CTx, uint32_t mode)
{
    MODIFY_REG(CTx->IO_CTRL, CT_CTRLIO_STRSTCFG, mode);
}


/**
 * @brief  enable or disable clk detect
 * @param  CT Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref LL_CT_CLKDET_DISABLE
 *         @arg @ref LL_CT_CLKDET_ENABLE
 * @retval None
 */
__STATIC_INLINE void LL_CT_SetClkDetectEn(CT_TypeDef *CTx, uint32_t en)
{
    MODIFY_REG(CTx->IO_CTRL, CT_CTRLIO_CKDTEN, en);
}


/**
 * @brief  enable or disable rts edge detect
 * @param  CT Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref LL_CT_RSTEDGEDET_DISABLE
 *         @arg @ref LL_CT_RSTEDGEDET_ENABLE
 * @retval None
 */
__STATIC_INLINE void LL_CT_SetResetEdgeDetectEn(CT_TypeDef *CTx, uint32_t en)
{
    MODIFY_REG(CTx->IO_CTRL, CT_CTRLIO_RSTEGDTEN, en);
}


/******************************************************CTRL_TRANS********************************************************/


/**
 * @brief  enable tx
 * @param  CT Instance
 * @param  This parameter can be one of the following values:
 * @retval None
 */
__STATIC_INLINE void LL_CT_EnableTx(CT_TypeDef *CTx)
{
    SET_BIT(CTx->CTRL, CT_CTRLTRNS_TXEN);
}


/**
 * @brief  disable tx
 * @param  CT Instance
 * @param  This parameter can be one of the following values:
 * @retval None
 */
__STATIC_INLINE void LL_CT_DisableTx(CT_TypeDef *CTx)
{
    CLEAR_BIT(CTx->CTRL, CT_CTRLTRNS_TXEN);
}


/**
 * @brief  enable rx
 * @param  CT Instance
 * @param  This parameter can be one of the following values:
 * @retval None
 */
__STATIC_INLINE void LL_CT_EnableRx(CT_TypeDef *CTx)
{
    SET_BIT(CTx->CTRL, CT_CTRLTRNS_RXEN);
}


/**
 * @brief  disable rx
 * @param  CT Instance
 * @param  This parameter can be one of the following values:
 * @retval None
 */
__STATIC_INLINE void LL_CT_DisableRx(CT_TypeDef *CTx)
{
    CLEAR_BIT(CTx->CTRL, CT_CTRLTRNS_RXEN);
}


/**
 * @brief  Set tx flag
 * @param  CT Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref LL_CT_TxFIFOEMPTY
 *         @arg @ref LL_CT_TxFIFOHALFEMPTY
 * @retval None
 */
__STATIC_INLINE void LL_CT_SetTxFifoFlagMode(CT_TypeDef *CTx, uint32_t cgf)
{
    MODIFY_REG(CTx->CTRL, CT_CTRLTRNS_TXFLAGCFG, cgf);
}


/**
 * @brief  Set tx rx mode
 * @param  CT Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref LL_CT_TxRxMODE_SIMUL
 *         @arg @ref LL_CT_TxRxMODE_SEQ
 * @retval None
 */
__STATIC_INLINE void LL_CT_SetTxRxWorkMode(CT_TypeDef *CTx, uint32_t cgf)
{
    MODIFY_REG(CTx->CTRL, CT_CTRLTRNS_TRMODE, cgf);
}


/******************************************************INT_CTRL********************************************************/


/**
 * @brief  enable Tx int
 * @param  CT Instance
 * @param  This parameter can be combination of the following values:
 *         @arg @ref LL_CT_TXIE
 *         @arg @ref LL_CT_RXIE
 *         @arg @ref LL_CT_ERRIE
 *         @arg @ref LL_CT_CLKSTOPIE
 *         @arg @ref LL_CT_CLKRESUMEIE
 *         @arg @ref LL_CT_RSTIE
 *         @arg @ref LL_CT_CLKCNTIE
 *         @arg @ref LL_CT_ETUCNTIE
 *         @arg @ref LL_CT_RXF0ERRIE
 *         @arg @ref LL_CT_TXF0ERRIE
 *         @arg @ref LL_CT_RXFRMERRIE
 *         @arg @ref LL_CT_TXRSDERRIE
 *         @arg @ref LL_CT_RXPRTYERRIE
 *         @arg @ref LL_CT_COLLEREIE
 *         @arg @ref LL_CT_RXFOPRYERRIE
 *         @arg @ref LL_CT_TXFOPRYERRIE
 *         @arg @ref LL_CT_TXDMAE
 *         @arg @ref LL_CT_RXDMAE
 * @retval
 */
__STATIC_INLINE void LL_CT_EnableINT(CT_TypeDef *CTx, uint32_t ct_int)
{
    SET_BIT(CTx->INT_CTRL, ct_int);
}


/**
 * @brief  disable Tx int
 * @param  CT Instance
 * @param  This parameter can be combination of the following values:
 *         @arg @ref LL_CT_TXIE
 *         @arg @ref LL_CT_RXIE
 *         @arg @ref LL_CT_ERRIE
 *         @arg @ref LL_CT_CLKSTOPIE
 *         @arg @ref LL_CT_CLKRESUMEIE
 *         @arg @ref LL_CT_RSTIE
 *         @arg @ref LL_CT_CLKCNTIE
 *         @arg @ref LL_CT_ETUCNTIE
 *         @arg @ref LL_CT_RXF0ERRIE
 *         @arg @ref LL_CT_TXF0ERRIE
 *         @arg @ref LL_CT_RXFRMERRIE
 *         @arg @ref LL_CT_TXRSDERRIE
 *         @arg @ref LL_CT_RXPRTYERRIE
 *         @arg @ref LL_CT_COLLEREIE
 *         @arg @ref LL_CT_RXFOPRYERRIE
 *         @arg @ref LL_CT_TXFOPRYERRIE
 *         @arg @ref LL_CT_TXDMAE
 *         @arg @ref LL_CT_RXDMAE
 * @retval
 */
__STATIC_INLINE void LL_CT_DisableINT(CT_TypeDef *CTx, uint32_t ct_int)
{
    CLEAR_BIT(CTx->INT_CTRL, ct_int);
}


/**
 * @brief  disable all int
 * @param  CT Instance
 * @retval
 */
__STATIC_INLINE void LL_CT_DisableAllINT(CT_TypeDef *CTx)
{
    CLEAR_REG(CTx->INT_CTRL);
}


/******************************************************STATUS_CLR********************************************************/


/**
 * @brief  clear error flag
 * @param  CT Instance
 * @param  This parameter can be combination of the following values:
 *         @arg @ref LL_CT_FLAG_CLEARERR
 *         @arg @ref LL_CT_FLAG_CLEARTXFIFO
 *         @arg @ref LL_CT_FLAG_CLEARRXFIFO
 *         @arg @ref LL_CT_FLAG_CLEARCLK
 *         @arg @ref LL_CT_FLAG_CLEARRST
 *         @arg @ref LL_CT_FLAG_CLEARCLKCNT
 *         @arg @ref LL_CT_FLAG_CLEARETUCNT
 *         @arg @ref LL_CT_FLAG_CLEARRXETUCNT
 *         @arg @ref LL_CT_FLAG_CLEARTXETUCNT
 */
__STATIC_INLINE void LL_CT_ClearFlag(CT_TypeDef *CTx, uint32_t flag)
{
    SET_BIT(CTx->STATUS_CLR, flag);
}


/**
 * @brief  Clear All Flag
 * @param  CT Instance
 * @retval
 */
__STATIC_INLINE void LL_CT_ClearAllFlag(CT_TypeDef *CTx)
{
    WRITE_REG(CTx->STATUS_CLR, 0x0F1F);
}


/******************************************************THD_CNT********************************************************/


/**
 * @brief  set CT clk count threshold
 * @param  CT Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref _VALUE_:0x00~0xFFFF
 * @retval None
 */
__STATIC_INLINE void LL_CT_SetClkCountThd(CT_TypeDef *CTx, uint32_t cnt)
{
    WRITE_REG(CTx->COUNT_THRESHOLD, cnt);
}


/******************************************************INTE********************************************************/


/**
 * @brief  set CT ETU count threshold
 * @param  CT Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref _VALUE_:0x00~0xFFFF
 * @retval None
 */
__STATIC_INLINE void LL_CT_SetClkETUThd(CT_TypeDef *CTx, uint32_t cnt)
{
    WRITE_REG(CTx->ETU_COUNT_THRESHOLD, cnt);
}


/******************************************************TX_FIFO*******************************************************/


/**
 * @brief  Set txfifo data
 * @param  CT Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref data:0x00~0xFF
 * @retval None
 */
__STATIC_INLINE void LL_CT_TransmitData(CT_TypeDef *CTx, uint8_t data)
{
    WRITE_REG(CTx->TXFIFO, data);
}


/******************************************************RX_FIFO*******************************************************/


/**
 * @brief  Get Rxfifo data
 * @param  CT Instance
 * @retval None
 */
__STATIC_INLINE uint32_t LL_CT_ReceiveData(CT_TypeDef *CTx)
{
    return (READ_REG(CTx->RXFIFO));
}


/******************************************************IO_STATUS*******************************************************/


/**
 * @brief  get IO Status Flag
 * @param  CT Instance
 * @param  This parameter can be combination of the following values:
 *         @arg @ref LL_CT_FLAG_CLKSTOP
 *         @arg @ref LL_CT_FLAG_CLKRSM
 *         @arg @ref LL_CT_FLAG_RSTNG
 *         @arg @ref LL_CT_FLAG_RSTPG
 */
__STATIC_INLINE uint32_t LL_CT_GetStatus_IO(CT_TypeDef *CTx, uint32_t flag)
{
    return (READ_BIT(CTx->IO_STATUS, flag));
}


/******************************************************STATUS*******************************************************/


/**
 * @brief  get STATUS Flag
 * @param  CT Instance
 * @param  This parameter can be combination of the following values:
 *         @arg @ref LL_CT_FLAG_TX
 *         @arg @ref LL_CT_FLAG_RX
 *         @arg @ref LL_CT_FLAG_ERR
 *         @arg @ref LL_CT_FLAG_RXBUSY
 *         @arg @ref LL_CT_FLAG_TXBUSY
 *         @arg @ref LL_CT_FLAG_RXFIFOERR
 *         @arg @ref LL_CT_FLAG_TXFIFOERR
 *         @arg @ref LL_CT_FLAG_RXFRMERR
 *         @arg @ref LL_CT_FLAG_TXRSDERR
 *         @arg @ref LL_CT_FLAG_RXPRYERR
 *         @arg @ref LL_CT_FLAG_COLLERR
 *         @arg @ref LL_CT_FLAG_RXFIFOPRYERR
 *         @arg @ref LL_CT_FLAG_EXTETUCNT
 *         @arg @ref LL_CT_FLAG_CLKCNT
 *         @arg @ref LL_CT_FLAG_ETUCNT
 *         @arg @ref LL_CT_FLAG_TXFIFOPRYERR
 */
__STATIC_INLINE uint32_t LL_CT_GetFlag(CT_TypeDef *CTx, uint32_t flag)
{
    return (READ_BIT(CTx->STATUS, flag));
}


/******************************************************FIFO_STATUS*******************************************************/


/**
 * @brief  get TxFIFO Status
 * @param  CT Instance
 * @param  This parameter can be combination of the following values:
 *         @arg @ref LL_CT_FLAG_TXFIFO
 *         @arg @ref LL_CT_FLAG_TXFIFOEMPTY
 *         @arg @ref LL_CT_FLAG_TXFIFOFULL
 *         @arg @ref LL_CT_FLAG_RXFIFO
 *         @arg @ref LL_CT_FLAG_RXFIFOEMPTY
 *         @arg @ref LL_CT_FLAG_RXFIFOFULL
 */
__STATIC_INLINE uint32_t LL_CT_GetStatus_Fifo(CT_TypeDef *CTx, uint32_t flag)
{
    return (READ_BIT(CTx->FIFO_STATUS, flag));
}


/**
 * @brief  Check the length of the stored data in CT fifo
 * @rmtoll STATUS     RXFIFO    LL_CT_GetLengthRxFifo
 * @param  CTx Instance
 * @param  flag
 * @retval value 0~7.
 */
__STATIC_INLINE uint32_t LL_CT_GetLengthRxFifo(CT_TypeDef *CTx)
{
    return (READ_BIT(CTx->FIFO_STATUS, LL_CT_FLAG_RXFIFO) >> CT_FFSTATUS_RXFFSTATS_Pos);
}


/******************************************************ETU_CNT*******************************************************/


/**
 * @brief  get Etu CNT
 * @param  CT Instance
 * @retval None
 */
__STATIC_INLINE uint32_t LL_CT_GetETUCnt(CT_TypeDef *CTx)
{
    return (READ_REG(CTx->ETU_COUNT));
}


/******************************************************RX_ETU_CNT*******************************************************/


/**
 * @brief  get rx Etu CNT
 * @param  CT Instance
 * @retval None
 */
__STATIC_INLINE uint32_t LL_CT_GetRxETUCnt(CT_TypeDef *CTx)
{
    return (READ_REG(CTx->RX_ETU_COUNT));
}


/******************************************************TX_ETU_CNT*******************************************************/


/**
 * @brief  get tx Etu CNT
 * @param  CT Instance
 * @retval None
 */
__STATIC_INLINE uint32_t LL_CT_GetTxETUCnt(CT_TypeDef *CTx)
{
    return (READ_REG(CTx->TX_ETU_COUNT));
}


/******************************************************CNT*******************************************************/


/**
 * @brief  get clk CNT
 * @param  CT Instance
 * @retval None
 */
__STATIC_INLINE uint32_t LL_CT_GetClkCnt(CT_TypeDef *CTx)
{
    return (READ_REG(CTx->COUNT));
}


/**
 * @}CT Functions
 */


/**
 * @} CT_LL
 */

/* Exported functions --------------------------------------------------------*/


/** @addtogroup QSPI_Exported_Functions
 * @{
 */
void LL_CT_StructInit(CT_InitTypeDef *CT_InitStruct);


void LL_CT_Init(CT_TypeDef *CTx, CT_InitTypeDef *CT_InitStruct);


/**
 * @}
 */
/* End of exported functions -------------------------------------------------*/


/**
 * @}FM15F3XX_LL_Driver
 */
#endif /* defined (CT) */


#ifdef __cplusplus
}
#endif

#endif
/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/

