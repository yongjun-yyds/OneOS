/**
 ******************************************************************************
 * @file    fm15f3xx_ll_alarm.h
 * @author  WYL
 * @version V1.0.0
 * @date    2020-03-17
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __FM15F3XX_ALARM_H
#define __FM15F3XX_ALARM_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "fm15f3xx.h"


/** @addtogroup FM15F366_LL_Driver
 * @{
 */


/** @defgroup ALM_LL ALM
 * @{
 */
/* Private types -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private constants ---------------------------------------------------------*/
/* Private macros ------------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/


/** @defgroup ALM_LL_MONITORINIT_Functions
 * @{
 */

/* Exported constants --------------------------------------------------------*/


/** @defgroup ALM_LL_Exported_Constants ALM Exported Constants
 * @{
 */


/** @defgroup LL_ALM_Sensors0En
 * @{
 */
#define LL_ALM_SENSORE0_LIGHT   ALM_SENSORE0_ENLIGHT
#define LL_ALM_SENSORE0_VDET    ALM_SENSORE0_ENVDET
#define LL_ALM_SENSORE0_LDO12L  ALM_SENSORE0_ENLDO12L
#define LL_ALM_SENSORE0_LDO16L  ALM_SENSORE0_ENLDO16L
#define LL_ALM_SENSORE0_GLITCH  ALM_SENSORE0_ENGLITCH
#define LL_ALM_SENSORE0_IRC4M   ALM_SENSORE0_ENIRC4M


/** @defgroup LL_ALM_Sensors0En
 * @{
 */


/** @defgroup LL_ALM_Sensors0CFG
 * @{
 */
#define LL_ALM_VDDH_350V    (0x00000000U << ALM_SENSORE0_VDDH_Pos)
#define LL_ALM_VDDH_375V    (0x00000001U << ALM_SENSORE0_VDDH_Pos)
#define LL_ALM_BOR_160V     (0x00000000U << ALM_SENSORE0_BOR_Pos)
#define LL_ALM_BOR_256V     (0x00000001U << ALM_SENSORE0_BOR_Pos)
#define LL_ALM_VDDL_180V    (0x00000000U << ALM_SENSORE0_VDDL_Pos)
#define LL_ALM_VDDL_190V    (0x00000001U << ALM_SENSORE0_VDDL_Pos)
#define LL_ALM_VDDL_200V    (0x00000002U << ALM_SENSORE0_VDDL_Pos)
#define LL_ALM_VDDL_210V    (0x00000003U << ALM_SENSORE0_VDDL_Pos)
#define LL_ALM_VDDL_270V    (0x00000000U << ALM_SENSORE0_VDDL_Pos)
#define LL_ALM_VDDL_280V    (0x00000001U << ALM_SENSORE0_VDDL_Pos)
#define LL_ALM_VDDL_290V    (0x00000002U << ALM_SENSORE0_VDDL_Pos)
#define LL_ALM_VDDL_300V    (0x00000003U << ALM_SENSORE0_VDDL_Pos)


/** @defgroup LL_ALM_Sensors0CFG
 * @{
 */


/** @defgroup LL_ALM_Sensors1En
 * @{
 */
#define LL_ALM_SENSORE1_FRPTA   ALM_SENSORE1_ENFRPTA
#define LL_ALM_SENSORE1_FRPTB   ALM_SENSORE1_ENFRPTB
#define LL_ALM_SENSORE1_SLD     ALM_SENSORE1_ENSLD


/**
 * @}
 */


/** @defgroup LL_ALM_BaseTimer CLKSEL
 * @{
 */
#define LL_ALM_BTCLKSEL_SYSTEST     (0x00U)
#define LL_ALM_BTCLKSEL_IRC4M       (0x01U)
#define LL_ALM_BTCLKSEL_IRC16M      (0x02U)
#define LL_ALM_BTCLKSEL_PLL         (0x03U)
#define LL_ALM_BTCLKSEL_OSC         (0x04U)
#define LL_ALM_BTCLKSEL_DFS         (0x05U)
#define LL_ALM_BTCLKSEL_PLLDIV      (0x06U)
#define LL_ALM_BTCLKSEL_GPIO        (0x07U)
#define LL_ALM_BTCLKSEL_LDO12PUMP   (0x08U)
#define LL_ALM_BTCLKSEL_LDO16PUMP   (0x09U)
#define LL_ALM_BTCLKSEL_TRNG        (0x10U)
#define LL_ALM_BTCLKSEL_USB48M      (0x11U)
#define LL_ALM_BTCLKSEL_SYS         (0x12U)
#define LL_ALM_BTCLKSEL_SECURITY    (0x13U)
#define LL_ALM_BTCLKSEL_USB12M      (0x14U)
#define LL_ALM_BTCLKSEL_RTCXTAL32K  (0x15U)


/**
 * @}
 */


/** @defgroup LL_ALM_BTMode
 * @{
 */
#define LL_ALM_BT_ONESHOTMODE   0x0U
#define LL_ALM_BT_CYCLINGMODE   ALM_BT0CFG_BST0MD


/**
 * @}
 */


/** @defgroup LL_ALMRCD_SENSORS
 * @{
 */
#define LL_ALM_ALARMRESETA_LIGHT        (0x00000001U)
#define LL_ALM_ALARMRESETA_VDDL         (0x00000002U)
#define LL_ALM_ALARMRESETA_VDDH         (0x00000004U)
#define LL_ALM_ALARMRESETA_LDO12L       (0x00000008U)
#define LL_ALM_ALARMRESETA_LDO16L       (0x00000010U)
#define LL_ALM_ALARMRESETA_VDDGLITCH    (0x00000020U)
#define LL_ALM_ALARMRESETA_IRC4M        (0x00000040U)
#define LL_ALM_ALARMRESETA_BASATIMER0   (0x00000080U)
#define LL_ALM_ALARMRESETA_BASATIMER1   (0x00000100U)
#define LL_ALM_ALARMRESETA_SHIELD       (0x00001000U)

#define LL_ALM_ALARMRESETB_TAMPERALARM  (0x00000001U)
#define LL_ALM_ALARMRESETB_RTCALARM     (0x00000002U)
#define LL_ALM_ALARMRESETB_RTC1S        (0x00000004U)
#define LL_ALM_ALARMRESETB_BKPCHIPRST   (0x00000008U)

#define LL_ALM_ALARMRESETC_FLASHPE1     (0x00000001U)
#define LL_ALM_ALARMRESETC_FLASHPE2     (0x00000002U)
#define LL_ALM_ALARMRESETC_FLASHPEFF    (0x00000004U)
#define LL_ALM_ALARMRESETC_FLASHNEFF    (0x00000008U)
#define LL_ALM_ALARMRESETC_FLASHCE      (0x00000010U)
#define LL_ALM_ALARMRESETC_RAMPE        (0x00000020U)
#define LL_ALM_ALARMRESETC_RAERPE       (0x00000040U)
#define LL_ALM_ALARMRESETC_CACHERPE     (0x00000080U)
#define LL_ALM_ALARMRESETC_PMUTOE       (0x00000100U)
#define LL_ALM_ALARMRESETC_BUSTOE       (0x00000200U)
#define LL_ALM_ALARMRESETC_CPULOCKUP    (0x00000400U)
#define LL_ALM_ALARMRESETC_WDT          (0x00000800U)
#define LL_ALM_ALARMRESETC_REGPE        (0x00004000U)
#define LL_ALM_ALARMRESETC_MODECTRLRE   (0x00008000U)
#define LL_ALM_ALARMRESETC_RNGE         (0x00010000U)
#define LL_ALM_ALARMRESETC_BCAE         (0x00020000U)
#define LL_ALM_ALARMRESETC_HASHE        (0x00040000U)
#define LL_ALM_ALARMRESETC_PAEE         (0x00080000U)
#define LL_ALM_ALARMRESETC_CPUSELF      (0x00100000U)

#define LL_ALM_ALARMRESETD_SYSSELFALARM1    (0x00000001U)
#define LL_ALM_ALARMRESETD_SYSSELFALARM2    (0x00000002U)
#define LL_ALM_ALARMRESETD_GPIOA            (0x00000004U)
#define LL_ALM_ALARMRESETD_GPIOB            (0x00000008U)
#define LL_ALM_ALARMRESETD_GPIOC            (0x00000010U)
#define LL_ALM_ALARMRESETD_GPIOD            (0x00000020U)
#define LL_ALM_ALARMRESETD_GPIOE            (0x00000040U)
#define LL_ALM_ALARMRESETD_GPIOF            (0x00000080U)

#define LL_ALM_RESETE_POR       (0x00000001U)
#define LL_ALM_RESETE_BOR       (0x00000002U)
#define LL_ALM_RESETE_LDO12L    (0x00000004U)
#define LL_ALM_RESETE_LDO16L    (0x00000008U)
#define LL_ALM_RESETE_NRST      (0x00000010U)
#define LL_ALM_RESETE_LLWUEXIT  (0x00000020U)
#define LL_ALM_RESETE_LLWUERR   (0x00000040U)


/**
 * @}
 */


/** @defgroup LL_ALM_GetAlmRcdD
 * @{
 */
#define LL_ALM_NRST_ALL     0x0U
#define LL_ALM_NRST_LDO12   ALM_RSTBP_RSTBP


/**
 * @}
 */


/** @defgroup LL_ALM_MONITORCFG
 * @{
 */
#define LL_ALM_MONITOR_RECEN    (0x80U)
#define LL_ALM_MONITOR_RSTEN    (0x90U)
#define LL_ALM_MONITOR_RSTVDDEN (0xB0U)
#define LL_ALM_MONITOR_INTEN    (0x98U)
#define LL_ALM_MONITOR_DMAEN    (0x94U)


/**
 * @}
 */


/**
 * @}
 */

/* Exported macro ------------------------------------------------------------*/


/** @defgroup ALM_LL_Exported_Macros ALM Exported Macros
 * @{
 */


/**
 * @}
 */

/* Exported functions --------------------------------------------------------*/


/** @defgroup ALM_LL_Exported_Functions ALM Exported Functions
 * @{
 */


/** @defgroup ALM_LL_EF_CLKCFG clock configuration
 * @{
 */


/**
 * @brief  enable  Sensors
 * @rmtoll Sensors   LL_ALM_EnableSensors0
 * @param  Prescaler This parameter can be one of the following values:
 *         @arg @ref LL_ALM_SENSORE0_LIGHT
 *         @arg @ref LL_ALM_SENSORE0_VDET
 *         @arg @ref LL_ALM_SENSORE0_LDO12L
 *         @arg @ref LL_ALM_SENSORE0_LDO16L
 *         @arg @ref LL_ALM_SENSORE0_GLITCH
 *         @arg @ref LL_ALM_SENSORE0_IRC4M
 * @retval None
 */
__STATIC_INLINE void LL_ALM_EnableSensors0(uint32_t sensors)
{
    SET_BIT(ALM_SENSORS0->EN, sensors);
}


/**
 * @brief  disable Sensors
 * @rmtoll Sensors   LL_ALM_DisableSensors0
 * @param  Prescaler This parameter can be one of the following values:
 *         @arg @ref LL_ALM_SENSORE0_LIGHT
 *         @arg @ref LL_ALM_SENSORE0_VDET
 *         @arg @ref LL_ALM_SENSORE0_LDO12L
 *         @arg @ref LL_ALM_SENSORE0_LDO16L
 *         @arg @ref LL_ALM_SENSORE0_GLITCH
 *         @arg @ref LL_ALM_SENSORE0_IRC4M
 * @retval None
 */
__STATIC_INLINE void LL_ALM_DisableSensors0(uint32_t sensors)
{
    CLEAR_BIT(ALM_SENSORS0->EN, sensors);
}


/**
 * @brief  enable  Sensors
 * @rmtoll Sensors     LL_ALM_Sensors1En
 * @param  Prescaler This parameter can be one of the following values:
 *         @arg @ref LL_ALM_SENSORE1_ENFRPTA
 *         @arg @ref LL_ALM_SENSORE1_ENFRPTB
 *         @arg @ref LL_ALM_SENSORE1_ENSLD
 * @retval None
 */
__STATIC_INLINE void LL_ALM_EnableSensors1(uint32_t sensors)
{
    SET_BIT(ALM_SENSORS1->EN, sensors);
}


/**
 * @brief  disable  Sensors
 * @rmtoll Sensors     LL_ALM_Sensors1En
 * @param  Prescaler This parameter can be one of the following values:
 *         @arg @ref LL_ALM_SENSORE1_ENFRPTA
 *         @arg @ref LL_ALM_SENSORE1_ENFRPTB
 *         @arg @ref LL_ALM_SENSORE1_ENSLD
 * @retval None
 */
__STATIC_INLINE void LL_ALM_DisableSensors1(uint32_t sensors)
{
    CLEAR_BIT(ALM_SENSORS1->EN, sensors);
}


/**
 * @brief  disable All Sensors
 * @rmtoll Sensors   LL_ALM_DisableAllSensors
 * @retval None
 */
__STATIC_INLINE void LL_ALM_DisableAllSensors(void)
{
    /* disable sensors0  */
    CLEAR_BIT(ALM_SENSORS0->EN, 0x777B);
    /* disable sensors1  */
    CLEAR_BIT(ALM_SENSORS1->EN, 0x1180);
}


/**
 * @brief  Set VDDH  Sensors
 * @rmtoll Sensors   LL_ALM_SetVDDH
 * @param  value This parameter can be one of the following values:
 *         @arg @ref LL_ALM_VDDH_350V
 *         @arg @ref LL_ALM_VDDH_375V
 * @retval None
 */
__STATIC_INLINE void LL_ALM_SetVDDH(uint32_t value)
{
    MODIFY_REG(ALM_SENSORS0->CFG, ALM_SENSORE0_VDDH_Msk, value);
}


/**
 * @brief  Set BOR Sensors
 * @rmtoll Sensors   LL_ALM_SetBOR
 * @param  value This parameter can be one of the following values:
 *         @arg @ref LL_ALM_BOR_160V
 *         @arg @ref LL_ALM_BOR_256V
 * @retval None
 */
__STATIC_INLINE void LL_ALM_SetBOR(uint32_t value)
{
    MODIFY_REG(ALM_SENSORS0->CFG, ALM_SENSORE0_BOR_Msk, value);
}


/**
 * @brief  Set VDDL  Sensors
 * @rmtoll Sensors   LL_ALM_SetVDDL
 * @param  value This parameter can be one of the following values:
 *         @arg @ref LL_ALM_VDDL_180V/LL_ALM_VDDL_270V
 *         @arg @ref LL_ALM_VDDL_190V/LL_ALM_VDDL_280V
 *         @arg @ref LL_ALM_VDDL_200V/LL_ALM_VDDL_290V
 *         @arg @ref LL_ALM_VDDL_210V/LL_ALM_VDDL_300V
 * @retval None
 */
__STATIC_INLINE void LL_ALM_SetVDDL(uint32_t value)
{
    MODIFY_REG(ALM_SENSORS0->CFG, ALM_SENSORE0_VDDL_Msk, value);
}


/**
 * @brief  BASE TIMER0 CLKA SELECT
 * @rmtoll BTCLKCFG     LL_ALM_SetBaseTimer0ClkA
 * @param  Prescaler This parameter can be one of the following values:
 *         @arg @ref LL_ALM_BTCLKSEL_SYSTEST
 *         @arg @ref LL_ALM_BTCLKSEL_IRC4M
 *         @arg @ref LL_ALM_BTCLKSEL_IRC16M
 *         @arg @ref LL_ALM_BTCLKSEL_PLL
 *         @arg @ref LL_ALM_BTCLKSEL_OSC
 *         @arg @ref LL_ALM_BTCLKSEL_DFS
 *         @arg @ref LL_ALM_BTCLKSEL_PLLDIV
 *         @arg @ref LL_ALM_BTCLKSEL_GPIO
 *         @arg @ref LL_ALM_BTCLKSEL_LDO12PUMP
 *         @arg @ref LL_ALM_BTCLKSEL_LDO16PUMP
 *         @arg @ref LL_ALM_BTCLKSEL_TRNG
 *         @arg @ref LL_ALM_BTCLKSEL_USB48M
 *         @arg @ref LL_ALM_BTCLKSEL_SYS
 *         @arg @ref LL_ALM_BTCLKSEL_SECURITY
 *         @arg @ref LL_ALM_BTCLKSEL_USB12M
 *         @arg @ref LL_ALM_BTCLKSEL_RTCXTAL32K
 * @retval None
 */
__STATIC_INLINE void LL_ALM_SetBaseTimer0ClkA(uint32_t sel)
{
    MODIFY_REG(ALM_SENSORS1->BTCLKC, ALM_BTCLKCFG_T0CLKASEL, sel);
}


/**
 * @brief  BASE TIMER0 CLKB SELECT
 * @rmtoll BTCLKCFG     LL_ALM_T0CLKASEL
 * @param  Prescaler This parameter can be one of the following values:
 *         @arg @ref LL_ALM_BTCLKSEL_SYSTEST
 *         @arg @ref LL_ALM_BTCLKSEL_IRC4M
 *         @arg @ref LL_ALM_BTCLKSEL_IRC16M
 *         @arg @ref LL_ALM_BTCLKSEL_PLL
 *         @arg @ref LL_ALM_BTCLKSEL_OSC
 *         @arg @ref LL_ALM_BTCLKSEL_DFS
 *         @arg @ref LL_ALM_BTCLKSEL_PLLDIV
 *         @arg @ref LL_ALM_BTCLKSEL_GPIO
 *         @arg @ref LL_ALM_BTCLKSEL_LDO12PUMP
 *         @arg @ref LL_ALM_BTCLKSEL_LDO16PUMP
 *         @arg @ref LL_ALM_BTCLKSEL_TRNG
 *         @arg @ref LL_ALM_BTCLKSEL_USB48M
 *         @arg @ref LL_ALM_BTCLKSEL_SYS
 *         @arg @ref LL_ALM_BTCLKSEL_SECURITY
 *         @arg @ref LL_ALM_BTCLKSEL_USB12M
 *         @arg @ref LL_ALM_BTCLKSEL_RTCXTAL32K
 * @retval None
 */
__STATIC_INLINE void LL_ALM_SetBaseTimer0ClkB(uint32_t sel)
{
    MODIFY_REG(ALM_SENSORS1->BTCLKC, ALM_BTCLKCFG_T0CLKBSEL, sel << ALM_BTCLKCFG_T0CLKBSEL_Pos);
}


/**
 * @brief  BASE TIMER1 CLKA SELECT
 * @rmtoll BTCLKCFG     LL_ALM_SetBaseTimer1ClkA
 * @param  Prescaler This parameter can be one of the following values:
 *         @arg @ref LL_ALM_BTCLKSEL_SYSTEST
 *         @arg @ref LL_ALM_BTCLKSEL_IRC4M
 *         @arg @ref LL_ALM_BTCLKSEL_IRC16M
 *         @arg @ref LL_ALM_BTCLKSEL_PLL
 *         @arg @ref LL_ALM_BTCLKSEL_OSC
 *         @arg @ref LL_ALM_BTCLKSEL_DFS
 *         @arg @ref LL_ALM_BTCLKSEL_PLLDIV
 *         @arg @ref LL_ALM_BTCLKSEL_GPIO
 *         @arg @ref LL_ALM_BTCLKSEL_LDO12PUMP
 *         @arg @ref LL_ALM_BTCLKSEL_LDO16PUMP
 *         @arg @ref LL_ALM_BTCLKSEL_TRNG
 *         @arg @ref LL_ALM_BTCLKSEL_USB48M
 *         @arg @ref LL_ALM_BTCLKSEL_SYS
 *         @arg @ref LL_ALM_BTCLKSEL_SECURITY
 *         @arg @ref LL_ALM_BTCLKSEL_USB12M
 *         @arg @ref LL_ALM_BTCLKSEL_RTCXTAL32K
 * @retval None
 */
__STATIC_INLINE void LL_ALM_SetBaseTimer1ClkA(uint32_t sel)
{
    MODIFY_REG(ALM_SENSORS1->BTCLKC, ALM_BTCLKCFG_T1CLKASEL, sel << ALM_BTCLKCFG_T1CLKASEL_Pos);
}


/**
 * @brief  BASE TIMER1 CLKB SELECT
 * @rmtoll BTCLKCFG     LL_ALM_SetBaseTimer1ClkB
 * @param  Prescaler This parameter can be one of the following values:
 *         @arg @ref LL_ALM_BTCLKSEL_SYSTEST
 *         @arg @ref LL_ALM_BTCLKSEL_IRC4M
 *         @arg @ref LL_ALM_BTCLKSEL_IRC16M
 *         @arg @ref LL_ALM_BTCLKSEL_PLL
 *         @arg @ref LL_ALM_BTCLKSEL_OSC
 *         @arg @ref LL_ALM_BTCLKSEL_DFS
 *         @arg @ref LL_ALM_BTCLKSEL_PLLDIV
 *         @arg @ref LL_ALM_BTCLKSEL_GPIO
 *         @arg @ref LL_ALM_BTCLKSEL_LDO12PUMP
 *         @arg @ref LL_ALM_BTCLKSEL_LDO16PUMP
 *         @arg @ref LL_ALM_BTCLKSEL_TRNG
 *         @arg @ref LL_ALM_BTCLKSEL_USB48M
 *         @arg @ref LL_ALM_BTCLKSEL_SYS
 *         @arg @ref LL_ALM_BTCLKSEL_SECURITY
 *         @arg @ref LL_ALM_BTCLKSEL_USB12M
 *         @arg @ref LL_ALM_BTCLKSEL_RTCXTAL32K
 * @retval None
 */
__STATIC_INLINE void LL_ALM_SetBaseTimer1ClkB(uint32_t sel)
{
    MODIFY_REG(ALM_SENSORS1->BTCLKC, ALM_BTCLKCFG_T1CLKBSEL, sel << ALM_BTCLKCFG_T1CLKBSEL_Pos);
}


/**
 * @brief  NON GATE BASE TIMER0 CLKA/B
 * @rmtoll Sensors     LL_ALM_NGT0CLKAB
 * @retval None
 */
__STATIC_INLINE void LL_ALM_DisableBaseTime0ClkGate(void)
{
    SET_BIT(ALM_SENSORS1->BTCLKC, ALM_BTCLKCFG_GTBS_T0CLKA | ALM_BTCLKCFG_GTBS_T0CLKB);
}


/**
 * @brief  GATE BASE TIMER0 CLKA/B
 * @rmtoll Sensors     LL_ALM_NGT0CLKAB
 * @retval None
 */
__STATIC_INLINE void LL_ALM_EnableBaseTime0ClkGate(void)
{
    CLEAR_BIT(ALM_SENSORS1->BTCLKC, ALM_BTCLKCFG_GTBS_T0CLKA | ALM_BTCLKCFG_GTBS_T0CLKB);
}


/**
 * @brief  NON GATE BASE TIMER1 CLKA/B
 * @rmtoll Sensors     LL_ALM_NGT0CLKAB
 * @retval None
 */
__STATIC_INLINE void LL_ALM_DisableBaseTime1ClkGate(void)
{
    SET_BIT(ALM_SENSORS1->BTCLKC, ALM_BTCLKCFG_GTBS_T1CLKA | ALM_BTCLKCFG_GTBS_T1CLKB);
}


/**
 * @brief  GATE BASE TIMER1 CLKA/B
 * @rmtoll Sensors     LL_ALM_NGT0CLKAB
 * @retval None
 */
__STATIC_INLINE void LL_ALM_EnableBaseTime1ClkGate(void)
{
    CLEAR_BIT(ALM_SENSORS1->BTCLKC, ALM_BTCLKCFG_GTBS_T1CLKA | ALM_BTCLKCFG_GTBS_T1CLKB);
}


/**
 * @brief  ENABLE BASE TIMER
 * @rmtoll Sensors     LL_ALM_EnableBaseTimer
 * @retval None
 */
__STATIC_INLINE void LL_ALM_EnableBaseTimer(uint32_t x)
{
    SET_BIT(ALM_SENSORS1->BT[x].CFG, ALM_BT0CFG_BST0EN);
}


/**
 * @brief  DISABLE BASE TIMER
 * @rmtoll Sensors     LL_ALM_DisableBaseTimer
 * @retval None
 */
__STATIC_INLINE void LL_ALM_DisableBaseTimer(uint32_t x)
{
    CLEAR_BIT(ALM_SENSORS1->BT[x].CFG, ALM_BT0CFG_BST0EN);
}


/**
 * @brief  ENABLE BASE TIMER FREDET
 * @rmtoll Sensors     LL_ALM_EnableBaseTimerFreqDet
 * @retval None
 */
__STATIC_INLINE void LL_ALM_EnableBaseTimerFreqDet(uint32_t x)
{
    SET_BIT(ALM_SENSORS1->BT[x].CFG, ALM_BT0CFG_BST0FDETEN);
}


/**
 * @brief  DISABLE BASE TIMER FREDET
 * @rmtoll Sensors     LL_ALM_DisableBaseTimerFreqDet
 * @retval None
 */
__STATIC_INLINE void LL_ALM_DisableBaseTimerFreqDet(uint32_t x)
{
    CLEAR_BIT(ALM_SENSORS1->BT[x].CFG, ALM_BT0CFG_BST0FDETEN);
}


/**
 * @brief  BASE TIMER MODE
 * @rmtoll Sensors     LL_ALM_SetBaseTimerMode
 * @param  Prescaler This parameter can be one of the following values:
 *         @arg @ref LL_ALM_BT_ONESHOTMODE
 *         @arg @ref LL_ALM_BT_CYCLINGMODE
 * @retval None
 */
__STATIC_INLINE void LL_ALM_SetBaseTimerMode(uint32_t x, uint32_t mode)
{
    SET_BIT(ALM_SENSORS1->BT[x].CFG, mode);
}


/**
 * @brief  BASE TIMER  REF OF CLK
 * @rmtoll Sensors     LL_ALM_BTCLKRef
 * @param  Prescaler This parameter can be one of the following values:
 *         @arg @ref 0x000~0xFFF
 * @retval None
 */
__STATIC_INLINE void LL_ALM_SetBaseTimerClkRef(uint32_t x, uint32_t cnt)
{
    WRITE_REG(ALM_SENSORS1->BT[x].CLKAC, cnt);
}


/**
 * @brief  BASE TIMER LOWER LIMITS OF CLK
 * @rmtoll Sensors     LL_ALM_SetBaseTimerClkL
 * @param  Prescaler This parameter can be one of the following values:
 *         @arg @ref 0x000~0xFFF
 * @retval None
 */
__STATIC_INLINE void LL_ALM_SetBaseTimerClkL(uint32_t x, uint32_t cnt)
{
    WRITE_REG(ALM_SENSORS1->BT[x].CLKBCL, cnt);
}


/**
 * @brief  BASE TIMER HIGH LIMITS OF CLK
 * @rmtoll Sensors     LL_ALM_SetBaseTimerClkH
 * @param  Prescaler This parameter can be one of the following values:
 *         @arg @ref 0x000~0xFFF
 * @retval None
 */
__STATIC_INLINE void LL_ALM_SetBaseTimerClkH(uint32_t x, uint32_t cnt)
{
    WRITE_REG(ALM_SENSORS1->BT[x].CLKBCH, cnt);
}


/**
 * @brief  CLEAR THE RESULT OF BASE TIMER && RESTART THE COMPARE IN ONESHOT MODE
 * @rmtoll Sensors     LL_ALM_ClearBaseTimer
 * @retval None
 */
__STATIC_INLINE void LL_ALM_ClearBaseTimer(uint32_t x)
{
    SET_BIT(ALM_SENSORS1->BT[x].CTRL, ALM_BT0CTRL_CLR);
}


/**
 * @brief  GET BASE TIMER BUSY FLAG
 * @rmtoll Sensors     LL_ALM_IsBusy_BaseTimer
 * @retval None
 */
__STATIC_INLINE uint32_t LL_ALM_IsBusy_BaseTimer(uint32_t x)
{
    return (READ_BIT(ALM_SENSORS1->BT[x].SR, ALM_BT0SR_BUSY) == ALM_BT0SR_BUSY);
}


/**
 * @brief  BASE TIMER IS IN BUSY
 * @rmtoll Sensors     LL_ALM_IsValidBaseTimer
 * @retval None
 */
__STATIC_INLINE uint32_t LL_ALM_IsValid_BaseTimer(uint32_t x)
{
    return (READ_BIT(ALM_SENSORS1->BT[x].RPT, ALM_BT0RPT_VALID) == ALM_BT0RPT_VALID);
}


/**
 * @brief  BASE TIMER FWARN= HWARN + LWARN
 * @rmtoll Sensors     LL_ALM_IsWarning_BaseTimerFreq
 * @retval None
 */
__STATIC_INLINE uint32_t LL_ALM_IsWarning_BaseTimerFreq(uint32_t x)
{
    return (READ_BIT(ALM_SENSORS1->BT[x].RPT, ALM_BT0RPT_FWARN) == ALM_BT0RPT_FWARN);
}


/**
 * @brief  BASE TIMER LWARN CLKB_COUNT < CLKL
 * @rmtoll Sensors     LL_ALM_IsWarning_BaseTimerLowFreq
 * @retval None
 */
__STATIC_INLINE uint32_t LL_ALM_IsWarning_BaseTimerLowFreq(uint32_t x)
{
    return (READ_BIT(ALM_SENSORS1->BT[x].RPT, ALM_BT0RPT_LWARN) == ALM_BT0RPT_LWARN);
}


/**
 * @brief  BASE TIMER HWARN CLKB_COUNT > CLKH
 * @rmtoll Sensors     LL_ALM_IsWarning_BaseTimerHighFreq
 * @retval None
 */
__STATIC_INLINE uint32_t LL_ALM_IsWarning_BaseTimerHighFreq(uint32_t x)
{
    return (READ_BIT(ALM_SENSORS1->BT[x].RPT, ALM_BT0RPT_HWARN) == ALM_BT0RPT_HWARN);
}


/**
 * @brief  BASE TIMER HWARN CLKB_COUNT < CLKH
 * @rmtoll Sensors     LL_ALM_GetBaseTimerResult
 * @retval None
 */
__STATIC_INLINE uint32_t LL_ALM_GetBaseTimerResult(uint32_t x)
{
    return (READ_BIT(ALM_SENSORS1->BT[x].RPT, ALM_BT0RPT_RESULT) >> ALM_BT0RPT_RESULT_Pos);
}


/**
 * @brief  SET ALARM OF LIGHT SENSOR
 * @rmtoll Sensors     LL_ALM_ConfigLight
 * @param  Prescaler This parameter can be one of the following values:
 *         @arg @ref
            [7]->FILTER_CFG : 0: ONLY SYNED; 1: FILTER 2 CYCLE;
            [6]->RESET_SENDOUT: 0: RESET PAD PULL DOWN; 1: DIDNOT ACCESS RESET PAD;
            [5]->RESET_CFG : 0: RESET MAIN DOMAIN��1: RESET VCC_EXT DOMAIN;
            [4]->RESET_EN : RESET ENABLE;
            [3]->INT_EN : INT&WAKEUP_EN FOR CPU;
            [2]->DMA_EN : INT&WAKEUP_EN FOR DMA;
            [1]->INV_EN : ALARM_USE = INV_EN ^ ALARM;
            [0]->RECORD_ENB : 0: RECORD ; 1 : NOT RECORD.
 * @retval None
 */
__STATIC_INLINE void LL_ALM_ConfigLight(uint32_t cgf)
{
    MODIFY_REG(ALM_MONITOR->GROUPA0, ALM_GROUPA0_A00, cgf);
}


/**
 * @brief  SET ALARM OF VCC_EXT_L
 * @rmtoll Sensors     LL_ALM_ConfigVddL
 * @param  Prescaler This parameter can be one of the following values:
 *         @arg @ref
            [7]->FILTER_CFG : 0: ONLY SYNED; 1: FILTER 2 CYCLE;
            [6]->RESET_SENDOUT: 0: RESET PAD PULL DOWN; 1: DIDNOT ACCESS RESET PAD;
            [5]->RESET_CFG : 0: RESET MAIN DOMAIN��1: RESET VCC_EXT DOMAIN;
            [4]->RESET_EN : RESET ENABLE;
            [3]->INT_EN : INT&WAKEUP_EN FOR CPU;
            [2]->DMA_EN : INT&WAKEUP_EN FOR DMA;
            [1]->INV_EN : ALARM_USE = INV_EN ^ ALARM;
            [0]->RECORD_ENB : 0: RECORD ; 1 : NOT RECORD.
 * @retval None
 */
__STATIC_INLINE void LL_ALM_ConfigVddL(uint32_t cgf)
{
    MODIFY_REG(ALM_MONITOR->GROUPA0, ALM_GROUPA0_A01, cgf << ALM_GROUPA0_A01_Pos);
}


/**
 * @brief  SET ALARM OF VCC_EXT_H
 * @rmtoll Sensors     LL_ALM_ConfigVddH
 * @param  Prescaler This parameter can be one of the following values:
 *         @arg @ref
            [7]->FILTER_CFG : 0: ONLY SYNED; 1: FILTER 2 CYCLE;
            [6]->RESET_SENDOUT: 0: RESET PAD PULL DOWN; 1: DIDNOT ACCESS RESET PAD;
            [5]->RESET_CFG : 0: RESET MAIN DOMAIN��1: RESET VCC_EXT DOMAIN;
            [4]->RESET_EN : RESET ENABLE;
            [3]->INT_EN : INT&WAKEUP_EN FOR CPU;
            [2]->DMA_EN : INT&WAKEUP_EN FOR DMA;
            [1]->INV_EN : ALARM_USE = INV_EN ^ ALARM;
            [0]->RECORD_ENB : 0: RECORD ; 1 : NOT RECORD.
 * @retval None
 */
__STATIC_INLINE void LL_ALM_ConfigVddH(uint32_t cgf)
{
    MODIFY_REG(ALM_MONITOR->GROUPA0, ALM_GROUPA0_A02, cgf << ALM_GROUPA0_A02_Pos);
}


/**
 * @brief  SET ALARM OF LDO12_L
 * @rmtoll Sensors     LL_ALM_ConfigLdo12L
 * @param  Prescaler This parameter can be one of the following values:
 *         @arg @ref
            [7]->FILTER_CFG : 0: ONLY SYNED; 1: FILTER 2 CYCLE;
            [6]->RESET_SENDOUT: 0: RESET PAD PULL DOWN; 1: DIDNOT ACCESS RESET PAD;
            [5]->RESET_CFG : 0: RESET MAIN DOMAIN��1: RESET VCC_EXT DOMAIN;
            [4]->RESET_EN : RESET ENABLE;
            [3]->INT_EN : INT&WAKEUP_EN FOR CPU;
            [2]->DMA_EN : INT&WAKEUP_EN FOR DMA;
            [1]->INV_EN : ALARM_USE = INV_EN ^ ALARM;
            [0]->RECORD_ENB : 0: RECORD ; 1 : NOT RECORD.
 * @retval None
 */
__STATIC_INLINE void LL_ALM_ConfigLDO12L(uint32_t cgf)
{
    MODIFY_REG(ALM_MONITOR->GROUPA0, ALM_GROUPA0_A03, cgf << ALM_GROUPA0_A03_Pos);
}


/**
 * @brief  SET ALARM OF LDO16_L
 * @rmtoll Sensors     LL_ALM_ConfigLdo16L
 * @param  Prescaler This parameter can be one of the following values:
 *         @arg @ref
            [7]->FILTER_CFG : 0: ONLY SYNED; 1: FILTER 2 CYCLE;
            [6]->RESET_SENDOUT: 0: RESET PAD PULL DOWN; 1: DIDNOT ACCESS RESET PAD;
            [5]->RESET_CFG : 0: RESET MAIN DOMAIN��1: RESET VCC_EXT DOMAIN;
            [4]->RESET_EN : RESET ENABLE;
            [3]->INT_EN : INT&WAKEUP_EN FOR CPU;
            [2]->DMA_EN : INT&WAKEUP_EN FOR DMA;
            [1]->INV_EN : ALARM_USE = INV_EN ^ ALARM;
            [0]->RECORD_ENB : 0: RECORD ; 1 : NOT RECORD.
 * @retval None
 */
__STATIC_INLINE void LL_ALM_ConfigLDO16L(uint32_t cgf)
{
    MODIFY_REG(ALM_MONITOR->GROUPA1, ALM_GROUPA1_A04, cgf << ALM_GROUPA1_A04_Pos);
}


/**
 * @brief  SET ALARM OF VCC_EXT_GLITCH
 * @rmtoll Sensors     LL_ALM_ConfigVddGlitch
 * @param  Prescaler This parameter can be one of the following values:
 *         @arg @ref
            [7]->FILTER_CFG : 0: ONLY SYNED; 1: FILTER 2 CYCLE;
            [6]->RESET_SENDOUT: 0: RESET PAD PULL DOWN; 1: DIDNOT ACCESS RESET PAD;
            [5]->RESET_CFG : 0: RESET MAIN DOMAIN��1: RESET VCC_EXT DOMAIN;
            [4]->RESET_EN : RESET ENABLE;
            [3]->INT_EN : INT&WAKEUP_EN FOR CPU;
            [2]->DMA_EN : INT&WAKEUP_EN FOR DMA;
            [1]->INV_EN : ALARM_USE = INV_EN ^ ALARM;
            [0]->RECORD_ENB : 0: RECORD ; 1 : NOT RECORD.
 * @retval None
 */
__STATIC_INLINE void LL_ALM_ConfigVddGlitch(uint32_t cgf)
{
    MODIFY_REG(ALM_MONITOR->GROUPA1, ALM_GROUPA1_A05, cgf << ALM_GROUPA1_A05_Pos);
}


/**
 * @brief  SET ALARM OF IRC4M
 * @rmtoll Sensors     LL_ALM_ConfigIrc4M
 * @param  Prescaler This parameter can be one of the following values:
 *         @arg @ref
            [7]->FILTER_CFG : 0: ONLY SYNED; 1: FILTER 2 CYCLE;
            [6]->RESET_SENDOUT: 0: RESET PAD PULL DOWN; 1: DIDNOT ACCESS RESET PAD;
            [5]->RESET_CFG : 0: RESET MAIN DOMAIN��1: RESET VCC_EXT DOMAIN;
            [4]->RESET_EN : RESET ENABLE;
            [3]->INT_EN : INT&WAKEUP_EN FOR CPU;
            [2]->DMA_EN : INT&WAKEUP_EN FOR DMA;
            [1]->INV_EN : ALARM_USE = INV_EN ^ ALARM;
            [0]->RECORD_ENB : 0: RECORD ; 1 : NOT RECORD.
 * @retval None
 */
__STATIC_INLINE void LL_ALM_ConfigIRC4M(uint32_t cgf)
{
    MODIFY_REG(ALM_MONITOR->GROUPA1, ALM_GROUPA1_A06, cgf << ALM_GROUPA1_A06_Pos);
}


/**
 * @brief  SET ALARM OF BASE TIMER0
 * @rmtoll Sensors     LL_ALM_ConfigBaseTimer0
 * @param  Prescaler This parameter can be one of the following values:
 *         @arg @ref
            [7]->FILTER_CFG : 0: ONLY SYNED; 1: FILTER 2 CYCLE;
            [6]->RESET_SENDOUT: 0: RESET PAD PULL DOWN; 1: DIDNOT ACCESS RESET PAD;
            [5]->RESET_CFG : 0: RESET MAIN DOMAIN��1: RESET VCC_EXT DOMAIN;
            [4]->RESET_EN : RESET ENABLE;
            [3]->INT_EN : INT&WAKEUP_EN FOR CPU;
            [2]->DMA_EN : INT&WAKEUP_EN FOR DMA;
            [1]->INV_EN : ALARM_USE = INV_EN ^ ALARM;
            [0]->RECORD_ENB : 0: RECORD ; 1 : NOT RECORD.
 * @retval None
 */
__STATIC_INLINE void LL_ALM_ConfigBaseTimer0(uint32_t cgf)
{
    MODIFY_REG(ALM_MONITOR->GROUPA1, ALM_GROUPA1_A07, cgf << ALM_GROUPA1_A07_Pos);
}


/**
 * @brief  SET ALARM OF BASE TIMER1
 * @rmtoll Sensors     LL_ALM_ConfigBaseTimer1
 * @param  Prescaler This parameter can be one of the following values:
 *         @arg @ref
            [7]->FILTER_CFG : 0: ONLY SYNED; 1: FILTER 2 CYCLE;
            [6]->RESET_SENDOUT: 0: RESET PAD PULL DOWN; 1: DIDNOT ACCESS RESET PAD;
            [5]->RESET_CFG : 0: RESET MAIN DOMAIN��1: RESET VCC_EXT DOMAIN;
            [4]->RESET_EN : RESET ENABLE;
            [3]->INT_EN : INT&WAKEUP_EN FOR CPU;
            [2]->DMA_EN : INT&WAKEUP_EN FOR DMA;
            [1]->INV_EN : ALARM_USE = INV_EN ^ ALARM;
            [0]->RECORD_ENB : 0: RECORD ; 1 : NOT RECORD.
 * @retval None
 */
__STATIC_INLINE void LL_ALM_ConfigBaseTimer1(uint32_t cgf)
{
    MODIFY_REG(ALM_MONITOR->GROUPA2, ALM_GROUPA2_A08, cgf << ALM_GROUPA2_A08_Pos);
}


/**
 * @brief  SET ALARM OF SHIELD
 * @rmtoll Sensors     LL_ALM_ConfigShield
 * @param  Prescaler This parameter can be one of the following values:
 *         @arg @ref
            [7]->FILTER_CFG : 0: ONLY SYNED; 1: FILTER 2 CYCLE;
            [6]->RESET_SENDOUT: 0: RESET PAD PULL DOWN; 1: DIDNOT ACCESS RESET PAD;
            [5]->RESET_CFG : 0: RESET MAIN DOMAIN��1: RESET VCC_EXT DOMAIN;
            [4]->RESET_EN : RESET ENABLE;
            [3]->INT_EN : INT&WAKEUP_EN FOR CPU;
            [2]->DMA_EN : INT&WAKEUP_EN FOR DMA;
            [1]->INV_EN : ALARM_USE = INV_EN ^ ALARM;
            [0]->RECORD_ENB : 0: RECORD ; 1 : NOT RECORD.
 * @retval None
 */
__STATIC_INLINE void LL_ALM_ConfigShield(uint32_t cgf)
{
    MODIFY_REG(ALM_MONITOR->GROUPA3, ALM_GROUPA3_A09, cgf << ALM_GROUPA3_A09_Pos);
}


/**
 * @brief  SET ALARM OF TAMPER
 * @rmtoll Sensors     LL_ALM_ConfigMonitorTamperAlarm
 * @param  Prescaler This parameter can be one of the following values:
 *         @arg @ref
            [7]->FILTER_CFG : 0: ONLY SYNED; 1: FILTER 2 CYCLE;
            [6]->RESET_SENDOUT: 0: RESET PAD PULL DOWN; 1: DIDNOT ACCESS RESET PAD;
            [5]->RESET_CFG : 0: RESET MAIN DOMAIN��1: RESET VCC_EXT DOMAIN;
            [4]->RESET_EN : RESET ENABLE;
            [3]->INT_EN : INT&WAKEUP_EN FOR CPU;
            [2]->DMA_EN : INT&WAKEUP_EN FOR DMA;
            [1]->INV_EN : ALARM_USE = INV_EN ^ ALARM;
            [0]->RECORD_ENB : 0: RECORD ; 1 : NOT RECORD.
 * @retval None
 */
__STATIC_INLINE void LL_ALM_ConfigTamperAlarm(uint32_t cgf)
{
    MODIFY_REG(ALM_MONITOR->GROUPB0, ALM_GROUPB0_B00, cgf << ALM_GROUPB0_B00_Pos);
}


/**
 * @brief  SET ALARM OF RTC THRESHOLD
 * @rmtoll Sensors     LL_ALM_ConfigRtcAlarm
 * @param  Prescaler This parameter can be one of the following values:
 *         @arg @ref
            [7]->FILTER_CFG : 0: ONLY SYNED; 1: FILTER 2 CYCLE;
            [6]->RESET_SENDOUT: 0: RESET PAD PULL DOWN; 1: DIDNOT ACCESS RESET PAD;
            [5]->RESET_CFG : 0: RESET MAIN DOMAIN��1: RESET VCC_EXT DOMAIN;
            [4]->RESET_EN : RESET ENABLE;
            [3]->INT_EN : INT&WAKEUP_EN FOR CPU;
            [2]->DMA_EN : INT&WAKEUP_EN FOR DMA;
            [1]->INV_EN : ALARM_USE = INV_EN ^ ALARM;
            [0]->RECORD_ENB : 0: RECORD ; 1 : NOT RECORD.
 * @retval None
 */
__STATIC_INLINE void LL_ALM_ConfigRtcAlarm(uint32_t cgf)
{
    MODIFY_REG(ALM_MONITOR->GROUPB0, ALM_GROUPB0_B01, cgf << ALM_GROUPB0_B01_Pos);
}


/**
 * @brief  SET ALARM OF RTC 1S
 * @rmtoll Sensors     LL_ALM_ConfigRtc1s
 * @param  Prescaler This parameter can be one of the following values:
 *         @arg @ref
            [7]->FILTER_CFG : 0: ONLY SYNED; 1: FILTER 2 CYCLE;
            [6]->RESET_SENDOUT: 0: RESET PAD PULL DOWN; 1: DIDNOT ACCESS RESET PAD;
            [5]->RESET_CFG : 0: RESET MAIN DOMAIN��1: RESET VCC_EXT DOMAIN;
            [4]->RESET_EN : RESET ENABLE;
            [3]->INT_EN : INT&WAKEUP_EN FOR CPU;
            [2]->DMA_EN : INT&WAKEUP_EN FOR DMA;
            [1]->INV_EN : ALARM_USE = INV_EN ^ ALARM;
            [0]->RECORD_ENB : 0: RECORD ; 1 : NOT RECORD.
 * @retval None
 */
__STATIC_INLINE void LL_ALM_ConfigRtc1s(uint32_t cgf)
{
    MODIFY_REG(ALM_MONITOR->GROUPB0, ALM_GROUPB0_B02, cgf << ALM_GROUPB0_B02_Pos);
}


/**
 * @brief  SET ALARM OF RTC CHIPRST REQ
 * @rmtoll Sensors     LL_ALM_ConfigBkpChipRst
 * @param  Prescaler This parameter can be one of the following values:
 *         @arg @ref
            [7]->FILTER_CFG : 0: ONLY SYNED; 1: FILTER 2 CYCLE;
            [6]->RESET_SENDOUT: 0: RESET PAD PULL DOWN; 1: DIDNOT ACCESS RESET PAD;
            [5]->RESET_CFG : 0: RESET MAIN DOMAIN��1: RESET VCC_EXT DOMAIN;
            [4]->RESET_EN : RESET ENABLE;
            [3]->INT_EN : INT&WAKEUP_EN FOR CPU;
            [2]->DMA_EN : INT&WAKEUP_EN FOR DMA;
            [1]->INV_EN : ALARM_USE = INV_EN ^ ALARM;
            [0]->RECORD_ENB : 0: RECORD ; 1 : NOT RECORD.
 * @retval None
 */
__STATIC_INLINE void LL_ALM_ConfigBkpChipRst(uint32_t cgf)
{
    MODIFY_REG(ALM_MONITOR->GROUPB0, ALM_GROUPB0_B03, cgf << ALM_GROUPB0_B03_Pos);
}


/**
 * @brief  SET ALARM OF FLASH PARITY ERROR 1
 * @rmtoll Sensors     LL_ALM_ConfigFlashPE1
 * @param  Prescaler This parameter can be one of the following values:
 *         @arg @ref
            [7]->FILTER_CFG : 0: ONLY SYNED; 1: FILTER 2 CYCLE;
            [6]->RESET_SENDOUT: 0: RESET PAD PULL DOWN; 1: DIDNOT ACCESS RESET PAD;
            [5]->RESET_CFG : 0: RESET MAIN DOMAIN��1: RESET VCC_EXT DOMAIN;
            [4]->RESET_EN : RESET ENABLE;
            [3]->INT_EN : INT&WAKEUP_EN FOR CPU;
            [2]->DMA_EN : INT&WAKEUP_EN FOR DMA;
            [1]->INV_EN : ALARM_USE = INV_EN ^ ALARM;
            [0]->RECORD_ENB : 0: RECORD ; 1 : NOT RECORD.
 * @retval None
 */
__STATIC_INLINE void LL_ALM_ConfigFlashPE1(uint32_t cgf)
{
    MODIFY_REG(ALM_MONITOR->GROUPC0, ALM_GROUPC0_C00, cgf << ALM_GROUPC0_C00_Pos);
}


/**
 * @brief  SET ALARM OF FLASH PARITY ERROR 2
 * @rmtoll Sensors     LL_ALM_ConfigFlashPE2
 * @param  Prescaler This parameter can be one of the following values:
 *         @arg @ref
            [7]->FILTER_CFG : 0: ONLY SYNED; 1: FILTER 2 CYCLE;
            [6]->RESET_SENDOUT: 0: RESET PAD PULL DOWN; 1: DIDNOT ACCESS RESET PAD;
            [5]->RESET_CFG : 0: RESET MAIN DOMAIN��1: RESET VCC_EXT DOMAIN;
            [4]->RESET_EN : RESET ENABLE;
            [3]->INT_EN : INT&WAKEUP_EN FOR CPU;
            [2]->DMA_EN : INT&WAKEUP_EN FOR DMA;
            [1]->INV_EN : ALARM_USE = INV_EN ^ ALARM;
            [0]->RECORD_ENB : 0: RECORD ; 1 : NOT RECORD.
 * @retval None
 */
__STATIC_INLINE void LL_ALM_ConfigFlashPE2(uint32_t cgf)
{
    MODIFY_REG(ALM_MONITOR->GROUPC0, ALM_GROUPC0_C01, cgf << ALM_GROUPC0_C01_Pos);
}


/**
 * @brief  SET ALARM OF FLASH PARITY ERROR FF
 * @rmtoll Sensors     LL_ALM_ConfigFlashPEFF
 * @param  Prescaler This parameter can be one of the following values:
 *         @arg @ref
            [7]->FILTER_CFG : 0: ONLY SYNED; 1: FILTER 2 CYCLE;
            [6]->RESET_SENDOUT: 0: RESET PAD PULL DOWN; 1: DIDNOT ACCESS RESET PAD;
            [5]->RESET_CFG : 0: RESET MAIN DOMAIN��1: RESET VCC_EXT DOMAIN;
            [4]->RESET_EN : RESET ENABLE;
            [3]->INT_EN : INT&WAKEUP_EN FOR CPU;
            [2]->DMA_EN : INT&WAKEUP_EN FOR DMA;
            [1]->INV_EN : ALARM_USE = INV_EN ^ ALARM;
            [0]->RECORD_ENB : 0: RECORD ; 1 : NOT RECORD.
 * @retval None
 */
__STATIC_INLINE void LL_ALM_ConfigFlashPEFF(uint32_t cgf)
{
    MODIFY_REG(ALM_MONITOR->GROUPC0, ALM_GROUPC0_C02, cgf << ALM_GROUPC0_C02_Pos);
}


/**
 * @brief  SET ALARM OF FLASH NOT ERROR FF
 * @rmtoll Sensors     LL_ALM_ConfigFlashNEFF
 * @param  Prescaler This parameter can be one of the following values:
 *         @arg @ref
            [7]->FILTER_CFG : 0: ONLY SYNED; 1: FILTER 2 CYCLE;
            [6]->RESET_SENDOUT: 0: RESET PAD PULL DOWN; 1: DIDNOT ACCESS RESET PAD;
            [5]->RESET_CFG : 0: RESET MAIN DOMAIN��1: RESET VCC_EXT DOMAIN;
            [4]->RESET_EN : RESET ENABLE;
            [3]->INT_EN : INT&WAKEUP_EN FOR CPU;
            [2]->DMA_EN : INT&WAKEUP_EN FOR DMA;
            [1]->INV_EN : ALARM_USE = INV_EN ^ ALARM;
            [0]->RECORD_ENB : 0: RECORD ; 1 : NOT RECORD.
 * @retval None
 */
__STATIC_INLINE void LL_ALM_ConfigFlashNEFF(uint32_t cgf)
{
    MODIFY_REG(ALM_MONITOR->GROUPC0, ALM_GROUPC0_C03, cgf << ALM_GROUPC0_C03_Pos);
}


/**
 * @brief  SET ALARM OF FLASH CTRL ERROR
 * @rmtoll Sensors     LL_ALM_ConfigFlashCtrlE
 * @param  Prescaler This parameter can be one of the following values:
 *         @arg @ref
            [7]->FILTER_CFG : 0: ONLY SYNED; 1: FILTER 2 CYCLE;
            [6]->RESET_SENDOUT: 0: RESET PAD PULL DOWN; 1: DIDNOT ACCESS RESET PAD;
            [5]->RESET_CFG : 0: RESET MAIN DOMAIN��1: RESET VCC_EXT DOMAIN;
            [4]->RESET_EN : RESET ENABLE;
            [3]->INT_EN : INT&WAKEUP_EN FOR CPU;
            [2]->DMA_EN : INT&WAKEUP_EN FOR DMA;
            [1]->INV_EN : ALARM_USE = INV_EN ^ ALARM;
            [0]->RECORD_ENB : 0: RECORD ; 1 : NOT RECORD.
 * @retval None
 */
__STATIC_INLINE void LL_ALM_ConfigFlashCtrlE(uint32_t cgf)
{
    MODIFY_REG(ALM_MONITOR->GROUPC1, ALM_GROUPC1_C04, cgf << ALM_GROUPC1_C04_Pos);
}


/**
 * @brief  SET ALARM OF RAM PARITY ERROR
 * @rmtoll Sensors     LL_ALM_ConfigRamPE
 * @param  Prescaler This parameter can be one of the following values:
 *         @arg @ref
            [7]->FILTER_CFG : 0: ONLY SYNED; 1: FILTER 2 CYCLE;
            [6]->RESET_SENDOUT: 0: RESET PAD PULL DOWN; 1: DIDNOT ACCESS RESET PAD;
            [5]->RESET_CFG : 0: RESET MAIN DOMAIN��1: RESET VCC_EXT DOMAIN;
            [4]->RESET_EN : RESET ENABLE;
            [3]->INT_EN : INT&WAKEUP_EN FOR CPU;
            [2]->DMA_EN : INT&WAKEUP_EN FOR DMA;
            [1]->INV_EN : ALARM_USE = INV_EN ^ ALARM;
            [0]->RECORD_ENB : 0: RECORD ; 1 : NOT RECORD.
 * @retval None
 */
__STATIC_INLINE void LL_ALM_ConfigRamPE(uint32_t cgf)
{
    MODIFY_REG(ALM_MONITOR->GROUPC1, ALM_GROUPC1_C05, cgf << ALM_GROUPC1_C05_Pos);
}


/**
 * @brief  SET ALARM OF RAE RAM PARITY ERROR
 * @rmtoll Sensors     LL_ALM_ConfigRaeRPE
 * @param  Prescaler This parameter can be one of the following values:
 *         @arg @ref
            [7]->FILTER_CFG : 0: ONLY SYNED; 1: FILTER 2 CYCLE;
            [6]->RESET_SENDOUT: 0: RESET PAD PULL DOWN; 1: DIDNOT ACCESS RESET PAD;
            [5]->RESET_CFG : 0: RESET MAIN DOMAIN��1: RESET VCC_EXT DOMAIN;
            [4]->RESET_EN : RESET ENABLE;
            [3]->INT_EN : INT&WAKEUP_EN FOR CPU;
            [2]->DMA_EN : INT&WAKEUP_EN FOR DMA;
            [1]->INV_EN : ALARM_USE = INV_EN ^ ALARM;
            [0]->RECORD_ENB : 0: RECORD ; 1 : NOT RECORD.
 * @retval None
 */
__STATIC_INLINE void LL_ALM_ConfigRAERamPE(uint32_t cgf)
{
    MODIFY_REG(ALM_MONITOR->GROUPC1, ALM_GROUPC1_C06, cgf << ALM_GROUPC1_C06_Pos);
}


/**
 * @brief  SET ALARM OF CACHE RAM PARITY ERROR
 * @rmtoll Sensors     LL_ALM_ConfigCacheRPE
 * @param  Prescaler This parameter can be one of the following values:
 *         @arg @ref
            [7]->FILTER_CFG : 0: ONLY SYNED; 1: FILTER 2 CYCLE;
            [6]->RESET_SENDOUT: 0: RESET PAD PULL DOWN; 1: DIDNOT ACCESS RESET PAD;
            [5]->RESET_CFG : 0: RESET MAIN DOMAIN��1: RESET VCC_EXT DOMAIN;
            [4]->RESET_EN : RESET ENABLE;
            [3]->INT_EN : INT&WAKEUP_EN FOR CPU;
            [2]->DMA_EN : INT&WAKEUP_EN FOR DMA;
            [1]->INV_EN : ALARM_USE = INV_EN ^ ALARM;
            [0]->RECORD_ENB : 0: RECORD ; 1 : NOT RECORD.
 * @retval None
 */
__STATIC_INLINE void LL_ALM_ConfigCacheRPE(uint32_t cgf)
{
    MODIFY_REG(ALM_MONITOR->GROUPC1, ALM_GROUPC1_C07, cgf << ALM_GROUPC1_C07_Pos);
}


/**
 * @brief  SET ALARM OF PMU TIMER OVERFLOW ERROR
 * @rmtoll Sensors     LL_ALM_ConfigPmuTOE
 * @param  Prescaler This parameter can be one of the following values:
 *         @arg @ref
            [7]->FILTER_CFG : 0: ONLY SYNED; 1: FILTER 2 CYCLE;
            [6]->RESET_SENDOUT: 0: RESET PAD PULL DOWN; 1: DIDNOT ACCESS RESET PAD;
            [5]->RESET_CFG : 0: RESET MAIN DOMAIN��1: RESET VCC_EXT DOMAIN;
            [4]->RESET_EN : RESET ENABLE;
            [3]->INT_EN : INT&WAKEUP_EN FOR CPU;
            [2]->DMA_EN : INT&WAKEUP_EN FOR DMA;
            [1]->INV_EN : ALARM_USE = INV_EN ^ ALARM;
            [0]->RECORD_ENB : 0: RECORD ; 1 : NOT RECORD.
 * @retval None
 */
__STATIC_INLINE void LL_ALM_ConfigPMUTOE(uint32_t cgf)
{
    MODIFY_REG(ALM_MONITOR->GROUPC2, ALM_GROUPC2_C08, cgf << ALM_GROUPC2_C08_Pos);
}


/**
 * @brief  SET ALARM OF BUS TIMER OVERFLOW ERROR
 * @rmtoll Sensors     LL_ALM_ConfigBusTOE
 * @param  Prescaler This parameter can be one of the following values:
 *         @arg @ref
            [7]->FILTER_CFG : 0: ONLY SYNED; 1: FILTER 2 CYCLE;
            [6]->RESET_SENDOUT: 0: RESET PAD PULL DOWN; 1: DIDNOT ACCESS RESET PAD;
            [5]->RESET_CFG : 0: RESET MAIN DOMAIN��1: RESET VCC_EXT DOMAIN;
            [4]->RESET_EN : RESET ENABLE;
            [3]->INT_EN : INT&WAKEUP_EN FOR CPU;
            [2]->DMA_EN : INT&WAKEUP_EN FOR DMA;
            [1]->INV_EN : ALARM_USE = INV_EN ^ ALARM;
            [0]->RECORD_ENB : 0: RECORD ; 1 : NOT RECORD.
 * @retval None
 */
__STATIC_INLINE void LL_ALM_ConfigBusTOE(uint32_t cgf)
{
    MODIFY_REG(ALM_MONITOR->GROUPC2, ALM_GROUPC2_C09, cgf << ALM_GROUPC2_C09_Pos);
}


/**
 * @brief  SET ALARM OF CPU LOCKUP
 * @rmtoll Sensors     LL_ALM_ConfigCpuLockUp
 * @param  Prescaler This parameter can be one of the following values:
 *         @arg @ref
            [7]->FILTER_CFG : 0: ONLY SYNED; 1: FILTER 2 CYCLE;
            [6]->RESET_SENDOUT: 0: RESET PAD PULL DOWN; 1: DIDNOT ACCESS RESET PAD;
            [5]->RESET_CFG : 0: RESET MAIN DOMAIN��1: RESET VCC_EXT DOMAIN;
            [4]->RESET_EN : RESET ENABLE;
            [3]->INT_EN : INT&WAKEUP_EN FOR CPU;
            [2]->DMA_EN : INT&WAKEUP_EN FOR DMA;
            [1]->INV_EN : ALARM_USE = INV_EN ^ ALARM;
            [0]->RECORD_ENB : 0: RECORD ; 1 : NOT RECORD.
 * @retval None
 */
__STATIC_INLINE void LL_ALM_ConfigCPULockUp(uint32_t cgf)
{
    MODIFY_REG(ALM_MONITOR->GROUPC2, ALM_GROUPC2_C10, cgf << ALM_GROUPC2_C10_Pos);
}


/**
 * @brief  SET ALARM OF WDT
 * @rmtoll Sensors     LL_ALM_ConfigWDT
 * @param  Prescaler This parameter can be one of the following values:
 *         @arg @ref
            [7]->FILTER_CFG : 0: ONLY SYNED; 1: FILTER 2 CYCLE;
            [6]->RESET_SENDOUT: 0: RESET PAD PULL DOWN; 1: DIDNOT ACCESS RESET PAD;
            [5]->RESET_CFG : 0: RESET MAIN DOMAIN��1: RESET VCC_EXT DOMAIN;
            [4]->RESET_EN : RESET ENABLE;
            [3]->INT_EN : INT&WAKEUP_EN FOR CPU;
            [2]->DMA_EN : INT&WAKEUP_EN FOR DMA;
            [1]->INV_EN : ALARM_USE = INV_EN ^ ALARM;
            [0]->RECORD_ENB : 0: RECORD ; 1 : NOT RECORD.
 * @retval None
 */
__STATIC_INLINE void LL_ALM_ConfigWDT(uint32_t cgf)
{
    MODIFY_REG(ALM_MONITOR->GROUPC2, ALM_GROUPC2_C11, cgf << ALM_GROUPC2_C11_Pos);
}


/**
 * @brief  SET ALARM OF REGISTER PARITY ERROR
 * @rmtoll Sensors     LL_ALM_ConfigRegPE
 * @param  Prescaler This parameter can be one of the following values:
 *         @arg @ref
            [7]->FILTER_CFG : 0: ONLY SYNED; 1: FILTER 2 CYCLE;
            [6]->RESET_SENDOUT: 0: RESET PAD PULL DOWN; 1: DIDNOT ACCESS RESET PAD;
            [5]->RESET_CFG : 0: RESET MAIN DOMAIN��1: RESET VCC_EXT DOMAIN;
            [4]->RESET_EN : RESET ENABLE;
            [3]->INT_EN : INT&WAKEUP_EN FOR CPU;
            [2]->DMA_EN : INT&WAKEUP_EN FOR DMA;
            [1]->INV_EN : ALARM_USE = INV_EN ^ ALARM;
            [0]->RECORD_ENB : 0: RECORD ; 1 : NOT RECORD.
 * @retval None
 */
__STATIC_INLINE void LL_ALM_ConfigRegPE(uint32_t cgf)
{
    MODIFY_REG(ALM_MONITOR->GROUPC3, ALM_GROUPC3_C12, cgf << ALM_GROUPC3_C12_Pos);
}


/**
 * @brief  SET ALARM OF MODECTRL RPT ERROR
 * @rmtoll Sensors     LL_ALM_ConfigModeCtrlRE
 * @param  Prescaler This parameter can be one of the following values:
 *         @arg @ref
            [7]->FILTER_CFG : 0: ONLY SYNED; 1: FILTER 2 CYCLE;
            [6]->RESET_SENDOUT: 0: RESET PAD PULL DOWN; 1: DIDNOT ACCESS RESET PAD;
            [5]->RESET_CFG : 0: RESET MAIN DOMAIN��1: RESET VCC_EXT DOMAIN;
            [4]->RESET_EN : RESET ENABLE;
            [3]->INT_EN : INT&WAKEUP_EN FOR CPU;
            [2]->DMA_EN : INT&WAKEUP_EN FOR DMA;
            [1]->INV_EN : ALARM_USE = INV_EN ^ ALARM;
            [0]->RECORD_ENB : 0: RECORD ; 1 : NOT RECORD.
 * @retval None
 */
__STATIC_INLINE void LL_ALM_ConfigModeCtrlRE(uint32_t cgf)
{
    MODIFY_REG(ALM_MONITOR->GROUPC3, ALM_GROUPC3_C13, cgf << ALM_GROUPC3_C13_Pos);
}


/**
 * @brief  SET ALARM OF RNG ERROR
 * @rmtoll Sensors     LL_ALM_ConfigRngE
 * @param  Prescaler This parameter can be one of the following values:
 *         @arg @ref
            [7]->FILTER_CFG : 0: ONLY SYNED; 1: FILTER 2 CYCLE;
            [6]->RESET_SENDOUT: 0: RESET PAD PULL DOWN; 1: DIDNOT ACCESS RESET PAD;
            [5]->RESET_CFG : 0: RESET MAIN DOMAIN��1: RESET VCC_EXT DOMAIN;
            [4]->RESET_EN : RESET ENABLE;
            [3]->INT_EN : INT&WAKEUP_EN FOR CPU;
            [2]->DMA_EN : INT&WAKEUP_EN FOR DMA;
            [1]->INV_EN : ALARM_USE = INV_EN ^ ALARM;
            [0]->RECORD_ENB : 0: RECORD ; 1 : NOT RECORD.
 * @retval None
 */
__STATIC_INLINE void LL_ALM_ConfigRngE(uint32_t cgf)
{
    MODIFY_REG(ALM_MONITOR->GROUPC4, ALM_GROUPC4_C14, cgf << ALM_GROUPC4_C14_Pos);
}


/**
 * @brief  SET ALARM OF BCA ERROR
 * @rmtoll Sensors     LL_ALM_ConfigBCAE
 * @param  Prescaler This parameter can be one of the following values:
 *         @arg @ref
            [7]->FILTER_CFG : 0: ONLY SYNED; 1: FILTER 2 CYCLE;
            [6]->RESET_SENDOUT: 0: RESET PAD PULL DOWN; 1: DIDNOT ACCESS RESET PAD;
            [5]->RESET_CFG : 0: RESET MAIN DOMAIN��1: RESET VCC_EXT DOMAIN;
            [4]->RESET_EN : RESET ENABLE;
            [3]->INT_EN : INT&WAKEUP_EN FOR CPU;
            [2]->DMA_EN : INT&WAKEUP_EN FOR DMA;
            [1]->INV_EN : ALARM_USE = INV_EN ^ ALARM;
            [0]->RECORD_ENB : 0: RECORD ; 1 : NOT RECORD.
 * @retval None
 */
__STATIC_INLINE void LL_ALM_ConfigBCAE(uint32_t cgf)
{
    MODIFY_REG(ALM_MONITOR->GROUPC4, ALM_GROUPC4_C15, cgf << ALM_GROUPC4_C15_Pos);
}


/**
 * @brief  SET ALARM OF HASH ERROR
 * @rmtoll Sensors     LL_ALM_ConfigHashE
 * @param  Prescaler This parameter can be one of the following values:
 *         @arg @ref
            [7]->FILTER_CFG : 0: ONLY SYNED; 1: FILTER 2 CYCLE;
            [6]->RESET_SENDOUT: 0: RESET PAD PULL DOWN; 1: DIDNOT ACCESS RESET PAD;
            [5]->RESET_CFG : 0: RESET MAIN DOMAIN��1: RESET VCC_EXT DOMAIN;
            [4]->RESET_EN : RESET ENABLE;
            [3]->INT_EN : INT&WAKEUP_EN FOR CPU;
            [2]->DMA_EN : INT&WAKEUP_EN FOR DMA;
            [1]->INV_EN : ALARM_USE = INV_EN ^ ALARM;
            [0]->RECORD_ENB : 0: RECORD ; 1 : NOT RECORD.
 * @retval None
 */
__STATIC_INLINE void LL_ALM_ConfigHashE(uint32_t cgf)
{
    MODIFY_REG(ALM_MONITOR->GROUPC4, ALM_GROUPC4_C16, cgf << ALM_GROUPC4_C16_Pos);
}


/**
 * @brief  SET ALARM OF PAE ERROR
 * @rmtoll Sensors     LL_ALM_ConfigPAEE
 * @param  Prescaler This parameter can be one of the following values:
 *         @arg @ref
            [7]->FILTER_CFG : 0: ONLY SYNED; 1: FILTER 2 CYCLE;
            [6]->RESET_SENDOUT: 0: RESET PAD PULL DOWN; 1: DIDNOT ACCESS RESET PAD;
            [5]->RESET_CFG : 0: RESET MAIN DOMAIN��1: RESET VCC_EXT DOMAIN;
            [4]->RESET_EN : RESET ENABLE;
            [3]->INT_EN : INT&WAKEUP_EN FOR CPU;
            [2]->DMA_EN : INT&WAKEUP_EN FOR DMA;
            [1]->INV_EN : ALARM_USE = INV_EN ^ ALARM;
            [0]->RECORD_ENB : 0: RECORD ; 1 : NOT RECORD.
 * @retval None
 */
__STATIC_INLINE void LL_ALM_ConfigPAEE(uint32_t cgf)
{
    MODIFY_REG(ALM_MONITOR->GROUPC4, ALM_GROUPC4_C17, cgf << ALM_GROUPC4_C17_Pos);
}


/**
 * @brief  SET ALARM OF CPUSELF
 * @rmtoll Sensors     LL_ALM_ConfigPaeE
 * @param  Prescaler This parameter can be one of the following values:
 *         @arg @ref
            [7]->FILTER_CFG : 0: ONLY SYNED; 1: FILTER 2 CYCLE;
            [6]->RESET_SENDOUT: 0: RESET PAD PULL DOWN; 1: DIDNOT ACCESS RESET PAD;
            [5]->RESET_CFG : 0: RESET MAIN DOMAIN��1: RESET VCC_EXT DOMAIN;
            [4]->RESET_EN : RESET ENABLE;
            [3]->INT_EN : INT&WAKEUP_EN FOR CPU;
            [2]->DMA_EN : INT&WAKEUP_EN FOR DMA;
            [1]->INV_EN : ALARM_USE = INV_EN ^ ALARM;
            [0]->RECORD_ENB : 0: RECORD ; 1 : NOT RECORD.
 * @retval None
 */
__STATIC_INLINE void LL_ALM_ConfigCPUSelf(uint32_t cgf)
{
    MODIFY_REG(ALM_MONITOR->GROUPC5, ALM_GROUPC5_C18, cgf << ALM_GROUPC5_C18_Pos);
}


/**
 * @brief  SET ALARM OF SYSTEM SELF ALARM 1
 * @rmtoll Sensors     LL_ALM_ConfigSysAlarm1
 * @param  Prescaler This parameter can be one of the following values:
 *         @arg @ref
            [7]->FILTER_CFG : 0: ONLY SYNED; 1: FILTER 2 CYCLE;
            [6]->RESET_SENDOUT: 0: RESET PAD PULL DOWN; 1: DIDNOT ACCESS RESET PAD;
            [5]->RESET_CFG : 0: RESET MAIN DOMAIN��1: RESET VCC_EXT DOMAIN;
            [4]->RESET_EN : RESET ENABLE;
            [3]->INT_EN : INT&WAKEUP_EN FOR CPU;
            [2]->DMA_EN : INT&WAKEUP_EN FOR DMA;
            [1]->INV_EN : ALARM_USE = INV_EN ^ ALARM;
            [0]->RECORD_ENB : 0: RECORD ; 1 : NOT RECORD.
 * @retval None
 */
__STATIC_INLINE void LL_ALM_ConfigSysAlarm1(uint32_t cgf)
{
    MODIFY_REG(ALM_MONITOR->GROUPD0, ALM_GROUPD0_D00, cgf << ALM_GROUPD0_D00_Pos);
}


/**
 * @brief  SET ALARM OF SYSTEM SELF ALARM 2
 * @rmtoll Sensors     LL_ALM_ConfigSysAlarm2
 * @param  Prescaler This parameter can be one of the following values:
 *         @arg @ref
            [7]->FILTER_CFG : 0: ONLY SYNED; 1: FILTER 2 CYCLE;
            [6]->RESET_SENDOUT: 0: RESET PAD PULL DOWN; 1: DIDNOT ACCESS RESET PAD;
            [5]->RESET_CFG : 0: RESET MAIN DOMAIN��1: RESET VCC_EXT DOMAIN;
            [4]->RESET_EN : RESET ENABLE;
            [3]->INT_EN : INT&WAKEUP_EN FOR CPU;
            [2]->DMA_EN : INT&WAKEUP_EN FOR DMA;
            [1]->INV_EN : ALARM_USE = INV_EN ^ ALARM;
            [0]->RECORD_ENB : 0: RECORD ; 1 : NOT RECORD.
 * @retval None
 */
__STATIC_INLINE void LL_ALM_ConfigSysAlarm2(uint32_t cgf)
{
    MODIFY_REG(ALM_MONITOR->GROUPD0, ALM_GROUPD0_D01, cgf << ALM_GROUPD0_D01_Pos);
}


/**
 * @brief  SET ALARM OF PORT A
 * @rmtoll Sensors     LL_ALM_ConfigGPIOA
 * @param  Prescaler This parameter can be one of the following values:
 *         @arg @ref
            [7]->FILTER_CFG : 0: ONLY SYNED; 1: FILTER 2 CYCLE;
            [6]->RESET_SENDOUT: 0: RESET PAD PULL DOWN; 1: DIDNOT ACCESS RESET PAD;
            [5]->RESET_CFG : 0: RESET MAIN DOMAIN��1: RESET VCC_EXT DOMAIN;
            [4]->RESET_EN : RESET ENABLE;
            [3]->INT_EN : INT&WAKEUP_EN FOR CPU;
            [2]->DMA_EN : INT&WAKEUP_EN FOR DMA;
            [1]->INV_EN : ALARM_USE = INV_EN ^ ALARM;
            [0]->RECORD_ENB : 0: RECORD ; 1 : NOT RECORD.
 * @retval None
 */
__STATIC_INLINE void LL_ALM_ConfigGPIOA(uint32_t cgf)
{
    MODIFY_REG(ALM_MONITOR->GROUPD0, ALM_GROUPD0_D02, cgf << ALM_GROUPD0_D02_Pos);
}


/**
 * @brief  SET ALARM OF PORT B
 * @rmtoll Sensors     LL_ALM_ConfigGPIOB
 * @param  Prescaler This parameter can be one of the following values:
 *         @arg @ref
            [7]->FILTER_CFG : 0: ONLY SYNED; 1: FILTER 2 CYCLE;
            [6]->RESET_SENDOUT: 0: RESET PAD PULL DOWN; 1: DIDNOT ACCESS RESET PAD;
            [5]->RESET_CFG : 0: RESET MAIN DOMAIN��1: RESET VCC_EXT DOMAIN;
            [4]->RESET_EN : RESET ENABLE;
            [3]->INT_EN : INT&WAKEUP_EN FOR CPU;
            [2]->DMA_EN : INT&WAKEUP_EN FOR DMA;
            [1]->INV_EN : ALARM_USE = INV_EN ^ ALARM;
            [0]->RECORD_ENB : 0: RECORD ; 1 : NOT RECORD.
 * @retval None
 */
__STATIC_INLINE void LL_ALM_ConfigGPIOB(uint32_t cgf)
{
    MODIFY_REG(ALM_MONITOR->GROUPD0, ALM_GROUPD0_D03, cgf << ALM_GROUPD0_D03_Pos);
}


/**
 * @brief  SET ALARM OF PORT C
 * @rmtoll Sensors     LL_ALM_ConfigGPIOC
 * @param  Prescaler This parameter can be one of the following values:
 *         @arg @ref
            [7]->FILTER_CFG : 0: ONLY SYNED; 1: FILTER 2 CYCLE;
            [6]->RESET_SENDOUT: 0: RESET PAD PULL DOWN; 1: DIDNOT ACCESS RESET PAD;
            [5]->RESET_CFG : 0: RESET MAIN DOMAIN��1: RESET VCC_EXT DOMAIN;
            [4]->RESET_EN : RESET ENABLE;
            [3]->INT_EN : INT&WAKEUP_EN FOR CPU;
            [2]->DMA_EN : INT&WAKEUP_EN FOR DMA;
            [1]->INV_EN : ALARM_USE = INV_EN ^ ALARM;
            [0]->RECORD_ENB : 0: RECORD ; 1 : NOT RECORD.
 * @retval None
 */
__STATIC_INLINE void LL_ALM_ConfigGPIOC(uint32_t cgf)
{
    MODIFY_REG(ALM_MONITOR->GROUPD1, ALM_GROUPD1_D04, cgf << ALM_GROUPD1_D04_Pos);
}


/**
 * @brief  SET ALARM OF PORT D
 * @rmtoll Sensors     LL_ALM_ConfigGPIOD
 * @param  Prescaler This parameter can be one of the following values:
 *         @arg @ref
            [7]->FILTER_CFG : 0: ONLY SYNED; 1: FILTER 2 CYCLE;
            [6]->RESET_SENDOUT: 0: RESET PAD PULL DOWN; 1: DIDNOT ACCESS RESET PAD;
            [5]->RESET_CFG : 0: RESET MAIN DOMAIN��1: RESET VCC_EXT DOMAIN;
            [4]->RESET_EN : RESET ENABLE;
            [3]->INT_EN : INT&WAKEUP_EN FOR CPU;
            [2]->DMA_EN : INT&WAKEUP_EN FOR DMA;
            [1]->INV_EN : ALARM_USE = INV_EN ^ ALARM;
            [0]->RECORD_ENB : 0: RECORD ; 1 : NOT RECORD.
 * @retval None
 */
__STATIC_INLINE void LL_ALM_ConfigGPIOD(uint32_t cgf)
{
    MODIFY_REG(ALM_MONITOR->GROUPD1, ALM_GROUPD1_D05, cgf << ALM_GROUPD1_D05_Pos);
}


/**
 * @brief  SET ALARM OF PORT E
 * @rmtoll Sensors     LL_ALM_ConfigGPIOE
 * @param  Prescaler This parameter can be one of the following values:
 *         @arg @ref
            [7]->FILTER_CFG : 0: ONLY SYNED; 1: FILTER 2 CYCLE;
            [6]->RESET_SENDOUT: 0: RESET PAD PULL DOWN; 1: DIDNOT ACCESS RESET PAD;
            [5]->RESET_CFG : 0: RESET MAIN DOMAIN��1: RESET VCC_EXT DOMAIN;
            [4]->RESET_EN : RESET ENABLE;
            [3]->INT_EN : INT&WAKEUP_EN FOR CPU;
            [2]->DMA_EN : INT&WAKEUP_EN FOR DMA;
            [1]->INV_EN : ALARM_USE = INV_EN ^ ALARM;
            [0]->RECORD_ENB : 0: RECORD ; 1 : NOT RECORD.
 * @retval None
 */
__STATIC_INLINE void LL_ALM_ConfigGPIOE(uint32_t cgf)
{
    MODIFY_REG(ALM_MONITOR->GROUPD1, ALM_GROUPD1_D06, cgf << ALM_GROUPD1_D06_Pos);
}


/**
 * @brief  SET ALARM OF PORT F
 * @rmtoll Sensors     LL_ALM_ConfigGPIOF
 * @param  Prescaler This parameter can be one of the following values:
 *         @arg @ref
            [7]->FILTER_CFG : 0: ONLY SYNED; 1: FILTER 2 CYCLE;
            [6]->RESET_SENDOUT: 0: RESET PAD PULL DOWN; 1: DIDNOT ACCESS RESET PAD;
            [5]->RESET_CFG : 0: RESET MAIN DOMAIN��1: RESET VCC_EXT DOMAIN;
            [4]->RESET_EN : RESET ENABLE;
            [3]->INT_EN : INT&WAKEUP_EN FOR CPU;
            [2]->DMA_EN : INT&WAKEUP_EN FOR DMA;
            [1]->INV_EN : ALARM_USE = INV_EN ^ ALARM;
            [0]->RECORD_ENB : 0: RECORD ; 1 : NOT RECORD.
 * @retval None
 */
__STATIC_INLINE void LL_ALM_ConfigGPIOF(uint32_t cgf)
{
    MODIFY_REG(ALM_MONITOR->GROUPD1, ALM_GROUPD1_D07, cgf << ALM_GROUPD1_D07_Pos);
}


/**
 * @brief  Disable all Monitor
 * @rmtoll Sensors     LL_ALM_DisableAllMonitor
 * @retval None
 */
__STATIC_INLINE void LL_ALM_DisableAllMonitor(void)
{
    /* disable GROUPA0 */
    SET_BIT(ALM_MONITOR->GROUPA0, 0x01010101);
    /* disable GROUPA1 */
    SET_BIT(ALM_MONITOR->GROUPA1, 0x01010101);
    /* disable GROUPA2 */
    SET_BIT(ALM_MONITOR->GROUPA2, 0x01);
    /* disable GROUPA3 */
    SET_BIT(ALM_MONITOR->GROUPA3, 0x01);
    /* disable GROUPB0 */
    SET_BIT(ALM_MONITOR->GROUPB0, 0x01010101);
    /* disable GROUPC0 */
    SET_BIT(ALM_MONITOR->GROUPC0, 0x01010101);
    /* disable GROUPC1 */
    SET_BIT(ALM_MONITOR->GROUPC1, 0x01010101);
    /* disable GROUPC2 */
    SET_BIT(ALM_MONITOR->GROUPC2, 0x01010101);
    /* disable GROUPC3 */
    SET_BIT(ALM_MONITOR->GROUPC3, 0x01010000);
    /* disable GROUPC4 */
    SET_BIT(ALM_MONITOR->GROUPC4, 0x01010101);
    /* disable GROUPC5 */
    SET_BIT(ALM_MONITOR->GROUPC5, 0x01);
    /* disable GROUPD0 */
    SET_BIT(ALM_MONITOR->GROUPD0, 0x01010101);
    /* disable GROUPD1 */
    SET_BIT(ALM_MONITOR->GROUPD1, 0x01010101);
}


/**
 * @brief  READ THE ALARM RECORD A
 * @rmtoll Sensors     LL_ALM_IsActiveAlarmRecordA
 * @param  Prescaler This parameter can be combination of the following values:
 *         @arg @ref LL_ALM_ALARMRESETA_LIGHT
 *         @arg @ref LL_ALM_ALARMRESETA_VDDL
 *         @arg @ref LL_ALM_ALARMRESETA_VDDH
 *         @arg @ref LL_ALM_ALARMRESETA_LDO12L
 *         @arg @ref LL_ALM_ALARMRESETA_LDO16L
 *         @arg @ref LL_ALM_ALARMRESETA_VDDGLITCH
 *         @arg @ref LL_ALM_ALARMRESETA_IRC4M
 *         @arg @ref LL_ALM_ALARMRESETA_FRPTA
 *         @arg @ref LL_ALM_ALARMRESETA_FRPTB
 *         @arg @ref LL_ALM_ALARMRESETA_SHIELD
 * @retval
 */
__STATIC_INLINE uint32_t LL_ALM_IsActiveAlarm_RecordA(uint32_t Alarm)
{
    return (READ_BIT(ALM_MONITOR->ALMRCDA, Alarm) == Alarm);
}


/**
 * @brief  READ THE ALARM RECORD B
 * @rmtoll Sensors     LL_ALM_IsActiveAlarmRecordB
 * @param  Prescaler This parameter can be combination of the following values:
 *         @arg @ref LL_ALM_ALARMRESETB_TAMPERALARM
 *         @arg @ref LL_ALM_ALARMRESETB_RTCALARM
 *         @arg @ref LL_ALM_ALARMRESETB_RTC1S
 *         @arg @ref LL_ALM_ALARMRESETB_BKPCHIPRST
 * @retval
 */
__STATIC_INLINE uint32_t LL_ALM_IsActiveAlarm_RecordB(uint32_t Alarm)
{
    return (READ_BIT(ALM_MONITOR->ALMRCDB, Alarm) == Alarm);
}


/**
 * @brief  READ THE ALARM RECORD C
 * @rmtoll Sensors     LL_ALM_IsActiveAlarmRecordC
 * @param  Alarm This parameter can be combination of the following values:
 *         @arg @ref LL_ALM_ALARMRESETC_FLASHERR1
 *         @arg @ref LL_ALM_ALARMRESETC_FLASHERR2
 *         @arg @ref LL_ALM_ALARMRESETC_FLASHERRFF
 *         @arg @ref LL_ALM_ALARMRESETC_FLASHFF
 *         @arg @ref LL_ALM_ALARMRESETC_FLASHERR
 *         @arg @ref LL_ALM_ALARMRESETC_RAMPARERR
 *         @arg @ref LL_ALM_ALARMRESETC_RAERAMPARERR
 *         @arg @ref LL_ALM_ALARMRESETC_CACHEPARERR
 *         @arg @ref LL_ALM_ALARMRESETC_PMUTIMEOUT
 *         @arg @ref LL_ALM_ALARMRESETC_BUSTIMEOUT
 *         @arg @ref LL_ALM_ALARMRESETC_CPULOCKUP
 *         @arg @ref LL_ALM_ALARMRESETC_WDT
 *         @arg @ref LL_ALM_ALARMRESETC_REGPARERR
 *         @arg @ref LL_ALM_ALARMRESETC_RPTERR
 *         @arg @ref LL_ALM_ALARMRESETC_RNGERR
 *         @arg @ref LL_ALM_ALARMRESETC_BCAERR
 *         @arg @ref LL_ALM_ALARMRESETC_HASHERR
 *         @arg @ref LL_ALM_ALARMRESETC_PAEERR
 *         @arg @ref LL_ALM_ALARMRESETC_CPUSELF
 * @retval
 */
__STATIC_INLINE uint32_t LL_ALM_IsActiveAlarm_RecordC(uint32_t Alarm)
{
    return (READ_BIT(ALM_MONITOR->ALMRCDC, Alarm) == Alarm);
}


/**
 * @brief  READ THE ALARM RECORD D
 * @rmtoll Sensors     LL_ALM_IsActiveAlarmRecordD
 * @param  Alarm This parameter can be combination of the following values:
 *         @arg @ref LL_ALM_ALARMRESETD_SYSSELFALARM1
 *         @arg @ref LL_ALM_ALARMRESETD_SYSSELFALARM2
 *         @arg @ref LL_ALM_ALARMRESETD_GPIOA
 *         @arg @ref LL_ALM_ALARMRESETD_GPIOB
 *         @arg @ref LL_ALM_ALARMRESETD_GPIOC
 *         @arg @ref LL_ALM_ALARMRESETD_GPIOD
 *         @arg @ref LL_ALM_ALARMRESETD_GPIOE
 *         @arg @ref LL_ALM_ALARMRESETD_GPIOF
 * @retval
 */
__STATIC_INLINE uint32_t LL_ALM_IsActiveAlarm_RecordD(uint32_t Alarm)
{
    return (READ_BIT(ALM_MONITOR->ALMRCDD, Alarm) == Alarm);
}


/**
 * @brief  WRITE 1 to CLEAR ALL THE ALARM RPT
 * @rmtoll Sensors     LL_ALM_ClearAllAlarm
 * @retval None
 */
__STATIC_INLINE void LL_ALM_ClearAllAlarm(void)
{
    SET_BIT(ALM_MONITOR->ALMRCDCLR, ALM_ALMRCDCL_RPTCLR);
}


/**
 * @brief  WRITE 1 to CLEAR ALL THE ALARM RECORD
 * @rmtoll Sensors     LL_ALM_ClearAllAlarmRecord
 * @retval None
 */
__STATIC_INLINE void LL_ALM_ClearAllAlarmRecord(void)
{
    SET_BIT(ALM_MONITOR->ALMRCDCLR, ALM_ALMRCDCL_RCDCLR);
}


/**
 * @brief  SET NRST RERST RANGE
 * @rmtoll Sensors     LL_ALM_SetNrstRange
 * @param  range This parameter can be one of the following values:
 *         @arg @ref LL_ALM_NRST_LDO12
 *         @arg @ref LL_ALM_NRST_ALL
 * @retval None
 */
__STATIC_INLINE void LL_ALM_SetNrstRange(uint32_t range)
{
    MODIFY_REG(ALM_NRST->RSTBP, ALM_RSTBP_RSTBP, range);
}


/**
 * @brief  READ THE RST RECORD A
 * @rmtoll Sensors     LL_ALM_IsActiveResetRecordA
 * @param  Alarm This parameter can be combination of the following values:
 *         @arg @ref LL_ALM_ALARMRESETA_LIGHT
 *         @arg @ref LL_ALM_ALARMRESETA_VDDL
 *         @arg @ref LL_ALM_ALARMRESETA_VDDH
 *         @arg @ref LL_ALM_ALARMRESETA_LDO12L
 *         @arg @ref LL_ALM_ALARMRESETA_LDO16L
 *         @arg @ref LL_ALM_ALARMRESETA_VDDGLITCH
 *         @arg @ref LL_ALM_ALARMRESETA_IRC4M
 *         @arg @ref LL_ALM_ALARMRESETA_FRPTA
 *         @arg @ref LL_ALM_ALARMRESETA_FRPTB
 *         @arg @ref LL_ALM_ALARMRESETA_SHIELD
 * @retval
 */
__STATIC_INLINE uint32_t LL_ALM_IsActiveReset_RecordA(uint32_t rstrcd)
{
    return (READ_BIT(ALM_RST->RCDA, rstrcd) == rstrcd);
}


/**
 * @brief  READ THE RST RECORD B
 * @rmtoll Sensors     LL_ALM_IsActiveResetRecordB
 * @param  Alarm This parameter can be combination of the following values:
 *         @arg @ref LL_ALM_ALARMRESETB_TAMPERALARM
 *         @arg @ref LL_ALM_ALARMRESETB_RTCALARM
 *         @arg @ref LL_ALM_ALARMRESETB_RTC1S
 *         @arg @ref LL_ALM_ALARMRESETB_BKPCHIPRST
 * @retval
 */
__STATIC_INLINE uint32_t LL_ALM_IsActiveReset_RecordB(uint32_t rstrcd)
{
    return (READ_BIT(ALM_RST->RCDB, rstrcd) == rstrcd);
}


/**
 * @brief  READ THE RST RECORD C
 * @rmtoll Sensors     LL_ALM_IsActiveResetRecordC
 * @param  Alarm This parameter can be combination of the following values:
 *         @arg @ref LL_ALM_ALARMRESETC_FLASHERR1
 *         @arg @ref LL_ALM_ALARMRESETC_FLASHERR2
 *         @arg @ref LL_ALM_ALARMRESETC_FLASHERRFF
 *         @arg @ref LL_ALM_ALARMRESETC_FLASHFF
 *         @arg @ref LL_ALM_ALARMRESETC_FLASHERR
 *         @arg @ref LL_ALM_ALARMRESETC_RAMPARERR
 *         @arg @ref LL_ALM_ALARMRESETC_RAERAMPARERR
 *         @arg @ref LL_ALM_ALARMRESETC_CACHEPARERR
 *         @arg @ref LL_ALM_ALARMRESETC_PMUTIMEOUT
 *         @arg @ref LL_ALM_ALARMRESETC_BUSTIMEOUT
 *         @arg @ref LL_ALM_ALARMRESETC_CPULOCKUP
 *         @arg @ref LL_ALM_ALARMRESETC_WDT
 *         @arg @ref LL_ALM_ALARMRESETC_REGPARERR
 *         @arg @ref LL_ALM_ALARMRESETC_RPTERR
 *         @arg @ref LL_ALM_ALARMRESETC_RNGERR
 *         @arg @ref LL_ALM_ALARMRESETC_BCAERR
 *         @arg @ref LL_ALM_ALARMRESETC_HASHERR
 *         @arg @ref LL_ALM_ALARMRESETC_PAEERR
 *         @arg @ref LL_ALM_ALARMRESETC_CPUSELF
 * @retval
 */
__STATIC_INLINE uint32_t LL_ALM_IsActiveReset_RecordC(uint32_t rstrcd)
{
    return (READ_BIT(ALM_RST->RCDC, rstrcd) == rstrcd);
}


/**
 * @brief  READ THE RST RECORD D
 * @rmtoll Sensors     LL_ALM_IsActiveResetRecordD
 * @param  Alarm This parameter can be combination of the following values:
 *         @arg @ref LL_ALM_ALARMRESETD_SYSSELFALARM1
 *         @arg @ref LL_ALM_ALARMRESETD_SYSSELFALARM2
 *         @arg @ref LL_ALM_ALARMRESETD_GPIOA
 *         @arg @ref LL_ALM_ALARMRESETD_GPIOB
 *         @arg @ref LL_ALM_ALARMRESETD_GPIOC
 *         @arg @ref LL_ALM_ALARMRESETD_GPIOD
 *         @arg @ref LL_ALM_ALARMRESETD_GPIOE
 *         @arg @ref LL_ALM_ALARMRESETD_GPIOF
 * @retval None
 */
__STATIC_INLINE uint32_t LL_ALM_IsActiveReset_RecordD(uint32_t rstrcd)
{
    return (READ_BIT(ALM_RST->RCDD, rstrcd) == rstrcd);
}


/**
 * @brief  READ THE RST RECORD E
 * @rmtoll Sensors     LL_ALM_IsActiveRstRcdE
 * @param  Alarm This parameter can be combination of the following values:
 *         @arg @ref LL_ALM_RESETE_POR
 *         @arg @ref LL_ALM_RESETE_BOR
 *         @arg @ref LL_ALM_RESETE_LDO12L
 *         @arg @ref LL_ALM_RESETE_LDO16L
 *         @arg @ref LL_ALM_RESETE_NRST
 *         @arg @ref LL_ALM_RESETE_LLWUEXIT
 *         @arg @ref LL_ALM_RESETE_LLWUERR
 * @retval None
 */
__STATIC_INLINE uint32_t LL_ALM_IsActiveReset_RecordE(uint32_t reset)
{
    return (READ_BIT(ALM_RST->RCDE, reset) == reset);
}


/**
 * @brief  WRITE 1 to CLEAR ALL THE RST RCD
 * @rmtoll Sensors     LL_ALM_ClearAllResetRecord
 * @retval None
 */
__STATIC_INLINE void LL_ALM_ClearAllResetRecord(void)
{
    SET_BIT(ALM_RST->RCDCLR, ALM_RST_RCDCL);
}


/**
 * @brief  ENABLE POR AND BOR RESET
 * @rmtoll Sensors     LL_ALM_EnableGroupEReset
 * @param  reset This parameter can be combination of the following values:
 *         @arg @ref LL_ALM_RESETE_POR
 *         @arg @ref LL_ALM_RESETE_BOR
 *         @arg @ref LL_ALM_RESETE_LDO12L
 *         @arg @ref LL_ALM_RESETE_LDO16L
 *         @arg @ref LL_ALM_RESETE_NRST
 *         @arg @ref LL_ALM_RESETE_LLWUEXIT
 *         @arg @ref LL_ALM_RESETE_LLWUERR
 */
__STATIC_INLINE void LL_ALM_EnableGroupEReset(uint32_t reset)
{
    SET_BIT(ALM_RST->ENGROUPE, reset);
}


/**
 * @}
 */


/**
 * @}
 */


/**
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif /* FM15F366_ALARM_H */

/************************ (C) COPYRIGHT         *****END OF FILE****/
