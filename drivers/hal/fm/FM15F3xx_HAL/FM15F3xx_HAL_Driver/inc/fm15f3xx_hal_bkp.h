/**
 ******************************************************************************
 * @file    fm15f3xx_hal_bkp.h
 * @author  CLF
 * @version V1.0.0
 * @brief   Header file of BKP HAL module.
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __FM15F3xx_HAL_BKP_H
#define __FM15F3xx_HAL_BKP_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "fm15f3xx_hal_def.h"
#include "fm15f3xx_ll_bkp.h"


/** @addtogroup FM15F3xx_HAL_Driver
 * @{
 */


/** @addtogroup BKP
 * @{
 */

/* Exported types ------------------------------------------------------------*/


/** @defgroup BKP_Exported_Types BKP Exported Types
 * @{
 */


/** @defgroup BKP State Structure definition
 * @{
 */
typedef enum {
    HAL_BKP_STATE_RESET     = 0x00U,    /*!< BKP not yet initialized or disabled */
    HAL_BKP_STATE_READY     = 0x01U,    /*!< BKP initialized and ready for use   */
    HAL_BKP_STATE_BUSY      = 0x02U,    /*!< BKP internal process is ongoing     */
    HAL_BKP_STATE_TIMEOUT   = 0x03U,    /*!< BKP timeout state                   */
    HAL_BKP_STATE_ERROR     = 0x04U     /*!< BKP error state                     */
} HAL_BKP_StateTypeDef;


/**
 * @}
 */


/**
 * @brief  BKP Self Detect Init structure definition
 */
typedef struct {
    uint32_t SelfDetectMode;                /*! < Specifies the self detect mode.
                                               The parameter can be a value of @ref BKP_Self_Detect_Mode_Definitions.*/

    uint8_t HighFreqEn;                     /*! < Specifies the self detect high frequency enable or not.
                                               The parameter can be a value of @ref BKP_SelfDet_HighFreq_En_Definitions.*/

    uint8_t VolDetectTime;                  /*! < Specifies the voltage detect time.
                                               The parameter must be a number between Min_Data = 0x0 and Max-Data = 0xF.*/

    uint8_t VolDetectTimeInterval;          /*! < Specifies the voltage detect time interval.
                                               The parameter must be a number between Min_Data = 0x0 and Max-Data = 0x7.*/

    uint32_t VolDetectAutoEn;               /*! < Specifies voltage auto detect function enable or not.
                                               The parameter can be a value of @ref BKP_Vol_Detect_Auto_En_Definitions.*/

    uint8_t VolLowTrim;                     /*! < Specifies the voltage detect low voltage trim.
                                               The parameter must be a number between Min_Data = 0x0 and Max-Data = 0x7.*/

    uint8_t VolHighTrim;                    /*! < Specifies the voltage detect low voltage trim.
                                               The parameter must be a number between Min_Data = 0x0 and Max-Data = 0x7.*/

    uint8_t FreqLowTrim;                    /*! < Specifies the frequency detect low trim.
                                               The parameter must be a number between Min_Data = 0x0 and Max-Data = 0x7.*/

    uint8_t FreqHighTrim;                   /*! < Specifies the frequency detect high trim.
                                               The parameter must be a number between Min_Data = 0x0 and Max-Data = 0x7.*/

    uint8_t TemperLowTrim;                  /*! < Specifies the temperature detect low trim.
                                               The parameter must be a number between Min_Data = 0x00 and Max-Data = 0x1F.*/

    uint8_t TemperHighTrim;                 /*! < Specifies the temperature detect high trim.
                                               The parameter must be a number between Min_Data = 0x00 and Max-Data = 0x1F.*/

    uint8_t GlitchTrim;                     /*! < Specifies the glitch detect trim.
                                               The parameter must be a number between Min_Data = 0x0 and Max-Data = 0x1.*/

    uint32_t BGTemperMode;                  /*! < Specifies the bandgap and temperature work mode.
                                               The parameter can be a value of @ref BKP_BGTemper_Mode_Type_Definitions.*/
} BKP_SelfDetectionTypeDef;


/**
 * @brief  BKP tamper Detection Init structure definition
 */
typedef struct {
    uint32_t TamperDetectMode;              /*! < Specifies the Tamper Detection Mode.
                                                  The parameter can be a value of @ref BKP_Tamper_Detect_Mode_Definitions.*/

    uint32_t FilterClockSource;             /*! < Specifies the Tamper pin glitch filter clock source.
                                                  The parameter can be a value of @ref BKP_Tamper_Glitch_Filter_Clk_Source_Definitions.*/

    uint32_t FilterWidth;                   /*! < Specifies the Tamper pin glitch filter width.
                                                  The parameter can be a value of @ref BKP_Tamper_Glitch_Filter_Width_Definitions.*/

    uint32_t FilterEn;                      /*! < Specifies the Tamper pin glitch filter enable or not.
                                                  The parameter can be a value of @ref BKP_Tamper_Glitch_Filter_En_Definitions.*/

    uint32_t PinSampleFreq;                 /*! < Specifies the Tamper pin sample frequency.
                                                  The parameter can be a value of @ref BKP_Tamper_Pin_Sample_Frequency_Definitions.*/

    uint32_t PinSampleWidth;                /*! < Specifies the Tamper pin sample width.
                                                  The parameter can be a value of @ref BKP_Tamper_Pin_Sample_Width_Definitions.*/

    uint32_t PinPullSelect;                 /*! < Specifies the Tamper pin pull select.
                                                  The parameter can be a value of @ref BKP_Tamper_Pin_Pull_Select_Definitions.*/

    uint32_t PinPullEn;                     /*! < Specifies the Tamper pin pull enable or not.
                                                  The parameter can be a value of @ref BKP_Tamper_Pin_Pull_En_Definitions.*/

    uint32_t PinExpectData;                 /*! < Specifies the Tamper pin expect data.
                                                  The parameter can be a value of @ref BKP_Tamper_Expect_Data_Definitions.*/

    uint32_t PinPolarity;                   /*! < Specifies the Tamper pin polarity.
                                                  The parameter can be a value of @ref BKP_Tamper_Pin_Polarity_Definitions.*/

    uint32_t PinDirection;                  /*! < Specifies the Tamper pin direction.
                                                  The parameter can be a value of @ref BKP_Tamper_Pin_Direction_Definitions.*/

    uint32_t Pin;                           /*! < Specifies the Tamper pin.
                                                  The parameter can be a value of @ref BKP_Tamper_Pin_Definitions.*/

    uint32_t PinHysterSel;                  /*! < Specifies the Tamper pin hysteresis Select.
                                                  The parameter can be a value of @ref BKP_Tamper_Pin_Hysteresis_Select_Definitions.*/

    uint32_t PinPassfilterEn;               /*! < Specifies the Tamper pin passive filter enable or not.
                                                  The parameter can be a value of @ref BKP_Tamper_Pin_Passive_Filter_En_Definitions.*/

    uint32_t PinDriveStrength;              /*! < Specifies the Tamper pin drive strength enable or not.
                                                  The parameter can be a value of @ref BKP_Tamper_Pin_Drive_Strength_En_Definitions.*/

    uint32_t PinSlewRate;                   /*! < Specifies the Tamper pin slew rate.
                                                  The parameter can be a value of @ref BKP_Tamper_Pin_Slew_Rate_Definitions.*/

    uint32_t LFSRIntialValue;               /*! < Specifies the Dynamic tamper LFSR intial value.
                                                  The parameter must be a number between Min_Data = 0x0000 and Max-Data = 0xFFFF.*/

    uint32_t LFSRTap;                       /*! < Specifies the Dynamic tamper LFSR Tap.
                                                  The parameter can be a value of @ref BKP_Tamper_LFSR_Tap_Definitions.*/

    uint32_t TamperPinEn;                   /*! < Specifies the Tamper pin enable.
                                                  The parameter can be a value of @ref BKP_Tamper_Pin_Enable_Definitions.*/

    uint32_t TamperIntEn;                   /*! < Specifies the Tamper pin enable.
                                                  The parameter can be a value of @ref BKP_Tamper_Int_Type_Definitions.*/

    uint32_t TamperActiveLFSRx;             /*! < Specifies the Tamper active LFSR1 or LFSR0.
                                                  The parameter can be a value of @ref BKP_Tamper_LFSR_Type_Definitions.*/
} BKP_TamperDetectionTypeDef;


/**
 * @brief  BKP GPIO Init structure definition
 */
typedef struct {
    uint32_t GpioMode;                      /*! < Specifies the gpio pin mode.
                                                  The parameter can be a value of @ref BKP_GPIO_Pin_Mode_Definitions.*/

    uint32_t Pin;                           /*! < Specifies the gpio pin.
                                                  The parameter can be a value of @ref BKP_GPIO_Pins_Definitions.*/

    uint32_t Direction;                     /*! < Specifies the gpio pin direction.
                                                  The parameter can be a value of @ref BKP_Pin_Direction_Definitions.*/

    uint32_t PinInvEn;                      /*! < Specifies the gpioG0 output reverse enable or not.
                                                  The parameter can be a value of @ref BKP_Wakeup_Pin_Inv_Definitions.*/

    uint32_t PinODEn;                       /*! < Specifies the gpioG0 OpenDrain enable or not.
                                                  The parameter can be a value of @ref BKP_Wakeup_Pin_OpenDrain_Definitions.*/

    uint32_t WakupType;                     /*! < Specifies the gpioG0 RTC Status output type.
                                                  The parameter can be a value of @ref BKP_Wakeup_Output_Type_Definitions.*/

    uint32_t DigSignalOutputType;           /*! < Specifies the gpioG2 Dig signal output type.
                                                  The parameter can be a value of @ref BKP_Dig_Sig_Ouput_Definitions.*/
} BKP_GPIOTypeDef;


/**
 * @brief BKP Handle Structure definition
 */
typedef struct {
    BKP_TypeDef *Instance;                  /*! < Register base address */

    uint32_t RegfileType;                   /*! < Specifies the normal regfile type is normal or key.
                                                  The parameter can be a value of @ref BKP_Regfile_Type_Definitions.*/

    __IO HAL_BKP_StateTypeDef State;        /*! < RTC Time communication state */
} BKP_HandleTypeDef;


/**
 * @}
 */

/* Exported constants --------------------------------------------------------*/


/** @defgroup BKP_Exported_Constants BKP Exported Constants
 * @{
 */


/** @defgroup BKP_Tamper_Detect_Mode_Definitions
 * @{
 */
#define BKP_TAMPER_STATIC   (0x00000001)                    /*Tamper Static Detection*/
#define BKP_TAMPER_DYNAMIC  (0x00000002)                    /*Tamper Dynamic Detection*/


/**
 * @}
 */


/** @defgroup BKP_Tamper_Glitch_Filter_Clk_Source_Definitions
 * @{
 */
#define BKP_FILTERCLKSRC_512Hz      (0x00000000U)
#define BKP_FILTERCLKSRC_32768Hz    BKP_PIN_GLITCH_FILTER_PSC


/**
 * @}
 */


/** @defgroup BKP_Tamper_Glitch_Filter_width_Definitions
 * @{
 */
#define BKP_FILTERWIDTH_0   (0x00000000U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_1   (0x00000001U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_2   (0x00000002U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_3   (0x00000003U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_4   (0x00000004U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_5   (0x00000005U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_6   (0x00000006U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_7   (0x00000007U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_8   (0x00000008U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_9   (0x00000009U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_10  (0x0000000aU << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_11  (0x0000000bU << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_12  (0x0000000cU << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_13  (0x0000000dU << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_14  (0x0000000eU << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_15  (0x0000000fU << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_16  (0x00000010U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_17  (0x00000011U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_18  (0x00000012U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_19  (0x00000013U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_20  (0x00000014U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_21  (0x00000015U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_22  (0x00000016U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_23  (0x00000017U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_24  (0x00000018U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_25  (0x00000019U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_26  (0x0000001aU << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_27  (0x0000001bU << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_28  (0x0000001cU << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_29  (0x0000001dU << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_30  (0x0000001eU << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_31  (0x0000001fU << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_32  (0x00000020U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_33  (0x00000021U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_34  (0x00000022U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_35  (0x00000023U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_36  (0x00000024U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_37  (0x00000025U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_38  (0x00000026U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_39  (0x00000027U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_40  (0x00000028U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_41  (0x00000029U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_42  (0x0000002aU << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_43  (0x0000002bU << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_44  (0x0000002cU << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_45  (0x0000002dU << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_46  (0x0000002eU << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_47  (0x0000002fU << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_48  (0x00000030U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_49  (0x00000031U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_50  (0x00000032U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_51  (0x00000033U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_52  (0x00000034U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_53  (0x00000035U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_54  (0x00000036U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_55  (0x00000037U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_56  (0x00000038U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_57  (0x00000039U << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_58  (0x0000003aU << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_59  (0x0000003bU << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_60  (0x0000003cU << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_61  (0x0000003dU << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_62  (0x0000003eU << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)
#define BKP_FILTERWIDTH_63  (0x0000003fU << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)


/**
 * @}
 */


/** @defgroup BKP_Tamper_Glitch_Filter_En_Definitions
 * @{
 */
#define BKP_PINFILTER_DISABLE   0x00000000U
#define BKP_PINFILTER_ENABLE    BKP_PIN_GLITCH_FILTER_EN


/**
 * @}
 */


/** @defgroup BKP_Tamper_Pin_Sample_Frequency_Definitions
 * @{
 */
#define BKP_PINSAMPLEFREQ_0 (0x00000000U)                                           //0-8
#define BKP_PINSAMPLEFREQ_1 (0x00000001U << BKP_PIN_SAMPLE_FREQ_Pos)                //1-32
#define BKP_PINSAMPLEFREQ_2 (0x00000002U << BKP_PIN_SAMPLE_FREQ_Pos)                //2-128
#define BKP_PINSAMPLEFREQ_3 (0x00000003U << BKP_PIN_SAMPLE_FREQ_Pos)                //3-512


/**
 * @}
 */


/** @defgroup BKP_Tamper_Pin_Sample_Width_Definitions
 * @{
 */
#define BKP_PINSAMPLEWIDTH_0    (0x00000000U)
#define BKP_PINSAMPLEWIDTH_1    (0x00000001U << BKP_PIN_SAMPLE_WIDTH_Pos)
#define BKP_PINSAMPLEWIDTH_2    (0x00000002U << BKP_PIN_SAMPLE_WIDTH_Pos)
#define BKP_PINSAMPLEWIDTH_3    (0x00000003U << BKP_PIN_SAMPLE_WIDTH_Pos)


/**
 * @}
 */


/** @defgroup BKP_Tamper_Pin_Pull_Select_Definitions
 * @{
 */
#define BKP_PINPULLSEL_PULLDOWN (0x00000000U)
#define BKP_PINPULLSEL_PULLUP   (0x00000001U << BKP_PIN_PULL_SELECT_Pos)


/**
 * @}
 */


/** @defgroup BKP_Tamper_Pin_Pull_En_Definitions
 * @{
 */
#define BKP_PINPULL_DISABLE (0x00000000U)
#define BKP_PINPULL_ENABLE  (0x00000001U << BKP_PIN_PULL_ENABLE_Pos)


/**
 * @}
 */


/** @defgroup BKP_Tamper_Expect_Data_Definitions
 * @{
 */
#define BKP_PINEXPECTDATA_LOW       (0x00000000U << BKP_PIN_EXPECT_DATA_Pos)
#define BKP_PINEXPECTDATA_ACTIVE0   (0x00000001U << BKP_PIN_EXPECT_DATA_Pos)
#define BKP_PINEXPECTDATA_ACTIVE1   (0x00000002U << BKP_PIN_EXPECT_DATA_Pos)
#define BKP_PINEXPECTDATA_A0XORA1   (0x00000003U << BKP_PIN_EXPECT_DATA_Pos)


/**
 * @}
 */


/** @defgroup BKP_Tamper_Pin_Polarity_Definitions
 * @{
 */
#define BKP_PIN0POLARITY_POSITIVE   (0x00000000U)
#define BKP_PIN0POLARITY_NEGATIVE   (0x00000001U)

#define BKP_PIN1POLARITY_POSITIVE   (0x00000000U)
#define BKP_PIN1POLARITY_NEGATIVE   (0x00000002U)

#define BKP_PIN2POLARITY_POSITIVE   (0x00000000U)
#define BKP_PIN2POLARITY_NEGATIVE   (0x00000004U)

#define BKP_PIN3POLARITY_POSITIVE   (0x00000000U)
#define BKP_PIN3POLARITY_NEGATIVE   (0x00000008U)

#define BKP_PIN4POLARITY_POSITIVE   (0x00000000U)
#define BKP_PIN4POLARITY_NEGATIVE   (0x00000010U)

#define BKP_PIN5POLARITY_POSITIVE   (0x00000000U)
#define BKP_PIN5POLARITY_NEGATIVE   (0x00000020U)

#define BKP_PIN6POLARITY_POSITIVE   (0x00000000U)
#define BKP_PIN6POLARITY_NEGATIVE   (0x00000040U)

#define BKP_PIN7POLARITY_POSITIVE   (0x00000000U)
#define BKP_PIN7POLARITY_NEGATIVE   (0x00000080U)


/**
 * @}
 */


/** @defgroup BKP_Tamper_Pin_Direction_Definitions
 * @{
 */
#define BKP_PIN0DIR_INPUT   (0x00000000U)
#define BKP_PIN0DIR_OUTPUT  (0x00000001U)

#define BKP_PIN1DIR_INPUT   (0x00000000U)
#define BKP_PIN1DIR_OUTPUT  (0x00000002U)

#define BKP_PIN2DIR_INPUT   (0x00000000U)
#define BKP_PIN2DIR_OUTPUT  (0x00000004U)

#define BKP_PIN3DIR_INPUT   (0x00000000U)
#define BKP_PIN3DIR_OUTPUT  (0x00000008U)

#define BKP_PIN4DIR_INPUT   (0x00000000U)
#define BKP_PIN4DIR_OUTPUT  (0x00000010U)

#define BKP_PIN5DIR_INPUT   (0x00000000U)
#define BKP_PIN5DIR_OUTPUT  (0x00000020U)

#define BKP_PIN6DIR_INPUT   (0x00000000U)
#define BKP_PIN6DIR_OUTPUT  (0x00000040U)

#define BKP_PIN7DIR_INPUT   (0x00000000U)
#define BKP_PIN7DIR_OUTPUT  (0x00000080U)


/**
 * @}
 */


/** @defgroup BKP_Tamper_Pin_Definitions
 * @{
 */
#define BKP_TAMPER_PIN0 (0x00000001U)
#define BKP_TAMPER_PIN1 (0x00000002U)
#define BKP_TAMPER_PIN2 (0x00000004U)
#define BKP_TAMPER_PIN3 (0x00000008U)
#define BKP_TAMPER_PIN4 (0x00000010U)
#define BKP_TAMPER_PIN5 (0x00000020U)
#define BKP_TAMPER_PIN6 (0x00000040U)
#define BKP_TAMPER_PIN7 (0x00000080U)


/**
 * @}
 */


/** @defgroup BKP_Tamper_Pin_Hysteresis_Select_Definitions
 * @{
 */
#define BKP_PINHYSTER_LEVEL0    (0x00000000U)       //0- 305mv~440mv
#define BKP_PINHYSTER_LEVEL1    (0x00000001U)       //1- 490mv~705mv


/**
 * @}
 */


/** @defgroup BKP_Tamper_Pin_Passive_Filter_En_Definitions
 * @{
 */
#define BKP_PINPASSFILTER_DISABLE   (0x00000000U)
#define BKP_PINPASSFILTER_ENABLE    (0x00000001U << BKP_PIN_PASSIVE_FILTEN_Pos)


/**
 * @}
 */


/** @defgroup BKP_Tamper_Pin_Drive_Strength_En_Definitions
 * @{
 */
#define BKP_PINDRISTRENGTH_LOW  (0x00000000U << BKP_PIN_DRI_STRENGTHEN_Pos)
#define BKP_PINDRISTRENGTH_HIGH (0x00000001U << BKP_PIN_DRI_STRENGTHEN_Pos)


/**
 * @}
 */


/** @defgroup BKP_Tamper_Pin_Slew_Rate_Definitions
 * @{
 */
#define BKP_PINSLEW_SLOW    (0x00000000U << BKP_PIN_SLEW_RATE_Pos)
#define BKP_PINSLEW_FAST    (0x00000001U << BKP_PIN_SLEW_RATE_Pos)


/**
 * @}
 */


/** @defgroup BKP_Tamper_LFSR_Tap_Definitions
 * @{
 */
#define BKP_ACTIVELFSR0_TAP0    (0x0 << BKP_ACTIVE_LFSR0TAPSEL_Pos)
#define BKP_ACTIVELFSR0_TAP1    (0x1 << BKP_ACTIVE_LFSR0TAPSEL_Pos)
#define BKP_ACTIVELFSR0_TAP2    (0x2 << BKP_ACTIVE_LFSR0TAPSEL_Pos)
#define BKP_ACTIVELFSR0_TAP3    (0x3 << BKP_ACTIVE_LFSR0TAPSEL_Pos)
#define BKP_ACTIVELFSR0_TAP4    (0x4 << BKP_ACTIVE_LFSR0TAPSEL_Pos)
#define BKP_ACTIVELFSR0_TAP5    (0x5 << BKP_ACTIVE_LFSR0TAPSEL_Pos)
#define BKP_ACTIVELFSR0_TAP6    (0x6 << BKP_ACTIVE_LFSR0TAPSEL_Pos)
#define BKP_ACTIVELFSR0_TAP7    (0x7 << BKP_ACTIVE_LFSR0TAPSEL_Pos)

#define BKP_ACTIVELFSR1_TAP0    (0x0 << BKP_ACTIVE_LFSR1TAPSEL_Pos)
#define BKP_ACTIVELFSR1_TAP1    (0x1 << BKP_ACTIVE_LFSR1TAPSEL_Pos)
#define BKP_ACTIVELFSR1_TAP2    (0x2 << BKP_ACTIVE_LFSR1TAPSEL_Pos)
#define BKP_ACTIVELFSR1_TAP3    (0x3 << BKP_ACTIVE_LFSR1TAPSEL_Pos)
#define BKP_ACTIVELFSR1_TAP4    (0x4 << BKP_ACTIVE_LFSR1TAPSEL_Pos)
#define BKP_ACTIVELFSR1_TAP5    (0x5 << BKP_ACTIVE_LFSR1TAPSEL_Pos)
#define BKP_ACTIVELFSR1_TAP6    (0x6 << BKP_ACTIVE_LFSR1TAPSEL_Pos)
#define BKP_ACTIVELFSR1_TAP7    (0x7 << BKP_ACTIVE_LFSR1TAPSEL_Pos)


/**
 * @}
 */


/** @defgroup BKP_Tamper_Pin_Enable_Definitions
 * @{
 */
#define BKP_TAMPER_0    BKP_TAMPER_EN_PIN0DE
#define BKP_TAMPER_1    BKP_TAMPER_EN_PIN1DE
#define BKP_TAMPER_2    BKP_TAMPER_EN_PIN2DE
#define BKP_TAMPER_3    BKP_TAMPER_EN_PIN3DE
#define BKP_TAMPER_4    BKP_TAMPER_EN_PIN4DE
#define BKP_TAMPER_5    BKP_TAMPER_EN_PIN5DE
#define BKP_TAMPER_6    BKP_TAMPER_EN_PIN6DE
#define BKP_TAMPER_7    BKP_TAMPER_EN_PIN7DE


/**
 * @}
 */


/** @defgroup BKP_Self_Detect_Mode_Definitions
 * @{
 */
#define BKP_SELF_DET_VOL    (0x00000001U)
#define BKP_SELF_DET_FREQ   (0x00000002U)
#define BKP_SELF_DET_GLITCH (0x00000004U)


/**
 * @}
 */


/** @defgroup BKP_Vol_Detect_Auto_En_Definitions
 * @{
 */
#define BKP_VOLDET_AUTO_DISABLE (0x00000000U)
#define BKP_VOLDET_AUTO_ENABLE  (BKP_TAMPER_AUTOEN_VDAE)


/**
 * @}
 */


/** @defgroup BKP_BGTemper_Mode_Type_Definitions
 * @{
 */
#define BKP_BGTEMPER_ALWAYS (0x00000000U)
#define BKP_BGTEMPER_INTER  BKP_TAMPER_EN_BGSWME


/**
 * @}
 */


/** @defgroup BKP_GPIO_Pin_Mode_Definitions
 * @{
 */
#define BKP_GPIO_PIN_GENERIC    (0x00000001U)
#define BKP_GPIO_PIN_RTC_STATUS (0x00000002U)
#define BKP_GPIO_RTC_CLK        (0x00000003U)
#define BKP_GPIO_DIG_SIG_OUT    (0x00000004U)


/**
 * @}
 */


/** @defgroup BKP_GPIO_Pins_Definitions
 * @{
 */
#define BKP_GPIO_PIN0   (0x00000001U)
#define BKP_GPIO_PIN1   (0x00000002U)
#define BKP_GPIO_PIN2   (0x00000004U)
#define BKP_GPIO_PIN3   (0x00000008U)
#define BKP_GPIO_PIN4   (0x00000010U)
#define BKP_GPIO_PIN5   (0x00000020U)
#define BKP_GPIO_PIN6   (0x00000040U)
#define BKP_GPIO_PIN7   (0x00000080U)
#define BKP_GPIO_ALLPIN (0x000000FFU)


/**
 * @}
 */


/** @defgroup BKP_Pin_Direction_Definitions
 * @{
 */
#define BKP_GPIO_PIN_INPUT  (0x00000000U)
#define BKP_GPIO_PIN_OUTPUT (0x00000001U)


/**
 * @}
 */


/** @defgroup BKP_Wakeup_Pin_Inv_Definitions
 * @{
 */
#define BKP_PIN_INV_DISABLE (0x00000000U)
#define BKP_PIN_INV_ENABLE  RTC_TIME_WAKUPCFG_PININV


/**
 * @}
 */


/** @defgroup BKP_Wakeup_Pin_OpenDrain_Definitions
 * @{
 */
#define BKP_PIN_OD_DISABLE  (0x00000000U)
#define BKP_PIN_OD_ENABLE   RTC_TIME_WAKUPCFG_PINODEN


/**
 * @}
 */


/** @defgroup BKP_Wakeup_Output_Type_Definitions
 * @{
 */
#define BKP_RTC_CYCLE_FLAG      RTC_TIME_WAKUPEN_CYCFGWUEN
#define BKP_RTC_1S_FLAG         RTC_TIME_WAKUPEN_1SFGWUEN
#define BKP_RTC_ALARM_FLAG      RTC_TIME_WAKUPEN_SECONDFGWUEN
#define BKP_RTC_OV_FLAG         RTC_TIME_WAKUPEN_OVFGWUEN
#define BKP_RTC_INVALID_FLAG    RTC_TIME_WAKUPEN_INVLDFGWUEN
#define BKP_RTC_CYCLE_RECORD    RTC_TIME_WAKUPEN_CYCWUEN
#define BKP_RTC_1S_RECORD       RTC_TIME_WAKUPEN_1SWUEN
#define BKP_RTC_ALARM_RECORD    RTC_TIME_WAKUPEN_SECONDWUEN
#define BKP_RTC_OV_RECORD       RTC_TIME_WAKUPEN_OVWUEN
#define BKP_RTC_INVALID_RECORD  RTC_TIME_WAKUPEN_INVLDWUEN


/**
 * @}
 */


/** @defgroup BKP_Dig_Sig_Ouput_Definitions
 * @{
 */
#define BKP_DIG_SIGNAL_HT   (0x00000000U)
#define BKP_DIG_SIGNAL_LT   (0x1U << BKP_DIG_SIGNAL_SEL_Pos)
#define BKP_DIG_SIGNAL_HV   (0x2U << BKP_DIG_SIGNAL_SEL_Pos)
#define BKP_DIG_SIGNAL_LV   (0x3U << BKP_DIG_SIGNAL_SEL_Pos)
#define BKP_DIG_SIGNAL_HF   (0x4U << BKP_DIG_SIGNAL_SEL_Pos)
#define BKP_DIG_SIGNAL_LF   (0x5U << BKP_DIG_SIGNAL_SEL_Pos)
#define BKP_DIG_SIGNAL_SF   (0x6U << BKP_DIG_SIGNAL_SEL_Pos)
#define BKP_DIG_SIGNAL_RFU  (0x7U << BKP_DIG_SIGNAL_SEL_Pos)


/**
 * @}
 */


/** @defgroup BKP_Tamper_Int_Type_Definitions
 * @{
 */
#define BKP_INT_TAMPER          (0x0001U)
#define BKP_INT_FREQ_TAMPER     (0x0002U)
#define BKP_INT_VOL_TAMPER      (0x0004U)
#define BKP_INT_TEMP_TAMPER     (0x0008U)
#define BKP_INT_GLITCH_TAMPER   (0x0010U)
#define BKP_INT_TAMPER_0        (0x0020U)
#define BKP_INT_TAMPER_1        (0x0040U)
#define BKP_INT_TAMPER_2        (0x0080U)
#define BKP_INT_TAMPER_3        (0x0100U)
#define BKP_INT_TAMPER_4        (0x0200U)
#define BKP_INT_TAMPER_5        (0x0400U)
#define BKP_INT_TAMPER_6        (0x0800U)
#define BKP_INT_TAMPER_7        (0x1000U)
#define BKP_INT_SHIELD_TAMPER   (0x2000U)
#define BKP_INT_STFREQ_TAMPER   (0x4000U)
#define BKP_INT_STVOL_TAMPER    (0x8000U)
#define BKP_INT_STTEMP_TAMPER   (0x1000U)
#define BKP_INT_STGLITCH_TAMPER (0x2000U)
#define BKP_INT_ALL             (0x3FFFU)


/**
 * @}
 */


/** @defgroup BKP_Tamper_Flag_Type_Definitions
 * @{
 */
#define BKP_FLAG_TAMPER             (0x00000001U)
#define BKP_FLAG_TAMPER_ACK         (0x00000002U)
#define BKP_FLAG_FREQ_TAMPER        (0x00000004U)
#define BKP_FLAG_VOL_TAMPER         (0x00000008U)
#define BKP_FLAG_TEMP_TAMPER        (0x00000010U)
#define BKP_FLAG_GLITCH_TAMPER      (0x00000020U)
#define BKP_FLAG_TAMPER_0           (0x00000040U)
#define BKP_FLAG_TAMPER_1           (0x00000080U)
#define BKP_FLAG_TAMPER_2           (0x00000100U)
#define BKP_FLAG_TAMPER_3           (0x00000200U)
#define BKP_FLAG_TAMPER_4           (0x00000400U)
#define BKP_FLAG_TAMPER_5           (0x00000800U)
#define BKP_FLAG_TAMPER_6           (0x00001000U)
#define BKP_FLAG_TAMPER_7           (0x00002000U)
#define BKP_FLAG_SHIELD_TAMPER      (0x00004000U)
#define BKP_FLAG_STFREQ_TAMPER      (0x00008000U)
#define BKP_FLAG_STVOL_TAMPER       (0x00010000U)
#define BKP_FLAG_STTEMP_TAMPER      (0x00020000U)
#define BKP_FLAG_STGLITCH_TAMPER    (0x00040000U)
#define BKP_FLAG_RTC_STOP_FDET      (0x00100000U)
#define BKP_FLAG_RTC_LOW_FDET       (0x00200000U)
#define BKP_FLAG_RTC_HIGH_FDET      (0x00400000U)
#define BKP_FLAG_RTC_LOW_VOL        (0x00800000U)
#define BKP_FLAG_RTC_HIGH_VOL       (0x01000000U)
#define BKP_FLAG_RTC_LOW_TEMP       (0x02000000U)
#define BKP_FLAG_RTC_HIGH_TEMP      (0x04000000U)
#define BKP_FLAG_All                (0x0007FFFFU)


/**
 * @}
 */


/** @defgroup BKP_Regfile_Type_Definitions
 * @{
 */
#define BKP_REGFILE_NORMAL      (0x00000001U)
#define BKP_REGFILE_SECURE      (0x00000002U)
#define BKP_REGFILE_UNDEFINE    (0)


/**
 * @}
 */


/**
 * @brief  tamper_pass_cfg
 */
#define BKP_TAMPERFLAG_PASS_ALL     (0x1FFFF)
#define BKP_TAMPERFLAG_PASS_DEFAULT (0x0FF)


/**
 * @}
 */


/**
 * @brief  BKP_Tamper_LFSR_Type_Definitions
 */
#define BKP_TAMPER_ACTIVE_LFSR0     (0x00000001U)
#define BKP_TAMPER_ACTIVE_LFSR1     (0x00000002U)
#define BKP_TAMPER_ACTIVE_LFSR01    (0x00000004U)


/**
 * @}
 */


/**
 * @brief  BKP_SelfDet_HighFreq_En_Definitions
 */
#define BKP_SDET_HIGHFREQ_DISABLE   (0x00000001U)
#define BKP_SDET_HIGHFREQ_ENABLE    (0x00000002U)


/**
 * @}
 */


/**
 * @brief  BKP_Passflah_Type_Definitions
 */
#define BKP_PASSFLAG_FREQTAMPER     (0x00001U)
#define BKP_PASSFLAG_VOLTAMPER      (0x00002U)
#define BKP_PASSFLAG_TEMPTAMPER     (0x00004U)
#define BKP_PASSFLAG_GLIHTAMPER     (0x00008U)
#define BKP_PASSFLAG_TAMPER_0       (0x00010U)
#define BKP_PASSFLAG_TAMPER_1       (0x00020U)
#define BKP_PASSFLAG_TAMPER_2       (0x00040U)
#define BKP_PASSFLAG_TAMPER_3       (0x00080U)
#define BKP_PASSFLAG_TAMPER_4       (0x00100U)
#define BKP_PASSFLAG_TAMPER_5       (0x00200U)
#define BKP_PASSFLAG_TAMPER_6       (0x00400U)
#define BKP_PASSFLAG_TAMPER_7       (0x00800U)
#define BKP_PASSFLAG_STFREQTAMPER   (0x01000U)
#define BKP_PASSFLAG_STVOLTAMPER    (0x02000U)
#define BKP_PASSFLAG_STTEMPTAMPER   (0x04000U)
#define BKP_PASSFLAG_STGLIHTAMPER   (0x08000U)
#define BKP_PASSFLAG_STSTOPTAMPER   (0x10000U)


/**
 * @}
 */


/**
 * @}
 */

/* Exported macro ------------------------------------------------------------*/


/** @defgroup RTC_Exported_Macros RTC Exported Macros
 * @{
 */


/** @brief Reset BKP handle state
 * @param  __HANDLE__ specifies the BKP handle.
 * @retval None
 */
#define __HAL_BKP_RESET_HANDLE_STATE( __HANDLE__ ) ( (__HANDLE__)->State = HAL_BKP_STATE_RESET)


/**
 * @brief  Enable the BKP Tamper.
 * @param  __HANDLE__ specifies the BKP handle.
 * @param  __INTERRUPT__ specifies the BKP Tamper sources to be enabled or disabled.
 *          This parameter can be any combination of @ref BKP_Tamper_Pin_Enable_Definitions
 * @retval None
 */
#define __HAL_BKP_TAMPER_ENABLE( __HANDLE__, __TAMPER__ ) ( (__HANDLE__)->Instance->EN |= (__TAMPER__) )


/**
 * @brief  Enable the BKP Tamper interrupt.
 * @param  __HANDLE__ specifies the BKP handle.
 * @param  __INTERRUPT__ specifies the BKP Tamper interrupt sources to be enabled or disabled.
 *          This parameter can be any combination of @ref BKP_Tamper_Int_Type_Definitions
 * @retval None
 */
#define __HAL_BKP_TAMPER_ENABLE_IT( __HANDLE__, __INTERRUPT__ ) ( (__HANDLE__)->Instance->INTEN |= (__INTERRUPT__) )


/**
 * @brief  Disbale the BKP Tamper interrupt.
 * @param  __HANDLE__ specifies the BKP handle.
 * @param  __INTERRUPT__ specifies the BKP Tamper interrupt sources to be enabled or disabled.
 *          This parameter can be any combination of @ref BKP_Tamper_Int_Type_Definitions
 * @retval None
 */
#define __HAL_BKP_TAMPER_DISABLE_IT( __HANDLE__, __INTERRUPT__ ) ( (__HANDLE__)->Instance->INTEN &= ~(__INTERRUPT__) )


/**
 * @brief  Get the selected BKP Tamper's flag status.
 * @param  __HANDLE__ specifies the BKP handle.
 * @param  __FLAG__ specifies the BKP Tamper Flag to check.
 *          This parameter can be any combination of @ref BKP_Tamper_Flag_Type_Definitions
 * @retval None
 */
#define __HAL_BKP_TAMPER_ISAVTIVE_FLAG( __HANDLE__, __FLAG__ ) (READ_BIT( ( (__HANDLE__)->Instance->STATUS), (__FLAG__) ) == (__FLAG__) )


/**
 * @brief  Clear the selected BKP Tamper's flag status.
 * @param  __HANDLE__ specifies the BKP handle.
 * @param  __FLAG__ specifies the BKP Tamper Flag to check.
 *          This parameter can be any combination of @ref BKP_Tamper_Flag_Type_Definitions
 * @retval None
 */
#define __HAL_BKP_TAMPER_CLEAR_FLAG( __HANDLE__, __FLAG__ ) ( ( (__HANDLE__)->Instance->STATUS) |= (__FLAG__) )


/**
 * @}
 */

/* Exported functions --------------------------------------------------------*/


/** @addtogroup BKP_Exported_Functions
 * @{
 */


/** @addtogroup BKP_Exported_Functions_Group1
 * @{
 */
/* Initialization and de-initialization functions ----------------------------*/
HAL_StatusTypeDef HAL_BKP_Init(BKP_HandleTypeDef *hbkp);


void HAL_BKP_MspInit(BKP_HandleTypeDef *hbkp);


HAL_StatusTypeDef HAL_BKP_DeInit(BKP_HandleTypeDef *hbkp);


void HAL_BKP_MspDeInit(BKP_HandleTypeDef *hbkp);


/**
 * @}
 */


/** @addtogroup BKP_Exported_Functions_Group2
 * @{
 */
/* Regfile write,read and clear functions ------------------------------------*/
HAL_StatusTypeDef HAL_BKP_RegfileWrite(BKP_HandleTypeDef *hbkp, uint8_t *buff, uint8_t len, uint8_t mode);


HAL_StatusTypeDef HAL_BKP_RegfileRead(BKP_HandleTypeDef *hbkp, uint8_t *buff, uint8_t len, uint8_t mode);


HAL_StatusTypeDef HAL_BKP_KeyRegfileClear(BKP_HandleTypeDef *hbkp);


HAL_StatusTypeDef HAL_BKP_NormalRegfileClear(BKP_HandleTypeDef *hbkp);


/**
 * @}
 */


/** @addtogroup BKP_Exported_Functions_Group3
 * @{
 */
/* BKP Tamper Init and Self Detect Init functions ----------------------------*/
HAL_StatusTypeDef HAL_BKP_TamperInit(BKP_HandleTypeDef *hbkp, BKP_TamperDetectionTypeDef * sTamper);


HAL_StatusTypeDef HAL_BKP_SelfDetectInit(BKP_HandleTypeDef *hbkp, BKP_SelfDetectionTypeDef *sSelf, const uint32_t trim);


/**
 * @}
 */


/** @addtogroup BKP_Exported_Functions_Group4
 * @{
 */
/* BKP Gpio Init function ----------------------------------------------------*/
HAL_StatusTypeDef HAL_BKP_GPIOInit(BKP_HandleTypeDef *hbkp, BKP_GPIOTypeDef * sGpio);


void HAL_BKP_GPIOSetPin(BKP_HandleTypeDef *hbkp, BKP_GPIOTypeDef * sGpio);


void HAL_BKP_GPIOClearPin(BKP_HandleTypeDef *hbkp, BKP_GPIOTypeDef * sGpio);


void HAL_BKP_GPIOTogglePin(BKP_HandleTypeDef *hbkp, BKP_GPIOTypeDef * sGpio);


/**
 * @}
 */


/** @addtogroup BKP_Exported_Functions_Group5
 * @{
 */
/* BKP Tamper Int function ---------------------------------------------------*/
void HAL_BKP_TamperIRQHandler(BKP_HandleTypeDef *hbkp);


void HAL_BKP_TamperEventCallback(BKP_HandleTypeDef *hbkp);


/**
 * @}
 */


/** @addtogroup BKP_Exported_Functions_Group6
 * @{
 */
/* Peripheral State function -------------------------------------------------*/
HAL_BKP_StateTypeDef HAL_BKP_GetState(BKP_HandleTypeDef *hbkp);


/**
 * @}
 */


/**
 * @}
 */

/* Private macros ------------------------------------------------------------*/


/** @defgroup BKP_Private_Macros BKP Private Macros
 * @{
 */


/** @defgroup BKP_Private_Exported_Constants BKP Private Exported Constants
 * @{
 */
#define BKP_ALLBIT_MSK              (0xFFFFFFFFU)
#define BKP_TAMPER_LFSR_INIT_SEL    (0x1 << 29U)
#define BKP_GLITCH_TRIM_POS         (26U)
#define BKP_TEMPER_HIGH_TRIM_POS    (21U)
#define BKP_TEMPER_LOW_TRIM_POS     (16U)
#define BKP_VOL_HIGH_TRIM_POS       (12U)
#define BKP_VOL_LOW_TRIM_POS        (8U)
#define BKP_FREQ_HIGH_TRIM_POS      (4U)
#define BKP_VOL_DET_TIME_POS        (16U)
#define BKP_VOL_DET_TIME_INTER_POS  (16U)


/**
 * @}
 */


/** @defgroup BKP_IS_BKP_Definitions BKP Private macros to check input parameters
 * @{
 */
#define IS_BKP_TAMPER_MODE( MODE )                  ( ( (MODE) == BKP_TAMPER_STATIC) || \
                                                      ( (MODE) == BKP_TAMPER_DYNAMIC) )
#define IS_BKP_TAMPER_GLITCH_FILTER_CLK( SOURCE )   ( ( (SOURCE) == BKP_FILTERCLKSRC_512Hz) || \
                                                      ( (SOURCE) == BKP_FILTERCLKSRC_32768Hz) )
#define IS_BKP_TAMPER_GLITCH_FILTER_WIDTH( WIDTH )  ( ( (WIDTH) == BKP_FILTERWIDTH_0) || ( (WIDTH) == BKP_FILTERWIDTH_1) || ( (WIDTH) == BKP_FILTERWIDTH_2) || \
                                                      ( (WIDTH) == BKP_FILTERWIDTH_3) || ( (WIDTH) == BKP_FILTERWIDTH_4) || ( (WIDTH) == BKP_FILTERWIDTH_5) || \
                                                      ( (WIDTH) == BKP_FILTERWIDTH_6) || ( (WIDTH) == BKP_FILTERWIDTH_7) || ( (WIDTH) == BKP_FILTERWIDTH_8) || \
                                                      ( (WIDTH) == BKP_FILTERWIDTH_9) || ( (WIDTH) == BKP_FILTERWIDTH_10) || ( (WIDTH) == BKP_FILTERWIDTH_11) || \
                                                      ( (WIDTH) == BKP_FILTERWIDTH_12) || ( (WIDTH) == BKP_FILTERWIDTH_13) || ( (WIDTH) == BKP_FILTERWIDTH_14) || \
                                                      ( (WIDTH) == BKP_FILTERWIDTH_15) || ( (WIDTH) == BKP_FILTERWIDTH_16) || ( (WIDTH) == BKP_FILTERWIDTH_17) || \
                                                      ( (WIDTH) == BKP_FILTERWIDTH_18) || ( (WIDTH) == BKP_FILTERWIDTH_19) || ( (WIDTH) == BKP_FILTERWIDTH_20) || \
                                                      ( (WIDTH) == BKP_FILTERWIDTH_21) || ( (WIDTH) == BKP_FILTERWIDTH_22) || ( (WIDTH) == BKP_FILTERWIDTH_23) || \
                                                      ( (WIDTH) == BKP_FILTERWIDTH_24) || ( (WIDTH) == BKP_FILTERWIDTH_25) || ( (WIDTH) == BKP_FILTERWIDTH_26) || \
                                                      ( (WIDTH) == BKP_FILTERWIDTH_27) || ( (WIDTH) == BKP_FILTERWIDTH_28) || ( (WIDTH) == BKP_FILTERWIDTH_29) || \
                                                      ( (WIDTH) == BKP_FILTERWIDTH_30) || ( (WIDTH) == BKP_FILTERWIDTH_31) || ( (WIDTH) == BKP_FILTERWIDTH_32) || \
                                                      ( (WIDTH) == BKP_FILTERWIDTH_33) || ( (WIDTH) == BKP_FILTERWIDTH_34) || ( (WIDTH) == BKP_FILTERWIDTH_35) || \
                                                      ( (WIDTH) == BKP_FILTERWIDTH_36) || ( (WIDTH) == BKP_FILTERWIDTH_37) || ( (WIDTH) == BKP_FILTERWIDTH_38) || \
                                                      ( (WIDTH) == BKP_FILTERWIDTH_39) || ( (WIDTH) == BKP_FILTERWIDTH_40) || ( (WIDTH) == BKP_FILTERWIDTH_41) || \
                                                      ( (WIDTH) == BKP_FILTERWIDTH_42) || ( (WIDTH) == BKP_FILTERWIDTH_43) || ( (WIDTH) == BKP_FILTERWIDTH_44) || \
                                                      ( (WIDTH) == BKP_FILTERWIDTH_45) || ( (WIDTH) == BKP_FILTERWIDTH_46) || ( (WIDTH) == BKP_FILTERWIDTH_47) || \
                                                      ( (WIDTH) == BKP_FILTERWIDTH_48) || ( (WIDTH) == BKP_FILTERWIDTH_49) || ( (WIDTH) == BKP_FILTERWIDTH_50) || \
                                                      ( (WIDTH) == BKP_FILTERWIDTH_51) || ( (WIDTH) == BKP_FILTERWIDTH_52) || ( (WIDTH) == BKP_FILTERWIDTH_53) || \
                                                      ( (WIDTH) == BKP_FILTERWIDTH_54) || ( (WIDTH) == BKP_FILTERWIDTH_55) || ( (WIDTH) == BKP_FILTERWIDTH_56) || \
                                                      ( (WIDTH) == BKP_FILTERWIDTH_57) || ( (WIDTH) == BKP_FILTERWIDTH_59) || ( (WIDTH) == BKP_FILTERWIDTH_60) || \
                                                      ( (WIDTH) == BKP_FILTERWIDTH_61) || ( (WIDTH) == BKP_FILTERWIDTH_62) || ( (WIDTH) == BKP_FILTERWIDTH_63) )
#define IS_BKP_TAMPER_GLITCH_FILTER_EN( EN )        ( ( (EN) == BKP_PINFILTER_DISABLE) || \
                                                      ( (EN) == BKP_PINFILTER_ENABLE) )
#define IS_BKP_TAMPER_PIN_SAMPLE_FREQ( FREQ )       ( ( (FREQ) == BKP_PINSAMPLEFREQ_0) || ( (FREQ) == BKP_PINSAMPLEFREQ_1) || \
                                                      ( (FREQ) == BKP_PINSAMPLEFREQ_2) || ( (FREQ) == BKP_PINSAMPLEFREQ_3) )
#define IS_BKP_TAMPER_PIN_SAMPLE_WIDTH( WIDTH )     ( ( (WIDTH) == BKP_PINSAMPLEWIDTH_0) || ( (WIDTH) == BKP_PINSAMPLEWIDTH_1) || \
                                                      ( (WIDTH) == BKP_PINSAMPLEWIDTH_2) || ( (WIDTH) == BKP_PINSAMPLEWIDTH_3) )
#define IS_BKP_TAMPER_PIN_PULL_SEL( SEL )           ( ( (SEL) == BKP_PINPULLSEL_PULLDOWN) || \
                                                      ( (SEL) == BKP_PINPULLSEL_PULLUP) )
#define IS_BKP_TAMPER_PIN_PULL_EN( EN )             ( ( (EN) == BKP_PINPULL_DISABLE) || \
                                                      ( (EN) == BKP_PINPULL_ENABLE) )
#define IS_BKP_TAMPER_PIN_EXPECTDATA( DATA )        ( ( (DATA) == BKP_PINEXPECTDATA_LOW) || ( (DATA) == BKP_PINEXPECTDATA_ACTIVE0) || \
                                                      ( (DATA) == BKP_PINEXPECTDATA_ACTIVE1) || ( (DATA) == BKP_PINEXPECTDATA_A0XORA1) )
#define IS_BKP_TAMPER_PIN_POLARITY( POL )           ( ( (POL) == BKP_PIN0POLARITY_POSITIVE) || ( (POL) == BKP_PIN0POLARITY_NEGATIVE) || ( (POL) == BKP_PIN1POLARITY_POSITIVE) || \
                                                      ( (POL) == BKP_PIN1POLARITY_NEGATIVE) || ( (POL) == BKP_PIN2POLARITY_POSITIVE) || ( (POL) == BKP_PIN2POLARITY_NEGATIVE) || \
                                                      ( (POL) == BKP_PIN3POLARITY_POSITIVE) || ( (POL) == BKP_PIN3POLARITY_NEGATIVE) || ( (POL) == BKP_PIN4POLARITY_POSITIVE) || \
                                                      ( (POL) == BKP_PIN4POLARITY_NEGATIVE) || ( (POL) == BKP_PIN5POLARITY_POSITIVE) || ( (POL) == BKP_PIN5POLARITY_NEGATIVE) || \
                                                      ( (POL) == BKP_PIN6POLARITY_POSITIVE) || ( (POL) == BKP_PIN6POLARITY_NEGATIVE) || ( (POL) == BKP_PIN7POLARITY_POSITIVE) || \
                                                      ( (POL) == BKP_PIN7POLARITY_NEGATIVE) )
#define IS_BKP_TAMPER_PIN_DIRECTION( DIR )          ( ( (DIR) == BKP_PIN0DIR_INPUT) || ( (DIR) == BKP_PIN0DIR_OUTPUT) || ( (DIR) == BKP_PIN1DIR_INPUT) || \
                                                      ( (DIR) == BKP_PIN1DIR_OUTPUT) || ( (DIR) == BKP_PIN2DIR_INPUT) || ( (DIR) == BKP_PIN2DIR_OUTPUT) || \
                                                      ( (DIR) == BKP_PIN3DIR_INPUT) || ( (DIR) == BKP_PIN3DIR_OUTPUT) || ( (DIR) == BKP_PIN4DIR_INPUT) || \
                                                      ( (DIR) == BKP_PIN4DIR_OUTPUT) || ( (DIR) == BKP_PIN5DIR_INPUT) || ( (DIR) == BKP_PIN5DIR_OUTPUT) || \
                                                      ( (DIR) == BKP_PIN6DIR_INPUT) || ( (DIR) == BKP_PIN6DIR_OUTPUT) || ( (DIR) == BKP_PIN7DIR_INPUT) || \
                                                      ( (DIR) == BKP_PIN7DIR_OUTPUT) )
#define IS_BKP_TAMPER_PIN( PIN )                    ( ( (PIN) == BKP_TAMPER_PIN0) || ( (PIN) == BKP_TAMPER_PIN1) || ( (PIN) == BKP_TAMPER_PIN2) || \
                                                      ( (PIN) == BKP_TAMPER_PIN3) || ( (PIN) == BKP_TAMPER_PIN4) || ( (PIN) == BKP_TAMPER_PIN5) || \
                                                      ( (PIN) == BKP_TAMPER_PIN6) || ( (PIN) == BKP_TAMPER_PIN7) )
#define IS_BKP_TAMPER_PIN_HYSTER_SEL( SEL )         ( ( (SEL) == BKP_PINHYSTER_LEVEL0) || \
                                                      ( (SEL) == BKP_PINHYSTER_LEVEL1) )
#define IS_BKP_TAMPER_PIN_PASSFILTER_EN( EN )       ( ( (EN) == BKP_PINPASSFILTER_DISABLE) || \
                                                      ( (EN) == BKP_PINPASSFILTER_ENABLE) )
#define IS_BKP_TAMPER_PIN_DRISTRENGTH_EN( EN )      ( ( (EN) == BKP_PINDRISTRENGTH_LOW) || \
                                                      ( (EN) == BKP_PINDRISTRENGTH_HIGH) )
#define IS_BKP_TAMPER_PIN_SLEWRATE( RATE )          ( ( (RATE) == BKP_PINSLEW_SLOW) || \
                                                      ( (RATE) == BKP_PINSLEW_FAST) )
#define IS_BKP_TAMPER_LFSR_TAP( TAP )               ( ( (TAP) == BKP_ACTIVELFSR0_TAP0) || ( (TAP) == BKP_ACTIVELFSR0_TAP1) || ( (TAP) == BKP_ACTIVELFSR0_TAP2) || \
                                                      ( (TAP) == BKP_ACTIVELFSR0_TAP3) || ( (TAP) == BKP_ACTIVELFSR0_TAP4) || ( (TAP) == BKP_ACTIVELFSR0_TAP5) || \
                                                      ( (TAP) == BKP_ACTIVELFSR0_TAP6) || ( (TAP) == BKP_ACTIVELFSR0_TAP7) || ( (TAP) == BKP_ACTIVELFSR1_TAP0) || \
                                                      ( (TAP) == BKP_ACTIVELFSR1_TAP1) || ( (TAP) == BKP_ACTIVELFSR1_TAP2) || ( (TAP) == BKP_ACTIVELFSR1_TAP3) || \
                                                      ( (TAP) == BKP_ACTIVELFSR1_TAP4) || ( (TAP) == BKP_ACTIVELFSR1_TAP5) || ( (TAP) == BKP_ACTIVELFSR1_TAP6) || \
                                                      ( (TAP) == BKP_ACTIVELFSR1_TAP7) )
#define IS_BKP_TAMPER_PIN_EN( EN )                  ( ( (EN) == BKP_TAMPER_0) || ( (EN) == BKP_TAMPER_1) || ( (EN) == BKP_TAMPER_2) || \
                                                      ( (EN) == BKP_TAMPER_3) || ( (EN) == BKP_TAMPER_4) || ( (EN) == BKP_TAMPER_5) || \
                                                      ( (EN) == BKP_TAMPER_6) || ( (EN) == BKP_TAMPER_7) || ( (EN) == (0U) ) )
#define IS_BKP_GPIO_PIN_MODE( MODE )                ( ( (MODE) == BKP_GPIO_PIN_GENERIC) || ( (MODE) == BKP_GPIO_PIN_RTC_STATUS) || \
                                                      ( (MODE) == BKP_GPIO_RTC_CLK) || ( (MODE) == BKP_GPIO_DIG_SIG_OUT) )
#define IS_BKP_GPIO_PIN( PIN )                      ( ( (PIN) == BKP_GPIO_PIN0) || ( (PIN) == BKP_GPIO_PIN1) || ( (PIN) == BKP_GPIO_PIN2) || \
                                                      ( (PIN) == BKP_GPIO_PIN3) || ( (PIN) == BKP_GPIO_PIN4) || ( (PIN) == BKP_GPIO_PIN5) || \
                                                      ( (PIN) == BKP_GPIO_PIN6) || ( (PIN) == BKP_GPIO_PIN7) || ( (PIN) == BKP_GPIO_ALLPIN) )
#define IS_BKP_GPIO_DIR( DIR )                      ( ( (DIR) == BKP_GPIO_PIN_INPUT) || \
                                                      ( (DIR) == BKP_GPIO_PIN_OUTPUT) )
#define IS_BKP_TAMEPER_INT( INT )                   ( ( (INT) == BKP_INT_TEMP_TAMPER) || ( (INT) == BKP_INT_FREQ_TAMPER) || ( (INT) == BKP_INT_VOL_TAMPER) || \
                                                      ( (INT) == BKP_INT_TAMPER) || ( (INT) == BKP_INT_GLITCH_TAMPER) || ( (INT) == BKP_INT_TAMPER_0) || \
                                                      ( (INT) == BKP_INT_TAMPER_1) || ( (INT) == BKP_INT_TAMPER_2) || ( (INT) == BKP_INT_TAMPER_3) || \
                                                      ( (INT) == BKP_INT_TAMPER_4) || ( (INT) == BKP_INT_TAMPER_5) || ( (INT) == BKP_INT_TAMPER_6) || \
                                                      ( (INT) == BKP_INT_TAMPER_7) || ( (INT) == BKP_INT_SHIELD_TAMPER) || ( (INT) == BKP_INT_STFREQ_TAMPER) || \
                                                      ( (INT) == BKP_INT_STVOL_TAMPER) || ( (INT) == BKP_INT_STTEMP_TAMPER) || ( (INT) == BKP_INT_STGLITCH_TAMPER) || \
                                                      ( (INT) == BKP_INT_ALL) || ( (INT) == (0U) ) )
#define IS_BKP_TAMEPER_FLAG( FLAG )                 ( ( (FLAG) == BKP_FLAG_TAMPER) || ( (FLAG) == BKP_FLAG_TAMPER_ACK) || ( (FLAG) == BKP_FLAG_FREQ_TAMPER) || \
                                                      ( (FLAG) == BKP_FLAG_VOL_TAMPER) || ( (FLAG) == BKP_FLAG_TEMP_TAMPER) || ( (FLAG) == BKP_FLAG_GLITCH_TAMPER) || \
                                                      ( (FLAG) == BKP_FLAG_TAMPER_0) || ( (FLAG) == BKP_FLAG_TAMPER_1) || ( (FLAG) == BKP_FLAG_TAMPER_2) || \
                                                      ( (FLAG) == BKP_FLAG_TAMPER_3) || ( (FLAG) == BKP_FLAG_TAMPER_4) || ( (FLAG) == BKP_FLAG_TAMPER_5) || \
                                                      ( (FLAG) == BKP_FLAG_TAMPER_6) || ( (FLAG) == BKP_FLAG_TAMPER_7) || ( (FLAG) == BKP_FLAG_SHIELD_TAMPER) || \
                                                      ( (FLAG) == BKP_FLAG_STFREQ_TAMPER) || ( (FLAG) == BKP_FLAG_STVOL_TAMPER) || ( (FLAG) == BKP_FLAG_STTEMP_TAMPER) || \
                                                      ( (FLAG) == BKP_FLAG_STGLITCH_TAMPER) || ( (FLAG) == BKP_FLAG_RTC_STOP_FDET) || ( (FLAG) == BKP_FLAG_RTC_LOW_FDET) || \
                                                      ( (FLAG) == BKP_FLAG_RTC_HIGH_FDET) || ( (FLAG) == BKP_FLAG_RTC_LOW_VOL) || ( (FLAG) == BKP_FLAG_RTC_HIGH_VOL) || \
                                                      ( (FLAG) == BKP_FLAG_RTC_LOW_TEMP) || ( (FLAG) == BKP_FLAG_RTC_HIGH_TEMP) || ( (FLAG) == BKP_FLAG_All) )
#define IS_BKP_REGFILE_TYPE( TYPE )                 ( ( (TYPE) == BKP_REGFILE_NORMAL) || ( (TYPE) == BKP_REGFILE_UNDEFINE) || \
                                                      ( (TYPE) == BKP_REGFILE_SECURE) )
#define IS_BKP_VOL_DETECT_TIME( TIME )              ( ( ( (TIME) > 0x0U) && ( (TIME) <= 0xFU) ) )
#define IS_BKP_VOL_DETECT_TIME_INTER( INTER )       ( ( ( (INTER) > 0x0U) && ( (INTER) <= 0x7U) ) )
#define IS_BKP_LFSR_INIT_VAL( VAL )                 ( ( ( (VAL) > 0x0000U) && ( (VAL) <= 0xFFFFU) ) )
#define IS_BKP_RTC_STATUS_OUT( PIN )                ( (PIN) == BKP_GPIO_PIN0)
#define IS_BKP_RTC_CLK_OUT( PIN )                   ( (PIN) == BKP_GPIO_PIN1)
#define IS_BKP_DIGSIG_OUT( PIN )                    ( (PIN) == BKP_GPIO_PIN2)
#define IS_BKP_DIG_SIGNAL_TYPE( TYPE )              ( ( (TYPE) == BKP_DIG_SIGNAL_HT) || ( (TYPE) == BKP_DIG_SIGNAL_LV) || ( (TYPE) == BKP_DIG_SIGNAL_SF) || \
                                                      ( (TYPE) == BKP_DIG_SIGNAL_LT) || ( (TYPE) == BKP_DIG_SIGNAL_HF) || ( (TYPE) == BKP_DIG_SIGNAL_RFU) || \
                                                      ( (TYPE) == BKP_DIG_SIGNAL_HV) || ( (TYPE) == BKP_DIG_SIGNAL_LF) )
#define IS_PIN_INV_EN( EN )                         ( ( (EN) == BKP_PIN_INV_ENABLE) || \
                                                      ( (EN) == BKP_PIN_INV_DISABLE) )
#define IS_PIN_OD_EN( EN )                          ( ( (EN) == BKP_PIN_OD_ENABLE) || \
                                                      ( (EN) == BKP_PIN_OD_DISABLE) )
#define IS_WAKUP_OUTPUT_TYPE( TYPE )                ( ( (TYPE) == BKP_RTC_CYCLE_FLAG) || ( (TYPE) == BKP_RTC_1S_FLAG) || ( (TYPE) == BKP_RTC_ALARM_FLAG) || \
                                                      ( (TYPE) == BKP_RTC_OV_FLAG) || ( (TYPE) == BKP_RTC_INVALID_FLAG) || ( (TYPE) == BKP_RTC_CYCLE_RECORD) || \
                                                      ( (TYPE) == BKP_RTC_1S_RECORD) || ( (TYPE) == BKP_RTC_ALARM_RECORD) || ( (TYPE) == BKP_RTC_OV_RECORD) || \
                                                      ( (TYPE) == BKP_RTC_INVALID_RECORD) )
#define IS_BKP_PIN_CLKIN_EN( EN )                   ( ( (EN) == BKP_PIN_CLKIN_ENABLE) || \
                                                      ( (EN) == BKP_PIN_CLKIN_DISABLE) )
#define IS_BKP_SELF_DET_MODE( MODE )                ( ( (MODE) == BKP_SELF_DET_VOL) || ( (MODE) == BKP_SELF_DET_FREQ) || \
                                                      ( (MODE) == BKP_SELF_DET_GLITCH) )
#define IS_BKP_VDET_AUTO_EN( EN )                   ( ( (EN) == BKP_VOLDET_AUTO_ENABLE) || \
                                                      ( (EN) == BKP_VOLDET_AUTO_DISABLE) )
#define IS_BKP_BGTEMPER_TYPE( TYPE )                ( ( (TYPE) == BKP_BGTEMPER_ALWAYS) || \
                                                      ( (TYPE) == BKP_BGTEMPER_INTER) )
#define IS_VOL_LOW_TRIM( TRIM )                     ( ( ( (TRIM) > 0x0U) && ( (TRIM) <= 0x7U) ) )
#define IS_VOL_HIGH_TRIM( TRIM )                    ( ( ( (TRIM) > 0x0U) && ( (TRIM) <= 0x7U) ) )
#define IS_FREQ_LOW_TRIM( TRIM )                    ( ( ( (TRIM) > 0x0U) && ( (TRIM) <= 0x7U) ) )
#define IS_FREQ_HIGH_TRIM( TRIM )                   ( ( ( (TRIM) > 0x0U) && ( (TRIM) <= 0x7U) ) )
#define IS_TEMPER_LOW_TRIM( TRIM )                  ( ( ( (TRIM) > 0x00U) && ( (TRIM) <= 0x1FU) ) )
#define IS_TEMPER_HIGH_TRIM( TRIM )                 ( ( ( (TRIM) > 0x00U) && ( (TRIM) <= 0x1FU) ) )
#define IS_GLITCH_TRIM( TRIM )                      ( ( ( (TRIM) == 0x00U) || ( (TRIM) <= 0x1U) ) )
#define IS_TAMPER_LFSR_TYPE( TYPE )                 ( ( (TYPE) == BKP_TAMPER_ACTIVE_LFSR0) || ( (TYPE) == BKP_TAMPER_ACTIVE_LFSR1) || \
                                                      ( (TYPE) == BKP_TAMPER_ACTIVE_LFSR01) )
#define IS_BKP_SDET_HIGHFREQ_EN( EN )               ( ( (EN) == BKP_SDET_HIGHFREQ_DISABLE) || \
                                                      ( (EN) == BKP_SDET_HIGHFREQ_ENABLE) )


/**
 * @}
 */


/**
 * @}
 */


/**
 * @}
 */


/**
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif /* __FM15F3xx_HAL_BKP_H */

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
