/**
 ******************************************************************************
 * @file    fm15f3xx_ll_stimer.h
 * @author  SRG
 * @version V1.0.0
 * @date    2020-03-17
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */

#ifndef FM15F3XX_LL_STIMER_H
#define FM15F3XX_LL_STIMER_H
#ifdef __cplusplus
extern "C" {
#endif

#include "fm15f3xx.h"


typedef enum STn {
    /*!< ST_TIMER_NUM */
    ST0 = 0,
    ST1 = 1,
    ST2 = 2,
    ST3 = 3,
    ST4 = 4,
    ST5 = 5
} STn_Type;

typedef enum TGS_CHx {
    /*!< ST_TIMER_TGS_CH_NUM */
    TGS_CH0 = 0,
    TGS_CH1 = 1,
    TGS_CH2 = 2,
    TGS_CH3 = 3,
    TGS_CH4 = 4,
    TGS_CH5 = 5,
    TGS_CH6 = 6,
    TGS_CH7 = 7
} TGS_CHx_Type;


/** @addtogroup FM15F3XX_LL_Driver
 * @{
 */


/** @defgroup STIMER_LL STIMER
 * @{
 */

/* Private types -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/


/** @defgroup TIM_LL_Private_Variables TIM Private Variables
 * @{
 */


/**
 * @}
 */
/* Private constants ---------------------------------------------------------*/
/* Private macros ------------------------------------------------------------*/


/** @defgroup STIMER_LL_ES_INIT STIMER ExSTIMERed Init structures
 * @{
 */


/**
 * @brief LL STIMER Init Structure definition
 */


/**
 * @brief  TIM Time Base configuration structure definition.
 */
typedef struct {
    STn_Type Stx;               /*!< Specifies the number of st .
                                     This parameter can be a value between 0~5.*/

    uint16_t Prescaler;         /*!< Specifies the prescaler value used to divide the TIM clock.
                                     This parameter can be a value of @ref STIM_LL_EC_PSC.

                                     This feature can be modified afterwards using unitary function @ref LL_STIM_SetSt0Prescaler().*/

    uint32_t CounterMode;       /*!< Specifies the counter mode.
                                     This parameter can be a value of @ref STIM_LL_EC_MODE.

                                     This feature can be modified afterwards using unitary function @ref LL_TIM_SetCounterMode().*/

    uint32_t Autoreload;        /*!< Specifies the auto reload value to be loaded into the active
                                     Auto-Reload Register at the next update event.
                                     This parameter must be a number between Min_Data=0x0000 and Max_Data=0xFFFFFFFF.

                                     This feature can be modified afterwards using unitary function @ref LL_STIM_SetMod0Value().*/
} LL_TIM_InitTypeDef;


/**
 * @}
 */


/* ExSTIMERed constants --------------------------------------------------------*/


/** @defgroup STIMER_LL_ExSTIMERed_Constants STIMER ExSTIMERed Constants
 * @{
 */


/** @defgroup STIM_LL_EC_PSC Select simple timer prescaler
 * @{
 */
#define LL_TIM_PSC_DIV1     (0x0U)
#define LL_TIM_PSC_DIV2     (0x1U)
#define LL_TIM_PSC_DIV4     (0x2U)
#define LL_TIM_PSC_DIV8     (0x3U)
#define LL_TIM_PSC_DIV16    (0x4U)
#define LL_TIM_PSC_DIV32    (0x5U)
#define LL_TIM_PSC_DIV64    (0x6U)
#define LL_TIM_PSC_DIV128   (0x7U)
#define LL_TIM_PSC_DIV256   (0x8U)
#define LL_TIM_PSC_DIV512   (0x9U)


/**
 * @}
 */


/** @defgroup STIM_LL_EC_MODE Select counter mode
 * @{
 */
#define LL_TIM_MODE_MODCNT  (0x0U << ST_CFG_MODE_Pos)               /*!< MOD Count  */
#define LL_TIM_MODE_FREECNT (0x1U << ST_CFG_MODE_Pos)               /*!< Free Count  */
#define LL_TIM_MODE_PWM     (0x2U << ST_CFG_MODE_Pos)               /*!< PWM     */
#define LL_TIM_MODE_TRIGGER (0x3U << ST_CFG_MODE_Pos)               /*!< Trigger  */


/**
 * @}
 */


/** @defgroup STIM_LL_EC_TGS_PADINV Select polarity of trigger gather sysytem pad
 * @{
 */
#define LL_TIM_TGS_NOINVERT (0x0U << ST_TGSCH_PADINV_Pos)
#define LL_TIM_TGS_INVERT     (0x1U << ST_TGSCH_PADINV_Pos)


/**
 * @}
 */


/** @defgroup STIM_LL_EC_TGS_EDGE Select edge of trigger gather system input
 * @{
 */
#define LL_TIM_TGS_EDGE_HIGH        (0x0U << ST_TGSCH_PADEDGE_Pos)
#define LL_TIM_TGS_EDGE_RISING      (0x1U << ST_TGSCH_PADEDGE_Pos)
#define LL_TIM_TGS_EDGE_FALLING     (0x2U << ST_TGSCH_PADEDGE_Pos)
#define LL_TIM_TGS_EDGE_BOTHEDGE    (0x3U << ST_TGSCH_PADEDGE_Pos)


/**
 * @}
 */


/** @defgroup STIM_LL_EC_TGS_CLK Select CLK of trigger gather system input
 * @{
 */
#define LL_TIM_TGS_CLK_ST0  (0x0U << ST_TGSCH_INCLKSEL_Pos)
#define LL_TIM_TGS_CLK_ST1  (0x1U << ST_TGSCH_INCLKSEL_Pos)
#define LL_TIM_TGS_CLK_ST2  (0x2U << ST_TGSCH_INCLKSEL_Pos)
#define LL_TIM_TGS_CLK_ST3  (0x3U << ST_TGSCH_INCLKSEL_Pos)
#define LL_TIM_TGS_CLK_ST4  (0x4U << ST_TGSCH_INCLKSEL_Pos)
#define LL_TIM_TGS_CLK_ST5  (0x5U << ST_TGSCH_INCLKSEL_Pos)
#define LL_TIM_TGS_CLK_BUS  (0x6U << ST_TGSCH_INCLKSEL_Pos)
#define LL_TIM_TGS_CLK_SYS  (0x7U << ST_TGSCH_INCLKSEL_Pos)


/**
 * @}
 */


/** @defgroup STIM_LL_EC_TGS_Source Select CLK of trigger gather system input
 * @{
 */
#define LL_TIM_TGS_SRC_TGS_CH_SW                (0x00U << ST_TGSCH_INSOURCE_Pos)
#define LL_TIM_TGS_SRC_ST0                      (0x01U << ST_TGSCH_INSOURCE_Pos)
#define LL_TIM_TGS_SRC_ST1                      (0x02U << ST_TGSCH_INSOURCE_Pos)
#define LL_TIM_TGS_SRC_ST2                      (0x03U << ST_TGSCH_INSOURCE_Pos)
#define LL_TIM_TGS_SRC_ST3                      (0x04U << ST_TGSCH_INSOURCE_Pos)
#define LL_TIM_TGS_SRC_ST4                      (0x05U << ST_TGSCH_INSOURCE_Pos)
#define LL_TIM_TGS_SRC_ST5                      (0x06U << ST_TGSCH_INSOURCE_Pos)
#define LL_TIM_TGS_SRC_DMA_CH0                  (0x07U << ST_TGSCH_INSOURCE_Pos)
#define LL_TIM_TGS_SRC_DMA_CH1                  (0x08U << ST_TGSCH_INSOURCE_Pos)
#define LL_TIM_TGS_SRC_DMA_CH2                  (0x09U << ST_TGSCH_INSOURCE_Pos)
#define LL_TIM_TGS_SRC_DMA_CH3                  (0x0AU << ST_TGSCH_INSOURCE_Pos)
#define LL_TIM_TGS_SRC_CMP                      (0x0BU << ST_TGSCH_INSOURCE_Pos)
#define LL_TIM_TGS_SRC_DAC_FINISH               (0x0CU << ST_TGSCH_INSOURCE_Pos)
#define LL_TIM_TGS_SRC_ADC_CH0_FINISH           (0x0DU << ST_TGSCH_INSOURCE_Pos)
#define LL_TIM_TGS_SRC_UART0_RXFIFO_NONEMPTY    (0x0EU << ST_TGSCH_INSOURCE_Pos)
#define LL_TIM_TGS_SRC_UART0_TXFIFO_EMPTY       (0x0FU << ST_TGSCH_INSOURCE_Pos)
#define LL_TIM_TGS_SRC_UART1_RXFIFO_NONEMPTY    (0x10U << ST_TGSCH_INSOURCE_Pos)
#define LL_TIM_TGS_SRC_UART1_TXFIFO_EMPTY       (0x11U << ST_TGSCH_INSOURCE_Pos)
#define LL_TIM_TGS_SRC_I2C0_RXBUF_NONEMPTY      (0x12U << ST_TGSCH_INSOURCE_Pos)
#define LL_TIM_TGS_SRC_I2C0_TXBUF_EMPTY         (0x13U << ST_TGSCH_INSOURCE_Pos)
#define LL_TIM_TGS_SRC_SPI0_RXFIFO_NONEMPTY     (0x14U << ST_TGSCH_INSOURCE_Pos)
#define LL_TIM_TGS_SRC_SPI0_TXFIFO_EMPTY        (0x15U << ST_TGSCH_INSOURCE_Pos)
#define LL_TIM_TGS_SRC_SPI1_RXFIFO_NONEMPTY     (0x16U << ST_TGSCH_INSOURCE_Pos)
#define LL_TIM_TGS_SRC_SPI1_TXFIFO_EMPTY        (0x17U << ST_TGSCH_INSOURCE_Pos)
#define LL_TIM_TGS_SRC_CT0_RXFIFO_NONEMPTY      (0x18U << ST_TGSCH_INSOURCE_Pos)
#define LL_TIM_TGS_SRC_CT0_TXFIFO_EMPTY         (0x19U << ST_TGSCH_INSOURCE_Pos)
#define LL_TIM_TGS_SRC_TGS_PAD0                 (0x1AU << ST_TGSCH_INSOURCE_Pos)
#define LL_TIM_TGS_SRC_TGS_PAD1                 (0x1BU << ST_TGSCH_INSOURCE_Pos)
#define LL_TIM_TGS_SRC_TGS_PAD2                 (0x1CU << ST_TGSCH_INSOURCE_Pos)
#define LL_TIM_TGS_SRC_TGS_PAD3                 (0x1DU << ST_TGSCH_INSOURCE_Pos)
#define LL_TIM_TGS_SRC_TGS_PAD4                 (0x1EU << ST_TGSCH_INSOURCE_Pos)
#define LL_TIM_TGS_SRC_TGS_PAD5                 (0x1FU << ST_TGSCH_INSOURCE_Pos)


/**
 * @}
 */


/** @defgroup STIM_LL_EC_TR_OUTINV Select polarity of trigger output
 * @{
 */
#define LL_TIM_TR_OUT_NOINVERT  (0x0U << ST_CFG_OUTINV_Pos)
#define LL_TIM_TR_OUT_INVERT    (0x1U << ST_CFG_OUTINV_Pos)


/**
 * @}
 */


/** @defgroup STIM_LL_EC_TR_OUTMODE Select out mode of trigger
 * @{
 */
#define LL_TIM_TR_OUT_HOLD  (0x0U << ST_CFG_OUTPULSE_Pos)
#define LL_TIM_TR_OUT_PULSE (0x1U << ST_CFG_OUTPULSE_Pos)
//#define LL_TIM_TR_OUT_COUNTERHOLD      (0x2U << ST_STCFG_OUTMODE_Pos)


/**
 * @}
 */
///** @defgroup STIM_LL_EC_TR_IN Select polarity of trigger input
//  * @{
//  */
//#define LL_TIM_TR_IN_NONINVERTED       (0x0U << ST_TRINCFG_INV_Pos)
//#define LL_TIM_TR_IN_INVERTED          (0x1U << ST_TRINCFG_INV_Pos)
///**
//  * @}
//  */

///** @defgroup STIM_LL_EC_TRIN_EDGE Select edge of trigger input
//  * @{
//  */
//#define LL_TIM_TRIN_EDGE_HIGH          (0x0U << AT_TRCFG_EDGE_Pos)
//#define LL_TIM_TRIN_EDGE_RISING        (0x1U << AT_TRCFG_EDGE_Pos)
//#define LL_TIM_TRIN_EDGE_FALLING       (0x2U << AT_TRCFG_EDGE_Pos)
//#define LL_TIM_TRIN_EDGE_BOTHEDGE      (0x3U << AT_TRCFG_EDGE_Pos)
///**
//  * @}
//  */


/** @defgroup STIM_LL_EC_TRIN_ACTION Select action of trigger input
 * @{
 */
#define LL_TIM_TRIN_ACTION_STOP     (0x0U << ST_TRINCFG_ACTION_Pos)
#define LL_TIM_TRIN_ACTION_START    (0x1U << ST_TRINCFG_ACTION_Pos)
#define LL_TIM_TRIN_ACTION_PAUSE    (0x2U << ST_TRINCFG_ACTION_Pos)
#define LL_TIM_TRIN_ACTION_COUNT    (0x3U << ST_TRINCFG_ACTION_Pos)


/**
 * @}
 */


/** @defgroup STIM_LL_EC_TRIN_TGS ch Select
 * @{
 */
#define LL_TIM_TRIN_TGS_CH0 (0x0U << ST_TRINCFG_TGSCH_Pos)
#define LL_TIM_TRIN_TGS_CH1 (0x1U << ST_TRINCFG_TGSCH_Pos)
#define LL_TIM_TRIN_TGS_CH2 (0x2U << ST_TRINCFG_TGSCH_Pos)
#define LL_TIM_TRIN_TGS_CH3 (0x3U << ST_TRINCFG_TGSCH_Pos)
#define LL_TIM_TRIN_TGS_CH4 (0x4U << ST_TRINCFG_TGSCH_Pos)
#define LL_TIM_TRIN_TGS_CH5 (0x5U << ST_TRINCFG_TGSCH_Pos)
#define LL_TIM_TRIN_TGS_CH6 (0x6U << ST_TRINCFG_TGSCH_Pos)
#define LL_TIM_TRIN_TGS_CH7 (0x7U << ST_TRINCFG_TGSCH_Pos)


/**
 * @}
 */


/** @defgroup STIM_LL_EC_FAULT_TGS ch Select
 * @{
 */
#define LL_TIM_FAULT_TGS_CH0    (0x0U << ST_FAULTCFG_TGSCH_Pos)
#define LL_TIM_FAULT_TGS_CH1    (0x1U << ST_FAULTCFG_TGSCH_Pos)
#define LL_TIM_FAULT_TGS_CH2    (0x2U << ST_FAULTCFG_TGSCH_Pos)
#define LL_TIM_FAULT_TGS_CH3    (0x3U << ST_FAULTCFG_TGSCH_Pos)
#define LL_TIM_FAULT_TGS_CH4    (0x4U << ST_FAULTCFG_TGSCH_Pos)
#define LL_TIM_FAULT_TGS_CH5    (0x5U << ST_FAULTCFG_TGSCH_Pos)
#define LL_TIM_FAULT_TGS_CH6    (0x6U << ST_FAULTCFG_TGSCH_Pos)
#define LL_TIM_FAULT_TGS_CH7    (0x7U << ST_FAULTCFG_TGSCH_Pos)


/**
 * @}
 */


/** @defgroup STIM_LL_EC_FAULT_RESET Fault Reset config
 * @{
 */
#define LL_TIM_FAULT_RESET_EN   (0x0U << ST_FAULTCFG_RESET_Pos)
#define LL_TIM_FAULT_RESET_ALL  (0x1U << ST_FAULTCFG_RESET_Pos)


/**
 * @}
 */

/* ExSTIMERed macro ------------------------------------------------------------*/


/** @defgroup STIMER_LL_ExSTIMERed_Macros STIMER ExSTIMERed Macros
 * @{
 */


/**
 * @brief  HELPER macro calculating the prescaler value to achieve the required counter clock frequency.
 * @note ex: @ref __LL_TIM_CALC_PSC (80000000, 1000000);
 * @param  __TIMCLK__ timer input clock frequency (in Hz)
 * @param  __CNTCLK__ counter clock frequency (in Hz)
 * @retval Prescaler value  (between Min_Data=0 and Max_Data=65535)
 */
#define __LL_STIM_CALC_PSC( __STIMCLK__, __CNTCLK__ )   \
    ( (__STIMCLK__) >= (__CNTCLK__) ) ? (uint32_t) ( (__STIMCLK__) / (__CNTCLK__) -1U) : 0U


/** @defgroup STIMER_LL_EM_WRITE_READ Common Write and read registers Macros
 * @{
 */


/**
 * @brief  Write a value in STIMER register
 * @param  __INSTANCE__ STIMER Instance
 * @param  __REG__ Register to be written
 * @param  __VALUE__ Value to be written in the register
 * @retval None
 */
#define LL_STIMER_WriteReg( __INSTANCE__, __REG__, __VALUE__ ) WRITE_REG( __INSTANCE__->__REG__, (__VALUE__) )


/**
 * @brief  Read a value in STIMER register
 * @param  __INSTANCE__ STIMER Instance
 * @param  __REG__ Register to be read
 * @retval Register value
 */
#define LL_STIMER_ReadReg( __INSTANCE__, __REG__ ) READ_REG( __INSTANCE__->__REG__ )


/**
 * @}
 */

/* ExSTIMERed functions --------------------------------------------------------*/


/** @defgroup STIMER_LL_ExSTIMERed_Functions STIMER ExSTIMERed Functions
 * @{
 */


/** @defgroup STIMER_LL_EF_STIMER_Configuration STIMER Configuration
 * @{
 */


/**
 * @brief  Enable simple timer load.
 * @rmtoll LOAD                     LL_STIM_CVALUpdate
 * @param  STn_Type This parameter can be one of the following values:
 *         @arg @ref ST0
 *         @arg @ref ST1
 *         @arg @ref ST2
 *         @arg @ref ST3
 *         @arg @ref ST4
 *         @arg @ref ST5
 * @retval None
 */
__STATIC_INLINE void LL_STIM_CaptureCurrentValue(STn_Type STn)
{
    SET_BIT(STIMER->LOAD, ST_LOAD_STLOAD(STn));
}


/**
 * @brief  Enable simple timer.
 * @rmtoll CTRL          st_en           LL_STIM_Enable
 * @param  STn_Type This parameter can be one of the following values:
 *         @arg @ref ST0
 *         @arg @ref ST1
 *         @arg @ref ST2
 *         @arg @ref ST3
 *         @arg @ref ST4
 *         @arg @ref ST5
 * @retval None
 */
__STATIC_INLINE void LL_STIM_Enable(STn_Type STn)
{
    SET_BIT(STIMER->CTRL, ST_CTRL_STEN(STn));
}


/**
 * @brief  Disable simple timer.
 * @rmtoll CTRL          st_en           LL_STIM_Disable
 * @param  STn_Type This parameter can be one of the following values:
 *         @arg @ref ST0
 *         @arg @ref ST1
 *         @arg @ref ST2
 *         @arg @ref ST3
 *         @arg @ref ST4
 *         @arg @ref ST5
 * @retval None
 */
__STATIC_INLINE void LL_STIM_Disable(STn_Type STn)
{
    CLEAR_BIT(STIMER->CTRL, ST_CTRL_STEN(STn));
}


/**
 * @brief  Reset simple timer.
 * @note The hardware automatically returns this bit back to 0
 * @rmtoll RESET          st_reset           LL_STIM_Reset
 * @param  STn_Type This parameter can be one of the following values:
 *         @arg @ref ST0
 *         @arg @ref ST1
 *         @arg @ref ST2
 *         @arg @ref ST3
 *         @arg @ref ST4
 *         @arg @ref ST5
 * @retval None
 */
__STATIC_INLINE void LL_STIM_Reset(STn_Type STn)
{
    SET_BIT(STIMER->RESET, ST_RESET_STRESET(STn));
}


/**
 * @brief  Enable the prescaler of st.
 * @rmtoll CTRL          st_en           LL_STIM_EnablePrescaler
 * @param  STn_Type This parameter can be one of the following values:
 *         @arg @ref ST0
 *         @arg @ref ST1
 *         @arg @ref ST2
 *         @arg @ref ST3
 *         @arg @ref ST4
 *         @arg @ref ST5
 * @retval None
 */
__STATIC_INLINE void LL_STIM_EnablePrescaler(STn_Type STn)
{
    SET_BIT(STIMER->PSC[STn], ST_PSC_STPSCEN);
}


/**
 * @brief  Disable the prescaler of st.
 * @rmtoll STPSC          STPSCEN           LL_STIM_DisablePrescaler
 * @param  STn_Type This parameter can be one of the following values:
 *         @arg @ref ST0
 *         @arg @ref ST1
 *         @arg @ref ST2
 *         @arg @ref ST3
 *         @arg @ref ST4
 *         @arg @ref ST5
 * @retval None
 */
__STATIC_INLINE void LL_STIM_DisablePrescaler(STn_Type STn)
{
    CLEAR_BIT(STIMER->PSC[STn], ST_PSC_STPSCEN);    //Disable prescaler
}


/**
 * @brief  Set the prescaler value of st0.
 * @note The counter clock frequency CK_CNT is equal to fCK_PSC / (STPSC[3:0] + 1).
 * @note The prescaler can be changed on the fly as this control register is buffered. The new
 *       prescaler ratio is taken into account at the next update event.
 * @rmtoll STPSC          ST0PSCEN|ST0PSCCFG           LL_STIM_SetPrescaler
 * @param  STn_Type This parameter can be one of the following values:
 *         @arg @ref ST0
 *         @arg @ref ST1
 *         @arg @ref ST2
 *         @arg @ref ST3
 *         @arg @ref ST4
 *         @arg @ref ST5
 * @param  Prescaler This parameter can be one of the following values:
 *         @arg @ref LL_TIM_PSC_DIV1
 *         @arg @ref LL_TIM_PSC_DIV2
 *         @arg @ref LL_TIM_PSC_DIV4
 *         @arg @ref LL_TIM_PSC_DIV8
 *         @arg @ref LL_TIM_PSC_DIV16
 *         @arg @ref LL_TIM_PSC_DIV32
 *         @arg @ref LL_TIM_PSC_DIV64
 *         @arg @ref LL_TIM_PSC_DIV128
 *         @arg @ref LL_TIM_PSC_DIV256
 *         @arg @ref LL_TIM_PSC_DIV512
 * @retval None
 */
__STATIC_INLINE void LL_STIM_SetPrescaler(STn_Type STn, uint32_t Prescaler)
{
    SET_BIT(STIMER->PSC[STn], ST_PSC_STPSCEN);      //Enable prescaler
    MODIFY_REG(STIMER->PSC[STn], ST_PSC_STPSCCFG, Prescaler << ST_PSC_STPSCCFG_Pos);
}


/**
 * @brief  Set the counter mode of simple timer
 * @rmtoll STCFG         MODE            LL_STIM_SetRunMode
 * @param  STn_Type This parameter can be one of the following values:
 *         @arg @ref ST0
 *         @arg @ref ST1
 *         @arg @ref ST2
 *         @arg @ref ST3
 *         @arg @ref ST4
 *         @arg @ref ST5
 * @param  Mode This parameter can be one of the following values:
 *         @arg @ref LL_TIM_MODE_MODCNT
 *         @arg @ref LL_TIM_MODE_FREECNT
 *         @arg @ref LL_TIM_MODE_PWM
 *         @arg @ref LL_TIM_MODE_TRIGGER
 * @retval None
 */
__STATIC_INLINE void LL_STIM_SetRunMode(STn_Type STn, uint32_t Mode)
{
    MODIFY_REG(STIMER->CFG[STn], ST_CFG_MODE, Mode);
}


/**
 * @brief  Enable one-shot mode
 * @rmtoll STCFG          OSM           LL_STIM_EnableOneshot
 * @param  STn_Type This parameter can be one of the following values:
 *         @arg @ref ST0
 *         @arg @ref ST1
 *         @arg @ref ST2
 *         @arg @ref ST3
 *         @arg @ref ST4
 *         @arg @ref ST5
 * @retval None
 */
__STATIC_INLINE void LL_STIM_EnableOneshot(STn_Type STn)
{
    SET_BIT(STIMER->CFG[STn], ST_CFG_OSM);
}


/**
 * @brief  Disable one-shot mode,Enable continue mode
 * @rmtoll STCFG          OSM           LL_STIM_DisableOneshot
 * @param  STn_Type This parameter can be one of the following values:
 *         @arg @ref ST0
 *         @arg @ref ST1
 *         @arg @ref ST2
 *         @arg @ref ST3
 *         @arg @ref ST4
 *         @arg @ref ST5
 * @retval None
 */
__STATIC_INLINE void LL_STIM_DisableOneshot(STn_Type STn)
{
    CLEAR_BIT(STIMER->CFG[STn], ST_CFG_OSM);
}


/**
 * @brief  Stop counting when MCU enter STOP mode.
 * @rmtoll STCFG          STOPFZ           LL_STIM_StopCountInStop
 * @param  STn_Type This parameter can be one of the following values:
 *         @arg @ref ST0
 *         @arg @ref ST1
 *         @arg @ref ST2
 *         @arg @ref ST3
 *         @arg @ref ST4
 *         @arg @ref ST5
 * @retval None
 */
__STATIC_INLINE void LL_STIM_StopCountInStop(STn_Type STn)
{
    SET_BIT(STIMER->CFG[STn], ST_CFG_STOPFZ);
}


/**
 * @brief  Continue counting when MCU enter STOP mode.
 * @rmtoll STCFG          STOPFZ           LL_STIM_ContinueCountInStop
 * @param  STn_Type This parameter can be one of the following values:
 *         @arg @ref ST0
 *         @arg @ref ST1
 *         @arg @ref ST2
 *         @arg @ref ST3
 *         @arg @ref ST4
 *         @arg @ref ST5
 * @retval None
 */
__STATIC_INLINE void LL_STIM_ContinueCountInStop(STn_Type STn)
{
    CLEAR_BIT(STIMER->CFG[STn], ST_CFG_STOPFZ);
}


/**
 * @brief  Select the  trigger polarity of simple timer.
 * @rmtoll STCFG         OUTINV           LL_STIM_SetTrigInvert
 * @param  STn_Type This parameter can be one of the following values:
 *         @arg @ref ST0
 *         @arg @ref ST1
 *         @arg @ref ST2
 *         @arg @ref ST3
 *         @arg @ref ST4
 *         @arg @ref ST5
 * @param  outinv This parameter can be one of the following values:
 *         @arg @ref LL_TIM_TR_OUT_NONINVERT
 *         @arg @ref LL_TIM_TR_OUT_INVERT
 * @retval None
 */
__STATIC_INLINE void LL_STIM_SetTrigInv(STn_Type STn, uint32_t outinv)
{
    MODIFY_REG(STIMER->CFG[STn], ST_CFG_OUTINV, outinv);
}


/**
 * @brief  Configure the  out mode of trigger.
 * @rmtoll STCFG         OUTMODE           LL_STIM_SetTrigMode
 * @param  STn_Type This parameter can be one of the following values:
 *         @arg @ref ST0
 *         @arg @ref ST1
 *         @arg @ref ST2
 *         @arg @ref ST3
 *         @arg @ref ST4
 *         @arg @ref ST5
 * @param  outmode This parameter can be one of the following values:
 *         @arg @ref LL_TIM_TR_OUT_HOLD
 *         @arg @ref LL_TIM_TR_OUT_PULSE
 * @retval None
 */
__STATIC_INLINE void LL_STIM_SetTrigOutMode(STn_Type STn, uint32_t outpulse)
{
    MODIFY_REG(STIMER->CFG[STn], ST_CFG_OUTPULSE, outpulse);
}


/**
 * @brief  Enable simple timer trigger gather system input.
 * @rmtoll TGSCH          LL_STIM_EnableTGSIN
 * @param  TGS_CHx_Type This parameter can be one of the following values:
 *         @arg @ref TGS_CH0
 *         @arg @ref TGS_CH1
 *         @arg @ref TGS_CH2
 *         @arg @ref TGS_CH3
 *         @arg @ref TGS_CH4
 *         @arg @ref TGS_CH5
 *         @arg @ref TGS_CH6
 *         @arg @ref TGS_CH7
 * @retval None
 */
__STATIC_INLINE void LL_STIM_EnableTGS(TGS_CHx_Type CHx)
{
    SET_BIT(STIMER->TGSCH[CHx], ST_TGSCH_INEN);
}


/**
 * @brief  Disable simple timer trigger gather system input.
 * @rmtoll TGSCH           LL_STIM_DisableTGSIN
 * @param  TGS_CHx_Type This parameter can be one of the following values:
 *         @arg @ref TGS_CH0
 *         @arg @ref TGS_CH1
 *         @arg @ref TGS_CH2
 *         @arg @ref TGS_CH3
 *         @arg @ref TGS_CH4
 *         @arg @ref TGS_CH5
 *         @arg @ref TGS_CH6
 *         @arg @ref TGS_CH7
 * @retval None
 */
__STATIC_INLINE void LL_STIM_DisableTGS(TGS_CHx_Type CHx)
{
    CLEAR_BIT(STIMER->TGSCH[CHx], ST_TGSCH_INEN);
}


/**
 * @brief  set invert of simple timer input trigger gather system pad.
 * @rmtoll TGSCH          LL_STIM_SetTGSInv
 * @param  TGS_CHx_Type This parameter can be one of the following values:
 *         @arg @ref TGS_CH0
 *         @arg @ref TGS_CH1
 *         @arg @ref TGS_CH2
 *         @arg @ref TGS_CH3
 *         @arg @ref TGS_CH4
 *         @arg @ref TGS_CH5
 *         @arg @ref TGS_CH6
 *         @arg @ref TGS_CH7
 * @param  Configuration This parameter can be one of the following values:
 *         @arg @ref LL_TIM_TGS_INVERT
 *         @arg @ref LL_TIM_TGS_NOINVERT
 * @retval None
 */
__STATIC_INLINE void LL_STIM_SetTGSInv(TGS_CHx_Type CHx, uint32_t cfg)
{
    MODIFY_REG(STIMER->TGSCH[CHx], ST_TGSCH_PADINV, cfg);
}


/**
 * @brief  set edge of simple timer input trigger gather system pad.
 * @rmtoll TGSCH          LL_STIM_SetTGSTrigEdge
 * @param  TGS_CHx_Type This parameter can be one of the following values:
  *         @arg @ref TGS_CH0
 *         @arg @ref TGS_CH1
 *         @arg @ref TGS_CH2
 *         @arg @ref TGS_CH3
 *         @arg @ref TGS_CH4
 *         @arg @ref TGS_CH5
 *         @arg @ref TGS_CH6
 *         @arg @ref TGS_CH7
 * @param  Configuration This parameter can be one of the following values:
 *         @arg @ref LL_TIM_TGS_EDGE_HIGH
 *         @arg @ref LL_TIM_TGS_EDGE_RISING
 *         @arg @ref LL_TIM_TGS_EDGE_FALLING
 *         @arg @ref LL_TIM_TGS_EDGE_BOTHEDG
 * @retval None
 */
__STATIC_INLINE void LL_STIM_SetTGSTrigEdge(TGS_CHx_Type CHx, uint32_t cfg)
{
    MODIFY_REG(STIMER->TGSCH[CHx], ST_TGSCH_PADEDGE, cfg);
}


/**
 * @brief  set filter cycle of simple timer input trigger gather system pad.
 * @rmtoll TGSCH          LL_STIM_SetTGSFilterCnt
 * @param  TGS_CHx_Type This parameter can be one of the following values:
 *         @arg @ref TGS_CH0
 *         @arg @ref TGS_CH1
 *         @arg @ref TGS_CH2
 *         @arg @ref TGS_CH3
 *         @arg @ref TGS_CH4
 *         @arg @ref TGS_CH5
 *         @arg @ref TGS_CH6
 *         @arg @ref TGS_CH7
 * @param  Configuration cycle(0~15)

 * @retval None
 */
__STATIC_INLINE void LL_STIM_SetTGSFilterCnt(TGS_CHx_Type CHx, uint32_t Cycle)
{
    MODIFY_REG(STIMER->TGSCH[CHx], ST_TGSCH_PADFILT, Cycle);
}


/**
 * @brief  set Clock of simple timer input trigger gather system pad.
 * @rmtoll TGSCH          LL_STIM_SetTGSClkSrc
 * @param  TGS_CHx_Type This parameter can be one of the following values:
 *         @arg @ref TGS_CH0
 *         @arg @ref TGS_CH1
 *         @arg @ref TGS_CH2
 *         @arg @ref TGS_CH3
 *         @arg @ref TGS_CH4
 *         @arg @ref TGS_CH5
 *         @arg @ref TGS_CH6
 *         @arg @ref TGS_CH7
 * @param  Configuration This parameter can be one of the following values:
 *         @arg @ref LL_TIM_TGS_CLK_ST0
 *         @arg @ref LL_TIM_TGS_CLK_ST1
 *         @arg @ref LL_TIM_TGS_CLK_ST2
 *         @arg @ref LL_TIM_TGS_CLK_ST3
 *         @arg @ref LL_TIM_TGS_CLK_ST4
 *         @arg @ref LL_TIM_TGS_CLK_ST5
 *         @arg @ref LL_TIM_TGS_CLK_BUS
 *         @arg @ref LL_TIM_TGS_CLK_SYS

 * @retval None
 */
__STATIC_INLINE void LL_STIM_SetTGSClkSrc(TGS_CHx_Type CHx, uint32_t Clk)
{
    MODIFY_REG(STIMER->TGSCH[CHx], ST_TGSCH_INCLKSEL, Clk);
}


/**
 * @brief  set trigger source of simple timer input trigger gather system pad.
 * @rmtoll TGSCH          LL_STIM_SetTGSTrigSrc
 * @param  TGS_CHx_Type This parameter can be one of the following values:
 *         @arg @ref TGS_CH0
 *         @arg @ref TGS_CH1
 *         @arg @ref TGS_CH2
 *         @arg @ref TGS_CH3
 *         @arg @ref TGS_CH4
 *         @arg @ref TGS_CH5
 *         @arg @ref TGS_CH6
 *         @arg @ref TGS_CH7
 * @param  Configuration This parameter can be one of the following values:
 *         @arg @ref LL_TIM_TGS_SRC_TGS_CH_SW
 *         @arg @ref LL_TIM_TGS_SRC_ST0
 *         @arg @ref LL_TIM_TGS_SRC_ST1
 *         @arg @ref LL_TIM_TGS_SRC_ST2
 *         @arg @ref LL_TIM_TGS_SRC_ST3
 *         @arg @ref LL_TIM_TGS_SRC_ST4
 *         @arg @ref LL_TIM_TGS_SRC_ST5
 *         @arg @ref LL_TIM_TGS_SRC_DMA_CH0
 *         @arg @ref LL_TIM_TGS_SRC_DMA_CH1
 *         @arg @ref LL_TIM_TGS_SRC_DMA_CH2
 *         @arg @ref LL_TIM_TGS_SRC_DMA_CH3
 *         @arg @ref LL_TIM_TGS_SRC_CMP
 *         @arg @ref LL_TIM_TGS_SRC_DAC_FINISH
 *         @arg @ref LL_TIM_TGS_SRC_ADC_CH0_FINISH
 *         @arg @ref LL_TIM_TGS_SRC_UART0_RXFIFO_EMPTY
 *         @arg @ref LL_TIM_TGS_SRC_UART0_TXFIFO_EMPTY
 *         @arg @ref LL_TIM_TGS_SRC_UART1_RXFIFO_EMPTY
 *         @arg @ref LL_TIM_TGS_SRC_UART1_TXFIFO_EMPTY
 *         @arg @ref LL_TIM_TGS_SRC_FI2C0_RXBUF_EMPTY
 *         @arg @ref LL_TIM_TGS_SRC_I2C0_TXBUF_EMPTY
 *         @arg @ref LL_TIM_TGS_SRC_FSPI0_RXFIFO_EMPTY
 *         @arg @ref LL_TIM_TGS_SRC_SPI0_TXFIFO_EMPTY
 *         @arg @ref LL_TIM_TGS_SRC_FSPI1_RXFIFO_EMPTY
 *         @arg @ref LL_TIM_TGS_SRC_SPI1_TXFIFO_EMPTY
 *         @arg @ref LL_TIM_TGS_SRC_FCT0_RXFIFO_EMPTY
 *         @arg @ref LL_TIM_TGS_SRC_CT0_TXFIFO_EMPTY
 *         @arg @ref LL_TIM_TGS_SRC_TGS_PAD0
 *         @arg @ref LL_TIM_TGS_SRC_TGS_PAD1
 *         @arg @ref LL_TIM_TGS_SRC_TGS_PAD2
 *         @arg @ref LL_TIM_TGS_SRC_TGS_PAD3
 *         @arg @ref LL_TIM_TGS_SRC_TGS_PAD4
 *         @arg @ref LL_TIM_TGS_SRC_TGS_PAD5
 * @retval None
 */
__STATIC_INLINE void LL_STIM_SetTGSInSrc(TGS_CHx_Type CHx, uint32_t Src)
{
    MODIFY_REG(STIMER->TGSCH[CHx], ST_TGSCH_INSOURCE, Src);
}


/**
 * @brief  set trigger gather system soft trigger.
 * @rmtoll TGSINCFG          LL_STIM_SoftTrigTGS
 * @param  TGS_CHx_Type This parameter can be one of the following values:
 *         @arg @ref TGS_CH0
 *         @arg @ref TGS_CH1
 *         @arg @ref TGS_CH2
 *         @arg @ref TGS_CH3
 *         @arg @ref TGS_CH4
 *         @arg @ref TGS_CH5
 *         @arg @ref TGS_CH6
 *         @arg @ref TGS_CH7
 * @retval None
 */
__STATIC_INLINE void LL_STIM_SoftTrigTGS(TGS_CHx_Type CHx)
{
    SET_BIT(STIMER->TGSSWTRIG, ST_TGSSWTRIG_CH0 << (4 * CHx));
}


/**
 * @brief  Enable simple timer trigger input.
 * @rmtoll TRINCFG          EN           LL_STIM_EnableTrigIn
 * @param  STn_Type This parameter can be one of the following values:
 *         @arg @ref ST0
 *         @arg @ref ST1
 *         @arg @ref ST2
 *         @arg @ref ST3
 *         @arg @ref ST4
 *         @arg @ref ST5
 * @retval None
 */
__STATIC_INLINE void LL_STIM_EnableTrigIn(STn_Type STn)
{
    SET_BIT(STIMER->TRINCFG[STn], ST_TRINCFG_EN);
}


/**
 * @brief  Disable simple timer trigger input.
 * @rmtoll TRINCFG          EN           LL_STIM_DisableTrigIn
 * @param  STn_Type This parameter can be one of the following values:
 *         @arg @ref ST0
 *         @arg @ref ST1
 *         @arg @ref ST2
 *         @arg @ref ST3
 *         @arg @ref ST4
 *         @arg @ref ST5
 * @retval None
 */
__STATIC_INLINE void LL_STIM_DisableTrigIn(STn_Type STn)
{
    CLEAR_BIT(STIMER->TRINCFG[STn], ST_TRINCFG_EN);
}


/**
 * @brief  Configure the  trigger  input.
 * @param  STn_Type This parameter can be one of the following values:
 *         @arg @ref ST0
 *         @arg @ref ST1
 *         @arg @ref ST2
 *         @arg @ref ST3
 *         @arg @ref ST4
 *         @arg @ref ST5
 * @param  Configuration This parameter must be a combination of all the following values:
 *         st_trin_tgs_ch This parameter can be one of the following values:
 *         @arg @ref LL_TIM_TRIN_TGS_CH0
 *         @arg @ref LL_TIM_TRIN_TGS_CH1
 *         @arg @ref LL_TIM_TRIN_TGS_CH2
 *         @arg @ref LL_TIM_TRIN_TGS_CH3
 *         @arg @ref LL_TIM_TRIN_TGS_CH4
 *         @arg @ref LL_TIM_TRIN_TGS_CH5
 *         @arg @ref LL_TIM_TRIN_TGS_CH6
 *         @arg @ref LL_TIM_TRIN_TGS_CH7
 *         st_trin_action This parameter can be one of the following values:
 *         @arg @ref LL_TIM_TRIN_ACTION_STOP
 *         @arg @ref LL_TIM_TRIN_ACTION_START
 *         @arg @ref LL_TIM_TRIN_ACTION_PAUSE
 *         @arg @ref LL_TIM_TRIN_ACTION_COUNT
 * @retval None
 */
__STATIC_INLINE void LL_STIM_ConfigTrigIn(STn_Type STn, uint32_t ch, uint32_t action)
{
    MODIFY_REG(STIMER->TRINCFG[STn], ST_TRINCFG_TGSCH | ST_TRINCFG_ACTION, ch | action);
}


/**
 * @brief  Enable simple timer fault stop.
 * @rmtoll FAULTCFG          EN           LL_STIM_EnableFault
 * @param  STn_Type This parameter can be one of the following values:
 *         @arg @ref ST0
 *         @arg @ref ST1
 *         @arg @ref ST2
 *         @arg @ref ST3
 *         @arg @ref ST4
 *         @arg @ref ST5
 * @retval None
 */
__STATIC_INLINE void LL_STIM_EnableFault(STn_Type STn)
{
    SET_BIT(STIMER->FAULTCFG[STn], ST_FAULTCFG_EN);
}


/**
 * @brief  Disable simple timer fault stop.
 * @rmtoll FAULTCFG          EN           LL_STIM_DisableFault
 * @param  STn_Type This parameter can be one of the following values:
 *         @arg @ref ST0
 *         @arg @ref ST1
 *         @arg @ref ST2
 *         @arg @ref ST3
 *         @arg @ref ST4
 *         @arg @ref ST5
 * @retval None
 */
__STATIC_INLINE void LL_STIM_DisableFault(STn_Type STn)
{
    CLEAR_BIT(STIMER->FAULTCFG[STn], ST_FAULTCFG_EN);
}


/**
 * @brief  Configure the fault stop channel.
 * @param  STn_Type This parameter can be one of the following values:
 *         @arg @ref ST0
 *         @arg @ref ST1
 *         @arg @ref ST2
 *         @arg @ref ST3
 *         @arg @ref ST4
 *         @arg @ref ST5
 * @param  Configuration This parameter must be a combination of all the following values:
 *         st_trin_tgs_ch This parameter can be one of the following values:
 *         @arg @ref LL_TIM_FAULT_TGS_CH0
 *         @arg @ref LL_TIM_FAULT_TGS_CH1
 *         @arg @ref LL_TIM_FAULT_TGS_CH2
 *         @arg @ref LL_TIM_FAULT_TGS_CH3
 *         @arg @ref LL_TIM_FAULT_TGS_CH4
 *         @arg @ref LL_TIM_FAULT_TGS_CH5
 *         @arg @ref LL_TIM_FAULT_TGS_CH6
 *         @arg @ref LL_TIM_FAULT_TGS_CH7
 * @retval None
 */
__STATIC_INLINE void LL_STIM_SetFaultTrigIn(STn_Type STn, uint32_t ch)
{
    MODIFY_REG(STIMER->FAULTCFG[STn], ST_FAULTCFG_TGSCH, ch);
}


/**
 * @brief  Configure the fault stop reset.
 * @param  STn_Type This parameter can be one of the following values:
 *         @arg @ref ST0
 *         @arg @ref ST1
 *         @arg @ref ST2
 *         @arg @ref ST3
 *         @arg @ref ST4
 *         @arg @ref ST5
 * @param  Configuration This parameter must be the following values:
 *         @arg @ref LL_TIM_FAULT_RESET_ALL
 *         @arg @ref LL_TIM_FAULT_RESET_EN
 * @retval None
 */
__STATIC_INLINE void LL_STIM_SetFaultResetRange(STn_Type STn, uint32_t cfg)
{
    MODIFY_REG(STIMER->FAULTCFG[STn], ST_FAULTCFG_RESET, cfg);
}


/**
 * @brief  Set the stimer mod0 value
 * @rmtoll STMOD0         STMOD0            LL_STIM_SetMod0Value
 * @param  STn_Type This parameter can be one of the following values:
 *         @arg @ref ST0
 *         @arg @ref ST1
 *         @arg @ref ST2
 *         @arg @ref ST3
 *         @arg @ref ST4
 *         @arg @ref ST5
 * @param  Mod0Value This parameter can be one of the following values:
            0~0xFFFFFFFFU
 * @retval None
 */
__STATIC_INLINE void LL_STIM_SetMod0Value(STn_Type STn, uint32_t Mod0Value)
{
    WRITE_REG(STIMER->MOD0[STn], Mod0Value);
}


/**
 * @brief  Get the stimer mod0 value
 * @rmtoll STMOD0         STMOD0            LL_STIM_GetMod0Value
 * @param  STn_Type This parameter can be one of the following values:
 *         @arg @ref ST0
 *         @arg @ref ST1
 *         @arg @ref ST2
 *         @arg @ref ST3
 *         @arg @ref ST4
 *         @arg @ref ST5
 * @retval MOD0Value
 */
__STATIC_INLINE uint32_t LL_STIM_GetMod0Value(STn_Type STn)
{
    return ((uint32_t)(READ_REG(STIMER->MOD0[STn])));
}


/**
 * @brief  Set the stimer mod1 value
 * @rmtoll STMOD1         STMOD1            LL_STIM_SetMod1Value
 * @param  STn_Type This parameter can be one of the following values:
 *         @arg @ref ST0
 *         @arg @ref ST1
 *         @arg @ref ST2
 *         @arg @ref ST3
 *         @arg @ref ST4
 *         @arg @ref ST5
 * @param  Mod1Value This parameter can be one of the following values:
            0~0xFFFFFFFFU
 * @retval None
 */
__STATIC_INLINE void LL_STIM_SetMod1Value(STn_Type STn, uint32_t Mod1Value)
{
    WRITE_REG(STIMER->MOD1[STn], Mod1Value);
}


/**
 * @brief  Get the stimer mod1 value
 * @rmtoll STMOD1         STMOD1            LL_STIM_GetMod1Value
 * @param  STn_Type This parameter can be one of the following values:
 *         @arg @ref ST0
 *         @arg @ref ST1
 *         @arg @ref ST2
 *         @arg @ref ST3
 *         @arg @ref ST4
 *         @arg @ref ST5
 * @retval MOD1Value
 */
__STATIC_INLINE uint32_t LL_STIM_GetMod1Value(STn_Type STn)
{
    return ((uint32_t)(READ_REG(STIMER->MOD1[STn])));
}


/**
 * @brief  Get the counter current value
 * @note STCVAL is read-only
 * @rmtoll STCVAL         STCVAL            LL_STIM_GetCVAL
 * @param  STn_Type This parameter can be one of the following values:
 *         @arg @ref ST0
 *         @arg @ref ST1
 *         @arg @ref ST2
 *         @arg @ref ST3
 *         @arg @ref ST4
 *         @arg @ref ST5
 * @retval STCVAL
 */
__STATIC_INLINE uint32_t LL_STIM_GetCurrentValue(STn_Type STn)
{
    LL_STIM_CaptureCurrentValue(STn);
    __nop();
    __nop();
    __nop();
    __nop();
    __nop();
    __nop();/*高主频时需增加延迟*/
    __nop();
    __nop();
    __nop();
    __nop();
    __nop();
    __nop();
    return ((uint32_t)(READ_REG(STIMER->CVAL[STn])));
}


/**
 * @brief  Enable stimer0,1,2 or 3 interrupt.
 * @rmtoll STCFG          INTDMAE|DMASEL           LL_STIM_EnableINT
 * @param  STn_Type This parameter can be one of the following values:
 *         @arg @ref ST0
 *         @arg @ref ST1
 *         @arg @ref ST2
 *         @arg @ref ST3
 *         @arg @ref ST4
 *         @arg @ref ST5
 * @retval None
 */
__STATIC_INLINE void LL_STIM_EnableINT(STn_Type STn)
{
    SET_BIT(STIMER->CFG[STn], ST_CFG_INTDMAE);
    CLEAR_BIT(STIMER->CFG[STn], ST_CFG_DMASEL);
}


/**
 * @brief  Enable stimer0,1,2 or 3 DMA.
 * @rmtoll STCFG          INTDMAE|DMASEL           LL_STIM_EnableDMA
 * @param  STn_Type This parameter can be one of the following values:
 *         @arg @ref ST0
 *         @arg @ref ST1
 *         @arg @ref ST2
 *         @arg @ref ST3
 *         @arg @ref ST4
 *         @arg @ref ST5
 * @retval None
 */
__STATIC_INLINE void LL_STIM_EnableDMA(STn_Type STn)
{
    SET_BIT(STIMER->CFG[STn], ST_CFG_INTDMAE);
    SET_BIT(STIMER->CFG[STn], ST_CFG_DMASEL);
}


/**
 * @brief  Disable stimer0,1,2 or 3 interrupt and DMA.
 * @rmtoll STCFG          INTDMAE           LL_STIM_DisableINTandDMA
 * @param  STn_Type This parameter can be one of the following values:
 *         @arg @ref ST0
 *         @arg @ref ST1
 *         @arg @ref ST2
 *         @arg @ref ST3
 *         @arg @ref ST4
 *         @arg @ref ST5
 * @retval None
 */
__STATIC_INLINE void LL_STIM_DisableINTandDMA(STn_Type STn)
{
    CLEAR_BIT(STIMER->CFG[STn], ST_CFG_INTDMAE);
}


/**
 * @brief  Indicate whether st flag is set.
 * @note ST FLAG is read-only
 * @rmtoll FLAG         ST FLAG            LL_STIM_IsActiveFlag
 * @param  STn_Type This parameter can be one of the following values:
 *         @arg @ref ST0
 *         @arg @ref ST1
 *         @arg @ref ST2
 *         @arg @ref ST3
 *         @arg @ref ST4
 *         @arg @ref ST5
 * @retval State of bit (1 or 0).
 */
__STATIC_INLINE uint32_t LL_STIM_IsActiveFlag(STn_Type STn)
{
    return ((uint32_t)(READ_BIT(STIMER->FLAG, ST_FLAG_STFLAG(STn)) == ST_FLAG_STFLAG(STn)));
}


/**
 * @brief  stimer_ifo_clear.
 * @rmtoll IFCL           ST1IFCL         LL_STIM_ClearFlag
 * @param  STn_Type This parameter can be one of the following values:
 *         @arg @ref ST0
 *         @arg @ref ST1
 *         @arg @ref ST2
 *         @arg @ref ST3
 *         @arg @ref ST4
 *         @arg @ref ST5
 * @retval None
 */
__STATIC_INLINE void LL_STIM_ClearFlag(STn_Type STn)
{
    WRITE_REG(STIMER->IFCL, ST_IFCL_STIFCL(STn));
}


__STATIC_INLINE void LL_STIM_ClearFlag_IT(STn_Type STn)
{
    WRITE_REG(STIMER->IFCL, ST_IFCL_STIFCL(STn));
}


#ifdef __cplusplus
}
#endif
#endif

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
