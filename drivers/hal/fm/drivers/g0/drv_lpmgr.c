/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_lpmgr.c
 *
 * @brief       This file implements low power manager for fm33.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <board.h>

#define DBG_EXT_TAG "drv.lpmgr"
#define DBG_EXT_LVL DBG_EXT_INFO

//����ʱSWD�������������գ����ܻ��Ӧ��ƽ����IO����Ĵ�����ת���𹦺ı��
//���SWD�����ⲿ�����Ļ�������ǰ�ɿ�������ʹ��
void SWD_IO_PullUp(FunState NewState)
{
    if (DISABLE == NewState)    //�ر�SWDTLK.SWDTDO����ʹ��
    {
        AltFunIO(GPIOG, GPIO_Pin_8, ALTFUN_NORMAL);    // PG8;//SWDTCK
        AltFunIO(GPIOG, GPIO_Pin_9, ALTFUN_NORMAL);    // PG9;//SWDTDO
    }
    else    //��SWDTCK,SWDTDO����ʹ��
    {
        AltFunIO(GPIOG, GPIO_Pin_8, ALTFUN_PULLUP);    // PG8;//SWDTCK
        AltFunIO(GPIOG, GPIO_Pin_9, ALTFUN_PULLUP);    // PG9;//SWDTDO
    }
}

void fm_MF_PMU_Init(void)
{
    SWD_IO_PullUp(ENABLE);
    IWDT_IWDTCFG_IWDTSLP4096S_Setable(ENABLE);    //��������ʱ�Ƿ�����4096s������
    IWDT_Clr();                                   //��ϵͳ���Ź�
}

void fm_Sleep(uint32_t mode)
{
    PMU_SleepCfg_InitTypeDef SleepCfg_InitStruct;

    /*�µ縴λ����*/
    // pdr��bor�����µ縴λ����Ҫ��һ��
    //����Դ��ѹ�����µ縴λʱ��оƬ�ᱻ��λס
    // pdr��ѹ��λ��׼���ǹ��ļ���(�����޲�����
    // bor��ѹ��λ׼ȷ������Ҫ����2uA����
    ANAC_PDRCON_PDREN_Setable(ENABLE);                  //��PDR
    ANAC_PDRCON_PDRCFG_Set(ANAC_PDRCON_PDRCFG_1P5V);    // pdr��ѹ������1.5V
    ANAC_BORCON_OFF_BOR_Setable(ENABLE);                //�ر�BOR 3uA
    RCC_SYSCLKSEL_LPM_RCLP_OFF_Setable(ENABLE);         //�ر�rclp 0.2uA

    SleepCfg_InitStruct.PMOD  = PMU_LPMCFG_PMOD_SLEEP;    //����ģʽ����
    SleepCfg_InitStruct.SLPDP = mode;                     // Sleep����
    SleepCfg_InitStruct.CVS   = DISABLE;                  //�ں˵�ѹ���Ϳ���
    SleepCfg_InitStruct.XTOFF = PMU_LPMCFG_XTOFF_DIS;     //����XTLF
    SleepCfg_InitStruct.SCR   = 0;                        // M0ϵͳ���ƼĴ�����һ������Ϊ0����

    PMU_SleepCfg_Init(&SleepCfg_InitStruct);    //��������

    IWDT_Clr();
    __WFI();    //��������
    IWDT_Clr();
}

/**
 ***********************************************************************************************************************
 * @brief           Put device into sleep mode.
 *
 * @param[in]       lpm             Low power manager structure.
 * @param[in]       mode            Low power mode.
 *
 * @return          None.
 ***********************************************************************************************************************
 */
static os_err_t lpm_sleep(lpmgr_sleep_mode_e mode)
{
    switch (mode)
    {
    case SYS_SLEEP_MODE_NONE:
        break;

    case SYS_SLEEP_MODE_IDLE:
        // __WFI();
        break;

    case SYS_SLEEP_MODE_LIGHT:
        fm_MF_PMU_Init(); /* sleep mode */
        fm_Sleep(PMU_LPMCFG_SLPDP_SLEEP);
        break;

    case SYS_SLEEP_MODE_DEEP:

        fm_MF_PMU_Init(); /* DeepSleep mode */
        fm_Sleep(PMU_LPMCFG_SLPDP_DEEPSLEEP);
        break;

    default:
        OS_ASSERT(0);
    }

    return OS_SUCCESS;
}

int drv_lpmgr_hw_init(void)
{
    os_lpmgr_init(lpm_sleep);
    return 0;
}

OS_INIT_CALL(drv_lpmgr_hw_init, OS_INIT_LEVEL_PRE_DEVICE, OS_INIT_SUBLEVEL_MIDDLE);
