/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_hwtimer.c
 *
 * @brief       This file implements timer driver for fm33.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <board.h>
#include <drv_hwtimer.h>

#include <os_memory.h>
#include <timer/timer.h>

#ifdef OS_USING_CLOCKSOURCE
#include <timer/clocksource.h>
#endif

#ifdef OS_USING_CLOCKEVENT
#include <timer/clockevent.h>
#endif

#define DRV_EXT_LVL DBG_EXT_INFO
#define DRV_EXT_TAG "drv.hwtimer"
#include <drv_log.h>

static os_list_node_t fm33_timer_list = OS_LIST_INIT(fm33_timer_list);

os_bool_t fm33_timer_is_32b(struct fm33_timer *timer)
{
    OS_ASSERT(timer != OS_NULL);

    if (timer->info->type == TYPE_ETIM12 || timer->info->type == TYPE_ETIM34)
    {
        return OS_TRUE;
    }
    else
    {
        return OS_FALSE;
    }
}

static uint32_t fm33_timer_get_freq(struct fm33_timer *timer)
{
    OS_ASSERT(timer != OS_NULL);

    RCC_ClocksType clock_source;

    RCC_GetClocksFreq(&clock_source);

    return clock_source.APBCLK_Frequency;
}

static void fm33_timer_update_callback(hwtimer_type_t type)
{
    struct fm33_timer *timer;

    os_list_for_each_entry(timer, &fm33_timer_list, struct fm33_timer, list)
    {
#ifdef OS_USING_CLOCKEVENT
        if (timer->mode == TIMER_MODE_TIM && timer->info->type == type)
        {
            os_clockevent_isr((os_clockevent_t *)timer);

            break;
        }
#endif
    }
}

/*timer irq handlers*/
#ifdef BSP_USING_ETIM
void ETIM1_IRQHandler(void)
{
    if (SET == ETIMx_ETxIF_OVIF_Chk(ETIM1))
    {
        ETIMx_ETxIF_OVIF_Clr(ETIM1);
    }
}

void ETIM2_IRQHandler(void)
{
    if (SET == ETIMx_ETxIF_OVIF_Chk(ETIM2))
    {
        ETIMx_ETxIF_OVIF_Clr(ETIM2);
        fm33_timer_update_callback(TYPE_ETIM12);
    }
}

void ETIM3_IRQHandler(void)
{
    if (SET == ETIMx_ETxIF_OVIF_Chk(ETIM3))
    {
        ETIMx_ETxIF_OVIF_Clr(ETIM3);
    }
}

void ETIM4_IRQHandler(void)
{
    if (SET == ETIMx_ETxIF_OVIF_Chk(ETIM4))
    {
        ETIMx_ETxIF_OVIF_Clr(ETIM4);
        fm33_timer_update_callback(TYPE_ETIM34);
    }
}

#endif /*BSP_USING_ETIM*/

static void hal_timer_start(struct fm33_timer *timer, uint32_t prescaler, uint64_t count, os_bool_t irq_enable)
{
    OS_ASSERT(timer != OS_NULL);
    OS_ASSERT(count != 0);

    switch (timer->info->type)
    {
#ifdef BSP_USING_ETIM
    case TYPE_ETIM12:
    case TYPE_ETIM34:
    {
        ETIM_InitTypeDef init_para;
        uint32_t      tim_count;

        /*serial timer clk source control*/
        RCC_PERCLK_SetableEx(timer->info->tim_src.clk, ENABLE);
        NVIC_DisableIRQ(timer->info->tim_src.irqn);

        init_para.sig_src_para.SIG1SEL   = ETIMx_ETxINSEL_SIG1SEL_GROUP1;
        init_para.sig_src_para.GRP1SEL   = timer->info->grp1sel;
        init_para.sig_src_para.PRESCALE1 = prescaler - 1;
        init_para.sig_src_para.PRESCALE2 = prescaler - 1;

        init_para.ctrl_para.EXFLT     = ENABLE;
        init_para.ctrl_para.MOD       = ETIMx_ETxCR_MOD_COUNTER;
        init_para.ctrl_para.CASEN     = ENABLE;
        init_para.ctrl_para.EDGESEL   = ETIMx_ETxCR_EDGESEL_RISING;
        init_para.ctrl_para.CAPCLR    = DISABLE;
        init_para.ctrl_para.INITVALUE = 0;
        init_para.ctrl_para.CMP       = 0;
        init_para.ctrl_para.CEN       = DISABLE;

        ETIMx_Init(timer->info->tim_src.inst, &init_para);

        /*serial timer irq control*/
        RCC_PERCLK_SetableEx(timer->info->tim_irq.clk, ENABLE);
        if (irq_enable == OS_TRUE)
        {
            NVIC_DisableIRQ(timer->info->tim_irq.irqn);
            NVIC_SetPriority(timer->info->tim_irq.irqn, 2);
            NVIC_EnableIRQ(timer->info->tim_irq.irqn);
        }

        init_para.ctrl_para.CMPIE     = DISABLE;
        init_para.ctrl_para.CAPIE     = DISABLE;
        init_para.ctrl_para.OVIE      = ENABLE;
        init_para.ctrl_para.INITVALUE = 0;
        init_para.ctrl_para.CMP       = 0;
        init_para.ctrl_para.CEN       = DISABLE;

        ETIMx_Init(timer->info->tim_irq.inst, &init_para);

        /*irq triggered while count increases from initial val to 0xffffffff*/
        tim_count = 0xffffffff - count;

        ETIMx_ETxIVR_Write(timer->info->tim_src.inst, tim_count & 0xffff);
        ETIMx_ETxIVR_Write(timer->info->tim_irq.inst, (tim_count >> 16) & 0xffff);

        ETIMx_ETxCR_CEN_Setable(timer->info->tim_src.inst, ENABLE);
        ETIMx_ETxCR_CEN_Setable(timer->info->tim_irq.inst, ENABLE);
    }
    break;
#endif

    default:
        break;
    }
}

static void hal_timer_stop(struct fm33_timer *timer)
{
    OS_ASSERT(timer != OS_NULL);

    switch (timer->info->type)
    {
#ifdef BSP_USING_ETIM
    case TYPE_ETIM12:
    case TYPE_ETIM34:
        ETIMx_ETxCR_CEN_Setable(timer->info->tim_src.inst, DISABLE);
        ETIMx_ETxCR_CEN_Setable(timer->info->tim_irq.inst, DISABLE);
        ETIMx_ETxIF_OVIF_Clr(timer->info->tim_src.inst);
        ETIMx_ETxIF_OVIF_Clr(timer->info->tim_irq.inst);
        break;
#endif
    default:
        break;
    }
}

static uint64_t hal_timer_read(struct fm33_timer *timer)
{
    OS_ASSERT(timer != OS_NULL);

    uint64_t timer_counter;
    switch (timer->info->type)
    {
#ifdef BSP_USING_ETIM
    case TYPE_ETIM12:
    case TYPE_ETIM34:
    {
        uint32_t init_src, init_irq;
        uint32_t count_src, count_irq;

        init_src  = ETIMx_ETxIVR_Read(timer->info->tim_src.inst);
        init_irq  = ETIMx_ETxIVR_Read(timer->info->tim_irq.inst);
        count_src = ETIMx_ETxCNT_Read(timer->info->tim_src.inst);
        count_irq = ETIMx_ETxCNT_Read(timer->info->tim_irq.inst);

        timer_counter = (count_src | (count_irq << 16)) - (init_src | (init_irq << 16));
    }
    break;
#endif

    default:
        break;
    }

    return timer_counter;
}

static uint64_t fm33_timer_read(void *clock)
{
    struct fm33_timer *timer;

    timer = (struct fm33_timer *)clock;

    return hal_timer_read(timer);
}

#ifdef OS_USING_CLOCKEVENT
static void fm33_timer_start(os_clockevent_t *ce, uint32_t prescaler, uint64_t count)
{
    struct fm33_timer *timer;

    timer = (struct fm33_timer *)ce;

    if (timer->info->type != TYPE_LPTIM)
    {
        OS_ASSERT(prescaler != 0);
    }
    OS_ASSERT(count != 0);

    hal_timer_start(timer, prescaler, count, OS_TRUE);
}

static void fm33_timer_stop(os_clockevent_t *ce)
{
    struct fm33_timer *timer;

    OS_ASSERT(ce != OS_NULL);

    timer = (struct fm33_timer *)ce;

    hal_timer_stop(timer);
}

static const struct os_clockevent_ops fm33_tim_ops = {
    .start = fm33_timer_start,
    .stop  = fm33_timer_stop,
    .read  = fm33_timer_read,
};
#endif

/**
 ***********************************************************************************************************************
 * @brief           fm33_tim_probe:probe timer device.
 *
 * @param[in]       none
 *
 * @return          Return timer probe status.
 * @retval          OS_SUCCESS         timer register success.
 * @retval          OS_FAILURE       timer register failed.
 ***********************************************************************************************************************
 */
static int fm33_tim_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    struct fm33_timer      *timer;
    struct fm33_timer_info *info;

    timer = os_calloc(1, sizeof(struct fm33_timer));
    OS_ASSERT(timer);

    info = (struct fm33_timer_info *)dev->info;

    timer->info = info;
    timer->freq = fm33_timer_get_freq(timer);
    timer->mode = info->mode;

#ifdef OS_USING_PWM
    if (timer->mode == TIMER_MODE_PWM)
    {
        fm33_pwm_register(dev->name, timer);
    }
#endif

#ifdef OS_USING_PULSE_ENCODER
    if (timer->mode == TIMER_MODE_PULSE_ENCODER)
    {
        fm33_pulse_encoder_register(dev->name, timer);
    }
#endif

    if (timer->mode == TIMER_MODE_TIM)
    {
#ifdef OS_USING_CLOCKSOURCE
        if (os_clocksource_best() == OS_NULL && fm33_timer_is_32b(timer))
        {
            hal_timer_start(timer, 1, 0xffffffffull, OS_FALSE);

            timer->clock.cs.rating = 320;
            timer->clock.cs.freq   = timer->freq;
            timer->clock.cs.mask   = 0xffffffffull;
            timer->clock.cs.read   = fm33_timer_read;

            os_clocksource_register(dev->name, &timer->clock.cs);
        }
        else
#endif
        {
#ifdef OS_USING_CLOCKEVENT
            timer->clock.ce.rating = fm33_timer_is_32b(timer) ? 320 : 160;
            timer->clock.ce.freq   = timer->freq;
            timer->clock.ce.mask   = 0xffffffffull;

            timer->clock.ce.prescaler_mask = 0xfffful;
            timer->clock.ce.prescaler_bits = 16;

            timer->clock.ce.count_mask = fm33_timer_is_32b(timer) ? 0xfffffffful : 0xfffful;
            timer->clock.ce.count_bits = fm33_timer_is_32b(timer) ? 32 : 16;

            timer->clock.ce.feature = OS_CLOCKEVENT_FEATURE_PERIOD;

            timer->clock.ce.min_nsec = NSEC_PER_SEC / timer->clock.ce.freq;

            timer->clock.ce.ops = &fm33_tim_ops;

            os_clockevent_register(dev->name, &timer->clock.ce);
#endif
        }
    }

    os_list_add(&fm33_timer_list, &timer->list);

    return OS_SUCCESS;
}

OS_DRIVER_INFO fm33_tim_driver = {
    .name  = "TIMER_Type",
    .probe = fm33_tim_probe,
};

OS_DRIVER_DEFINE(fm33_tim_driver, OS_INIT_LEVEL_PRE_DEVICE, OS_INIT_SUBLEVEL_MIDDLE);

