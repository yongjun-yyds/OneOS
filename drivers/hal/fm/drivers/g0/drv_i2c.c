/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        d2v_i2c.c
 *
 * @brief       this file implements i2c bus related functions.
 *
 * @details
 *
 * @revision
 * Date          Author          Notes
 * 2020-02-20    OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <board.h>
#include <os_types.h>
#include <device.h>
#include <arch_interrupt.h>
#include <os_assert.h>
#include <drv_common.h>
#include <os_memory.h>
#include <string.h>
#include <timer/clocksource.h>
#include <drv_i2c.h>

#define DBG_TAG "drv.i2c"
#include <drv_log.h>

#define SSP_TBIT   (8 * clkmode)
#define STARTBIT   0
#define RESTARTBIT 1
#define STOPBIT    2
#define ACKBIT     3

static os_list_node_t fm33_i2c_list = OS_LIST_INIT(fm33_i2c_list);

static uint8_t I2C_Send_Bit(uint8_t BIT_def)
{
    uint8_t i;

    I2C_I2CCTRL_I2CEN_Setable(ENABLE);

    switch (BIT_def)
    {
    case STARTBIT:
        I2C_SEND_STARTBIT();
        break;

    case RESTARTBIT:
        I2C_SEND_RESTARTBIT();
        break;

    case STOPBIT:
        I2C_SEND_STOPBIT();
        break;

    case ACKBIT:
        I2C_SEND_ACKBIT();
        break;

    default:
        break;
    }

    for (i = 0; i < 2 * SSP_TBIT; i++)
    {
        if (SET == I2C_I2CIR_I2CIF_Chk())
            break;
    }

    if (i < 2 * SSP_TBIT)
    {
        I2C_I2CIR_I2CIF_Clr();
        i = I2C_I2CSTA_RW_Chk();

        return 0;
    }

    return 1;
}

static uint8_t I2C_Send_Byte(uint8_t x_byte)
{
    uint16_t i;

    I2C_I2CBUF_Write(x_byte);

    for (i = 0; i < 18 * SSP_TBIT; i++)
    {
        if (SET == I2C_I2CIR_I2CIF_Chk())
            break;
    }

    if (i < 18 * SSP_TBIT)
    {
        I2C_I2CIR_I2CIF_Clr();

        if (SET == I2C_I2CSTA_ACKSTA_Chk())
        {
            return 2;
        }

        return 0;
    }

    return 1;
}

static uint8_t I2C_Receive_Byte(uint8_t *x_byte)
{
    uint16_t i;

    I2C_I2CCTRL_RCEN_Setable(ENABLE);

    for (i = 0; i < 18 * SSP_TBIT; i++)
    {
        if (SET == I2C_I2CIR_I2CIF_Chk())
            break;
    }

    if (i < 18 * SSP_TBIT)
    {
        *x_byte = I2C_I2CBUF_Read();
        I2C_I2CIR_I2CIF_Clr();
        return 0;
    }

    return 1;
}

static uint8_t Wait_for_end(uint8_t Device)
{
    uint8_t result;

    Do_DelayStart();
    {
        I2C_Send_Bit(STARTBIT);

        result = I2C_Send_Byte(Device);

        I2C_Send_Bit(STOPBIT);

        if (result == 0)
            return 0;
    }
    While_DelayMsEnd(5 * clkmode);

    return 1;
}

os_err_t HAL_I2C_Master_Receive(struct fm33_i2c *fm_i2c,
                                uint16_t      DevAddress,
                                uint8_t      *pData,
                                uint16_t      Size,
                                uint32_t      Timeout)
{
    uint16_t i;

    if (I2C_Send_Bit(RESTARTBIT) != 0)
    {
        return OS_FAILURE;
    }

    if (I2C_Send_Byte(DevAddress | 1) != 0)
    {
        return OS_FAILURE;
    }

    for (i = 0; i < Size; i++)
    {
        if (I2C_Receive_Byte(pData + i) != 0)
        {
            return OS_FAILURE;
        }

        i < (Size - 1) ? I2C_SEND_ACK_0() : I2C_SEND_ACK_1();

        if (I2C_Send_Bit(ACKBIT) != 0)
        {
            I2C_SEND_ACK_0();
            return OS_FAILURE;
        }

        I2C_SEND_ACK_0();
    }

    if (I2C_Send_Bit(STOPBIT) != 0)
    {
        return OS_FAILURE;
    }

    return OS_SUCCESS;
}

os_err_t HAL_I2C_Master_Transmit(struct fm33_i2c *fm_i2c,
                                 uint16_t      DevAddress,
                                 uint8_t      *pData,
                                 uint16_t      Size,
                                 uint32_t         Timeout)
{
    uint16_t i;

    I2C_ResetI2C();

    if (I2C_Send_Bit(STARTBIT) != 0)
    {
        return OS_FAILURE;
    }

    I2C_I2CCTRL_RCEN_Setable(DISABLE);

    if (I2C_Send_Byte(DevAddress) != 0)
    {
        return OS_FAILURE;
    }

    for (i = 0; i < Size; i++)
    {
        if (I2C_Send_Byte(pData[i]) != 0)
        {
            return OS_FAILURE;
        }
    }

    if (Size > 2)
    {
        if (I2C_Send_Bit(STOPBIT) != 0)
        {
            return OS_FAILURE;
        }

        Wait_for_end(DevAddress);
    }

    return OS_SUCCESS;
}

static os_err_t fm33_check_and_solve_bus_busy(struct fm33_i2c *fm_i2c)
{
    return OS_SUCCESS;
}

static os_size_t fm33_i2c_transfer(struct os_i2c_bus_device *bus, struct os_i2c_msg msgs[], uint32_t num)
{
    os_err_t    result           = OS_SUCCESS;
    uint32_t msgs_count       = 0;
    uint32_t msgs_count_cur   = 0;
    uint32_t msgs_buf_length  = 0;
    uint16_t dev_addr         = 0;
    uint16_t i2c_flag         = 0;
    uint8_t *i2c_data_buf     = OS_NULL;
    uint8_t *i2c_data_buf_cur = OS_NULL;

    struct fm33_i2c *fm_i2c;
    fm_i2c = os_container_of(bus, struct fm33_i2c, i2c);

    while (msgs_count_cur < num)
    {
        msgs_buf_length = msgs[msgs_count_cur].len;
        i2c_flag        = msgs[msgs_count_cur].flags;
        msgs_count      = msgs_count_cur + 1;
        dev_addr        = msgs[msgs_count_cur].addr << 1;
        while (msgs_count < num)
        {
            if ((msgs[msgs_count].flags & OS_I2C_NO_START) && (msgs[msgs_count].addr == msgs[msgs_count_cur].addr) &&
                ((i2c_flag & 0x01) == (msgs[msgs_count].flags & 0x01)))
            {
                msgs_buf_length += msgs[msgs_count].len;
                msgs_count++;
            }
            else
            {
                break;
            }
        }

        if (msgs_count - msgs_count_cur > 1)
        {
            i2c_data_buf = (uint8_t *)os_calloc(1, msgs_buf_length);
            if (i2c_data_buf == OS_NULL)
            {
                LOG_E(DBG_TAG, "i2c calloc failed! too many message with same condition to merge!");
            }

            i2c_data_buf_cur = i2c_data_buf;

            if ((i2c_flag & 0x01) == OS_I2C_WR)
            {
                while (msgs_count_cur < msgs_count)
                {
                    memcpy(i2c_data_buf_cur, msgs[msgs_count_cur].buf, msgs[msgs_count_cur].len);
                    i2c_data_buf_cur += msgs[msgs_count_cur].len;
                    msgs_count_cur++;
                }
            }

            if (i2c_flag & OS_I2C_RD)
            {
                result = HAL_I2C_Master_Receive(fm_i2c, dev_addr, i2c_data_buf, msgs_buf_length, bus->timeout);
                while (msgs_count_cur < msgs_count)
                {
                    memcpy(msgs[msgs_count_cur].buf, i2c_data_buf_cur, msgs[msgs_count_cur].len);
                    i2c_data_buf_cur += msgs[msgs_count_cur].len;
                    msgs_count_cur++;
                }
            }
            else
            {
                result = HAL_I2C_Master_Transmit(fm_i2c, dev_addr, i2c_data_buf, msgs_buf_length, bus->timeout);
            }
            os_free(i2c_data_buf);
        }
        else
        {
            if (i2c_flag & OS_I2C_RD)
            {
                result = HAL_I2C_Master_Receive(fm_i2c,
                                                dev_addr,
                                                msgs[msgs_count_cur].buf,
                                                msgs[msgs_count_cur].len,
                                                bus->timeout);
            }
            else
            {
                result = HAL_I2C_Master_Transmit(fm_i2c,
                                                 dev_addr,
                                                 msgs[msgs_count_cur].buf,
                                                 msgs[msgs_count_cur].len,
                                                 bus->timeout);
            }
            msgs_count_cur++;
        }

        if (result != OS_SUCCESS)
        {
            break;
        }
    }

    if (result == OS_SUCCESS)
    {
        return num;
    }
    else
    {
        result = fm33_check_and_solve_bus_busy(fm_i2c);
        if (result == OS_FAILURE)
        {
            LOG_E(DBG_TAG, "unlock i2c bus error!");
        }
        else
        {
            LOG_I(DBG_TAG, "unlock i2c bus success!");
        }
        return 0;
    }
}

static os_err_t fm33_i2c_init(struct fm33_i2c *dev)
{
    RCC_PERCLK_SetableEx(I2CCLK, ENABLE);

    AltFunIO(dev->info->sda.port, dev->info->sda.gpio, ALTFUN_OPENDRAIN);
    AltFunIO(dev->info->scl.port, dev->info->scl.gpio, ALTFUN_OPENDRAIN);

    I2C_I2CBRG_Write(I2C_BaudREG_Calc(dev->info->baud.i2c_clk, dev->info->baud.apb_clk));
    I2C_I2CIR_I2CIE_Setable(DISABLE);
    I2C_I2CERR_ERRIE_Setable(DISABLE);

    NVIC_DisableIRQ(I2C_IRQn);

    GPIO_ResetBits(dev->info->sda.port, dev->info->sda.gpio);
    GPIO_ResetBits(dev->info->scl.port, dev->info->scl.gpio);

    return OS_SUCCESS;
}

static const struct os_i2c_bus_device_ops i2c_bus_ops = {
    .i2c_transfer       = fm33_i2c_transfer,
    .i2c_slave_transfer = OS_NULL,
    .i2c_bus_control    = OS_NULL,
};

static int fm33_i2c_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    os_ubase_t level;

    struct fm33_i2c_info *i2c_info = (struct fm33_i2c_info *)dev->info;
    struct fm33_i2c      *fm_i2c   = os_calloc(1, sizeof(struct fm33_i2c));
    OS_ASSERT(fm_i2c);

    fm_i2c->info = i2c_info;
    fm33_i2c_init(fm_i2c);

    struct os_i2c_bus_device *dev_i2c = &fm_i2c->i2c;
    dev_i2c->ops                      = &i2c_bus_ops;

    os_spin_lock_irqsave(&gs_device_lock, &level);
    os_list_add_tail(&fm33_i2c_list, &fm_i2c->list);
    os_spin_unlock_irqrestore(&gs_device_lock, level);

    return os_i2c_bus_device_register(dev_i2c, dev->name, dev_i2c);
}

OS_DRIVER_INFO fm33_i2c_driver = {
    .name  = "I2C_Type",
    .probe = fm33_i2c_probe,
};

OS_DRIVER_DEFINE(fm33_i2c_driver, OS_INIT_LEVEL_DEVICE, OS_INIT_SUBLEVEL_HIGH);

