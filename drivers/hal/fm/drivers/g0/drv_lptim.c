/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_lptim.c
 *
 * @brief       This file implements low power timer driver for fm33.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <board.h>

#define DRV_EXT_LVL DBG_EXT_INFO
#define DRV_EXT_TAG "drv.lptimer"
#include <drv_log.h>

#define LPTIME_FRE ((32 * 1000) >> 7)

struct fm_lptimer
{
    os_clockevent_t    ce;
    TIM_HandleTypeDef *handle;
    uint32_t        freq;
    os_list_node_t     list;
};

static os_list_node_t fm_lptimer_list = OS_LIST_INIT(fm_lptimer_list);

void lptim_irqhandler(LPTIM_Type *lptim)
{
    struct fm_lptimer *lptimer = OS_NULL;
    os_list_for_each_entry(lptimer, &fm_lptimer_list, struct fm_lptimer, list)
    {
        if (lptimer->handle->instance == lptim)
        {
            os_clockevent_isr((os_clockevent_t *)lptimer);
            LPTIM_LPTIF_COMPIF_Clr();
            LPTIM_LPTIF_OVIF_Clr();
            LPTIM_LPTIF_TRIGIF_Clr();
        }
    }
}

void LPTIM_IRQHandler(void)
{
    lptim_irqhandler(LPTIM);
}

/* 32kHzʱ��,ʹ��128��Ƶ��һ����͹���ʱ��:0xffff*1/(32*1000/128) = 262.14 s */
static void fm_lptimer_start(os_clockevent_t *ce, uint32_t prescaler, uint64_t count)
{
    OS_ASSERT(prescaler == 1);
    OS_ASSERT(count != 0);

    LPTIM_InitTypeDef init_para;

    //ʹ��LPTIMER������ʱ��
    RCC_PERCLK_SetableEx(LPTFCLK, ENABLE);
    RCC_PERCLK_SetableEx(LPTRCLK, ENABLE);
    init_para.LPTIM_TMODE = LPTIM_LPTCFG_TMODE_PWMIM;
    init_para.LPTIM_MODE  = LPTIM_LPTCFG_MODE_SINGLE;

    init_para.LPTIM_CLK_SEL = LPTIM_LPTCFG_CLKSEL_LSCLK;
    init_para.LPTIM_CLK_DIV = LPTIM_LPTCFG_DIVSEL_128;

    init_para.LPTIM_compare_value = count;
    init_para.LPTIM_target_value  = 0xffff;

    LPTIM_Init(&init_para);

    NVIC_DisableIRQ(LPTIM_IRQn);
    NVIC_SetPriority(LPTIM_IRQn, 2);
    NVIC_EnableIRQ(LPTIM_IRQn);

    LPTIM_LPTIE_COMPIE_Setable(ENABLE); /* ʹ�ñȽ��жϻ��ѵ͹��� */
    LPTIM_LPTCTRL_LPTEN_Setable(ENABLE);
}

static void fm_lptimer_stop(os_clockevent_t *ce)
{
    LPTIM_LPTIE_COMPIE_Setable(DISABLE);
    LPTIM_LPTCTRL_LPTEN_Setable(DISABLE);
}

uint64_t fm_lptimer_read(void *clock)
{
    return (uint64_t)LPTIM_LPTCNT_Read();
}

static const struct os_clockevent_ops fm_lptim_ops = {
    .start = fm_lptimer_start,
    .stop  = fm_lptimer_stop,
    .read  = fm_lptimer_read,
};

/**
 ***********************************************************************************************************************
 * @brief           fm_tim_probe:probe timer device.
 *
 * @param[in]       none
 *
 * @return          Return timer probe status.
 * @retval          OS_SUCCESS         timer register success.
 * @retval          OS_FAILURE       timer register failed.
 ***********************************************************************************************************************
 */
static int fm_lptim_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    struct fm_lptimer *timer;
    TIM_HandleTypeDef *lptim;

    timer = os_calloc(1, sizeof(struct fm_lptimer));
    OS_ASSERT(timer);

    lptim = (TIM_HandleTypeDef *)dev->info;

    timer->handle = lptim;

    timer->freq = LPTIME_FRE;

    timer->ce.rating = 50;
    timer->ce.freq   = timer->freq;
    timer->ce.mask   = 0xfffful;

    timer->ce.prescaler_mask = 1;
    timer->ce.prescaler_bits = 0;

    timer->ce.count_mask = 0xfffful;
    timer->ce.count_bits = 16;

    timer->ce.feature = OS_CLOCKEVENT_FEATURE_ONESHOT;

    timer->ce.min_nsec = NSEC_PER_SEC / timer->ce.freq;

    timer->ce.ops = &fm_lptim_ops;
    os_clockevent_register(dev->name, &timer->ce);

    os_list_add(&fm_lptimer_list, &timer->list);

    return OS_SUCCESS;
}

OS_DRIVER_INFO fm_lptim_driver = {
    .name  = "LPTIM_HandleTypeDef",
    .probe = fm_lptim_probe,
};

OS_DRIVER_DEFINE(fm_lptim_driver, OS_INIT_LEVEL_PRE_DEVICE, OS_INIT_SUBLEVEL_HIGH);

