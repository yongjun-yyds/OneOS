/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_rtc.c
 *
 * @brief       This file implements RTC driver for fm33.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <os_memory.h>
#include <sys/time.h>
#include <drv_log.h>
#include "drv_rtc.h"
#include <string.h>

#define DBG_TAG "drv.rtc"
#include <drv_log.h>
#include <dlog.h>

#define DEFAULT_RTC_TIME                                                                                               \
    {                                                                                                                  \
        0x20, 0x02, 0x21, 0x04, 0x09, 0x00, 0x00,                                                                      \
    }

struct fm33_alarm
{
    uint32_t hour;
    uint32_t minute;
    uint32_t second;
};

struct fm33_rtc
{
    os_device_t           rtc;
    struct fm33_rtc_info *info;
};

static uint8_t Hex_To_ByteDec(uint8_t PuB_Dat)
{
    uint8_t dat;
    dat = (PuB_Dat / 16) * 10 + PuB_Dat % 16;
    return dat;
}

static uint8_t IntDec_To_Hex(int PuB_Dat)
{
    uint8_t dat;
    dat = (PuB_Dat / 10) * 16 + PuB_Dat % 10;
    return dat;
}

static uint8_t RTC_GetRTC(RTC_TimeDateTypeDef *para)
{
    uint8_t n, i;
    uint8_t Result = 1;

    RTC_TimeDateTypeDef TempTime1, TempTime2;

    for (n = 0; n < 3; n++)
    {
        RTC_TimeDate_GetEx(&TempTime1);
        RTC_TimeDate_GetEx(&TempTime2);

        for (i = 0; i < 7; i++)
        {
            if (((uint8_t *)(&TempTime1))[i] != ((uint8_t *)(&TempTime2))[i])
            {
                break;
            }
        }

        if (i == 7)
        {
            Result = 0;
            memcpy((uint8_t *)(para), (uint8_t *)(&TempTime1), 7);
            break;
        }
    }
    return Result;
}

static uint8_t RTC_SetRTC(RTC_TimeDateTypeDef *para)
{
    uint8_t          n, i;
    uint8_t          Result;
    RTC_TimeDateTypeDef TempTime1;

    for (n = 0; n < 3; n++)
    {
        RTC_RTCWE_Write(RTC_WRITE_ENABLE);
        RTC_TimeDate_SetEx(para);
        RTC_RTCWE_Write(RTC_WRITE_DISABLE);

        Result = RTC_GetRTC(&TempTime1);
        if (Result == 0)
        {
            Result = 1;

            for (i = 0; i < 7; i++)
            {
                if (((uint8_t *)(&TempTime1))[i] != ((uint8_t *)(para))[i])
                {
                    break;
                }
            }

            if (i == 7)
            {
                Result = 0;
                break;
            }
        }
    }

    return Result;
}

static uint8_t RTC_SetAlarm(struct tm *p_tm)
{
    RTC_AlarmTmieTypeDef TempAlarmTime;

    TempAlarmTime.Hour   = IntDec_To_Hex(p_tm->tm_hour);
    TempAlarmTime.Minute = IntDec_To_Hex(p_tm->tm_min);
    TempAlarmTime.Second = IntDec_To_Hex(p_tm->tm_sec);

    RTC_AlarmTime_SetEx(&TempAlarmTime);

    RTC_RTCIE_SetableEx(ENABLE, RTC_RTCIE_ALARM_IE_Msk);
    RTC_RTCIF_ClrEx(RTC_RTCIE_ALARM_IE_Msk);
    NVIC_DisableIRQ(RTC_IRQn);
    NVIC_SetPriority(RTC_IRQn, 2);
    NVIC_EnableIRQ(RTC_IRQn);

    return OS_SUCCESS;
}

OS_WEAK void rtc_alarm_callback(void *args)
{
    os_kprintf("rtc alarm call back.\r\n");
}

void RTC_IRQHandler(void)
{
    if (SET == RTC_RTCIF_ChkEx(RTC_RTCIE_ALARM_IE_Msk))
    {
        RTC_RTCIF_ClrEx(RTC_RTCIE_ALARM_IE_Msk);
        rtc_alarm_callback(OS_NULL);
    }
}

static time_t fm33_rtc_get_timestamp(void)
{
    time_t              ret;
    struct tm           tm_new;
    RTC_TimeDateTypeDef rtc_time;

    RTC_GetRTC(&rtc_time);

    tm_new.tm_sec  = Hex_To_ByteDec(rtc_time.Second);
    tm_new.tm_min  = Hex_To_ByteDec(rtc_time.Minute);
    tm_new.tm_hour = Hex_To_ByteDec(rtc_time.Hour);
    tm_new.tm_mday = Hex_To_ByteDec(rtc_time.Date);
    tm_new.tm_mon  = Hex_To_ByteDec(rtc_time.Month) - 1;
    tm_new.tm_year = Hex_To_ByteDec(rtc_time.Year) + 100;

    LOG_D(DBG_TAG, "get rtc time.");

    ret = mktime(&tm_new);

    return ret;
}

static os_err_t fm33_rtc_set_time_stamp(time_t time_stamp)
{
    struct tm *p_tm;

    p_tm = localtime(&time_stamp);
    if (p_tm->tm_year < 100)
    {
        return OS_FAILURE;
    }

    RTC_TimeDateTypeDef rtc_time;

    rtc_time.Second = IntDec_To_Hex(p_tm->tm_sec);
    rtc_time.Minute = IntDec_To_Hex(p_tm->tm_min);
    rtc_time.Hour   = IntDec_To_Hex(p_tm->tm_hour);
    rtc_time.Date   = IntDec_To_Hex(p_tm->tm_mday);
    rtc_time.Month  = IntDec_To_Hex(p_tm->tm_mon + 1);
    rtc_time.Year   = IntDec_To_Hex(p_tm->tm_year - 100);

    RTC_SetRTC(&rtc_time);

    return OS_SUCCESS;
}

static os_err_t fm33_rtc_set_alarm_stamp(time_t time_stamp)
{
    struct tm *p_tm;
    os_ubase_t level;

    os_spin_lock_irqsave(&gs_device_lock, &level);
    p_tm  = localtime(&time_stamp);
    if (p_tm->tm_year < 100)
    {
        os_spin_unlock_irqrestore(&gs_device_lock, level);
        return OS_FAILURE;
    }

    RTC_SetAlarm(p_tm);

    os_spin_unlock_irqrestore(&gs_device_lock, level);

    return OS_SUCCESS;
}

static void fm33_rtc_init(struct fm33_rtc *rtc)
{
    RCC_PERCLK_SetableEx(RTCCLK, ENABLE);

    RTC_FSEL_FSEL_Set(RTC_FSEL_FSEL_PLL1HZ);
    RTC_Trim_Proc(0);
    RTC_PR1SEN_PR1SEN_Setable(DISABLE);

    RTC_STAMPEN_STAMP0EN_Setable(DISABLE);
    RTC_STAMPEN_STAMP1EN_Setable(DISABLE);

    RTC_RTCIE_SetableEx(DISABLE, 0xFFFFFFFF);

    NVIC_DisableIRQ(RTC_IRQn);
}

static os_err_t os_rtc_control(os_device_t *dev, int cmd, void *args)
{
    os_err_t result = OS_FAILURE;

    OS_ASSERT(dev != OS_NULL);

    switch (cmd)
    {
    case OS_DEVICE_CTRL_RTC_GET_TIME:
        *(uint32_t *)args = fm33_rtc_get_timestamp();
        LOG_D(DBG_TAG, "get rtc_time %x", *(uint32_t *)args);
        result = OS_SUCCESS;
        break;

    case OS_DEVICE_CTRL_RTC_SET_TIME:
        if (fm33_rtc_set_time_stamp(*(uint32_t *)args))
        {
            result = OS_FAILURE;
        }
        LOG_D(DBG_TAG, "set rtc_time %x", *(uint32_t *)args);
        result = OS_SUCCESS;
        break;
    case OS_DEVICE_CTRL_RTC_SET_ALARM:
        if (fm33_rtc_set_alarm_stamp(*(uint32_t *)args))
        {
            result = OS_FAILURE;
        }
        result = OS_SUCCESS;
        break;
    default:
        result = OS_FAILURE;
        break;
    }

    return result;
}

const static struct os_device_ops rtc_ops = {
    .control = os_rtc_control,
};

static int fm33_rtc_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    struct fm33_rtc *fm_rtc;

    fm_rtc = os_calloc(1, sizeof(struct fm33_rtc));

    OS_ASSERT(fm_rtc);

    fm_rtc->info = (struct fm33_rtc_info *)dev->info;

    fm33_rtc_init(fm_rtc);

    fm_rtc->rtc.type = OS_DEVICE_TYPE_RTC;
    fm_rtc->rtc.ops  = &rtc_ops;

    return os_device_register(&fm_rtc->rtc, dev->name);
}

OS_DRIVER_INFO fm33_rtc_driver = {
    .name  = "RTC_Type",
    .probe = fm33_rtc_probe,
};

OS_DRIVER_DEFINE(fm33_rtc_driver, OS_INIT_LEVEL_PRE_DEVICE, OS_INIT_SUBLEVEL_LOW);

