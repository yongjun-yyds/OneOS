/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_iwdt.c
 *
 * @brief       This file implements watchdog driver for fm33
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include <os_memory.h>
#include <bus/bus.h>

#include "drv_iwdt.h"

#define DBG_TAG "drv.iwdt"
#include <dlog.h>

struct fm33_iwdg
{
    os_watchdog_t wdg;

    struct fm33_iwdg_info *info;
};

static void fm33_iwdg_start(struct fm33_iwdg *iwdg)
{
    LL_WDT_Init(WDT0, iwdg->info->period);
    LL_WDT_Start(WDT0);
}

static void fm33_iwdg_refresh(struct fm33_iwdg *iwdg)
{
    LL_WDT_FeedDog(WDT0);
}

static os_err_t fm33_iwdg_set_timeout(struct fm33_iwdg *iwdg, uint32_t timeout)
{
    return OS_FAILURE;
}

static os_err_t fm33_iwdg_get_timeout(struct fm33_iwdg *iwdg, uint32_t *timeout)
{
    *timeout = iwdg->info->period;
    return OS_SUCCESS;
}

static os_err_t fm33_iwdt_init(os_watchdog_t *wdt)
{
    struct fm33_iwdg *iwdg = (struct fm33_iwdg *)wdt;

    LL_CMU_SetWDTClkSrc(iwdg->info->clk_src);

    return OS_SUCCESS;
}

static os_err_t fm33_iwdt_control(os_watchdog_t *wdt, int cmd, void *arg)
{
    struct fm33_iwdg *iwdg    = (struct fm33_iwdg *)wdt;
    uint32_t       timeout = *(uint32_t *)arg;
    switch (cmd)
    {
    case OS_DEVICE_CTRL_WDT_KEEPALIVE:
        fm33_iwdg_refresh(iwdg);
        return OS_SUCCESS;

    case OS_DEVICE_CTRL_WDT_SET_TIMEOUT:
        return fm33_iwdg_set_timeout(iwdg, timeout);

    case OS_DEVICE_CTRL_WDT_GET_TIMEOUT:
        return fm33_iwdg_get_timeout(iwdg, (uint32_t *)arg);

    case OS_DEVICE_CTRL_WDT_START:
        fm33_iwdg_start(iwdg);
        LOG_I(DBG_TAG, "iwdt start.");
        return OS_SUCCESS;
    }

    return OS_NOSYS;
}

const static struct os_watchdog_ops ops = {
    .init    = &fm33_iwdt_init,
    .control = &fm33_iwdt_control,
};

static int fm33_iwdt_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    struct fm33_iwdg *iwdg;

    iwdg = os_calloc(1, sizeof(struct fm33_iwdg));

    OS_ASSERT(iwdg);

    iwdg->info    = (struct fm33_iwdg_info *)dev->info;
    iwdg->wdg.ops = &ops;

    if (os_hw_watchdog_register(&iwdg->wdg, dev->name, OS_NULL) != OS_SUCCESS)
    {
        LOG_E(DBG_TAG, "iwdt device register failed.");
        return OS_FAILURE;
    }
    LOG_D(DBG_TAG, "iwdt device register success.");
    return OS_SUCCESS;
}

OS_DRIVER_INFO fm33_iwdt_driver = {
    .name  = "IWDG_Type",
    .probe = fm33_iwdt_probe,
};

OS_DRIVER_DEFINE(fm33_iwdt_driver, OS_INIT_LEVEL_PRE_DEVICE, OS_INIT_SUBLEVEL_LOW);

