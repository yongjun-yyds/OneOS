/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_gpio.c
 *
 * @brief       This file implements gpio driver for htxx.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include "drv_gpio.h"
#include <pin/pin.h>
#include <driver.h>
#include <os_memory.h>

#define DBG_TAG "drv.gpio"
#include <dlog.h>

#ifdef OS_USING_PIN

uint32_t              gpio_port_map = 0;
static struct pin_index *indexs[GPIO_PORT_MAX];
GPIO_TypeDef            *gpio_port_base[GPIO_PORT_MAX];
static os_list_node_t    fm_gpio_irq_list = OS_LIST_INIT(fm_gpio_irq_list);

#define ITEM_NUM(items) sizeof(items) / sizeof(items[0])

#define GPIO_INFO_MAP(gpio)                                                                                            \
    {                                                                                                                  \
        gpio_port_map |= (1 << GPIO_INDEX_##gpio);                                                                     \
        gpio_port_base[GPIO_INDEX_##gpio] = GPIO##gpio;                                                                \
    }

void __os_hw_pin_init(void)
{
#if defined(GPIOA)
    GPIO_INFO_MAP(A);
#endif

#if defined(GPIOB)
    GPIO_INFO_MAP(B);
#endif

#if defined(GPIOC)
    GPIO_INFO_MAP(C);
#endif

#if defined(GPIOD)
    GPIO_INFO_MAP(D);
#endif

#if defined(GPIOE)
    GPIO_INFO_MAP(E);
#endif

#if defined(GPIOF)
    GPIO_INFO_MAP(F);
#endif

#if defined(GPIOG)
    GPIO_INFO_MAP(G);
#endif

#if defined(GPIOH)
    GPIO_INFO_MAP(H);
#endif

#if defined(GPIOI)
    GPIO_INFO_MAP(I);
#endif
#if defined(GPIOJ)
    GPIO_INFO_MAP(J);
#endif

#if defined(GPIOK)
    GPIO_INFO_MAP(K);
#endif

#if defined(GPIOZ)
    GPIO_INFO_MAP(Z);
#endif
}

/*GPIO_Pin_n*/
static struct pin_index *get_pin_index(os_base_t pin)
{
    struct pin_index *index = OS_NULL;

    if (pin < GPIO_PIN_MAX)
    {
        index = indexs[__PORT_INDEX(pin)];
    }

    return index;
}
/*GPIOx*/
GPIO_TypeDef *get_pin_base(os_base_t pin)
{
    struct pin_index *index = get_pin_index(pin);

    if (index != OS_NULL)
    {
        return index->gpio_port;
    }
    return OS_NULL;
}

static IRQn_Type fm_pin_irqn(os_base_t pin)
{
    return (IRQn_Type)((uint8_t)GPIOA_IRQn + __PORT_INDEX(pin));
}

static void fm_pin_write(struct os_device *dev, os_base_t pin, os_base_t value)
{
    const struct pin_index *index;

    index = get_pin_index(pin);
    if (index == OS_NULL)
    {
        return;
    }

    HAL_GPIO_WritePin(index->gpio_port, PIN_OFFSET(pin), (GPIO_PinState)value);
}

static int fm_pin_read(struct os_device *dev, os_base_t pin)
{
    int                     value;
    const struct pin_index *index;

    value = PIN_LOW;

    index = get_pin_index(pin);
    if (index == OS_NULL)
    {
        return value;
    }

    value = (int)HAL_GPIO_ReadPin(index->gpio_port, PIN_OFFSET(pin));

    return value;
}

static void fm_pin_mode(struct os_device *dev, os_base_t pin, os_base_t mode)
{
    struct pin_index   *index;
    os_ubase_t           level;
    LL_GPIO_InitTypeDef GPIO_InitStruct = {0};

    index = get_pin_index(pin);
    if (index == OS_NULL)
    {
        return;
    }

    HAL_GPIO_DeInit(index->gpio_port, PIN_OFFSET(pin));

    GPIO_InitStruct.Pin           = PIN_OFFSET(pin);
    GPIO_InitStruct.Alt           = LL_GPIO_ALT_GPIO;
    GPIO_InitStruct.Dir           = LL_GPIO_DIRECTION_OUT;
    GPIO_InitStruct.DriveStrength = LL_GPIO_DRIVES_STRONG;
    GPIO_InitStruct.Irq           = LL_GPIO_INTorDMA_DISABLE;
    GPIO_InitStruct.Lock          = LL_GPIO_LK_UNLOCK;
    GPIO_InitStruct.OType         = LL_GPIO_OUTPUT_NOOPENDRAIN;
    GPIO_InitStruct.Speed         = LL_GPIO_SLEWRATE_HIGH;
    GPIO_InitStruct.WECconfig     = LL_GPIO_WKUP_CLOSED;
    GPIO_InitStruct.PuPd          = LL_GPIO_PULL_NO;

    os_spin_lock_irqsave(&gs_device_lock, &level);
    index->pin_pulls[__PIN_INDEX(pin)].pull_state = GPIO_NOPULL;
    os_spin_unlock_irqrestore(&gs_device_lock, level);

    if (mode == PIN_MODE_OUTPUT)
    {
        /* output setting */
        GPIO_InitStruct.Dir = LL_GPIO_DIRECTION_OUT;
    }
    else if (mode == PIN_MODE_INPUT)
    {
        /* input setting: not pull. */
        GPIO_InitStruct.Dir  = LL_GPIO_DIRECTION_IN;
        GPIO_InitStruct.PuPd = LL_GPIO_PULL_NO;

        os_spin_lock_irqsave(&gs_device_lock, &level);
        index->pin_pulls[__PIN_INDEX(pin)].pull_state = GPIO_NOPULL;
        os_spin_unlock_irqrestore(&gs_device_lock, level);
    }
    else if (mode == PIN_MODE_INPUT_PULLUP)
    {
        /* input setting: pull up. */
        GPIO_InitStruct.Dir  = LL_GPIO_DIRECTION_IN;
        GPIO_InitStruct.PuPd = LL_GPIO_PULL_UP;

        os_spin_lock_irqsave(&gs_device_lock, &level);
        index->pin_pulls[__PIN_INDEX(pin)].pull_state = LL_GPIO_PULL_UP;
        os_spin_unlock_irqrestore(&gs_device_lock, level);
    }
    else if (mode == PIN_MODE_INPUT_PULLDOWN)
    {
        /* input setting: pull down. */
        GPIO_InitStruct.Dir  = LL_GPIO_DIRECTION_IN;
        GPIO_InitStruct.PuPd = LL_GPIO_PULL_DOWN;

        os_spin_lock_irqsave(&gs_device_lock, &level);
        index->pin_pulls[__PIN_INDEX(pin)].pull_state = LL_GPIO_PULL_DOWN;
        os_spin_unlock_irqrestore(&gs_device_lock, level);
    }
    else if (mode == PIN_MODE_OUTPUT_OD)
    {
        /* output setting: od. */
        GPIO_InitStruct.Dir   = LL_GPIO_DIRECTION_OUT;
        GPIO_InitStruct.OType = LL_GPIO_OUTPUT_OPENDRAIN;
    }

    LL_GPIO_Init(index->gpio_port, &GPIO_InitStruct);
}

static os_err_t
fm_pin_attach_irq(struct os_device *device, int32_t pin, uint32_t mode, void (*hdr)(void *args), void *args)
{
    const struct pin_index *index;
    struct pin_irq_param   *irq;
    os_ubase_t               level;

    index = get_pin_index(pin);
    if (index == OS_NULL)
    {
        return OS_NOSYS;
    }
    os_spin_lock_irqsave(&gs_device_lock, &level);
    os_list_for_each_entry(irq, &fm_gpio_irq_list, struct pin_irq_param, list)
    {
        if (irq->pin == pin)
        {
            if (irq->hdr == hdr && irq->mode == mode && irq->args == args)
            {
                os_spin_unlock_irqrestore(&gs_device_lock, level);
                return OS_SUCCESS;
            }
            else
            {
                os_spin_unlock_irqrestore(&gs_device_lock, level);
                return OS_BUSY;
            }
        }
    }
    os_spin_unlock_irqrestore(&gs_device_lock, level);
    irq = os_calloc(1, sizeof(struct pin_irq_param));
    OS_ASSERT(irq != OS_NULL);

    irq->pin  = pin;
    irq->hdr  = hdr;
    irq->mode = mode;
    irq->args = args;

    os_list_add_tail(&fm_gpio_irq_list, &irq->list);

    return OS_SUCCESS;
}

static os_err_t fm_pin_dettach_irq(struct os_device *device, int32_t pin)
{
    const struct pin_index *index;
    os_ubase_t               level;
    struct pin_irq_param   *irq;

    index = get_pin_index(pin);
    if (index == OS_NULL)
    {
        return OS_NOSYS;
    }

    os_spin_lock_irqsave(&gs_device_lock, &level);
    os_list_for_each_entry(irq, &fm_gpio_irq_list, struct pin_irq_param, list)
    {
        if (irq->pin == pin)
        {
            os_list_del(&irq->list);
            os_spin_unlock_irqrestore(&gs_device_lock, level);
            return OS_SUCCESS;
        }
    }
    os_spin_unlock_irqrestore(&gs_device_lock, level);
    return OS_EMPTY;
}

static os_err_t fm_pin_irq_enable(struct os_device *device, os_base_t pin, uint32_t enabled)
{
    const struct pin_index *index;
    os_ubase_t               level;
    struct pin_irq_param   *irq;

    index = get_pin_index(pin);
    if (index == OS_NULL)
    {
        return OS_NOSYS;
    }

    if (enabled == PIN_IRQ_ENABLE)
    {
        os_spin_lock_irqsave(&gs_device_lock, &level);

        os_list_for_each_entry(irq, &fm_gpio_irq_list, struct pin_irq_param, list)
        {
            if (irq->pin == pin)
            {
                if (irq->irq_ref > 0)
                {
                    os_spin_unlock_irqrestore(&gs_device_lock, level);
                    return OS_SUCCESS;
                }
                switch (irq->mode)
                {
                case PIN_IRQ_MODE_RISING:
                    LL_GPIO_SetPinINT(index->gpio_port, PIN_OFFSET(pin), LL_GPIO_INTonRISE);
                    break;
                case PIN_IRQ_MODE_FALLING:
                    LL_GPIO_SetPinINT(index->gpio_port, PIN_OFFSET(pin), LL_GPIO_INTonFALL);
                    break;
                case PIN_IRQ_MODE_HIGH_LEVEL:
                    LL_GPIO_SetPinINT(index->gpio_port, PIN_OFFSET(pin), LL_GPIO_INTonHIGH);
                    break;
                case PIN_IRQ_MODE_LOW_LEVEL:
                    LL_GPIO_SetPinINT(index->gpio_port, PIN_OFFSET(pin), LL_GPIO_INTonZERO);
                    break;
                case PIN_IRQ_MODE_RISING_FALLING:
                    LL_GPIO_SetPinINT(index->gpio_port, PIN_OFFSET(pin), LL_GPIO_INTonBOTHEDGE);
                    break;
                default:
                    os_spin_unlock_irqrestore(&gs_device_lock, level);
                    return OS_NOSYS;
                }
                irq->irq_ref++;
                NVIC_EnableIRQ(fm_pin_irqn(pin));
                os_spin_unlock_irqrestore(&gs_device_lock, level);
            }
        }
    }
    else if (enabled == PIN_IRQ_DISABLE)
    {
        os_spin_lock_irqsave(&gs_device_lock, &level);
        os_list_for_each_entry(irq, &fm_gpio_irq_list, struct pin_irq_param, list)
        {
            if (irq->pin == pin)
            {
                if (irq->irq_ref == 0)
                {
                    os_spin_unlock_irqrestore(&gs_device_lock, level);
                    return OS_SUCCESS;
                }
                LL_GPIO_SetPinINT(index->gpio_port, PIN_OFFSET(pin), LL_GPIO_INTorDMA_DISABLE);

                irq->irq_ref--;
                os_spin_unlock_irqrestore(&gs_device_lock, level);
            }
        }
    }
    else
    {
        return OS_NOSYS;
    }
    return OS_SUCCESS;
}
const static struct os_pin_ops _fm_pin_ops = {
    fm_pin_mode,
    fm_pin_write,
    fm_pin_read,
    fm_pin_attach_irq,
    fm_pin_dettach_irq,
    fm_pin_irq_enable,
};

OS_INLINE void pin_irq_hdr(void)
{
    struct pin_index     *index;
    struct pin_irq_param *irq;

    os_list_for_each_entry(irq, &fm_gpio_irq_list, struct pin_irq_param, list)
    {
        index = get_pin_index(irq->pin);

        if (LL_GPIO_GetFlagPin_IT(index->gpio_port, PIN_OFFSET(irq->pin)))
        {
            LL_GPIO_ClearFlagPin_IT(index->gpio_port, PIN_OFFSET(irq->pin));
            if (index->handler_mask == OS_TRUE)
            {
                index->handler_mask = OS_FALSE;
#if 0
                return;
#endif
            }
            if (irq->hdr != OS_NULL)
            {
                irq->hdr(irq->args);
            }
        }
    }
}

#if defined(GPIOA)
void GPIOA_Handler(void)
{
    pin_irq_hdr();
}
#endif

#if defined(GPIOB)
void GPIOB_Handler(void)
{
    pin_irq_hdr();
}
#endif

#if defined(GPIOC)
void GPIOC_Handler(void)
{
    pin_irq_hdr();
}
#endif

#if defined(GPIOD)
void GPIOD_Handler(void)
{
    pin_irq_hdr();
}
#endif

#if defined(GPIOE)
void GPIOE_Handler(void)
{
    pin_irq_hdr();
}
#endif

#if defined(GPIOF)
void GPIOF_Handler(void)
{
    pin_irq_hdr();
}
#endif

/**
 ***********************************************************************************************************************
 * @brief           os_hw_pin_init:enable gpio clk,register pin device.
 *
 * @param[in]       none
 *
 * @return          Return init result.
 * @retval          OS_SUCCESS       init success.
 * @retval          Others       init failed.
 ***********************************************************************************************************************
 */
int os_hw_pin_init(void)
{
    struct pin_index *tmp       = OS_NULL;
    uint8_t        gpio_port = 0;
    uint8_t        gpio_pin  = 0;

    __os_hw_pin_init();

    for (gpio_port = 0; gpio_port < GPIO_PORT_MAX; gpio_port++)
    {
        if ((gpio_port_map & (1 << gpio_port)))
        {
            tmp = (struct pin_index *)os_malloc(sizeof(struct pin_index));
            if (tmp == OS_NULL)
            {
                os_kprintf(DBG_TAG, "os_malloc error!!!");
                return OS_NOMEM;
            }

            tmp->gpio_port = gpio_port_base[gpio_port];
            /*Handler mask is a circumvention measure used to mask false interrupt
            that is generated after the interrupt is enabled*/
            tmp->handler_mask = OS_TRUE;

            for (gpio_pin = 0; gpio_pin < GPIO_PIN_PER_PORT; gpio_pin++)
            {
                tmp->pin_pulls[gpio_pin].pull_state = 0;
            }

            indexs[gpio_port] = tmp;
        }
        else
        {
            indexs[gpio_port] = OS_NULL;
        }
    }

    return os_device_pin_register(0, &_fm_pin_ops, OS_NULL);
}

#endif /* OS_USING_PIN */
