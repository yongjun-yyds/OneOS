/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\ you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an \"AS IS\" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * \@file        drv_uart.c
 *
 * \@brief       This file implements uart driver for FM33A0xx.
 *
 * \@revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include "drv_uart.h"

static os_list_node_t fm33_uart_list = OS_LIST_INIT(fm33_uart_list);

struct fm33_uart
{
    struct os_serial_device serial;
    struct fm33_usart_info *info;

    soft_dma_t sdma;

    uint8_t *rx_buff;
    uint32_t rx_size;
    uint32_t rx_index;

    const uint8_t *tx_buff;
    uint32_t       tx_size;
    uint32_t       tx_index;

    DMA_HandleTypeDef dma_handle;

    os_list_node_t list;
};

static void uart_isr(struct os_serial_device *serial)
{
    struct fm33_uart *uart = os_container_of(serial, struct fm33_uart, serial);

    if (LL_UART_GetFlag(uart->info->husart, UART_STATUS_RXFIFONE) != RESET)
    {
        uint8_t temp_cnt = 0;
        uint8_t i;

        temp_cnt = LL_UART_GetLengthRxFifo(uart->info->husart);

        if (temp_cnt > 0)
        {
            for (i = 0; i < temp_cnt; i++)
            {
                uart->rx_buff[uart->rx_index++] = LL_UART_ReceiveData8(uart->info->husart);
            }
        }

        LL_UART_ClearFlag(uart->info->husart, UART_STATUS_RXFIFONE);

        if (uart->rx_index == (uart->rx_size / 2))
        {
            soft_dma_half_irq(&uart->sdma);
        }

        if (uart->rx_index == uart->rx_size)
        {
            uart->rx_index = 0;
            soft_dma_full_irq(&uart->sdma);
        }
    }

    if (LL_UART_GetFlag(uart->info->husart, LL_UART_FLAG_TXFIFOE) != RESET)
    {
        if (uart->tx_size)
        {
            if (uart->tx_index < uart->tx_size)
            {
                LL_UART_TransmitData8(uart->info->husart, uart->tx_buff[uart->tx_index++]);
            }
        }
    }

    if (LL_UART_GetFlag(uart->info->husart, LL_UART_FLAG_TXDONE) != RESET)
    {
        LL_UART_ClearFlag(uart->info->husart, LL_UART_FLAG_TXFIFOE | LL_UART_FLAG_TXDONE);
        LL_UART_DisableINT(uart->info->husart, LL_UART_TXFEIE | LL_UART_TXDONEIE);

        if ((uart->tx_size > 0) && (uart->tx_index >= uart->tx_size))
        {
            os_hw_serial_isr_txdone(serial);
        }
    }
}

static void usart_rx_irq_callback(uint32_t usart_index)
{
    struct fm33_uart *uart;

    os_list_for_each_entry(uart, &fm33_uart_list, struct fm33_uart, list)
    {
        if (uart->info->index == usart_index)
        {
            uart_isr(&uart->serial);
            break;
        }
    }
}

void LL_DMA_XferHalfCpltCallback(uint32_t Channel)
{
    struct fm33_uart *uart = OS_NULL;

    os_list_for_each_entry(uart, &fm33_uart_list, struct fm33_uart, list)
    {
        if (Channel == uart->info->hdma_rx_channel)
        {
            soft_dma_half_irq(&uart->sdma);
        }
    }
}

void LL_DMA_XferCpltCallback(uint32_t Channel)
{
    LL_DMA_XferHalfCpltCallback(Channel);
}

void LL_DMA_ErrorCallback(uint32_t Error)
{
    os_kprintf("LL_DMA_ErrorCallback\r\n");
}

#if defined(BSP_USING_UART0)
void UART0_Handler(void)
{
    usart_rx_irq_callback(0);
}
#endif /* BSP_USING_UART0 */

#if defined(BSP_USING_UART1)
void UART1_Handler(void)
{
    usart_rx_irq_callback(1);
}
#endif /* BSP_USING_UART1 */

#if defined(BSP_USING_UART2)
void UART2_Handler(void)
{
    usart_rx_irq_callback(2);
}
#endif /* BSP_USING_UART2 */

static uint32_t fm33_sdma_int_get_index(soft_dma_t *dma)
{
    struct fm33_uart *uart = os_container_of(dma, struct fm33_uart, sdma);

    return uart->rx_index;
}

static os_err_t fm33_sdma_int_start(soft_dma_t *dma, void *buff, uint32_t size)
{
    struct fm33_uart *uart = os_container_of(dma, struct fm33_uart, sdma);

    uart->rx_buff  = buff;
    uart->rx_size  = size;
    uart->rx_index = 0;

    LL_UART_ClearFlag(uart->info->husart, UART_STATUS_RXFIFONE);
    LL_UART_EnableINT(uart->info->husart, LL_UART_RXNEIE);

    return OS_SUCCESS;
}

static uint32_t fm33_sdma_int_stop(soft_dma_t *dma)
{
    struct fm33_uart *uart = os_container_of(dma, struct fm33_uart, sdma);

    uart->rx_buff = OS_NULL;

    LL_UART_DisableINT(uart->info->husart, LL_UART_RXNEIE);
    LL_UART_ClearFlag(uart->info->husart, UART_STATUS_RXFIFONE);

    return uart->rx_index;
}

static uint32_t fm33_sdma_dma_get_index(soft_dma_t *dma)
{
    struct fm33_uart *uart  = os_container_of(dma, struct fm33_uart, sdma);
    uint32_t       index = uart->rx_size - LL_DMA_GetMajorLoopCiter(uart->info->hdma_rx_channel);

    return index;
}

static void hal_uart_rx_dma_init(soft_dma_t *dma)
{
    Dma_TcdAttr_t         TcdAttr;
    DmaTcdMinorLoop_t     TcdMinorLoop;
    DmaTcdMajorLoop_t     TcdMajorLoop;
    LL_DMA_TransferConfig config;
    uint32_t              trigSrc, priority, channel, bytesEachRequest, transferBytes;

    struct fm33_uart *uart = os_container_of(dma, struct fm33_uart, sdma);

    bytesEachRequest = 1;
    transferBytes    = uart->rx_size;

    channel  = uart->info->hdma_rx_channel;
    priority = LL_DMA_PRIORITY_FIX_0;
    trigSrc  = uart->info->hdma_trig_src;

    TcdAttr.SrcTransferWidth  = DMA_TRANSFERWIDTH_8BIT;
    TcdAttr.DestTransferWidth = DMA_TRANSFERWIDTH_8BIT;
    TcdAttr.SrcOffset         = 0;
    TcdAttr.DestOffset        = 0;
    TcdAttr.SrcTransferBurst  = DMA_TRANSFERBURST_SINGLE;
    TcdAttr.DestTransferBurst = DMA_TRANSFERBURST_SINGLE;
    TcdAttr.Smod              = DMA_MODULO_DISABLE;
    TcdAttr.Dmod              = DMA_MODULO_DISABLE;

    TcdMinorLoop.MinorLoopBytes = bytesEachRequest;
    TcdMinorLoop.SmolEn         = DISABLE;
    TcdMinorLoop.DmolEn         = ENABLE;
    TcdMinorLoop.Moff           = 1;

    TcdMajorLoop.MajorLoopCounts = transferBytes / bytesEachRequest;
    TcdMajorLoop.SLastSGA        = 0;
    TcdMajorLoop.DLastSGA        = 0;

    TcdMinorLoop.MinorLinkEn = DISABLE;
    TcdMinorLoop.MinorLinkch = 0xff;

    TcdMajorLoop.MajorLinkEn  = DISABLE;
    TcdMajorLoop.MajorLinkch  = 0xff;
    TcdMajorLoop.TcdScatterEn = DISABLE;

    config.TcdAttr      = TcdAttr;
    config.TcdMinorLoop = TcdMinorLoop;
    config.TcdMajorLoop = TcdMajorLoop;

    /* Clear channel Reg */
    DMA->CR &= ~(DMA_CR_HOE | DMA_CR_HALT | DMA_CR_CLM);
    DMA->ERQ &= ~(0x1 << channel);
    DMA->EEI &= ~(0x1 << channel);
    DMA->INT &= ~(0x1 << channel);
    DMA->ERR &= ~(0x1 << channel);
    DMA->EARS &= ~(0x1 << channel);
    DMA->DONE &= ~(0x1 << channel);
    if (0 != TcdMinorLoop.Moff)
        DMA->CR |= BIT7;
    DMA_SetTransferConfig(channel, &config, (dma_tcd_t *)0);
    DMA_SetTransferConfig(channel, &config, (dma_tcd_t *)0);

    DMA->CR |= DMA_CR_ERCA;
    DMA->CHPRI |= priority << (channel * 4);

    DMAMUX->CHCFG[channel] = trigSrc;

    LL_UART_EnableINT(uart->info->husart, LL_UART_RXFNEDE | LL_UART_RXFHFIE);
}

static os_err_t fm33_sdma_dma_init(soft_dma_t *dma)
{
    struct fm33_uart *uart = os_container_of(dma, struct fm33_uart, sdma);

    hal_uart_rx_dma_init(dma);

    NVIC_EnableIRQ(uart->info->hdma_irqn);
    NVIC_EnableIRQ(DMA_Error_IRQn);

    return OS_SUCCESS;
}

static os_err_t fm33_sdma_dma_start(soft_dma_t *dma, void *buff, uint32_t size)
{
    struct fm33_uart *uart    = os_container_of(dma, struct fm33_uart, sdma);
    uint32_t       Channel = uart->info->hdma_rx_channel;

    uart->rx_buff = buff;
    uart->rx_size = size;

    DMA->TCD[Channel].SADDR = (uint32_t)&uart->info->husart->RXFIFO;
    DMA->TCD[Channel].DADDR = (uint32_t)uart->rx_buff;
    DMA->TCD[Channel].BITER_CITER_ELINKNO =
        (uart->rx_size & DMA_WORD5_BITER_Msk) | ((uart->rx_size << DMA_WORD5_CITER_Pos) & DMA_WORD5_CITER_Msk);
    LL_DMA_ENABLE_IT(Channel, LL_DMA_IT_MAJORLOOP_HT & LL_DMA_IT_MAJORLOOP_MASK);
    DMA->ERQ |= (0x1 << Channel);
    LL_DMA_MUX_ENABLE(Channel);

    return OS_SUCCESS;
}
static uint32_t fm33_sdma_dma_stop(soft_dma_t *dma)
{
    struct fm33_uart *uart = os_container_of(dma, struct fm33_uart, sdma);

    LL_DMA_MUX_DISABLE(uart->info->hdma_rx_channel);

    return (uart->rx_size - LL_DMA_GetMajorLoopCiter(uart->info->hdma_rx_channel));
}

static void fm33_usart_sdma_callback(soft_dma_t *dma)
{
    struct fm33_uart *uart = os_container_of(dma, struct fm33_uart, sdma);

    os_hw_serial_isr_rxdone((struct os_serial_device *)uart);
}

static void fm33_usart_sdma_init(struct fm33_uart *uart, dma_ring_t *ring)
{
    soft_dma_t *dma = &uart->sdma;

    soft_dma_stop(dma);

    memset(&dma->hard_info, 0, sizeof(dma->hard_info));

    dma->hard_info.max_size     = 64 * 1024;
    dma->hard_info.flag         = HARD_DMA_FLAG_HALF_IRQ | HARD_DMA_FLAG_FULL_IRQ;
    dma->hard_info.data_timeout = uart_calc_byte_timeout_us(uart->serial.config.baud_rate);

    if (uart->info->hdma_enable == OS_FALSE)
    {
        dma->hard_info.mode = HARD_DMA_MODE_NORMAL;

        dma->ops.get_index = fm33_sdma_int_get_index;
        dma->ops.dma_init  = OS_NULL;
        dma->ops.dma_start = fm33_sdma_int_start;
        dma->ops.dma_stop  = fm33_sdma_int_stop;
    }
    else
    {
        dma->hard_info.mode = HARD_DMA_MODE_NORMAL;

        dma->ops.get_index = fm33_sdma_dma_get_index;
        dma->ops.dma_init  = fm33_sdma_dma_init;
        dma->ops.dma_start = fm33_sdma_dma_start;
        dma->ops.dma_stop  = fm33_sdma_dma_stop;
    }

    dma->cbs.dma_half_callback    = fm33_usart_sdma_callback;
    dma->cbs.dma_full_callback    = fm33_usart_sdma_callback;
    dma->cbs.dma_timeout_callback = fm33_usart_sdma_callback;

    soft_dma_init(dma);
    soft_dma_start(dma, ring);
    soft_dma_irq_enable(&uart->sdma, OS_TRUE);
}

static uint32_t LL_Get_UART_Clk(void)
{
    uint32_t clksource = 0;
    uint32_t clkdiv    = 0;
    clksource          = LL_CMU_GetUARTClkSrc();
    clkdiv             = (READ_BIT(CMU->MDLCLKA, CMU_MDLCLKA_UARTDIV)) >> CMU_MDLCLKA_UARTDIV_Pos;

    if (clksource == LL_CMU_UARTCLK_SYSCLK)
        clksource = LL_CMU_GetSysClock() / (LL_CMU_GetSysClkPrescaler() + 1);
    else if (clksource == LL_CMU_UARTCLK_IOCLK)
    {
#ifdef IO_Clock
        clksource = IO_Clock;
#endif
    }
    else if (clksource == LL_CMU_UARTCLK_IRC16M)
    {
        clksource = 16000000;
    }
    else if (clksource == LL_CMU_UARTCLK_OSCCLK)
    {
#ifdef OSC_Clock
        clksource = OSC_Clock;
#endif
    }
    else
        clksource = 16000000;

    return (clksource >> clkdiv);
}

static os_err_t fm33_uart_init(struct os_serial_device *serial, struct serial_configure *cfg)
{
    struct fm33_uart   *uart            = OS_NULL;
    uint32_t         uart_clk        = 0;
    LL_UART_InitTypeDef UART_InitStruct = {0};

    OS_ASSERT(serial != OS_NULL);
    OS_ASSERT(cfg != OS_NULL);

    uart = os_container_of(serial, struct fm33_uart, serial);
    OS_ASSERT(uart != OS_NULL);

    LL_GPIO_SetPinMux(uart->info->port, uart->info->tx_pin, LL_GPIO_ALT_3);
    LL_GPIO_SetPinMux(uart->info->port, uart->info->rx_pin, LL_GPIO_ALT_3);
    LL_GPIO_SetPinPull(uart->info->port, uart->info->rx_pin, LL_GPIO_PULL_UP);

    LL_UART_DeInit(uart->info->husart);

    LL_CMU_SetUARTClkSrc(uart->info->clk_src);
    LL_CMU_SetUARTClkPrescaler(uart->info->prescaler);

    UART_InitStruct.BaudRate          = cfg->baud_rate;
    UART_InitStruct.SampleFactor      = LL_UART_SAMPLEFACTOR_4;
    UART_InitStruct.TransferDirection = LL_UART_DIRECTION_TX_RX;

    switch (cfg->data_bits)
    {
    case DATA_BITS_8:
        if (cfg->parity == PARITY_NONE)
        {
            UART_InitStruct.Pdsel = LL_UART_PDSEL_N_8;
        }
        else if (cfg->parity == PARITY_ODD)
        {
            UART_InitStruct.Pdsel = LL_UART_PDSEL_O_8;
        }
        else if (cfg->parity == PARITY_EVEN)
        {
            UART_InitStruct.Pdsel = LL_UART_PDSEL_E_8;
        }
        else
        {
            return OS_INVAL;
        }
        break;
    case DATA_BITS_9:
        if (cfg->parity == PARITY_NONE)
        {
            UART_InitStruct.Pdsel = LL_UART_PDSEL_N_9;
        }
        else
        {
            return OS_INVAL;
        }
        break;
    default:
        return OS_INVAL;
    }

    switch (cfg->stop_bits)
    {
    case STOP_BITS_1:
        UART_InitStruct.StopBits = LL_UART_STOPSEL_1BIT;
        break;
    case STOP_BITS_2:
        UART_InitStruct.StopBits = LL_UART_STOPSEL_2BIT;
        break;
    default:
        return OS_INVAL;
    }

    uart_clk = LL_Get_UART_Clk();
    LL_UART_Init(uart->info->husart, UART_InitStruct, uart_clk);

    LL_UART_DisableAllINT(uart->info->husart);
    LL_UART_EnableINT(uart->info->husart, LL_UART_RXNEIE);
    LL_UART_ClearFlag(uart->info->husart,
                      LL_UART_FLAG_TXFIFOERR | LL_UART_FLAG_RXFIFOERR | LL_UART_FLAG_FERR | LL_UART_FLAG_PERR);

    NVIC_EnableIRQ(uart->info->irqn);

    fm33_usart_sdma_init(uart, &serial->rx_fifo->ring);

    return OS_SUCCESS;
}

static os_err_t fm33_uart_deinit(struct os_serial_device *serial)
{
    OS_ASSERT(serial != OS_NULL);

    struct fm33_uart *uart = os_container_of(serial, struct fm33_uart, serial);

    if (uart->info->hdma_enable != OS_FALSE)
    {
        soft_dma_stop(&uart->sdma);
    }

    return OS_SUCCESS;
}

static int fm33_uart_start_send(struct os_serial_device *serial, const uint8_t *buff, os_size_t size)
{
    struct fm33_uart *uart = os_container_of(serial, struct fm33_uart, serial);

    OS_ASSERT(serial != OS_NULL);

    uart->tx_buff  = buff;
    uart->tx_size  = size;
    uart->tx_index = 0;

    LL_UART_ClearFlag(uart->info->husart, LL_UART_FLAG_TXFIFOE | LL_UART_FLAG_TXDONE);
    LL_UART_EnableINT(uart->info->husart, LL_UART_TXFEIE | LL_UART_TXDONEIE);

    LL_UART_TransmitData8(uart->info->husart, uart->tx_buff[uart->tx_index++]);

    return size;
}

/* clang-format off */
static int fm33_uart_poll_send(struct os_serial_device *serial, const uint8_t *buff, os_size_t size)
{
    struct fm33_uart *uart = os_container_of(serial, struct fm33_uart, serial);

    int       i;
    os_ubase_t level;

    OS_ASSERT(serial != OS_NULL);

    for (i = 0; i < size; i++)
    {
        os_spin_lock_irqsave(&gs_device_lock, &level);
        LL_UART_TransmitData8(uart->info->husart, buff[i]);
        while (!LL_UART_GetFlag(uart->info->husart, UART_STATUS_TXDONE));
        os_spin_unlock_irqrestore(&gs_device_lock, level);
    }

    return size;
}
/* clang-format on */

static const struct os_uart_ops fm33_uart_ops = {
    .init   = fm33_uart_init,
    .deinit = fm33_uart_deinit,

    .start_send = fm33_uart_start_send,
    .poll_send  = fm33_uart_poll_send,
};

static int fm33_usart_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    os_ubase_t level;

    struct serial_configure config = OS_SERIAL_CONFIG_DEFAULT;

    struct fm33_uart *usart;

    usart = os_calloc(1, sizeof(struct fm33_uart));
    OS_ASSERT(usart);

    usart->info                     = (struct fm33_usart_info *)dev->info;
    struct os_serial_device *serial = &usart->serial;

    os_spin_lock_irqsave(&gs_device_lock, &level);
    os_list_add_tail(&fm33_uart_list, &usart->list);
    os_spin_unlock_irqrestore(&gs_device_lock, level);

    serial->ops    = &fm33_uart_ops;
    serial->config = config;

    /* register uart device */
    os_hw_serial_register(serial, dev->name, usart);

    return 0;
}

OS_DRIVER_INFO fm33_usart_driver = {
    .name  = "Usart_Type",
    .probe = fm33_usart_probe,
};

OS_DRIVER_DEFINE(fm33_usart_driver, OS_INIT_LEVEL_PRE_DEVICE, OS_INIT_SUBLEVEL_HIGH);

