/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_hwtimer.c
 *
 * @brief       This file implements timer driver for fm33.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <board.h>
#include <os_memory.h>
#include <timer/timer.h>

#ifdef OS_USING_CLOCKSOURCE
#include <timer/clocksource.h>
#endif

#ifdef OS_USING_CLOCKEVENT
#include <timer/clockevent.h>
#endif

#include <drv_hwtimer.h>

#ifdef OS_USING_PWM
#include "drv_pwm.h"
#endif

#ifdef OS_USING_PULSE_ENCODER
#include "drv_pulse_encoder.h"
#endif

#define DRV_EXT_LVL DBG_EXT_INFO
#define DRV_EXT_TAG "drv.hwtimer "
#include <drv_log.h>

static os_list_node_t fm33_timer_list = OS_LIST_INIT(fm33_timer_list);

static void fm33_timer_update_callback(STn_Type htim)
{
    struct fm33_timer *timer;

    os_list_for_each_entry(timer, &fm33_timer_list, struct fm33_timer, list)
    {
        if (timer->info->instance == htim && timer->clock.ce.parent.type == OS_DEVICE_TYPE_CLOCKEVENT)
        {
#ifdef OS_USING_CLOCKEVENT
            if (timer->info->mode == TIMER_MODE_TIM)
            {
                os_clockevent_isr((os_clockevent_t *)timer);
            }
#endif
        }
    }
}

/*timer irq handlers*/
#ifdef BSP_USING_STIM0
void STIMER0_Handler(void)
{
    if (LL_STIM_IsActiveFlag(ST0))
    {
        LL_STIM_ClearFlag(ST0);
        fm33_timer_update_callback(ST0);
    }
}
#endif

#ifdef BSP_USING_STIM1
void STIMER1_Handler(void)
{
    if (LL_STIM_IsActiveFlag(ST1))
    {
        LL_STIM_ClearFlag(ST1);
        fm33_timer_update_callback(ST1);
    }
}
#endif

#ifdef BSP_USING_STIM2
void STIMER2_Handler(void)
{
    if (LL_STIM_IsActiveFlag(ST2))
    {
        LL_STIM_ClearFlag(ST2);
        fm33_timer_update_callback(ST2);
    }
}
#endif

#ifdef BSP_USING_STIM3
void STIMER3_Handler(void)
{
    if (LL_STIM_IsActiveFlag(ST3))
    {
        LL_STIM_ClearFlag(ST3);
        fm33_timer_update_callback(ST3);
    }
}
#endif

#ifdef BSP_USING_STIM4
void STIMER4_Handler(void)
{
    if (LL_STIM_IsActiveFlag(ST4))
    {
        LL_STIM_ClearFlag(ST4);
        fm33_timer_update_callback(ST4);
    }
}
#endif

#ifdef BSP_USING_STIM5
void STIMER5_Handler(void)
{
    if (LL_STIM_IsActiveFlag(ST5))
    {
        LL_STIM_ClearFlag(ST5);
        fm33_timer_update_callback(ST5);
    }
}
#endif

static uint32_t fm33_timer_get_freq(struct fm33_timer *timer)
{
    uint32_t freq = 0;
    uint32_t clk_src;
    uint32_t clk_div = (1U << timer->info->prescaler);

    OS_ASSERT(timer != OS_NULL);

    switch (timer->info->type)
    {
#if defined(BSP_USING_STIM)
    case TYPE_STIM:
        clk_src = timer->info->clk_src;

        if (clk_src == LL_CMU_STCLK_IRC16M)
        {
            freq = 16000000 / clk_div;
        }
        else if (clk_src == LL_CMU_STCLK_IRC4M)
        {
            freq = 4000000 / clk_div;
        }
        else if (clk_src == LL_CMU_STCLK_OSCCLK)
        {
            freq = 12000000 / clk_div;
        }
        else
        {
            os_kprintf("unknown timer clk source.\r\n");
            OS_ASSERT(OS_FALSE);
        }
        break;
#endif
    default:
        break;
    }

    return freq;
}

static void _timer_start(struct fm33_timer *timer, uint32_t prescaler, uint64_t count, os_bool_t irq_enable)
{
    OS_ASSERT(count != 0);
    OS_ASSERT(timer != NULL);

    STn_Type tim = timer->info->instance;

    switch (timer->info->type)
    {
#if defined(BSP_USING_STIM)
    case TYPE_STIM:
    {
        LL_CMU_SetStimerClkSrc(tim, timer->info->clk_src);
        LL_STIM_Disable(tim);
        LL_STIM_Reset(tim);
        LL_STIM_SetPrescaler(tim, timer->info->prescaler);
        LL_STIM_SetRunMode(tim, LL_TIM_MODE_MODCNT);
        LL_STIM_DisableOneshot(tim);
        LL_STIM_SetMod0Value(tim, count);

        if (irq_enable)
        {
            LL_STIM_EnableINT(tim);
            NVIC_EnableIRQ(timer->info->irqn);
        }

        LL_STIM_Enable(tim);
    }
    break;
#endif
    default:
        break;
    }
}

static uint64_t fm33_timer_read(void *clock)
{
    struct fm33_timer *timer         = OS_NULL;
    uint64_t        timer_counter = 0;

    timer = (struct fm33_timer *)clock;
    OS_ASSERT(timer != OS_NULL);

    switch (timer->info->type)
    {
#if defined(BSP_USING_STIM)
    case TYPE_STIM:
        timer_counter = LL_STIM_GetCurrentValue(timer->info->instance);
        break;
#endif
    default:
        break;
    }

    return timer_counter;
}

#ifdef OS_USING_CLOCKEVENT
static void fm33_timer_start(os_clockevent_t *ce, uint32_t prescaler, uint64_t count)
{
    struct fm33_timer *timer = OS_NULL;

    OS_ASSERT(count != 0);

    timer = (struct fm33_timer *)ce;
    OS_ASSERT(timer != NULL);

    OS_ASSERT(prescaler == 1);

    _timer_start(timer, prescaler, count, OS_TRUE);
}

static void fm33_timer_stop(os_clockevent_t *ce)
{
    struct fm33_timer *timer = OS_NULL;

    OS_ASSERT(ce != OS_NULL);

    timer = (struct fm33_timer *)ce;
    OS_ASSERT(timer != OS_NULL);

    switch (timer->info->type)
    {
#if defined(BSP_USING_STIM)
    case TYPE_STIM:
        LL_STIM_Disable(timer->info->instance);
        LL_STIM_ClearFlag(timer->info->instance);
        break;
#endif
    default:
        break;
    }
}

static const struct os_clockevent_ops fm33_tim_ops = {
    .start = fm33_timer_start,
    .stop  = fm33_timer_stop,
    .read  = fm33_timer_read,
};
#endif

/**
 ***********************************************************************************************************************
 * @brief           fm33_tim_probe:probe timer device.
 *
 * @param[in]       none
 *
 * @return          Return timer probe status.
 * @retval          OS_SUCCESS         timer register success.
 * @retval          OS_FAILURE       timer register failed.
 ***********************************************************************************************************************
 */
static int fm33_tim_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    struct fm33_timer *timer = OS_NULL;

    timer = os_calloc(1, sizeof(struct fm33_timer));
    OS_ASSERT(timer);

    timer->info = (struct fm33_timer_info *)dev->info;

    timer->freq = fm33_timer_get_freq(timer);

#ifdef OS_USING_PWM
    if (timer->info->mode == TIMER_MODE_PWM)
    {
        fm33_pwm_register(dev->name, timer);
    }
#endif

#ifdef OS_USING_PULSE_ENCODER
    if (timer->info->mode == TIMER_MODE_PULSE_ENCODER)
    {
        fm33_pulse_encoder_register(dev->name, timer);
    }
#endif

    if (timer->info->mode == TIMER_MODE_TIM)
    {
#ifdef OS_USING_CLOCKSOURCE
        if (os_clocksource_best() == OS_NULL && timer->info->bits == 32)
        {
            _timer_start(timer, 1, 0xffffffffull, OS_FALSE);

            timer->clock.cs.rating = 320;
            timer->clock.cs.freq   = timer->freq;
            timer->clock.cs.mask   = 0xffffffffull;
            timer->clock.cs.read   = fm33_timer_read;

            os_clocksource_register(dev->name, &timer->clock.cs);
        }
        else
#endif
        {
#ifdef OS_USING_CLOCKEVENT
            timer->clock.ce.rating = (timer->info->bits == 32) ? 320 : 160;
            timer->clock.ce.freq   = timer->freq;
            timer->clock.ce.mask   = 0xffffffffull;

            timer->clock.ce.prescaler_mask = 0xfffful;
            timer->clock.ce.prescaler_bits = 0;

            timer->clock.ce.count_mask = (timer->info->bits == 32) ? 0xfffffffful : 0xfffful;
            timer->clock.ce.count_bits = timer->info->bits;

            timer->clock.ce.feature = OS_CLOCKEVENT_FEATURE_PERIOD;

            timer->clock.ce.min_nsec = NSEC_PER_SEC / timer->clock.ce.freq;

            timer->clock.ce.ops = &fm33_tim_ops;

            os_clockevent_register(dev->name, &timer->clock.ce);
#endif
        }
    }

    os_list_add(&fm33_timer_list, &timer->list);

    return OS_SUCCESS;
}

OS_DRIVER_INFO fm33_tim_driver = {
    .name  = "TIM_Type",
    .probe = fm33_tim_probe,
};

OS_DRIVER_DEFINE(fm33_tim_driver, OS_INIT_LEVEL_PRE_DEVICE, OS_INIT_SUBLEVEL_MIDDLE);

