/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_gpio.c
 *
 * @brief       This file implements gpio driver for stm32.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <os_memory.h>
#include "drv_gpio.h"
#include <os_stddef.h>
#ifdef OS_USING_PIN

#define DBG_TAG "drv_gpio"

uint64_t gpio_port_map = 0;

static struct pin_index *indexs[GPIO_PORT_MAX];
uint32_t gpio_port_base[GPIO_PORT_MAX];
uint32_t gpio_irq_con[GPIO_PORT_MAX];
uint32_t gpio_irq_mask[GPIO_PORT_MAX];
uint32_t gpio_irq_pend[GPIO_PORT_MAX];

static os_list_node_t exynos_gpio_irq_list = OS_LIST_INIT(exynos_gpio_irq_list);

#define ITEM_NUM(items) sizeof(items) / sizeof(items[0])

#define GPIO_INFO_MAP(gpio)                                                                                               \
{                                                                                                                         \
    gpio_port_map |= (1ULL << GPIO_INDEX_##gpio);                                                                            \
    gpio_port_base[GPIO_INDEX_##gpio] = GpioPort##gpio;                                                                   \
    gpio_irq_con[GPIO_INDEX_##gpio] = GpioIrqCon##gpio;                                                                   \
    gpio_irq_mask[GPIO_INDEX_##gpio] = GpioIrqMask##gpio;                                                                   \
    gpio_irq_pend[GPIO_INDEX_##gpio] = GpioIrqPend##gpio;                                                                   \
}

struct pin_irq_bankinfo GPIO_BANK_INFO[4] = {
    {EXT_INT_SERVICE_XB,GPIO_INDEX_A0},
    {EXT_INT_SERVICE_XA,GPIO_INDEX_K0},
    {EXT_INT_SERVICE_XC,GPIO_INDEX_V0},
    {0u,0u},
};

static struct pin_index *get_pin_index(os_base_t pin)
{
    struct pin_index *index = OS_NULL;

    if(pin < GPIO_PIN_MAX)
    {
        index = indexs[__PORT_INDEX(pin)];
    }

    return index;
}

void __os_hw_pin_init(void)
{
    GPIO_INFO_MAP(A0);
    GPIO_INFO_MAP(A1);
    GPIO_INFO_MAP(B);
    GPIO_INFO_MAP(C0);
    GPIO_INFO_MAP(C1);
    GPIO_INFO_MAP(D0);
    GPIO_INFO_MAP(D1);
    GPIO_INFO_MAP(F0);
    GPIO_INFO_MAP(F1);
    GPIO_INFO_MAP(F2);
    GPIO_INFO_MAP(F3);    
    GPIO_INFO_MAP(J0);
    GPIO_INFO_MAP(J1);
    GPIO_INFO_MAP(K0);
    GPIO_INFO_MAP(K1);
    GPIO_INFO_MAP(K2);
    GPIO_INFO_MAP(K3);
    GPIO_INFO_MAP(L0);
    GPIO_INFO_MAP(L1);
    GPIO_INFO_MAP(L2);
    GPIO_INFO_MAP(Y0);
    GPIO_INFO_MAP(Y1);
    GPIO_INFO_MAP(Y2);
    GPIO_INFO_MAP(Y3);
    GPIO_INFO_MAP(Y4);
    GPIO_INFO_MAP(Y5);
    GPIO_INFO_MAP(Y6);
    GPIO_INFO_MAP(M0);
    GPIO_INFO_MAP(M1);
    GPIO_INFO_MAP(M2);
    GPIO_INFO_MAP(M3);
    GPIO_INFO_MAP(M4);    
    GPIO_INFO_MAP(X0);
    GPIO_INFO_MAP(X1);
    GPIO_INFO_MAP(X2);
    GPIO_INFO_MAP(X3);
    GPIO_INFO_MAP(Z);
    GPIO_INFO_MAP(V0);
    GPIO_INFO_MAP(V1);
    GPIO_INFO_MAP(V2);
    GPIO_INFO_MAP(V3);
    GPIO_INFO_MAP(V4);
}

void irq_func_gpio_common(int irqno, void *param)
{
    struct pin_index *index;
    int group;
    int svcno;
    int pin;
    struct pin_irq_param   *irq;
    struct pin_irq_bankinfo *bank = (struct pin_irq_bankinfo *)param;

    Gpio_ServerceGroupIrq(bank->base+EXT_INT_SERVICE_PEND_OFFSET,&group,&svcno);

    if(Gpio_GetIrqStatus(bank->base+4, svcno))
    {
        pin = (bank->start_port+(group-1))*GPIO_PIN_PER_PORT;
        pin += svcno;
        index = get_pin_index(pin);
        Gpio_ClearIrq(index->irq_pend, PIN_OFFSET(pin));

        os_list_for_each_entry(irq, &exynos_gpio_irq_list, struct pin_irq_param, list)
        {
            if(irq->pin == pin)
            {
                irq->hdr(irq->args);
                //index = get_pin_index(pin);
                //Gpio_ClearIrq(index->irq_pend, PIN_OFFSET(pin));
            }
        }
    }

    return;
}

void irq_func_eint16_32(int irqno, void *param)
{
    uint32_t pin = ~0u;
    uint32_t bit = 0;
    struct pin_irq_param   *irq;

    for(bit=0;bit<8;bit++)
    {
        if(Gpio_GetIrqStatus(GpioIrqPendX2, PIN_OFFSET(bit)))
        {
            pin = GET_PIN(X2,0)+bit;
            Gpio_ClearIrq(GpioIrqPendX2, bit);
            break;
        }

        if(Gpio_GetIrqStatus(GpioIrqPendX3, PIN_OFFSET(bit)))
        {
            pin = GET_PIN(X3,0)+bit;
            Gpio_ClearIrq(GpioIrqPendX3, bit);
            break;
        }
    }

    if(pin == ~0u)
    {
        return;
    }

    os_list_for_each_entry(irq, &exynos_gpio_irq_list, struct pin_irq_param, list)
    {
        if(irq->pin == pin)
        {
            irq->hdr(irq->args);
            //index = get_pin_index(pin);
            //Gpio_ClearIrq(index->irq_pend, PIN_OFFSET(pin));
        }
    }

    return;
}

void irq_func_eint0_15(int irqno, void *param)
{

    struct pin_index *index;
    uint32_t pin;
    struct pin_irq_param   *irq;

    if(irqno >= IRQ_EINT0 && irqno <= IRQ_EINT7)
    {
        pin = GET_PIN(X0,0) + (irqno - IRQ_EINT0);
    }
    else
    {
        pin = GET_PIN(X1,0) + (irqno - IRQ_EINT8);
    }

    index = get_pin_index(pin);

    if(Gpio_GetIrqStatus(index->irq_pend, PIN_OFFSET(pin)))
    {
        os_list_for_each_entry(irq, &exynos_gpio_irq_list, struct pin_irq_param, list)
        {
            if(irq->pin == pin)
            {
                irq->hdr(irq->args);
                Gpio_ClearIrq(index->irq_pend, PIN_OFFSET(pin));
            }
        }
    }
    return;
}

struct EXYNOS_IRQ_STAT exynos_irq_stat[] = {
    {IRQ_GPIO_LB, 0,irq_func_gpio_common},
    {IRQ_GPIO_RT, 0,irq_func_gpio_common},
    {IRQ_GPIO_C2C, 0,irq_func_gpio_common},
    {IRQ_EINT16_31, 0,irq_func_eint16_32},
};

struct EXYNOS_IRQ_STAT *pState[] = {
    &exynos_irq_stat[0],
    &exynos_irq_stat[1],
    &exynos_irq_stat[2],
    &exynos_irq_stat[3],
};

uint32_t get_pin_base(os_base_t pin)
{
    struct pin_index *index = get_pin_index(pin);

    OS_ASSERT(index != OS_NULL);

    return index->gpio_port;
}

static void exynos_pin_write(struct os_device *dev, os_base_t pin, os_base_t value)
{
    const struct pin_index *index;

    index = get_pin_index(pin);
    if (index == OS_NULL)
    {
        return;
    }

    Gpio_WriteOutputIO(index->gpio_port, PIN_OFFSET(pin), (boolean_t)value);
}

static int exynos_pin_read(struct os_device *dev, os_base_t pin)
{
    int                     value;
    const struct pin_index *index;

    value = PIN_LOW;

    index = get_pin_index(pin);
    if (index == OS_NULL)
    {
        return value;
    }

    value = Gpio_GetInputIO(index->gpio_port, PIN_OFFSET(pin));

    return value;
}

static void exynos_pin_mode(struct os_device *dev, os_base_t pin, os_base_t mode)
{
    struct pin_index *index;
    stc_gpio_cfg_t    GPIO_Cfg;

#if (PIN_PULL_STATE)
    os_ubase_t         level;
#endif

    index = get_pin_index(pin);
    if (index == OS_NULL)
    {
        return;
    }

    switch(mode) {
        case PIN_MODE_OUTPUT:
            GPIO_Cfg.enDir = GpioDirOut;
            GPIO_Cfg.enPd = GpioPdDisable;
            GPIO_Cfg.enPu = GpioPuDisable;
            break;
        case PIN_MODE_INPUT:
            GPIO_Cfg.enDir = GpioDirIn;
            GPIO_Cfg.enPd = GpioPdDisable;
            GPIO_Cfg.enPu = GpioPuDisable;
            break;
        case PIN_MODE_INPUT_PULLUP:
            GPIO_Cfg.enDir = GpioDirIn;
            GPIO_Cfg.enPd = GpioPdDisable;
            GPIO_Cfg.enPu = GpioPuEnable;
            break;
        case PIN_MODE_INPUT_PULLDOWN:
            GPIO_Cfg.enDir = GpioDirIn;
            GPIO_Cfg.enPd = GpioPdEnable;
            GPIO_Cfg.enPu = GpioPuDisable;
            break;
        case PIN_MODE_OUTPUT_OD:
            os_kprintf("gpio od mode not support!\n\r");
            break;
        default:
            break;
    }
#if (PIN_PULL_STATE)
    /* remeber the pull state. */
    level = os_hw_interrupt_disable();
    index->pin_pulls[__PIN_INDEX(pin)].pull_up   = GPIO_Cfg.enPu;
    index->pin_pulls[__PIN_INDEX(pin)].pull_down = GPIO_Cfg.enPd;
    os_hw_interrupt_enable(level);
#endif
    Gpio_Init(index->gpio_port, PIN_OFFSET(pin), &GPIO_Cfg);
}

    static os_err_t
exynos_pin_attach_irq(struct os_device *device, int32_t pin, uint32_t mode, void (*hdr)(void *args), void *args)
{
    const struct pin_index *index;
    os_ubase_t               level;
    struct pin_irq_param   *irq;
    struct pin_irq_param   *tmp;

    index = get_pin_index(pin);
    if (index == OS_NULL)
    {
        return OS_NOSYS;
    }

    irq = os_calloc(1, sizeof(struct pin_irq_param));
    OS_ASSERT(irq != OS_NULL);

    os_spin_lock_irqsave(&gs_device_lock, &level);
    os_list_for_each_entry(tmp, &exynos_gpio_irq_list, struct pin_irq_param, list)
    {
        if(tmp->pin == pin)
        {
            if(tmp->hdr  == hdr   &&
                    tmp->mode == mode  &&
                    tmp->args == args)
            {
                os_spin_unlock_irqrestore(&gs_device_lock, level);
                os_free(irq);
                return OS_SUCCESS;
            }
            else
            {
                os_spin_unlock_irqrestore(&gs_device_lock, level);
                os_free(irq);
                return OS_BUSY;
            }
        }
    }

    irq->pin  = pin;
    irq->hdr  = hdr;
    irq->mode = mode;
    irq->args = args;

    os_list_add_tail(&exynos_gpio_irq_list, &irq->list);
    os_spin_unlock_irqrestore(&gs_device_lock, level);
    return OS_SUCCESS;
}

static os_err_t exynos_pin_dettach_irq(struct os_device *device, int32_t pin)
{
    const struct pin_index *index;
    os_ubase_t               level;
    struct pin_irq_param   *irq;

    index = get_pin_index(pin);
    if (index == OS_NULL)
    {
        return OS_NOSYS;
    }

    os_spin_lock_irqsave(&gs_device_lock, &level);
    os_list_for_each_entry(irq, &exynos_gpio_irq_list, struct pin_irq_param, list)
    {
        if(irq->pin == pin)
        {
            os_list_del(&irq->list);
            os_spin_unlock_irqrestore(&gs_device_lock, level);
            return OS_SUCCESS;
        }
    }
    os_spin_unlock_irqrestore(&gs_device_lock, level);
    return OS_EMPTY;
}
static os_err_t exynos_pin_irq_enable(struct os_device *device, os_base_t pin, uint32_t enabled)
{
    const struct pin_index   *index;
    os_ubase_t                 level;
    struct pin_irq_param     *irq;

    index = get_pin_index(pin);
    if (index == OS_NULL)
    {
        return OS_NOSYS;
    }

    if (enabled == PIN_IRQ_ENABLE)
    {
        os_spin_lock_irqsave(&gs_device_lock, &level);

        os_list_for_each_entry(irq, &exynos_gpio_irq_list, struct pin_irq_param, list)
        {
            if(irq->pin == pin)
            {

                switch (irq->mode)
                {
                    case PIN_IRQ_MODE_RISING:
                        Gpio_EnableIrq(index->gpio_port, index->irq_con,index->irq_mask,index->irq_pend,PIN_OFFSET(pin), GpioIrqRising);
                        break;
                    case PIN_IRQ_MODE_FALLING:
                        Gpio_EnableIrq(index->gpio_port, index->irq_con,index->irq_mask,index->irq_pend, PIN_OFFSET(pin), GpioIrqFalling);
                        break;
                    case PIN_IRQ_MODE_HIGH_LEVEL:
                        Gpio_EnableIrq(index->gpio_port, index->irq_con,index->irq_mask,index->irq_pend, PIN_OFFSET(pin), GpioIrqHigh);
                        break;
                    case PIN_IRQ_MODE_LOW_LEVEL:
                        Gpio_EnableIrq(index->gpio_port, index->irq_con,index->irq_mask,index->irq_pend, PIN_OFFSET(pin), GpioIrqLow);
                        break;
                    case PIN_IRQ_MODE_RISING_FALLING:
                        Gpio_EnableIrq(index->gpio_port, index->irq_con,index->irq_mask,index->irq_pend, PIN_OFFSET(pin), GpioIrqBoth);
                        break;
                    default:
                        os_spin_unlock_irqrestore(&gs_device_lock, level);
                        return OS_NOSYS;
                }
                Gpio_ClearIrq(index->irq_pend, PIN_OFFSET(pin));
                os_spin_unlock_irqrestore(&gs_device_lock, level);
            }
        }
    }
    else if (enabled == PIN_IRQ_DISABLE)
    {
        os_spin_lock_irqsave(&gs_device_lock, &level);
        os_list_for_each_entry(irq, &exynos_gpio_irq_list, struct pin_irq_param, list)
        {
            if(irq->pin == pin)
            {
                switch (irq->mode)
                {
                    case PIN_IRQ_MODE_RISING:
                        Gpio_DisableIrq(index->irq_mask, PIN_OFFSET(pin));
                        break;
                    case PIN_IRQ_MODE_FALLING:
                        Gpio_DisableIrq(index->irq_mask, PIN_OFFSET(pin));
                        break;
                    case PIN_IRQ_MODE_HIGH_LEVEL:
                        Gpio_DisableIrq(index->irq_mask, PIN_OFFSET(pin));
                        break;
                    case PIN_IRQ_MODE_LOW_LEVEL:
                        Gpio_DisableIrq(index->irq_mask, PIN_OFFSET(pin));
                        break;
                    case PIN_IRQ_MODE_RISING_FALLING:
                    default:
                        os_spin_unlock_irqrestore(&gs_device_lock, level);
                        return OS_NOSYS;
                }

                os_spin_unlock_irqrestore(&gs_device_lock, level);
            }
        }

    }
    else
    {
        return OS_NOSYS;
    }
    return OS_SUCCESS;
}
const static struct os_pin_ops _exynos_pin_ops = {
    exynos_pin_mode,
    exynos_pin_write,
    exynos_pin_read,
    exynos_pin_attach_irq,
    exynos_pin_dettach_irq,
    exynos_pin_irq_enable,
};

/**
 ***********************************************************************************************************************
 * @brief           os_hw_pin_init:enable gpio clk,register pin device.
 *
 * @param[in]       none
 *
 * @return          Return init result.
 * @retval          OS_SUCCESS       init success.
 * @retval          Others       init failed.
 ***********************************************************************************************************************
 */
static os_err_t os_hw_pin_init(void)
{
    int index = 0;
    int ret = 0;
    struct pin_index *tmp = OS_NULL;
    uint8_t gpio_port = 0;

#if (PIN_PULL_STATE)
    uint8_t gpio_pin = 0;
#endif

    __os_hw_pin_init();

    for(index=0;index<sizeof(pState)/sizeof(pState[0]);index++)
    {

        os_hw_interrupt_install(pState[index]->irqno, pState[index]->func, &GPIO_BANK_INFO[index], "GPIO");
        os_hw_interrupt_set_target_cpus(pState[index]->irqno, 0x1);
        os_hw_interrupt_umask(pState[index]->irqno);
    }

    for(index=IRQ_EINT0;index<=IRQ_EINT15;index++)
    {
        os_hw_interrupt_install(index, irq_func_eint0_15, 0, "GPIO");
        os_hw_interrupt_set_target_cpus(index, 0x1);
        os_hw_interrupt_umask(index);
    }

    for(gpio_port = 0; gpio_port < GPIO_PORT_MAX; gpio_port++)
    {
        if((gpio_port_map & (1ull << gpio_port)))
        {
            tmp = (struct pin_index *)os_calloc(1, sizeof(struct pin_index));

            OS_ASSERT(tmp != OS_NULL);

            tmp->gpio_port = gpio_port_base[gpio_port];
            tmp->irq_con = gpio_irq_con[gpio_port];
            tmp->irq_mask = gpio_irq_mask[gpio_port];
            tmp->irq_pend = gpio_irq_pend[gpio_port];
            if(gpio_port >= GPIO_INDEX_A0 && gpio_port <= GPIO_INDEX_J1)
            {
                tmp->irq_bank = 0;
            }
            else if(gpio_port >= GPIO_INDEX_K0 && gpio_port <= GPIO_INDEX_M4)
            {
                tmp->irq_bank = 1;
            }
            else if(gpio_port >= GPIO_INDEX_V0 && gpio_port <= GPIO_INDEX_V4)
            {
                tmp->irq_bank = 2;
            }
            else if(gpio_port >= GPIO_INDEX_X2 && gpio_port <= GPIO_INDEX_X3)
            {
                tmp->irq_bank = 3;
            }
            else if(gpio_port == GPIO_INDEX_Z)
            {
                tmp->irq_bank = OS_NOSYS;
            }else if(gpio_port >= GPIO_INDEX_X0 && gpio_port <= GPIO_INDEX_X1)
            {
                tmp->irq_bank = OS_EMPTY;
            }
            else
            {
                os_free(tmp);
                return OS_FAILURE;
            }
#if (PIN_PULL_STATE)
            for(gpio_pin = 0; gpio_pin < GPIO_PIN_PER_PORT; gpio_pin++)
            {
                tmp->pin_pulls[gpio_pin].pull_up   = 0;
                tmp->pin_pulls[gpio_pin].pull_down = 0;
            }
#endif
            indexs[gpio_port] = tmp;

        }
        else
        {
            indexs[gpio_port] = OS_NULL;
        }
    }
    ret = os_device_pin_register(0, &_exynos_pin_ops, OS_NULL);

    if(ret == 0)
    {
        return OS_SUCCESS;
    }
    return OS_FAILURE;
}
OS_INIT_CALL(os_hw_pin_init, OS_INIT_LEVEL_POST_KERNEL,OS_INIT_SUBLEVEL_LOW);


#endif /* OS_USING_PIN */
