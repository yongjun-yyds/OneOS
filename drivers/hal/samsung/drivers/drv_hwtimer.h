/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_hwtimer.h
 *
 * @brief       This file provides functions declaration for STM32 timer driver.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __DRV_HWTIMER_H__
#define __DRV_HWTIMER_H__

#include <timer/clocksource.h>
#include <timer/clockevent.h>

#define LT0_BASE_OFFSET 0x0300
#define LT1_BASE_OFFSET 0x0400
#define LT2_BASE_OFFSET 0x0500
#define LT3_BASE_OFFSET 0x0600

#define LT_TCNTB_REG     0x0000 /* Specifies the tick integer count buffer register        */
#define LT_TCNTO_REG     0x0004 /* Specifies the tick integer count observation register   */
#define LT_ICNTB_REG     0x0008 /* Specifies the interrupt count buffer register           */
#define LT_ICNTO_REG     0x000C /* Specifies the interrupt count observation register      */
#define LT_FRCNTB_REG    0x0010 /* Specifies the free running count buffer register        */
#define LT_FRCNTO_REG    0x0014 /* Specifies the free running count observation register   */
#define LT_TCON_REG      0x0020 /* Specifies the timer control register                    */
#define LT_INT_CSTAT_REG 0x0030 /* Specifies the clears interrupt                          */
#define LT_INT_ENB_REG   0x0034 /* Specifies the interrupt enable for L_IRQ0               */
#define LT_WSTAT_REG     0x0040 /* Specifies the write status                              */

#define PWM_TIMER_BASE              0x139D0000

#define PWM_TIMER_TCFG0_REG         0x0000
#define PWM_TIMER_TCFG1_REG         0x0004
#define PWM_TIMER_TCON_REG          0x0008
#define PWM_TIMER_TCNTB4_REG        0x003C
#define PWM_TIMER_TCNTO4_REG        0x0040
#define PWM_TIMER_TINT_CSTAT_REG    0x0044

#define TIMER_4_PRESCALER_MASK    (0x0000ff00)
#define TIMER_4_PRESCALER_OFFSET  (8)


#define TIMER_4_START (1 << 20)
#define TIMER_4_STOP  (0 << 20)

#define TIMER_4_MANUAL_UPDATE  (1 << 21)

#define TIMER_4_AUTO_ON  (1 << 22)
#define TIMER_4_AUTO_OFF (0 << 22)

#define TIMER_4_INT_ENABLE  (1 << 4)
#define TIMER_4_INT_DISABLE (0 << 4)

#define TIMER_4_INT_IRQ_CLEAR (1 << 9)



#define EXYNOS_TIMER_0  0x00
#define EXYNOS_TIMER_1  0x01
#define EXYNOS_TIMER_2  0x02
#define EXYNOS_TIMER_3  0x03
#define EXYNOS_TIMER_4  0x04

#define EXYNOS_TIMER_L0 0x80
#define EXYNOS_TIMER_L1 0x81
#define EXYNOS_TIMER_L2 0x82
#define EXYNOS_TIMER_L3 0x83

#define EXYNOS_GTIMER   0XFF

struct exynos_timer
{
    union _clock
    {
        os_clocksource_t cs;
        os_clockevent_t  ce;
    } clock;

    uint32_t        freq;
    os_list_node_t     list;
    void              *user_data;
    struct exynos_timer_info *timer_info;
};


struct exynos_timer_info 
{
    int hw_base;
    int irqno;
    int tim_idx;
};



#endif

