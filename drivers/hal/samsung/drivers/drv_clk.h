#ifndef __DRV_CLK_H_
#define __DRV_CLK_H_
#include <exynos4412-clk.h>

void os_hw_clk_init(void);
os_err_t clk_get_rate(const char * name, uint64_t * rate);
#endif
