/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with 
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_common.h
 *
 * @brief       This file provides common functions..
 *
 * @revision
 * Date         Author          Notes
 * 2020-11-17   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __DRV_COMMON_H__
#define __DRV_COMMON_H__

#include <board.h>

#define GLOBAL_MCT_TIMER_BASE       0x10050000
#define GLOBAL_TIMER_FRC_LOW_REG    0x100
#define GLOBAL_TIMER_FRC_HIGH_REG   0x104
#define GLOBAL_TIMER_CNT_WSTATE_REG 0x110

#define GLOBAL_TIMER_CTL_REG       0x240
#define GLOBAL_TIMER_ENABLE        (1<<8)

#define GLOBAL_TIMER_INT_STA_REG   0x0C // INTERRUPT STATUS 

#define GLOBAL_TIMER_COMP0_LOW_REG       0x200
#define GLOBAL_TIMER_COMP0_HIGH_REG      0x204
#define GLOBAL_TIMER_COMP0_AUTO_INC_REG  0x208
#define GLOBAL_TIMER_COMP1_LOW_REG       0x210
#define GLOBAL_TIMER_COMP1_HIGH_REG      0x214
#define GLOBAL_TIMER_COMP1_AUTO_INC_REG  0x218
#define GLOBAL_TIMER_COMP2_LOW_REG       0x220
#define GLOBAL_TIMER_COMP2_HIGH_REG      0x224
#define GLOBAL_TIMER_COMP2_AUTO_INC_REG  0x228
#define GLOBAL_TIMER_COMP3_LOW_REG       0x230
#define GLOBAL_TIMER_COMP3_HIGH_REG      0x234
#define GLOBAL_TIMER_COMP3_AUTO_INC_REG  0x238
#define GLOBAL_TIMER_COMP_WSTATE_REG     0x24c
#define GLOBAL_TIMER_INT_CSTAT_REG       0x244
#define GLOBAL_TIMER_INT_ENB_REG         0x248

#endif
