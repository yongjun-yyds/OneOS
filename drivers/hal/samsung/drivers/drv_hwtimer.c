/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_hwtimer.c
 *
 * @brief       This file implements timer driver for exynos.
 *
 * @revision
 * Date         Author          Notes
 * 2022-11-2   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <board.h>
#include <os_memory.h>
#include <timer/timer.h>
#include "oneos_config.h"

#ifdef OS_USING_CLOCKSOURCE
#include <timer/clocksource.h>
#endif

#ifdef OS_USING_CLOCKEVENT
#include <timer/clockevent.h>
#endif

#include <drv_common.h>
#include <drv_clk.h>
#include <drv_hwtimer.h>
#include <arch_misc.h>


static os_list_node_t exynos_timer_list = OS_LIST_INIT(exynos_timer_list);


static uint64_t exynos_gtim_cnt_get(void *clock)
{
    struct exynos_timer *timer;
    int *reg = 0;
    int base_addr = 0;
    uint64_t ret_value;

    timer = (struct exynos_timer *)clock;

    base_addr = timer->timer_info->hw_base;
    
    reg = (int *)(base_addr + GLOBAL_TIMER_FRC_HIGH_REG);
    ret_value  = (uint64_t)(*reg);
    reg = (int *)(base_addr + GLOBAL_TIMER_FRC_LOW_REG);
    ret_value  = (ret_value << 32) | (*reg);
    
    return ret_value;
}

#if defined(OS_USING_CLOCKSOURCE) || defined(OS_USING_CLOCKEVENT)
static uint64_t exynos_tim4_read(void *clock)
{
    int      base_addr;
    uint64_t ret_value;
    struct exynos_timer *timer;

    timer     = (struct exynos_timer *)clock;
    base_addr = timer->timer_info->hw_base;
    ret_value = (uint64_t)(*(int *)(base_addr + PWM_TIMER_TCNTO4_REG));

    return ret_value;
}
#endif

#ifdef OS_USING_CLOCKEVENT

void exynos_tim4_isr(int irqno, void *param)
{
    struct exynos_timer *timer;
    int *reg = 0;

    struct exynos_timer *timer_isr = (struct exynos_timer *)param;

    os_list_for_each_entry(timer, &exynos_timer_list, struct exynos_timer, list)
    {
        if (timer->timer_info->tim_idx == timer_isr->timer_info->tim_idx)
        {
#ifdef OS_USING_CLOCKEVENT
            /* clean interrupt status bit */
            reg = (int *)(timer_isr->timer_info->hw_base + PWM_TIMER_TINT_CSTAT_REG);
            *reg |= TIMER_4_INT_IRQ_CLEAR;

            os_clockevent_isr((os_clockevent_t *)timer);
#endif
            return;
        }
    }
}

static void exynos_tim4_start(os_clockevent_t *ce, uint32_t prescaler, uint64_t count)
{
    struct exynos_timer *timer;
    volatile int *reg;
    volatile int  base_addr;

    OS_ASSERT(prescaler != 0);
    OS_ASSERT(count != 0);

    OS_UNREFERENCE(prescaler);

    timer = (struct exynos_timer *)ce;
    base_addr = timer->timer_info->hw_base;

    /* config timer disable auto reload*/
    reg   = (int *)(base_addr + PWM_TIMER_TCON_REG);
    *reg |= TIMER_4_AUTO_ON;

    /* config timer prescaler */
    reg = (int *)(base_addr + PWM_TIMER_TCFG0_REG);
    *reg &= ~TIMER_4_PRESCALER_MASK;
    *reg |= (1 << TIMER_4_PRESCALER_OFFSET) & TIMER_4_PRESCALER_MASK;

    /* config count buffer */
    reg  = (int *)(base_addr + PWM_TIMER_TCNTB4_REG);
    *reg = (int)(count);

    /* config timer disable auto reload*/
    reg   = (int *)(base_addr + PWM_TIMER_TCON_REG);
    *reg |= TIMER_4_MANUAL_UPDATE;

    /* config timer disable auto reload*/
    reg   = (int *)(base_addr + PWM_TIMER_TCON_REG);
    *reg &= ~TIMER_4_MANUAL_UPDATE;

    /* enable interrupt */
    reg   = (int *)(base_addr + PWM_TIMER_TINT_CSTAT_REG);
    *reg |= TIMER_4_INT_ENABLE;

    /* start timer */
    reg   = (int *)(base_addr + PWM_TIMER_TCON_REG);
    *reg |= TIMER_4_START;
}

static void exynos_tim4_stop(os_clockevent_t *ce)
{
    struct exynos_timer *timer;
    volatile int *reg       = 0;
    volatile int  base_addr = 0;

    OS_ASSERT(ce != OS_NULL);

    timer = (struct exynos_timer *)ce;
    base_addr = timer->timer_info->hw_base;

    /* stop timer */
    reg = (int *)(base_addr + PWM_TIMER_TCON_REG);
    *reg &= TIMER_4_STOP;

    /* disable interrupt */
    reg  = (int *)(base_addr + PWM_TIMER_TINT_CSTAT_REG);
    *reg &= TIMER_4_INT_DISABLE;
}

static const struct os_clockevent_ops exynos_tim4_ops = 
{
    .start = exynos_tim4_start,
    .stop  = exynos_tim4_stop,
    .read  = exynos_tim4_read,
};

#endif

static int exynos_tim_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    struct exynos_timer *timer;

    timer = os_calloc(1, sizeof(struct exynos_timer));

    timer->timer_info = (struct exynos_timer_info *)dev->info;

#ifdef OS_USING_CLOCKSOURCE
    if (timer->timer_info->tim_idx == EXYNOS_GTIMER)
    {
        if (os_clocksource_best() == OS_NULL)
        {
            timer->clock.cs.rating = 640;
            timer->clock.cs.freq   = 24 * 1000 * 1000;
            timer->clock.cs.mask   = 0xffffffffffffffffull;
            timer->clock.cs.read   = exynos_gtim_cnt_get;
            os_clocksource_register(dev->name, &timer->clock.cs);
        }
    }
    else
#endif
    {
#ifdef OS_USING_CLOCKEVENT
        if (timer->timer_info->tim_idx == EXYNOS_TIMER_4)
        {
            /* set irq callback */
            os_hw_interrupt_install(timer->timer_info->irqno, exynos_tim4_isr, timer, dev->name);
            os_hw_interrupt_umask(timer->timer_info->irqno);
            os_hw_interrupt_set_target_cpus(timer->timer_info->irqno, 1);
    
            timer->clock.ce.rating = 320;
            timer->clock.ce.freq   = 50 * 1000 * 1000;
            timer->clock.ce.mask   = 0xffffffffull;

            timer->clock.ce.prescaler_mask = 0x00000000ul;
            timer->clock.ce.prescaler_bits = 0;

            timer->clock.ce.count_mask = 0xfffffffful;
            timer->clock.ce.count_bits = 32;
            
            timer->clock.ce.feature = OS_CLOCKEVENT_FEATURE_PERIOD;
            
            timer->clock.ce.min_nsec = NSEC_PER_SEC / timer->clock.ce.freq;

            timer->clock.ce.ops = &exynos_tim4_ops;
            os_clockevent_register(dev->name, &timer->clock.ce);
        }
#endif
    }

    os_list_add(&exynos_timer_list, &timer->list);

    return OS_SUCCESS;
}

OS_DRIVER_INFO exynos_tim_driver = {
    .name  = "TIM_HandleTypeDef",
    .probe = exynos_tim_probe,
};

OS_DRIVER_DEFINE(exynos_tim_driver, OS_INIT_LEVEL_DEVICE, OS_INIT_SUBLEVEL_MIDDLE);
