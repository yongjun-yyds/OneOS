#include <oneos_config.h>
#include <os_types.h>
#include <os_errno.h>
#include <os_stddef.h>
#include <os_clock.h>
#include <string.h>
#include <os_memory.h>
#include <os_util.h>
#include <cp15.h>
#include "bcm283x.h"
#include "board.h"
#include <arch_misc.h>
#include <interrupt.h>
#include <drv_clk.h>
#include <drv_common.h>

#define CORE0_TIMER_IRQ_CTRL    HWREG32(0x40000040)

uint32_t platform_get_gic_dist_base(void)
{
    return 0x10490000;
}

uint32_t platform_get_gic_cpu_base(void)
{
    return 0x10480000;
}

static void os_hw_gtimer_isr(int irqno, void *param)
{
    unsigned int reg = 0;

    reg = *(unsigned int *)(GLOBAL_MCT_TIMER_BASE + GLOBAL_TIMER_INT_CSTAT_REG);

    if (reg & 0x1)
    {
        *(unsigned int *)(GLOBAL_MCT_TIMER_BASE + GLOBAL_TIMER_INT_CSTAT_REG) = reg | (1<<0);
        os_tick_increase();
    }
}

uint32_t g_tick_freq = 0;

uint32_t os_tick_frequency(void)
{
    return g_tick_freq;
}

uint64_t os_tick_timer_cnt(void)
{
    uint64_t ret_cnt = 0;
    
    ret_cnt = *(uint32_t*)(GLOBAL_MCT_TIMER_BASE+GLOBAL_TIMER_FRC_HIGH_REG);
    ret_cnt = (ret_cnt << 32) | (*(uint32_t*)(GLOBAL_MCT_TIMER_BASE+GLOBAL_TIMER_FRC_LOW_REG));
    
    return ret_cnt ;
}

void os_hw_systick_timer_init(void)
{

    volatile os_ubase_t* tmp = OS_NULL;
    volatile os_ubase_t base = 0;
    uint64_t xtal;

    clk_get_rate("xtal", &xtal);
    g_tick_freq = xtal;

    base = GLOBAL_MCT_TIMER_BASE;

    /*enbale G_IRQ1*/
    tmp = (os_ubase_t*)(base + GLOBAL_TIMER_INT_ENB_REG);
    *tmp |= (1 << 0);

    /* disable global timer disable Global FRC */
    tmp = (os_ubase_t*)(base + GLOBAL_TIMER_CTL_REG);
    *tmp &= ~(1 << 8);

    /* clear the global timer cnt */
    tmp = (os_ubase_t*)(base + GLOBAL_TIMER_FRC_HIGH_REG);
    *tmp = 0x0;

    tmp = (os_ubase_t*)(base + GLOBAL_TIMER_CNT_WSTATE_REG);
    while (!(*tmp & 0x02));
    *tmp |= 0x02;
    
    /* g_cnt low */
    tmp = (os_ubase_t*)(base + GLOBAL_TIMER_FRC_LOW_REG);
    *tmp = 0x0;

    tmp = (os_ubase_t*)(base + GLOBAL_TIMER_CNT_WSTATE_REG);
    while (!(*tmp & 0x01));
    *tmp |= 0x01;

    /* clear the comparator 0 cnt */
    tmp = (os_ubase_t*)(base + GLOBAL_TIMER_COMP0_HIGH_REG);
    *tmp = 0x0;

    tmp = (os_ubase_t*)(base + GLOBAL_TIMER_COMP_WSTATE_REG);
    while (!(*tmp & 0x02));
    *tmp |= 0x02;

    /* comp0 cnt low */
    tmp = (os_ubase_t*)(base + GLOBAL_TIMER_COMP0_LOW_REG);
    //*tmp = 0x0;
    *tmp = (xtal/OS_TICK_PER_SECOND);

    tmp = (os_ubase_t*)(base + GLOBAL_TIMER_COMP_WSTATE_REG);
    while (!(*tmp & 0x01));
    *tmp |= 0x01;

    /* auto incresement */
    tmp = (os_ubase_t*)(base + GLOBAL_TIMER_COMP0_AUTO_INC_REG);
    *tmp = (xtal/OS_TICK_PER_SECOND); 

    tmp = (os_ubase_t*)(base + GLOBAL_TIMER_COMP_WSTATE_REG);
    while (!(*tmp & 0x04));
    *tmp |= 0x04;

    /* enable timer */
    tmp = (os_ubase_t*)(base + GLOBAL_TIMER_CTL_REG);
    *tmp |= ((1 << 8) | (1 << 0) | (1 << 1));

    tmp = (os_ubase_t*)(base + GLOBAL_TIMER_COMP_WSTATE_REG);
    while (!(*tmp & (1 << 16))); // check whether mct is assert
    *tmp |= (1 << 16); // write 1 to clear this bit

    os_hw_interrupt_install(IRQ_G0_IRQ,os_hw_gtimer_isr,OS_NULL,"gtimer");
    os_hw_interrupt_umask(IRQ_G0_IRQ);
    os_hw_interrupt_set_target_cpus(IRQ_G0_IRQ,1);

}
/*For ipi, MBox is used here for simplicity. The interrupt framework comes out and needs to be replaced*/

void core_mail_init(void)
{

    CORE_MB_INTCTL(0) = 0xf;
    CORE_MB_INTCTL(1) = 0xf;
    CORE_MB_INTCTL(2) = 0xf;
    CORE_MB_INTCTL(3) = 0xf;
    os_hw_interrupt_umask(2);

}

void core_mail(int cpu_index,unsigned int addr)
{
    CORE_MB_W_REG(cpu_index,0) = addr;
}

void core_mail_ack(void)
{
    int cpu_index;
    cpu_index = os_cpu_id_get();
    CORE_MB_R_REG(cpu_index,0) = CORE_MB_R_REG(cpu_index,0);


}

os_err_t os_hw_board_init(void)
{      
    os_hw_interrupt_init();
    os_hw_clk_init();

    os_hw_systick_timer_init();
    os_default_heap_init();
    os_kprintf("mem begin:0x%lx size 0x%lx\n\r",HEAP_BEGIN,(os_size_t)HEAP_END - (os_size_t)HEAP_BEGIN);
    os_default_heap_add((void *)HEAP_BEGIN, (os_size_t)HEAP_END - (os_size_t)HEAP_BEGIN,OS_MEM_ALG_DEFAULT);
    os_kprintf("global timer freq is %d\r\n",g_tick_freq);
    return OS_SUCCESS;
}
OS_INIT_CALL(os_hw_board_init, OS_INIT_LEVEL_PRE_KERNEL_1, OS_INIT_SUBLEVEL_HIGH);

