#include <drv_clk.h>

void os_hw_clk_init(void)
{
    exynos4412_clk_initial(); 
}

os_err_t clk_get_rate(const char * name, uint64_t * rate) 
{
    return exynos4412_clk_get_rate(name,rate);
}
