#include <oneos_config.h>
#include <os_types.h>
#include <os_stddef.h>
#include <os_errno.h>
#include "board.h"
#include "bcm283x.h"
#include "drv_uart.h"
#include <bus/bus.h>
#include <os_memory.h>
#include <os_util.h>
#include <os_assert.h>
#include <drv_cfg.h>
#include <interrupt.h>
#include <serial.h>
#include <os_stddef.h>
#include <drv_common.h>
#include <drv_uart.h>


static os_list_node_t exynos_uart_list = OS_LIST_INIT(exynos_uart_list);

static  OS_DEFINE_SPINLOCK(gs_uart_lock);

static os_err_t exynos_uart_configure(struct os_serial_device *serial, struct serial_configure *cfg)
{

    struct exynos_uart* uart = serial->parent.user_data;
    exynos4412_serial_init(uart->uart_info->index,cfg->baud_rate,cfg->data_bits,cfg->parity,cfg->stop_bits);
    return OS_SUCCESS;
}

void uart_putc(char c)
{
    exynos4412_serial_write(2,(unsigned char *)&c,1);
}

void __os_hw_console_output(char *log_buff)
{
    char * ch;
    ch = log_buff;

    while(*ch)
    {
        uart_putc(*ch);
        ch++;
    }
    return ;
}
static void uart_isr(struct os_serial_device *serial)
{
    unsigned int len = 0;
    struct exynos_uart* uart = serial->parent.user_data;

    len = exynos4412_serial_read(uart->uart_info->index,uart->rx_buff + uart->rx_index,uart->uart_info->fifo_size);
    uart->rx_index += len;
    exynos4412_generate_interrupt(uart->uart_info->index);
    soft_dma_timeout_irq(&uart->sdma);
}



static void os_hw_uart_isr(int irqno, void *param)
{
    struct exynos_uart *uart = (struct exynos_uart *)param;
    uart_isr(&uart->serial_dev);
}

static int exynos_uart_poll_send(struct os_serial_device *serial, const uint8_t *buff, os_size_t size)
{
    struct exynos_uart *uart;

    uart = serial->parent.user_data;

    exynos4412_serial_write(uart->uart_info->index,(uint8_t *)buff,size);

    return size;
}

static void exynos_uart_sdma_callback(soft_dma_t *dma)
{
    struct exynos_uart *uart = os_container_of(dma, struct exynos_uart, sdma);
    os_hw_serial_isr_rxdone((struct os_serial_device *)uart);
}

static uint32_t exynos_sdma_int_get_index(soft_dma_t *dma)
{
    struct exynos_uart *uart = os_container_of(dma, struct exynos_uart, sdma);

    return uart->rx_index;
}

static os_err_t exynos_sdma_int_start(soft_dma_t *dma, void *buff, uint32_t size)
{
    struct exynos_uart *uart = os_container_of(dma, struct exynos_uart, sdma);

    uart->rx_buff  = buff;
    uart->rx_index = 0;
    uart->rx_size  = size;

    exynos4412_generate_interrupt(uart->uart_info->index);
    os_hw_interrupt_umask(uart->uart_info->irqno);

    return OS_SUCCESS;
}

static uint32_t exynos_sdma_int_stop(soft_dma_t *dma)
{
    struct exynos_uart *uart = os_container_of(dma, struct exynos_uart, sdma);

    os_hw_interrupt_mask(uart->uart_info->irqno);

    return exynos_sdma_int_get_index(dma);
}

static void exynos_uart_sdma_init(struct exynos_uart *uart, dma_ring_t *ring)
{
    soft_dma_t *dma = &uart->sdma;

    soft_dma_stop(dma);

    memset(&dma->hard_info, 0, sizeof(dma->hard_info));

    dma->hard_info.mode         = HARD_DMA_MODE_NORMAL;
    dma->hard_info.max_size     = 64 * 1024;
    dma->hard_info.data_timeout = uart_calc_byte_timeout_us(uart->serial_dev.config.baud_rate);

    if (uart->uart_info->use_dma == 0)
    {
        dma->hard_info.flag         = HARD_DMA_FLAG_HALF_IRQ | HARD_DMA_FLAG_FULL_IRQ;
        dma->ops.get_index          = exynos_sdma_int_get_index;
        dma->ops.dma_init           = OS_NULL;
        dma->ops.dma_start          = exynos_sdma_int_start;
        dma->ops.dma_stop           = exynos_sdma_int_stop;
    }
    else
    {
        dma->hard_info.flag         = 0;
        dma->ops.get_index          = OS_NULL;
        dma->ops.dma_init           = OS_NULL;
        dma->ops.dma_start          = OS_NULL;
        dma->ops.dma_stop           = OS_NULL;
    }

    dma->cbs.dma_half_callback      = exynos_uart_sdma_callback;
    dma->cbs.dma_full_callback      = exynos_uart_sdma_callback;
    dma->cbs.dma_timeout_callback   = exynos_uart_sdma_callback;

    soft_dma_init(dma);
    soft_dma_start(dma, ring);
    soft_dma_irq_enable(&uart->sdma, OS_TRUE);
}

static os_err_t exynos_uart_init(struct os_serial_device *serial, struct serial_configure *cfg)
{
    struct exynos_uart *uart;

    OS_ASSERT(serial != OS_NULL);
    OS_ASSERT(cfg != OS_NULL);

    uart = (struct exynos_uart *)serial->parent.user_data;

    exynos_uart_configure(serial,cfg);


    exynos_uart_sdma_init(uart, &serial->rx_fifo->ring);

    return OS_SUCCESS;
}

static os_err_t exynos_uart_deinit(struct os_serial_device *serial)
{
    struct exynos_uart *uart;

    OS_ASSERT(serial != OS_NULL);

    uart = os_container_of(serial, struct exynos_uart, serial_dev);

    if(uart->uart_info->use_dma ==0)
    {
        os_hw_interrupt_mask(uart->uart_info->irqno);
    }

    soft_dma_stop(&uart->sdma);

    return 0;
}

static const struct os_uart_ops exynos_uart_ops =
{
    .init         = exynos_uart_init,
    .deinit       = exynos_uart_deinit,
    .start_send   = OS_NULL,
    .poll_send    = exynos_uart_poll_send,
};

static void os_hw_uart_init(struct exynos_uart *uart,const os_device_info_t *dev)
{
    struct serial_configure *config = OS_NULL;

    config = &(uart->serial_dev.config);

    exynos4412_serial_init(uart->uart_info->index,config->baud_rate,config->data_bits,config->parity,config->stop_bits);

    os_hw_interrupt_install(uart->uart_info->irqno,os_hw_uart_isr,uart,dev->name);
    os_hw_interrupt_set_target_cpus(uart->uart_info->irqno,1);
    os_hw_interrupt_mask(uart->uart_info->irqno);

}

static int exynos_usart_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    os_err_t result  = 0;
    os_ubase_t   level;
    struct serial_configure config  = OS_SERIAL_CONFIG_DEFAULT;
    struct exynos_uart *uart = os_malloc(sizeof(struct exynos_uart));

    OS_ASSERT(uart);    
    memset(uart,0,sizeof(struct exynos_uart));

    uart->uart_info = (struct exynos_usart_info *)dev->info;

    uart->state = RESET;

    struct os_serial_device *serial = &(uart->serial_dev);

    serial->ops = &exynos_uart_ops;
    serial->config = config;

    os_hw_uart_init(uart,dev);

    os_spin_lock_irq(&gs_uart_lock, &level);

    os_list_add_tail(&exynos_uart_list, &uart->list);

    os_spin_unlock_irq(&gs_uart_lock, level);

    result = os_hw_serial_register(serial, dev->name, uart);

    OS_ASSERT(result == OS_SUCCESS);

    return OS_SUCCESS;
}

OS_DRIVER_INFO exynos_usart_driver = {
    .name   = "UART_HandleTypeDef",
    .probe  = exynos_usart_probe,
};

OS_DRIVER_DEFINE(exynos_usart_driver, OS_INIT_LEVEL_PRE_DEVICE, OS_INIT_SUBLEVEL_HIGH);

