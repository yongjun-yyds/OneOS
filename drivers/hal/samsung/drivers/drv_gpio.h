/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_gpio.h
 *
 * @brief       This file provides struct/macro declaration and functions declaration for STM32 gpio driver.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __DRV_GPIO_H__
#define __DRV_GPIO_H__

#include <drv_common.h>
#include "board.h"
#include "exynos4412-gpio.h"
#include <interrupt.h>

enum GPIO_PORT_INDEX
{
    GPIO_INDEX_A0 = 0,
    GPIO_INDEX_A1,
    GPIO_INDEX_B,
    GPIO_INDEX_C0,
    GPIO_INDEX_C1,
    GPIO_INDEX_D0,
    GPIO_INDEX_D1,
    GPIO_INDEX_F0,
    GPIO_INDEX_F1,
    GPIO_INDEX_F2,
    GPIO_INDEX_F3,
    GPIO_INDEX_J0,
    GPIO_INDEX_J1,
    GPIO_INDEX_K0,
    GPIO_INDEX_K1,
    GPIO_INDEX_K2,
    GPIO_INDEX_K3,
    GPIO_INDEX_L0,
    GPIO_INDEX_L1,
    GPIO_INDEX_L2,
    GPIO_INDEX_Y0,
    GPIO_INDEX_Y1,
    GPIO_INDEX_Y2,
    GPIO_INDEX_Y3,
    GPIO_INDEX_Y4,
    GPIO_INDEX_Y5,
    GPIO_INDEX_Y6,
    GPIO_INDEX_M0,
    GPIO_INDEX_M1,
    GPIO_INDEX_M2,
    GPIO_INDEX_M3,
    GPIO_INDEX_M4,
    GPIO_INDEX_X0,
    GPIO_INDEX_X1,
    GPIO_INDEX_X2,
    GPIO_INDEX_X3,
    GPIO_INDEX_Z,
    GPIO_INDEX_V0,
    GPIO_INDEX_V1,
    GPIO_INDEX_V2,
    GPIO_INDEX_V3,
    GPIO_INDEX_V4,
    GPIO_INDEX_MAX
};

#define GPIO_PIN_PER_PORT           (16)
#define GPIO_PORT_MAX               (GPIO_INDEX_MAX)
#define GPIO_PIN_MAX                (GPIO_PORT_MAX * GPIO_PIN_PER_PORT)

#define __PORT_INDEX(pin)           (pin / GPIO_PIN_PER_PORT)
#define __PIN_INDEX(pin)            (pin % GPIO_PIN_PER_PORT)

#define GET_PIN(PORTx, PIN)         (((GPIO_INDEX_##PORTx - GPIO_INDEX_A0) * GPIO_PIN_PER_PORT) + PIN)
#define PIN_BASE(__pin)             get_pin_base(__pin)
#define PIN_OFFSET(__pin)           (uint32_t)((__pin) % GPIO_PIN_PER_PORT)

#define PIN_PULL_STATE               0
#if (PIN_PULL_STATE)
struct pin_pull_state
{
    en_gpio_pu_t pull_up;
    en_gpio_pd_t pull_down;
};
#endif
struct pin_index
{
    uint32_t irq_bank;
    uint32_t gpio_port;
    uint32_t irq_con;
    uint32_t irq_mask;
    uint32_t irq_pend;
#if (PIN_PULL_STATE)
    struct pin_pull_state pin_pulls[GPIO_PIN_PER_PORT];
#endif
};

struct pin_irq_map
{
    uint16_t pinbit;
    //IRQn_Type   irqno;
};

struct pin_irq_param
{
    int32_t    pin;
    void (*hdr)(void *args);
    void *args;
    uint16_t mode;
    uint8_t	irq_ref;

    os_list_node_t list;
};

struct EXYNOS_IRQ_STAT {
    uint32_t irqno;
    uint32_t ref;
    void (*func)(int irqno, void *param);
};

struct pin_irq_bankinfo{
    uint32_t base;
    uint32_t start_port;
};

#endif /* __DRV_GPIO_H__ */
