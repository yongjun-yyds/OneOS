/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with 
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        dev_usart.c
 *
 * @brief       This file implements usart devices for ingenic uart
 *
 * @revision
 * Date         Author          Notes
 * 2020-11-17   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <drv_hwtimer.h>
#include <irqno.h>

#ifdef BSP_USING_TIMER
struct exynos_timer_info timer_info = 
{
    .irqno       = IRQ_G0_IRQ,
    .hw_base     = GLOBAL_MCT_TIMER_BASE,
    .tim_idx     = EXYNOS_GTIMER,
};
OS_HAL_DEVICE_DEFINE("TIM_HandleTypeDef", "gtimer", timer_info);

struct exynos_timer_info timer4_info = 
{
    .irqno       = IRQ_TIMER4,
    .hw_base     = PWM_TIMER_BASE,
    .tim_idx     = EXYNOS_TIMER_4,
};
OS_HAL_DEVICE_DEFINE("TIM_HandleTypeDef", "timer4", timer4_info);


#ifdef BSP_USING_LOCAL_TIMER
#if 0
struct exynos_timer_info l0_info = 
{
    .irqno       = IRQ_INTG19,
    .hw_base     = GLOBAL_MCT_TIMER_BASE + LT0_BASE_OFFSET,
    .is_local    = 1,
    .irq_bit_idx = 0,
    .cpu_idx     = 0,
};
OS_HAL_DEVICE_DEFINE("TIM_HandleTypeDef", "ltimer0", l0_info);


struct exynos_timer_info l1_info = 
{
    .irqno       = IRQ_INTG18,
    .hw_base     = GLOBAL_MCT_TIMER_BASE + LT1_BASE,
    .is_local    = 1,
    .irq_bit_idx = 0,
    .cpu_idx     = 1,
};
OS_HAL_DEVICE_DEFINE("TIM_HandleTypeDef", "ltimer1", l1_info);

struct exynos_timer_info l2_info = 
{
    .irqno       = IRQ_INTG17,
    .hw_base     = GLOBAL_MCT_TIMER_BASE + LT2_BASE,
    .is_local    = 1,
    .irq_bit_idx = 7,
    .cpu_idx     = 2,
};

OS_HAL_DEVICE_DEFINE("TIM_HandleTypeDef", "ltimer2", l2_info);

struct exynos_timer_info l3_info = 
{
    .irqno       = IRQ_INTG16,
    .hw_base     = GLOBAL_MCT_TIMER_BASE + LT3_BASE,
    .is_local    = 1,
    .irq_bit_idx = 7,
    .cpu_idx     = 3,
};

OS_HAL_DEVICE_DEFINE("TIM_HandleTypeDef", "ltimer3", l3_info);
#endif

#endif


#endif

