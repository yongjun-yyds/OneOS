#ifndef __EXYNOS_4412_UART_H_
#define __EXYNOS_4412_UART_H_

#define EXYNOS4412_UART0_BASE		(0x13800000)
#define EXYNOS4412_UART1_BASE		(0x13810000)
#define EXYNOS4412_UART2_BASE		(0x13820000)
#define EXYNOS4412_UART3_BASE		(0x13830000)

#define EXYNOS4412_ULCON	 	 	(0x00)
#define EXYNOS4412_UCON	 	 		(0x04)
#define EXYNOS4412_UFCON	 	 	(0x08)
#define EXYNOS4412_UMCON	 	 	(0x0C)
#define EXYNOS4412_UTRSTAT			(0x10)
#define EXYNOS4412_UERSTAT			(0x14)
#define EXYNOS4412_UFSTAT			(0x18)
#define EXYNOS4412_UMSTAT			(0x1C)
#define EXYNOS4412_UTXH				(0x20)
#define EXYNOS4412_URXH				(0x24)
#define EXYNOS4412_UBRDIV			(0x28)
#define EXYNOS4412_UFRACVAL			(0x2C)
#define EXYNOS4412_UINTP			(0x30)
#define EXYNOS4412_UINTSP			(0x34)
#define EXYNOS4412_UINTM			(0x38)

#define EXYNOS4412_UFSTAT_TXFULL	(1<<24)
#define EXYNOS4412_UFSTAT_RXFULL	(1<<8)
#define EXYNOS4412_UFSTAT_TXCOUNT	(0xFF<<16)
#define EXYNOS4412_UFSTAT_RXCOUNT	(0xFF<<0)
#define EXYNOS4412_UTRSTAT_TXE	  	(1<<2)
#define EXYNOS4412_UTRSTAT_TXFE		(1<<1)
#define EXYNOS4412_UTRSTAT_RXDR		(1<<0)
#define EXYNOS4412_UERSTAT_OVERRUN	(1<<0)
#define EXYNOS4412_UERSTAT_PARITY	(1<<1)
#define EXYNOS4412_UERSTAT_FRAME	(1<<2)
#define EXYNOS4412_UERSTAT_BREAK	(1<<3)
#define EXYNOS4412_UMSTAT_CTS	  	(1<<0)
#define EXYNOS4412_UMSTAT_DCTS		(1<<4)

#define B2400    2400
#define B4800    4800
#define B9600    9600
#define B19200   19200
#define B38400   38400
#define B57600   57600
#define B115200  115200
#define B230400  230400
#define B460800  460800
#define B921600  921600

#define D_BITS_5 5
#define D_BITS_6 6
#define D_BITS_7 7
#define D_BITS_8 8
#define D_BITS_9 9

#define S_BITS_1 0
#define S_BITS_2 1
#define S_BITS_3 2
#define S_BITS_4 3

#define P_NONE 0
#define P_ODD  1
#define P_EVEN 2

unsigned int exynos4412_serial_init(int ch,unsigned int brand,unsigned int data,unsigned int parity,unsigned int stop);
unsigned int exynos4412_serial_write(int ch,unsigned char * buf, unsigned int count);
unsigned int exynos4412_serial_read(int ch, unsigned char * buf, unsigned int count);
unsigned int exynos4412_generate_interrupt(int ch);


#endif
