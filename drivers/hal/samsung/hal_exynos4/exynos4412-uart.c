#include <exynos4412-uart.h>
#include <exynos4412-gpio.h>
#include <exynos4412-clk.h>

unsigned int exynos4412_serial_setup(int ch, unsigned int baud, unsigned int data, unsigned int parity, unsigned int stop)
{
    const unsigned int udivslot_code[16] = {0x0000, 0x0080, 0x0808, 0x0888,
        0x2222, 0x4924, 0x4a52, 0x54aa,
        0x5555, 0xd555, 0xd5d5, 0xddd5,
        0xdddd, 0xdfdd, 0xdfdf, 0xffdf};
    unsigned int ibaud, baud_div_reg, baud_divslot_reg;
    unsigned char data_bit_reg, parity_reg, stop_bit_reg;
    unsigned long long sclk_uart;

    switch(baud)
    {
        case B2400:
            ibaud = 2400;
            break;
        case B4800:
            ibaud = 4800;
            break;
        case B9600:
            ibaud = 9600;
            break;
        case B19200:
            ibaud = 19200;
            break;
        case B38400:
            ibaud = 38400;
            break;
        case B57600:
            ibaud = 57600;
            break;
        case B115200:
            ibaud = 115200;
            break;
        case B230400:
            ibaud = 230400;
            break;
        case B460800:
            ibaud = 460800;
            break;
        case B921600:
            ibaud = 921600;
            break;
        default:
            return 1;
    }

    switch(data)
    {
        case D_BITS_5:
            data_bit_reg = 0x0;		break;
        case D_BITS_6:
            data_bit_reg = 0x1;		break;
        case D_BITS_7:
            data_bit_reg = 0x2;		break;
        case D_BITS_8:
            data_bit_reg = 0x3;		break;
        default:
            return 1;
    }

    switch(parity)
    {
        case P_NONE:
            parity_reg = 0x0;		break;
        case P_EVEN:
            parity_reg = 0x5;		break;
        case P_ODD:
            parity_reg = 0x4;		break;
        default:
            return 1;
    }

    switch(stop)
    {
        case S_BITS_1:
            stop_bit_reg = 0;		break;
        case S_BITS_2:
            stop_bit_reg = 1;		break;
        default:
            return 1;
    }

    switch(ch)
    {
        case 0:
            if(exynos4412_clk_get_rate("sclk_uart0", &sclk_uart))
                return 1;
            break;
        case 1:
            if(exynos4412_clk_get_rate("sclk_uart1", &sclk_uart))
                return 1;
            break;
        case 2:
            if(exynos4412_clk_get_rate("sclk_uart2", &sclk_uart))
                return 1;
            break;
        case 3:
            if(exynos4412_clk_get_rate("sclk_uart3", &sclk_uart))
                return 1;
            break;
        default:
            return 1;
    }

    baud_div_reg = (unsigned int)((sclk_uart / (ibaud * 16)) ) - 1;
    baud_divslot_reg = udivslot_code[( (unsigned int)((sclk_uart % (ibaud*16)) / ibaud) ) & 0xf];

    switch(ch)
    {
        case 0:
            writel(baud_div_reg,EXYNOS4412_UART0_BASE + EXYNOS4412_UBRDIV);
            writel(baud_divslot_reg,EXYNOS4412_UART0_BASE + EXYNOS4412_UFRACVAL);
            writel((data_bit_reg<<0 | stop_bit_reg <<2 | parity_reg<<3),EXYNOS4412_UART0_BASE + EXYNOS4412_ULCON);
            break;
        case 1:
            writel(baud_div_reg,EXYNOS4412_UART1_BASE + EXYNOS4412_UBRDIV);
            writel(baud_divslot_reg,EXYNOS4412_UART1_BASE + EXYNOS4412_UFRACVAL);
            writel((data_bit_reg<<0 | stop_bit_reg <<2 | parity_reg<<3),EXYNOS4412_UART1_BASE + EXYNOS4412_ULCON);
            break;
        case 2:
            writel(baud_div_reg,EXYNOS4412_UART2_BASE + EXYNOS4412_UBRDIV);
            writel(baud_div_reg,EXYNOS4412_UART2_BASE + EXYNOS4412_UFRACVAL);
            writel((data_bit_reg<<0 | stop_bit_reg <<2 | parity_reg<<3),EXYNOS4412_UART2_BASE + EXYNOS4412_ULCON);
            break;
        case 3:
            writel(baud_div_reg,EXYNOS4412_UART3_BASE + EXYNOS4412_UBRDIV);
            writel(baud_divslot_reg,EXYNOS4412_UART3_BASE + EXYNOS4412_UFRACVAL);
            writel((data_bit_reg<<0 | stop_bit_reg <<2 | parity_reg<<3),EXYNOS4412_UART3_BASE + EXYNOS4412_ULCON);
            break;
        default:
            return 1;
    }

    return 0;
}

unsigned int exynos4412_serial_read(int ch, unsigned char * buf, unsigned int count)
{
    unsigned int base = EXYNOS4412_UART0_BASE;
    unsigned int i;

    switch(ch)
    {
        case 0:
            base = EXYNOS4412_UART0_BASE;
            break;
        case 1:
            base = EXYNOS4412_UART1_BASE;
            break;
        case 2:
            base = EXYNOS4412_UART2_BASE;
            break;
        case 3:
            base = EXYNOS4412_UART3_BASE;
            break;
        default:
            break;
    }

    for(i = 0; i < count; i++)
    {
        if( (readl(base + EXYNOS4412_UTRSTAT) & EXYNOS4412_UTRSTAT_RXDR) )
            buf[i] = readb(base + EXYNOS4412_URXH);
        else
            break;
    }

    return i;
}

unsigned int exynos4412_serial_write(int ch,unsigned char * buf, unsigned int count)
{
    unsigned int base = EXYNOS4412_UART0_BASE;
    unsigned int i;

    switch(ch)
    {
        case 0:
            base = EXYNOS4412_UART0_BASE;
            break;
        case 1:
            base = EXYNOS4412_UART1_BASE;
            break;
        case 2:
            base = EXYNOS4412_UART2_BASE;
            break;
        case 3:
            base = EXYNOS4412_UART3_BASE;
            break;
        default:
            break;
    }

    for(i = 0; i < count; i++)
    {
        while( !(readl(base + EXYNOS4412_UTRSTAT) & EXYNOS4412_UTRSTAT_TXFE) );
        writeb(buf[i],base + EXYNOS4412_UTXH);
    }

    return i;
}

unsigned int exynos4412_generate_interrupt(int ch)
{
    unsigned int base = EXYNOS4412_UART0_BASE;

    switch(ch)
    {
        case 0:
            base = EXYNOS4412_UART0_BASE;
            break;
        case 1:
            base = EXYNOS4412_UART1_BASE;
            break;
        case 2:
            base = EXYNOS4412_UART2_BASE;
            break;
        case 3:
            base = EXYNOS4412_UART3_BASE;
            break;
        default:
            break;
    }

    writel(readl(base + EXYNOS4412_UINTP),base + EXYNOS4412_UINTP); 

    return 0;
}

unsigned int exynos4412_serial_init(int ch,unsigned int brand,unsigned int data,unsigned int parity,unsigned int stop)
{
    switch(ch)
    {
        case 0:
            /* Configure GPA01, GPA00 for TXD0, RXD0 and pull up */
            writel((readl(GpioPortA0 + GPIO_CON_OFFSET) & ~(0xf<<4)) | (0x2<<4),GpioPortA0 + GPIO_CON_OFFSET);
            writel((readl(GpioPortA0 + GPIO_CON_OFFSET) & ~(0xf<<0)) | (0x2<<0),GpioPortA0 + GPIO_CON_OFFSET);
            writel((readl(GpioPortA0 + GPIO_PUD_OFFSET) & ~(0x3<<2)) | (0x2<<2),GpioPortA0 + GPIO_PUD_OFFSET);
            writel((readl(GpioPortA0 + GPIO_PUD_OFFSET) & ~(0x3<<0)) | (0x2<<0),GpioPortA0 + GPIO_PUD_OFFSET);

            writel(0x00000185,EXYNOS4412_UART0_BASE + EXYNOS4412_UCON);
            writel((1<<0) | (7<<4),EXYNOS4412_UART0_BASE + EXYNOS4412_UFCON);
            writel(0x00000000,EXYNOS4412_UART0_BASE + EXYNOS4412_UMCON);
            writel(0x00000004,EXYNOS4412_UART0_BASE + EXYNOS4412_UINTM);    
            break;

        case 1:
            /* Configure GPA05, GPA04 for TXD1, RXD1 */
            writel((readl(GpioPortA0 + GPIO_CON_OFFSET) & ~(0xf<<20)) | (0x2<<20),GpioPortA0 + GPIO_CON_OFFSET);
            writel((readl(GpioPortA0 + GPIO_CON_OFFSET) & ~(0xf<<16)) | (0x2<<16),GpioPortA0 + GPIO_CON_OFFSET);
            writel((readl(GpioPortA0 + GPIO_PUD_OFFSET) & ~(0x3<<10)) | (0x2<<10),GpioPortA0 + GPIO_PUD_OFFSET);
            writel((readl(GpioPortA0 + GPIO_PUD_OFFSET) & ~(0x3<<8)) | (0x2<<8),GpioPortA0 + GPIO_PUD_OFFSET);

            writel(0x00000185,EXYNOS4412_UART1_BASE + EXYNOS4412_UCON);
            writel((1<<0) | (7<<4),EXYNOS4412_UART1_BASE + EXYNOS4412_UFCON);
            writel(0x00000000,EXYNOS4412_UART1_BASE + EXYNOS4412_UMCON);
            writel(0x00000004,EXYNOS4412_UART1_BASE + EXYNOS4412_UINTM);    
            break;

        case 2:
            /* Configure GPA11, GPA10 for TXD2, RXD2 */
            writel((readl(GpioPortA1 + GPIO_CON_OFFSET) & ~(0xf<<4)) | (0x2<<4),GpioPortA1 + GPIO_CON_OFFSET);
            writel((readl(GpioPortA1 + GPIO_CON_OFFSET) & ~(0xf<<0)) | (0x2<<0),GpioPortA1 + GPIO_CON_OFFSET);
            writel((readl(GpioPortA1 + GPIO_PUD_OFFSET) & ~(0x3<<2)) | (0x2<<2),GpioPortA1 + GPIO_PUD_OFFSET);
            writel((readl(GpioPortA1 + GPIO_PUD_OFFSET) & ~(0x3<<0)) | (0x2<<0),GpioPortA1 + GPIO_PUD_OFFSET);

            writel(0x00000185,EXYNOS4412_UART2_BASE + EXYNOS4412_UCON);
            writel((1<<0) | (7<<4),EXYNOS4412_UART2_BASE + EXYNOS4412_UFCON);
            writel(0x00000000,EXYNOS4412_UART2_BASE + EXYNOS4412_UMCON);
            writel(0x00000004,EXYNOS4412_UART2_BASE + EXYNOS4412_UINTM);    
            break;

        case 3:
            /* Configure GPA15, GPA14 for TXD3, RXD3 */
            writel((readl(GpioPortA1 + GPIO_CON_OFFSET) & ~(0xf<<20)) | (0x2<<20),GpioPortA1 + GPIO_CON_OFFSET);
            writel((readl(GpioPortA1 + GPIO_CON_OFFSET) & ~(0xf<<16)) | (0x2<<16),GpioPortA1 + GPIO_CON_OFFSET);
            writel((readl(GpioPortA1 + GPIO_PUD_OFFSET) & ~(0x3<<10)) | (0x2<<10),GpioPortA1 + GPIO_PUD_OFFSET);
            writel((readl(GpioPortA1 + GPIO_PUD_OFFSET) & ~(0x3<<8)) | (0x2<<8),GpioPortA1 + GPIO_PUD_OFFSET);

            writel(0x00000185,EXYNOS4412_UART3_BASE + EXYNOS4412_UCON);
            writel((1<<0) | (7<<4),EXYNOS4412_UART3_BASE + EXYNOS4412_UFCON);
            writel(0x00000000,EXYNOS4412_UART3_BASE + EXYNOS4412_UMCON);
            writel(0x00000004,EXYNOS4412_UART3_BASE + EXYNOS4412_UINTM);    
            break;

        default:
            return 1;
    }

    return exynos4412_serial_setup(ch, brand, data, parity, stop);
}


