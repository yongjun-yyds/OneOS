#include <exynos4412-gpio.h>

void writel(unsigned int value, unsigned int address)
{
    *((volatile unsigned int *) address) = value;
}

void writeb(unsigned char value, unsigned int address)
{
    *((volatile unsigned char *) address) = value;
}

unsigned char readb(unsigned int address)
{
    return *((volatile unsigned int *)address);
}

unsigned int readl(unsigned int address)
{
    return *((volatile unsigned int *)address);
}

unsigned int Gpio_Init(unsigned int enPort, unsigned int enPin, stc_gpio_cfg_t  *pstcGpioCfg)
{
    unsigned int value = 0;

    value = readl(enPort+GPIO_CON_OFFSET);

    value &= ~(0xF<<(enPin*4));

    if(pstcGpioCfg->enDir == GpioDirOut)
    {
        value |= (1<<(enPin*4));
    }     
    writel(value,(enPort+GPIO_CON_OFFSET));

    value = readl(enPort+GPIO_PUD_OFFSET);

    value &= ~(3<<(enPin*2));

    if(pstcGpioCfg->enPd == GpioPdEnable)
    {
        value |= (1<<(enPin*2));
    }
    else if(pstcGpioCfg->enPu == GpioPuEnable)
    {
        value |= (3<<(enPin*2));
    }
    writel(value,(enPort+GPIO_PUD_OFFSET));

    return 0;
}

unsigned int Gpio_WriteOutputIO(unsigned int enPort, unsigned int enPin, boolean_t bVal)
{

    unsigned int value = 0;

    value = readl(enPort+GPIO_DAT_OFFSET);

    if(bVal == 1)
    {
        value |= (1<<enPin);
    }
    else if(bVal == 0)
    {
        value &= ~(1<<enPin);
    }
    writel(value,(enPort+GPIO_DAT_OFFSET));
    return 0;
}

boolean_t Gpio_GetInputIO(unsigned int enPort, unsigned int enPin)
{
    unsigned int value = 0;

    value = readl(enPort+GPIO_DAT_OFFSET);

    return (value >> enPin) & 0x1;
}

unsigned int Gpio_EnableIrq(unsigned int base,unsigned int irqcon,unsigned int irqmask,unsigned int irqpend,unsigned int pin,en_gpio_irqtype_t mode)
{

    unsigned int value = 0;

    if(!base || !irqcon || !irqmask || !irqpend)
    {
        return 1;
    }
    value = readl(base+GPIO_CON_OFFSET);
    value &= ~(0xf << (4*pin));
    value |= (0xf << (4*pin));
    writel(value,(base+GPIO_CON_OFFSET));

    value = readl(base+GPIO_PUD_OFFSET);
    value &= ~(0x3 << (2*pin));
    value |= (0x2 << (2*pin));
    writel(value,(base+GPIO_PUD_OFFSET));

    value = readl(irqcon);
    value &= ~(0x7 << (4*pin));
    if(mode == GpioIrqHigh)
    {
        value |= (0x1 << (4*pin));
    }
    else if(mode == GpioIrqLow)
    {
        value |= (0x0 << (4*pin));
    }
    else if(mode == GpioIrqRising)
    {
        value |= (0x3 << (4*pin));
    }
    else if(mode == GpioIrqFalling)
    {
        value |= (0x2 << (4*pin));
    }
    else if(mode == GpioIrqBoth)
    {
        value |= (0x4 << (4*pin));
    }
    else
    {
        return 1;
    }
    writel(value,(irqcon));

    value = readl(irqmask);
    value &= ~(0x1 << (1*pin));
    writel(value,irqmask);
    return 0;
}

unsigned int Gpio_DisableIrq(unsigned int irqmask,unsigned int pin)
{
    unsigned int value = 0;

    value = readl(irqmask);
    value |= (0x1 << (pin));
    writel(value,irqmask);

    return 0;

}

unsigned int Gpio_GetIrqStatus(unsigned int irqpend,unsigned int pin)
{
    unsigned int value = 0;

    value = readl(irqpend);

    return (value>>pin)&1u;

}

unsigned int Gpio_ClearIrq(unsigned int irqpend,unsigned int pin)
{
    unsigned int value = 0;

    value = readl(irqpend);
    value |= (0x1 << (pin));
    writel(value,irqpend);

    return 0;

}

void Gpio_ServerceGroupIrq(unsigned int base,unsigned int *group,unsigned int *pin)
{
    unsigned int value = 0;

    value = readl(base);

    *pin = value & 0x7;

    *group = (value >> 3) & 0x1F;

    return;
}

