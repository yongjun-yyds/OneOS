#ifndef __EXYNOS_4412_GPIO_H_
#define __EXYNOS_4412_GPIO_H_

#define GPIO_CON_OFFSET                  (0)
#define GPIO_DAT_OFFSET                  (0x4)
#define GPIO_PUD_OFFSET                  (0x8)
#define GPIO_DRV_OFFSET                  (0xC)
#define GPIO_CONPDN_OFFSET               (0x10)
#define GPIO_PUDPDN_OFFSET               (0x14)


#define GpioPortA0   (0x11400000)
#define GpioPortA1   (0x11400020)
#define GpioPortB    (0x11400040)
#define GpioPortC0   (0x11400060)
#define GpioPortC1   (0x11400080)
#define GpioPortD0   (0x114000A0)
#define GpioPortD1   (0x114000C0)
#define GpioPortF0   (0x11400180)
#define GpioPortF1   (0x114001A0)
#define GpioPortF2   (0x114001C0)
#define GpioPortF3   (0x114001E0)
#define GpioPortJ0   (0x11400240)
#define GpioPortJ1   (0x11400260)
#define GpioPortK0   (0x11000040)
#define GpioPortK1   (0x11000060)
#define GpioPortK2   (0x11000080)
#define GpioPortK3   (0x110000A0)
#define GpioPortL0   (0x110000C0)
#define GpioPortL1   (0x110000E0)
#define GpioPortL2   (0x11000100)
#define GpioPortY0   (0x11000120)
#define GpioPortY1   (0x11000140)
#define GpioPortY2   (0x11000160)
#define GpioPortY3   (0x11000180)
#define GpioPortY4   (0x110001A0)
#define GpioPortY5   (0x110001C0)
#define GpioPortY6   (0x110001E0)
#define GpioPortM0   (0x11000260)
#define GpioPortM1   (0x11000280)
#define GpioPortM2   (0x110002A0)
#define GpioPortM3   (0x110002C0)
#define GpioPortM4   (0x110002E0)
#define GpioPortX0   (0x11000C00)
#define GpioPortX1   (0x11000C20)
#define GpioPortX2   (0x11000C40)
#define GpioPortX3   (0x11000C60)
#define GpioPortZ    (0x03860000)
#define GpioPortV0   (0x106E0000)
#define GpioPortV1   (0x106E0020)
#define GpioPortV2   (0x106E0060)
#define GpioPortV3   (0x106E0080)
#define GpioPortV4   (0x106E00C0)

#define GpioIrqConA0   (0x11400700)
#define GpioIrqConA1   (0x11400704)
#define GpioIrqConB    (0x11400708)
#define GpioIrqConC0   (0x1140070C)
#define GpioIrqConC1   (0x11400710)
#define GpioIrqConD0   (0x11400714)
#define GpioIrqConD1   (0x11400718)
#define GpioIrqConF0   (0x11400730)
#define GpioIrqConF1   (0x11400734)
#define GpioIrqConF2   (0x11400738)
#define GpioIrqConF3   (0x1140073C)
#define GpioIrqConJ0   (0x11400740)
#define GpioIrqConJ1   (0x11400744)
#define GpioIrqConK0   (0x11000708)
#define GpioIrqConK1   (0x1100070C)
#define GpioIrqConK2   (0x11000710)
#define GpioIrqConK3   (0x11000714)
#define GpioIrqConL0   (0x11000718)
#define GpioIrqConL1   (0x1100071C)
#define GpioIrqConL2   (0x11000720)
#define GpioIrqConY0   (0)
#define GpioIrqConY1   (0)
#define GpioIrqConY2   (0)
#define GpioIrqConY3   (0)
#define GpioIrqConY4   (0)
#define GpioIrqConY5   (0)
#define GpioIrqConY6   (0)
#define GpioIrqConM0   (0x11000724)
#define GpioIrqConM1   (0x11000728)
#define GpioIrqConM2   (0x1100072C)
#define GpioIrqConM3   (0x11000730)
#define GpioIrqConM4   (0x11000734)
#define GpioIrqConX0   (0x11000E00)
#define GpioIrqConX1   (0x11000E04)
#define GpioIrqConX2   (0x11000E08)
#define GpioIrqConX3   (0x11000E0C)
#define GpioIrqConZ    (0)
#define GpioIrqConV0   (0x106E0700)
#define GpioIrqConV1   (0x106E0704)
#define GpioIrqConV2   (0x106E0708)
#define GpioIrqConV3   (0x106E070C)
#define GpioIrqConV4   (0x106E0710)

#define GpioIrqMaskA0   (0x11400900)
#define GpioIrqMaskA1   (0x11400904)
#define GpioIrqMaskB    (0x11400908)
#define GpioIrqMaskC0   (0x1140090C)
#define GpioIrqMaskC1   (0x11400910)
#define GpioIrqMaskD0   (0x11400914)
#define GpioIrqMaskD1   (0x11400918)
#define GpioIrqMaskF0   (0x11400930)
#define GpioIrqMaskF1   (0x11400934)
#define GpioIrqMaskF2   (0x11400938)
#define GpioIrqMaskF3   (0x1140093C)
#define GpioIrqMaskJ0   (0x11400940)
#define GpioIrqMaskJ1   (0x11400944)
#define GpioIrqMaskK0   (0x11000908)
#define GpioIrqMaskK1   (0x1100090C)
#define GpioIrqMaskK2   (0x11000910)
#define GpioIrqMaskK3   (0x11000914)
#define GpioIrqMaskL0   (0x11000918)
#define GpioIrqMaskL1   (0x1100091C)
#define GpioIrqMaskL2   (0x11000920)
#define GpioIrqMaskY0   (0)
#define GpioIrqMaskY1   (0)
#define GpioIrqMaskY2   (0)
#define GpioIrqMaskY3   (0)
#define GpioIrqMaskY4   (0)
#define GpioIrqMaskY5   (0)
#define GpioIrqMaskY6   (0)
#define GpioIrqMaskM0   (0x11000924)
#define GpioIrqMaskM1   (0x11000928)
#define GpioIrqMaskM2   (0x1100092C)
#define GpioIrqMaskM3   (0x11000930)
#define GpioIrqMaskM4   (0x11000934)
#define GpioIrqMaskX0   (0x11000F00)
#define GpioIrqMaskX1   (0x11000F04)
#define GpioIrqMaskX2   (0x11000F08)
#define GpioIrqMaskX3   (0x11000F0C)
#define GpioIrqMaskZ    (0)
#define GpioIrqMaskV0   (0x106E0900)
#define GpioIrqMaskV1   (0x106E0904)
#define GpioIrqMaskV2   (0x106E0908)
#define GpioIrqMaskV3   (0x106E090C)
#define GpioIrqMaskV4   (0x106E0910)



#define GpioIrqPendA0   (0x11400A00)
#define GpioIrqPendA1   (0x11400A04)
#define GpioIrqPendB    (0x11400A08)
#define GpioIrqPendC0   (0x11400A0C)
#define GpioIrqPendC1   (0x11400A10)
#define GpioIrqPendD0   (0x11400A14)
#define GpioIrqPendD1   (0x11400A18)
#define GpioIrqPendF0   (0x11400A30)
#define GpioIrqPendF1   (0x11400A34)
#define GpioIrqPendF2   (0x11400A38)
#define GpioIrqPendF3   (0x11400A3C)
#define GpioIrqPendJ0   (0x11400A40)
#define GpioIrqPendJ1   (0x11400A44)
#define GpioIrqPendK0   (0x11000A08)
#define GpioIrqPendK1   (0x11000A0C)
#define GpioIrqPendK2   (0x11000A10)
#define GpioIrqPendK3   (0x11000A14)
#define GpioIrqPendL0   (0x11000A18)
#define GpioIrqPendL1   (0x11000A1C)
#define GpioIrqPendL2   (0x11000A20)
#define GpioIrqPendY0   (0)
#define GpioIrqPendY1   (0)
#define GpioIrqPendY2   (0)
#define GpioIrqPendY3   (0)
#define GpioIrqPendY4   (0)
#define GpioIrqPendY5   (0)
#define GpioIrqPendY6   (0)
#define GpioIrqPendM0   (0x11000A24)
#define GpioIrqPendM1   (0x11000A28)
#define GpioIrqPendM2   (0x11000A2C)
#define GpioIrqPendM3   (0x11000A30)
#define GpioIrqPendM4   (0x11000A34)
#define GpioIrqPendX0   (0x11000F40)
#define GpioIrqPendX1   (0x11000F44)
#define GpioIrqPendX2   (0x11000F48)
#define GpioIrqPendX3   (0x11000F4C)
#define GpioIrqPendZ    (0)
#define GpioIrqPendV0   (0x106E0A00)
#define GpioIrqPendV1   (0x106E0A04)
#define GpioIrqPendV2   (0x106E0A08)
#define GpioIrqPendV3   (0x106E0A0C)
#define GpioIrqPendV4   (0x106E0A10)

#define EXT_INT_SERVICE_XB		(0x11400B08)
#define EXT_INT_SERVICE_PEND_XB	(0x11400B0C)
#define EXT_INT_GRPFIXPRI_XB	(0x11400B10)

#define EXT_INT_SERVICE_XA		(0x11000B08)
#define EXT_INT_SERVICE_PEND_XA	(0x11000B0C)
#define EXT_INT_GRPFIXPRI_XA	(0x11000B10)

#define EXT_INT_SERVICE_XC		(0x106E0B08)
#define EXT_INT_SERVICE_PEND_XC	(0x106E0B0C)
#define EXT_INT_GRPFIXPRI_XC	(0x106E0B10)

#define EXT_INT_SERVICE_PEND_OFFSET	(0x4)

typedef unsigned char     boolean_t;

typedef enum en_gpio_dir
{
    GpioDirOut = 0u,               
    GpioDirIn  = 1u,                
}en_gpio_dir_t;

typedef enum en_gpio_pu
{
    GpioPuDisable = 0u,               
    GpioPuEnable  = 1u,              
}en_gpio_pu_t;

typedef enum en_gpio_pd
{
    GpioPdDisable = 0u,               
    GpioPdEnable  = 1u,             
}en_gpio_pd_t;

typedef enum en_gpio_od
{
    GpioOdDisable = 0u,            
    GpioOdEnable  = 1u,            
}en_gpio_od_t;

typedef enum en_gpio_irqtype
{
    GpioIrqHigh     = 0u,           
    GpioIrqLow      = 1u,          
    GpioIrqRising   = 2u,           
    GpioIrqFalling  = 3u,
    GpioIrqBoth = 4u,
}en_gpio_irqtype_t;

typedef struct
{
    unsigned char       bOutputVal;     
    en_gpio_dir_t       enDir;          
    en_gpio_pu_t        enPu;          
    en_gpio_pd_t        enPd;          
    en_gpio_od_t        enOD;           
}stc_gpio_cfg_t;
void writel(unsigned int value, unsigned int address);
void writeb(unsigned char value, unsigned int address);
unsigned char readb(unsigned int address);
unsigned int readl(unsigned int address);

unsigned int Gpio_Init(unsigned int enPort, unsigned int enPin, stc_gpio_cfg_t  *pstcGpioCfg);
unsigned int Gpio_WriteOutputIO(unsigned int enPort, unsigned int enPin, boolean_t bVal);
boolean_t Gpio_GetInputIO(unsigned int enPort, unsigned int enPin);
unsigned int Gpio_EnableIrq(unsigned int base,unsigned int irqcon,unsigned int irqmask,unsigned int irqpend,unsigned int pin,en_gpio_irqtype_t mode);
unsigned int Gpio_DisableIrq(unsigned int irqmask,unsigned int pin);
unsigned int Gpio_GetIrqStatus(unsigned int irqpend,unsigned int pin);
unsigned int Gpio_ClearIrq(unsigned int irqpend,unsigned int pin);
void Gpio_ServerceGroupIrq(unsigned int base,unsigned int *group,unsigned int *pin);


#endif
