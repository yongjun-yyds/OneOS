  /*
  ***********************************************************************************
  *
  * COPYRIGHT(c) 2021, China Mobile IOT
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of China Mobile IOT nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ***********************************************************************************
  */

#ifndef __CM_SPI_H_
#define __CM_SPI_H_
#include "cm1610.h"

/** @
  * @defgroup SPI BaudSpeed: Spi_Clk = CPU_Clk / (1<< (div+1))
  */
#define SPI_Clk_None_Div   0
#define SPI_Clk_2_Div      1
#define SPI_Clk_4_Div      2
#define SPI_Clk_8_Div      3
#define SPI_Clk_16_Div     4
#define SPI_Clk_32_Div     5
#define SPI_Clk_64_Div     6
#define SPI_Clk_128_Div    7
#define IS_SPI_Clk_Div(PRESCALER)  (PRESCALER == SPI_Clk_None_Div ||\
                                    PRESCALER == SPI_Clk_2_Div||\
                                    PRESCALER == SPI_Clk_4_Div||\
                                    PRESCALER == SPI_Clk_8_Div||\
                                    PRESCALER == SPI_Clk_16_Div||\
                                    PRESCALER == SPI_Clk_32_Div||\
                                    PRESCALER == SPI_Clk_64_Div||\
                                    PRESCALER == SPI_Clk_128_Div)

/** @
  * @defgroup SPI CPOL polarity
  */
#define  SPI_CPOL_Low         ((uint8_t)0<<4)
#define  SPI_CPOL_High	      ((uint8_t)1<<4)

#define IS_SPI_CPOL(CPOL) 		(((CPOL) == SPI_CPOL_High) || \
                                      ((CPOL) == SPI_CPOL_Low))

/** @
  * @defgroup SPI CPHA phase
  */
#define SPI_CPHA_First_Edge	  ((uint8_t)0<<5)
#define SPI_CPHA_Second_Edge	((uint8_t)1<<5)

#define IS_SPI_CPHA(CPHA) 		(((CPHA) == SPI_CPHA_First_Edge) || \
                                      ((CPHA) == SPI_CPHA_Second_Edge))

/** @
  * @defgroup SPI RW Delay
  */
#define IS_SPI_RW_Delay(RW_Delay)   (RW_Delay>0 && RW_Delay<127)

/** @
  * @defgroup SPI_MEM_SELECT
  */
#define SPI_SCHED_RAM	              ((uint8_t)0<<7)
#define SPI_64K_MRAM	              ((uint8_t)1<<7)

#define SPI_AUTO_INCR_ADDR	      	((uint8_t)1<<6)
/**
  * @brief  SPI Init structure definition
  */
typedef struct
{
  uint8_t BaudRatePrescaler;     /*!< Specifies the Baud Rate prescaler value which will be
                                      used to configure the transmit and receive SCK clock.
                                      This parameter can be a value of @ref SPI_BaudRate_Prescaler.
                                      @note The communication clock is derived from the master
                                      clock. The slave clock does not need to be set. */

  uint8_t CPOL;                  /*!< Specifies the serial clock steady state.
                                      This parameter can be a value of @ref SPI_Clock_Polarity */

  uint8_t CPHA;                  /*!< Specifies the clock active edge for the bit capture.
                                      This parameter can be a value of @ref SPI_Clock_Phase */

  uint8_t RW_Delay;              /* Specifies the Delay time between send  and receive data,the
                                    value must be 0 to 127 */
}SPI_InitTypeDef;

/**
  * @brief  Initializes the SPI peripheral according to the specified
  *         parameters in the SPI_InitStruct.
  * @param  SPI_InitStruct: pointer to a SPI_InitTypeDef structure
  *         that contains the configuration information for the specified SPI
  *         peripheral.
  * @retval None
  */
void SPI_Init(SPI_InitTypeDef* SPI_InitStruct);

/**
  * @brief  DeInit SPI with
  * @retval None
  */
void SPI_DeInit(void);

/**
  * @brief  Transmits one data via SPI DMA.
  * @param  data: the data you want transmit.
  * @retval None
  */
void SPI_SendData(uint8_t data);

/**
  * @brief  Transmits datas via SPI DMA.
  * @param  buf: pointer to a buf that contains the data you want send.
  * @param  len: the buf length
  * @retval None
  */
void  SPI_SendBuff(uint8_t *buff, int len);

/**
  * @brief  Send data first then  recerive data.
  * @param: TxBuff: pointer to a TxBuff  that contains the data you want send.
  * @param: TxLen: the length of send datas
  * @param: RxBuff: pointer to a TxBuff  that contains the data you want receive.
  * @param: RxLen: the length of receive datas
  * @retval None
  */
void SPI_SendAndReceiveData(uint8_t *TxBuff, uint16_t TxLen, uint8_t *RxBuff, uint16_t RxLen);
#endif /* __CM_SPI_H_ */

