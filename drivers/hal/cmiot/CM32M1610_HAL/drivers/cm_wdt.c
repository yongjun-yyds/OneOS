  /*
  ***********************************************************************************
  *
  * COPYRIGHT(c) 2021, China Mobile IOT
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of China Mobile IOT nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ***********************************************************************************
  */

#include "cm_wdt.h"

uint8_t WDT2_load =0;

void WDT_Kick()
{
    WDT_Enable();
    HWRITE(CORE_WDT2, WDT2_load);
}

void WDT_Init(WDT_InitTypeDef WDT_init_struct)
{
    uint8_t en_rst;
    en_rst = HREAD(CORE_ENCRYPT);

    WDT2_load = WDT_init_struct.setload;
    if (RESET_MODE == WDT_init_struct.mode)
    {
        en_rst &= ~(BIT_1);
        HWRITE(CORE_WDT2, WDT_init_struct.setload);
    }
    else if (INTR_MODE == WDT_init_struct.mode)
    {
        en_rst |= BIT_1;
        HWRITE(CORE_WDT2, WDT_init_struct.setload);
    }
    HWRITE(reg_map(CORE_ENCRYPT), en_rst);
    en_rst = HREAD(CORE_ENCRYPT);
}

void WDT_Enable()
{
    uint8_t wdt_en;
    wdt_en = HREAD(CORE_CONFIG);
    wdt_en|= 0x08;
    HWRITE(CORE_CONFIG, wdt_en);
}

void WDT_Disable()
{
    uint8_t wdt_en;
    wdt_en = HREAD(CORE_CONFIG);
    wdt_en &= ~(0x08);
    HWRITE(CORE_CONFIG, wdt_en);
}

void WDT_GetReload(uint8_t reload)
{
    WDT2_load = reload;
    HWRITE(CORE_WDT2, WDT2_load);
}

void WDT_ClearITPendingBit()
{
    WDT_Kick();
}
