  /*
  ***********************************************************************************
  *
  * COPYRIGHT(c) 2021, China Mobile IOT
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of China Mobile IOT nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ***********************************************************************************
  */
#include "cm_drv_common.h"
#include "cm_def.h"

/*************************COMMON FUNC***************************/
#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof((arr)[0]))

void HW_REG_24BIT(uint32_t reg, uint32_t data)
{
    HW_REG_8BIT(reg, data & 0xFF);
    hw_delay();
    HW_REG_8BIT(reg + 1,(data >> 8)&0xff );
    hw_delay();
    HW_REG_8BIT(reg + 2,(data >> 16)&0xff );
}

void HW_REG_16BIT(uint32_t reg, uint16_t word)
{
    HW_REG_8BIT(reg, word & 0x00FF);
    hw_delay();
    HW_REG_8BIT(reg + 1,(word >> 8));
}
uint16_t HR_REG_16BIT(uint32_t reg)
{
    uint16_t return_data = 0;
    uint16_t H_data = 0;
    hw_delay();
    return_data = HR_REG_8BIT(reg);
    hw_delay();
    H_data = HR_REG_8BIT(reg + 1);
    return_data = (return_data | ((H_data << 8) & 0xFF00));
    return return_data;
}

uint32_t HR_REG_24BIT(uint32_t reg)
{
    uint32_t return_data = 0;
    hw_delay();
    return_data = HR_REG_8BIT(reg);
    hw_delay();
    return_data = return_data |(HR_REG_8BIT(reg + 1)<<8);
    hw_delay();
    return_data = return_data | (HR_REG_8BIT(reg + 2)<<16);

    return return_data;
}

void xmemcpy(uint8_t* dest,const uint8_t* src, uint16_t len)
{
    for(int i = 0; i<len;i++)
    {
        HWRITE((int)(dest+i),(HREAD((int)src+i)));
    }
    return;
}

uint16_t xstrlen(const char *s)
{
    int n;
    for(n=0;*s!='\0';s++) {
            n++;
      }
        return n;
}

void error_handle()
{
    OS_ENTER_CRITICAL();
    while(1);
}

void Lpm_LockLpm(uint16_t lpmNum)
{
    uint16_t temp;
    temp = HR_REG_16BIT(reg_map(M0_LPM_REG))|lpmNum;
    HW_REG_16BIT(reg_map(M0_LPM_REG),temp);
}

void Lpm_unLockLpm(uint16_t lpmNum)
{
    uint16_t temp;
    temp = HR_REG_16BIT(reg_map(M0_LPM_REG)) &(~lpmNum);
    HW_REG_16BIT(reg_map(M0_LPM_REG),temp);
}

BOOL Lpm_CheckLpmFlag(void)
{
    return NONE_LPM_FLAG == HR_REG_16BIT(reg_map(M0_LPM_REG));
}

void whileDelay(int delayValue)
{
    int delayMS;
    for (int k=0; k < delayValue; k++){
        delayMS = 20000;
        while(delayMS--);
    }
}
void whileDelayshort(int delayValue)
{
    int delayMS;
    for (int k=0; k < delayValue; k++){
        delayMS = 2000;
        while(delayMS--);
    }
}

BOOL xramcmp(volatile uint8_t *op1,volatile uint8_t *op2,int len){
    for(int i = 0; i<len;i++){
        if (HREAD((int)op1+i) != HREAD((int)op2+i))
            return FALSE;
    }
    return TRUE;
}

void xramcpy(volatile uint8_t *des,volatile uint8_t *src,int len){
    for(int i = 0; i<len;i++)
    {
        HWRITE(((int)des+i),(HREAD((int)src+i)));
    }
}

uint32_t math_abs(int32_t value)
{
    return (value < 0)?(-value):value;
}

