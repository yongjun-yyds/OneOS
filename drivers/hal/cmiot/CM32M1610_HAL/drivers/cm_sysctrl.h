  /*
  ***********************************************************************************
  *
  * COPYRIGHT(c) 2021, China Mobile IOT
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of China Mobile IOT nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ***********************************************************************************
  */

#ifndef __CM_SYSCTRL_H__
#define __CM_SYSCTRL_H__

#include "cm1610.h"

#ifdef __cplusplus
extern "C" {
#endif

/*======AHB_control====*/
#define SYSCTRL_AHBPeriph_AUTH_ROM    REG_CLOCK_OFF_AUTH_ROM
#define SYSCTRL_AHBPeriph_EFUSE       REG_CLOCK_OFF_EFUSE
#define SYSCTRL_AHBPeriph_DEBUG_UART  REG_CLOCK_OFF_DEBUG_UART
#define SYSCTRL_AHBPeriph_ITRACE      REG_CLOCK_OFF_ITRACE
#define SYSCTRL_AHBPeriph_QSPI        REG_CLOCK_OFF_QSPI
#define SYSCTRL_AHBPeriph_AUDIO       REG_CLOCK_OFF_AUDIO_DAC
#define SYSCTRL_AHBPeriph_VOICE       REG_CLOCK_OFF_VOICE_FILTER
#define SYSCTRL_AHBPeriph_I2C         REG_CLOCK_OFF_I2C
#define SYSCTRL_AHBPeriph_CM0         REG_CLOCK_OFF_CM0
#define SYSCTRL_AHBPeriph_MRAM        REG_CLOCK_OFF_MRAM
#define SYSCTRL_AHBPeriph_USB         REG_CLOCK_OFF_USB
#define SYSCTRL_AHBPeriph_SBC         REG_CLOCK_OFF_SBC
#define SYSCTRL_AHBPeriph_SPI         REG_CLOCK_OFF_SPI
#define SYSCTRL_AHBPeriph_SDIO        REG_CLOCK_OFF_SDIO
#define SYSCTRL_AHBPeriph_I2S         REG_CLOCK_OFF_I2S
#define SYSCTRL_AHBPeriph_UART        REG_CLOCK_OFF_UART
#define IS_SYSCTRL_AHB_PERIPH(PERIPH) ((PERIPH &0xffff) != 0)

/* system clock frequency*/
typedef enum
{
    CLOCK_24M = 24,      // The clock source is crystal
    CLOCK_29M = 29,      // The clock source is rc
    CLOCK_48M = 48,
    CLOCK_96M = 96,
    CLOCK_153M = 153,
    CLOCK_192M = 192,
    CLOCK_32K = 32
}Sysclk_TypeDef;

/* system clock source*/
typedef enum
{
    Sysclk_crystal= 0,
    Sysclk_dpll,
    Sysclk_rc,
    Sysclk_lpoclk
} SysClkSource_TypeDef;

/**
  * @brief  Enables or disables the APB peripheral clock.
  * @param  SYSCTRL_APBPeriph: specifies the APB peripheral to gates its clock.
  *
  *         For @b this parameter can be any combination
  *         of the enum SYSCTRL_AHB_PERIPH.
  * @param  NewState: new state of the specified peripheral clock.
  *         This parameter can be: ENABLE or DISABLE.
  * @retval None
  */

void SYSCTRL_AHBPeriphClockCmd(uint16_t SYSCTRL_AHBPeriph, FunctionalState NewState);

/**
  * @brief  Config CPU Enter sleep mode
  * @param  SleepMode_TypeDef: Select SleepMode.
  * @retval None
  */

void SYSCTRL_ClkSourceSel(SysClkSource_TypeDef source);

/**
  * @brief  Config CLK
  * @param  CLK_multiple: multiple value
  * @retval None
  */

void  SYSCTRL_CLKConfig(Sysclk_TypeDef CLK_multiple);

/**
  * @brief  Get system clock frequency
  * @param  None
  * @retval CPU_MHZ
  */
uint32_t SYSCTRL_GetSystemClock(void);

/**
 * @brief Blocking delay
 * @param ms : the delay time, Unit of microseconds
 * @retval none
 */
void delay_us(uint32_t us);

/**
 * @brief Blocking delay
 * @param ms : the delay time,Unit of milliseconds
 * @retval none
 */
void delay_ms(uint32_t ms);

/**
 * @brief calibration and get RC clock
 * @param none
 * @retval RC clock(HZ)
 */
uint32_t SYSCTRL_GetRcClk(void);

/**
 * @brief force calibration RC clock
 * @param none
 * @retval none
 */
void SYSCTRL_CalibrationRcClk(void);

/**
 * @brief ICE function switch
 * @param ICE switch status
 * @retval none
 */
void SYSCTRL_IceCmd(FunctionalState NewState);

/**
 * @brief Reset system
 * @param none
 * @retval none
 */
void SYSCTRL_SoftReset(void);

#endif      /*__SYSCTRL_H*/
