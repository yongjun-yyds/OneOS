  /*
  ***********************************************************************************
  *
  * COPYRIGHT(c) 2021, China Mobile IOT
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of China Mobile IOT nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ***********************************************************************************
  */

#ifndef __CM_GPIO_H_
#define __CM_GPIO_H_
#include "cm1610.h"

#define GPIO_GROUP_NUM      4
#define GPIO_PIN_NUM        8

#define ISGPIOGROUP(groupx)    (groupx<GPIO_GROUP_NUM)

#define IS_GPIO_PIN(PIN) (((((PIN) & ~(uint8_t)0xFF)) == 0x00) && ((PIN) != (uint8_t)0x00))

#define IS_GPIO_MODE(mode) (((mode) == GPIO_Mode_IN_FLOATING)||\
                            ((mode) == GPIO_Mode_IPU)||\
                            ((mode)==GPIO_Mode_IPD)||\
                            ((mode)==GPIO_Mode_Out_PP)||\
                            ((mode)==GPIO_Mode_AIN))

/*gpio pin define*/
#define GPIO_Pin_0      ((uint8_t)0x01)  /*!< Pin 0 selected */
#define GPIO_Pin_1      ((uint8_t)0x02)  /*!< Pin 1 selected */
#define GPIO_Pin_2      ((uint8_t)0x04)  /*!< Pin 2 selected */
#define GPIO_Pin_3      ((uint8_t)0x08)  /*!< Pin 3 selected */
#define GPIO_Pin_4      ((uint8_t)0x10)  /*!< Pin 4 selected */
#define GPIO_Pin_5      ((uint8_t)0x20)  /*!< Pin 5 selected */
#define GPIO_Pin_6      ((uint8_t)0x40)  /*!< Pin 6 selected */
#define GPIO_Pin_7      ((uint8_t)0x80)  /*!< Pin 7 selected */

#define IS_GET_GPIO_PIN(PIN) (((PIN) == GPIO_Pin_0) || \
                              ((PIN) == GPIO_Pin_1) || \
                              ((PIN) == GPIO_Pin_2) || \
                              ((PIN) == GPIO_Pin_3) || \
                              ((PIN) == GPIO_Pin_4) || \
                              ((PIN) == GPIO_Pin_5) || \
                              ((PIN) == GPIO_Pin_6) || \
                              ((PIN) == GPIO_Pin_7))

typedef enum
{
GPIO_MODE_INPUT         =     0,
GPIO_MODE_QSPI_NCS      =     2,
GPIO_MODE_QSPI_SCK      =     3,
GPIO_MODE_QSPI_IO0      =     4,
GPIO_MODE_QSPI_IO1      =     5,
GPIO_MODE_QSPI_IO2      =     6,
GPIO_MODE_QSPI_IO3      =     7,
GPIO_MODE_UART_TXD      =     8,
GPIO_MODE_UART_RXD      =     9,
GPIO_MODE_UART_RTS      =     10,
GPIO_MODE_UART_CTS      =     11,
GPIO_MODE_UARTB_TXD     =     12,
GPIO_MODE_UARTB_RXD     =     13,
GPIO_MODE_UARTB_RTS     =     14,
GPIO_MODE_UARTB_CTS     =     15,
GPIO_MODE_PWM_OUT       =     16,
GPIO_MODE_I2S_DOUT      =     24,
GPIO_MODE_I2S_LRCKOUT   =     25,
GPIO_MODE_I2S_CLKOUT    =     26,
GPIO_MODE_I2S_DIN       =     28,
GPIO_MODE_I2S_LRCKIN    =     29,
GPIO_MODE_I2S_CLKIN     =     30,
GPIO_MODE_SPID_MISO     =     31,
GPIO_MODE_SPID_NCS      =     32,
GPIO_MODE_SPID_SCK      =     33,
GPIO_MODE_SPID_MOSI     =     34,
GPIO_MODE_SPID_SDIO     =     35,
GPIO_MODE_QDEC_X0       =     38,
GPIO_MODE_QDEC_X1       =     39,
GPIO_MODE_QDEC_Y0       =     40,
GPIO_MODE_QDEC_Y1       =     41,
GPIO_MODE_QDEC_Z0       =     42,
GPIO_MODE_QDEC_Z1       =     43,
GPIO_MODE_IIC_SCL       =     44,
GPIO_MODE_IIC_SDA       =     45,
GPIO_MODE_JTAG_SWCLK    =     60,
GPIO_MODE_JTAG_SWDAT    =     61,
GPIO_MODE_OUTPUT_LOW    =     62,
GPIO_MODE_OUTPUT_HIGH   =     63,
GPIO_MODE_PULLUP        =     0x40,
GPIO_MODE_PULLDOWN      =     0x80,
GPIO_MODE_ANALOG        =     0xc0
} GPIO_FunTypeDef;

typedef enum
{
    GPIOA,
    GPIOB,
    GPIOC,
    GPIOD
} GPIO_TypeDef;

typedef enum
{
    Bit_RESET,
    Bit_SET
} BitAction;

typedef enum
{
    GPIO_Mode_IN_FLOATING = 0x01,
    GPIO_Mode_IPU         = 0x02,
    GPIO_Mode_IPD         = 0x03,
    GPIO_Mode_AIN         = 0x04,/*!<  analog signal mode */
    GPIO_Mode_Out_PP      = 0x05/*!<  defult output low */
} GPIO_ModeTypeDef;

typedef struct
{
    uint8_t         GPIO_Pin;/*gpio pin define*/
    GPIO_ModeTypeDef GPIO_Mode;
} GPIO_InitTypeDef;

/**
 * @brief  Configure gpio function (the same group of GPIO, multiple pins can be configured at one time)
 *
 * @param  GPIOx: where x can be (GPIOA...GPIOD) to select the GPIO group.
 *
 * @param  GPIO_Pin: select the pin to config.(GPIO_Pin_0...GPIO_Pin_7)(More than one can be configured at a time)
 *
 * @param  function:GPIO function, pull-up and pull-down can be combined with other functions or operations when passing parameters.
 *
 * @retval none
 */
void GPIO_Config(GPIO_TypeDef GPIOx, uint8_t GPIO_Pin, GPIO_FunTypeDef function);

/**
 * @brief  gpio mode Init
 *
 * @param  GPIOx: where x can be (GPIOA...GPIOD) to select the GPIO group.
 *
 * @param  GPIO_InitStruct:GPIO_InitStruct
 *
 * @retval none
 */
void GPIO_Init(GPIO_TypeDef GPIOx, GPIO_InitTypeDef *GPIO_InitStruct);

/**
  * @brief  GPIO pull-up function
  *
  * @param  GPIOx: where x can be (GPIOA...GPIOD) to select the GPIO group.
  *
  * @param  GPIO_Pin:  select the pin to read.(GPIO_Pin_0...GPIO_Pin_7)
  *
  * @param  NewState: new state of the port pin Pull Up.(ENABLE or DISABLE)
  *
  * @retval
  */
void GPIO_PullUpCmd(GPIO_TypeDef GPIOx, uint8_t GPIO_Pin, FunctionalState NewState);

/**
  * @brief  GPIO pull-down function
  *
  * @param  GPIOx: where x can be (GPIOA...GPIOD) to select the GPIO group.
  *
  * @param  GPIO_Pin:  select the pin to read.(GPIO_Pin_0...GPIO_Pin_7)
  *
  * @param  NewState: new state of the port pin Pull Down.(ENABLE or DISABLE)
  *
  * @retval
  */
void GPIO_PullDownCmd(GPIO_TypeDef GPIOx, uint8_t GPIO_Pin, FunctionalState NewState);

/**
 * @brief  Reads the GPIO input data(status) for bit.
 *
 * @param  GPIOx: where x can be (GPIOA...GPIOD) to select the GPIO group.
 *
 * @param  GPIO_Pin:  select the pin to read.(GPIO_Pin_0...GPIO_Pin_7)
 *
 * @retval The input status(0 is low,1 is high)
 */
uint8_t GPIO_ReadDataBit(GPIO_TypeDef GPIOx, uint8_t GPIO_Pin);

/**
  * @brief  Reads the GPIO input data(status) for group.
  *
  * @param  GPIOx: where x can be (GPIOA...GPIOD) to select the GPIO group.
  *
  * @retval GPIO input data(status).
  */
uint8_t GPIO_ReadData(GPIO_TypeDef GPIOx);

/**
  * @brief  set the GPIO port to low level
  *
  * @param  GPIOx: where x can be (GPIOA...GPIOD) to select the GPIO group.
  *
  * @param  GPIO_Pin:  select the pin to set.(GPIO_Pin_0...GPIO_Pin_7)
  *
  * @retval None
  */
void GPIO_ResetBits(GPIO_TypeDef GPIOx, uint8_t GPIO_Pin);

/**
  * @brief  set the GPIO port to high level
  *
  * @param  GPIOx: where x can be (GPIOA...GPIOD) to select the GPIO group.
  *
  * @param  GPIO_Pin:  select the pin to set.(GPIO_Pin_0...GPIO_Pin_7)
  *
  * @retval None
  */
void GPIO_SetBits(GPIO_TypeDef GPIOx, uint8_t GPIO_Pin);

/**
  * @brief  Writes data to the GPIO group port(only output mode)
  *
  * @param  GPIOx: where x can be (GPIOA...GPIOD) to select the GPIO group.
  *
  * @param  value: specifies the value to be written to the port output data register.
  *
  * @retval None
  */
void GPIO_Write(GPIO_TypeDef GPIOx, uint8_t GPIO_Pin);

/**
  * @brief  set the GPIO port to high or low level
  *
  * @param  GPIOx: where x can be (GPIOA...GPIOD) to select the GPIO group.
  *
  * @param  GPIO_Pin:  select the pin to set.(GPIO_Pin_0...GPIO_Pin_7)
  *
  * @param  BitVal:This parameter can be one of the BitAction enum values:
  *     @arg Bit_RESET: to clear the port pin
  *     @arg Bit_SET: to set the port pin
  * @retval None
  */
void GPIO_WriteBit(GPIO_TypeDef GPIOx, uint8_t GPIO_Pin, BitAction BitVal);

#endif

