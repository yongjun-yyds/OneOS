  /*
  ***********************************************************************************
  *
  * COPYRIGHT(c) 2021, China Mobile IOT
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of China Mobile IOT nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ***********************************************************************************
  */
#ifndef __CM_CHRG_H__
#define __CM_CHRG_H__

#include "cm1610.h"
#include "cm_lpm.h"

/**CHARG_OUT_DEFINE**/
#define CHARG_OUT_4D19V 0x08
#define CHARG_OUT_4D36V 0x0B
#define CHARG_OUT_4D54V 0x0E
#define CHARG_OUT_4D74V 0x0F
#define CHARG_OUT_DEFAULT CHARG_OUT_4D19V

typedef enum
{
    CHARG_CURRENT_20mA,
    CHARG_CURRENT_30mA,
    CHARG_CURRENT_40mA,
    CHARG_CURRENT_50mA,
    CHARG_CURRENT_60mA,
    CHARG_CURRENT_70mA,
    CHARG_CURRENT_80mA,
    CHARG_CURRENT_90mA,
    CHARG_CURRENT_100mA,
    CHARG_CURRENT_110mA,
    CHARG_CURRENT_120mA,
    CHARG_CURRENT_130mA,
    CHARG_CURRENT_140mA,
    CHARG_CURRENT_150mA,
    CHARG_CURRENT_160mA,
    CHARG_CURRENT_170mA
}CHARG_CURRENT;

typedef enum
{
    POWER_CHGR_IN,
    POWER_VBAT
}CHARG_SystemPowerSource;

/**
  * @brief	Configure the charge cut-off voltage,
            the default value is 4.2V, and the default
            value of the recharge voltage is 4.1V. At
            this time, the value is 0X08, and the step
            length is 53mV. The data width is 4 effective
            data bits.
  * @param	the para will be macro CHARG_OUT_DEFINE
  * @retval 	none
  */
void CHARGE_VolSel(uint8_t ChargOutVol);

/**
  * @brief	Config charge current
  * @param	the para will be macro CHARG_CURRENT
  * @retval 	none
  */
void CHARGE_CurSel(CHARG_CURRENT ChargCurtSel);

/**
  * @brief	System Power Select when usb insert
  * @param	powerSource:POWER_CHGR_IN or POWER_VBAT
  * @retval 	none
  */
void CHARGE_SystemPowerSelect(CHARG_SystemPowerSource powerSource);

/**
  * @brief	USB insertion detection
  * @param	none
  * @retval 	Insert is true, otherwise false
  */
BOOL CHARGE_InsertDet(void);

/**
  * @brief	Charge status detection
  * @param	none
  * @retval 	Charging is true, otherwise it is false
  */
BOOL CHARGE_State(void);
#endif
