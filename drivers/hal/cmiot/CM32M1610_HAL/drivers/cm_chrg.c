  /*
  ***********************************************************************************
  *
  * COPYRIGHT(c) 2021, China Mobile IOT
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of China Mobile IOT nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ***********************************************************************************
  */
#include "cm_chrg.h"

void CHARGE_VolSel(uint8_t ChargOutVol)
{
    uint32_t charge_ctrl=LPM_Read(CORE_LPM_CHARGER_CTRL);
    charge_ctrl &= 0xfffff00f;
    charge_ctrl |= (((uint32_t)ChargOutVol) << 8);
    charge_ctrl |= (((uint32_t)ChargOutVol) << 4);
    LPM_Write(LPM_CHARGE_CTRL1,charge_ctrl);
}

void CHARGE_CurSel(CHARG_CURRENT ChargCurtSel)
{
    uint32_t charge_ctrl=LPM_Read(CORE_LPM_CHARGER_CTRL);
    charge_ctrl &= 0xffff0fff;
    charge_ctrl |= (((uint32_t)ChargCurtSel)<<12);
    LPM_Write(LPM_CHARGE_CTRL1,charge_ctrl);
}

void CHARGE_SystemPowerSelect(CHARG_SystemPowerSource powerSource)
{
    uint32_t charge_ctrl=LPM_Read(CORE_LPM_CHARGER_CTRL);
    if(powerSource==POWER_VBAT)
    {
        charge_ctrl |= (((uint32_t)1)<<20);
    }
    else
    {
        charge_ctrl &= (~(((uint32_t)1)<<20));
    }
    LPM_Write(LPM_CHARGE_CTRL1,charge_ctrl);
}

BOOL CHARGE_InsertDet(void)
{
    uint8_t charEvent=HREAD(CORE_CHGR_EVENT2);
    if(charEvent & 0x02)
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

BOOL CHARGE_State(void)
{
    uint16_t chrgEvt = HREADW(CORE_CHGR_EVENT);
    if((((chrgEvt & CHGR_STATE_ICHG)!= 0) ||((chrgEvt & CHGR_STATE_VBAT_LV) != 0))
        && ((chrgEvt & CHGR_PGOOD_AON)!= 0)
        && ((chrgEvt & CHGR_PGOOD )!= 0)
        && ((chrgEvt & CHGR_IN_PRESENT) != 0))
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }

}
