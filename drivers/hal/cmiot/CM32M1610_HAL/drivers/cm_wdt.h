  /*
  ***********************************************************************************
  *
  * COPYRIGHT(c) 2021, China Mobile IOT
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of China Mobile IOT nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ***********************************************************************************
  */

#ifndef __CM_WDT_H__
#define __CM_WDT_H__

#include "cm1610.h"

/**
  * @brief  watch dog mode definition
  */
typedef enum
{
  RESET_MODE = 0x02,
  INTR_MODE = 0xfd
}WDT_ModeTypedef;

#define WDT_reload  0x78   // 0.128*(0x80-WDT_reload)=1.024s

/**
  * @brief  watch dog Init Structure definition
  */
typedef struct
{
  WDT_ModeTypedef mode;
  uint8_t setload;       // The time range is limited to [0x7F, 0x00] = [0.128, 16.384]s;
}WDT_InitTypeDef;

/**
 * @brief: Initialize WDT
 *
 * @param: WDT_init_struct : the WDT_InitTypeDef Structure
 *
 * @retval: none
 */
void WDT_Init(WDT_InitTypeDef WDT_init_struct);

/*
 * @brief: Enable WDT
 *
 * @param:  none
 *
 * @return: none
 */
void WDT_Enable(void);

/*
 * @brief: Disable WDT
 *
 * @param:  none
 *
 * @return: none
 */

void WDT_Disable(void);

/*
 * @brief: Reset the count of WDT
 *
 * @param:  reload : (0x00 ~ 0x7F)
 *
 * @return: none
 */
void WDT_SetReload(uint8_t reload);

/**
 * @brief: kick (feed watchdog)
 *
 * @param:  none
 *
 * @retval: none
 */
void WDT_Kick(void);

/**
 * @brief: Clear WDTx interrupt
 *
 * @param:  none
 *
 * @retval: none
 */
void WDT_ClearITPendingBit(void);

#endif
