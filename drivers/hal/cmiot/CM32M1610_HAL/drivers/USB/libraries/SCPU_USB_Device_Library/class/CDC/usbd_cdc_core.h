  /*
  ***********************************************************************************
  *
  * COPYRIGHT(c) 2021, China Mobile IOT
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of China Mobile IOT nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ***********************************************************************************
  */

/* Includes ------------------------------------------------------------------*/

#ifndef __USB_CDC_CORE_H_
#define __USB_CDC_CORE_H_

#include  "usbd_ioreq.h"

/** @addtogroup STM32_USB_OTG_DEVICE_LIBRARY
  * @{
  */

/** @defgroup usbd_cdc
  * @brief This file is the Header file for USBD_cdc.c
  * @{
  */

/** @defgroup usbd_cdc_Exported_Defines
  * @{
  */
#define USB_CDC_CONFIG_DESC_SIZ                (67)
#define USB_CDC_DESC_SIZ                       (67-9)

#define CDC_DESCRIPTOR_TYPE                     0x21

#define DEVICE_CLASS_CDC                        0x02
#define DEVICE_SUBCLASS_CDC                     0x00

#define USB_DEVICE_DESCRIPTOR_TYPE              0x01
#define USB_CONFIGURATION_DESCRIPTOR_TYPE       0x02
#define USB_STRING_DESCRIPTOR_TYPE              0x03
#define USB_INTERFACE_DESCRIPTOR_TYPE           0x04
#define USB_ENDPOINT_DESCRIPTOR_TYPE            0x05

#define STANDARD_ENDPOINT_DESC_SIZE             0x09

#define CDC_DATA_IN_PACKET_SIZE                CDC_DATA_MAX_PACKET_SIZE

#define CDC_DATA_OUT_PACKET_SIZE               CDC_DATA_MAX_PACKET_SIZE

#define CDC_APP_RX_DATA_SIZE                   APP_RX_DATA_SIZE

#define CDC_APP_TX_DATA_SIZE                   APP_TX_DATA_SIZE

/*---------------------------------------------------------------------*/
/*  CDC definitions                                                    */
/*---------------------------------------------------------------------*/

/**************************************************/
/* CDC Requests                                   */
/**************************************************/
#define SEND_ENCAPSULATED_COMMAND               0x00
#define GET_ENCAPSULATED_RESPONSE               0x01
#define SET_COMM_FEATURE                        0x02
#define GET_COMM_FEATURE                        0x03
#define CLEAR_COMM_FEATURE                      0x04
#define SET_LINE_CODING                         0x20
#define GET_LINE_CODING                         0x21
#define SET_CONTROL_LINE_STATE                  0x22
#define SEND_BREAK                              0x23
#define NO_CMD                                  0xFF

/** @defgroup USBD_CORE_Exported_TypesDefinitions
  * @{
  */
typedef struct _CDC_IF_PROP
{
    uint16_t (*pIf_Init)(void);
    uint16_t (*pIf_DeInit)(void);
    uint16_t (*pIf_Ctrl)(uint32_t Cmd, uint8_t *Buf, uint32_t Len);
    uint16_t (*pIf_DataTx)(uint8_t *Buf, uint32_t Len);
    uint16_t (*pIf_DataRx)(uint8_t *Buf, uint32_t Len);
}
CDC_IF_Prop_TypeDef;

struct APP_DATA_STRUCT_DEF
{
    /* 通过PC接收数据(将会通过串口发出的数据)信息 */
    struct
    {
        uint8_t APP_Tx_Buffer[CDC_APP_TX_DATA_SIZE];     // 接收数据缓存
        volatile uint32_t APP_Tx_ptr_in;            // 缓存接收数据起始偏移地址
        volatile uint32_t APP_Tx_ptr_out;           // 缓存读取数据起始偏移地址
        volatile uint32_t Tx_counter;               // 发送缓存等待被读取的数据个数
    } tx_structure;
    /* 通过串口接收数据(将会通过USB发送给PC的数据)信息 */
    struct
    {
        uint8_t APP_Rx_Buffer[CDC_APP_RX_DATA_SIZE];    // 接收数据缓存
        volatile uint32_t APP_Rx_ptr_in;            // 缓存接收数据起始偏移地址
        volatile uint32_t APP_Rx_ptr_out;           // 缓存读取数据起始偏移地址
        volatile uint32_t Rx_counter;               // 接收缓存等待被读取的数据个数
    } rx_structure;
    uint8_t COM_config_cmp; //串口状态(1:串口处于连接状态；0：串口处于断开状态)
} ;

/** @defgroup USBD_CORE_Exported_Macros
  * @{
  */

/** @defgroup USBD_CORE_Exported_Variables
  * @{
  */

extern USBD_Class_cb_TypeDef  USBD_CDC_cb;

/** @defgroup USB_CORE_Exported_Functions
  * @{
  */

#endif  // __USB_CDC_CORE_H_

