  /*
  ***********************************************************************************
  *
  * COPYRIGHT(c) 2021, China Mobile IOT
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of China Mobile IOT nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ***********************************************************************************
  */

#ifdef USB_OTG_HS_INTERNAL_DMA_ENABLED
    #pragma     data_alignment = 4
#endif /* USB_OTG_HS_INTERNAL_DMA_ENABLED */

/* Includes ------------------------------------------------------------------*/
#include "usbd_cdc_vcp.h"
#include "usb_conf.h"
#include "usbd_cdc_core.h"
#include "cm_uart.h"
#include "usb_main.h"
#include "usb_core.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
LINE_CODING linecoding =
{
    115200, /* baud rate*/
    0x00,   /* stop bits-1*/
    0x00,   /* parity - none*/
    0x08    /* nb. of bits 8*/
};

//UART_InitTypeDef USART_InitStructure;
USB_OTG_CORE_HANDLE  *pdev;
/* These are external variables imported from CDC core to be used for IN
   transfer management. */
#ifdef USER_SPECIFIED_DATA_SOURCE
    extern uint8_t  *APP_Rx_Buffer;
    uint8_t COM_Rx_data_buf[2048];
#else
    //extern uint8_t  APP_Rx_Buffer [];
    extern struct APP_DATA_STRUCT_DEF APP_Gdata_param;
#endif                            /* Write CDC received data in this buffer.
These data will be sent over USB IN endpoint
in the CDC core functions. */
//extern volatile uint32_t APP_Rx_ptr_in;
/* Increment this pointer or roll it back to
   start address when writing received data
   in the buffer APP_Rx_Buffer. */

/* Private function prototypes -----------------------------------------------*/
static uint16_t VCP_Init(void);
static uint16_t VCP_DeInit(void);
static uint16_t VCP_Ctrl(uint32_t Cmd, uint8_t *Buf, uint32_t Len);
//static uint16_t VCP_DataTx   (uint8_t* Buf, uint32_t Len);
static uint16_t VCP_DataRx(uint8_t *Buf, uint32_t Len);

static uint16_t VCP_COMConfig(uint8_t Conf);

CDC_IF_Prop_TypeDef VCP_fops =
{
    VCP_Init,
    VCP_DeInit,
    VCP_Ctrl,
    VCP_DataTx,
    VCP_DataRx
};

/* Private functions ---------------------------------------------------------*/
/**
  * @brief  VCP_Init
  *         Initializes the Media on the STM32
  * @param  None
  * @retval Result of the opeartion (USBD_OK in all cases)
  */
static uint16_t VCP_Init(void)
{
//    NVIC_InitTypeDef NVIC_InitStructure;

    /**/
#ifdef USER_SPECIFIED_DATA_SOURCE
    APP_Rx_Buffer = COM_Rx_data_buf;
#else
    APP_Gdata_param.COM_config_cmp = 0;
#endif
    memset(&APP_Gdata_param, 0, sizeof(APP_Gdata_param));
    /* EVAL_COM1 default configuration */
    /* EVAL_COM1 configured as follow:
        - BaudRate = 115200 baud
        - Word Length = 8 Bits
        - One Stop Bit
        - Parity Odd
        - Hardware flow control disabled
        - Receive and transmit enabled
    */
//    USART_InitStructure.UART_BaudRate = 115200;
//    USART_InitStructure.UART_WordLength = UART_WordLength_8b;
//    USART_InitStructure.UART_StopBits = UART_StopBits_1;
//    USART_InitStructure.UART_Parity = UART_Parity_Odd;
//    UART_Init(UART1, &USART_InitStructure);

//    /* Enable the USART Receive interrupt */
//    UART_ITConfig(UART1, UART_IT_RX_RECVD, ENABLE);

//    /* Enable USART Interrupt */
//    NVIC_InitStructure.NVIC_IRQChannel = UART1_IRQn;
//    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
//    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
//    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
//    NVIC_Init(&NVIC_InitStructure);

    return USBD_OK;
}

/**
  * @brief  VCP_DeInit
  *         DeInitializes the Media on the STM32
  * @param  None
  * @retval Result of the opeartion (USBD_OK in all cases)
  */
static uint16_t VCP_DeInit(void)
{

    return USBD_OK;
}

/**
  * @brief  VCP_Ctrl
  *         Manage the CDC class requests
  * @param  Cmd: Command code
  * @param  Buf: Buffer containing command data (request parameters)
  * @param  Len: Number of data to be sent (in bytes)
  * @retval Result of the opeartion (USBD_OK in all cases)
  */
static uint16_t VCP_Ctrl(uint32_t Cmd, uint8_t *Buf, uint32_t Len)
{

    switch (Cmd)
    {
    case SEND_ENCAPSULATED_COMMAND:
        /* Not  needed for this driver */
        break;

    case GET_ENCAPSULATED_RESPONSE:
        /* Not  needed for this driver */
        break;

    case SET_COMM_FEATURE:
        /* Not  needed for this driver */
        break;

    case GET_COMM_FEATURE:
        /* Not  needed for this driver */
        break;

    case CLEAR_COMM_FEATURE:
        /* Not  needed for this driver */
        break;

    case SET_LINE_CODING:

        linecoding.bitrate = (uint32_t)(Buf[0] | (Buf[1] << 8) | (Buf[2] << 16) | (Buf[3] << 24));
        linecoding.format = Buf[4];
        linecoding.paritytype = Buf[5];
        linecoding.datatype = Buf[6];
        APP_Gdata_param.COM_config_cmp = 1;
        /* Set the new configuration */
        VCP_COMConfig(OTHER_CONFIG);
        break;

    case GET_LINE_CODING:
        Buf[0] = (uint8_t)(linecoding.bitrate);
        Buf[1] = (uint8_t)(linecoding.bitrate >> 8);
        Buf[2] = (uint8_t)(linecoding.bitrate >> 16);
        Buf[3] = (uint8_t)(linecoding.bitrate >> 24);
        Buf[4] = linecoding.format;
        Buf[5] = linecoding.paritytype;
        Buf[6] = linecoding.datatype;
        break;

    case SET_CONTROL_LINE_STATE:
        /* Not  needed for this driver */
        break;

    case SEND_BREAK:
        /* Not  needed for this driver */
        break;

    default:
        break;
    }

    return USBD_OK;
}

int isUsbBufFull()
{
    int ret = 0;
    ret = APP_Gdata_param.rx_structure.Rx_counter >= CDC_APP_RX_DATA_SIZE;
    return ret;
}
/**
  * @brief  VCP_DataTx
  *         CDC received data to be send over USB IN endpoint are managed in
  *         this function.
  * @param  Buf: Buffer of data to be sent
  * @param  Len: Number of data to be sent (in bytes)
  * @retval Result of the opeartion: USBD_OK if all operations are OK else USBD_FAIL
  */
uint16_t VCP_DataTx(uint8_t *Buf, uint32_t Len)
{
#ifdef USER_SPECIFIED_DATA_SOURCE
    uint32_t i = 0;
    for (i = 0; i < Len; i++)
    {
        *(APP_Rx_Buffer + APP_Rx_ptr_in) = *Buf++;
        APP_Rx_ptr_in++;
    }
#else
    uint32_t i = 0;
    if (linecoding.datatype == 7)
    {
        for (i = 0; i < Len; i++)
        {
            APP_Gdata_param.rx_structure.APP_Rx_Buffer[APP_Gdata_param.rx_structure.APP_Rx_ptr_in++] = (*Buf++) & 0x7F;
            APP_Gdata_param.rx_structure.Rx_counter++;
            /* To avoid buffer overflow */
            if (APP_Gdata_param.rx_structure.APP_Rx_ptr_in == CDC_APP_RX_DATA_SIZE)
            {
                APP_Gdata_param.rx_structure.APP_Rx_ptr_in = 0;
            }
        }
    }
    else if (linecoding.datatype == 8)
    {
        for (i = 0; i < Len; i++)
        {
            APP_Gdata_param.rx_structure.APP_Rx_Buffer[APP_Gdata_param.rx_structure.APP_Rx_ptr_in++] = *Buf++;
            APP_Gdata_param.rx_structure.Rx_counter++;
            /* To avoid buffer overflow */
            if (APP_Gdata_param.rx_structure.APP_Rx_ptr_in == CDC_APP_RX_DATA_SIZE)
            {
                APP_Gdata_param.rx_structure.APP_Rx_ptr_in = 0;
            }
        }
    }
#endif
    return USBD_OK;
}

/**
  * @brief  Get_TxBuf_length
  *         Get the length of the remaining data to be transmitted
  * @param  NONE
  * @retval Result receive data length
  */
uint32_t VCP_GetTxBuflen(void)
{
    uint32_t ret = 0x00;
    if (APP_Gdata_param.rx_structure.APP_Rx_ptr_out < APP_Gdata_param.rx_structure.APP_Rx_ptr_in)
        ret = APP_Gdata_param.rx_structure.APP_Rx_ptr_in - APP_Gdata_param.rx_structure.APP_Rx_ptr_out;
    else
        ret = CDC_APP_RX_DATA_SIZE + APP_Gdata_param.rx_structure.APP_Rx_ptr_in \
              - APP_Gdata_param.rx_structure.APP_Rx_ptr_out;
    return ret;
}

/**
  * @brief  Get_RxBuf_rsaddr
  *         Get reading receive data starting position.
  * @param  NONE
  * @retval Result received data is read starting position
  */
uint32_t VCP_GetTxBufrsaddr(void)
{
    return (APP_Gdata_param.rx_structure.APP_Rx_ptr_out);
}

/**
  * @brief  Get_RxData
  *         Get receive data by byte
  * @param  NONE
  * @retval Result receive data
  */
extern void NVIC_EnableIRQ(IRQn_Type IRQnx);
extern void NVIC_DisableIRQ(IRQn_Type IRQnx);
volatile unsigned char APP_Tx_ptr_out_count;
volatile unsigned char APP_Tx_ptr_in_count;
int32_t VCP_GetRxChar(void)
{
    uint8_t ret;
    __asm("CPSID i");
    ret = usbbuf[urp];
    urp = INCPTR(urp, 1);
    if ((VCP_GetRxBuflen() + CDC_DATA_MAX_PACKET_SIZE) < sizeof(usbbuf))
    {
        usb_restart_rcv();
    }
    __asm("CPSIE i");
    return ret;
}
/**
  * @brief  Get_RxBuf_length
  *         Get receive data length
  * @param  NONE
  * @retval Result receive data length
  */

uint32_t VCP_GetRxBuflen(void)
{
    uint32_t get_uwp = uwp;
    uint32_t get_urp = urp;
    return get_uwp < get_urp ? sizeof(usbbuf) + get_uwp - get_urp : get_uwp - get_urp;
}

/**
  * @brief  Get_RxBuf_rsaddr
  *         Get reading receive data starting position.
  * @param  NONE
  * @retval Result received data is read starting position
  */
int8_t *VCP_GetRxBufrsaddr(void)
{
    return ((int8_t *) & (APP_Gdata_param.tx_structure.APP_Tx_Buffer[APP_Gdata_param.tx_structure.APP_Tx_ptr_out]));
}

/**
  * @brief  VCP_DataRx
  *         Data received over USB OUT endpoint are sent over CDC interface
  *         through this function.
  *
  *         @note
  *         This function will block any OUT packet reception on USB endpoint
  *         untill exiting this function. If you exit this function before transfer
  *         is complete on CDC interface (ie. using DMA controller) it will result
  *         in receiving more data while previous ones are still not sent.
  *
  * @param  Buf: Buffer of data to be received
  * @param  Len: Number of data received (in bytes)
  * @retval Result of the opeartion: USBD_OK if all operations are OK else VCP_FAIL
  */
static uint16_t VCP_DataRx(uint8_t *Buf, uint32_t Len)
{
    uint32_t i;

    for (i = 0; i < Len; i++)
    {

        APP_Gdata_param.tx_structure.APP_Tx_Buffer[APP_Gdata_param.tx_structure.APP_Tx_ptr_in++] = Buf[i];
        if (APP_Gdata_param.tx_structure.APP_Tx_ptr_in >= APP_RX_DATA_SIZE)
        {
            APP_Gdata_param.tx_structure.APP_Tx_ptr_in = 0;
            APP_Tx_ptr_in_count++;
        }

        APP_Gdata_param.tx_structure.Tx_counter++;
    }

    if ((VCP_GetRxBuflen() + CDC_DATA_MAX_PACKET_SIZE) >= APP_RX_DATA_SIZE)
    {
        if (usbFlowCtrl == 0)
        {
            usbFlowCtrl = 1;
            NVIC_DisableIRQ(usb_handler_IRQn);
        }
    }
    return USBD_OK;
}

/**
  * @brief  VCP_COMConfig
  *         Configure the COM Port with default values or values received from host.
  * @param  Conf: can be DEFAULT_CONFIG to set the default configuration or OTHER_CONFIG
  *         to set a configuration received from the host.
  * @retval None.
  */
static uint16_t VCP_COMConfig(uint8_t Conf)
{

    return USBD_OK;
}

/**
  * @brief  usb_stop_rcv
  * @param  None.
  * @retval None.
  */
void usb_stop_rcv(void)
{
    __asm("CPSID i");
    NVIC_DisableIRQ(usb_handler_IRQn);
    __asm("CPSIE i");
}

/**
  * @brief  usb_restart_rcv
  * @param  None.
  * @retval None.
  */
void usb_restart_rcv(void)
{
    __asm("CPSID i");
    NVIC_EnableIRQ(usb_handler_IRQn);
    __asm("CPSIE i");
}

