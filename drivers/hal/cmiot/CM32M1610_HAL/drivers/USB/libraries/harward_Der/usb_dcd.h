  /*
  ***********************************************************************************
  *
  * COPYRIGHT(c) 2021, China Mobile IOT
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of China Mobile IOT nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ***********************************************************************************
  */

#ifndef __DCD_H__
#define __DCD_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Include ------------------------------------------------------------------*/
#include "usb_core.h"
#include "cm_type.h"
/* Exported types -----------------------------------------------------------*/
/* Exported constants -------------------------------------------------------*/
/* Exported macro -----------------------------------------------------------*/
/* Exported functions -------------------------------------------------------*/
/* Exported variables -------------------------------------------------------*/
/** @addtogroup USB_OTG_DRIVER
* @{
*/

/** @defgroup USB_DCD
* @brief This file is the
* @{
*/

/** @defgroup USB_DCD_Exported_Defines
* @{
*/
#define USB_OTG_EP_CONTROL                       0
#define USB_OTG_EP_ISOC                          1
#define USB_OTG_EP_BULK                          2
#define USB_OTG_EP_INT                           3
#define USB_OTG_EP_MASK                          3

/*  Device Status */
#define USB_OTG_DEFAULT                          1
#define USB_OTG_ADDRESSED                        2
#define USB_OTG_CONFIGURED                       3
#define USB_OTG_SUSPENDED                        4
#define USB_OTG_END                        5
#define USB_OTG_READ10                        6
/**
* @}
*/

/** @defgroup USB_DCD_Exported_Types
* @{
*/
/********************************************************************************
Data structure type
********************************************************************************/
typedef struct
{
    uint8_t  bLength;
    uint8_t  bDescriptorType;
    uint8_t  bEndpointAddress;
    uint8_t  bmAttributes;
    uint16_t wMaxPacketSize;
    uint8_t  bInterval;
}
EP_DESCRIPTOR, *PEP_DESCRIPTOR;

/**
* @}
*/

/** @defgroup USB_DCD_Exported_Macros
* @{
*/
/**
* @}
*/

/** @defgroup USB_DCD_Exported_Variables
* @{
*/
/**
* @}
*/

/** @defgroup USB_DCD_Exported_FunctionsPrototype
* @{
*/
/********************************************************************************
EXPORTED FUNCTION FROM THE USB-OTG LAYER
********************************************************************************/
void       DCD_Init(USB_OTG_CORE_HANDLE *pdev,
                    USB_OTG_CORE_ID_TypeDef coreID);

void        DCD_DevConnect(USB_OTG_CORE_HANDLE *pdev);
void        DCD_DevDisconnect(USB_OTG_CORE_HANDLE *pdev);
void        DCD_EP_SetAddress(USB_OTG_CORE_HANDLE *pdev,
                              uint8_t address);
uint32_t    DCD_EP_Open(USB_OTG_CORE_HANDLE *pdev,
                        uint8_t ep_addr,
                        uint16_t ep_mps,
                        uint8_t ep_type);

uint32_t    DCD_EP_Close(USB_OTG_CORE_HANDLE *pdev,
                         uint8_t  ep_addr);

uint32_t   DCD_EP_PrepareRx(USB_OTG_CORE_HANDLE *pdev,
                            uint8_t   ep_addr,
                            uint8_t *pbuf,
                            uint16_t  buf_len);

uint32_t    DCD_EP_Tx(USB_OTG_CORE_HANDLE *pdev,
                      uint8_t  ep_addr,
                      uint8_t  *pbuf,
                      uint32_t   buf_len);
uint32_t    DCD_EP_Stall(USB_OTG_CORE_HANDLE *pdev,
                         uint8_t   epnum);
uint32_t    DCD_EP_ClrStall(USB_OTG_CORE_HANDLE *pdev,
                            uint8_t epnum);
uint32_t    DCD_EP_Flush(USB_OTG_CORE_HANDLE *pdev,
                         uint8_t epnum);
uint32_t    DCD_Handle_ISR(USB_OTG_CORE_HANDLE *pdev);

uint32_t DCD_GetEPStatus(USB_OTG_CORE_HANDLE *pdev,
                         uint8_t epnum);

void DCD_SetEPStatus(USB_OTG_CORE_HANDLE *pdev,
                     uint8_t epnum,
                     uint32_t Status);

/**
* @}
*/

#ifdef __cplusplus
}
#endif

#endif  /* __DCD_H__ */

