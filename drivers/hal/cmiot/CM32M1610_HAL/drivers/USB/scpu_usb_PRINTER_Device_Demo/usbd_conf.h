  /*
  ***********************************************************************************
  *
  * COPYRIGHT(c) 2021, China Mobile IOT
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of China Mobile IOT nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ***********************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __USBD_CONF__H__
#define __USBD_CONF__H__

/* Includes ------------------------------------------------------------------*/
#include "usb_conf.h"

/** @defgroup USB_CONF_Exported_Defines
  * @{
  */
#define USBD_CFG_MAX_NUM                1
#define USBD_ITF_MAX_NUM                1

#define USBD_SELF_POWERED

#define USB_MAX_STR_DESC_SIZ            255

/** @defgroup USB_VCP_Class_Layer_Parameter
  * @{
  */
#define PRINTER_IN_EP                       0x81  /* EP1 for data IN */
#define PRINTER_OUT_EP                      0x01  /* EP1 for data OUT */
#define PRINTER_CMD_EP                      0x82  /* EP2 for PRINTER commands */

/* PRINTER Endpoints parameters: you can fine tune these values depending on the needed baudrates and performance. */
#ifdef USE_USB_OTG_HS
    #define PRINTER_DATA_MAX_PACKET_SIZE       512  /* Endpoint IN & OUT Packet size */
    #define PRINTER_CMD_PACKET_SZE             8    /* Control Endpoint Packet size */

    #define PRINTER_IN_FRAME_INTERVAL          40   /* Number of micro-frames between IN transfers */
    #define APP_RX_DATA_SIZE               2048 /* Total size of IN buffer:
    APP_RX_DATA_SIZE*8/MAX_BAUDARATE*1000 should be > PRINTER_IN_FRAME_INTERVAL*8 */
#else
    #define PRINTER_DATA_MAX_PACKET_SIZE       256   /* Endpoint IN & OUT Packet size */
    #define PRINTER_CMD_PACKET_SZE             8    /* Control Endpoint Packet size */

    #define PRINTER_IN_FRAME_INTERVAL          5    /* Number of frames between IN transfers */
    #define APP_TX_DATA_SIZE              (1024)
    #define APP_RX_DATA_SIZE               1024 /* Total size of IN buffer:
    APP_RX_DATA_SIZE*8/MAX_BAUDARATE*1000 should be > PRINTER_IN_FRAME_INTERVAL */
#endif /* USE_USB_OTG_HS */

#define APP_FOPS                        VCP_fops

/** @defgroup USB_CONF_Exported_Types
  * @{
  */

/** @defgroup USB_CONF_Exported_Macros
  * @{
  */

/** @defgroup USB_CONF_Exported_Variables
  * @{
  */

/** @defgroup USB_CONF_Exported_FunctionsPrototype
  * @{
  */

#endif //__USBD_CONF__H__

