  /*
  ***********************************************************************************
  *
  * COPYRIGHT(c) 2021, China Mobile IOT
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of China Mobile IOT nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ***********************************************************************************
  */

/* Includes ------------------------------------------------------------------*/

//#include "usbd_usr.h"
#include "usbd_desc.h"
//#include <string.h>
//#include <stdlib.h>
//#include <stdio.h>

#include "cm1610.h"
#include "usb_main.h"
#include "cm_nvic.h"
#include "cm_sysctrl.h"
#include "usbd_cdc_core.h"
#include "usbd_cdc_vcp.h"

void USB_NVIC_Config(void);

#ifndef USB_USER_MRAM
    volatile uint8_t   usb_rxbuf[71]       __attribute__((at((reg_map(USB_RXBUF) + 3) / 4 * 4)));
    volatile uint8_t   usb_txbuf0[65]      __attribute__((at((reg_map(USB_TXBUF0) + 3) / 4 * 4)));
    volatile uint8_t   usb_txbuf1[65]      __attribute__((at((reg_map(USB_TXBUF1) + 3) / 4 * 4)));
    volatile uint8_t   usb_txbuf2[100]      __attribute__((at((reg_map(USB_TXBUF2) + 3) / 4 * 4)));
    //volatile uint8_t   usb_txbuf3[65]      __attribute__ ((at(reg_map(USB_TXBUF3))));
#else
    volatile uint8_t   usb_rxbuf[980] ;
    volatile uint8_t   usb_txbuf0[65] ;
    volatile uint8_t   usb_txbuf1[65] ;
    volatile uint8_t   usb_txbuf2[100] ;
    volatile uint8_t   usb_txbuf3[100];
#endif

/*cdc flowctrl*/
volatile uint8_t  usbbuf[1024] = {0};
volatile uint8_t  usbFlowCtrl = 0;
volatile uint32_t uwp = 0, urp = 0;

/*cdc test check*/
volatile uint32_t cdc_total_len = 0, cdc_total_chcksum = 0;

USB_OTG_CORE_HANDLE  USB_OTG_dev;

void USB_Reg_Init()
{
    USBHWRITE(core_usb_config, 0x00);
    USBHWRITE(core_usb_drv, 0x00);
    USBHWRITE(core_usb_addr, 0x00);
    USBHWRITEW(core_usb_rx_saddr, (int)(void *)(&usb_rxbuf));
    USBHWRITEW(core_usb_rxptr, (int)(void *)(&usb_rxbuf));
    USBHWRITEW(core_usb_rx_eaddr, (int)(void *)(&usb_rxbuf) + (sizeof(usb_rxbuf) - 1));
    USBHWRITEW(core_usb_tx_saddr0, (int)(void *)&usb_txbuf0);
    USBHWRITEW(core_usb_tx_saddr1, (int)(void *)&usb_txbuf1);
    USBHWRITEW(core_usb_tx_saddr2, (int)(void *)&usb_txbuf2);
    USBHWRITE(core_usb_config, 0x3c);
#ifndef USB_USER_MRAM
    USBHWRITE(core_usb_hmode, 0x04);
#else
    USBHWRITE(core_usb_hmode, 0x0c);
#endif
    USBHWRITE(core_usb_int_mask, 0x16);
}

void USB_Init(void)
{
    SYSCTRL_AHBPeriphClockCmd(SYSCTRL_AHBPeriph_USB, ENABLE);
    USB_Reg_Init();
    memset(&USB_OTG_dev, 0, sizeof(USB_OTG_dev));
    memset((uint8_t *)usb_rxbuf, 0xff, sizeof(usb_rxbuf));
    USBD_Init(&USB_OTG_dev,
#ifdef USE_USB_OTG_HS
              USB_OTG_HS_CORE_ID,
#else
              USB_OTG_FS_CORE_ID,
#endif
              &USR_desc,
              &USBD_CDC_cb,
              &USRD_cb);
    USB_NVIC_Config();
}

void USB_NVIC_Config(void)
{
    NVIC_EnableIRQ(usb_handler_IRQn);
    NVIC_SetPriority(usb_handler_IRQn, 0);
}

void USB_IRQHandler(void)
{
    USBD_OTG_ISR_Handler(&USB_OTG_dev);
}

