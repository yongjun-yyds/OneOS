  /*
  ***********************************************************************************
  *
  * COPYRIGHT(c) 2021, China Mobile IOT
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of China Mobile IOT nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ***********************************************************************************
  */

#include "usbd_usr.h"
//#include "usbd_desc.h"
#include <string.h>
#include <stdio.h>
#include "usb_dcd_int.h"
// #include "usbd_hid_core.h"
#include "btreg.h"

#define mem_usbrxbuf                                0x2000
#define mem_txbuf0                                  0x4d2d
#define mem_txbuf1                                  0x4d91
#define mem_txbuf2                                  0x4df5

//#define USB_USER_MRAM  1
#ifndef USB_USER_MRAM
    #define USB_RXBUF  mem_usbrxbuf
    #define USB_TXBUF0 mem_txbuf0
    #define USB_TXBUF1 mem_txbuf1
    #define USB_TXBUF2 mem_txbuf2
    #define USB_TXBUF3 mem_txbuf3
#endif

#define INCPTR(x, inc)      (x + inc >= sizeof(usbbuf) ? x + inc - sizeof(usbbuf) : x + inc)

extern volatile uint8_t usb_rxbuf[71];
extern volatile uint8_t usb_txbuf0[65];
extern volatile uint8_t usb_txbuf1[65];
extern volatile uint8_t usb_txbuf2[100];

extern volatile uint8_t  usbbuf[1024];
extern volatile uint8_t  usbFlowCtrl;
extern volatile uint32_t uwp, urp;

extern volatile uint32_t cdc_total_len;
extern volatile uint32_t cdc_total_chcksum;

void USB_Init(void);
void USB_Continue(int parms);
