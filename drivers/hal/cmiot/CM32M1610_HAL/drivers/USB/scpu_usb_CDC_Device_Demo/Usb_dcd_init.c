  /*
  ***********************************************************************************
  *
  * COPYRIGHT(c) 2021, China Mobile IOT
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of China Mobile IOT nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ***********************************************************************************
  */

/* Include ------------------------------------------------------------------*/
#include "usb_dcd_int.h"
#include "usb_defines.h"
#include "usbd_desc.h"
#include "usbd_usr.h"
#include "usb_main.h"
#include "usbd_cdc_core.h"
/** @addtogroup USB_OTG_DRIVER
* @{
*/

/** @defgroup USB_DCD_INT
* @brief This file contains the interrupt subroutines for the Device mode.
* @{
*/

/** @defgroup USB_DCD_INT_Private_Defines
* @{
*/
/**
* @}
*/

/* Interrupt Handlers */
static uint32_t DCD_HandleInEP_ISR(USB_OTG_CORE_HANDLE *pdev, uint16_t ep_intr);
uint32_t DCD_HandleOutEP_ISR(USB_OTG_CORE_HANDLE *pdev, uint8_t ep_intr);
uint32_t DCD_HandleUsbReset_ISR(void);

/**
* @brief  USBD_OTG_ISR_Handler
*         handles all USB Interrupts
* @param  pdev: device instance
* @retval status
*/

byte usb_getbyte(void)
{
    volatile byte t;
    t = *usb_rxptr;
    *usb_rxptr = 0xff;
    if (++usb_rxptr > usb_rxbuf + (sizeof(usb_rxbuf) - 1))
        usb_rxptr = usb_rxbuf;
    return t;
}

word usb_getword(void)
{
    return usb_getbyte() | (int)usb_getbyte() << 8;
}

extern USBD_DCD_INT_cb_TypeDef USBD_DCD_INT_cb;
extern uint8_t  Audiovolmute[2];
extern uint8_t  AudioCtl[64] ;
uint8_t setup_cnt = 0;
uint8_t SetAddress_Flag = 0;
uint8_t Address_Value = 0;
uint16_t RXaddr, Saddr, Datalen;
int ADUIOCTL_FLAG;
extern uint16_t cer;
int cer_old;
uint32_t USBD_OTG_ISR_Handler(USB_OTG_CORE_HANDLE *pdev)
{
    USB_OTG_IRQ_TypeDef gintr_status;
    uint32_t retval = 0;
    uint16_t rx_ep;
    uint8_t ep_num;
    uint8_t ep0outdata = 0;
    uint8_t next_rcvflag = 0;

    if (USB_OTG_IsDeviceMode(pdev)) /* ensure that we are in device mode */
    {
        gintr_status.d8 = USBHREAD(core_usb_status);
        USBHWRITE(core_usb_status, gintr_status.d8);
        if (!gintr_status.d8)
            return 0;
        if (gintr_status.b.reset)
        {
            retval |= DCD_HandleUsbReset_ISR();
            // TODO:
            USBHWRITE(core_usb_status, 0x08);
        }
        if (gintr_status.b.tx_empty)
        {

            retval |= DCD_HandleInEP_ISR(pdev, 0x05);
        }
        if (gintr_status.b.rx_ready)
        {
#ifndef USB_USER_MRAM

            usb_rxptr = (byte *)(USBHREADW(core_usb_rxptr) | 0x10000000);
#else
            usb_rxptr = (byte *)(USBHREADW(core_usb_rxptr) | 0x10010000);
#endif

            for (int i = 0; i < 64; i++)
            {
                rx_ep = usb_getword();
                if (rx_ep == 0xFFFF)
                {
                    for (int j = 0; j < 2; j++)
                    {
                        usb_rxptr --;
                        if (usb_rxptr < usb_rxbuf)
                        {
                            usb_rxptr = usb_rxbuf + (sizeof(usb_rxbuf) - 1);
                        }
                    }
                    if (ep0outdata == 0)
                    {
                        USBHWRITE(core_usb_status, 0x04);
                        next_rcvflag = 0;
                        for (int m = 0; m < 2000; m++)
                        {
                            if (USBHREAD(core_usb_status) & 0x04)
                            {
                                next_rcvflag = 1;
                                USBHWRITEW(core_usb_rxptr, usb_rxptr);
                                break ;
                            }
                        }
                        if (next_rcvflag == 0)
                        {
                            USBHWRITEW(core_usb_rxptr, usb_rxptr);
                        }
                    }
                    else
                    {
                        USBHWRITEW(core_usb_rxptr, usb_rxptr);
                    }
                    break;
                }
                rx_DataLength = rx_ep & 0xfff;
                ep_num = (uint8_t)(rx_ep >> 12);

                if (ep_num == 0)
                {
                    ep0outdata = 1;
                    if (rx_DataLength == 8)
                    {
                        if (SetAddress_Flag)
                        {
                            SetAddress_Flag = 0;
                        }
                        USB_OTG_ReadPacket(pdev, pdev->dev.setup_packet, 0, rx_DataLength);
                        /* deal with receive setup packet */
                        USBD_DCD_INT_fops->SetupStage(pdev);
                    }
                    else
                    {

                    }
                }
                else
                {
                    retval |= DCD_HandleOutEP_ISR(pdev, ep_num);

                }
            }
        }

    }

    return retval;
}

/**
* @brief  DCD_HandleInEP_ISR
*         Indicates that an IN EP has a pending Interrupt
* @param  pdev: device instance
* @retval status
*/
static uint32_t DCD_HandleInEP_ISR(USB_OTG_CORE_HANDLE *pdev, uint16_t ep_intr)
{
    USB_OTG_EP *ep;
    uint16_t epnum = 0;

    while (ep_intr)
    {
        ep = &pdev->dev.in_ep[epnum];
        /* Setup and start the Transfer */
        ep->is_in = 1;
        ep->num = epnum;

        if (ep_intr & 0x01) /* In ITR */
        {
            if (pdev->dev.in_ep[epnum].rem_data_len == 0)
            {
                if ((pdev->dev.zero_replay_flag) & 1 << ep->num)
                {
                    USB_OTG_EPReply_Zerolen(pdev, ep);
                    pdev->dev.zero_replay_flag = 0;
                }
            }
            else
            {
                if (pdev->dev.in_ep[epnum].xfer_len - pdev->dev.in_ep[epnum].xfer_count == pdev->dev.in_ep[epnum].maxpacket)
                {
                    //                      //MyPrintf("tg\n\r");
                    USB_OTG_WritePacket(pdev,
                                        pdev->dev.in_ep[epnum].xfer_buff + pdev->dev.in_ep[epnum].xfer_count,
                                        epnum,
                                        pdev->dev.in_ep[epnum].maxpacket);
                    pdev->dev.in_ep[epnum].xfer_count = pdev->dev.in_ep[epnum].xfer_len;
                    pdev->dev.in_ep[epnum].rem_data_len = 0;
                    pdev->dev.zero_replay_flag = 1 << ep->num;
                }
                else if (pdev->dev.in_ep[epnum].xfer_len - pdev->dev.in_ep[epnum].xfer_count > pdev->dev.in_ep[epnum].maxpacket)
                {
                    USB_OTG_WritePacket(pdev,
                                        pdev->dev.in_ep[epnum].xfer_buff + pdev->dev.in_ep[epnum].xfer_count,
                                        epnum,
                                        pdev->dev.in_ep[epnum].maxpacket);
                    pdev->dev.in_ep[epnum].xfer_count += pdev->dev.in_ep[epnum].maxpacket;
                    pdev->dev.in_ep[epnum].rem_data_len = pdev->dev.in_ep[epnum].xfer_len - pdev->dev.in_ep[epnum].xfer_count;
                }
                else
                {
                    USB_OTG_WritePacket(pdev,
                                        pdev->dev.in_ep[epnum].xfer_buff + pdev->dev.in_ep[epnum].xfer_count,
                                        epnum,
                                        pdev->dev.in_ep[epnum].xfer_len - pdev->dev.in_ep[epnum].xfer_count);
                    pdev->dev.in_ep[epnum].xfer_count = pdev->dev.in_ep[epnum].xfer_len;
                    pdev->dev.in_ep[epnum].rem_data_len = 0;

                    USBD_DCD_INT_fops->DataInStage(pdev, epnum);
                    pdev->dev.zero_replay_flag = 0;
                }
            }
        }

        epnum++;
        ep_intr = ep_intr >> 1;
    }
    return 1;
}

uint32_t DCD_HandleOutEP_ISR(USB_OTG_CORE_HANDLE *pdev, uint8_t ep_intr)
{

    if (ep_intr == CDC_OUT_EP)
    {
        if (rx_DataLength)
        {
            USBD_DCD_INT_fops->DataOutStage(pdev, ep_intr);
        }
    }
    return 1;
}

/**
* @brief  DCD_HandleUsbReset_ISR
*         This interrupt occurs when a USB Reset is detected
* @param  pdev: device instance
* @retval status
*/
extern void USB_Reg_Init(void);
extern USB_OTG_CORE_HANDLE USB_OTG_dev;
extern void USB_NVIC_Config(void);
static uint32_t DCD_HandleUsbReset_ISR()
{
    USBHWRITE(core_usb_addr, 0x00);
    USBHWRITE(core_usb_status, 0x7F);
    USBHWRITE(core_usb_hmode, 0x00);
    USBHWRITE(core_usb_clear, 0x80);
    USB_Reg_Init();
    memset(&USB_OTG_dev, 0, sizeof(USB_OTG_dev));

    USBD_Init(&USB_OTG_dev,
#ifdef USE_USB_OTG_HS
              USB_OTG_HS_CORE_ID,
#else
              USB_OTG_FS_CORE_ID,
#endif
              &USR_desc,
              &USBD_CDC_cb,
              &USRD_cb);
    USB_NVIC_Config();
    return 1;
}

