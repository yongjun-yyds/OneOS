  /*
  ***********************************************************************************
  *
  * COPYRIGHT(c) 2021, China Mobile IOT
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of China Mobile IOT nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ***********************************************************************************
  */
#include "cm_bt.h"
//#include "config.h"
#include "cm_sysctrl.h"


/*********************************************
 * FunctionName : BT_SendBleCmd
 * Description  : 发送Ble cmd
 * Parameters   : cmd : ble cmd
 * Returns      : none
**********************************************/
void BT_SendBleCmd(uint8_t cmd)
{
	IPC_TxControlCmd(cmd);
}


/*********************************************
 * FunctionName : BT_SetBleAdv
 * Description  : 设置Ble 广播
 * Parameters   : advdata: 广播数据
 *                DataLen : 必须是0x1f
 * Returns      : none
**********************************************/
void BT_SetBleAdv(uint8_t *advdata, uint8_t DataLen)
{
	IPC_TxCommon(IPC_TYPE_ADV, advdata, DataLen);
}

/*********************************************
 * FunctionName : BT_SetSppAddr
 * Description  : 设置BT SPP MAC 地址
 * Parameters   : bt_addr :6个字节MAC地址
 * Returns      : none
**********************************************/
void BT_SetSppAddr(uint8_t *bt_addr)
{
    for(uint8_t i = 0; i < 6; i++)
        HWRITEW(mem_lap+i, bt_addr[5-i]);
}

/*********************************************
 * FunctionName : BT_SetBleAddr
 * Description  : 设置BT BLE MAC 地址
 * Parameters   : bt_addr :6个字节MAC地址
 * Returns      : none
**********************************************/
void BT_SetBleAddr(uint8_t * bt_addr)
{
	for(uint8_t i = 0; i < 6; i++)
		HWRITEW(mem_le_lap+i, bt_addr[5-i]);
}

/*********************************************
 * FunctionName : BT_SetSppName
 * Description  : 设置BT SPP 名称
 * Parameters   : spp_name : 蓝牙3.0名称, name_len :名称长度小于16字节
 * Returns      : none
**********************************************/
void BT_SetSppName(uint8_t * spp_name, uint16_t name_len)
{
    if(name_len > 16)
        name_len = 16;

	name_len+=2;

    HWRITEW(mem_local_name, name_len-1);
    HWRITEW(mem_local_name+1, 0x09);
    HWRITEW(mem_local_name_length, name_len);


    for(int i = 2;i < name_len;i++)
		HWRITEW(mem_local_name+i,(uint8_t)spp_name[i - 2]);

	return;

}

/*********************************************
 * FunctionName : BT_SetBleName
 * Description  : 设置BT BLE 名称
 * Parameters   : ble_name : 蓝牙4.0名称, name_len :名称长度小于16字节
 * Returns      : none
**********************************************/
void BT_SetBleName(uint8_t* ble_name, uint16_t name_len)
{
	if(name_len > 16)
        name_len = 16;

	name_len+=2;

	HWRITEW(mem_le_scan_data_len,name_len);
    HWRITEW(mem_le_scan_data,name_len-1);
    HWRITEW(mem_le_scan_data+1,0x09);

	for(int i=2;i<name_len;i++)
		HWRITEW(mem_le_scan_data+i,(uint8_t)ble_name[i-2]);
	for(int i=0;i<name_len-2;i++)
		HWRITEW(mem_le_attlist+21+i,(uint8_t)ble_name[i]);

	return;
}


//BEL 扫描数据
void BT_SetBleScanData(uint8_t* scan_data, int DataLen)
{
    IPC_TxCommon(IPC_TYPE_SCAN, scan_data, DataLen);

}


/*********************************************
 * FunctionName : BT_SetVisibility
 * Description  : BT , BLE 可发现
 * Parameters   : bt_discoverable 0--bt_discoverable OFF;1--bt_discoverable ON
				  ble_discoverable:0--ble_discoverable OFF;1--ble_discoverable ON
 * Returns      : none
**********************************************/
void BT_SetVisibility(Boolean bt_discoverable, Boolean ble_discoverable)
{
    uint8_t bt_visibility_data = 0;
    uint8_t ble_visibility_data = 0;

    if (bt_discoverable == ENABLE)
    {
        bt_visibility_data  = 0x03;
    }
    else
    {
        bt_visibility_data  = 0x0;
    }

    if (ble_discoverable == ENABLE)
    {
        ble_visibility_data  = 0x01;
    }
    else
    {
        ble_visibility_data  = 0x0;
    }

    HWRITEW(mem_scan_mode, bt_visibility_data);
    HWRITEW(mem_le_adv_enable, ble_visibility_data);
}

/*********************************************
 * FunctionName : BT_SendBleData
 * Description  : BLE 发送数据
 * Parameters   : ble_data : ble send data, datalen :数据长度
 * Returns      : none
**********************************************/
void BT_SendBleData(uint8_t * ble_data, uint16_t datalen)
{

    IPC_TxCommon(IPC_TYPE_BLE, ble_data, datalen);

}

/*********************************************
 * FunctionName : BT_DeleteBleService
 * Description  : BLE 删除服务
 * Parameters   : none
 * Returns      : none
**********************************************/
void BT_DeleteBleService(void)
{
	uint8_t ble_delservice;
	ble_delservice = DELETE_SERVICE;

	IPC_TxCommon(IPC_TYPE_SERVICE, &ble_delservice, 0x01);
}


/*********************************************
 * FunctionName : BT_AddBleService
 * Description  : BLE 发送数据
 * Parameters   : ble_service_uuid : ble 服务uuid, service_uuid_len :服务uuid长度
 * Returns      : 0:failed
 *         	      other:Service handle
**********************************************/
void BT_AddBleService(uint8_t* ble_service_uuid, uint16_t service_uuid_len)
{
    uint8_t ble_service[DATA_MAX_LEN] = {0};
    ble_service[0] = ADD_SERVICE;
	ble_service[1] = service_uuid_len;
    memcpy((ble_service + 2), ble_service_uuid, service_uuid_len);

    IPC_TxCommon(IPC_TYPE_SERVICE, ble_service, (service_uuid_len + 2));
}

/*********************************************
 * FunctionName : BT_AddBleCharacteristic
 * Description  : BLE 发送数据
 * Parameters   : ble_data : ble send data, datalen :数据长度
 * Returns      : none
**********************************************/
uint16_t BT_AddBleCharacteristic(uint8_t* ble_Characteristic_uuid, uint16_t service_Characteristic_payload_len)
{
	uint16_t handel = 0;
    uint8_t ble_Characteristic[DATA_MAX_LEN] = {0};
    ble_Characteristic[0] = ADD_CHARACTERISTIC;
    memcpy((ble_Characteristic + 1), ble_Characteristic_uuid, service_Characteristic_payload_len);

    IPC_TxCommon(IPC_TYPE_SERVICE, ble_Characteristic, (service_Characteristic_payload_len + 1));

	delay_ms(10);
	handel = HREADW(mem_le_add_uuid_handle);

	return handel;
}

/*********************************************
 * FunctionName : BT_BleDisconnect
 * Description  : BLE 断开连接
 * Parameters   : void
 * Returns      : none
**********************************************/
void BT_BleDisconnect(void)
{
	BT_SendBleCmd(BT_CMD_LE_DISCONNECT);
}



/*********************************************
 * FunctionName : BT_BleConnectParmUpdateReq
 * Description  : BLE 更新连接参数
 * Parameters   : data : ble updete data, len :数据长度
 * Returns      : none
**********************************************/
void BT_BleConnectParmUpdateReq(uint8_t *data, int len)
{

	for(int i = 0;i < len; i++)
		HWRITEW(mem_le_connection_updata_param+i,(uint8_t)data[i]);

	BT_SendBleCmd(BT_CMD_LE_UPDATE_CONN);

}

/*********************************************
 * FunctionName : BT_BleUpdateMtuReq
 * Description  : BLE 更新MTU 数据
 * Parameters   : newMtuNum : mtu range <23 - 517>
 * Returns      : none
**********************************************/
void BT_BleUpdateMtuReq(uint16_t newMtuNum)
{
	uint8_t ble_mtu;
	ble_mtu = BT_CMD_MTU_EXCHANGE;

	HWRITEW(mem_le_local_mtu,(uint8_t)(newMtuNum&0xff));
	HWRITEW(mem_le_local_mtu+1,(uint8_t)(newMtuNum >> 8));

	IPC_TxCommon(IPC_TYPE_CMD, &ble_mtu, 0x01);
}

/*********************************************
 * FunctionName : BT_SetBleParing
 * Description  : BLE 设置配对模式
 * Parameters   : mode : value range enum PAIR_TypeDef
 * Returns      : none
**********************************************/
void BT_SetBleParing(PAIR_TypeDef mode)
{

	switch (mode)
	{
	case PAIR_NONE:
		HWRITE(mem_le_pairing_mode, 0x00);
		HWRITE(mem_le_pres_auth, 0x00);
		HWRITE(mem_le_pres_iocap, 0x03);
		break;

	case PAIR_JUST_WORK:
		HWRITE(mem_le_pairing_mode, 0x01);
		HWRITE(mem_le_pres_iocap, 0x03);
		HWRITE(mem_le_pres_auth, 0x05);
		break;

	case PAIR_PASSKEY:
		HWRITE(mem_le_pairing_mode, 0x02);
		HWRITE(mem_le_pres_auth, 0x05);
		HWRITE(mem_le_pres_iocap, 0x00);
		break;

	default:
		break;
	}

}

/*********************************************
 * FunctionName : BT_ReadNVRAM
 * Description  : BLE 读NVRAM配置
 * Parameters   : void
 * Returns      : none
**********************************************/
void BT_ReadNVRAM(uint8_t* nvdata, uint8_t len)
{
    uint16_t i = 0;
    do
    {
        nvdata[i] = HREAD(mem_nv_data+i);
		i++;
    } while (i < len);
}



/*********************************************
 * FunctionName : BT_SetNVRAM
 * Description  : BLE 设置NVRAM配置
 * Parameters   : void
 * Returns      : none
**********************************************/
void BT_SetNVRAM(uint8_t * NvData, int len)
{
    for(int i = 0;i < len; i++)
        HWRITE(mem_nv_data+i, NvData[i]);
}

/*********************************************
 * FunctionName : BT_GetBtStatus
 * Description  : 获取蓝牙状态
 * Parameters   : void
 * Returns      : uint8_t
**********************************************/
uint8_t BT_GetBtStatus(void)
{
    union GET_BT_STATE state;

    state.bit.usned = 0;

    // SPP 可发现
    if((HREAD(mem_scan_mode) & 0x01)){
		state.bit.bt_candiscover = 1;
    }
    else{
		state.bit.bt_candiscover = 0;
    }
    // SPP 可连接
	if((HREAD(mem_scan_mode) & 0x02)){
		state.bit.bt_canconnect = 1;
    }
    else{
		state.bit.bt_canconnect = 0;
    }

    // BLE 可发现，可连接
    if(HREAD(mem_le_adv_enable)){
		state.bit.ble_can_discover_connect = 1;
	}
	else{
		state.bit.ble_can_discover_connect = 0;
	}

    //预留 HID 连接
    if((HREAD(mem_ui_state_map) & 0x04))
	{

	}

	// SPP已连接
	if((HREAD(mem_ui_state_map) & 0x10)){
		state.bit.bt_connectstate = 1;
	}
	else{
		state.bit.bt_connectstate = 0;
	}

	// BLE 已连接
	if((HREAD(mem_ui_state_map+1) & 0x02)){
		state.bit.ble_connectstate = 1;
	}
	else{
		state.bit.ble_connectstate = 0;
	}

	return state.all;
}

uint16_t BT_GetVersion(void)
{
    return HREADW(mem_ota_version);
}
