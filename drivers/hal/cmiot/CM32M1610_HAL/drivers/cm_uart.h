  /*
  ***********************************************************************************
  *
  * COPYRIGHT(c) 2021, China Mobile IOT
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of China Mobile IOT nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ***********************************************************************************
  */

#ifndef __CM_UART_H_
#define __CM_UART_H_

#include "cm1610.h"
#ifdef __cplusplus
extern "C" {
#endif

#define IS_UARTE_BAUDRATE(BAUDRATE) ((BAUDRATE > 3000) && (BAUDRATE <3000000))

/*which 7 bit can decide which ram to use for the UART, set 1 is MRAM, clear 0 is SRAM*/
#define UArt_Baud_Ram_Sele  (1<<15)

/** @
  * @defgroup USART_Word_Length
  */
#define USART_WordLength_8b                  (0<<2)
#define USART_WordLength_9b                  (1<<2)

#define IS_USART_WORD_LENGTH(LENGTH) 		(((LENGTH) == USART_WordLength_8b) || \
                                      ((LENGTH) == USART_WordLength_9b))

/** @defgroup USART_Stop_Bits
  * @{
  */
#define USART_StopBits_1                 (0<<3)
#define USART_StopBits_2                 (1<<3)
#define IS_USART_STOPBITS(STOPBITS)		 (((STOPBITS) == USART_StopBits_1) ||  \
              ((STOPBITS) == USART_StopBits_2) )

/** @defgroup USART_Parity
* @{
*/
#define USART_Parity_Even                    (0<<1)
#define USART_Parity_Odd                     (1 << 1)
#define IS_USART_PARITY(PARITY)		 ( ((PARITY) == USART_Parity_Even) || \
                                 ((PARITY) == USART_Parity_Odd))

/** @defgroup USART_Mode
  * @{
  */
#define USART_Mode_Single_Line        (1<<6)
#define USART_Mode_duplex                      (0<<6)
#define IS_USART_MODE(MODE) 		(((MODE) == USART_Mode_Single_Line) ||\
        ((MODE) == USART_Mode_duplex))

/** @defgroup USART_Hardware_Flow_Control
  * @{
  */
#define USART_HardwareFlowControl_None       (0<<4)
#define USART_HardwareFlowControl_ENABLE       (1<<4)
#define IS_USART_HARDWARE_FLOW_CONTROL(CONTROL)\
                              (((CONTROL) == USART_HardwareFlowControl_None) || \
                               ((CONTROL) == USART_HardwareFlowControl_ENABLE))

/**
  * @brief  USART channel define
  */
typedef enum
{
  UARTA = 0,
  UARTB = 1,
}USART_TypeDef;

#define IS_USARTAB(USARTx)         (USARTx == UARTA)||\
            (USARTx == UARTB)

/**
  * @brief  USART Init Structure definition
  */

/** @defgroup  USART_RXBuffer
  * @{
  */
#define IS_USART_RXBuffer(RxBuffer)     ((RxBuffer > 0x10000000) || (RxBuffer < 0x10001fff) ) |\
                                        ((RxBuffer < 0x10004000) ||(RxBuffer > 0x10004fff))|\
                                        ((RxBuffer < 0x10010000) ||(RxBuffer > 0x10013fff))

/** @defgroup  USART_TXBuffer
  * @{
  */
#define IS_USART_TXBuffer(TxBuffer)     ((0x10000000 < TxBuffer ) ||(TxBuffer < 0x10001fff) ) |\
                                        ((TxBuffer < 0x10004000) ||(TxBuffer > 0x10004fff))|\
                                        ((TxBuffer < 0x10010000) ||(TxBuffer > 0x10013fff))

typedef struct
{
  uint32_t USART_BaudRate;            /*!< This member configures the USART communication baud rate.
                                         3K - 3M bps*/

  uint16_t USART_WordLength;          /*!< Specifies the number of data bits transmitted or received in a frame.
                                           This parameter can be a value of @ref USART_Word_Length */

  uint16_t USART_StopBits;            /*!< Specifies the number of stop bits transmitted.s */

  uint16_t USART_Parity;              /*!< Specifies the parity mode. */

  uint16_t USART_Mode;                /*!< Specifies wether the Receive or Transmit mode is enabled or disabled.e */

  uint16_t USART_HardwareFlowControl; /*!< Specifies wether the hardware flow control mode is enabled
                                           or disabled.
                                           This parameter can be a value of @ref USART_Hardware_Flow_Control */

} USART_InitTypeDef;

/**
  * @brief  Initializes the UARTx peripheral according to the specified
  *         parameters in the UART_InitStruct.
  * @param  UARTx : Select the UART peripheral. UARTA,UARTB
  * @param  UART_InitStruct : pointer to a UART_InitTypeDef structure
  *         that contains the configuration information for the specified UART
  *         peripheral.
  * @return None
  */
void USART_Init(USART_TypeDef USARTx, USART_InitTypeDef* USART_InitStruct);

/**
  * @brief  Deinit the UARTx peripheral registers
  *         to their default reset values.
  * @param  UARTx : Select the UART peripheral.UARTA,UARTB
  * @return None
  */
void USART_DeInit(USART_TypeDef USARTx);

/**
  * @brief  Transmits single data through the UARTx peripheral.
  * @param  UARTx : Select the UART peripheral. UARTA,UARTB
  * @param  Data : the data to transmit.
  * @return None
  */
void USART_SendData(USART_TypeDef USARTx, uint8_t Data);

/**
  * @brief  Returns the most recent received data by the UARTx peripheral.
  * @param  UARTx : Select the UART peripheral. UARTA,UARTB
  * @return The received data.
  */
uint8_t USART_ReceiveData(USART_TypeDef USARTx);

/**
  * @brief  Saves the received data to a buff
  * @param  UARTx : Select the UART peripheral. UARTA,UARTB
  * @param	RxBuff : The buffer where data receive to.
  * @param	RxSize : The length of receive data.
  * @return None
  */
uint16_t USART_ReadBuff(USART_TypeDef USARTx, uint8_t* RxBuff, uint16_t RxSize);

/**
  * @brief  Transmits specified length data through the UARTx peripheral.
  * @param  UARTx : Select the UART peripheral. UARTA,UARTB
  * @param  TxBuff : The buffer where data transmit from.
  * @param	TxLen : The specified length to transmit.
  * @return None
  */
uint16_t USART_SendBuff(USART_TypeDef USARTx, uint8_t* TxBuff, uint16_t TxLen);

/**
  * @brief  Judge Rx fifo empty is or not.
  * @param  UARTx: Select the UART peripheral.
  * @retval TRUE:Rx fifo is not empty.
  *         FALSE:Rx fifo is empty;
  */
Boolean USART_IsRXFIFONotEmpty(USART_TypeDef USARTx);

/**
  * @brief  Get the length of RX DMA.
  * @param  UARTx : Select the UART peripheral. UARTA,UARTB
  * @return None.
  */
uint16_t USART_GetRxCount(USART_TypeDef USARTx);

/**
  * @brief  Returns the most recent received data by the UARTx peripheral.
  * @param  UARTx : Select the UART peripheral. UARTA,UARTB
  * @param  Num : interrupt Number of data.
  * @return The received data.
  */
void USART_SetRxITNum(USART_TypeDef USARTx, uint8_t Num);

/**
  * @brief  ets the RX interrupt trigger timeout.
  * @param  time : timeout of trigger interrupt.(us)
  * @return None.
  */
void USART_SetRxTimeout(USART_TypeDef USARTx, uint16_t time);

/**
  * @brief  Config Tx Interrupt trigger
  * @param  UARTx : Select the UART peripheral. UARTA,UARTB
  * @param  NewState: ENABLE, DISABLE
  * @return None.
  */

void USART_TxITConfig(USART_TypeDef USARTx, FunctionalState NewState);

/**
  * @brief  Clear Tx interrupt
  * @param  UARTx : Select the UART peripheral. UARTA,UARTB
  * @return None.
  */
void USART_ClearTxITPendingBit(USART_TypeDef USARTx);

/**
  * @brief  Judge interrupt type
  * @param  UARTx : Select the UART peripheral. UARTA,UARTB
  * @return TRUE: is Tx IRQ; FALSE: is Rx IRQ.
  */
Boolean UART_IsTxIRQ(USART_TypeDef USARTx);

#ifdef __cplusplus
}
#endif

#endif /* __CM_UART_H_ */

