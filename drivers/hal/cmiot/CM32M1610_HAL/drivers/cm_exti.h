  /*
  ***********************************************************************************
  *
  * COPYRIGHT(c) 2021, China Mobile IOT
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of China Mobile IOT nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ***********************************************************************************
  */
#ifndef _EXTI_H_
#define _EXTI_H_
#include "cm1610.h"
#include "cm_gpio.h"

#define	EXIT_Num         GPIO_GROUP_NUM
#define	EXIT_Pin_Num     GPIO_PIN_NUM

typedef enum
{
    EXTI_Trigger_Off,
    EXTI_Trigger_Rising,
    EXTI_Trigger_Falling,
    EXTI_Trigger_Rising_Falling
} EXTI_TriggerTypeDef;

#define EXTI_Line0          ((uint32_t)0x0000)  /*!< External interrupt line 0 */
#define EXTI_Line1          ((uint32_t)0x0001)  /*!< External interrupt line 1 */
#define EXTI_Line2          ((uint32_t)0x0002)  /*!< External interrupt line 2 */
#define EXTI_Line3          ((uint32_t)0x0003)  /*!< External interrupt line 3 */

#define EXTI_PinSource0          ((uint8_t)0x801)  /*!< Pin 0 selected */
#define EXTI_PinSource1          ((uint8_t)0x802)  /*!< Pin 1 selected */
#define EXTI_PinSource2          ((uint8_t)0x804)  /*!< Pin 2 selected */
#define EXTI_PinSource3          ((uint8_t)0x808)  /*!< Pin 3 selected */
#define EXTI_PinSource4          ((uint8_t)0x810)  /*!< Pin 4 selected */
#define EXTI_PinSource5          ((uint8_t)0x820)  /*!< Pin 5 selected */
#define EXTI_PinSource6          ((uint8_t)0x840)  /*!< Pin 6 selected */
#define EXTI_PinSource7          ((uint8_t)0x880)  /*!< Pin 7 selected */

/**
  * @brief  Clear interrupt flag
  * @param	EXTI_Line:EXTI_Line0...EXTI_Line3
  * @param	EXTI_PinSource:EXTI_PinSource0...EXTI_PinSource7 or EXTI_PinSourceAll
  * @retval none
  */
void EXTI_ClearITPendingBit(uint32_t EXTI_Line, uint32_t EXTI_PinSource);

/**
  * @brief  Deinitializes the EXTI registers to default reset values.
  * @param	none
  * @retval none
  */
void EXTI_DeInit(void);

/**
  * @brief  get interrupt status
  * @param	EXTI_Line:EXTI_Line0...EXTI_Line3
  * @retval none
  */
uint8_t EXTI_GetITLineStatus(uint32_t EXTI_Line);

/**
  * @brief  EXTI LineConfig
  * @param	EXTI_Line:EXTI_Line0...EXTI_Line3
  * @param	EXTI_PinSource:EXTI_PinSource0...EXTI_PinSource7 or EXTI_PinSourceAll
  * @param	EXTI_Trigger:EXTI Trigger mode
  * @retval none
  */
void EXTI_LineConfig(uint32_t EXTI_Line, uint8_t EXTI_PinSource, EXTI_TriggerTypeDef EXTI_Trigger);

#endif
