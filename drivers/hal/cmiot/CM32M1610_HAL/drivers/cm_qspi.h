  /*
  ***********************************************************************************
  *
  * COPYRIGHT(c) 2021, China Mobile IOT
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of China Mobile IOT nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ***********************************************************************************
  */

#ifndef __CM_QSPI_H_
#define __CM_QSPI_H_
#include "cm1610.h"

#define FlashCMD_PageErase          0x81
#define W25X_WRITE_ENABLE           0x06
#define W25X_WRITE_DISABLE          0x04
#define W25X_READ_STATUS1           0x05
#define W25X_READ_STATUS2           0x35
#define W25X_WRITE_STATUS           0x01
#define W25X_READ_DATA              0x03
#define W25X_FASTREAD_DATA          0x0B
#define W25X_FASTREAD_DUAL1         0x3B
#define W25X_FASTREAD_DUAL2         0xBB

#define W25X_FASTREAD_QUAD1         0x6B
#define W25X_FASTREAD_QUAD2         0xEB
#define W25X_FASTREAD_QUAD3         0xE7

#define W25X_PAGE_PROGRAM           0x02
#define W25X_SECTOR_ERASE           0x20
#define W25X_BLOCK_ERASE32K         0x52
#define W25X_BLOCK_ERASE64K         0xD8
#define W25X_CHIP_ERASE             0xC7
#define W25X_POWER_DOWN             0xB9
#define W25X_RELEASE_POWERDOWN      0xAB
#define W25X_DEVICEID               0xAB
#define W25X_MANUFACT_DEVICEID      0x90
#define W25X_JEDEC_DEVICEID         0x9F

#define QSPICFG_DUAL_MODE			(1 << 0)
#define QSPICFG_QUAD_MODE			(2 << 0)
#define QSPICFG_MBYTE				(1 << 4)
#define QSPICFG_MBYTE_CONT		    (1 << 5)
#define QSPICFG_MRAM				(1 << 6)
#define QCSFT_DUMMY                 8

#define QSPICFG_MODE_3B			    QSPICFG_DUAL_MODE | 8 << QCSFT_DUMMY                           | QSPICFG_MRAM
#define QSPICFG_MODE_6B			    QSPICFG_QUAD_MODE | 8 << QCSFT_DUMMY                           | QSPICFG_MRAM
#define QSPICFG_MODE_BB			    QSPICFG_DUAL_MODE | QSPICFG_MBYTE | 0x6000                     | QSPICFG_MRAM
#define QSPICFG_MODE_EB			    QSPICFG_QUAD_MODE | QSPICFG_MBYTE | 0x6000 | 4 << QCSFT_DUMMY  | QSPICFG_MRAM
#define QSPICFG_MODE_E7			    QSPICFG_QUAD_MODE | QSPICFG_MBYTE | 0x6000 | 2 << QCSFT_DUMMY  | QSPICFG_MRAM

/*
 * @brief: flash read
 * @param: flash_addr
 * @param: len
 * @param: rxbuf
 * @return: ERROR ,SUCCESS
 */
uint8_t  QSPI_ReadFlashData(uint32_t flash_addr,uint32_t rlen,uint8_t *rbuf);

/*
 * @brief: flash write
 * @param: flash_addr
 * @param: len
 * @param: txbuf
 * @return: ERROR ,SUCCESS
 */
uint8_t  QSPI_WriteFlashData(uint32_t flash_addr,uint32_t len,uint8_t *tbuf);

/*
 * @brief:chip erase
 * @return: ERROR ,SUCCESS
 */
uint8_t  QSPI_ChipEraseFlash(void);

/*
 * @brief:block erase  64K
 * @param:flash addr
 * @return: ERROR ,SUCCESS
 */
uint8_t  QSPI_64kBlockEraseFlash(uint32_t flash_addr);

/*
 * @brief:block erase  32K
 * @param:flash addr
 * @return: ERROR ,SUCCESS
 */
uint8_t  QSPI_32kBlockEraseFlash(uint32_t flash_addr);

/*
 * @brief:sector erase 4kB
 * @param:flash addr
 * @return: ERROR ,SUCCESS
 */
uint8_t  QSPI_SectorEraseFlash(uint32_t flash_addr);

/*
 * @brief:sector erase 256Byte(1page)
 * @param:flash addr
 * @return: ERROR ,SUCCESS
 */
uint8_t QSPI_PageEraseFlash(uint32_t flash_addr);

/*
 * @brief:Lock Flash
 * @param:none
 * @return: ERROR ,SUCCESS
 */
uint8_t QSPI_LockFlash(void);

/*
 * @brief:Unlock Flash
 * @param:none
 * @return: ERROR ,SUCCESS
 */
uint8_t QSPI_UnlockFlash(void);

#endif

