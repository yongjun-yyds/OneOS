  /*
  ***********************************************************************************
  *
  * COPYRIGHT(c) 2021, China Mobile IOT
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of China Mobile IOT nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ***********************************************************************************
  */

#ifndef __CM_IIC_H__
#define __CM_IIC_H__
#include "cm1610.h"

/**
  *@brief IIC Core parameters struct
  */
typedef struct
{
    uint8_t IIC_Scll;	/*!< Specifies the Clock Pulse Width Low. */
    uint8_t IIC_Sclh;   /*!< Specifies the Clock Pulse Width High. */
    uint8_t IIC_Stsu;   /*!< Specifies the Start Setup Time. */
    uint8_t IIC_Sthd;   /*!< Specifies the Start Hold Time. */
    uint8_t IIC_Sosu;   /*!< Specifies the Stop Setup Time. */
    uint8_t IIC_Dtsu;   /*!< Specifies the Data Setup Time. */
    uint8_t IIC_Dthd;   /*!< Specifies the Data Hold Time. */
} IIC_InitTypeDef;

/**
  * @brief	Initializes the IIC peripheral according to the specified
  *        	parameters in the IIC_InitTypeDef.
  * @param  IIC_InitStruct: pointer to a IIC_InitTypeDef structure
  *         that contains the configuration information for the specified IIC
  *         peripheral.
  * @retval None
  */
void IIC_Init(IIC_InitTypeDef *IIC_InitStruct);

/**
  * @brief	Write slave devices.
  * @param  Src: pointer to the buffer that contains the data you want to send.
  * @param  len: the length of send data
  * @note	  After writting slave device, must delay a period of time before reading slave device.
  * @retval None
  */
void IIC_SendData(uint8_t *Src, uint16_t len);

/**
  * @brief	Read slave devices.
  * @param  Src: pointer to the buffer that contains the data you want to send.
  * @param  len: the length of send data
  * @param  Dest: pointer to the buffer that contains the data received from slaver.
  * @param	Destlen: the length of received data
  * @retval None
  */
void IIC_ReceiveData(uint8_t *Src, uint16_t Srclen, uint8_t *Dest, uint16_t Destlen);

#endif
