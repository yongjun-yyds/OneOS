/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_iwdg.c
 *
 * @brief       This file implements iwdg driver for cm32
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <board.h>
#include <os_memory.h>
#include <bus/bus.h>
#include "drv_iwdg.h"
#include "watchdog.h"

#define DBG_TAG "drv.iwdg"
#include <dlog.h>

static os_watchdog_t cm32_watchdog;

uint32_t timeout_max = 26;
uint32_t lsi_freq    = 40000;

static os_err_t cm32_iwdt_init(os_watchdog_t *wdt)
{
    IWDG_WriteConfig(IWDG_WRITE_ENABLE);

    /* IWDG counter clock: LSI/256 */
    IWDG_SetPrescalerDiv(IWDG_PRESCALER_DIV256);

    return OS_SUCCESS;
}

static os_err_t cm32_iwdt_control(os_watchdog_t *wdt, int cmd, void *arg)
{
    uint32_t timeout = 0;

    switch (cmd)
    {
    case OS_DEVICE_CTRL_WDT_KEEPALIVE:
        IWDG_ReloadKey();
        return OS_SUCCESS;

    /* set window value */
    case OS_DEVICE_CTRL_WDT_SET_TIMEOUT:
        timeout = *(uint32_t *)arg;
        if (timeout > timeout_max)
        {
            LOG_E(DBG_TAG, "timeout not support, the max timeout can be 26s.");
            break;
        }
        else
        {
            IWDG_CntReload(timeout * lsi_freq / 256);
            return OS_SUCCESS;
        }

    case OS_DEVICE_CTRL_WDT_GET_TIMEOUT:
        break;

    case OS_DEVICE_CTRL_WDT_START:
        IWDG_Enable();

        LOG_I(DBG_TAG, "wdt start.");
        return OS_SUCCESS;
    }

    return OS_NOSYS;
}

const static struct os_watchdog_ops ops = {
    .init    = &cm32_iwdt_init,
    .control = &cm32_iwdt_control,
};

static int cm32_iwdg_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    os_err_t ret = OS_SUCCESS;

    cm32_watchdog.ops = &ops;

    ret = os_hw_watchdog_register(&cm32_watchdog, dev->name, OS_NULL);

    if (ret != OS_SUCCESS)
    {
        LOG_E(DBG_TAG, "Os device register failed %d\n", ret);
    }

    return ret;
}

OS_DRIVER_INFO cm32_iwdt_driver = {
    .name  = "IWDG_HandleTypeDef",
    .probe = cm32_iwdg_probe,
};

OS_DRIVER_DEFINE(cm32_iwdt_driver, OS_INIT_LEVEL_PRE_DEVICE, OS_INIT_SUBLEVEL_LOW);

