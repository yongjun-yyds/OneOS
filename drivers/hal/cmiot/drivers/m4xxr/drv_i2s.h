/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_i2s.h
 *
 * @brief       This file implements spi driver for cm32.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __DRV_I2S_H_
#define __DRV_I2S_H_

struct cm32_i2s_info
{
    SPI_Module      *i2s_base;
    I2S_InitType     i2s_init_structure;

    uint32_t         dma_tx_clk;
    IRQn_Type        dma_tx_irqn;
    DMA_ChannelType *dma_tx_channel;

    uint32_t         dma_rx_clk;
    IRQn_Type        dma_rx_irqn;
    DMA_ChannelType *dma_rx_channel;

    uint32_t         gpio_remap;
    GPIO_Module     *port_ck;
    uint16_t         pin_ck;
    GPIO_Module     *port_sd;
    uint16_t         pin_sd;
    GPIO_Module     *port_ws;
    uint16_t         pin_ws;
    GPIO_Module     *port_mck;
    uint16_t         pin_mck;
};

#endif /*__DRV_I2S_H_ */
