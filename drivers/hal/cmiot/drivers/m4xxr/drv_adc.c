/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at

 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_adc.c
 *
 * @brief       This file implements adc driver for cm32.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <board.h>
#include <os_stddef.h>
#include <os_memory.h>
#include <bus/bus.h>
#include <string.h>
#include <drv_log.h>
#include "nuclei_sdk_soc.h"

#include <drv_adc.h>

#define DBG_TAG "drv.adc"
#include <dlog.h>

struct cm32_adc
{
    struct os_adc_device  adc;
    struct cm32_adc_info *info;
};

void gpio_port_clk_enable(GPIO_Module *port)
{
    if (port == GPIOA)
        RCC_EnableAPB2PeriphClk(RCC_APB2_PERIPH_GPIOA, ENABLE);
    else if (port == GPIOB)
        RCC_EnableAPB2PeriphClk(RCC_APB2_PERIPH_GPIOB, ENABLE);
    else if (port == GPIOC)
        RCC_EnableAPB2PeriphClk(RCC_APB2_PERIPH_GPIOC, ENABLE);
    else if (port == GPIOD)
        RCC_EnableAPB2PeriphClk(RCC_APB2_PERIPH_GPIOD, ENABLE);
    else if (port == GPIOE)
        RCC_EnableAPB2PeriphClk(RCC_APB2_PERIPH_GPIOE, ENABLE);
    else if (port == GPIOF)
        RCC_EnableAPB2PeriphClk(RCC_APB2_PERIPH_GPIOF, ENABLE);
}

static os_err_t _adc_init(struct cm32_adc_info *info)
{
    GPIO_InitType GPIO_InitStructure;
    ADC_InitType  ADC_InitStructure;

    for (int i = 0; i < 20; i++)
    {
        if (info->channel_bitmap & 1 << i)
        {
            GPIO_InitStruct(&GPIO_InitStructure);

            gpio_port_clk_enable(info->gpio_cfg[i].port);

            GPIO_InitStructure.Pin       = info->gpio_cfg[i].pin;
            GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
            GPIO_Init(info->gpio_cfg[i].port, &GPIO_InitStructure);
        }
    }

    if (info->adc_base == ADC1)
        RCC_EnableAHBPeriphClk(RCC_AHB_PERIPH_ADC1, ENABLE);
    else if (info->adc_base == ADC2)
        RCC_EnableAHBPeriphClk(RCC_AHB_PERIPH_ADC2, ENABLE);
    else if (info->adc_base == ADC3)
        RCC_EnableAHBPeriphClk(RCC_AHB_PERIPH_ADC3, ENABLE);
    else if (info->adc_base == ADC4)
        RCC_EnableAHBPeriphClk(RCC_AHB_PERIPH_ADC4, ENABLE);

    ADC_ConfigClk(info->adc_base, ADC_CTRL3_CKMOD_AHB, RCC_ADCHCLK_DIV16);
    RCC_ConfigAdc1mClk(RCC_ADC1MCLK_SRC_HSE, RCC_ADC1MCLK_DIV8);

    ADC_InitStructure.WorkMode       = ADC_WORKMODE_INDEPENDENT;
    ADC_InitStructure.MultiChEn      = DISABLE;
    ADC_InitStructure.ContinueConvEn = DISABLE;
    ADC_InitStructure.ExtTrigSelect  = ADC_EXT_TRIGCONV_NONE;
    ADC_InitStructure.DatAlign       = ADC_DAT_ALIGN_R;
    ADC_InitStructure.ChsNumber      = 1;
    ADC_Init(info->adc_base, &ADC_InitStructure);
}

static os_err_t cm32_adc_enabled(struct os_adc_device *dev, os_bool_t enable)
{
    struct cm32_adc *cm32_adc;

    OS_ASSERT(dev != OS_NULL);

    cm32_adc = os_container_of(dev, struct cm32_adc, adc);

    if (!enable)
    {
        ADC_Enable(cm32_adc->info->adc_base, DISABLE);
        return OS_SUCCESS;
    }
    else
    {
        /* Enable ADC */
        ADC_Enable(cm32_adc->info->adc_base, ENABLE);
        /* Check ADC Ready */
        while(ADC_GetFlagStatusReady(cm32_adc->info->adc_base, ADC_FLAG_RDY) == RESET)
        ;
        /* Start ADC calibration */
        ADC_StartCalibration(cm32_adc->info->adc_base);
        /* Check the end of ADC calibration */
        while (ADC_GetCalibrationStatus(cm32_adc->info->adc_base))
        ;
        return OS_SUCCESS;
    }
}

static os_err_t cm32_adc_control(struct os_adc_device *dev, int cmd, void *arg)
{
    return OS_SUCCESS;
}


static os_err_t cm32_adc_read(struct os_adc_device *dev, uint32_t channel, int32_t *buff)
{
    struct cm32_adc *cm32_adc;

    cm32_adc = os_container_of(dev, struct cm32_adc, adc);

    ADC_ConfigRegularChannel(cm32_adc->info->adc_base, channel, 1, ADC_SAMP_TIME_239CYCLES5);
    /* Start ADC Software Conversion */
    ADC_EnableSoftwareStartConv(cm32_adc->info->adc_base, ENABLE);

    while(ADC_GetFlagStatus(cm32_adc->info->adc_base, ADC_FLAG_ENDC)==0)
    {
    }

    ADC_ClearFlag(cm32_adc->info->adc_base, ADC_FLAG_ENDC);
    ADC_ClearFlag(cm32_adc->info->adc_base, ADC_FLAG_STR);

    *buff = (int32_t)((uint64_t)ADC_GetDat(cm32_adc->info->adc_base) * dev->mult >> dev->shift);

    return OS_SUCCESS;
}

static const struct os_adc_ops cm32_adc_ops = {
    .adc_enabled = cm32_adc_enabled,
    .adc_control = cm32_adc_control,
    .adc_read    = cm32_adc_read,
};

static int cm32_adc_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    os_err_t ret = OS_SUCCESS;
    struct cm32_adc *cm32_adc;

    cm32_adc = (struct cm32_adc *)os_calloc(1, sizeof(struct cm32_adc));
    if (cm32_adc == OS_NULL)
    {
        return OS_FAILURE;
    }

    cm32_adc->info = (struct cm32_adc_info *)dev->info;

    _adc_init(cm32_adc->info);

    cm32_adc->adc.ops       = &cm32_adc_ops;
    cm32_adc->adc.max_value = (1UL << 12) - 1;
    cm32_adc->adc.ref_low   = 0; /* ref 0 - 3.3v */
    cm32_adc->adc.ref_hight = 3300;

    ret = os_hw_adc_register(&cm32_adc->adc, dev->name, NULL);

    return ret;
}

OS_DRIVER_INFO cm32_adc_driver = {
    .name  = "ADC_HandleTypeDef",
    .probe = cm32_adc_probe,
};

OS_DRIVER_DEFINE(cm32_adc_driver, OS_INIT_LEVEL_DEVICE, OS_INIT_SUBLEVEL_LOW);
