/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_crypto_crc.c
 *
 * @brief       This file implements stm32 crypto crc driver.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <os_task.h>
#include <device.h>
#include <stdlib.h>
#include <string.h>
#include "drv_crypto.h"
#include <board.h>
#include <os_memory.h>

static uint8_t crc_param_width;

static int32_t cm32_crc_config(struct hwcrypto_crc *ctx)
{
    if (ctx->crc_cfg.width == 16)
    {
        if (ctx->crc_cfg.poly != 0x8005)
        {
            os_kprintf("param error!\r\n"
                       "support crc param:\r\n"
                       "poly    : 0x8005 \r\n"
                       "width   : 0x00000010\r\n");

            os_kprintf("unsupport crc param:\r\n"
                       "poly    : 0x%08x\r\n"
                       "width   : 0x%08x\r\n",
                       ctx->crc_cfg.poly,
                       ctx->crc_cfg.width);
            return -1;
        }
    }
    else if (ctx->crc_cfg.width == 32)
    {
        if (ctx->crc_cfg.last_val != 0xFFFFFFFF || ctx->crc_cfg.poly != 0x04C11DB7)
        {
            os_kprintf("param error!\r\n"
                       "support crc param:\r\n"
                       "last_val: 0xFFFFFFFF\r\n"
                       "poly    : 0x04C11DB7 \r\n"
                       "width   : 0x00000020\r\n");

            os_kprintf("unsupport crc param:\r\n"
                       "last_val: 0x%08x\r\n"
                       "poly    : 0x%08x\r\n"
                       "width   : 0x%08x\r\n",
                       ctx->crc_cfg.last_val,
                       ctx->crc_cfg.poly,
                       ctx->crc_cfg.width);
            return -1;
        }
    }
    else
    {
        os_kprintf("invalide crc width %d, support 16, 32.\r\n", ctx->crc_cfg.width);
        return -1;
    }

    if (ctx->crc_cfg.width == 16)
    {
        CRC->CRC16D = ctx->crc_cfg.last_val;
    }

    crc_param_width = ctx->crc_cfg.width;

    return 0;
}

static uint8_t reverse_byte_bit(uint8_t n)
{
    n = ((n >> 1) & 0x55) | ((n << 1) & 0xaa);
    n = ((n >> 2) & 0x33) | ((n << 2) & 0xcc);
    n = ((n >> 4) & 0x0f) | ((n << 4) & 0xf0);

    return n;
}

static uint32_t reverse_bit(uint32_t n, uint16_t width)
{
    n = ((n >> 1) & 0x55555555) | ((n << 1) & 0xaaaaaaaa);
    n = ((n >> 2) & 0x33333333) | ((n << 2) & 0xcccccccc);
    n = ((n >> 4) & 0x0f0f0f0f) | ((n << 4) & 0xf0f0f0f0);
    n = ((n >> 8) & 0x00ff00ff) | ((n << 8) & 0xff00ff00);
    n = ((n >> 16) & 0x0000ffff) | ((n << 16) & 0xffff0000);

    switch (width)
    {
    case 16:
        return (n >> 16) & 0xffff;
    case 32:
        return n;
    }

    return 0;
}

static uint32_t cm32_crc_update(struct hwcrypto_crc *ctx, const uint8_t *in, os_size_t length)
{
    int                          i;
    uint32_t                  tmp;
    uint32_t                  result      = 0;
    struct cm32_hwcrypto_device *cm32_hw_dev = (struct cm32_hwcrypto_device *)ctx->parent.device->user_data;

    os_mutex_lock(cm32_hw_dev->mutex, OS_WAIT_FOREVER);

    if (ctx->crc_cfg.width == 16)
    {
        for (i = 0; i < length; i++)
        {
            if (ctx->crc_cfg.flags & CRC_FLAG_REFIN)
                result = CRC16_CalcCRC(reverse_byte_bit(in[i]));
            else
                result = CRC16_CalcCRC(in[i]);
        }
    }
    else if (ctx->crc_cfg.width == 32)
    {
        if (length % 4 != 0)
        {
            os_kprintf("crc length must be 4 bytes align.\r\n");
            os_mutex_unlock(cm32_hw_dev->mutex);
            return 0;
        }

        for (i = 0; i < length / 4; i++, in += 4)
        {
            if (ctx->crc_cfg.flags & CRC_FLAG_REFIN)
                tmp = (reverse_byte_bit(in[0]) << 24) | (reverse_byte_bit(in[1]) << 16) |
                      (reverse_byte_bit(in[2]) << 8) | (reverse_byte_bit(in[3]) << 0);
            else
                tmp = (in[0] << 24) | (in[1] << 16) | (in[2] << 8) | (in[3] << 0);

            result = CRC32_CalcCrc(tmp);
        }
    }
    else
    {
        os_kprintf("invalide crc width %d, support 16, 32.\r\n", ctx->crc_cfg.width);
        goto _exit;
    }

    if (ctx->crc_cfg.flags & CRC_FLAG_REFOUT)
        result = reverse_bit(result, ctx->crc_cfg.width);

    result = result ^ ctx->crc_cfg.xorout;

    ctx->crc_cfg.last_val = result;

    if (ctx->crc_cfg.width == 16)
        CRC->CRC16D = result;

_exit:
    os_mutex_unlock(cm32_hw_dev->mutex);

    return result;
}

static const struct hwcrypto_crc_ops crc_ops = {
    .config = cm32_crc_config,
    .update = cm32_crc_update,
};

static os_err_t cm32_crc_crypto_create(struct os_hwcrypto_ctx *ctx)
{
    struct cm32_hwcrypto_device *cm32_hw_dev = (struct cm32_hwcrypto_device *)ctx->device->user_data;

    RCC_EnableAHBPeriphClk(RCC_AHB_PERIPH_CRC, ENABLE);

    ((struct hwcrypto_crc *)ctx)->ops = &crc_ops;

    return OS_SUCCESS;
}

static void cm32_crc_crypto_destroy(struct os_hwcrypto_ctx *ctx)
{
    if (crc_param_width == 16)
    {
        CRC16_SetCleanEnable();
    }
    else if (crc_param_width == 32)
    {
        CRC32_ResetCrc();
    }

    RCC_EnableAHBPeriphClk(RCC_AHB_PERIPH_CRC, DISABLE);
}

static os_err_t cm32_crc_crypto_clone(struct os_hwcrypto_ctx *des, const struct os_hwcrypto_ctx *src)
{
    return OS_SUCCESS;
}

static void cm32_crc_crypto_reset(struct os_hwcrypto_ctx *ctx)
{
}

static const struct os_hwcrypto_ops _ops = {
    .create  = cm32_crc_crypto_create,
    .destroy = cm32_crc_crypto_destroy,
    .copy    = cm32_crc_crypto_clone,
    .reset   = cm32_crc_crypto_reset,
};

static int cm32_crc_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    struct cm32_hwcrypto_device *cm_hwcrypto = os_calloc(1, sizeof(struct cm32_hwcrypto_device));

    OS_ASSERT(cm_hwcrypto);

    struct os_hwcrypto_device *hwcrypto = &cm_hwcrypto->hwcrypto;

    hwcrypto->ops = &_ops;

    hwcrypto->user_data = hwcrypto;

    if (os_hwcrypto_crc_register(hwcrypto, dev->name) != OS_SUCCESS)
    {
        os_kprintf("cm32 crc probe failed %s.\r\n", dev->name);
        os_free(cm_hwcrypto);
        return -1;
    }

    cm_hwcrypto->mutex = os_mutex_create(OS_NULL, OS_HWCRYPTO_DEFAULT_NAME, OS_FALSE);
    return 0;
}

OS_DRIVER_INFO cm32_crc_driver = {
    .name  = "CRC_HandleTypeDef",
    .probe = cm32_crc_probe,
};

OS_DRIVER_DEFINE(cm32_crc_driver, OS_INIT_LEVEL_DEVICE, OS_INIT_SUBLEVEL_MIDDLE);

