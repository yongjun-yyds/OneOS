/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_i2s.c
 *
 * @brief       This file implements i2s driver for cm32
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include <oneos_config.h>
#include <audio/i2s.h>
#include <drv_i2s.h>

struct cm32_i2s_tx_block
{
    os_list_node_t  node;
    uint8_t        *data;
    os_size_t       data_len;
};

struct cm32_i2s_tcb
{
    os_list_node_t  read_list;
    os_list_node_t  write_list;
    uint8_t         status;
    os_semaphore_id sem;
    DMA_InitType    DMA_InitStructure;
};

struct cm32_i2s_rx_block
{
    os_list_node_t  node;
    uint8_t        *data;
    os_size_t       data_len;
};

struct cm32_i2s_rcb
{
    os_list_node_t  read_list;
    os_list_node_t  write_list;
    uint8_t         status;
    os_semaphore_id sem;

    uint8_t      *dma_read_buffer;
    uint8_t       dma_read_index;
};

struct cm32_i2s
{
    os_i2s_device_t       i2s;
    struct cm32_i2s_info *info;
    os_list_node_t        list;
    struct cm32_i2s_tcb   tcb;
    struct cm32_i2s_rcb   rcb;
};

static os_list_node_t cm32_i2s_list = OS_LIST_INIT(cm32_i2s_list);

void cm32_i2s_dma_tc_callback(DMA_ChannelType *dma_channel)
{
    struct cm32_i2s          *i2s_dev;
    struct os_device_cb_info  info;
    os_list_node_t *node;
    struct cm32_i2s_tx_block *tx_block;

    os_list_for_each_entry(i2s_dev, &cm32_i2s_list, struct cm32_i2s, list)
    {
        if (i2s_dev->info->dma_tx_channel == dma_channel)
        {

            node = os_list_first(&i2s_dev->tcb.read_list);
            os_list_del(node);
            os_list_add_tail(&i2s_dev->tcb.write_list, node);
            tx_block = (struct cm32_i2s_tx_block*)node;
            info.data = tx_block->data;
            info.size = tx_block->data_len;
            os_hw_i2s_isr(&i2s_dev->i2s, &info);
            os_semaphore_post(i2s_dev->tcb.sem);
        
            if (!os_list_empty(&i2s_dev->tcb.read_list))
            {
                node = os_list_first(&i2s_dev->tcb.read_list);
                tx_block = (struct cm32_i2s_tx_block*)node;
        
                i2s_dev->tcb.DMA_InitStructure.MemAddr = (uint32_t)tx_block->data;
                i2s_dev->tcb.DMA_InitStructure.BufSize = (uint32_t)tx_block->data_len / 2;
                DMA_Init(i2s_dev->info->dma_tx_channel, &i2s_dev->tcb.DMA_InitStructure);
                DMA_EnableChannel(i2s_dev->info->dma_tx_channel, ENABLE);
            }
            else
            {
                i2s_dev->tcb.status = 0;
            }
            
            break;
        }
    }
}

void cm32_i2s_dma_rc_callback(DMA_ChannelType *dma_channel)
{
    struct cm32_i2s          *i2s_dev;
    struct os_device_cb_info  info;
    os_list_node_t *node;
    struct cm32_i2s_rx_block *rx_block;

    os_list_for_each_entry(i2s_dev, &cm32_i2s_list, struct cm32_i2s, list)
    {
        if (i2s_dev->info->dma_rx_channel == dma_channel)
        {
            if (!os_list_empty(&i2s_dev->rcb.write_list))
            {
                node = os_list_first(&i2s_dev->rcb.write_list);
                os_list_del(node);
                rx_block = (struct cm32_i2s_rx_block*)node;

                for (int i = 0; i < OS_AUDIO_RECORD_FIFO_SIZE; i++)
                {
                    rx_block->data[i] = i2s_dev->rcb.dma_read_buffer[i2s_dev->rcb.dma_read_index * OS_AUDIO_RECORD_FIFO_SIZE + i];
                }
                
                if (i2s_dev->rcb.dma_read_index == 0)
                {
                    i2s_dev->rcb.dma_read_index = 1;
                }
                else
                {
                    i2s_dev->rcb.dma_read_index = 0;
                }
                
                os_list_add_tail(&i2s_dev->rcb.read_list, node);
                os_semaphore_post(i2s_dev->rcb.sem);
            }
            else
            {
                if (i2s_dev->rcb.dma_read_index == 0)
                {
                    i2s_dev->rcb.dma_read_index = 1;
                }
                else
                {
                    i2s_dev->rcb.dma_read_index = 0;
                }
            }

            break;
        }
    }
}

#ifdef BSP_USING_I2S2
void DMA1_Channel4_IRQHandler(void)
{
    if (DMA_GetIntStatus(DMA1_INT_TXC4, DMA1))
    {
        DMA_ClrIntPendingBit(DMA1_INT_TXC4, DMA1);
        cm32_i2s_dma_rc_callback(DMA1_CH4);
    }
    else if (DMA_GetIntStatus(DMA1_INT_HTX4, DMA1))
    {
        DMA_ClrIntPendingBit(DMA1_INT_HTX4, DMA1);
        cm32_i2s_dma_rc_callback(DMA1_CH4);
    }
}

void DMA1_Channel5_IRQHandler(void)
{
    if (DMA_GetIntStatus(DMA1_INT_TXC5, DMA1))
    {
        DMA_ClrIntPendingBit(DMA1_INT_TXC5, DMA1);
        cm32_i2s_dma_tc_callback(DMA1_CH5);
    }
}
#endif

#ifdef BSP_USING_I2S3
void DMA2_Channel1_IRQHandler(void)
{
    if (DMA_GetIntStatus(DMA2_INT_TXC1, DMA2))
    {
        DMA_ClrIntPendingBit(DMA2_INT_TXC1, DMA2);
        cm32_i2s_dma_rc_callback(DMA2_CH1);
    }
    else if (DMA_GetIntStatus(DMA2_INT_HTX1, DMA2))
    {
        DMA_ClrIntPendingBit(DMA2_INT_HTX1, DMA2);
        cm32_i2s_dma_rc_callback(DMA2_CH1);
    }
}

void DMA2_Channel2_IRQHandler(void)
{
    if (DMA_GetIntStatus(DMA2_INT_TXC2, DMA2))
    {
        DMA_ClrIntPendingBit(DMA2_INT_TXC2, DMA2);
        cm32_i2s_dma_tc_callback(DMA2_CH2);
    }
}
#endif

static void cm32_i2s_pin_init(struct cm32_i2s_info *i2s_info)
{
    GPIO_InitType GPIO_InitStructure;

    GPIO_InitStruct(&GPIO_InitStructure);

    uint32_t RCC_APB2_PERIPH_CK_PORT, RCC_APB2_PERIPH_SD_PORT, RCC_APB2_PERIPH_WS_PORT, RCC_APB2_PERIPH_MCK_PORT;

    /* ck */
    if (i2s_info->port_ck == GPIOA)
    {
        RCC_APB2_PERIPH_CK_PORT = RCC_APB2_PERIPH_GPIOA;
    }
    else if (i2s_info->port_ck == GPIOB)
    {
        RCC_APB2_PERIPH_CK_PORT = RCC_APB2_PERIPH_GPIOB;
    }
    else if (i2s_info->port_ck == GPIOC)
    {
        RCC_APB2_PERIPH_CK_PORT = RCC_APB2_PERIPH_GPIOC;
    }
    else if (i2s_info->port_ck == GPIOD)
    {
        RCC_APB2_PERIPH_CK_PORT = RCC_APB2_PERIPH_GPIOD;
    }
    else if (i2s_info->port_ck == GPIOE)
    {
        RCC_APB2_PERIPH_CK_PORT = RCC_APB2_PERIPH_GPIOE;
    }

    /* sd*/
    if (i2s_info->port_sd == GPIOA)
    {
        RCC_APB2_PERIPH_SD_PORT = RCC_APB2_PERIPH_GPIOA;
    }
    else if (i2s_info->port_sd == GPIOB)
    {
        RCC_APB2_PERIPH_SD_PORT = RCC_APB2_PERIPH_GPIOB;
    }
    else if (i2s_info->port_sd == GPIOC)
    {
        RCC_APB2_PERIPH_SD_PORT = RCC_APB2_PERIPH_GPIOC;
    }
    else if (i2s_info->port_sd == GPIOD)
    {
        RCC_APB2_PERIPH_SD_PORT = RCC_APB2_PERIPH_GPIOD;
    }
    else if (i2s_info->port_sd == GPIOE)
    {
        RCC_APB2_PERIPH_SD_PORT = RCC_APB2_PERIPH_GPIOE;
    }

    /* ws */
    if (i2s_info->port_ws == GPIOA)
    {
        RCC_APB2_PERIPH_WS_PORT = RCC_APB2_PERIPH_GPIOA;
    }
    else if (i2s_info->port_ws == GPIOB)
    {
        RCC_APB2_PERIPH_WS_PORT = RCC_APB2_PERIPH_GPIOB;
    }
    else if (i2s_info->port_ws == GPIOC)
    {
        RCC_APB2_PERIPH_WS_PORT = RCC_APB2_PERIPH_GPIOC;
    }
    else if (i2s_info->port_ws == GPIOD)
    {
        RCC_APB2_PERIPH_WS_PORT = RCC_APB2_PERIPH_GPIOD;
    }
    else if (i2s_info->port_ws == GPIOE)
    {
        RCC_APB2_PERIPH_WS_PORT = RCC_APB2_PERIPH_GPIOE;
    }

    /* mck */
    if (i2s_info->port_mck == GPIOA)
    {
        RCC_APB2_PERIPH_MCK_PORT = RCC_APB2_PERIPH_GPIOA;
    }
    else if (i2s_info->port_mck == GPIOB)
    {
        RCC_APB2_PERIPH_MCK_PORT = RCC_APB2_PERIPH_GPIOB;
    }
    else if (i2s_info->port_mck == GPIOC)
    {
        RCC_APB2_PERIPH_MCK_PORT = RCC_APB2_PERIPH_GPIOC;
    }
    else if (i2s_info->port_mck == GPIOD)
    {
        RCC_APB2_PERIPH_MCK_PORT = RCC_APB2_PERIPH_GPIOD;
    }
    else if (i2s_info->port_mck == GPIOE)
    {
        RCC_APB2_PERIPH_MCK_PORT = RCC_APB2_PERIPH_GPIOE;
    }

    RCC_EnableAPB2PeriphClk(RCC_APB2_PERIPH_CK_PORT | RCC_APB2_PERIPH_SD_PORT | RCC_APB2_PERIPH_WS_PORT | RCC_APB2_PERIPH_MCK_PORT, ENABLE);

    if (i2s_info->gpio_remap != 0)
    {
        RCC_EnableAPB2PeriphClk(RCC_APB2_PERIPH_AFIO, ENABLE);
        GPIO_ConfigPinRemap(i2s_info->gpio_remap, ENABLE);
    }

    GPIO_InitStructure.Pin        = i2s_info->pin_ck;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF_PP;
    GPIO_Init(i2s_info->port_ck, &GPIO_InitStructure);

    GPIO_InitStructure.Pin = i2s_info->pin_sd;
    GPIO_Init(i2s_info->port_sd, &GPIO_InitStructure);

    GPIO_InitStructure.Pin       = i2s_info->pin_ws;
    GPIO_Init(i2s_info->port_ws, &GPIO_InitStructure);

    GPIO_InitStructure.Pin       = i2s_info->pin_mck;
    GPIO_Init(i2s_info->port_mck, &GPIO_InitStructure);
}

static os_err_t cm32_i2s_init(struct os_i2s_device *dev)
{
    struct cm32_i2s *p_cm32_i2s;

    p_cm32_i2s = os_container_of(dev, struct cm32_i2s, i2s);

    cm32_i2s_pin_init(p_cm32_i2s->info);

    if (p_cm32_i2s->info->i2s_base == SPI2)
    {
        RCC_EnableAPB1PeriphClk(RCC_APB1_PERIPH_SPI2, ENABLE);
    }
    else if (p_cm32_i2s->info->i2s_base == SPI3)
    {
        RCC_EnableAPB1PeriphClk(RCC_APB1_PERIPH_SPI3, ENABLE);
    }

    SPI_I2S_DeInit(p_cm32_i2s->info->i2s_base);
    I2S_Init(p_cm32_i2s->info->i2s_base, &p_cm32_i2s->info->i2s_init_structure);

    return OS_SUCCESS;
}

static os_err_t cm32_i2s_dma_transmit(os_i2s_device_t *dev, uint8_t *buff, uint32_t size)
{
    os_base_t        level;
    struct cm32_i2s *i2s_dev;
    os_list_node_t   *node;
    struct cm32_i2s_tx_block *tx_block;

    i2s_dev = os_container_of(dev, struct cm32_i2s, i2s);

    os_semaphore_wait(i2s_dev->tcb.sem, OS_WAIT_FOREVER);

    level = os_irq_lock();
    node = os_list_first(&i2s_dev->tcb.write_list);
    os_list_del(node);
    tx_block = (struct cm32_i2s_tx_block*)node;
    tx_block->data     = buff;
    tx_block->data_len = size;
    os_list_add_tail(&i2s_dev->tcb.read_list, node);
    os_irq_unlock(level);

    if (i2s_dev->tcb.status == 0)
    {
        node = os_list_first(&i2s_dev->tcb.read_list);
        tx_block = (struct cm32_i2s_tx_block*)node;

        i2s_dev->tcb.DMA_InitStructure.PeriphAddr     = (uint32_t)(i2s_dev->info->i2s_base) + 0x0C;
        i2s_dev->tcb.DMA_InitStructure.MemAddr        = (uint32_t)tx_block->data;
        i2s_dev->tcb.DMA_InitStructure.Direction      = DMA_DIR_PERIPH_DST;
        i2s_dev->tcb.DMA_InitStructure.BufSize        = tx_block->data_len / 2;
        i2s_dev->tcb.DMA_InitStructure.PeriphInc      = DMA_PERIPH_INC_DISABLE;
        i2s_dev->tcb.DMA_InitStructure.DMA_MemoryInc  = DMA_MEM_INC_ENABLE;
        i2s_dev->tcb.DMA_InitStructure.PeriphDataSize = DMA_PERIPH_DATA_SIZE_HALFWORD;
        i2s_dev->tcb.DMA_InitStructure.MemDataSize    = DMA_MEMORY_DATA_SIZE_HALFWORD;
        i2s_dev->tcb.DMA_InitStructure.CircularMode   = DMA_MODE_NORMAL;
        i2s_dev->tcb.DMA_InitStructure.Priority       = DMA_PRIORITY_VERY_HIGH;
        i2s_dev->tcb.DMA_InitStructure.Mem2Mem        = DMA_M2M_DISABLE;
        DMA_Init(i2s_dev->info->dma_tx_channel, &i2s_dev->tcb.DMA_InitStructure);
        DMA_EnableChannel(i2s_dev->info->dma_tx_channel, ENABLE);
        i2s_dev->tcb.status = 1;
    }

    return OS_SUCCESS;
}

static os_err_t cm32_i2s_dma_receive(os_i2s_device_t *dev, uint8_t *buff, uint32_t size)
{
    os_base_t        level;
    struct cm32_i2s *i2s_dev;
    os_list_node_t  *node;
    struct cm32_i2s_rx_block *rx_block;

    i2s_dev = os_container_of(dev, struct cm32_i2s, i2s);

    if (i2s_dev->rcb.status == 0)
    {
        return OS_FAILURE;
    }

    os_semaphore_wait(i2s_dev->rcb.sem, OS_WAIT_FOREVER);

    if (!os_list_empty(&i2s_dev->rcb.read_list))
    {
        level = os_irq_lock();
        node = os_list_first(&i2s_dev->rcb.read_list);
        os_list_del(node);
        os_irq_unlock(level);

        rx_block = (struct cm32_i2s_rx_block*)node;

        for (int i = 0; i < OS_AUDIO_RECORD_FIFO_SIZE; i++)
        {
            buff[i] = rx_block->data[i];
        }

        level = os_irq_lock();
        os_list_add_tail(&i2s_dev->rcb.write_list, node);
        os_irq_unlock(level);
    }

    return OS_SUCCESS;
}


static os_err_t cm32_i2s_dma_tx_enable(os_i2s_device_t *dev, os_bool_t enable)
{
    struct cm32_i2s *p_cm32_i2s;
    os_list_node_t  *node;

    p_cm32_i2s = os_container_of(dev, struct cm32_i2s, i2s);

    if (enable == OS_TRUE)
    {
        p_cm32_i2s->tcb.sem = os_semaphore_create(OS_NULL, "i2s_tx_sem", OS_AUDIO_REPLAY_MP_BLOCK_COUNT, OS_AUDIO_REPLAY_MP_BLOCK_COUNT);
        os_list_init(&p_cm32_i2s->tcb.read_list);
        os_list_init(&p_cm32_i2s->tcb.write_list);
        
        struct cm32_i2s_tx_block *tx_block;
        for (int i = 0; i < OS_AUDIO_REPLAY_MP_BLOCK_COUNT; i++)
        {
            tx_block = (struct cm32_i2s_tx_block*)os_calloc(1, sizeof(struct cm32_i2s_tx_block));
            if (tx_block == OS_NULL)
            {
                return OS_NOMEM;
            }

            os_list_add_tail(&p_cm32_i2s->tcb.write_list, &tx_block->node);
        }

        RCC_EnableAHBPeriphClk(p_cm32_i2s->info->dma_tx_clk, ENABLE);

        /* Enable the DMA Channel Interrupt */
        ECLIC_SetLevelIRQ(p_cm32_i2s->info->dma_tx_irqn, 0);
        ECLIC_SetPriorityIRQ(p_cm32_i2s->info->dma_tx_irqn, 0);
        ECLIC_SetTrigIRQ(p_cm32_i2s->info->dma_tx_irqn, ECLIC_LEVEL_TRIGGER);
        ECLIC_EnableIRQ(p_cm32_i2s->info->dma_tx_irqn);

        DMA_DeInit(p_cm32_i2s->info->dma_tx_channel);
        DMA_ConfigInt(p_cm32_i2s->info->dma_tx_channel, DMA_INT_TXC, ENABLE);
        
        SPI_I2S_EnableDma(p_cm32_i2s->info->i2s_base, SPI_I2S_DMA_TX, ENABLE);
        I2S_Enable(p_cm32_i2s->info->i2s_base, ENABLE);
    }
    else
    {
        I2S_Enable(p_cm32_i2s->info->i2s_base, DISABLE);
        SPI_I2S_EnableDma(p_cm32_i2s->info->i2s_base, SPI_I2S_DMA_TX, DISABLE);
        DMA_EnableChannel(p_cm32_i2s->info->dma_tx_channel, DISABLE);

        ECLIC_DisableIRQ(p_cm32_i2s->info->dma_tx_irqn);
        __ECLIC_ClearPendingIRQ(p_cm32_i2s->info->dma_tx_irqn);

        os_semaphore_destroy(p_cm32_i2s->tcb.sem);

        while(!os_list_empty(&p_cm32_i2s->tcb.read_list))
        {
            node = os_list_first(&p_cm32_i2s->tcb.read_list);
            os_list_del(node);
            os_free(node);
        }

        while(!os_list_empty(&p_cm32_i2s->tcb.write_list))
        {
            node = os_list_first(&p_cm32_i2s->tcb.write_list);
            os_list_del(node);
            os_free(node);
        }

        p_cm32_i2s->tcb.status = 0;
    }

    return OS_SUCCESS;
}

static os_err_t cm32_i2s_dma_rx_enable(os_i2s_device_t *dev, os_bool_t enable)
{
    struct cm32_i2s *p_cm32_i2s;
    os_list_node_t  *node;
    struct cm32_i2s_rx_block *rx_block;
    DMA_InitType     DMA_InitStructure;

    p_cm32_i2s = os_container_of(dev, struct cm32_i2s, i2s);

    if (enable == OS_TRUE)
    {
        p_cm32_i2s->rcb.sem = os_semaphore_create(OS_NULL, "i2s_rx_sem", 0, OS_AUDIO_RECORD_FIFO_COUNT);
        os_list_init(&p_cm32_i2s->rcb.read_list);
        os_list_init(&p_cm32_i2s->rcb.write_list);

        struct cm32_i2s_rx_block *rx_block;
        for (int i = 0; i < OS_AUDIO_RECORD_FIFO_COUNT; i++)
        {
            rx_block = (struct cm32_i2s_rx_block*)os_calloc(1, sizeof(struct cm32_i2s_rx_block));
            if (rx_block == OS_NULL)
            {
                return OS_NOMEM;
            }

            rx_block->data = os_calloc(1, OS_AUDIO_RECORD_FIFO_SIZE);
            if (rx_block->data == OS_NULL)
            {
                return OS_NOMEM;
            }

            rx_block->data_len = OS_AUDIO_RECORD_FIFO_SIZE;
            os_list_add_tail(&p_cm32_i2s->rcb.write_list, &rx_block->node);
        }

        p_cm32_i2s->rcb.dma_read_index = 0;
        p_cm32_i2s->rcb.dma_read_buffer = os_calloc(1, OS_AUDIO_RECORD_FIFO_SIZE * OS_AUDIO_RECORD_FIFO_COUNT);
        if (p_cm32_i2s->rcb.dma_read_buffer == OS_NULL)
        {
            return OS_NOMEM;
        }

        RCC_EnableAHBPeriphClk(p_cm32_i2s->info->dma_rx_clk, ENABLE);

        DMA_DeInit(p_cm32_i2s->info->dma_rx_channel);
        DMA_InitStructure.PeriphAddr     = (uint32_t)(p_cm32_i2s->info->i2s_base) + 0x0C;
        DMA_InitStructure.MemAddr        = (uint32_t)p_cm32_i2s->rcb.dma_read_buffer;
        DMA_InitStructure.Direction      = DMA_DIR_PERIPH_SRC;
        DMA_InitStructure.BufSize        = OS_AUDIO_RECORD_FIFO_SIZE * OS_AUDIO_RECORD_FIFO_COUNT / 2;
        DMA_InitStructure.PeriphInc      = DMA_PERIPH_INC_DISABLE;
        DMA_InitStructure.DMA_MemoryInc  = DMA_MEM_INC_ENABLE;
        DMA_InitStructure.PeriphDataSize = DMA_PERIPH_DATA_SIZE_HALFWORD;
        DMA_InitStructure.MemDataSize    = DMA_MEMORY_DATA_SIZE_HALFWORD;
        DMA_InitStructure.CircularMode   = DMA_MODE_CIRCULAR;
        DMA_InitStructure.Priority       = DMA_PRIORITY_VERY_HIGH;
        DMA_InitStructure.Mem2Mem        = DMA_M2M_DISABLE;
        DMA_Init(p_cm32_i2s->info->dma_rx_channel, &DMA_InitStructure);

        /* Enable the DMA Channel Interrupt */
        ECLIC_SetLevelIRQ(p_cm32_i2s->info->dma_rx_irqn, 0);
        ECLIC_SetPriorityIRQ(p_cm32_i2s->info->dma_rx_irqn, 0);
        ECLIC_SetTrigIRQ(p_cm32_i2s->info->dma_rx_irqn, ECLIC_LEVEL_TRIGGER);
        ECLIC_EnableIRQ(p_cm32_i2s->info->dma_rx_irqn);

        DMA_ConfigInt(p_cm32_i2s->info->dma_rx_channel, DMA_INT_TXC, ENABLE);
        DMA_ConfigInt(p_cm32_i2s->info->dma_rx_channel, DMA_INT_HTX, ENABLE);

        SPI_I2S_EnableDma(p_cm32_i2s->info->i2s_base, SPI_I2S_DMA_RX, ENABLE);
        I2S_Enable(p_cm32_i2s->info->i2s_base, ENABLE);
        DMA_EnableChannel(p_cm32_i2s->info->dma_rx_channel, ENABLE);
        
        p_cm32_i2s->rcb.status = 1;
    }
    else
    {
        I2S_Enable(p_cm32_i2s->info->i2s_base, DISABLE);
        SPI_I2S_EnableDma(p_cm32_i2s->info->i2s_base, SPI_I2S_DMA_RX, DISABLE);
        DMA_EnableChannel(p_cm32_i2s->info->dma_rx_channel, DISABLE);

        ECLIC_DisableIRQ(p_cm32_i2s->info->dma_rx_irqn);
        __ECLIC_ClearPendingIRQ(p_cm32_i2s->info->dma_rx_irqn);

        os_semaphore_destroy(p_cm32_i2s->rcb.sem);

        while(!os_list_empty(&p_cm32_i2s->rcb.read_list))
        {
            node = os_list_first(&p_cm32_i2s->rcb.read_list);
            os_list_del(node);
            rx_block = (struct cm32_i2s_rx_block *)node;
            os_free(rx_block->data);
            os_free(node);
        }

        while(!os_list_empty(&p_cm32_i2s->rcb.write_list))
        {
            node = os_list_first(&p_cm32_i2s->rcb.write_list);
            os_list_del(node);
            rx_block = (struct cm32_i2s_rx_block *)node;
            os_free(rx_block->data);
            os_free(node);
        }

        if (p_cm32_i2s->rcb.dma_read_buffer != OS_NULL)
        {
            os_free(p_cm32_i2s->rcb.dma_read_buffer);
        }

        p_cm32_i2s->rcb.status = 0;
    }

    return OS_SUCCESS;
}

static os_err_t cm32_i2s_set_frq(os_i2s_device_t *i2s, uint32_t frequency)
{
    struct cm32_i2s *p_cm32_i2s;
    p_cm32_i2s = (struct cm32_i2s *)i2s;

    p_cm32_i2s->info->i2s_init_structure.AudioFrequency = frequency;

    SPI_I2S_DeInit(p_cm32_i2s->info->i2s_base);
    I2S_Init(p_cm32_i2s->info->i2s_base, &p_cm32_i2s->info->i2s_init_structure);

    return OS_SUCCESS;
}

static os_err_t cm32_i2s_set_channel(os_i2s_device_t *i2s, uint8_t channels)
{
    return OS_SUCCESS;
}

static struct os_i2s_ops cm32_i2s_ops = {
    .init        = cm32_i2s_init,
    .transimit   = cm32_i2s_dma_transmit,
    .receive     = cm32_i2s_dma_receive,
    .enable_tx   = cm32_i2s_dma_tx_enable,
    .enable_rx   = cm32_i2s_dma_rx_enable,
    .set_frq     = cm32_i2s_set_frq,
    .set_channel = cm32_i2s_set_channel,
};

static int cm32_i2s_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    os_base_t             level;
    struct cm32_i2s      *i2s_dev;
    struct os_i2s_device *i2s;

    i2s_dev = os_calloc(1, sizeof(struct cm32_i2s));
    OS_ASSERT(i2s_dev);

    i2s_dev->info = (struct cm32_i2s_info *)dev->info;

    i2s      = &i2s_dev->i2s;
    i2s->ops = &cm32_i2s_ops;

    level = os_irq_lock();
    os_list_add_tail(&cm32_i2s_list, &i2s_dev->list);
    os_irq_unlock(level);

    os_i2s_register(i2s, dev->name, NULL);

    return OS_SUCCESS;
}

OS_DRIVER_INFO cm32_i2s_driver = {
    .name  = "I2S_HandleTypeDef",
    .probe = cm32_i2s_probe,
};

OS_DRIVER_DEFINE(cm32_i2s_driver, OS_INIT_LEVEL_PRE_DEVICE, OS_INIT_SUBLEVEL_LOW);
