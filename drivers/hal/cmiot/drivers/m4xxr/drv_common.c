/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_common.c
 *
 * @brief       This file provides board init functions.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <oneos_config.h>
#include <os_types.h>
#include <os_stddef.h>
#include <os_errno.h>
#include <os_clock.h>
#include <os_memory.h>
#include "drv_common.h"
#include <nuclei_sdk_soc.h>
#include "board.h"
#include "drv_gpio.h"

#ifdef OS_USING_CLOCKSOURCE
#include <timer/clocksource.h>
#endif

#define SYSTICK_TICK_CONST (SystemCoreClock / OS_TICK_PER_SECOND)

/* Interrupt level for kernel systimer interrupt and software timer interrupt */
#define OS_KERNEL_INTERRUPT_LEVEL 0

#define SysTick_Handler eclic_mtip_handler

void os_tick_handler(void)
{
    os_tick_increase();
#ifdef OS_USING_CLOCKSOURCE
    os_clocksource_update();
#endif
}

#ifdef OS_USING_SYSTICK_FOR_CLOCKSOURCE
static uint64_t riscv_systick_read(void *clock)
{
    return SysTimer->MTIMER;
}

static os_clocksource_t riscv_systick_clocksource;

void riscv_systick_clocksource_init(void)
{
    uint32_t control_value = SysTimer_GetControlValue();
    SysTimer_Stop();
    SysTimer_SetControlValue(control_value | SysTimer_MTIMECTL_CLKSRC_Msk);
    SysTick_Config(0xffffffffffffffffull);
    ECLIC_DisableIRQ(SysTimer_IRQn);
    SysTimer_Start();

    riscv_systick_clocksource.rating = 640;
    riscv_systick_clocksource.freq   = SystemCoreClock;
    riscv_systick_clocksource.mask   = 0xffffffffffffffffull;
    riscv_systick_clocksource.read   = riscv_systick_read;

    os_clocksource_register("systick", &riscv_systick_clocksource);
}
#else
void os_hw_systick_init(void)
{
    uint64_t ticks = SYSTICK_TICK_CONST;

    /* Make SWI and SysTick the lowest priority interrupts. */
    /* Stop and clear the SysTimer. SysTimer as Non-Vector Interrupt */
    SysTimer_Stop();
    SysTimer_SetControlValue(SysTimer_GetControlValue() 
                           | SysTimer_MTIMECTL_CLKSRC_Msk 
                           | SysTimer_MTIMECTL_CMPCLREN_Msk);
 
     /* If CMPCLREN bit is set in ECLIC mode, it needs to be set to edge trigger mode. */
    __ECLIC_SetTrigIRQ(SysTimer_IRQn, ECLIC_POSTIVE_EDGE_TRIGGER);
    SysTick_Config(ticks);
    SysTimer_Start();

    /* Set SWI interrupt level to lowest level/priority, SysTimerSW as Vector Interrupt */
    ECLIC_SetShvIRQ(SysTimerSW_IRQn, ECLIC_VECTOR_INTERRUPT);
    ECLIC_SetLevelIRQ(SysTimerSW_IRQn, OS_KERNEL_INTERRUPT_LEVEL);
    ECLIC_EnableIRQ(SysTimerSW_IRQn);
}

/**
 * This is the timer interrupt service routine.
 *
 */
void SysTick_Handler(void)
{   
    os_tick_handler();
}
#endif

/* clang-format off */
void _Error_Handler(char *s, int num)
{
    volatile int loop = 1;
    while (loop);
}
/* clang-format on */

/**
 ***********************************************************************************************************************
 * @brief           This function will initial cm32 board.
 *
 * @param[in]       none
 *
 * @return          none
 ***********************************************************************************************************************
 */
static os_err_t os_hw_board_init(void)
{
    os_irq_enable();
    
    interrupt_stack_size = M4XXR_STACK_SIZE;

    /* Heap initialization */
#if defined(OS_USING_HEAP)
    if ((os_size_t)HEAP_END > (os_size_t)HEAP_BEGIN)
    {
        os_default_heap_init();
        os_default_heap_add((void *)HEAP_BEGIN, (os_size_t)HEAP_END - (os_size_t)HEAP_BEGIN, OS_MEM_ALG_DEFAULT);
    }
#endif

    return OS_SUCCESS;
}

OS_INIT_CALL(os_hw_board_init, OS_INIT_LEVEL_PRE_KERNEL_1, OS_INIT_SUBLEVEL_MIDDLE);

static os_err_t board_post_init(void)
{
#ifdef OS_USING_PIN
    os_hw_pin_init();
#endif

#ifdef OS_USING_SYSTICK_FOR_CLOCKSOURCE
    riscv_systick_clocksource_init();
#else
    os_hw_systick_init();
#endif

    return OS_SUCCESS;
}

OS_INIT_CALL(board_post_init, OS_INIT_LEVEL_POST_KERNEL, OS_INIT_SUBLEVEL_LOW);

void os_hw_cpu_reset(void)
{
    SysTimer_SoftwareReset();
}

