/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_crypto_rng.c
 *
 * @brief       This file implements stm32 crypto rng driver.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <os_task.h>
#include <device.h>
#include <stdlib.h>
#include <string.h>
#include "drv_crypto.h"
#include <board.h>
#include <os_memory.h>

void rngc_configure(void)
{
    RNGC_InitType Rngc_Init;

    RCC_EnableAHBPeriphClk(RCC_AHB_PERIPH_RNGC, ENABLE);
    RCC_EnableAPB1PeriphClk(RCC_APB1_PERIPH_RNGC, ENABLE);

    RNGC_DeInit();

    Rngc_Init.Prescaler = RCC_RNGCCLK_SYSCLK_DIV1;
    Rngc_Init.Ring0En   = true;
    Rngc_Init.Ring1En   = true;
    RNGC_Init(&Rngc_Init);

    RNGC_SetRngMode(RNG_MODE_RANDOM_SOURCE);
    RNGC_SetRngMode(RNG_MODE_TRNG);
}

static uint32_t cm32_rng_rand(struct hwcrypto_rng *ctx)
{
    uint32_t gen_random = 0;

    struct cm32_hwcrypto_device *cm32_hw_dev = (struct cm32_hwcrypto_device *)ctx->parent.device->user_data;

    os_mutex_lock(cm32_hw_dev->mutex, OS_WAIT_FOREVER);

    gen_random = RNGC_GetRand();

    os_mutex_unlock(cm32_hw_dev->mutex);

    return gen_random;
}

static const struct hwcrypto_rng_ops rng_ops = {
    .update = cm32_rng_rand,
};

static os_err_t cm32_rng_crypto_create(struct os_hwcrypto_ctx *ctx)
{
    struct cm32_hwcrypto_device *cm32_hw_dev = (struct cm32_hwcrypto_device *)ctx->device->user_data;

    rngc_configure();

    ((struct hwcrypto_rng *)ctx)->ops = &rng_ops;

    return OS_SUCCESS;
}

static void cm32_rng_crypto_destroy(struct os_hwcrypto_ctx *ctx)
{
}

static os_err_t cm32_rng_crypto_clone(struct os_hwcrypto_ctx *des, const struct os_hwcrypto_ctx *src)
{
    return OS_SUCCESS;
}

static void cm32_rng_crypto_reset(struct os_hwcrypto_ctx *ctx)
{
}

static const struct os_hwcrypto_ops _ops = {
    .create  = cm32_rng_crypto_create,
    .destroy = cm32_rng_crypto_destroy,
    .copy    = cm32_rng_crypto_clone,
    .reset   = cm32_rng_crypto_reset,
};

static int cm32_rng_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    struct cm32_hwcrypto_device *cm_hwcrypto = os_calloc(1, sizeof(struct cm32_hwcrypto_device));

    OS_ASSERT(cm_hwcrypto);

    struct os_hwcrypto_device *hwcrypto = &cm_hwcrypto->hwcrypto;

    hwcrypto->ops = &_ops;

    hwcrypto->user_data = hwcrypto;

    if (os_hwcrypto_rng_register(hwcrypto, dev->name) != OS_SUCCESS)
    {
        os_kprintf("cm32 rng probe failed %s.\r\n", dev->name);
        os_free(cm_hwcrypto);
        return -1;
    }

    cm_hwcrypto->mutex = os_mutex_create(OS_NULL, OS_HWCRYPTO_DEFAULT_NAME, OS_FALSE);
    return 0;
}

OS_DRIVER_INFO cm32_rng_driver = {
    .name  = "RNG_HandleTypeDef",
    .probe = cm32_rng_probe,
};

OS_DRIVER_DEFINE(cm32_rng_driver, OS_INIT_LEVEL_DEVICE, OS_INIT_SUBLEVEL_MIDDLE);

