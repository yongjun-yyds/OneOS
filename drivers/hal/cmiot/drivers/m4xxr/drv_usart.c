/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_usart.c
 *
 * @brief       This file implements usart driver for cm32
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <drv_cfg.h>
#include <device.h>
#include <string.h>
#include "drv_usart.h"

static os_list_node_t cm32_uart_list = OS_LIST_INIT(cm32_uart_list);

/* uart driver */
typedef struct cm_uart
{
    struct os_serial_device serial_dev;
    struct cm32_usart_info *info;
    soft_dma_t              sdma;
    uint32_t             sdma_hard_size;

    uint8_t       *rx_buff;
    uint32_t       rx_index;
    uint32_t       rx_size;
    const uint8_t *tx_buff;
    uint32_t       tx_index;
    uint32_t       tx_size;
    os_list_node_t list;
} cm_uart_t;

static void uart_isr(struct os_serial_device *serial)
{
    OS_ASSERT(serial != OS_NULL);
    cm_uart_t *uart = os_container_of(serial, cm_uart_t, serial_dev);

    OS_ASSERT(uart != OS_NULL);

    if (USART_GetIntStatus(uart->info->idx, USART_INT_RXDNE) != RESET)
    {
        uart->rx_buff[uart->rx_index++] = USART_ReceiveData(uart->info->idx);

        if (uart->rx_index == (uart->rx_size / 2))
        {
            soft_dma_half_irq(&uart->sdma);
        }

        if (uart->rx_index == uart->rx_size)
        {
            uart->rx_index = 0;
            soft_dma_full_irq(&uart->sdma);
        }
    }

    if (USART_GetIntStatus(uart->info->idx, USART_INT_TXDE) != RESET)
    {
        if (uart->tx_size)
        {
            if (uart->tx_index < uart->tx_size)
            {
                USART_SendData(uart->info->idx, uart->tx_buff[uart->tx_index++]);
            }
            else
            {
                uart->tx_size  = 0;
                uart->tx_index = 0;

                USART_ConfigInt(uart->info->idx, USART_INT_TXDE, DISABLE);
                os_hw_serial_isr_txdone(serial);
            }
        }
    }

    if (USART_GetIntStatus(uart->info->idx, USART_INT_IDLEF) != RESET)
    {
        USART_ReceiveData(uart->info->idx);

        soft_dma_timeout_irq(&uart->sdma);
    }
}

void uart_irq(int device_num)
{
    struct cm_uart *uart;

    os_list_for_each_entry(uart, &cm32_uart_list, struct cm_uart, list)
    {
        if (uart->info->uart_device == device_num)
        {
            uart_isr(&uart->serial_dev);
            break;
        }
    }
}

void USART1_IRQHandler(void)
{
    uart_irq(1);
}

void USART2_IRQHandler(void)
{
    uart_irq(2);
}

void USART3_IRQHandler(void)
{
    uart_irq(3);
}

void UART4_IRQHandler(void)
{
    uart_irq(4);
}

void UART5_IRQHandler(void)
{
    uart_irq(5);
}

void UART6_IRQHandler(void)
{
    uart_irq(6);
}

void UART7_IRQHandler(void)
{
    uart_irq(7);
}

static void uart_dma_isr(struct os_serial_device *serial)
{
    OS_ASSERT(serial != OS_NULL);
    cm_uart_t *uart = os_container_of(serial, cm_uart_t, serial_dev);

    OS_ASSERT(uart != OS_NULL);

    soft_dma_half_irq(&uart->sdma);
}

void uart_dma_irq(int device_num)
{
    struct cm_uart *uart;

    os_list_for_each_entry(uart, &cm32_uart_list, struct cm_uart, list)
    {
        if ((uart->info->dma_support == 1) && (uart->info->uart_device == device_num))
        {
            uart_dma_isr(&uart->serial_dev);
            break;
        }
    }
}

#ifdef UART3_USING_DMA
void DMA1_Channel3_IRQHandler(void)
{
    if (DMA_GetIntStatus(DMA1_INT_HTX3, DMA1))
    {
        DMA_ClrIntPendingBit(DMA1_INT_HTX3, DMA1);
    }

    if (DMA_GetIntStatus(DMA1_INT_TXC3, DMA1))
    {
        DMA_ClrIntPendingBit(DMA1_INT_TXC3, DMA1);
    }

    uart_dma_irq(3);
}
#endif

#ifdef UART1_USING_DMA
void DMA1_Channel5_IRQHandler(void)
{
    if (DMA_GetIntStatus(DMA1_INT_HTX5, DMA1))
    {
        DMA_ClrIntPendingBit(DMA1_INT_HTX5, DMA1);
    }

    if (DMA_GetIntStatus(DMA1_INT_TXC5, DMA1))
    {
        DMA_ClrIntPendingBit(DMA1_INT_TXC5, DMA1);
    }

    uart_dma_irq(1);
}
#endif

#ifdef UART2_USING_DMA
void DMA1_Channel6_IRQHandler(void)
{
    if (DMA_GetIntStatus(DMA1_INT_HTX6, DMA1))
    {
        DMA_ClrIntPendingBit(DMA1_INT_HTX6, DMA1);
    }

    if (DMA_GetIntStatus(DMA1_INT_TXC6, DMA1))
    {
        DMA_ClrIntPendingBit(DMA1_INT_TXC6, DMA1);
    }

    uart_dma_irq(2);
}
#endif

#ifdef UART5_USING_DMA
void DMA1_Channel8_IRQHandler(void)
{
    if (DMA_GetIntStatus(DMA1_INT_HTX8, DMA1))
    {
        DMA_ClrIntPendingBit(DMA1_INT_HTX8, DMA1);
    }

    if (DMA_GetIntStatus(DMA1_INT_TXC8, DMA1))
    {
        DMA_ClrIntPendingBit(DMA1_INT_TXC8, DMA1);
    }

    uart_dma_irq(5);
}
#endif

#ifdef UART6_USING_DMA
void DMA2_Channel1_IRQHandler(void)
{
    if (DMA_GetIntStatus(DMA2_INT_HTX1, DMA2))
    {
        DMA_ClrIntPendingBit(DMA2_INT_HTX1, DMA2);
    }

    if (DMA_GetIntStatus(DMA2_INT_TXC1, DMA2))
    {
        DMA_ClrIntPendingBit(DMA2_INT_TXC1, DMA2);
    }

    uart_dma_irq(6);
}
#endif

#ifdef UART4_USING_DMA
void DMA2_Channel3_IRQHandler(void)
{
    if (DMA_GetIntStatus(DMA2_INT_HTX3, DMA2))
    {
        DMA_ClrIntPendingBit(DMA2_INT_HTX3, DMA2);
    }

    if (DMA_GetIntStatus(DMA2_INT_TXC3, DMA2))
    {
        DMA_ClrIntPendingBit(DMA2_INT_TXC3, DMA2);
    }

    uart_dma_irq(4);
}
#endif

#ifdef UART7_USING_DMA
void DMA2_Channel6_IRQHandler(void)
{
    if (DMA_GetIntStatus(DMA2_INT_HTX6, DMA2))
    {
        DMA_ClrIntPendingBit(DMA2_INT_HTX6, DMA2);
    }

    if (DMA_GetIntStatus(DMA2_INT_TXC6, DMA2))
    {
        DMA_ClrIntPendingBit(DMA2_INT_TXC6, DMA2);
    }

    uart_dma_irq(7);
}
#endif

/* interrupt rx mode */
static uint32_t cm_uart_sdma_int_get_index(soft_dma_t *dma)
{
    cm_uart_t *uart = os_container_of(dma, cm_uart_t, sdma);

    return uart->rx_index;
}

static os_err_t cm_uart_sdma_int_start(soft_dma_t *dma, void *buff, uint32_t size)
{
    cm_uart_t *uart = os_container_of(dma, cm_uart_t, sdma);

    uart->rx_buff  = buff;
    uart->rx_index = 0;
    uart->rx_size  = size;

    USART_ConfigInt(uart->info->idx, USART_INT_RXDNE, ENABLE);

    return OS_SUCCESS;
}

static uint32_t cm_uart_sdma_int_stop(soft_dma_t *dma)
{
    cm_uart_t *uart = os_container_of(dma, cm_uart_t, sdma);

    USART_ConfigInt(uart->info->idx, USART_INT_RXDNE, DISABLE);

    return uart->rx_index;
}

/* dma rx mode */
static uint32_t cm_uart_sdma_dma_get_index(soft_dma_t *dma)
{
    cm_uart_t *uart = os_container_of(dma, cm_uart_t, sdma);

    if (uart->info->dma_support == 1)
    {
        return uart->sdma_hard_size - DMA_GetCurrDataCounter(uart->info->dma_channel);
    }

    return 0;
}

static os_err_t cm_uart_sdma_dma_init(soft_dma_t *dma)
{
    return OS_SUCCESS;
}

static os_err_t cm_uart_sdma_dma_start(soft_dma_t *dma, void *buff, uint32_t size)
{
    cm_uart_t *uart = os_container_of(dma, cm_uart_t, sdma);

    uart->sdma_hard_size = size;

    DMA_InitType DMA_InitStructure;

    /* DMA clock enable */
    RCC_EnableAHBPeriphClk(uart->info->dma_clk, ENABLE);

    /* Enable the DMA Channel Interrupt */
    ECLIC_SetLevelIRQ(uart->info->dma_irqn, 0);
    ECLIC_SetPriorityIRQ(uart->info->dma_irqn, 0);
    ECLIC_SetTrigIRQ(uart->info->dma_irqn, ECLIC_LEVEL_TRIGGER);
    ECLIC_EnableIRQ(uart->info->dma_irqn);

    DMA_DeInit(uart->info->dma_channel);
    DMA_InitStructure.PeriphAddr     = uart->info->periph_addr + 0x04;
    DMA_InitStructure.MemAddr        = (uint32_t)buff;
    DMA_InitStructure.Direction      = DMA_DIR_PERIPH_SRC;
    DMA_InitStructure.BufSize        = size;
    DMA_InitStructure.PeriphInc      = DMA_PERIPH_INC_DISABLE;
    DMA_InitStructure.DMA_MemoryInc  = DMA_MEM_INC_ENABLE;
    DMA_InitStructure.PeriphDataSize = DMA_PERIPH_DATA_SIZE_BYTE;
    DMA_InitStructure.MemDataSize    = DMA_MEMORY_DATA_SIZE_BYTE;
    DMA_InitStructure.CircularMode   = DMA_MODE_CIRCULAR;
    DMA_InitStructure.Priority       = DMA_PRIORITY_VERY_HIGH;
    DMA_InitStructure.Mem2Mem        = DMA_M2M_DISABLE;
    DMA_Init(uart->info->dma_channel, &DMA_InitStructure);

    DMA_ConfigInt(uart->info->dma_channel, DMA_INT_HTX, ENABLE);
    DMA_ConfigInt(uart->info->dma_channel, DMA_INT_TXC, ENABLE);

    USART_EnableDMA(uart->info->idx, USART_DMAREQ_RX, ENABLE);

    DMA_EnableChannel(uart->info->dma_channel, ENABLE);

    return OS_SUCCESS;
}

static uint32_t cm_uart_sdma_dma_stop(soft_dma_t *dma)
{
    cm_uart_t *uart = os_container_of(dma, cm_uart_t, sdma);

    if (uart->info->dma_support == 1)
    {
        DMA_ConfigInt(uart->info->dma_channel, DMA_INT_HTX, DISABLE);
        DMA_ConfigInt(uart->info->dma_channel, DMA_INT_TXC, DISABLE);
        USART_EnableDMA(uart->info->idx, USART_DMAREQ_RX, DISABLE);
        DMA_EnableChannel(uart->info->dma_channel, DISABLE);
    }

    return cm_uart_sdma_dma_get_index(dma);
}

/* sdma callback */
static void cm_uart_sdma_callback(soft_dma_t *dma)
{
    cm_uart_t *uart = os_container_of(dma, cm_uart_t, sdma);

    os_hw_serial_isr_rxdone((struct os_serial_device *)uart);
}

static void cm_uart_sdma_init(struct cm_uart *uart, dma_ring_t *ring)
{
    soft_dma_t *dma = &uart->sdma;

    soft_dma_stop(dma);

    memset(&dma->hard_info, 0, sizeof(dma->hard_info));

    dma->hard_info.max_size     = 64 * 1024;
    dma->hard_info.flag         = HARD_DMA_FLAG_FULL_IRQ | HARD_DMA_FLAG_TIMEOUT_IRQ;
    dma->hard_info.data_timeout = uart_calc_byte_timeout_us(uart->serial_dev.config.baud_rate);

    if (uart->info->dma_support == 0)
    {
        dma->hard_info.mode = HARD_DMA_MODE_NORMAL;

        dma->ops.get_index = cm_uart_sdma_int_get_index;
        dma->ops.dma_init  = OS_NULL;
        dma->ops.dma_start = cm_uart_sdma_int_start;
        dma->ops.dma_stop  = cm_uart_sdma_int_stop;
    }
    else
    {
        dma->hard_info.mode = HARD_DMA_MODE_CIRCULAR;

        dma->hard_info.flag |= HARD_DMA_FLAG_HALF_IRQ;

        dma->ops.get_index = cm_uart_sdma_dma_get_index;
        dma->ops.dma_init  = cm_uart_sdma_dma_init;
        dma->ops.dma_start = cm_uart_sdma_dma_start;
        dma->ops.dma_stop  = cm_uart_sdma_dma_stop;
    }

    dma->cbs.dma_half_callback    = cm_uart_sdma_callback;
    dma->cbs.dma_full_callback    = cm_uart_sdma_callback;
    dma->cbs.dma_timeout_callback = cm_uart_sdma_callback;

    soft_dma_init(dma);
    soft_dma_start(dma, ring);
    soft_dma_irq_enable(&uart->sdma, OS_TRUE);
}

void GPIO_Configuration_uart(const struct cm32_usart_info *uart_info)
{
    OS_ASSERT(uart_info != OS_NULL);

    GPIO_InitType GPIO_InitStructure;

    if (uart_info->gpio_remap != 0)
    {
        GPIO_ConfigPinRemap(uart_info->gpio_remap, ENABLE);
    }

    /* Configure UARTx Tx as alternate function push-pull */
    GPIO_InitStructure.Pin        = uart_info->tx_pin;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(uart_info->tx_port, &GPIO_InitStructure);

    /* Configure UARTx Rx as alternate function push-pull and pull-up */
    GPIO_InitStructure.Pin       = uart_info->rx_pin;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_Init(uart_info->rx_port, &GPIO_InitStructure);
}

void RCC_Configuration_uart(const struct cm32_usart_info *uart_info)
{
    OS_ASSERT(uart_info != OS_NULL);

    /* Enable GPIO clock */
    RCC_EnableAPB2PeriphClk(uart_info->uart_tx_clk | uart_info->uart_rx_clk | RCC_APB2_PERIPH_AFIO, ENABLE);

    /* Enable UARTx Clock */
    if ((uart_info->uart_device == 1) || (uart_info->uart_device == 6) || (uart_info->uart_device == 7))
    {
        RCC_EnableAPB2PeriphClk(uart_info->uart_clk, ENABLE);
    }
    else
    {
        RCC_EnableAPB1PeriphClk(uart_info->uart_clk, ENABLE);
    }
}

void ECLIC_Configuration_uart(const struct cm32_usart_info *uart_info)
{
    ECLIC_SetCfgNlbits(0);
    ECLIC_SetPriorityIRQ(uart_info->irqn, 1);
    ECLIC_SetTrigIRQ(uart_info->irqn, ECLIC_LEVEL_TRIGGER);
    ECLIC_EnableIRQ(uart_info->irqn);
}

static os_err_t _cm_uart_init(const struct cm32_usart_info *uart_info, struct serial_configure *cfg)
{
    USART_InitType USART_InitStructure;

    /* System Clocks Configuration */
    RCC_Configuration_uart(uart_info);

    /* Configure the GPIO ports */
    GPIO_Configuration_uart(uart_info);

    /* UARTx configuration */
    USART_InitStructure.BaudRate   = cfg->baud_rate;
    USART_InitStructure.WordLength = USART_WL_8B;
    switch (cfg->stop_bits)
    {
    case STOP_BITS_1:
        USART_InitStructure.StopBits = USART_STPB_1;
        break;
    case STOP_BITS_2:
        USART_InitStructure.StopBits = USART_STPB_2;
        break;
    default:
        return OS_INVAL;
    }
    switch (cfg->parity)
    {
    case PARITY_NONE:
        USART_InitStructure.Parity = USART_PE_NO;
        break;
    case PARITY_ODD:
        USART_InitStructure.Parity = USART_PE_ODD;
        break;
    case PARITY_EVEN:
        USART_InitStructure.Parity = USART_PE_EVEN;
        break;
    default:
        return OS_INVAL;
    }
    USART_InitStructure.HardwareFlowControl = USART_HFCTRL_NONE;
    USART_InitStructure.Mode                = USART_MODE_RX | USART_MODE_TX;

    /* Configure UARTx */
    USART_Init(uart_info->idx, &USART_InitStructure);
    /* Enable the UARTx */
    USART_Enable(uart_info->idx, ENABLE);

    if (uart_info->dma_support == 0)
    {
        USART_ConfigInt(uart_info->idx, USART_INT_RXDNE, ENABLE);
    }
    else
    {
        USART_EnableDMA(uart_info->idx, USART_DMAREQ_RX, ENABLE);
    }

    USART_ConfigInt(uart_info->idx, USART_INT_IDLEF, ENABLE);

    ECLIC_Configuration_uart(uart_info);

    return OS_SUCCESS;
}

static os_err_t cm_uart_init(struct os_serial_device *serial, struct serial_configure *cfg)
{
    OS_ASSERT(serial != OS_NULL);
    OS_ASSERT(cfg != OS_NULL);

    cm_uart_t *uart = os_container_of(serial, cm_uart_t, serial_dev);

    OS_ASSERT(uart != OS_NULL);

    _cm_uart_init(uart->info, cfg);

    cm_uart_sdma_init(uart, &uart->serial_dev.rx_fifo->ring);

    return OS_SUCCESS;
}

static os_err_t cm_uart_deinit(struct os_serial_device *serial)
{
    OS_ASSERT(serial != OS_NULL);

    cm_uart_t *uart = os_container_of(serial, cm_uart_t, serial_dev);

    if (uart->info->dma_support == 0)
    {
        USART_ConfigInt(uart->info->idx, USART_INT_RXDNE, DISABLE);
    }
    else
    {
        DMA_ConfigInt(uart->info->dma_channel, DMA_INT_HTX, DISABLE);
        DMA_ConfigInt(uart->info->dma_channel, DMA_INT_TXC, DISABLE);
        USART_EnableDMA(uart->info->idx, USART_DMAREQ_RX, DISABLE);
        DMA_EnableChannel(uart->info->dma_channel, DISABLE);
    }

    USART_ConfigInt(uart->info->idx, USART_INT_IDLEF, DISABLE);

    soft_dma_stop(&uart->sdma);
    soft_dma_deinit(&uart->sdma);

    USART_Enable(uart->info->idx, DISABLE);
    USART_DeInit(uart->info->idx);

    uart->tx_size  = 0;
    uart->tx_index = 0;

    return OS_SUCCESS;
}

static int cm_uart_start_send(struct os_serial_device *serial, const uint8_t *buff, os_size_t size)
{
    struct cm_uart *uart;

    uart = (struct cm_uart *)serial->parent.user_data;

    uart->tx_size  = size;
    uart->tx_index = 0;
    uart->tx_buff  = buff;

    USART_ConfigInt(uart->info->idx, USART_INT_TXDE, ENABLE);
    USART_SendData(uart->info->idx, uart->tx_buff[uart->tx_index++]);

    return size;
}

static int cm_uart_poll_send(struct os_serial_device *serial, const uint8_t *buff, os_size_t size)
{
    int       i;
    os_ubase_t level;

    OS_ASSERT(serial != OS_NULL);
    cm_uart_t *uart = os_container_of(serial, cm_uart_t, serial_dev);

    OS_ASSERT(uart != OS_NULL);

    for (i = 0; i < size; i++)
    {
        os_spin_lock_irqsave(&gs_device_lock, &level);

        USART_SendData(uart->info->idx, buff[i]);
        /* Loop until UARTx DAT register is empty */
        while (USART_GetFlagStatus(uart->info->idx, USART_FLAG_TXDE) == RESET)
        {
        }

        os_spin_unlock_irqrestore(&gs_device_lock, level);
    }

    return size;
}

static const struct os_uart_ops cm_uart_ops = {
    .init   = cm_uart_init,
    .deinit = cm_uart_deinit,

    .start_send = cm_uart_start_send,
    .poll_send  = cm_uart_poll_send,
};

static int cm32_usart_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    os_ubase_t               level;
    struct serial_configure config = OS_SERIAL_CONFIG_DEFAULT;

    struct cm_uart *usart;

    usart = os_calloc(1, sizeof(struct cm_uart));
    OS_ASSERT(usart);

    usart->info                     = (struct cm32_usart_info *)dev->info;
    struct os_serial_device *serial = &usart->serial_dev;

    os_spin_lock_irqsave(&gs_device_lock, &level);
    os_list_add_tail(&cm32_uart_list, &usart->list);
    os_spin_unlock_irqrestore(&gs_device_lock, level);

    serial->ops    = &cm_uart_ops;
    serial->config = config;

    /* register uart device */
    os_hw_serial_register(serial, dev->name, usart);

    return 0;
}

OS_DRIVER_INFO cm32_usart_driver = {
    .name  = "Usart_Type",
    .probe = cm32_usart_probe,
};

OS_DRIVER_DEFINE(cm32_usart_driver, OS_INIT_LEVEL_PRE_DEVICE, OS_INIT_SUBLEVEL_HIGH);

#ifdef OS_USING_CONSOLE

static const struct cm32_usart_info *console_uart = OS_NULL;

void __os_hw_console_output(char *str)
{
    int i;

    if (console_uart == OS_NULL)
        return;

    for (i = 0; i < strlen(str); i++)
    {
        USART_SendData(console_uart->idx, str[i]);
        /* Loop until UARTx DAT register is empty */
        while (USART_GetFlagStatus(console_uart->idx, USART_FLAG_TXDE) == RESET)
        {
        }
    }
}

static int cm32_usart_early_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    if (strcmp(dev->name, OS_CONSOLE_DEVICE_NAME))
        return OS_SUCCESS;

    console_uart = (struct cm32_usart_info *)dev->info;

    struct serial_configure config = OS_SERIAL_CONFIG_DEFAULT;

    _cm_uart_init(console_uart, &config);

    return OS_SUCCESS;
}

OS_DRIVER_INFO cm32_usart_early_driver = {
    .name  = "Usart_Type",
    .probe = cm32_usart_early_probe,
};

OS_DRIVER_DEFINE(cm32_usart_early_driver, OS_INIT_LEVEL_PRE_KERNEL_1, OS_INIT_SUBLEVEL_LOW);
#endif

