/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with 
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_tsc.h
 *
 * @brief       This file implements lcd driver
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __DRV_TSC_H__
#define __DRV_TSC_H__
#include <drv_common.h>
#include "board.h"
#include <cm32m4xxr_tsc.h>

enum TSC_PORT_INDEX
{
    TSC_INDEX_A = 0,
    TSC_INDEX_B,
    TSC_INDEX_C,
    TSC_INDEX_D,

    TSC_INDEX_MAX
};

#define TSC_PIN_PER_PORT (16)
#define TSC_PORT_MAX     (TSC_INDEX_MAX)
#define TSC_PIN_MAX      (TSC_PORT_MAX * TSC_PIN_PER_PORT)

#define __TSC_PORT_INDEX(pin) (pin / TSC_PIN_PER_PORT)
#define __TSC_PIN_INDEX(pin)  (pin % TSC_PIN_PER_PORT)

#define GET_TSC(PORTx, PIN) (BSP_GPIO_TSC_BASE + ((TSC_INDEX_##PORTx - TSC_INDEX_A) * TSC_PIN_PER_PORT) + PIN)
#define TSC_OFFSET(__pin)   (1 << ((__pin) % TSC_PIN_PER_PORT))

struct tsc_index
{
    GPIO_Module *tsc_port;
};

struct tsc_irq_map
{
    uint16_t     chl;
    GPIO_Module *tsc_port;
    uint16_t     pin;
    uint16_t     num;
};

GPIO_Module *get_tsc_base(os_base_t pin);
int          os_hw_tsc_init(void);

#endif /* __DRV_TSC_H__ */
