/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with 
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_tsc.c
 *
 * @brief       This file implements tsc driver
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include <drv_tsc.h>

#define DBG_TAG "drv_tsc"

#define TSC_POART_A_BSAE 0
#define TSC_POART_B_BSAE 16
#define TSC_POART_C_BSAE 32
#define TSC_POART_D_BSAE 48

#define TSC_WK_TIME 0x1fff/*0x2fe*/

static uint32_t g_tsc_chl = 0;

uint32_t                 tsc_port_map = 0;
static struct tsc_index *indexs[TSC_PORT_MAX];
GPIO_Module             *tsc_port_base[TSC_PORT_MAX];

#define TSC_ITEM_NUM(items) sizeof(items) / sizeof(items[0])

#define TSC_INFO_MAP(gpio)                                                                                            \
    {                                                                                                                  \
        tsc_port_map |= (1 << TSC_INDEX_##gpio);                                                                     \
        tsc_port_base[TSC_INDEX_##gpio] = GPIO##gpio;                                                                \
    }

void __os_hw_tsc_init(void)
{
#if defined(GPIOA)
    TSC_INFO_MAP(A);
#endif
#if defined(GPIOB)
    TSC_INFO_MAP(B);
#endif
#if defined(GPIOC)
    TSC_INFO_MAP(C);
#endif
#if defined(GPIOD)
    TSC_INFO_MAP(D);
#endif
}

static const struct tsc_irq_map tsc_irq_map[] = {
    {0,  GPIOA, GPIO_PIN_4,  TSC_POART_A_BSAE + 4},
    {1,  GPIOA, GPIO_PIN_5,  TSC_POART_A_BSAE + 5},
    {2,  GPIOB, GPIO_PIN_14, TSC_POART_B_BSAE + 14},
    {3,  GPIOB, GPIO_PIN_15, TSC_POART_B_BSAE + 15},
    {4,  GPIOD, GPIO_PIN_8,  TSC_POART_D_BSAE + 8},
    {5,  GPIOD, GPIO_PIN_9,  TSC_POART_D_BSAE + 9},
    {6,  GPIOD, GPIO_PIN_11, TSC_POART_D_BSAE + 11},
    {7,  GPIOD, GPIO_PIN_12, TSC_POART_D_BSAE + 12},
    {8,  GPIOC, GPIO_PIN_6,  TSC_POART_C_BSAE + 6},
    {9,  GPIOC, GPIO_PIN_7,  TSC_POART_C_BSAE + 7},
    {10, GPIOC, GPIO_PIN_8,  TSC_POART_C_BSAE + 8},
    {11, GPIOC, GPIO_PIN_9,  TSC_POART_C_BSAE + 9},
    {12, GPIOC, GPIO_PIN_10, TSC_POART_C_BSAE + 10},
    {13, GPIOC, GPIO_PIN_11, TSC_POART_C_BSAE + 11},
    {14, GPIOC, GPIO_PIN_12, TSC_POART_C_BSAE + 12},
    {15, GPIOD, GPIO_PIN_2,  TSC_POART_D_BSAE + 2},
    {16, GPIOD, GPIO_PIN_4,  TSC_POART_D_BSAE + 4},
    {17, GPIOD, GPIO_PIN_5,  TSC_POART_D_BSAE + 5},
    {18, GPIOD, GPIO_PIN_6,  TSC_POART_D_BSAE + 6},
    {19, GPIOD, GPIO_PIN_7,  TSC_POART_D_BSAE + 7},
    {20, GPIOB, GPIO_PIN_6,  TSC_POART_B_BSAE + 6},
    {21, GPIOB, GPIO_PIN_7,  TSC_POART_B_BSAE + 7},
    {22, GPIOB, GPIO_PIN_8,  TSC_POART_B_BSAE + 8},
    {23, GPIOB, GPIO_PIN_9,  TSC_POART_B_BSAE + 9},
};

static struct os_pin_irq_hdr tsc_irq_hdr_tab[] = {
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
};

static struct tsc_index *get_tsc_index(os_base_t pin)
{
    struct tsc_index *index = OS_NULL;

    if (pin < TSC_PIN_MAX)
    {
        index = indexs[__TSC_PORT_INDEX(pin)];
    }

    return index;
}

GPIO_Module *get_tsc_base(os_base_t pin)
{
    struct tsc_index *index = get_tsc_index(pin);

    if (index != OS_NULL)
    {
        return index->tsc_port;
    }
    return OS_NULL;
}

int get_tsc_chl(int pin)
{
    for (int i = 0; i < 24; i++)
    {
        if (pin == tsc_irq_map[i].num)
            return tsc_irq_map[i].chl;
    }

    return -1;
}

void _tsc_auto_adjust(int chl)
{
    uint32_t tCnt;

    RCC_EnableAPB1PeriphClk(RCC_APB1_PERIPH_PWR,ENABLE);    /* Enable PWR  peripheral Clock */
    PWR_BackupAccessEnable(ENABLE);                         /* Set bit 8 of PWR_CTRL1.Open PWR DBP */
    RCC_ConfigRtcClk(RCC_RTCCLK_SRC_LSI);                   /* Set LSI as RTC clock */
    RCC_EnableRtcClk(ENABLE);                               /* Enable RTC clock */
    RTC_EnableWakeUpTsc(TSC_WK_TIME);                       /* Set TSC wakeup time as 0x2FE*LSI */

    TSC_ConfigHWMode(0x1UL << chl, ENABLE);
    delay_ms(300); /* wait for a TSC HW det interval to refresh the cnt value */
    while(TSC_GetStatus(TSC_STS_TYP_CHN_NUM) != chl);          /* make sure current detect channel is correct */
    while((tCnt = TSC_GetStatus(TSC_STS_TYP_CNTVALUE)) == 0); /* wait until detect done */
    TSC_ConfigHWMode(0x1UL << chl, DISABLE);                   /* disable hw detect */
    
    TSC_ConfigThreshold(0x1UL << chl, tCnt, tCnt/3);           /* set the cnt value as threshold */

    RTC_EnableWakeUpTsc(TSC_WK_TIME); /* Set wakeup time as the longest in normal work */
}

void _tsc_config_interrupt(uint32_t enabled)
{
    EXTI_InitType EXTI_InitStruct;

    if (enabled)
    {
        EXTI_InitStruct.EXTI_Line    = EXTI_LINE21;
        EXTI_InitStruct.EXTI_LineCmd = ENABLE;
        EXTI_InitStruct.EXTI_Mode    = EXTI_Mode_Interrupt;
        EXTI_InitStruct.EXTI_Trigger = EXTI_Trigger_Rising;

        EXTI_Init(&EXTI_InitStruct);

        /* Configure the ECLIC level and priority Bits */
        ECLIC_SetCfgNlbits(0); /* 0 bits for level, 4 bits for priority */

        /* Enable the TSC Interrupt */
        ECLIC_SetPriorityIRQ(TSC_IRQn, 1);
        ECLIC_SetTrigIRQ(TSC_IRQn, ECLIC_LEVEL_TRIGGER);
        ECLIC_EnableIRQ(TSC_IRQn);
    }
    else
    {
        EXTI_InitStruct.EXTI_Line    = EXTI_LINE21;
        EXTI_InitStruct.EXTI_LineCmd = DISABLE;
        EXTI_InitStruct.EXTI_Mode    = EXTI_Mode_Interrupt;
        EXTI_InitStruct.EXTI_Trigger = EXTI_Trigger_Rising;

        EXTI_Init(&EXTI_InitStruct);
        EXTI_ClrStatusFlag(EXTI_LINE21);

        ECLIC_DisableIRQ(TSC_IRQn);
        ECLIC_ClearPendingIRQ(TSC_IRQn);
    }
}

static os_err_t cm32_tsc_init(int pin)
{
    uint32_t      RCC_APB2_PERIPH_CLK_PORT;
    GPIO_InitType GPIO_InitStructure;
    TSC_InitType  Init = {0};

    int chl = get_tsc_chl(pin);

    TSC_ConfigClock(TSC_CLK_SRC_LSI);

    /* Initialize gpio used for tsc channel */
    if (tsc_irq_map[chl].tsc_port == GPIOA)
    {
        RCC_APB2_PERIPH_CLK_PORT = RCC_APB2_PERIPH_GPIOA;
    }
    else if (tsc_irq_map[chl].tsc_port == GPIOB)
    {
        RCC_APB2_PERIPH_CLK_PORT = RCC_APB2_PERIPH_GPIOB;
    }
    else if (tsc_irq_map[chl].tsc_port == GPIOC)
    {
        RCC_APB2_PERIPH_CLK_PORT = RCC_APB2_PERIPH_GPIOC;
    }
    else if (tsc_irq_map[chl].tsc_port == GPIOD)
    {
        RCC_APB2_PERIPH_CLK_PORT = RCC_APB2_PERIPH_GPIOD;
    }

    RCC_EnableAPB2PeriphClk(RCC_APB2_PERIPH_CLK_PORT, ENABLE);

    GPIO_InitStructure.GPIO_Mode    = GPIO_Mode_AIN;
    GPIO_InitStructure.GPIO_Speed   = GPIO_INPUT;
    GPIO_InitStructure.Pin          = tsc_irq_map[chl].pin;
    GPIO_Init(tsc_irq_map[chl].tsc_port, &GPIO_InitStructure);

    Init.Period = TSC_DET_PERIOD_8;     /* Detect priod= 8/TSC_CLK */
    Init.Filter = TSC_DET_FILTER_4;     /* Detect filter= 1 sample */
    Init.Type   = TSC_DET_TYPE_LESS;    /* Enable less detect */
    Init.Int    = TSC_IT_DET_ENABLE;    /* Enable interrupt */
    Init.PadOpt = TSC_PAD_INTERNAL_RES; /* Use internal resistor */
    Init.Speed  = TSC_PAD_SPEED_0;      /* Low speed */
    TSC_Init(&Init);

    TSC_ConfigInternalResistor(TSC_CHN_SEL_ALL, TSC_RESR0_CHN_RESIST0_125K);

    _tsc_config_interrupt(DISABLE);

    _tsc_auto_adjust(chl);

    _tsc_config_interrupt(ENABLE);

    return  OS_SUCCESS;
}

static void cm32_tsc_write(struct os_device *dev, os_base_t pin, os_base_t value)
{

}

static os_err_t cm32_tsc_read(struct os_device *dev, os_base_t pin)
{
    return 1;
}

static void cm32_tsc_mode(struct os_device *dev, os_base_t pin, os_base_t mode)
{
    int8_t tsc_chl;

    tsc_chl = get_tsc_chl(pin);

    if (tsc_chl == -1)
    {
        os_kprintf("This pin not support tsc!\r\n");
        return;
    }
}

OS_INLINE int32_t bit2bitno(uint32_t bit)
{
    int i;
    for (i = 0; i < 32; i++)
    {
        if ((0x01 << i) == bit)
        {
            return i;
        }
    }
    return -1;
}

static os_err_t cm32_tsc_attach_irq(struct os_device *device, int32_t pin,
                                          uint32_t mode, void (*hdr)(void *args), void *args)
{
    const struct tsc_index *index;
    os_ubase_t               level;
    int32_t              irqindex = -1;

    int8_t tsc_chl;

    tsc_chl = get_tsc_chl(pin);

    if (tsc_chl == -1)
    {
        os_kprintf("This pin not support tsc!\r\n");
        return OS_NOSYS;
    }

    index = get_tsc_index(pin);
    if (index == OS_NULL)
    {
        return OS_NOSYS;
    }

    irqindex = tsc_chl;

    if (irqindex < 0 || irqindex >= TSC_ITEM_NUM(tsc_irq_map))
    {
        return OS_NOSYS;
    }

    os_spin_lock_irqsave(&gs_device_lock, &level);

    if (tsc_irq_hdr_tab[irqindex].pin == pin && tsc_irq_hdr_tab[irqindex].hdr == hdr &&
        tsc_irq_hdr_tab[irqindex].mode == mode && tsc_irq_hdr_tab[irqindex].args == args)
    {
        os_spin_unlock_irqrestore(&gs_device_lock, level);
        return OS_SUCCESS;
    }

    if (tsc_irq_hdr_tab[irqindex].pin != -1)
    {
        os_spin_unlock_irqrestore(&gs_device_lock, level);
        return OS_BUSY;
    }

    tsc_irq_hdr_tab[irqindex].pin  = pin;
    tsc_irq_hdr_tab[irqindex].hdr  = hdr;
    tsc_irq_hdr_tab[irqindex].mode = mode;
    tsc_irq_hdr_tab[irqindex].args = args;

    os_spin_unlock_irqrestore(&gs_device_lock, level);

    return OS_SUCCESS;
}

static os_err_t cm32_tsc_detach_irq(struct os_device *device, int32_t pin)
{
    const struct tsc_index *index;
    os_ubase_t               level;
    int32_t              irqindex = -1;

    int8_t tsc_chl;

    tsc_chl = get_tsc_chl(pin);

    if (tsc_chl == -1)
    {
        os_kprintf("This pin not support tsc!\r\n");
        return OS_NOSYS;
    }

    index = get_tsc_index(pin);
    if (index == OS_NULL)
    {
        return OS_NOSYS;
    }

    irqindex = tsc_chl;

    if (irqindex < 0 || irqindex >= TSC_ITEM_NUM(tsc_irq_map))
    {
        return OS_NOSYS;
    }

    os_spin_lock_irqsave(&gs_device_lock, &level);
    if (tsc_irq_hdr_tab[irqindex].pin == -1)
    {
        os_spin_unlock_irqrestore(&gs_device_lock, level);
        return OS_SUCCESS;
    }

    tsc_irq_hdr_tab[irqindex].pin  = -1;
    tsc_irq_hdr_tab[irqindex].hdr  = OS_NULL;
    tsc_irq_hdr_tab[irqindex].mode = 0;
    tsc_irq_hdr_tab[irqindex].args = OS_NULL;

    os_spin_unlock_irqrestore(&gs_device_lock, level);

    return OS_SUCCESS;
}

static os_err_t cm32_tsc_irq_enable(struct os_device *device, os_base_t pin, uint32_t enabled)
{
    const struct tsc_index   *index;
    const struct tsc_irq_map *irqmap;
    os_ubase_t                 level;
    int32_t                irqindex = -1;

    int8_t tsc_chl;

    tsc_chl = get_tsc_chl(pin);

    if (tsc_chl == -1)
    {
        os_kprintf("This pin not support tsc!\r\n");
        return OS_NOSYS;
    }

    index = get_tsc_index(pin);
    if (index == OS_NULL)
    {
        return OS_NOSYS;
    }

    irqindex = tsc_chl;

    if (enabled == PIN_IRQ_ENABLE)
    {
        if (irqindex < 0 || irqindex >= TSC_ITEM_NUM(tsc_irq_map))
        {
            return OS_NOSYS;
        }

        os_spin_lock_irqsave(&gs_device_lock, &level);

        if (tsc_irq_hdr_tab[irqindex].pin == -1)
        {
            os_spin_unlock_irqrestore(&gs_device_lock, level);
            return OS_NOSYS;
        }

        g_tsc_chl |= 0x1UL << tsc_chl;

        cm32_tsc_init(pin);

        TSC_ConfigHWMode(g_tsc_chl, ENABLE);

        os_spin_unlock_irqrestore(&gs_device_lock, level);
    }
    else if (enabled == PIN_IRQ_DISABLE)
    {
        os_spin_lock_irqsave(&gs_device_lock, &level);

        if (tsc_irq_hdr_tab[irqindex].pin == -1)
        {
            os_spin_unlock_irqrestore(&gs_device_lock, level);
            return OS_NOSYS;
        }

        g_tsc_chl &= ~(0x1UL << tsc_chl);

        TSC_ConfigHWMode(g_tsc_chl, ENABLE);

        os_spin_unlock_irqrestore(&gs_device_lock, level);
    }
    else
    {
        return OS_NOSYS;
    }

    return OS_SUCCESS;
}

void TSC_IRQHandler(void)
{
    if(EXTI_GetStatusFlag(EXTI_LINE21)==SET)
    {
        int irqno = TSC_GetStatus(TSC_STS_TYP_CHN_NUM);

        if (tsc_irq_hdr_tab[irqno].hdr)
        {
            tsc_irq_hdr_tab[irqno].hdr(tsc_irq_hdr_tab[irqno].args);
        }

        EXTI_ClrStatusFlag(EXTI_LINE21);

        TSC_ConfigHWMode(g_tsc_chl, ENABLE);
    }
}

const static struct os_pin_ops cm32_tsc_ops = {
    .pin_mode  = cm32_tsc_mode,
    .pin_write = cm32_tsc_write,
    .pin_read  = cm32_tsc_read,

    .pin_attach_irq = cm32_tsc_attach_irq,
    .pin_detach_irq = cm32_tsc_detach_irq,
    .pin_irq_enable = cm32_tsc_irq_enable,
};

int os_hw_tsc_init(void)
{
    struct tsc_index *tmp       = OS_NULL;
    uint8_t        gpio_port = 0;
    uint8_t        gpio_pin  = 0;

    __os_hw_tsc_init();

    for (gpio_port = 0; gpio_port < TSC_PORT_MAX; gpio_port++)
    {
        if ((tsc_port_map & (1 << gpio_port)))
        {
            tmp = (struct tsc_index *)os_calloc(1, sizeof(struct tsc_index));

            if (tmp == OS_NULL)
            {
                LOG_E(DBG_TAG, "os_malloc error!!!");
                return OS_NOMEM;
            }

            tmp->tsc_port = tsc_port_base[gpio_port];

            indexs[gpio_port] = tmp;
        }
        else
        {
            indexs[gpio_port] = OS_NULL;
        }
    }

    return os_device_pin_register(BSP_GPIO_TSC_BASE, &cm32_tsc_ops, OS_NULL);
}

OS_INIT_CALL(os_hw_tsc_init, OS_INIT_LEVEL_DEVICE, OS_INIT_SUBLEVEL_LOW);
