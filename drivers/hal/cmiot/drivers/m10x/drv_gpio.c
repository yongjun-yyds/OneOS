/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_gpio.c
 *
 * @brief       This file implements gpio driver for cm32.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <os_assert.h>
#include "drv_gpio.h"

#ifdef OS_USING_PIN

#define DBG_TAG "drv_gpio"

uint32_t              gpio_port_map = 0;
static struct pin_index *indexs[GPIO_PORT_MAX];
GPIO_Module             *gpio_port_base[GPIO_PORT_MAX];

#define ITEM_NUM(items) sizeof(items) / sizeof(items[0])

#define GPIO_INFO_MAP(gpio)                                                                                            \
    {                                                                                                                  \
        gpio_port_map |= (1 << GPIO_INDEX_##gpio);                                                                     \
        gpio_port_base[GPIO_INDEX_##gpio] = GPIO##gpio;                                                                \
    }

void __os_hw_pin_init(void)
{
#if defined(GPIOA)
    GPIO_INFO_MAP(A);
#endif
#if defined(GPIOB)
    GPIO_INFO_MAP(B);
#endif
#if defined(GPIOC)
    GPIO_INFO_MAP(C);
#endif
#if defined(GPIOD)
    GPIO_INFO_MAP(D);
#endif
#if defined(GPIOE)
    GPIO_INFO_MAP(E);
#endif
#if defined(GPIOF)
    GPIO_INFO_MAP(F);
#endif
#if defined(GPIOG)
    GPIO_INFO_MAP(G);
#endif
#if defined(GPIOH)
    GPIO_INFO_MAP(H);
#endif
#if defined(GPIOI)
    GPIO_INFO_MAP(I);
#endif
#if defined(GPIOJ)
    GPIO_INFO_MAP(J);
#endif
#if defined(GPIOK)
    GPIO_INFO_MAP(K);
#endif
}

/* clang-format off */
static const struct pin_irq_map pin_irq_map[] = {
    {EXTI0_IRQn, GPIO_PIN_SOURCE0, EXTI_LINE0},
    {EXTI1_IRQn, GPIO_PIN_SOURCE1, EXTI_LINE1},
    {EXTI2_IRQn, GPIO_PIN_SOURCE2, EXTI_LINE2},
    {EXTI3_IRQn, GPIO_PIN_SOURCE3, EXTI_LINE3},
    {EXTI4_IRQn, GPIO_PIN_SOURCE4, EXTI_LINE4},
    {EXTI9_5_IRQn, GPIO_PIN_SOURCE5, EXTI_LINE5},
    {EXTI9_5_IRQn, GPIO_PIN_SOURCE6, EXTI_LINE6},
    {EXTI9_5_IRQn, GPIO_PIN_SOURCE7, EXTI_LINE7},
    {EXTI9_5_IRQn, GPIO_PIN_SOURCE8, EXTI_LINE8},
    {EXTI9_5_IRQn, GPIO_PIN_SOURCE9, EXTI_LINE9},
    {EXTI15_10_IRQn, GPIO_PIN_SOURCE10, EXTI_LINE10},
    {EXTI15_10_IRQn, GPIO_PIN_SOURCE11, EXTI_LINE11},
    {EXTI15_10_IRQn, GPIO_PIN_SOURCE12, EXTI_LINE12},
    {EXTI15_10_IRQn, GPIO_PIN_SOURCE13, EXTI_LINE13},
    {EXTI15_10_IRQn, GPIO_PIN_SOURCE14, EXTI_LINE14},
    {EXTI15_10_IRQn, GPIO_PIN_SOURCE15, EXTI_LINE15},
};
/* clang-format on */

static struct os_pin_irq_hdr pin_irq_hdr_tab[] = {
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
};

static struct pin_index *get_pin_index(os_base_t pin)
{
    struct pin_index *index = OS_NULL;

    if (pin < GPIO_PIN_MAX)
    {
        index = indexs[__PORT_INDEX(pin)];
    }

    return index;
}

GPIO_Module *get_pin_base(os_base_t pin)
{
    struct pin_index *index = get_pin_index(pin);

    if (index != OS_NULL)
    {
        return index->gpio_port;
    }
    return OS_NULL;
}

static void cm32_pin_write(struct os_device *dev, os_base_t pin, os_base_t value)
{
    const struct pin_index *index;

    index = get_pin_index(pin);
    if (index == OS_NULL)
    {
        return;
    }

    GPIO_WriteBit(index->gpio_port, PIN_OFFSET(pin), (Bit_OperateType)value);
}

static int cm32_pin_read(struct os_device *dev, os_base_t pin)
{
    int                     value;
    const struct pin_index *index;

    value = PIN_LOW;

    index = get_pin_index(pin);
    if (index == OS_NULL)
    {
        return value;
    }

    value = GPIO_ReadInputDataBit(index->gpio_port, PIN_OFFSET(pin));

    return value;
}

static void cm32_pin_mode(struct os_device *dev, os_base_t pin, os_base_t mode)
{
    os_ubase_t         level;
    struct pin_index *index;

    index = get_pin_index(pin);
    if (index == OS_NULL)
    {
        return;
    }

    GPIO_InitType GPIO_InitStructure;

    /* Check the parameters */
    OS_ASSERT(IS_GPIO_ALL_PERIPH(index->gpio_port));

    /* Enable the GPIO Clock */
    if (index->gpio_port == GPIOA)
    {
        RCC_EnableAPB2PeriphClk(RCC_APB2_PERIPH_GPIOA, ENABLE);
    }
    else if (index->gpio_port == GPIOB)
    {
        RCC_EnableAPB2PeriphClk(RCC_APB2_PERIPH_GPIOB, ENABLE);
    }
    else if (index->gpio_port == GPIOC)
    {
        RCC_EnableAPB2PeriphClk(RCC_APB2_PERIPH_GPIOC, ENABLE);
    }
    else
    {
        if (index->gpio_port == GPIOD)
        {
            RCC_EnableAPB2PeriphClk(RCC_APB2_PERIPH_GPIOD, ENABLE);
        }
    }

    /* Configure the GPIO pin */
    if (PIN_OFFSET(pin) <= GPIO_PIN_ALL)
    {
        GPIO_InitStruct(&GPIO_InitStructure);
        GPIO_InitStructure.Pin       = PIN_OFFSET(pin);
        GPIO_InitStructure.GPIO_Pull = GPIO_No_Pull;

        switch (mode)
        {
        case PIN_MODE_OUTPUT:
            GPIO_InitStructure.GPIO_Current = GPIO_DC_4mA;
            GPIO_InitStructure.GPIO_Mode    = GPIO_Mode_Out_PP;
            break;

        case PIN_MODE_INPUT:
            GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Input;
            break;

        case PIN_MODE_INPUT_PULLUP:
            GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Input;
            GPIO_InitStructure.GPIO_Pull = GPIO_Pull_Up;
            break;

        case PIN_MODE_INPUT_PULLDOWN:
            GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Input;
            GPIO_InitStructure.GPIO_Pull = GPIO_Pull_Down;
            break;

        case PIN_MODE_OUTPUT_OD:
            GPIO_InitStructure.GPIO_Current = GPIO_DC_4mA;
            GPIO_InitStructure.GPIO_Mode    = GPIO_Mode_Out_OD;
            break;

        default:
            break;
        }

        /* remeber the pull state. */
        os_spin_lock_irqsave(&gs_device_lock, &level);
        index->pin_pulls[__PIN_INDEX(pin)].pull_state = GPIO_InitStructure.GPIO_Pull;
        os_spin_unlock_irqrestore(&gs_device_lock, level);
        GPIO_InitPeripheral(index->gpio_port, &GPIO_InitStructure);
    }
}

OS_INLINE int32_t bit2bitno(uint32_t bit)
{
    int i;
    for (i = 0; i < 32; i++)
    {
        if ((0x01 << i) == bit)
        {
            return i;
        }
    }
    return -1;
}

OS_INLINE const struct pin_irq_map *get_pin_irq_map(uint32_t pinbit)
{
    int32_t mapindex = bit2bitno(pinbit);
    if (mapindex < 0 || mapindex >= ITEM_NUM(pin_irq_map))
    {
        return OS_NULL;
    }
    return &pin_irq_map[mapindex];
};

static os_err_t
cm32_pin_attach_irq(struct os_device *device, int32_t pin, uint32_t mode, void (*hdr)(void *args), void *args)
{
    const struct pin_index *index;
    os_ubase_t               level;
    int32_t              irqindex = -1;

    index = get_pin_index(pin);
    if (index == OS_NULL)
    {
        return OS_NOSYS;
    }

    irqindex = bit2bitno(PIN_OFFSET(pin));
    if (irqindex < 0 || irqindex >= ITEM_NUM(pin_irq_map))
    {
        return OS_NOSYS;
    }

    os_spin_lock_irqsave(&gs_device_lock, &level);

    if (pin_irq_hdr_tab[irqindex].pin == pin && pin_irq_hdr_tab[irqindex].hdr == hdr &&
        pin_irq_hdr_tab[irqindex].mode == mode && pin_irq_hdr_tab[irqindex].args == args)
    {
        os_spin_unlock_irqrestore(&gs_device_lock, level);
        return OS_SUCCESS;
    }

    if (pin_irq_hdr_tab[irqindex].pin != -1)
    {
        os_spin_unlock_irqrestore(&gs_device_lock, level);
        return OS_BUSY;
    }

    pin_irq_hdr_tab[irqindex].pin  = pin;
    pin_irq_hdr_tab[irqindex].hdr  = hdr;
    pin_irq_hdr_tab[irqindex].mode = mode;
    pin_irq_hdr_tab[irqindex].args = args;

    os_spin_unlock_irqrestore(&gs_device_lock, level);

    return OS_SUCCESS;
}

static os_err_t cm32_pin_dettach_irq(struct os_device *device, int32_t pin)
{
    const struct pin_index *index;
    os_ubase_t               level;
    int32_t              irqindex = -1;

    index = get_pin_index(pin);
    if (index == OS_NULL)
    {
        return OS_NOSYS;
    }

    irqindex = bit2bitno(PIN_OFFSET(pin));
    if (irqindex < 0 || irqindex >= ITEM_NUM(pin_irq_map))
    {
        return OS_NOSYS;
    }

    os_spin_lock_irqsave(&gs_device_lock, &level);
    if (pin_irq_hdr_tab[irqindex].pin == -1)
    {
        os_spin_unlock_irqrestore(&gs_device_lock, level);
        return OS_SUCCESS;
    }

    pin_irq_hdr_tab[irqindex].pin  = -1;
    pin_irq_hdr_tab[irqindex].hdr  = OS_NULL;
    pin_irq_hdr_tab[irqindex].mode = 0;
    pin_irq_hdr_tab[irqindex].args = OS_NULL;

    os_spin_unlock_irqrestore(&gs_device_lock, level);

    return OS_SUCCESS;
}

OS_INLINE uint8_t get_gpio_port_source(GPIO_Module *GPIO_BASE)
{
    uint8_t GPIO_PORT_SOURCE;

    if (GPIO_BASE == GPIOA)
    {
        GPIO_PORT_SOURCE = GPIOA_PORT_SOURCE;
    }
    else if (GPIO_BASE == GPIOB)
    {
        GPIO_PORT_SOURCE = GPIOB_PORT_SOURCE;
    }
    else if (GPIO_BASE == GPIOC)
    {
        GPIO_PORT_SOURCE = GPIOC_PORT_SOURCE;
    }
    else
    {
        GPIO_PORT_SOURCE = GPIOD_PORT_SOURCE;
    }

    return GPIO_PORT_SOURCE;
};

static os_err_t cm32_pin_irq_enable(struct os_device *device, os_base_t pin, uint32_t enabled)
{
    const struct pin_index   *index;
    const struct pin_irq_map *irqmap;
    os_ubase_t                 level;
    int32_t                irqindex = -1;

    GPIO_InitType GPIO_InitStructure;
    NVIC_InitType NVIC_InitStructure;

    RCC_EnableAPB2PeriphClk(RCC_APB2_PERIPH_AFIO, ENABLE);

    GPIO_InitStruct(&GPIO_InitStructure);

    index = get_pin_index(pin);
    if (index == OS_NULL)
    {
        return OS_NOSYS;
    }

    GPIO_InitStructure.Pin            = PIN_OFFSET(pin);
    GPIO_InitStructure.GPIO_Pull      = index->pin_pulls[__PIN_INDEX(pin)].pull_state;
    GPIO_InitStructure.GPIO_Alternate = GPIO_NO_AF;

    if (enabled == PIN_IRQ_ENABLE)
    {
        irqindex = bit2bitno(PIN_OFFSET(pin));
        if (irqindex < 0 || irqindex >= ITEM_NUM(pin_irq_map))
        {
            return OS_NOSYS;
        }

        os_spin_lock_irqsave(&gs_device_lock, &level);

        if (pin_irq_hdr_tab[irqindex].pin == -1)
        {
            os_spin_unlock_irqrestore(&gs_device_lock, level);
            return OS_NOSYS;
        }

        irqmap = &pin_irq_map[irqindex];

        switch (pin_irq_hdr_tab[irqindex].mode)
        {
        case PIN_IRQ_MODE_RISING:
            GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IT_Rising;
            break;
        case PIN_IRQ_MODE_FALLING:
            GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IT_Falling;
            break;
        case PIN_IRQ_MODE_RISING_FALLING:
            GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IT_Rising_Falling;
            break;
        case PIN_IRQ_MODE_HIGH_LEVEL:
        case PIN_IRQ_MODE_LOW_LEVEL:
        default:
            os_spin_unlock_irqrestore(&gs_device_lock, level);
            return OS_FAILURE;
        }

        GPIO_InitPeripheral(index->gpio_port, &GPIO_InitStructure);

        GPIO_ConfigEXTILine(get_gpio_port_source(index->gpio_port), irqmap->pinsource);

        NVIC_InitStructure.NVIC_IRQChannel                   = irqmap->irqno;
        NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 3;
        NVIC_InitStructure.NVIC_IRQChannelSubPriority        = 3;
        NVIC_InitStructure.NVIC_IRQChannelCmd                = ENABLE;

        NVIC_ClearPendingIRQ(irqmap->irqno);
        EXTI_ClrITPendBit(irqmap->exti_line);
        NVIC_Init(&NVIC_InitStructure);

        os_spin_unlock_irqrestore(&gs_device_lock, level);
    }
    else if (enabled == PIN_IRQ_DISABLE)
    {
        irqmap = get_pin_irq_map(PIN_OFFSET(pin));
        if (irqmap == OS_NULL)
        {
            return OS_NOSYS;
        }

        os_spin_lock_irqsave(&gs_device_lock, &level);

        NVIC_InitStructure.NVIC_IRQChannel                   = irqmap->irqno;
        NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 3;
        NVIC_InitStructure.NVIC_IRQChannelSubPriority        = 3;
        NVIC_InitStructure.NVIC_IRQChannelCmd                = DISABLE;

        NVIC_Init(&NVIC_InitStructure);
        NVIC_ClearPendingIRQ(irqmap->irqno);

        GPIO_InitPeripheral(index->gpio_port, &GPIO_InitStructure);

        os_spin_unlock_irqrestore(&gs_device_lock, level);
    }
    else
    {
        return OS_NOSYS;
    }

    return OS_SUCCESS;
}

const static struct os_pin_ops _cm32_pin_ops = {
    .pin_mode       = cm32_pin_mode,
    .pin_write      = cm32_pin_write,
    .pin_read       = cm32_pin_read,
    .pin_attach_irq = cm32_pin_attach_irq,
    .pin_detach_irq = cm32_pin_dettach_irq,
    .pin_irq_enable = cm32_pin_irq_enable,
};

OS_INLINE void pin_irq_hdr(uint32_t exti_line, int irqno)
{
    if (EXTI_GetStatusFlag(exti_line) == SET)
    {
        if (pin_irq_hdr_tab[irqno].hdr)
        {
            pin_irq_hdr_tab[irqno].hdr(pin_irq_hdr_tab[irqno].args);
        }

        EXTI_ClrStatusFlag(exti_line);
    }
}

void EXTI0_IRQHandler(void)
{
    pin_irq_hdr(EXTI_LINE0, 0);
}

void EXTI1_IRQHandler(void)
{
    pin_irq_hdr(EXTI_LINE1, 1);
}

void EXTI2_IRQHandler(void)
{
    pin_irq_hdr(EXTI_LINE2, 2);
}

void EXTI3_IRQHandler(void)
{
    pin_irq_hdr(EXTI_LINE3, 3);
}

void EXTI4_IRQHandler(void)
{
    pin_irq_hdr(EXTI_LINE4, 4);
}

void EXTI9_5_IRQHandler(void)
{
    pin_irq_hdr(EXTI_LINE5, 5);
    pin_irq_hdr(EXTI_LINE6, 6);
    pin_irq_hdr(EXTI_LINE7, 7);
    pin_irq_hdr(EXTI_LINE8, 8);
    pin_irq_hdr(EXTI_LINE9, 9);
}

void EXTI15_10_IRQHandler(void)
{
    pin_irq_hdr(EXTI_LINE10, 10);
    pin_irq_hdr(EXTI_LINE11, 11);
    pin_irq_hdr(EXTI_LINE12, 12);
    pin_irq_hdr(EXTI_LINE13, 13);
    pin_irq_hdr(EXTI_LINE14, 14);
    pin_irq_hdr(EXTI_LINE15, 15);
}

/**
 ***********************************************************************************************************************
 * @brief           os_hw_pin_init:enable gpio clk,register pin device.
 *
 * @param[in]       none
 *
 * @return          Return init result.
 * @retval          OS_SUCCESS       init success.
 * @retval          Others       init failed.
 ***********************************************************************************************************************
 */
int os_hw_pin_init(void)
{
    struct pin_index *tmp       = OS_NULL;
    uint8_t        gpio_port = 0;
    uint8_t        gpio_pin  = 0;

    __os_hw_pin_init();

    for (gpio_port = 0; gpio_port < GPIO_PORT_MAX; gpio_port++)
    {
        if ((gpio_port_map & (1 << gpio_port)))
        {
            tmp = (struct pin_index *)os_calloc(1, sizeof(struct pin_index));

            if (tmp == OS_NULL)
            {
                LOG_E(DBG_TAG, "os_malloc error!!!");
                return OS_NOMEM;
            }

            tmp->gpio_port = gpio_port_base[gpio_port];

            for (gpio_pin = 0; gpio_pin < GPIO_PIN_PER_PORT; gpio_pin++)
            {
                tmp->pin_pulls[gpio_pin].pull_state = GPIO_No_Pull;
            }

            indexs[gpio_port] = tmp;
        }
        else
        {
            indexs[gpio_port] = OS_NULL;
        }
    }

    return os_device_pin_register(0, &_cm32_pin_ops, OS_NULL);
}

#endif /* OS_USING_PIN */
