/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_hwtimer.c
 *
 * @brief       This file implements timer driver for cm32.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <board.h>
#include <os_memory.h>
#include <timer/timer.h>

#ifdef OS_USING_CLOCKSOURCE
#include <timer/clocksource.h>
#endif

#ifdef OS_USING_CLOCKEVENT
#include <timer/clockevent.h>
#endif

#include <drv_hwtimer.h>

#define DBG_TAG "drv.hwtimer"
#include <dlog.h>

static os_list_node_t cm32_timer_list = OS_LIST_INIT(cm32_timer_list);

struct cm32_timer
{
    union _clock
    {
        os_clocksource_t cs;
        os_clockevent_t  ce;
    } clock;

    struct cm32_timer_info *info;
    uint32_t             freq;
    os_list_node_t          list;

    void *user_data;
};

static void timer_isr(struct cm32_timer *timer)
{
    if (TIM_GetIntStatus(timer->info->htim, TIM_INT_UPDATE) != RESET)
    {
        TIM_ClrIntPendingBit(timer->info->htim, TIM_INT_UPDATE);
        os_clockevent_isr((os_clockevent_t *)timer);
    }
}

static void TIMn_IRQHandler(TIM_Module *htim)
{
    struct cm32_timer *timer;

    os_list_for_each_entry(timer, &cm32_timer_list, struct cm32_timer, list)
    {
        if (timer->info->htim == htim)
        {
            timer_isr(timer);
            return;
        }
    }
}

void TIM1_UP_IRQHandler(void)
{
    TIMn_IRQHandler(TIM1);
}

void TIM2_IRQHandler(void)
{
    TIMn_IRQHandler(TIM2);
}

void TIM3_IRQHandler(void)
{
    TIMn_IRQHandler(TIM3);
}

void TIM4_IRQHandler(void)
{
    TIMn_IRQHandler(TIM4);
}

void TIM5_IRQHandler(void)
{
    TIMn_IRQHandler(TIM5);
}

void TIM8_UP_IRQHandler(void)
{
    TIMn_IRQHandler(TIM8);
}

static uint64_t cm32_timer_read(void *clock)
{
    struct cm32_timer *timer;

    timer = (struct cm32_timer *)clock;
    return TIM_GetCnt(timer->info->htim);
}

#ifdef OS_USING_CLOCKEVENT

static void cm32_timer_start(os_clockevent_t *ce, uint32_t prescaler, uint64_t count)
{
    struct cm32_timer *timer;

    TIM_TimeBaseInitType TIM_TimeBaseStructure;

    OS_ASSERT(prescaler != 0);
    OS_ASSERT(count != 0);

    timer = (struct cm32_timer *)ce;

    TIM_DeInit(timer->info->htim);

    /* Time base configuration */
    TIM_TimeBaseStructure.Period    = count & timer->clock.ce.count_mask;
    TIM_TimeBaseStructure.Prescaler = prescaler - 1;
    TIM_TimeBaseStructure.ClkDiv    = 0;
    TIM_TimeBaseStructure.CntMode   = TIM_CNT_MODE_UP;

    TIM_ClrIntPendingBit(timer->info->htim, TIM_INT_UPDATE);
    TIM_InitTimeBase(timer->info->htim, &TIM_TimeBaseStructure);

    /* Prescaler configuration */
    TIM_ConfigPrescaler(timer->info->htim, prescaler - 1, TIM_PSC_RELOAD_MODE_IMMEDIATE);

    /* TIMx enable update irq */
    TIM_ClrIntPendingBit(timer->info->htim, TIM_INT_UPDATE);
    TIM_ConfigInt(timer->info->htim, TIM_INT_UPDATE, ENABLE);

    /* TIMx enable counter */
    TIM_Enable(timer->info->htim, ENABLE);
}

static void cm32_timer_stop(os_clockevent_t *ce)
{
    struct cm32_timer *timer;

    OS_ASSERT(ce != OS_NULL);
    timer = (struct cm32_timer *)ce;

    TIM_ConfigInt(timer->info->htim, TIM_INT_UPDATE, DISABLE);
    TIM_Enable(timer->info->htim, DISABLE);
}

static const struct os_clockevent_ops cm32_tim_ops = {
    .start = cm32_timer_start,
    .stop  = cm32_timer_stop,
    .read  = cm32_timer_read,
};
#endif

static void __os_hw_tim_init(struct cm32_timer *tim_drv)
{
    NVIC_InitType NVIC_InitStructure;

    if (tim_drv->info->htim == TIM1 || tim_drv->info->htim == TIM8)
    {
        RCC_EnableAPB2PeriphClk(tim_drv->info->periph_clk, ENABLE);
    }
    else
    {
        RCC_EnableAPB1PeriphClk(tim_drv->info->periph_clk, ENABLE);
    }

    NVIC_InitStructure.NVIC_IRQChannel                   = tim_drv->info->irqn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority        = 1;
    NVIC_InitStructure.NVIC_IRQChannelCmd                = ENABLE;

    NVIC_Init(&NVIC_InitStructure);
}

static int cm32_tim_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    uint32_t          freq;
    struct cm32_timer   *timer;
    RCC_ClocksType       RCC_ClockStructure;
    TIM_TimeBaseInitType TIM_TimeBaseStructure;

    timer = os_calloc(1, sizeof(struct cm32_timer));
    OS_ASSERT(timer);

    timer->info = (struct cm32_timer_info *)dev->info;

    __os_hw_tim_init(timer);

    RCC_GetClocksFreqValue(&RCC_ClockStructure);

    if (timer->info->htim == TIM1 || timer->info->htim == TIM8)
    {
        freq = RCC_ClockStructure.Pclk2Freq * 2;
    }
    else
    {
        freq = RCC_ClockStructure.Pclk1Freq * 2;
    }

    if (timer->info->mode == TIMER_MODE_TIM)
    {
#ifdef OS_USING_CLOCKSOURCE
        if (os_clocksource_best() == OS_NULL)
        {
            TIM_TimeBaseStructure.Period    = 0xfffful;
            TIM_TimeBaseStructure.Prescaler = 539;
            TIM_TimeBaseStructure.ClkDiv    = 0;
            TIM_TimeBaseStructure.CntMode   = TIM_CNT_MODE_UP;

            TIM_InitTimeBase(timer->info->htim, &TIM_TimeBaseStructure);
            TIM_ConfigPrescaler(timer->info->htim, 539, TIM_PSC_RELOAD_MODE_IMMEDIATE);
            TIM_Enable(timer->info->htim, ENABLE);

            timer->freq = freq / (539 + 1);

            timer->clock.cs.rating = 160;
            timer->clock.cs.freq   = timer->freq;
            timer->clock.cs.mask   = 0xffffull;
            timer->clock.cs.read   = cm32_timer_read;

            os_clocksource_register(dev->name, &timer->clock.cs);
        }
        else
#endif
        {
#ifdef OS_USING_CLOCKEVENT

            timer->freq = freq;

            timer->clock.ce.rating         = 160;
            timer->clock.ce.freq           = timer->freq;
            timer->clock.ce.mask           = 0xfffful;
            timer->clock.ce.prescaler_mask = 0xfffful;
            timer->clock.ce.prescaler_bits = 16;
            timer->clock.ce.count_mask     = 0xfffful;
            timer->clock.ce.count_bits     = 16;
            timer->clock.ce.feature        = OS_CLOCKEVENT_FEATURE_PERIOD;
            timer->clock.ce.min_nsec       = NSEC_PER_SEC / timer->clock.ce.freq;
            timer->clock.ce.ops            = &cm32_tim_ops;

            os_clockevent_register(dev->name, &timer->clock.ce);
#endif
        }
    }

    os_list_add(&cm32_timer_list, &timer->list);

    return OS_SUCCESS;
}

OS_DRIVER_INFO cm32_tim_driver = {
    .name  = "TIMER_Type",
    .probe = cm32_tim_probe,
};

OS_DRIVER_DEFINE(cm32_tim_driver, OS_INIT_LEVEL_PRE_DEVICE, OS_INIT_SUBLEVEL_MIDDLE);

