/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_can.h
 *
 * @brief       This file implements CAN driver for cm32.
 *
 * @revision
 * Date         Author          Notes
 * 2022-07-28   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __DRV_CAN_H_
#define __DRV_CAN_H_

#include <os_types.h>
#include <cm32m101a_gpio.h>
#include <cm32m101a_can.h>

struct cm32_can_info
{
    CAN_Module *can_instance;

    GPIO_Module  *gpio;
    uint32_t   gpio_rcc;
    GPIO_InitType gpio_init_structure_rx;
    GPIO_InitType gpio_init_structure_tx;
};

#endif /* __DRV_CAN_H_ */
