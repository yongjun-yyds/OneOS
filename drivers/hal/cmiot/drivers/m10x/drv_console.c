/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_console.c
 *
 * @brief       This file implements dma irq for cm32
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <drv_usart.h>
#include <drv_lpuart.h>

#ifdef OS_USING_CONSOLE
void __os_hw_console_output(char *str)
{
#ifdef BSP_USING_UART
     __os_hw_console_output_uart(str);
#endif

#ifdef BSP_USING_LPUART
    __os_hw_console_output_lpuart(str);
#endif
}
#endif
