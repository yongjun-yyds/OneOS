/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_can.c
 *
 * @brief       This file implements CAN driver for cm32.
 *
 * @revision
 * Date         Author          Notes
 * 2022-07-28   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include <can.h>
#include <bus.h>
#include <os_list.h>

#include <drv_can.h>

#define CAN_TXMAILBOX_0 ((uint8_t)0x00)
#define CAN_TXMAILBOX_1 ((uint8_t)0x01)
#define CAN_TXMAILBOX_2 ((uint8_t)0x02)

#define CAN_FLAGS_ESTS    ((uint32_t)0x00F00000)
#define CAN_BTR_CALCULATE ((uint32_t)4500000)

struct cm32_can
{
    struct os_can_device can;

    struct cm32_can_info *can_info;

    CAN_FilterInitType filter_init_structure;

    struct os_can_msg *rx_msg;
};

static struct cm32_can *gs_cm32_can = OS_NULL;

static os_err_t cm32_can_config(struct os_can_device *can, struct can_configure *cfg)
{
    struct cm32_can *cm_can;
    CAN_InitType     CAN_InitStructure;

    OS_ASSERT(can);
    OS_ASSERT(cfg);

    cm_can = (struct cm32_can *)can;

    CAN_InitStruct(&CAN_InitStructure);

    switch (cfg->mode)
    {
    case OS_CAN_MODE_NORMAL:
        CAN_InitStructure.OperatingMode = CAN_Normal_Mode;
        break;
    case OS_CAN_MODE_LISEN:
        CAN_InitStructure.OperatingMode = CAN_Silent_Mode;
        break;
    case OS_CAN_MODE_LOOPBACK:
        CAN_InitStructure.OperatingMode = CAN_LoopBack_Mode;
        break;
    case OS_CAN_MODE_LOOPBACKANLISEN:
        CAN_InitStructure.OperatingMode = CAN_Silent_LoopBack_Mode;
        break;
    }

    CAN_InitStructure.RSJW = CAN_RSJW_1tq;
    CAN_InitStructure.TBS2 = CAN_TBS2_2tq;

    switch (cfg->baud_rate)
    {
    case CAN10kBaud:
    case CAN20kBaud:
    case CAN50kBaud:
    case CAN100kBaud:
    case CAN125kBaud:
    case CAN250kBaud:
    case CAN500kBaud:
        /*set this param according to Demo for SDK_0.7.1*/
        CAN_InitStructure.TBS1              = CAN_TBS1_3tq;
        CAN_InitStructure.BaudRatePrescaler = (uint32_t)(CAN_BTR_CALCULATE / cfg->baud_rate);
        break;
    case CAN1MBaud:
        /*set this param according to Demo for SDK_1.3.0*/
        CAN_InitStructure.TBS1              = CAN_TBS1_6tq;
        CAN_InitStructure.BaudRatePrescaler = 3;
        break;
    default:
        os_kprintf("Please select the CAN Baudrate between 10Kbit/s to 1Mbit/s.\r\n");
    }

    if (CAN_Init(cm_can->can_info->can_instance, &CAN_InitStructure) == 0)
    {
        os_kprintf("%s init failed.\r\n", device_name(&can->parent));
        return OS_FAILURE;
    }

    CAN_InitFilter(&cm_can->filter_init_structure);

    return OS_SUCCESS;
}

static os_err_t cm32_can_control(struct os_can_device *can, int cmd, void *arg)
{
    uint32_t                  argval;
    struct cm32_can             *cm_can;
    struct os_can_filter_config *filter_cfg;
    struct cm32_can_info        *can_info;

    OS_ASSERT(can != OS_NULL);

    cm_can   = (struct cm32_can *)can;
    can_info = cm_can->can_info;

    switch (cmd)
    {
    case OS_CAN_CMD_SET_FILTER:
        if (OS_NULL == arg)
        {
            CAN_InitFilter(&cm_can->filter_init_structure);
        }
        else
        {
            filter_cfg = (struct os_can_filter_config *)arg;
            for (int i = 0; i < filter_cfg->count; i++)
            {
                cm_can->filter_init_structure.Filter_Num   = filter_cfg->items[i].hdr;
                cm_can->filter_init_structure.Filter_Mode  = filter_cfg->items[i].mode;
                cm_can->filter_init_structure.Filter_Scale = CAN_Filter_32bitScale;
                cm_can->filter_init_structure.Filter_HighId =
                    ((filter_cfg->items[i].id & 0x7ff) << 5) | (filter_cfg->items[i].id >> 24);
                cm_can->filter_init_structure.Filter_LowId =
                    ((filter_cfg->items[i].id >> 11) | (filter_cfg->items[i].ide << 2) |
                     (filter_cfg->items[i].rtr << 1) & 0xffff);
                cm_can->filter_init_structure.FilterMask_HighId     = (filter_cfg->items[i].mask >> 16) & 0xFFFF;
                cm_can->filter_init_structure.FilterMask_LowId      = filter_cfg->items[i].mask & 0xFFFF;
                cm_can->filter_init_structure.Filter_FIFOAssignment = CAN_FIFO0;
                cm_can->filter_init_structure.Filter_Act            = ENABLE;

                CAN_InitFilter(&cm_can->filter_init_structure);
            }
        }
        break;
    case OS_CAN_CMD_SET_MODE:
        argval = (uint32_t)arg;
        if (argval != OS_CAN_MODE_NORMAL && argval != OS_CAN_MODE_LISEN && argval != OS_CAN_MODE_LOOPBACK &&
            argval != OS_CAN_MODE_LOOPBACKANLISEN)
        {
            return OS_FAILURE;
        }
        if (argval != cm_can->can.config.mode)
        {
            can->config.mode = argval;
            return cm32_can_config(can, &can->config);
        }
        break;
    case OS_CAN_CMD_SET_BAUD:
        argval = (uint32_t)arg;
        if (argval != can->config.baud_rate)
        {
            can->config.baud_rate = argval;
            return cm32_can_config(can, &can->config);
        }
        break;
    case OS_CAN_CMD_SET_PRIV:
        argval = (uint32_t)arg;
        if (argval != OS_CAN_MODE_PRIV && argval != OS_CAN_MODE_NOPRIV)
        {
            return OS_FAILURE;
        }
        if (argval != can->config.privmode)
        {
            can->config.privmode = argval;
            return cm32_can_config(can, &can->config);
        }
        break;
    case OS_CAN_CMD_GET_STATUS:
    {
        can->status.rcverrcnt   = CAN_GetReceiveErrCounter(can_info->can_instance);
        can->status.snderrcnt   = CAN_GetLSBTransmitErrCounter(can_info->can_instance);
        can->status.lasterrtype = 0;
        can->status.errcode     = CAN_GetLastErrCode(can_info->can_instance);

        memcpy(arg, &can->status, sizeof(can->status));
    }
    break;
    }

    return OS_SUCCESS;
}

static int cm32_can_recvmsg(struct os_can_device *can, struct os_can_msg *pmsg, uint32_t fifo)
{
    struct cm32_can      *cm_can;
    struct cm32_can_info *can_info;

    CanRxMessage CAN_RxMessage = {0};

    OS_ASSERT(can);
    cm_can   = (struct cm32_can *)can;
    can_info = cm_can->can_info;

    /* get data */
    CAN_ReceiveMessage(can_info->can_instance, fifo, &CAN_RxMessage);

    /* get id */
    if (CAN_ID_STD == CAN_RxMessage.IDE)
    {
        pmsg->ide = OS_CAN_STDID;
        pmsg->id  = CAN_RxMessage.StdId;
    }
    else
    {
        pmsg->ide = OS_CAN_EXTID;
        pmsg->id  = CAN_RxMessage.ExtId;
    }
    /* get type */
    if (OS_CAN_DTR == CAN_RxMessage.RTR)
    {
        pmsg->rtr = OS_CAN_DTR;
    }
    else
    {
        pmsg->rtr = OS_CAN_RTR;
    }
    /* get len */
    pmsg->len = CAN_RxMessage.DLC;
    /* get hdr */
    pmsg->hdr = CAN_RxMessage.FMI;
    memcpy(pmsg->data, CAN_RxMessage.Data, CAN_RxMessage.DLC);

    return OS_SUCCESS;
}

static int cm32_can_start_send(struct os_can_device *can, const struct os_can_msg *pmsg)
{
    struct cm32_can      *cm_can;
    struct cm32_can_info *can_info;
    uint8_t            ret;

    CanTxMessage TxMessage = {0};

    OS_ASSERT(can);
    cm_can   = (struct cm32_can *)can;
    can_info = cm_can->can_info;

    if (OS_CAN_STDID == pmsg->ide)
    {
        TxMessage.IDE   = CAN_ID_STD;
        TxMessage.StdId = pmsg->id;
    }
    else
    {
        TxMessage.IDE   = CAN_ID_EXT;
        TxMessage.ExtId = pmsg->id;
    }

    if (OS_CAN_DTR == pmsg->rtr)
    {
        TxMessage.RTR = CAN_RTRQ_DATA;
    }
    else
    {
        TxMessage.RTR = CAN_RTRQ_REMOTE;
    }

    TxMessage.DLC = pmsg->len;

    memcpy(TxMessage.Data, pmsg->data, pmsg->len);

    ret = CAN_TransmitMessage(can_info->can_instance, &TxMessage);
    if (ret == CAN_TxSTS_NoMailBox)
    {
        os_kprintf("%s hal send failed.\r\n", device_name(&can->parent), ret);
        return -1;
    }

    CAN_INTConfig(can_info->can_instance, CAN_INT_TME, ENABLE);
    CAN_INTConfig(can_info->can_instance, CAN_INT_ERR, ENABLE);

    return 0;
}

static int cm32_can_stop_send(struct os_can_device *can)
{
    struct cm32_can      *cm_can;
    struct cm32_can_info *can_info;

    OS_ASSERT(can);
    cm_can   = (struct cm32_can *)can;
    can_info = cm_can->can_info;

    CAN_INTConfig(can_info->can_instance, CAN_INT_TME, DISABLE);
    CAN_INTConfig(can_info->can_instance, CAN_INT_ERR, DISABLE);

    return 0;
}

static int cm32_can_start_recv(struct os_can_device *can, struct os_can_msg *msg)
{
    struct cm32_can      *cm_can;
    struct cm32_can_info *can_info;

    OS_ASSERT(can);

    cm_can   = (struct cm32_can *)can;
    can_info = cm_can->can_info;

    cm_can->rx_msg = msg;

    CAN_INTConfig(can_info->can_instance, CAN_INT_FMP0 | CAN_INT_FF0 | CAN_INT_FOV0, ENABLE);
    CAN_INTConfig(can_info->can_instance, CAN_INT_FMP1 | CAN_INT_FF1 | CAN_INT_FOV1, ENABLE);

    return 0;
}

static int cm32_can_stop_recv(struct os_can_device *can)
{
    struct cm32_can      *cm_can;
    struct cm32_can_info *can_info;

    OS_ASSERT(can);

    cm_can   = (struct cm32_can *)can;
    can_info = cm_can->can_info;

    CAN_INTConfig(can_info->can_instance, CAN_INT_FMP0 | CAN_INT_FF0 | CAN_INT_FOV0, DISABLE);
    CAN_INTConfig(can_info->can_instance, CAN_INT_FMP1 | CAN_INT_FF1 | CAN_INT_FOV1, DISABLE);

    return 0;
}

static int cm32_can_recv_state(struct os_can_device *can)
{
    return 0;
}

void CAN_TX_IRQHandler(void)
{
    struct cm32_can *cm_can;

    cm_can = gs_cm32_can;

    CAN_INTConfig(CAN, CAN_INT_TME, DISABLE);

    if (CAN_TransmitSTS(CAN, CAN_TXMAILBOX_0) == CANTXSTSOK || CAN_TransmitSTS(CAN, CAN_TXMAILBOX_1) == CANTXSTSOK ||
        CAN_TransmitSTS(CAN, CAN_TXMAILBOX_2) == CANTXSTSOK)
    {
        CAN_ClearINTPendingBit(CAN, CAN_INT_TME);
        os_hw_can_isr_txdone((struct os_can_device *)cm_can, OS_CAN_EVENT_TX_DONE);
    }
    else
    {
        CAN_ClearINTPendingBit(CAN, CAN_INT_TME);
        os_hw_can_isr_txdone((struct os_can_device *)cm_can, OS_CAN_EVENT_TX_FAIL);
    }
}

void CAN_RX0_IRQHandler(void)
{
    struct cm32_can      *cm_can;
    struct cm32_can_info *can_info;

    cm_can   = gs_cm32_can;
    can_info = cm_can->can_info;

    CAN_ClearINTPendingBit(can_info->can_instance, CAN_INT_FF0);
    CAN_ClearINTPendingBit(can_info->can_instance, CAN_INT_FOV0);

    CAN_INTConfig(can_info->can_instance, CAN_INT_FMP0 | CAN_INT_FF0 | CAN_INT_FOV0, DISABLE);

    if (cm_can->rx_msg == OS_NULL)
        return;

    cm32_can_recvmsg(&cm_can->can, cm_can->rx_msg, CAN_FIFO0);
    cm_can->rx_msg = OS_NULL;

    os_hw_can_isr_rxdone((struct os_can_device *)cm_can);
}

void CAN_RX1_IRQHandler(void)
{
    struct cm32_can      *cm_can;
    struct cm32_can_info *can_info;

    cm_can   = gs_cm32_can;
    can_info = cm_can->can_info;

    CAN_ClearINTPendingBit(can_info->can_instance, CAN_INT_FF1);
    CAN_ClearINTPendingBit(can_info->can_instance, CAN_INT_FOV1);

    CAN_INTConfig(can_info->can_instance, CAN_INT_FMP1 | CAN_INT_FF1 | CAN_INT_FOV1, DISABLE);

    if (cm_can->rx_msg == OS_NULL)
        return;

    cm32_can_recvmsg(&cm_can->can, cm_can->rx_msg, CAN_FIFO1);
    cm_can->rx_msg = OS_NULL;

    os_hw_can_isr_rxdone((struct os_can_device *)cm_can);
}

void CAN_SCE_IRQHandler(void)
{
    struct cm32_can      *cm_can;
    struct cm32_can_info *can_info;

    cm_can   = gs_cm32_can;
    can_info = cm_can->can_info;

    if (CAN_GetFlagSTS(can_info->can_instance, CAN_FLAGS_ESTS))
    {
        CAN_ClearFlag(can_info->can_instance, CAN_FLAG_LEC);
    }
}

void can_nvic_config(IRQn_Type irq)
{
    NVIC_InitType NVIC_InitStructure;

    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_1);

    NVIC_InitStructure.NVIC_IRQChannel                   = irq;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority        = 0x0;
    NVIC_InitStructure.NVIC_IRQChannelCmd                = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}

static const struct os_can_ops cm32_can_ops = {
    .configure  = cm32_can_config,
    .control    = cm32_can_control,
    .start_send = cm32_can_start_send,
    .stop_send  = cm32_can_stop_send,
    .start_recv = cm32_can_start_recv,
    .stop_recv  = cm32_can_stop_recv,
    .recv_state = cm32_can_recv_state,
};

static int cm32_can_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    struct can_configure config = CANDEFAULTCONFIG;
    struct cm32_can     *cm_can = os_calloc(1, sizeof(struct cm32_can));

    OS_ASSERT(cm_can);

    cm_can->can_info = (struct cm32_can_info *)dev->info;

    config.privmode = OS_CAN_MODE_NOPRIV;
    config.ticks    = 50;

#ifdef OS_CAN_USING_HDR
    config.maxhdr = 14;
#endif

    cm_can->can.config = config;

    gs_cm32_can = cm_can;

    /*config NVIC*/
    can_nvic_config(CAN_TX_IRQn);
    can_nvic_config(CAN_RX0_IRQn);
    can_nvic_config(CAN_RX1_IRQn);
    can_nvic_config(CAN_SCE_IRQn);
    /*config GPIO*/
    RCC_EnableAPB2PeriphClk(cm_can->can_info->gpio_rcc, ENABLE);
    GPIO_InitPeripheral(cm_can->can_info->gpio, &cm_can->can_info->gpio_init_structure_rx);
    GPIO_InitPeripheral(cm_can->can_info->gpio, &cm_can->can_info->gpio_init_structure_tx);

    RCC_EnableAPB1PeriphClk(RCC_APB1_PERIPH_CAN, ENABLE);
    CAN_DeInit(cm_can->can_info->can_instance);

    /* config Default filter */
    cm_can->filter_init_structure.Filter_HighId         = 0x0000;
    cm_can->filter_init_structure.Filter_LowId          = 0x0000;
    cm_can->filter_init_structure.FilterMask_HighId     = 0x0000;
    cm_can->filter_init_structure.FilterMask_LowId      = 0x0000;
    cm_can->filter_init_structure.Filter_FIFOAssignment = CAN_FIFO0;
    cm_can->filter_init_structure.Filter_Num            = 0;
    cm_can->filter_init_structure.Filter_Mode           = CAN_Filter_IdMaskMode;
    cm_can->filter_init_structure.Filter_Scale          = CAN_Filter_32bitScale;
    cm_can->filter_init_structure.Filter_Act            = ENABLE;

    os_hw_can_register(&cm_can->can, dev->name, &cm32_can_ops, cm_can);

    return OS_SUCCESS;
}

OS_DRIVER_INFO cm32_can_driver = {
    .name  = "CAN_HandleTypeDef",
    .probe = cm32_can_probe,
};

OS_DRIVER_DEFINE(cm32_can_driver, OS_INIT_LEVEL_PRE_DEVICE, OS_INIT_SUBLEVEL_LOW);

