/**
 ***********************************************************************************************************************
 * Copyright (c) 2022, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_flash.c
 *
 * @brief       This file provides flash init/read/write/erase functions for cm32.
 *
 * @revision
 * Date         Author          Notes
 * 2022-01-22   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include "board.h"
#include <os_memory.h>
#include <string.h>

#ifdef BSP_USING_ON_CHIP_FLASH
#include "drv_flash.h"

#if defined(OS_USING_FAL)
#include "fal.h"
#endif

#include <cm_qspi.h>
#include <cm_sysctrl.h>
/**
 * Read data from flash.
 * @note This operation's units is word.
 *
 * @param addr flash address
 * @param buf buffer to store read data
 * @param size read bytes size
 *
 * @return result
 */

int cm32_flash_read(uint32_t addr, uint8_t *buf, os_size_t size)
{
    if ((addr + size) > CM32_FLASH_END_ADDRESS)
    {
        os_kprintf("read outrange flash size! addr is (0x%p)", (void *)(addr + size));
        return OS_INVAL;
    }

    QSPI_ReadFlashData(addr, size, buf);

    return size;
}

/**
 * Write data to flash.
 * @note This operation's units is word.
 * @note This operation must after erase. @see flash_erase.
 *
 * @param addr flash address
 * @param buf the write data buffer
 * @param size write bytes size
 *
 * @return result
 */

int cm32_flash_write(uint32_t addr, const uint8_t *buf, os_size_t size)
{
    uint8_t result = 0;

    if ((addr + size) > CM32_FLASH_END_ADDRESS)
    {
        os_kprintf("ERROR: write outrange flash size! addr is (0x%p)\n", (void *)(addr + size));
        return OS_INVAL;
    }

    if (addr % 4 != 0)
    {
        os_kprintf("write addr must be 4-byte alignment\n");
        return OS_INVAL;
    }

    if (size < 1)
    {
        return OS_FAILURE;
    }

    result = QSPI_WriteFlashData(addr, size, (uint8_t *)buf);
    if (result == ERROR)
    {
        return OS_FAILURE;
    }

    return size;
}

/**
 * Erase data on flash.
 * @note This operation is irreversible.
 * @note This operation's units is different which on many chips.
 *
 * @param addr flash address
 * @param size erase bytes size
 *
 * @return result
 */
int cm32_flash_erase(uint32_t addr, os_size_t size)
{
    uint16_t sector_start = 0;
    uint16_t sector_end   = 0;
    uint16_t sector_cnt   = 0;

    if ((addr + size) > CM32_FLASH_END_ADDRESS)
    {
        os_kprintf("ERROR: erase outrange flash size! addr is (0x%p)\n", (void *)(addr + size));
        return OS_INVAL;
    }

    addr         = addr - addr % CM32_SECTOR_SIZE;
    sector_start = addr / CM32_SECTOR_SIZE;
    sector_end   = (addr + size) / CM32_SECTOR_SIZE + (((addr + size) % CM32_SECTOR_SIZE) ? 1 : 0);

    for (sector_cnt = 0; sector_cnt < (sector_end - sector_start); sector_cnt++)
    {
        QSPI_SectorEraseFlash((sector_start + sector_cnt) * CM32_SECTOR_SIZE);
    }

    return size;
}

#if defined(OS_USING_FAL)

static int cm32_flash_init(fal_flash_t *flash)
{
    return 0;
}

#include "fal_drv_flash.c"

#endif
#endif /* BSP_USING_ON_CHIP_FLASH */
