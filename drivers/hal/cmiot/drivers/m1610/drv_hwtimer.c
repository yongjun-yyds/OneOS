/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_hwtimer.c
 *
 * @brief       This file implements timer driver for cm32.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <board.h>
#include <os_memory.h>
#include <timer/timer.h>
#include <timer/clocksource.h>

#ifdef OS_USING_CLOCKEVENT
#include <timer/clockevent.h>
#endif

#include <cm_nvic.h>
#include <drv_hwtimer.h>

struct cm32_timer
{
    union _clock
    {
        os_clocksource_t cs;
        os_clockevent_t  ce;
    } clock;

    struct cm32_timer_info *info;

    uint32_t    freq;
    os_list_node_t list;
};

static os_list_node_t cm32_timer_list = OS_LIST_INIT(cm32_timer_list);

void cm32_timer_irq_handle(uint32_t htimer)
{
    struct cm32_timer *timer;

    os_list_for_each_entry(timer, &cm32_timer_list, struct cm32_timer, list)
    {
        if (timer->info->htimer == htimer)
        {
            os_clockevent_isr(&timer->clock.ce);
            break;
        }
    }
}

void TIMER0_IRQHandler(void)
{
    cm32_timer_irq_handle(TIM0);
}

void TIMER1_IRQHandler(void)
{
    cm32_timer_irq_handle(TIM1);
}

void TIMER2_IRQHandler(void)
{
    cm32_timer_irq_handle(TIM2);
}

void TIMER3_IRQHandler(void)
{
    cm32_timer_irq_handle(TIM3);
}

void TIMER4_IRQHandler(void)
{
    cm32_timer_irq_handle(TIM4);
}

void TIMER5_IRQHandler(void)
{
    cm32_timer_irq_handle(TIM5);
}

void TIMER6_IRQHandler(void)
{
    cm32_timer_irq_handle(TIM6);
}

void TIMER7_IRQHandler(void)
{
    cm32_timer_irq_handle(TIM7);
}

static void cm32_timer_start(os_clockevent_t *ce, uint32_t prescaler, uint64_t count)
{
    struct cm32_timer *timer;
    TIM_InitTypeDef    TIM_InitStruct;

    OS_ASSERT(prescaler != 0);
    OS_ASSERT(count != 0);

    timer = (struct cm32_timer *)ce;

    TIM_InitStruct.TIMx           = timer->info->htimer;
    TIM_InitStruct.period         = count;
    TIM_InitStruct.module_clk_div = timer->info->clk_div;
    TIM_Init(&TIM_InitStruct);
    TIM_Cmd(timer->info->htimer, ENABLE);

    NVIC_SetPriority(timer->info->irqno, 0);
    NVIC_EnableIRQ(timer->info->irqno);
}

static void cm32_timer_stop(os_clockevent_t *ce)
{
    struct cm32_timer *timer;

    OS_ASSERT(ce != OS_NULL);
    timer = (struct cm32_timer *)ce;

    NVIC_DisableIRQ(timer->info->irqno);
    TIM_DeInit(timer->info->htimer);
}

static uint64_t cm32_timer_read(void *clock)
{
    /*
        struct cm32_timer *timer;

        timer = (struct cm32_timer *)clock;
        return Bt_M0_Cnt32Get(timer->unit);
    */

    return 10;
}

static const struct os_clockevent_ops cm32_tim_ops = {
    .start = cm32_timer_start,
    .stop  = cm32_timer_stop,
    .read  = cm32_timer_read,
};

static int cm32_timer_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    struct cm32_timer      *timer;
    struct cm32_timer_info *info;

    timer = os_calloc(1, sizeof(struct cm32_timer));
    OS_ASSERT(timer);

    info = (struct cm32_timer_info *)dev->info;

    timer->info = info;
    timer->freq = TIM_getModuleClk() >> info->clk_div;

    timer->clock.ce.rating = 160;
    timer->clock.ce.freq   = timer->freq;
    timer->clock.ce.mask   = 0xffffull;

    timer->clock.ce.prescaler_mask = 1;
    timer->clock.ce.prescaler_bits = 1;

    timer->clock.ce.count_mask = 0xfffful;
    timer->clock.ce.count_bits = 16;

    timer->clock.ce.min_nsec = NSEC_PER_SEC / timer->clock.ce.freq;
    timer->clock.ce.feature  = OS_CLOCKEVENT_FEATURE_PERIOD;

    timer->clock.ce.ops = &cm32_tim_ops;

    os_clockevent_register(dev->name, &timer->clock.ce);

    os_list_add(&cm32_timer_list, &timer->list);

    return OS_SUCCESS;
}

OS_DRIVER_INFO cm32_tim_driver = {
    .name  = "TIMER_Type",
    .probe = cm32_timer_probe,
};

OS_DRIVER_DEFINE(cm32_tim_driver, OS_INIT_LEVEL_PRE_DEVICE, OS_INIT_SUBLEVEL_MIDDLE);

