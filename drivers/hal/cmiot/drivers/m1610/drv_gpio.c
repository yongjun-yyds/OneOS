/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_gpio.c
 *
 * @brief       This file implements gpio driver for cm32.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <os_assert.h>
#include <cm_exti.h>
#include <cm_nvic.h>

#include <driver.h>
#include <pin/pin.h>
#include <drv_gpio.h>

struct pin_index
{
    GPIO_TypeDef gpio_port;
    uint32_t  gpio_pin;
    int8_t    irq_enable;
};

/* clang-format off */
static struct pin_index pin_index_tbl[] = {
#if defined(GPIO_PORTA)
    PIN_INDEX_INI(A, GPIO_Pin_0),
    PIN_INDEX_INI(A, GPIO_Pin_1),
    PIN_INDEX_INI(A, GPIO_Pin_2),
    PIN_INDEX_INI(A, GPIO_Pin_3),
    PIN_INDEX_INI(A, GPIO_Pin_4),
    PIN_INDEX_INI(A, GPIO_Pin_5),
    PIN_INDEX_INI(A, GPIO_Pin_6),
    PIN_INDEX_INI(A, GPIO_Pin_7),
#if defined(GPIO_PORTB)
    PIN_INDEX_INI(B, GPIO_Pin_0),
    PIN_INDEX_INI(B, GPIO_Pin_1),
    PIN_INDEX_INI(B, GPIO_Pin_2),
    PIN_INDEX_INI(B, GPIO_Pin_3),
    PIN_INDEX_INI(B, GPIO_Pin_4),
    PIN_INDEX_INI(B, GPIO_Pin_5),
    PIN_INDEX_INI(B, GPIO_Pin_6),
    PIN_INDEX_INI(B, GPIO_Pin_7),
#if defined(GPIO_PORTC)
    PIN_INDEX_INI(C, GPIO_Pin_0),
    PIN_INDEX_INI(C, GPIO_Pin_1),
    PIN_INDEX_INI(C, GPIO_Pin_2),
    PIN_INDEX_INI(C, GPIO_Pin_3),
    PIN_INDEX_INI(C, GPIO_Pin_4),
    PIN_INDEX_INI(C, GPIO_Pin_5),
    PIN_INDEX_INI(C, GPIO_Pin_6),
    PIN_INDEX_INI(C, GPIO_Pin_7),
#if defined(GPIO_PORTD)
    PIN_INDEX_INI(D, GPIO_Pin_0),
    PIN_INDEX_INI(D, GPIO_Pin_1),
    PIN_INDEX_INI(D, GPIO_Pin_2),
    PIN_INDEX_INI(D, GPIO_Pin_3),
    PIN_INDEX_INI(D, GPIO_Pin_4),
    PIN_INDEX_INI(D, GPIO_Pin_5),
    PIN_INDEX_INI(D, GPIO_Pin_6),
    PIN_INDEX_INI(D, GPIO_Pin_7),
#endif /* defined(GPIO_PORTA) */
#endif /* defined(GPIO_PORTB) */
#endif /* defined(GPIO_PORTC) */
#endif /* defined(GPIO_PORTD) */
};
/* clang-format on */

static struct pin_index *get_pin_index(os_base_t pin)
{
    struct pin_index *index = OS_NULL;

    if (pin < GPIO_PIN_MAX)
    {
        index = &pin_index_tbl[pin];
    }

    return index;
}

static void cm32_pin_mode(struct os_device *dev, os_base_t pin, os_base_t mode)
{
    struct pin_index *index;
    GPIO_InitTypeDef  GPIO_InitStruct;

    index = get_pin_index(pin);
    if (index == OS_NULL)
    {
        return;
    }

    switch (mode)
    {
    case PIN_MODE_OUTPUT:
        GPIO_InitStruct.GPIO_Mode = GPIO_Mode_Out_PP;
        break;

    case PIN_MODE_INPUT:
        GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN_FLOATING;
        break;

    case PIN_MODE_INPUT_PULLUP:
        GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IPU;
        break;

    case PIN_MODE_INPUT_PULLDOWN:
        GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IPD;
        break;

    default:
        return;
    }

    GPIO_InitStruct.GPIO_Pin = index->gpio_pin;
    GPIO_Init(index->gpio_port, &GPIO_InitStruct);
}

static void cm32_pin_write(struct os_device *dev, os_base_t pin, os_base_t value)
{
    struct pin_index *index;

    index = get_pin_index(pin);
    if (index == OS_NULL)
    {
        return;
    }

    if (PIN_HIGH == value)
    {
        GPIO_SetBits(index->gpio_port, index->gpio_pin);
    }
    else
    {
        GPIO_ResetBits(index->gpio_port, index->gpio_pin);
    }
}

static int cm32_pin_read(struct os_device *dev, os_base_t pin)
{
    int                     value;
    const struct pin_index *index;

    value = PIN_LOW;

    index = get_pin_index(pin);
    if (index == OS_NULL)
    {
        return value;
    }

    value = GPIO_ReadDataBit(index->gpio_port, index->gpio_pin);

    return value;
}

/* clang-format off */
static struct os_pin_irq_hdr pin_irq_hdr_tab[] = {
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL},
};
/* clang-format on */

static os_err_t
cm32_pin_attach_irq(struct os_device *device, int32_t pin, uint32_t mode, void (*hdr)(void *args), void *args)
{
    struct pin_index *index;
    os_ubase_t         level;

    index = get_pin_index(pin);
    if (index == OS_NULL)
    {
        return OS_NOSYS;
    }

    os_spin_lock_irqsave(&gs_device_lock, &level);

    if (pin_irq_hdr_tab[pin].pin == pin && pin_irq_hdr_tab[pin].hdr == hdr && pin_irq_hdr_tab[pin].mode == mode &&
        pin_irq_hdr_tab[pin].args == args)
    {
        os_spin_unlock_irqrestore(&gs_device_lock, level);
        return OS_SUCCESS;
    }

    if (pin_irq_hdr_tab[pin].pin != -1)
    {
        os_spin_unlock_irqrestore(&gs_device_lock, level);
        return OS_BUSY;
    }

    pin_irq_hdr_tab[pin].pin  = pin;
    pin_irq_hdr_tab[pin].hdr  = hdr;
    pin_irq_hdr_tab[pin].mode = mode;
    pin_irq_hdr_tab[pin].args = args;

    os_spin_unlock_irqrestore(&gs_device_lock, level);

    return OS_SUCCESS;
}

static os_err_t cm32_pin_dettach_irq(struct os_device *device, int32_t pin)
{
    struct pin_index *index;
    os_ubase_t         level;

    index = get_pin_index(pin);
    if (index == OS_NULL)
    {
        return OS_NOSYS;
    }

    os_spin_lock_irqsave(&gs_device_lock, &level);
    if (pin_irq_hdr_tab[pin].pin == -1)
    {
        os_spin_unlock_irqrestore(&gs_device_lock, level);
        return OS_SUCCESS;
    }

    if (index->irq_enable == PIN_IRQ_ENABLE)
    {
        return OS_NOSYS;
    }

    pin_irq_hdr_tab[pin].pin  = -1;
    pin_irq_hdr_tab[pin].hdr  = OS_NULL;
    pin_irq_hdr_tab[pin].mode = 0;
    pin_irq_hdr_tab[pin].args = OS_NULL;

    os_spin_unlock_irqrestore(&gs_device_lock, level);

    return OS_SUCCESS;
}

static os_err_t cm32_pin_irq_enable(struct os_device *device, os_base_t pin, uint32_t enabled)
{
    struct pin_index *index;
    os_ubase_t         level;
    uint8_t        EXTI_Trigger_mode;

    index = get_pin_index(pin);
    if (index == OS_NULL)
    {
        return OS_NOSYS;
    }

    if (enabled == PIN_IRQ_ENABLE)
    {
        os_spin_lock_irqsave(&gs_device_lock, &level);

        if (pin_irq_hdr_tab[pin].pin == -1)
        {
            os_spin_unlock_irqrestore(&gs_device_lock, level);
            return OS_NOSYS;
        }

        switch (pin_irq_hdr_tab[pin].mode)
        {
        case PIN_IRQ_MODE_RISING:
            EXTI_Trigger_mode = EXTI_Trigger_Rising;
            break;
        case PIN_IRQ_MODE_FALLING:
            EXTI_Trigger_mode = EXTI_Trigger_Falling;
            break;
        case PIN_IRQ_MODE_RISING_FALLING:
            EXTI_Trigger_mode = EXTI_Trigger_Rising_Falling;
            break;
        default:
            os_spin_unlock_irqrestore(&gs_device_lock, level);
            return OS_FAILURE;
        }

        EXTI_LineConfig(index->gpio_port, index->gpio_pin, (EXTI_TriggerTypeDef)EXTI_Trigger_mode);

        index->irq_enable = PIN_IRQ_ENABLE;

        os_spin_unlock_irqrestore(&gs_device_lock, level);
    }
    else if (enabled == PIN_IRQ_DISABLE)
    {
        os_spin_lock_irqsave(&gs_device_lock, &level);

        if (pin_irq_hdr_tab[pin].pin == -1)
        {
            os_spin_unlock_irqrestore(&gs_device_lock, level);
            return OS_NOSYS;
        }

        EXTI_Trigger_mode = EXTI_Trigger_Off;
        EXTI_LineConfig(index->gpio_port, index->gpio_pin, (EXTI_TriggerTypeDef)EXTI_Trigger_mode);

        index->irq_enable = PIN_IRQ_DISABLE;

        os_spin_unlock_irqrestore(&gs_device_lock, level);
    }
    else
    {
        return OS_NOSYS;
    }

    return OS_SUCCESS;
}

void pin_irq_hdr(uint32_t exti_line)
{
    uint8_t pin = EXTI_GetITLineStatus(exti_line);
    int     i   = 0, index;

    for (i = 0; i < GPIO_PIN_NUM; ++i)
    {
        if (pin & ((uint8_t)1 << i))
        {
            index = (exti_line * 8 + i);
            if (pin_irq_hdr_tab[index].hdr && pin_index_tbl[index].irq_enable == PIN_IRQ_ENABLE)
            {
                pin_irq_hdr_tab[index].hdr(pin_irq_hdr_tab[index].args);
            }
        }
    }
}

void EXTI0_IRQHandler(void)
{
    pin_irq_hdr(EXTI_Line0);
}

void EXTI1_IRQHandler(void)
{
    pin_irq_hdr(EXTI_Line1);
}

void EXTI2_IRQHandler(void)
{
    pin_irq_hdr(EXTI_Line2);
}

void EXTI3_IRQHandler(void)
{
    pin_irq_hdr(EXTI_Line3);
}

const static struct os_pin_ops _cm32_pin_ops = {
    cm32_pin_mode,
    cm32_pin_write,
    cm32_pin_read,
    cm32_pin_attach_irq,
    cm32_pin_dettach_irq,
    cm32_pin_irq_enable,
};

/**
 ***********************************************************************************************************************
 * @brief           os_hw_pin_init:enable gpio clk,register pin device.
 *
 * @param[in]       none
 *
 * @return          Return init result.
 * @retval          OS_SUCCESS       init success.
 * @retval          Others       init failed.
 ***********************************************************************************************************************
 */
int os_hw_pin_init(void)
{
    EXTI_DeInit();

    NVIC_SetPriority(gpio_handler_IRQn, 1);
    NVIC_EnableIRQ(gpio_handler_IRQn);

    return os_device_pin_register(0, &_cm32_pin_ops, OS_NULL);
}
