/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_gpio.h
 *
 * @brief       This file provides struct/macro declaration and functions declaration for STM32 gpio driver.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __DRV_GPIO_H__
#define __DRV_GPIO_H__

#include <drv_common.h>
#include "board.h"
#include "drv/gpio.h"
#include "drv/gpio_pin.h"
#include "drv/pin.h"
#include "dw_gpio_ll.h"

#define IRQ_ENABLE  1
#define IRQ_DISABLE 0

enum GPIO_PORT_INDEX
{
    GPIO_INDEX_A = 0,
    GPIO_INDEX_MAX
};

#define GPIO_PIN_PER_PORT (32)
#define GPIO_PORT_MAX     (GPIO_INDEX_MAX)
#define GPIO_PIN_MAX      (GPIO_PORT_MAX * GPIO_PIN_PER_PORT)

#define __PORT_INDEX(pin) (pin / GPIO_PIN_PER_PORT)
#define __PIN_INDEX(pin)  (pin % GPIO_PIN_PER_PORT)

#define GET_PIN(PORTx, PIN) (P##PORTx##PIN)
#define PIN_BASE(__pin)     get_pin_base(__pin)
#define PIN_OFFSET(__pin)   (1 << ((__pin) % GPIO_PIN_PER_PORT))

struct pin_pull_state
{
    int8_t pull_state;
};

static void gpio_callback(csi_gpio_t *gpio, uint32_t pins, void *arg);
int         os_hw_pin_init(void);

#endif /* __DRV_GPIO_H__ */
