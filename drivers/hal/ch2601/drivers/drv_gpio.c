/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_gpio.c
 *
 * @brief       This file implements gpio driver for stm32.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include "drv_gpio.h"
#include <pin/pin.h>
#include <driver.h>
#include <os_memory.h>

#ifdef OS_USING_PIN

#define DBG_TAG "drv_gpio"

#define ITEM_NUM(items) sizeof(items) / sizeof(items[0])

csi_gpio_pin_t        gpio_info[GPIO_PIN_MAX];
struct pin_pull_state pin_pulls[GPIO_PIN_MAX];

static os_err_t ch_pin_dettach_irq(struct os_device *device, int32_t pin);

static struct os_pin_irq_hdr pin_irq_hdr_tab[] = {
    {-1, 0, OS_NULL, OS_NULL}, {-1, 0, OS_NULL, OS_NULL}, {-1, 0, OS_NULL, OS_NULL}, {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL}, {-1, 0, OS_NULL, OS_NULL}, {-1, 0, OS_NULL, OS_NULL}, {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL}, {-1, 0, OS_NULL, OS_NULL}, {-1, 0, OS_NULL, OS_NULL}, {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL}, {-1, 0, OS_NULL, OS_NULL}, {-1, 0, OS_NULL, OS_NULL}, {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL}, {-1, 0, OS_NULL, OS_NULL}, {-1, 0, OS_NULL, OS_NULL}, {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL}, {-1, 0, OS_NULL, OS_NULL}, {-1, 0, OS_NULL, OS_NULL}, {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL}, {-1, 0, OS_NULL, OS_NULL}, {-1, 0, OS_NULL, OS_NULL}, {-1, 0, OS_NULL, OS_NULL},
    {-1, 0, OS_NULL, OS_NULL}, {-1, 0, OS_NULL, OS_NULL}, {-1, 0, OS_NULL, OS_NULL}, {-1, 0, OS_NULL, OS_NULL},
};

static uint32_t csi_output_gpio_read(csi_gpio_t *gpio, uint32_t pin_mask)
{
    CSI_PARAM_CHK(gpio, CSI_ERROR);
    CSI_PARAM_CHK(pin_mask, CSI_ERROR);

    dw_gpio_regs_t *reg = (dw_gpio_regs_t *)HANDLE_REG_BASE(gpio);
    return (dw_gpio_read_output_port(reg) & pin_mask);
}

static csi_gpio_pin_state_t csi_output_gpio_pin_read(csi_gpio_pin_t *pin)
{
    csi_gpio_pin_state_t state;

    if (csi_output_gpio_read(pin->gpio, (uint32_t)1U << pin->pin_idx) != 0U)
    {
        state = GPIO_PIN_HIGH;
    }
    else
    {
        state = GPIO_PIN_LOW;
    }

    return state;
}

static csi_gpio_dir_t csi_get_gpio_dir(csi_gpio_t *gpio, uint32_t pin_mask)
{
    CSI_PARAM_CHK(gpio, CSI_ERROR);
    CSI_PARAM_CHK(pin_mask, CSI_ERROR);

    dw_gpio_regs_t *reg = (dw_gpio_regs_t *)HANDLE_REG_BASE(gpio);
    uint32_t        tmp = dw_gpio_read_port_direction(reg);

    if (tmp & pin_mask)
        return GPIO_DIRECTION_OUTPUT;
    else
        return GPIO_DIRECTION_INPUT;
}

static csi_gpio_dir_t csi_get_gpio_pin_dir(csi_gpio_pin_t *pin)
{
    CSI_PARAM_CHK(pin, CSI_ERROR);

    return csi_get_gpio_dir(pin->gpio, (uint32_t)1U << pin->pin_idx);
}

static void ch_pin_write(struct os_device *dev, os_base_t pin, os_base_t value)
{
    csi_gpio_pin_t *pin_info = &gpio_info[pin];

    if (pin_info->gpio == OS_NULL)
    {
        return;
    }

    csi_gpio_pin_write(pin_info, value);
}

static int ch_pin_read(struct os_device *dev, os_base_t pin)
{
    int             value;
    csi_gpio_pin_t *pin_info = &gpio_info[pin];

    value = PIN_LOW;

    pin_info = &gpio_info[pin];
    if (pin_info->gpio == OS_NULL)
    {
        return value;
    }

    if (csi_get_gpio_pin_dir(pin_info) == GPIO_DIRECTION_INPUT)
        value = csi_gpio_pin_read(pin_info);
    else
        value = csi_output_gpio_pin_read(pin_info);

    return value;
}

static void ch_pin_mode(struct os_device *dev, os_base_t pin, os_base_t mode)
{
    os_ubase_t       level;
    uint8_t      state = GPIO_MODE_PULLNONE;
    csi_gpio_pin_t *pin_info;
    csi_error_t     ret = CSI_OK;

    pin_info = &gpio_info[pin];

    /* Configure GPIO_InitStructure */
    csi_pin_set_mux(pin, PIN_FUNC_GPIO);
    if (pin_info->pin_idx != pin)
        ret = csi_gpio_pin_init(pin_info, pin);
    if (ret == CSI_ERROR)
        return;

    if (mode == PIN_MODE_OUTPUT)
    {
        /* output setting */
        state = GPIO_MODE_PULLNONE;
        csi_gpio_pin_mode(pin_info, GPIO_MODE_PULLNONE);
        csi_gpio_pin_dir(pin_info, GPIO_DIRECTION_OUTPUT);
    }
    else if (mode == PIN_MODE_INPUT)
    {
        /* input setting: not pull. */
        state = GPIO_MODE_PULLNONE;
        csi_gpio_pin_mode(pin_info, GPIO_MODE_PULLNONE);
        csi_gpio_pin_dir(pin_info, GPIO_DIRECTION_INPUT);
    }
    else if (mode == PIN_MODE_INPUT_PULLUP)
    {
        /* input setting: pull up. */
        state = GPIO_MODE_PULLUP;
        csi_gpio_pin_mode(pin_info, GPIO_MODE_PULLUP);
        csi_gpio_pin_dir(pin_info, GPIO_DIRECTION_INPUT);
    }
    else if (mode == PIN_MODE_INPUT_PULLDOWN)
    {
        /* input setting: pull down. */
        state = GPIO_MODE_PULLDOWN;
        csi_gpio_pin_mode(pin_info, GPIO_MODE_PULLDOWN);
        csi_gpio_pin_dir(pin_info, GPIO_DIRECTION_INPUT);
    }
    else if (mode == PIN_MODE_OUTPUT_OD)
    {
        /* output setting: od. */
        state = GPIO_MODE_PULLNONE;
        csi_gpio_pin_mode(pin_info, GPIO_MODE_PULLNONE);
        csi_gpio_pin_dir(pin_info, GPIO_DIRECTION_OUTPUT);
    }
    else if (mode == PIN_MODE_DISABLE)
    {
        csi_gpio_pin_irq_enable(pin_info, IRQ_ENABLE);

        csi_gpio_pin_uninit(pin_info);

        return;
    }

    /* remeber the pull state. */
    os_spin_lock_irqsave(&gs_device_lock, &level);
    pin_pulls[pin].pull_state = state;
    os_spin_unlock_irqrestore(&gs_device_lock, level);
}

OS_INLINE int32_t bit2bitno(uint32_t bit)
{
    int i;
    for (i = 0; i < 32; i++)
    {
        if ((0x01 << i) == bit)
        {
            return i;
        }
    }
    return -1;
}

static os_err_t
ch_pin_attach_irq(struct os_device *device, int32_t pin, uint32_t mode, void (*hdr)(void *args), void *args)
{
    os_ubase_t       level;
    csi_gpio_pin_t *pin_info;
    csi_error_t     ret;

    pin_info = &gpio_info[pin];
    if (pin_info->pin_idx != pin)
    {
        return OS_NOSYS;
    }

    os_spin_lock_irqsave(&gs_device_lock, &level);
    if (pin_irq_hdr_tab[pin].pin == pin && pin_irq_hdr_tab[pin].hdr == hdr && pin_irq_hdr_tab[pin].mode == mode &&
        pin_irq_hdr_tab[pin].args == args)
    {
        os_spin_unlock_irqrestore(&gs_device_lock, level);
        return OS_SUCCESS;
    }
    if (pin_irq_hdr_tab[pin].pin != -1)
    {
        os_spin_unlock_irqrestore(&gs_device_lock, level);
        return OS_BUSY;
    }
    pin_irq_hdr_tab[pin].pin  = pin;
    pin_irq_hdr_tab[pin].hdr  = hdr;
    pin_irq_hdr_tab[pin].mode = mode;
    pin_irq_hdr_tab[pin].args = args;

    if (pin_irq_hdr_tab[pin].pin == -1)
    {
        os_spin_unlock_irqrestore(&gs_device_lock, level);
        return OS_NOSYS;
    }
    /* Configure GPIO_InitStructure */
    switch (pin_irq_hdr_tab[pin].mode)
    {
    case PIN_IRQ_MODE_RISING:
        csi_gpio_pin_irq_mode(pin_info, GPIO_IRQ_MODE_RISING_EDGE);
        break;
    case PIN_IRQ_MODE_FALLING:
        csi_gpio_pin_irq_mode(pin_info, GPIO_IRQ_MODE_FALLING_EDGE);
        break;
    case PIN_IRQ_MODE_HIGH_LEVEL:
        csi_gpio_pin_irq_mode(pin_info, GPIO_IRQ_MODE_HIGH_LEVEL);
        break;
    case PIN_IRQ_MODE_LOW_LEVEL:
        csi_gpio_pin_irq_mode(pin_info, GPIO_IRQ_MODE_LOW_LEVEL);
        break;
    default:
        os_spin_unlock_irqrestore(&gs_device_lock, level);
        return OS_FAILURE;
    }

    ret = csi_gpio_attach_callback(pin_info->gpio, gpio_callback, OS_NULL);

    if (ret != CSI_OK)
        return OS_FAILURE;

    os_spin_unlock_irqrestore(&gs_device_lock, level);

    return OS_SUCCESS;
}

static os_err_t ch_pin_dettach_irq(struct os_device *device, int32_t pin)
{
    os_ubase_t       level;
    csi_gpio_pin_t *pin_info;

    pin_info = &gpio_info[pin];
    if (pin_info->pin_idx != pin)
    {
        return OS_NOSYS;
    }

    os_spin_lock_irqsave(&gs_device_lock, &level);
    if (pin_irq_hdr_tab[pin].pin == -1)
    {
        os_spin_unlock_irqrestore(&gs_device_lock, level);
        return OS_SUCCESS;
    }
    pin_irq_hdr_tab[pin].pin  = -1;
    pin_irq_hdr_tab[pin].hdr  = OS_NULL;
    pin_irq_hdr_tab[pin].mode = 0;
    pin_irq_hdr_tab[pin].args = OS_NULL;

    csi_gpio_pin_irq_enable(pin_info, IRQ_DISABLE);
    csi_gpio_detach_callback(pin_info->gpio);
    os_spin_unlock_irqrestore(&gs_device_lock, level);

    return OS_SUCCESS;
}

static os_err_t ch_pin_irq_enable(struct os_device *device, os_base_t pin, uint32_t enabled)
{
    os_ubase_t       level;
    csi_gpio_pin_t *pin_info;

    pin_info = &gpio_info[pin];
    if (pin_info->pin_idx != pin)
    {
        return OS_NOSYS;
    }

    if (enabled == PIN_IRQ_ENABLE)
    {
        if (pin < 0 || pin >= GPIO_PIN_MAX)
        {
            return OS_NOSYS;
        }

        os_spin_lock_irqsave(&gs_device_lock, &level);

        if (pin_irq_hdr_tab[pin].pin == -1)
        {
            os_spin_unlock_irqrestore(&gs_device_lock, level);
            return OS_NOSYS;
        }
        csi_gpio_pin_irq_enable(pin_info, IRQ_ENABLE);

        os_spin_unlock_irqrestore(&gs_device_lock, level);
    }
    else if (enabled == PIN_IRQ_DISABLE)
    {
        os_spin_lock_irqsave(&gs_device_lock, &level);
        csi_gpio_pin_irq_enable(pin_info, IRQ_DISABLE);
        os_spin_unlock_irqrestore(&gs_device_lock, level);
    }
    else
    {
        return OS_NOSYS;
    }

    return OS_SUCCESS;
}
const static struct os_pin_ops _ch_pin_ops = {
    ch_pin_mode,
    ch_pin_write,
    ch_pin_read,
    ch_pin_attach_irq,
    ch_pin_dettach_irq,
    ch_pin_irq_enable,
};

static void gpio_callback(csi_gpio_t *gpio, uint32_t pins, void *arg)
{
    uint32_t irq_index = bit2bitno(pins);

    if (irq_index >= 0 && irq_index < ITEM_NUM(pin_irq_hdr_tab))
    {
        if (pin_irq_hdr_tab[irq_index].hdr)
        {
            pin_irq_hdr_tab[irq_index].hdr(pin_irq_hdr_tab[irq_index].args);
        }
    }
}

/**
 ***********************************************************************************************************************
 * @brief           os_hw_pin_init:enable gpio clk,register pin device.
 *
 * @param[in]       none
 *
 * @return          Return init result.
 * @retval          OS_SUCCESS       init success.
 * @retval          Others       init failed.
 ***********************************************************************************************************************
 */
int os_hw_pin_init(void)
{

    return os_device_pin_register(0, &_ch_pin_ops, OS_NULL);
}

#endif /* OS_USING_PIN */
