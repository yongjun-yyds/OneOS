/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_usart.c
 *
 * @brief       This file implements usart driver for chv.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <drv_usart.h>
#include <os_task.h>
#include <os_errno.h>
#include <os_assert.h>
#include <drv_cfg.h>
#include <os_memory.h>

#if !defined(BSP_USING_USART0) && !defined(BSP_USING_USART1)
#error "Please define at least one BSP_USING_UARTx"
#endif

static os_list_node_t ch_uart_list = OS_LIST_INIT(ch_uart_list);

static void _usart_rxirq_callback(csi_uart_t *uart, csi_uart_event_t event, void *arg);
static void _uart_intr_recv_data(csi_uart_t *huart);

static void dma_isr(struct os_serial_device *serial);

static void _usart_rxirq_callback(csi_uart_t *uart, csi_uart_event_t event, void *arg)
{
    return;
}

static void _uart_intr_recv_data(csi_uart_t *huart)
{
    dw_uart_regs_t *uart_base  = (dw_uart_regs_t *)huart->dev.reg_base;
    uint32_t        rxfifo_num = dw_uart_get_receive_fifo_waiting_data(uart_base);
    uint32_t        rxdata_num = (rxfifo_num > huart->rx_size) ? huart->rx_size : rxfifo_num;
    struct ch_uart *uart       = (struct ch_uart *)huart->arg;

    if ((huart->rx_data == NULL) || (huart->rx_size == 0U))
    {
        if (huart->callback)
        {
            huart->callback(huart, UART_EVENT_RECEIVE_FIFO_READABLE, huart->arg);
        }
        else
        {
            do
            {
                dw_uart_getchar(uart_base);
            } while (--rxfifo_num);
        }
    }
    else
    {

        do
        {
            uart->rx_buff[uart->rx_index] = dw_uart_getchar(uart_base);
            uart->rx_index++;
        } while (--rxdata_num);

        if (uart->rx_index == (uart->rx_size / 2))
        {
            soft_dma_half_irq(&uart->sdma);
        }

        if (uart->rx_index == (uart->rx_size - 1))
        {
            soft_dma_full_irq(&uart->sdma);
        }

        if (huart->rx_size == 0U)
        {
            huart->state.readable = 1U;

            if (huart->callback)
            {
                huart->callback(huart, UART_EVENT_RECEIVE_COMPLETE, huart->arg);
            }
        }
    }
}

static void _uart_irq_handler(void *arg)
{
    csi_uart_t     *huart     = (csi_uart_t *)arg;
    dw_uart_regs_t *uart_base = (dw_uart_regs_t *)huart->dev.reg_base;

    uint8_t intr_state;

    intr_state = (uint8_t)(uart_base->IIR & 0xfU);

    switch (intr_state)
    {
    case DW_UART_IIR_IID_RECV_LINE_STATUS: /* interrupt source: Overrun/parity/framing errors or break interrupt */
        break;

    case DW_UART_IIR_IID_THR_EMPTY: /* interrupt source:sendter holding register empty */
        break;

    case DW_UART_IIR_IID_RECV_DATA_AVAIL: /* interrupt source:receiver data available or receiver fifo trigger level
                                             reached */
    case DW_UART_IIR_IID_CHARACTER_TIMEOUT:
        _uart_intr_recv_data(huart);
        break;

    default:
        break;
    }
}

static uint32_t ch_sdma_int_get_index(soft_dma_t *dma)
{
    struct ch_uart *uart = os_container_of(dma, struct ch_uart, sdma);

    return uart->rx_index;
}

static os_err_t ch_sdma_int_start(soft_dma_t *dma, void *buff, uint32_t size)
{
    struct ch_uart *uart = os_container_of(dma, struct ch_uart, sdma);
    csi_uart_t     *huart;
    dw_uart_regs_t *uart_base;

    uart->rx_buff  = buff;
    uart->rx_index = 0;
    uart->rx_size  = size;

    huart     = uart->usart_info->uart_periph;
    uart_base = (dw_uart_regs_t *)huart->dev.reg_base;

    huart->rx_data = uart->rx_buff;
    huart->rx_size = uart->rx_size;

    dw_uart_enable_recv_irq(uart_base);

    return OS_SUCCESS;
}

static uint32_t ch_sdma_int_stop(soft_dma_t *dma)
{
    struct ch_uart *uart = os_container_of(dma, struct ch_uart, sdma);
    csi_uart_t     *huart;
    dw_uart_regs_t *uart_base;

    huart     = uart->usart_info->uart_periph;
    uart_base = (dw_uart_regs_t *)huart->dev.reg_base;

    dw_uart_disable_recv_irq(uart_base);

    return ch_sdma_int_get_index(dma);
}

static uint32_t ch_sdma_dma_get_index(soft_dma_t *dma)
{

    return 0;
}

static os_err_t ch_sdma_dma_init(soft_dma_t *dma)
{
    return OS_SUCCESS;
}

static os_err_t ch_sdma_dma_start(soft_dma_t *dma, void *buff, uint32_t size)
{

    return OS_SUCCESS;
}

static uint32_t ch_sdma_dma_stop(soft_dma_t *dma)
{

    return ch_sdma_dma_get_index(dma);
}

static void ch_usart_sdma_callback(soft_dma_t *dma)
{

    struct ch_uart *uart = os_container_of(dma, struct ch_uart, sdma);

    os_hw_serial_isr_rxdone((struct os_serial_device *)uart);
}

static void ch_usart_sdma_init(struct ch_uart *uart, dma_ring_t *ring)
{
    soft_dma_t *dma = &uart->sdma;

    soft_dma_stop(dma);

    memset(&dma->hard_info, 0, sizeof(dma->hard_info));

    dma->hard_info.mode         = HARD_DMA_MODE_NORMAL;
    dma->hard_info.max_size     = 64 * 1024;
    dma->hard_info.data_timeout = uart_calc_byte_timeout_us(uart->serial.config.baud_rate);

    if (uart->usart_info->uart_periph->rx_dma == NULL)
    {
        dma->hard_info.flag = HARD_DMA_FLAG_HALF_IRQ | HARD_DMA_FLAG_FULL_IRQ;

        dma->ops.get_index = ch_sdma_int_get_index;
        dma->ops.dma_init  = OS_NULL;
        dma->ops.dma_start = ch_sdma_int_start;
        dma->ops.dma_stop  = ch_sdma_int_stop;
    }
    else
    {
        dma->hard_info.flag = 0;
        dma->ops.get_index  = ch_sdma_dma_get_index;
        dma->ops.dma_init   = ch_sdma_dma_init;
        dma->ops.dma_start  = ch_sdma_dma_start;
        dma->ops.dma_stop   = ch_sdma_dma_stop;
    }

    dma->cbs.dma_half_callback    = ch_usart_sdma_callback;
    dma->cbs.dma_full_callback    = ch_usart_sdma_callback;
    dma->cbs.dma_timeout_callback = ch_usart_sdma_callback;

    soft_dma_init(dma);
    soft_dma_start(dma, ring);
    soft_dma_irq_enable(&uart->sdma, OS_TRUE);
}

static os_err_t ch_usart_init(struct os_serial_device *serial, struct serial_configure *cfg)
{
    ch_uart_t           *uart;
    csi_uart_data_bits_t data_bits;
    csi_uart_parity_t    parity;
    csi_uart_stop_bits_t stop_bits;
    dw_uart_regs_t      *uart_base;
    uint32_t          level = 0;

    OS_ASSERT(serial != OS_NULL);
    OS_ASSERT(cfg != OS_NULL);

    uart = os_container_of(serial, ch_uart_t, serial);

    OS_ASSERT(uart != OS_NULL);

    uart_base = (dw_uart_regs_t *)HANDLE_REG_BASE(uart->usart_info->uart_periph);

    csi_pin_set_mux(uart->usart_info->gpio_tx, uart->usart_info->tx_pin_func);
    csi_pin_set_mux(uart->usart_info->gpio_rx, uart->usart_info->rx_pin_func);

    csi_uart_init(uart->usart_info->uart_periph, uart->usart_info->idex);

    csi_uart_baud(uart->usart_info->uart_periph, cfg->baud_rate);

    switch (cfg->data_bits)
    {
    case DATA_BITS_9:
        data_bits = UART_DATA_BITS_9;
        break;

    case DATA_BITS_8:
        data_bits = UART_DATA_BITS_8;
        break;

    case DATA_BITS_7:
        data_bits = UART_DATA_BITS_7;
        break;

    case DATA_BITS_6:
        data_bits = UART_DATA_BITS_6;
        break;

    case DATA_BITS_5:
        data_bits = UART_DATA_BITS_5;
        break;

    default:
        data_bits = UART_DATA_BITS_6;
        break;
    }

    switch (cfg->stop_bits)
    {
    case STOP_BITS_2:
        stop_bits = UART_STOP_BITS_2;
        break;
    case STOP_BITS_1:
        stop_bits = UART_STOP_BITS_1;
        break;
    default:
        stop_bits = UART_STOP_BITS_1;
        break;
    }

    switch (cfg->parity)
    {
    case PARITY_ODD:
        parity = UART_PARITY_ODD;
        break;
    case PARITY_EVEN:
        parity = UART_PARITY_EVEN;
        break;
    default:
        parity = UART_PARITY_NONE;
        break;
    }
    csi_uart_format(uart->usart_info->uart_periph, data_bits, parity, stop_bits);
    csi_uart_flowctrl(uart->usart_info->uart_periph, UART_FLOWCTRL_NONE);

    os_spin_lock_irqsave(&gs_device_lock, &level);

    uart->usart_info->uart_periph->callback = OS_NULL;
    uart->usart_info->uart_periph->arg      = uart;
    uart->usart_info->uart_periph->send     = OS_NULL;
    uart->usart_info->uart_periph->receive  = OS_NULL;
    csi_irq_attach((uint32_t)(uart->usart_info->uart_periph->dev.irq_num),
                   &_uart_irq_handler,
                   &uart->usart_info->uart_periph->dev);
    csi_irq_enable((uint32_t)(uart->usart_info->uart_periph->dev.irq_num));
    dw_uart_enable_recv_irq(uart_base);
    os_spin_unlock_irqrestore(&gs_device_lock, level);

    ch_usart_sdma_init(uart, &serial->rx_fifo->ring);

    return OS_SUCCESS;
}

static os_err_t ch_usart_deinit(struct os_serial_device *serial)
{
    struct ch_uart *uart;

    OS_ASSERT(serial != OS_NULL);

    uart = os_container_of(serial, struct ch_uart, serial);

    if (uart->usart_info->uart_periph->rx_dma)
    {

        csi_uart_uninit(uart->usart_info->uart_periph);
    }
    else
    {
        csi_uart_uninit(uart->usart_info->uart_periph);
    }

    soft_dma_stop(&uart->sdma);

    return 0;
}

static void dma_isr(struct os_serial_device *serial)
{
    return;
}

static int ch_uart_poll_send(struct os_serial_device *serial, const uint8_t *buff, os_size_t size)
{
    struct ch_uart *uart;
    os_size_t       i;
    os_ubase_t       level;

    OS_ASSERT(serial != OS_NULL);

    uart = os_container_of(serial, struct ch_uart, serial);

    OS_ASSERT(uart != OS_NULL);

    for (i = 0; i < size; i++)
    {
        os_spin_lock_irqsave(&gs_device_lock, &level);

        csi_uart_putc(uart->usart_info->uart_periph, buff[i]);

        os_spin_unlock_irqrestore(&gs_device_lock, level);
    }

    return size;
}
/* clang-format on */

static const struct os_uart_ops ch_uart_ops = {
    .init       = ch_usart_init,
    .deinit     = ch_usart_deinit,
    .start_send = OS_NULL,
    .poll_send  = ch_uart_poll_send,
};

static int ch_usart_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    struct serial_configure config = OS_SERIAL_CONFIG_DEFAULT;
    os_err_t                result = 0;
    os_ubase_t               level;

    struct ch_uart *uart = os_calloc(1, sizeof(struct ch_uart));

    OS_ASSERT(uart);

    uart->usart_info = (struct ch_usart_info *)dev->info;

    struct os_serial_device *serial = &uart->serial;

    serial->ops    = &ch_uart_ops;
    serial->config = config;

    os_spin_lock_irqsave(&gs_device_lock, &level);
    os_list_add_tail(&ch_uart_list, &uart->list);
    os_spin_unlock_irqrestore(&gs_device_lock, level);

    result = os_hw_serial_register(serial, dev->name, uart);

    OS_ASSERT(result == OS_SUCCESS);

    return result;
}

OS_DRIVER_INFO ch_usart_driver = {
    .name  = "csi_uart_t",
    .probe = ch_usart_probe,
};

OS_DRIVER_DEFINE(ch_usart_driver, OS_INIT_LEVEL_PRE_DEVICE, OS_INIT_SUBLEVEL_HIGH);

#ifdef OS_USING_CONSOLE
csi_uart_t *console_uart = 0;
void        __os_hw_console_output(char *str)
{
    if (console_uart == 0)
        return;
    while (*str)
    {
        csi_uart_putc(console_uart, *str++);
    }
}

static void _usart_hard_init(ch_usart_info_t *info)
{
    csi_uart_t *uart = info->uart_periph;

    csi_uart_init(uart, info->idex);
    csi_uart_baud(uart, 115200);
    csi_uart_format(uart, UART_DATA_BITS_8, UART_PARITY_NONE, UART_STOP_BITS_1);
    csi_uart_flowctrl(uart, UART_FLOWCTRL_NONE);
}

static os_err_t _usart_early_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    ch_usart_info_t *info = (ch_usart_info_t *)dev->info;
    _usart_hard_init(info);

    if (!strcmp(dev->name, OS_CONSOLE_DEVICE_NAME))
    {
        console_uart = (csi_uart_t *)info->uart_periph;
    }
    return OS_SUCCESS;
}

OS_DRIVER_INFO _usart_early_driver = {
    .name  = "csi_uart_t",
    .probe = _usart_early_probe,
};

OS_DRIVER_DEFINE(_usart_early_driver, OS_INIT_LEVEL_PRE_KERNEL_1, OS_INIT_SUBLEVEL_LOW);
#endif

