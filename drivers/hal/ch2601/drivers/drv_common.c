/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_common.c
 *
 * @brief       This file provides systick time init/IRQ and board init functions.
 *
 * @revision
 * Date         Author          Notes
 * 2022-01-10   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include <string.h>
#include <os_types.h>
#include <os_stddef.h>

#include <arch_interrupt.h>
#include <os_clock.h>
#include <os_memory.h>
#include <oneos_config.h>

#include <drv_common.h>
#include <board.h>

#ifdef OS_USING_DMA
#include <dma.h>
#endif

#ifdef OS_USING_CLOCKSOURCE
#include <timer/clocksource.h>
#include <timer/clocksource_cortexm.h>
#endif

#include <drv/timer.h>
#include <drv_gpio.h>

#define SYS_TICK_TIMER_IDX 1U
static csi_timer_t tick_timer;

extern uint32_t g_system_clock;

void os_tick_handler(void)
{
    os_tick_increase();
#ifdef OS_USING_CLOCKSOURCE
    os_clocksource_update();
#endif
}

static void tick_event_cb(csi_timer_t *timer_handle, void *arg)
{
    os_tick_handler();
}

os_err_t os_tick_init(void)
{
    os_err_t ret;

    ret = csi_timer_init(&tick_timer, SYS_TICK_TIMER_IDX);

    if (ret == CSI_OK)
    {
        ret = csi_timer_attach_callback(&tick_timer, tick_event_cb, NULL);

        if (ret == CSI_OK)
        {
            ret = csi_timer_start(&tick_timer, (1000000U / OS_TICK_PER_SECOND));
        }
    }

    return ret;
}

void hardware_init(void)
{
}

void os_hw_cpu_reset(void)
{
}

/**
 ***********************************************************************************************************************
 * @brief           This function will initial ch32 board.
 *
 * @param[in]       none
 *
 * @return          none
 ***********************************************************************************************************************
 */
static os_err_t os_hw_board_init(void)
{
    hardware_init();

    /* Heap initialization */

#ifdef OS_USING_HEAP
    if ((os_size_t)HEAP_END > (os_size_t)HEAP_BEGIN)
    {
        os_default_heap_init();
        os_default_heap_add((void *)HEAP_BEGIN, (os_size_t)HEAP_END - (os_size_t)HEAP_BEGIN, OS_MEM_ALG_DEFAULT);
    }
#endif

#ifdef OS_USING_DMA_RAM
    extern void os_dma_mem_init(void);
    os_dma_mem_init();
#endif

    return OS_SUCCESS;
}
OS_INIT_CALL(os_hw_board_init, OS_INIT_LEVEL_PRE_KERNEL_1, OS_INIT_SUBLEVEL_MIDDLE);

static os_err_t board_post_init(void)
{

#ifdef OS_USING_PIN
    os_hw_pin_init();
#endif

    os_tick_init();

    return OS_SUCCESS;
}
OS_INIT_CALL(board_post_init, OS_INIT_LEVEL_POST_KERNEL, OS_INIT_SUBLEVEL_LOW);
