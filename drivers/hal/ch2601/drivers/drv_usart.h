/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_usart.h
 *
 * @brief        This file provides functions declaration for usart driver.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __DRV_UART_H__
#define __DRV_UART_H__

#include <os_task.h>
#include <device.h>
#include <drv_cfg.h>
#include "drv/uart.h"
#include "dw_uart_ll.h"
#include <soft_dma.h>
#include "drv/irq.h"
#include "drv/pin.h"

/* ch2601 uart driver */
typedef struct ch_uart
{
    struct os_serial_device serial;

    struct ch_usart_info *usart_info;
    soft_dma_t            sdma;
    uint32_t           sdma_hard_size;
    uint8_t           *rx_buff;
    uint32_t           rx_index;
    uint32_t           rx_size;
    os_list_node_t        list;
    os_size_t             tx_count;
    os_size_t             tx_size;
    const uint8_t     *tx_buff;
} ch_uart_t;

typedef struct ch_usart_info
{
    csi_uart_t *uart_periph;
    uint32_t idex;
    uint32_t gpio_tx;
    pin_func_t  tx_pin_func;
    uint32_t gpio_rx;
    pin_func_t  rx_pin_func;
} ch_usart_info_t;

int os_hw_usart_init(void);

#endif /* __DRV_USART_H__ */

/******************* end of file *******************/
