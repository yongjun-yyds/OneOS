/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_hwtimer.c
 *
 * @brief       This file implements timer driver for mm32.
 *
 * @revision
 * Date         Author          Notes
 * 2021-05-31   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include <drv_common.h>
#include <drv_hwtimer.h>
#include <os_memory.h>
#include <timer/timer.h>
#include <bus.h>
#ifdef OS_USING_CLOCKSOURCE
#include <timer/clocksource.h>
#endif

#ifdef OS_USING_CLOCKEVENT
#include <timer/clockevent.h>
#endif

#define DRV_EXT_TAG "drv.hwtimer"
#include <drv_log.h>

static os_list_node_t ch_timer_list = OS_LIST_INIT(ch_timer_list);

static uint32_t ch2601_timer_get_freq(struct ch_timer *timer)
{
    uint32_t  clk = 0;
    csi_timer_t *htim;

    OS_ASSERT(timer->info != OS_NULL);
    htim = timer->info;

    clk = soc_get_timer_freq(htim->dev.idx);

    return clk;
}

static void timer_start(csi_timer_t *timer, uint32_t count)
{
    dw_timer_regs_t *timer_base = (dw_timer_regs_t *)HANDLE_REG_BASE(timer);

    dw_timer_set_mode_load(timer_base);

    dw_timer_write_load(timer_base, count);

    dw_timer_set_disable(timer_base);
    dw_timer_set_enable(timer_base);
    dw_timer_set_unmask(timer_base);
}

static void tim_irq_call(csi_timer_t *timer_handle, void *arg)
{
    struct ch_timer *timer;
    os_list_for_each_entry(timer, &ch_timer_list, struct ch_timer, list)
    {
        if (timer->info == timer_handle)
        {
#ifdef OS_USING_CLOCKEVENT
            os_clockevent_isr((os_clockevent_t *)timer);
#endif
            break;
        }
    }
}

#if defined(OS_USING_CLOCKSOURCE) || defined(OS_USING_CLOCKEVENT)
static uint64_t ch_timer_read(void *clock)
{
    struct ch_timer *timer;
    uint32_t      count;

    timer = (struct ch_timer *)clock;

    count = csi_timer_get_load_value(timer->info) - csi_timer_get_remaining_value(timer->info);

    return count;
}
#endif

#ifdef OS_USING_CLOCKEVENT
static void ch_timer_start(os_clockevent_t *ce, uint32_t prescaler, uint64_t count)
{
    struct ch_timer *timer;

    OS_ASSERT(prescaler != 0);
    OS_ASSERT(count != 0);

    timer = (struct ch_timer *)ce;

    csi_timer_t *timer_handle = timer->info;

    timer_start(timer_handle, count);
}

static void ch_timer_stop(os_clockevent_t *ce)
{
    struct ch_timer *timer;

    OS_ASSERT(ce != OS_NULL);

    timer = (struct ch_timer *)ce;

    csi_timer_t *timer_handle = timer->info;

    csi_timer_stop(timer_handle);
    csi_timer_uninit(timer_handle);
}

static const struct os_clockevent_ops ch_tim_ops = {
    .start = ch_timer_start,
    .stop  = ch_timer_stop,
    .read  = ch_timer_read,
};
#endif

static int ch_tim_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    struct ch_timer *timer;
    os_err_t         ret = OS_SUCCESS;
    csi_timer_t     *csi_tim;

    timer = os_calloc(1, sizeof(struct ch_timer));
    OS_ASSERT(timer);

    timer->info = (csi_timer_t *)dev->info;

    csi_tim = timer->info;

    ret = csi_timer_init(csi_tim, csi_tim->dev.idx);

    timer->freq = ch2601_timer_get_freq(timer);

#ifdef OS_USING_CLOCKSOURCE
    if (os_clocksource_best() == OS_NULL)
    {
        timer_start(csi_tim, 0xffffffff);

        timer->clock.cs.rating = 320;
        timer->clock.cs.freq   = timer->freq;
        timer->clock.cs.mask   = 0xfffffffful;
        timer->clock.cs.read   = ch_timer_read;

        os_clocksource_register(dev->name, &timer->clock.cs);
    }
    else
#endif
    {
#ifdef OS_USING_CLOCKEVENT
        ret = csi_timer_attach_callback(csi_tim, tim_irq_call, NULL);

        timer->clock.ce.rating = 320;
        timer->clock.ce.freq   = timer->freq;
        timer->clock.ce.mask   = 0xfffffffful;

        timer->clock.ce.prescaler_mask = 1;
        timer->clock.ce.prescaler_bits = 0;

        timer->clock.ce.count_mask = 0xfffffffful;
        timer->clock.ce.count_bits = 32;

        timer->clock.ce.feature = OS_CLOCKEVENT_FEATURE_PERIOD;

        timer->clock.ce.min_nsec = NSEC_PER_SEC / timer->clock.ce.freq;

        timer->clock.ce.ops = &ch_tim_ops;
        os_clockevent_register(dev->name, &timer->clock.ce);
#endif
    }

    os_list_add(&ch_timer_list, &timer->list);

    return ret;
}

OS_DRIVER_INFO ch_tim_driver = {
    .name  = "csi_timer",
    .probe = ch_tim_probe,
};

OS_DRIVER_DEFINE(ch_tim_driver, OS_INIT_LEVEL_PRE_DEVICE, OS_INIT_SUBLEVEL_MIDDLE);

