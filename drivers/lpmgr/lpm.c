#include <os_errno.h>
#include <os_util.h>
#include <os_clock.h>
#include <arch_interrupt.h>
#include <os_stddef.h>
#include <device.h>

OS_WEAK os_err_t lpm_timer_start_once(uint32_t timeout_ms)
{
    (void)timeout_ms;

    return OS_SUCCESS;
}

OS_WEAK void lpm_enter_sleep(void)
{
    ;
}

os_err_t lpm_start(uint32_t timeout_ms)
{
    os_ubase_t level;
    os_err_t   ret = OS_SUCCESS;

    os_spin_lock_irqsave(&gs_device_lock, &level);

    ret = lpm_timer_start_once(timeout_ms);
    if (ret != OS_SUCCESS)
    {
        os_spin_unlock_irqrestore(&gs_device_lock, level);
        return ret;
    }

    lpm_enter_sleep();

    os_spin_unlock_irqrestore(&gs_device_lock, level);

    os_kprintf("[%s]-[%d], timeout[%d ms], cur_tick[%d]\r\n", __FILE__, __LINE__, timeout_ms, os_tick_get_value());

    return OS_SUCCESS;
}
