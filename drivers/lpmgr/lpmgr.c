/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        lpmgr.c
 *
 * @brief       this file implements Low power management related functions
 *
 * @details
 *
 * @revision
 * Date          Author          Notes
 * 2020-02-20    OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <string.h>
#include <stdlib.h>
#include <arch_interrupt.h>
#include <os_timer.h>
#include <os_memory.h>
#include <os_assert.h>
#include <os_errno.h>
#include <lpmgr.h>
#include <dlog.h>

#define DBG_TAG "lpmgr"

struct lpmgr_update_callback
{
    void (*update_cb)(uint64_t nsec, void *priv);
    void          *priv;
    os_list_node_t list;
};

#define LP_SLEEP_STATUS_NONE    (0)
#define LP_SLEEP_STATUS_ACTIVE  (1)
#define LP_SLEEP_STATUS_UPDATED (2)

static struct os_lpmgr_dev *gs_lpmgr = NULL;

static int gs_lpce_irq = -1;

static int gs_lp_sleep_state = LP_SLEEP_STATUS_NONE;

static uint64_t gs_lp_sleep_nsec = 0;

static os_list_node_t gs_register_dev_list          = OS_LIST_INIT(gs_register_dev_list);
static os_list_node_t gs_lpmgr_update_callback_list = OS_LIST_INIT(gs_lpmgr_update_callback_list);
static os_list_node_t gs_register_pre_dev_list      = OS_LIST_INIT(gs_register_pre_dev_list);
static os_list_node_t gs_lpmgr_notify_list          = OS_LIST_INIT(gs_lpmgr_notify_list);

static os_err_t __os_lpmgr_device_register(void *priv, const struct os_lpmgr_device_ops *ops, os_bool_t head)
{
    os_ubase_t level;

    struct os_lpmgr_device *device_lpm;

    OS_ASSERT(OS_NULL != ops);

    if (OS_NULL == ops->resume || OS_NULL == ops->suspend)
    {
        LOG_D(DBG_TAG, "no resume or suspend func\n");
        return OS_FAILURE;
    }

    os_list_for_each_entry(device_lpm, &gs_register_dev_list, struct os_lpmgr_device, list)
    {
        if ((device_lpm->priv == priv) && (device_lpm->ops == ops))
        {
            LOG_D(DBG_TAG, "dev[%p], ops[%p] alread register!\n", priv, ops);
            return OS_SUCCESS;
        }
    }

    device_lpm = os_calloc(1, sizeof(struct os_lpmgr_device));

    OS_ASSERT(device_lpm != OS_NULL);

    device_lpm->priv = priv;
    device_lpm->ops  = ops;

    os_spin_lock_irqsave(&gs_device_lock, &level);

    if (head)
    {
        os_list_add(&gs_register_dev_list, &device_lpm->list);
    }
    else
    {
        os_list_add_tail(&gs_register_dev_list, &device_lpm->list);
    }

    os_spin_unlock_irqrestore(&gs_device_lock, level);

    LOG_D(DBG_TAG, "register dev[%p], ops[%p]!\n", priv, ops);

    return OS_SUCCESS;
}

os_err_t os_lpmgr_device_register_high_prio(void *priv, const struct os_lpmgr_device_ops *ops)
{
    return __os_lpmgr_device_register(priv, ops, OS_TRUE);
}

os_err_t os_lpmgr_device_register(void *priv, const struct os_lpmgr_device_ops *ops)
{
    return __os_lpmgr_device_register(priv, ops, OS_FALSE);
}

void os_lpmgr_device_unregister(void *priv, const struct os_lpmgr_device_ops *ops)
{
    struct os_lpmgr_device *device_lpm;

    os_list_for_each_entry(device_lpm, &gs_register_dev_list, struct os_lpmgr_device, list)
    {
        if ((device_lpm->priv == priv) && (device_lpm->ops == ops))
        {
            os_list_del(&device_lpm->list);
            os_free(device_lpm);
            return;
        }
    }
}

static int lpmgr_device_suspend(lpmgr_sleep_mode_e mode)
{
    int ret = OS_SUCCESS;

    struct os_lpmgr_device *device_lpm;

    os_list_for_each_entry(device_lpm, &gs_register_dev_list, struct os_lpmgr_device, list)
    {
        ret = device_lpm->ops->suspend(device_lpm->priv, mode);
        if (ret != OS_SUCCESS)
            break;
    }

    return ret;
}

static void lpmgr_device_resume(lpmgr_sleep_mode_e mode)
{
    struct os_lpmgr_device *device_lpm;

    os_list_for_each_entry(device_lpm, &gs_register_dev_list, struct os_lpmgr_device, list)
    {
        device_lpm->ops->resume(device_lpm->priv, mode);
    }
}

os_err_t os_lpmgr_preprocess_dev_register(void *priv, const struct os_lpmgr_device_ops *ops)
{
    os_ubase_t level;

    struct os_lpmgr_device *prep_dev_lpm;

    OS_ASSERT(OS_NULL != ops);

    if (OS_NULL == ops->resume || OS_NULL == ops->suspend)
    {
        LOG_D(DBG_TAG, "no resume or suspend func\n");
        return OS_FAILURE;
    }

    os_list_for_each_entry(prep_dev_lpm, &gs_register_pre_dev_list, struct os_lpmgr_device, list)
    {
        if ((prep_dev_lpm->priv == priv) && (prep_dev_lpm->ops == ops))
        {
            LOG_D(DBG_TAG, "dev[%p], ops[%p] alread register!\n", priv, ops);
            return OS_SUCCESS;
        }
    }

    prep_dev_lpm = os_calloc(1, sizeof(struct os_lpmgr_device));

    OS_ASSERT(prep_dev_lpm != OS_NULL);

    prep_dev_lpm->priv = priv;
    prep_dev_lpm->ops  = ops;

    os_spin_lock_irqsave(&gs_device_lock, &level);

    os_list_add_tail(&gs_register_pre_dev_list, &prep_dev_lpm->list);

    os_spin_unlock_irqrestore(&gs_device_lock, level);

    LOG_D(DBG_TAG, "register dev[%p], ops[%p]!\n", priv, ops);

    return OS_SUCCESS;
}

void os_lpmgr_preprocess_dev_unregister(void *priv, const struct os_lpmgr_device_ops *ops)
{
    struct os_lpmgr_device *prep_dev_lpm;

    os_list_for_each_entry(prep_dev_lpm, &gs_register_pre_dev_list, struct os_lpmgr_device, list)
    {
        if ((prep_dev_lpm->priv == priv) && (prep_dev_lpm->ops == ops))
        {
            os_list_del(&prep_dev_lpm->list);
            os_free(prep_dev_lpm);
            return;
        }
    }
}

static int pre_gettick_dev_suspend(lpmgr_sleep_mode_e mode)
{
    int ret = OS_SUCCESS;

    struct os_lpmgr_device *prep_dev_lpm;

    os_list_for_each_entry(prep_dev_lpm, &gs_register_pre_dev_list, struct os_lpmgr_device, list)
    {
        ret = prep_dev_lpm->ops->suspend(prep_dev_lpm->priv, mode);
        if (ret != OS_SUCCESS)
            break;
    }

    return ret;
}

static void pre_gettick_dev_resume(lpmgr_sleep_mode_e mode)
{
    struct os_lpmgr_device *prep_dev_lpm;

    os_list_for_each_entry(prep_dev_lpm, &gs_register_pre_dev_list, struct os_lpmgr_device, list)
    {
        prep_dev_lpm->ops->resume(prep_dev_lpm->priv, mode);
    }
}

static void lpmgr_notify_call(os_lpmgr_sys_e evt, lpmgr_sleep_mode_e mode)
{
    struct lpmgr_notify *node;
    os_list_for_each_entry(node, &gs_lpmgr_notify_list, struct lpmgr_notify, list)
    {
        if (node->notify)
        {
            node->notify(evt, mode, node->data);
        }
    }
}

static int lpmgr_enter_sleep_call(lpmgr_sleep_mode_e mode)
{
    int ret = OS_SUCCESS;
    /* Notify app will enter sleep mode */
    lpmgr_notify_call(SYS_ENTER_SLEEP, mode);

    /* Suspend all peripheral device */
    ret = lpmgr_device_suspend(mode);
    if (ret != OS_SUCCESS)
    {
        lpmgr_device_resume(mode);
        lpmgr_notify_call(SYS_EXIT_SLEEP, mode);

        return OS_FAILURE;
    }

    return OS_SUCCESS;
}

static int lpmgr_exit_sleep_call(lpmgr_sleep_mode_e mode)
{
    lpmgr_device_resume(mode);
    lpmgr_notify_call(SYS_EXIT_SLEEP, mode);

    return OS_SUCCESS;
}

void os_lpmgr_update_callback_register(void (*update_cb)(uint64_t nsec, void *priv), void *priv)
{
    os_ubase_t                     level;
    struct lpmgr_update_callback *cb;

    cb = os_calloc(1, sizeof(struct lpmgr_update_callback));

    OS_ASSERT(cb != OS_NULL);

    cb->update_cb = update_cb;
    cb->priv      = priv;

    os_spin_lock_irqsave(&gs_device_lock, &level);
    os_list_add_tail(&gs_lpmgr_update_callback_list, &cb->list);
    os_spin_unlock_irqrestore(&gs_device_lock, level);
}

static void os_lpmgr_update(uint64_t nsec)
{
    struct lpmgr_update_callback *cb;

    if (nsec == 0)
    {
        return;
    }

    os_list_for_each_entry(cb, &gs_lpmgr_update_callback_list, struct lpmgr_update_callback, list)
    {
        cb->update_cb(nsec, cb->priv);
    }
}

static void os_lpmgr_irq_entry(int irq)
{
    os_ubase_t level;

    struct os_lpmgr_dev *lpmgr = gs_lpmgr;

    os_spin_lock_irqsave(&gs_device_lock, &level);

    if (gs_lp_sleep_state == LP_SLEEP_STATUS_ACTIVE)
    {
        if (gs_lp_sleep_nsec != 0)
        {
            if (irq != gs_lpce_irq)
            {
                gs_lp_sleep_nsec = os_clockevent_read(lpmgr->lpce);
                os_clockevent_stop(lpmgr->lpce);
            }

            os_lpmgr_update(gs_lp_sleep_nsec);
        }
        gs_lp_sleep_state = LP_SLEEP_STATUS_UPDATED;
    }

    if (gs_lp_sleep_state != LP_SLEEP_STATUS_NONE)
    {
        /* period trig 1ms, avoid sleep forever */
        os_clockevent_start_oneshot(lpmgr->lpce, max(1000000ULL, lpmgr->lpce->min_nsec));
    }

    os_spin_unlock_irqrestore(&gs_device_lock, level);
}

OS_IRQ_HOOK(os_lpmgr_irq_entry);

static os_err_t lpmgr_start(struct os_lpmgr_dev *lpmgr)
{
    os_err_t    ret = OS_SUCCESS;
    os_ubase_t   level;
    uint64_t nsec = 0;

    ret = pre_gettick_dev_suspend(SYS_SLEEP_MODE_NONE);
    if (ret != OS_SUCCESS)
    {
        pre_gettick_dev_resume(SYS_SLEEP_MODE_NONE);
        return OS_BUSY;
    }

    os_spin_lock_irqsave(&gs_device_lock, &level);
    os_tick_t timeout_tick = os_tickless_get_sleep_ticks();

    if (timeout_tick != OS_TICK_MAX)
    {
        nsec = ((uint64_t)timeout_tick * lpmgr->mult >> lpmgr->shift);

        if (nsec < lpmgr->lpce->min_nsec || nsec < BSP_USING_MINSLEEP_MS * 1000000)
        {
            pre_gettick_dev_resume(SYS_SLEEP_MODE_NONE);
            os_spin_unlock_irqrestore(&gs_device_lock, level);
            return OS_BUSY;
        }

        ret = lpmgr_enter_sleep_call(lpmgr->sleep_mode);
        if (ret != OS_SUCCESS)
        {
            pre_gettick_dev_resume(SYS_SLEEP_MODE_NONE);
            lpmgr_exit_sleep_call(lpmgr->sleep_mode);
            os_spin_unlock_irqrestore(&gs_device_lock, level);
            return OS_BUSY;
        }

        if (nsec > lpmgr->lpce->max_nsec)
            nsec = lpmgr->lpce->max_nsec;

        gs_lp_sleep_nsec = nsec;
        os_clockevent_start_oneshot(lpmgr->lpce, nsec);
    }
    else
    {
        gs_lp_sleep_nsec = 0;
        os_clockevent_stop(lpmgr->lpce);
        ret = lpmgr_enter_sleep_call(lpmgr->sleep_mode);
        if (ret != OS_SUCCESS)
        {
            pre_gettick_dev_resume(SYS_SLEEP_MODE_NONE);
            lpmgr_exit_sleep_call(lpmgr->sleep_mode);
            os_spin_unlock_irqrestore(&gs_device_lock, level);
            return OS_BUSY;
        }
    }

    gs_lp_sleep_state = LP_SLEEP_STATUS_ACTIVE;

    os_spin_unlock_irqrestore(&gs_device_lock, level);

    lpmgr->sleep(lpmgr->sleep_mode);

    OS_ASSERT(gs_lp_sleep_state != LP_SLEEP_STATUS_ACTIVE);
    gs_lp_sleep_state = LP_SLEEP_STATUS_NONE;

    pre_gettick_dev_resume(SYS_SLEEP_MODE_NONE);
    lpmgr_exit_sleep_call(lpmgr->sleep_mode);

    return OS_SUCCESS;
}

void os_low_power_manager(void)
{
    OS_ASSERT(gs_lpmgr != NULL);

    os_schedule_lock();

    if (gs_lpmgr->sleep_mode > SYS_SLEEP_MODE_IDLE)
        lpmgr_start(gs_lpmgr);

    os_schedule_unlock();
}

static void lpmgr_check_sleep_mode(struct os_lpmgr_dev *lpmgr)
{
    lpmgr_sleep_mode_e index;
    lpmgr->sleep_mode = SYS_SLEEP_MODE_IDLE;

    for (index = SYS_SLEEP_MODE_NONE; index < SYS_SLEEP_MODE_MAX; index++)
    {
        if (lpmgr->modes[index])
        {
            lpmgr->sleep_mode = index;
            break;
        }
    }
}

/**
 ***********************************************************************************************************************
 * @brief           Upper application or device driver requests the system stall in corresponding power mode
 *
 * @param[in]       sleep_mode                the parameter of run mode or sleep mode
 *
 * @return          no return value
 ***********************************************************************************************************************
 */
void os_lpmgr_request(lpmgr_sleep_mode_e sleep_mode)
{
    os_ubase_t            level;
    struct os_lpmgr_dev *lpmgr = gs_lpmgr;

    if (sleep_mode > (SYS_SLEEP_MODE_MAX - 1))
        return;

    os_spin_lock_irqsave(&gs_device_lock, &level);
    if (lpmgr->modes[sleep_mode] < 255)
        lpmgr->modes[sleep_mode]++;

    lpmgr_check_sleep_mode(lpmgr);

    os_spin_unlock_irqrestore(&gs_device_lock, level);
}

/**
 ***********************************************************************************************************************
 * @brief           Upper application or device driver releases the system stall in corresponding power mode
 *
 * @param[in]       sleep_mode                the parameter of run mode or sleep mode
 *
 * @return          no return value
 ***********************************************************************************************************************
 */
void os_lpmgr_release(lpmgr_sleep_mode_e sleep_mode)
{
    os_ubase_t           level;
    struct os_lpmgr_dev *lpmgr = gs_lpmgr;

    if (sleep_mode > (SYS_SLEEP_MODE_MAX - 1))
        return;

    os_spin_lock_irqsave(&gs_device_lock, &level);
    if (lpmgr->modes[sleep_mode] > 0)
        lpmgr->modes[sleep_mode]--;

    lpmgr_check_sleep_mode(lpmgr);

    os_spin_unlock_irqrestore(&gs_device_lock, level);
}

static void os_kernel_update_ns(uint64_t nsec, void *priv)
{
    os_tick_t tick = ((uint64_t)nsec * OS_TICK_PER_SECOND / NSEC_PER_SEC);
    os_tickless_update(tick);
}

static void lpce_event_handler(os_clockevent_t *ce)
{
    gs_lpce_irq = os_irq_num();
}

/**
 ***********************************************************************************************************************
 * @brief           This function registers a notify callback.
 *
 * @param[in]       struct os_notify_cb_info *info.
 *
 * @return          Regist result.
 * @retval          OS_SUCCESS          Successful.
 * @retval          OS_INVAL       Fail.
 ***********************************************************************************************************************
 */
os_err_t os_lpmgr_notify_register(struct lpmgr_notify *cb_info)
{
    os_ubase_t            level;
    struct lpmgr_notify *node;

    OS_ASSERT(OS_NULL != cb_info);
    OS_ASSERT(OS_NULL != cb_info->notify);

    os_spin_lock_irqsave(&gs_device_lock, &level);

    os_list_for_each_entry(node, &gs_lpmgr_notify_list, struct lpmgr_notify, list)
    {
        if (cb_info->notify == node->notify && cb_info->data == node->data)
        {
            os_spin_unlock_irqrestore(&gs_device_lock, level);
            LOG_I(DBG_TAG, "lpmgr_notify notify[0x%p] already register!\r\n", cb_info->notify);
            return OS_SUCCESS;
        }
    }

    os_list_add_tail(&gs_lpmgr_notify_list, &cb_info->list);

    os_spin_unlock_irqrestore(&gs_device_lock, level);

    LOG_D(DBG_TAG, "notify[0x%p]register success!\r\n", cb_info->notify);

    return OS_SUCCESS;
}

/**
 ***********************************************************************************************************************
 * @brief           This function will remove a previously registered notify callback.
 *
 * @param[in]       void *arg            para.
 *
 * @return          Regist result.
 * @retval          OS_SUCCESS          Successful.
 * @retval          OS_INVAL       Fail.
 ***********************************************************************************************************************
 */
os_err_t os_lpmgr_notify_unregister(struct lpmgr_notify *cb_info)
{
    os_ubase_t            level;
    struct lpmgr_notify *node;

    OS_ASSERT(OS_NULL != cb_info);

    os_spin_lock_irqsave(&gs_device_lock, &level);

    os_list_for_each_entry(node, &gs_lpmgr_notify_list, struct lpmgr_notify, list)
    {
        if (cb_info->notify == node->notify)
        {
            os_list_del(&node->list);
            LOG_I(DBG_TAG, "unregister lpmgr_notify notify[0x%p]!\r\n", cb_info->notify);
            break;
        }
    }

    os_spin_unlock_irqrestore(&gs_device_lock, level);

    return OS_FAILURE;
}

/* clang-format off */
void os_lpmgr_init(os_err_t (*sleep)(lpmgr_sleep_mode_e mode))
{
    struct os_lpmgr_dev *lpmgr;
    uint32_t          max_sec;

    lpmgr = (struct os_lpmgr_dev *)os_calloc(1, sizeof(struct os_lpmgr_dev));
    OS_ASSERT(lpmgr != NULL);

    lpmgr->lpce = (os_clockevent_t *)os_device_open_s(LPMGR_TIMER_DEVICE_NAME);
    OS_ASSERT(lpmgr->lpce != NULL);
    OS_ASSERT(lpmgr->lpce->prescaler_mask == 1);

    /* find lpce irq number */
    os_clockevent_register_isr(lpmgr->lpce, lpce_event_handler);

    os_clockevent_start_oneshot(lpmgr->lpce, 10);

    volatile int i = 1000000;

    while (gs_lpce_irq == -1 && i--);

    os_clockevent_stop(lpmgr->lpce);

    OS_ASSERT(gs_lpce_irq != -1);

    os_kprintf("lpce:%s, irq:%d\r\n", LPMGR_TIMER_DEVICE_NAME, gs_lpce_irq);

    os_clockevent_register_isr(lpmgr->lpce, OS_NULL);

    lpmgr->sleep = sleep;

    lpmgr->sleep_mode = SYS_SLEEP_MODE_IDLE;

    max_sec = lpmgr->lpce->mask / lpmgr->lpce->freq;
    calc_mult_shift(&lpmgr->mult, &lpmgr->shift, OS_TICK_PER_SECOND, NSEC_PER_SEC, max_sec);
    calc_mult_shift(&lpmgr->mult_t, &lpmgr->shift_t, NSEC_PER_SEC, OS_TICK_PER_SECOND, max_sec);
    lpmgr->parent.type = OS_DEVICE_TYPE_PM;
    lpmgr->parent.ops  = OS_NULL;

    os_device_register(&lpmgr->parent, OS_LPMGR_DEVICE_NAME);

    gs_lpmgr = lpmgr;

    os_device_open(&lpmgr->parent);

    os_lpmgr_update_callback_register(os_kernel_update_ns, OS_NULL);
}
/* clang-format on */

#ifdef OS_USING_SHELL
#include <shell.h>

static const char *gs_lpmgr_sleep_str[] = SYS_SLEEP_MODE_NAMES;

static void lpmgr_release_mode(int argc, char **argv)
{
    lpmgr_sleep_mode_e mode = SYS_SLEEP_MODE_NONE;
    if (argc >= 2)
    {
        mode = (lpmgr_sleep_mode_e)atoi(argv[1]);
    }

    os_lpmgr_release(mode);
}
SH_CMD_EXPORT(power_release, lpmgr_release_mode, "release power management mode");

static void lpmgr_request_mode(int argc, char **argv)
{
    lpmgr_sleep_mode_e mode = SYS_SLEEP_MODE_NONE;
    if (argc >= 2)
    {
        mode = (lpmgr_sleep_mode_e)atoi(argv[1]);
    }

    os_lpmgr_request(mode);
}
SH_CMD_EXPORT(power_request, lpmgr_request_mode, "request power management mode");

static void lpmgr_dump_status(void)
{
    uint32_t          index;
    struct os_lpmgr_dev *lpmgr = gs_lpmgr;

    /* dump power status */
    os_kprintf("| Power Management Mode | Counter | Timer |\r\n");
    os_kprintf("+-----------------------+---------+-------+\r\n");

    os_kprintf("lpmgr current sleep mode: %s\r\n", gs_lpmgr_sleep_str[lpmgr->sleep_mode]);

    /* dump lpmgr devices */
    index = 0;
    struct os_lpmgr_device *device_lpm;

    os_kprintf("| no    |    priv    | ops        |\r\n");
    os_kprintf("+-------+------------+------------+\r\n");

    os_list_for_each_entry(device_lpm, &gs_register_dev_list, struct os_lpmgr_device, list)
    {
        os_kprintf("| %-5d | 0x%p | 0x%p |\r\n", index++, device_lpm->priv, device_lpm->ops);
    }

    os_kprintf("total register dev num: %d\r\n", index);

    index = 0;
    struct lpmgr_notify *lpm_notify;

    os_kprintf("| no    |    func    | data       |\r\n");
    os_kprintf("+-------+------------+------------+\r\n");
    os_list_for_each_entry(lpm_notify, &gs_lpmgr_notify_list, struct lpmgr_notify, list)
    {
        os_kprintf("| %-5d | 0x%p | 0x%p |\r\n", index++, lpm_notify->notify, lpm_notify->data);
    }

    os_kprintf("total register notify num: %d\r\n", index);
}
SH_CMD_EXPORT(power_status, lpmgr_dump_status, "dump power management status");

#endif
