/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        mo_bluetooth.c
 *
 * @brief       module link kit ble api declaration
 *
 * @revision
 * Date         Author          Notes
 * 2021-10-13   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include "mo_bluetooth.h"
#include <stdlib.h>

#define MO_LOG_TAG "molink.bt"
#define MO_LOG_LVL MO_LOG_INFO
#include "mo_log.h"

#ifdef MOLINK_USING_BLUETOOTH_OPS

static mo_bt_ops_t *get_bt_ops(mo_object_t *self)
{
    mo_bt_ops_t *ops = (mo_bt_ops_t *)self->ops_table[MODULE_OPS_BLUETOOTH];

    if (OS_NULL == ops)
    {
        ERROR("Module %s does not support bluetooth operates", self->name);
    }

    return ops;
}

/**
 ***********************************************************************************************************************
 * @brief           Execute AT command to set bt uart baudrate
 *
 * @param[in]       self            The descriptor of molink module instance
 * @param[in]       baudrate        The uart baudrate to set
 *
 * @return          On success, return OS_SUCCESS; on error, return a error code.
 * @retval          OS_SUCCESS      Set baudrate successfully
 * @retval          OS_FAILURE      Set baudrate error
 * @retval          OS_TIMEOUT      Set baudrate timeout
 ***********************************************************************************************************************
 */
os_err_t mo_bt_baudrate_set(mo_object_t *self, uint32_t baudrate)
{
    OS_ASSERT(OS_NULL != self);

    mo_bt_ops_t *ops = get_bt_ops(self);

    if (OS_NULL == ops)
    {
        return OS_FAILURE;
    }

    if (OS_NULL == ops->bt_baudrate_set)
    {
        ERROR("Module %s does not support bt uart baudrate set operate", self->name);
        return OS_FAILURE;
    }

    return ops->bt_baudrate_set(self, baudrate);
}

/**
 ***********************************************************************************************************************
 * @brief           Execute AT command to get bt uart baudrate
 *
 * @param[in]       self            The descriptor of molink module instance
 * @param[in]       baudrate_buf    The buffer to store uart baudrate
 *
 * @return          On success, return OS_SUCCESS; on error, return a error code.
 * @retval          OS_SUCCESS      Get baudrate successfully
 * @retval          OS_FAILURE      Get baudrate error
 * @retval          OS_TIMEOUT      Get baudrate timeout
 ***********************************************************************************************************************
 */
os_err_t mo_bt_baudrate_get(mo_object_t *self, uint32_t *baudrate_buf)
{
    OS_ASSERT(OS_NULL != self);
    OS_ASSERT(OS_NULL != baudrate_buf);

    mo_bt_ops_t *ops = get_bt_ops(self);

    if (OS_NULL == ops)
    {
        return OS_FAILURE;
    }

    if (OS_NULL == ops->bt_baudrate_get)
    {
        ERROR("Module %s does not support bt uart baudrate get operate", self->name);
        return OS_FAILURE;
    }

    return ops->bt_baudrate_get(self, baudrate_buf);
}

/**
 ***********************************************************************************************************************
 * @brief           Execute AT command to set bt device name
 *
 * @param[in]       self            The descriptor of molink module instance
 * @param[in]       dev_name        The bluetooth device Ctype string name to set
 *
 * @return          On success, return OS_SUCCESS; on error, return a error code.
 * @retval          OS_SUCCESS      Set device name successfully
 * @retval          OS_FAILURE      Set device name error
 * @retval          OS_TIMEOUT      Set device name timeout
 ***********************************************************************************************************************
 */
os_err_t mo_bt_device_name_set(mo_object_t *self, char *dev_name, uint32_t dev_name_len)
{
    OS_ASSERT(OS_NULL != self);
    OS_ASSERT(OS_NULL != dev_name);

    mo_bt_ops_t *ops = get_bt_ops(self);

    if (OS_NULL == ops)
    {
        return OS_FAILURE;
    }

    if (OS_NULL == ops->bt_device_name_set)
    {
        ERROR("Module %s does not support bt device name set operate", self->name);
        return OS_FAILURE;
    }

    return ops->bt_device_name_set(self, dev_name, dev_name_len);
}

/**
 ***********************************************************************************************************************
 * @brief           Execute AT command to get bt device name
 *
 * @param[in]       self            The descriptor of molink module instance
 * @param[out]      dev_name_buf    The buffer to store bluetooth device Ctype string name
 *
 * @return          On success, return OS_SUCCESS; on error, return a error code.
 * @retval          OS_SUCCESS      Get device name successfully
 * @retval          OS_FAILURE      Get device name error
 * @retval          OS_TIMEOUT      Get device name timeout
 ***********************************************************************************************************************
 */
os_err_t mo_bt_device_name_get(mo_object_t *self, char *dev_name_buf, uint32_t dev_name_buf_len)
{
    OS_ASSERT(OS_NULL != self);
    OS_ASSERT(OS_NULL != dev_name_buf);

    mo_bt_ops_t *ops = get_bt_ops(self);

    if (OS_NULL == ops)
    {
        return OS_FAILURE;
    }

    if (OS_NULL == ops->bt_device_name_get)
    {
        ERROR("Module %s does not support bt device name get operate", self->name);
        return OS_FAILURE;
    }

    return ops->bt_device_name_get(self, dev_name_buf, dev_name_buf_len);
}

/**
 ***********************************************************************************************************************
 * @brief           Execute AT command to set bt MAC
 *
 * @param[in]       self            The descriptor of molink module instance
 * @param[in]       bt_mac          The MAC string to set(without'\:')
 *
 * @return          On success, return OS_SUCCESS; on error, return a error code.
 * @retval          OS_SUCCESS      Set MAC successfully
 * @retval          OS_FAILURE      Set MAC error
 * @retval          OS_TIMEOUT      Set MAC timeout
 ***********************************************************************************************************************
 */
os_err_t mo_bt_mac_set(mo_object_t *self, char bt_mac[BT_MAX_HWADDR_STR_LEN])
{
    OS_ASSERT(OS_NULL != self);
    OS_ASSERT(OS_NULL != bt_mac);

    mo_bt_ops_t *ops = get_bt_ops(self);

    if (OS_NULL == ops)
    {
        return OS_FAILURE;
    }

    if (OS_NULL == ops->bt_mac_set)
    {
        ERROR("Module %s does not support bt mac set operate", self->name);
        return OS_FAILURE;
    }

    return ops->bt_mac_set(self, bt_mac);
}

/**
 ***********************************************************************************************************************
 * @brief           Execute AT command to get bt MAC
 *
 * @param[in]       self            The descriptor of molink module instance
 * @param[in]       bt_mac_buf      The buffer to store bluetooth MAC string(without '\:')
 *
 * @return          On success, return OS_SUCCESS; on error, return a error code.
 * @retval          OS_SUCCESS      Get MAC successfully
 * @retval          OS_FAILURE      Get MAC error
 * @retval          OS_TIMEOUT      Get MAC timeout
 ***********************************************************************************************************************
 */
os_err_t mo_bt_mac_get(mo_object_t *self, char bt_mac_buf[BT_MAX_HWADDR_STR_LEN])
{
    OS_ASSERT(OS_NULL != self);
    OS_ASSERT(OS_NULL != bt_mac_buf);

    mo_bt_ops_t *ops = get_bt_ops(self);

    if (OS_NULL == ops)
    {
        return OS_FAILURE;
    }

    if (OS_NULL == ops->bt_mac_get)
    {
        ERROR("Module %s does not support bt mac get operate", self->name);
        return OS_FAILURE;
    }

    return ops->bt_mac_get(self, bt_mac_buf);
}

/**
 ***********************************************************************************************************************
 * @brief           Execute AT command to set bt adv mode
 *
 * @param[in]       self            The descriptor of molink module instance
 * @param[in]       adv_mode        The bt adv mode to set
 *
 * @return          On success, return OS_SUCCESS; on error, return a error code.
 * @retval          OS_SUCCESS      Set adv mode successfully
 * @retval          OS_FAILURE      Set adv mode error
 * @retval          OS_TIMEOUT      Set adv mode timeout
 ***********************************************************************************************************************
 */
os_err_t mo_bt_adv_mode_set(mo_object_t *self, bt_adv_mode_t adv_mode)
{
    OS_ASSERT(OS_NULL != self);

    mo_bt_ops_t *ops = get_bt_ops(self);

    if (OS_NULL == ops)
    {
        return OS_FAILURE;
    }

    if (OS_NULL == ops->bt_adv_mode_set)
    {
        ERROR("Module %s does not support bt adv mode set operate", self->name);
        return OS_FAILURE;
    }

    return ops->bt_adv_mode_set(self, adv_mode);
}

/**
 ***********************************************************************************************************************
 * @brief           Execute AT command to get bt adv mode
 *
 * @param[in]       self            The descriptor of molink module instance
 * @param[in]       adv_mode_buf    The buffer to store bt adv mode
 *
 * @return          On success, return OS_SUCCESS; on error, return a error code.
 * @retval          OS_SUCCESS      Get adv mode successfully
 * @retval          OS_FAILURE      Get adv mode error
 * @retval          OS_TIMEOUT      Get adv mode timeout
 ***********************************************************************************************************************
 */
os_err_t mo_bt_adv_mode_get(mo_object_t *self, bt_adv_mode_t *adv_mode_buf)
{
    OS_ASSERT(OS_NULL != self);
    OS_ASSERT(OS_NULL != adv_mode_buf);

    mo_bt_ops_t *ops = get_bt_ops(self);

    if (OS_NULL == ops)
    {
        return OS_FAILURE;
    }

    if (OS_NULL == ops->bt_adv_mode_get)
    {
        ERROR("Module %s does not support bt adv mode get operate", self->name);
        return OS_FAILURE;
    }

    return ops->bt_adv_mode_get(self, adv_mode_buf);
}

/**
 ***********************************************************************************************************************
 * @brief           Execute AT command to set bt adv interval
 *
 * @param[in]       self            The descriptor of molink module instance
 * @param[in]       interval        The bt adv interval to set [unit:0.625ms]
 *
 * @return          On success, return OS_SUCCESS; on error, return a error code.
 * @retval          OS_SUCCESS      Set adv interval successfully
 * @retval          OS_FAILURE      Set adv interval error
 * @retval          OS_TIMEOUT      Set adv interval timeout
 ***********************************************************************************************************************
 */
os_err_t mo_bt_adv_interval_set(mo_object_t *self, uint32_t interval)
{
    OS_ASSERT(OS_NULL != self);

    mo_bt_ops_t *ops = get_bt_ops(self);

    if (OS_NULL == ops)
    {
        return OS_FAILURE;
    }

    if (OS_NULL == ops->bt_adv_interval_set)
    {
        ERROR("Module %s does not support bt adv interval set operate", self->name);
        return OS_FAILURE;
    }

    return ops->bt_adv_interval_set(self, interval);
}

/**
 ***********************************************************************************************************************
 * @brief           Execute AT command to get bt adv interval
 *
 * @param[in]       self            The descriptor of molink module instance
 * @param[in]       interval_buf    The buffer to store bt adv interval [unit:0.625ms]
 *
 * @return          On success, return OS_SUCCESS; on error, return a error code.
 * @retval          OS_SUCCESS      Get adv interval successfully
 * @retval          OS_FAILURE      Get adv interval error
 * @retval          OS_TIMEOUT      Get adv interval timeout
 ***********************************************************************************************************************
 */
os_err_t mo_bt_adv_interval_get(mo_object_t *self, uint32_t *interval_buf)
{
    OS_ASSERT(OS_NULL != self);
    OS_ASSERT(OS_NULL != interval_buf);

    mo_bt_ops_t *ops = get_bt_ops(self);

    if (OS_NULL == ops)
    {
        return OS_FAILURE;
    }

    if (OS_NULL == ops->bt_adv_interval_get)
    {
        ERROR("Module %s does not support bt adv interval get operate", self->name);
        return OS_FAILURE;
    }

    return ops->bt_adv_interval_get(self, interval_buf);
}

/**
 ***********************************************************************************************************************
 * @brief           Execute AT command to set bt connect interval
 *
 * @param[in]       self            The descriptor of molink module instance
 * @param[in]       interval        The bt connect interval to set. [unit:0.625ms]
 *
 * @return          On success, return OS_SUCCESS; on error, return a error code.
 * @retval          OS_SUCCESS      Set connect interval successfully
 * @retval          OS_FAILURE      Set connect interval error
 * @retval          OS_TIMEOUT      Set connect interval timeout
 ***********************************************************************************************************************
 */
os_err_t mo_bt_connect_interval_set(mo_object_t *self, uint32_t interval)
{
    OS_ASSERT(OS_NULL != self);

    mo_bt_ops_t *ops = get_bt_ops(self);

    if (OS_NULL == ops)
    {
        return OS_FAILURE;
    }

    if (OS_NULL == ops->bt_connect_interval_set)
    {
        ERROR("Module %s does not support bt connect interval set operate", self->name);
        return OS_FAILURE;
    }

    return ops->bt_connect_interval_set(self, interval);
}

/**
 ***********************************************************************************************************************
 * @brief           Execute AT command to get bt connect interval
 *
 * @param[in]       self            The descriptor of molink module instance
 * @param[in]       interval_buf    The buffer to store bt connect interval [unit:0.625ms]
 *
 * @return          On success, return OS_SUCCESS; on error, return a error code.
 * @retval          OS_SUCCESS      Get connect interval successfully
 * @retval          OS_FAILURE      Get connect interval error
 * @retval          OS_TIMEOUT      Get connect interval timeout
 ***********************************************************************************************************************
 */
os_err_t mo_bt_connect_interval_get(mo_object_t *self, uint32_t *interval_buf)
{
    OS_ASSERT(OS_NULL != self);
    OS_ASSERT(OS_NULL != interval_buf);

    mo_bt_ops_t *ops = get_bt_ops(self);

    if (OS_NULL == ops)
    {
        return OS_FAILURE;
    }

    if (OS_NULL == ops->bt_connect_interval_get)
    {
        ERROR("Module %s does not support bt connect interval get operate", self->name);
        return OS_FAILURE;
    }

    return ops->bt_connect_interval_get(self, interval_buf);
}

/**
 ***********************************************************************************************************************
 * @brief           Execute AT command to set bt pin code
 *
 * @param[in]       self            The descriptor of molink module instance
 * @param[in]       bt_pin          The Ctype string of bt pin code to set
 *
 * @return          On success, return OS_SUCCESS; on error, return a error code.
 * @retval          OS_SUCCESS      Set pin code successfully
 * @retval          OS_FAILURE      Set pin code error
 * @retval          OS_TIMEOUT      Set pin code timeout
 ***********************************************************************************************************************
 */
os_err_t mo_bt_pin_set(mo_object_t *self, char bt_pin[BT_PIN_STR_LEN])
{
    OS_ASSERT(OS_NULL != self);
    OS_ASSERT(OS_NULL != bt_pin);

    mo_bt_ops_t *ops = get_bt_ops(self);

    if (OS_NULL == ops)
    {
        return OS_FAILURE;
    }

    if (OS_NULL == ops->bt_pin_set)
    {
        ERROR("Module %s does not support bt pin set operate", self->name);
        return OS_FAILURE;
    }

    return ops->bt_pin_set(self, bt_pin);
}

/**
 ***********************************************************************************************************************
 * @brief           Execute AT command to get bt pin code
 *
 * @param[in]       self            The descriptor of molink module instance
 * @param[in]       bt_pin_buf      The buffer to store Ctype string bt pin code
 *
 * @return          On success, return OS_SUCCESS; on error, return a error code.
 * @retval          OS_SUCCESS      Get pin code successfully
 * @retval          OS_FAILURE      Get pin code error
 * @retval          OS_TIMEOUT      Get pin code timeout
 ***********************************************************************************************************************
 */
os_err_t mo_bt_pin_get(mo_object_t *self, char bt_pin_buf[BT_PIN_STR_LEN])
{
    OS_ASSERT(OS_NULL != self);
    OS_ASSERT(OS_NULL != bt_pin_buf);

    mo_bt_ops_t *ops = get_bt_ops(self);

    if (OS_NULL == ops)
    {
        return OS_FAILURE;
    }

    if (OS_NULL == ops->bt_pin_get)
    {
        ERROR("Module %s does not support bt pin get operate", self->name);
        return OS_FAILURE;
    }

    return ops->bt_pin_get(self, bt_pin_buf);
}

/**
 ***********************************************************************************************************************
 * @brief           Execute AT command to set bt pin mode
 *
 * @param[in]       self            The descriptor of molink module instance
 * @param[in]       bt_pin          The bt pin mode to set
 *
 * @return          On success, return OS_SUCCESS; on error, return a error code.
 * @retval          OS_SUCCESS      Set pin mode successfully
 * @retval          OS_FAILURE      Set pin mode error
 * @retval          OS_TIMEOUT      Set pin mode timeout
 ***********************************************************************************************************************
 */
os_err_t mo_bt_pin_mode_set(mo_object_t *self, bt_pin_mode_t pin_mode)
{
    OS_ASSERT(OS_NULL != self);

    mo_bt_ops_t *ops = get_bt_ops(self);

    if (OS_NULL == ops)
    {
        return OS_FAILURE;
    }

    if (OS_NULL == ops->bt_pin_mode_set)
    {
        ERROR("Module %s does not support bt pin mode set operate", self->name);
        return OS_FAILURE;
    }

    return ops->bt_pin_mode_set(self, pin_mode);
}

/**
 ***********************************************************************************************************************
 * @brief           Execute AT command to get bt pin mode
 *
 * @param[in]       self            The descriptor of molink module instance
 * @param[in]       pin_mode_buf    The buffer to store bt pin mode
 *
 * @return          On success, return OS_SUCCESS; on error, return a error code.
 * @retval          OS_SUCCESS      Get pin mode successfully
 * @retval          OS_FAILURE      Get pin mode error
 * @retval          OS_TIMEOUT      Get pin mode timeout
 ***********************************************************************************************************************
 */
os_err_t mo_bt_pin_mode_get(mo_object_t *self, bt_pin_mode_t *pin_mode_buf)
{
    OS_ASSERT(OS_NULL != self);
    OS_ASSERT(OS_NULL != pin_mode_buf);

    mo_bt_ops_t *ops = get_bt_ops(self);

    if (OS_NULL == ops)
    {
        return OS_FAILURE;
    }

    if (OS_NULL == ops->bt_pin_mode_get)
    {
        ERROR("Module %s does not support bt pin mode get operate", self->name);
        return OS_FAILURE;
    }

    return ops->bt_pin_mode_get(self, pin_mode_buf);
}

/**
 ***********************************************************************************************************************
 * @brief           Execute AT command to reboot bt module
 *
 * @param[in]       self            The descriptor of molink module instance
 *
 * @return          On success, return OS_SUCCESS; on error, return a error code.
 * @retval          OS_SUCCESS      Reboot successfully
 * @retval          OS_FAILURE      Reboot error
 * @retval          OS_TIMEOUT      Reboot timeout
 ***********************************************************************************************************************
 */
os_err_t mo_bt_reboot(mo_object_t *self)
{
    OS_ASSERT(OS_NULL != self);

    mo_bt_ops_t *ops = get_bt_ops(self);

    if (OS_NULL == ops)
    {
        return OS_FAILURE;
    }

    if (OS_NULL == ops->bt_reboot)
    {
        ERROR("Module %s does not support bt reboot operate", self->name);
        return OS_FAILURE;
    }

    return ops->bt_reboot(self);
}

/**
 ***********************************************************************************************************************
 * @brief           Execute AT command to disconnect
 *
 * @param[in]       self            The descriptor of molink module instance
 *
 * @return          On success, return OS_SUCCESS; on error, return a error code.
 * @retval          OS_SUCCESS      Disconnect successfully
 * @retval          OS_FAILURE      Disconnect error
 * @retval          OS_TIMEOUT      Disconnect timeout
 ***********************************************************************************************************************
 */
os_err_t mo_bt_disconnect(mo_object_t *self)
{
    OS_ASSERT(OS_NULL != self);

    mo_bt_ops_t *ops = get_bt_ops(self);

    if (OS_NULL == ops)
    {
        return OS_FAILURE;
    }

    if (OS_NULL == ops->bt_disconnect)
    {
        ERROR("Module %s does not support bt disconnect operate", self->name);
        return OS_FAILURE;
    }

    return ops->bt_disconnect(self);
}

/**
 ***********************************************************************************************************************
 * @brief           Execute AT command to set bt radio power level
 *
 * @param[in]       self            The descriptor of molink module instance
 * @param[in]       radio_level     The bt radio power level to set
 *
 * @return          On success, return OS_SUCCESS; on error, return a error code.
 * @retval          OS_SUCCESS      Set radio power level successfully
 * @retval          OS_FAILURE      Set radio power level error
 * @retval          OS_TIMEOUT      Set radio power level timeout
 ***********************************************************************************************************************
 */
os_err_t mo_bt_radio_power_level_set(mo_object_t *self, uint32_t radio_level)
{
    OS_ASSERT(OS_NULL != self);

    mo_bt_ops_t *ops = get_bt_ops(self);

    if (OS_NULL == ops)
    {
        return OS_FAILURE;
    }

    if (OS_NULL == ops->bt_radio_power_level_set)
    {
        ERROR("Module %s does not support bt radio power level set operate", self->name);
        return OS_FAILURE;
    }

    return ops->bt_radio_power_level_set(self, radio_level);
}

/**
 ***********************************************************************************************************************
 * @brief           Execute AT command to get bt radio power level
 *
 * @param[in]       self            The descriptor of molink module instance
 * @param[in]       radio_level_buf The buffer to store bt radio power level
 *
 * @return          On success, return OS_SUCCESS; on error, return a error code.
 * @retval          OS_SUCCESS      Get radio power level successfully
 * @retval          OS_FAILURE      Get radio power level error
 * @retval          OS_TIMEOUT      Get radio power level timeout
 ***********************************************************************************************************************
 */
os_err_t mo_bt_radio_power_level_get(mo_object_t *self, uint32_t *radio_level_buf)
{
    OS_ASSERT(OS_NULL != self);
    OS_ASSERT(OS_NULL != radio_level_buf);

    mo_bt_ops_t *ops = get_bt_ops(self);

    if (OS_NULL == ops)
    {
        return OS_FAILURE;
    }

    if (OS_NULL == ops->bt_radio_power_level_get)
    {
        ERROR("Module %s does not support bt radio power level get operate", self->name);
        return OS_FAILURE;
    }

    return ops->bt_radio_power_level_get(self, radio_level_buf);
}

#endif /* MOLINK_USING_BLUETOOTH_OPS */
