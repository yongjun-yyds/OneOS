/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        mo_bluetooth.h
 *
 * @brief       module link kit bt api declaration
 *
 * @revision
 * Date         Author          Notes
 * 2021-10-13   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __MO_BLUETOOTH_H__
#define __MO_BLUETOOTH_H__

#include "mo_type.h"
#include "mo_object.h"

#ifdef MOLINK_USING_BLUETOOTH_OPS

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define BT_MAX_HWADDR_LEN     (12)
#define BT_MAX_HWADDR_STR_LEN (1 + BT_MAX_HWADDR_LEN)
#define BT_PIN_LEN            (6)
#define BT_PIN_STR_LEN        (1 + BT_PIN_LEN)

/**
 ***********************************************************************************************************************
 * @enum        mo_bt_mode_t
 *
 * @brief       molink bluetooth module working mode
 ***********************************************************************************************************************
 */
typedef enum bt_mode
{
    BT_MODE_CMD = 0,
    BT_MODE_PASSTHROUGH,
    BT_MODE_SLEEP,
    BT_MODE_SLEEP_PASSTHROUGH,
    BT_MODE_NONE,
} bt_mode_t;

/**
 ***********************************************************************************************************************
 * @enum        bt_adv_mode_t
 *
 * @brief       molink bluetooth advertising mode
 ***********************************************************************************************************************
 */
typedef enum bt_adv_mode
{
    BT_ADV_DISABLE = 0,
    BT_ADV_ENABLE,
    BT_ADV_RESERVED,
} bt_adv_mode_t;

/**
 ***********************************************************************************************************************
 * @enum        bt_pin_mode_t
 *
 * @brief       molink bluetooth PIN mode
 ***********************************************************************************************************************
 */
typedef enum bt_pin_mode
{
    BT_PIN_DISABLE = 0,
    BT_PIN_ENABLE,
    BT_PIN_RESERVED,
} bt_pin_mode_t;

/**
 ***********************************************************************************************************************
 * @struct      mo_bt_ops_t
 *
 * @brief       molink module bluetooth ops table
 ***********************************************************************************************************************
 */
typedef struct mo_bt_ops
{
    os_err_t (*bt_baudrate_set)(mo_object_t *self, uint32_t baudrate);
    os_err_t (*bt_baudrate_get)(mo_object_t *self, uint32_t *baudrate_buf);
    os_err_t (*bt_device_name_set)(mo_object_t *self, char *dev_name, uint32_t dev_name_len);
    os_err_t (*bt_device_name_get)(mo_object_t *self, char *dev_name_buf, uint32_t dev_name_buf_len);
    os_err_t (*bt_mac_set)(mo_object_t *self, char bt_mac[BT_MAX_HWADDR_STR_LEN]);
    os_err_t (*bt_mac_get)(mo_object_t *self, char bt_mac_buf[BT_MAX_HWADDR_STR_LEN]);
    os_err_t (*bt_adv_mode_set)(mo_object_t *self, bt_adv_mode_t adv_mode);
    os_err_t (*bt_adv_mode_get)(mo_object_t *self, bt_adv_mode_t *adv_mode_buf);
    os_err_t (*bt_adv_interval_set)(mo_object_t *self, uint32_t interval);
    os_err_t (*bt_adv_interval_get)(mo_object_t *self, uint32_t *interval_buf);
    os_err_t (*bt_connect_interval_set)(mo_object_t *self, uint32_t interval);
    os_err_t (*bt_connect_interval_get)(mo_object_t *self, uint32_t *interval_buf);
    os_err_t (*bt_pin_set)(mo_object_t *self, char bt_pin[BT_PIN_STR_LEN]);
    os_err_t (*bt_pin_get)(mo_object_t *self, char bt_pin_buf[BT_PIN_STR_LEN]);
    os_err_t (*bt_pin_mode_set)(mo_object_t *self, bt_pin_mode_t pin_mode);
    os_err_t (*bt_pin_mode_get)(mo_object_t *self, bt_pin_mode_t *pin_mode_buf);
    os_err_t (*bt_reboot)(mo_object_t *self);
    os_err_t (*bt_disconnect)(mo_object_t *self);
    os_err_t (*bt_radio_power_level_set)(mo_object_t *self, uint32_t radio_level);
    os_err_t (*bt_radio_power_level_get)(mo_object_t *self, uint32_t *radio_level_buf);
} mo_bt_ops_t;

#if 0
/* NOTE THIS TWO API MOVE INTO ADAPT LAYER */
os_err_t mo_bt_mode_set(mo_object_t *self, bt_mode_t mode);
os_err_t mo_bt_mode_get(mo_object_t *self, bt_mode_t *mode);
#endif

os_err_t mo_bt_baudrate_set(mo_object_t *self, uint32_t baudrate);
os_err_t mo_bt_baudrate_get(mo_object_t *self, uint32_t *baudrate_buf);
os_err_t mo_bt_device_name_set(mo_object_t *self, char *dev_name, uint32_t dev_name_len);
os_err_t mo_bt_device_name_get(mo_object_t *self, char *dev_name_buf, uint32_t dev_name_buf_len);
os_err_t mo_bt_mac_set(mo_object_t *self, char bt_mac[BT_MAX_HWADDR_LEN]);
os_err_t mo_bt_mac_get(mo_object_t *self, char bt_mac_buf[BT_MAX_HWADDR_LEN]);
os_err_t mo_bt_adv_mode_set(mo_object_t *self, bt_adv_mode_t adv_mode);
os_err_t mo_bt_adv_mode_get(mo_object_t *self, bt_adv_mode_t *adv_mode_buf);
os_err_t mo_bt_adv_interval_set(mo_object_t *self, uint32_t interval);
os_err_t mo_bt_adv_interval_get(mo_object_t *self, uint32_t *interval_buf);
os_err_t mo_bt_connect_interval_set(mo_object_t *self, uint32_t interval);
os_err_t mo_bt_connect_interval_get(mo_object_t *self, uint32_t *interval_buf);
os_err_t mo_bt_pin_set(mo_object_t *self, char bt_pin[BT_PIN_STR_LEN]);
os_err_t mo_bt_pin_get(mo_object_t *self, char bt_pin_buf[BT_PIN_STR_LEN]);
os_err_t mo_bt_pin_mode_set(mo_object_t *self, bt_pin_mode_t pin_mode);
os_err_t mo_bt_pin_mode_get(mo_object_t *self, bt_pin_mode_t *pin_mode_buf);

os_err_t mo_bt_reboot(mo_object_t *self);
os_err_t mo_bt_disconnect(mo_object_t *self);
os_err_t mo_bt_radio_power_level_set(mo_object_t *self, uint32_t radio_level);
os_err_t mo_bt_radio_power_level_get(mo_object_t *self, uint32_t *radio_level_buf);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* MOLINK_USING_BLUETOOTH_OPS */

#endif /* __MO_BLUETOOTH_H__ */
