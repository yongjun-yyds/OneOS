/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        yq8228.c
 *
 * @brief       yq8228 factory mode api
 *
 * @revision
 * Date         Author          Notes
 * 2020-10-21   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include "yq8228.h"

#include <stdlib.h>
#include <string.h>
#include <stddef.h>
#include <os_memory.h>

#define MO_LOG_TAG "yq8228"
#define MO_LOG_LVL MO_LOG_DEBUG
#include "mo_log.h"

#ifdef MOLINK_USING_YQ8228

#ifdef QY8228_USING_PIN_CTL
extern void yq8228_enter_cmd_mode(void);
extern void yq8228_exit_cmd_mode(void);
#endif /* QY8228_USING_PIN_CTL */

#define YQ8228_RETRY_TIMES (10)

#ifdef YQ8228_USING_GENERAL_OPS
static const struct mo_general_ops gs_general_ops = {
    .get_firmware_version = yq8228_get_firmware_version,
};
#endif /* YQ8228_USING_GENERAL_OPS */

#ifdef YQ8228_USING_BLUETOOTH_OPS
static const struct mo_bt_ops gs_bluetooth_ops = {
    .bt_baudrate_set          = yq8228_bt_baudrate_set,
    .bt_baudrate_get          = yq8228_bt_baudrate_get,
    .bt_device_name_set       = yq8228_bt_device_name_set,
    .bt_device_name_get       = yq8228_bt_device_name_get,
    .bt_mac_set               = yq8228_bt_mac_set,
    .bt_mac_get               = yq8228_bt_mac_get,
    .bt_adv_mode_set          = yq8228_bt_adv_mode_set,
    .bt_adv_mode_get          = yq8228_bt_adv_mode_get,
    .bt_adv_interval_set      = yq8228_bt_adv_interval_set,
    .bt_adv_interval_get      = yq8228_bt_adv_interval_get,
    .bt_connect_interval_set  = yq8228_bt_connect_interval_set,
    .bt_connect_interval_get  = yq8228_bt_connect_interval_get,
    .bt_pin_set               = yq8228_bt_pin_set,
    .bt_pin_get               = yq8228_bt_pin_get,
    .bt_pin_mode_set          = yq8228_bt_pin_mode_set,
    .bt_pin_mode_get          = yq8228_bt_pin_mode_get,
    .bt_reboot                = yq8228_bt_reboot,
    .bt_disconnect            = yq8228_bt_disconnect,
    .bt_radio_power_level_set = yq8228_bt_radio_power_level_set,
    .bt_radio_power_level_get = yq8228_bt_radio_power_level_get,
};
#endif /* YQ8228_USING_BLUETOOTH_OPS */

os_err_t yq8228_connection_check(mo_object_t *self)
{
    at_parser_t *parser = &self->parser;

    os_err_t result = at_parser_connect(parser, YQ8228_RETRY_TIMES);
    if (result != OS_SUCCESS)
    {
        ERROR("Connect to %s module failed, please check whether the module connection is correct", self->name);
    }

    return result;
}

mo_object_t *module_yq8228_create(const char *name, void *parser_config)
{
    os_err_t     result = OS_FAILURE;
    mo_yq8228_t *module = OS_NULL;

#ifdef QY8228_USING_PIN_CTL
    yq8228_enter_cmd_mode();
#endif /* QY8228_USING_PIN_CTL */

    module = (mo_yq8228_t *)os_calloc(1, sizeof(mo_yq8228_t));
    if (OS_NULL == module)
    {
        ERROR("Create YQ8228 failed, no enough memory.");
        return OS_NULL;
    }

    result = mo_object_init(&(module->parent), name, parser_config);
    if (result != OS_SUCCESS)
    {
        os_free(module);
        return OS_NULL;
    }

    result = yq8228_connection_check(&module->parent);
    if (result != OS_SUCCESS)
    {
        goto __exit;
    }

#ifdef YQ8228_USING_GENERAL_OPS
    module->parent.ops_table[MODULE_OPS_GENERAL] = &gs_general_ops;
#endif /* YQ8228_USING_GENERAL_OPS */

#ifdef YQ8228_USING_BLUETOOTH_OPS
    module->parent.ops_table[MODULE_OPS_BLUETOOTH] = &gs_bluetooth_ops;
#endif /* YQ8228_USING_BLUETOOTH_OPS */

__exit:
    if (OS_SUCCESS != result)
    {
        if (mo_object_get_by_name(name) != OS_NULL)
        {
            mo_object_deinit(&module->parent);
        }

        os_free(module);

#ifdef QY8228_USING_PIN_CTL
        yq8228_exit_cmd_mode();
#endif /* QY8228_USING_PIN_CTL */

        return OS_NULL;
    }

    return &(module->parent);
}

os_err_t module_yq8228_destroy(mo_object_t *self)
{
    mo_yq8228_t *module = os_container_of(self, mo_yq8228_t, parent);

    mo_object_deinit(self);
    os_free(module);

#ifdef QY8228_USING_PIN_CTL
    yq8228_exit_cmd_mode();
#endif /* QY8228_USING_PIN_CTL */

    return OS_SUCCESS;
}

#ifdef YQ8228_AUTO_CREATE
#include <serial.h>

static struct serial_configure uart_config = OS_SERIAL_CONFIG_DEFAULT;

int yq8228_auto_create(void)
{
    os_device_t *device = os_device_find(YQ8228_DEVICE_NAME);

    if (OS_NULL == device)
    {
        ERROR("Auto create failed, Can not find YQ8228 interface device %s!", YQ8228_DEVICE_NAME);
        return OS_FAILURE;
    }

    uart_config.baud_rate = YQ8228_DEVICE_RATE;

    os_device_control(device, OS_DEVICE_CTRL_CONFIG, &uart_config);

    mo_parser_config_t parser_config = {.parser_name   = YQ8228_NAME,
                                        .parser_device = device,
                                        .recv_buff_len = YQ8228_RECV_BUFF_LEN};

    mo_object_t *module = module_yq8228_create(YQ8228_NAME, &parser_config);

    if (OS_NULL == module)
    {
        ERROR("Auto create failed, Can not create %s module object!", YQ8228_NAME);
        return OS_FAILURE;
    }

    INFO("Auto create %s module object success!", YQ8228_NAME);
    return OS_SUCCESS;
}
OS_INIT_CALL(yq8228_auto_create, OS_INIT_LEVEL_COMPONENT, OS_INIT_SUBLEVEL_MIDDLE);

#endif /* YQ8228_AUTO_CREATE */

#endif /* MOLINK_USING_YQ8228 */
