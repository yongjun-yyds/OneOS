/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        yq8228_bt.c
 *
 * @brief       yq8228 module link kit bluetooth api
 *
 * @revision
 * Date         Author          Notes
 * 2020-10-21   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include "yq8228_bt.h"
#include <string.h>
#include <os_assert.h>
#include <serial.h>

static struct serial_configure gs_uart_reset_cfg = OS_SERIAL_CONFIG_DEFAULT;

#define MO_LOG_TAG "yq8228.bt"
#define MO_LOG_LVL MO_LOG_DEBUG
#include "mo_log.h"

#ifdef YQ8228_USING_BLUETOOTH_OPS

#define YQ8228_BT_PWR_MAX     (15)
#define YQ8228_BT_PWR_MIN     (1)
#define YQ8228_BT_TIMEOUT_DFT (1 * OS_TICK_PER_SECOND)

extern os_err_t at_parser_unbind_device(at_parser_t *parser);
extern os_err_t at_parser_rebind_device(at_parser_t *parser);
extern os_err_t yq8228_connection_check(mo_object_t *self);

os_err_t yq8228_bt_baudrate_set(mo_object_t *module, uint32_t baudrate)
{
    at_parser_t *parser     = &module->parser;
    os_err_t     result     = OS_SUCCESS;
    uint32_t     value_read = 0;

    char resp_buff[AT_RESP_BUFF_SIZE_DEF] = {0};

    at_resp_t resp = {.buff      = resp_buff,
                      .buff_size = sizeof(resp_buff),
                      .line_num  = 1,
                      .timeout   = YQ8228_BT_TIMEOUT_DFT};

    result = at_parser_exec_cmd(parser, &resp, "AT+BDBAUD=%u", baudrate);
    if (result != OS_SUCCESS)
    {
        return result;
    }

    if (at_resp_get_data_by_kw(&resp, "+SET BAUD=", "+SET BAUD=%u", &value_read) <= 0)
    {
        ERROR("Get %s module baudrate value failed", module->name);
        return OS_FAILURE;
    }

    if (baudrate != value_read)
    {
        ERROR("Set %s module baudrate failed", module->name);
    }
    else
    {
        /* Restart device only for DAIAO */
        DEBUG("module %s start restart device with baudrate:%u", module->name, value_read);
        result = at_parser_unbind_device(parser);
        OS_ASSERT(OS_SUCCESS == result);

        gs_uart_reset_cfg.baud_rate = value_read;
        os_device_control(parser->device, OS_DEVICE_CTRL_CONFIG, &gs_uart_reset_cfg);

        result = at_parser_rebind_device(parser);
        OS_ASSERT(OS_SUCCESS == result);

        at_parser_exec_unlock(parser);

        DEBUG("Test module %s connection status", module->name);

        result = yq8228_connection_check(module);
        OS_ASSERT(OS_SUCCESS == result);

        INFO("module %s restart device OK", module->name);
    }

    return OS_SUCCESS;
}

os_err_t yq8228_bt_baudrate_get(mo_object_t *module, uint32_t *baudrate_buf)
{
    at_parser_t *parser = &module->parser;
    os_err_t     result = OS_SUCCESS;

    char resp_buff[AT_RESP_BUFF_SIZE_DEF] = {0};

    at_resp_t resp = {.buff      = resp_buff,
                      .buff_size = sizeof(resp_buff),
                      .line_num  = 1,
                      .timeout   = YQ8228_BT_TIMEOUT_DFT};

    result = at_parser_exec_cmd(parser, &resp, "AT+BDBAUD");
    if (result != OS_SUCCESS)
    {
        return result;
    }

    if (at_resp_get_data_by_kw(&resp, "+BAUD=", "+BAUD=%u", baudrate_buf) <= 0)
    {
        ERROR("Get %s module baudrate value failed", module->name);
        return OS_FAILURE;
    }

    DEBUG("module %s baudrate:%u", module->name, *baudrate_buf);

    return OS_SUCCESS;
}

os_err_t yq8228_bt_device_name_set(mo_object_t *module, char *dev_name, uint32_t dev_name_len)
{
    at_parser_t *parser = &module->parser;
    os_err_t     result = OS_SUCCESS;

    char resp_buff[AT_RESP_BUFF_SIZE_DEF] = {0};
    /* we have got no idea the max name string len, so we set to 128 chars temporarily. */
    char name_buff[AT_RESP_BUFF_SIZE_128] = {0};

    at_resp_t resp = {.buff      = resp_buff,
                      .buff_size = sizeof(resp_buff),
                      .line_num  = 1,
                      .timeout   = YQ8228_BT_TIMEOUT_DFT};

    /* The temporary limit is 128. */
    if (AT_RESP_BUFF_SIZE_128 <= dev_name_len)
    {
        ERROR("Set %s module bluetooth name, name length invalid.", module->name);
        return OS_INVAL;
    }

    result = at_parser_exec_cmd(parser, &resp, "AT+NAME=%.*s", dev_name_len, dev_name);
    if (result != OS_SUCCESS)
    {
        return result;
    }

    if (at_resp_get_data_by_kw(&resp, "+SET NAME=", "+SET NAME=%[^\r]", name_buff) <= 0)
    {
        ERROR("Get %s module bluetooth set name failed", module->name);
        return OS_FAILURE;
    }

    if (0 != strncmp(name_buff, dev_name, dev_name_len))
    {
        ERROR("Set %s module bluetooth name failed, return value error.", module->name);
        return OS_FAILURE;
    }

    DEBUG("module %s set name:%s", module->name, name_buff);

    return OS_SUCCESS;
}

os_err_t yq8228_bt_device_name_get(mo_object_t *module, char *dev_name_buf, uint32_t dev_name_buf_len)
{
    at_parser_t *parser = &module->parser;
    os_err_t     result = OS_SUCCESS;

    char resp_buff[AT_RESP_BUFF_SIZE_DEF] = {0};
    /* we have got no idea the max name string len, so we set to 128 chars temporarily. */
    char name_buff[AT_RESP_BUFF_SIZE_128] = {0};

    at_resp_t resp = {.buff      = resp_buff,
                      .buff_size = sizeof(resp_buff),
                      .line_num  = 1,
                      .timeout   = YQ8228_BT_TIMEOUT_DFT};

    result = at_parser_exec_cmd(parser, &resp, "AT+NAME");
    if (result != OS_SUCCESS)
    {
        return result;
    }

    if (at_resp_get_data_by_kw(&resp, "+NAME=", "+NAME=%[^\r]", name_buff) <= 0)
    {
        ERROR("Get %s module bluetooth name failed", module->name);
        return OS_FAILURE;
    }

    /* C type string, 1byte for \0 */
    if (strlen(name_buff) >= dev_name_buf_len)
    {
        ERROR("Get %s module bluetooth name failed, buffer size not enough.", module->name);
        return OS_FAILURE;
    }

    memcpy(dev_name_buf, name_buff, 1 + strlen(name_buff));

    DEBUG("module %s set name:%s", module->name, dev_name_buf);

    return OS_SUCCESS;
}

os_err_t yq8228_bt_mac_set(mo_object_t *module, char bt_mac[BT_MAX_HWADDR_STR_LEN])
{
    at_parser_t *parser = &module->parser;
    os_err_t     result = OS_SUCCESS;

    char resp_buff[AT_RESP_BUFF_SIZE_DEF] = {0};
    char mac_buff[BT_MAX_HWADDR_STR_LEN]  = {0};

    at_resp_t resp = {.buff      = resp_buff,
                      .buff_size = sizeof(resp_buff),
                      .line_num  = 1,
                      .timeout   = YQ8228_BT_TIMEOUT_DFT};

    result = at_parser_exec_cmd(parser, &resp, "AT+MAC=%.*s", BT_MAX_HWADDR_LEN, bt_mac);
    if (result != OS_SUCCESS)
    {
        return result;
    }

    if (at_resp_get_data_by_kw(&resp, "+SET MAC=", "+SET MAC=%[^\r]", mac_buff) <= 0)
    {
        ERROR("Get %s module bluetooth set MAC failed", module->name);
        return OS_FAILURE;
    }

    if (0 != strncmp(mac_buff, bt_mac, BT_MAX_HWADDR_LEN))
    {
        ERROR("Set %s module bluetooth MAC failed, return value error.", module->name);
        return OS_FAILURE;
    }

    DEBUG("module %s set MAC:%s", module->name, mac_buff);

    return OS_SUCCESS;
}

os_err_t yq8228_bt_mac_get(mo_object_t *module, char bt_mac_buf[BT_MAX_HWADDR_STR_LEN])
{
    at_parser_t *parser = &module->parser;
    os_err_t     result = OS_SUCCESS;

    char resp_buff[AT_RESP_BUFF_SIZE_DEF] = {0};
    char mac_buff[BT_MAX_HWADDR_STR_LEN]  = {0};

    at_resp_t resp = {.buff      = resp_buff,
                      .buff_size = sizeof(resp_buff),
                      .line_num  = 1,
                      .timeout   = YQ8228_BT_TIMEOUT_DFT};

    result = at_parser_exec_cmd(parser, &resp, "AT+MAC");
    if (result != OS_SUCCESS)
    {
        return result;
    }

    if (at_resp_get_data_by_kw(&resp, "+MAC=", "+MAC=%[^\r]", mac_buff) <= 0)
    {
        ERROR("Get %s module bluetooth set MAC failed", module->name);
        return OS_FAILURE;
    }

    if (BT_MAX_HWADDR_LEN != strlen(mac_buff))
    {
        ERROR("Get %s module bluetooth MAC failed, return value error.", module->name);
        return OS_FAILURE;
    }

    memcpy(bt_mac_buf, mac_buff, BT_MAX_HWADDR_STR_LEN);
    DEBUG("module %s get MAC:%s", module->name, bt_mac_buf);

    return OS_SUCCESS;
}

os_err_t yq8228_bt_adv_mode_set(mo_object_t *module, bt_adv_mode_t adv_mode)
{
    at_parser_t *parser  = &module->parser;
    os_err_t     result  = OS_SUCCESS;
    uint32_t     adv_get = 0;

    char resp_buff[AT_RESP_BUFF_SIZE_DEF] = {0};

    at_resp_t resp = {.buff      = resp_buff,
                      .buff_size = sizeof(resp_buff),
                      .line_num  = 1,
                      .timeout   = YQ8228_BT_TIMEOUT_DFT};

    if (BT_ADV_DISABLE != adv_mode && BT_ADV_ENABLE != adv_mode)
    {
        ERROR("Error %s module's ADV mode", module->name);
        return result;
    }

    result = at_parser_exec_cmd(parser, &resp, "AT+ADV=%d", adv_mode);
    if (result != OS_SUCCESS)
    {
        return result;
    }

    if (at_resp_get_data_by_kw(&resp, "+SET ADV=", "+SET ADV=%u", &adv_get) <= 0)
    {
        ERROR("Get %s module bluetooth set ADV mode failed", module->name);
        return OS_FAILURE;
    }

    if (adv_mode != adv_get)
    {
        ERROR("Set %s module bluetooth ADV mode failed, return value error.", module->name);
        return OS_FAILURE;
    }

    DEBUG("module %s set ADV:%d", module->name, adv_mode);

    return OS_SUCCESS;
}

os_err_t yq8228_bt_adv_mode_get(mo_object_t *module, bt_adv_mode_t *adv_mode_buf)
{
    at_parser_t *parser  = &module->parser;
    os_err_t     result  = OS_SUCCESS;
    uint32_t     adv_get = 0;

    char resp_buff[AT_RESP_BUFF_SIZE_DEF] = {0};

    at_resp_t resp = {.buff      = resp_buff,
                      .buff_size = sizeof(resp_buff),
                      .line_num  = 1,
                      .timeout   = YQ8228_BT_TIMEOUT_DFT};

    result = at_parser_exec_cmd(parser, &resp, "AT+ADV");
    if (result != OS_SUCCESS)
    {
        return result;
    }

    if (at_resp_get_data_by_kw(&resp, "+ADV=", "+ADV=%u", &adv_get) <= 0)
    {
        ERROR("Get %s module bluetooth ADV mode failed", module->name);
        return OS_FAILURE;
    }

    if (BT_ADV_DISABLE != adv_get && BT_ADV_ENABLE != adv_get)
    {
        ERROR("Error %s module's ADV mode", module->name);
        return result;
    }

    *adv_mode_buf = (bt_adv_mode_t)adv_get;

    DEBUG("module %s get ADV mode:%d", module->name, *adv_mode_buf);

    return OS_SUCCESS;
}

os_err_t yq8228_bt_adv_interval_set(mo_object_t *module, uint32_t interval)
{
    at_parser_t *parser       = &module->parser;
    os_err_t     result       = OS_SUCCESS;
    uint32_t     interval_get = 0;

    char resp_buff[AT_RESP_BUFF_SIZE_DEF] = {0};

    at_resp_t resp = {.buff      = resp_buff,
                      .buff_size = sizeof(resp_buff),
                      .line_num  = 1,
                      .timeout   = YQ8228_BT_TIMEOUT_DFT};

    result = at_parser_exec_cmd(parser, &resp, "AT+ADVINT=%u", interval);
    if (result != OS_SUCCESS)
    {
        return result;
    }

    if (at_resp_get_data_by_kw(&resp, "+SET ADV INTV=", "+SET ADV INTV=%u", &interval_get) <= 0)
    {
        ERROR("Get %s module bluetooth set ADV interval value failed", module->name);
        return OS_FAILURE;
    }

    if (interval != interval_get)
    {
        ERROR("Set %s module bluetooth ADV interval failed, return value error.", module->name);
        return OS_FAILURE;
    }

    DEBUG("module %s set ADV interval:%u", module->name, interval_get);

    return OS_SUCCESS;
}

os_err_t yq8228_bt_adv_interval_get(mo_object_t *module, uint32_t *interval_buf)
{
    at_parser_t *parser = &module->parser;
    os_err_t     result = OS_SUCCESS;

    char resp_buff[AT_RESP_BUFF_SIZE_DEF] = {0};

    at_resp_t resp = {.buff      = resp_buff,
                      .buff_size = sizeof(resp_buff),
                      .line_num  = 1,
                      .timeout   = YQ8228_BT_TIMEOUT_DFT};

    result = at_parser_exec_cmd(parser, &resp, "AT+ADVINT");
    if (result != OS_SUCCESS)
    {
        return result;
    }

    if (at_resp_get_data_by_kw(&resp, "+ADV INTV=", "+ADV INTV=%u", interval_buf) <= 0)
    {
        ERROR("Get %s module bluetooth ADV interval value failed", module->name);
        return OS_FAILURE;
    }

    DEBUG("module %s ADV interval:%u", module->name, *interval_buf);

    return OS_SUCCESS;
}

os_err_t yq8228_bt_connect_interval_set(mo_object_t *module, uint32_t interval)
{
    at_parser_t *parser       = &module->parser;
    os_err_t     result       = OS_SUCCESS;
    uint32_t     interval_get = 0;

    char resp_buff[AT_RESP_BUFF_SIZE_DEF] = {0};

    at_resp_t resp = {.buff      = resp_buff,
                      .buff_size = sizeof(resp_buff),
                      .line_num  = 1,
                      .timeout   = YQ8228_BT_TIMEOUT_DFT};

    result = at_parser_exec_cmd(parser, &resp, "AT+CONNINT=%u", interval);
    if (result != OS_SUCCESS)
    {
        return result;
    }

    if (at_resp_get_data_by_kw(&resp, "+SET CONN INTV=", "+SET CONN INTV=%u", &interval_get) <= 0)
    {
        ERROR("Get %s module bluetooth set connect interval value failed", module->name);
        return OS_FAILURE;
    }

    if (interval != interval_get)
    {
        ERROR("Set %s module bluetooth connect interval failed, return value error.", module->name);
        return OS_FAILURE;
    }

    DEBUG("module %s set connect interval:%u", module->name, interval_get);

    return OS_SUCCESS;
}

os_err_t yq8228_bt_connect_interval_get(mo_object_t *module, uint32_t *interval_buf)
{
    at_parser_t *parser = &module->parser;
    os_err_t     result = OS_SUCCESS;

    char resp_buff[AT_RESP_BUFF_SIZE_DEF] = {0};

    at_resp_t resp = {.buff      = resp_buff,
                      .buff_size = sizeof(resp_buff),
                      .line_num  = 1,
                      .timeout   = YQ8228_BT_TIMEOUT_DFT};

    result = at_parser_exec_cmd(parser, &resp, "AT+CONNINT");
    if (result != OS_SUCCESS)
    {
        return result;
    }

    if (at_resp_get_data_by_kw(&resp, "+CONN INTV=", "+CONN INTV=%u", interval_buf) <= 0)
    {
        ERROR("Get %s module bluetooth connect interval value failed", module->name);
        return OS_FAILURE;
    }

    DEBUG("module %s connect interval:%u", module->name, *interval_buf);

    return OS_SUCCESS;
}

os_err_t yq8228_bt_pin_set(mo_object_t *module, char bt_pin[BT_PIN_STR_LEN])
{
    at_parser_t *parser = &module->parser;
    os_err_t     result = OS_SUCCESS;
    uint32_t     count  = 0;

    char resp_buff[AT_RESP_BUFF_SIZE_DEF] = {0};
    char pin_buff[BT_PIN_STR_LEN]         = {0};

    /* After test, yq8228 accepts the pin code and convert to int32, there is no limitation of the pin code length.
    ** But after we set none 6 length of pin code, the module started working unstable, any connection will be rejected.
    */

    if (BT_PIN_LEN != strlen(bt_pin))
    {
        ERROR("%s module bluetooth PIN code length invalid, pin code combined with 6 numbers.", module->name);
        return OS_FAILURE;
    }

    while (BT_PIN_LEN != count)
    {
        if ('9' < bt_pin[count] || '0' > bt_pin[count])
        {
            ERROR("%s module bluetooth PIN invalid.", module->name);
            return OS_FAILURE;
        }
        count++;
    }

    at_resp_t resp = {.buff      = resp_buff,
                      .buff_size = sizeof(resp_buff),
                      .line_num  = 1,
                      .timeout   = YQ8228_BT_TIMEOUT_DFT};

    result = at_parser_exec_cmd(parser, &resp, "AT+ENCPINCODE=%.*s", BT_PIN_LEN, bt_pin);
    if (result != OS_SUCCESS)
    {
        return result;
    }

    if (at_resp_get_data_by_kw(&resp, "+SET ENCPINCODE=", "+SET ENCPINCODE=%[^\r]", pin_buff) <= 0)
    {
        ERROR("Get %s module bluetooth set PIN failed", module->name);
        return OS_FAILURE;
    }

    if (0 != strncmp(bt_pin, pin_buff, BT_PIN_LEN))
    {
        ERROR("Set %s module bluetooth PIN failed, return value error.", module->name);
        return OS_FAILURE;
    }

    DEBUG("module %s set PIN:%s", module->name, bt_pin);

    return OS_SUCCESS;
}

os_err_t yq8228_bt_pin_get(mo_object_t *module, char bt_pin_buf[BT_PIN_STR_LEN])
{
    at_parser_t *parser = &module->parser;
    os_err_t     result = OS_SUCCESS;

    char resp_buff[AT_RESP_BUFF_SIZE_DEF]    = {0};
    char pin_buff[AT_RESP_BUFF_SIZE_DEF / 2] = {0};

    at_resp_t resp = {.buff      = resp_buff,
                      .buff_size = sizeof(resp_buff),
                      .line_num  = 1,
                      .timeout   = YQ8228_BT_TIMEOUT_DFT};

    result = at_parser_exec_cmd(parser, &resp, "AT+ENCPINCODE");
    if (result != OS_SUCCESS)
    {
        return result;
    }

    if (at_resp_get_data_by_kw(&resp, "+ENCPINCODE=", "+ENCPINCODE=%[^\r]", pin_buff) <= 0)
    {
        ERROR("Get %s module bluetooth PIN failed", module->name);
        return OS_FAILURE;
    }

    if (BT_PIN_LEN != strlen(pin_buff))
    {
        ERROR("Get %s module bluetooth PIN failed, return value error.", module->name);
        return OS_FAILURE;
    }

    memcpy(bt_pin_buf, pin_buff, BT_PIN_STR_LEN);

    DEBUG("module %s PIN:%s", module->name, bt_pin_buf);

    return OS_SUCCESS;
}

os_err_t yq8228_bt_pin_mode_set(mo_object_t *module, bt_pin_mode_t pin_mode)
{
    at_parser_t *parser   = &module->parser;
    os_err_t     result   = OS_SUCCESS;
    uint32_t     mode_get = 0;

    char resp_buff[AT_RESP_BUFF_SIZE_DEF] = {0};

    at_resp_t resp = {.buff      = resp_buff,
                      .buff_size = sizeof(resp_buff),
                      .line_num  = 1,
                      .timeout   = YQ8228_BT_TIMEOUT_DFT};

    if (BT_PIN_DISABLE != pin_mode && BT_PIN_ENABLE != pin_mode)
    {
        ERROR("Error %s module's PIN mode", module->name);
        return result;
    }

    result = at_parser_exec_cmd(parser, &resp, "AT+ENCEN=%d", pin_mode);
    if (result != OS_SUCCESS)
    {
        return result;
    }

    if (at_resp_get_data_by_kw(&resp, "+SET ENCEN=", "+SET ENCEN=%u", &mode_get) <= 0)
    {
        ERROR("Get %s module bluetooth set PIN mode failed", module->name);
        return OS_FAILURE;
    }

    if (pin_mode != mode_get)
    {
        ERROR("Set %s module bluetooth PIN mode failed, return value error.", module->name);
        return OS_FAILURE;
    }

    DEBUG("module %s set PIN mode:%d", module->name, mode_get);

    return OS_SUCCESS;
}

os_err_t yq8228_bt_pin_mode_get(mo_object_t *module, bt_pin_mode_t *pin_mode_buf)
{
    at_parser_t *parser   = &module->parser;
    os_err_t     result   = OS_SUCCESS;
    uint32_t     mode_get = 0;

    char resp_buff[AT_RESP_BUFF_SIZE_DEF] = {0};

    at_resp_t resp = {.buff      = resp_buff,
                      .buff_size = sizeof(resp_buff),
                      .line_num  = 1,
                      .timeout   = YQ8228_BT_TIMEOUT_DFT};

    result = at_parser_exec_cmd(parser, &resp, "AT+ENCEN");
    if (result != OS_SUCCESS)
    {
        return result;
    }

    if (at_resp_get_data_by_kw(&resp, "+ENCEN=", "+ENCEN=%u", &mode_get) <= 0)
    {
        ERROR("Get %s module bluetooth PIN mode failed", module->name);
        return OS_FAILURE;
    }

    if (BT_PIN_DISABLE != mode_get && BT_PIN_ENABLE != mode_get)
    {
        ERROR("Error %s module's PIN mode", module->name);
        return result;
    }

    *pin_mode_buf = (bt_pin_mode_t)mode_get;

    DEBUG("module %s get PIN mode:%d", module->name, *pin_mode_buf);

    return OS_SUCCESS;
}

os_err_t yq8228_bt_reboot(mo_object_t *module)
{
    at_parser_t *parser = &module->parser;
    os_err_t     result = OS_SUCCESS;

    char resp_buff[AT_RESP_BUFF_SIZE_DEF] = {0};

    at_resp_t resp = {.buff      = resp_buff,
                      .buff_size = sizeof(resp_buff),
                      .line_num  = 2,
                      .timeout   = 5 * OS_TICK_PER_SECOND};

    result = at_parser_exec_cmd(parser, &resp, "AT+REBOOT");
    if (result != OS_SUCCESS)
    {
        return result;
    }

    /* It appears that the '+MAC' is always return after reboot. */
    if (OS_NULL == at_resp_get_line_by_kw(&resp, "+REBOOT") || OS_NULL == at_resp_get_line_by_kw(&resp, "+MAC"))
    {
        ERROR("Get %s module bluetooth reboot result failed", module->name);
        return OS_FAILURE;
    }

    DEBUG("module %s reboot done.", module->name);

    return OS_SUCCESS;
}

os_err_t yq8228_bt_disconnect(mo_object_t *module)
{
    at_parser_t *parser = &module->parser;
    os_err_t     result = OS_SUCCESS;

    char resp_buff[AT_RESP_BUFF_SIZE_DEF] = {0};

    at_resp_t resp = {.buff      = resp_buff,
                      .buff_size = sizeof(resp_buff),
                      .line_num  = 1,
                      .timeout   = YQ8228_BT_TIMEOUT_DFT};

    result = at_parser_exec_cmd(parser, &resp, "AT+LEDISC=1");
    if (result != OS_SUCCESS)
    {
        return result;
    }

    if (OS_NULL == at_resp_get_line_by_kw(&resp, "+DISCONN"))
    {
        ERROR("Get %s module bluetooth disconnecting result failed", module->name);
        return OS_FAILURE;
    }

    DEBUG("module %s disconnected.", module->name);

    return OS_SUCCESS;
}

os_err_t yq8228_bt_radio_power_level_set(mo_object_t *module, uint32_t radio_level)
{
    at_parser_t *parser    = &module->parser;
    os_err_t     result    = OS_SUCCESS;
    uint32_t     level_get = 0;

    char resp_buff[AT_RESP_BUFF_SIZE_DEF] = {0};

    at_resp_t resp = {.buff      = resp_buff,
                      .buff_size = sizeof(resp_buff),
                      .line_num  = 1,
                      .timeout   = YQ8228_BT_TIMEOUT_DFT};

    if (YQ8228_BT_PWR_MIN > radio_level || YQ8228_BT_PWR_MAX < radio_level)
    {
        ERROR("%s module bluetooth set radio power level: param invalid", module->name);
        return OS_FAILURE;
    }

    result = at_parser_exec_cmd(parser, &resp, "AT+PWR=%u", radio_level);
    if (result != OS_SUCCESS)
    {
        return result;
    }

    if (at_resp_get_data_by_kw(&resp, "+SET PWR=", "+SET PWR=%u", &level_get) <= 0)
    {
        ERROR("Get %s module bluetooth set radio power level result failed", module->name);
        return OS_FAILURE;
    }

    if (radio_level != level_get)
    {
        ERROR("Set %s module bluetooth radio power level failed, return value error.", module->name);
        return OS_FAILURE;
    }

    DEBUG("module %s set radio power level:%u", module->name, level_get);

    return OS_SUCCESS;
}

os_err_t yq8228_bt_radio_power_level_get(mo_object_t *module, uint32_t *radio_level_buf)
{
    at_parser_t *parser = &module->parser;
    os_err_t     result = OS_SUCCESS;

    char resp_buff[AT_RESP_BUFF_SIZE_DEF] = {0};

    at_resp_t resp = {.buff      = resp_buff,
                      .buff_size = sizeof(resp_buff),
                      .line_num  = 1,
                      .timeout   = YQ8228_BT_TIMEOUT_DFT};

    result = at_parser_exec_cmd(parser, &resp, "AT+PWR");
    if (result != OS_SUCCESS)
    {
        return result;
    }

    if (at_resp_get_data_by_kw(&resp, "+POWER=", "+POWER=%u", radio_level_buf) <= 0)
    {
        ERROR("Get %s module bluetooth radio power level failed", module->name);
        return OS_FAILURE;
    }

    DEBUG("module %s get PIN mode:%u", module->name, *radio_level_buf);

    return OS_SUCCESS;
}

#endif /* YQ8228_USING_BLUETOOTH_OPS */
