/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        yq8228_pinctl.c
 *
 * @brief       yq8228 internal functions
 *
 * @revision
 * Date         Author          Notes
 * 2020-10-21   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <oneos_config.h>
#include <drv_gpio.h>
#include <os_task.h>
#include <os_stddef.h>
#include "pin/pin.h"

#ifdef QY8228_USING_PIN_CTL

#define YQ8228_MODE_PIN   GET_PIN(D, 0)
#define YQ8228_WAKEUP_PIN GET_PIN(D, 2)
#define YQ8228_RESET_PIN  GET_PIN(C, 10)

void yq8228_enter_cmd_mode(void)
{
    os_pin_mode(YQ8228_WAKEUP_PIN, PIN_MODE_OUTPUT);
    os_pin_mode(YQ8228_MODE_PIN, PIN_MODE_OUTPUT);

    os_task_msleep(1);
    os_pin_write(YQ8228_WAKEUP_PIN, PIN_LOW);
    os_task_msleep(1);
    os_pin_write(YQ8228_MODE_PIN, PIN_LOW);
}

void yq8228_exit_cmd_mode(void)
{
    os_pin_mode(YQ8228_WAKEUP_PIN, PIN_MODE_OUTPUT);
    os_pin_mode(YQ8228_MODE_PIN, PIN_MODE_OUTPUT);

    os_task_msleep(1);
    os_pin_write(YQ8228_WAKEUP_PIN, PIN_HIGH);
    os_task_msleep(1);
    os_pin_write(YQ8228_MODE_PIN, PIN_HIGH);
}

#endif /* QY8228_USING_PIN_CTL */
