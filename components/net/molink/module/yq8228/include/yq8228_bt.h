/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        yq8228_bluetooth.h
 *
 * @brief       yq8228 module link kit bluetooth api declaration
 *
 * @revision
 * Date         Author          Notes
 * 2020-10-21   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __YQ8228_BT_H__
#define __YQ8228_BT_H__

#include "mo_bluetooth.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#ifdef YQ8228_USING_BLUETOOTH_OPS

os_err_t yq8228_bt_baudrate_set(mo_object_t *module, uint32_t baudrate);
os_err_t yq8228_bt_baudrate_get(mo_object_t *module, uint32_t *baudrate_buf);
os_err_t yq8228_bt_device_name_set(mo_object_t *module, char *dev_name, uint32_t dev_name_len);
os_err_t yq8228_bt_device_name_get(mo_object_t *module, char *dev_name_buf, uint32_t dev_name_buf_len);
os_err_t yq8228_bt_mac_set(mo_object_t *module, char bt_mac[BT_MAX_HWADDR_STR_LEN]);
os_err_t yq8228_bt_mac_get(mo_object_t *module, char bt_mac_buf[BT_MAX_HWADDR_STR_LEN]);
os_err_t yq8228_bt_adv_mode_set(mo_object_t *module, bt_adv_mode_t adv_mode);
os_err_t yq8228_bt_adv_mode_get(mo_object_t *module, bt_adv_mode_t *adv_mode_buf);
os_err_t yq8228_bt_adv_interval_set(mo_object_t *module, uint32_t interval);
os_err_t yq8228_bt_adv_interval_get(mo_object_t *module, uint32_t *interval_buf);
os_err_t yq8228_bt_connect_interval_set(mo_object_t *module, uint32_t interval);
os_err_t yq8228_bt_connect_interval_get(mo_object_t *module, uint32_t *interval_buf);
os_err_t yq8228_bt_pin_set(mo_object_t *module, char bt_pin[BT_PIN_STR_LEN]);
os_err_t yq8228_bt_pin_get(mo_object_t *module, char bt_pin_buf[BT_PIN_STR_LEN]);
os_err_t yq8228_bt_pin_mode_set(mo_object_t *module, bt_pin_mode_t pin_mode);
os_err_t yq8228_bt_pin_mode_get(mo_object_t *module, bt_pin_mode_t *pin_mode_buf);
os_err_t yq8228_bt_reboot(mo_object_t *module);
os_err_t yq8228_bt_disconnect(mo_object_t *module);
os_err_t yq8228_bt_radio_power_level_set(mo_object_t *module, uint32_t radio_level);
os_err_t yq8228_bt_radio_power_level_get(mo_object_t *module, uint32_t *radio_level_buf);

#endif /* YQ8228_USING_BLUETOOTH_OPS */

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __YQ8228_BT_H__ */
