/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        yq8228.h
 *
 * @brief       yq8228 factory mode api declaration
 *
 * @revision
 * Date         Author          Notes
 * 2020-10-21   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __YQ8228_H__
#define __YQ8228_H__

#include "mo_object.h"

#ifdef YQ8228_USING_GENERAL_OPS
#include "yq8228_general.h"
#endif /* YQ8228_USING_GENERAL_OPS */

#ifdef YQ8228_USING_BLUETOOTH_OPS
#include "yq8228_bt.h"
#endif /* YQ8228_USING_BLUETOOTH_OPS */

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#ifdef MOLINK_USING_YQ8228

#ifndef YQ8228_NAME
#define YQ8228_NAME "yq8228"
#endif

#ifndef YQ8228_DEVICE_NAME
#define YQ8228_DEVICE_NAME "lpuart1"
#endif

#ifndef YQ8228_RECV_BUFF_LEN
#define YQ8228_RECV_BUFF_LEN (500)
#endif

#ifndef YQ8228_DEVICE_RATE
#define YQ8228_DEVICE_RATE (19200)
#endif

typedef struct mo_yq8228
{
    mo_object_t parent;

} mo_yq8228_t;

mo_object_t *module_yq8228_create(const char *name, void *parser_config);
os_err_t     module_yq8228_destroy(mo_object_t *self);

#endif /* MOLINK_USING_YQ8228 */

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __YQ8228_H__ */
