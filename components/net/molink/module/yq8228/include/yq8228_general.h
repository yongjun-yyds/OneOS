/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        yq8228_general.h
 *
 * @brief       yq8228 module link kit general api declaration
 *
 * @revision
 * Date         Author          Notes
 * 2021-10-25   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __YQ8228_GENERAL_H__
#define __YQ8228_GENERAL_H__

#include "mo_general.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#ifdef YQ8228_USING_GENERAL_OPS
os_err_t yq8228_get_firmware_version(mo_object_t *self, mo_firmware_version_t *version);
#endif /* YQ8228_USING_GENERAL_OPS */

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __YQ8228_GENERAL_H__ */
