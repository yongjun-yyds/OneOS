/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        ec200x_600s_wifi.h
 *
 * @brief       ec200x_600s module link kit wifi api declaration
 *
 * @revision
 * Date         Author          Notes
 * 2022-10-09   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __EC200X_600S_WIFI_H__
#define __EC200X_600S_WIFI_H__

#include "mo_ipaddr.h"
#include "mo_wifi.h"
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#ifdef EC200X_600S_USING_WIFI_OPS

os_err_t ec200x_600s_wifi_scan_info(mo_object_t *module, char *ssid, mo_wifi_scan_result_t *scan_result);

#endif /* EC200X_600S_USING_WIFI_OPS */

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __EC200X_600S_WIFI_H__ */
