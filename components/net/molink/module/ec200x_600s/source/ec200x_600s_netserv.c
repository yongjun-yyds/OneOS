/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        ec200x_600s_netserv.c
 *
 * @brief       ec200x_600s module link kit netserv api implement
 *
 * @revision
 * Date         Author          Notes
 * 2020-03-25   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include "ec200x_600s_netserv.h"
#include "ec200x_600s.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#ifdef MOLINK_USING_IP
#include <mo_ipaddr.h>
#endif /* MOLINK_USING_IP */

#define MO_LOG_TAG "ec200x_600s.netserv"
#define MO_LOG_LVL MO_LOG_INFO
#include <mo_log.h>

#ifdef EC200X_600S_USING_NETSERV_OPS

os_err_t ec200x_600s_set_attach(mo_object_t *self, uint8_t attach_stat)
{
    at_parser_t *parser = &self->parser;

    char resp_buff[AT_RESP_BUFF_SIZE_DEF] = {0};

    at_resp_t resp = {.buff = resp_buff, .buff_size = sizeof(resp_buff), .timeout = 140 * OS_TICK_PER_SECOND};

    return at_parser_exec_cmd(parser, &resp, "AT+CGATT=%hhu", attach_stat);
}

os_err_t ec200x_600s_get_attach(mo_object_t *self, uint8_t *attach_stat)
{
    at_parser_t *parser = &self->parser;

    char resp_buff[AT_RESP_BUFF_SIZE_DEF] = {0};

    at_resp_t resp = {.buff = resp_buff, .buff_size = sizeof(resp_buff), .timeout = 3 * OS_TICK_PER_SECOND};

    os_err_t result = at_parser_exec_cmd(parser, &resp, "AT+CGATT?");
    if (result != OS_SUCCESS)
    {
        return OS_FAILURE;
    }

    if (at_resp_get_data_by_kw(&resp, "+CGATT:", "+CGATT: %hhu", attach_stat) <= 0)
    {
        ERROR("Get %s module attach state failed", self->name);
        return OS_FAILURE;
    }

    return OS_SUCCESS;
}

os_err_t ec200x_600s_set_reg(mo_object_t *self, uint8_t reg_n)
{
    at_parser_t *parser = &self->parser;

    char resp_buff[AT_RESP_BUFF_SIZE_DEF] = {0};

    at_resp_t resp = {.buff = resp_buff, .buff_size = sizeof(resp_buff), .timeout = 3 * OS_TICK_PER_SECOND};

    return at_parser_exec_cmd(parser, &resp, "AT+CEREG=%hhu", reg_n);
}

os_err_t ec200x_600s_get_reg(mo_object_t *self, eps_reg_info_t *info)
{
    at_parser_t *parser = &self->parser;

    char resp_buff[AT_RESP_BUFF_SIZE_256] = {0};

    at_resp_t resp = {.buff = resp_buff, .buff_size = sizeof(resp_buff), .timeout = 3 * OS_TICK_PER_SECOND};

    os_err_t result = at_parser_exec_cmd(parser, &resp, "AT+CEREG?");
    if (result != OS_SUCCESS)
    {
        return OS_FAILURE;
    }

    if (at_resp_get_data_by_kw(&resp, "+CEREG:", "+CEREG: %hhu,%hhu", &info->reg_n, &info->reg_stat) <= 0)
    {
        ERROR("Get %s module register state failed", self->name);
        return OS_FAILURE;
    }

    return OS_SUCCESS;
}

os_err_t ec200x_600s_set_cgact(mo_object_t *self, uint8_t cid, uint8_t act_stat)
{
    at_parser_t *parser = &self->parser;

    char resp_buff[AT_RESP_BUFF_SIZE_DEF] = {0};

    at_resp_t resp = {.buff = resp_buff, .buff_size = sizeof(resp_buff), .timeout = 10 * OS_TICK_PER_SECOND};

    return at_parser_exec_cmd(parser, &resp, "AT+CGACT=%hhu,%hhu", act_stat, cid);
}

os_err_t ec200x_600s_get_cgact(mo_object_t *self, uint8_t *cid, uint8_t *act_stat)
{
    at_parser_t *parser = &self->parser;

    char resp_buff[AT_RESP_BUFF_SIZE_DEF] = {0};

    at_resp_t resp = {.buff = resp_buff, .buff_size = sizeof(resp_buff), .timeout = 3 * OS_TICK_PER_SECOND};

    os_err_t result = at_parser_exec_cmd(parser, &resp, "AT+CGACT?");
    if (result != OS_SUCCESS)
    {
        return OS_FAILURE;
    }

    if (at_resp_get_data_by_kw(&resp, "+CGACT:", "+CGACT: %hhu,%hhu", cid, act_stat) <= 0)
    {
        ERROR("Get %s module cgact state failed", self->name);
        return OS_FAILURE;
    }

    return OS_SUCCESS;
}

os_err_t ec200x_600s_get_csq(mo_object_t *self, uint8_t *rssi, uint8_t *ber)
{
    at_parser_t *parser = &self->parser;

    char resp_buff[AT_RESP_BUFF_SIZE_DEF] = {0};

    at_resp_t resp = {.buff = resp_buff, .buff_size = sizeof(resp_buff), .timeout = 3 * OS_TICK_PER_SECOND};

    os_err_t result = at_parser_exec_cmd(parser, &resp, "AT+CSQ");
    if (result != OS_SUCCESS)
    {
        return OS_FAILURE;
    }

    if (at_resp_get_data_by_kw(&resp, "+CSQ:", "+CSQ: %hhu,%hhu", rssi, ber) <= 0)
    {
        ERROR("Get %s module signal quality failed", self->name);
        return OS_FAILURE;
    }

    return OS_SUCCESS;
}

#define ONEPOS_NET_TYPE_GSM          (1)
#define ONEPOS_NET_TYPE_LTE          (5)
#define EC200X_600S_DB_TO_DBM(rxlev) ((rxlev)-111)
#define EC200X_600S_RESP_SIZE        (1024) /* Estimated value, no ref about it. */

os_err_t ec200x_600s_get_cell_info(mo_object_t *self, onepos_cell_info_t *onepos_cell_info)
{
    os_err_t     result    = OS_SUCCESS;
    at_parser_t *parser    = &self->parser;
    cell_info_t *cell_info = OS_NULL;
    int32_t      arg_num   = 0;
    int32_t      rx_lev    = 0;

    char *resp_buff = os_calloc(1, EC200X_600S_RESP_SIZE);
    if (OS_NULL == resp_buff)
    {
        ERROR("[%s:%d] No enough memory!", __func__, __LINE__);
        return OS_NOMEM;
    }

    /* Estimated value, ref: time depends on the network status. */
    at_resp_t resp = {.buff = resp_buff, .buff_size = EC200X_600S_RESP_SIZE, .timeout = 60 * OS_TICK_PER_SECOND};

    result = at_parser_exec_cmd(parser, &resp, "AT+QCELLINFO?");
    if (OS_SUCCESS != result)
    {
        ERROR("[%s:%d] Query cell info failed!", __func__, __LINE__);
        goto __exit;
    }

    /* Get network type. */
    if (OS_NULL != strstr(at_resp_get_line(&resp, 1), "GSM"))
    {
        onepos_cell_info->net_type = ONEPOS_NET_TYPE_GSM;
    }
    else if (OS_NULL != strstr(at_resp_get_line(&resp, 1), "LTE"))
    {
        onepos_cell_info->net_type = ONEPOS_NET_TYPE_LTE;
    }
    else
    {
        ERROR("[%s:%d] Unkonwn network type!", __func__, __LINE__);
        result = OS_INVAL;
        goto __exit;
    }

    /* Total counts = resp.line_counts - 1 */
    cell_info = os_calloc((resp.line_counts - 1), sizeof(cell_info_t));
    if (OS_NULL == cell_info)
    {
        ERROR("[%s:%d] No enough memory!", __func__, __LINE__);
        result = OS_NOMEM;
        goto __exit;
    }

    onepos_cell_info->cell_num = 0;

    DEBUG("     MCC         MNC          CID         LAC       RSSI");
    DEBUG("------------ ------------ ------------ ------------ ----");

    for (int index = 1; index < resp.line_counts; index++)
    {
        /*  GSM:
            [+QCELLINFO: <cell_type>,"GSM",<MCC>,<MNC>,<LAC>,<cellID>,
                         <BSIC>,<RX_lev>,<RX_dbm>,<ARFCN>,-,-,-
                         […]]

            LTE:
            [+QCELLINFO: <cell_type>,"LTE",<MCC>,<MNC>,<TAC>,<cellID>,
                         <PCI>,<RX_lev>,<RX_dbm>,<EARFCN>,<RSRP><RSSI><SINR>
                         […]]
        */
        arg_num = at_resp_get_data_by_line(&resp,
                                           index,
                                           "+QCELLINFO: %*[^,],%*[^,],%u,%u,%X,%X,%*u,%u,",
                                           &cell_info[onepos_cell_info->cell_num].mcc,
                                           &cell_info[onepos_cell_info->cell_num].mnc,
                                           &cell_info[onepos_cell_info->cell_num].lac,
                                           &cell_info[onepos_cell_info->cell_num].cid,
                                           &rx_lev);

        if (5 != arg_num)
        {
            WARN("[%s:%d] unknown arg of cell info!", __func__, __LINE__);
            continue;
        }

        cell_info[onepos_cell_info->cell_num].ss = EC200X_600S_DB_TO_DBM(rx_lev);

        DEBUG("%-12u %-12u %-12X %-12X %-4d",
              cell_info[onepos_cell_info->cell_num].mcc,
              cell_info[onepos_cell_info->cell_num].mnc,
              cell_info[onepos_cell_info->cell_num].cid,
              cell_info[onepos_cell_info->cell_num].lac,
              cell_info[onepos_cell_info->cell_num].ss);

        onepos_cell_info->cell_num++;
    }

    onepos_cell_info->cell_info = cell_info;

__exit:

    if (resp.buff != OS_NULL)
    {
        os_free(resp.buff);
    }

    return result;
}

#endif /* EC200X_600S_USING_NETSERV_OPS */
