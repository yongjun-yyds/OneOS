/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        ec200x_600s_wifi.c
 *
 * @brief       ec200x_600s module link kit wifi api implement
 *
 * @revision
 * Date         Author          Notes
 * 2022-10-09   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include "ec200x_600s_wifi.h"
#include "ec200x_600s.h"
#include <stdio.h>

#define MO_LOG_TAG "ec200x_600s.wifi"
#define MO_LOG_LVL MO_LOG_INFO
#include <mo_log.h>

#ifdef EC200X_600S_USING_WIFI_OPS

#define EC200X_600S_SCAN_RESP_BUFF_LEN 3072

os_err_t ec200x_600s_wifi_scan_info(mo_object_t *module, char *ssid, mo_wifi_scan_result_t *scan_result)
{
    at_parser_t *parser = &module->parser;
    os_err_t     result = OS_SUCCESS;

    at_resp_t resp = {.buff      = os_calloc(1, EC200X_600S_SCAN_RESP_BUFF_LEN),
                      .buff_size = EC200X_600S_SCAN_RESP_BUFF_LEN,
                      .timeout   = 5 * OS_TICK_PER_SECOND};

    if (OS_NULL == resp.buff)
    {
        ERROR("[%s:%d] os_calloc wifi scan info response memory failed!", __func__, __LINE__);
        result = OS_NOMEM;
        goto __exit;
    }

    /* EC200U only support scan all ap list */
    result = at_parser_exec_cmd(parser, &resp, "AT+QWIFISCAN=12000,1,30");
    if (result != OS_SUCCESS)
    {
        goto __exit;
    }

    scan_result->info_array = (mo_wifi_info_t *)os_calloc(resp.line_counts, sizeof(mo_wifi_info_t));
    if (OS_NULL == scan_result->info_array)
    {
        ERROR("[%s:%d] os_calloc wifi scan info memory failed!", __func__, __LINE__);
        result = OS_NOMEM;
        goto __exit;
    }

    scan_result->info_num = 0;

    for (int i = 0; i < resp.line_counts; i++)
    {
        mo_wifi_info_t *tmp = &scan_result->info_array[i];

        int32_t get_result = at_resp_get_data_by_line(&resp,
                                                      i + 1,
                                                      "+QWIFISCAN: (-,-,%d,\"%[^\"]\",%d)",
                                                      &tmp->rssi,
                                                      tmp->bssid.bssid_str,
                                                      &tmp->channel);

        if (get_result > 0)
        {
            /* Not support scanning encryption info & ssid. */

            sscanf(tmp->bssid.bssid_str,
                   "%2x:%2x:%2x:%2x:%2x:%2x",
                   (int32_t *)&tmp->bssid.bssid_array[0],
                   (int32_t *)&tmp->bssid.bssid_array[1],
                   (int32_t *)&tmp->bssid.bssid_array[2],
                   (int32_t *)&tmp->bssid.bssid_array[3],
                   (int32_t *)&tmp->bssid.bssid_array[4],
                   (int32_t *)&tmp->bssid.bssid_array[5]);

            scan_result->info_num++;
        }
    }

__exit:
    if (result != OS_SUCCESS)
    {
        if (scan_result->info_array != OS_NULL)
        {
            os_free(scan_result->info_array);
        }
    }

    if (resp.buff != OS_NULL)
    {
        os_free(resp.buff);
    }

    return result;
}

#endif /* EC200X_600S_USING_WIFI_OPS */
