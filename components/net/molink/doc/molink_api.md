# MoLink API

MoLink组件[``<mo_api.h>``](../api/include/mo_api.h)提供了OneOS组件中的基础功能。通过将无线通信模组抽象为mo对象（模组对象），向用户层提供简洁易用的操作接口。同时MoLink组件设计兼容了通信模组的OpenCPU开发模式，极大提升了用户程序的可移植性，应用程序的无线连网功能可在AT模式和OpenCPU模式下无缝切换。后期MoLink组件将适配数量众多的无线通信模组，这样用户可以根据实际需求，便捷选择模组型号，轻松配置进行切换。

------

## 目录

- [MoLink API](#molink-api)
  - [目录](#目录)
  - [API](#api)
    - [1. 模组实例管理接口](#1-模组实例管理接口)
    - [2. 通用控制接口](#2-通用控制接口)
    - [3. 网络服务接口](#3-网络服务接口)
    - [4. 套接字接口](#4-套接字接口)
    - [5. MQTT客户端接口-建设当中](#5-mqtt客户端接口-建设当中)
    - [6. 蓝牙控制接口](#6-蓝牙控制接口)
    - [7. WIFI控制接口](#7-wifi控制接口)
    - [7. 模组自动识别功能](#7-模组自动识别功能)
    - [8. CTM2M云平台连接](#8-ctm2m云平台连接)
  - [应用示例](#应用示例)
    - [模组实例管理接口使用示例](#模组实例管理接口使用示例)
    - [通用控制接口使用示例](#通用控制接口使用示例)
    - [网络服务接口使用示例](#网络服务接口使用示例)
    - [MQTT客户端接口使用示例](#mqtt客户端接口使用示例)
    - [蓝牙控制接口使用示例](#蓝牙控制接口使用示例)
    - [WIFI控制接口使用示例](#wifi控制接口使用示例)
    - [CTM2M云平台接口使用示例](#ctm2m云平台接口使用示例)
    - [模组重置异常检测处理示例](#模组重置异常检测处理示例)
  - [FAQ](#faq)

## API

### 1. 模组实例管理接口

模组的管理基于模组实例管理框架，由统一管理接口控制，用户可以不必再关心冗杂的模组的AT指令收发及解析，调用MoLink API轻松实现模组管理及具体业务。

模组实例相关接口总览：

| **接口**       | **说明**             |
| :------------- | :------------------- |
| mo_create      | 创建模组对象         |
| mo_destroy     | 销毁模组对象         |
| mo_get_by_name | 根据名称获取模组对象 |
| mo_get_default | 获取默认模组对象     |
| mo_set_default | 设置默认模组对象     |

------

#### 1.2 创建销毁

MoLink提供自动创建和手动创建两种模组创建方式。用户可根据设备及具体应用场景进行选择。

**自动创建**

自动创建方式：使用oneos-cube可视化配置工具menuconfig，在``(Top) → Components→ Network→ Molink``路径下，使能物联网模组支持功能``（[*] Enable IoT modules support）``，在此目录下，选择使能模组及配置是否自动创建模组。

如图 以M5310-A为例：

1) 在menuconfig中进入具体模组配置目录``(Top) → Components→ Network→ Molink→ Enable IoT modules support → Module→ NB-IOT Modules Support→ M5310A → M5310A Config``

2) 使能自动创建功能``[*] Enable M5310A Module Object Auto Create``

3) 在使能自动创建后会出现次级配置选项，配置好模组信息如：设备接口名``Interface Device Name``、模组波特率``Interface Device Rate``和AT指令最大接收长度``The maximum length of AT command data accepted``。保存配置后编译烧录即可在OneOS运行时自动创建模组。

*) 注意：使用自动创建需关注模组在自动创建时是否正常工作，若模组未开机或工作状态不正常，不能使用自动创建功能。具体使用方法见``用户指南 Molink-模组连接套件:图形化配置``

```log
[*] Enable M5310A Module Object Auto Create            <-------使能模组自动创建功能
(uart2) M5310A Interface Device Name                   <-------接口名
(115200) M5310A Interface Device Rate                  <-------模组波特率
(512)   The maximum length of AT command data accepted <-------单条AT指令最大接收长度
-*- Enable M5310A Module General Operates              <-------使能通用控制接口
-*- Enable M5310A Module Network Service Operates      <-------使能网络服务接口
[*] Enable M5310A Module Ping Operates                 <-------使能ping功能接口
[*] Enable M5310A Module Ifconfig Operates             <-------使能ifconfig接口
[*] Enable M5310A Module Network TCP/IP Operates       <-------使能TCP/IP功能接口
[ ] Enable M5310A Module BSD Socket Operates           <-------使能BSD套接字
[*] Enable M5310A Module Onenet Nb Operates            <-------使能OneNetNB平台接口
```

**手动创建**

MoLink也提供了手动创建模组实例功能，方便更加灵活管理模组。接口如下：

##### mo_create

该函数用于创建模组对象实例，其函数原型如下：

```c
mo_object_t *mo_create(const char *name, mo_type_t type, void *parser_config);
```

| **参数**      | **说明**                                      |
| :------------ | :-------------------------------------------- |
| name          | 模组名称                                      |
| type          | 模组型号                                      |
| parser_config | AT解析器参数结构体指针，OpenCPU架构此参数为空 |
| **返回**      | **说明**                                      |
| OS_NULL       | 创建失败                                      |
| 非OS_NULL     | 模组对象指针                                  |

*) 进行手动创建请勿使能此模组的自动创建功能。

**销毁**

##### mo_destroy

该函数用于销毁模组对象实例，其函数原型如下：

```c
os_err_t mo_destroy(mo_object_t *self, mo_type_t type);
```

| **参数** | **说明**       |
| :------- | :------------- |
| self     | 模组对象       |
| type     | 支持的模组型号 |
| **返回** | **说明**       |
| OS\_EOK  | 成功           |

------

#### 1.3 模组管理

##### mo_get_by_name

该函数用于根据名称获取模组对象，函数原型如下：

```c
mo_object_t *mo_get_by_name(const char *name);
```

| **参数**  | **说明**     |
| :-------- | :----------- |
| name      | 模组对象名称 |
| **返回**  | **说明**     |
| OS_NULL   | 获取失败     |
| 非OS_NULL | 模组对象指针 |

##### mo_get_default

该函数用于获取默认模组对象，函数原型如下：

```c
mo_object_t *mo_get_default(void);
```

| **参数**  | **说明**         |
| :-------- | :--------------- |
| 无        | 无               |
| **返回**  | **说明**         |
| OS_NULL   | 获取失败         |
| 非OS_NULL | 默认模组对象指针 |

##### mo_set_defaults

该函数用于设置默认模组对象，其函数原型如下：

```c
void mo_set_default(mo_object_t *self);
```

| **参数** | **说明** |
| :------- | :------- |
| self     | 模组对象 |
| **返回** | **说明** |
| 无       | 无       |

##### mo_timeout_warning_cb_register

该函数用于注册连续多次超时的回调函数，当注册后，连续多次超时可能模组进入异常状态时，会以回调形式通知用户进行处理，函数原型如下：

```c
os_err_t mo_timeout_warning_cb_register(mo_object_t *self, at_warn_cb_t callback, uint8_t threshold);
```

| **参数**  | **说明**         |
| :-------- | :--------------- |
| self      | 模组对象          |
| callback  | 用户回调          |
| threshold | 门限值           |
| **返回**  | **说明**         |
| OS\_EOK      | 成功          |
| 非OS\_EOK    | 失败          |

注意：请勿阻塞或在回调中进行耗时操作，如需处理异常，请在其他任务中进行。建议新建task处理或将通知转发到其他task中进行处理。

------

### 2. 通用控制接口

通用控制接口提供模组相关基本信息及功能查询设置，模组创建后，按需调用即可。

通用控制接口总览：

| **接口**                     | **说明**                   |
| :--------------------------- | :------------------------- |
| mo_at_test                   | 测试AT指令                 |
| mo_get_imei                  | 获取IMEI                   |
| mo_get_imsi                  | 获取IMSI                   |
| mo_get_iccid                 | 获取iccid                  |
| mo_get_cfun                  | 获取射频模式               |
| mo_set_cfun                  | 设置射频模式               |
| mo_get_firmware_version      | 获取模组固件版本信息       |
| mo_get_firmware_version_free | 释放获取的模组固件版本信息 |
| mo_get_eid                   | 获取SIM eID                |
| mo_gm_time                   | 获取本地分解时间            |
| mo_time                      | 获取本地UNIX时间            |

#### mo_at_test

该函数用于发送AT测试命令，其函数原型如下：

```c
os_err_t mo_at_test(mo_object_t *self);
```

| **参数**  | **说明** |
| :-------- | :------- |
| self      | 模组对象 |
| **返回**  | **说明** |
| OS\_EOK   | 成功     |
| 非OS\_EOK | 失败     |

#### mo_get_imei

该函数用于获取IMEI，该函数原型如下：

```c
os_err_t mo_get_imei(mo_object_t *self, char *value, os_size_t len);
```

| **参数**  | **说明**          |
| :-------- | :---------------- |
| self      | 模组对象          |
| value     | 存储IMEI的buf     |
| len       | 存储IMEI的buf长度 |
| **返回**  | **说明**          |
| OS\_EOK   | 成功              |
| 非OS\_EOK | 失败              |

#### mo_get_imsi

该函数用于获取IMSI，其函数原型如下：

```c
os_err_t mo_get_imsi(mo_object_t *self, char *value, os_size_t len);
```

| **参数**  | **说明**          |
| :-------- | :---------------- |
| self      | 模组对象          |
| value     | 存储IMSI的buf     |
| len       | 存储IMSI的buf长度 |
| **返回**  | **说明**          |
| OS\_EOK   | 成功              |
| 非OS\_EOK | 失败              |

#### mo_get_iccid

该函数用于获取ICCID，其函数原型如下：

```c
os_err_t mo_get_iccid(mo_object_t *self, char *value, os_size_t len);
```

| **参数**  | **说明**           |
| :-------- | :----------------- |
| self      | 模组对象           |
| value     | 存储ICCID的buf     |
| len       | 存储ICCID的buf长度 |
| **返回**  | **说明**           |
| OS\_EOK   | 成功               |
| 非OS\_EOK | 失败               |

#### mo_get_cfun

该函数用于获取射频模式，其函数原型如下：

```c
os_err_t mo_get_cfun(mo_object_t *self, uint8_t *fun_lvl);
```

| **参数**  | **说明**           |
| :-------- | :----------------- |
| self      | 模组对象           |
| fun_lvl   | 存储射频模式的指针 |
| **返回**  | **说明**           |
| OS\_EOK   | 成功               |
| 非OS\_EOK | 失败               |

#### mo_set_cfun

该函数用于设置射频模式，其函数原型如下：

```c
os_err_t mo_set_cfun(mo_object_t *self, uint8_t fun_lvl);
```

| **参数**  | **说明** |
| :-------- | :------- |
| self      | 模组对象 |
| fun_lvl   | 射频模式 |
| **返回**  | **说明** |
| OS\_EOK   | 成功     |
| 非OS\_EOK | 失败     |

*) fun_lvl的设置根据模组不同有所区别，需要具体查阅AT手册对应的值进行设置

#### mo_get_firmware_version

该函数用于获取模组的固件版本信息

```c
os_err_t mo_get_firmware_version(mo_object_t *self, mo_firmware_version_t *version);
```

| **参数**  | **说明**                     |
| :-------- | :--------------------------- |
| self      | 模组对象                     |
| version   | 存储固件版本号的结构体的指针 |
| **返回**  | **说明**                     |
| OS\_EOK   | 成功                         |
| 非OS\_EOK | 失败                         |

*) 该函数将动态申请用于存储固件版本信息的内存，调用该函数需调用mo_get_firmware_version_free函数释放内存。

#### mo_get_firmware_version_free

该函数用于释放获取的模组固件版本信息

```c
void mo_get_firmware_version_free(mo_firmware_version_t *version);
```

| **参数** | **说明**                     |
| :------- | :--------------------------- |
| version  | 存储固件版本号的结构体的指针 |

#### mo_get_eid

该函数用于获取SIM卡eID，其函数原型如下：

```c
os_err_t mo_get_eid(mo_object_t *self, char *eid, os_size_t len);
```

| **参数**  | **说明**      |
| :-------- | :------------ |
| self      | 模组对象      |
| eid       | 存储eID的指针 |
| len       | eID字符串长度 |
| **返回**  | **说明**      |
| OS\_EOK   | 成功          |
| 非OS\_EOK | 失败          |

#### mo_gm_time

该函数用于从模组获取本地分解时间，其函数原型如下：

```c
os_err_t mo_gm_time(mo_object_t *self, struct tm *l_tm);
```

| **参数**  | **说明**      |
| :-------- | :------------ |
| self      | 模组对象      |
| l_tm      | 存储分解时间结构体的指针 |
| **返回**  | **说明**      |
| OS\_EOK   | 成功          |
| 非OS\_EOK | 失败          |

#### mo_time

该函数用于获取本地UNIX时间，其函数原型如下：

```c
os_err_t mo_time(mo_object_t *self, time_t *timep);
```

| **参数**  | **说明**      |
| :-------- | :------------ |
| self      | 模组对象      |
| timep     | 存储UNIX时间的指针 |
| **返回**  | **说明**      |
| OS\_EOK   | 成功          |
| 非OS\_EOK | 失败          |

------

### 3. 网络服务接口

网络服务接口提供模组网络服务相关基本信息及功能查询设置，部分功能在模组侧有依赖关系，具体见不同模组的AT手册。

| **接口**               | **说明**                           |
| :--------------------- | :--------------------------------- |
| mo_set_attach          | 网络附着或去附着                   |
| mo_get_attach          | 获取网络附着状态                   |
| mo_set_reg             | 设置网络注册参数                   |
| mo_get_reg             | 获取网络注册状态                   |
| mo_set_cgact           | 网络激活或去激活                   |
| mo_get_cgact           | 获取网络激活状态                   |
| mo_get_csq             | 获取信号强度                       |
| mo_get_radio           | 获取无线信息                       |
| mo_get_ipaddr          | 获取IP地址                         |
| mo_set_dnsserver       | 设置DNS服务器地址                  |
| mo_get_dnsserver       | 查询DNS服务器地址                  |
| mo_get_cell_info       | 获取cell信息                       |
| mo_set_psm             | 设置PSM选项                        |
| mo_get_psm             | 查询PSM信息                        |
| mo_set_edrx_cfg        | 配置edrx参数                       |
| mo_get_edrx_cfg        | 查询edrx配置                       |
| mo_get_edrx_dynamic    | 查询edrx生效值(读取动态 eDRX 参数) |
| mo_set_band            | 多频段模块设置搜网的频段           |
| mo_set_earfcn          | 锁频                               |
| mo_get_earfcn          | 查询earfcn(锁频)信息               |
| mo_clear_stored_earfcn | 清除存储的频点信息                 |
| mo_clear_plmn          | 清除plmn等驻网记录                 |

#### mo_set_attach

该函数用于附着或去附着，其函数原型如下：

```c
os_err_t mo_set_attach(mo_object_t *self, uint8_t attach_stat);
```

| **参数**    | **说明**       |
| :---------- | :------------- |
| self        | 模组对象       |
| attach_stat | 欲设置附着状态 |
| **返回**    | **说明**       |
| OS\_EOK     | 成功           |
| 非OS\_EOK   | 失败           |

#### mo_get_attach

该函数用于获取附着状态，其函数原型如下：

```c
os_err_t mo_get_attach(mo_object_t *self, uint8_t *attach_stat);
```

| **参数**    | **说明**          |
| :---------- | :---------------- |
| self        | 模组对象          |
| attach_stat | 存储附着状态的buf |
| **返回**    | **说明**          |
| OS\_EOK     | 成功              |
| 非OS\_EOK   | 失败              |

#### mo_set_reg

该函数用于设置注册参数，其函数原型如下：

```c
os_err_t mo_set_reg(mo_object_t *self, uint8_t reg_n);
```

| **参数**  | **说明** |
| :-------- | :------- |
| self      | 模组对象 |
| reg_n     | 注册参数 |
| **返回**  | **说明** |
| OS\_EOK   | 成功     |
| 非OS\_EOK | 失败     |

#### mo_get_reg

该函数用于获取注册状态，其函数原型如下：

```c
os_err_t mo_get_reg(mo_object_t *self, eps_reg_info_t *info);
```

| **参数**  | **说明**          |
| :-------- | :---------------- |
| self      | 模组对象          |
| info      | 存储注册状态的结构体指针 |
| **返回**  | **说明**          |
| OS\_EOK   | 成功              |
| 非OS\_EOK | 失败              |

#### mo_set_cgact

该函数用于激活或去激活，其函数原型如下：

```c
os_err_t mo_set_cgact(mo_object_t *self, uint8_t cid, uint8_t act_n);
```

| **参数**  | **说明**       |
| :-------- | :------------- |
| self      | 模组对象       |
| cid       | CID参数        |
| act_n     | 激活参数，0或1 |
| **返回**  | **说明**       |
| OS\_EOK   | 成功           |
| 非OS\_EOK | 失败           |

#### mo_get_cgact

该函数用于获取激活状态，其函数原型如下：

```c
os_err_t mo_get_cgact(mo_object_t *self, uint8_t *cid, uint8_t *act_stat);
```

| **参数**  | **说明**          |
| :-------- | :---------------- |
| self      | 模组对象          |
| cid       | 存储CID参数的buf  |
| act_stat  | 存储激活参数的buf |
| **返回**  | **说明**          |
| OS\_EOK   | 成功              |
| 非OS\_EOK | 失败              |

#### mo_get_csq

该函数用于获取信号强度，其函数原型如下：

```c
os_err_t mo_get_csq(mo_object_t *self, uint8_t *rssi, uint8_t *ber);
```

| **参数**  | **说明**      |
| :-------- | :------------ |
| self      | 模组对象      |
| rssi      | 存储RSSI的buf |
| act_stat  | 存储BER的buf  |
| **返回**  | **说明**      |
| OS\_EOK   | 成功          |
| 非OS\_EOK | 失败          |

#### mo_get_radio

该函数用于获取无线信息，其函数原型如下：

```c
os_err_t mo_get_radio(mo_object_t *self, radio_info_t *radio_info);
```

| **参数**   | **说明**          |
| :--------- | :---------------- |
| self       | 模组对象          |
| radio_info | 存储无线信息的buf |
| **返回**   | **说明**          |
| OS\_EOK    | 成功              |
| 非OS\_EOK  | 失败              |

#### mo_get_ipaddr

该函数用于获取ip地址，其函数原型如下：

```c
os_err_t mo_get_ipaddr(mo_object_t *self, char ip[]);
```

| **参数**  | **说明**    |
| :-------- | :---------- |
| self      | 模组对象    |
| ip        | 存储IP的buf |
| **返回**  | **说明**    |
| OS\_EOK   | 成功        |
| 非OS\_EOK | 失败        |

#### mo_set_dnsserver

该函数用于设置DNS服务器地址信息，其函数原型如下：

```c
os_err_t mo_set_dnsserver(mo_object_t *self, dns_server_t dns);
```

| **参数**  | **说明**                   |
| :-------- | :------------------------- |
| self      | 模组对象                   |
| dns       | 设置DNS服务器地址信息的buf |
| **返回**  | **说明**                   |
| OS\_EOK   | 成功                       |
| 非OS\_EOK | 失败                       |

#### mo_get_dnsserver

该函数用于查询DNS服务器地址信息，其函数原型如下：

```c
os_err_t mo_get_dnsserver(mo_object_t *self, dns_server_t *dns);
```

| **参数**  | **说明**                   |
| :-------- | :------------------------- |
| self      | 模组对象                   |
| dns       | 存储DNS服务器地址信息的buf |
| **返回**  | **说明**                   |
| OS\_EOK   | 成功                       |
| 非OS\_EOK | 失败                       |

#### mo_get_cell_info

该函数用于获取cell信息，其函数原型如下：

```c
os_err_t mo_get_cell_info(mo_object_t *self, onepos_cell_info_t* onepos_cell_info);
```

| **参数**         | **说明**          |
| :--------------- | :---------------- |
| self             | 模组对象          |
| onepos_cell_info | 存储cell信息的buf |
| **返回**         | **说明**          |
| OS\_EOK          | 成功              |
| 非OS\_EOK        | 失败              |

#### mo_set_psm

该函数用于设置PSM选项，其函数原型如下：

```c
os_err_t mo_set_psm(mo_object_t *self, mo_psm_info_t info);
```

| **参数**  | **说明**         |
| :-------- | :--------------- |
| self      | 模组对象         |
| info      | 设置PSM选项的buf |
| **返回**  | **说明**         |
| OS\_EOK   | 成功             |
| 非OS\_EOK | 失败             |

#### mo_get_psm

该函数用于获取PSM选项，其函数原型如下：

```c
os_err_t mo_get_psm(mo_object_t *self, mo_psm_info_t *info);
```

| **参数**  | **说明**         |
| :-------- | :--------------- |
| self      | 模组对象         |
| info      | 存储PSM选项的buf |
| **返回**  | **说明**         |
| OS\_EOK   | 成功             |
| 非OS\_EOK | 失败             |

#### mo_set_edrx_cfg

该函数用于配置edrx参数信息，其函数原型如下：

```c
os_err_t mo_set_edrx_cfg(mo_object_t *self, mo_edrx_cfg_t cfg);
```

| **参数**  | **说明**          |
| :-------- | :---------------- |
| self      | 模组对象          |
| cfg       | 配置edrx参数的buf |
| **返回**  | **说明**          |
| OS\_EOK   | 成功              |
| 非OS\_EOK | 失败              |

#### mo_get_edrx_cfg

该函数用于查询edrx配置信息，其函数原型如下：

```c
os_err_t mo_get_edrx_cfg(mo_object_t *self, mo_edrx_t *edrx_local);
```

| **参数**  | **说明**          |
| :-------- | :---------------- |
| self      | 模组对象          |
| info      | 存储edrx配置的buf |
| **返回**  | **说明**          |
| OS\_EOK   | 成功              |
| 非OS\_EOK | 失败              |

#### mo_get_edrx_dynamic

该函数用于获取查询edrx生效值(读取动态 eDRX 参数)信息，其函数原型如下：

```c
os_err_t mo_get_edrx_dynamic(mo_object_t *self, mo_edrx_t *edrx_dynamic);
```

| **参数**  | **说明**            |
| :-------- | :------------------ |
| self      | 模组对象            |
| param     | 存储edrx生效值的buf |
| **返回**  | **说明**            |
| OS\_EOK   | 成功                |
| 非OS\_EOK | 失败                |

#### mo_set_band

该函数用于配置多频段模块设置搜网的频段信息，其函数原型如下：

```c
os_err_t mo_set_band(mo_object_t *self, char band_list[], uint8_t num);
```

| **参数**  | **说明**            |
| :-------- | :------------------ |
| self      | 模组对象            |
| band_list | 存储频段信息的buf   |
| num       | 存储band_list的长度 |
| **返回**  | **说明**            |
| OS\_EOK   | 成功                |
| 非OS\_EOK | 失败                |

#### mo_set_earfcn

该函数用于设置锁频选项，其函数原型如下：

```c
os_err_t mo_set_earfcn(mo_object_t *self, mo_earfcn_t earfcn);
```

| **参数**  | **说明**          |
| :-------- | :---------------- |
| self      | 模组对象          |
| earfcn    | 设置锁频选项的buf |
| **返回**  | **说明**          |
| OS\_EOK   | 成功              |
| 非OS\_EOK | 失败              |

#### mo_get_earfcn

该函数用于查询earfcn(锁频)信息 ，其函数原型如下：

```c
os_err_t mo_get_earfcn(mo_object_t *self, mo_earfcn_t *earfcn);
```

| **参数**  | **说明**                  |
| :-------- | :------------------------ |
| self      | 模组对象                  |
| earfcn    | 存储锁频配置相关信息的buf |
| **返回**  | **说明**                  |
| OS\_EOK   | 成功                      |
| 非OS\_EOK | 失败                      |

#### mo_clear_stored_earfcn

该函数用于清除存储的频点信息，其函数原型如下：

```c
os_err_t mo_clear_stored_earfcn(mo_object_t *self);
```

| **参数**  | **说明** |
| :-------- | :------- |
| self      | 模组对象 |
| **返回**  | **说明** |
| OS\_EOK   | 成功     |
| 非OS\_EOK | 失败     |

#### mo_clear_plmn

该函数用于清除plmn等信息，其函数原型如下：

```c
os_err_t mo_clear_plmn(mo_object_t *self);
```

| **参数**  | **说明** |
| :-------- | :------- |
| self      | 模组对象 |
| **返回**  | **说明** |
| OS\_EOK   | 成功     |
| 非OS\_EOK | 失败     |

### 4. 套接字接口

MoLink提供套接字接口，区分于普通套接字接口，以``mo_``作标志，使用方式与普通套接字接口基本无异，区别在于某些接口需要传入模组实例。详见
``components\net\molink\api\include\mo_socket.h``

另``OneOS socket组件``为用户提供了一套兼容BSD的标准接口，用户可在``自动创建``时选择使能BSD套接字服务，即可使用。

注意：目前UDP仅ESP8266支持可变远端发送。

### 5. MQTT客户端接口-建设当中

MQTT客户端提供接口来连接到MQTT代理来发布消息、订阅主题和接收发布的消息。

| **接口**                 | **说明**                         |
| :----------------------- | :------------------------------- |
| mo_mqttc_create          | 创建MQTT客户端                   |
| mo_mqttc_destroy         | 销毁MQTT客户端                   |
| mo_mqttc_connect         | 连接至MQTT代理                   |
| mo_mqttc_publish         | 发布一条消息                     |
| mo_mqttc_subscribe       | 订阅一个主题                     |
| mo_mqttc_unsubscribe     | 取消订阅的主题                   |
| mo_mqttc_set_msg_handler | 设置或移除指定主题的处理函数     |
| mo_mqttc_disconnect      | 断开与MQTT代理的连接             |
| mo_mqttc_isconnect       | 检查是否连接至MQTT代理           |
| mo_mqttc_yield           | 接收并处理MQTT消息               |
| mo_mqttc_start_task      | 启动一个任务来接收并处理MQTT消息 |

#### mo_mqttc_create

该函数用于创建一个MQTT客户端实例，其函数原型如下：

```c
mo_mqttc_t *mo_mqttc_create(mo_object_t *module, mqttc_create_opts_t *create_opts)
```

| **参数**    | **说明**           |
| :---------- | :----------------- |
| module      | 模组对象           |
| create_opts | MQTT客户端创建选项 |
| **返回**    | **说明**           |
| 非OS\_NULL  | 成功               |
| OS\_NULL    | 失败               |

MQTT客户端创建选项说明如下：

```c
typedef struct mqttc_create_opts
{
    mqttc_string_t address;         /* The MQTT server address */
    uint16_t    port;            /* The MQTT server port */
    uint32_t    command_timeout; /* The MQTT client ACK timeout, unit ms*/
    os_size_t      max_msgs;        /* The max number of messages.*/
} mqttc_create_opts_t;
```

| **重要成员**    | **说明**                                                        |
| :-------------- | :-------------------------------------------------------------- |
| address         | 服务器地址字符串                                                |
| port            | 服务器端口号                                                    |
| command_timeout | MQTT客户端ACK超时时间，单位为ms，若为0则使用模组提供的默认值    |
| max_msgs        | MQTT客户端一次接收的最大的消息数量，若为0则使用系统提供的默认值 |

#### mo_mqttc_connect

该函数用于连接至MQTT代理服务器，其函数原型如下：

```c
os_err_t mo_mqttc_connect(mo_mqttc_t *client, mqttc_conn_opts_t *connect_opts)
```

| **参数**     | **说明**           |
| :----------- | :----------------- |
| client       | MQTT客户端实例     |
| connect_opts | MQTT客户端连接选项 |
| **返回**     | **说明**           |
| OS\_EOK      | 成功               |
| 非OS\_EOK    | 失败               |

MQTT客户端连接选项说明如下：

```c
typedef struct mqttc_conn_opts
{
    mqttc_string_t client_id;    /* The MQTT client identifier string */

    uint8_t  mqtt_version;    /* The Version of MQTT to be used.  3 = 3.1 4 = 3.1.1 */
    uint32_t keep_alive;      /* The keep alive time, unit second */
    uint8_t  clean_session;   /* The flag of clean_session option */
    uint8_t  will_flag;       /* The flag of will option */

    mqttc_will_opts_t will_opts; /* The options of will */

    mqttc_string_t username;
    mqttc_string_t password;
} mqttc_conn_opts_t;
```

| **重要成员**  | **说明**                                                                  |
| :------------ | :------------------------------------------------------------------------ |
| client_id     | MQTT客户端标识                                                            |
| mqtt_version  | MQTT协议的版本，支持的版本号取由模组决定                                  |
| keep_alive    | MQTT客户端与服务器心跳保活时间，单位为秒，若设置为0则使用模组提供的默认值 |
| clean_session | 是否清除session信息                                                       |
| will_flag     | 是否使用遗言                                                              |
| will_opts     | 遗言选项                                                                  |
| username      | 用户名                                                                    |
| password      | 用户密钥                                                                  |

MQTT客户端遗愿选项说明如下：

```c
typedef struct mqttc_will_opts
{
    mqttc_string_t topic_name; /* The LWT topic to which the LWT message will be published */
    mqttc_string_t message;    /* The LWT payload */
    mqttc_qos_t    qos;        /* The quality of service setting for the LWT message */
    uint8_t     retained;   /* The retained flag for the LWT message */
} mqttc_will_opts_t;
```

| **重要成员** | **说明**       |
| :----------- | :------------- |
| topic_name   | 遗愿的主题     |
| message      | 遗愿的消息内容 |
| qos          | 遗愿的服务等级 |
| retained     | 遗愿是否保留   |

#### mo_mqttc_publish

该函数用于向指定的主题发布一条消息，其函数原型如下：

```c
os_err_t mo_mqttc_publish(mo_mqttc_t *client, const char *topic_filter, mqttc_msg_t *msg);
```

| **参数**     | **说明**           |
| :----------- | :----------------- |
| client       | MQTT客户端实例     |
| topic_filter | 需要发布的消息主题 |
| msg          | 需要发布的消息     |
| **返回**     | **说明**           |
| OS\_EOK      | 成功               |
| 非OS\_EOK    | 失败               |

MQTT消息结构体的说明如下:

```c
typedef struct mqttc_msg
{
    mqttc_qos_t qos;       /* The quality of service setting for the message */

    uint8_t  retained;  /* The retained flag for the message */
    uint8_t  dup;       /* The dup flag for the message*/

    void     *payload;     /* The data of the message */
    os_size_t payload_len; /* The length of data */
} mqttc_msg_t;

```

| **重要成员** | **说明**     |
| :----------- | :----------- |
| qos          | 服务质量     |
| retained     | 是否保留     |
| dup          | 重发标识     |
| payload      | 消息数据     |
| payloadlen   | 消息数据长度 |

#### mo_mqttc_subscribe

该函数用于订阅指定的主题并设置消息处理函数，其函数原型如下：

```c
os_err_t mo_mqttc_subscribe(mo_mqttc_t *client,
                            const char *topic_filter,
                            mqttc_qos_t qos,
                            mqttc_msg_handler_t handler);
```

| **参数**     | **说明**               |
| :----------- | :--------------------- |
| client       | MQTT客户端实例         |
| topic_filter | 需要订阅的消息主题     |
| qos          | 订阅消息的服务质量     |
| handler      | 订阅主题消息的处理函数 |
| **返回**     | **说明**               |
| OS\_EOK      | 成功                   |
| 非OS\_EOK    | 失败                   |

订阅主题消息的处理函数的说明如下:

```c
typedef void (*mqttc_msg_handler_t)(mqttc_msg_data_t*);
```

函数中的消息数据结构体的说明如下:

```c
typedef struct mqttc_msg_data
{
    mqttc_msg_t    message;
    mqttc_string_t topic_name;
} mqttc_msg_data_t;
```

| **重要成员** | **说明**       |
| :----------- | :------------- |
| message      | MQTT消息结构体 |
| topic_name   | 消息主题       |

#### mo_mqttc_unsubscribe

该函数取消指定的主题的订阅，其函数原型如下：

```c
os_err_t mo_mqttc_unsubscribe(mo_mqttc_t *client, const char *topic_filter)
```

| **参数**     | **说明**               |
| :----------- | :--------------------- |
| client       | MQTT客户端实例         |
| topic_filter | 需要取消订阅的消息主题 |
| **返回**     | **说明**               |
| OS\_EOK      | 成功                   |
| 非OS\_EOK    | 失败                   |

#### mo_mqttc_set_msg_handler

该函数用于设置或移除指定主题的处理函数，其函数原型如下：

```c
os_err_t mo_mqttc_set_msg_handler(mo_mqttc_t *client,
                                  const char *topic_filter,
                                  mqttc_msg_handler_t handler);
```

| **参数**     | **说明**                                              |
| :----------- | :---------------------------------------------------- |
| client       | MQTT客户端实例                                        |
| topic_filter | 需要设置的消息主题                                    |
| handler      | 消息处理函数的指针，如果为OS_NULL则移除原有的处理函数 |
| **返回**     | **说明**                                              |
| OS\_EOK      | 成功                                                  |
| 非OS\_EOK    | 失败                                                  |

#### mo_mqttc_disconnect

该函数用于断开与MQTT代理服务器的连接，其函数原型如下：

```c
os_err_t mo_mqttc_disconnect(mo_mqttc_t *client);
```

| **参数**  | **说明**       |
| :-------- | :------------- |
| client    | MQTT客户端实例 |
| **返回**  | **说明**       |
| OS\_EOK   | 成功           |
| 非OS\_EOK | 失败           |

#### mo_mqttc_isconnect

该函数检测是否连接至MQTT代理服务器，其函数原型如下：

```c
os_bool_t mo_mqttc_isconnect(mo_mqttc_t *client);
```

| **参数**    | **说明**       |
| :---------- | :------------- |
| client      | MQTT客户端实例 |
| **返回**    | **说明**       |
| OS\_TURE    | 已连接         |
| 非OS\_FALSE | 未连接         |

#### mo_mqttc_destroy

该函数用于销毁一个MQTT客户端实例，该函数原型如下：

```c
os_err_t mo_mqttc_destroy(mo_mqttc_t *client)
```

| **参数**  | **说明**       |
| :-------- | :------------- |
| client    | MQTT客户端实例 |
| **返回**  | **说明**       |
| OS\_EOK   | 成功           |
| 非OS\_EOK | 失败           |

> *) 销毁客户端前请先调用mo_mqttc_disconnect断开与服务器的连接。

#### mo_mqttc_yield

定时调用该函数来处理接收到的MQTT消息，该函数原型如下：

```c
os_err_t mo_mqttc_yield(mo_mqttc_t *client, uint32_t timeout_ms)
```

| **参数**   | **说明**                         |
| :--------- | :------------------------------- |
| client     | MQTT客户端实例                   |
| timeout_ms | 等待处理MQTT消息的时间，单位毫秒 |
| **返回**   | **说明**                         |
| OS\_EOK    | 成功                             |
| 非OS\_EOK  | 失败                             |

#### mo_mqttc_start_task

该函数用于启动一个线程来处理接收到的MQTT消息，该函数原型如下：

```c
os_err_t mo_mqttc_start_task(mo_mqttc_t *client);
```

| **参数**  | **说明**       |
| :-------- | :------------- |
| client    | MQTT客户端实例 |
| **返回**  | **说明**       |
| OS\_EOK   | 成功           |
| 非OS\_EOK | 失败           |

> 调用该函数后，就不应该再调用mo_mqttc_yield函数。

### 6. 蓝牙控制接口

蓝牙控制接口提供蓝牙基本配置查询及设置功能，针对采用类AT命令方式进行蓝牙控制命令交互的蓝牙模组。

| **接口**                   | **说明**                         |
| :--------------------------| :------------------------------- |
| mo_bt_baudrate_set         | 设置采用串口通信蓝牙模组波特率      |
| mo_bt_baudrate_get         | 查询采用串口通信蓝牙模组波特率      |
| mo_bt_device_name_set      | 设置蓝牙模组设备名                 |
| mo_bt_device_name_get      | 查询蓝牙模组设备名                 |
| mo_bt_mac_set              | 设置蓝牙MAC地址                   |
| mo_bt_mac_get              | 查询蓝牙MAC地址                   |
| mo_bt_adv_mode_set         | 设置蓝牙广播模式                  |
| mo_bt_adv_mode_get         | 查询蓝牙广播模式                  |
| mo_bt_adv_interval_set     | 设置蓝牙广播间隔                  |
| mo_bt_adv_interval_get     | 查询蓝牙广播间隔                  |
| mo_bt_connect_interval_set | 设置蓝牙连接间隔                  |
| mo_bt_connect_interval_get | 查询蓝牙连接间隔                  |
| mo_bt_pin_set              | 设置蓝牙PIN码                     |
| mo_bt_pin_get              | 查询蓝牙PIN码                     |
| mo_bt_pin_mode_set         | 设置蓝牙PIN码模式                 |
| mo_bt_pin_mode_get         | 查询蓝牙PIN码模式                 |
| mo_bt_reboot               | 触发蓝牙模组重启                  |
| mo_bt_disconnect           | 触发蓝牙模组断开连接               |
| mo_bt_radio_power_level_set| 设置蓝牙功率等级                  |
| mo_bt_radio_power_level_get| 查询蓝牙功率等级                  |

#### mo_bt_baudrate_set

该函数用于设置采用串口通信蓝牙模组波特率，其函数原型如下：

```c
os_err_t mo_bt_baudrate_set(mo_object_t *self, uint32_t baudrate);
```

| **参数**     | **说明**           |
| :----------- | :----------------- |
| self         | 模组对象            |
| baudrate     | 波特率             |
| **返回**     | **说明**           |
| OS\_EOK      | 成功               |
| 非OS\_EOK    | 失败               |

#### mo_bt_baudrate_get

该函数用于查询采用串口通信蓝牙模组波特率，其函数原型如下：

```c
os_err_t mo_bt_baudrate_get(mo_object_t *self, uint32_t *baudrate_buf);
```

| **参数**     | **说明**           |
| :----------- | :----------------- |
| self         | 模组对象            |
| baudrate     | 用户buff存储波特率值 |
| **返回**     | **说明**           |
| OS\_EOK      | 成功               |
| 非OS\_EOK    | 失败               |

#### mo_bt_device_name_set

该函数用于设置蓝牙模组设备名，其函数原型如下：

```c
os_err_t mo_bt_device_name_set(mo_object_t *self, char *dev_name, uint32_t dev_name_len);
```

| **参数**     | **说明**           |
| :----------- | :----------------- |
| self         | 模组对象            |
| dev_name     | 设备名称字符串      |
| dev_name_len | 字符串长度          |
| **返回**     | **说明**           |
| OS\_EOK      | 成功               |
| 非OS\_EOK    | 失败               |

#### mo_bt_device_name_get

该函数用于查询蓝牙模组设备名，其函数原型如下：

```c
os_err_t mo_bt_device_name_get(mo_object_t *self, char *dev_name_buf, uint32_t dev_name_buf_len);
```

| **参数**     | **说明**           |
| :----------- | :----------------- |
| self         | 模组对象            |
| dev_name_buf     | 用户buff用于存储设备名称 |
| dev_name_buf_len | 用户buff大小            |
| **返回**     | **说明**           |
| OS\_EOK      | 成功               |
| 非OS\_EOK    | 失败               |

#### mo_bt_mac_set

该函数用于设置蓝牙MAC地址，其函数原型如下：

```c
os_err_t mo_bt_mac_set(mo_object_t *self, char bt_mac[BT_MAX_HWADDR_LEN]);
```

| **参数**     | **说明**           |
| :----------- | :----------------- |
| self         | 模组对象            |
| bt_mac       | MAC地址字符串       |
| **返回**     | **说明**           |
| OS\_EOK      | 成功               |
| 非OS\_EOK    | 失败               |

#### mo_bt_mac_get

该函数用于查询蓝牙MAC地址，其函数原型如下：

```c
os_err_t mo_bt_mac_get(mo_object_t *self, char bt_mac_buf[BT_MAX_HWADDR_LEN]);
```

| **参数**     | **说明**           |
| :----------- | :----------------- |
| self         | 模组对象            |
| bt_mac_buf   | 用户buff用于存储MAC地址字符串 |
| **返回**     | **说明**           |
| OS\_EOK      | 成功               |
| 非OS\_EOK    | 失败               |

#### mo_bt_adv_mode_set

该函数用于设置蓝牙广播模式，其函数原型如下：

```c
os_err_t mo_bt_adv_mode_set(mo_object_t *self, bt_adv_mode_t adv_mode);
```

| **参数**     | **说明**           |
| :----------- | :----------------- |
| self         | 模组对象            |
| adv_mode     | 广播模式            |
| **返回**     | **说明**           |
| OS\_EOK      | 成功               |
| 非OS\_EOK    | 失败               |

#### mo_bt_adv_mode_get

该函数用于查询蓝牙广播模式，其函数原型如下：

```c
os_err_t mo_bt_adv_mode_get(mo_object_t *self, bt_adv_mode_t *adv_mode_buf);
```

| **参数**     | **说明**           |
| :----------- | :----------------- |
| self         | 模组对象            |
| adv_mode_buf | 用户存储广播模式的buff |
| **返回**     | **说明**           |
| OS\_EOK      | 成功               |
| 非OS\_EOK    | 失败               |

广播模式定义如下：

```c
typedef enum bt_adv_mode
{
    BT_ADV_DISABLE = 0,
    BT_ADV_ENABLE,
    BT_ADV_RESERVED,
} bt_adv_mode_t;
```

#### mo_bt_adv_interval_set

该函数用于设置蓝牙广播间隔，其函数原型如下：

```c
os_err_t mo_bt_adv_interval_set(mo_object_t *self, uint32_t interval);
```

| **参数**     | **说明**           |
| :----------- | :----------------- |
| self         | 模组对象            |
| interval     | 蓝牙广播间隔，单位[0.625ms] |
| **返回**     | **说明**           |
| OS\_EOK      | 成功               |
| 非OS\_EOK    | 失败               |

#### mo_bt_adv_interval_get

该函数用于查询蓝牙广播间隔，其函数原型如下：

```c
os_err_t mo_bt_adv_interval_get(mo_object_t *self, uint32_t *interval_buf);
```

| **参数**     | **说明**           |
| :----------- | :----------------- |
| self         | 模组对象            |
| interval_buf | 用户存储蓝牙广播间隔buff，单位[0.625ms] |
| **返回**     | **说明**           |
| OS\_EOK      | 成功               |
| 非OS\_EOK    | 失败               |

#### mo_bt_connect_interval_set

该函数用于设置蓝牙连接间隔，其函数原型如下：

```c
os_err_t mo_bt_connect_interval_set(mo_object_t *self, uint32_t interval);
```

| **参数**     | **说明**           |
| :----------- | :----------------- |
| self         | 模组对象            |
| interval     | 蓝牙连接间隔，单位[0.625ms] |
| **返回**     | **说明**           |
| OS\_EOK      | 成功               |
| 非OS\_EOK    | 失败               |

#### mo_bt_connect_interval_get

该函数用于查询蓝牙连接间隔，其函数原型如下：

```c
os_err_t mo_bt_connect_interval_get(mo_object_t *self, uint32_t *interval_buf);
```

| **参数**     | **说明**           |
| :----------- | :----------------- |
| self         | 模组对象            |
| interval_buf | 用户存储蓝牙连接间隔buff，单位[0.625ms] |
| **返回**     | **说明**           |
| OS\_EOK      | 成功               |
| 非OS\_EOK    | 失败               |

#### mo_bt_pin_set

该函数用于设置蓝牙PIN码，其函数原型如下：

```c
os_err_t mo_bt_pin_set(mo_object_t *self, char bt_pin[BT_PIN_STR_LEN]);
```

| **参数**     | **说明**           |
| :----------- | :----------------- |
| self         | 模组对象            |
| bt_pin       | 蓝牙PIN码字符串     |
| **返回**     | **说明**           |
| OS\_EOK      | 成功               |
| 非OS\_EOK    | 失败               |

#### mo_bt_pin_get

该函数用于查询蓝牙PIN码，其函数原型如下：

```c
os_err_t mo_bt_pin_get(mo_object_t *self, char bt_pin_buf[BT_PIN_STR_LEN]);
```

| **参数**     | **说明**           |
| :----------- | :----------------- |
| self         | 模组对象            |
| bt_pin_buf   | 用户存储蓝牙PIN码字符串 |
| **返回**     | **说明**           |
| OS\_EOK      | 成功               |
| 非OS\_EOK    | 失败               |

#### mo_bt_pin_mode_set

该函数用于设置蓝牙PIN码模式，其函数原型如下：

```c
os_err_t mo_bt_pin_mode_set(mo_object_t *self, bt_pin_mode_t pin_mode);
```

| **参数**     | **说明**           |
| :----------- | :----------------- |
| self         | 模组对象            |
| pin_mode     | 蓝牙PIN码模式       |
| **返回**     | **说明**           |
| OS\_EOK      | 成功               |
| 非OS\_EOK    | 失败               |

#### mo_bt_pin_mode_get

该函数用于查询蓝牙PIN码模式，其函数原型如下：

```c
os_err_t mo_bt_pin_mode_get(mo_object_t *self, bt_pin_mode_t *pin_mode_buf);
```

| **参数**     | **说明**           |
| :----------- | :----------------- |
| self         | 模组对象            |
| pin_mode_buf | 用户存储蓝牙PIN码模式 |
| **返回**     | **说明**           |
| OS\_EOK      | 成功               |
| 非OS\_EOK    | 失败               |

PIN码模式定义如下：

```c
typedef enum bt_pin_mode
{
    BT_PIN_DISABLE = 0,
    BT_PIN_ENABLE,
    BT_PIN_RESERVED,
} bt_pin_mode_t;
```

#### mo_bt_reboot

该函数用于触发蓝牙模组重启，其函数原型如下：

```c
os_err_t mo_bt_reboot(mo_object_t *self);
```

| **参数**     | **说明**           |
| :----------- | :----------------- |
| self         | 模组对象            |
| **返回**     | **说明**           |
| OS\_EOK      | 成功               |
| 非OS\_EOK    | 失败               |

#### mo_bt_disconnect

该函数用于触发蓝牙模组断开连接，其函数原型如下：

```c
os_err_t mo_bt_disconnect(mo_object_t *self);
```

| **参数**     | **说明**           |
| :----------- | :----------------- |
| self         | 模组对象            |
| **返回**     | **说明**           |
| OS\_EOK      | 成功               |
| 非OS\_EOK    | 失败               |

#### mo_bt_radio_power_level_set

该函数用于设置蓝牙功率等级，其函数原型如下：

```c
os_err_t mo_bt_radio_power_level_set(mo_object_t *self, uint32_t radio_level);
```

| **参数**     | **说明**           |
| :----------- | :----------------- |
| self         | 模组对象            |
| radio_level  | 蓝牙功率等级        |
| **返回**     | **说明**           |
| OS\_EOK      | 成功               |
| 非OS\_EOK    | 失败               |

#### mo_bt_radio_power_level_get

该函数用于查询蓝牙功率等级，其函数原型如下：

```c
os_err_t mo_bt_radio_power_level_get(mo_object_t *self, uint32_t *radio_level_buf);
```

| **参数**     | **说明**           |
| :----------- | :----------------- |
| self         | 模组对象            |
| radio_level_buf | 用户存储蓝牙功率等级buff |
| **返回**     | **说明**           |
| OS\_EOK      | 成功               |
| 非OS\_EOK    | 失败               |

### 7. WIFI控制接口

WIFI控制接口提供WIFI基本配置查询及设置功能，针对采用类AT命令方式进行蓝牙控制命令交互的WIFI模组。

| **接口**                   | **说明**                         |
| :--------------------------| :------------------------------- |
| mo_wifi_set_mode           | 设置WIFI模式                      |
| mo_wifi_get_mode           | 查询WIFI模式                      |
| mo_wifi_get_stat           | 查询WIFI状态                      |
| mo_wifi_get_sta_cip        | 查询WIFI station模式的CIP         |
| mo_wifi_set_ap_cip         | 设置WIFI AP模式下CIP              |
| mo_wifi_get_ap_cip         | 查询WIFI AP模式的CIP              |
| mo_wifi_get_sta_mac        | 查询WIFI station模式的MAC地址     |
| mo_wifi_get_ap_mac         | 查询WIFI AP模式的MAC地址          |
| mo_wifi_scan_info          | 获取扫描信息                      |
| mo_wifi_scan_info_free     | 获取的扫描信息buffer              |
| mo_wifi_connect_ap         | 发起连接至指定AP                  |
| mo_wifi_disconnect_ap      | 断开AP连接                        |
| mo_wifi_start_ap           | 启动AP                           |
| mo_wifi_stop_ap            | 停止AP                           |

#### mo_wifi_set_mode

该函数用于设置WIFI模式，其函数原型如下：

```c
os_err_t mo_wifi_set_mode(mo_object_t *module, mo_wifi_mode_t mode);
```

| **参数**     | **说明**           |
| :----------- | :----------------- |
| module       | 模组对象            |
| mode         | WIFI模式           |
| **返回**     | **说明**           |
| OS\_EOK      | 成功               |
| 非OS\_EOK    | 失败               |

#### mo_wifi_get_mode

该函数用于查询WIFI模式，其函数原型如下：

```c
mo_wifi_mode_t mo_wifi_get_mode(mo_object_t *module);
```

| **参数**     | **说明**           |
| :----------- | :----------------- |
| module       | 模组对象            |
| **返回**     | **说明**           |
| MO\_WIFI\_MODE\_NULL  | 失败       |
| 非MO\_WIFI\_MODE\_NULL| 成功       |

#### mo_wifi_get_stat

该函数用于查询WIFI状态，其函数原型如下：

```c
mo_wifi_stat_t mo_wifi_get_stat(mo_object_t *module);
```

| **参数**     | **说明**           |
| :----------- | :----------------- |
| module       | 模组对象            |
| **返回**     | **说明**           |
| MO\_WIFI\_MODE\_NULL | 失败       |
| 非MO\_WIFI\_MODE\_NULL| 成功      |

#### mo_wifi_get_sta_cip

该函数用于查询WIFI station模式的CIP，其函数原型如下：

```c
os_err_t mo_wifi_get_sta_cip(mo_object_t *module,
                             ip_addr_t *ip,
                             ip_addr_t *gw,
                             ip_addr_t *mask,
                             ip_addr_t *ip6_ll,
                             ip_addr_t *ip6_gl);
```

| **参数**     | **说明**           |
| :----------- | :----------------- |
| module       | 模组对象            |
| ip           | IP地址             |
| gw           | 网关IP             |
| mask         | 掩码               |
| ip6_ll       | IPV6本地链路地址    |
| ip6_gl       | IPV6全局地址        |
| **返回**     | **说明**           |
| OS\_EOK      | 成功               |
| 非OS\_EOK    | 失败               |

#### mo_wifi_set_ap_cip

该函数用于设置WIFI AP模式下CIP，其函数原型如下：

```c
os_err_t mo_wifi_set_ap_cip(mo_object_t *module, char *ip, char *gw, char *mask);
```

| **参数**     | **说明**           |
| :----------- | :----------------- |
| module       | 模组对象            |
| ip           | IP地址             |
| gw           | 网关IP             |
| mask         | 掩码               |
| **返回**     | **说明**           |
| OS\_EOK      | 成功               |
| 非OS\_EOK    | 失败               |

#### mo_wifi_get_ap_cip

该函数用于查询WIFI AP模式的CIP，其函数原型如下：

```c
os_err_t mo_wifi_get_ap_cip(mo_object_t *module, ip_addr_t *ip, ip_addr_t *gw, ip_addr_t *mask);
```

| **参数**     | **说明**           |
| :----------- | :----------------- |
| module       | 模组对象            |
| ip           | IP地址             |
| gw           | 网关IP             |
| mask         | 掩码               |
| **返回**     | **说明**           |
| OS\_EOK      | 成功               |
| 非OS\_EOK    | 失败               |

#### mo_wifi_get_sta_mac

该函数用于查询WIFI station模式的MAC地址，其函数原型如下：

```c
os_err_t mo_wifi_get_sta_mac(mo_object_t *module, char mac[]);
```

| **参数**     | **说明**           |
| :----------- | :----------------- |
| module       | 模组对象            |
| mac          | 用户存储获取MAC地址的buffer |
| **返回**     | **说明**           |
| OS\_EOK      | 成功               |
| 非OS\_EOK    | 失败               |

#### mo_wifi_get_ap_mac

该函数用于查询WIFI AP模式的MAC地址，其函数原型如下：

```c
os_err_t mo_wifi_get_ap_mac(mo_object_t *module, char mac[]);
```

| **参数**     | **说明**           |
| :----------- | :----------------- |
| module       | 模组对象            |
| mac          | 用户存储获取MAC地址的buffer |
| **返回**     | **说明**           |
| OS\_EOK      | 成功               |
| 非OS\_EOK    | 失败               |

#### mo_wifi_scan_info

该函数用于获取扫描信息，其函数原型如下：

```c
os_err_t mo_wifi_scan_info(mo_object_t *module, char *ssid, mo_wifi_scan_result_t *scan_result);
```

| **参数**     | **说明**           |
| :----------- | :----------------- |
| module       | 模组对象            |
| ssid         | SSID置空则全部扫描   |
| scan_result  | 用户指针用于接收扫描结果 |
| **返回**     | **说明**           |
| OS\_EOK      | 成功               |
| 非OS\_EOK    | 失败               |

#### mo_wifi_scan_info_free

该函数用于释放获取的扫描信息buffer，其函数原型如下：

```c
void mo_wifi_scan_info_free(mo_wifi_scan_result_t *scan_result);
```

| **参数**     | **说明**           |
| :----------- | :----------------- |
| scan_result  | mo_wifi_scan_info返回的scan_result |
| **返回**     | **说明**           |
| void         | void               |

#### mo_wifi_connect_ap

该函数用于发起连接至指定AP，其函数原型如下：

```c
os_err_t mo_wifi_connect_ap(mo_object_t *module, const char *ssid, const char *password);
```

| **参数**     | **说明**           |
| :----------- | :----------------- |
| module       | 模组对象            |
| ssid         | Service Set Identifier |
| password     | AP密码             |
| **返回**     | **说明**           |
| OS\_EOK      | 成功               |
| 非OS\_EOK    | 失败               |

#### mo_wifi_disconnect_ap

该函数用于断开AP连接，其函数原型如下：

```c
os_err_t mo_wifi_disconnect_ap(mo_object_t *module);
```

| **参数**     | **说明**           |
| :----------- | :----------------- |
| module       | 模组对象            |
| **返回**     | **说明**           |
| OS\_EOK      | 成功               |
| 非OS\_EOK    | 失败               |

#### mo_wifi_start_ap

该函数用于启动AP，其函数原型如下：

```c
os_err_t mo_wifi_start_ap(mo_object_t *module, const char *ssid, const char *password, uint8_t channel, uint8_t ecn);
```

| **参数**     | **说明**           |
| :----------- | :----------------- |
| module       | 模组对象            |
| ssid         | Service Set Identifier |
| password     | AP连接密码          |
| channel      | 通道号              |
| ecn          | 加密方式            |
| **返回**     | **说明**           |
| OS\_EOK      | 成功               |
| 非OS\_EOK    | 失败               |

#### mo_wifi_stop_ap

该函数用于停止AP，其函数原型如下：

```c
os_err_t mo_wifi_stop_ap(mo_object_t *module);
```

| **参数**     | **说明**           |
| :----------- | :----------------- |
| module       | 模组对象            |
| **返回**     | **说明**           |
| OS\_EOK      | 成功               |
| 非OS\_EOK    | 失败               |

### 7. 模组自动识别功能

MoLink部分模组提供了多模组自动识别的创建方式，在用户代码不变的情况下，切换不同模组，可不必再反复配置，交由MoLink自动识别当前模组并自动完成初始化。

#### 如何添加自动识别功能

目前功能适配BC20/BC26模组，其余模组根据项目需要，用户可参考BC20/BC26模组，自行添加自动识别相关代码。需要修改的文件为`conponents/net/molink/module/modulename/modulename.c`及对应`Kconfig`文件。

如果使用GCC编译注意需要在链接脚本中添加对应的`MolinkTab`段。

#### 使用前适配模组初始化特殊预处理

> 部分用户在初始化前可能涉及模组GPIO等相关控制操作，用户可将此函数挂入模组文件中，模组信息导出宏的misc_fn位置，以BC20模组为例，修改模组适配文件主文件`conponents/net/molink/module/bc20/bc20.c`，修改命令`MO_MODULE_EXPORT`中``misc_fn``位置为对应预处理函数即可。

```c
// 相关定义及说明

typedef struct mo_auto_find_info
{
    const char *name;               /* export module name */
    const char *at_cmd;             /* export module indentify AT command */
    const char *identifier;         /* export module identifier */
    const char *device_name;        /* export module device name */
    const int   uart_baud_rate;     /* export module device baud rate */
    const mo_misc_fn_t misc_fn;     /* export module device misc func, for module init preparation */
    const init_call_fn_t init_fn;     /* export module init func */
} mo_auto_find_info_t;

MO_MODULE_EXPORT(name, at_cmd, indentifier, device_name, uart_baud_rate, misc_fn, init_fn)
```

#### 配置工程

用户选定此模组自动识别功能，编译烧录即可使用。

> 用户选定所有可能需使用的模组并选择启用自动识别功能``AUTO PROBE``，其余配置与自动创建相同。以BC20模组为例

```c
[*] Enable BC20 Module Object Auto Create               ------->启用自动创建并配置好相关信息
[*]     Let MoLink determine whether the module exists. ------->选定自动识别功能
(BC20)      autofind identifier.                        ------->确认模组特征值eg.(AT+CGMM/CGMR)
(uart1) BC20 Interface Device Name                      ------->配置串口号
(115200) BC20 Interface Device Rate                     ------->配置预设波特率
(1500)  The maximum length of AT command data accepted  
-*- Enable BC20 Module General Operates
-*- Enable BC20 Module Network Service Operates
[*] Enable BC20 Module Ping Operates
[*] Enable BC20 Module Ifconfig Operates
[*] Enable BC20 Module Network TCP/IP Operates
[ ] Enable BC20 Module BSD Socket Operates
[ ] Enable BC20 Module CTM2M Operates
```

### 8. CTM2M云平台连接

MoLink模组连接套件支持电信云平台连接功能，提供了基于LWM2M协议对接电信AEP/OC平台的功能接口。

| **接口**                   | **说明**                         |
| :--------------------------| :------------------------------- |
| ctm2m_create               | 初始化接口                        |
| ctm2m_destroy              | 反初始化接口                      |
| ctm2m_set_ue_cfg           | UE配置接口                        |
| ctm2m_get_ue_cfg           | UE配置查询接口                    |
| ctm2m_register             | 向平台发起注册请求接口             |
| ctm2m_deregister           | 向平台发起注销请求接口             |
| ctm2m_send                 | 向平台发送业务数据接口             |
| ctm2m_resp                 | 命令响应接口                      |
| ctm2m_update               | 发送更新请求接口                  |

#### ctm2m_create

CTM2M功能初始化接口，其函数原型如下：

```c
ctm2m_t *ctm2m_create(mo_object_t *module, ctm2m_create_parm_t parm);
```

| **参数**     | **说明**           |
| :----------- | :----------------- |
| module       | 模组对象            |
| parm         | @ref: ctm2m_create_parm_t ctm2m初始化相关参数 |
| **返回**     | **说明**           |
| 返回值         | 成功返回ctm2m句柄，失败返回OS_NULL |

初始化相关结构体定义：

```c
typedef struct ctm2m_create_parm
{
    ctm2m_string_t      server_ip;  /* LWM2M 服务器IP地址，字符串形式 */
    uint32_t         port;       /* 服务器端口号 */
    uint32_t         lifetime;   /* lifetime 最小值为300 */
    ctm2m_string_t      obj_ins_list; 
        /* (可选项) 模组支持的obj/ins列表 格式示例：</3303/0>,</3303/1> */
    ctm2m_notify_cb_t   notify_cb;  /* 用户回调:用于接收服务器下发通知信息 */
    ctm2m_request_cb_t  request_cb; /* 用户回调:用于接收服务器下发请求/命令 */ 
    ctm2m_receive_cb_t  receive_cb; /* 用户回调：用于接收服务器下发数据 */
} ctm2m_create_parm_t;
```

相关用户回调：

```c
// 用户接收服务器通知回调
typedef void (*ctm2m_notify_cb_t)  (ctm2m_notify_t notify);
// 用户接收服务器请求(命令)回调
typedef void (*ctm2m_request_cb_t) (ctm2m_request_t request);
// 用户接收服务器下发数据回调
typedef void (*ctm2m_receive_cb_t) (ctm2m_string_t data);
```

#### ctm2m_destroy

CTM2M功能反初始化接口，其函数原型如下：

```c
os_err_t ctm2m_destroy(ctm2m_t *handle);
```

| **参数**     | **说明**           |
| :----------- | :----------------- |
| handle       | ctm2m句柄          |
| **返回**     | **说明**           |
| OS\_EOK      | 成功               |
| 非OS\_EOK    | 失败               |

#### ctm2m_set_ue_cfg

CTM2M UE配置接口，其函数原型如下：

```c
os_err_t ctm2m_set_ue_cfg(ctm2m_t *handle, ctm2m_ue_cfg_t cfg);
```

| **参数**     | **说明**           |
| :----------- | :----------------- |
| handle       | ctm2m句柄          |
| cfg          | 指定配置选项及值    |
| **返回**     | **说明**           |
| OS\_EOK      | 成功               |
| 非OS\_EOK    | 失败               |

UE配置相关类型定义：

```c
typedef struct ctm2m_ue_cfg
{
    ctm2m_uemode_t mode;    /* 指定需要配置UE的项 @ref:ctm2m_uemode_t */
    uint32_t    cfg;     /* 对应项配置值 */
} ctm2m_ue_cfg_t;

// 配置UE的项
typedef enum ctm2m_uemode
{
    CTM2M_UEMODE_NULL = 0,              /* invalid value */
    CTM2M_UEMODE_IDAUTH,                /* 1: IDAuth_Mode */
    CTM2M_UEMODE_TAUTIMER_UPDATE,       /* 2: Auto_TAUTimer_Update */
    CTM2M_UEMODE_UQ_MODE,               /* 3: ON_UQMode */
    CTM2M_UEMODE_CE_LVL2_POLICY,        /* 4: ON_CELevel2Policy */
    CTM2M_UEMODE_HEARTBEAT_UPDATE,      /* 5: Auto_Heartbeat */
    CTM2M_UEMODE_WAKEUP_NOTIFY,         /* 6: Wakeup_Notify */
    CTM2M_UEMODE_PROTOCOL_MODE,         /* 7: Protocol_Mode */
} ctm2m_uemode_t;

// 每项配置对应值
typedef enum ctm2m_auth_cfg
{
    CTM2M_AUTH_NULL = 0,                /* invalid value */
    CTM2M_AUTH_DEFAULT,                 /* default value, no authentication string */
    CTM2M_AUTH_OUTSIDE_SIMD,            /* SIMD authentication string from outside of module */
    CTM2M_AUTH_OUTSIDE_SM9,             /* SM9  authentication string from outside of module */
    CTM2M_AUTH_INSIDE_SIMD,             /* SIMD authentication string from inside of module  */
    CTM2M_AUTH_INSIDE_SM9,              /* SM9  authentication string from inside of module  */
} ctm2m_auth_cfg_t;

typedef enum ctm2m_tautimer_cfg
{
    CTM2M_TAU_NULL = 0,                 /* invalid value */
    CTM2M_TAU_DEFAULT,                  /* default no action */
    CTM2M_TAU_NOTIFY_MCU_ON,            /* notify MCU */
    CTM2M_TAU_NOTIFY_MCU_OFF,           /* not notify MCU, auto update inside of module */
} ctm2m_tautimer_cfg_t;

typedef enum ctm2m_uq_cfg
{
    CTM2M_UQ_NULL = 0,                  /* invalid value */
    CTM2M_UQ_MODE_OFF,                  /* UQ mode off */
    CTM2M_UQ_MODE_ON,                   /* UQ mode on */
} ctm2m_uq_cfg_t;

typedef enum ctm2m_ce_cfg
{
    CTM2M_CE_LVL2_NULL = 0,             /* invalid value */
    CTM2M_CE_LVL2_SEND_ON,              /* default send under CE level2 */
    CTM2M_CE_LVL2_SEND_OFF,             /* not send under CE level2 */
} ctm2m_ce_cfg_t;

typedef enum ctm2m_heartbeat_cfg
{
    CTM2M_AUTO_HEARTBEAT_NULL = 0,      /* invalid value */
    CTM2M_AUTO_HEARTBEAT_OFF,           /* no auto heartbeat */
    CTM2M_AUTO_HEARTBEAT_ON,            /* default auto heartbeat */
} ctm2m_heartbeat_cfg_t;

typedef enum ctm2m_wakeup_cfg
{
    CTM2M_WAKEUP_NOTIFY_NULL = 0,       /* invalid value */
    CTM2M_WAKEUP_NOTIFY_ON,             /* not notify to MCU */
    CTM2M_WAKEUP_NOTIFY_OFF,            /* default notify to MCU */
} ctm2m_wakeup_cfg_t;

typedef enum ctm2m_protocol_cfg
{
    CTM2M_PROTOCOL_NULL = 0,            /* invalid value */
    CTM2M_PROTOCOL_NOTIFY_ON,           /* protocol mode normal */
    CTM2M_PROTOCOL_NOTIFY_OFF,          /* protocol mode enhance */
} ctm2m_protocol_cfg_t;
```

#### ctm2m_get_ue_cfg

CTM2M UE配置查询接口，其函数原型如下：

```c
os_err_t ctm2m_get_ue_cfg(ctm2m_t *handle, ctm2m_ue_info_t *cfg_info);
```

| **参数**     | **说明**           |
| :----------- | :----------------- |
| handle       | ctm2m句柄          |
| cfg_info       | 查询到的UE配置信息 |
| **返回**     | **说明**           |
| OS\_EOK      | 成功               |
| 非OS\_EOK    | 失败               |

UE配置查询返回结构体定义如下：

```c
typedef struct ctm2m_ue_info
{
    ctm2m_auth_cfg_t         auth_mode;
    ctm2m_tautimer_cfg_t     tau_timer_mode;
    ctm2m_uq_cfg_t           uq_mode;
    ctm2m_ce_cfg_t           ce_mode;
    ctm2m_heartbeat_cfg_t    heartbeat_mode;
    ctm2m_wakeup_cfg_t       wakeup_mode;
    ctm2m_protocol_cfg_t     protocol_mode;
} ctm2m_ue_info_t;
```

#### ctm2m_register

向平台发起注册请求接口，其函数原型如下：

```c
os_err_t ctm2m_register(ctm2m_t *handle);
```

| **参数**     | **说明**           |
| :----------- | :----------------- |
| handle       | ctm2m句柄          |
| **返回**     | **说明**           |
| OS\_EOK      | 成功               |
| 非OS\_EOK    | 失败               |

#### ctm2m_deregister

向平台发起注销请求接口，其函数原型如下：

```c
os_err_t ctm2m_deregister(ctm2m_t *handle);
```

| **参数**     | **说明**           |
| :----------- | :----------------- |
| handle       | ctm2m句柄          |
| **返回**     | **说明**           |
| OS\_EOK      | 成功               |
| 非OS\_EOK    | 失败               |

#### ctm2m_send

向平台发送业务数据接口，其函数原型如下：

```c
os_err_t ctm2m_send(ctm2m_t *handle, ctm2m_send_t send, int32_t *send_msg_id);
```

| **参数**     | **说明**           |
| :----------- | :----------------- |
| handle       | ctm2m句柄          |
| send         | 发送业务数据内容及模式 |
| send_msg_id  | 用户指针，用于接收此条发送消息的消息ID |
| **返回**     | **说明**           |
| OS\_EOK      | 成功               |
| 非OS\_EOK    | 失败               |

重要参数说明：

```c
// 发送模式
typedef enum ctm2m_send_mode
{
    CTM2M_SEND_MODE_CON = 0,                 /* 0---CON mode */
    CTM2M_SEND_MODE_NON,                     /* 1---NON mode */
    CTM2M_SEND_MODE_NON_RAI,                 /* 2---NON with RAI flag */
    CTM2M_SEND_MODE_CON_RAI,                 /* 3---CON with RAI flag */
} ctm2m_send_mode_t;

typedef struct ctm2m_send
{
    ctm2m_send_mode_t mode;                   /* (可选项) 发送模式 */
    ctm2m_string_t    msg;                    /* 发送内容 字符串格式(可根据需求变更) */
} ctm2m_send_t;
```

#### ctm2m_resp

命令(请求命令)响应接口，其函数原型如下：

```c
os_err_t ctm2m_resp(ctm2m_t *handle, ctm2m_resp_t resp);
```

| **参数**     | **说明**           |
| :----------- | :----------------- |
| handle       | ctm2m句柄          |
| resp         | 响应内容            |
| **返回**     | **说明**           |
| OS\_EOK      | 成功               |
| 非OS\_EOK    | 失败               |

重要参数说明：

```c
typedef struct ctm2m_resp
{
    int32_t          msg_id;               /* msgID:从回调获取 */
    ctm2m_string_t      token;                /* token:从回调获取 */
    int32_t          resp_code;            /* 消息响应码:详见spec. */
    ctm2m_string_t      uri;                  /* uri:从回调获取 */
    ctm2m_observe_cfg_t observe;              /* observe:如下所示 */
    ctm2m_dataform_t    dataformate;          /* (可选项) 数据格式 */
    ctm2m_string_t      data;                 /* (可选项) 响应数据 */
} ctm2m_resp_t;

/* 
** 以MB26模组为例：mb26 observe defination
**
** 0---set observe and no following parameters
** 1---cancel observe and no following parameters
** 8---just for response case
** 9---there are <Dataformat>,<data> parameters following
*/
```

#### ctm2m_update

发送更新请求接口，其函数原型如下：

```c
os_err_t ctm2m_update(ctm2m_t *handle, ctm2m_update_t update);
```

| **参数**     | **说明**           |
| :----------- | :----------------- |
| handle       | ctm2m句柄          |
| update       | 更新内容            |
| **返回**     | **说明**           |
| OS\_EOK      | 成功               |
| 非OS\_EOK    | 失败               |

重要参数说明：

```c
typedef enum ctm2m_binding_mode
{
    CTM2M_BINDING_NULL = 0,                     /* 0: Binding Not set */
    CTM2M_BINDING_UQ_MODE,                      /* 1: Binding UQ mode */
    CTM2M_BINDING_U_MODE,                       /* 2: Binding U mode */
} ctm2m_binding_mode_t;

typedef struct ctm2m_update
{
    ctm2m_binding_mode_t mode;                  /* (可选项)绑定模式 */
    ctm2m_string_t       obj_list;              /* (可选项)对象列表字符串 */
} ctm2m_update_t;
```

#### CTM2M初始化

在初始化云平台连接之前，请确保模组驻网且准备就绪，可使用`os_err_t mo_get_reg(mo_object_t *self, eps_reg_info_t *info)`接口获取当前网络状态

```c
    ctm2m_create_parm_t parm;
    memset(&parm, 0, sizeof(ctm2m_create_parm_t));
    
    parm.server_ip.data = TEST_SERVER_IP;             // 服务器地址
    parm.server_ip.len  = strlen(TEST_SERVER_IP);
    parm.port           = TEST_SERVER_PORT;           // 服务器端口号
    parm.lifetime       = TEST_LIFETIME;              // lifetime (需注意spec.定义范围)
    parm.notify_cb      = notify_handler;             // 用户回调
    parm.request_cb     = request_handler;            // 用户回调
    parm.receive_cb     = receive_handler;            // 用户回调
    parm.obj_ins_list.data = "</3303/0>,</3303/1>";   // 可选项 见1.1
    parm.obj_ins_list.len  = strlen(parm.obj_ins_list.data);

    handle = ctm2m_create(module, parm);              // 初始化并返回连接实例句柄
    LOG_EXT_I("%s-%d ctm2m handle create %s.", __func__, __LINE__, OS_NULL != handle ? "SUCCESS" : "FAILED");
    if (OS_NULL == handle) return;                    // 所有接口请务必判断返回值
```

#### CTM2M可选配置-UE配置

配置UE各项的工作模式，模组有默认配置，非必须设置。可以使用接口``ctm2m_get_ue_cfg``查询当前配置。

```c
    /* 配置 */
    ctm2m_ue_cfg_t cfg = {CTM2M_UEMODE_HEARTBEAT_UPDATE, CTM2M_AUTO_HEARTBEAT_ON}; 
                                                    // 用户 UE 配置
    result = ctm2m_set_ue_cfg(handle, cfg);         // 设置
                                                    // 省略执行结果判断

    /* 查询 */
    ctm2m_ue_info_t cfg_info;
    memset(&cfg_info, 0, sizeof(ctm2m_ue_info_t));

    result = ctm2m_get_ue_cfg(handle, &cfg_info);    // 查询当前模组UE配置
                                                     // 省略执行结果判断
```

#### 注册

向平台发起注册请求 详见`ctm2m_register`

#### 业务流程

在注册完成后，即可进行相对应的业务流程。

流程可划分为两部分：`由设备侧主动发起的`和`由服务器下发请求和通知,设备侧按照需求进行响应`

- 由设备侧主动发起：进行业务数据的上报、发送更新请求。

- 由服务器下发触发：用户回调接收服务器下发的`通知`、`请求（命令）`和`业务数据下发`，用户根据业务需要去处理对应信息。

业务数据上报及更新请求:

```c
    int32_t  send_msg_id = 0;                    // 接收返回的发送消息ID
    ctm2m_send_t send;
    memset(&send, 0, sizeof(ctm2m_send_t));
    send.mode = CTM2M_SEND_MODE_CON;                // 发送模式
    send.msg.data = "3131";                         // 数据 普通字符串格式，套件会在内部转换为16进制字符串格式
    send.msg.len = strlen(send.msg.data);
    
    result = ctm2m_send(handle, send, &send_msg_id);
    
    /* 更新请求 */
    ctm2m_update_t update;
    memset(&update, 0, sizeof(ctm2m_update_t));
    update.mode = CTM2M_BINDING_NULL;               // 此例中可选项均忽略
    
    result = ctm2m_update(handle, update);
```

用户回调相关:

接收到平台下发命令通过用户回调通知到用户处理，判断若需要回复，则调用`ctm2m_resp`作出响应，其中需注意，请避免阻塞回调。

## 应用示例

### 模组实例管理接口使用示例

```c
#define RECV_BUF_LEN     (512)
#define TEST_MODULE_NAME "gm190"

/* manually create module */
mo_object_t *test_module = OS_NULL;
mo_object_t *temp_module = OS_NULL;
os_err_t     result      = OS_FAILURE;

mo_parser_config_t parser_config = {.parser_name   = TEST_MODULE_NAME,
                                    .parser_device = test_device,
                                    .recv_buff_len = RECV_BUF_LEN};

test_module = mo_create("gm190", MODULE_TYPE_GM190, &parser_config);
OS_ASSERT(OS_NULL != test_module);

/* set default module instance */
#if 0
/* auto set default when create, but also you can call this func */
result = mo_set_default(test_module);
OS_ASSERT(OS_FAILURE != result);
#endif

/* get default module instance */
temp_module = mo_get_default();
OS_ASSERT(OS_NULL == temp_module);

/* get module instance by name */
temp_module = mo_get_by_name(TEST_MODULE_NAME);
OS_ASSERT(test_module == temp_module);

/* destroy module */
result = mo_destroy(test_module, MODULE_TYPE_GM190);
OS_ASSERT(OS_SUCCESS == result);
```

### 通用控制接口使用示例

```c
#define IMEI_LEN    (15)
#define IMSI_LEN    (15)
#define ICCID_LEN   (20)
#define EID_LEN     (20)

os_err_t   result         = OS_FAILURE;
uint8_t get_cfun_lvl   = 0;
uint8_t set_cfun_lvl   = 1;
time_t     time           = 0;
struct     tm l_tm;

char imei[IMEI_LEN + 1]         = {0};
char imsi[IMSI_LEN + 1]         = {0};
char iccid[ICCID_LEN + 1]       = {0};
char eid[EID_LEN + 1]           = {0};
mo_firmware_version_t version   = {0};


mo_object_t *test_module = mo_get_default();
OS_ASSERT(OS_NULL != test_module);

/* test AT & test connection */
result = mo_at_test(test_module);
OS_ASSERT(OS_SUCCESS == result);

/* get IMEI */
result = mo_get_imei(test_module, imei, sizeof(imei));
OS_ASSERT(OS_SUCCESS == result);
os_kprintf("module imei:%s\r\n", imei);

/* get IMSI */
result = mo_get_imsi(test_module, imsi, sizeof(imsi));
OS_ASSERT(OS_SUCCESS == result);
os_kprintf("module imsi:%s\r\n", imsi);

/* get ICCID */
result = mo_get_iccid(test_module, iccid, sizeof(iccid));
OS_ASSERT(OS_SUCCESS == result);
os_kprintf("module iccid:%s\r\n", iccid);

/* set function level */
result = mo_set_cfun(test_module, set_cfun_lvl);
OS_ASSERT(OS_SUCCESS == result);

/* get function level */
result = mo_get_cfun(test_module, &get_cfun_lvl);
OS_ASSERT(OS_SUCCESS == result);
os_kprintf("module cfun:%u\r\n", cfun_lvl);

/* get module firmware version */
os_err_t result = mo_get_firmware_version(test_module, &version);
OS_ASSERT(OS_SUCCESS == result);

for (int i = 0; i < version.line_counts; i++)
{
    os_kprintf("%s\n", version.ver_info[i]);
}

mo_get_firmware_version_free(&version);

/* get eID */
result = mo_get_eid(test_module, eid, EID_LEN);
OS_ASSERT(OS_SUCCESS == result);
os_kprintf("module eid:%s\r\n", eid);

/* get broken-down time */
result = mo_gm_time(test_module, &l_tm);
OS_ASSERT(OS_SUCCESS == result);

/* get UNIX time */
result = mo_time(test_module, &time);
OS_ASSERT(OS_SUCCESS == result);
```

### 网络服务接口使用示例

```c
#include <mo_api.h>

#define INIT_DFT       (0)
#define INIT_BER_DFT   (99)
#define TEST_CID       (1)

/* for example */
#typedef enum test_attach_stat
{
    DETACHED = 0,
    ATTACHED,
    ATTACH_RESERVED,
} test_attach_stat_t;

/* for example */
#typedef enum test_reg_stat
{
    DISABLE_REG_URC = 0,
    ENABLE_REG_URC,
    ENABLE_REG_LO_URC,
    ENABLE_REG_LO_EMM_URC,
    PSM_ENABLE_REG_LO_URC,
    PSM_ENABLE_REG_LO_EMM_URC,
} test_reg_stat_t;

/* for example */
#typedef enum test_act_stat
{
    DEACTIVATED = 0,
    ACTIVATED,
    ACTIVATE_RESERVED,

} test_act_stat_t;

os_err_t   result          = OS_FAILURE;
uint8_t attach_stat_set = TEST_ATTACHED;
uint8_t attach_stat_get = INIT_DFT;
uint8_t reg_n           = ENABLE_REG_URC;
uint8_t cid_set         = TEST_CID;
uint8_t cid_get         = INIT_DFT;
uint8_t act_stat_set    = DEACTIVATED;
uint8_t act_stat_get    = INIT_DFT;
uint8_t rssi            = INIT_DFT;
uint8_t ber             = INIT_BER_DFT;

radio_info_t radio_info              = {0};
char ip_addr[IPADDR_MAX_STR_LEN + 1] = {0};

mo_psm_info_t psm_enable  = {MO_PSM_ENABLE, "", "", "00100011", "00100010"};
mo_psm_info_t psm_disable = {MO_PSM_DISABLE, "", "", "00100011", "00100010"};

eps_reg_info_t reg_info;
memset(&reg_info, 0, sizeof(eps_reg_info_t));
onepos_cell_info_t cell_info;
memset(&cell_info, 0, sizeof(onepos_cell_info_t));
mo_psm_info_t psm_info_get;
memset(&psm_info_get, 0, sizeof(mo_psm_info_t));

mo_object_t *test_module = mo_get_default();
OS_ASSERT(OS_NULL != test_module);

/* set module attach state */
mo_set_attach(est_module, attach_stat_set);
OS_ASSERT(OS_SUCCESS == result);

/* get module attach state */
result = mo_get_attach(test_module, &attach_stat_get);
OS_ASSERT(OS_SUCCESS == result);
os_kprintf("module attach state:%u\r\n", attach_stat_get);


/* set the presentation of an network registration urc data */
result = mo_set_reg(test_module, reg_n);
OS_ASSERT(OS_SUCCESS == result);

/*
 * get the presentation of an network registration urc data
 * and the network registration status
 */
result = mo_get_reg(test_module, &reg_info);
OS_ASSERT(OS_SUCCESS == result);
os_kprintf("module register n:%u"\r\n,     reg_info.reg_n);
os_kprintf("module register state:%u\r\n", reg_info.reg_stat);

/* set activate or deactivate PDP Context */
result = mo_set_cgact(test_module, cid_set, act_stat_set);
OS_ASSERT(OS_SUCCESS == result);

/* get the state of PDP context activation */
result = mo_get_cgact(test_module, &cid_get, &act_stat_get);
OS_ASSERT(OS_SUCCESS == result);
os_kprintf("module PDP context cid:%u,act stat:%u\r\n", cid_get, act_stat_get);

/* get the csq info */
result = mo_get_csq(test_module, &rssi, &ber);
OS_ASSERT(OS_SUCCESS == result);
os_kprintf("module csq rssi:%u\r\n", rssi);
os_kprintf("module csq ber:%u\r\n", ber);

/* get module radio info */
result = mo_get_radio(test_module, &radio_info);
OS_ASSERT(OS_SUCCESS == result);
os_kprintf("module cell id:%s\r\n", radio_info.cell_id);
os_kprintf("module ecl:%d\r\n",     radio_info.ecl);
os_kprintf("module snr:%d\r\n",     radio_info.snr);
os_kprintf("module earfcn:%d\r\n",  radio_info.earfcn);
os_kprintf("module rsrq:%d\r\n",    radio_info.rsrq);

/* get module ip address */
result = mo_get_ipaddr(test_module, ip_addr);
OS_ASSERT(OS_SUCCESS == result);
os_kprintf("get_ipaddr IP:%s\r\n", ip_addr);

/* get cell info */
result = mo_get_cell_info(test_module, &cell_info);
OS_ASSERT(OS_SUCCESS == result);
os_kprintf("module cell_info cell_num:%d\r\n", cell_info.cell_num);
os_kprintf("module cell_info net_type:%d\r\n", cell_info.net_type);
os_kprintf("module cell_info mnc:%d\r\n",      cell_info.cell_info->mnc);
os_kprintf("module cell_info mcc:%d\r\n",      cell_info.cell_info->mcc);
os_kprintf("module cell_info lac:%d\r\n",      cell_info.cell_info->lac);
os_kprintf("module cell_info cid:%d\r\n",      cell_info.cell_info->cid);
os_kprintf("module cell_info ss:%d\r\n",       cell_info.cell_info->ss);

/* set PSM(power saving mode) enable & other configuration */
result = mo_set_psm(test_module, psm_enable);
OS_ASSERT(OS_SUCCESS == result);

/* get get module PSM(power saving mode) info */
result = mo_get_psm(test_module, &psm_info_get);
OS_ASSERT(OS_SUCCESS == result);
os_kprintf("module psm_mode:%d\r\n",         psm_info_get.psm_mode);
os_kprintf("module periodic_rau:%s\r\n",     psm_info_get.periodic_rau);
os_kprintf("module gprs_ready_timer:%s\r\n", psm_info_get.gprs_ready_timer);
os_kprintf("module periodic_tau:%s\r\n",     psm_info_get.periodic_tau);
os_kprintf("module active_time:%s\r\n",      psm_info_get.active_time);
```

### MQTT客户端接口使用示例

```c
static void test_mqttc_handler(mqttc_msg_data_t *data)
{
    os_kprintf("Message arrived on topic %.*s: %.*s\n",
               data->topic_name.len,
               data->topic_name.data,
               data->message.payload_len,
               data->message.payload);
}

static void test_mqttc_start_task(void)
{
    mqttc_create_opts_t create_opts = {
        .address = 
        {
            .data = TEST_HOST, 
            .len = strlen(TEST_HOST)
        }, 
        .port = TEST_PORT
    };

    mo_mqttc_t *mqttc = mo_mqttc_create(test_module, &create_opts);

    mqttc_conn_opts_t conn_opts = {
        .client_id = {
            .data = TEST_CLIENT_ID,
            .len  = strlen(TEST_CLIENT_ID),
        },
        .mqtt_version  = 4,
        .keep_alive    = 60,
        .clean_session = 1,
        .will_flag     = 0,
    };

    os_err_t result = mo_mqttc_connect(mqttc, &conn_opts);

    result = mo_mqttc_start_task(mqttc);

    result = mo_mqttc_subscribe(mqttc, TEST_TOPIC, MQTTC_QOS_1, test_mqttc_handler);

    for (int i = 0; i < 5; i++)
    {
        char payload[64] = {0};
        sprintf(payload, "message number %d", i);

        mqttc_msg_t msg = {
            .qos = MQTTC_QOS_1, 
            .retained = 0, 
            .payload = payload, 
            .payload_len = strlen(payload)
        };

        result = mo_mqttc_publish(mqttc, TEST_TOPIC, &msg);
    }

    mo_mqttc_disconnect(mqttc);

    mo_mqttc_destroy(mqttc);
}
```

### 蓝牙控制接口使用示例

```c
#include <mo_api.h>

#define TEST_YQ8228_NAME            "yq8228"
#define TEST_YQ8228_BLE_NAME        "DAIAO"
#define TEST_NAME_BUF_MAX_SIZE      (128)
#define TEST_YQ8228_RECV_BUFF_LEN   (512)
#define TEST_YQ8228_DEVICE_RATE     (19200)

/* set&get bt device name */
char name_get[TEST_NAME_BUF_MAX_SIZE] = {0};

result = mo_bt_device_name_set(module, TEST_YQ8228_BLE_NAME, strlen(TEST_YQ8228_BLE_NAME));
OS_ASSERT(OS_SUCCESS == result);

result = mo_bt_device_name_get(module, name_get, TEST_NAME_BUF_MAX_SIZE);
OS_ASSERT(OS_SUCCESS == result);
os_kprintf("BT name:[%.*s]\r\n", TEST_NAME_BUF_MAX_SIZE, name_get);


/* set&get bt MAC */
char  bt_mac_buf[BT_MAX_HWADDR_STR_LEN] = "112233445566";
char *bt_mac_buf_dynamic = OS_NULL;

result = mo_bt_mac_set(module, bt_mac_buf);
OS_ASSERT(OS_SUCCESS == result);
memset(bt_mac_buf, 0, BT_MAX_HWADDR_STR_LEN);

result = mo_bt_mac_get(module, bt_mac_buf);
OS_ASSERT(OS_SUCCESS == result);
os_kprintf("BT MAC:[%.*s]\r\n", BT_MAX_HWADDR_LEN, bt_mac_buf);

/* set&get bt MAC(dynamic) */
bt_mac_buf_dynamic = os_calloc(1, BT_MAX_HWADDR_STR_LEN);
OS_ASSERT(OS_NULL != bt_mac_buf_dynamic);

memset(bt_mac_buf_dynamic, '1', BT_MAX_HWADDR_LEN);
result = mo_bt_mac_set(module, bt_mac_buf_dynamic);
OS_ASSERT(OS_SUCCESS == result);

memset(bt_mac_buf_dynamic, 0, BT_MAX_HWADDR_STR_LEN);
result = mo_bt_mac_get(module, bt_mac_buf_dynamic);
OS_ASSERT(OS_SUCCESS == result);
os_kprintf("BT MAC:[%.*s]\r\n", BT_MAX_HWADDR_LEN, bt_mac_buf_dynamic);

/* set&get bt advertise mode */
bt_adv_mode_t adv_mode_get = BT_ADV_ENABLE;

result = mo_bt_adv_mode_set(module, BT_ADV_DISABLE);
OS_ASSERT(OS_SUCCESS == result);

result = mo_bt_adv_mode_get(module, &adv_mode_get);
OS_ASSERT(OS_SUCCESS == result);
os_kprintf("BT ADV MODE:[%d]\r\n", adv_mode_get);

/* set&get bt advertise and connect interval, unit[0.625ms] */
uint32_t adv_interval_get = 0;
uint32_t conn_interval_get = 0;

result = mo_bt_adv_interval_set(module, 50);
OS_ASSERT(OS_SUCCESS == result);

result = mo_bt_adv_interval_get(module, &adv_interval_get);
OS_ASSERT(OS_SUCCESS == result);
os_kprintf("BT ADV int:[%d]\r\n", adv_interval_get);

result = mo_bt_connect_interval_set(module, 30);
OS_ASSERT(OS_SUCCESS == result);

result = mo_bt_connect_interval_get(module, &conn_interval_get);
OS_ASSERT(OS_SUCCESS == result);
os_kprintf("BT connect int:[%d]\r\n", conn_interval_get);

/* set&get bt PIN code */
char pin[BT_PIN_STR_LEN] = "123456";

result = mo_bt_pin_set(module, pin);
OS_ASSERT(OS_SUCCESS == result);

result = mo_bt_pin_get(module, pin);
OS_ASSERT(OS_SUCCESS == result);
os_kprintf("BT pin:[%s]\r\n", pin);

/* set&get bt PIN mode */
bt_pin_mode_t pin_mode_get = BT_PIN_ENABLE;

result = mo_bt_pin_mode_set(module, BT_PIN_DISABLE);
OS_ASSERT(OS_SUCCESS == result);

result = mo_bt_pin_mode_get(module, &pin_mode_get);
OS_ASSERT(OS_SUCCESS == result);
os_kprintf("BT pin mode:[%d]\r\n", pin_mode_get);

/* reboot bt module */
result = mo_bt_reboot(module);
os_kprintf("BT reboot %s.\r\n", OS_SUCCESS == result ? "SUCCESS": "FAILED");

/* disconnect bt connection */
result = mo_bt_disconnect(module);
os_kprintf("BT disconnect %s.\r\n", OS_SUCCESS == result ? "SUCCESS": "FAILED");

/* set&get bt radio power level */
uint32_t level_get = 0;

result = mo_bt_radio_power_level_set(module, 14);
OS_ASSERT(OS_SUCCESS == result);

result = mo_bt_radio_power_level_get(module, &level_get);
OS_ASSERT(OS_SUCCESS == result);
os_kprintf("BT radio power level:[%u]\r\n", level_get);

/* set&get bt uart baudrate */
uint32_t baudreate_get = 0;

result = mo_bt_baudrate_set(module, BAUD_RATE_9600);
OS_ASSERT(OS_SUCCESS == result);

result = mo_bt_baudrate_get(module, &baudreate_get);
OS_ASSERT(OS_SUCCESS == result);
os_kprintf("BT baudrate:[%u]\r\n", baudreate_get);
```

### WIFI控制接口使用示例

```c
#define WIFI_TC_CONNECT_SSID "Test"
#define WIFI_TC_CONNECT_PASSWORD "12345678"

/* WIFI mode set & get */
os_err_t result = mo_wifi_set_mode(test_module, MO_WIFI_MODE_STA);
OS_ASSERT(OS_SUCCESS == result);

mo_wifi_mode_t wifi_mode = mo_wifi_get_mode(test_module);
OS_ASSERT(MO_WIFI_MODE_STA == wifi_mode);

/* WIFI connect and disconnect ap */
os_err_t result = mo_wifi_connect_ap(test_module, "not_exist", "");
OS_ASSERT(OS_SUCCESS != result);

result = mo_wifi_connect_ap(test_module, WIFI_TC_CONNECT_SSID, WIFI_TC_CONNECT_PASSWORD);
OS_ASSERT(OS_SUCCESS == result);

os_task_msleep(5000);

result = mo_wifi_disconnect_ap(test_module);
OS_ASSERT(OS_SUCCESS == result);

/* WIFI get status */
os_err_t result = mo_wifi_connect_ap(test_module, WIFI_TC_CONNECT_SSID, WIFI_TC_CONNECT_PASSWORD);
OS_ASSERT(OS_SUCCESS == result);

mo_wifi_stat_t wifi_stat = mo_wifi_get_stat(test_module);
OS_ASSERT(MO_WIFI_STAT_CONNECTED == wifi_stat);

os_task_msleep(5000);

result = mo_wifi_disconnect_ap(test_module);
OS_ASSERT(OS_SUCCESS == result);

wifi_stat = mo_wifi_get_stat(test_module);
OS_ASSERT(MO_WIFI_STAT_DISCONNECTED == wifi_stat);

/* WIFI scan all info & free info */
mo_wifi_scan_result_t scan_result = {0};

os_err_t result = mo_wifi_scan_info(test_module, OS_NULL, &scan_result);
OS_ASSERT(OS_SUCCESS == result);

char *security;

for (int i = 0; i < scan_result.info_num; i++)
{
    os_kprintf("%-32.32s", &scan_result.info_array[i].ssid.val[0]);
    os_kprintf("%2x:%2x:%2x:%2x:%2x:%2x  ",
                scan_result.info_array[i].bssid.bssid_array[0],
                scan_result.info_array[i].bssid.bssid_array[1],
                scan_result.info_array[i].bssid.bssid_array[2],
                scan_result.info_array[i].bssid.bssid_array[3],
                scan_result.info_array[i].bssid.bssid_array[4],
                scan_result.info_array[i].bssid.bssid_array[5]);
    switch (scan_result.info_array[i].ecn_mode)
    {
    case MO_WIFI_ECN_OPEN:
        security = "OPEN";
        break;
    case MO_WIFI_ECN_WPA_PSK:
        security = "WPA_PSK";
        break;
    case MO_WIFI_ECN_WPA2_PSK:
        security = "WPA2_PSK";
        break;
    case MO_WIFI_ECN_WPA_WPA2_PSK:
        security = "WPA_WPA2_PSK";
        break;
    default:
        security = "UNKNOWN";
        break;
    }
    os_kprintf("%-14.14s ", security);
    os_kprintf("%-4d ", scan_result.info_array[i].rssi);
    os_kprintf("%-4d ", scan_result.info_array[i].channel);
}

mo_wifi_scan_info_free(&scan_result);
```

### CTM2M云平台接口使用示例

主要针对生态项目，以MB26模组为例：

```c
#define TEST_SERVER_IP     "221.229.214.202"
#define TEST_SERVER_PORT   (5683)
#define TEST_LIFETIME      (86400)

static ctm2m_t *handle = OS_NULL;

static void notify_handler(ctm2m_notify_t notify)
{
    INFO("%s-%d notify type[%d].", __func__, __LINE__, notify.type);
    INFO("%s-%d status[%d].", __func__, __LINE__, notify.status);
    INFO("%s-%d msg ID[%d].", __func__, __LINE__, notify.msg_id);
}

static void request_handler(ctm2m_request_t request)
{
    ctm2m_resp_t    resp;
    os_err_t        result = OS_SUCCESS;

    INFO("%s-%d msg ID[%d].", __func__, __LINE__, request.msg_id);
    INFO("%s-%d request type[%d].", __func__, __LINE__, request.type);
    INFO("%s-%d token[%s], len[%d].", __func__, __LINE__, request.token.data, request.token.len);
    INFO("%s-%d uri[%s], len[%d].", __func__, __LINE__, request.uri.data, request.uri.len);
    INFO("%s-%d observe type[%d].", __func__, __LINE__, request.observe);
    INFO("%s-%d dataformate[%d].", __func__, __LINE__, request.dataformate);
    INFO("%s-%d data[%s], len[%d].", __func__, __LINE__, request.data.data, request.data.len);

    memset(&resp, 0, sizeof(ctm2m_resp_t));
    resp.msg_id = request.msg_id;
    resp.resp_code = 205;
    resp.token.data = request.token.data;
    resp.token.len  = request.token.len;
    resp.uri.data = request.uri.data;
    resp.uri.len  = request.uri.len;
    resp.observe  = request.observe;
    resp.dataformate = request.dataformate;
    resp.data.data = request.data.data;
    resp.data.len  = request.data.len;

    result = ctm2m_resp(handle, resp);
    WARN("%s-%d ctm2m_resp execute %s.", __func__, __LINE__, OS_SUCCESS == result? "SUCCESS" : "FAILED");
}

static void receive_handler(ctm2m_string_t data)
{
    INFO("%s-%d received data[%s], len[%d].", __func__, __LINE__, data.data, data.len);
}

void main_work_flow(void)
{
    INFO("Into main_work_flow");

    os_err_t     result = OS_SUCCESS;
    mo_object_t *module = OS_NULL;
    
    module = mo_get_default();
    if (OS_NULL == module)
    {
        ERROR("%s-%d no default module.", __func__, __LINE__);
        return;
    }
    
    /**************************************************************************
    * make sure reg to network
    **************************************************************************/
    while (1)
    {
        eps_reg_info_t info = {0};
        mo_get_reg(module, &info);
        if (1 == info.reg_stat) break;
        os_task_msleep(1000);
    }
    INFO("%s-%d module regist to net OK.", __func__, __LINE__);

    /**************************************************************************
    * creat ctm2m handle
    **************************************************************************/
    ctm2m_create_parm_t parm;
    memset(&parm, 0, sizeof(ctm2m_create_parm_t));
    
    parm.server_ip.data = TEST_SERVER_IP;
    parm.server_ip.len  = strlen(TEST_SERVER_IP);
    parm.port           = TEST_SERVER_PORT;
    parm.lifetime       = TEST_LIFETIME;
    parm.notify_cb      = notify_handler;
    parm.request_cb     = request_handler;
    parm.receive_cb     = receive_handler;
    parm.obj_ins_list.data = "</3303/0>,</3303/1>";
    parm.obj_ins_list.len  = strlen(parm.obj_ins_list.data);

    handle = ctm2m_create(module, parm);
    INFO("%s-%d ctm2m handle create %s.", __func__, __LINE__, OS_NULL != handle ? "SUCCESS" : "FAILED");
    if (OS_NULL == handle) return;

    /**************************************************************************
    * [Optional]: test set&get ue cfg
    **************************************************************************/
    
    ctm2m_ue_cfg_t cfg = {CTM2M_UEMODE_HEARTBEAT_UPDATE, CTM2M_AUTO_HEARTBEAT_ON};
    ctm2m_ue_info_t cfg_info;
    memset(&cfg_info, 0, sizeof(ctm2m_ue_info_t));

    result = ctm2m_set_ue_cfg(handle, cfg);
    
    INFO("%s-%d ctm2m_set_ue_cfg %s.", __func__, __LINE__, OS_SUCCESS == result ? "SUCCESS" : "FAILED");
    
    if (OS_SUCCESS != result) return;
    
    result = ctm2m_get_ue_cfg(handle, &cfg_info);
    
    INFO("%s-%d ctm2m_get_ue_cfg %s.", __func__, __LINE__, OS_SUCCESS == result ? "SUCCESS" : "FAILED");
    
    if (OS_SUCCESS != result) return;

    INFO("%s-%d cfg_info auth_mode[%d].", __func__, __LINE__, cfg_info.auth_mode);
    INFO("%s-%d cfg_info tau_timer_mode[%d].", __func__, __LINE__, cfg_info.tau_timer_mode);
    INFO("%s-%d cfg_info uq_mode[%d].", __func__, __LINE__, cfg_info.uq_mode);
    INFO("%s-%d cfg_info ce_mode[%d].", __func__, __LINE__, cfg_info.ce_mode);
    INFO("%s-%d cfg_info heartbeat_mode[%d].", __func__, __LINE__, cfg_info.heartbeat_mode);
    INFO("%s-%d cfg_info wakeup_mode[%d].", __func__, __LINE__, cfg_info.wakeup_mode);
    INFO("%s-%d cfg_info protocol_mode[%d].", __func__, __LINE__, cfg_info.protocol_mode);
    
    /**************************************************************************
    * register: register to platform
    **************************************************************************/
    result = ctm2m_register(handle);

    INFO("%s-%d Platform register %s.", __func__, __LINE__, OS_SUCCESS == result ? "SUCCESS" : "FAILED");

    if (OS_SUCCESS != result) return;
    /**************************************************************************
    * work flow
    **************************************************************************/
    int test_time = 10;
    while (test_time--)
    {
        os_task_msleep(10000);

        int32_t  send_msg_id = 0; /* returning msg id */
        ctm2m_send_t send;
        memset(&send, 0, sizeof(ctm2m_send_t));
        send.mode = CTM2M_SEND_MODE_CON;
        send.msg.data = "3131";
        send.msg.len = strlen(send.msg.data);

        result = ctm2m_send(handle, send, &send_msg_id);
        INFO("%s-%d ctm2m_send %s.", __func__, __LINE__, OS_SUCCESS == result ? "SUCCESS": "FAILED");
        
        os_task_msleep(1000);

        ctm2m_update_t update;
        memset(&update, 0, sizeof(ctm2m_update_t));
        update.mode = CTM2M_BINDING_NULL;
        
        result = ctm2m_update(handle, update);
        INFO("%s-%d ctm2m_update %s.", __func__, __LINE__, OS_SUCCESS == result ? "SUCCESS": "FAILED");
    }

    /**************************************************************************
    * deregister & destroy process
    **************************************************************************/
    result = ctm2m_deregister(handle);
    if (OS_SUCCESS != result)
    {
        ERROR("%s-%d ctm2m_deregister failed.", __func__, __LINE__);
        /* NOTE: need to retry */
        return;
    }

    result = ctm2m_destroy(handle);
    if (OS_SUCCESS != result)
    {
        ERROR("%s-%d ctm2m_destroy failed.", __func__, __LINE__);
        /* NOTE: need to retry or restart module */
        return;
    }

    INFO("Exit main_work_flow");
}
```

### 模组重置异常检测处理示例

FAQ Q2模组异常重置问题检测处理示例：

```c
#include <oneos_config.h>
#include <os_stddef.h>
#include <os_util.h>
#include <os_assert.h>
#include <shell.h>
#include <sys/socket.h>
#include <mo_api.h>

#define SERVER_ADDR "XXX.XXX.XXX.XXX"
#define SERVER_PORT_TCP (XXXX)
#define DATA_LENGTH     (128)

/* ----------------------------[Demo diagram]-----------------------------
      ┌───────────────────┐
      │                   │
 ┌────►    fd=socket()    ├────────────┬───────────────┐
 │    │                   │            │               │
 │    └─────────┬─────────┘            │      ┌────────▼─────────┐
 │              │OK                    │      │                  │
 │              │                      │      │ check reset FLAG ├─────┐
 │    ┌─────────▼─────────┐            │      │                  │     │
 │    │                   │            │      └────────┬─────────┘     │
 │    │   ret=connect()   ├──────┐     │               │TRUE           │
 │    │                   │      │     │               │               │
 │    └─────────┬─────────┘      │     │      ┌────────▼─────────┐     │
 │              │OK              │     │      │   module reset   │     │
 │              │                │     │      │                  │     │
 │    ┌─────────▼─────────┐      │     │      │    (optional)    │     │
OK    │                   │      │     │      └────────┬─────────┘   FALSE
 │    │    ret=send()     ├──────┤     │               │               │
 │    │                   │      │    ERR              │               │
 │    └─────────┬─────────┘      │     │      ┌────────▼─────────┐     │
 │              │OK              │     │      │   destroy all    │     │
 │              │               ERR    │      │    sockets &     │     │
 │    ┌─────────▼─────────┐      │     │      │  restart MoLink  │     │
 │    │                   │      │     │      └────────┬─────────┘     │
 │    │    ret=recv()     ├──────┤     │               │               │
 │    │                   │      │     │               │◄──────────────┘
 │    └─────────┬─────────┘      │     │               ▼
 │              │OK              │     │      go back to beginning
 │              │                │     │
 │    ┌─────────▼─────────┐      │     │
 │    │                   │      │     │
 └────┤    ret=close()    ◄──────┘     │
      │                   │            │
      └─────────┬─────────┘            │
                │                      │
                └──────────────────────┘
----------------------------[Demo diagram]----------------------------- */


extern os_bool_t reset_flag;

extern int esp8266_auto_create(void);

/**
 *  Rewrite callback to throw module reset notification. 
 *  [ref]:molink/module/<modulename>/source/<modulename>.c --> gs_urc_table[] 
 * 
 *  os_bool_t reset_flag = OS_FALSE;
 * 
 *  void urc_ready_func(struct at_parser *parser, const char *data, os_size_t size)
 *  {
 *      os_kprintf("Module maybe reset. Please check.\r\n");
 *      reset_flag = OS_TRUE;
 *  }
 *  
 * */

int work_session(int fd)
{
    int    ret;
    struct sockaddr_in serv_addr;
    char   send_buff[DATA_LENGTH + 1] = {0};
    char   recv_buff[DATA_LENGTH + 1] = {0};
    const struct timeval timeout = {.tv_sec = 30, .tv_usec = 0};

    memset(send_buff, 'F', DATA_LENGTH);
    memset(&serv_addr, 0, sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(SERVER_PORT_TCP);
    serv_addr.sin_addr.s_addr = inet_addr(SERVER_ADDR);

    setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof(timeout));

    ret = connect(fd, (struct sockaddr *)&serv_addr, sizeof(serv_addr));
    if (0 > ret)
    {
        os_kprintf("[Demo] connect failed.\r\n");
        return ret;
    }

    while (1)
    {
        ret = send(fd, send_buff, DATA_LENGTH, 0);
        if (0 > ret)
        {
            os_kprintf("[Demo] send failed.\r\n");
            return ret;
        }

        ret = recv(fd, recv_buff, DATA_LENGTH, 0);
        if (0 > ret)
        {
            os_kprintf("[Demo] recv failed.\r\n");
            return ret;
        }
    }
}

os_bool_t anomaly_detection(void)
{
    if (!reset_flag) return OS_FALSE;

    os_kprintf("[Demo] module maybe reset.\r\n");

    /* User should do diagnostic here. */

    return OS_TRUE;
}

void anomaly_session(void)
{
    /* 1st step: close all socket.(Just 1 socket in demo and it has already been closed.) */

    /* 2nd step: user reset module.(optional) */

    /* 3rd step: reinitialize MoLink.(Reuse esp8266_auto_create func here for example.) */
    mo_object_t *module = mo_get_default();
    OS_ASSERT_EX((OS_NULL != module), "[Demo] Oops, unknown error occurred.\r\n");
    OS_ASSERT_EX((OS_SUCCESS == mo_destroy(module, MODULE_TYPE_ESP8266)), "[Demo] Oops, MoLink deinit failed.\r\n");
    OS_ASSERT_EX(OS_SUCCESS == esp8266_auto_create(), "[Demo] MoLink restart failed.\r\n");
}

os_err_t demo_main(int argc, char *argv[])
{
    int fd;

    /* Startup correlation checks are omitted here. */
    reset_flag = OS_FALSE;

    while (1)
    {
        fd = socket(AF_INET, SOCK_STREAM, 0);
        if (0 > fd)
        {
            os_kprintf("[Demo] socket create failed.\r\n");
            /* anomaly_detection should check the return value for the actual use case. */
            if (anomaly_detection())
            {
                anomaly_session();
            }
            continue;
        }

        work_session(fd);

        if (0 > closesocket(fd))
        {
            if (anomaly_detection())
            {
                anomaly_session();
                reset_flag = OS_FALSE;
            }
            else
            {
                OS_ASSERT_EX(0, "[Demo] close socket failed, unknown error.\r\n");
            }
        }
    }
}
SH_CMD_EXPORT(demo, demo_main, "demo_anomaly_detection");


```

## FAQ

Q1: 为何发送/接收较长数据时返回超时或错误，且很难返回成功？

A1: 对于NB类型模组，因网络因素受限，收发较大长度的数据常会有严重丢包重传等现象发生，不建议大量数据收发。

Q2: 模组不稳定，在使用过程中出现复位重置情况，该如何发现和进行处理？

A2：建议更新固件，或参考应用示例中``方式检测异常并进行处理。

Q3：在大数据量长跑过程中遇到ESP8266概率性重启的问题，如何解决？

A3：ESP8266模组固件老旧可能会导致在长时间收发数据的过程中，模组重启。建议升级固件。以下是乐鑫官网推荐的最新固件：[https://download.espressif.com/esp_at/firmware/ESP8266/ESP8266-IDF-AT_V2.2.1.0.zip](https://download.espressif.com/esp_at/firmware/ESP8266/ESP8266-IDF-AT_V2.2.1.0.zip)
