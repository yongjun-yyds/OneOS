#include "hgn.h"
#include <oneos_config.h>
#include <os_task.h>

#include <lwip/ip_addr.h>
#include <lwip/dhcp.h>
#include <lwip/dhcp6.h>
#include <oneos_lwip.h>
#ifdef LWIP_USING_DHCPD
#include <dhcp_server.h>
#endif

#include "lwip/ip4_nat.h"

#define HGN_NAT_IF_MASK   "255.255.255.0"
#define HGN_DHCP_START_ID 100
#define HGN_DHCP_END_ID   200

#include "ble_netif_lwip.h"
#define BLE_NAT_DBG "ble_nat"
#include <dlog.h>

uint8_t nat_default_mac[6] = {0xc0, 0xa8, 0xb9, 0x12, 0x34, 0x66};

static void hgn_ble_nat_task(void *parameter)
{
    uint8_t      *pub_addr;
    struct netif *ble_netif;
    struct netif *eth_netif;

    pub_addr = nat_default_mac;
    bleif_netif_ap_init(pub_addr);
    os_task_msleep(2000);

    hgn_uart_init();
    os_task_msleep(2000);

    while (1)
    {
        ble_netif = os_lwip_get_netif_by_devname(BLE_LWIP_IF_DEVICE_NAME);
        eth_netif = os_lwip_get_netif_by_devname(HGN_NAT_IF_NAME);
        if (OS_NULL != ble_netif && OS_NULL != eth_netif)
        {

            LOG_I(BLE_NAT_DBG, "bleif: %s, ethif: %s", BLE_LWIP_IF_DEVICE_NAME, HGN_NAT_IF_NAME);
            os_lwip_register_ipv4bind_cb(HGN_NAT_IF_NAME, oneos_netif_ipv4addr_bind_fn);
            netif_set_default(eth_netif);
            dhcp_start(eth_netif);
#if LWIP_IPV4
            ip4_addr_t addr;
            ip4_addr_t mask;

            if (!ip4addr_aton(HGN_NAT_IF_ADDR, &addr))
            {
                LOG_E(BLE_NAT_DBG, "wrong HGN_NAT_IF_ADDR %s", HGN_NAT_IF_ADDR);
            }
            {
            }
            ip4addr_aton(HGN_NAT_IF_MASK, &mask);

            /* set ip address */
            netif_set_ipaddr(ble_netif, &addr);

            /* set gateway address */
            netif_set_gw(ble_netif, &addr);

            /* set netmask address */
            netif_set_netmask(ble_netif, &mask);

#ifdef LWIP_USING_NAT
            ip4_nat_entry_t new_nat_entry;

            new_nat_entry.out_if = eth_netif;
            new_nat_entry.in_if  = ble_netif;

            ip4_addr_set(ip_2_ip4(&new_nat_entry.source_net), &addr);
            ip4_addr_set(ip_2_ip4(&new_nat_entry.source_netmask), &mask);

            IP4_ADDR(ip_2_ip4(&new_nat_entry.dest_net), 0, 0, 0, 0);
            IP4_ADDR(ip_2_ip4(&new_nat_entry.dest_netmask), 0, 0, 0, 0);
            ip4_nat_add(&new_nat_entry);
            LOG_I(BLE_NAT_DBG, "nat: %s/%s - 0.0.0.0/0.0.0.0", HGN_NAT_IF_ADDR, HGN_NAT_IF_MASK);
#endif

#ifdef LWIP_USING_DHCPD
            uint8_t dhcpd_start_id;
            uint8_t dhcpd_end_id;

            /* according to your app require to set */
            dhcpd_start_id = HGN_DHCP_START_ID;
            dhcpd_end_id   = HGN_DHCP_END_ID;
            dhcpd_start(ble_netif, HGN_NAT_IF_ADDR, dhcpd_start_id, dhcpd_end_id);
            LOG_I(BLE_NAT_DBG,
                  "dhcpd: %s, %d-%d, %s, %s",
                  HGN_NAT_IF_ADDR,
                  dhcpd_start_id,
                  dhcpd_end_id,
                  HGN_NAT_IF_MASK,
                  DHCP_DNS_SERVER_IP);
#endif
#endif
            break;
        }
        os_task_msleep(100);
    }
}

int hgn_ble_nat_start(void)
{
    os_task_id tid;

    tid = os_task_create(NULL, NULL, 2048, "hgn_ble_nat", hgn_ble_nat_task, NULL, 20);
    os_task_startup(tid);

    return 0;
}
OS_INIT_CALL(hgn_ble_nat_start, OS_INIT_LEVEL_APPLICATION, OS_INIT_SUBLEVEL_LOW);
