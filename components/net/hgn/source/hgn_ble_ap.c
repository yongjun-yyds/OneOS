#include "hgn.h"
#include <oneos_config.h>
#include <os_task.h>
#include <os_errno.h>

#include "nimble/nimble_npl.h"
#include "nimble/nimble_port.h"
#include "host/ble_hs.h"
#include "host/util/util.h"
#include "controller/ble_ll.h"
#include "services/ipss/ble_l2cap_ipsp.h"
#include "ble_netif_lwip.h"
#include "store/ram/ble_store_ram.h"

#define BLE_AP_DBG "ble_ap"
#include <dlog.h>

static uint8_t    g_own_addr_type;
static int        scan_flag = 0;
static hgn_cfg_t *hgn_cfg   = NULL;

static void ble_app_set_addr(void)
{
    ble_addr_t addr;
    int        rc;

    /* generate new non-resolvable private address */
    rc = ble_hs_id_gen_rnd(0, &addr);

    /* set generated address */
    rc = ble_hs_id_set_rnd(addr.val);
}

/* scan_event() calls scan(), so forward declaration is required */
static void scan(void);

/* connection has separate event handler from scan */
static int conn_event(struct ble_gap_event *event, void *arg)
{
    struct ble_gap_conn_desc conn_desc;
    int                      rc;

    switch (event->type)
    {
    case BLE_GAP_EVENT_CONNECT:
        if (event->connect.status == 0)
        {
            LOG_I(BLE_AP_DBG, "Connection was established");
            ble_gap_conn_find(event->connect.conn_handle, &conn_desc);

            if (hgn_cfg->sec)
            {
                rc = ble_gap_security_initiate(event->connect.conn_handle);
                if (rc)
                {
                    LOG_I(BLE_AP_DBG, "ble_gap_security_initiate err %d.", rc);
                }
            }
            else
            {
                extern int bleif_connect(uint16_t conn_handle);
                rc = bleif_connect(event->connect.conn_handle);
                LOG_D(BLE_AP_DBG, "l2cap connect rc=%d", rc);
            }
        }
        else
        {
            LOG_E(BLE_AP_DBG, "Connection failed, error code: %i", event->connect.status);
        }
        break;
    case BLE_GAP_EVENT_DISCONNECT:
        LOG_E(BLE_AP_DBG,
              "Disconnected conn %d, reason code: %i ",
              event->disconnect.conn.conn_handle,
              event->disconnect.reason);
        break;
    case BLE_GAP_EVENT_CONN_UPDATE_REQ:
        LOG_D(BLE_AP_DBG, "Connection update request received");
        break;
    case BLE_GAP_EVENT_CONN_UPDATE:
        if (event->conn_update.status == 0)
        {
            LOG_D(BLE_AP_DBG, "Connection update successful");
        }
        else
        {
            LOG_E(BLE_AP_DBG, "Connection update failed; reson: %d", event->conn_update.status);
        }
        break;
    case BLE_GAP_EVENT_L2CAP_UPDATE_REQ:
        LOG_D(BLE_AP_DBG, "l2cap[handle=%d] update successful", event->conn_update_req.conn_handle);
        break;

    case BLE_GAP_EVENT_PASSKEY_ACTION:
        LOG_I(BLE_AP_DBG, "PASSKEY_ACTION_EVENT started, passkey=%d", hgn_cfg->key);
        struct ble_sm_io pkey = {0};
        int              key  = 0;

        if (event->passkey.params.action == BLE_SM_IOACT_DISP)
        {
            pkey.action  = event->passkey.params.action;
            pkey.passkey = hgn_cfg->key; /*This is the passkey to be entered on peer*/
            rc           = ble_sm_inject_io(event->passkey.conn_handle, &pkey);
            LOG_D(BLE_AP_DBG, "ble_sm_inject_io result: %d", rc);
        }
        return 0;

    case BLE_GAP_EVENT_ENC_CHANGE:
        LOG_D(BLE_AP_DBG, "ble_sm_gap_status %d", event->enc_change.status);

        rc = ble_gap_conn_find(event->enc_change.conn_handle, &conn_desc);
        LOG_I(BLE_AP_DBG,
              "ble_sm_sec_state authenticated %d bonded %d encrypted %d key_size %d",
              conn_desc.sec_state.authenticated,
              conn_desc.sec_state.bonded,
              conn_desc.sec_state.encrypted,
              conn_desc.sec_state.key_size);

        if ((BLE_HS_SM_PEER_ERR(BLE_SM_ERR_CONFIRM_MISMATCH) == event->enc_change.status) ||
            (BLE_HS_ETIMEOUT == event->enc_change.status))
        {
            ble_gap_terminate(event->enc_change.conn_handle, 0x13);
        }
        else if (BLE_HS_HCI_ERR(BLE_ERR_PINKEY_MISSING) == event->enc_change.status)
        {
            /* Delete the old bond. */
            ble_store_util_delete_peer(&conn_desc.peer_id_addr);

            ble_gap_terminate(event->enc_change.conn_handle, 0x13);
        }
        else if (0 == event->enc_change.status)
        {
            extern int bleif_connect(uint16_t conn_handle);
            rc = bleif_connect(event->connect.conn_handle);
            LOG_D(BLE_AP_DBG, "l2cap connect rc=%d", rc);
        }

        break;

    case BLE_GAP_EVENT_REPEAT_PAIRING:
        /* We already have a bond with the peer, but it is attempting to
         * establish a new secure link.  This app sacrifices security for
         * convenience: just throw away the old bond and accept the new link.
         */

        /* Delete the old bond. */
        rc = ble_gap_conn_find(event->repeat_pairing.conn_handle, &conn_desc);
        ble_store_util_delete_peer(&conn_desc.peer_id_addr);

        /* Return BLE_GAP_REPEAT_PAIRING_RETRY to indicate that the host should
         * continue with the pairing operation.
         */
        return BLE_GAP_REPEAT_PAIRING_RETRY;

    default:
        LOG_D(BLE_AP_DBG, "Connection event type not supported, %d", event->type);
        break;
    }
    scan_flag = 1;
    return 0;
}

static int scan_event(struct ble_gap_event *event, void *arg)
{
    /* predef_uuid stores information about UUID of device,
       that we connect to */
    const ble_uuid128_t      predef_uuid = BLE_SPECIFIC_UUID;
    struct ble_hs_adv_fields parsed_fields;
    int                      uuid_cmp_result;

    memset(&parsed_fields, 0, sizeof(parsed_fields));

    switch (event->type)
    {
    /* advertising report has been received during discovery procedure */
    case BLE_GAP_EVENT_DISC:
        LOG_D(BLE_AP_DBG, "Advertising report received! Checking UUID...");
        ble_hs_adv_parse_fields(&parsed_fields, event->disc.data, event->disc.length_data);
        /* Predefined UUID is compared to recieved one;
           if doesn't fit - end procedure and go back to scanning,
           else - connect. */
        uuid_cmp_result = ble_uuid_cmp(&predef_uuid.u, &parsed_fields.uuids128->u);
        if (!uuid_cmp_result)
        {
            LOG_I(BLE_AP_DBG, "UUID fits, connecting...");
            ble_gap_disc_cancel();
            ble_gap_connect(g_own_addr_type, &(event->disc.addr), 10000, NULL, conn_event, NULL);
        }
        break;
    case BLE_GAP_EVENT_DISC_COMPLETE:
        LOG_D(BLE_AP_DBG, "Discovery completed, reason: %d", event->disc_complete.reason);
        break;
    default:
        LOG_D(BLE_AP_DBG, "Discovery event not handled");
        break;
    }
    scan_flag = 1;
    return 0;
}

static void scan(void)
{
    scan_flag = 0;
    int rc;

    /* set scan parameters:
        - scan interval in 0.625ms units
        - scan window in 0.625ms units
        - filter policy - 0 if whitelisting not used
        - limited - should limited discovery be used
        - passive - should passive scan be used
        - filter duplicates - 1 enables filtering duplicated advertisements */
    int                              ble_ap_scan_interval_num = (int)((float)hgn_cfg->intvl / 0.625);
    const struct ble_gap_disc_params scan_params              = {ble_ap_scan_interval_num, 200, 0, 0, 0, 1};

    /* performs discovery procedure */
    rc = ble_gap_disc(g_own_addr_type, 10000, &scan_params, scan_event, NULL);
}

static void on_sync(void)
{
    int     rc;
    uint8_t pub_addr[6];
    int     out_nrpa;

    memset(pub_addr, 0, sizeof(pub_addr));

    /* g_own_addr_type will store type of addres our BSP uses */

    rc = ble_hs_util_ensure_addr(0);
    rc = ble_hs_id_infer_auto(0, &g_own_addr_type);

    ble_hs_id_copy_addr(g_own_addr_type, pub_addr, &out_nrpa);
    LOG_I(BLE_AP_DBG,
          "pub_addr=%02x%02x%02x%02x%02x%02x",
          pub_addr[0],
          pub_addr[1],
          pub_addr[2],
          pub_addr[3],
          pub_addr[4],
          pub_addr[5]);

    LOG_I(BLE_AP_DBG, "hgn_ble_ap begin scaning");

    /* begin scanning */
    scan();
}

static void on_reset(int reason)
{
    LOG_I(BLE_AP_DBG, "Resetting state; reason=%d", reason);
}

static void hgn_ble_ap_task(void *parameter)
{
    os_err_t ret = OS_SUCCESS;

    hgn_cfg = (hgn_cfg_t *)calloc(1, sizeof(hgn_cfg_t));
    if (!hgn_cfg)
    {
        LOG_E(BLE_AP_DBG, "no memory for hgn_cfg");
        return;
    }

    ret = hgn_get_cfg(hgn_cfg);
    if (ret != OS_SUCCESS)
    {
        LOG_E(BLE_AP_DBG, "get hgn_cfg err");
        return;
    }

    nimble_port_oneos_init();
    ble_ll_set_public_addr(hgn_cfg->mac);
    ble_l2cap_ipsp_init();
    os_task_msleep(2000);

    hgn_uart_init();

    ble_hs_cfg.sync_cb  = on_sync;
    ble_hs_cfg.reset_cb = on_reset;

    ble_store_ram_init();

    LOG_E(BLE_AP_DBG, "ap sec=%d, scan intvl=%d", hgn_cfg->sec, hgn_cfg->intvl);
    /* As the last thing, process events from default event queue. */
    ble_hs_task_startup();

    while (1)
    {
        if (scan_flag == 1)
        {
            scan();
        }
        os_task_msleep(100);
    }
}

int hgn_ble_ap_start(void)
{
    os_task_id tid;

    tid = os_task_create(NULL, NULL, 2048, "hgn_ble_ap", hgn_ble_ap_task, NULL, 20);
    os_task_startup(tid);

    return 0;
}
OS_INIT_CALL(hgn_ble_ap_start, OS_INIT_LEVEL_APPLICATION, OS_INIT_SUBLEVEL_LOW);
