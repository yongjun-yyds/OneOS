/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

#include <stdint.h>
#include <oneos_config.h>
#include <os_errno.h>

#ifdef NET_USING_LWIP
#include <lwip/pbuf.h>
#include <lwip/netif.h>
#include <lwip/ip_addr.h>
#include <oneos_lwip.h>

#ifdef LWIP_USING_INTF_6LOWPAN_BLE
#include <netif/lowpan6_ble.h>
#endif /* end of LWIP_USING_INTF_6LOWPAN_BLE */

#if LWIP_IPV6
#include <lwip/ethip6.h>
#include <lwip/sockets.h>
#endif

#include <lwip/dhcp.h>
#if LWIP_IPV6_DHCP6
#include <lwip/dhcp6.h>
#endif
#endif /* end of NET_USING_LWIP */

#ifdef BLE_USING_NIMBLE
#include "os/os_mbuf.h"
#include "services/ipss/ble_l2cap_ipsp.h"
#include "host/ble_gap.h"
#endif /* end of BLE_USING_NIMBLE */
#include "ble_netif_lwip.h"

#include <dlog.h>
#include <stdlib.h>

#define BLE_NETIF_LWIP_DBG "ble_netif"

static ble_netif_dev_t *gs_ble_netif_dev;

#ifdef BLE_USING_NIMBLE
static void bleif_linkup_proc(uint16_t conn_handle, l2cap_intf_type_t type)
{
    struct ble_gap_conn_desc conn_desc;

    if (OS_NULL == gs_ble_netif_dev)
    {
        LOG_E(BLE_NETIF_LWIP_DBG, "ble netif is not create");
        return;
    }

    ble_gap_conn_find(conn_handle, &conn_desc);

    /* used for ip_addr compress, if not set, the ip_address will not compress
       rfc7668_set_local_addr_mac48((struct netif *)gs_ble_netif_dev->net_dev.userdata, conn_desc.our_id_addr.val, 6,
       1); rfc7668_set_peer_addr_mac48((struct netif *)gs_ble_netif_dev->net_dev.userdata, conn_desc.peer_id_addr.val,
       6, 1);*/

    if (type == L2CAP_INTF_SERVER)
    {
        /* as l2cap client[also ble ap, link is always up] */
        os_net_linkchange(&gs_ble_netif_dev->net_dev, OS_TRUE);
        bleif_netif_sta_start();
    }

    LOG_I(BLE_NETIF_LWIP_DBG, "recv[%04x,%d] up", conn_handle, type);

    return;
}

static void bleif_recv_data_proc(uint16_t conn_handle, const struct os_mbuf *data)
{
    struct os_mbuf_pkthdr *hdr;

    if (OS_NULL == gs_ble_netif_dev)
    {
        LOG_E(BLE_NETIF_LWIP_DBG, "please create ble netif first");
        return;
    }

    hdr = OS_MBUF_PKTHDR(data);
    if (hdr->omp_len == 0 || hdr->omp_len > 2000)
    {
        LOG_E(BLE_NETIF_LWIP_DBG, "bleif[%d] recv data len=%d len error", conn_handle, hdr->omp_len);
        return;
    }
    LOG_D(BLE_NETIF_LWIP_DBG, "bleif[%d] recv data len=%d len", conn_handle, hdr->omp_len);

    os_mbuf_copydata(data, 0, hdr->omp_len, gs_ble_netif_dev->net_dev.rx_buff);
    os_net_rx_data(&gs_ble_netif_dev->net_dev, gs_ble_netif_dev->net_dev.rx_buff, hdr->omp_len);

    return;
}

static os_err_t bleif_netdev_send(struct os_net_device *net_dev, uint8_t *data, int len)
{
    struct os_mbuf *sdu_xmit;
    int             if_idx = 1;
    os_err_t        res    = -1;
    int             rc;

    LOG_D(BLE_NETIF_LWIP_DBG, "bleif_netdev_send len=%d", len);
    do
    {
        sdu_xmit = os_msys_get_pkthdr(len, 0);
        if (OS_NULL == sdu_xmit)
        {
            LOG_E(BLE_NETIF_LWIP_DBG, "why alloc mbuf failed");
            break;
        }
        /*send all data*/
        rc = os_mbuf_append(sdu_xmit, data, len);

        rc = ble_l2cap_intf_send(if_idx, sdu_xmit);
        if (rc)
        {
            if (rc != BLE_HS_ESTALLED)
                os_mbuf_free_chain(sdu_xmit);
            break;
        }
        res = 0;
    } while (0);

    return res;
}
#endif

static os_err_t bleif_netdev_init(struct os_net_device *net_dev)
{
    LOG_I(BLE_NETIF_LWIP_DBG, "bleif_netdev_init enter in");

    return 0;
}

static os_err_t bleif_netdev_deinit(struct os_net_device *net_dev)
{
    LOG_I(BLE_NETIF_LWIP_DBG, "bleif_netdev_deinit enter in");

    return 0;
}

static os_err_t bleif_uart_send(struct os_net_device *net_dev, uint8_t *data, int len)
{
    if ((net_dev) && (data))
    {
        LOG_D(BLE_NETIF_LWIP_DBG, "bleif_uart_send %d", len);
        net_uart_send(data, len);
    }
    return 0;
}

void bleif_uart_recv(uint8_t *data, int count)
{
    if (data && (count > 0))
    {
        os_net_rx_data(&gs_ble_netif_dev->net_dev, data, count);
    }
}

static os_err_t bleif_netdev_get_macaddr(struct os_net_device *net_dev, uint8_t *addr)
{
    memcpy(addr, gs_ble_netif_dev->dev_addr, 6);

    return 0;
}

static os_err_t bleif_netdev_set_filter(struct os_net_device *net_dev, uint8_t *addr, os_bool_t enable)
{
    return 0;
}

const static struct os_net_device_ops gs_ble_netdev_ops = {
    .init   = bleif_netdev_init,
    .deinit = bleif_netdev_deinit,
#ifdef BLE_USING_NIMBLE
    .send = bleif_netdev_send,
#else
    .send = bleif_uart_send,
#endif
    .get_mac    = bleif_netdev_get_macaddr,
    .set_filter = bleif_netdev_set_filter,
};

/* if netif_mode = OS_TRUE, the if as ble_ap role */
static int bleif_dev_init(uint8_t *oui_id_addr, os_bool_t netif_mode)
{
    os_err_t rc;
    int      index;

    gs_ble_netif_dev = malloc(sizeof(ble_netif_dev_t));
    if (OS_NULL == gs_ble_netif_dev)
    {
        LOG_E(BLE_NETIF_LWIP_DBG, "why alloc buf failed");
        return -1;
    }

    memset(gs_ble_netif_dev, 0, sizeof(ble_netif_dev_t));
    index                               = 0;
    gs_ble_netif_dev->dev_addr[index++] = *oui_id_addr++;
    gs_ble_netif_dev->dev_addr[index++] = *oui_id_addr++;
    gs_ble_netif_dev->dev_addr[index++] = *oui_id_addr++;
    gs_ble_netif_dev->dev_addr[index++] = *oui_id_addr++;
    gs_ble_netif_dev->dev_addr[index++] = *oui_id_addr++;
    gs_ble_netif_dev->dev_addr[index++] = *oui_id_addr++;

    gs_ble_netif_dev->net_dev.info.MTU = ETHERNET_DEFAULT_MTU;
    gs_ble_netif_dev->net_dev.info.MRU = ETHERNET_DEFAULT_MTU;

    gs_ble_netif_dev->net_dev.info.mode = (os_net_mode_t)netif_mode;

    if (OS_TRUE == netif_mode)
    {
        gs_ble_netif_dev->net_dev.info.linkstatus = OS_TRUE;
    }
    else
    {
        gs_ble_netif_dev->net_dev.info.linkstatus = OS_FALSE;
    }

    gs_ble_netif_dev->net_dev.info.intf_type = net_dev_intf_ether;
    gs_ble_netif_dev->net_dev.ops            = &gs_ble_netdev_ops;

    do
    {
        rc = os_net_device_register(&gs_ble_netif_dev->net_dev, BLE_LWIP_IF_DEVICE_NAME);
        if (OS_SUCCESS != rc)
        {
            LOG_E(BLE_NETIF_LWIP_DBG, "os_net_device_register failed");

            break;
        }
    } while (0);

    if (OS_SUCCESS != rc)
    {
        free(gs_ble_netif_dev);
        gs_ble_netif_dev = OS_NULL;
    }

    return rc;
}

/*ap init*/
int bleif_netif_ap_init(uint8_t *oui_id_addr)
{
    return bleif_dev_init(oui_id_addr, OS_TRUE);
}

/*ap start connect*/
void bleif_netif_ap_start(void)
{
    LOG_I(BLE_NETIF_LWIP_DBG, "bleif_netif_ap_start");
}

#ifdef BLE_USING_NIMBLE
/*ap init*/
int bleif_netif_sta_init(uint8_t *oui_id_addr)
{
    os_err_t rc = OS_SUCCESS;

    rc = bleif_dev_init(oui_id_addr, OS_FALSE);
    if (OS_SUCCESS != rc)
    {
        return rc;
    }

    rc = ble_ll_ipsp_l2cap_create_server(bleif_recv_data_proc, bleif_linkup_proc);
    if (OS_SUCCESS != rc)
    {
        LOG_E(BLE_NETIF_LWIP_DBG, "l2cap server create failed");
    }
    return rc;
}

/*sta start connect*/
void bleif_netif_sta_start(void)
{
    struct netif *ble_netif;
    LOG_I(BLE_NETIF_LWIP_DBG, "bleif_netif_sta_start");
    ble_netif = os_lwip_get_netif_by_devname(BLE_LWIP_IF_DEVICE_NAME);
    if (OS_NULL != ble_netif)
    {
        netif_set_default(ble_netif);
        LOG_I(BLE_NETIF_LWIP_DBG, "bleif_netif dhcp start");
        dhcp_start(ble_netif);
#if LWIP_IPV6_DHCP6
        dhcp6_enable_stateless(ble_netif);
#endif
    }
}

#ifdef NET_USING_HGN_BLE_AP
static void l2cap_recv_data_proc(uint16_t conn_handle, const struct os_mbuf *data)
{
    struct os_mbuf_pkthdr *hdr;
    uint8_t               *net_data;
    uint8_t                if_idx = 0;
    uint8_t                src_addr[BLE_MAX_L2_ADDR_LEN];
    uint8_t                dst_addr[BLE_MAX_L2_ADDR_LEN];

    hdr = OS_MBUF_PKTHDR(data);
    if (hdr->omp_len == 0 || hdr->omp_len > 2000)
    {
        LOG_E(BLE_NETIF_LWIP_DBG, "bleif[%d] recv data len=%d len error", conn_handle, hdr->omp_len);
        return;
    }
    LOG_D(BLE_NETIF_LWIP_DBG, "bleif[%d] recv data len=%d len, send to uart", conn_handle, hdr->omp_len);

    net_data = malloc(hdr->omp_len);
    if (OS_NULL == net_data)
    {
        LOG_E(BLE_NETIF_LWIP_DBG, "bleif[%d] recv data len=%d alloc menmory failed", conn_handle, hdr->omp_len);
        return;
    }

    os_mbuf_copydata(data, 0, hdr->omp_len, net_data);
    memcpy(dst_addr, net_data, 6);
    memcpy(src_addr, net_data + 6, 6);
    ble_l2cap_set_ifidx(conn_handle, src_addr);
    if_idx = ble_l2cap_get_ifidx(dst_addr);
    LOG_D(BLE_NETIF_LWIP_DBG,
          "if_idx=%d recv len=%d,src=%02x%02x%02x%02x%02x%02x,dst=%02x%02x%02x%02x%02x%02x",
          if_idx,
          hdr->omp_len,
          src_addr[0],
          src_addr[1],
          src_addr[2],
          src_addr[3],
          src_addr[4],
          src_addr[5],
          dst_addr[0],
          dst_addr[1],
          dst_addr[2],
          dst_addr[3],
          dst_addr[4],
          dst_addr[5]);

    if (if_idx != 0xfe)
    {
        l2cap_data_send(net_data, hdr->omp_len);
    }
    if (if_idx == 0xfe || if_idx == 0xff)
    {
        uart_nrf_tx(net_data, hdr->omp_len);
    }

    free(net_data);

    return;
}

void l2cap_data_send(uint8_t *data, int count)
{
    struct os_mbuf *sdu_xmit;
    uint8_t         if_idx = 1;
    int             rc;
    uint8_t         dst_addr[BLE_MAX_L2_ADDR_LEN];

    LOG_D(BLE_NETIF_LWIP_DBG, "l2cap_data_send len=%d", count);
    memcpy(dst_addr, data, 6);
    if_idx = ble_l2cap_get_ifidx(dst_addr);
    LOG_D(BLE_NETIF_LWIP_DBG,
          "dst_addr=%02x%02x%02x%02x%02x%02x,ifidx=%d",
          dst_addr[0],
          dst_addr[1],
          dst_addr[2],
          dst_addr[3],
          dst_addr[4],
          dst_addr[5],
          if_idx);

    do
    {
        sdu_xmit = os_msys_get_pkthdr(count, 0);
        if (OS_NULL == sdu_xmit)
        {
            LOG_E(BLE_NETIF_LWIP_DBG, "why alloc mbuf failed");
            break;
        }
        /*send all data*/
        rc = os_mbuf_append(sdu_xmit, data, count);

        if (0xff == if_idx)
        {
            int index;
            for (index = 0; index < BLE_L2CAP_IPSP_CHAN_NUM_MAX; index++)
            {
                struct os_mbuf *sdu_copy;
                sdu_copy = os_mbuf_dup(sdu_xmit);
                rc       = ble_l2cap_intf_send(index + 1, sdu_copy);
                if (rc)
                {
                    if (rc != BLE_HS_ESTALLED)
                        os_mbuf_free_chain(sdu_copy);
                }
            }
            os_mbuf_free_chain(sdu_xmit);
        }
        else if (0xfe != if_idx)
        {
            rc = ble_l2cap_intf_send(if_idx, sdu_xmit);
            if (rc)
            {
                if (rc != BLE_HS_ESTALLED)
                    os_mbuf_free_chain(sdu_xmit);
            }
        }
    } while (0);
}

static void l2cap_linkup_proc(uint16_t conn_handle, l2cap_intf_type_t type)
{
    LOG_I(BLE_NETIF_LWIP_DBG, "recv[%04x,%d] up", conn_handle, type);

    return;
}

int bleif_connect(uint16_t conn_handle)
{
    int rc;

    rc = ble_l2cap_ipsp_l2cap_connect(conn_handle, l2cap_recv_data_proc, l2cap_linkup_proc);

    return rc;
}
#endif
#endif

err_t oneos_netif_ipv4addr_bind_fn(struct netif *netif, const ip4_addr_t *ipaddr)
{
    LOG_I(BLE_NETIF_LWIP_DBG,
          "netif[%c%c%d] bind ip:%s",
          netif->name[0],
          netif->name[1],
          netif->num,
          ip4addr_ntoa(ipaddr));

    return 0;
}
