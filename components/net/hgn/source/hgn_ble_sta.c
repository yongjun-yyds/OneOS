#include "hgn.h"
#include <oneos_config.h>
#include <os_task.h>
#include <os_errno.h>
#include "nimble/nimble_npl.h"
#include "nimble/nimble_port.h"
#include "nimble/ble.h"
#include "host/ble_hs.h"
#include "host/util/util.h"
#include "controller/ble_ll.h"
#include "services/gap/ble_svc_gap.h"
#include "services/ipss/ble_l2cap_ipsp.h"
#include "ble_netif_lwip.h"
#include "store/ram/ble_store_ram.h"

static uint8_t    g_own_addr_type;
static uint16_t   conn_handle;
static int        adv_flag   = 0;
static int        bleif_flag = 0;
static hgn_cfg_t *hgn_cfg    = NULL;

/* adv_event() calls advertise(), so forward declaration is required */
static void advertise(void);

#define BLE_STA_DBG "ble_sta"
#include <dlog.h>

static int adv_event(struct ble_gap_event *event, void *arg)
{
    struct ble_gap_conn_desc conn_desc;
    switch (event->type)
    {
    case BLE_GAP_EVENT_ADV_COMPLETE:
        adv_flag = 1;
        break;
    case BLE_GAP_EVENT_CONNECT:
        LOG_I(BLE_STA_DBG,
              "connection %s; status=%d",
              event->connect.status == 0 ? "established" : "failed",
              event->connect.status);
        ble_gap_conn_find(event->connect.conn_handle, &conn_desc);
        adv_flag = 0;
        break;
    case BLE_GAP_EVENT_CONN_UPDATE_REQ:
        /* connected device requests update of connection parameters,
           and these are being filled in - NULL sets default values */
        LOG_D(BLE_STA_DBG, "updating conncetion parameters...");
        event->conn_update_req.conn_handle = conn_handle;
        event->conn_update_req.peer_params = NULL;
        break;
    case BLE_GAP_EVENT_DISCONNECT:
        LOG_E(BLE_STA_DBG, "disconnect; reason=%d", event->disconnect.reason);

        /* reset conn_handle */
        conn_handle = BLE_HS_CONN_HANDLE_NONE;

        /* Connection terminated; resume advertising */
        adv_flag = 1;
        break;

    case BLE_GAP_EVENT_PASSKEY_ACTION:
        LOG_I(BLE_STA_DBG, "PASSKEY_ACTION_EVENT started, passkey=%d", hgn_cfg->key);
        struct ble_sm_io pkey = {0};
        int              key  = 0;
        int              rc;

        if (event->passkey.params.action == BLE_SM_IOACT_INPUT)
        {
            pkey.action  = event->passkey.params.action;
            pkey.passkey = hgn_cfg->key;
            rc           = ble_sm_inject_io(event->passkey.conn_handle, &pkey);
            LOG_D(BLE_STA_DBG, "ble_sm_inject_io result: %d", rc);
        }
        break;

    case BLE_GAP_EVENT_REPEAT_PAIRING:
        /* We already have a bond with the peer, but it is attempting to
         * establish a new secure link.  This app sacrifices security for
         * convenience: just throw away the old bond and accept the new link.
         */

        /* Delete the old bond. */
        rc = ble_gap_conn_find(event->repeat_pairing.conn_handle, &conn_desc);
        ble_store_util_delete_peer(&conn_desc.peer_id_addr);

        /* Return BLE_GAP_REPEAT_PAIRING_RETRY to indicate that the host should
         * continue with the pairing operation.
         */
        return BLE_GAP_REPEAT_PAIRING_RETRY;

    default:
        LOG_D(BLE_STA_DBG,
              "Advertising event not handled,"
              "event code: %u",
              event->type);
        break;
    }
    return 0;
}

static void advertise(void)
{
    adv_flag = 0;
    int rc;

    /* set adv parameters */
    struct ble_gap_adv_params adv_params;
    struct ble_hs_adv_fields  fields;
    /* advertising payload is split into advertising data and advertising
       response, because all data cannot fit into single packet; name of device
       is sent as response to scan request */
    struct ble_hs_adv_fields rsp_fields;

    /* fill all fields and parameters with zeros */
    memset(&adv_params, 0, sizeof(adv_params));
    memset(&fields, 0, sizeof(fields));
    memset(&rsp_fields, 0, sizeof(rsp_fields));

    adv_params.conn_mode = BLE_GAP_CONN_MODE_UND;
    adv_params.disc_mode = BLE_GAP_DISC_MODE_GEN;

    fields.flags                    = BLE_HS_ADV_F_DISC_GEN | BLE_HS_ADV_F_BREDR_UNSUP;
    const ble_uuid128_t predef_uuid = BLE_SPECIFIC_UUID;
    fields.uuids128                 = &predef_uuid;
    fields.num_uuids128             = 1;
    fields.uuids128_is_complete     = 0;
    fields.tx_pwr_lvl               = BLE_HS_ADV_TX_PWR_LVL_AUTO;

    rsp_fields.name             = (uint8_t *)hgn_cfg->name;
    rsp_fields.name_len         = strlen(hgn_cfg->name);
    rsp_fields.name_is_complete = 1;

    rc = ble_gap_adv_set_fields(&fields);

    rc = ble_gap_adv_rsp_set_fields(&rsp_fields);

    rc = ble_gap_adv_start(g_own_addr_type, NULL, hgn_cfg->intvl, &adv_params, adv_event, NULL);
}

static void on_sync(void)
{
    int     rc;
    uint8_t pub_addr[6];
    int     out_nrpa;

    memset(pub_addr, 0, sizeof(pub_addr));

    /* g_own_addr_type will store type of addres our BSP uses */
    rc = ble_hs_util_ensure_addr(0);
    rc = ble_hs_id_infer_auto(0, &g_own_addr_type);

    rc = ble_hs_id_copy_addr(g_own_addr_type, pub_addr, &out_nrpa);
    LOG_I(BLE_STA_DBG,
          "pub_addr=%02x%02x%02x%02x%02x%02x",
          pub_addr[0],
          pub_addr[1],
          pub_addr[2],
          pub_addr[3],
          pub_addr[4],
          pub_addr[5]);

    if (bleif_flag != 1)
    {
        rc = bleif_netif_sta_init(pub_addr);
        if (rc == 0)
        {
            os_task_msleep(3000);
            bleif_flag = 1;
        }
    }

    os_lwip_register_ipv4bind_cb(BLE_LWIP_IF_DEVICE_NAME, oneos_netif_ipv4addr_bind_fn);

    /* begin advertising */
    advertise();
}

static void on_reset(int reason)
{
    LOG_I(BLE_STA_DBG, "Resetting state; reason=%d", reason);
}

static void hgn_ble_sta_task(void *parameter)
{
    os_err_t ret = OS_SUCCESS;

    hgn_cfg = (hgn_cfg_t *)calloc(1, sizeof(hgn_cfg_t));
    if (!hgn_cfg)
    {
        LOG_E(BLE_STA_DBG, "no memory for hgn_cfg");
        return;
    }

    ret = hgn_get_cfg(hgn_cfg);
    if (ret != OS_SUCCESS)
    {
        LOG_E(BLE_STA_DBG, "get hgn_cfg err");
        return;
    }

    /* Initialize all packages. */
    nimble_port_oneos_init();
    ble_ll_set_public_addr(hgn_cfg->mac);
    ble_l2cap_ipsp_init();

    ble_hs_cfg.sync_cb  = on_sync;
    ble_hs_cfg.reset_cb = on_reset;

    ble_store_ram_init();

    ble_svc_gap_device_name_set(hgn_cfg->name);
    LOG_I(BLE_STA_DBG, "sta name=%s; adv intvl=%d", hgn_cfg->name, hgn_cfg->intvl);

    /* As the last thing, process events from default event queue. */
    ble_hs_task_startup();

    while (1)
    {
        if (adv_flag == 1)
        {
            advertise();
        }
        os_task_msleep(100);
    }
}

int hgn_ble_sta_start(void)
{
    os_task_id tid;

    tid = os_task_create(NULL, NULL, 2048, "hgn_ble_sta", hgn_ble_sta_task, NULL, 20);
    os_task_startup(tid);

    return 0;
}
OS_INIT_CALL(hgn_ble_sta_start, OS_INIT_LEVEL_APPLICATION, OS_INIT_SUBLEVEL_LOW);
