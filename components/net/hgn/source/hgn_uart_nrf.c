/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

#include <assert.h>

#include "nrf.h"
#include "nrf52_hal.h"
#include "ble_netif_lwip.h"

#include "os_task.h"
#include "os_sem.h"
#include "rw_buff.h"

#define BLE_UART_DBG "ble_uart"
#include <dlog.h>

#define UARTE_INT_ENDTX     UARTE_INTEN_ENDTX_Msk
#define UARTE_INT_ENDRX     UARTE_INTEN_ENDRX_Msk
#define UARTE_CONFIG_PARITY UARTE_CONFIG_PARITY_Msk
#define UARTE_CONFIG_HWFC   UARTE_CONFIG_HWFC_Msk
#define UARTE_ENABLE        UARTE_ENABLE_ENABLE_Enabled
#define UARTE_DISABLE       UARTE_ENABLE_ENABLE_Disabled

#define RX_PIN_NUMBER  8
#define TX_PIN_NUMBER  6
#define CTS_PIN_NUMBER 7
#define RTS_PIN_NUMBER 5

/*
 * Only one UART on NRF 52832.
 */
struct hal_uart
{
    uint8_t u_open : 1;
    uint8_t u_rx_stall : 1;
    uint8_t u_tx_started : 1;
    uint8_t u_rx_buf;
    uint8_t u_tx_buf[8];
    void   *u_func_arg;
};

#if defined(NRF52840_XXAA)
static struct hal_uart uart0;
static struct hal_uart uart1;
#else
static struct hal_uart uart0;
#endif

#define UART_TX_BUF_MAX 1536
#define UART_RX_BUF_MAX 1536

#define UART_TX_TIMEOUT 3000
#define UART_RX_TIMEOUT 3000

static os_semaphore_id hgn_uart_rx_sid;
static uint8_t         tx_buf[UART_TX_BUF_MAX];
static uint8_t         rx_buf[UART_RX_BUF_MAX];
static uint8_t         pkt_buf[UART_RX_BUF_MAX * 3];
static int             current_byte = 0;

RW_BUFF_DEF(uart0_rx_t, char, 0x800);
RW_BUFF_CLAIM(uart0_rx_t, uart0_rx);
RW_BUFF_DEF(uart0_tx_t, char, 0x800);
RW_BUFF_CLAIM(uart0_tx_t, uart0_tx);

void write_to_buf(uint8_t *buf, uint8_t *data, int len)
{
    uint16_t header_magic = NET_UART_HEADER_MAGIC;

    if (current_byte == 0)
    {
        if (memcmp(data, &header_magic, 2) == 0)
        {
            memcpy(buf + current_byte, data, len);
            current_byte += len;
        }
        else
        {
            LOG_D(BLE_UART_DBG, "write err header %02x%02x", data[0], data[1]);
        }
    }
    else
    {
        memcpy(buf + current_byte, data, len);
        current_byte += len;
    }

    if (current_byte > UART_RX_BUF_MAX * 2)
    {
        LOG_E(BLE_UART_DBG, "byte too long %d", current_byte);
        current_byte = 0;
    }
}

int is_pkt_complete(uint8_t *buf)
{
    uint16_t header_magic = NET_UART_HEADER_MAGIC;

    if (current_byte < 4)
    {
        return -1;
    }
    if (memcmp(buf, &header_magic, 2) == 0)
    {
        net_uart_header_t uart_header;
        memcpy(&uart_header, buf, 4);
        if (current_byte >= (uart_header.tot_len + 4))
        {
            return uart_header.tot_len;
        }
    }
    else
    {
        LOG_E(BLE_UART_DBG, "find err header, reset byte");
        current_byte = 0;
    }
    return -1;
}

int get_pkt_from_buf(uint8_t *ibuf, uint8_t **obuf)
{
    int pkt_len   = 0;
    int left_byte = 0;

    pkt_len = is_pkt_complete(ibuf);
    if (pkt_len > 0)
    {
        *obuf = ibuf + 4;
        left_byte = current_byte - pkt_len - 4;
        if (left_byte > 0)
        {
            memmove(ibuf, ibuf + pkt_len + 4, left_byte);
        }
        current_byte = left_byte;
        return pkt_len;
    }

    return 0;
}

static int hal_hgn_uart_tx_fill_buf(struct hal_uart *u)
{
    uart0_tx_t *uart0_tx_b = &uart0_tx;
    int         i          = 0;
    int         cnt        = RW_BUFF_CNT(uart0_tx_b);
    uint8_t     ch;
    if (cnt >= 8)
    {
        for (i = 0; i < 8; i++)
        {
            RW_BUFF_READ(uart0_tx_b, ch);
            u->u_tx_buf[i] = ch;
        }

        return 8;
    }
    else
    {
        for (i = 0; i < cnt; i++)
        {
            RW_BUFF_READ(uart0_tx_b, ch);
            u->u_tx_buf[i] = ch;
        }
        return cnt;
    }
}

void hal_uart_start_tx(int port)
{
    NRF_UARTE_Type  *nrf_uart;
    struct hal_uart *u;
    int              sr;
    int              rc;

#if defined(NRF52840_XXAA)
    if (port == 0)
    {
        nrf_uart = NRF_UARTE0;
        u        = &uart0;
    }
    else if (port == 1)
    {
        nrf_uart = NRF_UARTE1;
        u        = &uart1;
    }
    else
    {
        return;
    }
#else
    if (port != 0)
    {
        return;
    }
    nrf_uart = NRF_UARTE0;
    u        = &uart0;
#endif

    __HAL_DISABLE_INTERRUPTS(sr);
    if (u->u_tx_started == 0)
    {
        rc = hal_hgn_uart_tx_fill_buf(u);
        if (rc > 0)
        {
            nrf_uart->INTENSET      = UARTE_INT_ENDTX;
            nrf_uart->TXD.PTR       = (uint32_t)&u->u_tx_buf;
            nrf_uart->TXD.MAXCNT    = rc;
            nrf_uart->TASKS_STARTTX = 1;
            u->u_tx_started         = 1;
        }
    }
    __HAL_ENABLE_INTERRUPTS(sr);
}

static void uart_irq_handler(NRF_UARTE_Type *nrf_uart, struct hal_uart *u)
{
    int rc;

    if (nrf_uart->EVENTS_ENDTX)
    {
        nrf_uart->EVENTS_ENDTX = 0;
        rc                     = hal_hgn_uart_tx_fill_buf(u);
        if (rc > 0)
        {
            nrf_uart->TXD.PTR       = (uint32_t)&u->u_tx_buf;
            nrf_uart->TXD.MAXCNT    = rc;
            nrf_uart->TASKS_STARTTX = 1;
        }
        else
        {
            nrf_uart->INTENCLR     = UARTE_INT_ENDTX;
            nrf_uart->TASKS_STOPTX = 1;
            u->u_tx_started        = 0;
        }
    }
    if (nrf_uart->EVENTS_ENDRX)
    {
        nrf_uart->EVENTS_ENDRX = 0;

        uint8_t ch;
        ch                     = u->u_rx_buf;
        uart0_rx_t *uart0_rx_p = &uart0_rx;
        if (!RW_BUFF_FULL(uart0_rx_p))
        {
            RW_BUFF_WRITE(uart0_rx_p, ch);
            int cnt = RW_BUFF_CNT(uart0_rx_p);
            if ((0x800 - 0x400) == cnt)
            {
                os_semaphore_post(hgn_uart_rx_sid);
            }
        }

        if (rc < 0)
        {
            u->u_rx_stall = 1;
        }
        else
        {
            nrf_uart->TASKS_STARTRX = 1;
        }
    }
}

void UARTE0_UART0_IRQHandler(void)
{
    uart_irq_handler(NRF_UARTE0, &uart0);
}

static void uart0_irq_handler(void)
{
    uart_irq_handler(NRF_UARTE0, &uart0);
}

#if defined(NRF52840_XXAA)
static void uart1_irq_handler(void)
{
    uart_irq_handler(NRF_UARTE1, &uart1);
}
#endif

static uint32_t hal_uart_baudrate(int baudrate)
{
    switch (baudrate)
    {
    case 1200:
        return UARTE_BAUDRATE_BAUDRATE_Baud1200;
    case 2400:
        return UARTE_BAUDRATE_BAUDRATE_Baud2400;
    case 4800:
        return UARTE_BAUDRATE_BAUDRATE_Baud4800;
    case 9600:
        return UARTE_BAUDRATE_BAUDRATE_Baud9600;
    case 14400:
        return UARTE_BAUDRATE_BAUDRATE_Baud14400;
    case 19200:
        return UARTE_BAUDRATE_BAUDRATE_Baud19200;
    case 28800:
        return UARTE_BAUDRATE_BAUDRATE_Baud28800;
    case 38400:
        return UARTE_BAUDRATE_BAUDRATE_Baud38400;
    case 56000:
        return UARTE_BAUDRATE_BAUDRATE_Baud56000;
    case 57600:
        return UARTE_BAUDRATE_BAUDRATE_Baud57600;
    case 76800:
        return UARTE_BAUDRATE_BAUDRATE_Baud76800;
    case 115200:
        return UARTE_BAUDRATE_BAUDRATE_Baud115200;
    case 230400:
        return UARTE_BAUDRATE_BAUDRATE_Baud230400;
    case 250000:
        return UARTE_BAUDRATE_BAUDRATE_Baud250000;
    case 460800:
        return UARTE_BAUDRATE_BAUDRATE_Baud460800;
    case 921600:
        return UARTE_BAUDRATE_BAUDRATE_Baud921600;
    case 1000000:
        return UARTE_BAUDRATE_BAUDRATE_Baud1M;
    default:
        return 0;
    }
}

void uart_nrf_tx(uint8_t *data, int count)
{
    uart0_tx_t *uart0_tx_b = &uart0_tx;

    net_uart_header_t uart_header;
    uart_header.magic   = NET_UART_HEADER_MAGIC;
    uart_header.tot_len = count;

    memcpy(tx_buf, &uart_header, 4);
    for (int i = 0; i < 4; i++)
    {
        if (!RW_BUFF_FULL(uart0_tx_b))
        {
            RW_BUFF_WRITE(uart0_tx_b, tx_buf[i]);
        }
    }
    hal_uart_start_tx(0);

    memcpy(tx_buf, data, count);
    for (int i = 0; i < count; i++)
    {
        if (!RW_BUFF_FULL(uart0_tx_b))
        {
            RW_BUFF_WRITE(uart0_tx_b, tx_buf[i]);
        }
    }
    hal_uart_start_tx(0);
}

int hal_uart_init(int port, void *arg)
{
    struct nrf52_uart_cfg *cfg;
    NRF_UARTE_Type        *nrf_uart;

#if defined(NRF52840_XXAA)
    if (port == 0)
    {
        nrf_uart = NRF_UARTE0;
        NVIC_SetVector(UARTE0_UART0_IRQn, (uint32_t)uart0_irq_handler);
    }
    else if (port == 1)
    {
        nrf_uart = NRF_UARTE1;
        NVIC_SetVector(UARTE1_IRQn, (uint32_t)uart1_irq_handler);
    }
    else
    {
        return -1;
    }
#else
    if (port != 0)
    {
        return -1;
    }
    nrf_uart = NRF_UARTE0;
    NVIC_SetVector(UARTE0_UART0_IRQn, (uint32_t)uart0_irq_handler);
#endif

    cfg = (struct nrf52_uart_cfg *)arg;

    nrf_uart->PSEL.TXD = cfg->suc_pin_tx;
    nrf_uart->PSEL.RXD = cfg->suc_pin_rx;
    nrf_uart->PSEL.RTS = cfg->suc_pin_rts;
    nrf_uart->PSEL.CTS = cfg->suc_pin_cts;

    return 0;
}

int hal_uart_config(int port, int32_t baudrate)
{
    struct hal_uart *u;
    uint32_t         cfg_reg = 0;
    uint32_t         baud_reg;
    NRF_UARTE_Type  *nrf_uart;
    IRQn_Type        irqnum;

#if defined(NRF52840_XXAA)
    if (port == 0)
    {
        nrf_uart = NRF_UARTE0;
        irqnum   = UARTE0_UART0_IRQn;
        u        = &uart0;
    }
    else if (port == 1)
    {
        nrf_uart = NRF_UARTE1;
        irqnum   = UARTE1_IRQn;
        u        = &uart1;
    }
    else
    {
        return -1;
    }
#else
    if (port != 0)
    {
        return -1;
    }
    nrf_uart = NRF_UARTE0;
    irqnum   = UARTE0_UART0_IRQn;
    u        = &uart0;
#endif

    if (u->u_open)
    {
        return -1;
    }

#if 0
    /*
     * pin config
     * UART config
     * nvic config
     * enable uart
     */
    if (databits != 8) {
        return -1;
    }
    if (stopbits != 1) {
        return -1;
    }

    switch (parity) {
    case HAL_UART_PARITY_NONE:
        break;
    case HAL_UART_PARITY_ODD:
        return -1;
    case HAL_UART_PARITY_EVEN:
        cfg_reg |= UARTE_CONFIG_PARITY;
        break;
    }

    switch (flow_ctl) {
    case HAL_UART_FLOW_CTL_NONE:
        break;
    case HAL_UART_FLOW_CTL_RTS_CTS:
        cfg_reg |= UARTE_CONFIG_HWFC;
        if (nrf_uart->PSEL.RTS == 0xffffffff ||
          nrf_uart->PSEL.CTS == 0xffffffff) {
            /*
             * Can't turn on HW flow control if pins to do that are not
             * defined.
             */
            assert(0);
            return -1;
        }
        break;
    }
#endif

    baud_reg = hal_uart_baudrate(baudrate);
    if (baud_reg == 0)
    {
        return -1;
    }
    nrf_uart->ENABLE   = 0;
    nrf_uart->INTENCLR = 0xffffffff;
    nrf_uart->BAUDRATE = baud_reg;
    nrf_uart->CONFIG   = cfg_reg;

    NVIC_EnableIRQ(irqnum);

    nrf_uart->ENABLE = UARTE_ENABLE;

    nrf_uart->INTENSET      = UARTE_INT_ENDRX;
    nrf_uart->RXD.PTR       = (uint32_t)&u->u_rx_buf;
    nrf_uart->RXD.MAXCNT    = sizeof(u->u_rx_buf);
    nrf_uart->TASKS_STARTRX = 1;

    u->u_rx_stall   = 0;
    u->u_tx_started = 0;
    u->u_open       = 1;

    return 0;
}

void hgn_uart_rx_task_entry(void *para)
{
    int      count;
    uint8_t *data;
    
    while (1)
    {
        os_semaphore_wait(hgn_uart_rx_sid, 1);
        uart0_rx_t *uart0_rx_p = &uart0_rx;
        int         cnt        = RW_BUFF_CNT(uart0_rx_p);
        if (cnt)
        {
            RW_BUFF_READ_N(uart0_rx_p, rx_buf, cnt);

            write_to_buf(pkt_buf, rx_buf, cnt);

            while (current_byte > 0)
            {
                count = get_pkt_from_buf(pkt_buf, &data);
                if (count > 0)
                {
                    l2cap_data_send(data, count);
                }
                else
                {
                    break;
                }
            }
        }
    }
}

#include "syscfg/syscfg.h"
int hgn_uart_init(void)
{
    int rc;

    hgn_uart_rx_sid = os_semaphore_create(NULL, "hgn_uart_rx_sem", 0, OS_SEM_MAX_VALUE);

    os_task_id tid;
    tid = os_task_create(NULL, NULL, 4096, "hgn_uart_rx", hgn_uart_rx_task_entry, NULL, 5);
    assert(tid != 0);
    os_task_startup(tid);

    struct nrf52_uart_cfg nrf52_uart_cfg = {
        .suc_pin_tx  = TX_PIN_NUMBER, /* pins for IO */
        .suc_pin_rx  = RX_PIN_NUMBER,
        .suc_pin_rts = RTS_PIN_NUMBER,
        .suc_pin_cts = CTS_PIN_NUMBER,
    };

    /* Initialize all packages. */
    hal_uart_init(0, &nrf52_uart_cfg);

    rc = hal_uart_config(0, HGN_UART_BAUDRATE);
    if (rc != 0)
    {
        assert(0);
    }
    return 0;
}