#include <drv_cfg.h>

#include <oneos_config.h>
#include <os_errno.h>
#include <os_task.h>
#include <os_event.h>
#include <net_dev.h>
#include "ble_netif_lwip.h"

#define BLE_UART_DBG "ble_uart"
#include <dlog.h>

#define UART_TX_BUF_MAX 1536
#define UART_RX_BUF_MAX 1536

static os_device_t *hgn_uart;
static uint8_t      tx_buf[UART_TX_BUF_MAX];
static uint8_t      rx_buf[UART_RX_BUF_MAX];
static uint8_t      pkt_buf[UART_RX_BUF_MAX * 3];
static int          current_byte = 0;

void write_to_buf(uint8_t *buf, uint8_t *data, int len)
{
    uint16_t header_magic = NET_UART_HEADER_MAGIC;

    if (current_byte == 0)
    {
        if (memcmp(data, &header_magic, 2) == 0)
        {
            memcpy(buf + current_byte, data, len);
            current_byte += len;
        }
        else
        {
            LOG_D(BLE_UART_DBG, "write err header %02x%02x", data[0], data[1]);
        }
    }
    else
    {
        memcpy(buf + current_byte, data, len);
        current_byte += len;
    }

    if (current_byte > UART_RX_BUF_MAX * 2)
    {
        LOG_E(BLE_UART_DBG, "byte too long %d", current_byte);
        current_byte = 0;
    }
}

int is_pkt_complete(uint8_t *buf)
{
    uint16_t header_magic = NET_UART_HEADER_MAGIC;

    if (current_byte < 4)
    {
        return -1;
    }
    if (memcmp(buf, &header_magic, 2) == 0)
    {
        net_uart_header_t uart_header;
        memcpy(&uart_header, buf, 4);
        if (current_byte >= (uart_header.tot_len + 4))
        {
            return uart_header.tot_len;
        }
    }
    else
    {
        LOG_E(BLE_UART_DBG, "find err header, reset byte");
        current_byte = 0;
    }
    return -1;
}

int get_pkt_from_buf(uint8_t *ibuf, uint8_t **obuf)
{
    int pkt_len   = 0;
    int left_byte = 0;

    pkt_len = is_pkt_complete(ibuf);
    if (pkt_len > 0)
    {
        *obuf = ibuf + 4;
        left_byte = current_byte - pkt_len - 4;
        if (left_byte > 0)
        {
            memmove(ibuf, ibuf + pkt_len + 4, left_byte);
        }
        current_byte = left_byte;
        return pkt_len;
    }

    return 0;
}

void hgn_uart_rx_task(void *parameter)
{
    int      count;
    uint8_t *data;

    while (1)
    {
        count = os_device_read_block(hgn_uart, 0, rx_buf, UART_RX_BUF_MAX);
        if (count > 0)
        {
            write_to_buf(pkt_buf, rx_buf, count);

            while (current_byte > 0)
            {
                count = get_pkt_from_buf(pkt_buf, &data);
                if (count > 0)
                {
                    bleif_uart_recv(data, count);
                }
                else
                {
                    break;
                }
            }
        }
    }
}

void net_uart_send(uint8_t *data, int len)
{
    if (data && (len > 0))
    {
        net_uart_header_t uart_header;
        uart_header.magic   = NET_UART_HEADER_MAGIC;
        uart_header.tot_len = len;
        memcpy(tx_buf, &uart_header, 4);
        os_device_write_block(hgn_uart, 0, tx_buf, 4);

        memcpy(tx_buf, data, len);
        os_device_write_block(hgn_uart, 0, tx_buf, len);
    }
}

int hgn_uart_init(void)
{
    os_task_id              tid;
    struct serial_configure cfg = OS_SERIAL_CONFIG_DEFAULT;
    cfg.baud_rate               = HGN_UART_BAUDRATE;

    while (!hgn_uart)
    {
        hgn_uart = os_device_find(HGN_BRIDGE_UART_NAME);
        os_device_control(hgn_uart, OS_DEVICE_CTRL_CONFIG, &cfg);
        os_device_open(hgn_uart);
        os_task_msleep(3000);
    }
    OS_ASSERT(hgn_uart);

    tid = os_task_create(NULL, NULL, 8192, "hgn_uart_rx", hgn_uart_rx_task, NULL, 5);
    OS_ASSERT(0 != tid);
    os_task_startup(tid);

    return 0;
}
