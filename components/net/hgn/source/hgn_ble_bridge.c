#include "hgn.h"
#include <oneos_config.h>
#include <os_task.h>

#include <netif/bridgeif.h>
#include <netif/ethernet.h>
#include <lwip/ip_addr.h>
#include <lwip/dhcp.h>
#include <lwip/dhcp6.h>
#include <oneos_lwip.h>

#include "ble_netif_lwip.h"
#define BLE_BRIDGE_DBG "ble_bridge"
#include <dlog.h>

uint8_t bridge_default_mac[6] = {0xc0, 0xa8, 0xb9, 0x12, 0x34, 0x77};

typedef struct ble_bridge_dev
{
    struct os_net_device net_dev;
    uint8_t              dev_addr[BLE_MAX_L2_ADDR_LEN];
} ble_bridge_dev_t;

static ble_bridge_dev_t *gs_ble_bridge_dev;

static os_err_t bridge_get_macaddr(struct os_net_device *net_dev, uint8_t *addr)
{
    memcpy(addr, gs_ble_bridge_dev->dev_addr, 6);

    return 0;
}
const static struct os_net_device_ops ble_bridge_ops = {
    .init       = OS_NULL,
    .deinit     = OS_NULL,
    .send       = OS_NULL,
    .get_mac    = bridge_get_macaddr,
    .set_filter = OS_NULL,
};

static void hgn_ble_bridge_task(void *parameter)
{
    uint8_t *pub_addr;

    pub_addr = bridge_default_mac;
    bleif_netif_ap_init(pub_addr);
    os_task_msleep(2000);

    hgn_uart_init();
    os_task_msleep(2000);

    struct netif *ble_bridge;
    struct netif *ble_netif;
    struct netif *eth_netif;
    int           ble_flag = 0;
    int           eth_flag = 0;
    int           flag     = 0;
    int           rc;
    int           i;

    gs_ble_bridge_dev = malloc(sizeof(ble_bridge_dev_t));
    if (OS_NULL == gs_ble_bridge_dev)
    {
        LOG_E(BLE_BRIDGE_DBG, "bridge_dev malloc failed!");
        return;
    }
    memset(gs_ble_bridge_dev, 0, sizeof(ble_bridge_dev_t));

    for (i = 0; i < 6; i++)
    {
        gs_ble_bridge_dev->dev_addr[i] = pub_addr[i];
    }

    gs_ble_bridge_dev->net_dev.info.MTU = ETHERNET_DEFAULT_MTU;
    gs_ble_bridge_dev->net_dev.info.MRU = ETHERNET_DEFAULT_MTU;

    gs_ble_bridge_dev->net_dev.info.mode = (os_net_mode_t)OS_TRUE;

    gs_ble_bridge_dev->net_dev.info.linkstatus = OS_TRUE;

    gs_ble_bridge_dev->net_dev.info.intf_type = net_dev_intf_br;
    gs_ble_bridge_dev->net_dev.ops            = &ble_bridge_ops;

    rc = os_net_device_register(&gs_ble_bridge_dev->net_dev, "ble_bridge");
    if (OS_SUCCESS != rc)
    {
        LOG_E(BLE_BRIDGE_DBG, "os_net_device_register failed");
    }

    ble_bridge = os_lwip_get_netif_by_devname("ble_bridge");
    if (ble_bridge == NULL)
    {
        LOG_E(BLE_BRIDGE_DBG, "get bridge netif failed");
    }

    while (1)
    {
        ble_netif = os_lwip_get_netif_by_devname(BLE_LWIP_IF_DEVICE_NAME);
        if (OS_NULL != ble_netif && ble_flag == 0)
        {
            bridgeif_add_port(ble_bridge, ble_netif);
            netif_set_down(ble_netif);
            os_task_msleep(500);
            ble_flag = 1;
        }
        eth_netif = os_lwip_get_netif_by_devname(HGN_BRIDGE_ETH_IF_NAME);
        if (OS_NULL != eth_netif && eth_flag == 0)
        {
            bridgeif_add_port(ble_bridge, eth_netif);
            netif_set_down(eth_netif);
            os_task_msleep(500);
            eth_flag = 1;
        }
        if (eth_flag == 1 && ble_flag == 1 && flag == 0)
        {
            LOG_I(BLE_BRIDGE_DBG, "set up ifs");
            netif_set_up(ble_bridge);
            netif_set_default(ble_bridge);
#if LWIP_DHCP
            LOG_I(BLE_BRIDGE_DBG, "set up ipv4 dhcp");
            dhcp_start(ble_bridge);
#endif
#if LWIP_IPV6_DHCP6
            LOG_I(BLE_BRIDGE_DBG, "set up ipv6 dhcp");
            dhcp6_enable_stateless(ble_bridge);
#endif
            flag = 1;
        }
        os_task_msleep(100);
    }
}

int hgn_ble_bridge_start(void)
{
    os_task_id tid;

    tid = os_task_create(NULL, NULL, 2048, "hgn_ble_bridge", hgn_ble_bridge_task, NULL, 20);
    os_task_startup(tid);

    return 0;
}
OS_INIT_CALL(hgn_ble_bridge_start, OS_INIT_LEVEL_APPLICATION, OS_INIT_SUBLEVEL_LOW);
