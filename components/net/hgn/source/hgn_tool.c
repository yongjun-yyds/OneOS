#include "hgn.h"
#include <string.h>
#include <fal/fal.h>
#include <shell.h>
#include <os_errno.h>

#define HGN_TOOL_DBG "hgn_tool"
#include <dlog.h>

#define HGN_FAL_PARTNAME "cfg"
#ifdef NET_USING_HGN_BLE_AP
#define HGN_SAVED_MARK (0x8D)
#else
#define HGN_SAVED_MARK (0x5C)
#endif

uint8_t     ap_default_mac[MAC_LEN]  = {0xc0, 0xa8, 0xb9, 0x12, 0x34, 0x88};
uint8_t     sta_default_mac[MAC_LEN] = {0xc0, 0xa8, 0xb9, 0x12, 0x34, 0x11};
const char *default_name             = "blesta";

void hgn_print(hgn_cfg_t *cfg)
{
#ifdef NET_USING_HGN_BLE_AP
    os_kprintf("hgn ap sec    : %d\r\n", cfg->sec);
    os_kprintf("hgn ap key    : %d\r\n", cfg->key);
    os_kprintf("hgn scan intvl: %d\r\n", cfg->intvl);
#else
    os_kprintf("hgn sta name  : %s\r\n", cfg->name);
    os_kprintf("hgn sta key   : %d\r\n", cfg->key);
    os_kprintf("hgn adv intvl : %d\r\n", cfg->intvl);
#endif
    os_kprintf("hgn mac       : %02x%02x%02x%02x%02x%02x\r\n",
               cfg->mac[0],
               cfg->mac[1],
               cfg->mac[2],
               cfg->mac[3],
               cfg->mac[4],
               cfg->mac[5]);
}

void hgn_help(void)
{
    os_kprintf("hgn                 : hgn ap/sta tool help\r\n");
    os_kprintf("hgn get             : get and print cfg\r\n");
    os_kprintf("hgn reset           : reset cfg\r\n");
#ifdef NET_USING_HGN_BLE_AP
    os_kprintf("hgn set sec  <0/1>  : set ap sec enable, 0:disable, 1:enable\r\n");
    os_kprintf("hgn set scan <intvl>: set ap scan intvl, unit ms, 625-62500\r\n");
#else
    os_kprintf("hgn set adv  <intvl>: set sta adv intvl, unit ms, 50-1000\r\n");
    os_kprintf("hgn set name <name> : set sta name, string len < 13\r\n");
#endif
    os_kprintf("hgn set key  <key>  : set ble key, 6 digits, 100000-999999\r\n");
    os_kprintf("hgn set mac  <mac>  : set ble mac, 12 chars(0-9,a-f), the second char must be even\r\n");
    os_kprintf("reboot the device after setting\r\n");
}

os_err_t hgn_get_cfg(hgn_cfg_t *cfg)
{
    int         ret = 0;
    fal_part_t *part;

    part = fal_part_find(HGN_FAL_PARTNAME);
    if (part == OS_NULL)
    {
        LOG_E(HGN_TOOL_DBG, "invalide fal part");
        return OS_FAILURE;
    }

    ret = fal_part_read(part, 0, (unsigned char *)cfg, sizeof(hgn_cfg_t));

    if (ret != sizeof(hgn_cfg_t))
    {
        LOG_E(HGN_TOOL_DBG, "fal_part_read err");
        return OS_FAILURE;
    }

    if (HGN_SAVED_MARK != cfg->mark)
    {
        LOG_E(HGN_TOOL_DBG, "No init cfg ,set to default");
        cfg->sec = 0;
        cfg->key = 123456;
        memset(cfg->name, 0, HGN_MAX_NAMELEN);
#ifdef NET_USING_HGN_BLE_AP
        cfg->intvl = 6250;
        memcpy(cfg->mac, ap_default_mac, MAC_LEN);
#else
        cfg->intvl = 100;
        memcpy(cfg->mac, sta_default_mac, MAC_LEN);
        memcpy(cfg->name, default_name, strlen(default_name) + 1);
#endif
        cfg->mark = HGN_SAVED_MARK;
        hgn_set_cfg(cfg);
    }

    return OS_SUCCESS;
}

os_err_t hgn_reset_cfg(void)
{
    int         ret = 0;
    fal_part_t *part;

    part = fal_part_find(HGN_FAL_PARTNAME);
    if (part == OS_NULL)
    {
        LOG_E(HGN_TOOL_DBG, "invalide fal part");
        return OS_FAILURE;
    }

    ret = fal_part_erase(part, 0, 0x1000);
    if (ret < 0)
    {
        LOG_E(HGN_TOOL_DBG, "fal_part_erase err!");
        return OS_FAILURE;
    }
    return OS_SUCCESS;
}

os_err_t hgn_set_cfg(hgn_cfg_t *cfg)
{
    int         ret = 0;
    fal_part_t *part;

    part = fal_part_find(HGN_FAL_PARTNAME);
    if (part == OS_NULL)
    {
        LOG_E(HGN_TOOL_DBG, "invalide fal part");
        return OS_FAILURE;
    }

    ret = fal_part_erase(part, 0, 0x1000);
    if (ret < 0)
    {
        LOG_E(HGN_TOOL_DBG, "fal_part_erase err!");
        return OS_FAILURE;
    }

    ret = fal_part_write(part, 0, (unsigned char *)cfg, sizeof(hgn_cfg_t));
    if (sizeof(hgn_cfg_t) != ret)
    {
        LOG_E(HGN_TOOL_DBG, "fal_part_write err! wirte:%d err:%d", sizeof(hgn_cfg_t), ret);
        return OS_FAILURE;
    }
    return OS_SUCCESS;
}

static void hgn_tool(int argc, char **argv)
{
    os_size_t   count;
    uint8_t    *data;
    fal_part_t *part;
    hgn_cfg_t  *hgn_cfg = NULL;
    os_err_t    ret     = OS_SUCCESS;

    if (argc == 2)
    {
        if (!strcmp(argv[1], "get"))
        {
            hgn_cfg = (hgn_cfg_t *)calloc(1, sizeof(hgn_cfg_t));
            if (!hgn_cfg)
            {
                LOG_E(HGN_TOOL_DBG, "no memory for hgn_cfg");
                return;
            }

            ret = hgn_get_cfg(hgn_cfg);
            if (ret != OS_SUCCESS)
            {
                LOG_E(HGN_TOOL_DBG, "get hgn_cfg err");
                free(hgn_cfg);
                return;
            }

            hgn_print(hgn_cfg);
            free(hgn_cfg);
        }
        else if (!strcmp(argv[1], "reset"))
        {
            hgn_reset_cfg();
        }
        else
        {
            hgn_help();
        }
    }
    else if (argc == 4)
    {
        if (!strcmp(argv[1], "set"))
        {
            hgn_cfg = (hgn_cfg_t *)calloc(1, sizeof(hgn_cfg_t));
            if (!hgn_cfg)
            {
                LOG_E(HGN_TOOL_DBG, "no memory for hgn_cfg");
                return;
            }

            ret = hgn_get_cfg(hgn_cfg);
            if (ret != OS_SUCCESS)
            {
                LOG_E(HGN_TOOL_DBG, "get hgn_cfg err");
                free(hgn_cfg);
                return;
            }

            if (!strcmp(argv[2], "sec"))
            {
                int tsec;
                tsec = strtol(argv[3], NULL, 10);
                if (tsec == 0 || tsec == 1)
                {
                    hgn_cfg->sec = tsec;
                    hgn_set_cfg(hgn_cfg);
                    free(hgn_cfg);
                    return;
                }
                else
                {
                    hgn_help();
                    free(hgn_cfg);
                    return;
                }
            }
            else if (!strcmp(argv[2], "key"))
            {
                int tkey;
                tkey = strtol(argv[3], NULL, 10);
                if (tkey >= 100000 && tkey <= 999999)
                {
                    hgn_cfg->key = tkey;
                    hgn_set_cfg(hgn_cfg);
                    free(hgn_cfg);
                    return;
                }
                else
                {
                    hgn_help();
                    free(hgn_cfg);
                    return;
                }
            }
            else if (!strcmp(argv[2], "scan"))
            {
                int tintvl;
                tintvl = strtol(argv[3], NULL, 10);
                if (tintvl >= 625 && tintvl <= 62500)
                {
                    hgn_cfg->intvl = tintvl;
                    hgn_set_cfg(hgn_cfg);
                    free(hgn_cfg);
                    return;
                }
                else
                {
                    hgn_help();
                    free(hgn_cfg);
                    return;
                }
            }
            else if (!strcmp(argv[2], "adv"))
            {
                int tintvl;
                tintvl = strtol(argv[3], NULL, 10);
                if (tintvl >= 50 && tintvl <= 1000)
                {
                    hgn_cfg->intvl = tintvl;
                    hgn_set_cfg(hgn_cfg);
                    free(hgn_cfg);
                    return;
                }
                else
                {
                    hgn_help();
                    free(hgn_cfg);
                    return;
                }
            }
            else if (!strcmp(argv[2], "mac"))
            {
                if (strlen(argv[3]) == 12)
                {
                    uint8_t tmac[MAC_LEN];
                    char    mactmp[3] = {0};
                    int     mact;
                    for (int i = 0; i < 6; i++)
                    {
                        memcpy(mactmp, argv[3] + i * 2, 2);
                        mact    = strtol(mactmp, NULL, 16);
                        tmac[i] = mact;
                    }
                    if (tmac[0] & 1)
                    {
                        hgn_help();
                        free(hgn_cfg);
                        return;
                    }
                    os_kprintf("%02x%02x%02x%02x%02x%02x\r\n", tmac[0], tmac[1], tmac[2], tmac[3], tmac[4], tmac[5]);
                    memcpy(hgn_cfg->mac, tmac, MAC_LEN);
                    hgn_set_cfg(hgn_cfg);
                    free(hgn_cfg);
                    return;
                }
                else
                {
                    hgn_help();
                    free(hgn_cfg);
                    return;
                }
            }
            else if (!strcmp(argv[2], "name"))
            {
                if (strlen(argv[3]) < 13)
                {
                    memcpy(hgn_cfg->name, argv[3], strlen(argv[3]) + 1);
                    hgn_set_cfg(hgn_cfg);
                    free(hgn_cfg);
                    return;
                }
                else
                {
                    hgn_help();
                    free(hgn_cfg);
                    return;
                }
            }
            else
            {
                hgn_help();
                free(hgn_cfg);
                return;
            }
        }
        else
        {
            hgn_help();
        }
    }
    else
    {
        hgn_help();
    }
}

SH_CMD_EXPORT(hgn, hgn_tool, "hgn tool");
