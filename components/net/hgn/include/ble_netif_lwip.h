/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

#ifndef H_BLE_NETIF_LWIP_
#define H_BLE_NETIF_LWIP_

#ifdef __cplusplus
extern "C" {
#endif

/* undef redefined macros */
#undef htons
#undef ntohs
#undef htonl
#undef ntohl
#undef STATS_INC

#include <os_types.h>
#include <net_dev.h>
#include "oneos_lwip.h"

#define BLE_LWIP_IF_DEVICE_NAME "blenet"
#define BLE_L2CAP_IPSP_MTU      1536
#define ETHERNET_DEFAULT_MTU    1500
#define BLE_MAX_L2_ADDR_LEN     6
#define NET_UART_HEADER_MAGIC   0xE77E
#define HGN_UART_BAUDRATE       921600

#define BLE_SPECIFIC_UUID                                                                                              \
    BLE_UUID128_INIT(0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xab)

typedef struct ble_netif_dev
{
    struct os_net_device net_dev;
    uint8_t              dev_addr[BLE_MAX_L2_ADDR_LEN];
} ble_netif_dev_t;

typedef struct net_uart_header
{
    uint16_t magic;
    int16_t  tot_len;
} net_uart_header_t;

int   bleif_netif_ap_init(uint8_t *oui_id_addr);
int   bleif_netif_sta_init(uint8_t *oui_id_addr);
void  bleif_netif_ap_start(void);
void  bleif_netif_sta_start(void);
int   bleif_connect(uint16_t conn_handle);
void  net_uart_send(uint8_t *data, int count);
void  uart_nrf_tx(uint8_t *data, int count);
void  l2cap_data_send(uint8_t *data, int count);
void  bleif_uart_recv(uint8_t *data, int count);
int   hgn_uart_init(void);
err_t oneos_netif_ipv4addr_bind_fn(struct netif *netif, const ip4_addr_t *ipaddr);

#ifdef __cplusplus
}
#endif

#endif /* end of H_BLE_NETIF_LWIP_ */
