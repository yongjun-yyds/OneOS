#ifndef __RW_BUFF_H__
#define __RW_BUFF_H__

#define NAME_CAT(x, y) x##y

#define RW_BUFF_DEF(type_name, buff_type, buff_length)                                                                 \
    typedef struct                                                                                                     \
    {                                                                                                                  \
        unsigned short read;                                                                                           \
        unsigned short write;                                                                                          \
        buff_type      buff[buff_length];                                                                              \
    } type_name;

#define RW_BUFF_LEN(var) (sizeof((var)->buff) / sizeof((var)->buff[0]))

#define RW_BUFF_CLAIM(type_name, var_name) type_name var_name = {.read = 0, .write = 0}

#define RW_BUFF_EMPTY(var) (((var)->read) == ((var)->write))

#define RW_BUFF_FULL(var) ((((var)->write + 1) % (RW_BUFF_LEN(var))) == ((var)->read))

#define RW_BUFF_CNT(var) ((((var)->write + RW_BUFF_LEN(var)) - (var)->read) % RW_BUFF_LEN(var))

#define RW_BUFF_LEFT(var) ((((var)->read + RW_BUFF_LEN(var)) - (var)->write - 1) % RW_BUFF_LEN(var))

#define RW_BUFF_WRITE(var, x)                                                                                          \
    {                                                                                                                  \
        (var)->buff[(var)->write] = x;                                                                                 \
        (var)->write++;                                                                                                \
        if ((var)->write == RW_BUFF_LEN(var))                                                                          \
        {                                                                                                              \
            (var)->write = 0;                                                                                          \
        }                                                                                                              \
    }

#define RW_BUFF_WRITE_N(var, x, n)                                                                                     \
    {                                                                                                                  \
        int                     write = (var)->write;                                                                  \
        typeof((var)->buff[0]) *buff  = (var)->buff;                                                                   \
        int                     free  = RW_BUFF_LEN(var) - write;                                                      \
        int                     first = (free > n ? n : free); /* Get the min */                                       \
        int                     i     = 0;                                                                             \
        for (; i < first; i++)                                                                                         \
        {                                                                                                              \
            x[i] = buff[write + i];                                                                                    \
        }                                                                                                              \
        for (; i < n; i++)                                                                                             \
        {                                                                                                              \
            x[i] = buff[i - first];                                                                                    \
        }                                                                                                              \
        (var)->write += n;                                                                                             \
        if ((var)->write >= RW_BUFF_LEN(var))                                                                          \
        {                                                                                                              \
            (var)->write -= RW_BUFF_LEN(var);                                                                          \
        }                                                                                                              \
    }

#define RW_BUFF_READ(var, x)                                                                                           \
    {                                                                                                                  \
        x = (var)->buff[(var)->read];                                                                                  \
        (var)->read++;                                                                                                 \
        if ((var)->read == RW_BUFF_LEN(var))                                                                           \
        {                                                                                                              \
            (var)->read = 0;                                                                                           \
        }                                                                                                              \
    }

#define RW_BUFF_READ_N(var, x, n)                                                                                      \
    {                                                                                                                  \
        int                     read  = (var)->read;                                                                   \
        typeof((var)->buff[0]) *buff  = (var)->buff;                                                                   \
        int                     free  = RW_BUFF_LEN(var) - read;                                                       \
        int                     first = (free > n ? n : free); /* Get the min */                                       \
        int                     i     = 0;                                                                             \
        for (; i < first; i++)                                                                                         \
        {                                                                                                              \
            x[i] = buff[read + i];                                                                                     \
        }                                                                                                              \
        for (; i < n; i++)                                                                                             \
        {                                                                                                              \
            x[i] = buff[i - first];                                                                                    \
        }                                                                                                              \
        (var)->read += n;                                                                                              \
        if ((var)->read >= RW_BUFF_LEN(var))                                                                           \
        {                                                                                                              \
            (var)->read -= RW_BUFF_LEN(var);                                                                           \
        }                                                                                                              \
    }

#endif
