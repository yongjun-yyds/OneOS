/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        hgn.h
 *
 * @brief       hgn declaration
 *
 * @revision
 * Date         Author          Notes
 * 2022-05-24   OneOS Team      first version
 ***********************************************************************************************************************
 */

#ifndef __HGN_H__
#define __HGN_H__
#include <os_types.h>

#define MAC_LEN         (6)
#define HGN_MAX_NAMELEN (13)
typedef struct hgn_cfg
{
    int           sec;
    int           key;
    int           intvl;
    uint8_t       mac[MAC_LEN];
    char          name[HGN_MAX_NAMELEN];
    unsigned char mark;
} hgn_cfg_t;

os_err_t hgn_get_cfg(hgn_cfg_t *cfg);
os_err_t hgn_set_cfg(hgn_cfg_t *cfg);
int      hgn_uart_init(void);
int      hgn_bridge_start(void);
int      hgn_ble_ap_start(void);
int      hgn_ble_sta_start(void);

#endif /* end of __HGN_H__ */
