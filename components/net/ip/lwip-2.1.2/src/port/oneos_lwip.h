/**
 * @file
 *
 * A netif implementing the ZigBee Eencapsulation Protocol (ZEP).
 * This is used to tunnel 6LowPAN over UDP.
 */

/*
 * Copyright (c) 2018 Simon Goldschmidt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * This file is part of the oneos adapt lwIP TCP/IP stack.
 *
 * Author: Simon Goldschmidt <goldsimon@gmx.de>
 *
 */

#ifndef ONEOS_LWIP_H
#define ONEOS_LWIP_H

#include <os_types.h>
#include <lwip/netif.h>
#include <lwip/ip4_addr.h>
#include "net_dev.h"

#ifdef __cplusplus
extern "C" {
#endif

os_err_t os_lwip_register_ipv4bind_cb(char *device_name, netif_ipv4addr_bind_fn v4addr_bind_cb);
os_err_t os_lwip_netif_get_ipv4_info(char *device_name, ip4_addr_t *ip_addr, ip4_addr_t *gw_addr, ip4_addr_t *nm_addr);
os_err_t os_lwip_netif_get_mac(char *device_name, uint8_t *mac);
os_err_t os_net_protocol_lwip_init(struct os_net_device *net_dev);
os_err_t os_net_protocol_lwip_deinit(struct os_net_device *net_dev);
struct netif *os_lwip_get_netif_by_devname(char *device_name);
struct netif *os_lwip_get_default_netif(void);

#ifdef __cplusplus
}
#endif

#endif /* ONEOS_LWIP_H */
