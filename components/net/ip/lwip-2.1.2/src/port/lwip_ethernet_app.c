/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        can.c
 *
 * @brief       This file provides functions for registering can device.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <os_stddef.h>
#include <os_errno.h>
#include <dlog.h>
#include <oneos_lwip.h>

#if LWIP_DHCP
#include <lwip/dhcp.h>
#endif
#if LWIP_IPV6_DHCP6
#include <lwip/dhcp6.h>
#endif
#ifdef LWIP_USING_DHCPD
#include <dhcp_server.h>
#endif
#if LWIP_DNS
#include <lwip/dns.h>
#endif

#define LWIP_ETH_APP_DBG "lwip_eth_app"

int lwip_eth_app_start(void)
{
    struct netif *netif;
    uint8_t       cnt;
    u8_t          idx;

    cnt = 0;
    idx = 1;
    do
    {
        netif = netif_get_by_index(idx);
        if (OS_NULL != netif)
        {
            break;
        }
        os_task_msleep(100);
        cnt++;
    } while (cnt < 5);

    if (OS_NULL == netif)
    {
        LOG_E(LWIP_ETH_APP_DBG, "Not find idx=%d's netif, please check", idx);
        return OS_SUCCESS;
    }

    netif_set_default(netif);
#if LWIP_DHCP
    dhcp_start(netif);
#endif

#if LWIP_IPV6_DHCP6
    dhcp6_enable_stateless(netif);
#endif

    return OS_SUCCESS;
}

/* start demo at here, when user want to start your own app, you can reference this demo and modify it */
OS_INIT_CALL(lwip_eth_app_start, OS_INIT_LEVEL_APPLICATION, OS_INIT_SUBLEVEL_LOW);
