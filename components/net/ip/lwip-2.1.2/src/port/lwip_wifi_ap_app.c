/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        can.c
 *
 * @brief       This file provides functions for registering can device.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <os_stddef.h>
#include <os_errno.h>
#include <dlog.h>
#include <oneos_config.h>
#include <oneos_lwip.h>

#if LWIP_IPV6
#include <lwip/ethip6.h>
#include <lwip/sockets.h>
#endif
#if LWIP_DHCP
#include <lwip/dhcp.h>
#endif
#if LWIP_IPV6_DHCP6
#include <lwip/dhcp6.h>
#endif
#ifdef LWIP_USING_DHCPD
#include <dhcp_server.h>
#endif
#if LWIP_DNS
#include <lwip/dns.h>
#endif

#define LWIP_WIFI_AP_DBG     "lwip_wifi_ap"
#define LWIP_WIFI_AP_DEVNAME "ap6181"

#define LWIP_WIFI_AP_INTF_ADDR    "192.168.20.1"
#define LWIP_WIFI_AP_INTF_GATEWAY "192.168.20.1"
#define LWIP_WIFI_AP_INTF_MASK    "192.168.20.255"

int lwip_wifi_ap_app_start(void)
{
    struct netif *netif;
    uint8_t       cnt;

    cnt = 0;
    do
    {
        /* the device name in driver register */
        netif = os_lwip_get_netif_by_devname(LWIP_WIFI_AP_DEVNAME);
        if (OS_NULL != netif)
        {
            break;
        }
        os_task_msleep(100);
        cnt++;
    } while (cnt < 5);

    if (OS_NULL == netif)
    {
        LOG_E(LWIP_WIFI_AP_DBG, "not find %s's netif, please check");
        return OS_SUCCESS;
    }

    netif_set_default(netif);

#if LWIP_IPV4
    ip4_addr_t addr;

    /* set ip address */
    if (ip4addr_aton(LWIP_WIFI_AP_INTF_ADDR, &addr))
    {
        netif_set_ipaddr(netif, &addr);
    }

    /* set gateway address */
    if (ip4addr_aton(LWIP_WIFI_AP_INTF_GATEWAY, &addr))
    {
        netif_set_gw(netif, &addr);
    }

    /* set netmask address */
    if (ip4addr_aton(LWIP_WIFI_AP_INTF_MASK, &addr))
    {
        netif_set_netmask(netif, &addr);
    }

#ifdef LWIP_USING_DHCPD
    uint8_t dhcpd_start_id;
    uint8_t dhcpd_end_id;

    /* according to your app require to set */
    dhcpd_start_id = 10;
    dhcpd_end_id   = 240;
    dhcpd_start(netif, LWIP_WIFI_AP_INTF_ADDR, dhcpd_start_id, dhcpd_end_id);
#endif
#endif

#if LWIP_IPV6
    ip6_addr_t wifi_prefix;
    ip6_addr_t wifi_gip;
    s8_t       ipaddr_index;

#define WIFI_AP_IPV6_PREFIX "9902::01"
    lwip_inet_pton(AF_INET6, WIFI_AP_IPV6_PREFIX, (void *)&wifi_prefix);
    IP6_ADDR(&wifi_gip,
             wifi_prefix.addr[0],
             wifi_prefix.addr[1],
             netif_ip6_addr(netif, 0)->addr[2],
             netif_ip6_addr(netif, 0)->addr[3]);
    ip6_addr_assign_zone(&wifi_gip, IP6_UNICAST, netif);
    ipaddr_index = 0;
    netif_add_ip6_address(netif, &wifi_gip, &ipaddr_index);

    extern int ra6_netif_add(struct netif * netif);
    ra6_netif_add(netif);
#endif

    return OS_SUCCESS;
}

/* start demo at here, when user want to start your own app, you can reference this demo and modify it */
OS_INIT_CALL(lwip_wifi_ap_app_start, OS_INIT_LEVEL_APPLICATION, OS_INIT_SUBLEVEL_HIGH);
