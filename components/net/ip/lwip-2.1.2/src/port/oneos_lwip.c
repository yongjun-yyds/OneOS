/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        can.c
 *
 * @brief       This file provides functions for registering can device.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <os_errno.h>
#include <os_assert.h>
#include <os_memory.h>
#include <string.h>
#include <os_list.h>
#include <os_mutex.h>

#include <lwip/init.h>
#include <lwip/netif.h>
#include <lwip/netifapi.h>
#include <lwip/err.h>
#include <lwip/etharp.h>
#include <lwip/tcpip.h>
#include <dlog.h>

#ifdef LWIP_USING_INTF_6LOWPAN_BLE
#include <netif/lowpan6_ble.h>
#endif
#if LWIP_IPV6
#include <lwip/ethip6.h>
#include <lwip/sockets.h>
#endif
#if LWIP_DHCP
#include <lwip/dhcp.h>
#endif
#if LWIP_IPV6_DHCP6
#include <lwip/dhcp6.h>
#endif
#ifdef LWIP_USING_DHCPD
#include <dhcp_server.h>
#endif
#if LWIP_DNS
#include <lwip/dns.h>
#endif
#if LWIP_NETIF_HOSTNAME
#if DNS_LOCAL_HOSTLIST && DNS_LOCAL_HOSTLIST_IS_DYNAMIC
#include <lwip/ip_addr.h>
#include <lwip/dns.h>
#endif
#include <stdio.h>
#endif
#include <oneos_lwip.h>

#define ONEOS_LWIP_ETHERNET_MTU 1500
#define ONEOS_LWIP_DBG          "oneos_lwip"

typedef enum
{
    LWIP_INIT_PHASE_INIT  = 1,
    LWIP_INIT_PHASE_DOING = 2,
    LWIP_INIT_PHASE_END   = 3,
} oneos_lwip_init_phase_t;

struct os_lwip_info
{
    struct netif          netif;
    struct os_net_device *net_dev;
    os_list_node_t        list;
};

static os_list_node_t          os_lwip_list       = OS_LIST_INIT(os_lwip_list);
static os_mutex_id             os_lwip_mid        = 0;
static oneos_lwip_init_phase_t gs_lwip_init_phase = LWIP_INIT_PHASE_INIT;
static void                    set_if(char *netif_name, char *ip_addr, char *gw_addr, char *nm_addr);

static inline void os_lwip_lock(void)
{
    if (0 == os_lwip_mid)
    {
        os_lwip_mid = os_mutex_create(NULL, "oneos_lwip", OS_FALSE);
        if (0 == os_lwip_mid)
        {
            LOG_E(ONEOS_LWIP_DBG, "oneos_lwip mutex create failed");
        }
    }
    if (0 != os_lwip_mid)
    {
        os_mutex_lock(os_lwip_mid, OS_WAIT_FOREVER);
    }

    return;
}

static inline void os_lwip_unlock(void)
{
    if (0 == os_lwip_mid)
    {
        LOG_E(ONEOS_LWIP_DBG, "oneos_lwip mutex is null, please check");
    }
    else
    {
        os_mutex_unlock(os_lwip_mid);
    }

    return;
}

/* OS_TRUE: link up */
static os_err_t os_lwip_linkchange(struct os_net_device *net_dev, os_bool_t linkstatus)
{
    struct os_lwip_info *lwip_dev = (struct os_lwip_info *)net_dev->userdata;

    if (LWIP_INIT_PHASE_END == gs_lwip_init_phase)
    {
        if (linkstatus)
        {
            netifapi_netif_set_link_up(&lwip_dev->netif);
        }
        else
        {
            netifapi_netif_set_link_down(&lwip_dev->netif);
#if LWIP_IPV6_DHCP6
            for (int index = 1; index < LWIP_IPV6_NUM_ADDRESSES; index++)
            {
                netif_ip6_addr_set_state(&lwip_dev->netif, index, IP6_ADDR_INVALID);
            }
#endif
#if (LWIP_DHCP || LWIP_IPV6_DHCP6) && LWIP_DNS
            dns_clrserver();
#endif
            netifapi_netif_set_addr(&lwip_dev->netif, OS_NULL, OS_NULL, OS_NULL);
        }
    }

    return OS_SUCCESS;
}

static err_t os_lwip_linkoutput(struct netif *netif, struct pbuf *p)
{
    int offset;
    uint8_t *buff;
    struct pbuf *q;
    struct os_lwip_info *lwip_dev = (struct os_lwip_info *)netif;

    if (p->len == p->tot_len)
    {
        buff = p->payload;
    }
    else
    {
        OS_ASSERT(p->tot_len <= lwip_dev->net_dev->info.MTU);
        buff = lwip_dev->net_dev->tx_buff;
        
        for (offset = 0, q = p; (q != OS_NULL) && (offset < p->tot_len); q = q->next)
        {
            memcpy(buff + offset, q->payload, q->len);
            offset += q->len;
        }
    }

    return os_net_tx_data(lwip_dev->net_dev, buff, p->tot_len);
}

static os_err_t os_lwip_input(struct os_net_device *net_dev, uint8_t *data, int len)
{
    int offset;
    struct netif *netif;
    struct pbuf  *p, *q;
    
    OS_ASSERT(net_dev != OS_NULL);
    
    netif = (struct netif *)net_dev->userdata;
    OS_ASSERT(netif != OS_NULL);
    OS_ASSERT(netif->input != OS_NULL);

    p = pbuf_alloc(PBUF_RAW, len, PBUF_POOL);
    if (OS_NULL == p)
    {
        LOG_E(ONEOS_LWIP_DBG, "os_lwip_input alloc pbuf failed");
        return OS_NOMEM;
    }
    
    for (offset = 0, q = p; (OS_NULL != q) && (offset < p->tot_len); q = q->next)
    {
        memcpy(q->payload, data + offset, q->len);
        offset += q->len;
    }

    netif->input(p, netif);
    return OS_SUCCESS;
}

struct os_net_protocol_ops protocol_ops = 
{
    .recv       = os_lwip_input,
    .linkchange = os_lwip_linkchange,
};

#if LWIP_IPV4 && LWIP_IGMP
static err_t os_igmp_mac_filter(struct netif *netif, const ip4_addr_t *ip4_addr, enum netif_mac_filter_action action)
{
    struct os_lwip_info *lwip_dev;
    os_bool_t            enable;
    const uint8_t       *p;
    uint8_t              mac[6];

    p        = (const uint8_t *)ip4_addr;
    lwip_dev = (struct os_lwip_info *)netif;

    mac[0] = 0x01;
    mac[1] = 0x00;
    mac[2] = 0x5E;
    mac[3] = *(p + 1) & 0x7F;
    mac[4] = *(p + 2);
    mac[5] = *(p + 3);

    enable = OS_TRUE;
    if (NETIF_DEL_MAC_FILTER == action)
    {
        enable = OS_FALSE;
    }

    return os_net_set_filter(lwip_dev->net_dev, mac, enable);
}
#endif /* LWIP_IPV4 && LWIP_IGMP */

#if LWIP_IPV6 && LWIP_IPV6_MLD
static err_t os_mld_mac_filter(struct netif *netif, const ip6_addr_t *ip6_addr, enum netif_mac_filter_action action)
{
    struct os_lwip_info *lwip_dev;
    os_bool_t            enable;
    const uint8_t       *p;
    uint8_t              mac[6];

    p        = (const uint8_t *)&ip6_addr->addr[3];
    lwip_dev = (struct os_lwip_info *)netif;

    mac[0] = 0x33;
    mac[1] = 0x33;
    mac[2] = *(p + 0);
    mac[3] = *(p + 1);
    mac[4] = *(p + 2);
    mac[5] = *(p + 3);

    enable = OS_TRUE;
    if (NETIF_DEL_MAC_FILTER == action)
    {
        enable = OS_FALSE;
    }

    return os_net_set_filter(lwip_dev->net_dev, mac, enable);
}
#endif /* LWIP_IPV6 && LWIP_IPV6_MLD */

static err_t ether_if_init(struct netif *netif)
{
    struct os_lwip_info  *lwip_dev;
    struct os_net_device *net_dev;
    os_err_t              do_err;

    lwip_dev = (struct os_lwip_info *)netif;
    net_dev  = lwip_dev->net_dev;
    if (OS_NULL == net_dev)
    {
        LOG_E(ONEOS_LWIP_DBG, "why net_dev is null");
        return ERR_IF;
    }

    netif->flags = NETIF_FLAG_BROADCAST | NETIF_FLAG_ETHARP | NETIF_FLAG_ETHERNET;

    if (net_dev->info.MTU >= ONEOS_LWIP_ETHERNET_MTU)
    {
        netif->mtu = ONEOS_LWIP_ETHERNET_MTU;
    }
    else
    {
        netif->mtu = net_dev->info.MTU;
    }

    do_err = os_net_get_macaddr(net_dev, netif->hwaddr);
    if (OS_SUCCESS != do_err)
    {
        LOG_E(ONEOS_LWIP_DBG, "why get mac addr failed");
        return ERR_IF;
    }
    netif->hwaddr_len = 6;

    netif->name[0] = 'e';
    netif->name[1] = '0';

#if LWIP_NETIF_HOSTNAME
    sprintf(netif->hostname, "netif_%02x%02x%d", netif->name[0], netif->name[1], netif->num);
#if DNS_LOCAL_HOSTLIST && DNS_LOCAL_HOSTLIST_IS_DYNAMIC
    ip_addr_t local_addr = IPADDR4_INIT_BYTES(127, 0, 0, 1);
    dns_local_addhost(netif->hostname, &local_addr);
#endif
#endif /* LWIP_NETIF_HOSTNAME */

    netif->output = etharp_output;

#if LWIP_IPV4 && LWIP_IGMP
    netif->igmp_mac_filter = os_igmp_mac_filter;
    netif->flags |= NETIF_FLAG_IGMP;
#endif

#if LWIP_IPV6
    netif->output_ip6             = ethip6_output;
    netif->ip6_autoconfig_enabled = 1;
    netif_create_ip6_linklocal_address(netif, 1);

#if LWIP_IPV6_MLD
    netif->flags |= NETIF_FLAG_MLD6;

    /*
     * For hardware/netifs that implement MAC filtering.
     * All-nodes link-local is handled by default, so we must let the hardware know
     * to allow multicast packets in.
     * Should set mld_mac_filter previously. */
    netif->mld_mac_filter = os_mld_mac_filter;
    if (netif->mld_mac_filter != NULL)
    {
        ip6_addr_t ip6_allnodes_ll;
        ip6_addr_set_allnodes_linklocal(&ip6_allnodes_ll);
        netif->mld_mac_filter(netif, &ip6_allnodes_ll, NETIF_ADD_MAC_FILTER);
    }
#endif /* LWIP_IPV6_MLD */
#endif /* LWIP_IPV6 */

    /* set interface up */
    netif_set_up(netif);

    if (OS_TRUE == net_dev->info.linkstatus)
    {
        netifapi_netif_set_link_up(netif);
    }

    return ERR_OK;
}

#define ONEOS_LWIP_L2_HEADER_PRINT 0
#if ONEOS_LWIP_L2_HEADER_PRINT
// l2 header dump func
static void os_l2header_debug_print(struct pbuf *p)
{
    char *type;
    char *da;
    char *sa;

    da   = ((char *)p->payload);
    sa   = ((char *)p->payload) + 6;
    type = ((char *)p->payload) + 12;
    LOG_I(ONEOS_LWIP_DBG, "Mac header:");
    LOG_I(ONEOS_LWIP_DBG,
          "DA: %02x:%02x:%02x:%02x:%02x:%02x",
          *da,
          *(da + 1),
          *(da + 2),
          *(da + 3),
          *(da + 4),
          *(da + 5));
    LOG_I(ONEOS_LWIP_DBG,
          "SA: %02x:%02x:%02x:%02x:%02x:%02x",
          *sa,
          *(sa + 1),
          *(sa + 2),
          *(sa + 3),
          *(sa + 4),
          *(sa + 5));
    LOG_I(ONEOS_LWIP_DBG, "type: 0x%02x%02x", *type, *(type + 1));

    return;
}

static err_t os_tcpip_input(struct pbuf *p, struct netif *inp)
{
    os_l2header_debug_print(p);
    return tcpip_input(p, inp);
}
#endif

static os_err_t os_lwip_ether_netif_add(struct netif *netif)
{
    os_err_t do_err = OS_FAILURE;

    do_err = netifapi_netif_add(netif,
#if LWIP_IPV4
                                OS_NULL,
                                OS_NULL,
                                OS_NULL,
#endif /* end of LWIP_IPV4 */
                                OS_NULL,
                                ether_if_init,
#if ONEOS_LWIP_L2_HEADER_PRINT
                                os_tcpip_input);
#else
                                tcpip_input);
#endif /* end of ONEOS_LWIP_L2_HEADER_PRINT */

    return do_err;
}

#ifdef LWIP_USING_BRIDGE
#include <netif/bridgeif.h>
#include <netif/ethernet.h>
static os_err_t os_lwip_bridge_netif_add(struct netif *netif)
{
    os_err_t              do_err = OS_FAILURE;
    struct os_lwip_info  *lwip_dev;
    struct os_net_device *net_dev;
    uint8_t              *mac;

    lwip_dev = (struct os_lwip_info *)netif;
    net_dev  = lwip_dev->net_dev;
    if (OS_NULL == net_dev)
    {
        LOG_E(ONEOS_LWIP_DBG, "why net_dev is null");
        return ERR_IF;
    }

    do_err = os_net_get_macaddr(net_dev, netif->hwaddr);
    if (OS_SUCCESS != do_err)
    {
        LOG_E(ONEOS_LWIP_DBG, "why get mac addr failed");
        return ERR_IF;
    }
    netif->hwaddr_len = 6;
    mac               = (uint8_t *)&netif->hwaddr;

    bridgeif_initdata_t bridge_initdata = BRIDGEIF_INITDATA2(3, 32, 32, mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
    if (netif_add_noaddr(netif, &bridge_initdata, bridgeif_init, ethernet_input) == NULL)
    {
        return do_err;
    }

#if LWIP_NETIF_HOSTNAME
    sprintf(netif->hostname, "netif_%02x%02x%d", netif->name[0], netif->name[1], netif->num);
#endif /* LWIP_NETIF_HOSTNAME */

#if LWIP_IPV6
    netif->ip6_autoconfig_enabled = 1;
    netif_create_ip6_linklocal_address(netif, 1);
#endif /* LWIP_IPV6 */

    return OS_SUCCESS;
}
#endif

#ifdef LWIP_USING_INTF_6LOWPAN_BLE
static err_t sixlowpan_bt_if_init(struct netif *netif)
{
    struct os_lwip_info  *lwip_dev;
    struct os_net_device *net_dev;
    os_err_t              do_err;

    lwip_dev = (struct os_lwip_info *)netif;
    net_dev  = lwip_dev->net_dev;
    if (OS_NULL == net_dev)
    {
        LOG_E(ONEOS_LWIP_DBG, "why net_dev is null");
        return ERR_IF;
    }

    rfc7668_if_init(netif);

    do_err = os_net_get_macaddr(net_dev, netif->hwaddr);
    if (OS_SUCCESS != do_err)
    {
        LOG_E(ONEOS_LWIP_DBG, "why get mac addr failed");
        return ERR_IF;
    }
    netif->hwaddr_len = 6;

#if LWIP_NETIF_HOSTNAME
    sprintf(netif->hostname, "netif_%02x%02x%d", netif->name[0], netif->name[1], netif->num);
#endif /* LWIP_NETIF_HOSTNAME */

#if LWIP_IPV6
    netif->ip6_autoconfig_enabled = 1;
    netif_create_ip6_linklocal_address(netif, 1);
#endif /* LWIP_IPV6 */

    /* set interface up */
    netif_set_up(netif);

    if (OS_TRUE == net_dev->info.linkstatus)
    {
        netifapi_netif_set_link_up(netif);
    }

    return ERR_OK;
}
#endif

static os_err_t os_lwip_sixlowpan_bt_netif_add(struct netif *netif)
{
    os_err_t do_err;

    do_err = OS_FAILURE;
#ifdef LWIP_USING_INTF_6LOWPAN_BLE
    do_err = netifapi_netif_add(netif,
#if LWIP_IPV4
                                OS_NULL,
                                OS_NULL,
                                OS_NULL,
#endif /* end of LWIP_IPV4 */
                                OS_NULL,
                                sixlowpan_bt_if_init,
                                rfc7668_input);
#endif /* end of LWIP_USING_INTF_6LOWPAN_BLE */

    return do_err;
}

static os_err_t os_lwip_netif_add(struct os_lwip_info *lwip_dev)
{
    struct os_net_device *net_dev;
    struct netif         *netif;
    os_err_t              do_err;

    netif   = (struct netif *)lwip_dev;
    net_dev = (struct os_net_device *)lwip_dev->net_dev;

    do_err = OS_FAILURE;
    if (net_dev_intf_bt == net_dev->info.intf_type)
    {
        do_err = os_lwip_sixlowpan_bt_netif_add(netif);
    }
    else if (net_dev_intf_ether == net_dev->info.intf_type)
    {
        do_err = os_lwip_ether_netif_add(netif);
    }
#ifdef LWIP_USING_BRIDGE
    else if (net_dev_intf_br == net_dev->info.intf_type)
    {
        do_err = os_lwip_bridge_netif_add(netif);
    }
#endif

    return do_err;
}

static void oneos_lwip_netif_add_porc(void)
{
    struct os_lwip_info *info;

    info = OS_NULL;

    os_lwip_lock();
    os_list_for_each_entry(info, &os_lwip_list, struct os_lwip_info, list)
    {
        if (OS_NULL != info->net_dev)
        {
            (void)os_lwip_netif_add(info);
        }
    }
    os_lwip_unlock();

    return;
}

static void oneos_tcpip_init_done_callback(void *arg)
{
    if (LWIP_INIT_PHASE_DOING == gs_lwip_init_phase)
    {
        oneos_lwip_netif_add_porc();
        gs_lwip_init_phase = LWIP_INIT_PHASE_END;
        LOG_I(ONEOS_LWIP_DBG,
              "lwIP-%d.%d.%d initialize succ",
              LWIP_VERSION_MAJOR,
              LWIP_VERSION_MINOR,
              LWIP_VERSION_REVISION);
    }
    else
    {
        LOG_E(ONEOS_LWIP_DBG, "why lwip phase is not phase doing");
    }

    return;
}

void sys_init(void)
{
    /* nothing on OneOS porting */
}

int os_lwip_system_init(void)
{
    if (LWIP_INIT_PHASE_DOING == gs_lwip_init_phase || LWIP_INIT_PHASE_END == gs_lwip_init_phase)
    {
        LOG_E(ONEOS_LWIP_DBG, "lwip system already init[phase=%d]", gs_lwip_init_phase);
        return 0;
    }

    /* set default netif to NULL */
    netif_default = OS_NULL;

    tcpip_init(oneos_tcpip_init_done_callback, OS_NULL);
    LOG_I(ONEOS_LWIP_DBG,
          "lwIP-%d.%d.%d initialize start",
          LWIP_VERSION_MAJOR,
          LWIP_VERSION_MINOR,
          LWIP_VERSION_REVISION);

    gs_lwip_init_phase = LWIP_INIT_PHASE_DOING;

    return 0;
}
OS_INIT_CALL(os_lwip_system_init, OS_INIT_LEVEL_COMPONENT, OS_INIT_SUBLEVEL_MIDDLE);

static char *os_lwip_get_dev_name(struct netif *netif)
{
    struct os_lwip_info *info = (struct os_lwip_info *)netif;

    return info->net_dev->dev.name;
}

os_err_t os_net_protocol_lwip_init(struct os_net_device *net_dev)
{
    struct os_lwip_info *lwip_dev;

#if LWIP_NETIF_HOSTNAME
    char *hostname;
#define LWIP_HOSTNAME_LEN 16

    lwip_dev = malloc(sizeof(struct os_lwip_info) + LWIP_HOSTNAME_LEN);
    hostname = (char *)lwip_dev + sizeof(struct os_lwip_info);
#else
    lwip_dev = malloc(sizeof(struct os_lwip_info));
#endif
    if (OS_NULL == lwip_dev)
    {
        return OS_FAILURE;
    }

    os_list_init(&lwip_dev->list);
    memset(&lwip_dev->netif, 0, sizeof(struct netif));

#if LWIP_NETIF_HOSTNAME
    memset(hostname, 0, LWIP_HOSTNAME_LEN);
    lwip_dev->netif.hostname = hostname;
#endif

    lwip_dev->netif.linkoutput = os_lwip_linkoutput;
    lwip_dev->net_dev          = net_dev;
    os_lwip_lock();
    os_list_add(&os_lwip_list, &lwip_dev->list);
    os_lwip_unlock();
    net_dev->protocol_ops = &protocol_ops;
    net_dev->userdata     = lwip_dev;

    if (LWIP_INIT_PHASE_END == gs_lwip_init_phase)
    {
        os_lwip_netif_add(lwip_dev);
    }

    return OS_SUCCESS;
}

os_err_t os_lwip_register_ipv4bind_cb(char *device_name, netif_ipv4addr_bind_fn v4addr_bind_cb)
{
    struct os_lwip_info *info;
    struct netif        *netif;
    os_err_t             rt;

    info = OS_NULL;
    rt   = OS_FAILURE;

    os_lwip_lock();
    os_list_for_each_entry(info, &os_lwip_list, struct os_lwip_info, list)
    {
        if (OS_NULL != info->net_dev && !strcmp(info->net_dev->dev.name, device_name))
        {
            netif = (struct netif *)info;
#if LWIP_IPV4
            netif->v4_bind_cb = v4addr_bind_cb;
#endif
            rt = OS_SUCCESS;
            break;
        }
    }
    os_lwip_unlock();

    return rt;
}

os_err_t os_lwip_netif_get_ipv4_info(char *device_name, ip4_addr_t *ip_addr, ip4_addr_t *gw_addr, ip4_addr_t *nm_addr)
{
    struct os_lwip_info *info;
    struct netif        *netif;
    os_err_t             rt;

    info = OS_NULL;
    rt   = OS_FAILURE;

    os_lwip_lock();
    os_list_for_each_entry(info, &os_lwip_list, struct os_lwip_info, list)
    {
        if (OS_NULL != info->net_dev && !strcmp(info->net_dev->dev.name, device_name))
        {
            netif = (struct netif *)info;
            if (OS_NULL != ip_addr)
            {
                ip_addr->addr = ip_2_ip4(&netif->ip_addr)->addr;
            }
            if (OS_NULL != gw_addr)
            {
                gw_addr->addr = ip_2_ip4(&netif->gw)->addr;
            }
            if (OS_NULL != nm_addr)
            {
                nm_addr->addr = ip_2_ip4(&netif->netmask)->addr;
            }
            rt = OS_SUCCESS;
            break;
        }
    }
    os_lwip_unlock();

    return rt;
}

os_err_t os_lwip_netif_get_mac(char *device_name, uint8_t *mac)
{
    struct os_lwip_info *info;
    os_err_t             rt;

    info = OS_NULL;
    rt   = OS_FAILURE;

    os_lwip_lock();
    os_list_for_each_entry(info, &os_lwip_list, struct os_lwip_info, list)
    {
        if (OS_NULL != info->net_dev && !strcmp(info->net_dev->dev.name, device_name))
        {
            rt = os_net_get_macaddr(info->net_dev, mac);
            break;
        }
    }
    os_lwip_unlock();

    return rt;
}

struct netif *os_lwip_get_netif_by_devname(char *device_name)
{
    struct os_lwip_info *info;
    struct netif        *netif;

    info  = OS_NULL;
    netif = OS_NULL;

    os_lwip_lock();
    if (LWIP_INIT_PHASE_END == gs_lwip_init_phase)
    {
        os_list_for_each_entry(info, &os_lwip_list, struct os_lwip_info, list)
        {
            if (OS_NULL != info->net_dev && !strcmp(info->net_dev->dev.name, device_name))
            {
                netif = (struct netif *)info;
                break;
            }
        }
    }
    os_lwip_unlock();

    return netif;
}

os_err_t os_net_protocol_lwip_deinit(struct os_net_device *net_dev)
{
    struct os_lwip_info *lwip_dev;

    lwip_dev = OS_NULL;
    if ((OS_NULL != net_dev) && (OS_NULL != net_dev->userdata))
    {
        lwip_dev = (struct os_lwip_info *)net_dev->userdata;
        os_lwip_lock();
        os_list_del(&lwip_dev->list);
        os_lwip_unlock();
        netifapi_netif_remove(&lwip_dev->netif);
        os_free(lwip_dev);
    }

    return OS_SUCCESS;
}

struct netif *os_lwip_get_default_netif(void)
{
    return netif_get_default();
}

static void set_if(char *netif_name, char *ip_addr, char *gw_addr, char *nm_addr)
{
    struct netif *netif;
    ip4_addr_t    ipaddr;
    ip4_addr_t    gateway;
    ip4_addr_t    netmask;

    if (strlen(netif_name) != 3)
    {
        os_kprintf("network interface name[%s] invalid", netif_name);
        return;
    }

    netif = netif_find(netif_name);
    if (OS_NULL == netif)
    {
        os_kprintf("network interface name[%s] not find", netif_name);
        return;
    }

    ip4addr_aton(ip_addr, &ipaddr);
    ip4addr_aton(gw_addr, &gateway);
    ip4addr_aton(nm_addr, &netmask);

    netifapi_netif_set_addr(netif, &ipaddr, &netmask, &gateway);

    return;
}

#ifdef OS_USING_SHELL
#include "shell.h"

static int set_if_cmd(int argc, char **argv)
{
    if (argc != 5)
    {
        os_kprintf("input error!\r\n"
                   "example:\r\n"
                   "set_if <device> <ipaddr> <gatway> <netmask>\r\n");
        return (-1);
    }

    set_if(argv[1], argv[2], argv[3], argv[4]);

    return 0;
}

SH_CMD_EXPORT(set_if, set_if_cmd, "set network interface address");

#if LWIP_DNS
void set_dns(uint8_t dns_num, char *dns_server)
{
    ip_addr_t addr;

    if ((dns_server != OS_NULL) && ipaddr_aton(dns_server, &addr))
    {
        dns_setserver(dns_num, &addr);
    }

    const ip_addr_t* addr_get = dns_getserver(dns_num);
    if (OS_NULL != addr_get)
        os_kprintf("lwip dns server num[%d]:[%s]\r\n", dns_num, ipaddr_ntoa(addr_get));

    return;
}

os_err_t set_dns_by_index(int argc, char **argv)
{
    /* useage: set_dns <index> <dns_ipaddr> */
    if (3 != argc || OS_NULL == argv)
    {
        // throw error
        os_kprintf("useage: set_dns <index> <dns_ipaddr>\r\n");
        return OS_INVAL;
    }

    /* judge the index validation */
    if(atoi(argv[1]) >= DNS_MAX_SERVERS){
        os_kprintf("attention: <index> must < %d\r\n",DNS_MAX_SERVERS);
        return OS_INVAL;
    }
    /* set return value */
    set_dns(atoi(argv[1]), argv[2]);

    return OS_SUCCESS;
}

SH_CMD_EXPORT(set_dns, set_dns_by_index, "set DNS server address");
#endif /* LWIP_DNS */

void lwip_ifconfig(void)
{
    struct os_net_device *net_dev;
    struct os_lwip_info  *lwip_dev;
    struct netif         *netif;
    os_ubase_t            index;

    os_schedule_lock();

    netif = netif_list;

    while (netif != OS_NULL)
    {
        os_kprintf("network interface: %c%c%d[%s]%s\r\n",
                   netif->name[0],
                   netif->name[1],
                   netif->num,
                   os_lwip_get_dev_name(netif),
                   (netif == netif_default) ? " (Default)" : "");
        os_kprintf("MTU: %d\r\n", netif->mtu);
        os_kprintf("MAC: ");
        for (index = 0; index < netif->hwaddr_len; index++)
            os_kprintf("%02x ", netif->hwaddr[index]);
        os_kprintf("\r\nFLAGS:");
        if (netif->flags & NETIF_FLAG_UP)
            os_kprintf(" UP");
        else
            os_kprintf(" DOWN");
        if (netif->flags & NETIF_FLAG_LINK_UP)
            os_kprintf(" LINK_UP");
        else
            os_kprintf(" LINK_DOWN");
        if (netif->flags & NETIF_FLAG_ETHARP)
            os_kprintf(" ETHARP");
        if (netif->flags & NETIF_FLAG_BROADCAST)
            os_kprintf(" BROADCAST");
        if (netif->flags & NETIF_FLAG_IGMP)
            os_kprintf(" IGMP");
        if (netif->flags & NETIF_FLAG_MLD6)
            os_kprintf(" MLD6");
        os_kprintf("\r\n");

        lwip_dev = (struct os_lwip_info *)netif;
        net_dev  = lwip_dev->net_dev;
        if (net_dev_intf_bt != net_dev->info.intf_type)
        {
            os_kprintf("ip address: %s\r\n", ipaddr_ntoa(&(netif->ip_addr)));
            os_kprintf("gw address: %s\r\n", ipaddr_ntoa(&(netif->gw)));
            os_kprintf("net mask  : %s\r\n", ipaddr_ntoa(&(netif->netmask)));
        }
#if LWIP_IPV6
        {
            ip6_addr_t *addr = OS_NULL;
            int         addr_state;
            int         i;

            addr       = (ip6_addr_t *)&netif->ip6_addr[0];
            addr_state = netif->ip6_addr_state[0];

            os_kprintf("\r\nipv6 link-local: %s state:%02X %s\r\n",
                       ip6addr_ntoa(addr),
                       addr_state,
                       ip6_addr_isvalid(addr_state) ? "VALID" : "INVALID");

            for (i = 1; i < LWIP_IPV6_NUM_ADDRESSES; i++)
            {
                addr       = (ip6_addr_t *)&netif->ip6_addr[i];
                addr_state = netif->ip6_addr_state[i];

                os_kprintf("ipv6[%d] address: %s state:%02X %s\r\n",
                           i,
                           ip6addr_ntoa(addr),
                           addr_state,
                           ip6_addr_isvalid(addr_state) ? "VALID" : "INVALID");
            }
        }
        os_kprintf("\r\n");
#endif /* LWIP_IPV6 */
        netif = netif->next;
    }

#if LWIP_DNS
    {
        const ip_addr_t *ip_addr;

        for (index = 0; index < DNS_MAX_SERVERS; index++)
        {
            ip_addr = dns_getserver(index);
            os_kprintf("dns server #%d: %s\r\n", index, ipaddr_ntoa(ip_addr));
        }
    }
#endif /* LWIP_DNS */

    os_schedule_unlock();
}

SH_CMD_EXPORT(lwip_ifconfig, lwip_ifconfig, "list network interface information");

#if LWIP_TCP
#include <lwip/priv/tcp_priv.h>
#include <lwip/tcp.h>

void list_tcps(void)
{
    uint32_t        num               = 0;
    struct tcp_pcb *pcb               = OS_NULL;
    char            local_ip_str[16]  = {0};
    char            remote_ip_str[16] = {0};

    extern struct tcp_pcb         *tcp_active_pcbs;
    extern struct tcp_pcb         *tcp_tw_pcbs;
    extern union tcp_listen_pcbs_t tcp_listen_pcbs;

    os_kprintf("Active PCB states:\r\n");
    os_schedule_lock();
    for (pcb = tcp_active_pcbs; pcb != NULL; pcb = pcb->next)
    {
        strcpy(local_ip_str, ipaddr_ntoa(&(pcb->local_ip)));
        strcpy(remote_ip_str, ipaddr_ntoa(&(pcb->remote_ip)));

        os_kprintf("#%d %s:%d <==> %s:%d snd_nxt 0x%08X rcv_nxt 0x%08X ",
                   num++,
                   local_ip_str,
                   pcb->local_port,
                   remote_ip_str,
                   pcb->remote_port,
                   pcb->snd_nxt,
                   pcb->rcv_nxt);
        os_kprintf("state: %s\r\n", tcp_debug_state_str(pcb->state));
    }

    os_kprintf("Listen PCB states:\r\n");
    num = 0;
    for (pcb = (struct tcp_pcb *)tcp_listen_pcbs.pcbs; pcb != NULL; pcb = pcb->next)
    {
        os_kprintf("#%d local port %d ", num++, pcb->local_port);
        os_kprintf("state: %s\r\n", tcp_debug_state_str(pcb->state));
    }

    os_kprintf("TIME-WAIT PCB states:\r\n");
    num = 0;
    for (pcb = tcp_tw_pcbs; pcb != NULL; pcb = pcb->next)
    {
        strcpy(local_ip_str, ipaddr_ntoa(&(pcb->local_ip)));
        strcpy(remote_ip_str, ipaddr_ntoa(&(pcb->remote_ip)));

        os_kprintf("#%d %s:%d <==> %s:%d snd_nxt 0x%08X rcv_nxt 0x%08X ",
                   num++,
                   local_ip_str,
                   pcb->local_port,
                   remote_ip_str,
                   pcb->remote_port,
                   pcb->snd_nxt,
                   pcb->rcv_nxt);
        os_kprintf("state: %s\r\n", tcp_debug_state_str(pcb->state));
    }
    os_schedule_unlock();
}

SH_CMD_EXPORT(list_tcps, list_tcps, "list all of tcp connections");
#endif /* LWIP_TCP */

#if LWIP_UDP
#include "lwip/udp.h"

void list_udps(void)
{
    struct udp_pcb *pcb = OS_NULL;
    uint32_t        num = 0;

    char local_ip_str[16]  = {0};
    char remote_ip_str[16] = {0};

    os_kprintf("Active UDP PCB states:\r\n");
    os_schedule_lock();
    for (pcb = udp_pcbs; pcb != NULL; pcb = pcb->next)
    {
        strcpy(local_ip_str, ipaddr_ntoa(&(pcb->local_ip)));
        strcpy(remote_ip_str, ipaddr_ntoa(&(pcb->remote_ip)));

        os_kprintf("#%d %d %s:%d <==> %s:%d \r\n",
                   num,
                   (int)pcb->flags,
                   local_ip_str,
                   pcb->local_port,
                   remote_ip_str,
                   pcb->remote_port);

        num++;
    }
    os_schedule_unlock();
}

SH_CMD_EXPORT(list_udps, list_udps, "list all of udp connections");
#endif /* LWIP_UDP */

#endif /* OS_USING_SHELL */
