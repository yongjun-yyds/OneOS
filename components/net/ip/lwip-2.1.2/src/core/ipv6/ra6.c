//参照rfc4861 page28, parpare Source Link-layer Address/Prefix Information/MTU
//

#include "lwip/nd6.h"
#include "lwip/priv/nd6_priv.h"
#include "lwip/prot/nd6.h"
#include "lwip/prot/icmp6.h"
#include "lwip/pbuf.h"
#include "lwip/ip6.h"
#include "lwip/ip6_addr.h"
#include "lwip/ip6_zone.h"
#include "lwip/inet_chksum.h"
#include "lwip/netif.h"
#include "lwip/timeouts.h"

#define RA_ROUND_UP(n, d) (((n) + (d) - 1) / (d))

static void router_advertisement_tmr(void *arg)
{
  struct ra_header *ra_hdr;
  struct lladdr_option *src_lladdr;
  struct prefix_option *prefix_opt;
  struct mtu_option *mtu_opt;
  ip6_addr_t multicast_address;
  const ip6_addr_t *src_addr;
  const ip6_addr_t *dest_addr;
  struct netif *netif;
  struct pbuf *p;
  int len;
  
  netif = (struct netif *)arg;

  len = (RA_ROUND_UP(sizeof(struct lladdr_option), 8) << 3) + (RA_ROUND_UP(sizeof(struct prefix_option), 8) << 3) + (RA_ROUND_UP(sizeof(struct mtu_option), 8) << 3);
  len += sizeof(struct ra_header);

  p = pbuf_alloc(PBUF_IP, len, PBUF_RAM);
  if (OS_NULL == p) 
  {
    os_kprintf("why pbuf_alloc failed");
    return;
  }

  /* Set fields. */
  ra_hdr = (struct ra_header *)p->payload;

  ra_hdr->type = ICMP6_TYPE_RA;
  ra_hdr->code = 0;
  ra_hdr->chksum = 0;
  
  ra_hdr->current_hop_limit = ND6_HOPLIM;
  ra_hdr->flags = 00;
  ra_hdr->router_lifetime = 9000;
  ra_hdr->reachable_time = 10;
  ra_hdr->retrans_timer = 10000;

  len = sizeof(struct ra_header);
  src_lladdr = (struct lladdr_option *)((unsigned char *)p->payload + len);

  src_lladdr->type = ND6_OPTION_TYPE_SOURCE_LLADDR;
  src_lladdr->length = RA_ROUND_UP(sizeof(struct lladdr_option), 8);
  SMEMCPY(src_lladdr->addr, netif->hwaddr, netif->hwaddr_len);

  len = sizeof(struct ra_header) + (RA_ROUND_UP(sizeof(struct lladdr_option), 8) << 3);
  prefix_opt = (struct prefix_option *)((unsigned char *)p->payload + len);

  prefix_opt->type = ND6_OPTION_TYPE_PREFIX_INFO;
  prefix_opt->length = RA_ROUND_UP(sizeof(struct prefix_option), 8);
  prefix_opt->prefix_length = 64;
  prefix_opt->flags = 0;
  prefix_opt->flags = ND6_PREFIX_FLAG_ON_LINK;
#if LWIP_IPV6_AUTOCONFIG
  prefix_opt->flags |= ND6_PREFIX_FLAG_AUTONOMOUS;
#endif
  prefix_opt->valid_lifetime = lwip_htonl(9000);
  prefix_opt->preferred_lifetime = lwip_htonl(8000);
  prefix_opt->reserved2[0] = 0;
  prefix_opt->reserved2[1] = 0;
  prefix_opt->reserved2[2] = 0;
  prefix_opt->site_prefix_length = 16;
  prefix_opt->prefix.addr[0] = netif_ip6_addr(netif, 1)->addr[0];
  prefix_opt->prefix.addr[1] = netif_ip6_addr(netif, 1)->addr[1];
  prefix_opt->prefix.addr[2] = 0x00;
  prefix_opt->prefix.addr[3] = 0x00;

  len = sizeof(struct ra_header) + (RA_ROUND_UP(sizeof(struct lladdr_option), 8) << 3) + (RA_ROUND_UP(sizeof(struct prefix_option), 8) << 3);
  mtu_opt = (struct mtu_option *)((unsigned char *)p->payload + len);

  mtu_opt->type = ND6_OPTION_TYPE_MTU;
  mtu_opt->length =  RA_ROUND_UP(sizeof(struct mtu_option), 8);
  mtu_opt->reserved = 0;
  mtu_opt->mtu = netif->mtu;

  ip6_addr_set_allnodes_linklocal(&multicast_address);
  ip6_addr_assign_zone(&multicast_address, IP6_MULTICAST, netif);
  dest_addr = &multicast_address;
  src_addr = netif_ip6_addr(netif, 0);

#if CHECKSUM_GEN_ICMP6
  IF__NETIF_CHECKSUM_ENABLED(netif, NETIF_CHECKSUM_GEN_ICMP6)
  {
    ra_hdr->chksum = ip6_chksum_pseudo(p, IP6_NEXTH_ICMP6, p->len, src_addr, dest_addr);
  }
#endif /* CHECKSUM_GEN_ICMP6 */

  /* Send the packet out. */
  ip6_output_if(p, src_addr, dest_addr, ND6_HOPLIM, 0, IP6_NEXTH_ICMP6, netif);

  pbuf_free(p);

  return;
}

void router_advertisement_start(struct netif *netif)
{
    sys_timeout(10000, router_advertisement_tmr, netif);
}

static struct netif *ra_netif[2];
static int ra_cnt = 0;
int ra6_netif_add(struct netif *netif)
{
  if (!ra_cnt)
  {
    ra_netif[0] = netif;
    ra_cnt++;
  }
  else
  {
    ra_netif[1] = netif;
    ra_cnt = 2;
  }

  return 0;
}

void ra6_tmr(void)
{
  struct netif *next;
    
  //return;
  if (ra_cnt == 1)
  {
      next = ra_netif[0];
      router_advertisement_tmr((void *)next);
  }
  else if (ra_cnt == 2)
  {
      next = ra_netif[0];
      router_advertisement_tmr((void *)next);
      next = ra_netif[1];
      router_advertisement_tmr((void *)next);
  }
}
