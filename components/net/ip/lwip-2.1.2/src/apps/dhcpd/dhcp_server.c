/*
 * File      : dhcp_server.c
 *             A simple DHCP server implementation
 *
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 */

#include <stdio.h>
#include <stdint.h>
#include <os_task.h>
#include <os_assert.h>

#include <lwip/opt.h>
#include <lwip/sockets.h>
#include <lwip/inet_chksum.h>
#include <lwip/netifapi.h>
#include <netif/etharp.h>
#include <lwip/ip.h>
#include <lwip/init.h>
#include <lwip/prot/udp.h>

#if (LWIP_VERSION) >= 0x02000000U
    #include <lwip/prot/dhcp.h>
#endif

//#define DHCP_DEBUG_PRINTF

#ifdef  DHCP_DEBUG_PRINTF
    #define DEBUG_PRINTF        os_kprintf("\r\n[DHCP] "); os_kprintf
#else
    #define DEBUG_PRINTF(...)
#endif /* DHCP_DEBUG_PRINTF */

/* we need some routines in the DHCP of lwIP */
#undef  LWIP_DHCP
#define LWIP_DHCP   1
#include <lwip/dhcp.h>

/* buffer size for receive DHCP packet */
#define BUFSZ               1024

#ifndef MAC_ADDR_LEN
    #define MAC_ADDR_LEN     6
#endif

#ifndef MAC_TABLE_LEN
    #define MAC_TABLE_LEN     4
#endif

/** MEMCPY-like macro to copy to/from struct eth_addr's that are local variables
 * or known to be 32-bit aligned within the protocol header. */
#ifndef ETHADDR32_COPY
#define ETHADDR32_COPY(dst, src)  SMEMCPY(dst, src, ETH_HWADDR_LEN)
#endif

/** MEMCPY-like macro to copy to/from struct eth_addr's that are no local
 * variables and known to be 16-bit aligned within the protocol header. */
#ifndef ETHADDR16_COPY
#define ETHADDR16_COPY(dst, src)  SMEMCPY(dst, src, ETH_HWADDR_LEN)
#endif

#if (LWIP_VERSION) >= 0x02000000U
    ip4_addr_t gs_dhcpd_server_addr;
#else
    struct ip_addr gs_dhcpd_server_addr;
#endif /* end of LWIP_VERSION */
static uint8_t gs_dhcpd_client_ip_min;
static uint8_t gs_dhcpd_client_ip_max;
static uint8_t gs_next_client_ip;

struct mac_addr_t
{
    uint8_t add[MAC_ADDR_LEN];
};

struct mac_ip_item_t
{
    struct mac_addr_t mac_addr;
    uint8_t ip_addr_3;
};

static os_err_t _low_level_dhcp_send(struct netif *netif,
                                     const void *buffer,
                                     os_size_t size)
{
    struct pbuf *p;
    struct eth_hdr *ethhdr;
    struct ip_hdr *iphdr;
    struct udp_hdr *udphdr;

    p = pbuf_alloc(PBUF_LINK,
                   SIZEOF_ETH_HDR + sizeof(struct ip_hdr)
                   + sizeof(struct udp_hdr) + size,
                   PBUF_RAM);
    if (p == OS_NULL) return OS_NOMEM;

    ethhdr = (struct eth_hdr *)p->payload;
    iphdr  = (struct ip_hdr *)((char *)ethhdr + SIZEOF_ETH_HDR);
    udphdr = (struct udp_hdr *)((char *)iphdr + sizeof(struct ip_hdr));

    ETHADDR32_COPY(&ethhdr->dest, (struct eth_addr *)&ethbroadcast);
    ETHADDR16_COPY(&ethhdr->src, netif->hwaddr);
    ethhdr->type = PP_HTONS(ETHTYPE_IP);

    iphdr->src.addr  = 0x00000000; /* src: 0.0.0.0 */
    iphdr->dest.addr = 0xFFFFFFFF; /* src: 255.255.255.255 */

    IPH_VHL_SET(iphdr, 4, IP_HLEN / 4);
    IPH_TOS_SET(iphdr, 0x00);
    IPH_LEN_SET(iphdr, htons(IP_HLEN + sizeof(struct udp_hdr) + size));
    IPH_ID_SET(iphdr, htons(2));
    IPH_OFFSET_SET(iphdr, 0);
    IPH_TTL_SET(iphdr, 255);
    IPH_PROTO_SET(iphdr, IP_PROTO_UDP);
    IPH_CHKSUM_SET(iphdr, 0);
    IPH_CHKSUM_SET(iphdr, inet_chksum(iphdr, IP_HLEN));

    udphdr->src = htons(DHCP_SERVER_PORT);
    udphdr->dest = htons(DHCP_CLIENT_PORT);
    udphdr->len = htons(sizeof(struct udp_hdr) + size);
    udphdr->chksum = 0;

    memcpy((char *)udphdr + sizeof(struct udp_hdr),
           buffer, size);

    netif->linkoutput(netif, p);
    pbuf_free(p);

    return OS_SUCCESS;
}

static uint8_t get_ip(struct mac_addr_t *p_mac_addr)
{
    static struct mac_ip_item_t mac_table[MAC_TABLE_LEN];
    struct mac_addr_t bad_mac;
    static int offset = 0;
    uint8_t ip_addr_3;
    int i;

    memset(&bad_mac, 0, sizeof(bad_mac));
    if (!memcmp(&bad_mac, p_mac_addr, sizeof(bad_mac)))
    {
        DEBUG_PRINTF("mac address all zero");
        ip_addr_3 = gs_dhcpd_client_ip_max;
        goto _return;
    }

    memset(&bad_mac, 0xFF, sizeof(bad_mac));
    if (!memcmp(&bad_mac, p_mac_addr, sizeof(bad_mac)))
    {
        DEBUG_PRINTF("mac address all one");
        ip_addr_3 = gs_dhcpd_client_ip_max;
        goto _return;
    }

    for (i = 0; i < MAC_TABLE_LEN; i++)
    {
        if (!memcmp(&mac_table[i].mac_addr, p_mac_addr, sizeof(bad_mac)))
        {
            //use old ip
            ip_addr_3 = mac_table[i].ip_addr_3;
            DEBUG_PRINTF("return old ip: %d", (int)ip_addr_3);
            goto _return;
        }
    }

    /* add new ip */
    mac_table[offset].mac_addr = *p_mac_addr;
    mac_table[offset].ip_addr_3  = gs_next_client_ip;
    ip_addr_3 = mac_table[offset].ip_addr_3 ;

    offset++;
    if (offset >= MAC_TABLE_LEN)
        offset = 0;

    gs_next_client_ip++;
    if (gs_next_client_ip > gs_dhcpd_client_ip_max)
        gs_next_client_ip = gs_dhcpd_client_ip_min;

    DEBUG_PRINTF("create new ip: %d", (int)ip_addr_3);
    DEBUG_PRINTF("next_client_ip %d", gs_next_client_ip);

_return:
    return ip_addr_3;
}

static void dhcpd_thread_entry(void *parameter)
{
    struct netif *netif = OS_NULL;
    int sock;
    int bytes_read;
    char *recv_data;
    uint32_t addr_len;
    struct sockaddr_in server_addr, client_addr;
    struct dhcp_msg *msg;
    int optval = 1;
    struct mac_addr_t mac_addr;
    uint8_t DHCPD_SERVER_IPADDR0, DHCPD_SERVER_IPADDR1, DHCPD_SERVER_IPADDR2, DHCPD_SERVER_IPADDR3;

    /* get ethernet interface. */
    netif = (struct netif *) parameter;
    OS_ASSERT(netif != OS_NULL);

    /* our DHCP server information */
    {
        DHCPD_SERVER_IPADDR0 = (ntohl(gs_dhcpd_server_addr.addr) >> 24) & 0xFF;
        DHCPD_SERVER_IPADDR1 = (ntohl(gs_dhcpd_server_addr.addr) >> 16) & 0xFF;
        DHCPD_SERVER_IPADDR2 = (ntohl(gs_dhcpd_server_addr.addr) >>  8) & 0xFF;
        DHCPD_SERVER_IPADDR3 = (ntohl(gs_dhcpd_server_addr.addr) >>  0) & 0xFF;
    }
    DEBUG_PRINTF("DHCP server IP: %d.%d.%d.%d  client IP: %d.%d.%d.%d-%d",
                 DHCPD_SERVER_IPADDR0, DHCPD_SERVER_IPADDR1,
                 DHCPD_SERVER_IPADDR2, DHCPD_SERVER_IPADDR3,
                 DHCPD_SERVER_IPADDR0, DHCPD_SERVER_IPADDR1,
                 DHCPD_SERVER_IPADDR2, gs_dhcpd_client_ip_min, gs_dhcpd_client_ip_max);

    /* allocate buffer for receive */
    recv_data = malloc(BUFSZ);
    if (OS_NULL == recv_data)
    {
        /* No memory */
        DEBUG_PRINTF("Out of memory");
        return;
    }

    /* create a socket with UDP */
    if ((sock = lwip_socket(AF_INET, SOCK_DGRAM, 0)) == -1)
    {
        DEBUG_PRINTF("create socket failed, errno = %d", errno);
        free(recv_data);
        return;
    }

    /* set to receive broadcast packet */
    lwip_setsockopt(sock, SOL_SOCKET, SO_BROADCAST, &optval, sizeof(optval));

    /* initialize server address */
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(DHCP_SERVER_PORT);
    server_addr.sin_addr.s_addr = INADDR_ANY;
    memset(&(server_addr.sin_zero), 0, sizeof(server_addr.sin_zero));

    /* bind socket to the server address */
    if (lwip_bind(sock, (struct sockaddr *)&server_addr,
             sizeof(struct sockaddr)) == -1)
    {
        /* bind failed. */
        DEBUG_PRINTF("bind server address failed, errno=%d", errno);
        lwip_close(sock);
        free(recv_data);
        return;
    }

    addr_len = sizeof(struct sockaddr);
    DEBUG_PRINTF("DHCP server listen on port %d...", DHCP_SERVER_PORT);

    while (1)
    {
        bytes_read = lwip_recvfrom(sock, recv_data, BUFSZ - 1, 0,
                              (struct sockaddr *)&client_addr, (socklen_t *)&addr_len);
        if (bytes_read <= 0)
        {
            lwip_close(sock);
            free(recv_data);
            return;
        }
        else if (bytes_read < DHCP_MSG_LEN)
        {
            DEBUG_PRINTF("packet too short, wait for next!");
            continue;
        }

        msg = (struct dhcp_msg *)recv_data;
        /* check message type to make sure we can handle it */
        if ((msg->op != DHCP_BOOTREQUEST) || (msg->cookie != PP_HTONL(DHCP_MAGIC_COOKIE)))
        {
            continue;
        }

        memcpy(mac_addr.add, msg->chaddr, MAC_ADDR_LEN);

        /* handler. */
        {
            uint8_t *dhcp_opt;
            uint8_t option;
            uint8_t length;

            uint8_t message_type = 0;
            uint8_t finished = 0;
            uint32_t request_ip  = 0;

            uint8_t client_ip_3;

            client_ip_3 = get_ip(&mac_addr);

            dhcp_opt = (uint8_t *)msg + DHCP_OPTIONS_OFS;
            while (finished == 0)
            {
                option = *dhcp_opt;
                length = *(dhcp_opt + 1);

                switch (option)
                {
                case DHCP_OPTION_REQUESTED_IP:
                    request_ip = *(dhcp_opt + 2) << 24 | *(dhcp_opt + 3) << 16
                                 | *(dhcp_opt + 4) << 8 | *(dhcp_opt + 5);
                    break;

                case DHCP_OPTION_END:
                    finished = 1;
                    break;

                case DHCP_OPTION_MESSAGE_TYPE:
                    message_type = *(dhcp_opt + 2);
                    break;

                default:
                    break;
                } /* switch(option) */

                dhcp_opt += (2 + length);
            }

            /* reply. */
            dhcp_opt = (uint8_t *)msg + DHCP_OPTIONS_OFS;

            /* check. */
            if (request_ip)
            {
                uint32_t client_ip = DHCPD_SERVER_IPADDR0 << 24 | DHCPD_SERVER_IPADDR1 << 16
                                     | DHCPD_SERVER_IPADDR2 << 8 | client_ip_3;

                DEBUG_PRINTF("message_type: %d, request_ip: %08X, client_ip: %08X.", message_type, request_ip, client_ip);

                if (request_ip != client_ip)
                {
                    //OPTION->DHCP MESSAGE TYPE
                    *dhcp_opt++ = DHCP_OPTION_MESSAGE_TYPE;
                    *dhcp_opt++ = DHCP_OPTION_MESSAGE_TYPE_LEN;
                    *dhcp_opt++ = DHCP_NAK;

                    //OPTION->DHCP SERVER ID
                    *dhcp_opt++ = DHCP_OPTION_SERVER_ID;
                    *dhcp_opt++ = 4;
                    *dhcp_opt++ = DHCPD_SERVER_IPADDR0;
                    *dhcp_opt++ = DHCPD_SERVER_IPADDR1;
                    *dhcp_opt++ = DHCPD_SERVER_IPADDR2;
                    *dhcp_opt++ = DHCPD_SERVER_IPADDR3;

                    //OPTION->DHCP MSG
#define DHCPD_DEF_ERROR_MSG "Invalid Request Message"
                    uint8_t len;
                    len = strlen(DHCPD_DEF_ERROR_MSG);

                    *dhcp_opt++ = DHCP_OPTION_MSG;
                    *dhcp_opt++ = len;
                    memcpy(dhcp_opt, DHCPD_DEF_ERROR_MSG, len);
                    dhcp_opt += len;

                    //OPTION->END
                    *dhcp_opt++ = DHCP_OPTION_END;

                    DEBUG_PRINTF("requested IP invalid, reply DHCP_NAK");

                    if (netif != OS_NULL)
                    {
                        int send_byte = (dhcp_opt - (uint8_t *)msg);
                        _low_level_dhcp_send(netif, msg, send_byte);
                        DEBUG_PRINTF("DHCP server send %d byte", send_byte);
                    }

                    continue;
                }
            }

            if (message_type == DHCP_DISCOVER)
            {
                DEBUG_PRINTF("request DHCP_DISCOVER");
                DEBUG_PRINTF("reply   DHCP_OFFER");

                // DHCP_OPTION_MESSAGE_TYPE
                *dhcp_opt++ = DHCP_OPTION_MESSAGE_TYPE;
                *dhcp_opt++ = DHCP_OPTION_MESSAGE_TYPE_LEN;
                *dhcp_opt++ = DHCP_OFFER;

                // DHCP_OPTION_SERVER_ID
                *dhcp_opt++ = DHCP_OPTION_SERVER_ID;
                *dhcp_opt++ = 4;
                *dhcp_opt++ = DHCPD_SERVER_IPADDR0;
                *dhcp_opt++ = DHCPD_SERVER_IPADDR1;
                *dhcp_opt++ = DHCPD_SERVER_IPADDR2;
                *dhcp_opt++ = DHCPD_SERVER_IPADDR3;

                // DHCP_OPTION_LEASE_TIME
                *dhcp_opt++ = DHCP_OPTION_LEASE_TIME;
                *dhcp_opt++ = 4;
                *dhcp_opt++ = 0x00;
                *dhcp_opt++ = 0x01;
                *dhcp_opt++ = 0x51;
                *dhcp_opt++ = 0x80;
            }
            else if (message_type == DHCP_REQUEST)
            {
                DEBUG_PRINTF("request DHCP_REQUEST");
                DEBUG_PRINTF("reply   DHCP_ACK");

                // DHCP_OPTION_MESSAGE_TYPE
                *dhcp_opt++ = DHCP_OPTION_MESSAGE_TYPE;
                *dhcp_opt++ = DHCP_OPTION_MESSAGE_TYPE_LEN;
                *dhcp_opt++ = DHCP_ACK;

                // DHCP_OPTION_SERVER_ID
                *dhcp_opt++ = DHCP_OPTION_SERVER_ID;
                *dhcp_opt++ = 4;
                *dhcp_opt++ = DHCPD_SERVER_IPADDR0;
                *dhcp_opt++ = DHCPD_SERVER_IPADDR1;
                *dhcp_opt++ = DHCPD_SERVER_IPADDR2;
                *dhcp_opt++ = DHCPD_SERVER_IPADDR3;

                // DHCP_OPTION_SUBNET_MASK
                *dhcp_opt++ = DHCP_OPTION_SUBNET_MASK;
                *dhcp_opt++ = 4;
                *dhcp_opt++ = 0xFF;
                *dhcp_opt++ = 0xFF;
                *dhcp_opt++ = 0xFF;
                *dhcp_opt++ = 0x00;

#ifdef DHCPD_USING_ROUTER
                // DHCP_OPTION_ROUTER
                *dhcp_opt++ = DHCP_OPTION_ROUTER;
                *dhcp_opt++ = 4;
                *dhcp_opt++ = DHCPD_SERVER_IPADDR0;
                *dhcp_opt++ = DHCPD_SERVER_IPADDR1;
                *dhcp_opt++ = DHCPD_SERVER_IPADDR2;
                *dhcp_opt++ = 1;
#endif

                // DHCP_OPTION_DNS_SERVER, use the default DNS server address in lwIP
                *dhcp_opt++ = DHCP_OPTION_DNS_SERVER;
                *dhcp_opt++ = 4;

#ifndef DHCP_DNS_SERVER_IP
                *dhcp_opt++ = DHCPD_SERVER_IPADDR0;
                *dhcp_opt++ = DHCPD_SERVER_IPADDR1;
                *dhcp_opt++ = DHCPD_SERVER_IPADDR2;
                *dhcp_opt++ = 1;
#else
                {
#if (LWIP_VERSION) >= 0x02000000U
                    ip4_addr_t dns_addr;
#else
                    struct ip_addr dns_addr;
#endif /* LWIP_VERSION */
                    ip4addr_aton(DHCP_DNS_SERVER_IP, &dns_addr);

                    *dhcp_opt++ = (ntohl(dns_addr.addr) >> 24) & 0xFF;
                    *dhcp_opt++ = (ntohl(dns_addr.addr) >> 16) & 0xFF;
                    *dhcp_opt++ = (ntohl(dns_addr.addr) >>  8) & 0xFF;
                    *dhcp_opt++ = (ntohl(dns_addr.addr) >>  0) & 0xFF;
                }
#endif

                // DHCP_OPTION_LEASE_TIME
                *dhcp_opt++ = DHCP_OPTION_LEASE_TIME;
                *dhcp_opt++ = 4;
                *dhcp_opt++ = 0x00;
                *dhcp_opt++ = 0x01;
                *dhcp_opt++ = 0x51;
                *dhcp_opt++ = 0x80;
            }
            else
            {
                DEBUG_PRINTF("un handle message:%d", message_type);
            }

            // append DHCP_OPTION_END
            *dhcp_opt++ = DHCP_OPTION_END;

            /* send reply. */
            if ((message_type == DHCP_DISCOVER) || (message_type == DHCP_REQUEST))
            {
                msg->op = DHCP_BOOTREPLY;
                IP4_ADDR(&msg->yiaddr,
                         DHCPD_SERVER_IPADDR0, DHCPD_SERVER_IPADDR1,
                         DHCPD_SERVER_IPADDR2, client_ip_3);

                client_addr.sin_addr.s_addr = INADDR_BROADCAST;

                if (netif != OS_NULL)
                {
                    int send_byte = (dhcp_opt - (uint8_t *)msg);
                    _low_level_dhcp_send(netif, msg, send_byte);
                    DEBUG_PRINTF("DHCP server send %d byte", send_byte);
                }
            }
        } /* handler. */
    }
}

void dhcpd_start(struct netif *netif, const char *dhcpd_server_ip, uint8_t dhcpd_client_ip_min, uint8_t dhcpd_client_ip_max)
{
    int rc;

    if (OS_NULL == netif)
    {
        DEBUG_PRINTF("in dhcpd_start func netif param is NULL, invalid");
        return;
    }

    if (OS_NULL == dhcpd_server_ip || strlen(dhcpd_server_ip) > 15)
    {
        DEBUG_PRINTF("dhcpd_server_ip[%s] invalid", (OS_NULL == dhcpd_server_ip) ? "NULL": dhcpd_server_ip);
        return;
    }

#if (LWIP_VERSION) >= 0x02000000U
    rc = ip4addr_aton(dhcpd_server_ip, &gs_dhcpd_server_addr);
#else
    rc = ipaddr_aton(dhcpd_server_ip, &gs_dhcpd_server_addr);
#endif /* end of LWIP_VERSION */
    if (!rc)
    {
        DEBUG_PRINTF("dhcpd_server_ip[%s] invalid", dhcpd_server_ip);
        return;
    }

    if (dhcpd_client_ip_min >= dhcpd_client_ip_max || dhcpd_client_ip_min < 2 || dhcpd_client_ip_max > 254)
    {
        DEBUG_PRINTF("dhcpd_client_ip_min[%d] or dhcpd_client_ip_max[%d] invalid", dhcpd_client_ip_min, dhcpd_client_ip_max);
        return;
    }

    gs_dhcpd_client_ip_min = dhcpd_client_ip_min;
    gs_dhcpd_client_ip_max = dhcpd_client_ip_max;
    gs_next_client_ip = gs_dhcpd_client_ip_min;

    sys_thread_new("dhcpd",
                            dhcpd_thread_entry,
                            netif,
                            1024,
                            OS_TASK_PRIORITY_MAX - 3);

    return;
}

