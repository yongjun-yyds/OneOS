# tcpdump工具

## 简介

tcpdump：嵌入式设备捕获IP报文的工具， 抓包数据以文件形式保存在文件系统中，可通过tftp工具导出到pc， 进而使用wireshark 软件分析数据

## 依赖

* 使能optparse
```C
(Top) → Components→ Optparse
                                                                                          OneOS Configuration
[*] Optparse: portable, reentrant, embeddable, getopt-like option parser
[ ]     Enable Optparse test demo
```


* 使能文件系统
```C
(Top) → Components→ FileSystem
                                                                                          OneOS Configuration
[*] Enable virtual file system
(4)     The max number of mounted file system
(2)     The max number of file system type
(16)    The max number of opened files
[*]     Enable auto mount file system
[ ]     Enable DevFS file system
[ ]     Enable CuteFs file system
[ ]     Enable JFFS2
[ ]     Enable Yaffs2 file system
[*]     Enable FatFs
            Elm-ChaN's FatFs, generic FAT filesystem module  --->
[ ]     Enable NFS v3 client file system
[ ]     Enable little filesystem
```

* 使能tftp
```C
(Top) → Components→ Network→ TCP/IP→ LwIP
    ↑↑↑↑↑↑↑↑↑↑↑↑↑↑                     OneOS Configuration
[*]     Enable alloc ip address through DHCP
(1)         SOF broadcast
(1)         SOF broadcast recv
[*]     Enable DHCP server
[ ]     Enable tcpdump features
[*]     Enable tftp features
[*]     Enable ping features	
```

## 功能使用介绍

### tcpdump工具使能

```C
(Top) → Components→ Network→ TCP/IP→ LwIP
    ↑↑↑↑↑↑↑↑↑↑↑↑↑↑                      OneOS Configuration
[*]     Enable alloc ip address through DHCP
(1)         SOF broadcast
(1)         SOF broadcast recv
[*]     Enable DHCP server
[*]     Enable tcpdump features
[*]     Enable tftp features
[*]     Enable ping features
```

以上配置完成后退出并保存配置，编译运行OneOS

### tcpdump工具使用介绍

#### tcpdump命令参数介绍

```
-i: 指定监听的网络接口
-m: 选择保存模式（文件系统）
-w: 用户指定的文件名 xx.pcap
-p: 停止抓包
-h: 帮助信息
```

#### 设备环境确认

* shell命令确认板子网络是否正常
```C
sh />lwip_ifconfig
network interface: e00[eth] (Default)
MTU: 1500
MAC: 00 80 e1 08 45 1e 
FLAGS: UP LINK_UP ETHARP BROADCAST MLD6
ip address: 192.168.1.100
gw address: 192.168.1.1
net mask  : 255.255.255.0

ipv6 link-local: FE80::280:E1FF:FE08:451E state:30 VALID
ipv6[1] address: :: state:00 INVALID
ipv6[2] address: :: state:00 INVALID

dns server #0: 192.168.61.200
dns server #1: 172.22.206.1
sh />
```
* 查询板子的网络设备
```C
sh />device
device                  type         ref count
--------------- -------------------------------
filesystem      Block Device         1       
touch           Touch Device         0       
eth             Network Interface    0       
audio1          Sound Device         0       
audio0          Sound Device         0       
lcd             Graphic Device       0       
adc2            Miscellaneous Device 0       
adc1            Miscellaneous Device 0       
pin_1000        Miscellaneous Device 0       
sai_BlockB1     Sound Device         1       
sai_BlockA1     Sound Device         1       
iwdg1           Miscellaneous Device 0       
rtc             RTC                  0       
sfbus.qspi      Character Device     1       
dac1            Miscellaneous Device 0       
tim14           ClockEvent Device    0       
tim1            ClockEvent Device    1       
```

#### 抓包示例

* shell输入命令: tcpdump -ieth -mfile -wtext1.pcap
```C
sh />tcpdump -ieth -mfile -wtext1.pcap
[TCPDUMP]select  [eth] network interface
[TCPDUMP]select  [file-system] mode
[TCPDUMP]save in [text1.pcap]
into tcpdump_thread_entry...
sh />
```

* 抓ping包示例
 ```C
sh />lwip_ping www.baidu.com
36 bytes from 120.232.145.185 icmp_seq=0 ttl=51 time=82 ms
36 bytes from 120.232.145.185 icmp_seq=1 ttl=51 time=37 ms
36 bytes from 120.232.145.185 icmp_seq=2 ttl=51 time=37 ms
36 bytes from 120.232.145.185 icmp_seq=3 ttl=51 time=0 ms

Result: from 120.232.145.185, sent=4, recv=4, lose=0, minttl=51, maxttl=51
sh />
 ```
* 停止抓包，并查看抓包文件
  1）shell 命令输入"tcpdump -p",停止抓包，退出抓包线程
  2）ls命令查看text1.pcap文件，然后使用tftp命令导出文件即可
  
  ```C
	sh />ls
	Directory /:
	$                       <DIR>                    
	text1.pcap              1147                     
	sh />
  ```

### tftp命令导出数据包文件

* 设备端启动tftp服务
```C
sh />tftp_server
TFTP server start successfully.
sh />
```
* pc端操作
  
  1) 打开Windows PowerShell(或者cmd)，使用tftp命令下载设备端文件
     tftp命令详细如下：
	
	```C
	PS C:\Users\15> tftp
	
	向运行 TFTP 服务的远程计算机传入或从该计算机传出文件。
	
	TFTP [-i] host [GET | PUT] source [destination]
	
	  -i              指定二进制映像传输模式(也称为
	                  八进制)。在二进制映像模式中，逐字节地
	                  移动文件。在传输二进制文件时，
	                  使用此模式。
	  host            指定本地或远程主机。
	  GET             将远程主机上的文件目标传输到
	                  本地主机的文件源中。
	  PUT             将本地主机上的文件源传输到
	                  远程主机上的文件目标。
	  source          指定要传输的文件。
	  destination     指定要将文件传输到的位置。
	
	PS C:\Users\15>
  ```
  2)从tftp服务端(设备端)下载文件到pc本地
    命令如下：tftp -i 192.168.1.100 GET /text1.pcap e:\text1.pcap
  
  ```C
   PS C:\Users\15> tftp -i 192.168.1.100 GET /text1.pcap e:\text1.pcap                    传输成功: 1 秒 1147 字节，1147 字节/秒
   PS C:\Users\15> 
  ```

