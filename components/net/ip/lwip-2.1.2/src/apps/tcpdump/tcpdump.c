/*
 * Copyright (c) 2006-2022, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-07-13     never        the first version
 */

/**
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 * 
 * @brief adapte tcpdump tool to OneOS Operating system.
 *
 * @Change Logs
 * Date       Author          Notes
 * 2023-6-8   H-15            the first version
 */

#define OPTPARSE_IMPLEMENTATION
#define OPTPARSE_API static
#include "optparse.h"
#include <os_mb.h>
#include <os_errno.h>
#include <string.h>
#include "os_types.h"
#include "os_stddef.h"
#include "os_memory.h"
#include "os_clock.h"
#include "os_assert.h"
#include <oneos_lwip.h>

#define TCPDUMP_SUCCESS             (0)
#define TCPDUMP_FAILURE             (-1)

#define TCPDUMP_PIPE_DEVICE         ("urdbd")           /* rdb pipe */

#define TCPDUMP_DEFAULT_NANE        ("sample.pcap")

#define TCPDUMP_MAX_MSG             (10)
#define PCAP_FILE_HEADER_SIZE       (24)
#define PCAP_PKTHDR_SIZE            (16)

#define PCAP_FILE_ID                (0xA1B2C3D4)
#define PCAP_VERSION_MAJOR          (0x200)
#define PCAP_VERSION_MINOR          (0x400)
#define GREENWICH_MEAN_TIME         (0)
#define PRECISION_OF_TIME_STAMP     (0)
#define MAX_LENTH_OF_CAPTURE_PKG    (0xFFFF)

#define LINKTYPE_NULL               (0)
#define LINKTYPE_ETHERNET           (1)                 /* also for 100Mb and up */
#define LINKTYPE_EXP_ETHERNET       (2)                 /* 3Mb experimental Ethernet */
#define LINKTYPE_AX25               (3)
#define LINKTYPE_PRONET             (4)
#define LINKTYPE_CHAOS              (5)
#define LINKTYPE_TOKEN_RING         (6)                 /* DLT_IEEE802 is used for Token Ring */
#define LINKTYPE_ARCNET             (7)
#define LINKTYPE_SLIP               (8)
#define LINKTYPE_PPP                (9)
#define LINKTYPE_FDDI               (10)
#define LINKTYPE_PPP_HDLC           (50)                /* PPP in HDLC-like framing */
#define LINKTYPE_PPP_ETHER          (51)                /* NetBSD PPP-over-Ethernet */
#define LINKTYPE_ATM_RFC1483        (100)               /* LLC/SNAP-encapsulated ATM */
#define LINKTYPE_RAW                (101)               /* raw IP */
#define LINKTYPE_SLIP_BSDOS         (102)               /* BSD/OS SLIP BPF header */
#define LINKTYPE_PPP_BSDOS          (103)               /* BSD/OS PPP BPF header */
#define LINKTYPE_C_HDLC             (104)               /* Cisco HDLC */
#define LINKTYPE_IEEE802_11         (105)               /* IEEE 802.11 (wireless) */
#define LINKTYPE_ATM_CLIP           (106)               /* Linux Classical IP over ATM */
#define LINKTYPE_LOOP               (108)               /* OpenBSD loopback */
#define LINKTYPE_LINUX_SLL          (113)               /* Linux cooked socket capture */
#define LINKTYPE_LTALK              (114)               /* Apple LocalTalk hardware */
#define LINKTYPE_ECONET             (115)               /* Acorn Econet */
#define LINKTYPE_CISCO_IOS          (118)               /* For Cisco-internal use */
#define LINKTYPE_PRISM_HEADER       (119)               /* 802.11+Prism II monitor mode */
#define LINKTYPE_AIRONET_HEADER     (120)               /* FreeBSD Aironet driver stuff */

#define SHELL_CMD ("phi::m::w::")                         /* [-p] [-h] [-i] [-m] [-w] */
#define STRCMP(a, R, b)   (strcmp((a), (b)) R 0)

#define PACP_FILE_HEADER_CREATE(_head)                          \
do {                                                            \
    (_head)->magic = PCAP_FILE_ID;                              \
    (_head)->version_major = PCAP_VERSION_MAJOR;                \
    (_head)->version_minor = PCAP_VERSION_MINOR;                \
    (_head)->thiszone = GREENWICH_MEAN_TIME;                    \
    (_head)->sigfigs = PRECISION_OF_TIME_STAMP;                 \
    (_head)->snaplen = MAX_LENTH_OF_CAPTURE_PKG;                \
    (_head)->linktype = LINKTYPE_ETHERNET;                      \
} while (0)

#define PACP_ZERO_PKTHDR_CREATE(_head, _len)                    \
do{                                                             \
    (_head)->ts.tv_sec = 0;                                     \
    (_head)->ts.tv_usec = 0;                                    \
    (_head)->caplen = _len;                                     \
    (_head)->len = _len;                                        \
} while (0)

#define PACP_PKTHDR_CREATE(_head, _p)                           \
do{                                                             \
    (_head)->ts.tv_sec = _p->tick / 1000;                       \
    (_head)->ts.tv_usec = (_p->tick % 1000) * 1000;             \
    (_head)->caplen = _p->tot_len;                              \
    (_head)->len = _p->tot_len;                                 \
} while (0)

struct tcpdump_buf
{
    unsigned short tot_len;
    os_tick_t tick;
    void *buf;
};

struct pcap_file_header
{
    uint32_t magic;
    uint16_t version_major;
    uint16_t version_minor;
    int32_t thiszone;
    uint32_t sigfigs;
    uint32_t snaplen;
    uint32_t linktype;
};

struct _timeval
{
    uint32_t tv_sec;
    uint32_t tv_usec;
};

struct pcap_pkthdr
{
    struct _timeval ts;
    uint32_t caplen;
    uint32_t len;
};

enum tcpdump_return_param
{
    STOP = -2,
    HELP = -3,
};

static struct os_device_t *tcpdump_pipe;
static os_mailbox_id tcpdump_mb;

static struct netif *netif;
static netif_linkoutput_fn link_output;
static netif_input_fn input;

static const char *name;
static char *filename;
static const char *eth;
static char *ethname;
static const char *mode;
static int fd = -1;

static void tcpdump_filename_del(void);
static void tcpdump_ethname_del(void);
static void tcpdump_error_info_deal(void);
static void tcpdump_init_indicate(void);
static int tcpdump_pcap_file_save(const void *buf, int len);

typedef int (*tcpdump_write_fn)(const void *buf, int len);
static tcpdump_write_fn tcpdump_write;

// #define PKG_NETUTILS_TCPDUMP_PRINT

#ifdef  PKG_NETUTILS_TCPDUMP_PRINT
#define __is_print(ch) ((unsigned int)((ch) - ' ') < 127u - ' ')
static void _hex_dump(const uint8_t *ptr, size_t buflen)
{
    unsigned char *buf = (unsigned char *)ptr;
    int i, j;

    OS_ASSERT(ptr != OS_NULL);

    for (i = 0; i < buflen; i += 16)
    {
        os_kprintf("%08X: ", i);

        for (j = 0; j < 16; j++)
            if (i + j < buflen)
                os_kprintf("%02X ", buf[i + j]);
            else
                os_kprintf("   ");
        os_kprintf(" ");

        for (j = 0; j < 16; j++)
            if (i + j < buflen)
                os_kprintf("%c", __is_print(buf[i + j]) ? buf[i + j] : '.');
        os_kprintf("\r\n");
    }
}
#endif

/* get tx data */
static err_t _netif_linkoutput(struct netif *netif, struct pbuf *p)
{
    // pbuf->payload + sizeof(struct tcpdump_buf)
    struct tcpdump_buf *tbuf = (struct tcpdump_buf *)os_malloc(p->tot_len+sizeof(struct tcpdump_buf));

    OS_ASSERT(tbuf != OS_NULL);
    OS_ASSERT(netif != OS_NULL);

    tbuf->tick = os_tick_get_value();
    tbuf->buf = ((uint8_t *)tbuf) + sizeof(struct tcpdump_buf);
    tbuf->tot_len = p->tot_len;
    pbuf_copy_partial(p, tbuf->buf, p->tot_len, 0);

    if (p != OS_NULL)
    {
		if(OS_SUCCESS != os_mailbox_send(tcpdump_mb, (int32_t)tbuf, OS_WAIT_FOREVER))
        {
            os_free(tbuf);
        }
    }
    return link_output(netif, p);
}

/* get rx data */
static err_t _netif_input(struct pbuf *p, struct netif *inp)
{
    // pbuf->payload + sizeof(struct tcpdump_buf)
    struct tcpdump_buf *tbuf = (struct tcpdump_buf *)os_malloc(p->tot_len+sizeof(struct tcpdump_buf));

    OS_ASSERT(tbuf != OS_NULL);
    OS_ASSERT(netif != OS_NULL);

    tbuf->tick = os_tick_get_value();
    tbuf->buf = ((uint8_t *)tbuf) + sizeof(struct tcpdump_buf);
    tbuf->tot_len = p->tot_len;
    pbuf_copy_partial(p, tbuf->buf, p->tot_len, 0);

    if (p != OS_NULL)
    {
        if(OS_SUCCESS != os_mailbox_send(tcpdump_mb, (int32_t)tbuf, OS_WAIT_FOREVER))
        {
            os_free(tbuf);
        }
    }
    return input(p, inp);
}

/* import pcap file into your PC through file-system */
static int tcpdump_pcap_file_write(const void *buf, int len)
{
    int length;

    if (filename == OS_NULL)
    {
        os_kprintf("file name is null!\r\n");
        return TCPDUMP_FAILURE;
    }

    // if ((len == 0) && (fd > 0))
    // {
    //     close(fd);
    //     fd = -1;
    //     return TCPDUMP_FAILURE;
    // }

    if (fd < 0)
    {
        fd = open(filename, O_WRONLY | O_CREAT | O_TRUNC, 0);
        if (fd < 0)
        {
            //dbg_log(DBG_ERROR, "open file failed!\n");
            return TCPDUMP_FAILURE;
        }
    }

    length = write(fd, buf, len);
    if (length != len)
    {
        //dbg_log(DBG_ERROR, "write data failed, length: %d\n", length);
        close(fd);
        return TCPDUMP_FAILURE;
    }

    return TCPDUMP_SUCCESS;
}

/* Import pcap file into your PC through rdb tools */
static int tcpdump_pcap_file_save(const void *buf, int len)
{
    // os_device_write_block(tcpdump_pipe, 0, buf, len);
    return TCPDUMP_SUCCESS;
}

/* write ip mess and print */
static void tcpdump_ip_mess_write(struct tcpdump_buf *p)
{
    struct tcpdump_buf *tbuf = p;

    OS_ASSERT(tbuf != OS_NULL);

#ifdef PKG_NETUTILS_TCPDUMP_PRINT
    _hex_dump(tbuf->buf, tbuf->tot_len);
#endif

    /* write ip mess */
    if (tcpdump_write != OS_NULL)
    {
        // os_kprintf("tbuf->tot_len = %d\n", tbuf->tot_len);
        tcpdump_write(tbuf->buf, tbuf->tot_len);
    }
}

/* write pcap file header */
static int32_t tcpdump_pcap_file_init(void)
{
    struct pcap_file_header file_header;
    int res = -1;

    // if (tcpdump_pipe != OS_NULL)
    // {
    //     if (os_device_open(tcpdump_pipe) != 0)
    //     {
    //         return -1;
    //     }
    // }

    /* in rdb mode does not need to write pcap file header */
    if ((tcpdump_write != OS_NULL) && (tcpdump_write == tcpdump_pcap_file_write))
    {
        struct pcap_pkthdr pkthdr;

        /* pcap header */
        PACP_FILE_HEADER_CREATE(&file_header);
        res = tcpdump_write(&file_header, sizeof(file_header));

        /* Positioning at time zero */
        // char pacp_zero[] =
        // {
        //     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x00,
        //     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x00,
        //     0x08, 0x00,
        //     ' ', ' ', 'O', 'n', 'e', 'O', 'S', ' ', 'Z', 'E', 'R', 'O'
        // };
        // PACP_ZERO_PKTHDR_CREATE(&pkthdr, sizeof(pacp_zero));
        // tcpdump_write(&pkthdr, sizeof(pkthdr));
        // tcpdump_write(pacp_zero, sizeof(pacp_zero));
    }

#ifdef  PKG_NETUTILS_TCPDUMP_PRINT
    _hex_dump((uint8_t *)&file_header, PCAP_FILE_HEADER_SIZE);
#endif

    if (res != 0)
        return TCPDUMP_FAILURE;

    return TCPDUMP_SUCCESS;
}

static void tcpdump_thread_entry(void *param)
{
    // struct pbuf *pbuf = OS_NULL;
    struct tcpdump_buf *tbuf = OS_NULL;
    struct pcap_pkthdr pkthdr;
    uint32_t mbval;
    os_kprintf("into tcpdump_thread_entry...\r\n");
    while (1)
    {
        if(OS_SUCCESS == os_mailbox_recv(tcpdump_mb, &mbval, OS_WAIT_FOREVER))
        {
            tbuf = (struct tcpdump_buf *)mbval;
            OS_ASSERT(tbuf != OS_NULL);

            /* write pkthdr */
            if ((tcpdump_write != OS_NULL) && (tcpdump_write == tcpdump_pcap_file_write))
            {
                PACP_PKTHDR_CREATE(&pkthdr, tbuf);
                tcpdump_write(&pkthdr, sizeof(pkthdr));
            }

#ifdef  PKG_NETUTILS_TCPDUMP_PRINT
            _hex_dump((uint8_t *)&pkthdr, PCAP_PKTHDR_SIZE);
#endif
            tcpdump_ip_mess_write(tbuf);
            os_free(tbuf);
            tbuf = OS_NULL;
        }

        /* tcpdump deinit, the mailbox does not receive the data, exits the thread*/
        else
        {
            close(fd);
            fd = -1;
            
            // if (tcpdump_pipe != OS_NULL)
            //     os_device_close((os_device_t)tcpdump_pipe);

            tcpdump_write = OS_NULL;
            tcpdump_filename_del();
            tcpdump_ethname_del();
            return;
        }
    }
}

/* set file name */
static void tcpdump_filename_set(const char *name)
{
    filename = strdup(name);
}

/* delete file name */
static void tcpdump_filename_del(void)
{
    name = OS_NULL;
    if (filename != OS_NULL)
        os_free(filename);

    filename = OS_NULL;
}

/* set network interface name */
static void tcpdump_ethname_set(const char *eth)
{
    ethname = strdup(eth);
}

/* delete network interface name */
static void tcpdump_ethname_del(void)
{
    eth = OS_NULL;
    if (ethname != OS_NULL)
        os_free(ethname);
}

static int tcpdump_init(void)
{
    // struct eth_device *device;
    struct os_net_device *device;

    os_task_id tcpdump_task_id;
    os_ubase_t level;

    if (netif != OS_NULL)
    {
        //dbg_log(DBG_ERROR, "This command is running, please stop before you use the [tcpdump -p] command!\n");
        os_kprintf("This command is running, please stop before you use the [tcpdump -p] command!\n\r\n");
        return TCPDUMP_FAILURE;
    }

    /* print and set default state */
    tcpdump_init_indicate();

    #if 0
    tcpdump_pipe = os_device_find(TCPDUMP_PIPE_DEVICE);
    /* file-system mode does not judge pipe */
    if (tcpdump_write != tcpdump_pcap_file_write)
    {
        if (tcpdump_pipe == OS_NULL)
        {
            return TCPDUMP_FAILURE;
        }
    }
    #endif
    
	device = (struct os_net_device *)os_device_find(eth);
    if (device == OS_NULL)
    {
        os_kprintf("network interface [%s] device not find!\r\n", eth);
        return TCPDUMP_FAILURE;
    }
    #if 0 //todo
    struct os_lwip_info *lwip_dev = (struct os_lwip_info *)device->userdata;

    // if ((lwip_dev->netif == OS_NULL) || (lwip_dev->netif->linkoutput == OS_NULL))
    if ((lwip_dev->netif.linkoutput == OS_NULL))
    {
        os_kprintf("this device not eth\r\n", eth);
        return TCPDUMP_FAILURE;
    }
    #endif
    netif = os_lwip_get_netif_by_devname((char*)eth);
    if(netif == OS_NULL)
    {
        os_kprintf("get dev netif fail!\r\n");
        return TCPDUMP_FAILURE;
    }

    tcpdump_mb = os_mailbox_create_dynamic("tdrmb", OS_NET_TX_MB_MAX_NUM);
    if (!tcpdump_mb)
    {
        os_kprintf("tcp dump mp create fail!\r\n");
        return TCPDUMP_FAILURE;
    }

    tcpdump_task_id = os_task_create(OS_NULL, OS_NULL, 2048, "tcpdump", tcpdump_thread_entry, OS_NULL, 10);
    if (tcpdump_task_id == OS_NULL)
    {
        os_mailbox_destroy(tcpdump_mb);
        tcpdump_mb = NULL;
        os_kprintf("tcpdump thread create fail!\r\n");
        return TCPDUMP_FAILURE;
    }

    tcpdump_filename_set(name);
    tcpdump_ethname_set(eth);

    // netif = &lwip_dev->netif;

    /* linkoutput and input init */
    level = os_irq_lock();
    link_output = netif->linkoutput;
    netif->linkoutput = _netif_linkoutput;

    input = netif->input;
    netif->input = _netif_input;
    os_irq_unlock(level);
    /* linkoutput and input init */

    /* write pcap file header */
    tcpdump_pcap_file_init();

    os_task_startup(tcpdump_task_id);

    //dbg_log(DBG_INFO, "tcpdump start!\n");

    return TCPDUMP_SUCCESS;
}

static void tcpdump_deinit(void)
{
    os_ubase_t level;
    os_mailbox_id tcpdump_mb_tmp = OS_NULL;

    if (netif == OS_NULL)
    {
        //dbg_log(DBG_ERROR, "capture packet stopped, no repeat input required!\n");
        return;
    }

    /* linkoutput and input deinit */
    level = os_irq_lock();
    tcpdump_mb_tmp = tcpdump_mb;
    tcpdump_mb = OS_NULL;
    netif->linkoutput = link_output;
    netif->input = input;
    netif = OS_NULL;
    os_irq_unlock(level);;
    /* linkoutput and input deinit */

    os_task_msleep(10);
    os_mailbox_destroy(tcpdump_mb_tmp);
    tcpdump_mb_tmp = OS_NULL;
}

static void tcpdump_help_info_print(void)
{
    os_kprintf("\r\n");
    os_kprintf("|>------------------------- help -------------------------<|\r\n");
    os_kprintf("| tcpdump [-p] [-h] [-i interface] [-m mode] [-w file]     |\r\n");
    os_kprintf("|                                                          |\r\n");
    os_kprintf("| -h: help                                                 |\r\n");
    os_kprintf("| -i: specify the network interface for listening          |\r\n");
    os_kprintf("| -m: choose what mode(file-system or rdb) to save the file|\r\n");
    os_kprintf("| -w: write the captured packets into an xx.pcap file      |\r\n");
    os_kprintf("| -p: stop capturing packets                               |\r\n");
    os_kprintf("|                                                          |\r\n");
    os_kprintf("| e.g.:                                                    |\r\n");
    os_kprintf("| specify network interface and select save mode \\        |\r\n");
    os_kprintf("| and specify filename                                     |\r\n");
    os_kprintf("| tcpdump -ie0 -mfile -wtext.pcap                          |\r\n");
    os_kprintf("| tcpdump -ie0 -mrdb -wtext.pcap                           |\r\n");
    os_kprintf("|                                                          |\r\n");
    os_kprintf("| -m: file-system mode                                     |\r\n");
    os_kprintf("| tcpdump -mfile                                           |\r\n");
    os_kprintf("|                                                          |\r\n");
    os_kprintf("| -m: rdb mode                                             |\r\n");
    os_kprintf("| tcpdump -mrdb                                            |\r\n");
    os_kprintf("|                                                          |\r\n");
    os_kprintf("| -w: file                                                 |\r\n");
    os_kprintf("| tcpdump -wtext.pcap                                      |\r\n");
    os_kprintf("|                                                          |\r\n");
    os_kprintf("| -p: stop                                                 |\r\n");
    os_kprintf("| tcpdump -p                                               |\r\n");
    os_kprintf("|                                                          |\r\n");
    os_kprintf("| -h: help                                                 |\r\n");
    os_kprintf("| tcpdump -h                                               |\r\n");
    os_kprintf("|                                                          |\r\n");
    os_kprintf("| write commands but no arguments are illegal!!            |\r\n");
    os_kprintf("| e.g.: tcpdump -i / -i -mfile  / -i -mfile -wtext.pcap    |\r\n");
    os_kprintf("|>------------------------- help -------------------------<|\r\n");
    os_kprintf("\r\n");
}

static void tcpdump_error_info_deal(void)
{
    //dbg_log(DBG_ERROR, "tcpdump command is incorrect, please refer to the help information!\n");
    tcpdump_help_info_print();
}

/* print and set default state */
static void tcpdump_init_indicate(void)
{
    int name_flag = 0, eth_flag = 0, mode_flag = 0;

    if (eth == OS_NULL)
    {
        os_kprintf("[TCPDUMP]default selection [e0] network interface\r\n");
        eth = "e0";
        eth_flag = 1;
    }

    if (tcpdump_write == OS_NULL)
    {
        os_kprintf("[TCPDUMP]default selection [file-system] mode\r\n");
        tcpdump_write = tcpdump_pcap_file_write;
        mode_flag = 1;
    }

    if (name == OS_NULL)
    {
        os_kprintf("[TCPDUMP]default save in [sample.pcap]\r\n");
        name = TCPDUMP_DEFAULT_NANE;
        name_flag = 1;
    }

    if (eth_flag == 0)
        os_kprintf("[TCPDUMP]select  [%s] network interface\r\n", eth);

    if (mode_flag == 0)
    {
        if (STRCMP(mode, ==, "file"))
            os_kprintf("[TCPDUMP]select  [file-system] mode\r\n");

        if (STRCMP(mode, ==, "rdb"))
            os_kprintf("[TCPDUMP]select  [rdb] mode\r\n");
    }

    if (name_flag == 0)
        os_kprintf("[TCPDUMP]save in [%s]\r\n", name);
}

/* msh command-line deal */
static int tcpdump_cmd_deal(struct optparse *options)
{
    switch (options->optopt)
    {
    case 'p':
        tcpdump_deinit();
        return STOP;

    case 'h':
        tcpdump_help_info_print();
        return HELP;

    case 'i':
        /* it's illegal without parameters. */
        if (options->optarg == OS_NULL)
            return TCPDUMP_FAILURE;

        eth = options->optarg;
        return TCPDUMP_SUCCESS;

    case 'm':
        if (options->optarg == OS_NULL)
            return TCPDUMP_FAILURE;

        if (STRCMP(options->optarg, ==, "file"))
        {
            mode = options->optarg;
            tcpdump_write = tcpdump_pcap_file_write;
            return TCPDUMP_SUCCESS;
        }

        if (STRCMP(options->optarg, ==, "rdb"))
        {
            mode = options->optarg;
            tcpdump_write = tcpdump_pcap_file_save;
            return TCPDUMP_SUCCESS;
        }

        /* User input Error */
        return TCPDUMP_FAILURE;

    case 'w':
        if (options->optarg == OS_NULL)
            return TCPDUMP_FAILURE;

        name = options->optarg;
        break;

    default:
        return TCPDUMP_FAILURE;
    }

    return TCPDUMP_SUCCESS;
}

/* command-line parsing */
static int tcpdump_cmd_parse(char *argv[], const char *cmd)
{
    int ch, res, invalid_argv = 0;
    struct optparse options;

    optparse_init(&options, argv);

    while ((ch = optparse(&options, cmd)) != -1)
    {
        ch = ch;
        invalid_argv = 1;

        switch (options.optopt)
        {
        case 'p':
            return tcpdump_cmd_deal(&options);

        case 'h':
            return tcpdump_cmd_deal(&options);

        case 'i':
            res = tcpdump_cmd_deal(&options);
            break;

        case 'm':
            res = tcpdump_cmd_deal(&options);
            break;

        case 'w':
            res = tcpdump_cmd_deal(&options);
            break;

        default:
            tcpdump_error_info_deal();
            return TCPDUMP_FAILURE;
        }

        if (res == TCPDUMP_FAILURE)
        {
            tcpdump_error_info_deal();
            return res;
        }
    }

    /* judge invalid command */
    if (invalid_argv == 0)
    {
        tcpdump_error_info_deal();
        return TCPDUMP_FAILURE;
    }

    return TCPDUMP_SUCCESS;
}

static void tcpdump_cmd_argv_deinit(void)
{
    eth = OS_NULL;
    tcpdump_write = OS_NULL;
    name = OS_NULL;
}

static int tcpdump_run(int argc, char *argv[])
{
    int res = 0;

    if (argc == 1)
    {
        tcpdump_cmd_argv_deinit();
        tcpdump_init();
        return TCPDUMP_SUCCESS;
    }

    tcpdump_cmd_argv_deinit();
    res = tcpdump_cmd_parse(argv, SHELL_CMD);
    if (res == STOP)
        return TCPDUMP_SUCCESS;

    if (res == HELP)
        return TCPDUMP_SUCCESS;

    if (res == -1)
        return TCPDUMP_FAILURE;

    tcpdump_init();

    return TCPDUMP_SUCCESS;
}

#ifdef OS_USING_SHELL
#include <shell.h>
    SH_CMD_EXPORT(tcpdump, tcpdump_run, "test tcpdump cmd.");
#endif
// #endif /* PKG_NETUTILS_TCPDUMP */
