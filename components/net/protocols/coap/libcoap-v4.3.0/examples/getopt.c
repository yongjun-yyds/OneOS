/*
 * This file was copied from the following newsgroup posting:
 *
 * Newsgroups: mod.std.unix
 * Subject: public domain AT&T getopt source
 * Date: 3 Nov 85 19:34:15 GMT
 *
 * Here's something you've all been waiting for:  the AT&T public domain
 * source for getopt(3).  It is the code which was given out at the 1985
 * UNIFORUM conference in Dallas.  I obtained it by electronic mail
 * directly from AT&T.  The people there assure me that it is indeed
 * in the public domain.
 */

#include <stdio.h>
#include <string.h>

// static int   opterr = 1;
static int coap_optind = 1;
static int coap_optopt;
static char *coap_optarg;

static int coap_getopt(int argc, char *argv[], char *opts)
{
    static int sp = 1;
    int c;
    char *cp;

    if (sp == 1)
    {
        if (coap_optind >= argc || argv[coap_optind][0] != '-' || argv[coap_optind][1] == '\0')
            return EOF;
        else if (!strcmp(argv[coap_optind], "--"))
        {
            coap_optind++;
            return EOF;
        }
    }
    coap_optopt = c = argv[coap_optind][sp];
    if (c == ':' || NULL == (cp = strchr(opts, c)))
    {
        printf(": illegal option -- %c\r\n", c);
        if (argv[coap_optind][++sp] == '\0')
        {
            coap_optind++;
            sp = 1;
        }
        return '?';
    }
    if (*++cp == ':')
    {
        if (argv[coap_optind][sp + 1] != '\0')
            coap_optarg = &argv[coap_optind++][sp + 1];
        else if (++coap_optind >= argc)
        {
            printf(": option requires an argument -- %c\r\n", c);
            sp = 1;
            return '?';
        }
        else
            coap_optarg = argv[coap_optind++];
        sp = 1;
    }
    else
    {
        if (argv[coap_optind][++sp] == '\0')
        {
            sp = 1;
            coap_optind++;
        }
        coap_optarg = NULL;
    }

    return c;
}
