# CoAP协议组件-V4.3.0

------

# 简介

CoAP(Constrained Application Protocol，受限应用协议)是一种在物联网世界广泛应用的类web协议，我们OneOS在移植libcoap-4.2.1后，持续关注libcoap的迭代。因此在libcoap-v4.3.0发布后，根据RTOS特点，针对部分场景优化，将其移植到OneOS系统中来。

**特性支持：**

1. IPV6接入

2. 块传输、文件传输

3. 基于PSK秘钥方式的(D)TLS传输

4. 观察者模式

5. 用户自定义option

6. 可靠传输可选，底层采用TCP传输

   备注：OneOS目前仅LwIP支持IPV6

# 配置指南

启动menuconfig，配置路径：（Top) → Components→ Network→ Protocols→ CoAP，选择libcoap-v4.3.0.

```
                                               OneOS Configuration
[*] Enable libCoAP example
        Client or Server (Client)  --->
(8192) Config example task stack depth
[*] Using IPV6
[*] Using file system
[*] Using mbed DTLS
```

- Client or Server: 启动demo用例，配置成客户端或服务器端；
- Config example task stack depth：配置demo用例栈深度，推荐8192字节；
- Using IPV6：是否启用IPV6；
- Using file system：是否启用OneOS文件系统；
- Using mbed DTLS：是否启用DTLS。

# 使用示例

**服务器端**

- 启动服务器监听任务，默认采用IPV4

  coap_server

- 使用IPV6启动服务器监听任务，需要启用“Using IPV6”

  coap_server -A FE80::xxxx:xxxx:xxxx:xxxx

- 启用动态资源创建，xx为最大创建资源个数

  coap_server -d xx

- 启用psk加密接入，需要启用“Using mbed DTLS”

  coap_server -k xxxxxxxx

- 退出服务器监听任务

  coap_server exit



**客户端示例**

- 查询默认资源-IPV6，需要启用“Using IPV6”

  coap_client -m get coap://[::1]/

- 查询默认资源-IPV4

  coap_client -m get coap://192.168.1.xxx/

- 查询默认资源-DTLS，需要启用“Using mbed DTLS”

  coap_client -m get coaps://192.168.1.xxx/

- 在服务器上新建资源

  coap_client -m put -e "This is a test!" coap://192.168.1.xxx/test

- 向服务器上传文件，需提前在设备上准备文件


  coap_client -m put -f file_name coap://192.168.1.xxx/test

- 启动观察者模式观察某一个资源，持续xx秒

  coap_client -m get -s xx coaps://192.168.1.xxx/

- 自定义option

  coap_client -m get -O 12,0x2f coap://192.168.1.xxx/

- 可靠传输

  coap_client -m get -r  coap://192.168.1.xxx/

- 退出客户端任务

  coap_client exit







