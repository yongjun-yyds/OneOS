/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\ you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an \"AS IS\" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * \@file        coap_config.h
 *
 * \@brief       coap config file
 *
 * \@details
 *
 * \@revision
 * Date         Author          Notes
 * 2020-06-08   OneOS Team      first version
 ***********************************************************************************************************************
 */

#ifndef __COAP_CONFIG_H__
#define __COAP_CONFIG_H__

//#include "oneos/coap_config.h"
#include "oneos/coap_oneos.h"

#if defined(COAP_4_3_0_WITH_IPV6) && !defined(NET_USING_LWIP_IPV6)
#error "Not supported IPv6. You can use IPv6 of LwIP"
#endif

#if defined(COAP_4_3_0_WITH_MBEDTLS)
#define ENABLE_DTLS  0
#define WITH_MBEDTLS 0
#define HAVE_MBEDTLS 0
#endif

#if defined(COAP_4_3_0_WITH_MBEDTLS) && defined(COAP_4_3_0_WITH_CA)
#define COAP_WITH_CA
#endif

#ifndef HAVE_STDIO_H
#define HAVE_STDIO_H
#endif

#ifndef WITH_ONEOS
#define WITH_ONEOS
#endif
/*********coap_io***********/
#ifndef HAVE_STDIO_H
#define HAVE_STDIO_H
#endif

#ifndef HAVE_SYS_SELECT_H
#define HAVE_SYS_SELECT_H
#endif

#ifndef HAVE_SYS_SOCKET_H
#define HAVE_SYS_SOCKET_H
#endif

#ifndef HAVE_SYS_IOCTL_H
#define HAVE_SYS_IOCTL_H
#endif

#ifndef HAVE_UNISTD_H
#define HAVE_UNISTD_H
#endif

#ifndef IPV6_RECVPKTINFO
#define IPV6_RECVPKTINFO
#endif

#ifndef ESPIDF_VERSION
#define ESPIDF_VERSION
#endif

#ifndef HAVE_TIME_H
#define HAVE_TIME_H
#endif

// #ifndef HAVE_STDIO_H
// #define HAVE_STDIO_H
// #endif

// #ifndef HAVE_STDIO_H
// #define HAVE_STDIO_H
// #endif

// #ifndef HAVE_ARPA_INET_H
// #define HAVE_ARPA_INET_H
// #endif

#ifndef HAVE_MALLOC
#define HAVE_MALLOC
#endif

enum
{
    MEMP_COAP_RESOURCEATTR
};

/****start lwippools.h ****/
#if 1

#ifndef MEMP_NUM_COAPCONTEXT
#define MEMP_NUM_COAPCONTEXT 1
#endif

#ifndef MEMP_NUM_COAPENDPOINT
#define MEMP_NUM_COAPENDPOINT 1
#endif

/* 1 is sufficient as this is very short-lived */
#ifndef MEMP_NUM_COAPPACKET
#define MEMP_NUM_COAPPACKET 1
#endif

#ifndef MEMP_NUM_COAPNODE
#define MEMP_NUM_COAPNODE 4
#endif

#ifndef MEMP_NUM_COAPPDU
#define MEMP_NUM_COAPPDU MEMP_NUM_COAPNODE
#endif

#ifndef MEMP_NUM_COAPSESSION
#define MEMP_NUM_COAPSESSION 2
#endif

#ifndef MEMP_NUM_COAP_SUBSCRIPTION
#define MEMP_NUM_COAP_SUBSCRIPTION 4
#endif

#ifndef MEMP_NUM_COAPRESOURCE
#define MEMP_NUM_COAPRESOURCE 10
#endif

#ifndef MEMP_NUM_COAPRESOURCEATTR
#define MEMP_NUM_COAPRESOURCEATTR 20
#endif

#ifndef MEMP_NUM_COAPOPTLIST
#define MEMP_NUM_COAPOPTLIST 1
#endif

#ifndef MEMP_LEN_COAPOPTLIST
#define MEMP_LEN_COAPOPTLIST 12
#endif

#ifndef MEMP_NUM_COAPSTRING
#define MEMP_NUM_COAPSTRING 10
#endif

#ifndef MEMP_LEN_COAPSTRING
#define MEMP_LEN_COAPSTRING 32
#endif

#ifndef MEMP_NUM_COAPCACHE_KEYS
#define MEMP_NUM_COAPCACHE_KEYS (2U)
#endif /* MEMP_NUM_COAPCACHE_KEYS */

#ifndef MEMP_NUM_COAPCACHE_ENTRIES
#define MEMP_NUM_COAPCACHE_ENTRIES (2U)
#endif /* MEMP_NUM_COAPCACHE_ENTRIES */

#ifndef MEMP_NUM_COAPPDUBUF
#define MEMP_NUM_COAPPDUBUF 2
#endif

#ifndef MEMP_LEN_COAPPDUBUF
#define MEMP_LEN_COAPPDUBUF 32
#endif

#ifndef MEMP_NUM_COAPLGXMIT
#define MEMP_NUM_COAPLGXMIT 2
#endif

#ifndef MEMP_NUM_COAPLGCRCV
#define MEMP_NUM_COAPLGCRCV 2
#endif

#ifndef MEMP_NUM_COAPLGSRCV
#define MEMP_NUM_COAPLGSRCV 2
#endif

#endif
/****end lwippools.h ****/

// #include <lwip/opt.h>
// #include <lwip/debug.h>
// #include <lwip/def.h> /* provide ntohs, htons */

// #define WITH_LWIP 1

#ifndef COAP_CONSTRAINED_STACK
#define COAP_CONSTRAINED_STACK 1
#endif

#ifndef COAP_DISABLE_TCP
#define COAP_DISABLE_TCP 0
#endif

#define PACKAGE_NAME    "libcoap"
#define PACKAGE_VERSION "4.3.0"
#define PACKAGE_STRING  "libcoap 4.3.0"

#include <os_assert.h>
#define assert(x) OS_ASSERT(x)

/* it's just provided by libc. i hope we don't get too many of those, as
 * actually we'd need autotools again to find out what environment we're
 * building in */
#define HAVE_STRNLEN 1
#define HAVE_LIMITS_H

#endif /* __COAP_CONFIG_H__ */
