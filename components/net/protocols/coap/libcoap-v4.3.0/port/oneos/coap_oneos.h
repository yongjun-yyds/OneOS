/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\ you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an \"AS IS\" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * \@file
 *
 * \@brief
 *
 * \@details
 *
 * \@revision
 * Date         Author          Notes
 * 2020-06-08   OneOS Team      first version
 ***********************************************************************************************************************
 */

#ifndef __COAP_ONEOS_H__
#define __COAP_ONEOS_H__

/*ws2ipdef.h*/
#include <sys/socket.h>
#include <stdlib.h>
#include <errno_ext.h>
#include <libc_ext.h>

#ifndef DEF
#define DEF(x) 1
#endif

#undef _WIN32

#define IPV6_MULTICAST_HOPS 10    // IP multicast hop limit.
static inline char IN6_IS_ADDR_V4MAPPED(const struct in6_addr *a)
{
    return ((a->un.u32_addr[0] == 0) && (a->un.u32_addr[1] == 0) && (a->un.u32_addr[2] == 0) &&
            (a->un.u32_addr[3] == 0xffff));
}

static inline char *gai_strerror(int error)
{
    return "dns error!";
}
#if !defined(__GNUC__)
static inline char *strndup(const char *s, size_t n)
{
    char *ptr = malloc(n);
    if (ptr == NULL)
        return NULL;
    memcpy(ptr, s, n);
    return ptr;
}
#endif /* __GNUC__ */

#endif /* __COAP_ONEOS_H__ */
