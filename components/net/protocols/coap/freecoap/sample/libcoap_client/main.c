
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <coap_get_client.h>

#define KEY_FILE_NAME    "../../certs/client_privkey.pem"
#define CERT_FILE_NAME   "../../certs/client_cert.pem"
#define TRUST_FILE_NAME  "../../certs/root_server_cert.pem"
#define CRL_FILE_NAME    ""
#define COMMON_NAME      "dummy/server"
#define BUF_LEN          128

int libcoap_get_client_func(int argc, char **argv)
{
    libcoap_get_client_t client = {0};
    char buf[BUF_LEN] = {0};
    int ret = 0;

    if (argc != 3)
    {
        fprintf(stderr, "usage: libcoap_get_client host port\n");
        fprintf(stderr, "    host: IP address or host name to connect to\n");
        fprintf(stderr, "    port: port number to connect to\n");
        return EXIT_FAILURE;
    }
    ret = libcoap_get_client_init();
    if (ret < 0)
    {
        return EXIT_FAILURE;
    }
    ret = libcoap_get_client_create(&client,
                             argv[1],
                             argv[2],
                             KEY_FILE_NAME,
                             CERT_FILE_NAME,
                             TRUST_FILE_NAME,
                             CRL_FILE_NAME,
                             COMMON_NAME);
    if (ret < 0)
    {
        libcoap_get_client_init();
        return EXIT_FAILURE;
    }
    while (1)
    {
        ret = libcoap_client_get_info(&client, buf, sizeof(buf));
        if (ret < 0)
        {
            libcoap_get_client_destroy(&client);
            libcoap_get_client_deinit();
            return EXIT_FAILURE;
        }
        printf("libcoap get info: '%s'\n", buf);
        os_task_msleep(1000);
    }
    libcoap_get_client_destroy(&client);
    libcoap_get_client_deinit();
    return EXIT_SUCCESS;
}

#ifdef OS_USING_SHELL
#include <shell.h>

SH_CMD_EXPORT(libcoap_get_client, libcoap_get_client_func, "libcoap client get to server.");
#endif
