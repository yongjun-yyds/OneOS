
#ifndef LIBCOAP_GET_CLIENT_H
#define LIBCOAP_GET_CLIENT_H

#include <stddef.h>
#include "coap_client.h"

typedef struct
{
    coap_client_t coap_client;
}
libcoap_get_client_t;

int libcoap_get_client_init(void);
void libcoap_get_client_deinit(void);
int libcoap_get_client_create(libcoap_get_client_t *client,
                       const char *host,
                       const char *port,
                       const char *key_file_name,
                       const char *cert_file_name,
                       const char *trust_file_name,
                       const char *crl_file_name,
                       const char *common_name);
void libcoap_get_client_destroy(libcoap_get_client_t *client);
int libcoap_client_get_info(libcoap_get_client_t *client, char *buf, size_t len);

#endif
