/*
 * Copyright (c) 2017 Keith Cullen.
 * All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h>
#include <stdio.h>
#include "time_server.h"

#define KEY_FILE_NAME    "../../certs/server_privkey.pem"
#define CERT_FILE_NAME   "../../certs/server_cert.pem"
#define TRUST_FILE_NAME  "../../certs/root_client_cert.pem"
#define CRL_FILE_NAME    ""

#define COAP_TIME_SERVER_STACK_SIZE 20480

#include <os_task.h>
static os_task_id coap_time_server = OS_NULL;
static int param_argc = 0;
static char **param_argv = OS_NULL;

static void coap_param_free(void)
{
    for (int i = 0; i < param_argc && param_argv != NULL; i++)
    {
        if (param_argv[i] != NULL)
        {
            free(param_argv[i]);
            param_argv[i] = NULL;
        }
    }
    if (param_argv != NULL)
    {
        free(param_argv);
        param_argv = NULL;
    }
}

int time_coap_server_func(int argc, char **argv)
{
    time_server_t server = {0};
    int ret = 0;

    if (argc != 3)
    {
        fprintf(stderr, "usage: time_server host port\n");
        fprintf(stderr, "    host: IP address or host name to listen on (0.0.0.0 to listen on all interfaces)\n");
        fprintf(stderr, "    port: port number to listen on\n");
        return EXIT_FAILURE;
    }
    ret = time_server_init();
    if (ret < 0)
    {
        return EXIT_FAILURE;
    }
    ret = time_server_create(&server,
                             argv[1],
                             argv[2],
                             KEY_FILE_NAME,
                             CERT_FILE_NAME,
                             TRUST_FILE_NAME,
                             CRL_FILE_NAME);
    if (ret < 0)
    {
        time_server_deinit();
        return EXIT_FAILURE;
    }
    ret = time_server_run(&server);
    time_server_destroy(&server);
    time_server_deinit();
    if (ret < 0)
    {
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

void coap_time_server_entry(void *paramter)
{
    time_coap_server_func(param_argc, param_argv);
    coap_param_free();
    os_task_destroy(coap_time_server);
    os_kprintf("\r\nExit coap server task!\r\n");

    return;
}

static int time_coap_server_task(int argc, char **argv)
{
    int rc = -1;
    param_argc = argc;
    param_argv = malloc(sizeof(char *) * argc);
    if (param_argv == NULL)
    {
        goto exit;
    }
    for (int i = 0; i < argc; i++)
    {
        param_argv[i] = strdup(argv[i]);
        if (param_argv[i] == NULL)
        {
            rc = -1;
            goto exit;
        }
    }
    coap_time_server = os_task_create(NULL,NULL,COAP_TIME_SERVER_STACK_SIZE, "coap_time_server", coap_time_server_entry, NULL, 16);

    if(OS_NULL != coap_time_server)
    {
        return os_task_startup(coap_time_server);
    }

exit:
    coap_param_free();
    return rc;
}


#ifdef OS_USING_SHELL
#include <shell.h>

SH_CMD_EXPORT(coap_time_server, time_coap_server_task, "time coap server to client.");
#endif
