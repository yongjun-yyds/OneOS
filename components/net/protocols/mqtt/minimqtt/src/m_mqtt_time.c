/***********************************************************************************************************************
 * Copyright (c) 2021, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file
 *
 * @brief
 *
 * @revision
 * Date         Author          Notes
 * 2022-01-08   OneOS Team      First Version
 ***********************************************************************************************************************/
#include <os_clock.h>
#include <os_task.h>
#include <stdlib.h>

#ifndef FALSE
#define FALSE (0)
#endif

#ifndef TRUE
#define TRUE (1)
#endif

struct __timer
{
    size_t tick_wait;    /* tick wait setting for this timer */
    size_t ticks_record; /* record the tick value */
};

static size_t get_time_ms(void)
{
    float sys_ms = (float)os_tick_get_value() * 1000 / OS_TICK_PER_SECOND;
    return (size_t)sys_ms;
}

static size_t get_sys_tick(void)
{
    return os_tick_get_value();
}

static size_t get_tick_from_ms(size_t ms)
{
    return ms * OS_TICK_PER_SECOND / 1000;
}

static size_t get_ms_from_tick(size_t tick)
{
    return tick * 1000 / OS_TICK_PER_SECOND;
}

static struct __timer *timer_create(void)
{
    struct __timer *timer = malloc(sizeof(struct __timer));
    if (timer == NULL)
    {
        return NULL;
    }
    timer->tick_wait    = 0;
    timer->ticks_record = get_sys_tick();
    return (void *)timer;
}

static void timer_destroy(struct __timer *timer)
{
    if (timer == NULL)
        return;
    free(timer);
}

static int timer_set_timeout_ms(struct __timer *timer, uint32_t timeout_ms)
{
    timer->tick_wait    = get_tick_from_ms(timeout_ms);
    timer->ticks_record = get_sys_tick();
    return 0;
}

static uint8_t timer_is_expired(struct __timer *timer)
{
    size_t tick_now;
    size_t tick_pre;
    size_t tick_diff;

    if (timer == NULL)
        return FALSE;
    tick_now            = get_sys_tick();
    tick_pre            = timer->ticks_record;
    tick_diff           = (long)tick_now - (long)tick_pre;
    timer->ticks_record = tick_now;

    if (tick_diff < timer->tick_wait)
    {
        timer->tick_wait -= tick_diff;
        return FALSE;
    }
    else
    {
        timer->tick_wait = 0;
        return TRUE;
    }
}

static uint32_t timer_remained_ms(struct __timer *timer)
{
    if (timer == NULL || timer_is_expired(timer))
        return 0;    // time out
    else
        return get_ms_from_tick(timer->tick_wait);
}

static uint32_t timer_passtime_ms(struct __timer *timer)
{
    if (timer == NULL)
        return 0;
    if (timer_is_expired(timer))
        return get_ms_from_tick(timer->ticks_record);
    else
        return get_ms_from_tick(timer->ticks_record - timer->tick_wait);
}
