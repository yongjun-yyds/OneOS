/***********************************************************************************************************************
 * Copyright (c) 2021, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file
 *
 * @brief
 *
 * @revision
 * Date         Author          Notes
 * 2022-01-08   OneOS Team      First Version
 ***********************************************************************************************************************/

#include "m_mqtt_common.h"
#include <os_assert.h>
#include <stdlib.h>
#include <string.h>

void *m_mqtt_strdup(const char *str)
{
    char *new_str = malloc(strlen(str) + 1);
    if (new_str == NULL)
        return NULL;
    memcpy(new_str, str, strlen(str) + 1);
    return (void *)new_str;
}

int m_mqtt_buff_creat(m_mqtt_buff *mqtt_buff, size_t len)
{
    OS_ASSERT(mqtt_buff != NULL);
    mqtt_buff->buff = calloc(len, 1);
    if (mqtt_buff->buff == NULL)
        return ERR_M_MQTT_OUT_OF_MEMORY;
    mqtt_buff->len = len;
    return ERR_M_MQTT_SUCCESS;
}

int m_mqtt_handler_list_add(m_mqtt_message_handler_list **head,
                            const char                   *topic,
                            m_mqtt_message_handler        message_handler)
{
    m_mqtt_message_handler_list *node = calloc(sizeof(m_mqtt_message_handler_list), 1);

    if (node == NULL)
        return ERR_M_MQTT_OUT_OF_MEMORY;
    node->message_handler = message_handler;
    node->topic_filter    = m_mqtt_strdup(topic);
    if (node->topic_filter == NULL)
    {
        free(node);
        return ERR_M_MQTT_OUT_OF_MEMORY;
    }
    node->next = *head;
    *head      = node;
    return ERR_M_MQTT_SUCCESS;
}

int m_mqtt_handler_list_del(m_mqtt_message_handler_list **head, const char *topic)
{
    m_mqtt_message_handler_list *node = *head;
    m_mqtt_message_handler_list *tmp;
    while (node != NULL)
    {
        if (strcmp(node->topic_filter, topic) == 0)
        {
            break;
        }
        tmp  = node;
        node = node->next;
    }
    if (node == *head)
    {
        *head = NULL;
    }
    else if (node != NULL)
    {
        tmp->next = node->next;
    }
    if (node != NULL)
    {
        free(node->topic_filter);
        free(node);
    }
    return ERR_M_MQTT_SUCCESS;
}

void m_mqtt_handler_list_free(m_mqtt_message_handler_list **head)
{
    m_mqtt_message_handler_list *node = *head;
    m_mqtt_message_handler_list *tmp;
    *head = NULL;
    while (node != NULL)
    {
        tmp = node->next;
        free(node->topic_filter);
        free(node);
        node = tmp;
    }
}

m_mqtt_message_handler m_mqtt_handler_list_search(m_mqtt_message_handler_list *head, const char *topic)
{
    m_mqtt_message_handler_list *node = head;
    while (node != NULL)
    {
        if (strcmp(node->topic_filter, topic) == 0)
        {
            break;
        }
        node = node->next;
    }
    if (node != NULL)
    {
        return node->message_handler;
    }
    else
    {
        return NULL;
    }
}
