/***********************************************************************************************************************
 * Copyright (c) 2021, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file
 *
 * @brief
 *
 * @revision
 * Date         Author          Notes
 * 2022-01-08   OneOS Team      First Version
 ***********************************************************************************************************************/
#include "m_mqtt_client.h"
#include "m_mqtt_log.h"
#include "m_mqtt_common.h"
#include "packet/m_mqtt_connect.h"
#include "packet/m_mqtt_publish.h"
#include "packet/m_mqtt_subscribe.h"
#include "packet/m_mqtt_unsubscribe.h"
#include <os_mutex.h>
#include <os_task.h>
#include <os_clock.h>
#include <os_assert.h>
#include <arch_atomic.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/time.h>
#include <netdb.h>
#include <stdlib.h>
#include <string.h>
#include "m_mqtt_net.c"
#include "m_mqtt_time.c"

#define MAGIC_NUMBER 0x19
typedef struct
{
    char           *id;               /* 客户端ID */
    uint32_t        keep_alive_ms;    /* 保活时间，单位毫秒，0为不保活 */
    uint16_t        ping_outstanding; /* ping报文超时计数 */
    char           *username;         /* 用户名 */
    char           *password;         /* 密码 */
    int             net_handle;       /* 网络句柄 */
    uint16_t        packet_id;        /* 包序列号 */
    uint32_t        timeout;          /* 消息超时时间，毫秒 */
    os_mutex_id     mutex;            /* 互斥锁 */
    struct __timer *timer;            /* 定时器 */
    struct __timer *alive_timer;      /* 保活定时器 */
    os_atomic_t     magic_num;        /* 魔数 */
    os_atomic_t     is_connect;       /* 是否已经连接 */
    os_atomic_t     is_scheduling;    /* 是否在调度中 */
    m_mqtt_buff     tx;               /* 发送缓存区 */
    m_mqtt_buff     rx;               /* 接收缓存区 */

    m_mqtt_message_handler_list *head; /* 消息处理函数列表 */
} m_mqtt_client;

static void __m_mqtt_release_handle(m_mqtt_client *mqtt_client)
{
    if (mqtt_client == NULL || MAGIC_NUMBER != os_atomic_read(&mqtt_client->magic_num))
        return;
    os_atomic_set(&mqtt_client->magic_num, 0);
    while (os_atomic_read(&mqtt_client->is_scheduling))
    {
        os_task_msleep(100);
    }
    if (mqtt_client->mutex != NULL)
        os_mutex_recursive_lock(mqtt_client->mutex, OS_WAIT_FOREVER);
    if (mqtt_client->tx.buff != NULL)
        free(mqtt_client->tx.buff);
    if (mqtt_client->rx.buff != NULL)
        free(mqtt_client->rx.buff);
    timer_destroy(mqtt_client->timer);
    timer_destroy(mqtt_client->alive_timer);
    if (mqtt_client->mutex != NULL)
    {
        os_mutex_recursive_unlock(mqtt_client->mutex);
        os_mutex_destroy(mqtt_client->mutex);
    }
    // memset(mqtt_client, 0, sizeof(m_mqtt_client));
    free(mqtt_client);
    return;
}

static uint16_t __m_mqtt_get_next_packet_id(m_mqtt_client *mqtt_client)
{
#define MAX_M_MQTT_PACKET_ID 65535
    mqtt_client->packet_id = (mqtt_client->packet_id == MAX_M_MQTT_PACKET_ID) ? 1 : (mqtt_client->packet_id + 1);
    return mqtt_client->packet_id;
}

static int __m_mqtt_send_packet(m_mqtt_client *mqtt_client, size_t length)
{
    int rc;

    rc = net_send_with_timeout(mqtt_client->net_handle, mqtt_client->tx.buff, length, mqtt_client->timeout);
    if (rc <= 0)
    {
        return ERR_M_MQTT_NET;
    }
    timer_set_timeout_ms(mqtt_client->alive_timer, mqtt_client->keep_alive_ms);
    return ERR_M_MQTT_SUCCESS;
}

static int __m_mqtt_depacket(uint8_t *buf, size_t size, m_mqtt_packet *packet)
{
#define MAX_NO_OF_REMAINING_LENGTH_BYTES 4
    int rem_len    = 0;
    int len        = 0;
    int multiplier = 1;

    if (size < 2)
        return 0;

    do
    {
        if ((++len) > MAX_NO_OF_REMAINING_LENGTH_BYTES)
        {
            return (-1);
        }
        if (len >= size)
        {
            return 0;
        }
        rem_len += (buf[len] & 127) * multiplier;
        multiplier *= 128;
    } while ((buf[len] & 128) != 0);
    if (size < len + 1 + rem_len)
    {
        return 0;
    }
    else if (size > len + 1 + rem_len)
    {
        M_MQTT_LOG_I("buf is longer than protocol");
    }
    packet->header.byte = buf[0];
    packet->buff        = buf;
    packet->packet_len  = len + 1 + rem_len;
    packet->paload      = buf + len + 1;
    packet->paload_len  = rem_len;
    return 1;
}

static int __m_mqtt_deliver_sub_message(m_mqtt_client *mqtt_client, const char *topic, uint8_t *buf, size_t buf_len)
{
    m_mqtt_message_handler handler = m_mqtt_handler_list_search(mqtt_client->head, topic);

    if (handler == NULL)
    {
        M_MQTT_LOG_W("not subscribe the topic(%s)", topic);
        return ERR_M_MQTT_GENERAL;
    }
    handler((void *)mqtt_client, topic, (void *)buf, buf_len);
    return ERR_M_MQTT_SUCCESS;
}

static int __m_mqtt_send_ping_request(m_mqtt_client *mqtt_client)
{
    int rc = m_mqtt_serialize_pingreq(mqtt_client->tx.buff, mqtt_client->tx.len);
    if (rc <= 0)
        return rc;
    return __m_mqtt_send_packet(mqtt_client, rc);
}

static int __m_mqtt_keep_alive(m_mqtt_client *mqtt_client)
{
    if (!mqtt_client->keep_alive_ms)
    {
        return ERR_M_MQTT_SUCCESS;
    }

    if (timer_is_expired(mqtt_client->alive_timer))
    {
        if (mqtt_client->ping_outstanding > 4)
        {
            M_MQTT_LOG_E("ping request timeout(%d)", mqtt_client->ping_outstanding);
        }
        if (__m_mqtt_send_ping_request(mqtt_client) == ERR_M_MQTT_SUCCESS)    // send the ping packet
        {
            ++(mqtt_client->ping_outstanding);
        }
        else
        {
            return ERR_M_MQTT_NET;
        }
    }
    return ERR_M_MQTT_SUCCESS;
}

static int __m_mqtt_process(m_mqtt_client *mqtt_client, m_mqtt_packet *packet)
{
    int len = 0;
    int rc;
    int exp_len = 1;
    /*
    net_recv_with_timeout return value
    < 0 :socket error
    = 0 :timeout
    > 0 :receive packet
    */
    memset(packet, 0, sizeof(m_mqtt_packet));
    do
    {
        rc = net_recv_with_timeout(mqtt_client->net_handle,
                                   &mqtt_client->rx.buff[len],
                                   exp_len,
                                   timer_remained_ms(mqtt_client->timer));
        if (rc < 0)
        {
            os_atomic_set(&mqtt_client->is_connect, 0);
            return ERR_M_MQTT_NET;
        }
        if (rc == 0)
        {
            rc = ERR_M_MQTT_SUCCESS;    // time out
            goto exit;
        }
        len += rc;
        rc = __m_mqtt_depacket(mqtt_client->rx.buff, len, packet);
        if (rc < 0)
        {
            rc = ERR_M_MQTT_RX_DATA;
            goto exit;
        }
        else if (rc == 0)
        {
            continue;
        }
        exp_len = packet->paload_len;
    } while (len != packet->packet_len);

    // Refresh alive timer
    timer_set_timeout_ms(mqtt_client->alive_timer, mqtt_client->keep_alive_ms);

    switch (packet->header.bits.type)
    {
    case M_MQTT_CONNACK:     // connect ack
    case M_MQTT_PUBACK:      // publish ack
    case M_MQTT_SUBACK:      // subscribe ack
    case M_MQTT_UNSUBACK:    // unsubscribe ack
        break;
    case M_MQTT_PINGRESP:    // ping respond
        mqtt_client->ping_outstanding = 0;
        break;
    case M_MQTT_PUBLISH:
    {
        m_mqtt_header pub_header;
        uint16_t      packet_id;
        m_mqtt_buff   topic;
        uint8_t      *payload;
        size_t        payload_len;

        if (m_mqtt_deserialize_publish(&pub_header,
                                       &packet_id,
                                       &topic,
                                       &payload,
                                       &payload_len,
                                       packet->buff,
                                       packet->packet_len) != 1)
        {
            rc = ERR_M_MQTT_RX_DATA;
            M_MQTT_LOG_W("message deserialize fail");
            goto exit;
        }
        char *topic_name = calloc(topic.len + 1, 1);
        if (topic_name == NULL)
        {
            rc = ERR_M_MQTT_OUT_OF_MEMORY;
            goto exit;
        }
        memcpy(topic_name, topic.buff, topic.len);
        (void)__m_mqtt_deliver_sub_message(mqtt_client, topic_name, payload, payload_len);
        free(topic_name);
#ifdef NET_USING_MINI_MQTT_QOS2
        if (pub_header.bits.qos != mqtt_qos0)
        {
            if (pub_header.bits.qos == mqtt_qos1)
                len = m_mqtt_serialize_puback(mqtt_client->tx.buff, mqtt_client->tx.len, M_MQTT_PUBACK, packet_id);
            else
                len = m_mqtt_serialize_puback(mqtt_client->tx.buff, mqtt_client->tx.len, M_MQTT_PUBREC, packet_id);
#else
        if (pub_header.bits.qos == mqtt_qos1)
        {
            len = m_mqtt_serialize_puback(mqtt_client->tx.buff, mqtt_client->tx.len, M_MQTT_PUBACK, packet_id);
#endif /* NET_USING_MINI_MQTT_QOS2 */
            if (len <= 0)
            {
                M_MQTT_LOG_E("failed to construct message");
                rc = ERR_M_MQTT_TX_BUFF_SHORT;
                goto exit;
            }
            rc = __m_mqtt_send_packet(mqtt_client, len);
            if (rc < 0)
            {
                M_MQTT_LOG_E("failed to send message");
                goto exit;
            }
        }
        break;
    }
#ifdef NET_USING_MINI_MQTT_QOS2
    case M_MQTT_PUBREC:
    case M_MQTT_PUBREL:
    {
        uint8_t  packettype;
        uint8_t  dup;
        uint16_t packet_id;
        if (1 != m_mqtt_deserialize_ack(&packettype, &dup, &packet_id, packet->buff, packet->packet_len))
        {
            rc = ERR_M_MQTT_RX_DATA;
            M_MQTT_LOG_W("message deserialize fail");
            goto exit;
        }
        len = m_mqtt_serialize_puback(mqtt_client->tx.buff,
                                      mqtt_client->tx.len,
                                      (packet->header.bits.type == M_MQTT_PUBREC) ? M_MQTT_PUBREL : M_MQTT_PUBCOMP,
                                      packet_id);
        if (len <= 0)
        {
            M_MQTT_LOG_E("failed to construct message");
            rc = ERR_M_MQTT_TX_BUFF_SHORT;
            goto exit;
        }
        rc = __m_mqtt_send_packet(mqtt_client, len);
        if (rc < 0)
        {
            M_MQTT_LOG_E("failed to send message");
            goto exit;
        }
        break;
    }
    case M_MQTT_PUBCOMP:
        break;
#endif /* NET_USING_MINI_MQTT_QOS2 */
    // case M_MQTT_CONNECT:
    // case M_MQTT_SUBSCRIBE:
    // case M_MQTT_UNSUBSCRIBE:
    // case M_MQTT_PINGREQ:
    // case M_MQTT_DISCONNECT:
    //     M_MQTT_LOG_W( "this is server behavior");
    //     break;
    default:
        M_MQTT_LOG_W("not support type:%d", packet->header.bits.type);
        break;
    }

exit:
    rc = __m_mqtt_keep_alive(mqtt_client);
    if (rc != ERR_M_MQTT_SUCCESS)
    {
        M_MQTT_LOG_E("error(%d)", rc);
        os_atomic_set(&mqtt_client->is_connect, 0);
        if (rc > 0)
            M_MQTT_LOG_HEX_W("recv packet err!", mqtt_client->rx.buff, rc);
    }
    return rc;
}

static int __m_mqtt_wait_message(m_mqtt_client *mqtt_client, int type, m_mqtt_buff *payload)
{
    int           rc;
    m_mqtt_packet packet;
    do
    {
        rc            = __m_mqtt_process(mqtt_client, &packet);
        payload->buff = packet.buff;
        payload->len  = packet.packet_len;
        if (packet.header.bits.type == type)
            return type;
    } while (rc == ERR_M_MQTT_SUCCESS && !timer_is_expired(mqtt_client->timer));
    return rc;
}

#if !defined(NET_USING_MINI_MQTT_AUTO_YIELD)
int m_mqtt_yield(void *mqtt_handle)
{
    m_mqtt_client *mqtt_client = (m_mqtt_client *)mqtt_handle;
    int            ret;
    m_mqtt_packet  packet;

    if (mqtt_client == NULL || MAGIC_NUMBER != os_atomic_read(&mqtt_client->magic_num))
        return ERR_M_MQTT_PARAM;
    if (!os_atomic_read(&mqtt_client->is_connect))
        return ERR_M_MQTT_NOT_CONNECT;

    os_mutex_recursive_lock(mqtt_client->mutex, OS_WAIT_FOREVER);
    timer_set_timeout_ms(mqtt_client->timer, mqtt_client->timeout);
    ret = __m_mqtt_process(mqtt_client, &packet);
    os_mutex_recursive_unlock(mqtt_client->mutex);
    return ret;
}
#else
static void __m_mqtt_thread_func(void *mqtt_handle)
{
    m_mqtt_client *mqtt_client = (m_mqtt_client *)mqtt_handle;
    m_mqtt_packet packet;

    OS_ASSERT(mqtt_client != NULL);
    os_atomic_set(&mqtt_client->is_scheduling, 1);
    while (os_atomic_read(&mqtt_client->is_connect))
    {
        os_mutex_recursive_lock(mqtt_client->mutex, OS_WAIT_FOREVER);
        timer_set_timeout_ms(mqtt_client->timer, 1);
        (void)__m_mqtt_process(mqtt_client, &packet);
        os_mutex_recursive_unlock(mqtt_client->mutex);
        os_task_msleep(100);
    }
    M_MQTT_LOG_W("exit mqtt thread!");
    os_atomic_set(&mqtt_client->is_scheduling, 0);
}
static int __m_mqtt_start_auto_yield(void *mqtt_handle)
{
    os_task_id thread = os_task_create(NULL, NULL, MQTT_AUTO_YIELD_STACK_DEPTH, "MQTT", __m_mqtt_thread_func, mqtt_handle, OS_TASK_PRIORITY_MAX / 2);

    if (thread == NULL)
        return ERR_M_MQTT_OUT_OF_MEMORY;
    if (os_task_startup(thread))
    {
        os_task_destroy(thread);
        return ERR_M_MQTT_GENERAL;
    }
    else
        return ERR_M_MQTT_SUCCESS;
}
#endif /* NET_USING_MINI_MQTT_AUTO_YIELD */

void *m_mqtt_create(const m_mqtt_param *param)
{
    m_mqtt_client *mqtt_client = calloc(sizeof(m_mqtt_client), 1);
    if (mqtt_client == NULL)
        goto error;
    os_atomic_set(&mqtt_client->magic_num, MAGIC_NUMBER);
    os_atomic_set(&mqtt_client->is_connect, 0);
    if (ERR_M_MQTT_SUCCESS != m_mqtt_buff_creat(&(mqtt_client->tx), param->sendbuf_size))
        goto error;
    if (ERR_M_MQTT_SUCCESS != m_mqtt_buff_creat(&(mqtt_client->rx), param->recvbuf_size))
        goto error;
    if ((mqtt_client->mutex = os_mutex_create(OS_NULL, "m_mqtt", TRUE)) == 0)
        goto error;
    if ((mqtt_client->timer = timer_create()) == NULL)
        goto error;
    if ((mqtt_client->alive_timer = timer_create()) == NULL)
        goto error;
    mqtt_client->timeout = param->timeout;
    M_MQTT_LOG_D("mqtt create");
    return (void *)mqtt_client;
error:
    __m_mqtt_release_handle(mqtt_client);
    M_MQTT_LOG_E("out of memory");
    return NULL;
}

void m_mqtt_destory(void *mqtt_handle)
{
    m_mqtt_client *mqtt_client = (m_mqtt_client *)mqtt_handle;

    if (mqtt_client == NULL || MAGIC_NUMBER != os_atomic_read(&mqtt_client->magic_num))
        return;
    m_mqtt_disconnect(mqtt_handle);
    __m_mqtt_release_handle(mqtt_handle);
    M_MQTT_LOG_D("mqtt destory");
    return;
}

int m_mqtt_connect(void *mqtt_handle, const m_mqtt_connect_param *param)
{
    m_mqtt_client *mqtt_client = (m_mqtt_client *)mqtt_handle;
    int            ret         = 0, len;
    m_mqtt_buff    recv_data;

    if (mqtt_client == NULL || MAGIC_NUMBER != os_atomic_read(&mqtt_client->magic_num))
        return ERR_M_MQTT_PARAM;
    if (param == NULL)
        return ERR_M_MQTT_PARAM;

    os_mutex_recursive_lock(mqtt_client->mutex, OS_WAIT_FOREVER);
    mqtt_client->keep_alive_ms = param->keep_alive * 1000;
    timer_set_timeout_ms(mqtt_client->timer, mqtt_client->timeout);
    mqtt_client->net_handle = net_connect(param->server_addr, param->port);
    if (mqtt_client->net_handle < 0)
    {
        ret = ERR_M_MQTT_NET;
        goto exit;
    }
    len = m_mqtt_serialize_connect(mqtt_client->tx.buff,
                                   mqtt_client->tx.len,
                                   param->keep_alive,
                                   param->id,
                                   param->username,
                                   param->password);
    if (len < 0)
    {
        ret = len;
        goto exit;
    }
    ret = __m_mqtt_send_packet(mqtt_client, len);
    if (ret < 0)
    {
        goto exit;
    }
    if (__m_mqtt_wait_message(mqtt_client, M_MQTT_CONNACK, &recv_data) == M_MQTT_CONNACK)
    {
        uint8_t sessionPresent;
        uint8_t connack_rc;
        ret = m_mqtt_deserialize_connect_ack(&sessionPresent, &connack_rc, recv_data.buff, recv_data.len);
        if (ret == 1 && !connack_rc)
        {
            M_MQTT_LOG_I("connect success");
            ret = ERR_M_MQTT_SUCCESS;
        }
        else
        {
            M_MQTT_LOG_E("connect fail");
            ret = ERR_M_MQTT_SERVER;
            goto exit;
        }
    }
    else
    {
        M_MQTT_LOG_E("connect fail");
        ret = ERR_M_MQTT_SERVER;
        goto exit;
    }
    os_atomic_set(&mqtt_client->is_connect, 1);
#if defined(NET_USING_MINI_MQTT_AUTO_YIELD)
    ret = __m_mqtt_start_auto_yield(mqtt_client);
#endif
exit:
    os_mutex_recursive_unlock(mqtt_client->mutex);
    return ret;
}

void m_mqtt_disconnect(void *mqtt_handle)
{
    m_mqtt_client *mqtt_client = (m_mqtt_client *)mqtt_handle;
    int            len;

    if (mqtt_client == NULL || MAGIC_NUMBER != os_atomic_read(&mqtt_client->magic_num))
        return;
    if (!os_atomic_read(&mqtt_client->is_connect))
        return;
    os_mutex_recursive_lock(mqtt_client->mutex, OS_WAIT_FOREVER);
    os_atomic_set(&mqtt_client->is_connect, 0);
    timer_set_timeout_ms(mqtt_client->timer, mqtt_client->timeout);
    len = m_mqtt_serialize_disconnect(mqtt_client->tx.buff, mqtt_client->tx.len);
    if (len > 0 && __m_mqtt_send_packet(mqtt_client, len) == ERR_M_MQTT_SUCCESS)
    {
        M_MQTT_LOG_I("disconnect success");
    }
    else
    {
        M_MQTT_LOG_W("disconnect fali");
    }
    closesocket(mqtt_client->net_handle);
    mqtt_client->net_handle = -1;
    m_mqtt_handler_list_free(&mqtt_client->head);
    os_mutex_recursive_unlock(mqtt_client->mutex);
    return;
}

int m_mqtt_subscribe(void                  *mqtt_handle,
                     const char            *topic_filter,
                     m_mqtt_qos             qos,
                     m_mqtt_message_handler message_handler)
{
    m_mqtt_client *mqtt_client = (m_mqtt_client *)mqtt_handle;
    m_mqtt_buff    recv_data;
    int            ret, len;

    if (mqtt_client == NULL || MAGIC_NUMBER != os_atomic_read(&mqtt_client->magic_num))
        return ERR_M_MQTT_PARAM;
    if (!os_atomic_read(&mqtt_client->is_connect))
        return ERR_M_MQTT_NOT_CONNECT;
#ifndef NET_USING_MINI_MQTT_QOS2
    if (qos > mqtt_qos1)
        return ERR_M_MQTT_NOT_SUPPORT_QOS2;
#endif /* NET_USING_MINI_MQTT_QOS2 */
    os_mutex_recursive_lock(mqtt_client->mutex, OS_WAIT_FOREVER);
    timer_set_timeout_ms(mqtt_client->timer, mqtt_client->timeout);
    len = m_mqtt_serialize_subscribe(mqtt_client->tx.buff,
                                     mqtt_client->tx.len,
                                     0,
                                     __m_mqtt_get_next_packet_id(mqtt_client),
                                     topic_filter,
                                     qos);
    if (len <= 0)
    {
        ret = len;
        goto exit;
    }
    ret = __m_mqtt_send_packet(mqtt_client, len);
    if (ret != ERR_M_MQTT_SUCCESS)
        goto exit;

    if (__m_mqtt_wait_message(mqtt_client, M_MQTT_SUBACK, &recv_data) == M_MQTT_SUBACK)
    {
        unsigned short mypacketid;
        int            grantedQoSs = mqtt_sub_fail;
        ret = m_mqtt_deserialize_sub_ack(&mypacketid, &grantedQoSs, recv_data.buff, recv_data.len);
        if (ret == 1 && grantedQoSs == qos)
        {
            M_MQTT_LOG_I("topic(%s) subscribe successed", topic_filter);
            ret = m_mqtt_handler_list_add(&mqtt_client->head, topic_filter, message_handler);
        }
        else
        {
            M_MQTT_LOG_E("subscribe fail(ret:%d, grantedQoSs:%d)", ret, grantedQoSs);
            ret = ERR_M_MQTT_SERVER;
            goto exit;
        }
    }
    else
    {
        M_MQTT_LOG_E("subscribe fail");
        ret = ERR_M_MQTT_SERVER;
        goto exit;
    }
exit:
    os_mutex_recursive_unlock(mqtt_client->mutex);
    return ret;
}

int m_mqtt_unsubscribe(void *mqtt_handle, const char *topic_filter)
{
    m_mqtt_client *mqtt_client = (m_mqtt_client *)mqtt_handle;
    m_mqtt_buff    recv_data;
    int            ret, len;

    if (mqtt_client == NULL || MAGIC_NUMBER != os_atomic_read(&mqtt_client->magic_num))
        return ERR_M_MQTT_PARAM;
    if (!os_atomic_read(&mqtt_client->is_connect))
        return ERR_M_MQTT_NOT_CONNECT;
    os_mutex_recursive_lock(mqtt_client->mutex, OS_WAIT_FOREVER);
    timer_set_timeout_ms(mqtt_client->timer, mqtt_client->timeout);
    len = m_mqtt_serialize_unsubscribe(mqtt_client->tx.buff,
                                       mqtt_client->tx.len,
                                       0,
                                       __m_mqtt_get_next_packet_id(mqtt_client),
                                       topic_filter);
    if (len <= 0)
    {
        ret = len;
        goto exit;
    }
    ret = __m_mqtt_send_packet(mqtt_client, len);
    if (ret != ERR_M_MQTT_SUCCESS)
        goto exit;

    if (__m_mqtt_wait_message(mqtt_client, M_MQTT_UNSUBACK, &recv_data) == M_MQTT_UNSUBACK)
    {
        unsigned short mypacketid;
        ret = m_mqtt_deserialize_unsub_ack(&mypacketid, recv_data.buff, recv_data.len);
        if (ret == 1)
        {
            M_MQTT_LOG_I("topic(%s) unsubscribe successed", topic_filter);
            ret = m_mqtt_handler_list_del(&mqtt_client->head, topic_filter);
        }
        else
        {
            M_MQTT_LOG_E("unsubscribe fail(ret:%d)", ret);
            ret = ERR_M_MQTT_SERVER;
            goto exit;
        }
    }
    else
    {
        M_MQTT_LOG_E("unsubscribe fail");
        ret = ERR_M_MQTT_SERVER;
        goto exit;
    }
exit:
    os_mutex_recursive_unlock(mqtt_client->mutex);
    return ret;
}

int m_mqtt_publish(void *mqtt_handle, const char *topic, m_mqtt_message *message)
{
    m_mqtt_client *mqtt_client = (m_mqtt_client *)mqtt_handle;
    m_mqtt_buff    recv_data;
    int            ret, len;
    m_mqtt_header  header = {0};
    unsigned short mypacketid;
    unsigned char  dup, type;

    if (mqtt_client == NULL || message == NULL || MAGIC_NUMBER != os_atomic_read(&mqtt_client->magic_num))
        return ERR_M_MQTT_PARAM;
    if (!os_atomic_read(&mqtt_client->is_connect))
        return ERR_M_MQTT_NOT_CONNECT;
#ifndef NET_USING_MINI_MQTT_QOS2
    if (message->qos > mqtt_qos1)
        return ERR_M_MQTT_NOT_SUPPORT_QOS2;
#endif /* NET_USING_MINI_MQTT_QOS2 */
    os_mutex_recursive_lock(mqtt_client->mutex, OS_WAIT_FOREVER);
    timer_set_timeout_ms(mqtt_client->timer, mqtt_client->timeout);
    header.bits.qos = message->qos;
    len             = m_mqtt_serialize_publish(mqtt_client->tx.buff,
                                   mqtt_client->tx.len,
                                   &header,
                                   __m_mqtt_get_next_packet_id(mqtt_handle),
                                   topic,
                                   message->payload,
                                   message->payloadlen);
    if (len <= 0)
    {
        ret = len;
        goto exit;
    }
    ret = __m_mqtt_send_packet(mqtt_client, len);
    if (ret != ERR_M_MQTT_SUCCESS)
        goto exit;
    if (message->qos != mqtt_qos0)
    {
        ret = ERR_M_MQTT_SERVER;
#ifdef NET_USING_MINI_MQTT_QOS2
        if ((message->qos == mqtt_qos1 &&
             __m_mqtt_wait_message(mqtt_client, M_MQTT_PUBACK, &recv_data) == M_MQTT_PUBACK) ||
            (message->qos == mqtt_qos2 &&
             __m_mqtt_wait_message(mqtt_client, M_MQTT_PUBCOMP, &recv_data) == M_MQTT_PUBCOMP))
        {
            if (1 == m_mqtt_deserialize_ack(&type, &dup, &mypacketid, recv_data.buff, recv_data.len) &&
                mypacketid == mqtt_client->packet_id)
            {
                ret = ERR_M_MQTT_SUCCESS;
            }
        }
#else
        if (__m_mqtt_wait_message(mqtt_client, M_MQTT_PUBACK, &recv_data) == M_MQTT_PUBACK &&
            1 == m_mqtt_deserialize_ack(&type, &dup, &mypacketid, recv_data.buff, recv_data.len) &&
            mypacketid == mqtt_client->packet_id)
        {
            ret = ERR_M_MQTT_SUCCESS;
        }
#endif /* NET_USING_MINI_MQTT_QOS2 */
    }

exit:
    if (ret != ERR_M_MQTT_SUCCESS)
        M_MQTT_LOG_E("publish fail");
    else
        M_MQTT_LOG_I("message publish success");
    os_mutex_recursive_unlock(mqtt_client->mutex);
    return ret;
}

int m_mqtt_ping_request(void *mqtt_handle)
{
    m_mqtt_client *mqtt_client = (m_mqtt_client *)mqtt_handle;
    m_mqtt_buff    recv_data;
    int            ret, len;

    if (mqtt_client == NULL || MAGIC_NUMBER != os_atomic_read(&mqtt_client->magic_num))
        return ERR_M_MQTT_PARAM;
    if (!os_atomic_read(&mqtt_client->is_connect))
        return ERR_M_MQTT_NOT_CONNECT;
    os_mutex_recursive_lock(mqtt_client->mutex, OS_WAIT_FOREVER);
    timer_set_timeout_ms(mqtt_client->timer, mqtt_client->timeout);
    len = m_mqtt_serialize_pingreq(mqtt_client->tx.buff, mqtt_client->tx.len);
    if (len <= 0)
    {
        ret = len;
        goto exit;
    }
    ret = __m_mqtt_send_packet(mqtt_client, len);
    if (ret != ERR_M_MQTT_SUCCESS)
        goto exit;

    if (__m_mqtt_wait_message(mqtt_client, M_MQTT_PINGRESP, &recv_data) == M_MQTT_PINGRESP)
    {
        M_MQTT_LOG_I("ping req success");
        ret = ERR_M_MQTT_SUCCESS;
    }
    else
    {
        M_MQTT_LOG_E("ping fail");
        ret = ERR_M_MQTT_SERVER;
        goto exit;
    }
exit:
    os_mutex_recursive_unlock(mqtt_client->mutex);
    return ret;
}

int m_mqtt_get_state(void *mqtt_handle)
{
    m_mqtt_client *mqtt_client = (m_mqtt_client *)mqtt_handle;

    if (mqtt_client == NULL || MAGIC_NUMBER != os_atomic_read(&mqtt_client->magic_num))
        return ERR_M_MQTT_PARAM;
    return os_atomic_read(&mqtt_client->is_connect);
}
