/**
 ***********************************************************************************************************************
 * Copyright (c) 2022, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\ you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an \"AS IS\" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * \@file
 *
 * \@brief
 *
 * \@details
 *
 * \@revision
 * Date         Author          Notes
 * 2022-01-08   OneOS Team      first version
 ***********************************************************************************************************************
 */
#ifndef __M_MQTT_LOG_H__
#define __M_MQTT_LOG_H__
#include "oneos_config.h"
#include <stdint.h>
#include <stddef.h>
#if defined(OS_USING_DLOG)
#include "dlog.h"
#else
#endif /* OS_USING_DLOG */
#if defined(__cplusplus)
extern "C" {
#endif /* __cplusplus */

#define M_MQTT_LOG_FATAL   2
#define M_MQTT_LOG_ERROR   3
#define M_MQTT_LOG_WARNING 4
#define M_MQTT_LOG_INFO    6
#define M_MQTT_LOG_DEBUG   7

#if defined(MINI_MQTT_LOG_LEVEL_ERROR)
#define M_MQTT_LOG_LEVEL M_MQTT_LOG_ERROR
#elif defined(MINI_MQTT_LOG_LEVEL_WARNING)
#define M_MQTT_LOG_LEVEL M_MQTT_LOG_WARNING
#elif defined(MINI_MQTT_LOG_LEVEL_INFO)
#define M_MQTT_LOG_LEVEL M_MQTT_LOG_INFO
#elif defined(MINI_MQTT_LOG_LEVEL_DEBUG)
#define M_MQTT_LOG_LEVEL M_MQTT_LOG_DEBUG
#else
#define M_MQTT_LOG_LEVEL M_MQTT_LOG_FATAL
#endif

//#undef OS_USING_DLOG
#ifndef M_MQTT_HEX_WIDTH
#define M_MQTT_HEX_WIDTH 16
#endif

#if defined(OS_USING_DLOG)

#if (M_MQTT_LOG_FATAL <= M_MQTT_LOG_LEVEL)
#define M_MQTT_LOG_F(fmt, ...)           LOG_E("m_mqtt", fmt, ##__VA_ARGS__)
#define M_MQTT_LOG_HEX_F(tag, buf, size) LOG_HEX(tag, M_MQTT_HEX_WIDTH, buf, size)
#else
#define M_MQTT_LOG_F(fmt, ...)
#define M_MQTT_LOG_HEX_F(tag, buf, size)
#endif

#if (M_MQTT_LOG_ERROR <= M_MQTT_LOG_LEVEL)
#define M_MQTT_LOG_E(fmt, ...)           LOG_E("m_mqtt", fmt, ##__VA_ARGS__)
#define M_MQTT_LOG_HEX_E(tag, buf, size) LOG_HEX(tag, M_MQTT_HEX_WIDTH, buf, size)
#else
#define M_MQTT_LOG_E(fmt, ...)
#define M_MQTT_LOG_HEX_E(tag, buf, size)
#endif

#if (M_MQTT_LOG_WARNING <= M_MQTT_LOG_LEVEL)
#define M_MQTT_LOG_W(fmt, ...)           LOG_W("m_mqtt", fmt, ##__VA_ARGS__)
#define M_MQTT_LOG_HEX_W(tag, buf, size) LOG_HEX(tag, M_MQTT_HEX_WIDTH, buf, size)
#else
#define M_MQTT_LOG_W(fmt, ...)
#define M_MQTT_LOG_HEX_W(tag, buf, size)
#endif

#if (M_MQTT_LOG_INFO <= M_MQTT_LOG_LEVEL)
#define M_MQTT_LOG_I(fmt, ...)           LOG_I("m_mqtt", fmt, ##__VA_ARGS__)
#define M_MQTT_LOG_HEX_I(tag, buf, size) LOG_HEX(tag, M_MQTT_HEX_WIDTH, buf, size)
#else
#define M_MQTT_LOG_I(fmt, ...)
#define M_MQTT_LOG_HEX_I(tag, buf, size)
#endif

#if (M_MQTT_LOG_DEBUG <= M_MQTT_LOG_LEVEL)
#define M_MQTT_LOG_D(fmt, ...)           LOG_D("m_mqtt", fmt, ##__VA_ARGS__)
#define M_MQTT_LOG_HEX_D(tag, buf, size) LOG_HEX(tag, M_MQTT_HEX_WIDTH, buf, size)
#else
#define M_MQTT_LOG_D(fmt, ...)
#define M_MQTT_LOG_HEX_D(tag, buf, size)
#endif

#else

#define USING_M_MQTT_LOG
void m_mqtt_log_output(int level, const char *format, ...);    // const char *func, int line,
void m_mqtt_hexdump(const char *tag, size_t width, const unsigned char *buf, int size);

#if (M_MQTT_LOG_FATAL <= M_MQTT_LOG_LEVEL)
#define M_MQTT_LOG_F(fmt, ...)                                                                                         \
    m_mqtt_log_output(M_MQTT_LOG_FATAL, "[%s][%d]: " fmt, ##__VA_ARGS__, __FUNCTION__, __LINE__)
#define M_MQTT_LOG_HEX_F(tag, buf, size) m_mqtt_hexdump(tag, M_MQTT_HEX_WIDTH, buf, size)
#else
#define M_MQTT_LOG_F(fmt, ...)
#define M_MQTT_LOG_HEX_F(tag, buf, size)
#endif

#if (M_MQTT_LOG_ERROR <= M_MQTT_LOG_LEVEL)
#define M_MQTT_LOG_E(fmt, ...)                                                                                         \
    m_mqtt_log_output(M_MQTT_LOG_ERROR, "[%s][%d]: " fmt, ##__VA_ARGS__, __FUNCTION__, __LINE__)
#define M_MQTT_LOG_HEX_E(tag, buf, size) m_mqtt_hexdump(tag, M_MQTT_HEX_WIDTH, buf, size)
#else
#define M_MQTT_LOG_E(fmt, ...)
#define M_MQTT_LOG_HEX_E(tag, buf, size)
#endif

#if (M_MQTT_LOG_WARNING <= M_MQTT_LOG_LEVEL)
#define M_MQTT_LOG_W(fmt, ...)                                                                                         \
    m_mqtt_log_output(M_MQTT_LOG_WARNING, "[%s][%d]: " fmt, ##__VA_ARGS__, __FUNCTION__, __LINE__)
#define M_MQTT_LOG_HEX_W(tag, buf, size) m_mqtt_hexdump(tag, M_MQTT_HEX_WIDTH, buf, size)
#else
#define M_MQTT_LOG_W(fmt, ...)
#define M_MQTT_LOG_HEX_W(tag, buf, size)
#endif

#if (M_MQTT_LOG_INFO <= M_MQTT_LOG_LEVEL)
#define M_MQTT_LOG_I(fmt, ...)                                                                                         \
    m_mqtt_log_output(M_MQTT_LOG_INFO, "[%s][%d]: " fmt, ##__VA_ARGS__, __FUNCTION__, __LINE__)
#define M_MQTT_LOG_HEX_I(tag, buf, size) m_mqtt_hexdump(tag, M_MQTT_HEX_WIDTH, buf, size)
#else
#define M_MQTT_LOG_I(fmt, ...)
#define M_MQTT_LOG_HEX_I(tag, buf, size)
#endif

#if (M_MQTT_LOG_DEBUG <= M_MQTT_LOG_LEVEL)
#define M_MQTT_LOG_D(fmt, ...)                                                                                         \
    m_mqtt_log_output(M_MQTT_LOG_DEBUG, "[%s][%d]: " fmt, ##__VA_ARGS__, __FUNCTION__, __LINE__)
#define M_MQTT_LOG_HEX_D(tag, buf, size) m_mqtt_hexdump(tag, M_MQTT_HEX_WIDTH, buf, size)
#else
#define M_MQTT_LOG_D(fmt, ...)
#define M_MQTT_LOG_HEX_D(tag, buf, size)
#endif

#endif /* OS_USING_DLOG */

#if defined(__cplusplus)
}
#endif /* __cplusplus */
#endif /* __M_MQTT_LOG_H__ */
