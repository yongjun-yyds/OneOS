/**
 ***********************************************************************************************************************
 * Copyright (c) 2021, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        cms_con_mqtt_publish.c
 *
 * @brief
 *
 * @revision
 * Date         Author          Notes
 * 2021-4-13   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include "m_mqtt_publish.h"
#include "m_mqtt_packet.h"
#include <string.h>
/**
 * Determines the length of the MQTT publish packet that would be produced using the supplied parameters
 * @param qos the MQTT QoS of the publish (packetid is omitted for QoS 0)
 * @param topicName the topic name to be used in the publish
 * @param payloadlen the length of the payload to be sent
 * @return the length of buffer needed to contain the serialized version of the packet
 */
static int __m_mqtt_serialize_publish_length(int qos, const char *topic_name, int payloadlen)
{
    if (qos > 0)
        return m_mqtt_strlen(topic_name) + payloadlen + 4;
    else
        return m_mqtt_strlen(topic_name) + payloadlen + 2;
}

/**
 * Serializes the supplied publish data into the supplied buffer, ready for sending
 * @param buf the buffer into which the packet will be serialized
 * @param buflen the length in bytes of the supplied buffer
 * @param dup integer - the MQTT dup flag
 * @param qos integer - the MQTT QoS value
 * @param retained integer - the MQTT retained flag
 * @param packetid integer - the MQTT packet identifier
 * @param topicName char * - the MQTT topic in the publish
 * @param payload byte buffer - the MQTT publish payload
 * @param payloadlen integer - the length of the MQTT payload
 * @return the length of the serialized data.  <= 0 indicates error
 */
int m_mqtt_serialize_publish(uint8_t       *buf,
                             size_t         buflen,
                             m_mqtt_header *header,
                             uint16_t       packetid,
                             const char    *topic_name,
                             uint8_t       *payload,
                             size_t         payloadlen)
{
    uint8_t *ptr     = buf;
    int      rem_len = 0;

    rem_len = __m_mqtt_serialize_publish_length(header->bits.qos, topic_name, payloadlen);
    if (m_mqtt_packet_len(rem_len) > buflen)
    {
        return ERR_M_MQTT_TX_BUFF_SHORT;
    }

    header->bits.type = M_MQTT_PUBLISH;
    m_mqtt_packet_write_char(&ptr, header->byte); /* write header */

    ptr += m_mqtt_packet_encode(ptr, rem_len); /* write remaining length */

    m_mqtt_packet_write_cstring(&ptr, topic_name);

    if (header->bits.qos > 0)
        m_mqtt_packet_write_int(&ptr, packetid);

    memcpy(ptr, payload, payloadlen);
    ptr += payloadlen;

    return (ptr - buf);
}

/**
 * Serializes the ack packet into the supplied buffer.
 * @param buf the buffer into which the packet will be serialized
 * @param buflen the length in bytes of the supplied buffer
 * @param type the MQTT packet type
 * @param dup the MQTT dup flag
 * @param packetid the MQTT packet identifier
 * @return serialized length, or error if 0
 */
int m_mqtt_serialize_ack(uint8_t *buf, size_t buflen, uint8_t packettype, uint8_t dup, uint16_t packetid)
{
    m_mqtt_header header = {0};
    uint8_t      *ptr    = buf;

    if (buflen < 4)
    {
        return ERR_M_MQTT_TX_BUFF_SHORT;
    }
    header.bits.type = packettype;
    header.bits.dup  = dup;
    header.bits.qos  = (packettype == M_MQTT_PUBREL) ? 1 : 0;
    m_mqtt_packet_write_char(&ptr, header.byte); /* write header */

    ptr += m_mqtt_packet_encode(ptr, 2); /* write remaining length */
    m_mqtt_packet_write_int(&ptr, packetid);

    return (ptr - buf);
}
/**
 * Serializes a puback packet into the supplied buffer.
 * @param buf the buffer into which the packet will be serialized
 * @param buflen the length in bytes of the supplied buffer
 * @param packetid integer - the MQTT packet identifier
 * @return serialized length, or error if 0
 */
int m_mqtt_serialize_puback(uint8_t *buf, size_t buflen, m_mqtt_msg_types type, uint16_t packetid)
{
    return m_mqtt_serialize_ack(buf, buflen, type, 0, packetid);
}

/**
 * Deserializes the supplied (wire) buffer into publish data
 * @param dup returned integer - the MQTT dup flag
 * @param qos returned integer - the MQTT QoS value
 * @param retained returned integer - the MQTT retained flag
 * @param packetid returned integer - the MQTT packet identifier
 * @param topic_name returned char * - the MQTT topic in the publish
 * @param payload returned byte buffer - the MQTT publish payload
 * @param payloadlen returned integer - the length of the MQTT payload
 * @param buf the raw buffer data, of the correct length determined by the remaining length field
 * @param buflen the length in bytes of the data in the supplied buffer
 * @return error code.  1 is success
 */
int m_mqtt_deserialize_publish(m_mqtt_header *header,
                               uint16_t      *packetid,
                               m_mqtt_buff   *topic,
                               uint8_t      **payload,
                               size_t        *payloadlen,
                               uint8_t       *buf,
                               size_t         buflen)
{
    uint8_t *curdata = buf;
    uint8_t *enddata = NULL;
    int      mylen   = 0;

    header->byte = m_mqtt_packet_read_char(&curdata);
    if (header->bits.type != M_MQTT_PUBLISH)
        return 0;

    curdata += m_mqtt_packet_decode_buf(curdata, &mylen); /* read remaining length */
    enddata = curdata + mylen;

    if (!m_mqtt_packet_read_len_string(topic, &curdata, enddata) ||
        enddata - curdata < 0) /* do we have enough data to read the protocol version byte? */
        return 0;

    if (header->bits.qos > 0)
        *packetid = m_mqtt_packet_read_int(&curdata);

    *payloadlen = enddata - curdata;
    *payload    = curdata;
    return 1;
}

/**
 * Deserializes the supplied (wire) buffer into an ack
 * @param packettype returned integer - the MQTT packet type
 * @param dup returned integer - the MQTT dup flag
 * @param packetid returned integer - the MQTT packet identifier
 * @param buf the raw buffer data, of the correct length determined by the remaining length field
 * @param buflen the length in bytes of the data in the supplied buffer
 * @return error code.  1 is success, 0 is failure
 */
int m_mqtt_deserialize_ack(uint8_t *packettype, uint8_t *dup, uint16_t *packetid, uint8_t *buf, size_t buflen)
{
    m_mqtt_header header  = {0};
    uint8_t      *curdata = buf;
    uint8_t      *enddata = NULL;
    int           mylen;

    header.byte = m_mqtt_packet_read_char(&curdata);
    *dup        = header.bits.dup;
    *packettype = header.bits.type;

    curdata += m_mqtt_packet_decode_buf(curdata, &mylen); /* read remaining length */
    enddata = curdata + mylen;

    if (enddata - curdata < 2)
        return 0;
    *packetid = m_mqtt_packet_read_int(&curdata);
    return 1;
}
