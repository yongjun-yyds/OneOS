/*************************************************************************************************************************
 * Copyright (c) 2022, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\ you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an \"AS IS\" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * \@file
 *
 * \@brief
 *
 * \@details
 *
 * \@revision
 * Date         Author          Notes
 * 2022-01-08   OneOS Team      first version
 ***********************************************************************************************************************/
#ifndef __M_MQTT_PACKET_H__
#define __M_MQTT_PACKET_H__
#include "m_mqtt_def.h"

#if defined(__cplusplus)
extern "C" {
#endif

typedef enum
{
    M_MQTT_CONNECT = 1, /* 连接请求 */
    M_MQTT_CONNACK,     /* 确认连接请求 */
    M_MQTT_PUBLISH,     /* 发布消息 */
    M_MQTT_PUBACK,      /* 发布确认 */
    M_MQTT_PUBREC,      /* 发布收到 */
    M_MQTT_PUBREL,      /* 发布释放 */
    M_MQTT_PUBCOMP,     /* 发布完成 */
    M_MQTT_SUBSCRIBE,   /* 订阅主题 */
    M_MQTT_SUBACK,      /* 订阅确认 */
    M_MQTT_UNSUBSCRIBE, /* 取消订阅 */
    M_MQTT_UNSUBACK,    /* 取消订阅确认 */
    M_MQTT_PINGREQ,     /* 心跳请求 */
    M_MQTT_PINGRESP,    /* 心跳响应 */
    M_MQTT_DISCONNECT   /* 断开连接 */
} m_mqtt_msg_types;

/**
 * Bitfields for the MQTT header byte.
 */
typedef union
{
    uint8_t byte; /**< the whole byte */
#if defined(REVERSED)
    struct
    {
        uint8_t type : 4;   /**< message type nibble */
        uint8_t dup : 1;    /**< DUP flag bit */
        uint8_t qos : 2;    /**< QoS value, 0, 1 or 2 */
        uint8_t retain : 1; /**< retained flag bit */
    } bits;
#else
    struct
    {
        uint8_t retain : 1; /**< retained flag bit */
        uint8_t qos : 2;    /**< QoS value, 0, 1 or 2 */
        uint8_t dup : 1;    /**< DUP flag bit */
        uint8_t type : 4;   /**< message type nibble */
    } bits;
#endif
} m_mqtt_header;

typedef struct
{
    uint8_t      *buff;
    size_t        packet_len;
    m_mqtt_header header;
    size_t        paload_len;
    uint8_t      *paload;
} m_mqtt_packet;

size_t m_mqtt_strlen(const char *str);

int m_mqtt_packet_len(int rem_len);

int m_mqtt_packet_encode(uint8_t *buf, size_t length);

int m_mqtt_packet_decode(int (*getcharfn)(uint8_t *, int), int *value);

int m_mqtt_packet_decode_buf(uint8_t *buf, int *value);

int m_mqtt_packet_read_int(uint8_t **pptr);

char m_mqtt_packet_read_char(uint8_t **pptr);

void m_mqtt_packet_write_char(uint8_t **pptr, char c);

void m_mqtt_packet_write_int(uint8_t **pptr, int anInt);

void m_mqtt_packet_write_cstring(uint8_t **pptr, const char *string);

int m_mqtt_packet_read_len_string(m_mqtt_buff *mqtt_str, uint8_t **pptr, uint8_t *enddata);

int m_mqtt_serialize_zero(uint8_t *buf, size_t buflen, uint8_t packettype);

#ifdef __cplusplus /* If this is a C++ compiler, use C linkage */
}
#endif

#endif /* __M_MQTT_PACKET_H__ */
