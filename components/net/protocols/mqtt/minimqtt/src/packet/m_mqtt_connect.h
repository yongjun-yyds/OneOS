/*************************************************************************************************************************
 * Copyright (c) 2022, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\ you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an \"AS IS\" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * \@file
 *
 * \@brief
 *
 * \@details
 *
 * \@revision
 * Date         Author          Notes
 * 2022-01-08   OneOS Team      first version
 ***********************************************************************************************************************/
#ifndef __M_MQTT_CONNECT_H__
#define __M_MQTT_CONNECT_H__
#include "m_mqtt_packet.h"

#if defined(__cplusplus)
extern "C" {
#endif

int m_mqtt_serialize_connect(uint8_t    *buf,
                             size_t      len,
                             uint32_t    keep_alive_s,
                             const char *id,
                             const char *username,
                             const char *password);

int m_mqtt_deserialize_connect_ack(uint8_t *sessionPresent, uint8_t *connack_rc, uint8_t *buf, size_t buflen);

int m_mqtt_serialize_disconnect(uint8_t *buf, size_t buflen);

int m_mqtt_serialize_pingreq(uint8_t *buf, size_t buflen);

#if defined(__cplusplus)
}
#endif

#endif /* __M_MQTT_CONNECT_H__ */
