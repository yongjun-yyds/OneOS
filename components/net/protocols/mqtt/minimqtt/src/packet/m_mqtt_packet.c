/**
 ***********************************************************************************************************************
 * Copyright (c) 2021, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        cms_con_mqtt_packet.c
 *
 * @brief
 *
 * @revision
 * Date         Author          Notes
 * 2021-4-13   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include "m_mqtt_packet.h"
#include <string.h>

/**
 * Encodes the message length according to the MQTT algorithm
 * @param str the string to be calculated
 * @return length of str
 */
size_t m_mqtt_strlen(const char *str)
{
    if (str == NULL)
        return 0;
    return strlen(str);
}

/**
 * Encodes the message length according to the MQTT algorithm
 * @param buf the buffer into which the encoded data is written
 * @param length the length to be encoded
 * @return the number of bytes written to buffer
 */
int m_mqtt_packet_encode(uint8_t *buf, size_t length)
{
    size_t rc = 0;

    do
    {
        char d = length % 128;
        length /= 128;
        /* if there are more digits to encode, set the top bit of this digit */
        if (length > 0)
            d |= 0x80;
        buf[rc++] = d;
    } while (length > 0);

    return rc;
}

/**
 * Decodes the message length according to the MQTT algorithm
 * @param getcharfn pointer to function to read the next character from the data source
 * @param value the decoded length returned
 * @return the number of bytes read from the socket
 */
int m_mqtt_packet_decode(int (*getcharfn)(uint8_t *, int), int *value)
{
    uint8_t c;
    int     multiplier = 1;
    int     len        = 0;
#define MAX_NO_OF_REMAINING_LENGTH_BYTES 4

    *value = 0;
    do
    {
        int rc = ERR_M_MQTT_RX_DATA;

        if (++len > MAX_NO_OF_REMAINING_LENGTH_BYTES)
        {
            return ERR_M_MQTT_RX_DATA; /* bad data */
        }
        rc = (*getcharfn)(&c, 1);
        if (rc != 1)
            goto exit;
        *value += (c & 127) * multiplier;
        multiplier *= 128;
    } while ((c & 128) != 0);
exit:
    return len;
}

int m_mqtt_packet_len(int rem_len)
{
    rem_len++; /* header byte */

    /* now remaining_length field */
    if (rem_len < 128)
        rem_len++;
    else if (rem_len < 16384)
        rem_len += 2;
    else if (rem_len < 2097151)
        rem_len += 3;
    else
        rem_len += 4;
    return rem_len;
}

static uint8_t *bufptr;

static int bufchar(uint8_t *c, int count)
{
    int i;

    for (i = 0; i < count; ++i)
        *c = *bufptr++;
    return count;
}

int m_mqtt_packet_decode_buf(uint8_t *buf, int *value)
{
    bufptr = buf;
    return m_mqtt_packet_decode(bufchar, value);
}

/**
 * Calculates an integer from two bytes read from the input buffer
 * @param pptr pointer to the input buffer - incremented by the number of bytes used & returned
 * @return the integer value calculated
 */
int m_mqtt_packet_read_int(uint8_t **pptr)
{
    uint8_t *ptr = *pptr;
    int      len = 256 * (*ptr) + (*(ptr + 1));
    *pptr += 2;
    return len;
}

/**
 * Reads one character from the input buffer.
 * @param pptr pointer to the input buffer - incremented by the number of bytes used & returned
 * @return the character read
 */
char m_mqtt_packet_read_char(uint8_t **pptr)
{
    char c = **pptr;
    (*pptr)++;
    return c;
}

/**
 * Writes one character to an output buffer.
 * @param pptr pointer to the output buffer - incremented by the number of bytes used & returned
 * @param c the character to write
 */
void m_mqtt_packet_write_char(uint8_t **pptr, char c)
{
    **pptr = c;
    (*pptr)++;
}

/**
 * Writes an integer as 2 bytes to an output buffer.
 * @param pptr pointer to the output buffer - incremented by the number of bytes used & returned
 * @param anInt the integer to write
 */
void m_mqtt_packet_write_int(uint8_t **pptr, int anInt)
{
    **pptr = (uint8_t)(anInt / 256);
    (*pptr)++;
    **pptr = (uint8_t)(anInt % 256);
    (*pptr)++;
}

/**
 * Writes a "UTF" string to an output buffer.  Converts C string to length-delimited.
 * @param pptr pointer to the output buffer - incremented by the number of bytes used & returned
 * @param string the C string to write
 */
void m_mqtt_packet_write_cstring(uint8_t **pptr, const char *string)
{
    int len = strlen(string);
    m_mqtt_packet_write_int(pptr, len);
    memcpy(*pptr, string, len);
    *pptr += len;
}

/**
 * @param mqttstring the MQTTString structure into which the data is to be read
 * @param pptr pointer to the output buffer - incremented by the number of bytes used & returned
 * @param enddata pointer to the end of the data: do not read beyond
 * @return 1 if successful, 0 if not
 */
int m_mqtt_packet_read_len_string(m_mqtt_buff *mqtt_str, uint8_t **pptr, uint8_t *enddata)
{
    int rc = 0;

    /* the first two bytes are the length of the string */
    if (enddata - (*pptr) > 1) /* enough length to read the integer? */
    {
        mqtt_str->len = m_mqtt_packet_read_int(pptr); /* increments pptr to point past length */
        if (&(*pptr)[mqtt_str->len] <= enddata)
        {
            mqtt_str->buff = (uint8_t *)*pptr;
            *pptr += mqtt_str->len;
            rc = 1;
        }
    }
    return rc;
}

/**
 * Serializes a 0-length packet into the supplied buffer, ready for writing to a socket
 * @param buf the buffer into which the packet will be serialized
 * @param buflen the length in bytes of the supplied buffer, to avoid overruns
 * @param packettype the message type
 * @return serialized length, or error if 0
 */
int m_mqtt_serialize_zero(uint8_t *buf, size_t buflen, uint8_t packettype)
{
    m_mqtt_header  header = {0};
    unsigned char *ptr    = buf;

    if (buflen < 2)
    {
        return ERR_M_MQTT_TX_BUFF_SHORT;
    }
    header.byte      = 0;
    header.bits.type = packettype;
    m_mqtt_packet_write_char(&ptr, header.byte); /* write header */

    ptr += m_mqtt_packet_encode(ptr, 0); /* write remaining length */
    return (ptr - buf);
}
