/**
 ***********************************************************************************************************************
 * Copyright (c) 2021, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        cms_con_mqtt_subscribe.c
 *
 * @brief
 *
 * @revision
 * Date         Author          Notes
 * 2021-4-13   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include "m_mqtt_unsubscribe.h"
#include "m_mqtt_packet.h"
#include <string.h>
/**
 * Serializes the supplied unsubscribe data into the supplied buffer, ready for sending
 * @param buf the raw buffer data, of the correct length determined by the remaining length field
 * @param buflen the length in bytes of the data in the supplied buffer
 * @param dup integer - the MQTT dup flag
 * @param packetid integer - the MQTT packet identifier
 * @param topic_filter - array of topic filter names
 * @return the length of the serialized data.  <= 0 indicates error
 */
int m_mqtt_serialize_unsubscribe(uint8_t *buf, size_t buflen, uint8_t dup, uint16_t packet_id, const char *topic_filter)
{
    uint8_t      *ptr     = buf;
    m_mqtt_header header  = {0};
    int           rem_len = 0;

    rem_len = strlen(topic_filter) + 2 + 2; /* topic + packetid + topic length*/
    if (m_mqtt_packet_len(rem_len) > buflen)
    {
        return ERR_M_MQTT_TX_BUFF_SHORT;
    }

    header.byte      = 0;
    header.bits.type = M_MQTT_UNSUBSCRIBE;
    header.bits.dup  = dup;
    header.bits.qos  = 1;
    m_mqtt_packet_write_char(&ptr, header.byte); /* write header */

    ptr += m_mqtt_packet_encode(ptr, rem_len); /* write remaining length */

    m_mqtt_packet_write_int(&ptr, packet_id);
    m_mqtt_packet_write_cstring(&ptr, topic_filter);

    return (ptr - buf);
}

/**
 * Deserializes the supplied (wire) buffer into unsuback data
 * @param packetid returned integer - the MQTT packet identifier
 * @param buf the raw buffer data, of the correct length determined by the remaining length field
 * @param buflen the length in bytes of the data in the supplied buffer
 * @return error code.  1 is success, 0 is failure
 */
int m_mqtt_deserialize_unsub_ack(uint16_t *packetid, uint8_t *buf, size_t buflen)
{
    m_mqtt_header header  = {0};
    uint8_t      *curdata = buf;
    uint8_t      *enddata = NULL;
    int           mylen;

    header.byte = m_mqtt_packet_read_char(&curdata);
    if (M_MQTT_UNSUBACK != header.bits.type)
        return 0;

    curdata += m_mqtt_packet_decode_buf(curdata, &mylen); /* read remaining length */
    enddata = curdata + mylen;

    if (enddata - curdata < 2)
        return 0;
    *packetid = m_mqtt_packet_read_int(&curdata);

    return 1;
}
