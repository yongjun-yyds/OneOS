/*************************************************************************************************************************
 * Copyright (c) 2022, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\ you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an \"AS IS\" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * \@file
 *
 * \@brief
 *
 * \@details
 *
 * \@revision
 * Date         Author          Notes
 * 2022-01-08   OneOS Team      first version
 ***********************************************************************************************************************/
#ifndef __M_MQTT_PUBLISH_H__
#define __M_MQTT_PUBLISH_H__
#include "m_mqtt_packet.h"

#if defined(__cplusplus)
extern "C" {
#endif

int m_mqtt_serialize_publish(uint8_t       *buf,
                             size_t         buflen,
                             m_mqtt_header *header,
                             uint16_t       packetid,
                             const char    *topic_name,
                             uint8_t       *payload,
                             size_t         payloadlen);

int m_mqtt_deserialize_publish(m_mqtt_header *header,
                               uint16_t      *packetid,
                               m_mqtt_buff   *topic,
                               uint8_t      **payload,
                               size_t        *payloadlen,
                               uint8_t       *buf,
                               size_t         buflen);

int m_mqtt_serialize_ack(uint8_t *buf, size_t buflen, uint8_t packettype, uint8_t dup, uint16_t packetid);

int m_mqtt_serialize_puback(uint8_t *buf, size_t buflen, m_mqtt_msg_types type, uint16_t packetid);

int m_mqtt_deserialize_ack(uint8_t *packettype, uint8_t *dup, uint16_t *packetid, uint8_t *buf, size_t buflen);

#if defined(__cplusplus)
}
#endif

#endif /* __M_MQTT_PUBLISH_H__ */
