/**
 ***********************************************************************************************************************
 * Copyright (c) 2021, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        m_mqtt_connect.c
 *
 * @brief
 *
 * @revision
 * Date         Author          Notes
 * 2021-4-13   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include "m_mqtt_connect.h"
#include "m_mqtt_packet.h"
#include <string.h>

typedef union
{
    unsigned char all; /**< all connect flags */
#if defined(REVERSED)
    struct
    {
        unsigned int username : 1;     /**< 3.1 user name */
        unsigned int password : 1;     /**< 3.1 password */
        unsigned int willRetain : 1;   /**< will retain setting */
        unsigned int willQoS : 2;      /**< will QoS value */
        unsigned int will : 1;         /**< will flag */
        unsigned int cleansession : 1; /**< clean session flag */
        unsigned int : 1;              /**< unused */
    } bits;
#else
    struct
    {
        unsigned int : 1;              /**< unused */
        unsigned int cleansession : 1; /**< cleansession flag */
        unsigned int will : 1;         /**< will flag */
        unsigned int willQoS : 2;      /**< will QoS value */
        unsigned int willRetain : 1;   /**< will retain setting */
        unsigned int password : 1;     /**< 3.1 password */
        unsigned int username : 1;     /**< 3.1 user name */
    } bits;
#endif
} m_mqtt_connect_flags; /**< connect flags byte */

typedef union
{
    unsigned char all; /**< all connack flags */
#if defined(REVERSED)
    struct
    {
        unsigned int reserved : 7;       /**< unused */
        unsigned int sessionpresent : 1; /**< session present flag */
    } bits;
#else
    struct
    {
        unsigned int sessionpresent : 1; /**< session present flag */
        unsigned int reserved : 7;       /**< unused */
    } bits;
#endif
} m_mqtt_connack_flags; /**< connack flags byte */

/**
 * Serializes the connect options into the buffer.
 * @param buf the buffer into which the packet will be serialized
 * @param len the length in bytes of the supplied buffer
 * @param options the options to be used to build the connect packet
 * @return serialized length, or error if 0
 */
int m_mqtt_serialize_connect(uint8_t    *buf,
                             size_t      len,
                             uint32_t    keep_alive_s,
                             const char *id,
                             const char *username,
                             const char *password)
{
    unsigned char       *ptr     = buf;
    m_mqtt_header        header  = {0};
    m_mqtt_connect_flags flags   = {0};
    int                  rem_len = 10;

    if (id == NULL)
        return ERR_M_MQTT_PARAM;
    rem_len += strlen(id) + 2;
    if (username != NULL)
        rem_len += strlen(username) + 2;
    if (password != NULL)
        rem_len += strlen(password) + 2;

    if (m_mqtt_packet_len(rem_len) > len)
        return ERR_M_MQTT_TX_BUFF_SHORT;

    header.byte      = 0;
    header.bits.type = M_MQTT_CONNECT;
    m_mqtt_packet_write_char(&ptr, header.byte); /* write header */

    ptr += m_mqtt_packet_encode(ptr, rem_len); /* write remaining length */
    m_mqtt_packet_write_cstring(&ptr, "MQTT");
    m_mqtt_packet_write_char(&ptr, (char)4);

    flags.all               = 0;
    flags.bits.cleansession = 1;
    if (username != NULL)
        flags.bits.username = 1;
    if (password != NULL)
        flags.bits.password = 1;

    m_mqtt_packet_write_char(&ptr, flags.all);
    m_mqtt_packet_write_int(&ptr, keep_alive_s);
    m_mqtt_packet_write_cstring(&ptr, id);
    if (flags.bits.username)
        m_mqtt_packet_write_cstring(&ptr, username);
    if (flags.bits.password)
        m_mqtt_packet_write_cstring(&ptr, password);

    return (ptr - buf);
}

/**
 * Deserializes the supplied (wire) buffer into connack data - return code
 * @param sessionPresent the session present flag returned (only for MQTT 3.1.1)
 * @param connack_rc returned integer value of the connack return code
 * @param buf the raw buffer data, of the correct length determined by the remaining length field
 * @param len the length in bytes of the data in the supplied buffer
 * @return error code.  1 is success, 0 is failure
 */
int m_mqtt_deserialize_connect_ack(uint8_t *sessionPresent, uint8_t *connack_rc, uint8_t *buf, size_t buflen)
{
    m_mqtt_header        header  = {0};
    unsigned char       *curdata = buf;
    unsigned char       *enddata = NULL;
    int                  mylen;
    m_mqtt_connack_flags flags = {0};

    header.byte = m_mqtt_packet_read_char(&curdata);
    if (header.bits.type != M_MQTT_CONNACK)
        return 0;

    curdata += m_mqtt_packet_decode_buf(curdata, &mylen); /* read remaining length */
    enddata = curdata + mylen;
    if (enddata - curdata < 2)
        return 0;

    flags.all       = m_mqtt_packet_read_char(&curdata);
    *sessionPresent = flags.bits.sessionpresent;
    *connack_rc     = m_mqtt_packet_read_char(&curdata);

    return 1;
}

/**
 * Serializes a disconnect packet into the supplied buffer, ready for writing to a socket
 * @param buf the buffer into which the packet will be serialized
 * @param buflen the length in bytes of the supplied buffer, to avoid overruns
 * @return serialized length, or error if 0
 */
int m_mqtt_serialize_disconnect(uint8_t *buf, size_t buflen)
{
    return m_mqtt_serialize_zero(buf, buflen, M_MQTT_DISCONNECT);
}

/**
 * Serializes a disconnect packet into the supplied buffer, ready for writing to a socket
 * @param buf the buffer into which the packet will be serialized
 * @param buflen the length in bytes of the supplied buffer, to avoid overruns
 * @return serialized length, or error if 0
 */
int m_mqtt_serialize_pingreq(uint8_t *buf, size_t buflen)
{
    return m_mqtt_serialize_zero(buf, buflen, M_MQTT_PINGREQ);
}
