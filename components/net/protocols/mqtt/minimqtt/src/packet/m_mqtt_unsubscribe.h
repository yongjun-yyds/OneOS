/*************************************************************************************************************************
 * Copyright (c) 2022, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\ you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an \"AS IS\" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * \@file
 *
 * \@brief
 *
 * \@details
 *
 * \@revision
 * Date         Author          Notes
 * 2022-01-08   OneOS Team      first version
 ***********************************************************************************************************************/
#ifndef __CMS_CON_MQTT_UNSUBSCRIBE_H__
#define __CMS_CON_MQTT_UNSUBSCRIBE_H__
#include <stdint.h>
#include <stddef.h>

#if defined(__cplusplus)
extern "C" {
#endif

int m_mqtt_serialize_unsubscribe(uint8_t    *buf,
                                 size_t      buflen,
                                 uint8_t     dup,
                                 uint16_t    packet_id,
                                 const char *topic_filter);

int m_mqtt_deserialize_unsub_ack(uint16_t *packetid, uint8_t *buf, size_t buflen);

#if defined(__cplusplus)
}
#endif

#endif
