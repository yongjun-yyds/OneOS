/**
 ***********************************************************************************************************************
 * Copyright (c) 2022, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\ you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an \"AS IS\" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * \@file
 *
 * \@brief
 *
 * \@details
 *
 * \@revision
 * Date         Author          Notes
 * 2022-01-08   OneOS Team      first version
 ***********************************************************************************************************************
 */

#ifndef __M_MQTT_COMMMON_H__
#define __M_MQTT_COMMMON_H__
#include "m_mqtt_def.h"

#if defined(__cplusplus)
extern "C" {
#endif /* __cplusplus */

typedef struct _m_mqtt_message_handler_list_
{
    struct _m_mqtt_message_handler_list_ *next;
    char                                 *topic_filter;
    m_mqtt_message_handler                message_handler;
} m_mqtt_message_handler_list;

int m_mqtt_buff_creat(m_mqtt_buff *mqtt_buff, size_t len);

int m_mqtt_handler_list_add(m_mqtt_message_handler_list **head,
                            const char                   *topic,
                            m_mqtt_message_handler        message_handler);

int m_mqtt_handler_list_del(m_mqtt_message_handler_list **head, const char *topic);

void m_mqtt_handler_list_free(m_mqtt_message_handler_list **head);

m_mqtt_message_handler m_mqtt_handler_list_search(m_mqtt_message_handler_list *head, const char *topic);

#if defined(__cplusplus)
}
#endif /* __cplusplus */
#endif /* __M_MQTT_COMMMON_H__ */
