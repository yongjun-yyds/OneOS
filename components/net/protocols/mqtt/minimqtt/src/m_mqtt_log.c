/***********************************************************************************************************************
 * Copyright (c) 2021, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file
 *
 * @brief
 *
 * @revision
 * Date         Author          Notes
 * 2022-01-08   OneOS Team      First Version
 ***********************************************************************************************************************/
#include "m_mqtt_log.h"
#include <stdio.h>
#include <os_util.h>

#if defined(USING_M_MQTT_LOG)

static char str_buf[256];
void        m_mqtt_log_output(int level, const char *format, ...)
{
    if (level > M_MQTT_LOG_LEVEL)
        return;
    switch (level)
    {
    case M_MQTT_LOG_FATAL:
        os_kprintf("FATAL");
        break;
    case M_MQTT_LOG_ERROR:
        os_kprintf("ERROR");
        break;
    case M_MQTT_LOG_WARNING:
        os_kprintf("WARNING");
        break;
    case M_MQTT_LOG_INFO:
        os_kprintf("INFO");
        break;
    case M_MQTT_LOG_DEBUG:
        os_kprintf("DEBUG");
        break;
    default:
        break;
    }
    va_list args;
    va_start(args, format);
    vsnprintf(str_buf, sizeof(str_buf), format, args);
    va_end(args);
    os_kprintf("%s\r\n", str_buf);
    return;
}

void m_mqtt_hexdump(const char *tag, size_t width, const unsigned char *buf, int size)
{
    if (tag != NULL)
    {
        os_kprintf("%s\r\n", tag);
    }
    int i = 0;
    for (; i < size;)
    {
        os_kprintf("%02x ", buf[i]);
        ++i;
        if (i % width == 0)
            os_kprintf("\r\n");
    }
    if (i % width)
        os_kprintf("\r\n");
}

#endif /* USING_M_MQTT_LOG */
