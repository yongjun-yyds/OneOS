/***********************************************************************************************************************
 * Copyright (c) 2021, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file
 *
 * @brief
 *
 * @revision
 * Date         Author          Notes
 * 2022-01-08   OneOS Team      First Version
 ***********************************************************************************************************************/
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/time.h>
#include <netdb.h>
#include <stdlib.h>
#include <string.h>
#include <os_assert.h>

static int net_connect(const char *host, const char *port)
{
    struct addrinfo *addr_list;
    struct addrinfo *addr;
    struct addrinfo  addr_info;
    int              fd = -1;

    memset(&addr_info, 0, sizeof(struct addrinfo));
    addr_info.ai_socktype = SOCK_STREAM;
    addr_info.ai_protocol = IPPROTO_TCP;

    if (getaddrinfo(host, port, &addr_info, &addr_list))
        return fd;
    for (addr = addr_list; addr != NULL; addr = addr->ai_next)
    {
        fd = socket(addr->ai_family, addr->ai_socktype, addr->ai_protocol);
        if (fd < 0)
        {
            continue;
        }
        if (connect(fd, addr->ai_addr, addr->ai_addrlen) == 0)
        {
            break;
        }
        closesocket(fd);
        fd = -1;
    }
    freeaddrinfo(addr_list);
    return fd;
}

static int net_send_with_timeout(int fd, const void *buff, size_t len, int timeout_ms)
{
    if (timeout_ms)
    {
        struct timeval interval = {timeout_ms / 1000, (timeout_ms % 1000) * 1000};
        setsockopt(fd, SOL_SOCKET, SO_SNDTIMEO, (char *)&interval, sizeof(struct timeval));
    }
    return send(fd, buff, len, 0);
}

static int net_recv_with_timeout(int fd, void *buff, size_t max_len, int timeout_ms)
{
    fd_set         read_fds;
    struct timeval interval;
    int            rc;

    FD_ZERO(&read_fds);
    FD_SET(fd, &read_fds);
    interval.tv_sec  = timeout_ms / 1000;
    interval.tv_usec = (timeout_ms % 1000) * 1000 + 1;

    rc = select(fd + 1, &read_fds, NULL, NULL, &interval);
    if (rc <= 0)
    {
        return rc;
    }
    rc = recv(fd, buff, max_len, 0);
    if (rc <= 0)
    {
        closesocket(fd);
        rc = -1;
    }
    return rc;
}
