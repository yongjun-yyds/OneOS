# Mini MQTT协议组件

------

## 简介

MQTT是物联网领域常见的应用层传输协议，在多个应用场景均有MQTT协议的身影。

由于大多数物联网设备对硬件资源有着极其严格的要求；因此，我们在Paho MQTT的基础上，进一步优化MQTT Client的资源占用。

对比OneOS移植的Paho MQTT，Mini MQTT对RAM和ROM的要求仅为Paho MQTT的60%~70%（默认配置）。

## 使用说明

启动OneOS-cube，进入menuconfig，配置路径：(Top) → Components→ Network→ Protocols→ MQTT ，选择MQTT version为Mini MQTT

```shell
(Top) → Components→ Network→ Protocols→ MQTT→ Enable MQTT Stack → Mini MQTT
                                                OneOS Configuration
[*] Enable mini MQTT
[*]     Enable mini MQTT auto yield (NEW)
(2048)      Task stack depth (NEW)
[ ]     Enable mini MQTT QoS2 (NEW)
[*]     Enable mini MQTT client example (NEW)
        Log Level (Fatal)  --->
```

进行MQTT选项配置需要先在Menuconfig中选中Enable mini MQTT，然后再进行其他的配置选择。

- Enable mini MQTT：使能mini MQTT协议栈。
- Enable mini MQTT auto yield：MQTT协议栈自动调度; 若未使能，需要用户循环调用m_mqtt_yield实现服务端消息监听、心跳保持等业务。
- Task stack depth：MQTT自动调度协议栈大小。
- Enable mini MQTT QoS2 ：使能QoS2；关闭该功能可以减少组件尺寸。
- Enable mini MQTT client example：使能Mini MQTT客户端示例。
- Log Level：日志等级。

## API使用说明

[Mini MQTT API使用说明手册](doc/m_mqtt_api.md)

## 注意事项

#### 1. Mini MQTT不支持加密传输

若需要使用MQTTS接入，请选择Paho MQTT。