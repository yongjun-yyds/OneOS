# Mini MQTT API

------

## 简介

MQTT是物联网领域常见的应用层传输协议，在多个应用场景均有MQTT协议的身影。

由于大多数物联网设备对硬件资源有着极其严格的要求；因此，我们在Paho MQTT的基础上，进一步优化MQTT Client的资源占用。

## 枚举

**m_mqtt_err_code**

```c
typedef enum
{
    ERR_M_MQTT_SUCCESS          = 0,   /* 成功 */
    ERR_M_MQTT_GENERAL          = -1,  /* 通用错误码 */
    ERR_M_MQTT_PARAM            = -2,  /* 输入错误 */
    ERR_M_MQTT_OUT_OF_MEMORY    = -3,  /* 内存不足 */
    ERR_M_MQTT_NET              = -4,  /* 网络错误 */
    ERR_M_MQTT_TX_BUFF_SHORT    = -5,  /* TX缓存不足 */
    ERR_M_MQTT_RX_DATA          = -6,  /* 接收数据读取错误 */
    ERR_M_MQTT_SERVER           = -7,  /* 服务端返回错误 */
    ERR_M_MQTT_NOT_SUPPORT_TYPE = -8,  /* 不支持的服务类型 */
    ERR_M_MQTT_NOT_CONNECT      = -9,  /* 未连接到服务器 */
    ERR_M_MQTT_NOT_SUPPORT_QOS2 = -10, /* 不支持QoS2 */
} m_mqtt_err_code;
```

**m_mqtt_connect_state**

```c
typedef enum
{
    mqtt_not_connected,
    mqtt_connected
} m_mqtt_connect_state;
```



## API列表

| **接口**            | **说明**                 |
| :------------------ | :----------------------- |
| m_mqtt_create       | 创建mqtt实例             |
| m_mqtt_destory      | 销毁mqtt实例             |
| m_mqtt_connect      | mqtt连接到服务端         |
| m_mqtt_disconnect   | mqtt断开连接             |
| m_mqtt_subscribe    | mqtt向服务端订阅消息     |
| m_mqtt_unsubscribe  | mqtt消息去订阅           |
| m_mqtt_publish      | mqtt发布消息             |
| m_mqtt_ping_request | mqtt向服务端发送ping消息 |
| m_mqtt_get_state    | 查询mqtt当前连接状态     |
| m_mqtt_yield        | mqtt事务处理             |



## m_mqtt_create

该函数用于创建mqtt实例，需要用户指定超时时间、发送区缓存大小、接收区缓存大小。

```c
void *m_mqtt_create(const m_mqtt_param *param);
```

| **参数** | **说明**                                                   |
| :------- | :--------------------------------------------------------- |
| param    | 初始化参数，包含超时时间、发送区缓存大小、接收区缓存大小。 |
| **返回** | **说明**                                                   |
| void *   | NULL: 创建失败；非NULL：创建成功，为mqtt操作句柄。         |

## m_mqtt_destory

该函数用于销毁mqtt实例。

```c
void m_mqtt_destory(void *mqtt_handle);
```

| **参数**    | **说明**     |
| :---------- | :----------- |
| mqtt_handle | mqtt操作句柄 |
| **返回**    | **说明**     |
| void        |              |

## m_mqtt_connect

该函数用于客户端连接到服务器。

```c
int m_mqtt_connect(void *mqtt_handle, const m_mqtt_connect_param *connect_param);
```

| **参数**      | **说明**                                                                                |
| :------------ | :-------------------------------------------------------------------------------------- |
| mqtt_handle   | mqtt操作句柄                                                                            |
| connect_param | 连接相关参数，包含：服务器地址、端口、保活间隔(0为不发送ping包)、客户端ID、用户名、密码 |
| **返回**      | **说明**                                                                                |
| int           | 0：成功；非0：失败，参考m_mqtt_err_code。                                               |

## m_mqtt_disconnect

该函数用于断开与服务端的连接：

```c
void m_mqtt_disconnect(void *mqtt_handle);
```

| **参数**    | **说明**     |
| :---------- | :----------- |
| mqtt_handle | mqtt操作句柄 |
| **返回**    | **说明**     |
| void        |              |

## m_mqtt_subscribe

该函数用于客户端向服务器订阅消息。

```c
int m_mqtt_subscribe(void                  *mqtt_handle,
                     const char            *topic_filter,
                     m_mqtt_qos             qos,
                     m_mqtt_message_handler message_handler);
```

| **参数**        | **说明**                                  |
| :-------------- | :---------------------------------------- |
| mqtt_handle     | mqtt操作句柄                              |
| topic_filter    | 需要订阅的主题                            |
| qos             | QoS等级                                   |
| message_handler | 消息处理函数指针                          |
| **返回**        | **说明**                                  |
| int             | 0：成功；非0：失败，参考m_mqtt_err_code。 |

## m_mqtt_unsubscribe

该函数用于客户端向服务器**取消**订阅消息。

```c
int m_mqtt_unsubscribe(void *mqtt_handle, const char *topic_filter);
```

| **参数**     | **说明**                                  |
| :----------- | :---------------------------------------- |
| mqtt_handle  | mqtt操作句柄                              |
| topic_filter | 需要取消订阅的主题                        |
| **返回**     | **说明**                                  |
| int          | 0：成功；非0：失败，参考m_mqtt_err_code。 |

## m_mqtt_publish

该函数用于向服务端发布消息。

```c
int m_mqtt_publish(void *mqtt_handle, const char *topic, m_mqtt_message *message);
```

| **参数**     | **说明**                                    |
| :----------- | :------------------------------------------ |
| mqtt_handle  | mqtt操作句柄                                |
| topic_filter | 发布消息的主题                              |
| message      | 消息主体，包括：QoS等级，消息内容，消息长度 |
| **返回**     | **说明**                                    |
| int          | 0：成功；非0：失败，参考m_mqtt_err_code。   |

## m_mqtt_ping_request

该函数用于客户端向服务端发送ping报文。

```c
int m_mqtt_ping_request(void *mqtt_handle);
```

| **参数**    | **说明**                                  |
| :---------- | :---------------------------------------- |
| mqtt_handle | mqtt操作句柄                              |
| **返回**    | **说明**                                  |
| int         | 0：成功；非0：失败，参考m_mqtt_err_code。 |

## m_mqtt_get_state

该函数用于实现MQTT消息订阅，该函数原型如下：

```c
int m_mqtt_get_state(void *mqtt_handle);
```

| **参数**    | **说明**                                     |
| :---------- | :------------------------------------------- |
| mqtt_handle | mqtt操作句柄                                 |
| **返回**    | **说明**                                     |
| int         | 查询mqtt连接状态，参考m_mqtt_connect_state。 |

### m_mqtt_yield

该函数用于处理mqtt事务，仅当未开启自动调度时用户可用。

```c
int m_mqtt_yield(void *mqtt_handle);
```

| **参数**    | **说明**                                  |
| :---------- | :---------------------------------------- |
| mqtt_handle | mqtt操作句柄                              |
| **返回**    | **说明**                                  |
| int         | 0：成功；非0：失败，参考m_mqtt_err_code。 |

