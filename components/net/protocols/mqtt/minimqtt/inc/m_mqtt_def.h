/**
 ***********************************************************************************************************************
 * Copyright (c) 2022, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\ you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an \"AS IS\" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * \@file
 *
 * \@brief
 *
 * \@details
 *
 * \@revision
 * Date         Author          Notes
 * 2022-01-08   OneOS Team      first version
 ***********************************************************************************************************************
 */

#ifndef __M_MQTT_DEF_H__
#define __M_MQTT_DEF_H__
#include <stdint.h>
#include <stddef.h>

#if defined(__cplusplus)
extern "C" {
#endif /* __cplusplus */

#ifndef TRUE
#define TRUE (1)
#endif
#ifndef FALSE
#define FALSE (0)
#endif

typedef enum
{
    ERR_M_MQTT_SUCCESS          = 0,   /* 成功 */
    ERR_M_MQTT_GENERAL          = -1,  /* 通用错误码 */
    ERR_M_MQTT_PARAM            = -2,  /* 输入错误 */
    ERR_M_MQTT_OUT_OF_MEMORY    = -3,  /* 内存不足 */
    ERR_M_MQTT_NET              = -4,  /* 网络错误 */
    ERR_M_MQTT_TX_BUFF_SHORT    = -5,  /* TX缓存不足 */
    ERR_M_MQTT_RX_DATA          = -6,  /* 接收数据读取错误 */
    ERR_M_MQTT_SERVER           = -7,  /* 服务端返回错误 */
    ERR_M_MQTT_NOT_SUPPORT_TYPE = -8,  /* 不支持的服务类型 */
    ERR_M_MQTT_NOT_CONNECT      = -9,  /* 未连接到服务器 */
    ERR_M_MQTT_NOT_SUPPORT_QOS2 = -10, /* 不支持QoS2 */
} m_mqtt_err_code;

typedef enum
{
    mqtt_qos0,
    mqtt_qos1,
    mqtt_qos2,
    mqtt_sub_fail = 0x80
} m_mqtt_qos;

typedef enum
{
    mqtt_not_connected,
    mqtt_connected
} m_mqtt_connect_state;

typedef struct
{
    uint8_t *buff;
    size_t   len;
} m_mqtt_buff;

typedef void (*m_mqtt_message_handler)(void *mqtt_handle, const char *topic_name, void *payload, size_t payload_len);

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* __M_MQTT_DEF_H__ */
