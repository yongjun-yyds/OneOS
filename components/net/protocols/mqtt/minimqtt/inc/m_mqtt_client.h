/**
 ***********************************************************************************************************************
 * Copyright (c) 2022, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\ you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an \"AS IS\" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * \@file
 *
 * \@brief
 *
 * \@details
 *
 * \@revision
 * Date         Author          Notes
 * 2022-01-08   OneOS Team      first version
 ***********************************************************************************************************************
 */

#ifndef __M_MQTT_CLIENT_H__
#define __M_MQTT_CLIENT_H__
#include "m_mqtt_def.h"

#if defined(__cplusplus)
extern "C" {
#endif /* __cplusplus */

typedef struct
{
    uint32_t timeout;      /* 消息超时时间，毫秒 */
    size_t   sendbuf_size; /* mqtt实例发送区缓存大小 */
    size_t   recvbuf_size; /* mqtt实例接收区缓存大小 */
} m_mqtt_param;

typedef struct
{
    char    *server_addr; /* 服务器地址 */
    char    *port;        /* 服务器端口 */
    uint32_t keep_alive;  /* 保活时间，秒 */
    char    *id;          /* 客户端ID*/
    char    *username;    /* 用户名 */
    char    *password;    /* 密码 */
} m_mqtt_connect_param;

typedef struct
{
    m_mqtt_qos qos;        /* qos等级 */
    void      *payload;    /* 消息主体 */
    size_t     payloadlen; /* 消息长度 */
} m_mqtt_message;

void *m_mqtt_create(const m_mqtt_param *param);

void m_mqtt_destory(void *mqtt_handle);

int m_mqtt_connect(void *mqtt_handle, const m_mqtt_connect_param *connect_param);

void m_mqtt_disconnect(void *mqtt_handle);

int m_mqtt_subscribe(void                  *mqtt_handle,
                     const char            *topic_filter,
                     m_mqtt_qos             qos,
                     m_mqtt_message_handler message_handler);

int m_mqtt_unsubscribe(void *mqtt_handle, const char *topic_filter);

int m_mqtt_publish(void *mqtt_handle, const char *topic, m_mqtt_message *message);

int m_mqtt_ping_request(void *mqtt_handle);

int m_mqtt_get_state(void *mqtt_handle);

#if !defined(NET_USING_MINI_MQTT_AUTO_YIELD)
int m_mqtt_yield(void *mqtt_handle);
#endif /* NET_USING_MINI_MQTT_AUTO_YIELD */

#if defined(__cplusplus)
}
#endif /* __cplusplus */
#endif /* __M_MQTT_CLIENT_H__ */
