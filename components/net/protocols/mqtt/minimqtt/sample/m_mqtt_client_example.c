/***********************************************************************************************************************
 * Copyright (c) 2021, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file
 *
 * @brief
 *
 * @revision
 * Date         Author          Notes
 * 2022-01-08   OneOS Team      First Version
 ***********************************************************************************************************************/

#include "oneos_config.h"
#if defined(NET_USING_MINI_MQTT_CLIENT_EXAMPLE)
#include "m_mqtt_client.h"
#include <os_task.h>
#include <shell.h>
#include <atest.h>
#include <os_errno.h>
#include <os_assert.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#if !defined(MQTT_AUTO_YIELD_STACK_DEPTH)
#define MQTT_AUTO_YIELD_STACK_DEPTH 4096
#endif
#define MQTT_ATEST_BUFF_DEEP     128
#define MQTT_SERVER_HOST_ADDRESS "192.168.8.12"
#define MQTT_SERVER_HOST_PORT    "1883"

static void *mqtt_client = NULL;

static void atest_m_mqtt_init(void)
{
    m_mqtt_param init_param = {5000, MQTT_ATEST_BUFF_DEEP, MQTT_ATEST_BUFF_DEEP + 50};
    if (mqtt_client != NULL)
        return;
    mqtt_client = m_mqtt_create(&init_param);
    tp_assert_true(mqtt_client != NULL);
}

static void atest_m_mqtt_deinit(void)
{
    m_mqtt_destory(mqtt_client);
    mqtt_client = NULL;
}

#if !defined(NET_USING_MINI_MQTT_AUTO_YIELD)
static void atest_m_mqtt_thread_func(void *param)
{
    printf("start MQTT client yield!\r\n");
    while (m_mqtt_get_state(mqtt_client) == mqtt_connected)
    {
        m_mqtt_yield(mqtt_client);
        os_task_msleep(100);
    }
    printf("exit MQTT client task!\r\n");
}

static void atest_m_mqtt_thread(void)
{
    os_task_id task = os_task_create(NULL, NULL, MQTT_AUTO_YIELD_STACK_DEPTH, "MQTT Client", atest_m_mqtt_thread_func, NULL, OS_TASK_PRIORITY_MAX / 2);
                                 
    if (task == NULL)
    {
        printf("out of memory!\r\n");
        return;
    }
    if (os_task_startup(task))
    {
        printf("task error!\r\n");
        return;
    }
}
#endif /* NET_USING_MINI_MQTT_AUTO_YIELD */

static void atest_m_mqtt_connect(void)
{
    if (m_mqtt_get_state(mqtt_client) == mqtt_connected)
        return;

    m_mqtt_connect_param param;
    memset(&param, 0, sizeof(m_mqtt_connect_param));
    param.server_addr = MQTT_SERVER_HOST_ADDRESS;
    param.port        = MQTT_SERVER_HOST_PORT;
    param.id          = "12345678";
    param.username    = "oneos";
    param.password    = "123456";
    param.keep_alive  = 300;
    int ret           = m_mqtt_connect(mqtt_client, &param);
    tp_assert_true(ret == ERR_M_MQTT_SUCCESS);
    tp_assert_true(m_mqtt_get_state(mqtt_client) == mqtt_connected);
#if !defined(NET_USING_MINI_MQTT_AUTO_YIELD)
    atest_m_mqtt_thread();
#endif /* NET_USING_MINI_MQTT_AUTO_YIELD */
}

static void atest_m_mqtt_disconnect(void)
{
    if (m_mqtt_get_state(mqtt_client) == mqtt_not_connected)
        return;
    m_mqtt_disconnect(mqtt_client);
    tp_assert_true(m_mqtt_get_state(mqtt_client) == mqtt_not_connected);
}

static void message_handler(void *handle, const char *topic_name, void *payload, size_t payload_len)
{
    printf("%s\r\n", __func__);
    printf("topic:%s\r\n", topic_name);
    printf("message:%s\r\n", (uint8_t *)payload);
}

const char *topic_name = "/public/test/mqtest";
static void atest_m_mqtt_subscribe(void)
{
    int rc = m_mqtt_subscribe(mqtt_client, topic_name, mqtt_qos1, message_handler);
    tp_assert_true(rc == ERR_M_MQTT_SUCCESS);
}

static void atest_m_mqtt_unsubscribe(void)
{
    int rc = m_mqtt_unsubscribe(mqtt_client, topic_name);
    tp_assert_true(rc == ERR_M_MQTT_SUCCESS);
}

static char     long_buf[MQTT_ATEST_BUFF_DEEP] = {0};
static uint32_t count                          = 1;
static void     atest_m_mqtt_publish_qos0(void)
{
    m_mqtt_message mqtt_message;

    snprintf(long_buf, sizeof(long_buf), "qos0 message %d", count++);
    mqtt_message.qos        = mqtt_qos0;
    mqtt_message.payload    = (void *)long_buf;
    mqtt_message.payloadlen = strlen(long_buf);
    int rc                  = m_mqtt_publish(mqtt_client, topic_name, &mqtt_message);
    tp_assert_true(rc == ERR_M_MQTT_SUCCESS);
}

static void atest_m_mqtt_publish_qos1(void)
{
    m_mqtt_message mqtt_message;

    snprintf(long_buf, sizeof(long_buf), "qos1 message %d", count++);
    mqtt_message.qos        = mqtt_qos1;
    mqtt_message.payload    = (void *)long_buf;
    mqtt_message.payloadlen = strlen(long_buf);
    int rc                  = m_mqtt_publish(mqtt_client, topic_name, &mqtt_message);
    tp_assert_true(rc == ERR_M_MQTT_SUCCESS);
}

#ifdef NET_USING_MINI_MQTT_QOS2
static void atest_m_mqtt_publish_qos2(void)
{
    m_mqtt_message mqtt_message;

    snprintf(long_buf, sizeof(long_buf), "qos2 message %d", count++);
    mqtt_message.qos        = mqtt_qos2;
    mqtt_message.payload    = (void *)long_buf;
    mqtt_message.payloadlen = strlen(long_buf);
    int rc                  = m_mqtt_publish(mqtt_client, topic_name, &mqtt_message);
    tp_assert_true(rc == ERR_M_MQTT_SUCCESS);
}
#endif /* NET_USING_MINI_MQTT_QOS2 */

static void atest_m_mqtt_pingreq(void)
{
    int rc = m_mqtt_ping_request(mqtt_client);
    tp_assert_true(rc == ERR_M_MQTT_SUCCESS);
}

static void atest_m_mqtt_get_state(void)
{
    int rc = m_mqtt_get_state(mqtt_client);
    if (rc == mqtt_connected)
        printf("mqtt is connected!\r\n");
    else
        printf("mqtt is not connected!\r\n");
}

static void atest_m_mqtt_all(void)
{
    ATEST_UNIT_RUN(atest_m_mqtt_init);
    ATEST_UNIT_RUN(atest_m_mqtt_connect);
    ATEST_UNIT_RUN(atest_m_mqtt_subscribe);
    ATEST_UNIT_RUN(atest_m_mqtt_publish_qos0);
    ATEST_UNIT_RUN(atest_m_mqtt_publish_qos1);
#ifdef NET_USING_MINI_MQTT_QOS2
    ATEST_UNIT_RUN(atest_m_mqtt_publish_qos2);
#endif /* NET_USING_MINI_MQTT_QOS2 */
    ATEST_UNIT_RUN(atest_m_mqtt_pingreq);
    ATEST_UNIT_RUN(atest_m_mqtt_unsubscribe);
    ATEST_UNIT_RUN(atest_m_mqtt_disconnect);
    ATEST_UNIT_RUN(atest_m_mqtt_deinit);
}

ATEST_TC_EXPORT(m.mqtt.all, atest_m_mqtt_all, NULL, NULL, TC_PRIORITY_LOW);
ATEST_TC_EXPORT(m.mqtt.init, atest_m_mqtt_init, NULL, NULL, TC_PRIORITY_LOW);
ATEST_TC_EXPORT(m.mqtt.deinit, atest_m_mqtt_deinit, NULL, NULL, TC_PRIORITY_LOW);
ATEST_TC_EXPORT(m.mqtt.connect, atest_m_mqtt_connect, NULL, NULL, TC_PRIORITY_LOW);
ATEST_TC_EXPORT(m.mqtt.disconnect, atest_m_mqtt_disconnect, NULL, NULL, TC_PRIORITY_LOW);
ATEST_TC_EXPORT(m.mqtt.subscribe, atest_m_mqtt_subscribe, NULL, NULL, TC_PRIORITY_LOW);
ATEST_TC_EXPORT(m.mqtt.unsubscribe, atest_m_mqtt_unsubscribe, NULL, NULL, TC_PRIORITY_LOW);
ATEST_TC_EXPORT(m.mqtt.publish0, atest_m_mqtt_publish_qos0, NULL, NULL, TC_PRIORITY_LOW);
ATEST_TC_EXPORT(m.mqtt.publish1, atest_m_mqtt_publish_qos1, NULL, NULL, TC_PRIORITY_LOW);
#ifdef NET_USING_MINI_MQTT_QOS2
ATEST_TC_EXPORT(m.mqtt.publish2, atest_m_mqtt_publish_qos2, NULL, NULL, TC_PRIORITY_LOW);
#endif /* NET_USING_MINI_MQTT_QOS2 */
ATEST_TC_EXPORT(m.mqtt.ping, atest_m_mqtt_pingreq, NULL, NULL, TC_PRIORITY_LOW);
ATEST_TC_EXPORT(m.mqtt.state, atest_m_mqtt_get_state, NULL, NULL, TC_PRIORITY_LOW);

#endif /* MINI_MQTT_CLIENT */
