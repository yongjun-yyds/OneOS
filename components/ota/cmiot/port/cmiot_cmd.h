/**
 ***********************************************************************************************************************
 * Copyright (c) 2021, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        cmiot_cmd.h
 *
 * @brief       Interface related to OS.
 *
 * @revision
 * Date         Author          Notes
 * 2021-04-09   OneOS Team      First version.
 ***********************************************************************************************************************/
#ifndef __CMIOT_CMD_H__
#define __CMIOT_CMD_H__

#include "cmiot_type.h"
#include <os_util.h>

#ifdef __cplusplus
extern "C" {
#endif

os_err_t cmiot_cv(int argc, char **argv);
os_err_t cmiot_ru(int argc, char **argv);

#ifdef CMIOT_USING_MULTI_MASTER
os_err_t cmiot_cv_slave(int argc, char **argv);
os_err_t cmiot_ru_slave(int argc, char **argv);
#endif

#ifdef __cplusplus
}
#endif

#endif /* __CMIOT_CMD_H__ */
