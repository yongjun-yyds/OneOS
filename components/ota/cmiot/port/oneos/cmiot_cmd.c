/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        ota_cmd.c
 *
 * @brief       Implement commond functions
 *
 * @revision
 * Date         Author          Notes
 * 2020-06-16   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include "cmiot_cmd.h"
#include "cmiot_cfg.h"
#include "cmiot_main.h"
#ifdef OS_USING_SHELL
#include "shell.h"
#endif
#include <stdio.h>
#include <string.h>
#include <stdlib.h>


#if defined(OS_USING_SHELL) && !defined(CMIOT_USING_MULTI_SLAVE)
/**
 ***********************************************************************************************************************
 * @brief              This function will start the upgrade process
 * @param[in]       argc       The input param num
 * @param[in]       argv       The input param
 *
 * @retval             void
 ***********************************************************************************************************************
 */
os_err_t cmiot_cv(int argc, char **argv)
{
    cmiot_int8 rst = cmiot_upgrade();
    os_kprintf("start upgrade result:%d\r\n", rst);
    return 0;
}
SH_CMD_EXPORT(cmiot_cv, cmiot_cv, "start the upgrade process");

/**
 ***********************************************************************************************************************
 * @brief              This function will start the report upgrade process
 * @param[in]       argc       The input param num
 * @param[in]       argv       The input param
 *
 * @retval             void
 ***********************************************************************************************************************
 */
os_err_t cmiot_ru(int argc, char **argv)
{
    cmiot_int8 rst = cmiot_report_upgrade();
    os_kprintf("start report upgrade result:%d\r\n", rst);
    return 0;
}
SH_CMD_EXPORT(cmiot_ru, cmiot_ru, "start the report upgrade process");

#ifdef CMIOT_USING_MULTI_MASTER
/**
 ***********************************************************************************************************************
 * @brief              This function will start the upgrade process for slave
 * @param[in]       argc       The input param num
 * @param[in]       argv       The input param
 *
 * @retval             void
 ***********************************************************************************************************************
 */
os_err_t cmiot_cv_slave(int argc, char **argv)
{
    if (argc < 2)
    {
        os_kprintf("At least input one param.\r\n");
        return 0;
    }
    cmiot_int8 rst = cmiot_upgrade_slave(atoi(argv[1]));
    os_kprintf("start upgrade result:%d\r\n", rst);
    return 0;
}
SH_CMD_EXPORT(cmiot_cv_slave, cmiot_cv_slave, "start the upgrade process for slave");

/**
 ***********************************************************************************************************************
 * @brief              This function will start the report upgrade process for slave
 * @param[in]       argc       The input param num
 * @param[in]       argv       The input param
 *
 * @retval             void
 ***********************************************************************************************************************
 */
os_err_t cmiot_ru_slave(int argc, char **argv)
{
    if (argc < 2)
    {
        os_kprintf("At least input one param.\r\n");
        return 0;
    }

    cmiot_int8 rst = cmiot_report_upgrade_slave(atoi(argv[1]));
    os_kprintf("start report upgrade result:%d\r\n", rst);
    return 0;
}
SH_CMD_EXPORT(cmiot_ru_slave, cmiot_ru_slave, "start the report upgrade process for slave");
#endif
#endif
