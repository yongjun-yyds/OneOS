/**
 ***********************************************************************************************************************
 * Copyright (c) 2021, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        ota_fal.c
 *
 * @brief       Interface related to OS.
 *
 * @revision
 * Date         Author          Notes
 * 2020-10-13   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include "cmiot_fal.h"
#include "cmiot_cfg.h"
#include "cmiot_log.h"
#include <fal_part.h>

void *cmiot_ota_fal_part_find(cmiot_char *name)
{
    return (void *)fal_part_find(name);
}

cmiot_int32 cmiot_ota_fal_part_read(void *part, cmiot_uint32 addr, cmiot_char *buf, cmiot_uint32 size)
{
    return fal_part_read((fal_part_t *)part, (uint32_t)addr, (uint8_t*)buf, size);
}

cmiot_int32 cmiot_ota_fal_part_write(void *part, cmiot_uint32 addr, cmiot_char *buf, cmiot_uint32 size)
{
    return fal_part_write((fal_part_t *)part, (uint32_t)addr, (uint8_t*)buf, size);
}

cmiot_int32 cmiot_ota_fal_part_erase(void *part, cmiot_uint32 addr, size_t size)
{
    return fal_part_erase((fal_part_t *)part, (uint32_t)addr, size);
}

void *cmiot_hal_get_device(cmiot_uint8 type)
{
    cmiot_char *device = NULL;
    if ((type == CMIOT_FILETYPE_PATCH) || (type == CMIOT_FILETYPE_PATCH_INFO))
    {
        device = cmiot_download_name();
    }

    if (device != NULL)
    {
        return cmiot_ota_fal_part_find(device);
    }
    else
    {
        return NULL;
    }
}

cmiot_uint32 cmiot_hal_get_true_blocksize(cmiot_uint8 type)
{
    fal_part_t *part = (fal_part_t *)cmiot_hal_get_device(type);

    if (part && part->flash)
    {
        return (part->flash->block_size < 1024 ? 1024 : part->flash->block_size);
    }

    CMIOT_INFO("[CMIOT] ", "Get flash block size failed\r\n");

    return 0;
}

cmiot_uint32 cmiot_hal_get_true_pagesize(cmiot_uint8 type)
{
    fal_part_t *part = (fal_part_t *)cmiot_hal_get_device(type);

    if (part && part->flash)
    {
        return part->flash->page_size;
    }

    CMIOT_INFO("[CMIOT] ", "Get flash page size failed\r\n");

    return 0;
}

cmiot_uint32 cmiot_hal_get_delta_addr(void)
{
    fal_part_t *part = (fal_part_t *)cmiot_hal_get_device(CMIOT_FILETYPE_PATCH);

    if (part && part->info)
    {
        return part->info->offset;
    }

    CMIOT_INFO("[CMIOT] ", "Get download addr failed\r\n");

    return 0;
}

cmiot_uint32 cmiot_hal_get_download_size()
{
    fal_part_t *part = (fal_part_t *)cmiot_hal_get_device(CMIOT_FILETYPE_PATCH);

    if (part && part->info)
    {
        return part->info->size;
    }

    CMIOT_INFO("[CMIOT] ", "Get download size failed\r\n");
    return 0;
}

