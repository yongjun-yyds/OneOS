/*
 * Copyright 2015-2018 The OpenSSL Project Authors. All Rights Reserved.
 *
 * Licensed under the OpenSSL license (the "License").  You may not use
 * this file except in compliance with the License.  You can obtain a copy
 * in the file LICENSE in the source distribution or at
 * https://www.openssl.org/source/license.html
 */

#ifndef HEADER_CHACHA20_POLY1305_H
#define HEADER_CHACHA20_POLY1305_H

#include <stddef.h>

#define CHACHA_U8TOU32(p)                                                                                              \
    (((uint32_t)(p)[0]) | ((uint32_t)(p)[1] << 8) | ((uint32_t)(p)[2] << 16) | ((uint32_t)(p)[3] << 24))

#define CHACHA_KEY_SIZE 32
#define CHACHA_CTR_SIZE 16
#define CHACHA_BLK_SIZE 64

typedef struct
{
    union
    {
        double   align; /* this ensures even sizeof(EVP_CHACHA_KEY)%8==0 */
        uint32_t d[CHACHA_KEY_SIZE / 4];
    } key;
    uint32_t counter[CHACHA_CTR_SIZE / 4];
    uint8_t  buf[CHACHA_BLK_SIZE];
    uint32_t partial_len;
} EVP_CHACHA_KEY;

int chacha_init_key(EVP_CHACHA_KEY *key,
                    const uint8_t   user_key[CHACHA_KEY_SIZE],
                    const uint8_t   iv[CHACHA_CTR_SIZE],
                    int             enc);

int chacha_cipher(EVP_CHACHA_KEY *key, uint8_t *out, const uint8_t *inp, size_t len);

void chacha_deinit_key(EVP_CHACHA_KEY *key);

#endif
