
/*******************************************************************************
 ******     Copyright (c) 2018--2023 OSR.Co.Ltd. All rights reserved.     ******
 *******************************************************************************/

#ifndef OSR_SM4_H_
#define OSR_SM4_H_

#include <stdint.h>

#define OSR_CTR_SM4
#define OSR_CFB_SM4
#define OSR_OFB_SM4
#define OSR_GCM_SM4
#define OSR_CCM_SM4

#define RAND_DELAY

#ifdef __cplusplus
extern "C" {
#endif

typedef enum SM4_RET_CODE
{
    OSR_SM4Success = 0,
    OSR_SM4BufferNull,
    OSR_SM4InputTooLong,
    OSR_SM4InputLenInvalid,
    OSR_SM4CryptInvalid,
    OSR_SM4InOutSameBuffer,
    OSR_SM4VerifyFail,
    OSR_SM4Attacked
} OSR_SM4_RET_CODE;

typedef enum SM4_CRYPT
{
    OSR_SM4_DECRYPT = 0,
    OSR_SM4_ENCRYPT
} OSR_SM4_CRYPT;

typedef struct __sm4_context__
{
    uint32_t SM4_rk[32];
} SM4_CONTEXT_T;

OSR_SM4_RET_CODE OSR_SM4_Init(SM4_CONTEXT_T *ctx);

OSR_SM4_RET_CODE OSR_SM4_Free(SM4_CONTEXT_T *ctx);

OSR_SM4_RET_CODE OSR_SM4_Setkey(SM4_CONTEXT_T *ctx, const uint8_t key[16]);

OSR_SM4_RET_CODE
OSR_SM4_ECB(SM4_CONTEXT_T *ctx, const uint8_t *in, uint32_t inByteLen, const uint8_t En_De, uint8_t *out);

OSR_SM4_RET_CODE
OSR_SM4_CBC(SM4_CONTEXT_T *ctx,
            const uint8_t *in,
            uint32_t       inByteLen,
            const uint8_t  iv[16],
            const uint8_t  En_De,
            uint8_t       *out);

#ifdef OSR_OFB_SM4
OSR_SM4_RET_CODE
OSR_SM4_OFB(SM4_CONTEXT_T *ctx, const uint8_t *in, uint32_t inByteLen, const uint8_t iv[16], uint8_t *out);
#endif

#ifdef OSR_CFB_SM4
OSR_SM4_RET_CODE
OSR_SM4_CFB(SM4_CONTEXT_T *ctx,
            const uint8_t *in,
            uint32_t       inByteLen,
            const uint8_t  iv[16],
            const uint8_t  En_De,
            uint8_t       *out);
#endif

#ifdef OSR_CTR_SM4
OSR_SM4_RET_CODE
OSR_SM4_CTR(SM4_CONTEXT_T *ctx, const uint8_t *in, uint32_t inByteLen, const uint8_t CTR[16], uint8_t *out);
#endif

#ifdef OSR_CCM_SM4
OSR_SM4_RET_CODE OSR_SM4_CCM_Gene_Enc(SM4_CONTEXT_T *ctx,
                                      const uint8_t *in,
                                      uint32_t       inByteLen,
                                      const uint8_t *nonce,
                                      uint8_t        nonceByteLen,
                                      const uint8_t *adata,
                                      uint64_t       adataByteLen,
                                      uint8_t       *Tag,
                                      uint8_t        TagByteLen,
                                      uint8_t       *out);

OSR_SM4_RET_CODE OSR_SM4_CCM_Dec_Verify(SM4_CONTEXT_T *ctx,
                                        const uint8_t *in,
                                        uint32_t       inByteLen,
                                        const uint8_t *nonce,
                                        uint8_t        nonceByteLen,
                                        const uint8_t *adata,
                                        uint64_t       adataByteLen,
                                        uint8_t       *Tag,
                                        uint8_t        TagByteLen,
                                        uint8_t       *out);
#endif

#ifdef OSR_GCM_SM4
OSR_SM4_RET_CODE OSR_SM4_GCM_Gene_Enc(SM4_CONTEXT_T *ctx,
                                      uint8_t       *P,
                                      uint64_t       PByteLen,
                                      uint8_t       *iv,
                                      uint32_t       ivByteLen,
                                      uint8_t       *A,
                                      uint64_t       AByteLen,
                                      uint8_t       *T,
                                      uint32_t       TByteLen,
                                      uint8_t       *C);

OSR_SM4_RET_CODE OSR_SM4_GCM_Dec_Verify(SM4_CONTEXT_T *ctx,
                                        uint8_t       *C,
                                        uint64_t       CByteLen,
                                        uint8_t       *iv,
                                        uint32_t       ivByteLen,
                                        uint8_t       *A,
                                        uint64_t       AByteLen,
                                        uint8_t       *T,
                                        uint32_t       TByteLen,
                                        uint8_t       *P);
#endif

OSR_SM4_RET_CODE OSR_SM4_Close(SM4_CONTEXT_T *ctx);

OSR_SM4_RET_CODE OSR_SM4_Version(uint8_t version[4]);

#ifdef __cplusplus
}
#endif

#endif
