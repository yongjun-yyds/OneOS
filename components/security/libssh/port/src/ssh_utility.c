#include "device.h"
#include "os_assert.h"
#include "oneos_config.h"
#include "shell.h"
#include "ring_buff.h"
#include "console.h"
#include "os_task.h"
#include <fcntl.h>
#include "ssh_utility.h"
#include "libssh/libssh.h"

#define SSH_SHELL_NAME "ssh_shell"
#define SSHD_PTY_NAME "ssh_pty"
#define RINGBUF_LEN (1024*20)

static int sshd_fd = -1;
static os_device_t ssh_shell_dev;
static os_device_t ssh_pty_dev;
static struct rb_ring_buff *rb_rx = NULL;   //ssh->shell
static struct rb_ring_buff *rb_tx = NULL;   //shell->ssh
static int ssh_console_inited = 0;

static os_ssize_t sh_write(os_device_t *dev, os_off_t pos, const void *buffer, os_size_t size);
static os_ssize_t sh_read(os_device_t *dev, os_off_t pos, void *buffer, os_size_t size);
static os_ssize_t pty_write(os_device_t *dev, os_off_t pos, const void *buffer, os_size_t size);
static os_ssize_t pty_read(os_device_t *dev, os_off_t pos, void *buffer, os_size_t size);
static struct os_device_ops ssh_console_ops = {
    .read = sh_read,
    .write = sh_write,
};

static struct os_device_ops ssh_pty_ops = {
    .read = pty_read,
    .write = pty_write,
};


void set_sshd_fd(int fd)
{
    sshd_fd = fd;
}

int get_sshd_fd(void)
{
    return sshd_fd;
}

int get_sshd_console_state(void)
{
    return ssh_console_inited;
}

os_err_t ssh_console_install(void)
{
    int fd = -1;

    ssh_log_port("**ssh_console_install\n");
    if(1 == ssh_console_inited)
        return OS_FAILURE;

    rb_rx = rb_ring_buff_create(RINGBUF_LEN);
    if(NULL == rb_rx)
        return OS_NOMEM;

    rb_tx = rb_ring_buff_create(RINGBUF_LEN);
    if(NULL == rb_tx)
    {
        rb_ring_buff_destroy(rb_rx);
        return OS_NOMEM;
    }

    /* register ssh device */
    ssh_shell_dev.type = OS_DEVICE_TYPE_CHAR;
    ssh_shell_dev.ops = &ssh_console_ops;
    ssh_shell_dev.user_data = &ssh_pty_dev;
    ssh_shell_dev.rx_count = RINGBUF_LEN;

    ssh_pty_dev.type = OS_DEVICE_TYPE_CHAR;
    ssh_pty_dev.ops = &ssh_pty_ops;
    ssh_pty_dev.user_data = &ssh_shell_dev;
    ssh_pty_dev.rx_count = RINGBUF_LEN;

    /* register ssh device */
    os_device_register(&ssh_shell_dev, SSH_SHELL_NAME);
    os_device_register(&ssh_pty_dev, SSHD_PTY_NAME);

    fd= open("/dev/" SSHD_PTY_NAME, O_RDWR);
    if(fd < 0)
    {
        ssh_log_port("open sshd device error %d\r\n", fd);
        rb_ring_buff_destroy(rb_rx);
        rb_ring_buff_destroy(rb_tx);
        return OS_FAILURE;
    }
    set_sshd_fd(fd);

    sh_disconnect_console();
    os_console_set_device(SSH_SHELL_NAME);
    /* set console device as shell device */
    sh_reconnect_console();

    ssh_console_inited = 1;
    return OS_SUCCESS;
}

os_err_t ssh_console_uninstall(void)
{
    if(0 == ssh_console_inited)
        return OS_FAILURE;

    close(get_sshd_fd());
    set_sshd_fd(-1);

    sh_disconnect_console();    
    os_console_set_device(OS_CONSOLE_DEVICE_NAME);
    sh_reconnect_console();

    os_device_unregister(&ssh_shell_dev);
    os_device_unregister(&ssh_pty_dev);

    rb_ring_buff_destroy(rb_rx);
    rb_ring_buff_destroy(rb_tx);

    ssh_console_inited = 0;

    ssh_log_port("**ssh_console_uninstall\n");
    return OS_SUCCESS;
}

static os_ssize_t pty_read(os_device_t *dev, os_off_t pos, void *buffer, os_size_t size)
{
    int put_len = 0;
    put_len = rb_ring_buff_get(rb_tx, (uint8_t *)buffer, size);
    dev->rx_count = rb_ring_buff_data_len(rb_tx);
    ssh_log_port("pty_read: %d-", put_len);
    return put_len;
}

static os_ssize_t pty_write(os_device_t *dev, os_off_t pos, const void *buffer, os_size_t size)
{
    os_size_t put_len;
    char *buf = (char*)buffer;
    os_device_t *dev_next = (os_device_t *)dev->user_data;
    ssh_log_port("pty_write: %d-\n", size);
    put_len = rb_ring_buff_put(rb_rx, (uint8_t *)buffer, size);
    os_device_recv_notify(dev_next);

    return put_len;
}

static os_ssize_t sh_read(os_device_t *dev, os_off_t pos, void *buffer, os_size_t size)
{
    os_size_t put_len;
    char *buf = (char*)buffer;
    put_len = rb_ring_buff_get(rb_rx, (uint8_t *)buffer, size);

    ssh_log_port("sh_read: %d-", put_len);
    return put_len;
}


static os_ssize_t sh_write(os_device_t *dev, os_off_t pos, const void *buffer, os_size_t size)
{
    os_size_t put_len;
    char *buf = (char*)buffer;
    os_device_t *dev_next = (os_device_t *)dev->user_data;
    put_len = rb_ring_buff_put(rb_tx, (uint8_t *)buffer, size);

    dev_next->rx_count = rb_ring_buff_data_len(rb_tx);
    if(dev_next->rx_count <= 0)
    {
        ssh_log_port("dev_next->rx_count error %d\t\n", dev_next->rx_count);
        OS_ASSERT(dev_next->rx_count > 0);
    }
    // buf[10]=0;
    ssh_log_port("**sh_write: %d-%s\n", size, buffer);
    os_device_recv_notify(dev_next);
    ssh_log_port("**sh_write: end\n");

    return put_len;
}
