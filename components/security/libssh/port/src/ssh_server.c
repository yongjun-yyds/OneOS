
#include "oneos_config.h"
#include "config.h"

#include <libssh/callbacks.h>
#include <libssh/server.h>

#include <poll.h>
#include <fcntl.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <stdio.h>
#include "os_task.h"
#include "os_mutex.h"
#include "ssh_utility.h"

#define SSH_FOLDER     "/etc/ssh/"
#define RSA_KEY_NAME    "ssh_host_rsa_key"
#define DSA_KEY_NAME    "ssh_host_dsa_key"
#define ECDSA_KEY_NAME  "ssh_host_ecdsa_key"
#define SHADOW_NAME     "shadow"

#define BUF_SIZE 256

static os_mutex_dummy_t sshbind_mut;
static ssh_bind sshbind = NULL;
static ssh_session session = NULL;
static int ssh_server_started = 0;
static int ssh_server_connected = 0;
static char port[8] = "{0,}";

/* A userdata struct for session. */
struct session_data_struct {
    /* Pointer to the channel the session will allocate. */
    ssh_channel channel;
    int auth_attempts;
    int authenticated;
};

static void set_default_keys(ssh_bind sshbind,
                             int rsa_already_set,
                             int dsa_already_set,
                             int ecdsa_already_set) {
    if (!rsa_already_set) {
        ssh_bind_options_set(sshbind, SSH_BIND_OPTIONS_RSAKEY,
                              SSH_FOLDER RSA_KEY_NAME);
    }
    if (!dsa_already_set) {
        ssh_bind_options_set(sshbind, SSH_BIND_OPTIONS_DSAKEY,
                              SSH_FOLDER DSA_KEY_NAME);
    }
    if (!ecdsa_already_set) {
        ssh_bind_options_set(sshbind, SSH_BIND_OPTIONS_ECDSAKEY,
                              SSH_FOLDER ECDSA_KEY_NAME);
    }
    ssh_bind_options_set(sshbind, SSH_BIND_OPTIONS_HOSTKEY,
                          SSH_FOLDER RSA_KEY_NAME);
}
#define DEF_STR_SIZE 1024

static int pty_request(ssh_session session, ssh_channel channel,
                       const char *term, int cols, int rows, int py, int px,
                       void *userdata) {
    return SSH_OK;
}

static int pty_resize(ssh_session session, ssh_channel channel, int cols,
                      int rows, int py, int px, void *userdata) {
    ssh_log_port( "Failed to resize pty\n");
    return SSH_ERROR;
}


static int exec_request(ssh_session session, ssh_channel channel,
                        const char *command, void *userdata) {
    ssh_log_port( "Failed to open exec_request\n");
    return SSH_ERROR;
}
static int data_function(ssh_session session, ssh_channel channel, void *data,
                         uint32_t len, int is_stderr, void *userdata) {
    return write(get_sshd_fd(), data, len);
}

static int subsystem_request(ssh_session session, ssh_channel channel,
                             const char *subsystem, void *userdata) {
    ssh_log_port( "Failed to open subsystem_request\n");
    return SSH_ERROR;
}

static int shell_request(ssh_session session, ssh_channel channel,
                         void *userdata) {
    (void) session;
    (void) userdata;
    ssh_log_port( "open shell_request\n");
    if(OS_SUCCESS == ssh_console_install())
        return SSH_OK;
    else
        return SSH_ERROR;
}

static ssh_channel channel_open(ssh_session session, void *userdata) {
    struct session_data_struct *sdata = (struct session_data_struct *) userdata;

    sdata->channel = ssh_channel_new(session);
    return sdata->channel;
}

static int auth_password(ssh_session session, const char *user,
                         const char *pass, void *userdata) {
    struct session_data_struct *sdata = (struct session_data_struct *) userdata;

    (void) session;

    if (strcmp(user, LIBSSH_LOGIN_USERNAME) == 0 && ssh_check_pwd(pass) == OS_SUCCESS) {
        sdata->authenticated = 1;
        return SSH_AUTH_SUCCESS;
    }

    sdata->auth_attempts++;
    return SSH_AUTH_DENIED;
}

static int process_stdout(socket_t fd, int revents, void *userdata) {
    char buf[BUF_SIZE];
    int n = -1;
    ssh_channel channel = (ssh_channel) userdata;
    if (channel != NULL && (revents & POLLIN) != 0) {
        n = read(get_sshd_fd(), buf, BUF_SIZE);
        if (n > 0) {
            ssh_channel_write(channel, buf, n);
        }
    }

    return n;
}

static void handle_session(ssh_event event, ssh_session session) {
    int n;
    int rc = 0;    
    ssh_event server_event = NULL;

    /* Our struct holding information about the session. */
    struct session_data_struct sdata = {
        .channel = NULL,
        .auth_attempts = 0,
        .authenticated = 0
    };

    struct ssh_channel_callbacks_struct channel_cb = {
        .userdata = NULL,
        .channel_pty_request_function = pty_request,
        .channel_pty_window_change_function = pty_resize,
        .channel_shell_request_function = shell_request,
        .channel_exec_request_function = exec_request,
        .channel_data_function = data_function,
        .channel_subsystem_request_function = subsystem_request
    };

    struct ssh_server_callbacks_struct server_cb = {
        .userdata = &sdata,
        .auth_password_function = auth_password,
        .channel_open_request_session_function = channel_open,
    };

    ssh_set_auth_methods(session, SSH_AUTH_METHOD_PASSWORD);

    ssh_callbacks_init(&server_cb);
    ssh_callbacks_init(&channel_cb);

    ssh_set_server_callbacks(session, &server_cb);

    if (ssh_handle_key_exchange(session) != SSH_OK) {
        ssh_log_port( "ssh_get_error 1:%s\n", ssh_get_error(session));
        return;
    }

    ssh_event_add_session(event, session);

    n = 0;
    while (sdata.authenticated == 0 || sdata.channel == NULL) {
        /* If the user has used up all attempts, or if he hasn't been able to
         * authenticate in 10 seconds (n * 100ms), disconnect. */
        if (sdata.auth_attempts >= 3 || n >= 100) {
            ssh_log_port( "auth timeout");
            return;
        }

        if (ssh_event_dopoll(event, 100) == SSH_ERROR) {
            ssh_log_port( "ssh_get_error 2:%s\n", ssh_get_error(session));
            return;
        }
        n++;
    }

    ssh_set_channel_callbacks(sdata.channel, &channel_cb);

    do {
        /* Poll the main event which takes care of the session, the channel and
         * even our child process's stdout/stderr (once it's started). */
        if (ssh_event_dopoll(event, -1) == SSH_ERROR) {
          ssh_channel_close(sdata.channel);
          continue;
        }

        /* If child process's stdout/stderr has been registered with the event,
         * or the child process hasn't started yet, continue. */
        if (server_event != NULL) {
            continue;
        }
        /* Executed only once, once the child process starts. */
        /* If stdout valid, add stdout to be monitored by the poll event. */
        if (get_sshd_fd() != -1) {
            server_event = event;
            if (ssh_event_add_fd(event, get_sshd_fd(), POLLIN, process_stdout,
                                 sdata.channel) != SSH_OK) {
                ssh_log_port( "Failed to register stdout to poll context\n");
                ssh_channel_close(sdata.channel);
            }
            os_kprintf("%s", sh_get_prompt());
        }
    } while(ssh_channel_is_open(sdata.channel));

    if(NULL != server_event)
    {
        ssh_event_remove_fd(event, get_sshd_fd());
    }

    ssh_console_uninstall();
    ssh_log_port( "channel closed");
}

os_err_t ssh_server_start(char *port) 
{
    ssh_event event;
    int rc;
    if(1 == ssh_server_started)
    {
        os_kprintf("ssh server has started\r\n");
        return OS_BUSY;
    }

    rc = ssh_init();
    if (rc < 0) {
        os_kprintf( "ssh_init failed\n");
        return OS_FAILURE;
    }

    sshbind = ssh_bind_new();
    if (sshbind == NULL) {
        os_kprintf( "ssh_bind_new failed\n");
        ssh_finalize();
        return OS_FAILURE;
    }

    if(NULL != port){
        if(-1 == ssh_bind_options_set(sshbind, SSH_BIND_OPTIONS_BINDPORT_STR, port))
        {
            os_kprintf("ssh set port failed\n");
            ssh_bind_free(sshbind);
            sshbind = NULL;
            ssh_finalize();
            return OS_FAILURE;
        }
    }

    set_default_keys(sshbind, 0, 0, 0);

    if(ssh_bind_listen(sshbind) < 0) {
        os_kprintf( "ssh bind listen fail:%s\n", ssh_get_error(sshbind));
        ssh_bind_free(sshbind);
        sshbind = NULL;
        ssh_finalize();
        return OS_FAILURE;
    }

    os_kprintf("ssh server started\r\n");
    ssh_server_started = 1;
    os_mutex_create(&sshbind_mut, "bind_m", 0);

    while (1) 
    {
        session = ssh_new();
        if (session == NULL) {
            ssh_log_port( "Failed to allocate session\n");
            break;
        }

        /* Blocks until there is a new incoming connection. */
        if(ssh_bind_accept(sshbind, session) != SSH_ERROR) {
            /* Remove socket binding, which allows us to restart the
                * parent process, without terminating existing sessions. */
            // ssh_bind_free(sshbind);

            event = ssh_event_new();
            if (event != NULL) {
                /* Blocks until the SSH session ends by either
                    * child process exiting, or client disconnecting. */
                os_kprintf("ssh client connected\r\n");
                handle_session(event, session);
                os_kprintf("ssh client disconnected\r\n");
                ssh_event_free(event);
            } else {
                ssh_log_port( "Could not create polling context\n");
            }
            ssh_disconnect(session);
            ssh_free(session);
            session = NULL;
        } else {
            ssh_log_port( "ssh_get_error 4:%s\n", ssh_get_error(sshbind));
            ssh_disconnect(session);
            ssh_free(session);
            session = NULL;
            ssh_log_port( "out 1\r\n");
            break;
        }
    }

    ssh_log_port("ssh_bind_free: 0x%x", sshbind);
    os_mutex_lock(&sshbind_mut, OS_WAIT_FOREVER);
    ssh_bind_free(sshbind);
    sshbind = NULL;
    os_mutex_unlock(&sshbind_mut);
    ssh_finalize();

    os_mutex_destroy(&sshbind_mut);
    ssh_server_started = 0;
    os_kprintf("ssh server stoped\r\n");    
    return OS_SUCCESS;
}

static void ssh_server_main(void *param)
{
    ssh_server_start((char *)param);
}

static os_err_t ssh_start(int argc, char *argv[])  
{
    void *param = NULL;
    if(argc != 1 && argc != 2)
    {   os_kprintf("usage: ssh_start [port]\r\n");
        return OS_FAILURE;
    }
    if(2 == argc)
    {
        strncpy(port, argv[1], sizeof(port));
        param = port;
    }

    /*notice set pri lower than shell, to make sure ssh_stop quit quickly*/
    os_task_id task = os_task_create(NULL, NULL, 4096*4*2, "ssh_server", ssh_server_main, param, SHELL_TASK_PRIORITY - 1);
    
    if (task)
    {
        os_task_startup(task);
    }
    else{
        os_kprintf("os_task_create failed\r\n");
    }
    return 0;
}

os_err_t ssh_server_stop(void) 
{
    /*when client is connected ,do not support stop*/
    if(1 == get_sshd_console_state())
        return OS_FAILURE;

    /*when server is not open yet ,do not support stop*/
    if(0 == ssh_server_started)
        return OS_FAILURE;

    os_mutex_lock(&sshbind_mut, OS_WAIT_FOREVER);
    ssh_bind_free(sshbind);
    sshbind = NULL;
    os_mutex_unlock(&sshbind_mut);
    return OS_SUCCESS;
}

#ifdef OS_USING_SHELL
#include <shell.h>
SH_CMD_EXPORT(ssh_start, ssh_start, "ssh_start [port]");
SH_CMD_EXPORT(ssh_stop, ssh_server_stop, "stop ssh server");
#endif
