
#include "oneos_config.h"
#include "config.h"

#include <libssh/callbacks.h>
#include <libssh/server.h>
#include "libssh/libcrypto.h"
#include "libssh/crypto.h"

#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include "os_task.h"

#define SSH_FOLDER     "/etc/ssh/"
#define SHADOW_NAME     "shadow"

static int ssh_get_md5(char *des, const char *src)
{
    void *ctx;
    ctx = (void*)md5_init();
    if (ctx == NULL) {
        return SSH_ERROR;
    }

    md5_update(ctx, src, strlen(src));
    md5_final(des, ctx);

    return MD5_DIGEST_LEN;
}
static os_err_t ssh_set_new_pwd(const char *new)
{
    int fd = -1;
    int new_enc_len = 0;
    char new_enc[MD5_DIGEST_LEN+1] ={0,};

    new_enc_len = ssh_get_md5(new_enc, new);
    if(SSH_ERROR == new_enc_len)
    {
        os_kprintf("get new password md5 failed\r\n");
        return OS_FAILURE;
    }

    fd = open(SSH_FOLDER SHADOW_NAME, O_WRONLY | O_CREAT | O_TRUNC);
    if(-1 == fd)
    {
        os_kprintf("open shadow file failed\r\n");
        return OS_FAILURE;
    }
    write(fd, new_enc, new_enc_len);
    close(fd);
    return OS_SUCCESS;
}

os_err_t ssh_check_pwd(const char *pwd)
{    
    int fd = -1;
    int pwd_enc_len = 0;
    char pwd_enc[MD5_DIGEST_LEN+1] ={0,};
    char file_buf[MD5_DIGEST_LEN+1] ={0,};

    if (access(SSH_FOLDER SHADOW_NAME, F_OK) != -1)
    {
        fd = open(SSH_FOLDER SHADOW_NAME, O_RDWR);
        if(-1 == fd)
        {
            os_kprintf("open shadow file failed\r\n");
            return OS_FAILURE;
        }

        read(fd, file_buf, MD5_DIGEST_LEN);

        pwd_enc_len = ssh_get_md5(pwd_enc, pwd);
        if(SSH_ERROR == pwd_enc_len)
        {
            os_kprintf("get password md5 failed\r\n");
            close(fd);
            return OS_FAILURE;
        }

        if(strcmp(file_buf, pwd_enc))
        {
            os_kprintf("check password failed\r\n");
            close(fd);
            return OS_FAILURE;
        }
        close(fd);
    }
    else
    {
        if(strcmp(LIBSSH_LOGIN_PASSWORD, pwd))        
        {
            os_kprintf("check password failed\r\n");
            return OS_FAILURE;
        }
    }
    return OS_SUCCESS;
}

os_err_t ssh_setpassword(char *old_pd, char *new_pd) 
{
    if(NULL == old_pd || NULL == new_pd)
        return OS_FAILURE;

    if(OS_SUCCESS == ssh_check_pwd(old_pd))
    {
        if(OS_SUCCESS == ssh_set_new_pwd(new_pd))
        {
            os_kprintf("set ssh login password success\r\n");
            return OS_SUCCESS;
        }
    }

    return OS_FAILURE;
}
static os_err_t ssh_setpass(int argc, char **argv) 
{
    char *old = NULL;
    char *new = NULL;
    if (3 != argc)
    {
        os_kprintf("usage: ssh_setpassword <old> <new>\r\n");
        return -1;
    }
    old = argv[1];
    new = argv[2];
    ssh_setpassword(old, new);
}

#ifdef OS_USING_SHELL
#include <shell.h>
SH_CMD_EXPORT(ssh_setpassword, ssh_setpass, "ssh_setpassword <old> <new>");
#endif
