#include <stdio.h>
#include <pwd.h>
#include <string.h>
#include <sys/types.h>
#include "os_types.h"
char *gai_strerror(int code)
{
    return "dns error!";
}

struct passwd *getpwnam(const char *name)
{
    return NULL;
}

uid_t getuid(void)
{
    return 0;
}

int getpwuid_r(uid_t uid, struct passwd *pwd,
                      char *buf, size_t buflen, struct passwd **result)
{
    struct passwd *re = *result;
    if(uid != 0)
    {   
        re = NULL;
    }
    else
    {
        memset(re, 0, sizeof(struct passwd));
        re->pw_name = "root";
        re->pw_uid = 0;
        re->pw_gid = 0;
        re->pw_dir = "/root";
    }
    return 0;
}

pid_t waitpid(pid_t pid, int *wstatus, int options)
{
    return pid;
}

int gethostname(char *name, size_t len)
{
    if(len < sizeof("mcu_host"))
        return -1;
    strncpy(name, "mcu_host", sizeof("mcu_host"));
    return 0;
}
