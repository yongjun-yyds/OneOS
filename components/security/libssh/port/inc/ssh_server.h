#ifndef __SSH_SERVER_H__
#define __SSH_SERVER_H__
#include "os_types.h"
os_err_t ssh_server_start(char *port) ;
os_err_t ssh_server_stop(void) ;
os_err_t ssh_setpassword(char *old_pd, char *new_pd) ;
#endif