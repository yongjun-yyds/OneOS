#ifndef __SSH_UTILITY_H__
#define __SSH_UTILITY_H__
#include "os_types.h"
#include "os_errno.h"
#include "shell.h"
#include "ssh_oneos.h"
os_err_t ssh_check_pwd(const char *pwd);
os_err_t ssh_console_install(void);
os_err_t ssh_console_uninstall(void);
int get_sshd_fd(void);
int get_sshd_console_state(void);
#endif