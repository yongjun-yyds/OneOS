#ifndef __SSH_ONEOS_H__
#define __SSH_ONEOS_H__

#define __FILENAME__ (strrchr(__FILE__, '\\') + 1)
#define ssh_log_port(...) 
// #define ssh_log_port(...) \
//    os_jkprintf(__VA_ARGS__);os_jkprintf("  [%s:%d]\r\n",__FILENAME__,__LINE__)

/* getnameinfo constants */
#define NI_MAXHOST	1025
#define NI_MAXSERV	32

// static inline char *gai_strerror(int code);
const char *gai_strerror( int error );
#endif