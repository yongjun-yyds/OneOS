# libssh使用介绍
## menuconfig配置说明
增加如下配置:

set username：对应“LIBSSH_LOGIN_USERNAME”，配置默认登录用户名，建议用户名长度范围[1,10],不能为空

set password：对应“LIBSSH_LOGIN_PASSWORD”，配置默认登录密码，建议密码长度范围[6,20],不能为空

```c
(Top) → Components → Securit→ libssh config g
                                                  OneOS Configuration
(user) set username
(password) set password
```

## shell命令说明
参考linux的shell使用方法，提供3个shell命令：ssh_start、ssh_stop、ssh_setpassword。
```c
1.ssh_start        - ssh_start [port]
2.ssh_stop         - stop ssh server
3.ssh_setpassword  - ssh_setpassword <old> <new>
```

注意：

（1）ssh_start和ssh_stop用于打开和关闭ssh服务，只能在设备端使用，不能在ssh客户端输入。

（2）端口port取值范围[1,65535]，默认端口为22。

（3）ssh_setpassword可以在设备端和ssh客户端使用，要求输入正确的原始密钥才能更改。

（4）修改后的密钥保存在/etc/ssh/shadow文件，再次烧录固件后，密钥依然有效；只有删除该文件后，ssh才会使用menuconfig配置的密钥。

## api说明
### ssh_server_start

该函数用于打开ssh服务，函数原型如下：

```c
os_err_t ssh_server_start(char *port);
```

| **参数**  | **说明**         |
| :-------- | :--------------- |
| port        | 服务端口号，字符串形式传入，取值范围[1,65535]，如果传递NULL，默认端口为22。               |
| **返回**  | **说明**         |
| OS_SUCCESS   | 打开成功         |
| EBUSY       | 已经打开 |
| OS_FAILURE  | 打开失败 |


### ssh_server_stop

该函数用于关闭ssh服务，函数原型如下：

```c
os_err_t ssh_server_stop(void);
```

| **参数**  | **说明**         |
| :-------- | :--------------- |
| **返回**  | **说明**         |
| OS_SUCCESS   | 关闭成功         |
| OS_FAILURE  | 关闭失败 |


### ssh_setpassword

该函数用于设置密钥，函数原型如下：

```c
os_err_t ssh_setpassword(char *old_pd, char *new_pd);
```

| **参数**  | **说明**         |
| :-------- | :--------------- |
| old_pd | 老密码 |
| new_pd    | 新密码 |
| **返回**  | **说明**         |
| OS_SUCCESS   | 设置成功         |
| OS_FAILURE  | 设置失败 |


## 使用流程
下面介绍如何打开server服务，使用putty工具连接并发送shell命令。约定已经编译并烧录集成了ssh功能的固件，并且保证终端和pc在同一个局域网内。
### 准备key等相关文件
server需要准备一个key文件，“ssh_host_rsa_key”，用于密钥交换认证等。用户可以通过“ssh-keygen”制作，完成后拷贝到终端文件系统的“/etc/ssh/”目录下
### 开启server服务
在终端串口输入命令：ssh_start，服务开启成功后有输出“ssh server started”。
串口输入命令：lwip_ifconfig，查询终端的ip地址。
### putty连接到server
打开putty工具，host name输入上一步查询到的ip地址，点击“open”，如果有弹出请求验证提示框，点击“ok”，然后按照提示依次输入user和password，如果看到“sh />”表示连接成功。
### 发送shell命令
此时可以在putty的窗口上发送shell命令，比如“help”等
### 关闭与server的连接
本server不支持在putty上输入“exit”退出连接，如需退出，请点击窗口的关闭按钮。
### 关闭server服务
终端串口上输入“ssh_stop”可以关闭ssh服务
