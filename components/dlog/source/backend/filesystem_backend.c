/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        filesystem_backend.c
 *
 * @brief       filesystem backend realization.
 *
 * @revision
 * Date         Author          Notes
 * 2023-10-18   jixiaowei       first version
 ***********************************************************************************************************************
 */

#include <arch_interrupt.h>
#include <os_errno.h>
#include <unistd.h>
#include <string.h>
#include <dlog.h>
#include <vfs_fs.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <os_assert.h>
#include <board.h>
#include <filesystem_backend.h>
#include <oneos_config.h>

#ifdef DLOG_BACKEND_USING_FILESYSTEM

#define F_OK 0
#define W_OK 2
#define R_OK 4

#ifndef DLOG_FILE_CACHE_BUF_SIZE
#define DLOG_FILE_CACHE_BUF_SIZE 0
#endif

typedef enum
{
    DF_DIR_ERROR  = -2,
    DF_FILE_ERROR = -1,
    DF_OK         = 0
} dlog_file_error_code_t;

typedef enum
{
    FILE_EMPTY = 0,
    FILE_HALF,
    FILE_FULL
} dlog_file_status_t;

static dlog_backend_filesystem_t gs_backend_filesystem;

static void dlog_startup_seek_last_file(dlog_backend_filesystem_t *backend_filesystem)
{
    DIR           *directory;
    struct dirent *file;
    char           file_name[128];
    const char    *key = "new";
    char          *str_ptr;

    if (access(backend_filesystem->file_dir, F_OK) != 0)
    {
        backend_filesystem->cur_file_id = 1;
        DLOG_FILE_DBG("directory %s not found.\r\n", backend_filesystem->file_dir);
    }
    else
    {
        backend_filesystem->cur_file_id = 1;

        directory = opendir(backend_filesystem->file_dir);
        if (directory != NULL)
        {
            while ((file = readdir(directory)) != NULL)
            {
                if (file->d_type != DT_DIR)
                {
                    memset(file_name, 0, sizeof(file_name));
                    strncpy(file_name, file->d_name, sizeof(file_name));
                    DLOG_FILE_DBG("file_name: %s\r\n", file_name);
                    if (strstr(file_name, key))
                    {
                        str_ptr = strstr(file_name, "_");
                        if (str_ptr)
                        {
                            *str_ptr = '\0';
                            backend_filesystem->cur_file_id = atoi(file_name);
                            DLOG_FILE_DBG("find new file: %s, id = %d\r\n", file_name, backend_filesystem->cur_file_id);
                        }
                        break;
                    }
                }
            }
            closedir(directory);
        }
    }

    if (backend_filesystem->cur_file_id > backend_filesystem->file_max_num)
    {
        backend_filesystem->cur_file_id = 1;
    }

    snprintf(backend_filesystem->cur_file_path,
            DLOG_FILE_PATH_MAX_LEN,
            "%s%d_%s_%s",
            backend_filesystem->file_dir,
            backend_filesystem->cur_file_id,
            "new",
            backend_filesystem->file_name);
}

static int dlog_get_file(dlog_backend_filesystem_t *backend_filesystem)
{
    int fd = -1;

    if (backend_filesystem->cur_file_fd < 0)
    {
        DLOG_FILE_DBG("backend filesystem cur_file_fd < 0\r\n");

        dlog_startup_seek_last_file(backend_filesystem);

        if (access(backend_filesystem->file_dir, F_OK) != 0)
        {
            DLOG_FILE_DBG("directory %s not exist.\r\n", backend_filesystem->file_dir);
            if (mkdir(backend_filesystem->file_dir, 0777) == 0)
            {
                DLOG_FILE_DBG("newly create directory %s success!\r\n", backend_filesystem->file_dir);
            }
            else
            {
                DLOG_FILE_DBG("newly create directory %s failed!\r\n", backend_filesystem->file_dir);
                return DF_DIR_ERROR;
            }
        }

        if ((fd = open(backend_filesystem->cur_file_path, O_RDONLY)) >= 0)
        {
            close(fd);
            if ((fd = open(backend_filesystem->cur_file_path, O_RDWR | O_APPEND | O_CREAT)) >= 0)
            {
                DLOG_FILE_DBG("open exist file %s success\r\n", backend_filesystem->cur_file_path);
                backend_filesystem->cur_file_fd = fd;
            }
            else
            {
                DLOG_FILE_ERR("open exist file %s failed\r\n", backend_filesystem->cur_file_path);
            }
        }
        else /* dlog file not exist */
        {
            if ((fd = open(backend_filesystem->cur_file_path, O_RDWR | O_TRUNC | O_CREAT)) >= 0)
            {
                DLOG_FILE_DBG("newly create file %s success\r\n", backend_filesystem->cur_file_path);
                backend_filesystem->cur_file_fd = fd;
            }
            else
            {
                DLOG_FILE_ERR("newly create file %s failed\r\n", backend_filesystem->cur_file_path);
            }
        }
    }
    else
    {
        fd = backend_filesystem->cur_file_fd;
    }

    return fd;
}

static int dlog_file_rotate(dlog_backend_filesystem_t *backend_filesystem)
{
    /* rename n-1_log.txt => n_log.txt */
    char           cur_file_path[DLOG_FILE_PATH_MAX_LEN + 1]  = {0};
    char           file_path[DLOG_FILE_PATH_MAX_LEN + 1]      = {0};
    int            fd                                         = -1;
    int            ret                                        = -1;

    if (backend_filesystem->cur_file_fd >= 0)
    {
        DLOG_FILE_DBG("dlog file rotate close file: %s\r\n", backend_filesystem->cur_file_path);
        close(backend_filesystem->cur_file_fd);
        backend_filesystem->cur_file_fd = -1;
    }

    if (access(backend_filesystem->file_dir, F_OK) != 0)
    {
        DLOG_FILE_ERR("dlog file rotate open directory failed, path: %s\r\n", backend_filesystem->file_dir);
        return DF_DIR_ERROR;
    }

    snprintf(cur_file_path,
             DLOG_FILE_PATH_MAX_LEN,
             "%s%d_%s_%s",
             backend_filesystem->file_dir,
             backend_filesystem->cur_file_id,
             "new",
             backend_filesystem->file_name);
    snprintf(file_path,
             DLOG_FILE_PATH_MAX_LEN,
             "%s%d_%s",
             backend_filesystem->file_dir,
             backend_filesystem->cur_file_id,
             backend_filesystem->file_name);

    if (access(file_path, F_OK) == 0)
    {
        unlink(file_path);
        DLOG_FILE_INFO("dlog file rotate, delete old file %s\r\n", file_path);
    }
    rename(cur_file_path, file_path);
    DLOG_FILE_INFO("dlog file rotate, rename file %s => %s\r\n", cur_file_path, file_path);

    backend_filesystem->cur_file_id++;
    if (backend_filesystem->cur_file_id > backend_filesystem->file_max_num)
    {
        backend_filesystem->cur_file_id = 1;
    }

    snprintf(backend_filesystem->cur_file_path,
             DLOG_FILE_PATH_MAX_LEN,
             "%s%d_%s_%s",
             backend_filesystem->file_dir,
             backend_filesystem->cur_file_id,
             "new",
             backend_filesystem->file_name);

    fd = open(backend_filesystem->cur_file_path, O_RDWR | O_TRUNC | O_CREAT);
    if (fd >= 0)
    {
        DLOG_FILE_DBG("newly create file %s success\r\n", backend_filesystem->cur_file_path);
        backend_filesystem->cur_file_fd = fd;
        ret                             = DF_OK;
    }
    else
    {
        DLOG_FILE_ERR("newly create file %s failed\r\n", backend_filesystem->cur_file_path);
        ret = DF_FILE_ERROR;
    }

    return ret;
}

static ssize_t dlog_write_file_with_cache(dlog_backend_filesystem_t *backend_filesystem)
{
    char   *write_buff      = NULL;
    ssize_t write_len       = 0;
    ssize_t total_write_len = -1;
    ssize_t file_size       = 0;
    ssize_t cache_len       = 0;
    ssize_t remain_len      = 0;

    if (backend_filesystem->cache_buf_offset == 0)
    {
        DLOG_FILE_DBG("dlog filesystem backend file cache have nothing\r\n");
        return 0;
    }

    if (backend_filesystem->cache_buf_offset > backend_filesystem->cache_buf_size ||
        backend_filesystem->cache_buf_offset < 0)
    {
        DLOG_FILE_ERR("dlog filesystem backend file cache error\r\n");
        return -1;
    }

    if (backend_filesystem->cur_file_fd >= 0)
    {
        file_size = lseek(backend_filesystem->cur_file_fd, 0, SEEK_END);
        if (file_size != -1)
        {
            if (file_size >= backend_filesystem->file_max_size ||
                (backend_filesystem->file_max_size - file_size) < backend_filesystem->cache_buf_offset)
            {
                DLOG_FILE_DBG("dlog file with cache write file already full, %s now size: %d, cache buf offset: %d\r\n",
                              backend_filesystem->cur_file_path,
                              file_size,
                              backend_filesystem->cache_buf_offset);
                if (dlog_file_rotate(backend_filesystem) < 0)
                {
                    DLOG_FILE_DBG("dlog file with cache rotate file failed\r\n");
                    return -1;
                }
            }
            else
            {
                DLOG_FILE_DBG("dlog file with cache write file half, %s now size: %d\r\n",
                              backend_filesystem->cur_file_path,
                              file_size);
            }
        }
        else
        {
            DLOG_FILE_ERR("dlog file with cache lseek file failed, file path: %s\r\n",
                          backend_filesystem->cur_file_path);
            return -1;
        }
    }

    if (backend_filesystem->cur_file_fd >= 0)
    {
        write_buff = backend_filesystem->cache_buf;
        cache_len  = backend_filesystem->cache_buf_offset;
        remain_len = cache_len;
        while (remain_len > 0)
        {
            write_len = write(backend_filesystem->cur_file_fd, write_buff, remain_len);
            if (write_len > 0)
            {
                write_buff += write_len;
                remain_len -= write_len;
                total_write_len = cache_len - remain_len;
                DLOG_FILE_DBG("dlog file with cache write file success, write len %d\r\n", write_len);
            }
            else
            {
                close(backend_filesystem->cur_file_fd);
                backend_filesystem->cur_file_fd = -1;
                DLOG_FILE_ERR("dlog file with cache write file error, file name %s\r\n",
                              backend_filesystem->cur_file_path);
                return -1;
            }
        }

        fsync(backend_filesystem->cur_file_fd);
        backend_filesystem->cache_buf_offset = 0;
    }

    return total_write_len;
}

static ssize_t
dlog_write_file_with_noncache(dlog_backend_filesystem_t *backend_filesystem, char *log_buff, ssize_t log_len)
{
    char   *write_buff      = NULL;
    ssize_t write_len       = 0;
    ssize_t total_write_len = -1;
    ssize_t file_size       = 0;
    ssize_t remain_len      = 0;

    if (backend_filesystem->cur_file_fd >= 0)
    {
        file_size = lseek(backend_filesystem->cur_file_fd, 0, SEEK_END);
        if (file_size != -1)
        {
            if (file_size >= backend_filesystem->file_max_size ||
                (backend_filesystem->file_max_size - file_size) < log_len)
            {
                DLOG_FILE_DBG("dlog file with noncache write file already full, %s now size: %d\r\n",
                              backend_filesystem->cur_file_path,
                              file_size);
                if (dlog_file_rotate(backend_filesystem) < 0)
                {
                    DLOG_FILE_ERR("dlog file with noncache rotate file failed\r\n");
                    return -1;
                }
            }
            else
            {
                DLOG_FILE_DBG("dlog file with noncache write file half, %s now size: %d\r\n",
                              backend_filesystem->cur_file_path,
                              file_size);
            }
        }
        else
        {
            DLOG_FILE_ERR("dlog file with noncache lseek file failed, file path: %s\r\n",
                          backend_filesystem->cur_file_path);
            return -1;
        }
    }

    if (backend_filesystem->cur_file_fd >= 0)
    {
        write_buff = log_buff;
        remain_len = log_len;
        while (remain_len > 0)
        {
            write_len = write(backend_filesystem->cur_file_fd, write_buff, remain_len);
            if (write_len > 0)
            {
                write_buff += write_len;
                remain_len -= write_len;
                total_write_len = log_len - remain_len;
                DLOG_FILE_DBG("dlog file with noncache write file success, write len %d\r\n", write_len);
            }
            else
            {
                close(backend_filesystem->cur_file_fd);
                backend_filesystem->cur_file_fd = -1;
                DLOG_FILE_ERR("dlog file with noncache write file error, file name %s\r\n",
                              backend_filesystem->cur_file_path);
                return -1;
            }
        }

        fsync(backend_filesystem->cur_file_fd);
    }

    return total_write_len;
}

static void
dlog_filesystem_abnormal_put_data_in_cache(dlog_backend_filesystem_t *backend_filesystem, char *buff, ssize_t len)
{
    ssize_t cache_size;
    ssize_t cache_cur_offset;

    cache_size       = backend_filesystem->cache_buf_size;
    cache_cur_offset = backend_filesystem->cache_buf_offset;

    if (backend_filesystem->cache_buf != NULL && buff != NULL && cache_cur_offset >= 0 && cache_cur_offset < cache_size)
    {
        if (cache_cur_offset + len <= cache_size)
        {
            memcpy(backend_filesystem->cache_buf + cache_cur_offset, buff, len);
            cache_cur_offset += len;
        }
        else
        {
            memcpy(backend_filesystem->cache_buf + cache_cur_offset, buff, cache_size - cache_cur_offset);
            cache_cur_offset += (cache_size - cache_cur_offset);
        }
        backend_filesystem->cache_buf_offset = cache_cur_offset;
    }
    else
    {
        /* if cache full, the buff data will be discarded */
        DLOG_FILE_DBG("filesystem has not been mounted, cache may full!\r\n");
    }
}

static void dlog_filesystem_backend_flush_with_cache(dlog_backend_filesystem_t *backend_filesystem)
{
    if (vfs_check_mounted(DLOG_FILESYSTEM_FAL_PARTITION_NAME, "/", NULL) != 0)
    {
        DLOG_FILE_DBG("filesystem has not been mounted, flush is useless!\r\n");
        return;
    }

    if ((dlog_get_file(backend_filesystem)) < 0)
    {
        DLOG_FILE_DBG("dlog file with cache get file failed, cur_file_fd: %d, file path: %s \r\n",
                      backend_filesystem->cur_file_fd,
                      backend_filesystem->cur_file_path);
        return;
    }

    if (dlog_write_file_with_cache(backend_filesystem) < 0)
    {
        DLOG_FILE_ERR("dlog file with cache write file error\r\n");
    }
    else
    {
        DLOG_FILE_INFO("dlog file with cache has write all, now size = %d\r\n",
                       lseek(backend_filesystem->cur_file_fd, 0, SEEK_END));
    }
}

static void dlog_filesystem_backend_output_with_cache(dlog_backend_filesystem_t *backend_filesystem,
                                                      char                      *log_buff,
                                                      ssize_t                    log_len)
{
    char   *cache_start_ptr;
    ssize_t cache_size;
    ssize_t cache_cur_offset;
    ssize_t cache_free_len;
    ssize_t log_buff_offset;
    ssize_t copy_len;

    /* vfs_init or auto_mount_fs later than dlog_filesystem_backend_init，
       so here may not been mounted currently, log data will be cached */
    if (vfs_check_mounted(DLOG_FILESYSTEM_FAL_PARTITION_NAME, "/", NULL) != 0)
    {
        dlog_filesystem_abnormal_put_data_in_cache(backend_filesystem, log_buff, log_len);
        DLOG_FILE_DBG("filesystem has not been mounted, dlog to file is cached!\r\n");
        return;
    }

    cache_start_ptr = backend_filesystem->cache_buf;
    cache_size      = backend_filesystem->cache_buf_size;
    log_buff_offset = 0;

    while (log_buff_offset < log_len)
    {
        cache_cur_offset = backend_filesystem->cache_buf_offset;
        if (cache_cur_offset >= 0 && cache_cur_offset <= cache_size)
        {
            cache_free_len = cache_size - cache_cur_offset;
            if (log_len - log_buff_offset <= cache_free_len)
            {
                copy_len = log_len - log_buff_offset;
            }
            else
            {
                copy_len = cache_free_len;
            }

            if (copy_len > 0)
            {
                memcpy(cache_start_ptr + cache_cur_offset, log_buff + log_buff_offset, copy_len);
                log_buff_offset += copy_len;
                backend_filesystem->cache_buf_offset += copy_len;
            }

            if (backend_filesystem->cache_buf_offset == cache_size)
            {
                dlog_filesystem_backend_flush_with_cache(backend_filesystem);
            }
        }
        else
        {
            DLOG_FILE_ERR("dlog_filesystem_backend_output_with_cache should not come in\r\n");
        }
    }

    return;
}

static void dlog_filesystem_backend_output_with_noncache(dlog_backend_filesystem_t *backend_filesystem,
                                                         char                      *log_buff,
                                                         ssize_t                    log_len)
{
    /* vfs_init or auto_mount_fs later than dlog_filesystem_backend_init，
       so here may not been mounted currently, log data will be discarded
       because of not enable file cache */
    if (vfs_check_mounted(DLOG_FILESYSTEM_FAL_PARTITION_NAME, "/", NULL) != 0)
    {
        DLOG_FILE_DBG("filesystem has not been mounted, dlog to file is discarded!\r\n");
        return;
    }

    if ((dlog_get_file(backend_filesystem)) < 0)
    {
        DLOG_FILE_ERR("dlog file with noncache get file failed, cur_file_fd: %d, file path: %s \r\n",
                      backend_filesystem->cur_file_fd,
                      backend_filesystem->cur_file_path);
        return;
    }

    if (dlog_write_file_with_noncache(backend_filesystem, log_buff, log_len) <= 0)
    {
        DLOG_FILE_ERR("dlog file with noncache write file error\r\n");
    }
    else
    {
        DLOG_FILE_INFO("dlog file with noncache has write all, now size = %d\r\n",
                       lseek(backend_filesystem->cur_file_fd, 0, SEEK_END));
    }

    return;
}

static void dlog_filesystem_backend_output(struct dlog_backend *backend, char *log, os_size_t len)
{
    OS_ASSERT(OS_NULL != backend);
    OS_ASSERT(OS_NULL != log);

    dlog_backend_filesystem_t *backend_filesystem = (dlog_backend_filesystem_t *)backend;

    if (backend_filesystem->enable == OS_FALSE)
    {
        DLOG_FILE_WARN("dlog filesystem backend is been disabled\r\n");
        return;
    }

    /* 1 is '\0' or '\033', not write it into log file */
    len = len - 1;

    if (backend_filesystem->enable_cache)
    {
        dlog_filesystem_backend_output_with_cache(backend_filesystem, log, (ssize_t)len);
    }
    else
    {
        dlog_filesystem_backend_output_with_noncache(backend_filesystem, log, (ssize_t)len);
    }

    return;
}

static void dlog_filesystem_backend_flush(struct dlog_backend *backend)
{
    dlog_backend_filesystem_t *backend_filesystem = (dlog_backend_filesystem_t *)backend;
    OS_ASSERT(OS_NULL != backend);

    if (backend_filesystem->enable == OS_FALSE)
    {
        DLOG_FILE_WARN("dlog filesystem backend is been disabled\r\n");
        return;
    }

    dlog_filesystem_backend_flush_with_cache(backend_filesystem);

    return;
}

void dlog_filesystem_backend_enable(void)
{
    gs_backend_filesystem.enable = OS_TRUE;
}

void dlog_filesystem_backend_disable(void)
{
    if (gs_backend_filesystem.cur_file_fd >= 0)
    {
        if (gs_backend_filesystem.enable_cache == OS_TRUE)
        {
            dlog_filesystem_backend_flush_with_cache(&gs_backend_filesystem);
        }
        close(gs_backend_filesystem.cur_file_fd);
    }
    gs_backend_filesystem.cur_file_id = 0;
    gs_backend_filesystem.cur_file_fd = -1;
    gs_backend_filesystem.enable      = OS_FALSE;
}

void dlog_filesystem_backend_file_config(const char *file_name,
                                         const char *file_dir,
                                         ssize_t     file_max_size,
                                         int         file_max_num)
{
    dlog_filesystem_backend_disable();

    if (file_max_size != 0)
    {
        gs_backend_filesystem.file_max_size = file_max_size;
        DLOG_FILE_INFO("dlog file max size %d\r\n", file_max_size);
    }
    if (file_max_num != 0)
    {
        gs_backend_filesystem.file_max_num = file_max_num;
        DLOG_FILE_INFO("dlog file max num %d\r\n", file_max_num);
    }
    if (file_name != NULL)
    {
        memset(gs_backend_filesystem.file_name, 0, DLOG_FILE_NAME_MAX_LEN + 1);
        strncpy(gs_backend_filesystem.file_name, file_name, DLOG_FILE_NAME_MAX_LEN);
        DLOG_FILE_INFO("dlog file name %s\r\n", file_name);
    }
    if (file_dir != NULL)
    {
        memset(gs_backend_filesystem.file_dir, 0, DLOG_FILE_DIR_MAX_LEN + 1);
        strncpy(gs_backend_filesystem.file_dir, file_dir, DLOG_FILE_DIR_MAX_LEN);
        DLOG_FILE_INFO("dlog file dir %s\r\n", file_dir);
    }
    if (file_name != NULL || file_dir != NULL)
    {
        memset(gs_backend_filesystem.cur_file_path, 0, DLOG_FILE_PATH_MAX_LEN + 1);
        gs_backend_filesystem.cur_file_id = 1;
        snprintf(gs_backend_filesystem.cur_file_path,
                 DLOG_FILE_PATH_MAX_LEN,
                 "%s%d_%s_%s",
                 gs_backend_filesystem.file_dir,
                 gs_backend_filesystem.cur_file_id,
                 "new",
                 gs_backend_filesystem.file_name);
    }

    dlog_filesystem_backend_enable();
}

static int sh_dlog_filesystem_backend_file_config(int argc, char *argv[])
{
    char   *file_name     = NULL;
    char   *file_dir      = NULL;
    ssize_t file_max_size = 0;
    int     file_max_num  = 0;

    if (argc < 3)
    {
        DLOG_FILE_ERR("Input error, please input: dlog_fs_config -f log.txt -d /dlog/ -s 2048 -n 5\r\n");
        return -1;
    }

    for (int i = 1; i < argc; i += 2)
    {
        if (strstr(argv[i], "-f"))
        {
            if (i + 1 < argc)
            {
                file_name = argv[i + 1];
            }
            continue;
        }
        if (strstr(argv[i], "-d"))
        {
            if (i + 1 < argc)
            {
                file_dir = argv[i + 1];
            }
            continue;
        }
        if (strstr(argv[i], "-s"))
        {
            if (i + 1 < argc)
            {
                file_max_size = strtoul(argv[i + 1], NULL, 10);
            }
            continue;
        }
        if (strstr(argv[i], "-n"))
        {
            if (i + 1 < argc)
            {
                file_max_num = strtoul(argv[i + 1], NULL, 10);
            }
            continue;
        }
    }

    dlog_filesystem_backend_file_config(file_name, file_dir, file_max_size, file_max_num);

    return 0;
}

void dlog_filesystem_backend_deinit(void)
{
    if (gs_backend_filesystem.cur_file_fd >= 0)
    {
        if (gs_backend_filesystem.enable_cache == OS_TRUE)
        {
            dlog_filesystem_backend_flush_with_cache(&gs_backend_filesystem);
        }
        close(gs_backend_filesystem.cur_file_fd);
    }

    gs_backend_filesystem.cur_file_id  = 0;
    gs_backend_filesystem.cur_file_fd  = -1;
    gs_backend_filesystem.parent.flush = NULL;

    if (gs_backend_filesystem.cache_buf != NULL)
    {
        os_free(gs_backend_filesystem.cache_buf);
        gs_backend_filesystem.cache_buf = NULL;
    }

    dlog_backend_unregister((struct dlog_backend *)&gs_backend_filesystem);
}

int dlog_filesystem_backend_init(void)
{
    int ret;

    ret = dlog_init();
    if (OS_SUCCESS == ret)
    {
        memset(&gs_backend_filesystem, 0, sizeof(dlog_backend_filesystem_t));
        strncpy(gs_backend_filesystem.parent.name, "filesystem", OS_NAME_MAX);

        gs_backend_filesystem.parent.support_isr   = OS_FALSE;
        gs_backend_filesystem.parent.support_color = OS_FALSE;
        gs_backend_filesystem.parent.output        = dlog_filesystem_backend_output;
        gs_backend_filesystem.parent.flush         = NULL;

        gs_backend_filesystem.enable        = OS_TRUE;
        gs_backend_filesystem.file_max_size = DLOG_FILE_SIZE;
        gs_backend_filesystem.file_max_num  = DLOG_FILE_NUM;
        gs_backend_filesystem.cur_file_id   = 0;
        gs_backend_filesystem.cur_file_fd   = -1;
        gs_backend_filesystem.enable_cache  = DLOG_FILE_USING_CACHE;
        strncpy(gs_backend_filesystem.file_name, DLOG_FILE_NAME, DLOG_FILE_NAME_MAX_LEN);
        strncpy(gs_backend_filesystem.file_dir, DLOG_FILE_DIR, DLOG_FILE_DIR_MAX_LEN);

        gs_backend_filesystem.cache_buf        = NULL;
        gs_backend_filesystem.cache_buf_size   = 0;
        gs_backend_filesystem.cache_buf_offset = 0;

        if (gs_backend_filesystem.enable_cache == OS_TRUE)
        {
            gs_backend_filesystem.parent.flush     = dlog_filesystem_backend_flush;
            gs_backend_filesystem.cache_buf        = os_malloc(DLOG_FILE_CACHE_BUF_SIZE);
            gs_backend_filesystem.cache_buf_size   = DLOG_FILE_CACHE_BUF_SIZE;
            gs_backend_filesystem.cache_buf_offset = 0;
            OS_ASSERT(NULL != gs_backend_filesystem.cache_buf);
        }

        (void)dlog_backend_register((struct dlog_backend *)&gs_backend_filesystem);
    }

    return ret;
}
OS_INIT_CALL(dlog_filesystem_backend_init, OS_INIT_LEVEL_POST_KERNEL, OS_INIT_SUBLEVEL_MIDDLE);

#ifdef OS_USING_SHELL
#include <shell.h>
SH_CMD_EXPORT(dlog_fs_enable, dlog_filesystem_backend_enable, "dlog filesystem backend enable");
SH_CMD_EXPORT(dlog_fs_disable, dlog_filesystem_backend_disable, "dlog filesystem backend disable");
SH_CMD_EXPORT(dlog_fs_config, sh_dlog_filesystem_backend_file_config, "dlog filesystem backend file config");
#endif

#endif /* DLOG_BACKEND_USING_FILESYSTEM */
