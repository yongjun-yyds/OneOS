/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        filesystem_backend.h
 *
 * @brief       head file of filesystem backend.
 *
 * @revision
 * Date         Author          Notes
 * 2023-10-18   jixiaowei       first version
 ***********************************************************************************************************************
 */

#ifndef _FILESYSTEM_BACKEND_H_
#define _FILESYSTEM_BACKEND_H_

#include <sys/types.h>
#include <stdbool.h>
#include <dlog.h>
#include <oneos_config.h>

#ifdef DLOG_BACKEND_USING_FILESYSTEM

#define DF_PRT_LEVEL DLOG_FILESYSTE_INTERNAL_PRINT_LEVEL

#define DF_NO_PRT  0 /* No print */
#define DF_ERROR   1 /* Error conditions */
#define DF_WARNING 2 /* Warning conditions */
#define DF_INFO    3 /* Informational */
#define DF_DEBUG   4 /* Debug messages */

#if (DF_ERROR <= DF_PRT_LEVEL)
#define DLOG_FILE_ERR(fmt, ...) os_kprintf(fmt, ##__VA_ARGS__)
#else
#define DLOG_FILE_ERR(fmt, ...)
#endif

#if (DF_WARNING <= DF_PRT_LEVEL)
#define DLOG_FILE_WARN(fmt, ...) os_kprintf(fmt, ##__VA_ARGS__)
#else
#define DLOG_FILE_WARN(fmt, ...)
#endif

#if (DF_INFO <= DF_PRT_LEVEL)
#define DLOG_FILE_INFO(fmt, ...) os_kprintf(fmt, ##__VA_ARGS__)
#else
#define DLOG_FILE_INFO(fmt, ...)
#endif

#if (DF_DEBUG <= DF_PRT_LEVEL)
#define DLOG_FILE_DBG(fmt, ...) os_kprintf(fmt, ##__VA_ARGS__)
#else
#define DLOG_FILE_DBG(fmt, ...)
#endif

#define DLOG_FILE_NAME_MAX_LEN 63
#define DLOG_FILE_DIR_MAX_LEN  63
#define DLOG_FILE_PATH_MAX_LEN 127

typedef struct dlog_backend_filesystem
{
    struct dlog_backend parent;
    bool                enable;
    ssize_t             file_max_size;
    int                 file_max_num;
    char                file_name[DLOG_FILE_NAME_MAX_LEN + 1];
    char                file_dir[DLOG_FILE_DIR_MAX_LEN + 1];
    char                cur_file_path[DLOG_FILE_PATH_MAX_LEN + 1];
    int                 cur_file_id;
    int                 cur_file_fd;
    bool                enable_cache;
    char               *cache_buf;
    ssize_t             cache_buf_size;
    ssize_t             cache_buf_offset;
} dlog_backend_filesystem_t;

int  dlog_filesystem_backend_init(void);
void dlog_filesystem_backend_deinit(void);
void dlog_filesystem_backend_enable(void);
void dlog_filesystem_backend_disable(void);
void dlog_filesystem_backend_file_config(const char *file_name,
                                         const char *file_dir,
                                         ssize_t     file_max_size,
                                         int         file_max_num);

#endif /* DLOG_BACKEND_USING_FILESYSTEM */

#endif /* _FILESYSTEM_BACKEND_H_ */
