#include <vfs.h>
#include <vfs_fs.h>
#include <stdio.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <os_util.h>
#include <block/block_device.h>
#include "fx_api.h"

//#define FX_TAG "filex"
extern void  fx_lite_io_driver(FX_MEDIA *media_ptr);

#define ENTER_V_FX(v_ffx)        pthread_mutex_lock(&(v_ffx->lock))
#define LEAVE_V_FX(v_ffx)        pthread_mutex_unlock(&(v_ffx->lock))
struct v_fx_entry
{
    os_list_node_t          list_node;
    char                   *name;
    uint16_t             ref_cnt;
};

struct v_fx{
    FX_MEDIA               *fx_media;
    os_list_node_t          open_entry_head;
    pthread_mutex_t        lock;
};

static struct v_fx *v_fx_init(FX_MEDIA *v_fx_media)
{
    //char name[OS_NAME_MAX];
    struct v_fx *v_fx_ins;

    v_fx_ins = malloc(sizeof(struct v_fx));
    if (!v_fx_ins)
    {
        return OS_NULL;
    }

    v_fx_ins->fx_media = v_fx_media;
    os_list_init(&v_fx_ins->open_entry_head);

    pthread_mutexattr_t mutex_attr;
    int ret = 0;
    ret = pthread_mutexattr_init(&mutex_attr);
    ret = pthread_mutexattr_settype(&mutex_attr, PTHREAD_MUTEX_RECURSIVE);
    ret = pthread_mutex_init(&(v_fx_ins->lock), &mutex_attr);
    if (ret != 0)
    {
        os_kprintf("Error initializing v_fx_init mutex\n");
    }

    return v_fx_ins;
}

static int v_fx_deinit(struct v_fx *v_fx_ins)
{
    os_list_node_t *head;
    struct v_fx_entry *v_entry;

    if (os_list_empty(&v_fx_ins->open_entry_head))
    {
        pthread_mutex_destroy(&(v_fx_ins->lock));
        free(v_fx_ins);
        return 0;
    }
    else
    {
        head = &v_fx_ins->open_entry_head;
        ENTER_V_FX(v_fx_ins);
        os_list_for_each_entry(v_entry, head, struct v_fx_entry, list_node)
        {
            //LOG_W(FX_TAG, "This file/dir should be closed:%s", v_entry->name);
        }
        LEAVE_V_FX(v_fx_ins);
        return -1;
    }
}

static int v_fx_add_entry(struct v_fx *v_fx_ins, const char *path)
{
    os_list_node_t *head;
    struct v_fx_entry *v_entry;
    struct v_fx_entry *v_entry_temp;
    os_bool_t   foundflag = OS_FALSE;
    uint32_t len;
    int result = 0; 

    head = &v_fx_ins->open_entry_head;
    len = strlen(path);

    ENTER_V_FX(v_fx_ins);
    os_list_for_each_entry_safe(v_entry, v_entry_temp, head, struct v_fx_entry, list_node)
    {
        if (strcmp(v_entry->name, path) == 0)
        {
            v_entry->ref_cnt++;
            foundflag = OS_TRUE;
            break;
        }
    }
    if (foundflag == OS_FALSE)
    {
        v_entry = malloc(sizeof(struct v_fx_entry));
        if (!v_entry)
        {
            result = -1;
            goto end;
        }
        v_entry->name = malloc(len + 1);
        if (!v_entry->name)
        {
            result = -1;
            goto end;
        }
        memset(v_entry->name, 0 , len + 1);
        strlcpy(v_entry->name, path, len+1);
        os_list_add(&v_fx_ins->open_entry_head, &v_entry->list_node);
        v_entry->ref_cnt = 1;
    }

end:
    LEAVE_V_FX(v_fx_ins);

    return result;
}

static int v_fx_del_entry(struct v_fx *v_fx_ins, const char *path)
{
    os_list_node_t *head;
    struct v_fx_entry *v_entry;
    struct v_fx_entry *v_entry_temp;
    int result = -1; 

    head = &v_fx_ins->open_entry_head;
    ENTER_V_FX(v_fx_ins);
    os_list_for_each_entry_safe(v_entry, v_entry_temp, head, struct v_fx_entry, list_node)
    {
        if (strcmp(v_entry->name, path) == 0)
        {
            if (--v_entry->ref_cnt == 0)
            {
                os_list_del(&v_entry->list_node);
                free(v_entry->name);
                free(v_entry);
                result = 0;
            }
            break;
        }
    }
    LEAVE_V_FX(v_fx_ins);

    return result;
}

static os_bool_t v_fx_find_entry(struct v_fx *v_fx_ins, const char *path)
{
    os_list_node_t *head;
    struct v_fx_entry *v_entry;
    os_bool_t result = OS_FALSE; 

    head = &v_fx_ins->open_entry_head;
    ENTER_V_FX(v_fx_ins);
    os_list_for_each_entry(v_entry, head, struct v_fx_entry, list_node)
    {
        if (strcmp(v_entry->name, path) == 0)
        {
            result = OS_TRUE;
            break;
        }
    }
    LEAVE_V_FX(v_fx_ins);

    return result;
}



static int fx_ret_to_vfs(int val)
{
    int err = 0;

    switch (val)
    {
    case FX_SUCCESS:
    case FX_END_OF_FILE:
        err = 0;
        break;

    case FX_BOOT_ERROR:
    case FX_MEDIA_INVALID:
    case FX_MEDIA_NOT_OPEN:
    case FX_SECTOR_INVALID:
    case FX_IO_ERROR:
        err = -EIO;
        break;

    case FX_FAT_READ_ERROR:
    case FX_FILE_CORRUPT:
        err = -EILSEQ;
        break;

    case FX_NOT_FOUND:
    case FX_NO_MORE_ENTRIES:
        err = -ENOENT;
        break;

    case FX_ALREADY_CREATED:
        err = -EEXIST;
        break;

    case FX_NOT_DIRECTORY:
        err = -ENOTDIR;
        break;

    case FX_NOT_A_FILE:
        err = -EISDIR;
        break;

    case FX_ACCESS_ERROR:
        err = -EACCES;
        break;
    
    case FX_DIR_NOT_EMPTY:
        err = -ENOTEMPTY;
        break;

    case FX_NOT_OPEN:
        err = -EBADF;
        break;

    case FX_INVALID_PATH:
    case FX_INVALID_ATTR:
    case FX_INVALID_OPTION:
        err = -EINVAL;
        break;

    case FX_NO_MORE_SPACE:
        err = -ENOSPC;
        break;

    case FX_NOT_ENOUGH_MEMORY:
        err = -ENOMEM;
        break;

    case FX_INVALID_NAME:
        err = -ENAMETOOLONG;
        break;

    default:
        err = -1;
        break;
    }

    return err;
}

static int vfs_fx_mount(struct vfs_mountpoint *mnt_point, unsigned long mountflag, const void *data)
{
    int  ret = -ENOENT;
    FX_MEDIA     *v_fx_media;
    size_t        buffer_size;
    void         *media_memory;
    struct v_fx  *v_fx_ins;
#ifdef FX_ENABLE_FAULT_TOLERANT
    void         *fault_tolerant_buffer;
#endif

    if (!mnt_point || !mnt_point->dev)
    {
        return -EINVAL;
    }

    buffer_size = sizeof(FX_MEDIA) + FX_MEDIA_BUFSIZE + 4;
#ifdef FX_ENABLE_FAULT_TOLERANT
    buffer_size = buffer_size + FAULT_TOLERANT_SIZE + 4;
#endif

    v_fx_media = malloc(buffer_size);
    if (!v_fx_media)
    {
        return -ENOMEM;
    }
    memset(v_fx_media, 0, buffer_size);

    media_memory = (char*)v_fx_media + sizeof(FX_MEDIA) + 4;
#ifdef FX_ENABLE_FAULT_TOLERANT
    fault_tolerant_buffer = (char*)v_fx_media + sizeof(FX_MEDIA) + FX_MEDIA_BUFSIZE + 8;
#endif

    ret =  fx_media_open(v_fx_media, (char*)mnt_point->dev, fx_lite_io_driver, 0, media_memory, FX_MEDIA_BUFSIZE);
    if (ret != FX_SUCCESS) 
    {
        free(v_fx_media);
        ret = fx_ret_to_vfs(ret);
        return ret;
    }

#ifdef FX_ENABLE_FAULT_TOLERANT
    /* Enable fault tolerant. */
    ret = fx_fault_tolerant_enable(v_fx_media, fault_tolerant_buffer, FAULT_TOLERANT_SIZE);
    if (ret != FX_SUCCESS) 
    {
        free(v_fx_media);
        ret = fx_ret_to_vfs(ret);
        return ret;
    }
#endif

    v_fx_ins = v_fx_init(v_fx_media);
    mnt_point->fs_instance = (void *)v_fx_ins;
    return 0;
}

static int vfs_fx_unmount(struct vfs_mountpoint *mnt_point)
{
    int ret = -ENOENT;
    FX_MEDIA     *v_fx_media;
    struct v_fx  *v_fx_ins;

    if ((!mnt_point) || (!mnt_point->fs_instance) || (!mnt_point->dev))
    {
        return -EINVAL;
    }
    
    v_fx_ins  = mnt_point->fs_instance;
    v_fx_media = v_fx_ins->fx_media;

    if (v_fx_deinit(v_fx_ins) != 0)
    {
        return -EINVAL;
    }

    ret = fx_media_close(v_fx_media);
    if (ret != FX_SUCCESS)
    {
        ret = fx_ret_to_vfs(ret);
    }
    else
    {
        free(v_fx_media);
        mnt_point->fs_instance = OS_NULL;
        ret = 0;
    }

    return ret;
}

static int vfs_fx_mkfs(void *dev)
{
    int  ret = -ENOENT;
    FX_MEDIA     *v_fx_media;
    void         *media_memory;
    struct os_blk_geometry geometry;

    if (!dev)
    {
        return -EINVAL;
    }
    if (os_device_open((os_device_t *)dev) != OS_SUCCESS)
    {
        return-EIO;
    }
    if(os_device_control((os_device_t *)dev, OS_DEVICE_CTRL_BLK_GETGEOME, &geometry) != OS_SUCCESS)
    {
        return -EIO;
    }
    os_device_close((os_device_t *)dev);
    
    media_memory = malloc(FX_MEDIA_BUFSIZE);
    if (!media_memory)
    {
        return -ENOMEM;
    }
    memset(media_memory, 0, sizeof(FX_MEDIA_BUFSIZE));

    v_fx_media = malloc(sizeof(FX_MEDIA));
    if (!v_fx_media)
    {
        free(media_memory);
        return -ENOMEM;
    }
    memset(v_fx_media, 0, sizeof(FX_MEDIA));

    v_fx_media->fx_media_name = dev;

    ret = fx_media_format(v_fx_media,
                    fx_lite_io_driver,             // Driver entry
                    0,                             // disk memory pointer
                    media_memory,                  // Media buffer pointer
                    sizeof(media_memory),          // Media buffer size
                    ((os_device_t *)dev)->name,    // Volume Name
                    FX_FATS_NUM,                   // Number of FATs
                    32,                            // Directory Entries
                    0,                             // Hidden sectors
                    geometry.capacity/(geometry.block_size*FX_BLOCK_SIZE_FACTOR),  // Total sectors
                    geometry.block_size*FX_BLOCK_SIZE_FACTOR,           // Sector size
                    FX_SECTOR_PER_CLUSTER,         // Sectors per cluster
                    1,                             // Heads
                    1);                            // Sectors per track

    free(media_memory);
    free(v_fx_media);

    if (ret != FX_SUCCESS)
    {
        ret = fx_ret_to_vfs(ret);
    }
    else
    {
        ret = 0;
    }

    return ret;
}

static int vfs_fx_statfs(struct vfs_mountpoint *mnt_point, struct statfs *buf)
{
    FX_MEDIA     *v_fx_media;
    struct v_fx  *v_fx_ins;
    unsigned int  ret = 0;

    if ((!mnt_point) || (!mnt_point->fs_instance) || (!buf))
    {
        return -EINVAL;
    }

    v_fx_ins  = mnt_point->fs_instance;
    v_fx_media = v_fx_ins->fx_media;
    if (!v_fx_media)
    {
        return -EINVAL;
    }

    buf->f_bsize  =  v_fx_media -> fx_media_bytes_per_sector;
    buf->f_blocks =  v_fx_media -> fx_media_total_sectors;
    buf->f_bfree  =  v_fx_media -> fx_media_available_clusters * v_fx_media -> fx_media_sectors_per_cluster;

    return ret;
}

static int vfs_fx_open(struct vfs_file *file)
{
    uint32_t         oflag;
    uint32_t         fx_flag;
    char            *name;
    struct vfs_mountpoint *mnt_point;
    struct v_fx     *v_fx_ins;
    FX_MEDIA        *v_fx_media;
    FX_FILE         *v_fx_file;
    int              ret  = 0;
	UINT attributes;

    if ((!file) || (!file->path) || (!file->mnt_point) || (!file->mnt_point->fs_instance))
    {
        return -EINVAL;
    }

    mnt_point  = (struct vfs_mountpoint *)file->mnt_point;
    v_fx_ins = (struct v_fx *)mnt_point->fs_instance;
    v_fx_media = v_fx_ins->fx_media;

    oflag = file->flags;
    /* Escape the '/' provided by vfs code */
    name = (char *)file->path;
    // if ((name[0] == '/') && (name[1] == 0))
    // {
    //     name = ".";
    // }
    // else
    // {
    //     /* name[0] still will be '/' */
    //     name++;
    // }

    fx_flag = 0;
    /* Handle regular file operations */
    if (oflag & O_WRONLY)
    {
        /* fx filesystem not support write only, FX_OPEN_FOR_WRITE can read&&write */
        fx_flag = FX_OPEN_FOR_WRITE;
    }
    else if (oflag & O_RDWR)
    {
        fx_flag = FX_OPEN_FOR_WRITE;
    }
    else
    {
        fx_flag = FX_OPEN_FOR_READ;
    }

    ret =  fx_file_attributes_read(v_fx_media, (char *)name, &attributes);
    /* Opens the file, if it is existing. If not, a new file is created. */
    if ((ret == FX_NOT_FOUND) && (oflag & O_CREAT))
    {
        ret = fx_file_create(v_fx_media, name);
		if( ret == FX_SUCCESS)
		{
		    fx_media_flush(v_fx_media);
		}
    }
    else if ((ret == FX_SUCCESS) && (oflag & O_CREAT) && (oflag & O_EXCL))
    {
        /* Creates a new file. The function fails if the file is already existing. */
        ret = FX_ALREADY_CREATED;
    }

    v_fx_file = os_calloc(1, sizeof(FX_FILE));
    if (OS_NULL == v_fx_file)
    {
        ret = -ENOMEM;
    }
    
    if( ret == FX_SUCCESS)
    {
        ret =  fx_file_open(v_fx_media, v_fx_file, name, fx_flag);
        /* Creates a new file. If the file is existing, it is truncated and overwritten. */
        if ((ret == FX_SUCCESS) && (oflag & O_TRUNC))
        {
            ret =  fx_file_truncate(v_fx_file, 0);
			file->off_ptr = 0;
        }
        else if ((ret == FX_SUCCESS) && (oflag & O_APPEND))
        {
            ret =  fx_file_relative_seek(v_fx_file, 0, FX_SEEK_END);
			file->off_ptr = (int)v_fx_file -> fx_file_current_file_offset;
        }
        else
        {
            ret =  fx_file_seek(v_fx_file, 0);
			file->off_ptr = 0;
        }
    }

    if (ret != FX_SUCCESS)
    {
        free(v_fx_file);
        ret = fx_ret_to_vfs(ret);
        //LOG_E(FX_TAG, "openfile failed.\r\n");
    }
    else
    {
        /* Save this pointer, it will be used when calling read(), write(),
         * flush(), lseek(), and will be free when calling close(). */
        file->desc = v_fx_file;
        v_fx_add_entry(v_fx_ins, file->path);
    }

    return ret;
}

static int vfs_fx_close(struct vfs_file *file)
{
    int              ret = 0;
    FX_FILE         *v_fx_file;
    struct v_fx     *v_fx_ins;

    if ((!file) || (!file->mnt_point) || (!file->mnt_point->fs_instance) || (!file->desc))
    {
        return -EINVAL;
    }

    v_fx_ins = (struct v_fx *)file->mnt_point->fs_instance;

    v_fx_file = (FX_FILE *)file->desc;
    /* Handle regular file  */
    ret = fx_file_close(v_fx_file);
    if (ret != FX_SUCCESS)
    {
        free(v_fx_file);
        ret = fx_ret_to_vfs(ret);
        //LOG_E(FX_TAG, "close failed.\r\n");
    }
    else
    {
        free(file->desc);
        file->desc = OS_NULL;
        v_fx_del_entry(v_fx_ins, file->path);
    }

    

    return ret;
}

static int vfs_fx_read(struct vfs_file *file, void *buf, size_t size)
{
    ULONG            read_cnt;
    FX_FILE         *v_fx_file;
    int              ret;

    if ((!file) || (!file->mnt_point) || (!file->mnt_point->fs_instance) || (!file->desc) || (!buf))
    {
        return -EINVAL;
    }

    if (FT_DIRECTORY == file->type)
    {
        return -EISDIR;
    }

    if (file->flags & O_WRONLY)
    {
        return -EINVAL;
    }

    v_fx_file  = (FX_FILE *)file->desc;

    ret =  fx_file_seek(v_fx_file, file->off_ptr);
    ret =  fx_file_read(v_fx_file, buf, size, &read_cnt);
    if (ret != FX_SUCCESS)
    {
        ret = fx_ret_to_vfs(ret);
    }
    else
    {
        ret = (int)read_cnt;
		file->off_ptr += read_cnt;
    }

    return ret;
}

static int vfs_fx_write(struct vfs_file *file, const void *buf, size_t size)
{
    FX_FILE         *v_fx_file;
    struct vfs_mountpoint *mnt_point;
    struct v_fx     *v_fx_ins;
    FX_MEDIA        *v_fx_media;
    int              ret;

    if ((!file) || (!file->mnt_point) || (!file->mnt_point->fs_instance) || (!file->desc) || (!buf))
    {
        return  -EINVAL;
    }

    if ((!(file->flags & O_RDWR)) && (!(file->flags & O_WRONLY)))
    {
        return -EINVAL;
    }

    if (FT_DIRECTORY == file->type)
    {
        return -EISDIR;
    }

    mnt_point  = (struct vfs_mountpoint *)file->mnt_point;
    v_fx_ins = (struct v_fx *)mnt_point->fs_instance;
    v_fx_media = v_fx_ins->fx_media;
    v_fx_file  = (FX_FILE *)file->desc;
    ret =  fx_file_seek(v_fx_file, file->off_ptr);
    ret =  fx_file_write(v_fx_file, (void *)buf, size);
    if (ret != FX_SUCCESS)
    {
        ret = fx_ret_to_vfs(ret);
    }
    else
    {
        fx_media_flush(v_fx_media);
        ret = (int)size;
		file->off_ptr += size;
    }

    return ret;
}

static int vfs_fx_sync(struct vfs_file *file)
{
    int              ret;
    FX_MEDIA        *v_fx_media;
    struct v_fx     *v_fx_ins;

    if ((!file) || (!file->mnt_point) || (!file->mnt_point->fs_instance) || (!file->desc))
    {
        return -EINVAL;
    }

    v_fx_ins  = file->mnt_point->fs_instance;
    v_fx_media = v_fx_ins->fx_media;
    ret = fx_media_flush(v_fx_media);
    if (ret != FX_SUCCESS)
    {
        ret = fx_ret_to_vfs(ret);
    }
    else
    {
        ret = 0;
    }

    return ret;
}

static int vfs_fx_lseek(struct vfs_file *file, off_t offset, int whence)
{
    FX_FILE         *v_fx_file;
    int              fx_whence;
    int              ret = 0;

    if ((!file) || (!file->mnt_point) || (!file->mnt_point->fs_instance) || (!file->desc))
    {
        return -EINVAL;
    }

    if (file->type != FT_REGULAR)
    {
        return -EINVAL;
    }

    v_fx_file  = (FX_FILE *)file->desc;

    switch (whence)
    {
    case SEEK_SET:
        fx_whence = FX_SEEK_BEGIN;
        if (offset < 0)
        {
            ret = -EINVAL;
        }
        break;
    case SEEK_CUR:
        if (offset < 0)
        {
            fx_whence = FX_SEEK_BACK;
            offset = -offset;
        }
        else
        {
            fx_whence = FX_SEEK_FORWARD;
        }
        break;
    case SEEK_END:
        fx_whence = FX_SEEK_END;
        if (offset < 0)
        {
            offset = -offset;
        }
        else
        {
            offset = 0;
        }
        break;
    default:
        ret = -EINVAL;
        break;
    }

    if (0 == ret)
    {
        ret =  fx_file_relative_seek(v_fx_file, offset, fx_whence);

        if (ret != FX_SUCCESS)
        {
            ret = fx_ret_to_vfs(ret);
        }
        else
        {
            ret = (int)v_fx_file -> fx_file_current_file_offset;
            file->off_ptr = ret;
        }
    }

    return ret;
}

static int vfs_fx_opendir(struct vfs_dir *dp)
{
    uint32_t         oflag;
    char            *name;
    FX_MEDIA        *v_fx_media;
    struct v_fx     *v_fx_ins;
    UINT             attributes;
    int              ret  = 0;

    if ((!dp) || (!dp->fp) || (!dp->fp->path) || (!dp->fp->mnt_point) || (!dp->fp->mnt_point->fs_instance))
    {
        return -EINVAL;
    }

    oflag = dp->fp->flags;
    /* Escape  the '/' provided by vfs code */
    name = (char *)dp->fp->path;
    // if ((name[0] == '/') && (name[1] == 0))
    // {
    //     name = ".";
    // }
    // else 
    // {
    //     /* name[0] still will be '/' */
    //     name++;
    // }

    v_fx_ins   = dp->fp->mnt_point->fs_instance;
    v_fx_media = v_fx_ins->fx_media;
    // v_fx_file = os_calloc(1, sizeof(FX_FILE));
    // if (OS_NULL == data)
    // {
    //     return -ENOMEM;
    // }

    /* Create a dir*/
    if (oflag & O_CREAT)
    {
        ret = fx_directory_create(v_fx_media, name);
        if (ret != FX_SUCCESS)
        {
            ret = fx_ret_to_vfs(ret);
            return ret;
        }
        fx_media_flush(v_fx_media);
    }

    if (name[0] == '/' && name[1] == '\0')
    {
        return 0;
    }

    ret =  fx_directory_attributes_read(v_fx_media, name, &attributes);

    /* Check the attributes read status.  */
    if ((ret != FX_SUCCESS) || (attributes != FX_DIRECTORY))
    {
        ret = fx_ret_to_vfs(ret);
    }
    else
    {
        v_fx_add_entry(v_fx_ins, dp->fp->path);
    }

    return ret;
}

static int vfs_fx_closedir(struct vfs_dir *dp)
{
    int              ret = 0;
    //FX_MEDIA        *v_fx_media;
    struct v_fx     *v_fx_ins;

    if ((!dp) || (!dp->fp) || (!dp->fp->path) || (!dp->fp->mnt_point) || (!dp->fp->mnt_point->fs_instance))
    {
        return -EINVAL;
    }

    v_fx_ins = dp->fp->mnt_point->fs_instance;
    // v_fx_media = (FX_MEDIA *)dp->fp->mnt_point->fs_instance;
    // ret = lfs_dir_close(&part_info->lfs, (lfs_dir_t *)dp->fp->desc);
    // if (ret)
    // {
    //     ret = fx_ret_to_vfs(ret);
    // }
    // else
    // {
    //     ret = 0;
    // }

    // free(dp->fp->desc);
    // dp->fp->desc = OS_NULL;
    v_fx_del_entry(v_fx_ins, dp->fp->path);

    return ret;
}

static int vfs_fx_seekdir(struct vfs_dir *dp, off_t offset)
{
#if 1
    if ((!dp) || (!dp->fp))
    {
        return -EINVAL;
    }

    dp->fp->off_ptr = offset;
    return offset;

#else
    int              ret = 0;
    FX_MEDIA        *v_fx_media;
    char    *cur_path;
    off_t    cur_ptr = 0;
    char     entry_name[256];
    uint32_t attributes;
    ULONG    size;
    uint32_t year;
    uint32_t month;
    uint32_t day;
    uint32_t hour;
    uint32_t minute;
    uint32_t second;
    uint32_t i;

    if ((!dp) || (!dp->fp) || (!dp->fp->mnt_point) || (!dp->fp->mnt_point->fs_instance) || offset < 0)
    {
        return -EINVAL;
    }

    v_fx_media = (FX_MEDIA *)dp->fp->mnt_point->fs_instance;
    cur_path = (char *)dp->fp->path;
    cur_ptr  = dp->fp->off_ptr;

    /* Set the current path.   */
    ret =  fx_directory_default_set(v_fx_media, cur_path);
    /* Check the status.  */
    if (ret != FX_SUCCESS) 
    {
        /* Error setting the path.  Return to caller.  */
        LOG_E(FX_TAG,"Set the current path error!\r\n");
        ret = fx_ret_to_vfs(ret);
        return ret;
    }

    for (i = 0; i <= offset; i++)
	{
        if(i != 0)
        {
        /* 读取目录项，索引会自动下移 */
            ret = fx_directory_next_full_entry_find(v_fx_media,
                                                    entry_name, 
                                                    &attributes, 
                                                    &size,
                                                    &year, &month, &day, 
                                                    &hour, &minute, &second);
        
            if (ret != FX_SUCCESS || entry_name[0] == 0)
		    {
			break;
		    }
        }
        else
        {
            /* Pickup the first entry in the root directory.  */
            ret =  fx_directory_first_full_entry_find(v_fx_media, 
                                                         entry_name, 
                                                         &attributes, 
                                                         &size,
                                                         &year, &month, &day, 
                                                         &hour, &minute, &second);
        }
	}

    if (ret != FX_SUCCESS)
    {
        ret = fx_ret_to_vfs(ret);
    }
    else
    {
        cur_ptr = i;
        ret = (int)cur_ptr;
    }

    return ret;
#endif
}

/* Return the size of struct dirent*/
static int vfs_fx_readdir(struct vfs_dir *dp, struct dirent *dent)
{
    int              ret;
    FX_MEDIA        *v_fx_media;
    struct v_fx     *v_fx_ins;
    char    *cur_path;
    off_t    cur_ptr = 0;
    char     entry_name[256];
    UINT attributes;
    ULONG    size;
    UINT year;
    UINT month;
    UINT day;
    UINT hour;
    UINT minute;
    UINT second;
    uint32_t i;

    if ((!dp) || (!dp->fp) || (!dp->fp->mnt_point) || (!dp->fp->mnt_point->fs_instance) || (!dent))
    {
        return -EINVAL;
    }

    v_fx_ins  = dp->fp->mnt_point->fs_instance;
    v_fx_media = v_fx_ins->fx_media;

    cur_path = (char *)dp->fp->path;
    cur_ptr  = dp->fp->off_ptr;

    /* Set the current path. This should be / or NULL.   */
    ret =  fx_directory_default_set(v_fx_media, cur_path);
    /* Check the status.  */
    if (ret != FX_SUCCESS) 
    {
        /* Error setting the path.  Return to caller.  */
        //LOG_E(FX_TAG,"Set the current path error!\r\n");
        ret = fx_ret_to_vfs(ret);
        return ret;
    }

    for (i = 0; i <= cur_ptr; i++)
	{
        if(i != 0)
        {
        /* 读取目录项，索引会自动下移 */
            ret = fx_directory_next_full_entry_find(v_fx_media,
                                                    entry_name, 
                                                    &attributes, 
                                                    &size,
                                                    &year, &month, &day, 
                                                    &hour, &minute, &second);
        
            if (ret != FX_SUCCESS || entry_name[0] == 0)
		    {
			break;
		    }
        }
        else
        {
            /* Pickup the first entry in the root directory.  */
            ret =  fx_directory_first_full_entry_find(v_fx_media, 
                                                         entry_name, 
                                                         &attributes, 
                                                         &size,
                                                         &year, &month, &day, 
                                                         &hour, &minute, &second);
        }
	}

    if (ret != FX_SUCCESS)
    {
        /* Read error. */
        ret = fx_ret_to_vfs(ret);
    }
    else if(entry_name[0] == 0)
    {
        ret = 0;
    }
    else
    {
        /* Read success. */
        if (attributes & FX_ARCHIVE)
		{
			dent->d_type = DT_REG;
		}
        else if (attributes & FX_DIRECTORY)
        {
            dent->d_type = DT_DIR;
        }
		else
		{
			dent->d_type = DT_UNKNOWN;
		}

        /* Write the rest fields of struct dirent* dirp  */
        strlcpy(dent->d_name, entry_name, sizeof(dent->d_name));
        dp->fp->off_ptr = cur_ptr + 1;
        ret = 1;
    }

    return ret;
}

long vfs_fx_telldir(struct vfs_dir *dp)
{
    if ((!dp) || (!dp->fp))
    {
        return -EINVAL;
    }
    else
    {
        return dp->fp->off_ptr;
    }
}

static int vfs_fx_unlink(struct vfs_mountpoint *mnt_point, const char *path)
{
    int              ret = 0;
    FX_MEDIA        *v_fx_media;
    struct v_fx     *v_fx_ins;

    if ((!mnt_point) || (!mnt_point->fs_instance) || (!path))
    {
        return -EINVAL;
    }

    v_fx_ins   = mnt_point->fs_instance;
    v_fx_media = v_fx_ins->fx_media;
        
    /* Skip starting '/' */
    // if (path[0] == '/')
    // {
    //     path++;
    // }
    if (v_fx_find_entry(v_fx_ins, path) == OS_TRUE)
    {
        //LOG_W(FX_TAG, "This file/dir should be closed before delete:%s", path);
        return -EINVAL;
    }

    ret = fx_file_delete(v_fx_media, (char *)path);
    if (ret == FX_NOT_A_FILE)
    {
        ret = fx_directory_delete(v_fx_media, (char *)path);
    }
    
    if (ret == FX_SUCCESS)
    {
        fx_media_flush(v_fx_media);
        ret = 0;
    }
    else
    {
        ret = fx_ret_to_vfs(ret);
    }

    return ret;
}

static int vfs_fx_rename(struct vfs_mountpoint *mnt_point, const char *oldpath, const char *newpath)
{
    int              ret = 0;
    FX_MEDIA        *v_fx_media;
    struct v_fx     *v_fx_ins;

    if ((!mnt_point) || (!mnt_point->fs_instance) || (!oldpath) || (!newpath))
    {
        return -EINVAL;
    }

    v_fx_ins   = mnt_point->fs_instance;
    v_fx_media = v_fx_ins->fx_media;

    // if ('/' == *oldpath)
    // {
    //     oldpath += 1;
    // }    
    // if ('/' == *newpath)
    // {
    //     newpath += 1;
    // }
    if (v_fx_find_entry(v_fx_ins, oldpath) == OS_TRUE)
    {
        //LOG_W(FX_TAG, "This file/dir should be closed:%s", oldpath);
        return -EINVAL;
    }
    if (v_fx_find_entry(v_fx_ins, newpath) == OS_TRUE)
    {
        //LOG_W(FX_TAG, "This file/dir should be closed:%s", newpath);
        return -EINVAL;
    }

    ret = fx_file_rename(v_fx_media, (char *)oldpath, (char *)newpath);
    if (ret == FX_NOT_A_FILE)
    {
        ret = fx_directory_rename(v_fx_media, (char *)oldpath, (char *)newpath);
    }
    
    if (ret == FX_SUCCESS)
    {
        fx_media_flush(v_fx_media);
        ret = 0;
    }
    else
    {
        ret = fx_ret_to_vfs(ret);
    }

    return ret;
}

static int vfs_fx_stat(struct vfs_mountpoint *mnt_point, const char *path, struct stat *st)
{
    FX_MEDIA        *v_fx_media;
    struct v_fx     *v_fx_ins;
    int              ret = 0;
    UINT         attributes;
    ULONG    size;
    UINT year;
    UINT month;
    UINT day;
    UINT hour;
    UINT minute;
    UINT second;

    if ((!mnt_point) || (!mnt_point->fs_instance) || (!path) || (!st))
    {
        return -EINVAL;
    }

    // if (path[0] == '/')
    // {
    //     path++;
    // }

    if (path[0] == '/' && path[1] == '\0')
    {
        st->st_mode  = S_IRUSR | S_IRGRP | S_IROTH | S_IWUSR | S_IWGRP | S_IWOTH | 
                        S_IXUSR | S_IXGRP | S_IXOTH| S_IFDIR;
        st->st_size  = 0;
        st->st_mtime = 0;
        return 0;
    }

    //os_kprintf("ls path:%s\r\n",path);
        
    v_fx_ins   = mnt_point->fs_instance;
    v_fx_media = v_fx_ins->fx_media;

    ret = fx_directory_information_get(v_fx_media, (char *)path, &attributes, &size,
                                        &year, &month, &day, &hour, &minute, &second);

    if (ret != FX_SUCCESS)
    {
        ret = fx_ret_to_vfs(ret);
    }
    else
    {
        memset(st, 0, sizeof(struct stat));
        st->st_mode = S_IRUSR | S_IRGRP | S_IROTH | S_IWUSR | S_IWGRP | S_IWOTH;

        /* Convert to vfs stat structure */
        if (FX_ARCHIVE & attributes)
        {
            st->st_mode |= S_IFREG;
            st->st_size  = size;
        }
        else
        {
            /*Directory*/
            st->st_mode |= S_IFDIR | S_IXUSR | S_IXGRP | S_IXOTH;
        }

        ret = 0;
    }

    return ret;
}

static const struct vfs_filesystem_ops _fx_ops =
{
    "filex",

    vfs_fx_open,
    vfs_fx_close,
    vfs_fx_read,
    vfs_fx_write,
    vfs_fx_lseek,
    vfs_fx_sync,
    OS_NULL,               /* Not support ioctl. */
#ifdef OS_USING_IO_MULTIPLEXING
    OS_NULL,                /* Not support poll. */
#endif
    vfs_fx_opendir,
    vfs_fx_closedir,
    vfs_fx_readdir,
    vfs_fx_seekdir,
    vfs_fx_telldir,

    vfs_fx_rename,
    vfs_fx_unlink,
    vfs_fx_stat,

    vfs_fx_mkfs,
    vfs_fx_statfs,
    vfs_fx_mount,
    vfs_fx_unmount,
};


/**
 ***********************************************************************************************************************
 * @brief           This function do filex file system initialization work. 
 *
 * @param[]         None
 *
 * @return          Return 0 on successful, -1 on failed.
 ***********************************************************************************************************************
 */
int vfs_fx_init(void)
{
    /* Register filex file system */
    fx_system_initialize();
    vfs_register(&_fx_ops);

    return 0;
}
OS_INIT_CALL(vfs_fx_init, OS_INIT_LEVEL_COMPONENT, OS_INIT_SUBLEVEL_LOW);

