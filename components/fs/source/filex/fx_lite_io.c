/***************************************************************************
 * Copyright (c) 2024 Microsoft Corporation 
 * 
 * This program and the accompanying materials are made available under the
 * terms of the MIT License which is available at
 * https://opensource.org/licenses/MIT.
 * 
 * SPDX-License-Identifier: MIT
 **************************************************************************/

/* Include necessary system files.  */
#include <sys/errno.h>
#include <os_util.h>
#include <block/block_device.h>
#include <dlog.h>
#include "fx_api.h"

#define FX_TAG "filex"

/* The driver relies on the fx_media_format call to be made prior to
   the fx_media_open call. The following call will format the default
   drive, with a sector size of 512 bytes(maybe) per sector.

    fx_media_format(&sdio_disk,
                    fx_stm32_sd_driver,               // Driver entry
                    0,              // RAM disk memory pointer
                    media_memory,                 // Media buffer pointer
                    sizeof(media_memory),         // Media buffer size
                    "fx_disk",            // Volume Name
                    1,                            // Number of FATs
                    32,                           // Directory Entries
                    0,                            // Hidden sectors
                    geometry.capacity/geometry.block_size,     // Total sectors
                    geometry.block_size,                          // Sector size
                    128,                            // Sectors per cluster
                    255,                            // Heads
                    63);                           // Sectors per track
 */

VOID  fx_lite_io_driver(FX_MEDIA *media_ptr)
{

UCHAR *source_buffer;
UCHAR *destination_buffer;
os_ssize_t len;
os_device_t *device;

    /* Process the driver request specified in the media control block.  */
    switch (media_ptr -> fx_media_driver_request)
    {

    case FX_DRIVER_READ:
    {
        /* Calculate the disk sector offset. Note the disk is pointed to by
           the fx_media_driver_info pointer, which is supplied by the application in the
           call to fx_media_open.  */
        source_buffer =  (UCHAR *)media_ptr -> fx_media_driver_info + (media_ptr -> fx_media_driver_logical_sector + media_ptr -> fx_media_hidden_sectors);

        LOG_D(FX_TAG,"read from device\r\n");
        LOG_D(FX_TAG,"logical_sector:%d, hidden_sectors:%d, sectors:%d\r\n", media_ptr -> fx_media_driver_logical_sector, media_ptr -> fx_media_hidden_sectors, media_ptr -> fx_media_driver_sectors);
        LOG_D(FX_TAG,"source_buffer:%p, destination_buffer:%p\r\n", source_buffer, media_ptr -> fx_media_driver_buffer);
        
        /* Copy the disk sector into the destination.  */
        len = os_device_read_nonblock((os_device_t *)media_ptr -> fx_media_name, (os_off_t)source_buffer*FX_BLOCK_SIZE_FACTOR, (void *)media_ptr -> fx_media_driver_buffer, (os_size_t)media_ptr -> fx_media_driver_sectors*FX_BLOCK_SIZE_FACTOR);
        if(len != media_ptr -> fx_media_driver_sectors*FX_BLOCK_SIZE_FACTOR)
        {
            media_ptr -> fx_media_driver_status =  FX_IO_ERROR;
            break;
        }
        /* Successful driver request.  */
        media_ptr -> fx_media_driver_status =  FX_SUCCESS;
        break;
    }

    case FX_DRIVER_WRITE:
    {
        /* Calculate the disk sector offset. Note the disk memory is pointed to by
           the fx_media_driver_info pointer, which is supplied by the application in the
           call to fx_media_open.  */
        destination_buffer = (UCHAR *)media_ptr -> fx_media_driver_info + (media_ptr -> fx_media_driver_logical_sector + media_ptr -> fx_media_hidden_sectors);

        LOG_D(FX_TAG,"write to device\r\n");
        LOG_D(FX_TAG,"logical_sector:%d, hidden_sectors:%d, sectors:%d\r\n", media_ptr -> fx_media_driver_logical_sector, media_ptr -> fx_media_hidden_sectors, media_ptr -> fx_media_driver_sectors);
        LOG_D(FX_TAG,"source_buffer:%p, destination_buffer:%p, bytes_per_sector:%d\r\n", media_ptr -> fx_media_driver_buffer, destination_buffer, media_ptr -> fx_media_bytes_per_sector);
        
        /* Copy the source to the sector.  */
        len = os_device_write_nonblock((os_device_t *)media_ptr -> fx_media_name, (os_off_t)destination_buffer*FX_BLOCK_SIZE_FACTOR, (void *)media_ptr -> fx_media_driver_buffer, (os_size_t)media_ptr -> fx_media_driver_sectors*FX_BLOCK_SIZE_FACTOR);
        if(len != media_ptr -> fx_media_driver_sectors*FX_BLOCK_SIZE_FACTOR)
        {
            media_ptr -> fx_media_driver_status =  FX_IO_ERROR;
            break;
        }
        /* Successful driver request.  */
        media_ptr -> fx_media_driver_status =  FX_SUCCESS;
        break;
    }

    case FX_DRIVER_FLUSH:
    {
        /* Return driver success.  */
        media_ptr -> fx_media_driver_status =  FX_SUCCESS;
        break;
    }

    case FX_DRIVER_ABORT:
    {
        /* Return driver success.  */
        media_ptr -> fx_media_driver_status =  FX_SUCCESS;
        break;
    }

    case FX_DRIVER_INIT:
    {
        device = (os_device_t *)media_ptr -> fx_media_name;
        // device = os_device_find(media_ptr -> fx_media_name);
        // if (!device)
        // {
        //     return;
        // }

        if (os_device_open(device) != OS_SUCCESS)
        {
            return;
        }
        
        if (device == OS_NULL)
        {
            /* Error, the device is not found */
            media_ptr -> fx_media_driver_status =  FX_MEDIA_NOT_OPEN;
            return;
        }
		
        /* Successful driver request.  */
        media_ptr -> fx_media_name = (VOID *)device;
        /* The fx_media_driver_free_sector_update flag is used to instruct
           FileX to inform the driver whenever sectors are not being used.
           This is especially useful for FLASH managers so they don't have
           maintain mapping for sectors no longer in use. */
        media_ptr -> fx_media_driver_free_sector_update = FX_TRUE;
        media_ptr -> fx_media_driver_status =  FX_SUCCESS;
        break;
    }

    case FX_DRIVER_UNINIT:
    {
        /* For actual devices some shutdown processing may be necessary.  */
		device = (os_device_t *)media_ptr -> fx_media_name;
        os_device_close(device);
        /* Successful driver request.  */
        media_ptr -> fx_media_driver_status =  FX_SUCCESS;
        break;
    }

    case FX_DRIVER_BOOT_READ:
    {
        /* Read the boot record and return to the caller.  
           Calculate the disk boot sector offset, which is at the very beginning of
           the disk. Note the disk memory is pointed to by the fx_media_driver_info pointer, 
           which is supplied by the application in the call to fx_media_open.  */
        source_buffer      =  (UCHAR *)media_ptr -> fx_media_driver_info;
        destination_buffer =  (UCHAR *)media_ptr -> fx_media_driver_buffer;

        len = os_device_read_nonblock((os_device_t *)media_ptr -> fx_media_name,(os_off_t)source_buffer*FX_BLOCK_SIZE_FACTOR, (void *)media_ptr -> fx_media_driver_buffer,1*FX_BLOCK_SIZE_FACTOR);
        if(len != 1*FX_BLOCK_SIZE_FACTOR)
        {
            media_ptr -> fx_media_driver_status =  FX_IO_ERROR;
            break;
        }

        /* For driver, determine if the boot record is valid.  */
        if ((destination_buffer[0] != (UCHAR)0xEB)  ||
            ((destination_buffer[1] != (UCHAR)0x34)  &&
             (destination_buffer[1] != (UCHAR)0x76)) ||
            (destination_buffer[2] != (UCHAR)0x90))
        {
            /* Invalid boot record, return an error!  */
            media_ptr -> fx_media_driver_status =  FX_MEDIA_INVALID;
            return;
        }
        /* Successful driver request.  */
        media_ptr -> fx_media_driver_status =  FX_SUCCESS;
        break;
    }

    case FX_DRIVER_BOOT_WRITE:
    {
        /* Write the boot record and return to the caller.  */
        /* Calculate the disk boot sector offset, which is at the very beginning of the
           disk. Note the disk memory is pointed to by the fx_media_driver_info
           pointer, which is supplied by the application in the call to fx_media_open.  */
        destination_buffer =  (UCHAR *)media_ptr -> fx_media_driver_info;

        LOG_D(FX_TAG,"write boot record to device");
        LOG_D(FX_TAG,"source_buffer:%p, destination_buffer:%p, bytes_per_sector:%d", media_ptr -> fx_media_driver_buffer, destination_buffer, media_ptr -> fx_media_bytes_per_sector);

        len = os_device_write_nonblock((os_device_t *)media_ptr -> fx_media_name,(os_off_t)destination_buffer*FX_BLOCK_SIZE_FACTOR, (void *)media_ptr -> fx_media_driver_buffer,1*FX_BLOCK_SIZE_FACTOR);
        if(len != 1*FX_BLOCK_SIZE_FACTOR)
        {
            media_ptr -> fx_media_driver_status =  FX_IO_ERROR;
            break;
        }
        /* Successful driver request.  */
        media_ptr -> fx_media_driver_status =  FX_SUCCESS;
        break;
    }

    default:
    {
        /* Invalid driver request.  */
        media_ptr -> fx_media_driver_status =  FX_IO_ERROR;
        break;
    }
    }
}
