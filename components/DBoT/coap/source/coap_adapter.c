#include "coap_adapter.h"
#include "dbot_log.h"
#include "dbot_common.h"
#include "coap3/net.h"
#include "coap3/pdu.h"

#ifdef _WIN32
#include <winsock2.h>
#elif __linux__
#include <arpa/inet.h>
#endif

#define COAP_TOKEN      "\xfe\x0b\xad\xa9"
#define COAP_TOKEN_LEN  4
#define COAP_PORT       COAP_DEFAULT_PORT

static unsigned char _token_data[8];
coap_binary_t base_token = { 0, _token_data };

unsigned int wait_seconds = 90;                /* default timeout in seconds */
unsigned int wait_ms = 0;
unsigned int ready = 0;
int wait_ms_reset = 0;
static int doing_getting_block = 0;
static char *response_buff = NULL;
static int  written_buff_len = 0;
static nb_response_handler response_handler;
static coap_optlist_t *optlist = NULL;


static coap_pdu_t *
coap_new_request(coap_context_t *ctx,
                 coap_session_t *session,
                 unsigned char m,
                 coap_optlist_t **options,
                 unsigned char *data,
                 size_t length,
                 os_bool_t is_con) {
  coap_pdu_t *pdu;
  (void)ctx;

  if (!(pdu = coap_new_pdu(is_con ? COAP_MESSAGE_CON : COAP_MESSAGE_NON, m, session)))
    return NULL;

  if (!coap_add_token(pdu, base_token.length, base_token.s)) {
    coap_log(LOG_DEBUG, "cannot add token to request\r\n"); //need to update coap log
  }

  if (options)
    coap_add_optlist_pdu(pdu, options);

  if (length) {
    /* Let the underlying libcoap decide how this data should be sent */
    coap_add_data_large_request(session, pdu, length, data,
                                NULL, data);
  }

  return pdu;
}

static os_bool_t check_token(coap_bin_const_t *token)
{
    os_bool_t ret = true;
    int index = 0;
    if(base_token.length != token->length)
    {
        ret = false;
    }
    else
    {
        for(;index<base_token.length;index++)
        {
            if( *(base_token.s + index*sizeof(char))  !=  *(token->s + index*sizeof(char)))
            {
                ret = false;
                break;
            }
        }
    }
    return ret;
}

static inline void append_to_response(char *data, int length)
{
    if (MAX_RESPONSE_BUFFER_SIZE < written_buff_len + length)
    {
        DBOT_LOG_E("Oversized of response buffer[MAX = %d]\r\n", MAX_RESPONSE_BUFFER_SIZE);
        return;
    }
    memcpy(response_buff+written_buff_len, data, length);
    written_buff_len += length;
}

static coap_response_t message_handler(coap_session_t *session,
                                                   const coap_pdu_t *sent,
                                                   const coap_pdu_t *received,
                                                   const coap_mid_t mid)
{
  coap_opt_t *block_opt;
  coap_opt_iterator_t opt_iter;
  size_t len;
  const uint8_t *databuf;
  size_t offset;
  size_t total;
  coap_pdu_code_t rcv_code = coap_pdu_get_code(received);
  coap_pdu_type_t rcv_type = coap_pdu_get_type(received);
  coap_bin_const_t token = coap_pdu_get_token(received);

  DBOT_LOG_D("** process incoming %d.%02d response:\r\n",
           COAP_RESPONSE_CLASS(rcv_code), rcv_code & 0x1F);
  if (coap_get_log_level() < LOG_DEBUG)
    coap_show_pdu(LOG_INFO, received);

  /* check if this is a response to our original request */
  if (!check_token(&token)) {
    /* drop if this was just some message, or send RST in case of notification */
    if (!sent && (rcv_type == COAP_MESSAGE_CON ||
                  rcv_type == COAP_MESSAGE_NON)) {
      /* Cause a CoAP RST to be sent */
      return COAP_RESPONSE_FAIL;
    }
    return COAP_RESPONSE_OK;
  }

  if (rcv_type == COAP_MESSAGE_RST) {
    coap_log(LOG_INFO, "got RST\n");
    return COAP_RESPONSE_OK;
  }

  /* output the received data, if any */
  if (COAP_RESPONSE_CLASS(rcv_code) == 2) {


    if (coap_get_data_large(received, &len, &databuf, &offset, &total)) {
      append_to_response((char *)databuf, len);
    }

    /* Check if Block2 option is set */
    block_opt = coap_check_option(received, COAP_OPTION_BLOCK2, &opt_iter);
    if (block_opt) { /* handle Block2 */

        if (coap_opt_block_num(block_opt) == 0) {
        /* See if observe is set in first response */
         ready = 1;
        }

      if(COAP_OPT_BLOCK_MORE(block_opt)) {
        wait_ms = wait_seconds * 1000;
        wait_ms_reset = 1;
        doing_getting_block = 1;
      }
      else {
        doing_getting_block = 0;
      }
      return COAP_RESPONSE_OK;
    }
  } else {      /* no 2.05 */
    /* check if an error was signaled and output payload if so */
    DBOT_LOG_E("CoAP message send failed[err code = %d.%02d]\r\n", COAP_RESPONSE_CLASS(rcv_code), rcv_code & 0x1F);
    return COAP_RESPONSE_FAIL;    
  }

  /* our job is done, we can exit at any time */
  ready = 1;
  response_handler(mid, response_buff);
  return COAP_RESPONSE_OK;
}

void
static nb_init_publish_service(coap_context_t *ctx, service_handler_map_t *service_handler_map, int service_num) 
{
    coap_resource_t *r;
    service_handler_map_t service_handler_map_inst;
    int index = 0;
    for (index = 0; index < service_num; index++)
    {
        service_handler_map_inst = service_handler_map[index];
        r = coap_resource_init(coap_make_str_const(service_handler_map_inst.service_name), 0);
        if(!r)
        {
            DBOT_LOG_E("Resource init for %s service failed\r\n", service_handler_map_inst.service_name);
            continue;
        }
        coap_resource_set_get_observable(r, 1);
        coap_register_handler(r, service_handler_map_inst.method, service_handler_map_inst.service_handler);
        coap_add_resource(ctx, r);
    }
}

void convert_to_coap_addr(const char *addr, const int port, coap_address_t *coap_addr) 
{
    memset(coap_addr, 0, sizeof(coap_address_t));
    coap_addr->size = sizeof(coap_addr->addr);
    coap_addr->addr.sin.sin_family = AF_INET;
    coap_addr->addr.sin.sin_addr.s_addr = inet_addr(addr);
    coap_addr->addr.sin.sin_port = htons(port);
}

coap_context_t* nb_coap_server_init(service_handler_map_t *service_handler_map, int service_num)
{
    coap_context_t *coap_ctx;
    coap_address_t coap_addr;
    
    if(NULL == response_buff)
    {
      response_buff = calloc(1, MAX_RESPONSE_BUFFER_SIZE);
      if(NULL == response_buff)
      {
          DBOT_LOG_E("Failed to initialize coap buffer\r\n");
          return NULL;
      }
    }
    convert_to_coap_addr(SERVER_IP_STR, COAP_PORT, &coap_addr);
    coap_ctx = coap_new_context(&coap_addr);
    if(NULL != coap_ctx)
    {
        nb_init_publish_service(coap_ctx, service_handler_map, service_num);
    }
    else
    {
        DBOT_LOG_E("Failed to initialize context\r\n"); //should implemented as assertation added by chenyixi
        abort();
    }
    
    return coap_ctx;
}

void nb_coap_server_start(coap_context_t *ctx, uint32_t timeout)
{
    if(!ctx)
    {
        DBOT_LOG_E("Coap context is null. Cannot start a Coap server\r\n");
        return;
    }
    coap_startup();
    while(1) {
        coap_io_process(ctx, timeout);
    }
}

coap_context_t* nb_coap_client_init()
{
    coap_context_t* coap_ctx;
    coap_ctx = coap_new_context(NULL); 
    if (!coap_ctx) 
    {
      DBOT_LOG_E("Coap context initialize failed\r\n");
    }
    return coap_ctx;
}

static void init_base_token()
{
    char token_char[COAP_TOKEN_LEN] = COAP_TOKEN;
    base_token.length = COAP_TOKEN_LEN;
    memcpy((char *)base_token.s, token_char, base_token.length);
}

static void init_optlist(char *service_name)
{
    unsigned char _buf[MAX_SERVICE_NAME_SIZE];
    unsigned char *buf = _buf;
    size_t buflen = MAX_SERVICE_NAME_SIZE;
    int res;
    res = coap_split_path((const uint8_t *)service_name, strlen(service_name), buf, &buflen);
    while (res--) {
        coap_insert_optlist(&optlist,
                            coap_new_optlist(COAP_OPTION_URI_PATH,
                            coap_opt_length(buf),
                            coap_opt_value(buf)));
        buf += coap_opt_size(buf);
      }
    
}

static int nb_coap_send_block(char *payload, int data_len, char *server_ip, char *service_name, coap_request_t method, uint32_t timeout_ms, coap_context_t *coap_ctx, os_bool_t is_con)
{
    int ret_code = 0;
    int msg_id = 0;
    coap_pdu_t *pdu;
    coap_session_t *coap_session;
    // 只需要在coap服务启动的时候调用coap_startup
    // coap_startup();
    coap_context_set_block_mode(coap_ctx, COAP_BLOCK_USE_LIBCOAP);
    coap_address_t coap_addr;
    convert_to_coap_addr(server_ip, COAP_PORT, &coap_addr);
    coap_session = coap_new_client_session(coap_ctx, NULL, &coap_addr, COAP_PROTO_UDP);
    if(NULL == coap_session)
    {
        DBOT_LOG_E("failed to initialize client session\r\n");
        return -1;
    }
    
    if(NULL != service_name)
    {
        init_optlist(service_name);
    }
    coap_session_init_token(coap_session, base_token.length, base_token.s);
    coap_register_option(coap_ctx, COAP_OPTION_BLOCK2);
    if(is_con)
    {
        coap_register_response_handler(coap_ctx, message_handler);
    } 
    init_base_token();
    pdu = coap_new_request(coap_ctx, coap_session, method, &optlist, (uint8_t *)payload, data_len, is_con);
    if(!pdu)
    {
        ret_code = -1;
    }
    else
    {
        msg_id = coap_send(coap_session, pdu);
        if(COAP_INVALID_MID == msg_id)
        {
            ret_code = -1;
        }
        if (is_con)
            coap_io_process(coap_ctx, timeout_ms);
    }
    coap_delete_optlist(optlist);
    optlist = NULL;
    coap_session_release(coap_session);
    coap_free_context(coap_ctx);
    // 只需要在coap服务停止的的时候调用coap_cleanup
    // coap_cleanup();

    return ret_code;
}


int nb_send_coap_con_msg(client_req_t req, char *server_ip, coap_context_t *context)
{
    int msg_len = strlen(req.msg) + 1;
    response_handler = req.handler;
    return nb_coap_send_block(req.msg, msg_len, server_ip, req.service_name, req.method, req.wait_timeout_ms, context, true);
}

int nb_send_coap_nocon_msg(client_req_t req, char *server_ip, coap_context_t *context)
{
    int msg_len = strlen(req.msg) + 1;
    // DBOT_LOG_I("send msg: %s\n", req.msg);
    return nb_coap_send_block(req.msg, msg_len, server_ip, req.service_name, req.method, req.wait_timeout_ms, context, false);
}

void dbot_send_request(char *address, char *service_name, char *send_buf)
{
    coap_context_t *context;
    client_req_t req;

    context = nb_coap_client_init();
    req.handler = NULL;
    req.method = COAP_REQUEST_POST;
    req.msg = send_buf;
    req.service_name = service_name;
    req.wait_timeout_ms = COAP_IO_WAIT;
    nb_send_coap_nocon_msg(req, address, context);
}
