#ifndef __ONEBUS_CPADPT_H__
#define __ONEBUS_CPADPT_H__

#include <stdbool.h>

#include "cJSON.h"
#include "coap3/coap.h"
#include "nb_types.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef void(*nb_response_handler)(int mid, char *response);

typedef struct
{
    char *service_name;
    coap_request_t method;
    coap_method_handler_t service_handler;
}service_handler_map_t;

typedef struct
{
    char *service_name;
    coap_request_t method;
    uint32_t wait_timeout_ms;
    char *msg;
    nb_response_handler handler;
}client_req_t;


#define MAX_SERVICE_NAME_SIZE 64
#define MAX_RESPONSE_BUFFER_SIZE 1024

extern coap_context_t* nb_coap_client_init(void);
extern void nb_coap_server_start(coap_context_t *ctx, uint32_t timeout);
extern coap_context_t* nb_coap_server_init(service_handler_map_t *service_handler_map, int service_num);
extern int nb_send_coap_con_msg(client_req_t req, char *server_ip, coap_context_t *ctx);
extern int nb_send_coap_nocon_msg(client_req_t req, char *server_ip, coap_context_t *ctx);
extern void dbot_send_request(char *address, char *service_name, char *send_buf);

#ifdef __cplusplus
}
#endif

#endif
