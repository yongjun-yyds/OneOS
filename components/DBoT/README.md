
# DBoT

## 简介
DBoT(Distributed Bus of Things)，分布式物联总线。DBoT基于设备组建的网络构建一条虚拟的“总线”，并将设备分解为多个子应用连接到“总线”上，以此呈现给用户一个大的虚拟设备，用户可以基于该“总线”调用设备内的子应用，甚至可以对多个子应用进行组合构建更复杂的应用，以此达到分布式通信的目的。DBoT将设备根据功能分为两类：能力发布端，能力发现端，一个设备允许同时具备能力发布和能力发现的功能。能力发布：设备将自身具备的功能提炼为能力，能力具备一定的属性，将自己具备的能力发布到DBoT网络内。比如猫眼可以提炼为视频，音频两项能力，视频具备播放、停止、截屏属性；智能灯具可以提炼为灯能力，具备打开灯、关闭灯、调节灯光强度、调节灯光色彩四种属性。能力发现：设备可以到DBoT网络内搜索其他设备发布的能力，并根据自身需要调用其他设备的能力。

## 主要数据结构

### dbot能力定义
```c
typedef enum
{
    CAPABILITY_TYPE_LIGHT = 0,      /* 灯 */
    CAPABILITY_TYPE_TEMP = 1,       /* 温度 */
    CAPABILITY_TYPE_HUMI = 2,       /* 湿度 */
    CAPABILITY_TYPE_LCD = 3,        /* 屏 */
    CAPABILITY_TYPE_ILLU = 4,       /* 光照强度 */
    CAPABILITY_TYPE_CAMERA = 5,     /* 摄像头 */
    CAPABILITY_TYPE_MICROPHONE = 6, /* 麦克风 */
    CAPABILITY_TYPE_SPEAKER = 7,    /* 喇叭 */
    CAPABILITY_TYPE_SWITCH = 8,     /* 开关 */
    CAPABILITY_TYPE_FILESYSTEM,     /* 文件系统 */
    CAPABILITY_TYPE_UNKNOWN = 31,   /* 未知设备 */
} capability_list_t;
```

### dbot服务结构

```c
typedef struct dbot_service_context_s
{
    DDP_SERVICE_CONTEXT_S *ddp;          //DDP服务上下文
    DTCP_CLIENT_CONTEXT_S *dtcp_client;  //DTCP客户端上下文
    DTCP_SERVER_CONTEXT_S *dtcp_server;  //DTCP服务端上下文
    PUBLISH_MODULE_S *publish;           //能力发布模块
    DISCOVERY_MODULE_S *discovery;       //能力发现模块
    device_info_local_t *profile;        //设备信息
}DBOT_SERVICE_CONTEXT_S;
```

### 数据流转dtp服务结构

```c
typedef struct dtp_service_context_s
{
    DTP_SERVICE_TYPE_E type;             //流转角色类型
    union
    {
        UDP_CLIENT_S udp_client;
        UDP_SERVER_S udp_server;
        TCP_CLIENT_S tcp_client;
        TCP_SERVER_S tcp_server;
    }dtp_service;
    uint16_t port;                       //数据流转端口号
    DTP_SEND_FUNC dtp_send;              //接收数据函数指针
    DTP_RECV_FUNC dtp_recv;              //发送数据函数指针
    void *dbot_ctx;                      //dbot服务上下文
}DTP_SERVICE_CONTEXT_S;
```


## API介绍
| 接口                                     | 说明                             |
| ----------------------------------- | ----------------------------- |
| dbot_service_init                     | 初始化dbot设备相关信息     |
| dbot_service_start                   | 启动dbot服务                   |
| dbot_service_stop                   | 停止dbot服务                   |
| dbot_service_started                | 检查dbot是否已启动成功      |
| dbot_service_get_ctx                | 获取dbot服务上下文           |
| dbot_service_send_broadcast     | 往dbot网络内部广播消息      |
| dbot_service_add_resource        | 注册dbot监听的消息资源      |
| dbot_service_publish_service      | 向dbot内部发布设备能力      |
| dbot_service_discovery_service   | 在dbot内部发现设备及能力   |
| dbot_service_call_service           | 在dbot内部调用设备能力     |
| dbot_service_create_session       | 同设备建立回话                |
| dbot_service_remove_session     | 同设备断开回话                  |
| dbot_service_init_dtp               | 初始一个dtp服务，用于数据流转  |
| dbot_service_destory_dtp          | 销毁dtp服务                    |

### dbot_service_init
初始化dbot设备相关信息。
```c
int dbot_service_init(void);
```
| 参数             | 说明              |
| ---------------- | -------------- |
| void              |    无    |
| **返回**         | **说明**       |
| DBOT_OK       | 执行成功       |
| 其他              | 执行失败       |

### dbot_service_start
启动dbot服务。
```c
int dbot_service_start(void);
```
| 参数             | 说明              |
| ---------------- | -------------- |
| void              |    无    |
| **返回**         | **说明**       |
| DBOT_OK       | 执行成功       |
| 其他              | 执行失败       |

### dbot_service_stop
停止dbot服务。
```c
int dbot_service_stop(void);
```
| 参数             | 说明              |
| ---------------- | -------------- |
| void              |    无    |
| **返回**         | **说明**       |
| DBOT_OK        | 执行成功   |
| 其他            | 执行失败   |

### dbot_service_started
检查dbot是否已启动成功。
```c
bool dbot_service_started(void);
```
| 参数             | 说明              |
| ---------------- | -------------- |
| void              |    无    |
| **返回**         | **说明**       |
| true   | 已启动    |
| false         | 未启动    |

### dbot_service_get_ctx
获取dbot服务上下文。
```c
DBOT_SERVICE_CONTEXT_S *dbot_service_get_ctx(void);
```
| 参数             | 说明              |
| ---------------- | -------------- |
| void              |    无    |
| **返回**         | **说明**       |
| 非NULL          |  执行成功       |
| NULL             | 执行失败       |

### dbot_service_send_broadcast
往dbot网络内部广播消息。
```c
int dbot_service_send_broadcast(char *res_name, char *buf, int len);
```
| 参数             | 说明              |
| ---------------- | -------------- |
| res_name      |    消息资源名    |
| buf              |    消息内容    |
| len              |    消息长度    |
| **返回**         | **说明**       |
| DBOT_OK       | 执行成功       |
| 其他              | 执行失败       |

### dbot_service_add_resource
注册dbot监听的消息资源。
```c
int dbot_service_add_resource(const char *res_name, DDP_REQUEST_PROC func);
```
| 参数             | 说明              |
| ---------------- | -------------- |
| res_name      |    消息资源名    |
| func              |    消息回调函数    |
| **返回**         | **说明**       |
| DBOT_OK       | 执行成功       |
| 其他              | 执行失败       |

### dbot_service_publish_service
向dbot内部发布设备能力。
```c
int dbot_service_publish_service(uint32_t service,  uint32_t attr_num, ATTR_INFO_S *attrs);
```
| 参数             | 说明              |
| ---------------- | -------------- |
| service           |    能力id    |
| attr_num        |    能力属性数目    |
| attrs              |    能力属性内容    |
| **返回**         | **说明**       |
| DBOT_OK       | 执行成功       |
| 其他              | 执行失败       |

### dbot_service_discovery_service
在dbot内部发现设备及能力。
```c
int dbot_service_discovery_service(void);
```
| 参数             | 说明              |
| ---------------- | -------------- |
| void              |    无    |
| **返回**         | **说明**       |
| DBOT_OK       | 执行成功       |
| 其他              | 执行失败       |

### dbot_service_call_service
在dbot内部调用设备能力。
```c
int dbot_service_call_service(int device_id, uint32_t service, char *func, char *param, DTCP_CALL_RESULT_CB cb);
```
| 参数             | 说明              |
| ---------------- | -------------- |
| device_id        | 被调用设备id    |
| service           | 被调用设备能力id    |
| func              | 被调用设备能力属性    |
| param           | 调用参数    |
| cb                | 调用回调    |
| **返回**         | **说明**       |
| DBOT_OK       | 执行成功       |
| 其他              | 执行失败       |

### dbot_service_create_session
同设备建立回话。
```c
int dbot_service_create_session(int device_id);
```
| 参数             | 说明              |
| ---------------- | -------------- |
| int           |    设备ID    |
| **返回**         | **说明**       |
| DBOT_OK       | 执行成功       |
| 其他              | 执行失败       |

### dbot_service_remove_session
同设备断开回话。
```c
int dbot_service_remove_session(int device_id);
```
| 参数             | 说明              |
| ---------------- | -------------- |
| int           |    设备ID    |
| **返回**         | **说明**       |
| DBOT_OK       | 执行成功       |
| 其他              | 执行失败       |

### dbot_service_init_dtp
初始一个dtp服务，用于数据流转。
```c
DTP_SERVICE_CONTEXT_S *dbot_service_init_dtp(DTP_SERVICE_TYPE_E type, char *ip, uint16_t port);
```
| 参数             | 说明              |
| ---------------- | -------------- |
| type        | 流转服务类型    |
| ip           | 流转服务ip地址    |
| port              | 流转服务端口号    |
| **返回**         | **说明**       |
| DBOT_OK       | 执行成功       |
| 其他              | 执行失败       |

### dbot_service_destory_dtp
销毁dtp服务。
```c
int dbot_service_destory_dtp(DTP_SERVICE_CONTEXT_S *ctx);
```
| 参数             | 说明              |
| ---------------- | -------------- |
| ctx        | dtp上下文    |
| **返回**         | **说明**       |
| DBOT_OK       | 执行成功       |
| 其他              | 执行失败       |

## 配置选项
使用dbot可以通过menuconfig图形化工具进行配置，配置内容如下：
```c
(Top) → Components→ DBoT
                                                             OneOS Configuration
[*] Enable DBoT
[*]     Enable DBoT publish module
[*]     Enable DBoT discover module
(5)     Configure DBoT heartbeat interval time(seconds)
        Configure DBoT network (ethernet)  --->
[*]     Enable DBoT example
[*]         Enable light example
[ ]         Enable lcd example
[ ]         Enable illumination example
[*]         Enable camera example
[ ]         Enable recorder example
[ ]         Enable speaker example
[ ]         Enable filesystem example
[*]         Enable DBoT example auto start
[*]         Enable DBoT example shell cmd
```
配置内容包含了设备角色（发布/发现端），心跳间隔，联网方式，以及示例（dbot内置了灯，屏幕，光照传感，摄像头，麦克风，喇叭，文件系统7种能力示例）。

## 运行
dbot提供了以下一系列shell命令供使用：
| 命令                                     | 说明                             |
| ----------------------------------- | ----------------------------- |
| dbot_device_info                    | 查询设备基本信息              |
| dbot_show_service                  | 查询已发现的设备及能力      |
| dbot_call_service                    | 调用设备能力(简易)            |
| dbot_create_session                | 同设备建立回话                 |
| dbot_remove_session              | 同设备断开回话                 |
| dbot_discovery_service            | 发现设备及能力                 |
| dbot_show_directory               | 查询对端设备目录结构，需对端具备文件系统能力 |
| dbot_write_file                       | 向对端传输文件，需对端具备文件系统能力      |
| dbot_audio_dataflow               | 启动音频流转                   |
| dbot_audio_dataflow_stop        | 停止音频流转                   |
| dbot_video_dataflow | 启动视频流转 |
| dbot_video_dataflow_stop | 停止视频流转 |
