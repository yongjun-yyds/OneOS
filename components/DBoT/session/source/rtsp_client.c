#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <pthread.h>
#include <unistd.h>
#include <stdbool.h>
#include "rtsp_client.h"
#include "rtp.h"
#include "dbot_errno.h"
#include "dbot_log.h"

#if defined __linux__
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <errno.h>

#elif defined _WIN32
#include <winsock2.h>
#include <time.h>

#else
#include "os_clock.h"
#include "sys/socket.h"
#include "lwip/netif.h"

unsigned int usleep(unsigned int useconds)
{
    os_tick_t delta_tick;

    delta_tick = os_tick_get_value();
    
    os_task_tsleep(useconds);
    
    delta_tick = os_tick_get_value() - delta_tick;

    return delta_tick;
}
#endif

// rtsp://192.168.10.150:8554/live
int split_url(RTSP_CLIENT_CONTEXT_S *rtsp)
{
    if(NULL == rtsp)
    {
        DBOT_LOG_E("rtsp param is null\r\n");
        return DBOT_ERROR;
    }

    char tmp[RTSP_URL_MAX_LEN] = {0};
    strncpy(tmp, rtsp->rtsp_url + strlen("rtsp://"), RTSP_URL_MAX_LEN);
    char *host = strtok(tmp, "/");
    char *ip = strtok(host, ":");
    char *port = strtok(NULL, ":");
    if(NULL == ip || NULL == port)
    {
        DBOT_LOG_E("rtsp host format error\r\n");
        return DBOT_ERROR;
    }
    strcpy(rtsp->rtsp_ip, ip);
    rtsp->rtsp_port = atoi(port);
    return DBOT_OK;
}

int rtp_udp_start_server(RTSP_CLIENT_CONTEXT_S *rtsp_client)
{
    if(NULL == rtsp_client)
        return DBOT_ERROR;
    rtsp_client->rtp_udp_server_fd = socket(AF_INET, SOCK_DGRAM, 0);
    if(0 > rtsp_client->rtp_udp_server_fd)
    {
        DBOT_LOG_E("create rtp udp server failed\r\n");
        return DBOT_ERROR;
    }

    do
    {
        int on = 1;
        if(0 > setsockopt(rtsp_client->rtp_udp_server_fd, SOL_SOCKET, SO_REUSEADDR, (const char*)&on, sizeof(on)))
        {
            DBOT_LOG_E("set rtp udp port reuse failed\r\n");
            break;
        }

// #if defined __linux__
//         if(0 > fcntl(rtsp_client->rtp_udp_server_fd, F_SETFL, O_NONBLOCK))
//         {
//             DBOT_LOG_E("set rtp udp unblock failed\r\n");
//             break;
//         }
// #elif defined _WIN32
//         unsigned long int mode = 1;
//         if(0 > ioctlsocket(rtsp_client->rtp_udp_server_fd, FIONBIO, &mode))
//         {
//             DBOT_LOG_E("set rtp udp unblock failed\r\n");
//             break;
//         }
// #else

// #endif

        struct sockaddr_in addr;
        memset(&addr, 0, sizeof(addr));
        addr.sin_family = AF_INET;
        addr.sin_addr.s_addr = INADDR_ANY;
        addr.sin_port = 0;

        if(0 > bind(rtsp_client->rtp_udp_server_fd, (struct sockaddr*)&addr, sizeof(addr)))
        {
            DBOT_LOG_E("bind rtp udp port failed\r\n");
            break;
        }

        int len = sizeof(addr);
        memset(&addr, 0, sizeof(addr));
        if(0 > getsockname(rtsp_client->rtp_udp_server_fd, (struct sockaddr*)&addr, &len))
        {
            DBOT_LOG_E("get rtp udp port failed\r\n");
            break;
        }
        
        rtsp_client->rtp_udp_server_port = ntohs(addr.sin_port);
        DBOT_LOG_I("rtsp create rtp udp server succeed on port %d:%u\r\n", rtsp_client->rtp_udp_server_fd, rtsp_client->rtp_udp_server_port);
        return DBOT_OK;
    }while(0);

    closesocket(rtsp_client->rtp_udp_server_fd);
    rtsp_client->rtp_udp_server_fd = -1;

    return DBOT_ERROR;
}

int rtp_recv_video_stream(RTSP_CLIENT_CONTEXT_S *rtsp_client)
{
    if(NULL == rtsp_client)
        return DBOT_ERROR;

    int fd = -1;
    uint8_t *buf;
    struct sockaddr_in  addr;

    buf = (uint8_t*)malloc(RTP_STREAM_MAX_LEN);
    if(NULL == buf)
    {
        DBOT_LOG_E("malloc udp stream buf failed\r\n");
        return DBOT_ERROR;
    }
    memset(buf, 0, RTP_STREAM_MAX_LEN);

    if(rtsp_client->rtp_mode == RTP_TRANS_MODE_UDP)
    {
        fd = rtsp_client->rtp_udp_server_fd;
    }
    else
    {
        fd = rtsp_client->rtsp_client_fd;
    }
    int len = sizeof(addr);
    int ret = -1;
    fd_set fds;
    struct timeval tv;
    while(1)
    {
        memset(buf, 0, RTP_STREAM_MAX_LEN);
        FD_ZERO(&fds);
        FD_SET(fd, &fds);
        tv.tv_sec  = 0;
        tv.tv_usec = 1000;

        ret = select(fd + 1, &fds, NULL, NULL, &tv);
        if(0 >= ret)
            continue;

        if(FD_ISSET(fd, &fds))
        {
            if(rtsp_client->rtp_mode == RTP_TRANS_MODE_UDP)
                ret = recvfrom(fd, buf, RTP_STREAM_MAX_LEN, 0, (struct sockaddr*)&addr, &len);
            else
                ret = recv(fd, buf, RTP_STREAM_MAX_LEN, 0);

            if(0 < ret)
            {
                if(NULL != rtsp_client->rtp_play_video_stream)
                {
                    rtsp_client->rtp_play_video_stream(buf, ret);
                }
            }
        }
    }

    free(buf);
    return DBOT_ERROR;
}

void *rtp_play_task(void *arg)
{
    if(NULL == arg)
        return NULL;

    int ret = 0;
    RTSP_CLIENT_CONTEXT_S *rtsp_client = (RTSP_CLIENT_CONTEXT_S*)arg;
    ret = rtp_recv_video_stream(rtsp_client);
    if(0 > ret)
    {
        //播放失败，标记RTSP流程失败，在rtsp线程中重新建立连接并协商
        rtsp_client->rtsp_ready = 0;
    }

    DBOT_LOG_I("rtp task exit\r\n");
    return NULL;
}

int rtsp_client_connect_server(RTSP_CLIENT_CONTEXT_S *rtsp_client)
{
    if(NULL == rtsp_client)
        return DBOT_ERROR;

    int ret = -1;
    rtsp_client->rtsp_client_fd = socket(AF_INET, SOCK_STREAM, 0);
    if(0 > rtsp_client->rtsp_client_fd)
    {
        DBOT_LOG_E("create rtsp tcp client failed\r\n");
        return DBOT_ERROR;
    }

    int nagle = 1; 
    setsockopt(rtsp_client->rtsp_client_fd, IPPROTO_TCP, TCP_NODELAY, (char *)&nagle, sizeof(nagle));

    // fcntl(rtsp_client->rtsp_client_fd, F_SETFL, O_NONBLOCK);

    struct sockaddr_in  addr;
    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = inet_addr(rtsp_client->rtsp_ip);
    addr.sin_port = htons(rtsp_client->rtsp_port);

    // time_t t0 = time(NULL);
    // while((time(NULL) - t0) < 2)
    // {
    //     usleep(500 * 1000);
    //     ret = connect(rtsp_client->rtsp_client_fd, (struct sockaddr*)&addr, sizeof(addr));
    //     // 连接成功
    //     if(0 == ret)
    //     {
    //         break;
    //     }
    //     else if(-1 == ret)
    //     {
    //         // 信号中断，重试
    //         if(EINTR == errno)
    //         {
    //             continue;
    //         }
    //         // 正在建立连接中
    //         else if(EINPROGRESS == errno)
    //         {
    //             break;
    //         }
    //         // 其他错误，重试
    //         else
    //         {
    //             continue;
    //         }
    //     }
    // }
    ret = connect(rtsp_client->rtsp_client_fd, (struct sockaddr*)&addr, sizeof(addr));
    if(0 > ret)
    {
        DBOT_LOG_E("connect rtsp server failed\r\n");
        closesocket(rtsp_client->rtsp_client_fd);
        rtsp_client->rtsp_client_fd = -1;
        return DBOT_ERROR;
    }
    return DBOT_OK;
}

int rtsp_client_handle_option_resp(RTSP_CLIENT_CONTEXT_S *rtsp_client, char *resp)
{
    if(NULL == rtsp_client || NULL == resp)
        return -1;
    // TODO: parse options
    return 0;
}

int rtsp_client_handle_describe_resp(RTSP_CLIENT_CONTEXT_S *rtsp_client, char *resp)
{
    if(NULL == rtsp_client || NULL == resp)
        return -1;
    // TODO: parse describe
    return 0;
}

int rtsp_client_handle_setup_resp(RTSP_CLIENT_CONTEXT_S *rtsp_client, char *resp)
{
    if(NULL == rtsp_client || NULL == resp)
        return -1;
    // TODO: setup describe
    return 0;
}

int rtsp_client_handle_play_resp(RTSP_CLIENT_CONTEXT_S *rtsp_client, char *resp)
{
    if(NULL == rtsp_client || NULL == resp)
        return -1;
    // TODO: play describe
    return 0;
}

int rtsp_client_handle_teardown_resp(RTSP_CLIENT_CONTEXT_S *rtsp_client, char *resp)
{
    if(NULL == rtsp_client || NULL == resp)
        return -1;
    // TODO: play describe
    return 0;
}

void rtsp_client_generate_req(RTSP_CLIENT_CONTEXT_S *rtsp_client, RTSP_MSG_TYPE_E msg_type, char *msg)
{
    if(NULL == rtsp_client || NULL == msg)
        return;

    char tmp[10];
    switch(msg_type)
    {
        case RTSP_MSG_TYPE_OPTION:
            strcat(msg, "OPTIONS ");
            strcat(msg, rtsp_client->rtsp_url);
            strcat(msg, " RTSP/1.0\r\n");
            strcat(msg, "CSeq: 2\r\n");
            strcat(msg, "User-Agent: LibVLC/3.0.16 (LIVE555 Streaming Media v2016.11.28)\r\n");
            strcat(msg, "\r\n");
            break;
        case RTSP_MSG_TYPE_DESCRIBE:
            strcat(msg, "DESCRIBE ");
            strcat(msg, rtsp_client->rtsp_url);
            strcat(msg, " RTSP/1.0\r\n");
            strcat(msg, "CSeq: 3\r\n");
            strcat(msg, "User-Agent: LibVLC/3.0.16 (LIVE555 Streaming Media v2016.11.28)\r\n");
            strcat(msg, "Accept: application/sdp\r\n");
            strcat(msg, "\r\n");
            break;
        case RTSP_MSG_TYPE_SETUP:
            strcat(msg, "SETUP ");
            strcat(msg, rtsp_client->rtsp_url);
            strcat(msg, " RTSP/1.0\r\n");
            strcat(msg, "CSeq: 4\r\n");
            strcat(msg, "User-Agent: LibVLC/3.0.16 (LIVE555 Streaming Media v2016.11.28)\r\n");
            if(RTP_TRANS_MODE_UDP == rtsp_client->rtp_mode)
            {
                strcat(msg, "Transport: RTP/AVP;unicast;client_port=");
                memset(tmp, 0, sizeof(tmp));
                snprintf(tmp, sizeof(tmp), "%d", rtsp_client->rtp_udp_server_port);
                strcat(msg, tmp);
                strcat(msg, "-");
                memset(tmp, 0, sizeof(tmp));
                snprintf(tmp, sizeof(tmp), "%d", rtsp_client->rtp_udp_server_port + 1);
                strcat(msg, tmp);
                strcat(msg, "\r\n");
            }
            else
            {
                strcat(msg, "Transport: RTP/AVP/TCP;unicast;interleaved=0-1;\r\n");
            }
            break;
        case RTSP_MSG_TYPE_PLAY:
            strcat(msg, "PLAY ");
            strcat(msg, rtsp_client->rtsp_url);
            strcat(msg, " RTSP/1.0\r\n");
            strcat(msg, "CSeq: 5\r\n");
            strcat(msg, "User-Agent: LibVLC/3.0.16 (LIVE555 Streaming Media v2016.11.28)\r\n");
            strcat(msg, "Session: 3520560880");
            strcat(msg, "Range: npt=0.000-\r\n");
            strcat(msg, "\r\n");
            break;
        case RTSP_MSG_TYPE_TREARDOWN:
            break;
        default:
            break;
    }
}

int rtsp_client_handle_resp(RTSP_CLIENT_CONTEXT_S *rtsp_client, RTSP_MSG_TYPE_E msg_type, char *buf)
{
    int ret = 0;
    switch(msg_type)
    {
        case RTSP_MSG_TYPE_OPTION:
            ret = rtsp_client_handle_option_resp(rtsp_client, buf);
            break;
        case RTSP_MSG_TYPE_DESCRIBE:
            ret = rtsp_client_handle_describe_resp(rtsp_client, buf);
            break;
        case RTSP_MSG_TYPE_SETUP:
            ret = rtsp_client_handle_setup_resp(rtsp_client, buf);
            break;
        case RTSP_MSG_TYPE_PLAY:
            ret = rtsp_client_handle_play_resp(rtsp_client, buf);
            break;
        case RTSP_MSG_TYPE_TREARDOWN:
            ret = rtsp_client_handle_teardown_resp(rtsp_client, buf);
            break;
        default:
            break;
    }
    return ret;
}

int rtsp_client_recv(int fd, char *buf, int len, int timeout_s)
{
    int ret = -1;
    fd_set fds;
    struct timeval tv;

    time_t t0 = time(NULL);
    while((time(NULL) - t0) < timeout_s)
    {
        usleep(1000);
        FD_ZERO(&fds);
        FD_SET(fd, &fds);
        tv.tv_sec  = 0;
        tv.tv_usec = 10 * 1000;

        ret = select(fd + 1, &fds, NULL, NULL, &tv);
        if(0 >= ret)
            continue;

        if(FD_ISSET(fd, &fds))
        {
            ret = recv(fd, buf, len, 0);
            if(0 < ret)
            {
                break;
            }
        }
    }
    return ret;
}

int rtsp_client_shake_hand(RTSP_CLIENT_CONTEXT_S *rtsp_client, RTSP_MSG_TYPE_E msg_type)
{
    if(NULL == rtsp_client)
        return -1;

    int ret = 0;
    char *buf = malloc(512);
    if(NULL == buf)
        return -1;

    memset(buf, 0, 512);
    rtsp_client_generate_req(rtsp_client, msg_type, buf);

    do
    {
        ret = send(rtsp_client->rtsp_client_fd, buf, strlen(buf), 0);
        if(0 > ret)
            break;

        memset(buf, 0, 512);
        // ret = rtsp_client_recv(rtsp_client->rtsp_client_fd, buf, 512, 2);
        ret = recv(rtsp_client->rtsp_client_fd, buf, 512, 0);
        if(0 > ret)
            break;
        
        ret = rtsp_client_handle_resp(rtsp_client, msg_type, buf);
        if(0 > ret) 
            break;
    }while(0);

    free(buf);
    return ret;
}

void *rtsp_client_task(void *arg)
{
    if(NULL == arg)
        return NULL;
    
    int cnt = 5;

    RTSP_CLIENT_CONTEXT_S *rtsp_client = (RTSP_CLIENT_CONTEXT_S*)arg;
    do
    {
        usleep(500 * 1000);
        closesocket(rtsp_client->rtsp_client_fd);
        rtsp_client->rtsp_client_fd = -1;
        if(0 > rtsp_client_connect_server(rtsp_client))
        {
            cnt --;
            continue;
        }
        DBOT_LOG_D("rtsp connect succeed\r\n");
        if(0 > rtsp_client_shake_hand(rtsp_client, RTSP_MSG_TYPE_OPTION))
        {
            cnt --;
            continue;
        }
        DBOT_LOG_D("rtsp option succeed\r\n");
        if(0 > rtsp_client_shake_hand(rtsp_client, RTSP_MSG_TYPE_DESCRIBE))
        {
            cnt --;
            continue;
        }
        DBOT_LOG_D("rtsp describe succeed\r\n");
        if(0 > rtsp_client_shake_hand(rtsp_client, RTSP_MSG_TYPE_SETUP))
        {
            cnt --;
            continue;
        }
        DBOT_LOG_D("rtsp setup succeed\r\n");
        if(0 > rtsp_client_shake_hand(rtsp_client, RTSP_MSG_TYPE_PLAY))
        {
            cnt --;
            continue;
        }
        DBOT_LOG_D("rtsp play succeed\r\n");
        rtsp_client->rtsp_ready = 1;
        break;
    }while(cnt >0);

    if(rtsp_client->rtsp_ready)
    {
        pthread_attr_t attr;
        pthread_attr_init(&attr);
        pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
        pthread_attr_setstacksize(&attr, 2048);
        if(0 != pthread_create(&rtsp_client->rtp_task, &attr, rtp_play_task, (void*)rtsp_client))
        {
            DBOT_LOG_E("create rtp task failed\r\n");
            rtsp_client->rtsp_ready = 0;
        }
    }

    return NULL;
}

RTSP_CLIENT_CONTEXT_S *rtsp_start_client(const char *rtsp_url, RTP_STREAM_OUT_FUNC cb, RTP_TRANS_MODE_E mode)
{
    if(NULL == rtsp_url)
    {
        return NULL;
    }
    RTSP_CLIENT_CONTEXT_S *rtsp_client;
    rtsp_client = (RTSP_CLIENT_CONTEXT_S*)malloc(sizeof(RTSP_CLIENT_CONTEXT_S));
    if(NULL == rtsp_client)
    {
        DBOT_LOG_E("malloc rtsp client failed\r\n");
        return NULL;
    }

    do
    {
        memset(rtsp_client, 0, sizeof(RTSP_CLIENT_CONTEXT_S));
        strcpy(rtsp_client->rtsp_url, rtsp_url);
        if(0 > split_url(rtsp_client))
        {
            break;
        }

        if(0 > rtp_udp_start_server(rtsp_client))
        {
            DBOT_LOG_E("rtp udp play task exit\r\n");
            break;
        }

        DBOT_LOG_I("connect to rtsp server %s\r\n", rtsp_client->rtsp_url);
        rtsp_client->rtp_play_video_stream = cb;
        rtsp_client->rtp_mode = mode;
        pthread_attr_t attr;
        pthread_attr_init(&attr);
        pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
        pthread_attr_setstacksize(&attr, 2048);
        if(0 != pthread_create(&rtsp_client->rtsp_task, &attr, rtsp_client_task, (void*)rtsp_client))
        {
            DBOT_LOG_E("create rtsp task failed\r\n");
            break;
        }
        return rtsp_client;
    }while(0);

    free(rtsp_client);

    //TODO: client异常需要重新建立连接并协商
    return NULL;
}

void rtsp_client_stop(RTSP_CLIENT_CONTEXT_S *rtsp_client)
{
    if(-1 != rtsp_client->rtsp_client_fd)
    {
        closesocket(rtsp_client->rtsp_client_fd);
    }
    if(-1 != rtsp_client->rtp_udp_server_fd)
    {
        closesocket(rtsp_client->rtp_udp_server_fd);
    }

    // os_task_destory(rtsp_client->rtp_task);
    // os_task_destory(rtsp_client->rtsp_task);
    free(rtsp_client);
}

