#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include "dbot_log.h"
#include "dbot_errno.h"
#include "dbot_common.h"
#include "cJSON.h"
#include "dtp_service.h"
#include "dbot_service.h"
#include "publish_service.h"
#if defined __linux__
#include <sys/socket.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <errno.h>
#elif defined _WIN32
#include <winsock2.h>
#include <time.h>
#else
#include "os_clock.h"
#include "sys/socket.h"
#include "lwip/netif.h"
#endif


int _dtp_send(void *ctx, uint8_t *data, int len)
{
    RETURN_IF_NULL(ctx, DBOT_INVALID_PARAM);
    RETURN_IF_NULL(data, DBOT_INVALID_PARAM);

    int ret = DBOT_ERROR;
    DTP_SERVICE_CONTEXT_S *dtp_ctx = (DTP_SERVICE_CONTEXT_S*)ctx;
    switch(dtp_ctx->type)
    {
        case DTP_SERVICE_UDP_CLIENT:
            ret = dtp_ctx->dtp_service.udp_client.send((void*)(&dtp_ctx->dtp_service.udp_client), data, len);
            break;
        case DTP_SERVICE_UDP_SERVER:
            // ret = dtp_ctx->dtp_service.udp_server.recv((void*)(&dtp_ctx->dtp_service.udp_server), data, len);
            break;
        case DTP_SERVICE_TCP_CLIENT:
            ret = dtp_ctx->dtp_service.tcp_client.send((void*)(&dtp_ctx->dtp_service.tcp_client), data, len);
            break;
        case DTP_SERVICE_TCP_SERVER:
            // ret = dtp_ctx->dtp_service.tcp_server.recv((void*)(&dtp_ctx->dtp_service.tcp_server), data, len);
            break;
        default:
            break;
    }

    return ret;
}

int _dtp_recv(void *ctx, uint8_t *data, int len)
{
    RETURN_IF_NULL(ctx, DBOT_INVALID_PARAM);
    RETURN_IF_NULL(data, DBOT_INVALID_PARAM);

    int ret = DBOT_ERROR;
    DTP_SERVICE_CONTEXT_S *dtp_ctx = (DTP_SERVICE_CONTEXT_S*)ctx;
    switch(dtp_ctx->type)
    {
        case DTP_SERVICE_UDP_CLIENT:
            // ret = dtp_ctx->dtp_service.udp_client.send((void*)(&dtp_ctx->dtp_service.udp_client), data, len);
            break;
        case DTP_SERVICE_UDP_SERVER:
            ret = dtp_ctx->dtp_service.udp_server.recv((void*)(&dtp_ctx->dtp_service.udp_server), data, len);
            break;
        case DTP_SERVICE_TCP_CLIENT:
            // ret = dtp_ctx->dtp_service.tcp_client.send((void*)(&dtp_ctx->dtp_service.tcp_client), data, len);
            break;
        case DTP_SERVICE_TCP_SERVER:
            ret = dtp_ctx->dtp_service.tcp_server.recv((void*)(&dtp_ctx->dtp_service.tcp_server), data, len);
            break;
        default:
            break;
    }

    return ret;
}

/* DTP只提供模块，由能力调用端和提供端的应用部分自行调用DTP模块，构造服务，
** 比如提供端自己搭建好DTP服务，然后再属性函数里面返回DTP服务端口给调用端，
** 反之，调用端自己搭建好DTP服务，然后将端口通过调用接口传递给提供端。*/

DTP_SERVICE_CONTEXT_S *dbot_dtp_service_init(void *dbot_ctx, DTP_SERVICE_TYPE_E type, char *ip, uint16_t port)
{
    RETURN_NULL_IF_NULL(dbot_ctx);
    
    DTP_SERVICE_CONTEXT_S *dtp_ctx = calloc(1, sizeof(DTP_SERVICE_CONTEXT_S));
    RETURN_NULL_IF_NULL(dtp_ctx);
    int ret = DBOT_ERROR;
    switch(type)
    {
        case DTP_SERVICE_UDP_CLIENT:
            ret = create_udp_client(&dtp_ctx->dtp_service.udp_client, ip, port);
            dtp_ctx->port = dtp_ctx->dtp_service.udp_client.port;
            break;
        case DTP_SERVICE_UDP_SERVER:
            ret = create_udp_server(&dtp_ctx->dtp_service.udp_server, port);
            dtp_ctx->port = dtp_ctx->dtp_service.udp_server.port;
            break;
        case DTP_SERVICE_TCP_CLIENT:
            ret = create_tcp_client(&dtp_ctx->dtp_service.tcp_client, ip, port, 10);
            dtp_ctx->port = dtp_ctx->dtp_service.tcp_client.port;
            break;
        case DTP_SERVICE_TCP_SERVER:
            ret = create_tcp_server(&dtp_ctx->dtp_service.tcp_server, port, 10);
            dtp_ctx->port = dtp_ctx->dtp_service.tcp_server.port;
            break;
        default:
            break;
    }

    if(DBOT_OK == ret)
    {
        dtp_ctx->type = type;
        dtp_ctx->dbot_ctx = dbot_ctx;
        dtp_ctx->dtp_send = _dtp_send;
        dtp_ctx->dtp_recv = _dtp_recv;
    }
    else
    {
        FREE_POINTER(dtp_ctx);
    }

    return dtp_ctx;
}

int dbot_dtp_service_destory(DTP_SERVICE_CONTEXT_S *ctx)
{
    RETURN_IF_NULL(ctx, DBOT_INVALID_PARAM);

    switch(ctx->type)
    {
        case DTP_SERVICE_UDP_CLIENT:
            if(0 <= ctx->dtp_service.udp_client.fd)
            {
                closesocket(ctx->dtp_service.udp_client.fd);
            }
            break;
        case DTP_SERVICE_UDP_SERVER:
            if(0 <= ctx->dtp_service.udp_server.fd)
            {
                closesocket(ctx->dtp_service.udp_server.fd);
            }
            break;
        case DTP_SERVICE_TCP_CLIENT:
            if(0 <= ctx->dtp_service.tcp_client.fd)
            {
                closesocket(ctx->dtp_service.tcp_client.fd);
            }
            break;
        case DTP_SERVICE_TCP_SERVER:
            if(0 <= ctx->dtp_service.tcp_server.client_fd)
            {
                closesocket(ctx->dtp_service.tcp_server.client_fd);
            }
            if(0 <= ctx->dtp_service.tcp_server.fd)
            {
                closesocket(ctx->dtp_service.tcp_server.fd);
            }
            break;
        default:
            break;
    }
    FREE_POINTER(ctx);
    
    return DBOT_OK;
}
