#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include "dbot_log.h"
#include "dbot_errno.h"
#include "dbot_common.h"
#include "cJSON.h"
#include "dbot_service.h"
#include "dtcp_client.h"
#include "discovery_service.h"
#include "dtp_service.h"
#if defined __linux__
#include <sys/socket.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <errno.h>
#elif defined _WIN32
#include <winsock2.h>
#include <time.h>
#else
#include "os_clock.h"
#include "sys/socket.h"
#include "lwip/netif.h"
#endif

static DTCP_CLIENT_CONTEXT_S *s_dtcp_client_ctx = NULL;

static int dtcp_server_heartbeat_check(void)
{
    int ret = DBOT_OK;

    if(NULL != s_dtcp_client_ctx)
    {
        int now = get_current_time();
        if((now - s_dtcp_client_ctx->last_hb_reply_ts) > HB_OFFLINE_CYCLES * DBOT_HB_INTERVAL * 1000)
        {
            DBOT_LOG_I("dctp server offline\r\n");
            ret = DBOT_ERROR;
        }
    }

    return ret;
}

static int dtcp_client_heartbeat_handle(void *dbot_ctx, cJSON *req_json, char *req, char **resp)
{
    return DBOT_OK;
}

static int dtcp_client_call_handle(void *dbot_ctx, cJSON *req_json, char *req, char **resp)
{
    RETURN_IF_NULL(dbot_ctx, DBOT_INVALID_PARAM);
    RETURN_IF_NULL(req_json, DBOT_INVALID_PARAM);
    RETURN_IF_NULL(req, DBOT_INVALID_PARAM);
    RETURN_IF_NULL(resp, DBOT_INVALID_PARAM);

    cJSON *service = cJSON_GetObjectItem(req_json, "service");
    RETURN_IF_NULL(service, DBOT_INVALID_PARAM);
    cJSON *func = cJSON_GetObjectItem(req_json, "func");
    RETURN_IF_NULL(func, DBOT_INVALID_PARAM);
    cJSON *params = cJSON_GetObjectItem(req_json, "params");

    DBOT_SERVICE_CONTEXT_S *ctx = (DBOT_SERVICE_CONTEXT_S*)dbot_ctx;
    PUBLISH_MODULE_S *publish = ctx->publish;
    RETURN_IF_NULL(publish, DBOT_INVALID_PARAM);

    int i = 0;
    int ret = DBOT_ERROR;
    SERVICE_NODE_T *service_node = NULL;
    ATTR_INFO_S *attr_node = NULL;
    pthread_mutex_lock(&publish->service_list_lock);
    os_list_for_each_entry(service_node, &publish->service_list, SERVICE_NODE_T, node) 
    {
        // 确定调用的是否是正确的能力，正确的属性
        if (service->valueint == service_node->service_type) 
        {
            for(i = 0;i < service_node->attr_count;i ++)
            {
                if(!strcmp(service_node->attrs[i].func_name, func->valuestring))
                {
                    attr_node = &service_node->attrs[i];
                    break;
                }
            }
        }
    }
    pthread_mutex_unlock(&publish->service_list_lock);
    
    if(NULL != attr_node)
    {
        if(NULL != params)
        {
            char *str = cJSON_PrintUnformatted(params);
            if(NULL != str)
            {
                ret = attr_node->func(strlen(str), str, resp);
                FREE_POINTER(str);
            }
        }
        else
        {
            ret = attr_node->func(0, NULL, resp);
        }
    }

    if(NULL == *resp)
    {
        cJSON *root = cJSON_CreateObject();
        cJSON_AddNumberToObject(root, "code", ret);
        *resp = cJSON_PrintUnformatted(root);
        cJSON_Delete(root);
    }

    return ret;
}

static int dtcp_client_request_proc(void *dbot_ctx, int req_len, char *req, char **resp)
{
    RETURN_IF_NULL(dbot_ctx, DBOT_INVALID_PARAM);
    RETURN_IF_NULL(resp, DBOT_INVALID_PARAM);
    RETURN_IF_NULL(req, DBOT_INVALID_PARAM);

    int ret = DBOT_OK;
    cJSON *root = cJSON_Parse(req);
    RETURN_IF_NULL(root, DBOT_INVALID_PARAM);

    cJSON *option = cJSON_GetObjectItem(root, "option");
    if(NULL != option)
    {
        if(!strcmp(option->valuestring, "call"))
        {
            DBOT_LOG_D("dbot dtcp recv request: %s\r\n", req);
            ret = dtcp_client_call_handle(dbot_ctx, root, req, resp);
        }
        if(!strcmp(option->valuestring, "heartbeat_reply"))
        {
            ret = dtcp_client_heartbeat_handle(dbot_ctx, root, req, resp);
        }
    }

    cJSON_Delete(root);
    return ret;
}

static void send_heartbeat(void)
{
    char heartbeat[30] = "{\"option\":\"heartbeat\"}";
    if(get_current_time() - s_dtcp_client_ctx->last_hb_ts > DBOT_HB_INTERVAL * 1000)
    {
        send(s_dtcp_client_ctx->client.fd, heartbeat, strlen(heartbeat), 0);
        s_dtcp_client_ctx->last_hb_ts = get_current_time();
    }
}

static void *dtcp_client_thread_func(void *param)
{
    RETURN_NULL_IF_NULL(s_dtcp_client_ctx);

    int ret = -1;
    char *resp = NULL;
#ifdef _WIN32
    fd_set fds;
    struct timeval tv;
    tv.tv_sec  = 2;
    tv.tv_usec = 0;
#endif
    s_dtcp_client_ctx->last_hb_ts = get_current_time();
    s_dtcp_client_ctx->last_hb_reply_ts = get_current_time();

    while(1)
    {
        if(s_dtcp_client_ctx->quit_flag)
        {
            break;
        }
        dbot_msleep(10);

        // 复用本线程发送心跳以及检查心跳保活，减少线程创建开支
        if(DBOT_OK != dtcp_server_heartbeat_check())
        {
            break;
        }
        
        send_heartbeat();

        //windows mingw64环境下设置socket读写超时无法生效，recv函数会阻塞，导致无法在本线程中发送心跳，需要添加select通过操作来执行读数据检查
#ifdef _WIN32
        FD_ZERO(&fds);
        FD_SET(s_dtcp_client_ctx->client.fd, &fds);
        if(0 >= select(s_dtcp_client_ctx->client.fd + 1, &fds, NULL, NULL, &tv))
        {
            continue;
        }
        if(FD_ISSET(s_dtcp_client_ctx->client.fd, &fds))
        {
#endif
            memset(s_dtcp_client_ctx->data_buf, 0, MAX_SOCKET_RECV_BUFFER_LEN);
            ret = recv(s_dtcp_client_ctx->client.fd, s_dtcp_client_ctx->data_buf, MAX_SOCKET_RECV_BUFFER_LEN, 0);
            if(0 < ret)
            {
                // 每收到一次消息，更新时间戳
                s_dtcp_client_ctx->last_hb_reply_ts = get_current_time();

                s_dtcp_client_ctx->data_len = ret;
                dtcp_client_request_proc(s_dtcp_client_ctx->dbot_ctx, s_dtcp_client_ctx->data_len, s_dtcp_client_ctx->data_buf, &resp);
                if(NULL != resp)
                {
                    DBOT_LOG_D("dbot dtcp send response: %s\r\n", resp);
                    send(s_dtcp_client_ctx->client.fd, resp, strlen(resp), 0);
                    FREE_POINTER(resp);
                }
            }
            else if(0 == ret)
            {
                break;
            }
            else
            {
                if((errno == EAGAIN||errno == EWOULDBLOCK||errno == EINTR))
                {
                    continue;
                }
                break;
            }
#ifdef _WIN32
        }
#endif
    }
    DBOT_LOG_D("dtcp client thread quit\r\n");
    dbot_dtcp_client_stop();
    return NULL;
}

int dbot_dtcp_client_init(void *dbot_ctx, int device_id, char *dst_ip, uint16_t dst_port)
{
    RETURN_IF_NULL(dbot_ctx, DBOT_INVALID_PARAM);

    if(NULL != s_dtcp_client_ctx)
    {
        DBOT_LOG_E("dtcp client already exists\r\n");
        return DBOT_ERROR;
    }

    DBOT_SERVICE_CONTEXT_S *ctx = (DBOT_SERVICE_CONTEXT_S*)dbot_ctx;
    s_dtcp_client_ctx = calloc(1, sizeof(DTCP_CLIENT_CONTEXT_S));
    RETURN_IF_NULL(s_dtcp_client_ctx, DBOT_INVALID_PARAM);

    if(DBOT_OK != create_tcp_client(&s_dtcp_client_ctx->client, dst_ip, dst_port, 2))
    {
        FREE_POINTER(s_dtcp_client_ctx);
        return DBOT_SOCKET_FAIL;
    }

    s_dtcp_client_ctx->dbot_ctx = dbot_ctx;
    s_dtcp_client_ctx->device_id = device_id;
    ctx->dtcp_client = s_dtcp_client_ctx;
    return DBOT_OK;
}

int dbot_dtcp_client_start(void)
{
    RETURN_IF_NULL(s_dtcp_client_ctx, DBOT_INVALID_PARAM);

    char auth[30] = "{\"option\":\"auth\"}";

    if(0 != connect(s_dtcp_client_ctx->client.fd, (struct sockaddr*)&s_dtcp_client_ctx->client.dst_addr, sizeof(s_dtcp_client_ctx->client.dst_addr)))
    {
        DBOT_LOG_E("dtcp client connect server failed\r\n");
        return DBOT_SOCKET_FAIL;
    }

    if(0 >= send(s_dtcp_client_ctx->client.fd, auth, sizeof(auth), 0))
    {
        DBOT_LOG_E("dtcp client auth failed\r\n");
        return DBOT_SOCKET_FAIL;
    }
    
    if(0 >= recv(s_dtcp_client_ctx->client.fd, auth, sizeof(auth), 0))
    {
        DBOT_LOG_E("dtcp client auth failed\r\n");
        return DBOT_SOCKET_FAIL;
    }

    s_dtcp_client_ctx->client.connected = 1;
    s_dtcp_client_ctx->quit_flag = 0;

    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
    pthread_attr_setstacksize(&attr, 4096);
    if(0 != pthread_create(&s_dtcp_client_ctx->pid, &attr, dtcp_client_thread_func, NULL))
    {
        DBOT_LOG_E("create dtcp server thread failed\r\n");
        return DBOT_ERROR;
    }

    return DBOT_OK;
}

int dbot_dtcp_client_stop(void)
{
    RETURN_IF_NULL(s_dtcp_client_ctx, DBOT_INVALID_PARAM);

    s_dtcp_client_ctx->quit_flag = 1;
    if(s_dtcp_client_ctx->client.connected && s_dtcp_client_ctx->client.fd >= 0)
    {
        closesocket(s_dtcp_client_ctx->client.fd);
    }
    FREE_POINTER(s_dtcp_client_ctx);
    s_dtcp_client_ctx = NULL;
    dbot_service_get_ctx()->dtcp_client = NULL;
    return DBOT_OK;
}
