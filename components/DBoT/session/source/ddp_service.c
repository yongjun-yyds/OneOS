#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include "dbot_log.h"
#include "dbot_errno.h"
#include "dbot_common.h"
#include "cJSON.h"
#include "ddp_service.h"
#include "dbot_service.h"
#if defined __linux__
#include <sys/socket.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <errno.h>
#elif defined _WIN32
#include <winsock2.h>
#include <time.h>
#else
#include "os_clock.h"
#include "sys/socket.h"
#include "lwip/netif.h"
#endif

static DDP_SERVICE_CONTEXT_S *s_ddp_service_ctx = NULL;
static int s_ddp_quit_flag = 0;

#ifdef DBOT_PUBLISH
static int s_hb_ts = 0;
int dbot_ddp_send_heartbeat(void)
{
    int ret = DBOT_ERROR;
    if(get_current_time() - s_hb_ts >= DBOT_HB_INTERVAL * 1000)
    {
        char hb_str[50] = {0};
        snprintf(hb_str, sizeof(hb_str), "{\"resource\":\"%s\", \"device_id\":%d}", DBOT_HEARTBEAT_SERVICE, get_device_id());
        ret = dbot_ddp_service_send_request(NULL, hb_str, strlen(hb_str));
        s_hb_ts = get_current_time();
    }
    return ret;
}
#endif

/*
**dbot_ddp_service_proc_msg:ddp server处理收到的消息
**
**param:DDP_SERVICE_CONTEXT_S *ctx, ddp server上下文
**
**return:int, DBOT_OK-执行成功，其他-执行失败
*/
static int dbot_ddp_service_proc_msg(DDP_SERVICE_CONTEXT_S *ctx)
{
    int ret = DBOT_ERROR;
    char *resp = NULL;

    // DBOT_LOG_D("ddp recv request:%s\r\n", ctx->data_buf);
    cJSON *root = cJSON_Parse(ctx->data_buf);
    RETURN_IF_NULL(root, DBOT_ERROR);

    cJSON *resource = cJSON_GetObjectItem(root, "resource");
    if(NULL != resource)
    {
        DDP_RESOURCE_T *resource_node = NULL;
        pthread_mutex_lock(&ctx->res_list_lock);
        os_list_for_each_entry(resource_node, &ctx->res_list, DDP_RESOURCE_T, node) 
        {
            if (!strcmp(resource_node->res_name, resource->valuestring))
            {
                ret = resource_node->func(ctx->dbot_ctx, ctx->data_len, ctx->data_buf, &resp);
                FREE_POINTER(resp);
                break;
            }
        }
        pthread_mutex_unlock(&ctx->res_list_lock);
    }
    cJSON_Delete(root);
    return ret;
}

/*
**dbot_ddp_service_thread_func:ddp server线程执行函数
**
**param:void *param, 线程参数，传入ddp server上下文
**
**return: void*
*/
static void *dbot_ddp_service_thread_func(void *param)
{
    RETURN_NULL_IF_NULL(param);

    DDP_SERVICE_CONTEXT_S *ctx = (DDP_SERVICE_CONTEXT_S*)param;
    fd_set fds;
    struct timeval tv;
    int ret = -1;
#ifdef _WIN32
    int len = sizeof(struct sockaddr_in);
#else
    socklen_t len = sizeof(struct sockaddr_in);
#endif

#ifdef DBOT_PUBLISH
    s_hb_ts = get_current_time();
#endif
    while(1)
    {
        if(s_ddp_quit_flag)
        {
            break;
        }
#ifdef DBOT_PUBLISH
        dbot_ddp_send_heartbeat();
#endif
        memset(ctx->data_buf, 0, MAX_SOCKET_RECV_BUFFER_LEN);
        memset(&ctx->src_addr, 0, sizeof(ctx->src_addr));
        FD_ZERO(&fds);
        FD_SET(ctx->server_fd, &fds);
        tv.tv_sec  = 1;
        tv.tv_usec = 0;

        ret = select(ctx->server_fd + 1, &fds, NULL, NULL, &tv);
        if(0 >= ret)
            continue;

        if(FD_ISSET(ctx->server_fd, &fds))
        {
            ctx->data_len = recvfrom(ctx->server_fd, ctx->data_buf, MAX_SOCKET_RECV_BUFFER_LEN, 0, (struct sockaddr*)&ctx->src_addr, &len);
            if(0 < ret)
            {
                dbot_ddp_service_proc_msg(ctx);
            }
            else
            {
                DBOT_LOG_D("recv failed\n");
            }
        }
    }  

    return NULL;
}

/*
**dbot_ddp_service_init:初始化一个ddp server
**
**param:DBOT_SERVICE_CONTEXT_S *dbot_ctx
**
**return: void
*/
int dbot_ddp_service_init(void *dbot_ctx)
{
    RETURN_IF_NULL(dbot_ctx, DBOT_INVALID_PARAM);
    
    DBOT_SERVICE_CONTEXT_S *ctx = (DBOT_SERVICE_CONTEXT_S*)dbot_ctx;
    if(NULL == s_ddp_service_ctx)
    {
        s_ddp_service_ctx = calloc(1, sizeof(DDP_SERVICE_CONTEXT_S));
        RETURN_IF_NULL(s_ddp_service_ctx, DBOT_MALLOC_FAIL);

        os_list_init(&s_ddp_service_ctx->res_list);
        pthread_mutex_init(&s_ddp_service_ctx->res_list_lock, NULL);
        s_ddp_service_ctx->server_port = DDP_SERVICE_PORT;
        s_ddp_service_ctx->server_fd = -1;
        s_ddp_service_ctx->dbot_ctx = dbot_ctx;
        ctx->ddp = s_ddp_service_ctx;
        s_ddp_service_ctx->data_buf = calloc(1, MAX_SOCKET_RECV_BUFFER_LEN);
        if(NULL == s_ddp_service_ctx->data_buf)
        {
            FREE_POINTER(s_ddp_service_ctx);
            return DBOT_MALLOC_FAIL;
        }
        DBOT_LOG_I("dbot ddp server inited\r\n");
    }

    return DBOT_OK;
}

/*
**dbot_ddp_service_add_resource:往ddp server添加可访问的资源
**
**param:DDP_SERVICE_CONTEXT_S *ctx, ddp server上下文
**      const char *res_name, 资源名
**      DDP_REQUEST_TYPE_E req_type, 资源访问类型
**      DDP_RESPONSE_PROC func, 资源访问函数
**
**return: int, DBOT_OK-执行成功，其他-执行失败
*/
int dbot_ddp_service_add_resource(const char *res_name, DDP_REQUEST_PROC func)
{
    RETURN_IF_NULL(s_ddp_service_ctx, DBOT_INVALID_PARAM);
    RETURN_IF_NULL(res_name, DBOT_INVALID_PARAM);
    RETURN_IF_NULL(func, DBOT_INVALID_PARAM);

    DDP_RESOURCE_T *res_node = calloc(1, sizeof(DDP_RESOURCE_T));
    RETURN_IF_NULL(res_node, DBOT_MALLOC_FAIL);

    os_list_init(&res_node->node);
    strncpy(res_node->res_name, res_name, sizeof(res_node->res_name));
    res_node->func = func;

    pthread_mutex_lock(&s_ddp_service_ctx->res_list_lock);
    os_list_add_tail(&s_ddp_service_ctx->res_list, &res_node->node);
    pthread_mutex_unlock(&s_ddp_service_ctx->res_list_lock);

    return DBOT_OK;
}

/*
**dbot_ddp_service_start:启动ddp server服务
**
**param:DDP_SERVICE_CONTEXT_S *ctx, ddp server上下文
**
**return: int, DBOT_OK-执行成功，其他-执行失败
*/
int dbot_ddp_service_start(void)
{
    RETURN_IF_NULL(s_ddp_service_ctx, DBOT_INVALID_PARAM);

    s_ddp_service_ctx->server_fd = socket(AF_INET, SOCK_DGRAM, 0);
    if(0 > s_ddp_service_ctx->server_fd)
    {
        DBOT_LOG_E("create ddp server socket failed\r\n");
        return DBOT_SOCKET_FAIL;
    }

    do
    {
        int on = 1;
        if(0 != setsockopt(s_ddp_service_ctx->server_fd, SOL_SOCKET, SO_REUSEADDR, (const char*)&on, sizeof(on)))
        {
            DBOT_LOG_E("ddp server set socket reuse addr failed\r\n");
            break;
        }

#if defined _WIN32

#else
        struct timeval timeout;
        timeout.tv_sec = 2;
        timeout.tv_usec = 0;
        if(0 != setsockopt(s_ddp_service_ctx->server_fd, SOL_SOCKET, SO_SNDTIMEO, (const char*)&timeout, sizeof(struct timeval)))
        {
            DBOT_LOG_E("ddp server set socket send timeout failed\r\n");
            break;
        }
        
        if(0 != setsockopt(s_ddp_service_ctx->server_fd, SOL_SOCKET, SO_RCVTIMEO, (const char*)&timeout, sizeof(struct timeval)))
        {
            DBOT_LOG_E("ddp server set socket recv timeout failed\r\n");
            break;
        }
#endif
        // DDP需要广播支持
        if(0 != setsockopt(s_ddp_service_ctx->server_fd, SOL_SOCKET, SO_BROADCAST, (const char*)(&on), sizeof(on)))
        {
            DBOT_LOG_E("ddp server set socket enable broadcast failed\r\n");
            break;
        }

        struct sockaddr_in addr;
        memset(&addr, 0, sizeof(addr));
        addr.sin_family = AF_INET;
        addr.sin_addr.s_addr = INADDR_ANY;
        addr.sin_port = htons(s_ddp_service_ctx->server_port);
        if(0 > bind(s_ddp_service_ctx->server_fd, (struct sockaddr*)&addr, sizeof(addr)))
        {
            DBOT_LOG_E("bind ddp server port failed\r\n");
            break;
        }

        pthread_attr_t attr;
        pthread_attr_init(&attr);
        pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
        pthread_attr_setstacksize(&attr, 4096);
        if(0 != pthread_create(&s_ddp_service_ctx->server_pid, &attr, dbot_ddp_service_thread_func, (void*)s_ddp_service_ctx))
        {
            DBOT_LOG_E("create ddp service thread failed\r\n");
            break;
        }

        DBOT_LOG_I("dbot ddp server started on port %d:%u\r\n", s_ddp_service_ctx->server_fd, s_ddp_service_ctx->server_port);
        return DBOT_OK;
    } while (0);
    
    closesocket(s_ddp_service_ctx->server_fd);
    s_ddp_service_ctx->server_fd = -1;
    return DBOT_ERROR;
}

/*
**dbot_ddp_service_send_request:往指定ip发送ddp请求
**
**param:DDP_SERVICE_CONTEXT_S *ctx, ddp server上下文
**      const char *res_name, 资源名
**      char *ip, 目的ip
**      char *send_buf, 待发送内容
**      int buf_len, 待发送内容长度
**      DDP_RESPONSE_PROC cb, 异步回调, 若请求需要回复内容, 通过此回调获取
**
**return: int, DBOT_OK-执行成功，其他-执行失败
*/
int dbot_ddp_service_send_request(char *dst_ip, char *send_buf, int buf_len)
{
    RETURN_IF_NULL(s_ddp_service_ctx, DBOT_INVALID_PARAM);
    RETURN_IF_NULL(send_buf, DBOT_INVALID_PARAM);

    struct sockaddr_in addr;
    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(DDP_SERVICE_PORT);
    addr.sin_addr.s_addr = (NULL == dst_ip)?inet_addr(get_local_device_info()->broadcast_ip):inet_addr(dst_ip);

    int cnt = 3;
    while(cnt > 0)
    {
        if(0 < sendto(s_ddp_service_ctx->server_fd, send_buf, strlen(send_buf), 0, (struct sockaddr*)&addr, sizeof(struct sockaddr_in)))
        {
            break;
        }
        dbot_msleep(200);
        cnt --;
    }

    return DBOT_OK;
}

