#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include "dbot_log.h"
#include "dbot_errno.h"
#include "dbot_common.h"
#include "cJSON.h"
#include "dtcp_server.h"
#include "dbot_service.h"
#include "dtp_service.h"
#if defined __linux__
#include <sys/socket.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <errno.h>
#elif defined _WIN32
#include <winsock2.h>
#include <time.h>
#else
#include "os_clock.h"
#include "sys/socket.h"
#include "lwip/netif.h"
#endif

static DTCP_SERVER_CONTEXT_S *s_dtcp_server_ctx = NULL;

extern DEVICE_NODE_T *find_exist_device(int device_id);
extern void clear_service(DEVICE_NODE_T *dev_node);

void client_offline(DEVICE_NODE_T *device_node)
{
    if(NULL != device_node)
    {
        // 标记为离线状态，不删除设备
        device_node->online = 0;
        if(0 <= device_node->client.fd)
        {
            closesocket(device_node->client.fd);
            device_node->client.fd = -1;
            device_node->client.connected = 0;
        }
        // 设备离线则清除之前发现的设备能力
        clear_service(device_node);
        memset(&device_node->client.client_addr, 0, sizeof(device_node->client.client_addr));
    }
}

static void dtcp_server_heartbeat_check(DBOT_SERVICE_CONTEXT_S *dbot_ctx)
{
    if(NULL != dbot_ctx)
    {
        int now = get_current_time();
        DEVICE_NODE_T *device_node = NULL;
        pthread_mutex_lock(&dbot_ctx->discovery->device_list_lock);
        os_list_for_each_entry(device_node, &dbot_ctx->discovery->device_list, DEVICE_NODE_T, node)
        {
            if(device_node->online)
            {
                if((now - device_node->last_heartbeat_time) > HB_OFFLINE_CYCLES * DBOT_HB_INTERVAL * 1000)
                {
                    DBOT_LOG_I("device %d offline\r\n", device_node->device_info.device_id);
                    client_offline(device_node);
                }
            }
        }
        pthread_mutex_unlock(&dbot_ctx->discovery->device_list_lock);
    }
}

static void dtcp_service_service_check(DBOT_SERVICE_CONTEXT_S *dbot_ctx)
{
    if(NULL != dbot_ctx)
    {
        bool retry = false;
        DEVICE_NODE_T *device_node = NULL;
        pthread_mutex_lock(&dbot_ctx->discovery->device_list_lock);
        os_list_for_each_entry(device_node, &dbot_ctx->discovery->device_list, DEVICE_NODE_T, node)
        {
            if(device_node->online)
            {
                if(device_node->device_info.service_total > device_node->device_info.service_cnt)
                {
                    DBOT_LOG_D("device[%d] service not complete[%d-%d]\r\n", device_node->device_info.device_id, device_node->device_info.service_total, device_node->device_info.service_cnt);
                    retry = true;
                    break;
                }
            }
        }
        pthread_mutex_unlock(&dbot_ctx->discovery->device_list_lock);
        if(retry)
        {
            // 调用发现能力
            dbot_service_discovery_service();
        }
    }    
}

static int dtcp_server_auth_handle(DBOT_SERVICE_CONTEXT_S *dbot_ctx, cJSON *req_json, char *req, char **resp)
{
    cJSON *root = cJSON_CreateObject();
    cJSON_AddNumberToObject(root, "code", 0);
    *resp = cJSON_PrintUnformatted(root);
    cJSON_Delete(root);
    return DBOT_OK;
}

static int dtcp_server_heartbeat_handle(DBOT_SERVICE_CONTEXT_S *dbot_ctx, cJSON *req_json, char *req, DEVICE_NODE_T *client, char **resp)
{
    client->last_heartbeat_time = get_current_time();
    cJSON *root = cJSON_CreateObject();
    cJSON_AddStringToObject(root, "option", "heartbeat_reply");
    *resp = cJSON_PrintUnformatted(root);
    cJSON_Delete(root);
    return DBOT_OK;
}

static int dtcp_server_request_proc(DBOT_SERVICE_CONTEXT_S *dbot_ctx, int req_len, char *req, DEVICE_NODE_T *client, char **resp)
{
    RETURN_IF_NULL(dbot_ctx, DBOT_INVALID_PARAM);
    RETURN_IF_NULL(resp, DBOT_INVALID_PARAM);
    RETURN_IF_NULL(req, DBOT_INVALID_PARAM);

    int ret = DBOT_OK;
    cJSON *root = cJSON_Parse(req);
    RETURN_IF_NULL(root, DBOT_INVALID_PARAM);

    cJSON *option = cJSON_GetObjectItem(root, "option");
    if(NULL != option)
    {
        if(!strcmp(option->valuestring, "auth"))
        {
            ret = dtcp_server_auth_handle(dbot_ctx, root, req, resp);
        }
        if(!strcmp(option->valuestring, "heartbeat"))
        {
            ret = dtcp_server_heartbeat_handle(dbot_ctx, root, req, client, resp);
        }
    }

    cJSON_Delete(root);
    return ret;
}

int accept_client(DBOT_SERVICE_CONTEXT_S *dbot_ctx, int listen_fd)
{
    RETURN_IF_NULL(dbot_ctx, DBOT_INVALID_PARAM);
    RETURN_IF_NULL(s_dtcp_server_ctx, DBOT_INVALID_PARAM);

    int ret = DBOT_ERROR;
    int fd = -1;
    struct sockaddr_in addr;
#ifdef _WIN32
    int addr_len = sizeof(struct sockaddr_in);
#else
    socklen_t addr_len = sizeof(struct sockaddr_in);
    struct timeval timeout;
#endif
    DEVICE_NODE_T *device_node = NULL;

    memset(&addr, 0, sizeof(addr));
    fd = accept(listen_fd, (struct sockaddr *)&addr, &addr_len);
    if(0 > fd)
    {
        return DBOT_SOCKET_FAIL;
    }
    else
    {
        if(s_dtcp_server_ctx->max_client_num == s_dtcp_server_ctx->client_num)
        {
            closesocket(fd);
            DBOT_LOG_E("Client already connected, refuse new connection\r\n");
            ret = DBOT_SOCKET_FAIL;
        }
        else
        {
#if defined _WIN32

#else
            timeout.tv_sec = 2;
            timeout.tv_usec = 0;
            setsockopt(fd, SOL_SOCKET, SO_SNDTIMEO, (const char*)&timeout, sizeof(struct timeval));
            setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, (const char*)&timeout, sizeof(struct timeval));
#endif
            DBOT_LOG_I("dbot dtcp server accept new session %d\r\n", fd);
            pthread_mutex_lock(&dbot_ctx->discovery->device_list_lock);
            os_list_for_each_entry(device_node, &dbot_ctx->discovery->device_list, DEVICE_NODE_T, node)
            {
                if(inet_addr(device_node->device_info.device_ip) == addr.sin_addr.s_addr)
                {
                    device_node->last_heartbeat_time = get_current_time();
                    device_node->client.fd = fd;
                    device_node->client.connected = 1;
                    device_node->client.hold_up = 0;
                    memcpy(&device_node->client.client_addr, &addr, sizeof(device_node->client.client_addr));
                    break;
                }
            }
            pthread_mutex_unlock(&dbot_ctx->discovery->device_list_lock);
            // 更新最大套接字
            s_dtcp_server_ctx->max_fd = (fd > s_dtcp_server_ctx->max_fd)?fd:s_dtcp_server_ctx->max_fd;
            ret = DBOT_OK;
        }
    }

    return ret;
}

int recv_client_msg(DBOT_SERVICE_CONTEXT_S *dbot_ctx, DEVICE_NODE_T *client)
{
    RETURN_IF_NULL(s_dtcp_server_ctx, DBOT_INVALID_PARAM);
    RETURN_IF_NULL(dbot_ctx, DBOT_INVALID_PARAM);
    RETURN_IF_NULL(client, DBOT_INVALID_PARAM);

    int ret = -1;
    char *resp = NULL;
    memset(s_dtcp_server_ctx->data_buf, 0, MAX_SOCKET_RECV_BUFFER_LEN);
    ret = recv(client->client.fd, s_dtcp_server_ctx->data_buf, MAX_SOCKET_RECV_BUFFER_LEN, 0);
    if(0 < ret)
    {
        // 每收到一次消息更新时间戳
        client->last_heartbeat_time = get_current_time();
        
        s_dtcp_server_ctx->data_len = ret;
        dtcp_server_request_proc(dbot_ctx, s_dtcp_server_ctx->data_len, s_dtcp_server_ctx->data_buf, client, &resp);
        if(NULL != resp)
        {
            send(client->client.fd, resp, strlen(resp), 0);
            FREE_POINTER(resp);
        }
    }
    else if(0 == ret)
    {

    }
    else
    {
        if((errno == EAGAIN||errno == EWOULDBLOCK||errno == EINTR))
        {
            ret = 1;
        }
    }
    return ret;
}

static void *dtcp_server_thread_func(void *param)
{
    RETURN_NULL_IF_NULL(s_dtcp_server_ctx);

    fd_set fds;
    struct timeval tv;
    DEVICE_NODE_T *device_node = NULL;
    DBOT_SERVICE_CONTEXT_S *dbot_ctx = (DBOT_SERVICE_CONTEXT_S*)s_dtcp_server_ctx->dbot_ctx;
    tv.tv_sec  = 1;
    tv.tv_usec = 0;

    while(1)
    {
        if(s_dtcp_server_ctx->quit_flag)
        {
            break;
        }
        dbot_msleep(10);

        // 线程兼具设备心跳检查功能
        dtcp_server_heartbeat_check(dbot_ctx);

        // 设备能力完整性检查
        dtcp_service_service_check(dbot_ctx);

        FD_ZERO(&fds);
        // 监听套接字单独加入
        FD_SET(s_dtcp_server_ctx->server.fd, &fds);
        // 加入已连接的客户端
        pthread_mutex_lock(&dbot_ctx->discovery->device_list_lock);
        os_list_for_each_entry(device_node, &dbot_ctx->discovery->device_list, DEVICE_NODE_T, node)
        {
            if(device_node->client.connected && device_node->client.fd >= 0 && device_node->client.hold_up == 0)
            {
                FD_SET(device_node->client.fd, &fds);
            }
        }
        pthread_mutex_unlock(&dbot_ctx->discovery->device_list_lock);

        if(0 >= select(s_dtcp_server_ctx->max_fd + 1, &fds, NULL, NULL, &tv))
        {
            continue;
        }

        // 检查是否有客户端可连接
        if(FD_ISSET(s_dtcp_server_ctx->server.fd, &fds))
        {
            accept_client(dbot_ctx, s_dtcp_server_ctx->server.fd);
        }

        // 检查是否有客户端可读数据
        pthread_mutex_lock(&dbot_ctx->discovery->device_list_lock);
        os_list_for_each_entry(device_node, &dbot_ctx->discovery->device_list, DEVICE_NODE_T, node)
        {
            if(device_node->client.connected && device_node->client.fd >= 0 && device_node->client.hold_up == 0)
            {
                if(FD_ISSET(device_node->client.fd, &fds))
                {
                    if(0 >= recv_client_msg(dbot_ctx, device_node))
                    {
                        DBOT_LOG_E("Connect with %d is closed\r\n", device_node->device_info.device_id);
                        client_offline(device_node);
                    }
                }
            }
        }
        pthread_mutex_unlock(&dbot_ctx->discovery->device_list_lock);
    }
    return NULL;
}

int dbot_dtcp_server_init(void *dbot_ctx)
{
    RETURN_IF_NULL(dbot_ctx, DBOT_INVALID_PARAM);

    DBOT_SERVICE_CONTEXT_S *ctx = (DBOT_SERVICE_CONTEXT_S*)dbot_ctx;
    if(NULL == s_dtcp_server_ctx)
    {
        s_dtcp_server_ctx = calloc(1, sizeof(DTCP_SERVER_CONTEXT_S));
        RETURN_IF_NULL(s_dtcp_server_ctx, DBOT_INVALID_PARAM);

        if(DBOT_OK != create_tcp_server(&s_dtcp_server_ctx->server, 0, 2))
        {
            FREE_POINTER(s_dtcp_server_ctx);
            return DBOT_SOCKET_FAIL;
        }

        s_dtcp_server_ctx->dbot_ctx = dbot_ctx;
        s_dtcp_server_ctx->max_client_num = 1;
        s_dtcp_server_ctx->client_num = 0;
        s_dtcp_server_ctx->max_fd = s_dtcp_server_ctx->server.fd;
        ctx->dtcp_server = s_dtcp_server_ctx;       
        DBOT_LOG_I("dbot dtcp server inited\r\n");
    }
    
    return DBOT_OK;
}

int dbot_dtcp_server_start(void)
{
    RETURN_IF_NULL(s_dtcp_server_ctx, DBOT_INVALID_PARAM);

    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
    pthread_attr_setstacksize(&attr, 4096);
    if(0 != pthread_create(&s_dtcp_server_ctx->pid, &attr, dtcp_server_thread_func, NULL))
    {
        DBOT_LOG_E("create dtcp server thread failed\r\n");
        return DBOT_ERROR;
    }

    return DBOT_OK;
}

int dbot_dtcp_server_create_session(void *dbot_ctx, int device_id)
{
    RETURN_IF_NULL(dbot_ctx, DBOT_INVALID_PARAM);

    int cnt = 25;
    int check = 0;
    int ret = DBOT_ERROR;
    DBOT_SERVICE_CONTEXT_S *ctx = (DBOT_SERVICE_CONTEXT_S*)dbot_ctx;
    pthread_mutex_lock(&ctx->discovery->device_list_lock);
    DEVICE_NODE_T *device_node = find_exist_device(device_id);
    pthread_mutex_unlock(&ctx->discovery->device_list_lock);
    if(NULL != device_node)
    {
        if(!device_node->client.connected)
        {
            cJSON *root = cJSON_CreateObject();
            cJSON_AddStringToObject(root, "resource", DBOT_SESSION_SERVICE);
            cJSON_AddNumberToObject(root, "device_id", device_id);
            cJSON_AddStringToObject(root, "ip", dbot_service_get_ctx()->profile->device_ip);
            cJSON_AddNumberToObject(root, "port", s_dtcp_server_ctx->server.port);
            char *req = cJSON_PrintUnformatted(root);
            if(NULL != req)
            {
                if(DBOT_OK == dbot_ddp_service_send_request(device_node->device_info.device_ip, req, strlen(req)))
                {
                    check = 1;
                }

            }
        }
        else
        {
            DBOT_LOG_E("device %d already connected\r\n", device_id);
            ret = DBOT_OK;
        }
    }
    else
    {
        DBOT_LOG_E("device %d not exists\r\n", device_id);
    }

    while(check && cnt)
    {
        pthread_mutex_lock(&ctx->discovery->device_list_lock);
        device_node = find_exist_device(device_id);
        if(NULL != device_node)
        {
            if(device_node->client.connected && device_node->client.fd >= 0)
            {
                check = 0;
                device_node->online = 1;
                ret = DBOT_OK;
            }
        }
        pthread_mutex_unlock(&ctx->discovery->device_list_lock);
        cnt --;
        dbot_msleep(200);
    }
    
    DBOT_LOG_I("create session with %d %s\r\n", device_id, (DBOT_OK == ret)?"succeed":"failed");
    return ret;
}

int dbot_dtcp_server_remove_session(void *dbot_ctx, int device_id)
{
    RETURN_IF_NULL(dbot_ctx, DBOT_ERROR);

    DBOT_SERVICE_CONTEXT_S *ctx = (DBOT_SERVICE_CONTEXT_S*)dbot_ctx;
    // 查找是否存在该设备
    pthread_mutex_lock(&ctx->discovery->device_list_lock);
    DEVICE_NODE_T *device_node = find_exist_device(device_id);
    if(NULL != device_node)
    {
        if(device_node->client.connected && device_node->client.fd >= 0)
        {
            closesocket(device_node->client.fd);
            device_node->client.fd = -1;
            device_node->client.connected = 0;
            memset(&device_node->client.client_addr, 0, sizeof(device_node->client.client_addr));
            DBOT_LOG_I("dbot remove session with %d succeed\r\n", device_id);
        }
    }
    pthread_mutex_unlock(&ctx->discovery->device_list_lock);

    return DBOT_OK;
}

int dbot_dtcp_server_call_service(void *dbot_ctx, int device_id, uint32_t service, char *func, char *param, DTCP_CALL_RESULT_CB cb)
{
    RETURN_IF_NULL(dbot_ctx, DBOT_INVALID_PARAM);
    RETURN_IF_NULL(func, DBOT_INVALID_PARAM);

    int ret = DBOT_ERROR;
    DBOT_SERVICE_CONTEXT_S *ctx = (DBOT_SERVICE_CONTEXT_S*)dbot_ctx;
    // 查找是否存在该设备
    pthread_mutex_lock(&ctx->discovery->device_list_lock);
    DEVICE_NODE_T *device_node = find_exist_device(device_id);
    pthread_mutex_unlock(&ctx->discovery->device_list_lock);
    if(NULL == device_node)
    {
        DBOT_LOG_E("device %d not exists\r\n", device_id);
        return DBOT_DEVICE_NOT_EXISTS;
    }

    if(!device_node->client.connected || 0 > device_node->client.fd)
    {
        DBOT_LOG_E("device %d not connected\r\n", device_id);
        // 若不存在回话则尝试建立回话
        if(DBOT_OK != dbot_dtcp_server_create_session(dbot_ctx, device_id))
        {
            return DBOT_ERROR;
        }
    }
    cJSON *root = cJSON_CreateObject();
    RETURN_IF_NULL(root, DBOT_INVALID_PARAM);
    cJSON_AddStringToObject(root, "option", "call");
    cJSON_AddNumberToObject(root, "device_id", device_id);
    cJSON_AddNumberToObject(root, "service", service);
    cJSON_AddStringToObject(root, "func", func);
    cJSON *params = cJSON_Parse(param);
    if(NULL != params)
    {
        cJSON_AddItemToObject(root, "params", params);
    }
    char *str = cJSON_PrintUnformatted(root);
    if(NULL != str)
    {
        DBOT_LOG_D("dbot call service: %s\r\n", str);
        // 占用fd
        device_node->client.hold_up = 1;
        ret = send(device_node->client.fd, str, strlen(str), 0);
        if(0 < ret)
        {
            char *buf = calloc(1, MAX_SOCKET_RECV_BUFFER_LEN);
            if(NULL != buf)
            {
                char *resp = NULL;
                int ts = get_current_time();
                // 最多等待5s
                while((get_current_time() - ts) <= 5 * 1000)
                {
                    ret = recv(device_node->client.fd, buf, MAX_SOCKET_RECV_BUFFER_LEN, 0);
                    if(0 < ret)
                    {
                        device_node->last_heartbeat_time = get_current_time();
                        // 通用消息
                        if(NULL != strstr(buf, "option"))
                        {
                            dtcp_server_request_proc(ctx, ret, buf, device_node, &resp);
                            if(NULL != resp)
                            {
                                send(device_node->client.fd, resp, strlen(resp), 0);
                                FREE_POINTER(resp);
                            }
                        }
                        else
                        {
                            // 回复
                            if(NULL != cb)
                            {
                                cb(buf, ret);
                            }
                            ret = DBOT_OK;
                            break;
                        }
                    }
                    else
                    {
                        // DBOT_LOG_E("dtcp recv failed\r\n");
                        // client_offline(device_node);
                        ret = DBOT_ERROR;
                        break;
                    }
                }
                FREE_POINTER(buf);
            }
        }
        else
        {
            DBOT_LOG_E("dtcp send failed\r\n");
            ret = DBOT_ERROR;
        }
        
        if(DBOT_ERROR == ret)
        {
            DBOT_LOG_E("dbot call service failed[%d-%s]\r\n", errno, strerror(errno));
            dbot_dtcp_server_remove_session(dbot_ctx, device_id);
        }
        FREE_POINTER(str);
        // 解除fd占用
        device_node->client.hold_up = 0;
    }

    cJSON_Delete(root);

    return ret;
}


