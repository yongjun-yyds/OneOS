#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <pthread.h>
#include <unistd.h>
#include <stdbool.h>
#include "rtsp_server.h"
#include "rtp.h"
#include "dbot_list.h"
#include "dbot_errno.h"
#include "dbot_log.h"

#ifdef _WIN32
#include <winsock2.h>
#include <sys/time.h>
#elif __linux__
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <errno.h>
#else
#include "board.h"
#include "sys/socket.h"
#include "lwip/netif.h"
#endif

static struct rtsp_server_context *s_rtsp_server = NULL;
//an example of media representation in SDP using typicalparameters
static char sdp_info[512]= "v=0\r\n"
                            "o=- 1234567890987654321 0 IN IP4 0.0.0.0\r\n"
                            "s=oneos dbot sdp\r\n"
                            "c=IN IP4 0.0.0.0\r\n"
                            "t=0 0\r\n"
                            "m=video 0 RTP/AVP 98\r\n"
                            "a=rtpmap:98 jpeg/90000\r\n"
                            "a=fmtp:98 sampling=YCbCr-4:2:0;width=128;height=128\r\n";
static unsigned short seq_num = 0;
static unsigned char q_tabel1[64];
static unsigned char q_table2[64];

// static char sdp_info[2048]="v=0\r\n"
//             "o=- 1234567890987654321 0 IN IP4 0.0.0.0\r\n"
//             "s=oneos dbot sdp\r\n"
//             "c=IN IP4 0.0.0.0\r\n"
//             "t=0 0\r\n"
//             "m=video 0 RTP/AVP 97\r\n"
//             "a=range:npt=0-1.4\r\n"
//             "a=recvonly\r\n"
//             "a=rtpmap:97 JPEG/90000\r\n"
//             ;
int generate_rtp_header(char *dst, int payload_len, int is_last, unsigned short seq_num, unsigned long long pts,
                        unsigned long ssrc, enum rtp_transport transport)
{
    int index = 0;

    if (transport == RTP_TRANSPORT_TCP)
    {
        /* RTSP interleaved frame header */
        dst[index++] = 0x24;
        dst[index++] = 0x00;
        dst[index++] = ((payload_len + 12) >> 8) & 0xff;
        dst[index++] = (payload_len + 12) & 0xff;
    }

    /* RTP header */
    dst[index++] = 0x80;
    dst[index++] = (is_last ? 0x80 : 0x00) | 96;
    dst[index++] = (seq_num >> 8) & 0xff;
    dst[index++] = seq_num & 0xff;
    dst[index++] = (pts >> 24) & 0xff;
    dst[index++] = (pts >> 16) & 0xff;
    dst[index++] = (pts >> 8) & 0xff;
    dst[index++] = pts & 0xff;
    dst[index++] = (ssrc >> 24) & 0xff;
    dst[index++] = (ssrc >> 16) & 0xff;
    dst[index++] = (ssrc >> 8) & 0xff;
    dst[index++] = ssrc & 0xff;

    return index;
}

int _send(int fd, void *buf, int len)
{
    struct timeval tv;
    fd_set fds;
    int ret;

    do
    {
        tv.tv_sec  = 0;
        tv.tv_usec = 1000;
        FD_ZERO(&fds);
        FD_SET(fd, &fds);
        ret = select(fd + 1, NULL, &fds, NULL, &tv);
        if (ret > 0)
        {
            if (FD_ISSET(fd, &fds))
            {
                send(fd, buf, len, 0);
            }
        }
        else if (ret == 0)
        {
            continue;
        }
        else
        {
            return -1;
        }
    } while (ret == 0);

    return 0;
}

void send_stream(struct client_context *client, struct rtp_packet *pkt)
{
    if (client->server->rtp_info.transport == RTP_TRANSPORT_UDP)
    {
        client->addr.sin_port = htons(client->udp_send_port);
        sendto(client->server->rtp_info.udp_send_socket, pkt->buf, pkt->len, 0, (struct sockaddr *)&client->addr,
                        sizeof(struct sockaddr_in));
    }
    else
    {
        _send(client->fd, pkt->buf, pkt->len);
    }
}

void send_to_clients(struct rtsp_server_context *server, struct rtp_packet *pkt, char *url)
{
    struct client_context *client = NULL;
    pthread_mutex_lock(&server->client_list_lock);
    os_list_for_each_entry(client, &server->client_list, struct client_context, link)
    {
        if(client)
        {
            if(NULL != strstr(client->url, url))
            {
                if(client->start_play)
                {
                    send_stream(client, pkt);
                }
            }
        }
    }
    pthread_mutex_unlock(&server->client_list_lock);
}

unsigned int rtp_timestamp(void)
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    unsigned int  timestamp = (90000*tv.tv_sec);
    timestamp += (unsigned int )((2.0*90000*tv.tv_usec + 1000000.0)/2000000);   
    unsigned int const rtp_timestamp = timestamp;  
    return rtp_timestamp;
}

uint32_t crc(uint8_t *data, int len)
{
    uint32_t c;
    for(int i = 0;i < len;i ++)
        c = c ^ (data[i]);
    return c;
}

void generate_rtp_packets_and_send(struct rtsp_server_context *server, void *data, int size, unsigned int height, unsigned int width, char *url)
{
    rtp_hdr_t rtp_hdr;
    jpeg_hdr_t jpeg_hdr;
    jpeg_hdr_rst_t jpeg_rst_hdr;
    jpeg_hdr_qtable_t jpeg_qtable_hdr;
	unsigned char *ptr = NULL;
    unsigned int bytes_left = size;
    unsigned int data_len = 0;
    unsigned int offset = 0;
    unsigned int rtp_ssrc = 10;
    int dri = 0;

    if(NULL == data)
    {
        return;
    }

	/* Initialize RTP header */
	rtp_hdr.version = 2;
	rtp_hdr.p = 0;
	rtp_hdr.x = 0;
	rtp_hdr.cc = 0;
	rtp_hdr.m = 0;
	rtp_hdr.pt = RTP_PT_JPEG;
    rtp_hdr.seq = 0;
	rtp_hdr.ts = htonl(rtp_timestamp());
	rtp_hdr.ssrc = htonl(rtp_ssrc);
    // rtp_hdr.ssrc = htonl(crc((uint8_t*)data, size));

	/* Initialize JPEG header */
	jpeg_hdr.tspec = 0;
	jpeg_hdr.offset = 0;
	jpeg_hdr.type = 0 | ((dri != 0) ? RTP_JPEG_RESTART : 0);
	jpeg_hdr.q = 0;
	jpeg_hdr.width = (width / 8);
	jpeg_hdr.height = (height / 8);

	/* Initialize DRI header */
	if (dri != 0)
    {
		jpeg_rst_hdr.dri = dri;
		jpeg_rst_hdr.f = 1;        /* This code does not align RIs */
		jpeg_rst_hdr.l = 1;
		jpeg_rst_hdr.count = 0x3fff;
	}

	/* Initialize quantization table header */
	if (jpeg_hdr.q >= 128)
    {
		jpeg_qtable_hdr.mbz = 0;
		jpeg_qtable_hdr.precision = 0;
		jpeg_qtable_hdr.length = 128;
	}

    unsigned int tmp = 0;
    struct rtp_packet *pkt = NULL;
    pkt = (struct rtp_packet*)malloc(sizeof(struct rtp_packet));
    if(NULL == pkt)
    {
        return;
    }
    memset(pkt, 0, sizeof(struct rtp_packet));

    //RTP封装以及分片
    while(bytes_left > 0)
    {
        ptr = pkt->buf + RTP_HDR_SZ;
        tmp = htonl(offset);
        tmp = tmp >> 8;
        jpeg_hdr.offset = tmp;

        memcpy(ptr, &jpeg_hdr, sizeof(jpeg_hdr));
      	ptr += sizeof(jpeg_hdr);
        rtp_hdr.m = 0;

        //如果有复位标记，则将复位标记头放在jpeg头之后
        if(dri != 0)
        {
            memcpy(ptr, &jpeg_rst_hdr, sizeof(jpeg_rst_hdr));
            ptr += sizeof(jpeg_rst_hdr);
        }
        //发送的第一帧数据带上量化表
        if(jpeg_hdr.q >= 128 && 0 == jpeg_hdr.offset)
        {
            memcpy(ptr, &jpeg_qtable_hdr, sizeof(jpeg_qtable_hdr));
            ptr += sizeof(jpeg_qtable_hdr);
            memcpy(ptr, q_tabel1, 64);
            ptr += 64;
            memcpy(ptr, q_table2, 64);
            ptr += 64;
        }

        pkt->header_len = ptr - pkt->buf;
        //本帧可封装的数据长度
        data_len = PACKET_SIZE - (pkt->header_len);
        if (data_len >= bytes_left) 
        {
            data_len = bytes_left;
            rtp_hdr.m = 1;
        }
        pkt->len = pkt->header_len + data_len;
        rtp_hdr.seq = htons(seq_num);
        memcpy(pkt->buf, &rtp_hdr, RTP_HDR_SZ);
        memcpy(ptr, (uint8_t*)data + offset, data_len);
        //发送数据
        send_to_clients(server, pkt, url);
        //数据偏移，seq+1
        offset += data_len;
        bytes_left -= data_len;
        seq_num ++;
    }
    free(pkt);
}

void *client_thread_proc(void *arg)
{
    struct client_context *client_ctx = (struct client_context *)arg;

    while (client_ctx->server->stop == 0)
    {
        if (client_ctx->process_func)
        {
            if (client_ctx->process_func(client_ctx) < 0)
            {
                pthread_mutex_lock(&client_ctx->server->client_list_lock);
                os_list_del(&client_ctx->link);
                pthread_mutex_unlock(&client_ctx->server->client_list_lock);
                closesocket(client_ctx->fd);
                if (client_ctx->server->release_client)
                    client_ctx->server->release_client(client_ctx);
                free(client_ctx);
                break;
            }
        }
    }

    return NULL;
}

void *rtp_tcp_server_thread(void *arg)
{
    struct rtsp_server_context *server_ctx = (struct rtsp_server_context *)arg;
    struct client_context *client_ctx;
    int fd;
    pthread_attr_t attr;
#ifdef _WIN32
    int addr_len = sizeof(struct sockaddr_in);
#else
    socklen_t addr_len = sizeof(struct sockaddr_in);
#endif

    server_ctx->sd = socket(AF_INET, SOCK_STREAM, 0);
    if (server_ctx->sd < 0)
    {
        DBOT_LOG_E("create server socket failed\n");
        return NULL;
    }

    int on = 1;
    setsockopt(server_ctx->sd, SOL_SOCKET, SO_REUSEADDR, (const char*)&on, sizeof(on));

    // fcntl(server_ctx->sd, F_SETFL, O_NONBLOCK);
    struct sockaddr_in addr;
    memset(&addr, 0, sizeof(struct sockaddr_in));
    addr.sin_family      = AF_INET;
    addr.sin_port        = htons(server_ctx->port);
    addr.sin_addr.s_addr = INADDR_ANY;

    if (bind(server_ctx->sd, (struct sockaddr *)&addr, sizeof(addr)) != 0)
    {
        DBOT_LOG_E("Unable to bind\n");
        return NULL;
    }

    if (listen(server_ctx->sd, MAX_CLIENT_COUNT) != 0)
    {
        DBOT_LOG_E("Listen error\n");
        return NULL;
    }

    while (!server_ctx->stop)
    {
		memset(&addr, 0, sizeof(addr));
        fd = accept(server_ctx->sd, (struct sockaddr *)&addr, &addr_len);
        if (fd != -1)
        {
            client_ctx = calloc(1, sizeof(struct client_context));
            if (client_ctx == NULL)
            {
                DBOT_LOG_E("failed to allocate (a very small amount of) memory\n");
                closesocket(fd);
                continue;
            }
            memcpy(&client_ctx->addr, &addr, sizeof(addr));
            memset(client_ctx->url, 0, sizeof(client_ctx->url));
            client_ctx->server       = server_ctx;
            client_ctx->fd           = fd;
            client_ctx->start_play   = 0;
            if (server_ctx->init_client)
                server_ctx->init_client(client_ctx);

            pthread_attr_init(&attr);
            pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
            pthread_attr_setstacksize(&attr, 1024);
            if(0 != pthread_create(&client_ctx->thread_id, &attr, client_thread_proc, (void*)client_ctx))
            {
                DBOT_LOG_E("create rtsp client thread failed\r\n");
                closesocket(fd);
                free(client_ctx);
                continue;
            }

            pthread_mutex_lock(&server_ctx->client_list_lock);
            os_list_add_tail(&server_ctx->client_list, &client_ctx->link);
            pthread_mutex_unlock(&server_ctx->client_list_lock);
        }
    }

    closesocket(server_ctx->sd);
    return NULL;
}

void rtp_push_data(void *data, int size, unsigned int height, unsigned int width, char *url)
{
    if (s_rtsp_server == NULL)
        return;

    if (s_rtsp_server->client_list.next != &s_rtsp_server->client_list)
    {
        generate_rtp_packets_and_send(s_rtsp_server, data, size, height, width, url);
    }
}

/**
 * rtsp requests processing functions
 */
void generate_response_header(struct rtsp_session *session)
{
    char tmp[32];

    session->response_buf[0] = 0;
    strcat(session->response_buf, "RTSP/1.0 200 OK\r\n");
    snprintf(tmp, sizeof(tmp), "CSeq: %s\r\n", session->cseq);
    strcat(session->response_buf, tmp);
}

void send_response(int fd, void *buffer, int size)
{
    send(fd, buffer, size, 0);
}

void generate_session_number(struct rtsp_session *session)
{
    struct timeval tv;

    gettimeofday(&tv, NULL);
    session->session_num = tv.tv_sec * 1000L + tv.tv_usec / 1000L;
}

void rtsp_handle_options(struct rtsp_session *session)
{
    generate_response_header(session);
    strcat(session->response_buf, "Public: OPTIONS, DESCRIBE, SETUP, TEARDOWN, PLAY, PAUSE\r\n\r\n");
    send_response(session->ctx->fd, session->response_buf, strlen(session->response_buf));
}

void rtsp_handle_describe(struct rtsp_session *session)
{
    char tmp[32];

    generate_response_header(session);
    strcat(session->response_buf, "Content-Type: application/sdp\r\n");
    snprintf(tmp, sizeof(tmp), "Content-Length: %d\r\n\r\n", strlen(session->ctx->server->rtp_info.sdp.content));
    strcat(session->response_buf, tmp);
    strcat(session->response_buf, session->ctx->server->rtp_info.sdp.content);
    send_response(session->ctx->fd, session->response_buf, strlen(session->response_buf));
}

void rtsp_handle_setup(struct rtsp_session *session)
{
    char tmp[100];

    generate_session_number(session);
    generate_response_header(session);
    snprintf(tmp, sizeof(tmp), "Session: %lu\r\n", session->session_num);
    strcat(session->response_buf, tmp);

    if (session->ctx->server->rtp_info.transport == RTP_TRANSPORT_UDP)
    {
        if (session->ctx->udp_send_port == 0)
            session->ctx->udp_send_port = RTP_UDP_SEND_PORT;
        snprintf(tmp, 100,
                 "Transport: RTP/AVP;unicast;"
                 "client_port=%d-%d\r\n\r\n",
                 session->ctx->udp_send_port, session->ctx->udp_send_port + 1);
        strcat(session->response_buf, tmp);
    }
    else
    {
        strcat(session->response_buf,
               "Transport: RTP/AVP/TCP;unicast;interleaved=0-1;\r\n"
               "\r\n");
    }
    send_response(session->ctx->fd, session->response_buf, strlen(session->response_buf));
}

void rtsp_handle_play(struct rtsp_session *session)
{
    generate_response_header(session);
    strcat(session->response_buf, "\r\n");
    send_response(session->ctx->fd, session->response_buf, strlen(session->response_buf));
    session->ctx->start_play = 1;
}

int rtsp_get_request(struct rtsp_session *session)
{
    int msg_type;
    int ret;
    char *sub_str;
    fd_set fds;
    struct timeval tv;
    char *pos = NULL, *start = NULL;

    memset(session->read_buf, 0, REQUEST_READ_BUF_SIZE);

    FD_ZERO(&fds);
    FD_SET(session->ctx->fd, &fds);
    tv.tv_sec  = 0;
    tv.tv_usec = 10 * 1000;

    ret        = select(session->ctx->fd + 1, &fds, NULL, NULL, &tv);
    if (ret == 0)
        return RTSP_MSG_TIMEOUT;
    else if (ret < 0)
        return RTSP_MSG_ERROR;

    if (FD_ISSET(session->ctx->fd, &fds))
    {
        ret = recv(session->ctx->fd, session->read_buf, REQUEST_READ_BUF_SIZE - 1, 0);
        if (ret < 0)
            return RTSP_MSG_ERROR;
        else if (ret == 0)
            return RTSP_MSG_ERROR;
        session->read_buf[REQUEST_READ_BUF_SIZE - 1] = 0;
    }

    if (strstr(session->read_buf, "OPTIONS"))
        msg_type = RTSP_MSG_OPTIONS;
    else if (strstr(session->read_buf, "DESCRIBE"))
        msg_type = RTSP_MSG_DESCRIBE;
    else if (strstr(session->read_buf, "SETUP"))
        msg_type = RTSP_MSG_SETUP;
    else if (strstr(session->read_buf, "PLAY"))
        msg_type = RTSP_MSG_PLAY;
    else if (strstr(session->read_buf, "TEARDOWN"))
        msg_type = RTSP_MSG_TEARDOWN;
    else
        msg_type = RTSP_MSG_UNKNOWN;

    if(!strlen(session->ctx->url))
    {
        sub_str = strstr(session->read_buf, "rtsp://");
        pos = strstr(sub_str, " RTSP/1.0");
        if(NULL != sub_str && NULL != pos)
        {
            int url_len = pos - sub_str;
            if(url_len < RTSP_URL_MAX_LEN)
            {
                strncpy(session->ctx->url, sub_str, url_len);
                DBOT_LOG_I("client url:%s\r\n", session->ctx->url);
            }
        }
    }

    pos = strstr(session->read_buf, "\r\n");
    if (pos)
    {
        start = pos + 2;
        if ((sub_str = strstr(start, "CSeq: ")) != NULL)
        {
            pos = strstr(sub_str, "\r\n");
            if (pos)
            {
                memcpy(session->cseq, sub_str + 6, pos - sub_str - 6);
                session->cseq[pos - sub_str - 6] = 0;
                start                            = pos + 2;
            }
        }
        if ((sub_str = strstr(start, "client_port=")) != NULL)
        {
            pos = strstr(sub_str, "-");
            if (pos)
            {
                char tmp[10];
                memcpy(tmp, sub_str + 12, pos - sub_str - 12);
                tmp[pos - sub_str - 12]     = 0;
                session->ctx->udp_send_port = strtol(tmp, NULL, 0);
            }
        }
    }

    return msg_type;
}

int rtsp_message_process(struct rtsp_session *session)
{
    int type;
    type = rtsp_get_request(session);

    switch (type)
    {
    case RTSP_MSG_ERROR:
        return -1;
    case RTSP_MSG_OPTIONS:
        rtsp_handle_options(session);
        break;
    case RTSP_MSG_DESCRIBE:
        rtsp_handle_describe(session);
        break;
    case RTSP_MSG_SETUP:
        rtsp_handle_setup(session);
        break;
    case RTSP_MSG_PLAY:
        rtsp_handle_play(session);
        DBOT_LOG_I("rtsp: client start to play, socket=%d\n", session->ctx->fd);
        break;
    case RTSP_MSG_TEARDOWN:
        session->ctx->start_play = 0;
        DBOT_LOG_I("rtsp: client teardown, socket=%d\n", session->ctx->fd);
        return -1;
    default:
        break;
    }
    return 0;
}

int rtsp_process_func(struct client_context *client)
{
    return rtsp_message_process((struct rtsp_session *)client->session);
}

void init_rtsp_session(struct client_context *client)
{
    struct rtsp_session *session;
    DBOT_LOG_I("rtsp: new client connected, socket=%d\n", client->fd);
    session = malloc(sizeof(struct rtsp_session));
    memset(session, 0, sizeof(struct rtsp_session));
    session->ctx         = client;
    client->session      = session;
    client->process_func = rtsp_process_func;
}

void cleanup_rtsp_session(struct client_context *client)
{
    DBOT_LOG_I("rtsp: client disconnected, socket=%d\n", client->fd);
    if (client->session)
        free(client->session);
}

struct rtsp_server_context *rtsp_start_server(enum rtp_transport transport, int port)
{
    s_rtsp_server = calloc(1, sizeof(struct rtsp_server_context));
    if (s_rtsp_server == NULL)
    {
        DBOT_LOG_E("rtsp: failed to create server context object\r\n");
        return NULL;
    }

    os_list_init(&s_rtsp_server->client_list);
    pthread_mutex_init(&s_rtsp_server->client_list_lock, NULL);
    strncpy(s_rtsp_server->rtp_info.sdp.content, sdp_info, strlen(sdp_info));
    s_rtsp_server->rtp_info.sdp.len = strlen(sdp_info);

    if (transport != RTP_TRANSPORT_UDP && transport != RTP_TRANSPORT_TCP)
        s_rtsp_server->rtp_info.transport = RTP_TRANSPORT_TCP;
    else
        s_rtsp_server->rtp_info.transport = transport;

    if (s_rtsp_server->rtp_info.transport == RTP_TRANSPORT_UDP)
    {
        s_rtsp_server->rtp_info.udp_send_socket = socket(AF_INET, SOCK_DGRAM, 0);
        if (s_rtsp_server->rtp_info.udp_send_socket == -1)
        {
            DBOT_LOG_E("rtsp: failed to create send socket\r\n");
            free(s_rtsp_server);
            return NULL;
        }

        // int flag = fcntl(s_rtsp_server->rtp_info.udp_send_socket, F_GETFL, 0);
        // if(0 > fcntl(s_rtsp_server->rtp_info.udp_send_socket, F_SETFL, flag|O_NONBLOCK))
        // {
        //     DBOT_LOG_E("set rtsp udp unblock failed\r\n");
        //     closesocket(s_rtsp_server->rtp_info.udp_send_socket);
        //     free(s_rtsp_server);
        //     return NULL;
        // }
    }

    s_rtsp_server->stop           = 0;
    s_rtsp_server->port           = port;
    s_rtsp_server->init_client    = init_rtsp_session;
    s_rtsp_server->release_client = cleanup_rtsp_session;
    
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
    pthread_attr_setstacksize(&attr, 1024);
    if(0 != pthread_create(&s_rtsp_server->thread_id, &attr, rtp_tcp_server_thread, (void*)s_rtsp_server))
    {
        DBOT_LOG_E("rtsp: failed to create rtsp server thread\n");
        free(s_rtsp_server);
        return NULL;
    }

    DBOT_LOG_I("rtsp server started\r\n");
    return s_rtsp_server;
}
