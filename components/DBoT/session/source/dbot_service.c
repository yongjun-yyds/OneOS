#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <unistd.h>
#include "dbot_log.h"
#include "dbot_errno.h"
#include "dbot_service.h"
#include "dbot_list.h"

#ifdef _WIN32
#include <winsock2.h>
#endif

static DBOT_SERVICE_CONTEXT_S *s_dbot_ctx = NULL;
static bool s_dbot_started = false;

int dbot_service_init(void)
{
    int ret = DBOT_ERROR;
    do
    {
        if(DBOT_OK != dbot_log_init(DEBUG))
        {
            DBOT_LOG_E("dbot log init failed\n");
            break;
        }

        if(NULL == s_dbot_ctx)
        {
            s_dbot_ctx = calloc(1, sizeof(DBOT_SERVICE_CONTEXT_S));
            if(NULL == s_dbot_ctx)
            {
                DBOT_LOG_E("dbot malloc for service failed\r\n");
                break;
            }
        }

#ifdef _WIN32
        WSADATA wsaData;
        if(0 != WSAStartup(MAKEWORD(2, 2), &wsaData))
        {
            DBOT_LOG_E("dbot windows socket init failed\r\n");
            break;
        }
#endif

        if(DBOT_OK != init_local_device_info())
        {
            // DBOT_LOG_E("dbot device info init failed\r\n");
            break;
        }

        s_dbot_ctx->profile = get_local_device_info();
        
        if(DBOT_OK != dbot_ddp_service_init(s_dbot_ctx))
        {
            DBOT_LOG_E("dbot ddp service init failed\r\n");
            break;
        }

#ifdef DBOT_PUBLISH
        if(DBOT_OK != dbot_publish_init(s_dbot_ctx))
        {
            DBOT_LOG_E("dbot publish module init failed\r\n");
            break;
        }
#endif

#ifdef DBOT_DISCOVERY
        if(DBOT_OK != dbot_discover_init(s_dbot_ctx))
        {
            DBOT_LOG_E("dbot discover module init failed\r\n");
            break;
        }

        if(DBOT_OK != dbot_dtcp_server_init(s_dbot_ctx))
        {
            DBOT_LOG_E("dbot dtcp server init failed\r\n");
            break;
        }
#endif

        ret = DBOT_OK;
    } while (0);
    
    if(DBOT_OK != ret)
    {
        // TODO:释放内存
        FREE_POINTER(s_dbot_ctx);
    }

    return ret;
}

int dbot_service_start(void)
{
    if(!s_dbot_started)
    {
        dbot_ddp_service_start();

#ifdef DBOT_PUBLISH
        dbot_publish_start();
#endif

#ifdef DBOT_DISCOVERY
        dbot_discovery_start();
        dbot_dtcp_server_start();
#endif

        s_dbot_started = true;
    }

    return DBOT_OK;
}

// int dbot_service_stop(void)
// {
// #ifdef _WIN32
//     if(0 != WSACleanup())
//     {
//         DBOT_LOG_E("dbot windows socket stop failed\r\n");
//     }
// #endif
//     return DBOT_OK;
// }

bool dbot_service_started(void)
{
    return s_dbot_started;
}

DBOT_SERVICE_CONTEXT_S *dbot_service_get_ctx(void)
{
    return s_dbot_ctx;
}

int dbot_service_send_broadcast(char *res_name, char *buf, int len)
{
    return dbot_ddp_service_send_request(NULL, buf, len);
}

int dbot_service_add_resource(const char *res_name, DDP_REQUEST_PROC func)
{
    return dbot_ddp_service_add_resource(res_name, func);
}

#ifdef DBOT_PUBLISH
int dbot_service_add_service(uint32_t service,  uint32_t attr_num, ATTR_INFO_S *attrs)
{
    return dbot_add_service(service, attr_num, attrs);
}

int dbot_service_publish_service(void)
{
    return dbot_publish_service(get_local_device_info()->broadcast_ip);
}
#endif

#ifdef DBOT_DISCOVERY
int dbot_service_discovery_service(void)
{
    return dbot_discovery_service();
}

int dbot_service_call_service(int device_id, uint32_t service, char *func, char *param, DTCP_CALL_RESULT_CB cb)
{
    return dbot_dtcp_server_call_service(s_dbot_ctx, device_id, service, func, param, cb);
}

int dbot_service_create_session(int device_id)
{
    return dbot_dtcp_server_create_session(s_dbot_ctx, device_id);
}

int dbot_service_remove_session(int device_id)
{
    return dbot_dtcp_server_remove_session(s_dbot_ctx, device_id);
}
#endif

DTP_SERVICE_CONTEXT_S *dbot_service_init_dtp(DTP_SERVICE_TYPE_E type, char *ip, uint16_t port)
{
    return dbot_dtp_service_init(s_dbot_ctx, type, ip, port);
}

int dbot_service_destory_dtp(DTP_SERVICE_CONTEXT_S *ctx)
{
    return dbot_dtp_service_destory(ctx);
}
