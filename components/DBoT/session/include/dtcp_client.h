#ifndef __DTCP_CLIENT_H__
#define __DTCP_CLIENT_H__

#include <pthread.h>
#include "dbot_common.h"
#include "dbot_tcp.h"

typedef struct dtcp_client_context_s
{
    pthread_t pid;
    int device_id;
    int quit_flag;
    int data_len;
    int last_hb_ts;
    int last_hb_reply_ts;
    char data_buf[MAX_SOCKET_RECV_BUFFER_LEN];
    TCP_CLIENT_S client;
    void *dbot_ctx;
}DTCP_CLIENT_CONTEXT_S;

int dbot_dtcp_client_init(void *dbot_ctx, int device_id, char *dst_ip, uint16_t dst_port);
int dbot_dtcp_client_start(void);
int dbot_dtcp_client_stop(void);

#endif /* __DTCP_CLIENT_H__ */
