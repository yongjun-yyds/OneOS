#ifndef __RTSP_CLIENT_H__
#define __RTSP_CLIENT_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>
#include <unistd.h>
#include <pthread.h>

#ifdef __cplusplus
extern "C"
{
#endif

#if (defined __linux__) || (defined _WIN32) 
#ifndef debug
#define debug printf
#endif

#else
#include "board.h"

#ifndef debug
#define debug os_kprintf
#endif

#endif

#define RTSP_URL_MAX_LEN    200
#define IPV4_LEN            16
#define RTP_STREAM_MAX_LEN  1400

typedef void (*RTP_STREAM_OUT_FUNC)(void *buf, int len);

typedef enum _rtp_trans_mode
{
    RTP_TRANS_MODE_UDP = 0,
    RTP_TRANS_MODE_TCP
}RTP_TRANS_MODE_E;

typedef struct _rtsp_client_context
{
    char rtsp_url[RTSP_URL_MAX_LEN];
    char rtsp_ip[IPV4_LEN];
    int rtsp_ready;
    int rtsp_client_fd;
    int rtp_udp_server_fd;
    unsigned short rtsp_port;
    unsigned short rtp_udp_server_port;
    RTP_TRANS_MODE_E rtp_mode;
    pthread_t rtsp_task;
    pthread_t rtp_task;
    RTP_STREAM_OUT_FUNC rtp_play_video_stream;
}RTSP_CLIENT_CONTEXT_S;

typedef enum _rtsp_conn_step
{
    RTSP_MSG_TYPE_OPTION = 0,
    RTSP_MSG_TYPE_DESCRIBE,
    RTSP_MSG_TYPE_SETUP,
    RTSP_MSG_TYPE_PLAY,
    RTSP_MSG_TYPE_TREARDOWN
}RTSP_MSG_TYPE_E;

RTSP_CLIENT_CONTEXT_S *rtsp_start_client(const char *rtsp_url, RTP_STREAM_OUT_FUNC cb, RTP_TRANS_MODE_E mode);

#ifdef __cplusplus
}
#endif

#endif
