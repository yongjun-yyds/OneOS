#ifndef __DDP_SERVICE_H__
#define __DDP_SERVICE_H__

#include <pthread.h>
#include "dbot_list.h"
#include "dbot_common.h"
#if defined __linux__
#include <netinet/in.h>
#elif defined _WIN32
#include <winsock2.h>
#else
#include "sys/socket.h"
#endif

#define DDP_SERVICE_PORT        56830
#define DDP_MAX_RES_NAME_LEN    20

typedef int (*DDP_REQUEST_PROC)(void *dbot_ctx, int req_len, char *req, char **resp);

typedef struct ddp_resource_t
{
    os_list_node_t node;
    char res_name[DDP_MAX_RES_NAME_LEN];
    DDP_REQUEST_PROC func;
}DDP_RESOURCE_T;

typedef struct ddp_service_context_t
{
    os_list_node_t res_list;
    pthread_mutex_t res_list_lock;
    int server_fd;
    uint16_t server_port;
    pthread_t server_pid;
    struct sockaddr_in src_addr;
    int data_len;
    char *data_buf;
    void *dbot_ctx;
}DDP_SERVICE_CONTEXT_S;

int dbot_ddp_service_init(void *dbot_ctx);
int dbot_ddp_service_add_resource(const char *res_name, DDP_REQUEST_PROC func);
int dbot_ddp_service_start(void);
int dbot_ddp_service_send_request(char *dst_ip, char *send_buf, int buf_len);

#endif
