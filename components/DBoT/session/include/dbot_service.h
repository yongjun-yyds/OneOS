#ifndef __DBOT_SERVICE_H__
#define __DBOT_SERVICE_H__

#include <pthread.h>
#include "dbot_common.h"
#include "ddp_service.h"
#include "dtcp_server.h"
#include "dtcp_client.h"
#include "dtp_service.h"
#include "publish_service.h"
#include "discovery_service.h"

#ifdef __cplusplus
extern "C"
{
#endif

typedef struct dbot_service_context_s
{
    DDP_SERVICE_CONTEXT_S *ddp;
    DTCP_CLIENT_CONTEXT_S *dtcp_client;
    DTCP_SERVER_CONTEXT_S *dtcp_server;
    PUBLISH_MODULE_S *publish;
    DISCOVERY_MODULE_S *discovery;
    device_info_local_t *profile;
}DBOT_SERVICE_CONTEXT_S;

int dbot_service_init(void);
int dbot_service_start(void);
int dbot_service_stop(void);
bool dbot_service_started(void);
DBOT_SERVICE_CONTEXT_S *dbot_service_get_ctx(void);
int dbot_service_send_broadcast(char *res_name, char *buf, int len);
int dbot_service_add_resource(const char *res_name, DDP_REQUEST_PROC func);
int dbot_service_add_service(uint32_t service,  uint32_t attr_num, ATTR_INFO_S *attrs);
int dbot_service_publish_service(void);
int dbot_service_discovery_service(void);
int dbot_service_call_service(int device_id, uint32_t service, char *func, char *param, DTCP_CALL_RESULT_CB cb);
int dbot_service_create_session(int device_id);
int dbot_service_remove_session(int device_id);
DTP_SERVICE_CONTEXT_S *dbot_service_init_dtp(DTP_SERVICE_TYPE_E type, char *ip, uint16_t port);
int dbot_service_destory_dtp(DTP_SERVICE_CONTEXT_S *ctx);
#ifdef __cplusplus
}
#endif

#endif /* __DBOT_SERVER_H__ */
