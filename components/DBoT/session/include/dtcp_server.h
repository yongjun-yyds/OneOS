#ifndef __DTCP_SERVER_H__
#define __DTCP_SERVER_H__

#include <pthread.h>
#include "dbot_common.h"
#include "dbot_tcp.h"
#include "dbot_list.h"

#if defined __linux__
#include <netinet/in.h>
#elif defined _WIN32
#include <winsock2.h>
#else
#include "sys/socket.h"
#endif

typedef int(*DTCP_CALL_RESULT_CB)(char *resp, int len);

typedef struct dtcp_client_s
{
    int fd;
    int connected;
    int hold_up;
    struct sockaddr_in client_addr;
}DTCP_CLIENT_S;

typedef struct dtcp_server_context_s
{
    pthread_t pid;
    int data_len;
    int max_fd;
    int client_num;
    int max_client_num;
    int quit_flag;
    char data_buf[MAX_SOCKET_RECV_BUFFER_LEN];
    void *dbot_ctx;
    TCP_SERVER_S server;
}DTCP_SERVER_CONTEXT_S;

int dbot_dtcp_server_init(void *dbot_ctx);
int dbot_dtcp_server_start(void);
int dbot_dtcp_server_create_session(void *dbot_ctx, int device_id);
int dbot_dtcp_server_remove_session(void *dbot_ctx, int device_id);
int dbot_dtcp_server_call_service(void *dbot_ctx, int device_id, uint32_t service, char *func, char *param, DTCP_CALL_RESULT_CB cb);
#endif
