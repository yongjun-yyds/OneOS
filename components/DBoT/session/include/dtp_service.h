#ifndef __DTP_SERVICE_H__
#define __DTP_SERVICE_H__

#include "dbot_common.h"
#include "dbot_udp.h"
#include "dbot_tcp.h"

typedef int(*DTP_SEND_FUNC)(void *ctx, uint8_t *data, int len);
typedef int(*DTP_RECV_FUNC)(void *ctx, uint8_t *data, int len);

typedef enum dtp_service_type_e
{
    DTP_SERVICE_UDP_CLIENT = 0,
    DTP_SERVICE_UDP_SERVER,
    DTP_SERVICE_TCP_CLIENT,
    DTP_SERVICE_TCP_SERVER
}DTP_SERVICE_TYPE_E;


typedef struct dtp_service_context_s
{
    DTP_SERVICE_TYPE_E type;
    union
    {
        UDP_CLIENT_S udp_client;
        UDP_SERVER_S udp_server;
        TCP_CLIENT_S tcp_client;
        TCP_SERVER_S tcp_server;
    }dtp_service;
    uint16_t port;
    DTP_SEND_FUNC dtp_send;
    DTP_RECV_FUNC dtp_recv;
    void *dbot_ctx;
}DTP_SERVICE_CONTEXT_S;

DTP_SERVICE_CONTEXT_S *dbot_dtp_service_init(void *dbot_ctx, DTP_SERVICE_TYPE_E type, char *ip, uint16_t port);
int dbot_dtp_service_destory(DTP_SERVICE_CONTEXT_S *ctx);

#endif/* __DTP_SERVICE_H__ */
