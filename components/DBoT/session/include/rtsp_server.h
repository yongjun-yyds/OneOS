#ifndef __RTSP_H__
#define __RTSP_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <pthread.h>
#include <unistd.h>
#include <stdbool.h>

#ifdef _WIN32
#include <winsock2.h>
#else
#include <netinet/in.h>
#endif

#include "dbot_list.h"

/* rtp packet macros */
#ifndef TCP_MSS
#define TCP_MSS 1460
#endif
#define RTP_PACKET_MAX_SIZE 1400

/* rtsp macros */
#ifdef RT_LWIP_TCP_PCB_NUM
#define MAX_CLIENT_COUNT RT_LWIP_TCP_PCB_NUM
#else
#define MAX_CLIENT_COUNT 10
#endif
#define REQUEST_READ_BUF_SIZE 256
#define RESPONSE_BUF_SIZE 1024
#define RTP_UDP_SEND_PORT 3056
#define RTSP_THREAD_PRIORITY (RT_THREAD_PRIORITY_MAX >> 1)
#define RTSP_THREAD_STACK_SIZE 1024
#define RTSP_THREAD_TIMESLICE 10
#define RTSP_URL_MAX_LEN    200

enum rtp_transport
{
    RTP_TRANSPORT_UDP = 0,
    RTP_TRANSPORT_TCP
};

enum
{
    RTSP_MSG_TIMEOUT = 0,
    RTSP_MSG_ERROR,
    RTSP_MSG_OPTIONS,
    RTSP_MSG_DESCRIBE,
    RTSP_MSG_SETUP,
    RTSP_MSG_PLAY,
    RTSP_MSG_TEARDOWN,
    RTSP_MSG_UNKNOWN
};

struct sdp_info
{
    char content[1024];
    int len;
};

struct rtp_packet
{
    unsigned char buf[RTP_PACKET_MAX_SIZE];
    int len;
    int header_len;
};

struct rtp_info
{
    unsigned short sequence_num;
    unsigned long ssrc;
    struct sdp_info sdp;
    enum rtp_transport transport;
    int udp_send_socket;
};

struct client_context
{
    struct rtsp_server_context *server;
    int fd;
    char url[RTSP_URL_MAX_LEN];
    struct sockaddr_in addr;
    int udp_send_port;
    int start_play;
    void *session;
    int (*process_func)(struct client_context *ctx);
    pthread_t thread_id;
    os_list_node_t link;
};

struct rtsp_server_context
{
    int stop;
    int sd;
    pthread_t thread_id;
    int port;
    void (*init_client)(struct client_context *ctx);
    void (*release_client)(struct client_context *ctx);
    struct rtp_info rtp_info;
    pthread_mutex_t client_list_lock;
    os_list_node_t client_list;
};

struct rtsp_session
{
    struct client_context *ctx;
    char read_buf[REQUEST_READ_BUF_SIZE];
    char response_buf[RESPONSE_BUF_SIZE];
    char cseq[10];
    unsigned long session_num;
};

struct rtsp_server_context *rtsp_start_server(enum rtp_transport transport, int port);
void rtp_push_data(void *data, int size, unsigned int height, unsigned int width, char *url);

#endif
