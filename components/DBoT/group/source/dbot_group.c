#include "dbot_group.h"
#include "nb_types.h"
#include "dbot_lock.h"
#include "dbot_json.h"
#include "dbot_errno.h"
#include "dbot_log.h"

device_group_t device_group[MAX_GROUP_NUM] = {0};
char device_group_status[MAX_GROUP_NUM] = {0};
extern dbot_mutex_t gs_device_list_lock;
extern device_info_t gs_device_list[DBOT_MAX_DEVICES_NUM];

int new_group(char *group_name, int group_id)
{
   int index = 0;
   for (index = 0; index < MAX_GROUP_NUM; index++)
   {
       if(device_group_status[index] == 0)
       {
           break;
       }
   }
   if(MAX_GROUP_NUM == index)
   {
       DBOT_LOG_W("Cannot add more group as the group pool is full");
       return RET_ERR;
   }

   device_group[index].group_id = group_id;
   device_group[index].ref = 0;
   strcpy(&(device_group[index].group_name), group_name);
   device_group_status[index] = 1;
   return RET_OK;
}

int delete_group(int group_id)
{
    int index = 0;
    for (index = 0; index < MAX_GROUP_NUM; index++)
    {
        if(group_id == device_group[index].group_id)
        {
            if(device_group[index].ref > 0)
            {
                DBOT_LOG_W("Group(id = %d) is binded by devices, cannot delete it.", device_group[index].group_id);
                return RET_ERR;
            }
            memset(&device_group[index], 0, sizeof(device_group_t));
            device_group_status[index] = 0;
            break;
        }
    }
    return RET_OK;
}

static int send_bind_group(char *target_ipaddr, int group_id)
{
    char para_name[2][MAX_PARAM_NAME_LEN]={0}, para_value[2][MAX_PARAM_VALUE_LEN]={0};
    char *req = NULL;
    sprintf(para_name[0], "%s", "group_id");
    sprintf(para_value[0], "%d", group_id);
    sprintf(para_name[1], "%s", "operation");
    sprintf(para_value[1], "%s", "bind");
    pack_common_request(para_name, para_value, 2, &req);
    dbot_send_request(target_ipaddr, DBOT_GROUP_SERVICE, req);
    free(req);
    return RET_OK;
}

int bind_device_group(int device_id, int group_id)
{
    int device_index = 0;
    char *target_ipaddr = NULL;
    dbot_mutex_lock(gs_device_list_lock);
    for (device_index = 0; device_index < DBOT_MAX_DEVICES_NUM; device_index++)
    {
        if(gs_device_list[device_index].device_info.device_id == device_id)
        {
            break;
        }
    }
    if (DBOT_MAX_DEVICES_NUM == device_index)
    {
        DBOT_LOG_W("Device(device_id = %d) has not been registered.", device_id);
        return RET_ERR;
    }
    gs_device_list[device_index].device_info.group_id = group_id;
    strcpy(target_ipaddr, gs_device_list[device_index].device_info.device_ip);
    dbot_mutex_unlock(gs_device_list_lock);
    send_bind_group(target_ipaddr, group_id);
    return RET_OK;
}

int get_device_group(int device_id)
{
    int group_id = 0;
    int index;
    dbot_mutex_lock(gs_device_list_lock);
    for(index = 0; index < DBOT_MAX_DEVICES_NUM; index++)
    {
        if(device_id == gs_device_list[index].device_info.device_id)
        {
            group_id = gs_device_list[index].device_info.group_id;
            break;
        }
    }
    dbot_mutex_unlock(gs_device_list_lock);
    if(!group_id)
    {
        DBOT_LOG_W("Cannot find group id for device(device_id = %d)", device_id);
    }

    return group_id;
}

static internal_bind_group(int group_id)
{
    device_info_local_t *local_device = get_local_device_info();
    local_device->group_id = group_id;
}

static void process_group_req_data(char *req_data)
{
    char para_value[2][MAX_PARAM_VALUE_LEN]={0};
    char para_name[2][MAX_PARAM_VALUE_LEN] = {"group_id", "operation"};
    int group_id;

    if(DBOT_OK == unpack_common_request(req_data, para_name, para_value, 2))
    {
        if(0 == strcmp("bind", para_value[1]))
        {
            group_id = atoi(para_value[0]);
        }
        else if(0 == strcmp("unbind", para_value[1]))
        {
            group_id = 0;
        }
        else
        {
            DBOT_LOG_E("Unsuported operation on group service: service_name = %s", para_value[1]); 
            return;
        }
        internal_bind_group(group_id);
    }

    return;
}

static void group_service_handler(coap_resource_t *r,
   coap_session_t *session,
   const coap_pdu_t *req/* request */,
   const coap_string_t *coap_str/* query string */,
   coap_pdu_t *response /* response */)
{
    size_t size = 0;
    const uint8_t *pre_req_data = NULL;
    char *req_data = NULL;

    /* coap的buffer是复用的，需要用'\0'截断后面可能出现的以前的无用数据 */
    coap_get_data(req, &size, &pre_req_data);
    req_data = calloc(1, size + 1);
    if (NULL == req_data)
        return;
    memcpy(req_data, pre_req_data, size);

    DBOT_LOG_D("data: %s, size is %d\n", req_data, size);
    process_group_req_data(req_data);
    free(req_data);
}

int dbot_add_group_service(service_handler_map_t *service_map_array, int *array_len)
{
    service_handler_map_t *group_service = malloc(sizeof(service_handler_map_t));
    group_service->service_name = DBOT_GROUP_SERVICE;
    group_service->service_handler = group_service_handler;
    group_service->method = COAP_REQUEST_POST;
    dbot_add_service(service_map_array, array_len, group_service);
    free(group_service);
    return DBOT_OK;
}