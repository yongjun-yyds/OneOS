#ifndef _DBOT_GROUP_H_
#define _DBOT_GROUP_H_

#include "dbot_common.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct
{
    char group_name[MAX_GROUP_NAME_LEN];
    int group_id;
    int ref;
}device_group_t;

extern int bind_device_group(int device_id, int group_id);
extern int unbind_device_group(int device_id);
extern int new_group(char *group_name, int group_id);
extern int delete_group(int group_id);
extern int get_device_group(int device_id);
extern int dbot_add_group_service(service_handler_map_t *service_handler_map, int *service_num);


#ifdef __cplusplus
}
#endif

#endif