#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <pthread.h>
#include <wlan_dev.h>

#include <os_stddef.h>
#include "oneos_config.h"
#include "shell.h"

struct os_wlan_device *s_wlan_dev = OS_NULL;

static os_wlan_security_t security_map_from_str(char *str)
{
    os_wlan_security_t security;

    if (strcmp("OPEN", str) == 0)
        security = OS_WLAN_SECURITY_OPEN;
    else if (strcmp("WPA_AES_PSK", str) == 0)
        security = OS_WLAN_SECURITY_WPA_AES_PSK;
    else if (strcmp("WPA2_AES_PSK", str) == 0)
        security = OS_WLAN_SECURITY_WPA2_AES_PSK;
    else if (strcmp("WPA2_MIXED_PSK", str) == 0)
        security = OS_WLAN_SECURITY_WPA2_MIXED_PSK;
    else
        security = OS_WLAN_SECURITY_WPA2_AES_PSK;

    return security;
}

void *conncet_thread(void *arg)
{
    int ret = -1;

    os_wlan_security_t security = security_map_from_str("null");
    while(1)
    {
        if(OS_SUCCESS == os_wlan_join(s_wlan_dev, WIFI_AP_SSID, WIFI_AP_PASSWD, security))
        {
            os_kprintf("connect to wifi %s succeed\r\n", WIFI_AP_SSID);
            break;
        }
        os_kprintf("connect to wifi %s failed\r\n", WIFI_AP_SSID);
        os_task_msleep(2 * 1000);
    }
		return NULL;
}

int connect_wifi(void)
{
    s_wlan_dev = (struct os_wlan_device *)os_device_find(DBOT_NETWORK_INTERFACE_NAME);
    OS_ASSERT(s_wlan_dev != NULL);

    pthread_t pid;
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
    pthread_attr_setstacksize(&attr, 1024);
    if(0 != pthread_create(&pid, &attr, conncet_thread, NULL))
    {
        os_kprintf("create wifi connect thread failed\r\n");
        return -1;
    }

    return 0;
}

OS_CMPOENT_INIT(connect_wifi, OS_INIT_SUBLEVEL_LOW);
