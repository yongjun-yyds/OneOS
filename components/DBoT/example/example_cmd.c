#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/stat.h>
#include "dbot_log.h"
#include "dbot_errno.h"
#include "dbot_service.h"

#include "pb_encode.h"
#include "pb_decode.h"
#include "dbot_nanopb.h"
#include "fileproto.pb.h"
#include "crc-ccitt.h"

void show_device_service(void)
{
    if(!dbot_service_started())
    {
        DBOT_LOG_D("dbot not started\r\n");
        return;
    }

    DBOT_SERVICE_CONTEXT_S *ctx       = dbot_service_get_ctx();
    DISCOVERY_MODULE_S     *discovery = ctx->discovery;
    pthread_mutex_lock(&discovery->device_list_lock);
    DBOT_LOG("============================================================================================\r\n");
    DBOT_LOG("total %d clients\r\n", discovery->device_num);
    DEVICE_NODE_T  *dev     = NULL;
    service_info_t *service = NULL;
    os_list_for_each_entry(dev, &discovery->device_list, DEVICE_NODE_T, node)
    {
        DBOT_LOG("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\r\n");
        DBOT_LOG("device id:%d\r\n", dev->device_info.device_id);
        DBOT_LOG("status:%s\r\n", (dev->online) ? "online" : "offline");
        DBOT_LOG("session:%s\r\n", (dev->client.connected) ? "created" : "not created");
        os_list_for_each_entry(service, &dev->device_info.services.service_list, service_info_t, node)
        {
            DBOT_LOG("service:%d\r\n", service->service_type);
            DBOT_LOG("service attrs:%s\r\n", service->attrs.attr);
        }
        DBOT_LOG("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\r\n");
    }
    DBOT_LOG("============================================================================================\r\n");
    pthread_mutex_unlock(&discovery->device_list_lock);
}

void show_device_service_simple(void)
{
    if(!dbot_service_started())
    {
        DBOT_LOG_D("dbot not started\r\n");
        return;
    }

    DBOT_SERVICE_CONTEXT_S *ctx       = dbot_service_get_ctx();
    DISCOVERY_MODULE_S     *discovery = ctx->discovery;
    pthread_mutex_lock(&discovery->device_list_lock);
    DBOT_LOG("===============================================\r\n");
    DBOT_LOG("total %d devices\r\n", discovery->device_num);
    DEVICE_NODE_T *dev = NULL;
    os_list_for_each_entry(dev, &discovery->device_list, DEVICE_NODE_T, node)
    {
        DBOT_LOG("+++++++++++++++++++++++++++++++++++++++++++++++\r\n");
        DBOT_LOG("device id:\t%d\r\n", dev->device_info.device_id);
        DBOT_LOG("ip addr:\t%s\r\n", dev->device_info.device_ip);
        DBOT_LOG("status:\t\t%s\r\n", (dev->online) ? "online" : "offline");
        DBOT_LOG("+++++++++++++++++++++++++++++++++++++++++++++++\r\n");
    }
    DBOT_LOG("===============================================\r\n");
    pthread_mutex_unlock(&discovery->device_list_lock);
}

int call_service_resp_func(char *resp, int resp_len)
{
    if (NULL != resp)
        DBOT_LOG_D("%s\r\n", resp);
    return DBOT_OK;
}
int dbot_call_service(int argc, char **argv)
{
    if (4 > argc)
    {
        DBOT_LOG_D("usage:dbot_call_service [device id] [service id] [attr func name] [attr func params]\r\n");
        return -1;
    }

    if(!dbot_service_started())
    {
        DBOT_LOG_D("dbot not started\r\n");
        return -1;
    }

    return dbot_service_call_service(atoi(argv[1]),
                                     atoi(argv[2]),
                                     argv[3],
                                     (5 == argc) ? argv[4] : NULL,
                                     call_service_resp_func);
}

int dbot_create_session(int argc, char **argv)
{
    if (2 > argc)
    {
        DBOT_LOG_D("usage:dbot_create_session [device id]\r\n");
        return -1;
    }

    if(!dbot_service_started())
    {
        DBOT_LOG_D("dbot not started\r\n");
        return -1;
    }

    return dbot_service_create_session(atoi(argv[1]));
}

int dbot_remove_session(int argc, char **argv)
{
    if (2 > argc)
    {
        DBOT_LOG_D("usage:dbot_remove_session [device id]\r\n");
        return -1;
    }

    if(!dbot_service_started())
    {
        DBOT_LOG_D("dbot not started\r\n");
        return -1;
    }

    return dbot_service_remove_session(atoi(argv[1]));
}

/************************************显示目录***************************************************/
static DTP_SERVICE_CONTEXT_S *gs_show_directory_ctx       = NULL;
static pthread_t              gs_show_directory_pid       = 0;
static char                   gs_show_directory_path[128] = {0};

void free_show_directory_resource(void)
{
    if (NULL != gs_show_directory_ctx)
    {
        dbot_service_destory_dtp(gs_show_directory_ctx);
        gs_show_directory_ctx = NULL;
    }
}

bool client_list_files_response_callback(pb_istream_t *istream, const pb_field_iter_t *field)
{
    PB_UNUSED(field);
    FileInfo fileinfo = {0};

    if (!pb_decode(istream, FileInfo_fields, &fileinfo))
        return false;

#ifdef _WIN32
    printf("%-20s    ", fileinfo.name);
    printf("%-25lu\r\n", fileinfo.size);
#else
    os_kprintf("%-20s    ", fileinfo.name);
    if (fileinfo.type == DT_DIR)
    {
        os_kprintf("%-25s\r\n", "<DIR>");
    }
    else
    {
        os_kprintf("%-25lu\r\n", fileinfo.size);
    }
#endif

    return true;
}

static void *show_directory_thread_func(void *param)
{
    int32_t result = 0;

    /* Construct and send the request to server */
    {
        ListFilesRequest list_files_request = {0};

        /* If path not given, the server will list the root directory */
        if (strlen(gs_show_directory_path) == 0)
        {
            list_files_request.has_path = false;
        }
        else
        {
            list_files_request.has_path = true;
            if (strlen(gs_show_directory_path) + 1 > sizeof(list_files_request.path))
            {
                DBOT_LOG_E("Too long path \r\n");
                goto __err;
            }

            strcpy(list_files_request.path, gs_show_directory_path);
        }

        result = dbot_nanopb_msg_output((void *)gs_show_directory_ctx, ListFilesRequest_fields, &list_files_request);
        if (result != DBOT_OK)
        {
            DBOT_LOG_E("dbot nanopb msg output failed\r\n");
            goto __err;
        }
    }

    /* Read response from server */
    {
        ListFilesResponse list_files_response = {0};

        result = dbot_nanopb_msg_input((void *)gs_show_directory_ctx, ListFilesResponse_fields, &list_files_response);
        if (result != DBOT_OK)
        {
            DBOT_LOG_E("dbot nanopb msg input failed\r\n");
            goto __err;
        }

        /* If directory was not found on server side, path_error == true */
        if (list_files_response.path_error)
        {
            DBOT_LOG_E("Directory was not found on server side\r\n");
            goto __err;
        }
    }

__err:
    free_show_directory_resource();
    gs_show_directory_pid = 0;
    DBOT_LOG_D("show_directory task quit\r\n");

    return NULL;
}

static int show_directory_resp_func(char *resp, int resp_len)
{
    int    ret  = DBOT_ERROR;
    cJSON *root = NULL;
    cJSON *ip   = NULL;
    cJSON *port = NULL;

    if (NULL == resp)
    {
        return -1;
    }

    DBOT_LOG_D("recv resp %s\r\n", resp);

    if (NULL != gs_show_directory_ctx)
    {
        DBOT_LOG_D("show directory task already exists\r\n");
        return -1;
    }

    do
    {
        root = cJSON_Parse(resp);
        BREAK_IF(NULL == root);
        ip   = cJSON_GetObjectItem(root, "ip");
        port = cJSON_GetObjectItem(root, "port");
        BREAK_IF(NULL == ip || NULL == port);

        gs_show_directory_ctx = dbot_service_init_dtp(DTP_SERVICE_TCP_CLIENT, ip->valuestring, port->valueint);
        BREAK_IF(NULL == gs_show_directory_ctx);

        pthread_attr_t attr;
        pthread_attr_init(&attr);
        pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
        pthread_attr_setstacksize(&attr, 4096);
        BREAK_IF(0 != pthread_create(&gs_show_directory_pid, &attr, show_directory_thread_func, NULL));

        ret = DBOT_OK;
        DBOT_LOG_E("show directory task started\r\n");
    } while (0);

    DELETE_CJSON(root);
    if (DBOT_OK != ret)
    {
        free_show_directory_resource();
    }
    return ret;
}

int dbot_show_directory(int argc, char **argv)
{
    if (3 > argc)
    {
        DBOT_LOG_D("usage:dbot_show_directory [device id] [path]\r\n");
        return -1;
    }

    if(!dbot_service_started())
    {
        DBOT_LOG_D("dbot not started\r\n");
        return -1;
    }

    char param[138] = {0};
    memset(gs_show_directory_path, 0, sizeof(gs_show_directory_path));
    strncpy(gs_show_directory_path, argv[2], sizeof(gs_show_directory_path));
    snprintf(param, sizeof(param), "{\"path\":\"%s\"}", gs_show_directory_path);

    return dbot_service_call_service(atoi(argv[1]),
                                     CAPABILITY_TYPE_FILESYSTEM,
                                     "show_directory",
                                     param,
                                     show_directory_resp_func);
}
/************************************显示目录***************************************************/

/************************************写文件***************************************************/
// 传输文件应用，调用对端的文件系统能力
static DTP_SERVICE_CONTEXT_S *gs_write_file_ctx      = NULL;
static pthread_t              gs_write_file_pid      = 0;
static FILE                  *gs_write_file_fp       = NULL;
static char                   gs_write_file_name[100] = {0};
static int                    gs_show_image = 0;
static int                    gs_show_image_id = 0;

void free_write_file_resource(void)
{
    if (NULL != gs_write_file_ctx)
    {
        dbot_service_destory_dtp(gs_write_file_ctx);
        gs_write_file_ctx = NULL;
    }
    if (NULL != gs_write_file_fp)
    {
        fclose(gs_write_file_fp);
        gs_write_file_fp = NULL;
    }
}

static bool file_flow_encode_repeated_payload_callback(pb_ostream_t *ostream, const pb_field_t *field, void * const *arg)
{
    pb_byte_t payload[1460] = {0};
    uint32_t  read_len      = 0;
    FILE     *file_fp       = (FILE *)(*arg);

    if (ostream->callback != NULL)
    {
        dbot_msleep(3000);
    }

    while ((read_len = fread(payload, 1, sizeof(payload), file_fp)) > 0)
    {
        if (!pb_encode_tag_for_field(ostream, field))
            return false;

        if (!pb_encode_string(ostream, payload, read_len))
            return false;

        if (ostream->callback == NULL) /* callback NULL is for sizing */
        {
            continue;
        }
        dbot_msleep(40); /* waiting for opposite write in flash */
    }
    rewind(file_fp); /* this callback will come in twice, so need rewind */

    return true;
}

static void *write_file_thread_func(void *param)
{
    FileFlow    file_flow = {0};
    struct stat stat_buf  = {0};
    int32_t     result    = 0;

    file_flow.has_header = true;
    if (file_flow.has_header == true)
    {
        strncpy(file_flow.header.name, gs_write_file_name, sizeof(file_flow.header.name));
        stat(gs_write_file_name, &stat_buf);
#ifdef _WIN32
        file_flow.header.type = 0;
#else
        if (S_ISREG(stat_buf.st_mode))
            file_flow.header.type = DT_REG;
        else if (S_ISDIR(stat_buf.st_mode))
            file_flow.header.type = DT_DIR;
        else if (S_ISCHR(stat_buf.st_mode))
            file_flow.header.type = DT_CHR;
        else if (S_ISBLK(stat_buf.st_mode))
            file_flow.header.type = DT_BLK;
        else if (S_ISFIFO(stat_buf.st_mode))
            file_flow.header.type = DT_FIFO;
        else if (S_ISLNK(stat_buf.st_mode))
            file_flow.header.type = DT_LNK;
        else if (S_ISSOCK(stat_buf.st_mode))
            file_flow.header.type = DT_SOCK;
        else
            file_flow.header.type = DT_UNKNOWN;
#endif
        file_flow.header.size     = stat_buf.st_size;
        file_flow.header.has_sha1 = false;
    }
    else
    {
        /* if file_flow.has_header is false, file_flow.header will be ignore */
    }
    file_flow.payload.funcs.encode = &file_flow_encode_repeated_payload_callback;
    file_flow.payload.arg = gs_write_file_fp;

    result = dbot_nanopb_msg_output((void *)gs_write_file_ctx, FileFlow_fields, &file_flow);
    if (result != DBOT_OK)
    {
        DBOT_LOG_E("dbot nanopb msg output failed\r\n");
    }else
    {
        if(gs_show_image)
        {
            // 调用对端屏幕能力显示图片
            char params[50] = {0};
            snprintf(params, sizeof(params), "{\"filename\":\"%s\"}", gs_write_file_name);
            dbot_service_call_service(gs_show_image_id, CAPABILITY_TYPE_LCD, "lcd_show_image", params, NULL);
        }
    }

    free_write_file_resource();
    gs_write_file_pid = 0;
    DBOT_LOG_D("write file task quit\r\n");

    return NULL;
}

static int write_file_resp_func(char *resp, int resp_len)
{
    int    ret  = DBOT_ERROR;
    cJSON *root = NULL;
    cJSON *ip   = NULL;
    cJSON *port = NULL;

    if (NULL == resp)
    {
        return -1;
    }

    DBOT_LOG_D("recv resp %s\r\n", resp);

    if (NULL != gs_write_file_ctx)
    {
        DBOT_LOG_D("tranport file task already exists\r\n");
        return -1;
    }

    do
    {
        root = cJSON_Parse(resp);
        BREAK_IF(NULL == root);
        ip   = cJSON_GetObjectItem(root, "ip");
        port = cJSON_GetObjectItem(root, "port");
        BREAK_IF(NULL == ip || NULL == port);

        gs_write_file_fp = fopen(gs_write_file_name, "rb");
        BREAK_IF(NULL == gs_write_file_fp);

        gs_write_file_ctx = dbot_service_init_dtp(DTP_SERVICE_TCP_CLIENT, ip->valuestring, port->valueint);
        BREAK_IF(NULL == gs_write_file_ctx);

        pthread_attr_t attr;
        pthread_attr_init(&attr);
        pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
        pthread_attr_setstacksize(&attr, 5120);
        BREAK_IF(0 != pthread_create(&gs_write_file_pid, &attr, write_file_thread_func, NULL));

        ret = DBOT_OK;
        DBOT_LOG_E("write file task started\r\n");
    } while (0);

    DELETE_CJSON(root);
    if (DBOT_OK != ret)
    {
        free_write_file_resource();
    }
    return ret;
}

int dbot_write_file(int argc, char **argv)
{
    if (3 > argc)
    {
        DBOT_LOG_D("usage:dbot_write_file [device id] [file name]\r\n");
        return -1;
    }

    if(!dbot_service_started())
    {
        DBOT_LOG_D("dbot not started\r\n");
        return -1;
    }

    gs_show_image_id = atoi(argv[1]);
    if(4 <= argc)
    {
        gs_show_image = atoi(argv[3]);
    }
    else
    {
        gs_show_image = 0;
    }

    char param[100] = {0};
    memset(gs_write_file_name, 0, sizeof(gs_write_file_name));
    strncpy(gs_write_file_name, argv[2], sizeof(gs_write_file_name));

    char *p = gs_write_file_name;
    char *pos = NULL;
    while(1)
    {
        p = strstr(p, "/");
        if(NULL != p)
        {
            p += 1;
            pos = p;
        }
        else
        {
            break;
        }
    }
    snprintf(param, sizeof(param), "{\"file_name\":\"%s\"}", pos);

    return dbot_service_call_service(atoi(argv[1]),
                                     CAPABILITY_TYPE_FILESYSTEM,
                                     "write_file",
                                     param,
                                     write_file_resp_func);
}

/************************************写文件**************************************************/








/************************************读文件***************************************************/

#define FILESYSTEM_THREAD_STACK_SIZE 5120
#define FILE_NAME_MAX_LEN    128
#define FILE_FLOW_PACKAGE_FIX_SIZE    1460
static char gs_read_file_name[FILE_NAME_MAX_LEN] = {0};

typedef struct file_package_read_node
{
    os_list_node_t node;
    uint8_t *ptr;
    uint32_t size;
    uint32_t offset;
    uint32_t seq;
}file_package_read_node_t;

typedef struct file_flow_read_cache
{
    uint8_t  file_name[FILE_NAME_MAX_LEN];
    uint8_t  file_type;
    uint32_t file_expect_size;
    uint32_t file_actual_size;
    uint8_t  sha1[20];
    os_list_node_t file_package_list;
}file_flow_read_cache_t;

static pthread_t              gs_read_file_pid     = 0;
static DTP_SERVICE_CONTEXT_S *gs_read_file_dtp_ctx = NULL;
static FILE                  *gs_read_file_fp      = NULL;
static uint32_t               gs_recv_file_size     = 0;
static uint32_t               gs_sequence             = 0;
static file_flow_read_cache_t      gs_file_flow_read_cache   = {0};
static pthread_mutex_t        file_flow_cache_list_lock;
static pthread_t              gs_read_file_flow_cache_pid  = 0;

static void file_flow_cache_lock(void)
{
    pthread_mutex_lock(&file_flow_cache_list_lock);
}

static inline void file_flow_cache_unlock(void)
{
    pthread_mutex_unlock(&file_flow_cache_list_lock);
}

void free_read_file_task_resource(void)
{
    if (NULL != gs_read_file_dtp_ctx)
    {
        dbot_service_destory_dtp(gs_read_file_dtp_ctx);
        gs_read_file_dtp_ctx = NULL;
    }
}

static bool file_flow_decode_repeated_payload_callback(pb_istream_t *istream, const pb_field_t *field, void **arg)
{
   
    file_package_read_node_t *file_cache_pkg_node = NULL;
    uint8_t *payload   = NULL;
    size_t len         = istream->bytes_left;


    payload = calloc(1, FILE_FLOW_PACKAGE_FIX_SIZE);
    if (NULL == payload)
    {
        DBOT_LOG_E("file flow package payload calloc failed\r\n");
        return false;
    }

    if (len > FILE_FLOW_PACKAGE_FIX_SIZE)
    {
        free(payload);
        DBOT_LOG_E("recv file flow package too large\r\n");
        return false;
    }

    if(!pb_read(istream, payload, len))
    {
        free(payload);
        DBOT_LOG_E("read istream payload failed\r\n");
        return false;
    }

    file_cache_pkg_node = calloc(1, sizeof(file_package_read_node_t));
    if (NULL == file_cache_pkg_node)
    {
        free(payload);
        DBOT_LOG_E("file flow cache node calloc failed\r\n");
        return false;
    }

    file_cache_pkg_node->ptr = payload;
    file_cache_pkg_node->size = len;
    file_cache_pkg_node->offset = gs_recv_file_size;
    file_cache_pkg_node->seq = gs_sequence++;

    os_list_init(&file_cache_pkg_node->node);
    file_flow_cache_lock();
    os_list_add_tail(&gs_file_flow_read_cache.file_package_list, &file_cache_pkg_node->node);
    file_flow_cache_unlock();

    gs_recv_file_size += len;

    return true;
}

static void *read_file_flow_cache_thread_func(void *param)
{
    file_package_read_node_t *file_cache_pkg_node_tmp = NULL;
    uint32_t total_read_len = (uint32_t)-1;
    uint32_t read_len  = 0;

    pthread_mutex_init(&file_flow_cache_list_lock, NULL);
    memset(&gs_file_flow_read_cache, 0, sizeof(file_flow_read_cache_t));
    os_list_init(&gs_file_flow_read_cache.file_package_list);

    while(total_read_len != gs_file_flow_read_cache.file_actual_size)
    {
        if (!os_list_empty(&gs_file_flow_read_cache.file_package_list))
        {
            file_flow_cache_lock();
            file_cache_pkg_node_tmp = os_list_first_entry_or_null(&gs_file_flow_read_cache.file_package_list, file_package_read_node_t, node);
            os_list_del(&file_cache_pkg_node_tmp->node);
            file_flow_cache_unlock();
            if (file_cache_pkg_node_tmp != NULL)
            {
                if (file_cache_pkg_node_tmp->ptr != NULL)
                {
                    read_len = fwrite(file_cache_pkg_node_tmp->ptr, 1, file_cache_pkg_node_tmp->size, gs_read_file_fp); 
                    if (total_read_len == (uint32_t)-1)
                    {
                        total_read_len = 0;
                    }
										
                    total_read_len += read_len;
                    free(file_cache_pkg_node_tmp->ptr);
                }
                free(file_cache_pkg_node_tmp);
            }
        }
        else
        {
            dbot_msleep(10);
        }
    }

    pthread_mutex_destroy(&file_flow_cache_list_lock);
    if (NULL != gs_read_file_fp)
    {
        fclose(gs_read_file_fp);
        gs_read_file_fp = NULL;
    }
    DBOT_LOG_I("file flow cache task read file %d bytes\r\n", total_read_len);

    return NULL;
}

static void *read_file_thread(void *param)
{
    FileFlow file_flow      = {0};
    int32_t  result         = 0;

    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
    pthread_attr_setstacksize(&attr, 2048);
    if(0 != pthread_create(&gs_read_file_flow_cache_pid, &attr, read_file_flow_cache_thread_func, NULL))
    {
        DBOT_LOG_E("dbot creat file flow cache thread failed\r\n");
        goto __err;
    }

    file_flow.payload.funcs.decode = &file_flow_decode_repeated_payload_callback;
    file_flow.payload.arg = gs_read_file_fp;
    result = dbot_nanopb_msg_input((void *)gs_read_file_dtp_ctx, FileFlow_fields, &file_flow);
    if (result != DBOT_OK)
    {
        DBOT_LOG_E("dbot nanopb msg input failed\r\n");
    }

    if (file_flow.has_header == true)
    {
        strncpy((char *)gs_file_flow_read_cache.file_name, file_flow.header.name, sizeof(gs_file_flow_read_cache.file_name));
        gs_file_flow_read_cache.file_type = file_flow.header.type;
        gs_file_flow_read_cache.file_expect_size = file_flow.header.size;
        if (gs_recv_file_size != 0)
        {
            gs_file_flow_read_cache.file_actual_size = gs_recv_file_size;
        }
        else
        {
            gs_file_flow_read_cache.file_actual_size = (uint32_t)-1;
        }
        if (file_flow.header.has_sha1 == true)
        {
            /* user can read file from filesystem and check sha1 */
            memcpy(gs_file_flow_read_cache.sha1, file_flow.header.sha1.bytes, file_flow.header.sha1.size);
        }
        DBOT_LOG_D("file_flow has head: file_name:%s, file_type:%d, file_expect_size:%d, file_actual_size:%d\r\n", gs_file_flow_read_cache.file_name, gs_file_flow_read_cache.file_type, gs_file_flow_read_cache.file_expect_size, gs_file_flow_read_cache.file_actual_size);
    }
    else
    {
        /* without head, error accur very early, read_file_flow_cache_thread also need return */
        gs_file_flow_read_cache.file_actual_size = (uint32_t)-1;
    }

__err:
    free_read_file_task_resource();
    gs_read_file_pid = 0;
    DBOT_LOG_D("recv file task quit, total received payload %d bytes\r\n", gs_recv_file_size);
    return NULL;
}

static int read_file_resp_func(char *resp, int resp_len)
{
    int    ret  = DBOT_ERROR;
    cJSON *root = NULL;
	  cJSON *ip   = NULL;
    cJSON *port = NULL;


    if (NULL == resp)
    {
        return -1;
    }

    DBOT_LOG_D("recv resp %s\r\n", resp);

    if (NULL != gs_read_file_dtp_ctx)
    {
        DBOT_LOG_D("tranport file task already exists\r\n");
        return -1;
    }

    do
    {   
			  root = cJSON_Parse(resp);
        BREAK_IF(NULL == root);
        ip   = cJSON_GetObjectItem(root, "ip");
        port = cJSON_GetObjectItem(root, "port");
        BREAK_IF(NULL == ip || NULL == port);
        
        gs_read_file_fp = fopen(gs_read_file_name, "wb+");
        BREAK_IF(NULL == gs_read_file_fp);

        gs_read_file_dtp_ctx = dbot_service_init_dtp(DTP_SERVICE_TCP_CLIENT, ip->valuestring, port->valueint);
        BREAK_IF(NULL == gs_read_file_dtp_ctx);

        pthread_attr_t attr;
        pthread_attr_init(&attr);
			  struct sched_param param;
        param.sched_priority = 20;
        pthread_attr_setschedpolicy(&attr, SCHED_FIFO);
        pthread_attr_setschedparam(&attr, &param);
        pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
        pthread_attr_setstacksize(&attr, 5120);
        BREAK_IF(0 != pthread_create(&gs_read_file_pid, &attr, read_file_thread, NULL));


        ret              = DBOT_OK;
        gs_recv_file_size = 0;
        gs_sequence       = 0;
        DBOT_LOG_E("read file task started\r\n");
    } while (0);

    DELETE_CJSON(root);
    if (DBOT_OK != ret)
    {
        free_read_file_task_resource();
    }
    return ret;
}




int dbot_read_file(int argc, char **argv)
{
    if (3 > argc)
    {
        DBOT_LOG_D("usage:dbot_read_file [device id] [file name]\r\n");
        return -1;
    }

    if(!dbot_service_started())
    {
        DBOT_LOG_D("dbot not started\r\n");
        return -1;
    }

    char param[100] = {0};
    memset(gs_read_file_name, 0, sizeof(gs_read_file_name));
    strncpy(gs_read_file_name, argv[2], sizeof(gs_read_file_name));

    char *p = gs_read_file_name;
    char *pos = NULL;
    while(1)
    {
        p = strstr(p, "/");
        if(NULL != p)
        {
            p += 1;
            pos = p;
        }
        else
        {
            break;
        }
    }
    snprintf(param, sizeof(param), "{\"file_name\":\"%s\"}", pos);

    return dbot_service_call_service(atoi(argv[1]),
                                     CAPABILITY_TYPE_FILESYSTEM,
                                     "read_file",
                                     param,
                                     read_file_resp_func);
}


/************************************读文件***************************************************/







/************************************音频流转**************************************************/
// 在节点A调用B的麦克风能力录音并在C节点播放，即A告知C调用B的麦克风能力
static int s_audio_dataflow_src_id = 0;
static int s_audio_dataflow_dst_id = 0;
#ifdef SPEAKER_EXAMPLE
extern int speaker_start(int len, char *req, char **resp);
extern int speaker_stop(int len, char *req, char **resp);
#endif

static int audio_dataflow_resp_func(char *resp, int resp_len)
{
    RETURN_IF_NULL(resp, DBOT_INVALID_PARAM);

    return dbot_service_call_service(s_audio_dataflow_src_id, CAPABILITY_TYPE_MICROPHONE, "recorder_start", resp, NULL);
}

int dbot_audio_dataflow(int argc, char **argv)
{
    if (2 > argc)
    {
        DBOT_LOG_D("usage:dbot_audio_dataflow [source device id] [target device id]\r\n");
        return -1;
    }

    if(!dbot_service_started())
    {
        DBOT_LOG_D("dbot not started\r\n");
        return -1;
    }

    s_audio_dataflow_src_id = atoi(argv[1]);
    s_audio_dataflow_dst_id = atoi(argv[2]);

    // 首先建立回话
    if (DBOT_OK != dbot_service_create_session(s_audio_dataflow_src_id))
    {
        DBOT_LOG_D("create session with %d failed\r\n", s_audio_dataflow_src_id);
        return -1;
    }

    // 仅具备喇叭能力设备可以流转音频数据到自身
#ifdef SPEAKER_EXAMPLE
    if (s_audio_dataflow_dst_id == get_device_id())
    {
        DBOT_LOG_D("call mic by self\r\n");
    }
    else
    {
#endif
        if (DBOT_OK != dbot_service_create_session(s_audio_dataflow_dst_id))
        {
            DBOT_LOG_D("create session with %d failed\r\n", s_audio_dataflow_dst_id);
            return -1;
        }
#ifdef SPEAKER_EXAMPLE
    }
#endif

    // 中间还应该有一部去麦克风查询录音采样率，编码规格，录音通道数信息，这里默认使用oneos audio驱动配置的信息

    // 调用C节点的喇叭能力，在C节点建立dtcp server，在回调中调用B节点的麦克风能力，参数为C节点返回的dtcp server信息
    int    ret  = -1;
    cJSON *root = cJSON_CreateObject();
    cJSON_AddNumberToObject(root, "samplerate", 44100);
    cJSON_AddNumberToObject(root, "channel", 2);
    cJSON_AddNumberToObject(root, "bits", 16);
    cJSON_AddNumberToObject(root, "volume", 80);
    char *str = cJSON_PrintUnformatted(root);
    if (NULL != str)
    {
#ifdef SPEAKER_EXAMPLE
        if (s_audio_dataflow_dst_id == get_device_id())
        {
            char *resp = NULL;
            ret        = speaker_start(strlen(str), str, &resp);
            audio_dataflow_resp_func(resp, strlen(resp));
            FREE_POINTER(resp);
        }
        else
        {
#endif
            ret = dbot_service_call_service(s_audio_dataflow_dst_id,
                                            CAPABILITY_TYPE_SPEAKER,
                                            "speaker_start",
                                            str,
                                            audio_dataflow_resp_func);
#ifdef SPEAKER_EXAMPLE
        }
#endif
        FREE_POINTER(str);
    }
    cJSON_Delete(root);

    return ret;
}

int dbot_audio_dataflow_stop(int argc, char **argv)
{
    if (2 > argc)
    {
        DBOT_LOG_D("usage:dbot_audio_dataflow_stop [source device id] [target device id]\r\n");
        return -1;
    }

    if(!dbot_service_started())
    {
        DBOT_LOG_D("dbot not started\r\n");
        return -1;
    }

    s_audio_dataflow_src_id = atoi(argv[1]);
    s_audio_dataflow_dst_id = atoi(argv[2]);

    // dst_id若为调用者自己，则为直接调用能力

    // 首先建立回话
    if (DBOT_OK != dbot_service_create_session(s_audio_dataflow_src_id))
    {
        DBOT_LOG_D("create session with %d failed\r\n", s_audio_dataflow_src_id);
        return -1;
    }
#ifdef SPEAKER_EXAMPLE
    if (s_audio_dataflow_dst_id == get_device_id())
    {
        DBOT_LOG_D("call mic by self\r\n");
    }
    else
    {
#endif
        if (DBOT_OK != dbot_service_create_session(s_audio_dataflow_dst_id))
        {
            DBOT_LOG_D("create session with %d failed\r\n", s_audio_dataflow_dst_id);
            return -1;
        }
#ifdef SPEAKER_EXAMPLE
    }
#endif

    // 然后停止B节点的麦克风能力调用任务以及C节点的喇叭能力调用任务
    dbot_service_call_service(s_audio_dataflow_src_id, CAPABILITY_TYPE_MICROPHONE, "recorder_stop", NULL, NULL);
#ifdef SPEAKER_EXAMPLE
    if (s_audio_dataflow_dst_id == get_device_id())
    {
        speaker_stop(0, NULL, NULL);
    }
    else
    {
#endif
        dbot_service_call_service(s_audio_dataflow_dst_id, CAPABILITY_TYPE_SPEAKER, "speaker_stop", NULL, NULL);
#ifdef SPEAKER_EXAMPLE
    }
#endif

    return 0;
}
/************************************音频流转**************************************************/

/************************************视频流转**************************************************/
// 在节点A调用B的摄像头能力并在C节点播放，即A告知C调用B的摄像头能力
static int s_video_dataflow_src_id = 0;
static int s_video_dataflow_dst_id = 0;
#ifdef LCD_VIDEO_EXAMPLE
extern int lcd_video_start(int len, char *req, char **resp);
extern int lcd_video_stop(int len, char *req, char **resp);
#endif

static int video_dataflow_resp_func(char *resp, int resp_len)
{
    RETURN_IF_NULL(resp, DBOT_INVALID_PARAM);

    return dbot_service_call_service(s_video_dataflow_src_id, CAPABILITY_TYPE_CAMERA, "camera_start", resp, NULL);
}

int dbot_video_dataflow(int argc, char **argv)
{
    if (2 > argc)
    {
        DBOT_LOG_D("usage:dbot_video_dataflow [source device id] [target device id]\r\n");
        return -1;
    }

    if(!dbot_service_started())
    {
        DBOT_LOG_D("dbot not started\r\n");
        return -1;
    }

    s_video_dataflow_src_id = atoi(argv[1]);
    s_video_dataflow_dst_id = atoi(argv[2]);

    // 首先建立回话
    if (DBOT_OK != dbot_service_create_session(s_video_dataflow_src_id))
    {
        DBOT_LOG_D("create session with %d failed\r\n", s_video_dataflow_src_id);
        return -1;
    }

    // 仅具备屏幕能力设备可以流转音频数据到自身
#ifdef LCD_VIDEO_EXAMPLE
    if (s_video_dataflow_dst_id == get_device_id())
    {
        DBOT_LOG_D("call camera by self\r\n");
    }
    else
    {
#endif
        if (DBOT_OK != dbot_service_create_session(s_video_dataflow_dst_id))
        {
            DBOT_LOG_D("create session with %d failed\r\n", s_video_dataflow_dst_id);
            return -1;
        }
#ifdef LCD_VIDEO_EXAMPLE
    }
#endif

    // 调用C节点的LCD能力，在C节点建立dtcp server，在回调中调用B节点的摄像头能力，参数为C节点返回的dtcp server信息
    int ret = -1;
#ifdef LCD_VIDEO_EXAMPLE
    if (s_video_dataflow_dst_id == get_device_id())
    {
        char *resp = NULL;
        ret        = lcd_video_start(0, NULL, &resp);
        video_dataflow_resp_func(resp, strlen(resp));
        FREE_POINTER(resp);
    }
    else
    {
#endif
        ret = dbot_service_call_service(s_video_dataflow_dst_id,
                                        CAPABILITY_TYPE_LCD,
                                        "lcd_video_start",
                                        NULL,
                                        video_dataflow_resp_func);
#ifdef LCD_VIDEO_EXAMPLE
    }
#endif
    return ret;
}

int dbot_video_dataflow_stop(int argc, char **argv)
{
    if (2 > argc)
    {
        DBOT_LOG_D("usage:dbot_video_dataflow_stop [source device id] [target device id]\r\n");
        return -1;
    }

    if(!dbot_service_started())
    {
        DBOT_LOG_D("dbot not started\r\n");
        return -1;
    }

    s_video_dataflow_src_id = atoi(argv[1]);
    s_video_dataflow_dst_id = atoi(argv[2]);

    // dst_id若为调用者自己，则为直接调用能力

    // 首先建立回话
    if (DBOT_OK != dbot_service_create_session(s_video_dataflow_src_id))
    {
        DBOT_LOG_D("create session with %d failed\r\n", s_video_dataflow_src_id);
        return -1;
    }
#ifdef LCD_VIDEO_EXAMPLE
    if (s_video_dataflow_dst_id == get_device_id())
    {
        DBOT_LOG_D("call camera by self\r\n");
    }
    else
    {
#endif
        if (DBOT_OK != dbot_service_create_session(s_video_dataflow_dst_id))
        {
            DBOT_LOG_D("create session with %d failed\r\n", s_video_dataflow_dst_id);
            return -1;
        }
#ifdef LCD_VIDEO_EXAMPLE
    }
#endif

    // 然后停止B节点的摄像头能力调用任务以及C节点的显示屏能力调用任务
    dbot_service_call_service(s_video_dataflow_src_id, CAPABILITY_TYPE_CAMERA, "camera_stop", NULL, NULL);
#ifdef LCD_VIDEO_EXAMPLE
    if (s_video_dataflow_dst_id == get_device_id())
    {
        lcd_video_stop(0, NULL, NULL);
    }
    else
    {
#endif
        dbot_service_call_service(s_video_dataflow_dst_id, CAPABILITY_TYPE_LCD, "lcd_video_stop", NULL, NULL);
#ifdef LCD_VIDEO_EXAMPLE
    }
#endif
    return 0;
}
/************************************视频流转**************************************************/

/**********************************屏幕调摄像头*************************************************/
#ifdef LCD_VIDEO_EXAMPLE
int dbot_call_camera(int argc, char **argv)
{
    if (2 > argc)
    {
        DBOT_LOG_D("usage:dbot_call_camera [source device id]\r\n");
        return -1;
    }

    char id[20] = {0};
    snprintf(id, sizeof(id), "%d", get_device_id());
    char *cmd[3];
    cmd[1] = argv[1];
    cmd[2] = id;
    return dbot_video_dataflow(3, cmd);
}

int dbot_call_camera_stop(int argc, char **argv)
{
    if (2 > argc)
    {
        DBOT_LOG_D("usage:dbot_call_camera_stop [source device id]\r\n");
        return -1;
    }

    char id[20] = {0};
    snprintf(id, sizeof(id), "%d", get_device_id());
    char *cmd[3];
    cmd[1] = argv[1];
    cmd[2] = id;
    return dbot_video_dataflow_stop(3, cmd);
}
#endif
/**********************************屏幕调摄像头*************************************************/

/**********************************喇叭调麦克风*************************************************/
#ifdef SPEAKER_EXAMPLE
int dbot_call_mic(int argc, char **argv)
{
    if (2 > argc)
    {
        DBOT_LOG_D("usage:dbot_call_mic [source device id]\r\n");
        return -1;
    }

    char id[20] = {0};
    snprintf(id, sizeof(id), "%d", get_device_id());
    char *cmd[3];
    cmd[1] = argv[1];
    cmd[2] = id;
    return dbot_audio_dataflow(3, cmd);
}

int dbot_call_mic_stop(int argc, char **argv)
{
    if (2 > argc)
    {
        DBOT_LOG_D("usage:dbot_call_mic_stop [source device id]\r\n");
        return -1;
    }

    char id[20] = {0};
    snprintf(id, sizeof(id), "%d", get_device_id());
    char *cmd[3];
    cmd[1] = argv[1];
    cmd[2] = id;
    return dbot_audio_dataflow_stop(3, cmd);
}
#endif
/**********************************喇叭调麦克风*************************************************/

/***********************************屏幕截屏*************************************************/
#ifdef LCD_VIDEO_EXAMPLE
// 调用本机的屏幕截屏，截取的jpeg图片保存到sd卡
extern int lcd_snapshot(int len, char *req, char **resp);
int        dbot_lcd_snapshot(int argc, char **argv)
{
    return lcd_snapshot(0, NULL, NULL);
}
#endif
/***********************************屏幕截屏*************************************************/

/***********************************图片流转*************************************************/
int dbot_image_dataflow(int argc, char **argv)
{
    if (4 > argc)
    {
        DBOT_LOG_D("usage:dbot_image_dataflow [dst device id] [image filename] [show]\r\n");
        return -1;
    }

    if(!dbot_service_started())
    {
        DBOT_LOG_D("dbot not started\r\n");
        return -1;
    }

    return dbot_write_file(argc, argv);
}
/***********************************图片流转*************************************************/

#if !defined __linux__ && !defined _WIN32
#include "shell.h"
SH_CMD_EXPORT(hgn_show_device, show_device_service_simple, "show all devices");
SH_CMD_EXPORT(dbot_show_service, show_device_service, "dbot_show_service");
SH_CMD_EXPORT(dbot_call_service, dbot_call_service, "dbot_call_service");
SH_CMD_EXPORT(dbot_create_session, dbot_create_session, "dbot_create_session");
SH_CMD_EXPORT(dbot_remove_session, dbot_remove_session, "dbot_remove_session");
SH_CMD_EXPORT(dbot_discovery_service, dbot_service_discovery_service, "dbot_discovery_service");
SH_CMD_EXPORT(dbot_show_directory, dbot_show_directory, "dbot_show_directory");
SH_CMD_EXPORT(dbot_write_file, dbot_write_file, "dbot_write_file");
SH_CMD_EXPORT(dbot_read_file, dbot_read_file, "dbot_read_file");
SH_CMD_EXPORT(dbot_audio_dataflow, dbot_audio_dataflow, "dbot_audio_dataflow");
SH_CMD_EXPORT(dbot_audio_dataflow_stop, dbot_audio_dataflow_stop, "dbot_audio_dataflow_stop");
SH_CMD_EXPORT(dbot_video_dataflow, dbot_video_dataflow, "dbot_video_dataflow");
SH_CMD_EXPORT(dbot_video_dataflow_stop, dbot_video_dataflow_stop, "dbot_video_dataflow_stop");
SH_CMD_EXPORT(dbot_image_dataflow, dbot_image_dataflow, "dbot_image_dataflow");
#ifdef SPEAKER_EXAMPLE
SH_CMD_EXPORT(dbot_call_mic, dbot_call_mic, "dbot_call_mic");
SH_CMD_EXPORT(dbot_call_mic_stop, dbot_call_mic_stop, "dbot_call_mic_stop");
#endif
#ifdef LCD_VIDEO_EXAMPLE
SH_CMD_EXPORT(dbot_call_camera, dbot_call_camera, "dbot_call_camera");
SH_CMD_EXPORT(dbot_call_camera_stop, dbot_call_camera_stop, "dbot_call_camera_stop");
SH_CMD_EXPORT(dbot_lcd_snapshot, dbot_lcd_snapshot, "dbot_lcd_snapshot");
#endif
#endif
