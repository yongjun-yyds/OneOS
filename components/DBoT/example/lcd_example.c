#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include "cJSON.h"
#include "dbot_errno.h"
#include "dbot_log.h"
#include "publish_service.h"
#include "dbot_common.h"
#include "dbot_service.h"
#include "dtp_service.h"
#include "rtp.h"
#include <sys/socket.h>

#ifdef LCD_VIDEO_EXAMPLE
static DTP_SERVICE_CONTEXT_S *s_lcd_video_dtp_ctx = NULL;
static int s_lcd_video_quit_flag = 1;
static int s_lcd_video_task_started = 0;
static int s_lcd_snapshot = 0;

#define LCD_VIDEO_TASK_BEGIN     s_lcd_video_task_started = 1
#define LCD_VIDEO_TASK_END       s_lcd_video_task_started = 0
#define LCD_VIDEO_TASK_STARTED   s_lcd_video_task_started
#define LCD_VIDEO_TRANS_PROTOCOL  ATTR_TRANS_TCP

extern int jpeg_hardware_decode(uint8_t *pic_data, uint32_t pic_data_len);
int lcd_video_stop(int len, char *params, char **resp);

#define JPEG_BUF_SIZE   100 * 1024
uint8_t *jpeg_buf = NULL;
int jpeg_len = 0;
uint16_t jpeg_seq = 0;
uint32_t jpeg_ts = 0;

int save_snapshot_img(uint8_t *data, int len)
{
    int ret = 0;
    int offset = 0;
    char filename[50] = {0};
    snprintf(filename, sizeof(filename), "%d.jpeg", get_current_time());
    FILE *fp = fopen(filename, "wb");
    if (NULL != fp)
    {
        while(offset < len)
        {
            ret = fwrite(data + offset, 1, len - offset, fp);
            if(0 < ret)
            {
                offset += ret;
            }
        }
        DBOT_LOG_I("save snapshot to file %s\r\n", filename);
        fclose(fp);
        return DBOT_OK;
    }
    return DBOT_ERROR;
}

int collect_jpeg(uint8_t *data, int len)
{
    int ret = -1;

    if(NULL == jpeg_buf)
    {
        // jpeg_buf = os_dma_malloc_align(JPEG_BUF_SIZE, 32);
        jpeg_buf = malloc(JPEG_BUF_SIZE);
        if(NULL != jpeg_buf)
        {
            memset(jpeg_buf, 0, JPEG_BUF_SIZE);
        }
    }
    if(NULL == jpeg_buf)
    {
        return ret;
    }

    if(len < JPEG_MIN_SIZE)
    {
        return ret;
    }
    // rtp开头是0x80 0x1a或者0x80 0x9a
    if(data[0] != 0x80)
    {
        return ret;
    }
    if(data[1] != 0x1a && data[1] != 0x9a)
    {
        return ret;
    }

    rtp_hdr_t *rtp_hdr = (rtp_hdr_t*)data;
    jpeg_hdr_t *jpeg_hdr = (jpeg_hdr_t*)(data + RTP_HDR_SZ);
    int offset = jpeg_hdr->offset;
    offset = ntohl(offset);
    offset = offset >> 8;
    int seq = ntohs(rtp_hdr->seq);
    int ts = ntohl(rtp_hdr->ts);
    do
    {
        //偏移量为0，说明是一帧图像的第一帧RTP数据，清除之前的jpeg缓存
        if(0 == offset && 0 == rtp_hdr->m)
        {   //包头，清空jpeg缓存
            memset(jpeg_buf, 0, JPEG_BUF_SIZE);
            jpeg_len = 0;
            jpeg_seq = 0;
            jpeg_ts = 0;
        }
        if(0 != offset && 0 == rtp_hdr->m)
        {   //中间的包
            if(seq != jpeg_seq + 1)
            {   //包不连续，丢弃当前jpeg缓存
                memset(jpeg_buf, 0, JPEG_BUF_SIZE);
                jpeg_len = 0;
                jpeg_seq = 0;
                jpeg_ts = 0;
                // cout << "jpeg error 1" << endl;
                break;
            }
            if(ts != jpeg_ts)
            {   //丢包，错包，丢弃当前jpeg缓存
                memset(jpeg_buf, 0, JPEG_BUF_SIZE);
                jpeg_len = 0;
                jpeg_seq = 0;
                jpeg_ts = 0;
                // cout << "jpeg error 2" << endl;
                break;
            }
            if(offset != jpeg_len)
            {
                //丢包
                memset(jpeg_buf, 0, JPEG_BUF_SIZE);
                jpeg_len = 0;
                jpeg_seq = 0;
                jpeg_ts = 0;
                // cout << "jpeg error 3" << endl;
                break;
            }
        }

        if(jpeg_len + len - 20 > JPEG_BUF_SIZE)
        {
            memset(jpeg_buf, 0, JPEG_BUF_SIZE);
        }

        memcpy(jpeg_buf + jpeg_len, data + 20, len - 20);
        jpeg_len += (len - 20);
        jpeg_seq = seq;
        jpeg_ts = ts;

        if(1 == rtp_hdr->m)
        {   //包尾
            //校验jpeg 头和尾，并播放
            if(jpeg_buf[0] == 0xFF && jpeg_buf[1] == 0xD8 && jpeg_buf[jpeg_len -2] == 0xFF && jpeg_buf[jpeg_len -1] == 0xD9)
            {
                // os_kprintf("start jpeg hardware decode\r\n");
                if(s_lcd_snapshot)
                {
                    if(DBOT_OK == save_snapshot_img(jpeg_buf, jpeg_len))
                    {
                        s_lcd_snapshot = 0;
                    }
                }
                // os_kprintf("start of jpeg hardware decode[size:%d]\r\n", jpeg_len);
                jpeg_hardware_decode(jpeg_buf, jpeg_len);
                // os_kprintf("end of jpeg hardware decode\r\n");
                // dbot_msleep(10);
                ret = 0;
            }
        }
    }while(0);

    return ret;
}

void free_lcd_video_resource(void)
{
    if(NULL != s_lcd_video_dtp_ctx)
    {
        dbot_service_destory_dtp(s_lcd_video_dtp_ctx);
        s_lcd_video_dtp_ctx = NULL;
    }
}

static void *lcd_video_thread(void *arg)
{
    int len = 0;
    int max_frame_len = 1400;
    uint8_t *buff = malloc(max_frame_len);
    OS_ASSERT(buff);
    s_lcd_video_quit_flag = 0;
    while(1)
    {
        if(s_lcd_video_quit_flag)
        {
            break;
        }

        memset(buff, 0, max_frame_len);
        len = s_lcd_video_dtp_ctx->dtp_recv((void*)s_lcd_video_dtp_ctx, buff, max_frame_len);
        if(0 < len)
        {
            collect_jpeg(buff, len);
        }
        dbot_msleep(1);
    }
    FREE_POINTER(buff);
    free_lcd_video_resource();
    LCD_VIDEO_TASK_END;
    DBOT_LOG_D("lcd video thread quit\r\n");
    return NULL;
}

int lcd_video_start(int len, char *params, char **resp)
{
    int ret = DBOT_ERROR;
    cJSON *res = NULL;

    if(LCD_VIDEO_TASK_STARTED)
    {
        DBOT_LOG_E("lcd video already started, stop task now\r\n");
        lcd_video_stop(0, NULL, NULL);
        int cnt = 6;
        while(cnt > 0)
        {
            if(!LCD_VIDEO_TASK_STARTED)
            {
                break;
            }
            dbot_msleep(200);
            cnt --;
        }
    }

    if(LCD_VIDEO_TASK_STARTED)
    {
        DBOT_LOG_E("lcd video already started\r\n");
        return DBOT_ERROR;
    }

    do
    {
        DTP_SERVICE_TYPE_E dtp_type = (LCD_VIDEO_TRANS_PROTOCOL == ATTR_TRANS_TCP)?DTP_SERVICE_TCP_SERVER:DTP_SERVICE_UDP_SERVER;
        s_lcd_video_dtp_ctx = dbot_service_init_dtp(dtp_type, NULL, 0);
        BREAK_IF(NULL == s_lcd_video_dtp_ctx);
        if(NULL != resp)
        {
            res = cJSON_CreateObject();
            cJSON_AddStringToObject(res, "ip", get_local_device_info()->device_ip);
            cJSON_AddNumberToObject(res, "port", s_lcd_video_dtp_ctx->port);
            cJSON_AddNumberToObject(res, "protocol", 1);
            *resp = cJSON_PrintUnformatted(res);
            cJSON_Delete(res);
        }

        pthread_t pid;
        pthread_attr_t attr;
        pthread_attr_init(&attr);
        pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
        pthread_attr_setstacksize(&attr, 4096);
        struct sched_param sched;
        sched.sched_priority = 28;
        pthread_attr_setschedparam(&attr, &sched);
        BREAK_IF(0 !=  pthread_create(&pid, &attr, lcd_video_thread, NULL));

        ret = DBOT_OK;
        LCD_VIDEO_TASK_BEGIN;
        DBOT_LOG_E("lcd video task started\r\n");
    } while (0);
    
    if(DBOT_OK != ret)
    {
        free_lcd_video_resource();
    }
    return ret;
}

int lcd_video_stop(int len, char *params, char **resp)
{
    if(LCD_VIDEO_TASK_STARTED)
    {
        s_lcd_video_quit_flag = 1;
    }
    return DBOT_OK;
}

int lcd_snapshot(int len, char *params, char **resp)
{
    if(LCD_VIDEO_TASK_STARTED)
    {
        s_lcd_snapshot = 1;
    }
    return DBOT_OK;
}
#endif

#ifdef LCD_IMAGE_EXAMPLE
#include <lvgl.h>
int lcd_show_image(int len, char *params, char **resp)
{
    RETURN_IF_NULL(params, DBOT_INVALID_PARAM);

    cJSON *root = cJSON_Parse(params);
    if(NULL != root)
    {
        cJSON *file = cJSON_GetObjectItem(root, "filename");
        if(NULL != file)
        {
            lv_obj_t * img = lv_img_create(lv_scr_act());
            // lv_obj_set_pos(img, 30, 50);
            char filename[50] = {0};
            snprintf(filename, sizeof(filename), "A:%s", file->valuestring);
            lv_img_set_src(img, filename);

            // int len = 20 * 1024;
            // uint8_t *jpeg_data = calloc(len, 1);
            // int offset = 0;
            // int ret = -1;
            // if(NULL != jpeg_data)
            // {
            //     FILE *fp = fopen(file->valuestring, "rb");
            //     if (NULL != fp)
            //     {
            //         while(offset < len)
            //         {
            //             ret = fread(jpeg_data + offset, 1, len - offset, fp);
            //             if(0 < ret)
            //             {
            //                 offset += ret;
            //             }
            //             else
            //             {
            //                 break;
            //             }
            //         }
            //         fclose(fp);
            //         jpeg_hardware_decode(jpeg_data, offset);
            //     }
            //     FREE_POINTER(jpeg_data);
            // }
        }
        cJSON_Delete(root);
    }
	return 0;
}
#endif

static ATTR_INFO_S lcd_attrs[] =
{
#ifdef LCD_VIDEO_EXAMPLE
    {
        "lcd_video_start",  
        "", 
        lcd_video_start
    },
    {
        "lcd_video_stop",   
        "",  
        lcd_video_stop
    },
    {
        "lcd_snapshot",
        "",
        lcd_snapshot
    },
#endif
#ifdef LCD_IMAGE_EXAMPLE
    {
        "lcd_show_image",
        "",
        lcd_show_image
    }
#endif
};

#ifdef LCD_VIDEO_EXAMPLE
#include <vfs_fs.h>
#include <ramdisk.h>
#endif
// 播放视频时需要截屏并保存，由摄像头工程模板无法使用SD卡，采用ramdisk替代
int lcd_init(void)
{
#ifdef LCD_VIDEO_EXAMPLE
    ramdisk_dev_create("ramdisk0", 25*1024, 512);
    vfs_mount("ramdisk0", "/", "cute", 0, 0);
#endif
    return DBOT_OK;
}

int publish_lcd(void)
{
    return dbot_service_add_service(CAPABILITY_TYPE_LCD, sizeof(lcd_attrs)/sizeof(lcd_attrs[0]), lcd_attrs);
}
