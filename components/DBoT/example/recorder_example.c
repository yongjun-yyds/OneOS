#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include "cJSON.h"
#include "dbot_errno.h"
#include "dbot_log.h"
#include "publish_service.h"
#include "dbot_common.h"
#include "dbot_service.h"
#include "audio_framework.h"

#define RECORDER_TRANS_PROTOCOL  ATTR_TRANS_TCP

// #define RECORDER_SAVE_TO_FILE

#ifdef RECORDER_SAVE_TO_FILE
static FILE *s_recorder_fp = NULL;
#else
static DTP_SERVICE_CONTEXT_S *s_recorder_dtp_ctx = NULL;
#endif

static int s_recorder_quit_flag = 1;
static int s_recorder_task_started = 0;

#define RECORDER_TASK_BEGIN     s_recorder_task_started = 1
#define RECORDER_TASK_END       s_recorder_task_started = 0
#define RECORDER_TASK_STARTED   s_recorder_task_started

int recorder_stop(int len, char *req, char **resp);

void free_recorder_resource(void)
{
    stop_audio_recorder();

#ifdef RECORDER_SAVE_TO_FILE
    if(NULL != s_recorder_fp)
    {
        fclose(s_recorder_fp);
        s_recorder_fp = NULL;
    }
#else
    if(NULL != s_recorder_dtp_ctx)
    {
        dbot_service_destory_dtp(s_recorder_dtp_ctx);
        s_recorder_dtp_ctx = NULL;
    }
#endif
}

static void *recorder_thread(void *arg)
{
    int len = 0;
    int max_frame_len = (RECORDER_TRANS_PROTOCOL == ATTR_TRANS_TCP)?TCP_FRAME_MAX_LENGTH:UDP_FRAME_MAX_LENGTH;
    uint8_t *buff = malloc(max_frame_len);
    OS_ASSERT(buff);
    s_recorder_quit_flag = 0;
    while(1)
    {
        if(s_recorder_quit_flag)
        {
            break;
        }

        memset(buff, 0, max_frame_len);
        len = get_audio_recorder_data(buff, max_frame_len);
        if(0 < len)
        {
#ifdef RECORDER_SAVE_TO_FILE
            len = fwrite(buff, 1, len, s_recorder_fp);
#else
            len = s_recorder_dtp_ctx->dtp_send((void*)s_recorder_dtp_ctx, buff, len);
#endif
        }
        dbot_msleep(1);
    }
    FREE_POINTER(buff);
    free_recorder_resource();
    RECORDER_TASK_END;
    DBOT_LOG_D("recorder thread quit\r\n");
    return NULL;
}

int recorder_start(int len, char *req, char **resp)
{
    int ret = DBOT_ERROR;
    cJSON *root = NULL;
    cJSON *ip = NULL;
    cJSON *port = NULL;
    cJSON *res = NULL;

    RETURN_IF_NULL(req, DBOT_INVALID_PARAM);

    if(RECORDER_TASK_STARTED)
    {
        DBOT_LOG_E("recorder already started, stop task now\r\n");
        recorder_stop(0, NULL, NULL);
        int cnt = 6;
        while(cnt > 0)
        {
            if(!RECORDER_TASK_STARTED)
            {
                break;
            }
            dbot_msleep(200);
            cnt --;
        }
    }

    if(RECORDER_TASK_STARTED)
    {
        DBOT_LOG_E("recorder already started\r\n");
        return DBOT_ERROR;
    }

    do
    {
        root = cJSON_Parse(req);
        BREAK_IF(NULL == root);
        ip = cJSON_GetObjectItem(root, "ip");
        port = cJSON_GetObjectItem(root, "port");
        BREAK_IF(NULL == ip || NULL == port);

        BREAK_IF(DBOT_OK != start_audio_recorder(RECORDER_DEV_NAME));

#ifdef RECORDER_SAVE_TO_FILE
        s_recorder_fp = fopen("song.pcm", "wb");
        BREAK_IF(NULL == s_recorder_fp);
#else
        DTP_SERVICE_TYPE_E dtp_type = (RECORDER_TRANS_PROTOCOL == ATTR_TRANS_TCP)?DTP_SERVICE_TCP_CLIENT:DTP_SERVICE_UDP_CLIENT;
        s_recorder_dtp_ctx = dbot_service_init_dtp(dtp_type, ip->valuestring, port->valueint);
        BREAK_IF(NULL == s_recorder_dtp_ctx);
#endif
        pthread_t pid;
        pthread_attr_t attr;
        pthread_attr_init(&attr);
        pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
        pthread_attr_setstacksize(&attr, 4096);
        struct sched_param sched;
        sched.sched_priority = 30;
        pthread_attr_setschedparam(&attr, &sched);
        BREAK_IF(0 !=  pthread_create(&pid, &attr, recorder_thread, NULL));

        if(NULL != resp)
        {
            res = cJSON_CreateObject();
            cJSON_AddNumberToObject(res, "samplerate", OS_AUDIO_SAMPLERATE);
            cJSON_AddNumberToObject(res, "channel", OS_AUDIO_CHANNEL);
            cJSON_AddNumberToObject(res, "bits", 16);
            cJSON_AddNumberToObject(res, "volume", 80);
            *resp = cJSON_PrintUnformatted(res);
            cJSON_Delete(res);
        }

        ret = DBOT_OK;
        RECORDER_TASK_BEGIN;
        DBOT_LOG_E("recorder task started\r\n");
    } while (0);

    DELETE_CJSON(root);
    if(DBOT_OK != ret)
    {
        free_recorder_resource();
    }
    return ret;
}

int recorder_stop(int len, char *req, char **resp)
{
    if(RECORDER_TASK_STARTED)
    {
        s_recorder_quit_flag = 1;
    }

    return DBOT_OK;
}

static ATTR_INFO_S recorder_attrs[] =
{
    {
        "recorder_start",   
        "{\"protocol\":0, \"port\":0, \"ip\":\"0.0.0.0\"}",    
        recorder_start
    },
    {
        "recorder_stop",    
        "",                                
        recorder_stop
    }
};

int publish_recorder(void)
{
    return dbot_service_add_service(CAPABILITY_TYPE_MICROPHONE, sizeof(recorder_attrs)/sizeof(recorder_attrs[0]), recorder_attrs);
}

