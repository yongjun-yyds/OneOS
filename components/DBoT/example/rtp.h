#ifndef __RTP_H__
#define __RTP_H__

#include <stdio.h>
#include <assert.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#define RTP_JPEG_RESTART 0x40
#define RTP_HDR_SZ       12
#define JPEG_MIN_SIZE    21
#define RTP_PT_JPEG      26
#define PACKET_SIZE 1400

#define RTP_M_TO_0(x) x &= 0b01111111
#define RTP_M_TO_1(x) x |= 0b10000000

#define __RTP_LITTLE_ENDIAN 0
#define __RTP_BIG_ENDIAN    1
#define __RTP_ENDIAN_MODE __RTP_LITTLE_ENDIAN
typedef struct {
#if __RTP_ENDIAN_MODE == __RTP_BIG_ENDIAN
	uint16_t version:2;
	uint16_t p:1;
	uint16_t x:1;
	uint16_t cc:4;
	uint16_t m:1;
	uint16_t pt:7;
#else
	uint16_t cc:4;
	uint16_t x:1;
	uint16_t p:1;
	uint16_t version:2;
	uint16_t pt:7;
	uint16_t m:1;
#endif
	uint16_t seq;
	uint32_t ts;
	uint32_t ssrc;
} rtp_hdr_t;

typedef struct jpeghdr
{
    unsigned int tspec:8;   /* JPEG特征域类型 */
    unsigned int offset:24; /* 帧数据偏移量，分片用 */
    unsigned char type;     /* JPEG解码类型 */
    unsigned char q;        /* 前帧量化表, Q为127-255之间需要在JPEG头附加量化表头 */
    unsigned char width;    /* 帧宽=像素宽度/8 */
    unsigned char height;   /* 帧高=像素高度/8 */
}jpeg_hdr_t;

typedef struct
{
    unsigned short dri;
    unsigned int f:1;
    unsigned int l:1;
    unsigned int count:14;
 }jpeg_hdr_rst_t;

typedef struct 
{
    unsigned char  mbz;
    unsigned char  precision;
    unsigned short length;
}jpeg_hdr_qtable_t;

#define RTP_PACKET_MAX_SIZE 1400

typedef struct rtp_packet
{
    int len;
    int header_len;
    unsigned char buf[RTP_PACKET_MAX_SIZE];
}rtp_packet_t;

#endif
