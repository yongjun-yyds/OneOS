#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include "cJSON.h"
#include "dbot_errno.h"
#include "dbot_log.h"
#include "publish_service.h"
#include "dbot_common.h"
#include "dbot_service.h"
#include "audio_framework.h"

#define SPEAKER_TRANS_PROTOCOL  ATTR_TRANS_TCP

// #define SPEAKER_SAVE_TO_FILE

#ifdef SPEAKER_SAVE_TO_FILE
static FILE *s_speaker_fp = NULL;
#endif
static DTP_SERVICE_CONTEXT_S *s_speaker_dtp_ctx = NULL;
static int s_speaker_quit_flag = 1;
static int s_speaker_task_started = 0;

#define SPEAKER_TASK_BEGIN     s_speaker_task_started = 1
#define SPEAKER_TASK_END       s_speaker_task_started = 0
#define SPEAKER_TASK_STARTED   s_speaker_task_started

int speaker_stop(int len, char *req, char **resp);

void free_speaker_resource(void)
{
    stop_audio_player();

#ifdef SPEAKER_SAVE_TO_FILE
    if(NULL != s_speaker_fp)
    {
        fclose(s_speaker_fp);
        s_speaker_fp = NULL;
    }
#endif
    if(NULL != s_speaker_dtp_ctx)
    {
        dbot_service_destory_dtp(s_speaker_dtp_ctx);
        s_speaker_dtp_ctx = NULL;
    }
}

// 44100 * 2 * 16 = 1411200 bps
static void *speaker_thread(void *arg)
{
    int len = 0;
    int max_frame_len = (SPEAKER_TRANS_PROTOCOL == ATTR_TRANS_TCP)?TCP_FRAME_MAX_LENGTH:UDP_FRAME_MAX_LENGTH;
    uint8_t *buff = malloc(max_frame_len);
    OS_ASSERT(buff);
    s_speaker_quit_flag = 0;
    while(1)
    {
        if(s_speaker_quit_flag)
        {
            break;
        }

        memset(buff, 0, max_frame_len);
        len = s_speaker_dtp_ctx->dtp_recv((void*)s_speaker_dtp_ctx, buff, max_frame_len);
        if(0 < len)
        {
#ifdef SPEAKER_SAVE_TO_FILE
            len = fwrite(buff, 1, len, s_speaker_fp);
#else
            len = put_audio_player_data(buff, len);
#endif
        }
        dbot_msleep(1);
    }
    FREE_POINTER(buff);
    free_speaker_resource();
    SPEAKER_TASK_END;
    DBOT_LOG_D("speaker thread quit\r\n");
    return NULL;
}

int speaker_start(int len, char *req, char **resp)
{
    int ret = DBOT_ERROR;
    cJSON *root = NULL;
    cJSON *samplerate = NULL;
    cJSON *channel = NULL;
    cJSON *bits = NULL;
    cJSON *volume = NULL;
    cJSON *res = NULL;

    RETURN_IF_NULL(req, DBOT_INVALID_PARAM);
    
    if(SPEAKER_TASK_STARTED)
    {
        DBOT_LOG_E("speaker already started, stop task now\r\n");
        speaker_stop(0, NULL, NULL);
        int cnt = 6;
        while(cnt > 0)
        {
            if(!SPEAKER_TASK_STARTED)
            {
                break;
            }
            dbot_msleep(200);
            cnt --;
        }
    }

    if(SPEAKER_TASK_STARTED)
    {
        DBOT_LOG_E("speaker already started\r\n");
        return DBOT_ERROR;
    }

    do
    {
        root = cJSON_Parse(req);
        BREAK_IF(NULL == root);
        samplerate = cJSON_GetObjectItem(root, "samplerate");
        channel = cJSON_GetObjectItem(root, "channel");
        bits = cJSON_GetObjectItem(root, "bits");
        volume = cJSON_GetObjectItem(root, "volume");
        BREAK_IF(NULL == samplerate || NULL == channel || NULL == bits || NULL == volume);

        BREAK_IF(DBOT_OK != start_audio_player(SPEAKER_DEV_NAME, samplerate->valueint, channel->valueint, bits->valueint, volume->valueint));
        
#ifdef SPEAKER_SAVE_TO_FILE
        s_speaker_fp = fopen("song.pcm", "wb");
        BREAK_IF(NULL == s_speaker_fp);
#endif
        DTP_SERVICE_TYPE_E dtp_type = (SPEAKER_TRANS_PROTOCOL == ATTR_TRANS_TCP)?DTP_SERVICE_TCP_SERVER:DTP_SERVICE_UDP_SERVER;
        s_speaker_dtp_ctx = dbot_service_init_dtp(dtp_type, NULL, 0);
        BREAK_IF(NULL == s_speaker_dtp_ctx);
        if(NULL != resp)
        {
            res = cJSON_CreateObject();
            cJSON_AddStringToObject(res, "ip", get_local_device_info()->device_ip);
            cJSON_AddNumberToObject(res, "port", s_speaker_dtp_ctx->port);
            cJSON_AddNumberToObject(res, "protocol", 1);
            *resp = cJSON_PrintUnformatted(res);
            cJSON_Delete(res);
        }

        pthread_t pid;
        pthread_attr_t attr;
        pthread_attr_init(&attr);
        pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
        pthread_attr_setstacksize(&attr, 4096);
        struct sched_param sched;
        sched.sched_priority = 28;
        pthread_attr_setschedparam(&attr, &sched);
        BREAK_IF(0 !=  pthread_create(&pid, &attr, speaker_thread, NULL));

        ret = DBOT_OK;
        SPEAKER_TASK_BEGIN;
        DBOT_LOG_E("speaker task started\r\n");
    } while (0);

    DELETE_CJSON(root);
    if(DBOT_OK != ret)
    {
        free_speaker_resource();
    }
    return ret;
}

int speaker_stop(int len, char *req, char **resp)
{
    if(SPEAKER_TASK_STARTED)
    {
        s_speaker_quit_flag = 1;
    }
    return DBOT_OK;
}

static ATTR_INFO_S speaker_attrs[] =
{
    {
        "speaker_start",   
        "{\"samplerate\":44100, \"channel\":2, \"bits\":16, \"volume\":50}",    
        speaker_start
    },
    {
        "speaker_stop",    
        "",                                                                     
        speaker_stop
    }
};

int publish_speaker(void)
{
    return dbot_service_add_service(CAPABILITY_TYPE_SPEAKER, sizeof(speaker_attrs)/sizeof(speaker_attrs[0]), speaker_attrs);
}
