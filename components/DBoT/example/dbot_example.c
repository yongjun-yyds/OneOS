#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include "dbot_log.h"
#include "dbot_errno.h"
#include "dbot_service.h"

extern void show_device_service(void);
extern int publish_light(void);
extern int publish_illumination(void);
extern int publish_camera(void);
extern int camera_init(void);
extern int publish_lcd(void);
extern int lcd_init(void);
extern int light_init(void);
extern int illumination_init(const char *name);
extern int publish_recorder(void);
extern int publish_speaker(void);
extern int publish_filesystem(void);
extern int filesystem_init(void);
extern int camera_init(void);
void *dbot_func(void *arg)
{
    int ret = DBOT_ERROR;
    while(1)
    {
        ret = dbot_service_init();
        if(DBOT_OK == ret)
        {
            break;
        }
        sleep(1);
    }

    dbot_service_start();

#ifdef DBOT_PUBLISH

#ifdef LIGHT_EXAMPLE
    light_init();
    publish_light();
#endif/* LIGHT_EXAMPLE */

#ifdef ILLUMINATION_EXAMPLE
    illumination_init(ILLUMINATION_DEV_NAME);
    publish_illumination();
#endif/* ILLUMINATION_EXAMPLE */

#ifdef CAMERA_EXAMPLE
    publish_camera();
    camera_init();
#endif/* CAMERA_EXAMPLE */

#ifdef LCD_EXAMPLE
    lcd_init();
    publish_lcd();
#endif/* LCD_EXAMPLE */

#ifdef RECORDER_EXAMPLE
    publish_recorder();
#endif/* LCD_EXAMPLE */

#ifdef SPEAKER_EXAMPLE
    publish_speaker();
#endif

#ifdef FILESYSTEM_EXAMPLE
    filesystem_init();
    publish_filesystem();
#endif

    dbot_service_publish_service();
#endif/* DBOT_PUBLISH */

#ifdef DBOT_DISCOVERY
    dbot_service_discovery_service();
    dbot_msleep(5000);
    dbot_service_discovery_service();
    dbot_msleep(5000);
    dbot_service_discovery_service();
#endif

    return NULL;
}

#if defined __linux__ || defined _WIN32
int main(int argc, char **argv)
#else
int dbot_start(void)
#endif
{
    pthread_t pid;
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
    pthread_attr_setstacksize(&attr, 2048);
    pthread_create(&pid, &attr, dbot_func, NULL);

#if defined __linux__ || defined _WIN32
    while(1)
    {
        sleep(1);
    }
#endif

    return 0;
}

#ifdef DBOT_AUTO_START
#include "os_stddef.h"
OS_INIT_CALL(dbot_start, OS_INIT_LEVEL_COMPONENT, OS_INIT_SUBLEVEL_LOW);
#endif/* #ifdef DBOT_AUTO_START */


