#include <device.h>
#include <pthread.h>
#include <ring_buff.h>
#include "oneos_config.h"

#define DBOT_AUDIO_PLAYER_MAX_CACHE_NUM     50
#define DBOT_AUDIO_PLAYER_RINGBUFF_SIZE     OS_AUDIO_REPLAY_MP_BLOCK_SIZE * DBOT_AUDIO_PLAYER_MAX_CACHE_NUM

#define RECORD_BITS       16
#define RECORD_CHUNK_SZ     ((OS_AUDIO_SAMPLERATE * OS_AUDIO_CHANNEL * 2) * 20 / 1000)
#define DBOT_AUDIO_RECORDER_MAX_CACHE_NUM     10
#define DBOT_AUDIO_RECORDER_RINGBUFF_SIZE     RECORD_CHUNK_SZ * DBOT_AUDIO_RECORDER_MAX_CACHE_NUM

typedef struct _audio_player_s
{
    os_device_t *device;
    rb_ring_buff_t *cache;
    int quit_flag;
}AUDIO_PLAYER_S;

typedef AUDIO_PLAYER_S AUDIO_RECORDER_S;

int put_audio_player_data(uint8_t *data, int len);
int get_audio_player_data(uint8_t *data, int len);
int start_audio_player(char *audio_dev, int samplerate, int channel, int bits, int volume);
int stop_audio_player(void);

int put_audio_recorder_data(uint8_t *data, int len);
int get_audio_recorder_data(uint8_t *data, int len);
int start_audio_recorder(char *audio_dev);
int stop_audio_recorder(void);

