#include <stdio.h>
#include <stdlib.h>

#include <cJSON.h>

#include <sensors/sensor.h>

#include "dbot_errno.h"
#include "dbot_log.h"
#include "publish_service.h"
#include "dbot_service.h"

#define MAX_SENSOR_NAME_LEN     21
#define SENSOR_NAME_PRE_LEN     3
#define ILLUMINATION_VALUE      "value"

static os_device_t *s_sensor = NULL;

static void get_illumination_value(double *value)
{
    if(NULL == s_sensor)
        return;

    struct os_sensor_data sensor_data;
    struct os_sensor_info sensor_info;
    os_device_control(s_sensor, OS_SENSOR_CTRL_GET_INFO, &sensor_info);

    os_device_read_nonblock(s_sensor, 0, &sensor_data, sizeof(struct os_sensor_data));

    if (sensor_info.unit == OS_SENSOR_UNIT_MLUX)
    {
        *value = (double)sensor_data.data.light / (double)1000;
        // DBOT_LOG_D("sensor light (%d.%03d)\r\n", sensor_data.data.light / 1000, sensor_data.data.light % 1000);
    }
    else if (sensor_info.unit == OS_SENSOR_UNIT_LUX)
    {
        *value = (double)sensor_data.data.light;
        // DBOT_LOG_D("sensor light (%d)\r\n", sensor_data.data.light);
    }
    else if (sensor_info.unit == OS_SENSOR_UNIT_RAW)
    {
        *value = (double)sensor_data.data.light;
        // DBOT_LOG_D("sensor light raw(%d) mV\r\n", sensor_data.data.light);
    }
    else
    {
        // os_kprintf("invalid unit\r\n");
    }

    // DBOT_LOG_D("value is %lf\n", value);
    return;
}

static int _get_illumination_value(int len, char *params, char **resp)
{
    int ret = DBOT_OK;
    double value = 0;

    get_illumination_value(&value);
    // cJSON_AddNumberToObject(json_result, ILLUMINATION_VALUE, value);
    cJSON *root = cJSON_CreateObject();
    cJSON_AddNumberToObject(root, "illumination", value);
    *resp = cJSON_PrintUnformatted(root);
    cJSON_Delete(root);

    return ret;
}

static ATTR_INFO_S illumination_attrs[] =
{
    {"get_illumination_value",        "",     _get_illumination_value}
};

int illumination_init(const char *name)
{
    if(NULL == name)
        return -1;

    char sensor_name[SENSOR_NAME_PRE_LEN + MAX_SENSOR_NAME_LEN];

    snprintf(sensor_name, sizeof(sensor_name) - 1, "li_%s", name);

    s_sensor = os_device_open_s(sensor_name);
    OS_ASSERT(s_sensor != NULL);

    return 0;
}

int publish_illumination(void)
{
    return dbot_service_add_service(CAPABILITY_TYPE_ILLU, sizeof(illumination_attrs)/sizeof(illumination_attrs[0]), illumination_attrs);
}

