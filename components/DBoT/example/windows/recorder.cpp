#include <thread>
#include <iostream>
#include "recorder.h"
#include "dbot_service.h"
#include "dbot_errno.h"
#include <QtMultimedia/QAudioFormat>
#include <QtMultimedia/QAudioOutput>
#include <QApplication>
#include "mainwindow.h"

using namespace std;

Recorder *Recorder::instance = nullptr;
void recorder_thread(void);
int call_result_cb(char *resp, int len);
int call_result_cb1(char *resp, int len);
static bool s_audio_quit_flag = false;

// #define SAVE_FILE

#ifdef SAVE_FILE
static FILE *s_fp = NULL;
#else
static QAudioOutput *s_audio_output = nullptr;
QIODevice *s_audio_io = nullptr;
#endif

Recorder::Recorder()
{
    pthread_mutex_init(&m_udp_list_lock, NULL);
    m_player_status = PLAYER_STATUS_STOP;
    m_dtp_ctx = NULL;
}

Recorder::~Recorder()
{

}

Recorder *Recorder::get_instance(void)
{
    if(nullptr == instance)
    {
        instance = new Recorder();
    }
    return instance;
}

int Recorder::start(int device_id)
{
    DTP_SERVICE_CONTEXT_S *dtp = NULL;

    if(PLAYER_STATUS_START == m_player_status)
    {
        return -1;
    }

    if(NULL == m_dtp_ctx)
    {
        dtp = dbot_service_init_dtp(DTP_SERVICE_TCP_SERVER, NULL, 0);
        if(NULL == dtp)
        {
            return -1;
        }
        m_dtp_ctx = (void*)dtp;
    }
    else
    {
        dtp = (DTP_SERVICE_CONTEXT_S*)m_dtp_ctx;
    }
    cJSON *root = cJSON_CreateObject();
    cJSON_AddNumberToObject(root, "protocol", 0);
    cJSON_AddNumberToObject(root, "port", dtp->port);
    cJSON_AddStringToObject(root, "ip", dbot_service_get_ctx()->profile->device_ip);
    char *str = cJSON_PrintUnformatted(root);
    cJSON_Delete(root);
    int ret = dbot_service_call_service(device_id, CAPABILITY_TYPE_MICROPHONE, "recorder_start", str, call_result_cb);
    free(str);
    return ret;
}

int Recorder::stop(int device_id)
{
    if(PLAYER_STATUS_STOP == m_player_status)
    {
        return -1;
    }

    int ret = dbot_service_call_service(device_id, CAPABILITY_TYPE_MICROPHONE, "recorder_stop", NULL, call_result_cb1);
    if(DBOT_OK == ret)
    {
#ifdef SAVE_FILE
        fclose(s_fp);
#else
        s_audio_output->stop();
        delete s_audio_output;
        s_audio_output = nullptr;
        s_audio_io = nullptr;
#endif        
        // 关闭线程
        s_audio_quit_flag = true;
        dbot_service_destory_dtp((DTP_SERVICE_CONTEXT_S*)m_dtp_ctx);
        m_dtp_ctx = NULL;
        m_player_status = PLAYER_STATUS_STOP;
    }

    return ret;
}

DTP_BUF_S *Recorder::read(void)
{
    DTP_BUF_S *data = NULL;
    pthread_mutex_lock(&m_udp_list_lock);
    if(!m_dtp_buf_list.empty())
    {
        data = m_dtp_buf_list.front();
        m_dtp_buf_list.pop_front();
        m_buf_cnt --;
    }
    pthread_mutex_unlock(&m_udp_list_lock);

    return data;
}

int call_result_cb(char *resp, int len)
{
    cout << resp << endl;

    cJSON *root = cJSON_Parse(resp);
    cJSON *samplerate = cJSON_GetObjectItem(root, "samplerate");
    cJSON *bits = cJSON_GetObjectItem(root, "bits");
    cJSON *channel = cJSON_GetObjectItem(root, "channel");

#ifdef SAVE_FILE
    s_fp = fopen("test.pcm", "ab+");
#else
    QAudioFormat fmt;
    fmt.setSampleRate(samplerate->valueint);
    fmt.setSampleSize(bits->valueint);
    fmt.setChannelCount(channel->valueint);
    fmt.setCodec("audio/pcm");
    fmt.setByteOrder(QAudioFormat::LittleEndian);
    fmt.setSampleType(QAudioFormat::UnSignedInt);
    s_audio_output = new QAudioOutput(fmt);
    s_audio_io = s_audio_output->start();
    s_audio_io->open(QIODevice::ReadWrite);
    // s_audio_output->setVolume(5);
#endif
    cJSON_Delete(root);

    s_audio_quit_flag = false;
    std::thread recorder_th(recorder_thread);
    recorder_th.detach();
    Recorder::get_instance()->m_player_status = PLAYER_STATUS_START;

    return 0;
}

int call_result_cb1(char *resp, int len)
{
    return 0;
}

void recorder_thread(void)
{
    Recorder *audio = Recorder::get_instance();
    emit audio->notice_user("开始播放对端录音...");
    DTP_SERVICE_CONTEXT_S *dtp = (DTP_SERVICE_CONTEXT_S*)audio->m_dtp_ctx;
    int buf_size = UDP_FRAME_MAX_LENGTH;
    uint8_t *buffer = (uint8_t*)calloc(1, buf_size);
    if(NULL == buffer)
    {
        cout << "quit" << endl;
        return;
    }
    int len = 0;
    int period = get_current_time();
    int bytes = 0;

    int freesize = 0;
    int offset = 0;
    // int size = s_audio_output->periodSize();
    // std::shared_ptr<char> buffer(new char[size], std::default_delete<char[]>());
    // memset(buffer.get(), 0x00, size);

    while(1)
    {
        if(s_audio_quit_flag)
        {
            break;
        }

        memset(buffer, 0, buf_size);
        len = dtp->dtp_recv((void*)dtp, buffer, buf_size);
        if(0 < len)
        {
            bytes += len;
            if(get_current_time() - period > 10000)
            {
                cout << "recv " << bytes << " bytes in 10 seconds" << endl;
                bytes = 0;
                period = get_current_time();
            }
#ifdef SAVE_FILE
            fwrite(buffer, 1, len, s_fp);
#else
            // size = s_audio_output->periodSize();    // 保证播放数据不间断所需的数据大小
            // 声卡缓冲区空闲大小
            // if(0 == s_audio_output->bytesFree())
            // {
            //     continue;
            // }
            // s_audio_io->write((const char *)buffer, len);
            while(1)
            {
                freesize = s_audio_output->bytesFree();
                // cout << "freesize " << freesize << " total size " << len << endl;
                if(0 == freesize)
                {
                    continue;
                    dbot_msleep(1);
                }
                s_audio_io->write((const char *)buffer, len);
                break;
            }
#endif
        }
    }
    emit audio->notice_user("停止播放对端录音");
}
