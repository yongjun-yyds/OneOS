#include <QDebug>
#include <QImage>
#include <QLineEdit>
#include <QFileDialog>
#include <QFile>
#include <QMessageBox>
#include <vector>
#include <list>
#include <iostream>
#include <fstream>
#include <thread>
#include <unistd.h>
#include <pthread.h>
#include <cJSON.h>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "dbot_service.h"
#include "dbot_errno.h"
#include "dbot_common.h"
#include "recorder.h"
#include "speaker.h"
#include "camera.h"
#include "trans_file.h"
#include "filesystem_example.h"
#include "example_cmd.h"

using namespace std;

int call_cb(char *resp, int len)
{
    MainWindow::get_instance()->notice_user(QString(resp));
    return 0;
}

int call_common_attr(int dev_id, int service_id, const char *func)
{
    return dbot_service_call_service(dev_id, service_id, (char*)func, NULL, call_cb);
}

map<QString, int> call_attr_map = 
{
    {"turn_on_light",           1},
    {"turn_off_light",          2},
    {"get_light_status",        3},
    {"get_illumination_value",  4},
    {"speaker_start",           5},
    {"speaker_stop",            6},
    {"recorder_start",          7},
    {"recorder_stop",           8},
    {"camera_start",            9},
    {"camera_stop",             10},
    {"show_item",               11},
    {"write_file",              12},
    {"show_directory",          13}
};

map<int, QString> service_map = 
{
    {CAPABILITY_TYPE_LIGHT,     "灯"},
    {CAPABILITY_TYPE_LCD,       "屏幕"},
    {CAPABILITY_TYPE_ILLU,      "光照传感"},
    {CAPABILITY_TYPE_CAMERA,    "摄像头"},
    {CAPABILITY_TYPE_MICROPHONE,"麦克风"},
    {CAPABILITY_TYPE_SPEAKER,   "喇叭"},
    {CAPABILITY_TYPE_FILESYSTEM,   "文件系统"}
};

map<QString, int> service_map1 = 
{
    {"灯",      CAPABILITY_TYPE_LIGHT},
    {"屏幕",    CAPABILITY_TYPE_LCD},
    {"光照传感",CAPABILITY_TYPE_ILLU},
    {"摄像头",  CAPABILITY_TYPE_CAMERA},
    {"麦克风",  CAPABILITY_TYPE_MICROPHONE},
    {"喇叭",    CAPABILITY_TYPE_SPEAKER},
    {"文件系统",CAPABILITY_TYPE_FILESYSTEM}
};

MainWindow *MainWindow::instance = nullptr;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    int ret = DBOT_ERROR;

    ui->setupUi(this);
    while(1)
    {
        ret = dbot_service_init();
        if(DBOT_OK == ret)
        {
            break;
        }
        sleep(1);
    }

    dbot_service_start();
    publish_filesystem();
    Camera::publish_win_lcd();

    dbot_service_publish_service();

    connect(ui->discovery_button, SIGNAL(clicked()), this, SLOT(discovery()));
    connect(ui->select_button, SIGNAL(clicked()), this, SLOT(select_file()));
    connect(ui->dataflow_start_button, SIGNAL(clicked()), this, SLOT(dataflow_start()));
    connect(ui->dataflow_stop_button, SIGNAL(clicked()), this, SLOT(dataflow_stop()));
    connect(ui->device_tree, SIGNAL(itemDoubleClicked(QTreeWidgetItem*, int)), this, SLOT(device_tree_doublieclick(QTreeWidgetItem*, int)));
    connect(ui->dataflow_src_dev, SIGNAL(activated(int)), this, SLOT(dataflow_flush_service(int)));

    connect(Camera::get_instance(), SIGNAL(display_jpeg(uint8_t*, int)), this, SLOT(fresh_jpeg(uint8_t*, int)));
    // connect(TransFile::get_instance(), SIGNAL(progress_bar(int)), this, SLOT(progress(int)));
    // connect(TransFile::get_instance(), SIGNAL(notice_user(QString)), this, SLOT(notice(QString)));
    connect(Speaker::get_instance(), SIGNAL(notice_user(QString)), this, SLOT(notice(QString)));
    connect(Recorder::get_instance(), SIGNAL(notice_user(QString)), this, SLOT(notice(QString)));
    connect(Camera::get_instance(), SIGNAL(notice_user(QString)), this, SLOT(notice(QString)));
    connect(this, SIGNAL(notice_user(QString)), this, SLOT(notice(QString)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

MainWindow *MainWindow::get_instance(void)
{
    if(nullptr == instance)
    {
        instance = new MainWindow();
    }
    return instance;
}

void MainWindow::notice_box(QString title, QString text)
{
    QMessageBox::information(this, title,  text, QMessageBox::Ok);
}

void MainWindow::refresh_device_tree(void)
{
    // 清空tree
    ui->device_tree->clear();
    ui->dataflow_src_dev->clear();
    ui->dataflow_dst_dev->clear();
    ui->dataflow_src_service->clear();
    ui->progress_bar->reset();

    // 刷新tree
    DBOT_SERVICE_CONTEXT_S *dbot_ctx = dbot_service_get_ctx();
    pthread_mutex_lock(&dbot_ctx->discovery->device_list_lock);
    DEVICE_NODE_T *dev = NULL;
    service_info_t *service = NULL;
    QTreeWidgetItem *device_item, *online_item, *session_item, *service_item, *attr_item;
    cJSON *root, *attrs, *attr, *func;
    int attr_num, i;

    os_list_for_each_entry(dev, &dbot_ctx->discovery->device_list, DEVICE_NODE_T, node)
    {
        device_item = new QTreeWidgetItem(ui->device_tree);
        device_item->setText(0, QString::number(dev->device_info.device_id, 10));
        device_item->setForeground(0, (dev->online)?Qt::green:Qt::red);
        // 给设备添加复选框
        // device_item->setCheckState(0, Qt::CheckState::Unchecked);

        session_item = new QTreeWidgetItem(device_item);
        session_item->setText(0, (dev->client.connected)?"已建立回话":"未建立回话");
        session_item->setForeground(0, (dev->client.connected)?Qt::blue:Qt::gray);
        session_item->addChild(device_item);

        // 给dataflow的选择框添加内容
        ui->dataflow_src_dev->addItem(QString::number(dev->device_info.device_id, 10));
        ui->dataflow_dst_dev->addItem(QString::number(dev->device_info.device_id, 10));

        os_list_for_each_entry(service, &dev->device_info.services.service_list, service_info_t, node)
        {
            service_item = new QTreeWidgetItem(device_item);
            service_item->setText(0, service_map[service->service_type]);
            device_item->addChild(service_item);
            if(NULL != service->attrs.attr)
            {
                root = cJSON_Parse(service->attrs.attr);
                attrs = cJSON_GetObjectItem(root, "attr");
                attr_num = cJSON_GetArraySize(attrs);
                for(i = 0;i < attr_num;i ++)
                {
                    attr = cJSON_GetArrayItem(attrs, i);
                    func = cJSON_GetObjectItem(attr, "func");
                    attr_item = new QTreeWidgetItem(service_item);
                    attr_item->setText(0, QString(func->valuestring));
                    device_item->addChild(service_item);
                }
                cJSON_Delete(root);
            }
        }
    }
    pthread_mutex_unlock(&dbot_ctx->discovery->device_list_lock);
    ui->dataflow_src_dev->setCurrentIndex(-1);
    ui->dataflow_dst_dev->setCurrentIndex(-1);
}

void MainWindow::device_tree_check(QTreeWidgetItem *item, int colum)
{

}

void MainWindow::dataflow_flush_service(int index)
{
    ui->dataflow_src_service->clear();
    int src_dev_id = ui->dataflow_src_dev->currentText().toInt();
    DBOT_SERVICE_CONTEXT_S *dbot_ctx = dbot_service_get_ctx();
    pthread_mutex_lock(&dbot_ctx->discovery->device_list_lock);
    DEVICE_NODE_T *dev = NULL;
    service_info_t *service = NULL;
    os_list_for_each_entry(dev, &dbot_ctx->discovery->device_list, DEVICE_NODE_T, node)
    {
        if(dev->device_info.device_id == src_dev_id)
        {
            os_list_for_each_entry(service, &dev->device_info.services.service_list, service_info_t, node)
            {
                if(service->service_type == CAPABILITY_TYPE_MICROPHONE || service->service_type == CAPABILITY_TYPE_CAMERA)
                {
                    ui->dataflow_src_service->addItem(service_map[service->service_type]);
                }
            }
        }
    }
    pthread_mutex_unlock(&dbot_ctx->discovery->device_list_lock);
}

void MainWindow::device_tree_doublieclick(QTreeWidgetItem *item, int colum)
{
    QTreeWidgetItem *parent = item->parent();
    // 设备层级
    if(nullptr == parent)
    {
        return;
    }
    
    // 能力层级
    if(nullptr == parent->parent())
    {
        int device_id = atoi(parent->text(0).toStdString().c_str());
        if(0 == QString::compare(item->text(0), "未建立回话", Qt::CaseSensitive))
        {
            dbot_service_create_session(device_id);
        }
        if(0 == QString::compare(item->text(0), "已建立回话", Qt::CaseSensitive))
        {
            dbot_service_remove_session(device_id);
        }
        refresh_device_tree();
        return;
    }

    // 属性层级
    if(nullptr == parent->parent()->parent())
    {
        int attr_func_val = call_attr_map[item->text(0)];
        int device_id = atoi(parent->parent()->text(0).toStdString().c_str());
        int service_id = service_map1[parent->text(0)];
        const char *func = item->text(0).toStdString().c_str();
        QString file_path = ui->select_label->text();
        QFile file(file_path);
        switch(attr_func_val)
        {
            case 1:
            case 2:
            case 3:
            case 4:
                call_common_attr(device_id, service_id, func);
                break;
            // case 5:
            //     Speaker::get_instance()->start(device_id);
            //     break;
            // case 6:
            //     Speaker::get_instance()->stop(device_id);
            //     break;
            // case 7:
            //     Recorder::get_instance()->start(device_id);
            //     break;
            // case 8:
            //     Recorder::get_instance()->stop(device_id);
            //     break;
            case 9:
                Camera::get_instance()->start(device_id);
                break;
            case 10:
                Camera::get_instance()->stop(device_id);
                break;
            // case 11:
            //     break;
            case 12:
                if(!file.exists())
                {
                    notice_box(item->text(0), "请先选择要传输的文件!");
                }
                else
                {
                    write_file(parent->parent()->text(0), file_path);
                }
                break;
            // case 13:
            //     list_dir(parent->parent()->text(0), "/");
            //     break;
            default:
                notice_box(item->text(0), "暂不支持该功能!");
                break;
        }
    }
}

void MainWindow::notice(QString content)
{
    ui->notice_browser->append(content);
}

void MainWindow::progress(int percent)
{
    if(0 == percent)
    {
        ui->progress_bar->reset();
    }
    ui->progress_bar->setValue(percent);
}

void MainWindow::discovery(void)
{
    dbot_service_discovery_service();
    sleep(1);
    refresh_device_tree();
}

void MainWindow::select_file(void)
{
    QString file_name = QFileDialog::getOpenFileName(this, "选择文件", "./","ALL files(*.*)");
    ui->select_label->setText(file_name);
}

int dataflow_cb(char *resp, int len)
{
    qDebug() << resp;
    return 0;
}

void MainWindow::dataflow_start(void)
{
    int ability = service_map1[ui->dataflow_src_service->currentText()];
    char *cmd[3];
    cmd[1] = (char*)ui->dataflow_src_dev->currentText().toStdString().c_str();
    cmd[2] = (char*)ui->dataflow_dst_dev->currentText().toStdString().c_str();
    if(CAPABILITY_TYPE_MICROPHONE == ability)
    {
        dbot_audio_dataflow(3, (char**)cmd);
    }
    else if(CAPABILITY_TYPE_CAMERA == ability)
    {
        dbot_video_dataflow(3, (char**)cmd);
    }
}

void MainWindow::dataflow_stop(void)
{
    int ability = service_map1[ui->dataflow_src_service->currentText()];
    char *cmd[3];
    cmd[1] = (char*)ui->dataflow_src_dev->currentText().toStdString().c_str();
    cmd[2] = (char*)ui->dataflow_dst_dev->currentText().toStdString().c_str();
    if(CAPABILITY_TYPE_MICROPHONE == ability)
    {
        dbot_audio_dataflow_stop(3, (char**)cmd);
    }
    else if(CAPABILITY_TYPE_CAMERA == ability)
    {
        dbot_video_dataflow_stop(3, (char**)cmd);
    }
}

void MainWindow::write_file(QString dev_id, QString filename)
{
    char *cmd[3];
    cmd[1] = (char*)dev_id.toStdString().c_str();
    cmd[2] = (char*)filename.toStdString().c_str();
    dbot_write_file(3, (char**)cmd);
}

void MainWindow::list_dir(QString dev_id, QString dirname)
{
    char *cmd[3];
    cmd[1] = (char*)dev_id.toStdString().c_str();
    cmd[2] = (char*)dirname.toStdString().c_str();
    dbot_show_directory(3, (char**)cmd);
}

void MainWindow::fresh_jpeg(uint8_t *data, int len)
{
    if(0 == len)
    {
        ui->video_label->clear();
    }
    else
    {
        QImage img;
        // ov2640输出的jpeg是倒的，翻转后再显示
        QTransform transform;
        transform.rotate(180);
        if(img.loadFromData((uchar*)data, len))
        {
            // 缩放1/4
            // img = img.scaled(img.width()/4, img.height()/4, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
            ui->video_label->setPixmap(QPixmap::fromImage(img).transformed(transform, Qt::SmoothTransformation));
            ui->video_label->resize(img.width(), img.height());
        }
    }
}

uint32_t crc(uint8_t *data, int len)
{
    uint32_t c;
    for(int i = 0;i < len;i ++)
        c = c ^ (data[i]);
    return c;
}
