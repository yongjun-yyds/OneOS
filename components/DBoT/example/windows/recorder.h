#ifndef __RECORDER_H__
#define __RECORDER_H__

#include <QObject>
#include <list>
#include <pthread.h>

typedef enum _player_status_e
{
    PLAYER_STATUS_STOP = 0,
    PLAYER_STATUS_START,
    PLAYER_STATUS_PAUSE
}PLAYER_STATUS_E;

typedef struct _dtp_buf
{
    uint8_t buf[1500];
    int len;
}DTP_BUF_S;

class Recorder : public QObject
{
    Q_OBJECT

public:
    ~Recorder();
    static Recorder *get_instance(void);
    int start(int device_id);
    int stop(int device_id);
    DTP_BUF_S *read(void);
    uint64_t m_buf_cnt;
    std::list<DTP_BUF_S*> m_dtp_buf_list;
    pthread_mutex_t m_udp_list_lock;
    void *m_dtp_ctx;
    PLAYER_STATUS_E m_player_status;

private:
    static Recorder *instance;
    Recorder();
    
signals:
    void sig_recv(void);
    void notice_user(QString content);
};


#endif /* __RECORDER_H__ */