#ifndef __TRANS_FILE_H__
#define __TRANS_FILE_H__

#include <QObject>

class TransFile : public QObject
{
    Q_OBJECT

public:
    ~TransFile();
    static TransFile *get_instance(void);
    int start(int device_id, const char *param);
    void *m_dtp_ctx;

private:
    static TransFile *instance;
    TransFile();

signals:
    void progress_bar(int percent);
    void notice_user(QString content);
};

#endif
