#include <thread>
#include <iostream>
#include <fstream>
#include "trans_file.h"
#include "dbot_service.h"
#include "dbot_errno.h"
#include "pb_encode.h"
#include "pb_decode.h"
#include "dbot_nanopb.h"
#include "fileproto.pb.h"
#include "crc-ccitt.h"
#include "mainwindow.h"
#include <QFile>
#include <QFileInfo>


using namespace std;

TransFile *TransFile::instance = nullptr;
static bool s_write_quit_flag = false;
int write_file_call_result_cb(char *resp, int len);
void write_file_thread(void);
static char s_write_file_name[50] = {0};
static int s_file_size = 0;
static int s_total_size = 0;

TransFile::TransFile()
{
    m_dtp_ctx = NULL;
}

TransFile::~TransFile()
{

}

TransFile *TransFile::get_instance(void)
{
    if(nullptr == instance)
    {
        instance = new TransFile();
    }
    return instance;
}

int TransFile::start(int device_id, const char *param)
{
    if(NULL == m_dtp_ctx)
    {
        QString file_name = QString(param);
        QFileInfo file_info(file_name);
        memset(s_write_file_name, 0, sizeof(s_write_file_name));
        emit TransFile::get_instance()->notice_user(file_info.fileName());
        strcpy(s_write_file_name, file_info.fileName().toStdString().c_str());
        s_file_size = file_info.size();
        char param_json[100] = {0};
        snprintf(param_json, 100, "{\"file_name\":\"%s\", \"file_size\":%d}", s_write_file_name, s_file_size);

        return dbot_service_call_service(device_id, CAPABILITY_TYPE_FILESYSTEM, (char*)"transport_file", param_json, write_file_call_result_cb);
    }

    return 0;
}

int write_file_call_result_cb(char *resp, int len)
{
    if(NULL == resp)
    {
        return -1;
    }

    emit TransFile::get_instance()->notice_user(QString(resp));
    cJSON *root = cJSON_Parse(resp);
    if(NULL == root)
    {
        return -1;
    }
    cJSON *ip = cJSON_GetObjectItem(root, "ip");
    cJSON *port = cJSON_GetObjectItem(root, "port");
    if(NULL != ip && NULL != port)
    {
        TransFile *trans_file = TransFile::get_instance();
        trans_file->m_dtp_ctx = dbot_service_init_dtp(DTP_SERVICE_TCP_CLIENT, ip->valuestring, port->valueint);
        if(NULL != trans_file->m_dtp_ctx)
        {
            s_write_quit_flag = false;
            std::thread trans_file_th(write_file_thread);
            trans_file_th.detach();
        }
    }
    cJSON_Delete(root);
    return 0;
}

bool client_file_flow_callback(pb_ostream_t *ostream, const pb_field_iter_t *field)
{
    //FILE       *file_fp      = *(FILE **)field->pData;
    FilePackage file_package = {0};
    uint32_t    read_len     = 0;
    uint16_t    crc          = 0xFFFF;

    TransFile *trans_file = TransFile::get_instance();
    ifstream write_file_fin;
    write_file_fin.open(s_write_file_name, ios::in | ios::binary);
    while ((read_len = write_file_fin.read((char*)file_package.payload.bytes, sizeof(file_package.payload.bytes)).gcount()) > 0)
    {
        crc                       = 0xFFFF;
        crc                       = crc_ccitt(crc, file_package.payload.bytes, read_len);
        file_package.crc          = crc;
        file_package.payload.size = read_len;

        if (!pb_encode_tag_for_field(ostream, field))
            return false;

        if (!pb_encode_submessage(ostream, FilePackage_fields, &file_package))
            return false;

        s_total_size += read_len;
        // 更新进度条
        emit trans_file->progress_bar(s_total_size * 100/s_file_size);

        dbot_msleep(10);
    }
    write_file_fin.close();

    return true;
}

void write_file_thread(void)
{
    FileFlow     file_flow = {0};
    struct stat stat_buf  = {0};
    int32_t      result    = 0;

    TransFile *trans_file = TransFile::get_instance();
    s_total_size = 0;
    // 重置进度条
    emit trans_file->progress_bar(0);
    file_flow.has_header = true;
    strncpy(file_flow.header.name, s_write_file_name, sizeof(file_flow.header.name));

    file_flow.header.type     = 0;
    file_flow.header.size     = stat_buf.st_size;
    file_flow.header.has_sha1 = false;
    file_flow.package         = NULL;

    result = dbot_nanopb_msg_output((void *)trans_file->m_dtp_ctx, FileFlow_fields, &file_flow);
    if (result != DBOT_OK)
    {
        DBOT_LOG_E("dbot nanopb msg output failed\r\n");
    }
    dbot_service_destory_dtp((DTP_SERVICE_CONTEXT_S*)trans_file->m_dtp_ctx);
    trans_file->m_dtp_ctx = NULL;
    emit trans_file->notice_user("传输文件结束");
}
