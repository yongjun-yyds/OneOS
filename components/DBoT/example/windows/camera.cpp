#include <thread>
#include <iostream>
#include "camera.h"
#include "dbot_service.h"
#include "dbot_errno.h"
#include "rtp.h"

using namespace std;

void camera_thread(void);

Camera *Camera::instance = nullptr;
static bool s_camera_quit_flag = false;

Camera::Camera()
{
    m_dtp_ctx = NULL;
}

Camera::~Camera()
{

}

Camera *Camera::get_instance(void)
{
    if(nullptr == instance)
    {
        instance = new Camera();
    }
    return instance;
}

int Camera::create_lcd_task(void)
{
    if(NULL == m_dtp_ctx)
    {
        m_dtp_ctx = dbot_service_init_dtp(DTP_SERVICE_TCP_SERVER, NULL, 0);
        if(NULL == m_dtp_ctx)
        {
            cout << "create dpt server failed" << endl;
            return -1;
        }
    }
    s_camera_quit_flag = false;
    std::thread camera_th(camera_thread);
    camera_th.detach();
    return 0;
}

int Camera::remove_lcd_task(void)
{
    // 关闭线程
    s_camera_quit_flag = true;
    dbot_service_destory_dtp((DTP_SERVICE_CONTEXT_S*)m_dtp_ctx);
    m_dtp_ctx = NULL;
    return 0;
}

int Camera::lcd_video_start(int len, char *params, char **resp)
{
    lcd_video_stop(0, NULL, NULL);
    dbot_msleep(1000);
    
    int ret = Camera::get_instance()->create_lcd_task();
    if(0 == ret)
    {
        DTP_SERVICE_CONTEXT_S *dtp = (DTP_SERVICE_CONTEXT_S*)Camera::get_instance()->m_dtp_ctx;
        cJSON *root = cJSON_CreateObject();
        cJSON_AddNumberToObject(root, "protocol", 0);
        cJSON_AddNumberToObject(root, "port", dtp->port);
        cJSON_AddStringToObject(root, "ip", dbot_service_get_ctx()->profile->device_ip);
        *resp = cJSON_PrintUnformatted(root);
        cJSON_Delete(root);
    }
    return ret;
}

int Camera::lcd_video_stop(int len, char *params, char **resp)
{
    return Camera::get_instance()->remove_lcd_task();
}

int Camera::start(int device_id)
{
    char *resp = NULL;
    lcd_video_start(0, NULL, &resp);
    if(NULL != resp)
    {
        int ret = dbot_service_call_service(device_id, CAPABILITY_TYPE_CAMERA, "camera_start", resp, NULL);
        free(resp);
    }
    return 0;
}

int Camera::stop(int device_id)
{
    lcd_video_stop(0, NULL, NULL);
    dbot_service_call_service(device_id, CAPABILITY_TYPE_CAMERA, "camera_stop", NULL, NULL);
    return 0;
}

#define JPEG_BUF_SIZE   100 * 1024
uint8_t *jpeg_buf = NULL;
int jpeg_len = 0;
uint16_t jpeg_seq = 0;
uint32_t jpeg_ts = 0;

void collect_jpeg(uint8_t *data, int len)
{
    if(NULL == jpeg_buf)
    {
        jpeg_buf = (uint8_t*)malloc(JPEG_BUF_SIZE);
        if(NULL != jpeg_buf)
        {
            memset(jpeg_buf, 0, JPEG_BUF_SIZE);
        }
    }
    if(NULL == jpeg_buf)
    {
        return;
    }

    if(len < JPEG_MIN_SIZE)
    {
        return;
    }
    // rtp开头是0x80 0x1a或者0x80 0x9a
    if(data[0] != 0x80)
    {
        return;
    }
    if(data[1] != 0x1a && data[1] != 0x9a)
    {
        return;
    }

    rtp_hdr_t *rtp_hdr = (rtp_hdr_t*)data;
    jpeg_hdr_t *jpeg_hdr = (jpeg_hdr_t*)(data + RTP_HDR_SZ);
    int offset = jpeg_hdr->offset;
    offset = ntohl(offset);
    offset = offset >> 8;
    int seq = ntohs(rtp_hdr->seq);
    int ts = ntohl(rtp_hdr->ts);
    do
    {
        //偏移量为0，说明是一帧图像的第一帧RTP数据，清除之前的jpeg缓存
        if(0 == offset && 0 == rtp_hdr->m)
        {   //包头，清空jpeg缓存
            memset(jpeg_buf, 0, JPEG_BUF_SIZE);
            jpeg_len = 0;
            jpeg_seq = 0;
            jpeg_ts = 0;
        }
        if(0 != offset && 0 == rtp_hdr->m)
        {   //中间的包
            if(seq != jpeg_seq + 1)
            {   //包不连续，丢弃当前jpeg缓存
                memset(jpeg_buf, 0, JPEG_BUF_SIZE);
                jpeg_len = 0;
                jpeg_seq = 0;
                jpeg_ts = 0;
                // cout << "jpeg error 1" << endl;
                break;
            }
            if(ts != jpeg_ts)
            {   //丢包，错包，丢弃当前jpeg缓存
                memset(jpeg_buf, 0, JPEG_BUF_SIZE);
                jpeg_len = 0;
                jpeg_seq = 0;
                jpeg_ts = 0;
                // cout << "jpeg error 2" << endl;
                break;
            }
            if(offset != jpeg_len)
            {
                //丢包
                memset(jpeg_buf, 0, JPEG_BUF_SIZE);
                jpeg_len = 0;
                jpeg_seq = 0;
                jpeg_ts = 0;
                // cout << "jpeg error 3" << endl;
                break;
            }
        }

        if(jpeg_len + len - 20 > JPEG_BUF_SIZE)
        {
            memset(jpeg_buf, 0, JPEG_BUF_SIZE);
        }

        memcpy(jpeg_buf + jpeg_len, data + 20, len - 20);
        jpeg_len += (len - 20);
        jpeg_seq = seq;
        jpeg_ts = ts;

        if(1 == rtp_hdr->m)
        {   //包尾
            //校验jpeg 头和尾，并播放
            if(jpeg_buf[0] == 0xFF && jpeg_buf[1] == 0xD8 && jpeg_buf[jpeg_len -2] == 0xFF && jpeg_buf[jpeg_len -1] == 0xD9)
            {
                // uint32_t size = ntohl(rtp_hdr->ssrc);
                // uint32_t c = crc(jpeg_buf, jpeg_len); 
                // if(size != c)
                // {
                //     cout << "jpeg error 4" << endl;
                //     break;
                // }
                emit Camera::get_instance()->display_jpeg(jpeg_buf, jpeg_len);
                dbot_msleep(10);
            }
        }
    }while(0);
}

void camera_thread(void)
{
    Camera *camera = Camera::get_instance();
    emit camera->notice_user("开始播放对端视频...");
    DTP_SERVICE_CONTEXT_S *dtp = (DTP_SERVICE_CONTEXT_S*)camera->m_dtp_ctx;
    int buf_size = RTP_PACKET_MAX_SIZE;
    uint8_t *buffer = (uint8_t*)calloc(1, buf_size);
    if(NULL == buffer)
    {
        cout << "quit" << endl;
        return;
    }
    int len = 0;

    while(1)
    {
        if(s_camera_quit_flag)
        {
            break;
        }

        memset(buffer, 0, buf_size);
        len = dtp->dtp_recv((void*)dtp, buffer, buf_size);
        if(0 < len)
        {
            // cout << "recv " << len << " bytes" << endl;
            collect_jpeg(buffer, len);
        }
    }
    emit Camera::get_instance()->display_jpeg(NULL, 0);
    emit camera->notice_user("停止播放对端视频");
}

static ATTR_INFO_S win_lcd_attrs[] =
{
    {"lcd_video_start",       "",    Camera::lcd_video_start},
    {"lcd_video_stop",        "",    Camera::lcd_video_stop}
};

int Camera::publish_win_lcd(void)
{
    return dbot_service_add_service(CAPABILITY_TYPE_LCD, sizeof(win_lcd_attrs)/sizeof(win_lcd_attrs[0]), win_lcd_attrs);
}
