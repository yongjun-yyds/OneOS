#include <thread>
#include <iostream>
#include <fstream>
#include "speaker.h"
#include "dbot_service.h"
#include "dbot_errno.h"
#include "mainwindow.h"

using namespace std;

Speaker *Speaker::instance = nullptr;
void speaker_thread(void);
int speaker_call_result_cb(char *resp, int len);
int speaker_call_result_cb1(char *resp, int len);
static bool s_speaker_quit_flag = false;

Speaker::Speaker()
{
    m_dtp_ctx = NULL;
}

Speaker::~Speaker()
{

}

Speaker *Speaker::get_instance(void)
{
    if(nullptr == instance)
    {
        instance = new Speaker();
    }
    return instance;
}

int Speaker::start(int device_id)
{
    if(SPEAKER_STATUS_START == m_speaker_status)
    {
        return -1;
    }

    if(NULL == m_dtp_ctx)
    {
        cJSON *root = cJSON_CreateObject();
        cJSON_AddNumberToObject(root, "samplerate", 16000);
        cJSON_AddNumberToObject(root, "bits", 16);
        cJSON_AddNumberToObject(root, "channel", 1);
        cJSON_AddNumberToObject(root, "volume", 80);
        char *str = cJSON_PrintUnformatted(root);
        int ret =  dbot_service_call_service(device_id, CAPABILITY_TYPE_SPEAKER, "speaker_start", str, speaker_call_result_cb);
        FREE_POINTER(str);
        return ret;
    }

    return 0;
}

int Speaker::stop(int device_id)
{
    if(SPEAKER_STATUS_STOP == m_speaker_status)
    {
        return -1;
    }

    int ret = dbot_service_call_service(device_id, CAPABILITY_TYPE_SPEAKER, "speaker_stop", NULL, speaker_call_result_cb1);
    if(DBOT_OK == ret)
    {
        // 关闭线程
        s_speaker_quit_flag = true;
        dbot_service_destory_dtp((DTP_SERVICE_CONTEXT_S*)m_dtp_ctx);
        m_dtp_ctx = NULL;
        m_speaker_status = SPEAKER_STATUS_STOP;
    }
    return ret;
}

int speaker_call_result_cb1(char *resp, int len)
{
    cout << "recv resp " << resp << endl;
    return 0;
}

int speaker_call_result_cb(char *resp, int len)
{
    if(NULL == resp)
    {
        return -1;
    }

    cout << "recv resp " << resp << endl;
    cJSON *root = cJSON_Parse(resp);
    if(NULL == root)
    {
        return -1;
    }
    cJSON *ip = cJSON_GetObjectItem(root, "ip");
    cJSON *port = cJSON_GetObjectItem(root, "port");
    if(NULL != ip && NULL != port)
    {
        Speaker *speaker = Speaker::get_instance();
        speaker->m_dtp_ctx = dbot_service_init_dtp(DTP_SERVICE_UDP_CLIENT, ip->valuestring, port->valueint);
        if(NULL != speaker->m_dtp_ctx)
        {
            s_speaker_quit_flag = false;
            std::thread speaker_th(speaker_thread);
            speaker_th.detach();
            speaker->m_speaker_status = SPEAKER_STATUS_START;
        }
    }
    cJSON_Delete(root);
    return 0;
}

void speaker_thread(void)
{
    Speaker *speaker = Speaker::get_instance();
    emit speaker->notice_user("开始到对端播放...");
    DTP_SERVICE_CONTEXT_S *dtp = (DTP_SERVICE_CONTEXT_S*)speaker->m_dtp_ctx;
    // 每20ms发送一个chunk，16000采样率单通道pcm一个chunk640bytes
    int buf_size = 640;
    char buffer[buf_size];
    ifstream s_speaker_fin;
    s_speaker_fin.open("voice_of_cms.wav", ios::in | ios::binary);
    char head[44] = {0};
    size_t size = s_speaker_fin.read(head, sizeof(head)).gcount();
    int cnt = 0;
    while(1)
    {
        if(s_speaker_quit_flag)
        {
            break;
        }
        memset(buffer, 0, sizeof(buffer));
        size = s_speaker_fin.read(buffer, sizeof(buffer)).gcount();
        if(0 < size)
        {
            dtp->dtp_send(speaker->m_dtp_ctx, (uint8_t*)buffer, size);
            dbot_msleep(20);
        }
        else
        {
            break;
        }
    }
    s_speaker_fin.close();
    emit speaker->notice_user("停止到对端播放");
}
