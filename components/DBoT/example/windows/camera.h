#ifndef __CAMERA_H__
#define __CAMERA_H__

#include <QObject>

class Camera : public QObject
{
    Q_OBJECT

public:
    ~Camera();
    static Camera *get_instance(void);
    int create_lcd_task(void);
    int remove_lcd_task(void);
    int start(int device_id);
    int stop(int device_id);
    void *m_dtp_ctx;
    static int lcd_video_start(int len, char *params, char **resp);
    static int lcd_video_stop(int len, char *params, char **resp);
    static int publish_win_lcd(void);
private:
    static Camera *instance;
    Camera();
    
signals:
    void display_jpeg(uint8_t *data, int len);
    void notice_user(QString content);
};

#endif /* __CAMERA_H__ */