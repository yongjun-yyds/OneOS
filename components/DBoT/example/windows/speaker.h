#ifndef __SPEAKER_H__
#define __SPEAKER_H__

#include <QObject>

typedef enum _speaker_status_e
{
    SPEAKER_STATUS_STOP = 0,
    SPEAKER_STATUS_START,
    SPEAKER_STATUS_PAUSE
}SPEAKER_STATUS_E;

class Speaker : public QObject
{
    Q_OBJECT

public:
    ~Speaker();
    static Speaker *get_instance(void);
    int start(int device_id);
    int stop(int device_id);
    void *m_dtp_ctx;
    SPEAKER_STATUS_E m_speaker_status;

private:
    static Speaker *instance;
    Speaker();

signals:
    void notice_user(QString content);

};


#endif /* __AUDIO_PLAYER_H__ */