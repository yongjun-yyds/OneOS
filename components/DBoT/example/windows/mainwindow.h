#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "ui_mainwindow.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

#define RTP_PT_JPEG      26
#define JPEG_BUF_SIZE       50 * 1024

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    ~MainWindow();
    static MainWindow *get_instance(void);
    Ui::MainWindow *ui;
    void notice_box(QString title, QString text);

private slots:
    void device_tree_doublieclick(QTreeWidgetItem *item, int colum);
    void device_tree_check(QTreeWidgetItem *item, int colum);
    void dataflow_flush_service(int);
    void discovery(void);
    void select_file(void);
    void progress(int percent);
    void notice(QString content);
    void dataflow_start(void);
    void dataflow_stop(void);
    void write_file(QString dev_id, QString filename);
    void list_dir(QString dev_id, QString dirname);
    void fresh_jpeg(uint8_t *data, int len);
    
signals:
    void notice_user(QString content);

private:
    MainWindow(QWidget *parent = nullptr);
    static MainWindow *instance;
    uint8_t *jpeg_buf;
    int jpeg_len;
    uint16_t jpeg_seq;
    uint32_t jpeg_ts;

    uint8_t *m_audio_buf;
    uint32_t m_audio_seq;
    uint32_t m_audio_size;
    void refresh_device_tree(void);
};
#endif // MAINWINDOW_H
