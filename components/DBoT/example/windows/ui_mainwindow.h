/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTreeWidget>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QComboBox>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QWidget *notice_widget;
    QWidget *file_widget;
    QWidget *toolbar_widget;

    QMenuBar *menubar;
    QStatusBar *statusbar;
    QLabel *video_label;
    QTextBrowser *notice_browser;
    QTreeWidget *device_tree;

    QPushButton *discovery_button;
    QPushButton *dataflow_start_button;
    QPushButton *dataflow_stop_button;
    QComboBox *dataflow_src_dev;
    QComboBox *dataflow_src_service;
    QComboBox *dataflow_dst_dev;

    QPushButton *select_button;
    QLabel *select_label;
    QProgressBar *progress_bar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(800, 600);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 800, 21));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);

        toolbar_widget = new QWidget(centralwidget);
        toolbar_widget->setGeometry(0, 300, 800, 100);
        file_widget = new QWidget(centralwidget);
        file_widget->setGeometry(0, 400, 800, 100);
        notice_widget = new QWidget(centralwidget);
        notice_widget->setGeometry(0, 470, 800, 100);

        // 左上视频区
        video_label = new QLabel("视频", centralwidget);
        video_label->setGeometry(QRect(20, 20, 320, 240));

        // 右上设备能力区
        device_tree = new QTreeWidget(MainWindow);
        device_tree->setGeometry(QRect(460, 20, 320, 240));
        device_tree->setColumnCount(1);
        device_tree->setHeaderLabel("设备-能力-属性");

        // 下三提示区
        notice_browser = new QTextBrowser(notice_widget);
        notice_browser->setGeometry(QRect(20, 20, 760, 80));

        // 下二文件选择及进度区
        select_button = new QPushButton("选择文件", file_widget);
        select_button->setGeometry(QRect(20, 20, 80, 30));
        select_label = new QLabel("文件", file_widget);
        select_label->setGeometry(QRect(120, 20, 660, 30));
        progress_bar = new QProgressBar(file_widget);
        progress_bar->setGeometry(QRect(20, 50, 760, 30));
        progress_bar->setRange(0, 100);


        // 下一功能按钮区
        discovery_button = new QPushButton("发现设备", toolbar_widget);
        discovery_button->setGeometry(QRect(20, 20, 80, 30));

        dataflow_start_button = new QPushButton("开始流转", toolbar_widget);
        dataflow_start_button->setGeometry(QRect(20, 50, 80, 30));
        dataflow_src_dev = new QComboBox(toolbar_widget);
        dataflow_src_dev->setGeometry(QRect(120, 50, 80, 30));
        dataflow_src_service = new QComboBox(toolbar_widget);
        dataflow_src_service->setGeometry(QRect(220, 50, 80, 30));
        dataflow_dst_dev = new QComboBox(toolbar_widget);
        dataflow_dst_dev->setGeometry(QRect(320, 50, 80, 30));
        dataflow_stop_button = new QPushButton("停止流转", toolbar_widget);
        dataflow_stop_button->setGeometry(QRect(420, 50, 80, 30));

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "DBoT", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
