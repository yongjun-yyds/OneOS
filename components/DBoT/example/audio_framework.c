#include <stdio.h>
#include <stdlib.h>
#include <device.h>
#include <audio/audio.h>
#include "audio_framework.h"
#include "ring_buff.h"
#include "dbot_errno.h"
#include "dbot_log.h"
#include "dbot_common.h"

static int s_audio_player_status = 0;
static int s_audio_recorder_status = 0;
static AUDIO_PLAYER_S s_audio_player;
static AUDIO_RECORDER_S s_audio_recorder;

static os_tick_t s_audio_player_period = 0;
static int s_audio_player_bytes = 0;
static os_tick_t s_audio_recorder_period = 0;
static int s_audio_recorder_bytes = 0;

int put_audio_player_data(uint8_t *data, int len)
{
    RETURN_IF_NULL(data, DBOT_INVALID_PARAM);

    int ret = 0;
    if(s_audio_player_status)
    {
        int free_space_size = rb_ring_buff_space_len(s_audio_player.cache);
        if(0 < free_space_size && 0 < len)
        {
            ret = rb_ring_buff_put(s_audio_player.cache, data, MIN(len, free_space_size));
        }
    }
    return ret;
}

int get_audio_player_data(uint8_t *data, int len)
{
    RETURN_IF_NULL(data, DBOT_INVALID_PARAM);

    int ret = 0;
    if(s_audio_player_status)
    {
        int total_space_size = rb_ring_buff_data_len(s_audio_player.cache);
        if(0 < total_space_size && 0 < len)
        {
            ret = rb_ring_buff_get(s_audio_player.cache, data, MIN(len, total_space_size));
        }
    }
    return ret;
}

void free_audio_player_data(void)
{
    if(NULL != s_audio_player.cache)
    {
        rb_ring_buff_destroy(s_audio_player.cache);
        s_audio_player.cache = NULL;
    }
    if(NULL != s_audio_player.device)
    {
        os_device_close(s_audio_player.device);
    }
}

static void *audio_player_thread(void *param)
{
    int index = 0;
    int len = 0;
    uint8_t *data = malloc(OS_AUDIO_REPLAY_MP_BLOCK_SIZE);
    OS_ASSERT(data);
    DBOT_LOG_D("audio framework started\r\n");
    while(1)
    {
        if(s_audio_player.quit_flag)
        {
            break;
        }
        if(os_tick_get_value() - s_audio_player_period >= 10000)
        {
            DBOT_LOG_D("Audio player write %d bytes into speaker in 10 seconds, remain cache %d bytes\r\n", s_audio_player_bytes, rb_ring_buff_data_len(s_audio_player.cache));
            s_audio_player_bytes = 0;
            s_audio_player_period = os_tick_get_value();
        }
        memset(data, 0, OS_AUDIO_REPLAY_MP_BLOCK_SIZE);
        len = get_audio_player_data(data, OS_AUDIO_REPLAY_MP_BLOCK_SIZE);
        if(0 < len)
        {
            index = 0;
            while(index < len)
            {
                len = os_device_write_block(s_audio_player.device, 0, data + index, len - index);
                if(0 < len)
                {
                    index += len;
                    s_audio_player_bytes += len;
                }
                else
                {
                    continue;
                }
            }
        }
        dbot_msleep(1); 
    }
    FREE_POINTER(data);
    free_audio_player_data();
    s_audio_player_status = 0;
    DBOT_LOG_D("audio framework quit\r\n");
    return NULL;
}

int start_audio_player(char *audio_dev, int samplerate, int channel, int bits, int volume)
{
    RETURN_IF_NULL(audio_dev, DBOT_INVALID_PARAM);
    
    if(!s_audio_player_status)
    {
        memset(&s_audio_player, 0, sizeof(s_audio_player));

        struct os_audio_caps caps   = {0};
        s_audio_player.device = os_device_find(audio_dev);
        OS_ASSERT(s_audio_player.device);

        caps.config_type             = AUDIO_PARAM_CMD;
        caps.udata.config.samplerate = samplerate;
        caps.udata.config.channels   = channel;
        caps.udata.config.samplebits = bits;
        os_device_control(s_audio_player.device, AUDIO_CTL_CONFIGURE, &caps);
        caps.config_type = AUDIO_VOLUME_CMD;
        caps.udata.value = volume;
        os_device_control(s_audio_player.device, AUDIO_CTL_CONFIGURE, &caps);
        OS_ASSERT(OS_SUCCESS == os_device_open(s_audio_player.device));

        s_audio_player.quit_flag = 0;
        s_audio_player.cache = rb_ring_buff_create(DBOT_AUDIO_PLAYER_RINGBUFF_SIZE);
        OS_ASSERT(s_audio_player.cache);

        pthread_t pid;
        pthread_attr_t attr;
        pthread_attr_init(&attr);
        pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
        pthread_attr_setstacksize(&attr, 2048);
        struct sched_param sched;
        sched.sched_priority = 30;
        pthread_attr_setschedparam(&attr, &sched);
        OS_ASSERT(0 == pthread_create(&pid, &attr, audio_player_thread, NULL));

        s_audio_player_status = 1;
        s_audio_player_period = os_tick_get_value();
        s_audio_player_bytes = 0;
    }
    return DBOT_OK;
}

int stop_audio_player(void)
{
    if(s_audio_player_status)
    {
        s_audio_player.quit_flag = 1;
    }
    return 0;
}

int put_audio_recorder_data(uint8_t *data, int len)
{
    RETURN_IF_NULL(data, DBOT_INVALID_PARAM);

    int ret = 0;
    if(s_audio_recorder_status)
    {
        int free_space_size = rb_ring_buff_space_len(s_audio_recorder.cache);
        if(0 < free_space_size && 0 < len)
        {
            ret = rb_ring_buff_put(s_audio_recorder.cache, data, MIN(len, free_space_size));
        }
    }
    return ret;
}

int get_audio_recorder_data(uint8_t *data, int len)
{
    RETURN_IF_NULL(data, DBOT_INVALID_PARAM);

    int ret = 0;
    if(s_audio_recorder_status)
    {
        int total_space_size = rb_ring_buff_data_len(s_audio_recorder.cache);
        if(0 < total_space_size && 0 < len)
        {
            ret = rb_ring_buff_get(s_audio_recorder.cache, data, MIN(len, total_space_size));
        }
    }
    return ret;
}

void free_audio_recorder_data(void)
{
    if(NULL != s_audio_recorder.cache)
    {
        rb_ring_buff_destroy(s_audio_recorder.cache);
        s_audio_recorder.cache = NULL;
    }
    if(NULL != s_audio_recorder.device)
    {
        os_device_close(s_audio_recorder.device);
    }
}

static void *audio_recorder_thread(void *param)
{
    int len = 0;
    uint8_t *data = malloc(RECORD_CHUNK_SZ);
    OS_ASSERT(data);
    DBOT_LOG_D("audio framework started\r\n");
    while(1)
    {
        if(s_audio_recorder.quit_flag)
        {
            break;
        }
        if(os_tick_get_value() - s_audio_recorder_period >= 10000)
        {
            DBOT_LOG_D("Audio recorder read %d bytes from mic in 10 seconds, remain cache %d bytes\r\n", s_audio_recorder_bytes, rb_ring_buff_data_len(s_audio_recorder.cache));
            s_audio_recorder_bytes = 0;
            s_audio_recorder_period = os_tick_get_value();
        }
        memset(data, 0, RECORD_CHUNK_SZ);
        len = os_device_read_nonblock(s_audio_recorder.device, 0, data, RECORD_CHUNK_SZ);
        if(0 < len)
        {
            put_audio_recorder_data(data, len);
            s_audio_recorder_bytes += len;
        }
        dbot_msleep(1);    
    }
    FREE_POINTER(data);
    free_audio_recorder_data();
    s_audio_recorder_status = 0;
    DBOT_LOG_D("audio framework quit\r\n");
    return NULL;
}

int start_audio_recorder(char *audio_dev)
{
    RETURN_IF_NULL(audio_dev, DBOT_INVALID_PARAM);

    if(!s_audio_recorder_status)
    {
        memset(&s_audio_recorder, 0, sizeof(s_audio_recorder));

        struct os_audio_caps caps   = {0};
        s_audio_recorder.device = os_device_find(audio_dev);
        OS_ASSERT(s_audio_recorder.device);

        caps.config_type             = AUDIO_PARAM_CMD;
        caps.udata.config.samplerate = OS_AUDIO_SAMPLERATE;
        caps.udata.config.channels   = OS_AUDIO_CHANNEL;
        caps.udata.config.samplebits = RECORD_BITS;
        os_device_control(s_audio_recorder.device, AUDIO_CTL_CONFIGURE, &caps);
        OS_ASSERT(OS_SUCCESS == os_device_open(s_audio_recorder.device));

        s_audio_recorder.quit_flag = 0;
        s_audio_recorder.cache = rb_ring_buff_create(DBOT_AUDIO_RECORDER_RINGBUFF_SIZE);
        OS_ASSERT(s_audio_recorder.cache);

        pthread_t pid;
        pthread_attr_t attr;
        pthread_attr_init(&attr);
        pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
        pthread_attr_setstacksize(&attr, 2048);
        struct sched_param sched;
        sched.sched_priority = 30;
        pthread_attr_setschedparam(&attr, &sched);
        OS_ASSERT(0 == pthread_create(&pid, &attr, audio_recorder_thread, NULL));

        s_audio_recorder_status = 1;
        s_audio_recorder_period = os_tick_get_value();
        s_audio_recorder_bytes = 0;
    }
    return DBOT_OK;
}

int stop_audio_recorder(void)
{
    if(s_audio_recorder_status)
    {
        s_audio_recorder.quit_flag = 1;
    }
    return 0;
}
