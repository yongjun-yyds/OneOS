#include <oneos_config.h>
#include <device.h>
#include <graphic.h>
#include <drv_jpeg.h>
#include <graphic.h>
#include <cam_hal.h>

static os_device_t *disp_dev = 0;
static uint8_t Jpeg_HWDecodingEnd = 0;
static uint8_t dma2d_trans_ok     = 0;

extern int cam_verify_jpeg_eoi(const uint8_t *inbuf, uint32_t length);

static os_err_t enable_lcd(void)
{
    if(NULL == disp_dev)
    {
        disp_dev = os_device_find(OS_GUI_DISP_DEV_NAME);
        OS_ASSERT(disp_dev);
        os_device_open(disp_dev);
        extern void stm32_lcd_display_on(struct os_device * dev, os_bool_t on_off);
        stm32_lcd_display_on(disp_dev, 1);
    }
    return OS_SUCCESS;
}

void Jpeg_Decode_CpltCallback(void)
{
    Jpeg_HWDecodingEnd = 1;
}

void dma2d_trans_complete(void)
{
    dma2d_trans_ok = 1;
}

static void gpu_info_set(struct os_device_gpu_info *gpu_info,JPEG_ConfTypeDef jpeg_info,os_graphic_t *graphic,char *source)
{
    uint32_t      xPos = 0;
    uint32_t      yPos = 0;

    gpu_info->alpha = 256;
    gpu_info->src_format = OS_GRAPHIC_PIXEL_FORMAT_YUV;
    gpu_info->yuv_sampling_mode = jpeg_info.ChromaSubsampling;
    gpu_info->width = jpeg_info.ImageWidth;
    gpu_info->height = jpeg_info.ImageHeight;

    xPos = (graphic->info.width - jpeg_info.ImageWidth)/2;
    yPos = (graphic->info.height - jpeg_info.ImageHeight)/2;
    /* centre operation, 2:bytes of per_pixel */
    gpu_info->dest = (char *)((uint32_t)graphic->info.framebuffer_curr + ((yPos * graphic->info.width) + xPos) * 2);
    gpu_info->src = source;
}

int jpeg_hardware_decode(uint8_t *pic_data, uint32_t pic_data_len)
{
    int32_t jpegstart = 0;
    uint32_t jpeg_len;
    struct stm32_jpeg_info *stm32_jpeg;
    os_graphic_t *graphic;
    uint8_t *source_buff;
    uint8_t *yuv_buff;
    uint32_t yuv_size;
    struct os_device_gpu_info gpu_info;
    JPEG_ConfTypeDef jpeg_info;
    
    yuv_size = OS_GRAPHIC_LCD_WIDTH*OS_GRAPHIC_LCD_HEIGHT*2+102400;
    yuv_buff = os_dma_malloc_align(yuv_size, 32);
    if(NULL == yuv_buff)
    {
        return -1;
    }
    
    stm32_jpeg = (struct stm32_jpeg_info *)os_device_find("jpeg");
    enable_lcd();
    graphic  = (os_graphic_t *)disp_dev;
    uint8_t count =0;
    uint32_t time_count = 0;

    source_buff = pic_data;
    jpegstart = cam_verify_jpeg_soi(pic_data, pic_data_len);
    if(cam_verify_jpeg_soi(pic_data, pic_data_len)==-1 || cam_verify_jpeg_eoi(pic_data, pic_data_len)==-1)
    {
        // os_kprintf("jpeg_data_error\r\n");
        os_dma_free_align(yuv_buff);
        return -1;
    }
    jpeg_len = pic_data_len - jpegstart;
    
    memset(yuv_buff, 0, yuv_size);
    Jpeg_HWDecodingEnd = 0;
    dma2d_trans_ok = 0;
    time_count = 0;
    os_clean_dcache(source_buff,jpeg_len);
    jpeg_start_decode(stm32_jpeg->hjpeg,&source_buff[jpegstart],jpeg_len,yuv_buff,yuv_size);
    do
    {
        os_task_msleep(10);
        time_count++;
    }while(Jpeg_HWDecodingEnd == 0 && time_count <20);
    
    if(time_count>=20 )
    {
        // os_kprintf("error.time_count:%d\r\n",time_count);
        count++;
        os_dma_free_align(yuv_buff);
        return -1;
    }
    
    HAL_JPEG_GetInfo(stm32_jpeg->hjpeg, &jpeg_info); 
    // os_kprintf("jpeg info:with:%d,heigh:%d,jpeg_len:%d,count:%d\r\n",jpeg_info.ImageWidth,jpeg_info.ImageHeight,jpeg_len,count++);
    gpu_info_set(&gpu_info, jpeg_info, graphic,(char *)yuv_buff);
    os_device_control(disp_dev, OS_GRAPHIC_CTRL_GPU_BLEND, (void *)&gpu_info);
    os_device_control(disp_dev, OS_GRAPHIC_CTRL_FRAME_FLUSH, OS_NULL);
    while(dma2d_trans_ok == 0)
    {
        os_task_msleep(10);
    }
    os_dma_free_align(yuv_buff);
    return 0;
}



