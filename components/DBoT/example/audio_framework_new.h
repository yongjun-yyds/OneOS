#include "oneos_config.h"
#include "ring_buff.h"
#include "device.h"

#if 0
#define AUDIO_PLAYER_RINGBUFF_SIZE      (1024 * 160)    //audio player ring buffer大小
#define AUDIO_RECEIVE_DATA_BUFF_SIZE    (1024 * 16)     //APP发送播放数据到frame层的缓存大小
#define AUDIO_PLAY_DATA_BUFF_SIZE       (1024 * 16)     //player播放数据的缓存大小


#define AUDIO_RECORDER_RINGBUFF_SIZE    (1024 * 160)    //audio recorder ring buffer大小
#define AUDIO_SEND_DATA_BUFF_SIZE       (1024 * 16)     //frame发送录音数据到APP层的缓存大小
#define AUDIO_RECORD_DATA_BUFF_SIZE     (1024 * 16)     //recorder录音数据的缓存大小
#endif
#define RECORD_CHUNK_SZ     ((OS_AUDIO_SAMPLERATE * OS_AUDIO_CHANNEL * 2) * 20 / 1000)

#define AUDIO_PLAYER_RINGBUFF_SIZE      10240
#define AUDIO_RECEIVE_DATA_BUFF_SIZE    1024
#define AUDIO_PLAY_DATA_BUFF_SIZE       1024


#define AUDIO_RECORDER_RINGBUFF_SIZE    10240
#define AUDIO_SEND_DATA_BUFF_SIZE       1024
#define AUDIO_RECORD_DATA_BUFF_SIZE     1024

#define AUDIO_CALLBACK_INTERVAL         20

#ifndef MIN
#define MIN(a, b) ((a) < (b) ? (a) : (b))
#endif /* MIN */

#define FILE_NAME_MAX           256

/**
 *  框架层通知给应用层的消息类型
**/
typedef enum
{
    EVENT_NULL              = 0x00,
    EVENT_REQ_SEND          = 0x01,     //通知APP发送数据 
    EVENT_REQ_RECV          = 0x02,     //通知APP接收数据
    EVENT_FILE_DONE         = 0x03,     //通知APP文件播放完成
}audio_event_t;

typedef enum
{
    MSG_DATA_NULL           = 0x00,
    MSG_PLAY_STREAM         = 0x01,     //通知框架层播放流数据
}audio_msg_t;
    
typedef enum
{
    DATA_TRANS_NO_START     = 0x00,     //数据传输未开始
    DATA_TRANS_RUNNING      = 0x01,     //数据传输正在进行
    DATA_TRANS_FINISHED     = 0x02,     //数据传输已完成
}trans_statust_t;

typedef enum
{
    PLAYER_NO_CREATED       = 0x00,     //播放器未创建
    PLAYER_RUNNING          = 0x01,     //播放器正在运行
    PLAYER_PAUSE            = 0x02,     //播放器已暂停
}player_status_t; 

typedef enum
{
    RECORDER_NO_CREATED     = 0x00,     //录音器未创建
    RECORDER_RUNNING        = 0x01,     //录音器正在运行
    RECORDER_PAUSE          = 0x02,     //录音器已暂停
}recorder_status_t;

/**
 * @brief           :   audio player event callback
 *                      the APP layer communicates with the framework layer through this callback function
 * 
 * @param event     :   event is notified from framework layer to APP layer 
 * @param trans_data:   audio data can be put at this adrress
 * @param len       :   the length of data that can be transfer from APP layer to the framework layer
 *
 * @return          :   notify framework layer of data transfer status 
**/
typedef audio_msg_t (*trans_data_cb)(audio_event_t event, uint8_t* trans_data, uint32_t* trans_len);
typedef os_device_t audio_player_t;
typedef os_device_t audio_recorder_t;
typedef struct
{
    int samplerate;
    int bits;
    int channel;
    int format;
}audio_conf_t;

typedef struct
{
    audio_player_t      *player;
    audio_conf_t        config;
    rb_ring_buff_t      *data_cache; 
    uint8_t          *trans_data;
    uint32_t         *trans_len;    
    uint8_t          *play_data;
    player_status_t    player_status;
    audio_msg_t         (* cb)(audio_event_t event, uint8_t* trans_data, uint32_t* trans_len);
    audio_msg_t         app_msg;
}uis_player_t;

typedef struct
{
    audio_recorder_t    *recorder;
    audio_conf_t        config;
    rb_ring_buff_t      *data_cache; 
    uint8_t          *trans_data;
    uint32_t         *trans_len;    
    uint8_t          *record_data;
    recorder_status_t  recorder_status;
    audio_msg_t         (* cb)(audio_event_t event, uint8_t* trans_data, uint32_t *trans_len);
    audio_msg_t         app_msg;
}uis_recorder_t;


/* player API function */
os_err_t audio_start_play_stream(audio_conf_t config, trans_data_cb callback);
void audio_stop_play(void);
os_err_t audio_set_player_volume(uint32_t vol);

/* recorder API function */
os_err_t audio_start_record_stream(audio_conf_t config, trans_data_cb callback);
void audio_stop_record(void);


