#ifndef __EXAMPLE_CMD_H__
#define __EXAMPLE_CMD_H__

#ifdef __cplusplus
extern "C" {
#endif

int dbot_audio_dataflow(int argc, char **argv);
int dbot_audio_dataflow_stop(int argc, char **argv);
int dbot_video_dataflow(int argc, char **argv);
int dbot_video_dataflow_stop(int argc, char **argv);
int dbot_write_file(int argc, char **argv);
int dbot_show_directory(int argc, char **argv);

#ifdef __cplusplus
}
#endif

#endif /* __EXAMPLE_CMD_H__ */