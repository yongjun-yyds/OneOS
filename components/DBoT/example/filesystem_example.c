#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include "cJSON.h"
#include "dbot_errno.h"
#include "dbot_log.h"
#include "publish_service.h"
#include "dbot_common.h"
#include "dbot_service.h"

#include "pb_encode.h"
#include "pb_decode.h"
#include "fileproto.pb.h"
#include "dbot_nanopb.h"
#include "crc-ccitt.h"

#ifdef FILESYSTEM_USING_SHOW_DIRECTORY

#define SHOW_DIRECTORY_THREAD_STACK_SIZE 4096
#define SHOW_DIRECTORY_PATH_LEN_MAX      128
static char                   show_directory_path[SHOW_DIRECTORY_PATH_LEN_MAX] = {0};
static pthread_t              gs_show_directory_pid                             = 0;
static DTP_SERVICE_CONTEXT_S *gs_show_directory_dtp_ctx                         = NULL;

void free_show_directory_task_resource(void)
{
    if (NULL != gs_show_directory_dtp_ctx)
    {
        dbot_service_destory_dtp(gs_show_directory_dtp_ctx);
        gs_show_directory_dtp_ctx = NULL;
    }
}

bool server_list_files_response_callback(pb_ostream_t *ostream, const pb_field_iter_t *field)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
{
    DIR           *dir = *(DIR **)field->pData;
    struct dirent *file;
    FileInfo       fileinfo = {0};
    struct stat    stat_buf = {0};

    while ((file = readdir(dir)) != NULL)
    {
        fileinfo.inode = file->d_ino;
        strncpy(fileinfo.name, file->d_name, sizeof(fileinfo.name));
        fileinfo.name[sizeof(fileinfo.name) - 1] = '\0';
        fileinfo.type                            = file->d_type;
        if (file->d_type != DT_DIR)
        {
            memset(&stat_buf, 0, sizeof(struct stat));
            if (stat(file->d_name, &stat_buf) == 0)
            {
                fileinfo.size = stat_buf.st_size;
            }
            else
            {
                fileinfo.size = 0;
                DBOT_LOG_E("bad file: %s\r\n", file->d_name);
            }
        }
        else
        {
            fileinfo.size = 0;
        }

        /* This encodes the header for the field, based on the constant info from pb_field_t */
        if (!pb_encode_tag_for_field(ostream, field))
            return false;

        /* This encodes the data for the field, based on our FileInfo structure */
        if (!pb_encode_submessage(ostream, FileInfo_fields, &fileinfo))
            return false;
    }

    /* Because the main program uses pb_encode_delimited(), this callback will be
     * called twice. Rewind the directory for the next call */
    rewinddir(dir);

    return true;
}

/* Client send a ListFilesRequest, server will response with a ListFilesResponse message */
static void *show_directory_thread(void *param)
{
    int32_t result    = 0;
    DIR    *directory = NULL;

    /* Recv and decode ListFilesRequest, and open the requested directory */
    {
        ListFilesRequest list_files_request = {0};

        result = dbot_nanopb_msg_input((void *)gs_show_directory_dtp_ctx, ListFilesRequest_fields, &list_files_request);
        if (result == DBOT_ERROR)
        {
            DBOT_LOG_E("dbot nanopb msg input failed\r\n");
            goto __err;
        }

        directory = opendir(list_files_request.path);
        DBOT_LOG_D("listing directory: %s\r\n", list_files_request.path);
    }

    /* List the files in the directory and transmit the response to client */
    {
        ListFilesResponse list_files_response = {0};

        if (directory == NULL)
        {
            list_files_response.has_path_error = true;
            list_files_response.path_error     = true;
            DBOT_LOG_E("open directory failed, path error\r\n");
        }
        else
        {
            list_files_response.has_path_error = false;
            list_files_response.file           = directory;
        }

        result = dbot_nanopb_msg_output((void *)gs_show_directory_dtp_ctx, ListFilesResponse_fields, &list_files_response);
        if (result != DBOT_OK)
        {
            DBOT_LOG_E("dbot nanopb msg output failed\r\n");
            goto __err;
        }
    }

__err:
    if (directory != NULL)
    {
        closedir(directory);
    }

    free_show_directory_task_resource();
    gs_show_directory_pid = 0;
    DBOT_LOG_D("show directory info task quit\r\n");

    return NULL;
}

int show_directory(int len, char *req, char **resp)
{
    int    ret      = DBOT_ERROR;
    cJSON *root     = NULL;
    cJSON *path     = NULL;
    cJSON *res_root = NULL;

    RETURN_IF_NULL(req, DBOT_INVALID_PARAM);

    if (0 != gs_show_directory_pid)
    {
        DBOT_LOG_E("show directory task already exists\r\n");
        return DBOT_ERROR;
    }

    do
    {
        root = cJSON_Parse(req);
        BREAK_IF(NULL == root);
        path = cJSON_GetObjectItem(root, "path");
        BREAK_IF(NULL == path);
        memset(show_directory_path, 0, SHOW_DIRECTORY_PATH_LEN_MAX);
        strncpy(show_directory_path, path->valuestring, SHOW_DIRECTORY_PATH_LEN_MAX);

        gs_show_directory_dtp_ctx = dbot_service_init_dtp(DTP_SERVICE_TCP_SERVER, NULL, 0);
        BREAK_IF(NULL == gs_show_directory_dtp_ctx);

        pthread_attr_t attr;
        pthread_attr_init(&attr);
        pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
        pthread_attr_setstacksize(&attr, SHOW_DIRECTORY_THREAD_STACK_SIZE);
        BREAK_IF(0 != pthread_create(&gs_show_directory_pid, &attr, show_directory_thread, NULL))

        res_root = cJSON_CreateObject();
        if (NULL != res_root)
        {
            cJSON_AddStringToObject(res_root, "ip", dbot_service_get_ctx()->profile->device_ip);
            cJSON_AddNumberToObject(res_root, "port", gs_show_directory_dtp_ctx->port);
            cJSON_AddNumberToObject(res_root, "protocol", 1);
            *resp = cJSON_PrintUnformatted(res_root);
            cJSON_Delete(res_root);
        }
        ret = DBOT_OK;

        DBOT_LOG_E("show directory task started\r\n");
    } while (0);

    DELETE_CJSON(root);
    if (DBOT_OK != ret)
    {
        free_show_directory_task_resource();
    }

    return ret;
}
#endif /* FILESYSTEM_USING_SHOW_DIRECTORY */


#ifdef FILESYSTEM_USING_WRITE_FILE

#define FILESYSTEM_THREAD_STACK_SIZE 5120
#define FILE_NAME_MAX_LEN    128
#define FILE_FLOW_PACKAGE_FIX_SIZE    1460

typedef struct file_package_node
{
    os_list_node_t node;
    uint8_t *ptr;
    uint32_t size;
    uint32_t offset;
    uint32_t seq;
}file_package_node_t;

typedef struct file_flow_cache
{
    uint8_t  file_name[FILE_NAME_MAX_LEN];
    uint8_t  file_type;
    uint32_t file_expect_size;
    uint32_t file_actual_size;
    uint8_t  sha1[20];
    os_list_node_t file_package_list;
}file_flow_cache_t;

static pthread_t              gs_write_file_pid     = 0;
static DTP_SERVICE_CONTEXT_S *gs_write_file_dtp_ctx = NULL;
static FILE                  *gs_write_file_fp      = NULL;
static uint32_t               gs_recv_file_size     = 0;
static uint32_t               gs_sequence             = 0;
static file_flow_cache_t      gs_file_flow_cache   = {0};
static pthread_mutex_t        file_flow_cache_list_lock;
static pthread_t              gs_read_file_flow_cache_pid  = 0;

static void file_flow_cache_lock(void)
{
    pthread_mutex_lock(&file_flow_cache_list_lock);
}

static inline void file_flow_cache_unlock(void)
{
    pthread_mutex_unlock(&file_flow_cache_list_lock);
}

void free_write_file_task_resource(void)
{
    if (NULL != gs_write_file_dtp_ctx)
    {
        dbot_service_destory_dtp(gs_write_file_dtp_ctx);
        gs_write_file_dtp_ctx = NULL;
    }
}

static bool file_flow_decode_repeated_payload_callback(pb_istream_t *istream, const pb_field_t *field, void **arg)
{
    //FILE     *file_fp   = (FILE *)(*arg);
    file_package_node_t *file_cache_pkg_node = NULL;
    uint8_t *payload   = NULL;
    size_t len         = istream->bytes_left;


    payload = calloc(1, FILE_FLOW_PACKAGE_FIX_SIZE);
    if (NULL == payload)
    {
        DBOT_LOG_E("file flow package payload calloc failed\r\n");
        return false;
    }

    if (len > FILE_FLOW_PACKAGE_FIX_SIZE)
    {
        free(payload);
        DBOT_LOG_E("recv file flow package too large\r\n");
        return false;
    }

    if(!pb_read(istream, payload, len))
    {
        free(payload);
        DBOT_LOG_E("read istream payload failed\r\n");
        return false;
    }

    file_cache_pkg_node = calloc(1, sizeof(file_package_node_t));
    if (NULL == file_cache_pkg_node)
    {
        free(payload);
        DBOT_LOG_E("file flow cache node calloc failed\r\n");
        return false;
    }

    file_cache_pkg_node->ptr = payload;
    file_cache_pkg_node->size = len;
    file_cache_pkg_node->offset = gs_recv_file_size;
    file_cache_pkg_node->seq = gs_sequence++;

    os_list_init(&file_cache_pkg_node->node);
    file_flow_cache_lock();
    os_list_add_tail(&gs_file_flow_cache.file_package_list, &file_cache_pkg_node->node);
    file_flow_cache_unlock();

    gs_recv_file_size += len;

    return true;
}

static void *read_file_flow_cache_thread_func(void *param)
{
    file_package_node_t *file_cache_pkg_node_tmp = NULL;
    uint32_t total_write_len = (uint32_t)-1;
    uint32_t write_len  = 0;

    pthread_mutex_init(&file_flow_cache_list_lock, NULL);
    memset(&gs_file_flow_cache, 0, sizeof(file_flow_cache_t));
    os_list_init(&gs_file_flow_cache.file_package_list);

    while(total_write_len != gs_file_flow_cache.file_actual_size)
    {
        if (!os_list_empty(&gs_file_flow_cache.file_package_list))
        {
            file_flow_cache_lock();
            file_cache_pkg_node_tmp = os_list_first_entry_or_null(&gs_file_flow_cache.file_package_list, file_package_node_t, node);
            os_list_del(&file_cache_pkg_node_tmp->node);
            file_flow_cache_unlock();
            if (file_cache_pkg_node_tmp != NULL)
            {
                if (file_cache_pkg_node_tmp->ptr != NULL)
                {
                    write_len = fwrite(file_cache_pkg_node_tmp->ptr, 1, file_cache_pkg_node_tmp->size, gs_write_file_fp);
                    if (total_write_len == (uint32_t)-1)
                    {
                        total_write_len = 0;
                    }
                    total_write_len += write_len;
                    free(file_cache_pkg_node_tmp->ptr);
                }
                free(file_cache_pkg_node_tmp);
            }
        }
        else
        {
            dbot_msleep(10);
        }
    }

    pthread_mutex_destroy(&file_flow_cache_list_lock);
    if (NULL != gs_write_file_fp)
    {
        fclose(gs_write_file_fp);
        gs_write_file_fp = NULL;
    }
    DBOT_LOG_I("file flow cache task write file %d bytes\r\n", total_write_len);

    return NULL;
}

static void *write_file_thread(void *param)
{
    FileFlow file_flow      = {0};
    int32_t  result         = 0;

    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
    pthread_attr_setstacksize(&attr, 2048);
    if(0 != pthread_create(&gs_read_file_flow_cache_pid, &attr, read_file_flow_cache_thread_func, NULL))
    {
        DBOT_LOG_E("dbot creat file flow cache thread failed\r\n");
        goto __err;
    }

    file_flow.payload.funcs.decode = &file_flow_decode_repeated_payload_callback;
    file_flow.payload.arg = gs_write_file_fp;
    result = dbot_nanopb_msg_input((void *)gs_write_file_dtp_ctx, FileFlow_fields, &file_flow);
    if (result != DBOT_OK)
    {
        DBOT_LOG_E("dbot nanopb msg input failed\r\n");
    }

    if (file_flow.has_header == true)
    {
        strncpy((char *)gs_file_flow_cache.file_name, file_flow.header.name, sizeof(gs_file_flow_cache.file_name));
        gs_file_flow_cache.file_type = file_flow.header.type;
        gs_file_flow_cache.file_expect_size = file_flow.header.size;
        if (gs_recv_file_size != 0)
        {
            gs_file_flow_cache.file_actual_size = gs_recv_file_size;
        }
        else
        {
            gs_file_flow_cache.file_actual_size = (uint32_t)-1;
        }
        if (file_flow.header.has_sha1 == true)
        {
            /* user can read file from filesystem and check sha1 */
            memcpy(gs_file_flow_cache.sha1, file_flow.header.sha1.bytes, file_flow.header.sha1.size);
        }
        DBOT_LOG_D("file_flow has head: file_name:%s, file_type:%d, file_expect_size:%d, file_actual_size:%d\r\n", gs_file_flow_cache.file_name, gs_file_flow_cache.file_type, gs_file_flow_cache.file_expect_size, gs_file_flow_cache.file_actual_size);
    }
    else
    {
        /* without head, error accur very early, read_file_flow_cache_thread also need return */
        gs_file_flow_cache.file_actual_size = (uint32_t)-1;
    }

__err:
    free_write_file_task_resource();
    gs_write_file_pid = 0;
    DBOT_LOG_D("recv file task quit, total received payload %d bytes\r\n", gs_recv_file_size);

    return NULL;
}

int write_file(int len, char *req, char **resp)
{
    int    ret       = DBOT_ERROR;
    cJSON *root      = NULL;
    cJSON *file_name = NULL;
    cJSON *res_root  = NULL;

    RETURN_IF_NULL(req, DBOT_INVALID_PARAM);

    if (0 != gs_write_file_pid)
    {
        DBOT_LOG_E("write file task already exists\r\n");
        return DBOT_ERROR;
    }

    do
    {
        root = cJSON_Parse(req);
        BREAK_IF(NULL == root);
        file_name = cJSON_GetObjectItem(root, "file_name");
        BREAK_IF(NULL == file_name);

        if (access(file_name->valuestring, 0) == 0)
        {
            DBOT_LOG_E("file %s already exists\r\n", file_name->valuestring);
            break;
        }
        gs_write_file_fp = fopen(file_name->valuestring, "wb+");
        BREAK_IF(NULL == gs_write_file_fp);

        gs_write_file_dtp_ctx = dbot_service_init_dtp(DTP_SERVICE_TCP_SERVER, NULL, 0);
        BREAK_IF(NULL == gs_write_file_dtp_ctx);

        pthread_attr_t attr;
        pthread_attr_init(&attr);
        struct sched_param param;
        param.sched_priority = 25;
        pthread_attr_setschedpolicy(&attr, SCHED_FIFO);
        pthread_attr_setschedparam(&attr, &param);
        pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
        pthread_attr_setstacksize(&attr, FILESYSTEM_THREAD_STACK_SIZE);
        BREAK_IF(0 != pthread_create(&gs_write_file_pid, &attr, write_file_thread, NULL))

        res_root = cJSON_CreateObject();
        if (NULL != res_root)
        {
            cJSON_AddStringToObject(res_root, "ip", dbot_service_get_ctx()->profile->device_ip);
            cJSON_AddNumberToObject(res_root, "port", gs_write_file_dtp_ctx->port);
            cJSON_AddNumberToObject(res_root, "protocol", 1);
            *resp = cJSON_PrintUnformatted(res_root);
            cJSON_Delete(res_root);
        }
        ret              = DBOT_OK;
        gs_recv_file_size = 0;
        gs_sequence       = 0;
        DBOT_LOG_E("write file task started\r\n");
    } while (0);

    DELETE_CJSON(root);
    if (DBOT_OK != ret)
    {
        free_write_file_task_resource();
    }

    return ret;
}
#endif /* FILESYSTEM_USING_WRITE_FILE */












#ifdef FILESYSTEM_USING_READ_FILE 
static DTP_SERVICE_CONTEXT_S *gs_read_file_ctx      = NULL;
static pthread_t              gs_read_file_pid      = 0;
static FILE                  *gs_read_file_fp       = NULL;
static char                   gs_read_file_name[100] = {0};


void free_read_file_resource(void)
{
    if (NULL != gs_read_file_ctx)
    {
        dbot_service_destory_dtp(gs_read_file_ctx);
        gs_read_file_ctx = NULL;
    }
    if (NULL != gs_read_file_fp)
    {
        fclose(gs_read_file_fp);
        gs_read_file_fp = NULL;
    }
}

static bool file_flow_encode_repeated_payload_callback(pb_ostream_t *ostream, const pb_field_t *field, void * const *arg)
{
    pb_byte_t payload[1460] = {0};
    uint32_t  read_len      = 0;
    FILE     *file_fp       = (FILE *)(*arg);

    if (ostream->callback != NULL)
    {
        dbot_msleep(3000);
    }

    while ((read_len = fread(payload, 1, sizeof(payload), file_fp)) > 0)
    {
        if (!pb_encode_tag_for_field(ostream, field))
            return false;

        if (!pb_encode_string(ostream, payload, read_len))
            return false;

        if (ostream->callback == NULL) /* callback NULL is for sizing */
        {
            continue;
        }
        dbot_msleep(40); /* waiting for opposite write in flash */
    }
    rewind(file_fp); /* this callback will come in twice, so need rewind */

    return true;
}

static void *read_file_thread_func(void *param)
{
    FileFlow    file_flow = {0};
    struct stat stat_buf  = {0};
    int32_t     result    = 0;

    file_flow.has_header = true;
    if (file_flow.has_header == true)
    {
        strncpy(file_flow.header.name, gs_read_file_name, sizeof(file_flow.header.name));
        stat(gs_read_file_name, &stat_buf);
#ifdef _WIN32
        file_flow.header.type = 0;
#else
        if (S_ISREG(stat_buf.st_mode))
            file_flow.header.type = DT_REG;
        else if (S_ISDIR(stat_buf.st_mode))
            file_flow.header.type = DT_DIR;
        else if (S_ISCHR(stat_buf.st_mode))
            file_flow.header.type = DT_CHR;
        else if (S_ISBLK(stat_buf.st_mode))
            file_flow.header.type = DT_BLK;
        else if (S_ISFIFO(stat_buf.st_mode))
            file_flow.header.type = DT_FIFO;
        else if (S_ISLNK(stat_buf.st_mode))
            file_flow.header.type = DT_LNK;
        else if (S_ISSOCK(stat_buf.st_mode))
            file_flow.header.type = DT_SOCK;
        else
            file_flow.header.type = DT_UNKNOWN;
#endif
        file_flow.header.size     = stat_buf.st_size;
        file_flow.header.has_sha1 = false;
    }
    else
    {
        /* if file_flow.has_header is false, file_flow.header will be ignore */
    }
    file_flow.payload.funcs.encode = &file_flow_encode_repeated_payload_callback;
    file_flow.payload.arg = gs_read_file_fp;

    result = dbot_nanopb_msg_output((void *)gs_read_file_ctx, FileFlow_fields, &file_flow);
    if (result != DBOT_OK)
    {
        DBOT_LOG_E("dbot nanopb msg output failed\r\n");
    }

    free_read_file_resource();
    gs_read_file_pid = 0;
    DBOT_LOG_D("read file task quit\r\n");

    return NULL;
}

static int read_file(int len, char *req, char **resp)
{   
	  int    ret       = DBOT_ERROR;
    cJSON *root      = NULL;
	  cJSON *res_root  = NULL;
    cJSON *file_name  = NULL;

    if (0 != gs_read_file_pid)
    {
        DBOT_LOG_E("read file task already exists\r\n");
        return DBOT_ERROR;
    }

    do
    {
        
		  	root = cJSON_Parse(req);
        BREAK_IF(NULL == root);
			  file_name = cJSON_GetObjectItem(root, "file_name");
        BREAK_IF(NULL == file_name);
			  if (access(file_name->valuestring, 0) != 0)
        {
            DBOT_LOG_E("file %s not exists\r\n", file_name->valuestring);
            break;
        }
				
				
				strncpy(gs_read_file_name,file_name->valuestring, sizeof(gs_read_file_name));
				gs_read_file_fp = fopen(gs_read_file_name, "rb");
        BREAK_IF(NULL == gs_read_file_fp);

        gs_read_file_ctx = dbot_service_init_dtp(DTP_SERVICE_TCP_SERVER, NULL, 0);
        BREAK_IF(NULL == gs_read_file_ctx);

        pthread_attr_t attr;
        pthread_attr_init(&attr);
        pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
        pthread_attr_setstacksize(&attr, FILESYSTEM_THREAD_STACK_SIZE);
        BREAK_IF(0 != pthread_create(&gs_read_file_pid, &attr, read_file_thread_func, NULL))

				res_root = cJSON_CreateObject();
				if (NULL != res_root)
        {
            cJSON_AddStringToObject(res_root, "ip", dbot_service_get_ctx()->profile->device_ip);
            cJSON_AddNumberToObject(res_root, "port", gs_read_file_ctx->port);
            cJSON_AddNumberToObject(res_root, "protocol", 1);
            *resp = cJSON_PrintUnformatted(res_root);
            cJSON_Delete(res_root);
        }
        ret              = DBOT_OK;
        DBOT_LOG_E("read file task started\r\n");
    } while (0);

    DELETE_CJSON(root);
    if (DBOT_OK != ret)
    {
        free_read_file_resource();
    }

    return ret;
}

#endif /* FILESYSTEM_USING_READ_FILE */












static ATTR_INFO_S filesystem_attrs[] = {
#ifdef FILESYSTEM_USING_SHOW_DIRECTORY
    {"show_directory", "{\"path\":\"/\"}", show_directory},
#endif
#ifdef FILESYSTEM_USING_WRITE_FILE
    {"write_file", "{\"file_name\":\"music.pcm\"}", write_file},
#endif
#ifdef FILESYSTEM_USING_READ_FILE
    {"read_file", "{\"file_name\":\"music.pcm\"}", read_file},
#endif
};

#if !defined __linux__ && !defined _WIN32
#include <vfs_fs.h>
int filesystem_init(void)
{
    vfs_mount("sd0", "/", "fat", 0, 0);
    return DBOT_OK;
}
#endif

int publish_filesystem(void)
{
    return dbot_service_add_service(CAPABILITY_TYPE_FILESYSTEM,
                                        sizeof(filesystem_attrs) / sizeof(filesystem_attrs[0]),
                                        filesystem_attrs);
}
