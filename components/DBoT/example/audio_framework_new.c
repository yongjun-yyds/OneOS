#include "os_task.h"
#include "device.h"
#include "audio/audio.h"
#include "audio_framework.h"
#include "ring_buff.h"

uis_player_t uis_player;
uis_recorder_t uis_recorder;

os_task_id uis_audio_receive_task = OS_NULL;
os_task_id uis_audio_play_task = OS_NULL;
os_task_id uis_audio_send_task = OS_NULL;
os_task_id uis_audio_record_task = OS_NULL;

extern os_err_t os_task_resume(os_task_id task);
extern os_err_t os_task_suspend(os_task_id task);
static void uis_audio_receive_thread(void *parameter);
static void uis_audio_play_thread(void *parameter);
static void uis_audio_record_thread(void *parameter);
static void uis_audio_send_thread(void *parameter);

audio_player_t *audio_player_create(char *device_name)
{
    return os_device_find(SPEAKER_DEV_NAME);
}

void audio_player_config(audio_player_t *player, audio_conf_t *config)
{
    struct os_audio_caps caps   = {0};
    caps.config_type             = AUDIO_PARAM_CMD;
    caps.udata.config.samplerate = config->samplerate;
    caps.udata.config.channels   = config->channel;
    caps.udata.config.samplebits = config->bits;
    os_device_control(player, AUDIO_CTL_CONFIGURE, &caps);
}

void audio_player_start_pipe(audio_player_t *player)
{
    OS_ASSERT(OS_SUCCESS == os_device_open(player));
}

void audio_player_write_pipe(audio_player_t *player, uint8_t *data, int size)
{
    os_device_write_block(player, 0, data, size);
}

void audio_player_stop(audio_player_t *player)
{
    os_device_close(player);
}

void audio_player_delete(audio_player_t *player)
{

}

void audio_player_set_volume(audio_player_t *player, int vol)
{
    struct os_audio_caps caps   = {0};
    caps.config_type = AUDIO_VOLUME_CMD;
    caps.udata.value = vol;
    os_device_control(player, AUDIO_CTL_CONFIGURE, &caps);
}


audio_recorder_t *audio_recorder_create(char *device_name)
{
    return os_device_find(RECORDER_DEV_NAME);
}

void audio_recorder_config(audio_recorder_t *recorder, audio_conf_t *config)
{
    struct os_audio_caps caps   = {0};
    caps.config_type             = AUDIO_PARAM_CMD;
    caps.udata.config.samplerate = config->samplerate;
    caps.udata.config.channels   = config->channel;
    caps.udata.config.samplebits = config->bits;
    os_device_control(recorder, AUDIO_CTL_CONFIGURE, &caps);
}

void audio_recorder_start_pipe(audio_recorder_t *recorder)
{
    OS_ASSERT(OS_SUCCESS == os_device_open(recorder));
}

void audio_recorder_write_pipe(audio_recorder_t *recorder, uint8_t *data, int size)
{
    os_device_write_block(recorder, 0, data, size);
}

void audio_recorder_stop(audio_recorder_t *recorder)
{
    os_device_close(recorder);
}

void audio_recorder_delete(audio_recorder_t *recorder)
{

}

/**
 * @brief           :   create audio player
 * 
 * @param config    :   configuration parameter of player
 * @param callback  :   callback function that notify APP layer to send data to play
 * 
 * @ret             :   pointer of audio player 
**/
static audio_player_t *create_player(audio_conf_t config, trans_data_cb callback)
{
    /* create player */
    uis_player.player = audio_player_create("player");
    OS_ASSERT(uis_player.player);

    /* config player */
    audio_player_config(uis_player.player, &config);

    /* init ring buffer */
    uis_player.data_cache = rb_ring_buff_create(AUDIO_PLAYER_RINGBUFF_SIZE);
    OS_ASSERT(uis_player.data_cache);

    /* init data transfer buffer */
    uis_player.trans_data = os_malloc(AUDIO_RECEIVE_DATA_BUFF_SIZE);
    OS_ASSERT(uis_player.trans_data);

    /* init data transfer length */
    uis_player.trans_len = os_malloc(1);
    OS_ASSERT(uis_player.trans_len);
    
    /* init play data buffer */
    uis_player.play_data = os_malloc(AUDIO_PLAY_DATA_BUFF_SIZE);
    OS_ASSERT(uis_player.play_data);

    /* register callback function */
    uis_player.cb = callback;
    
    
    return uis_player.player;
}


/**
 * @brief           :   delete audio player
 *
 * @param           :   None
 *
 * @ret             :   None
**/
static void delete_player(void)
{ 
    if (uis_player.player)
    {
        audio_player_stop(uis_player.player);
    
        audio_player_delete(uis_player.player);

        uis_player.player = OS_NULL;
    }

    if (uis_player.data_cache)
    {
        rb_ring_buff_destroy(uis_player.data_cache);

        uis_player.data_cache = OS_NULL;
    }

    if (uis_player.trans_data)
    {
        os_free(uis_player.trans_data);

        uis_player.trans_data = OS_NULL;
    }

    if (uis_player.trans_len)
    {
        os_free(uis_player.trans_len);

        uis_player.trans_len = OS_NULL;
    }

    if (uis_player.play_data)
    {
        os_free(uis_player.play_data);

        uis_player.play_data = OS_NULL;
    }

    if (uis_player.player_status != PLAYER_NO_CREATED)
    {
        uis_player.player_status = PLAYER_NO_CREATED;
    }
    
    if (uis_player.cb)
    {
        uis_player.cb = OS_NULL;
    }
}



/**
 * @brief           :   start playing stream data 
 * 
 * @param config    :   configuration parameter of player
 * @param callback  :   callback function that notify APP layer to send data to play
 *
 * @ret             :   OS_SUCCESS       --start player successfully
 * @                :   OS_FAILURE    --start player failed
**/
os_err_t audio_start_play_stream(audio_conf_t config, trans_data_cb callback)
{   
    audio_player_t *player = OS_NULL;

    /* cannot create player repeatedly */
    if (uis_player.player_status != PLAYER_NO_CREATED)
    {
        os_kprintf("player is running, stop first.\r\n");
        return OS_FAILURE;        
    }

    /*  cannot create player when recorder is running */
    if (uis_recorder.recorder_status != RECORDER_NO_CREATED)
    {
        os_kprintf("recorder is running, stop first.\r\n");
        return OS_FAILURE;
    }    

    /* create player */
    player = create_player(config, callback);
    OS_ASSERT(player);

    /* create task to receive audio data */
    if (uis_audio_receive_task == OS_NULL)
    {        
        uis_audio_receive_task = os_task_create("ad_recv_task", uis_audio_receive_thread, player, 1024, 5);
        OS_ASSERT(uis_audio_receive_task); 

        /* start receive task */
        os_task_startup(uis_audio_receive_task);
    }

    /* create task to play audio data */
    if (uis_audio_play_task == OS_NULL)
    {
        uis_audio_play_task = os_task_create("ad_play_task", uis_audio_play_thread, player, 1024, 5);
        OS_ASSERT(uis_audio_play_task);    

        /* start play task */    
        os_task_startup(uis_audio_play_task); 
    }

    uis_player.player_status = PLAYER_RUNNING; 

    return OS_SUCCESS;
}

/**
 * @brief           :   stop audio player
 *
 * @param           :   None
 *
 * @ret             :   None
**/
void audio_stop_play(void)
{
    os_err_t ret;
    
    if (uis_audio_receive_task != OS_NULL)
    {   
        ret = os_task_destroy(uis_audio_receive_task);
        if (ret == OS_SUCCESS)
        {
            uis_audio_receive_task = OS_NULL; 
        }
    }

    if (uis_audio_play_task != OS_NULL)
    {        
        ret = os_task_destroy(uis_audio_play_task);
        if (ret == OS_SUCCESS)
        {
            uis_audio_play_task = OS_NULL; 
        }
    }

    if (uis_player.player != OS_NULL)
    {  
        delete_player();
    }
}

/**
 * @brief           :   set volume of player
 *
 * @param[vol]      :   volume, 0~100  
**/
os_err_t audio_set_player_volume(uint32_t vol)
{
    audio_player_set_volume(uis_player.player, vol);
    return 0;
}

static void put_data_into_ringbuff(void)
{  
    uint32_t free_space_size = 0;
    uint32_t data_put_size = 0;


    free_space_size = rb_ring_buff_space_len(uis_player.data_cache);
    data_put_size = *(uis_player.trans_len);
    
    /* check free space of audio cache, if free space size more than transfer data size, put audio data into audio cache */
    if (free_space_size > data_put_size)
    {
        rb_ring_buff_put(uis_player.data_cache, uis_player.trans_data, data_put_size);
    }
    else
    {
        os_kprintf("error: free space size less than send data size.\r\n");
    }   
        
    /* clear transfer data buffer */
    memset(uis_player.trans_data, 0 , AUDIO_RECEIVE_DATA_BUFF_SIZE);    

    /* clear transfer data length */
    *(uis_player.trans_len) = 0;    
    
}


static void uis_audio_receive_thread(void *parameter)
{    
    uint32_t     free_space_size;
    //audio_player_t  *player;

    //player = (audio_player_t *)parameter;
  
    while(1)
    {
        while(uis_player.player_status == PLAYER_PAUSE)
        {
            os_task_msleep(1);
        }
        
        /* check free space size of audio data ring buffer */
        free_space_size = rb_ring_buff_space_len(uis_player.data_cache);
            
        /* if free space size more than (2 * AUDIO_SEND_DATA_LEN), notify APP transfer audio data by callback function */
        if (free_space_size > (AUDIO_RECEIVE_DATA_BUFF_SIZE * 2))
        {
            *(uis_player.trans_len) = AUDIO_RECEIVE_DATA_BUFF_SIZE;
            uis_player.app_msg = uis_player.cb(EVENT_REQ_SEND, uis_player.trans_data, uis_player.trans_len);
        }

        /* app_msg is ret of callback */
        if (uis_player.app_msg == MSG_DATA_NULL)
        {
            /* no transfer data, do nothing */
        }
        else if (uis_player.app_msg == MSG_PLAY_STREAM)
        {
            put_data_into_ringbuff();
            uis_player.app_msg = MSG_DATA_NULL;
        }
                            
        os_task_msleep(AUDIO_CALLBACK_INTERVAL);
    }
}


static void uis_audio_play_thread(void *parameter)
{ 
    uint32_t     data_remain_size = 0;
    uint32_t     data_play_size = 0;
    audio_player_t  *player;
    
    player = (audio_player_t *)parameter;  

    audio_player_start_pipe(uis_player.player);
    
    while(1)
    {  
        while(uis_player.player_status == PLAYER_PAUSE)
        {
            os_task_msleep(1);
        }
            
        data_remain_size = rb_ring_buff_data_len(uis_player.data_cache);
            
        if (data_remain_size > 0)
        {   
            //os_kprintf("audio data remain %d bytes.\r\n",data_remain_size);

            /* get data from ring buffer */
            data_play_size = MIN(data_remain_size, AUDIO_PLAY_DATA_BUFF_SIZE);
            memset(uis_player.play_data, 0 , AUDIO_PLAY_DATA_BUFF_SIZE);
            
            rb_ring_buff_get(uis_player.data_cache, uis_player.play_data, data_play_size);

            /* write data into pipe */
            audio_player_write_pipe(player, uis_player.play_data, data_play_size);
        }

        os_task_msleep(1);
        
    }
}



/**
 * @brief           :   create audio recorder
 * 
 * @param config    :   configuration parameter of recorder
 * @param callback  :   callback function that notify APP layer to recive data from recorder
 * 
 * @ret             :   pointer of audio recorder 
**/
static audio_recorder_t *create_recorder(audio_conf_t config, trans_data_cb callback)
{
    /* create recorder */
    uis_recorder.recorder = audio_recorder_create("recorder");
    OS_ASSERT(uis_recorder.recorder);

    /* config recorder */
    audio_recorder_config(uis_recorder.recorder, &config);

    /* init ring buffer */
    uis_recorder.data_cache = rb_ring_buff_create(AUDIO_RECORDER_RINGBUFF_SIZE);
    OS_ASSERT(uis_recorder.data_cache);

    /* init data transfer buffer */
    uis_recorder.trans_data = os_malloc(AUDIO_RECEIVE_DATA_BUFF_SIZE);
    OS_ASSERT(uis_recorder.trans_data);

    /* init data transfer length */
    uis_recorder.trans_len = os_malloc(1);
    OS_ASSERT(uis_recorder.trans_len);
    
    /* init play data buffer */
    uis_recorder.record_data = os_malloc(AUDIO_RECORD_DATA_BUFF_SIZE);
    OS_ASSERT(uis_recorder.record_data);

    /**/
    uis_recorder.recorder_status = RECORDER_NO_CREATED;
    
    /* register callback function */
    uis_recorder.cb = callback;

    
    return uis_recorder.recorder;
}


/**
 * @brief           :   delete audio recorder
 *
 * @param           :   None
 *
 * @ret             :   None
**/
static void delete_recorder(void)
{ 
    if (uis_recorder.recorder)
    {
        audio_recorder_stop(uis_recorder.recorder);
    
        audio_recorder_delete(uis_recorder.recorder);

        uis_recorder.recorder = OS_NULL;
    }

    if (uis_recorder.data_cache)
    {
        rb_ring_buff_destroy(uis_recorder.data_cache);

        uis_recorder.data_cache = OS_NULL;
    }

    if (uis_recorder.trans_data)
    {
        os_free(uis_recorder.trans_data);

        uis_recorder.trans_data = OS_NULL;
    }

    if (uis_recorder.trans_len)
    {
        os_free(uis_recorder.trans_len);

        uis_recorder.trans_len = OS_NULL;
    }

    if (uis_recorder.record_data)
    {
        os_free(uis_recorder.record_data);

        uis_recorder.record_data = OS_NULL;
    }

    if (uis_recorder.recorder_status != RECORDER_NO_CREATED)
    {
        uis_recorder.recorder_status = RECORDER_NO_CREATED;
    }
    
    if (uis_recorder.cb)
    {
        uis_recorder.cb = OS_NULL;
    }
}


/**
 * @brief           :   start recording stream data 
 * 
 * @param config    :   configuration parameter of recorder
 * @param callback  :   callback function to notify APP layer receive record data
 *
 * @ret             :   OS_SUCCESS       --start recorder successfully
 * @                :   OS_FAILURE    --start recorder failed
**/
os_err_t audio_start_record_stream(audio_conf_t config, trans_data_cb callback)
{   
    audio_recorder_t *recorder = OS_NULL;

    /* cannot create recorder repeatedly */
    if (uis_recorder.recorder_status != RECORDER_NO_CREATED)
    {
        os_kprintf("recorder is running, stop first.\r\n");
        return OS_FAILURE;
    }

    /* cannot create recorder when player is running */
    if (uis_player.player_status != PLAYER_NO_CREATED)
    {
        os_kprintf("player is running, stop first.\r\n");
        return OS_FAILURE;        
    }

    /* create recorder */
    recorder = create_recorder(config, callback);
    OS_ASSERT(recorder);

    /* create task to record audio data */
    if (uis_audio_record_task == OS_NULL)
    {
        uis_audio_record_task = os_task_create("ad_record_task", uis_audio_record_thread, recorder, 1024, 5);
        OS_ASSERT(uis_audio_record_task);    

        /* start play task */   
        os_task_startup(uis_audio_record_task); 
    }

    /* create task to send audio data */
    if (uis_audio_send_task == OS_NULL)
    {       
        uis_audio_send_task = os_task_create("ad_send_task", uis_audio_send_thread, recorder, 1024, 5);
        OS_ASSERT(uis_audio_send_task); 

        /* start send task */
        os_task_startup(uis_audio_send_task);
    }

    uis_recorder.recorder_status = RECORDER_RUNNING;

    return OS_SUCCESS;
}


/**
 * @brief           :   stop audio recorder
 *
 * @param           :   None
 *
 * @ret             :   None
**/
void audio_stop_record(void)
{
    os_err_t ret;
    
    if (uis_audio_send_task != OS_NULL)
    {
        ret = os_task_destroy(uis_audio_send_task);
        if (ret == OS_SUCCESS)
        {
            uis_audio_send_task = OS_NULL; 
        }
    }

    if (uis_audio_record_task != OS_NULL)
    {
        ret = os_task_destroy(uis_audio_record_task);
        if (ret == OS_SUCCESS)
        {
            uis_audio_record_task = OS_NULL; 
        }
    }

    if (uis_recorder.recorder != OS_NULL)
    {
        delete_recorder();
    }
}

static void uis_audio_send_thread(void *parameter)
{
    uint32_t         data_remain_size = 0;
    uint32_t         data_send_size = 0;
    //audio_recorder_t    *recorder;
    
    //recorder = (audio_recorder_t *)parameter;  

    while(1)
    {  
        while(uis_recorder.recorder_status == RECORDER_PAUSE)
        {
            os_task_msleep(1);
        }

        data_remain_size = rb_ring_buff_data_len(uis_recorder.data_cache);           
        if (data_remain_size > 0)
        {   
            /* get data from ring buffer */
            data_send_size = MIN(data_remain_size, AUDIO_SEND_DATA_BUFF_SIZE);

            rb_ring_buff_get(uis_recorder.data_cache, uis_recorder.trans_data, data_send_size);
            
            *(uis_recorder.trans_len) = data_send_size;
            
            uis_recorder.cb(EVENT_REQ_RECV, uis_recorder.trans_data, uis_recorder.trans_len);
        }

        os_task_msleep(AUDIO_CALLBACK_INTERVAL);
    }

}

void put_redorder_data(audio_recorder_t *recorder, int len, void *data)
{
    uint32_t         free_space_size = 0;
    
    /* check free space size of audio data ring buffer */
    free_space_size = rb_ring_buff_space_len(uis_recorder.data_cache);   
    
    /* if free space size more than (2 * AUDIO_SEND_DATA_BUFF_SIZE), record data */
    if (free_space_size > (AUDIO_SEND_DATA_BUFF_SIZE * 2))
    {
        /* put data into ring buffer */          
        rb_ring_buff_put(uis_recorder.data_cache, uis_recorder.record_data, len); 
    }
    else
    {
        os_kprintf("record ring buffer has no enough space,drop this packet of record data.\r\n");
    }
}

static void uis_audio_record_thread(void *parameter)
{   
    int32_t          data = 0;
    audio_recorder_t *recorder = (audio_recorder_t *)parameter; 
    int len = 0;
    uint8_t *data = malloc(RECORD_CHUNK_SZ);
    OS_ASSERT(data);

    int debug_size = 0;
    os_tick_t debug_period = os_tick_get_value();
    while(1)
    {
        while(uis_recorder.recorder_status == RECORDER_PAUSE)
        {
            os_task_msleep(1);
        }

        if(os_tick_get_value() - debug_period >= 10000)
        {
            DBOT_LOG_D("Audio recorder read %d bytes from mic in 10 seconds\r\n", debug_size);
            debug_size = 0;
            debug_period = os_tick_get_value();
        }

        memset(data, 0, RECORD_CHUNK_SZ);
        len = os_device_read_nonblock(recorder, 0, data, RECORD_CHUNK_SZ);
        if(0 < len)
        {
            put_redorder_data(recorder, len, (void*)data);
            debug_size += len;
        }
        os_task_msleep(1);        
    }
}





