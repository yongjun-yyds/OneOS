#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include "device.h"
#include "camera.h"
#include "cJSON.h"
#include "dbot_errno.h"
#include "dbot_log.h"
#include "publish_service.h"
#include "dbot_common.h"
#include "dbot_service.h"
#include "dtp_service.h"
#include "rtp.h"

#define CAMERA_TRANS_PROTOCOL  ATTR_TRANS_TCP

static DTP_SERVICE_CONTEXT_S *s_camera_dtp_ctx = NULL;
static int s_camera_quit_flag = 1;
static int s_camera_task_started = 0;

#define CAMERA_TASK_BEGIN     s_camera_task_started = 1
#define CAMERA_TASK_END       s_camera_task_started = 0
#define CAMERA_TASK_STARTED   s_camera_task_started

int camera_stop(int len, char *req, char **resp);
extern os_err_t init_camera(int argc, char *argv[]);

void free_camera_resource(void)
{
    if(NULL != s_camera_dtp_ctx)
    {
        dbot_service_destory_dtp(s_camera_dtp_ctx);
        s_camera_dtp_ctx = NULL;
    }
}

static unsigned char q_tabel1[64];
static unsigned char q_table2[64];

int generate_rtp_packets_and_send(uint8_t *jpeg_data, int jpeg_data_len, unsigned int height, unsigned int width)
{
    // for(int i = 0;i < 20;i ++)
    // {
    //     os_kprintf("0x%x ", jpeg_data[i]);
    // }
    // os_kprintf("0x%x 0x%x\r\n", jpeg_data[jpeg_data_len-2], jpeg_data[jpeg_data_len-1]);

    RETURN_IF_NULL(jpeg_data, DBOT_INVALID_PARAM);

    int ret = DBOT_OK;
    uint8_t *data = NULL;
    int size = 0;
    for(int i = 0;i < jpeg_data_len;i ++)
    {
        if((jpeg_data[i]==0XFF) && (jpeg_data[i+1]==0XD8))
        {
            data = jpeg_data + i;
            size = jpeg_data_len - i;
            break;
        }
    }

    rtp_hdr_t rtp_hdr;
    jpeg_hdr_t jpeg_hdr;
    jpeg_hdr_rst_t jpeg_rst_hdr;
    jpeg_hdr_qtable_t jpeg_qtable_hdr;
	unsigned char *ptr = NULL;
    unsigned int bytes_left = size;
    unsigned int data_len = 0;
    unsigned int offset = 0;
    unsigned int rtp_ssrc = 10;
    int dri = 0;
	unsigned short seq_num = 0;

    /* Initialize RTP header */
	rtp_hdr.version = 2;
	rtp_hdr.p = 0;
	rtp_hdr.x = 0;
	rtp_hdr.cc = 0;
	rtp_hdr.m = 0;
	rtp_hdr.pt = RTP_PT_JPEG;
    rtp_hdr.seq = 0;
	rtp_hdr.ts = htonl(get_timestamp());
	rtp_hdr.ssrc = htonl(rtp_ssrc);
    // rtp_hdr.ssrc = htonl(crc((uint8_t*)data, size));

	/* Initialize JPEG header */
	jpeg_hdr.tspec = 0;
	jpeg_hdr.offset = 0;
	jpeg_hdr.type = 0 | ((dri != 0) ? RTP_JPEG_RESTART : 0);
	jpeg_hdr.q = 0;
	jpeg_hdr.width = (width / 8);
	jpeg_hdr.height = (height / 8);

	/* Initialize DRI header */
	if (dri != 0)
    {
		jpeg_rst_hdr.dri = dri;
		jpeg_rst_hdr.f = 1;        /* This code does not align RIs */
		jpeg_rst_hdr.l = 1;
		jpeg_rst_hdr.count = 0x3fff;
	}

	/* Initialize quantization table header */
	if (jpeg_hdr.q >= 128)
    {
		jpeg_qtable_hdr.mbz = 0;
		jpeg_qtable_hdr.precision = 0;
		jpeg_qtable_hdr.length = 128;
	}

    unsigned int tmp = 0;
    rtp_packet_t *pkt = NULL;
    pkt = (rtp_packet_t*)malloc(sizeof(rtp_packet_t));
    RETURN_IF_NULL(pkt, DBOT_MALLOC_FAIL);
    memset(pkt, 0, sizeof(rtp_packet_t));

    //RTP封装以及分片
    while(bytes_left > 0)
    {
        ptr = pkt->buf + RTP_HDR_SZ;
        tmp = htonl(offset);
        tmp = tmp >> 8;
        jpeg_hdr.offset = tmp;

        memcpy(ptr, &jpeg_hdr, sizeof(jpeg_hdr));
      	ptr += sizeof(jpeg_hdr);
        rtp_hdr.m = 0;

        //如果有复位标记，则将复位标记头放在jpeg头之后
        if(dri != 0)
        {
            memcpy(ptr, &jpeg_rst_hdr, sizeof(jpeg_rst_hdr));
            ptr += sizeof(jpeg_rst_hdr);
        }
        //发送的第一帧数据带上量化表
        if(jpeg_hdr.q >= 128 && 0 == jpeg_hdr.offset)
        {
            memcpy(ptr, &jpeg_qtable_hdr, sizeof(jpeg_qtable_hdr));
            ptr += sizeof(jpeg_qtable_hdr);
            memcpy(ptr, q_tabel1, 64);
            ptr += 64;
            memcpy(ptr, q_table2, 64);
            ptr += 64;
        }

        //头部总长度
        pkt->header_len = ptr - pkt->buf;
        //本帧可封装的数据长度
        data_len = PACKET_SIZE - pkt->header_len;
        if (data_len >= bytes_left) 
        {
            data_len = bytes_left;
            rtp_hdr.m = 1;
        }
        //包总长度
        pkt->len = pkt->header_len + data_len;
        rtp_hdr.seq = htons(seq_num);
        memcpy(pkt->buf, &rtp_hdr, RTP_HDR_SZ);
        memcpy(ptr, data + offset, data_len);
        //发送数据
        s_camera_dtp_ctx->dtp_send((void*)s_camera_dtp_ctx, (uint8_t*)pkt->buf, pkt->len);

        //数据偏移，seq+1
        offset += data_len;
        bytes_left -= data_len;
        seq_num ++;
    }
    free(pkt);
    return ret;
}

void *camera_thread(void *arg)
{
    s_camera_quit_flag = 0;

    while(1)
    {
        if(s_camera_quit_flag)
        {
            break;
        }

        camera_fb_t *pic = esp_camera_fb_get();
        if(NULL != pic)
        {
            generate_rtp_packets_and_send(pic->buf, pic->len, 320, 240);
            esp_camera_fb_return(pic);
        }
    }

    free_camera_resource();
    CAMERA_TASK_END;
    DBOT_LOG_D("camera thread quit\r\n");
    return NULL;
}

int camera_play(int len, char *req, char **resp)
{
    int ret = DBOT_ERROR;
    cJSON *root = NULL;
    cJSON *ip = NULL;
    cJSON *port = NULL;

    RETURN_IF_NULL(req, DBOT_INVALID_PARAM);

    if(CAMERA_TASK_STARTED)
    {
        DBOT_LOG_E("camera already started, stop task now\r\n");
        camera_stop(0, NULL, NULL);
        int cnt = 6;
        while(cnt > 0)
        {
            if(!CAMERA_TASK_STARTED)
            {
                break;
            }
            dbot_msleep(200);
            cnt --;
        }
    }

    if(CAMERA_TASK_STARTED)
    {
        DBOT_LOG_E("camera already started\r\n");
        return DBOT_ERROR;
    }

    do
    {
        root = cJSON_Parse(req);
        BREAK_IF(NULL == root);
        ip = cJSON_GetObjectItem(root, "ip");
        port = cJSON_GetObjectItem(root, "port");
        BREAK_IF(NULL == ip || NULL == port);

        DTP_SERVICE_TYPE_E dtp_type = (CAMERA_TRANS_PROTOCOL == ATTR_TRANS_TCP)?DTP_SERVICE_TCP_CLIENT:DTP_SERVICE_UDP_CLIENT;
        s_camera_dtp_ctx = dbot_service_init_dtp(dtp_type, ip->valuestring, port->valueint);
        BREAK_IF(NULL == s_camera_dtp_ctx);

        pthread_t pid;
        pthread_attr_t attr;
        pthread_attr_init(&attr);
        pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
        pthread_attr_setstacksize(&attr, 6144);
        BREAK_IF(0 !=  pthread_create(&pid, &attr, camera_thread, NULL));

        ret = DBOT_OK;
        CAMERA_TASK_BEGIN;
        DBOT_LOG_E("recorder task started\r\n");
    } while (0);
    
    DELETE_CJSON(root);
    if(DBOT_OK != ret)
    {
        free_camera_resource();
    }
    return ret;
}

int camera_stop(int len, char *req, char **resp)
{
    if(CAMERA_TASK_STARTED)
    {
        s_camera_quit_flag = 1;
    }

    return DBOT_OK;
}

char *init_cme[2] = {"111", "jpeg"};
int camera_init(void)
{
    OS_ASSERT(OS_SUCCESS == init_camera(2, init_cme));

    return DBOT_OK;
}

static ATTR_INFO_S camera_attrs[] =
{
    {"camera_start",        "{\"protocol\":0, \"port\":0}",     camera_play},
    {"camera_stop",         "",                                 camera_stop}
};

int publish_camera(void)
{
    return dbot_service_add_service(CAPABILITY_TYPE_CAMERA, sizeof(camera_attrs)/sizeof(camera_attrs[0]), camera_attrs);
}

