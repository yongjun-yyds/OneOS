#include <stdio.h>
#include <stdlib.h>
#include "cJSON.h"
#include "dbot_errno.h"
#include "dbot_log.h"
#include "publish_service.h"
#include "dbot_common.h"
#include "dbot_service.h"
#include <board.h>

static int32_t gs_light_switch = 1; /* 灯开关状态 */

static int light_operate(int operate_flag)
{
    int ret = -1;
    int i = 0;

    if (0 == operate_flag) {
        ret = 0;
        gs_light_switch = 0;
        DBOT_LOG_D("close light\r\n");
        for (i = 0; i < led_table_size; i++)
        {
            os_pin_write(led_table[i].pin, !led_table[i].active_level);
        }
    } else if (1 == operate_flag) {
        ret = 0;
        gs_light_switch = 1;
        DBOT_LOG_D("open light\r\n");
        for (i = 0; i < led_table_size; i++)
        {
            os_pin_write(led_table[i].pin, led_table[i].active_level);
        }
    } else {
        ret = -1;
        DBOT_LOG_E("unknown operate flag: %d\r\n", operate_flag);
    }
    return ret;
}

static int turn_on_light(int len, char *params, char **resp)
{
    return light_operate(1);
}
static int turn_off_light(int len, char *params, char **resp)
{
    return light_operate(0);
}

static int get_light_status(int len, char *params,char **resp)
{
    int ret = DBOT_OK;
    cJSON *root = cJSON_CreateObject();
    cJSON_AddNumberToObject(root, "light_status", gs_light_switch);
    *resp = cJSON_PrintUnformatted(root);
    cJSON_Delete(root);

    return ret;
}

/* 灯操作 */
static ATTR_INFO_S light_attrs[] =
{
    {"turn_on_light",       "",    turn_on_light},
    {"turn_off_light",      "",    turn_off_light},
    {"get_light_status",    "",    get_light_status}
};


int light_init(void)
{
    for (int i = 0; i < led_table_size; i++)
    {
        os_pin_mode(led_table[i].pin, PIN_MODE_OUTPUT);
    }
    return 0;
}
int publish_light(void)
{
    return dbot_service_add_service(CAPABILITY_TYPE_LIGHT, sizeof(light_attrs)/sizeof(light_attrs[0]), light_attrs);
}


