/**
 ***********************************************************************************************************************
 * Copyright (c) 2021, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with 
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on 
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the 
 * specific language governing permissions and limitations under the License.
 *
 * @file        dbot_log.h
 *
 * @brief       Support diagnostic functions
 *
 * @revision
 * Date         Author          Notes
 * 2022-01-12   Jianqing.Sun    First version
 ***********************************************************************************************************************
 */

#ifndef _DBOT_LOG_H
#define _DBOT_LOG_H

#include <stdio.h>
#include <stdint.h>

typedef enum
{
    ERR     =   1,
    WARN    =   2,
    INFO    =   3,
    DEBUG   =   4,
} DbgLevel_t;

void dbot_log(uint8_t log_level, const char *format, ...);
int dbot_log_init(uint8_t level);

#define DBOT_DEBUG 
#ifdef DBOT_DEBUG
#define MAX_LOG_BUF_LEN         1024
#define DBOT_LOG(fmt, ...)    dbot_log(DEBUG, fmt, ##__VA_ARGS__)
#define DBOT_LOG_E(fmt, ...)  dbot_log(ERR, "[%s][%d]" fmt, __func__, __LINE__, ##__VA_ARGS__)
#define DBOT_LOG_W(fmt, ...)  dbot_log(WARN, "[%s][%d]" fmt, __func__, __LINE__, ##__VA_ARGS__)
#define DBOT_LOG_I(fmt, ...)  dbot_log(INFO, "[%s][%d]" fmt, __func__, __LINE__, ##__VA_ARGS__)
#define DBOT_LOG_D(fmt, ...)  dbot_log(DEBUG, "[%s][%d]" fmt, __func__, __LINE__, ##__VA_ARGS__)
#else
#define DBOT_LOG_E(fmt, ...)
#define DBOT_LOG_W(fmt, ...)
#define DBOT_LOG_I(fmt, ...)
#define DBOT_LOG_D(fmt, ...)
#endif

#endif /* _DBOT_LOG_H */
