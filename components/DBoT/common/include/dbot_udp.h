#ifndef __DBOT_UDP_H__
#define __DBOT_UDP_H__

#include <stdint.h>

#if defined __linux__
#include <netinet/in.h>
#elif defined _WIN32
#include <winsock2.h>
#else
#include "sys/socket.h"
#endif

#define UDP_FRAME_MAX_LENGTH (1500 - 20 - 8)

typedef int(*UDP_SEND_FUNC)(void *ctx, uint8_t *data, int len);
typedef int(*UDP_RECV_FUNC)(void *ctx, uint8_t *data, int len);

typedef struct _udp_client_s
{
    int fd;
    uint16_t port;
    uint64_t seq;
    UDP_SEND_FUNC send;
    struct sockaddr_in dst_addr;
}UDP_CLIENT_S;

typedef struct _udp_server_s
{
    int fd;
    uint16_t port;
    UDP_RECV_FUNC recv;
    struct sockaddr_in src_addr;
}UDP_SERVER_S;

int create_udp_client(UDP_CLIENT_S *client, char *dst_ip, uint16_t dst_port);
int create_udp_server(UDP_SERVER_S *server, uint16_t src_port);

#endif /* __DBOT_UDP_H__ */
