#ifndef _LINUX_CRC_CCITT_H
#define _LINUX_CRC_CCITT_H

#ifdef __cplusplus
extern "C" {
#endif

unsigned short crc_ccitt(unsigned short crc, const unsigned char *buffer, int len);
unsigned short crc_ccitt_byte(unsigned short crc, const unsigned char c);

#ifdef __cplusplus
}
#endif

#endif /* _LINUX_CRC_CCITT_H */
