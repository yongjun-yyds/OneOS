/**
 ***********************************************************************************************************************
 * Copyright (c) 2021, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with 
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on 
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the 
 * specific language governing permissions and limitations under the License.
 *
 * @file        dbot_log.h
 *
 * @brief       Support diagnostic functions
 *
 * @revision
 * Date         Author          Notes
 * 2022-01-10   Jianqing.Sun    First version
 ***********************************************************************************************************************
 */

#ifndef _DBOT_ERRNO_H
#define _DBOT_ERRNO_H

#ifdef __cplusplus
extern "C"
{
#endif

typedef enum {
    DBOT_ERROR                = (-10000), /* 默认错误 */
    DBOT_INVALID_PARAM,                   /* 参数错误 */
    DBOT_OUT_OF_MEMORY,                   /* 内存不足 */
    DBOT_MALLOC_FAIL,

    /* coap相关错误 */
    DBOT_COAP_SELF_REQUEST,               /* 来自自身的COAP请求 */
    DBOT_SOCKET_FAIL,


    /* 认证相关错误值 */
    DBOT_NUM_EXCESSIVE        = -9000,    /* 认证连接数量过多 */
    DBOT_FD_REUSED,                       /* socket fd被重用 */
    DBOT_AUTHENTICATED,                   /* 此IP已认证 */
    DBOT_AUTH_FAILED,

    /* JSON相关错误 */
    DBOT_JSON_PARSE_ERR       = -8000,    /* json解析错误 */
    DBOT_JSON_PARAM_ERR,                  /* json参数错误 */

    /* RPC相关错误 */
    DBOT_RPC_SERVICE_NOT_FOUND = -7000,   /* 此服务未找到 */
    DBOT_RPC_FUNC_NOT_FOUND,              /* 唯一标识符在本地未找到对应的函数 */
    DBOT_RPC_EXECUTE_ERROR,               /* RPC函数执行错误 */


    /* 设备相关错误 */
    DBOT_DEVICE_NOT_EXISTS     = -6000,    /* 设备不存在 */
    DBOP_DEVICE_OPTION_ERROR,              /* 设备操作错误 */
    DBOT_OK                    = 0,        /* 成功 */
} dbot_errno_e;

#ifdef __cplusplus
}
#endif

#endif /* _DBOT_ERRNO_H */
