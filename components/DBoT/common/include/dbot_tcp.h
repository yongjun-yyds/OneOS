#ifndef __DBOT_TCP_H__
#define __DBOT_TCP_H__

#include <stdint.h>

#if defined __linux__
#include <netinet/in.h>
#elif defined _WIN32
#include <winsock2.h>
#else
#include "sys/socket.h"
#endif

#define TCP_FRAME_MAX_LENGTH (1500 - 20 - 20)

typedef int(*TCP_SEND_FUNC)(void *ctx, uint8_t *data, int len);
typedef int(*TCP_RECV_FUNC)(void *ctx, uint8_t *data, int len);

typedef struct _tcp_client_s
{
    int fd;
    uint16_t port;
    uint16_t connected;
    uint64_t seq;
    TCP_SEND_FUNC send;
    struct sockaddr_in dst_addr;
}TCP_CLIENT_S;

typedef struct _tcp_server_s
{
    int fd;
    int client_fd;
    uint16_t port;
    TCP_RECV_FUNC recv;
    struct sockaddr_in src_addr;
}TCP_SERVER_S;

int create_tcp_client(TCP_CLIENT_S *client, char *dst_ip, uint16_t dst_port, int tv);
int create_tcp_server(TCP_SERVER_S *server, uint16_t src_port, int tv);

#endif /* __DBOT_TCP_H__ */
