/**
 ***********************************************************************************************************************
 * Copyright (c) 2022, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with 
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on 
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the 
 * specific language governing permissions and limitations under the License.
 *
 * @file        dbot_common.h
 *
 * @brief       network bus common
 *
 * @revision
 * Date         Author          Notes
 * 2021-12-31   JianQing.Sun    First version
 ***********************************************************************************************************************
 */

#ifndef _DBOT_COMMON_H
#define _DBOT_COMMON_H

#include <stdint.h>
#include "dbot_list.h"
#include "cJSON.h"
#include "dbot_log.h"

#define DBOT_VERSION              "1.0.0"     /* 当前软件版本号 */
#define DEVICE_NAME_PRE             "dev"

#define MAX_MAC_STR_LEN             17
#define MAX_IP_STR_LEN              15          /* 字符串类型的IPv4地址长度 */
#define MAX_DEV_NAME_LEN            32          /* 设备名最大长度 */
#define MAX_GROUP_NAME_LEN          32
#define MAX_GROUP_NUM               16
#define MAX_PARAM_NAME_LEN          16
#define MAX_PARAM_VALUE_LEN         16
#define MAX_VERSION_LEN             32          /* 版本号最大长队 */

#define MAX_SOCKET_RECV_BUFFER_LEN   1500

/*    service defines begin     */
#define MAX_SERVICE_ATTR_FUNC_NAME_LEN          32
#define MAX_SERVICE_ATTR_FUNC_PARAM_LEN         256


/* DDP resource define begin */
#define DBOT_DISCOVERY_SERVICE      "discovery"
#define DBOT_PUBLISH_SERVICE        "publish"
#define DBOT_HEARTBEAT_SERVICE      "heartbeat"
#define DBOT_SESSION_SERVICE        "session"
/* DDP resource define end */

/* DTCP option define begin */
#define DBOT_CALL_OPTION            "call"
#define DBOT_AUTH_OPTION            "auth"
#define DBOT_HEARTBEAT_OTION        "heartbeat"
/* DTCP option define end */

#ifndef MIN
#define MIN(a, b) ((a) < (b) ? (a) : (b))
#endif /* MIN */

#ifndef RETURN_NULL_IF_NULL
#define RETURN_NULL_IF_NULL(ptr)    \
if(NULL == ptr)                     \
{                                   \
    DBOT_LOG_E("param is null\r\n");\
    return NULL;                    \
}
#endif

#ifndef RETURN_IF_NULL
#define RETURN_IF_NULL(ptr, ret)    \
if(NULL == ptr)                     \
{                                   \
    DBOT_LOG_E("param is null\r\n");\
    return ret;                     \
}
#endif

#ifndef CONTINUE_IF_NULL
#define CONTINUE_IF_NULL(ptr)    \
if(NULL == ptr)                     \
{                                   \
    DBOT_LOG_E("param is null\r\n");\
    continue;                     \
}
#endif

#ifndef FREE_POINTER 
#define FREE_POINTER(ptr)   \
if(NULL != ptr)             \
{                           \
    free(ptr);              \
    ptr=NULL;               \
}                           
#endif

#ifndef DELETE_CJSON
#define DELETE_CJSON(ptr)   \
if(NULL != ptr)             \
{                           \
    cJSON_Delete(ptr);      \
    ptr = NULL;             \
}
#endif

#ifndef BREAK_IF
#define BREAK_IF(ptr)  \
if(ptr)            \
{                           \
    DBOT_LOG_E("break\r\n");\
    break;                  \
}
#endif

typedef enum attr_trans_protocol_e
{
    ATTR_TRANS_TCP = 0,
    ATTR_TRANS_UDP
}ATTR_TRANS_PROTOCOL_E;

typedef enum attr_stream_type_e
{
    ATTR_STREAM_IN = 0,
    ATTR_STREAM_OUT
}ATTR_STREAM_TYPE_E;
/*    service defines end     */

typedef enum
{
    PHY_LINK_ETH = 0,
    PHY_LINK_WIFI,
    PHY_LINK_BLE,
    PHY_LINK_ZIGBEE
}PHY_LINK_PROTOCOL_E;

typedef enum
{
    DEVICE_ROLE_PUBLISH = 0,
    DEVICE_ROLE_DISCOVERY,
    DEVICE_ROLE_BOTH
}DEVICE_ROLE_E;

/*
 * 本地设备信息(被发现端设备)
 */
typedef struct _device_info_local_t {
    uint32_t device_id;                         /* 设备id */
    char device_ip[MAX_IP_STR_LEN + 1];         /* 设备ip */
    char version[MAX_VERSION_LEN + 1];          /* 软件版本号 */
    uint32_t seq;                               /* 包序列号 */
    uint8_t mac[6];
    char broadcast_ip[MAX_IP_STR_LEN + 1];
    DEVICE_ROLE_E role;
    PHY_LINK_PROTOCOL_E phy;
} device_info_local_t;

typedef struct _service_info
{
    os_list_node_t node;
    uint32_t service_type;
    int server_attr_num;
    union
    {
        char *attr;
        os_list_node_t attr_list;
    }attrs;
}service_info_t;

/*
 * 远端设备信息
 */
typedef struct _device_info_remote {
    int device_id;                              /* 设备id */
    char device_ip[MAX_IP_STR_LEN + 1];         /* 设备ip */
    uint32_t seq;                               /* 包序列号 */
    int service_total;
    int service_cnt;
    union
    {
        service_info_t service;
        os_list_node_t service_list;
    }services;
}device_info_remote;

typedef struct file_header_s
{
    uint16_t endpoint;		//是否帧尾
    uint16_t crc;			//校验和
    uint32_t size;          //文件大小
    uint32_t offset;		//当前数据包在本帧文件中偏移量
    uint32_t seq;			//当前数据包序列号
    uint64_t ts;			//当前数据包时间戳
}__attribute__((packed)) FILE_HEADER_S;

#ifdef __cplusplus
extern "C"{
#endif

unsigned int get_timestamp(void);
int get_current_time(void);
int init_local_device_info(void);
void deinit_local_device_info(void);
device_info_local_t *get_local_device_info(void);
int get_device_id(void);
unsigned int get_timestamp(void);
int dbot_msleep(int mseconds);

#ifdef __cplusplus
}
#endif

#endif /* _DBOT_COMMON_H */
