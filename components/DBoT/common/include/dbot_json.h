/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with 
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on 
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the 
 * specific language governing permissions and limitations under the License.
 *
 * @file        dbot_json.h
 *
 * @brief       dbot json
 *
 * @revision
 * Date         Author          Notes
 * 2022-01-17   Jianqing.Sun    First version
 ***********************************************************************************************************************
 */

#ifndef __DBOT_JSON_H__
#define __DBOT_JSON_H__

#include <stdint.h>

#include "cJSON.h"
#include "dbot_common.h"

int pack_discovery_package(uint32_t service_type, char **send_buf, void *attrs, int total);
int unpack_discovery_package(const char *req_data, device_info_remote *remote_device);
int pack_publish_package(uint32_t service_type, char **send_buf, void *attrs, int total);
int unpack_publish_package(const char *req_data, device_info_remote *remote_device);

#endif /* __DBOT_JSON_H__ */
