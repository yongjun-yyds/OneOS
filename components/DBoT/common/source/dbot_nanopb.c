/**
 ***********************************************************************************************************************
 * Copyright (c) 2021, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        dbot_nanopb.c
 *
 * @brief
 *
 * @revision
 * Date         Author          Notes
 * 2022-07-28   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include "pb_encode.h"
#include "pb_decode.h"
#include "dbot_service.h"
#include "dbot_common.h"
#include "dbot_errno.h"
#include "dbot_log.h"

static bool client_write_callback(pb_ostream_t *stream, const uint8_t *buf, size_t count)
{
    RETURN_IF_NULL(stream, DBOT_INVALID_PARAM);
    RETURN_IF_NULL(buf, DBOT_INVALID_PARAM);

    TCP_CLIENT_S *client = (TCP_CLIENT_S *)stream->state;
    RETURN_IF_NULL(client, DBOT_INVALID_PARAM);

    int ret      = 0;
    int sent_len = 0;

    if (0 == client->connected)
    {
        ret = connect(client->fd, (struct sockaddr *)&client->dst_addr, sizeof(client->dst_addr));
        client->connected = (0 == ret) ? 1 : 0;
        DBOT_LOG_D("tcp client connect %s\r\n", (0 == ret) ? "succeed" : "failed");
    }

    if (0 == client->connected)
    {
        return false;
    }

    while(sent_len < count)
    {
        ret = send(client->fd, buf + sent_len, count - sent_len, 0);
        if (ret > 0)
        {
            sent_len += ret;
        }
        else if (0 == ret)
        {
            DBOT_LOG_D("no data be sent\r\n");
        }
        else
        {
            if (EINTR == errno)
            {
                DBOT_LOG_D("EINTR be caught, continue\r\n");
                continue;
            }

            DBOT_LOG_E("send fail\r\n");
            sent_len = -1;
            break;
        }
    }

    //os_kprintf("tcp client total send=%d\r\n", sent_len);
    return sent_len == count;
}

static bool client_read_callback(pb_istream_t *stream, uint8_t *buf, size_t count)
{
    RETURN_IF_NULL(stream, DBOT_INVALID_PARAM);
    RETURN_IF_NULL(buf, DBOT_INVALID_PARAM);

    TCP_CLIENT_S *client = (TCP_CLIENT_S *)stream->state;
    RETURN_IF_NULL(client, DBOT_INVALID_PARAM);

    int ret      = 0;
    int recv_len = 0;
    struct timeval tv;
    fd_set sets;

    if (0 == client->connected)
    {
        ret = connect(client->fd, (struct sockaddr *)&client->dst_addr, sizeof(client->dst_addr));
        client->connected = (0 == ret) ? 1 : 0;
        DBOT_LOG_D("tcp client connect %s\r\n", (0 == ret) ? "succeed" : "failed");
    }

    if (0 == client->connected)
    {
        return false;
    }

    if (count == 0)
    {
        return true;
    }

    tv.tv_sec  = 5;
    tv.tv_usec = 0;

    while(recv_len < count)
    {
        FD_ZERO(&sets);
        FD_SET(client->fd, &sets);
        ret = select(client->fd + 1, &sets, NULL, NULL, &tv);
        if (ret > 0)
        {
            ret = recv(client->fd, buf + recv_len, count - recv_len, 0);
            if (ret > 0)
            {
                recv_len += ret;
            }
            else if (0 == ret)
            {
                stream->bytes_left = 0; /* EOF */
                recv_len = -1;
                DBOT_LOG_E("connection close\r\n");
                break;
            }
            else
            {
                if (EINTR == errno)
                {
                    DBOT_LOG_D("EINTR be caught, continue\r\n");
                    continue;
                }
                stream->bytes_left = 0; /* EOF */
                recv_len = -1;
                DBOT_LOG_E("recv failed\r\n");
                break;
            }
        }
        else if (0 == ret)
        {
            stream->bytes_left = 0; /* EOF */
            break;
        }
        else
        {
            stream->bytes_left = 0; /* EOF */
            recv_len = -1;
            DBOT_LOG_E( "select-recv fail\r\n");
            break;
        }
    }

    //os_kprintf("tcp client recv count =%d, ret=%d, recv_len=%d\r\n", count, ret, recv_len);
    return recv_len == count;
}

static bool server_write_callback(pb_ostream_t *stream, const uint8_t *buf, size_t count)
{
    RETURN_IF_NULL(stream, DBOT_INVALID_PARAM);
    RETURN_IF_NULL(buf, DBOT_INVALID_PARAM);

    TCP_SERVER_S *server = (TCP_SERVER_S *)stream->state;
    RETURN_IF_NULL(server, DBOT_INVALID_PARAM);

    int ret = 0;
    int sent_len = 0;

    if (server->client_fd < 0)
    {
        struct timeval tv;
#ifdef _WIN32
        int addr_len = sizeof(struct sockaddr_in);
#else
        socklen_t addr_len = sizeof(struct sockaddr_in);
#endif
        memset(&server->src_addr, 0, sizeof(server->src_addr));
        server->client_fd = accept(server->fd, (struct sockaddr *)&server->src_addr, &addr_len);
        if (server->client_fd >= 0)
        {
#if defined _WIN32

#else
            tv.tv_sec  = 5;
            tv.tv_usec = 0;
            setsockopt(server->client_fd, SOL_SOCKET, SO_SNDTIMEO, (const char *)&tv, sizeof(struct timeval));
            setsockopt(server->client_fd, SOL_SOCKET, SO_RCVTIMEO, (const char *)&tv, sizeof(struct timeval));
#endif
        }
        DBOT_LOG_D("tcp server accept %s\r\n", (0 <= server->client_fd) ? "succeed" : "failed");
    }

    if (server->client_fd < 0)
    {
        return false;
    }

    while(sent_len < count)
    {
        ret = send(server->client_fd, buf + sent_len, count - sent_len, 0);
        if (ret > 0)
        {
            sent_len += ret;
        }
        else if (0 == ret)
        {
            DBOT_LOG_D("no data be sent\r\n");
        }
        else
        {
            if (EINTR == errno)
            {
                DBOT_LOG_D("EINTR be caught, continue\r\n");
                continue;
            }

            DBOT_LOG_E("send fail\r\n");
            sent_len = -1;
            break;
        }
    }

    //os_kprintf("tcp server send=%d\r\n", sent_len);
    return sent_len == count;
}

static bool server_read_callback(pb_istream_t *stream, uint8_t *buf, size_t count)
{
    RETURN_IF_NULL(stream, DBOT_INVALID_PARAM);
    RETURN_IF_NULL(buf, DBOT_INVALID_PARAM);

    TCP_SERVER_S *server = (TCP_SERVER_S *)stream->state;
    RETURN_IF_NULL(server, DBOT_INVALID_PARAM);

    int    ret = 0;
    int    recv_len = 0;
    struct timeval tv;
    fd_set sets;

    if (server->client_fd < 0)
    {
#ifdef _WIN32
        int addr_len = sizeof(struct sockaddr_in);
#else
        socklen_t addr_len = sizeof(struct sockaddr_in);
#endif
        memset(&server->src_addr, 0, sizeof(server->src_addr));
        server->client_fd = accept(server->fd, (struct sockaddr *)&server->src_addr, &addr_len);
        if (server->client_fd >= 0)
        {
#if defined _WIN32

#else
            tv.tv_sec  = 5;
            tv.tv_usec = 0;
            setsockopt(server->client_fd, SOL_SOCKET, SO_SNDTIMEO, (const char *)&tv, sizeof(struct timeval));
            setsockopt(server->client_fd, SOL_SOCKET, SO_RCVTIMEO, (const char *)&tv, sizeof(struct timeval));
#endif
        }
        DBOT_LOG_D("tcp server accept %s\r\n", (0 <= server->client_fd) ? "succeed" : "failed");
    }

    if (server->client_fd < 0)
    {
        return false;
    }

    if (count == 0)
    {
        return true;
    }

    tv.tv_sec  = 5; /* large than 3s: if(ostream->callback != NULL) {dbot_msleep(3000);} */
    tv.tv_usec = 0;

    while(recv_len < count)
    {
        FD_ZERO(&sets);
        FD_SET(server->client_fd, &sets);
        ret = select(server->client_fd + 1, &sets, NULL, NULL, &tv);
        if (ret > 0)
        {
            ret = recv(server->client_fd, buf + recv_len, count - recv_len, 0);
            if (ret > 0)
            {
                recv_len += ret;
            }
            else if (0 == ret)
            {
                stream->bytes_left = 0; /* EOF */
                recv_len = -1;
                DBOT_LOG_E("connection close\r\n");
                break;
            }
            else
            {
                if (EINTR == errno)
                {
                    DBOT_LOG_D("EINTR be caught, continue\r\n");
                    continue;
                }
                stream->bytes_left = 0; /* EOF */
                recv_len = -1;
                DBOT_LOG_E("recv failed\r\n");
                break;
            }
        }
        else if (0 == ret)
        {
            stream->bytes_left = 0; /* EOF */
            DBOT_LOG_E( "select-recv fail, ret=0\r\n");
            break;
        }
        else
        {
            stream->bytes_left = 0; /* EOF */
            recv_len = -1;
            DBOT_LOG_E( "select-recv fail, ret=-1\r\n");
            break;
        }
    }

    //os_kprintf("tcp server recv count =%d, ret=%d, recv_total_len=%d\r\n", count, ret, recv_len);
    return recv_len == count;
}

static pb_ostream_t pb_ostream_from_client_socket(void *ctx)
{
    pb_ostream_t stream = {&client_write_callback, (void *)ctx, SIZE_MAX, 0};

    return stream;
}

static pb_istream_t pb_istream_from_client_socket(void *ctx)
{
    pb_istream_t stream = {&client_read_callback, (void *)ctx, SIZE_MAX};

    return stream;
}

static pb_ostream_t pb_ostream_from_server_socket(void *ctx)
{
    pb_ostream_t stream = {&server_write_callback, (void *)ctx, SIZE_MAX, 0};

    return stream;
}

static pb_istream_t pb_istream_from_server_socket(void *ctx)
{
    pb_istream_t stream = {&server_read_callback, (void *)ctx, SIZE_MAX};

    return stream;
}

int dbot_nanopb_msg_output(void *ctx, const pb_msgdesc_t *fields, const void *src_struct)
{
    /* Construct and send the data to server */
    RETURN_IF_NULL(ctx, DBOT_INVALID_PARAM);
    RETURN_IF_NULL(fields, DBOT_INVALID_PARAM);
    RETURN_IF_NULL(src_struct, DBOT_INVALID_PARAM);

    DTP_SERVICE_CONTEXT_S *dtp_ctx = (DTP_SERVICE_CONTEXT_S *)ctx;

    if (dtp_ctx->type == DTP_SERVICE_TCP_CLIENT)
    {
        TCP_CLIENT_S *client = (TCP_CLIENT_S *)(&dtp_ctx->dtp_service.tcp_client);
        RETURN_IF_NULL(client, DBOT_INVALID_PARAM);
        pb_ostream_t output = pb_ostream_from_client_socket(client);

        /* Encode the request. It is written to the socket immediately
         * through our custom stream. */
        if (!pb_encode_delimited(&output, fields, src_struct))
        {
            DBOT_LOG_E("tcp client encode failed: %s\r\n", PB_GET_ERROR(&output));
            return DBOT_ERROR;
        }
    }
    else if (dtp_ctx->type == DTP_SERVICE_TCP_SERVER)
    {
        TCP_SERVER_S *server = (TCP_SERVER_S *)(&dtp_ctx->dtp_service.tcp_server);
        RETURN_IF_NULL(server, DBOT_INVALID_PARAM);
        pb_ostream_t output = pb_ostream_from_server_socket(server);

        /* Encode the request. It is written to the socket immediately
         * through our custom stream. */
        if (!pb_encode_delimited(&output, fields, src_struct))
        {
            DBOT_LOG_E("tcp server encode failed: %s\r\n", PB_GET_ERROR(&output));
            return DBOT_ERROR;
        }
    }
    else
    {
        DBOT_LOG_E("dtp service type not support output!\r\n");
        return DBOT_ERROR;
    }

    return DBOT_OK;
}

int dbot_nanopb_msg_input(void *ctx, const pb_msgdesc_t *fields, void *dest_struct)
{
    /* Construct and send the data to server */
    RETURN_IF_NULL(ctx, DBOT_INVALID_PARAM);
    RETURN_IF_NULL(dest_struct, DBOT_INVALID_PARAM);

    DTP_SERVICE_CONTEXT_S *dtp_ctx = (DTP_SERVICE_CONTEXT_S *)ctx;

    if (dtp_ctx->type == DTP_SERVICE_TCP_SERVER)
    {
        TCP_SERVER_S *server = (TCP_SERVER_S *)(&dtp_ctx->dtp_service.tcp_server);
        RETURN_IF_NULL(server, DBOT_INVALID_PARAM);

        pb_istream_t input = pb_istream_from_server_socket(server);

        if (!pb_decode_delimited(&input, fields, dest_struct))
        {
            DBOT_LOG_E("tcp server decode failed: %s\r\n", PB_GET_ERROR(&input));
            return DBOT_ERROR;
        }
    }
    else if (dtp_ctx->type == DTP_SERVICE_TCP_CLIENT)
    {
        TCP_CLIENT_S *client = (TCP_CLIENT_S *)(&dtp_ctx->dtp_service.tcp_client);
        RETURN_IF_NULL(client, DBOT_INVALID_PARAM);

        pb_istream_t input = pb_istream_from_client_socket(client);

        if (!pb_decode_delimited(&input, fields, dest_struct))
        {
            DBOT_LOG_E("tcp client decode failed: %s\r\n", PB_GET_ERROR(&input));
            return DBOT_ERROR;
        }
    }
    else
    {
        DBOT_LOG_E("dtp service type not support input!\r\n");
        return DBOT_ERROR;
    }

    return DBOT_OK;
}
