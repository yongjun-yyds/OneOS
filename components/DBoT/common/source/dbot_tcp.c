#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include "dbot_log.h"
#include "dbot_errno.h"
#include "dbot_common.h"
#include "dbot_tcp.h"
#if defined __linux__
#include <sys/socket.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <errno.h>
#elif defined _WIN32
#include <winsock2.h>
#include <time.h>
#else
#include "os_clock.h"
#include "sys/socket.h"
#include "lwip/netif.h"
#endif

int _tcp_send(void *ctx, uint8_t *data, int len)
{
    RETURN_IF_NULL(ctx, DBOT_INVALID_PARAM);
    RETURN_IF_NULL(data, DBOT_INVALID_PARAM);

    int ret = 0;
    TCP_CLIENT_S *client = (TCP_CLIENT_S*)ctx;
    int offset = 0;

    if(0 == client->connected)
    {
        ret = connect(client->fd, (struct sockaddr*)&client->dst_addr, sizeof(client->dst_addr));
        client->connected = (0 == ret)?1:0;
        DBOT_LOG_D("tcp client connect %s\r\n", (0 == ret)?"succeed":"failed");
    }

    if(0 == client->connected)
    {
        return DBOT_ERROR;
    }

    while(offset < len)
    {
        ret = send(client->fd, data + offset, len - offset, 0);
        if(0 > ret)
        {
            break;
        }
        else
        {
            offset += ret;
        }
    }

    return offset;
}

int _tcp_recv(void *ctx, uint8_t *data, int len)
{
    RETURN_IF_NULL(ctx, DBOT_INVALID_PARAM);
    RETURN_IF_NULL(data, DBOT_INVALID_PARAM);

    struct timeval tv;
#ifdef _WIN32
    int addr_len = sizeof(struct sockaddr_in);
#else
    socklen_t addr_len = sizeof(struct sockaddr_in);
#endif
    TCP_SERVER_S *server = (TCP_SERVER_S*)ctx;
    memset(&server->src_addr, 0, sizeof(server->src_addr));
    if(0 > server->client_fd)
    {
        server->client_fd = accept(server->fd, (struct sockaddr *)&server->src_addr, &addr_len);
        if(0 <= server->client_fd)
        {
#if defined _WIN32

#else
            tv.tv_sec = 0;
            tv.tv_usec = 200 * 1000;
            setsockopt(server->client_fd, SOL_SOCKET, SO_SNDTIMEO, (const char*)&tv, sizeof(struct timeval));
            setsockopt(server->client_fd, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof(struct timeval));
#endif
        }
        // DBOT_LOG_D("tcp server accept %s\r\n", (0 <= server->client_fd)?"succeed":"failed");
    }

    if(0 > server->client_fd)
    {
        return DBOT_ERROR;
    }

    fd_set fds;
    int ret = -1;
    FD_ZERO(&fds);
    FD_SET(server->client_fd, &fds);
    tv.tv_sec  = 0;
    tv.tv_usec = 200 * 1000;

    // windows mingw64环境设置socket超时无法生效，此处需保留select防止调用线程被阻塞无法退出
    ret = select(server->client_fd + 1, &fds, NULL, NULL, &tv);
    if(0 < ret)
    {
        if(FD_ISSET(server->client_fd, &fds))
        {
            ret = recv(server->client_fd, data, len, 0);
            if(0 >= ret)
            {
                DBOT_LOG_E("tcp client disconnected\r\n");
                closesocket(server->client_fd);
                server->client_fd = -1;
            }
        }
    }

    return ret;
}

int create_tcp_client(TCP_CLIENT_S *client, char *dst_ip, uint16_t dst_port, int tv)
{
    RETURN_IF_NULL(client, DBOT_INVALID_PARAM);
    RETURN_IF_NULL(dst_ip, DBOT_INVALID_PARAM);

    int fd = socket(AF_INET, SOCK_STREAM, 0);
    if(0 > fd)
    {
        DBOT_LOG_E("create socket failed\r\n");
        return DBOT_SOCKET_FAIL;
    }

    do
    {
        if(0 < tv)
        {
#if defined _WIN32

#else
            struct timeval timeout;
            timeout.tv_sec = tv;
            timeout.tv_usec = 0;
            if(0 != setsockopt(fd, SOL_SOCKET, SO_SNDTIMEO, (const char*)&timeout, sizeof(struct timeval)))
            {
                DBOT_LOG_E("set socket send timeout failed\r\n");
                break;
            }

            if(0 != setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, (const char*)&timeout, sizeof(struct timeval)))
            {
                DBOT_LOG_E("set socket recv timeout failed\r\n");
                break;
            }

            int nodeley = 1;
            if(0 != setsockopt(fd, IPPROTO_TCP, TCP_NODELAY, (const void *)&nodeley, sizeof(int)))
            {
                DBOT_LOG_E("canel nagle failed\r\n");
                break;
            }
#endif
        }
        client->fd = fd;
        client->port = dst_port;
        client->connected = 0;
        client->dst_addr.sin_family = AF_INET;
        client->dst_addr.sin_port = htons(dst_port);
        client->dst_addr.sin_addr.s_addr = inet_addr(dst_ip);
        client->send = _tcp_send;
        return DBOT_OK;
    } while (0);
    
    closesocket(fd);
    return DBOT_ERROR;
}

int create_tcp_server(TCP_SERVER_S *server, uint16_t src_port, int tv)
{
    RETURN_IF_NULL(server, DBOT_INVALID_PARAM);

    int fd = socket(AF_INET, SOCK_STREAM, 0);
    if(0 > fd)
    {
        DBOT_LOG_E("create socket failed\r\n");
        return DBOT_SOCKET_FAIL;
    }

    do
    {
        int on = 1;
        if(0 != setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, (const char*)&on, sizeof(on)))
        {
            DBOT_LOG_E("set socket reuse addr failed\r\n");
            break;
        }
        if(0 < tv)
        {
#if defined _WIN32

#else
            struct timeval timeout;
            timeout.tv_sec = tv;
            timeout.tv_usec = 0;
            if(0 != setsockopt(fd, SOL_SOCKET, SO_SNDTIMEO, (const char*)&timeout, sizeof(struct timeval)))
            {
                DBOT_LOG_E("set socket send timeout failed\r\n");
                break;
            }
            if(0 != setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, (const char*)&timeout, sizeof(struct timeval)))
            {
                DBOT_LOG_E("set socket recv timeout failed\r\n");
                break;
            }
#endif
        }
        struct sockaddr_in addr;
        memset(&addr, 0, sizeof(addr));
        addr.sin_family = AF_INET;
        addr.sin_addr.s_addr = INADDR_ANY;
        addr.sin_port = htons(src_port);
        if(0 > bind(fd, (struct sockaddr*)&addr, sizeof(addr)))
        {
            DBOT_LOG_E("bind tcp server port failed\r\n");
            break;
        }

        if(0 > listen(fd, 1))
        {
            DBOT_LOG_E("listen tcp server port failed\n");
            break;
        }

#ifdef _WIN32
        int len = sizeof(struct sockaddr_in);
#else
        socklen_t len = sizeof(struct sockaddr_in);
#endif
        memset(&addr, 0, sizeof(addr));
        if(0 > getsockname(fd, (struct sockaddr*)&addr, &len))
        {
            DBOT_LOG_E("get dtp server port failed\r\n");
            break;
        }
        
        server->fd = fd;
        server->client_fd = -1;
        server->port = ntohs(addr.sin_port);
        server->recv = _tcp_recv;
        DBOT_LOG_I("tcp server started on port %d:%u\r\n", server->fd, server->port);
        return DBOT_OK;
    } while (0);
    
    closesocket(fd);
    return DBOT_ERROR;
}
