#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>
#include <stdint.h>
#include <unistd.h>

#include "dbot_errno.h"
#include "dbot_log.h"
#include "dbot_common.h"

static device_info_local_t *gs_device_info = NULL;
extern bool dbot_service_started(void);

#if defined __linux__
#include <sys/socket.h>
#include <ifaddrs.h>
#include <net/if.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <arpa/inet.h>

int get_mac_addr(uint8_t *s, size_t n, const char *ifname)
{
    struct ifreq ifreq;
    memset(&ifreq, 0, sizeof(struct ifreq));
    strncpy(ifreq.ifr_name, ifname, sizeof(ifreq.ifr_name));

    int fd = socket(AF_INET, SOCK_STREAM, 0);
    if(0 > fd)
    {
        return DBOT_ERROR;
    }
    if(0 > ioctl(fd, SIOCGIFHWADDR, &ifreq))
    {
        closesocket(fd);
        return DBOT_ERROR;
    }
    memcpy(s, ifreq.ifr_hwaddr.sa_data, n);

    return DBOT_OK;
}

int get_local_netif_info(device_info_local_t *device_info)
{
    int ret = DBOT_ERROR;
    struct ifaddrs *ifa = NULL;
    struct ifaddrs *node = NULL;
    struct sockaddr_in *sin = NULL;

    if(0 <= getifaddrs(&ifa))
    {
        if(NULL != ifa)
        {
            for(node = ifa;node != NULL;node = node->ifa_next)
            {
                sin = (struct sockaddr_in *)node->ifa_addr;
                // 不处理非ipv4地址，空地址，环回地址
                if(AF_INET != sin->sin_family || 0 == sin->sin_addr.s_addr || inet_addr("127.0.0.1") == sin->sin_addr.s_addr)
                {
                    continue;
                }
                strncpy(device_info->device_ip, inet_ntoa(sin->sin_addr), sizeof(device_info->device_ip));

                sin = (struct sockaddr_in *)node->ifa_ifu.ifu_broadaddr;
                strncpy(device_info->broadcast_ip, inet_ntoa(sin->sin_addr), sizeof(device_info->broadcast_ip));

                ret = get_mac_addr(device_info->mac, sizeof(device_info->mac), node->ifa_name);
                if(DBOT_OK == ret)
                {
                    break;
                }
            }
            freeifaddrs(ifa);
        }
    }

    return ret;
}

int dbot_msleep(int mseconds)
{
    return usleep(mseconds * 1000);
}
#elif defined _WIN32
#include <winsock2.h>
#include <iphlpapi.h>
#include <ws2tcpip.h>
#include <sys/time.h>

// GetAdaptersInfo只能获取到ipv4地址，若需要ipv6地址则改用GetAdaptersAddress
int get_local_netif_info(device_info_local_t *device_info)
{
    int ret = DBOT_ERROR;
    IP_ADAPTER_INFO *adapter_info = NULL;
    IP_ADAPTER_INFO *adapter = NULL;
    ULONG len = sizeof(IP_ADAPTER_INFO);
    struct in_addr addr;
    char *ip_addr_str = NULL;
    char *mask_addr_str = NULL;
    uint32_t ip_addr_net = 0;
    uint32_t mask_addr_net = 0;

    adapter_info = calloc(1, len);
    if(NULL == adapter_info)
    {
        DBOT_LOG_E("malloc failed\r\n");
        return ret;
    }

    // Make an initial call to GetAdaptersInfo to get
    // the necessary size into the ulOutBufLen variable
    // MSDN官方建议如此，不可删
    if(ERROR_BUFFER_OVERFLOW == GetAdaptersInfo(adapter_info, &len))
    {
        free(adapter_info);
        adapter_info = calloc(1, len);
        if(NULL == adapter_info)
        {
            DBOT_LOG_E("malloc failed\r\n");
            return ret;
        }
    }
    if(NO_ERROR == GetAdaptersInfo(adapter_info, &len))
    {
        for(adapter = adapter_info;adapter != NULL;adapter = adapter->Next)
        {
            ip_addr_str = adapter->IpAddressList.IpAddress.String;
            mask_addr_str = adapter->IpAddressList.IpMask.String;
            ip_addr_net = inet_addr(ip_addr_str);
            mask_addr_net = inet_addr(mask_addr_str);
            // 忽略环回地址和空地址
            if(!strcmp("127.0.0.1", ip_addr_str) || !strcmp("0.0.0.0", ip_addr_str))
            {
                continue;
            }
            // 忽略网关地址
            if(0x01000000 == (ip_addr_net & mask_addr_net))
            {
                continue;
            }
            strncpy(device_info->device_ip, ip_addr_str, sizeof(device_info->device_ip));
            addr.s_addr = ip_addr_net | ~mask_addr_net;
            strncpy(device_info->broadcast_ip, inet_ntoa(addr), sizeof(device_info->broadcast_ip));
            memcpy(device_info->mac, adapter->Address, sizeof(device_info->mac));
            ret = DBOT_OK;
            break;
        }
    }
    else
    {
        DBOT_LOG_E("get adapter info failed\r\n");
    }
    free(adapter_info);

    return ret;
}

int dbot_msleep(int mseconds)
{
    return usleep(mseconds * 1000);
}

#else
#include <arpa/inet.h>
#include "oneos_config.h"
#include "oneos_lwip.h"
#include "os_clock.h"

int get_local_netif_info(device_info_local_t *device_info)
{
    struct netif *net_card = os_lwip_get_default_netif();
    if(NULL == net_card)
    {
        return DBOT_ERROR;
    }
    uint32_t ip4 = ip_addr_get_ip4_u32(&net_card->ip_addr);
    uint32_t mask4 = ip_addr_get_ip4_u32(&net_card->netmask);

    if(0 == ip4 || inet_addr("127.0.0.1") == ip4)
    {
        return DBOT_ERROR;
    }
    strncpy(device_info->device_ip, inet_ntoa(ip4), sizeof(device_info->device_ip));

    uint32_t broadcast_addr = (ip4 | ~mask4);
    strncpy(device_info->broadcast_ip, inet_ntoa(broadcast_addr), sizeof(device_info->broadcast_ip));

    int i = 0;
    for (i = 0; i < net_card->hwaddr_len; i++)
    {
        device_info->mac[i] = net_card->hwaddr[i];
    }

    return DBOT_OK;
}

int dbot_msleep(int mseconds)
{
    return os_task_msleep(mseconds);
}
#endif

unsigned int get_timestamp(void)
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    unsigned int  timestamp = (90000*tv.tv_sec);
    timestamp += (unsigned int )((2.0*90000*tv.tv_usec + 1000000.0)/2000000);   
    unsigned int const rtp_timestamp = timestamp;  
    return rtp_timestamp;
}

int get_current_time(void)
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return tv.tv_sec * 1000 + tv.tv_usec / 1000;
}

device_info_local_t *get_local_device_info(void)
{
    return gs_device_info;
}

int get_device_id(void)
{
    return gs_device_info->device_id;
}

int init_local_device_info(void)
{
    if(NULL == gs_device_info)
    {
        gs_device_info = (device_info_local_t *)calloc(1, sizeof(device_info_local_t));
        if(NULL == gs_device_info)
        {
            DBOT_LOG_E("calloc device info fail\n");
            return DBOT_OUT_OF_MEMORY;
        }

        if(DBOT_OK != get_local_netif_info(gs_device_info))
        {
            DBOT_LOG_E("network is not ready\r\n");
            free(gs_device_info);
            gs_device_info = NULL;
            return DBOT_ERROR;
        }

#if defined DBOT_PUBLISH && defined DBOT_DISCOVERY
        gs_device_info->role = DEVICE_ROLE_BOTH;
#elif defined DBOT_PUBLISH && !defined DBOT_DISCOVERY
        gs_device_info->role = DEVICE_ROLE_PUBLISH;
#elif !defined DBOT_PUBLISH && defined DBOT_DISCOVERY
        gs_device_info->role = DEVICE_ROLE_DISCOVERY;
#endif

#ifdef DBOT_NETWORK_USE_ETH
        gs_device_info->phy = PHY_LINK_ETH;
#endif
#ifdef DBOT_NETWORK_USE_WIFI
        gs_device_info->phy = PHY_LINK_WIFI;
#endif
#ifdef DBOT_NETWORK_USE_BLE
        gs_device_info->phy = PHY_LINK_BLE;
#endif

        uint32_t ip_net = inet_addr(gs_device_info->device_ip);
        gs_device_info->device_id = (gs_device_info->mac[3]) << 16 | (gs_device_info->mac[4]) << 8 | (gs_device_info->mac[5]);
        strncpy(gs_device_info->version, DBOT_VERSION, sizeof(gs_device_info->version));
        DBOT_LOG_D("network info:device id[%d], host ip[%s], broadcast ip[%s], mac[%02X:%02X:%02X:%02X:%02X:%02X]\r\n", 
                                                                                                            gs_device_info->device_id,
                                                                                                            gs_device_info->device_ip, 
                                                                                                            gs_device_info->broadcast_ip,
                                                                                                            gs_device_info->mac[0],
                                                                                                            gs_device_info->mac[1],
                                                                                                            gs_device_info->mac[2],
                                                                                                            gs_device_info->mac[3],
                                                                                                            gs_device_info->mac[4],
                                                                                                            gs_device_info->mac[5]);
    }
    return DBOT_OK;
}

void deinit_local_device_info(void)
{
    if (NULL != gs_device_info) {
        free(gs_device_info);
        gs_device_info = NULL;
    }
}

void show_device_info(void)
{
    if(!dbot_service_started())
    {
        DBOT_LOG_D("dbot not started\r\n");
        return;
    }

    DBOT_LOG("id:\t%d\r\n", get_local_device_info()->device_id);
    DBOT_LOG("ip:\t%s\r\n", get_local_device_info()->device_ip);
    DBOT_LOG("phy:\t%d[0-eth, 1-wifi, 2-ble, 3-zigbee]\r\n", get_local_device_info()->phy);
}

#if !defined __linux__ && !defined _WIN32
#include "shell.h"
SH_CMD_EXPORT(hgn_device_info, show_device_info, "show device info");
SH_CMD_EXPORT(dbot_device_info, show_device_info, "show device info");
#endif
