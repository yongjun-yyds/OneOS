#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include "dbot_log.h"
#include "dbot_errno.h"
#include "dbot_common.h"
#include "dbot_udp.h"
#if defined __linux__
#include <sys/socket.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <errno.h>
#elif defined _WIN32
#include <winsock2.h>
#include <time.h>
#else
#include "os_clock.h"
#include "sys/socket.h"
#include "lwip/netif.h"
#endif

int _udp_send(void *ctx, uint8_t *data, int len)
{
    RETURN_IF_NULL(ctx, DBOT_INVALID_PARAM);
    RETURN_IF_NULL(data, DBOT_INVALID_PARAM);

    int ret = -1;
    int offset = 0;
    int addr_len = sizeof(struct sockaddr_in);
    UDP_CLIENT_S *client = (UDP_CLIENT_S*)ctx;

    while(offset < len)
    {
        ret = sendto(client->fd, data + offset, len - offset, 0, (struct sockaddr*)&client->dst_addr, addr_len);
        if(0 > ret)
        {
            break;
        }
        else
        {
            offset += ret;
        }
    }
    return offset;
}

int _udp_recv(void *ctx, uint8_t *data, int len)
{
    RETURN_IF_NULL(ctx, DBOT_INVALID_PARAM);
    RETURN_IF_NULL(data, DBOT_INVALID_PARAM);

    fd_set fds;
    struct timeval tv;
    int ret = -1;
#ifdef _WIN32
    int addr_len = sizeof(struct sockaddr_in);
#else
    socklen_t addr_len = sizeof(struct sockaddr_in);
#endif
    UDP_SERVER_S *server = (UDP_SERVER_S*)ctx;
    memset(&server->src_addr, 0, sizeof(server->src_addr));
    FD_ZERO(&fds);
    FD_SET(server->fd, &fds);
    tv.tv_sec  = 2;
    tv.tv_usec = 0;

    // windows mingw64环境设置socket超时无法生效，此处需保留select防止调用线程被阻塞无法退出
    ret = select(server->fd + 1, &fds, NULL, NULL, &tv);
    if(0 < ret)
    {
        if(FD_ISSET(server->fd, &fds))
        {
            ret = recvfrom(server->fd, data, len, 0, (struct sockaddr*)&server->src_addr, &addr_len);
        }
    }

    return ret;
}

int create_udp_client(UDP_CLIENT_S *client, char *dst_ip, uint16_t dst_port)
{
    RETURN_IF_NULL(client, DBOT_INVALID_PARAM);
    RETURN_IF_NULL(dst_ip, DBOT_INVALID_PARAM);

    int fd = socket(AF_INET, SOCK_DGRAM, 0);
    if(0 > fd)
    {
        DBOT_LOG_E("create socket failed\r\n");
        return DBOT_SOCKET_FAIL;
    }

    do
    {
#if defined _WIN32

#else
        struct timeval timeout;
        timeout.tv_sec = 2;
        timeout.tv_usec = 0;

        if(0 != setsockopt(fd, SOL_SOCKET, SO_SNDTIMEO, (const char*)&timeout, sizeof(struct timeval)))
        {
            DBOT_LOG_E("set socket send timeout failed\r\n");
            break;
        }
        if(0 != setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, (const char*)&timeout, sizeof(struct timeval)))
        {
            DBOT_LOG_E("set socket recv timeout failed\r\n");
            break;
        }
#endif
        client->fd = fd;
        client->port = dst_port;
        client->dst_addr.sin_family = AF_INET;
        client->dst_addr.sin_port = htons(dst_port);
        client->dst_addr.sin_addr.s_addr = inet_addr(dst_ip);
        client->send = _udp_send;
        return DBOT_OK;
    } while (0);
    
    closesocket(fd);
    return DBOT_ERROR;
}

int create_udp_server(UDP_SERVER_S *server, uint16_t src_port)
{
    RETURN_IF_NULL(server, DBOT_INVALID_PARAM);

    int fd = socket(AF_INET, SOCK_DGRAM, 0);
    if(0 > fd)
    {
        DBOT_LOG_E("create socket failed\r\n");
        return DBOT_SOCKET_FAIL;
    }

    do
    {
        int on = 1;
        if(0 != setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, (const char*)&on, sizeof(on)))
        {
            DBOT_LOG_E("set socket reuse addr failed\r\n");
            break;
        }
#if defined _WIN32

#else
        struct timeval timeout;
        timeout.tv_sec = 2;
        timeout.tv_usec = 0;
        if(0 != setsockopt(fd, SOL_SOCKET, SO_SNDTIMEO, (const char*)&timeout, sizeof(struct timeval)))
        {
            DBOT_LOG_E("set socket send timeout failed\r\n");
            break;
        }
        if(0 != setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, (const char*)&timeout, sizeof(struct timeval)))
        {
            DBOT_LOG_E("set socket recv timeout failed\r\n");
            break;
        }
#endif
        struct sockaddr_in addr;
        memset(&addr, 0, sizeof(addr));
        addr.sin_family = AF_INET;
        addr.sin_addr.s_addr = INADDR_ANY;
        addr.sin_port = htons(src_port);
        if(0 > bind(fd, (struct sockaddr*)&addr, sizeof(addr)))
        {
            DBOT_LOG_E("bind dtp server port failed\r\n");
            break;
        }

#ifdef _WIN32
        int len = sizeof(struct sockaddr_in);
#else
        socklen_t len = sizeof(struct sockaddr_in);
#endif
        memset(&addr, 0, sizeof(addr));
        if(0 > getsockname(fd, (struct sockaddr*)&addr, &len))
        {
            DBOT_LOG_E("get dtp server port failed\r\n");
            break;
        }
        
        server->fd = fd;
        server->port = ntohs(addr.sin_port);
        server->recv = _udp_recv;
        DBOT_LOG_I("udp server started on port %d:%u\r\n", server->fd, server->port);
        return DBOT_OK;
    } while (0);
    
    closesocket(fd);
    return DBOT_ERROR;
}
