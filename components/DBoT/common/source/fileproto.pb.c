/**
 ***********************************************************************************************************************
 * Copyright (c) 2021, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        dbot_nanopb.c
 *
 * @brief
 *
 * @revision
 * Date         Author          Notes
 * 2022-08-19   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include "fileproto.pb.h"
#if !defined _WIN32 && !defined __linux__
#include <oneos_config.h>
#endif


#if PB_PROTO_HEADER_VERSION != 40
#error Regenerate this file with the current version of nanopb generator.
#endif

PB_BIND(ListFilesRequest, ListFilesRequest, AUTO)

PB_BIND(FileInfo, FileInfo, AUTO)

PB_BIND(ListFilesResponse, ListFilesResponse, AUTO)

PB_BIND(FileHeader, FileHeader, AUTO)

PB_BIND(FileFlow, FileFlow, AUTO)

#ifdef DBOT_DISCOVERY
extern bool client_list_files_response_callback(pb_istream_t *istream, const pb_field_iter_t *field);
#endif
#ifdef DBOT_PUBLISH
extern bool server_list_files_response_callback(pb_ostream_t *ostream, const pb_field_iter_t *field);
#endif

bool ListFilesResponse_callback(pb_istream_t *istream, pb_ostream_t *ostream, const pb_field_iter_t *field)
{
#ifdef DBOT_DISCOVERY
    if (istream != NULL && field->tag == ListFilesResponse_file_tag)
    {
        /* This callback function will be called once for each file received from the server */
        return client_list_files_response_callback(istream, field);
    }
#endif

#ifdef DBOT_PUBLISH
#ifdef FILESYSTEM_USING_SHOW_DIRECTORY
    if (ostream != NULL && field->tag == ListFilesResponse_file_tag)
    {
        /* This callback function will be called during the encoding */
        return server_list_files_response_callback(ostream, field);
    }

#endif
#endif

    return true;
}
