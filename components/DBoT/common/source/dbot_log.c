#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>
#include <stdint.h>
#include <unistd.h>
#include <stdarg.h>

#include "dbot_log.h"
#include "dbot_errno.h"

#if defined __linux__ || defined _WIN32

#else
#include "board.h"
#endif

static uint8_t gs_log_level = DEBUG; /* 默认日志级别 */
static char *gs_log_buf = NULL;

/*
 * dbot_log:
 *      根据日志级别判断是否打印日志
 *      @log_level: 需要打印的日志级别
 *
 * Date:
 *      2022-01-13
 *
 * Author:
 *      Jianqing.Sun
 */
void dbot_log(uint8_t log_level, const char *fmt, ...)
{
	va_list args;

    if (log_level > gs_log_level) {
        return;
    }

	memset(gs_log_buf, 0, MAX_LOG_BUF_LEN);

	va_start(args, fmt);
	vsnprintf(gs_log_buf, MAX_LOG_BUF_LEN - 1, fmt, args);
	va_end(args);

#if defined __linux__ || defined _WIN32
    printf("%s", gs_log_buf);
#else
    os_kprintf("%s", gs_log_buf);
#endif
    /* TODO: 将日志写入日志文件？ */
}

int dbot_log_init(uint8_t level)
{
    if(NULL == gs_log_buf)
    {
        gs_log_level = level;
        gs_log_buf = calloc(1, MAX_LOG_BUF_LEN);
        return (NULL == gs_log_buf)?DBOT_ERROR:DBOT_OK;
    }
    else
    {
        return DBOT_OK;
    }
}
