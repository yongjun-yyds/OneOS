#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>

#include "dbot_common.h"
#include "dbot_log.h"
#include "dbot_errno.h"
#include "dbot_json.h"
#include "publish_service.h"
#include "discovery_service.h"

#define DEVICE_ID           "device_id"
#define DEVICE_IP           "device_ip"
#define DEVICE_MODE         "mode"
#define DEVICE_SERVICE      "service"
#define GROUP_ID            "group_id"
#define MSG_SEQ             "seq"
#define SERVICE_ATTR        "attr"

#define RPC_SERVICE_ID      "id"
#define RPC_SERVICE_FUNC    "func"
#define RPC_SERVICE_PARAMS  "param"
#define RPC_SERVICE_RESULT  "result"
#define RPC_SERVICE_ERRNO   "errno"

int pack_discovery_package(uint32_t service_type, char **send_buf, void *attrs, int total)
{
    cJSON *root = NULL;
    int32_t ret = DBOT_ERROR;

    root = cJSON_CreateObject();
    if (NULL == root) {
        DBOT_LOG_E("create cjson root failed\n");
        ret = DBOT_OUT_OF_MEMORY;
    } else {
        device_info_local_t *local_device_info = get_local_device_info();

        cJSON_AddStringToObject(root, "resource", DBOT_PUBLISH_SERVICE);
        cJSON_AddNumberToObject(root, "total", total);
        cJSON_AddNumberToObject(root, DEVICE_ID, local_device_info->device_id);
        cJSON_AddStringToObject(root, DEVICE_IP, local_device_info->device_ip);
        cJSON_AddNumberToObject(root, DEVICE_SERVICE, service_type);
        cJSON_AddNumberToObject(root, MSG_SEQ, ++local_device_info->seq);
        if(NULL != attrs)
        {
            cJSON *array = cJSON_Parse(attrs);
            if(NULL != array)
            {
                cJSON_AddItemToObject(root, SERVICE_ATTR, array);
            }
        }

        *send_buf = (char *)cJSON_PrintUnformatted(root);
        if (NULL == *send_buf) {
            DBOT_LOG_E("cJSON_PrintUnformatted failed\n");
            ret = DBOT_OUT_OF_MEMORY;
        } else {
            // DBOT_LOG_D("send buffer is %s\n", *send_buf);
            ret = DBOT_OK;
        }
        cJSON_Delete(root);
    }
    return ret;
}

int unpack_discovery_package(const char *req_data, device_info_remote *remote_device)
{
    cJSON *result_root = cJSON_Parse(req_data);
    if(NULL == result_root) 
    {
        DBOT_LOG_E("json parse error!\n");
        return DBOT_JSON_PARSE_ERR;
    }

    cJSON *json_id = cJSON_GetObjectItem(result_root, DEVICE_ID);
    if(NULL != json_id) 
    {
        remote_device->device_id = json_id->valueint;
    }

    cJSON *total = cJSON_GetObjectItem(result_root, "total");
    if(NULL != total) 
    {
        remote_device->service_total = total->valueint;
    }

    cJSON *json_ip = cJSON_GetObjectItem(result_root, DEVICE_IP);
    if(NULL != json_ip) 
    {
        strncpy(remote_device->device_ip, json_ip->valuestring, MAX_IP_STR_LEN);
    }

    cJSON *json_service = cJSON_GetObjectItem(result_root, DEVICE_SERVICE);
    if(NULL != json_service)
    {
        remote_device->services.service.service_type = json_service->valueint;
    }

    cJSON *attr = cJSON_GetObjectItem(result_root, SERVICE_ATTR);
    if(NULL != attr)
    {
        int attr_cnt = cJSON_GetArraySize(attr);
        remote_device->services.service.server_attr_num = attr_cnt;
        if(0 != attr_cnt)
        {
            cJSON *tmp_root = cJSON_CreateObject();
            cJSON *tmp_attr = cJSON_Duplicate(attr, 1);
            cJSON_AddItemToObject(tmp_root, SERVICE_ATTR, tmp_attr);
            // 需要在函数外面释放attrs内存
            remote_device->services.service.attrs.attr = cJSON_PrintUnformatted(tmp_root);
            cJSON_Delete(tmp_root);
        }
    }

    cJSON *json_seq = cJSON_GetObjectItem(result_root, MSG_SEQ);
    if(NULL != json_seq)
    {
        remote_device->seq = json_seq->valueint;
    }

    cJSON_Delete(result_root);
    return DBOT_OK;
}

int pack_publish_package(uint32_t service_type, char **send_buf, void *attrs, int total)
{
    return pack_discovery_package(service_type, send_buf, attrs, total);
}

int unpack_publish_package(const char *req_data, device_info_remote *remote_device)
{
    return unpack_discovery_package(req_data, remote_device);
}
