/**
 ***********************************************************************************************************************
 * Copyright (c) 2021, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with 
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on 
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the 
 * specific language governing permissions and limitations under the License.
 *
 * @file        discovery_service.h
 *
 * @brief       discovery service functions
 *
 * @revision
 * Date         Author          Notes
 * 2022-01-13   Jianqing.Sun    First version
 ***********************************************************************************************************************
 */

#ifndef _DISCOVERY_SERVIVE_H
#define _DISCOVERY_SERVIVE_H

#include <pthread.h>
#include "dbot_list.h"
#include "dbot_common.h"
#include "dtcp_server.h"

#define HB_CHECK_PERIOD_TIME    2
#define HB_OFFLINE_CYCLES       12

typedef struct device_node_t {
    os_list_node_t node;
    device_info_remote device_info;
    long last_heartbeat_time;
    uint32_t online;
    uint32_t count;
    DTCP_CLIENT_S client;
}DEVICE_NODE_T;

typedef struct discovery_module_t
{
    os_list_node_t device_list;
    pthread_mutex_t device_list_lock;
    pthread_t hb_check_pid;
    uint32_t device_num;
    void *dbot_ctx;
}DISCOVERY_MODULE_S;

int dbot_discover_init(void *dbot_ctx);
int dbot_discovery_start(void);
int dbot_discovery_service(void);

#endif /* _DISCOVERY_SERVIVE_H */
