/**
 ***********************************************************************************************************************
 * Copyright (c) 2021, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with 
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on 
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the 
 * specific language governing permissions and limitations under the License.
 *
 * @file        publish_service.h
 *
 * @brief       publish service functions
 *
 * @revision
 * Date         Author          Notes
 * 2022-01-13   Jianqing.Sun    First version
 ***********************************************************************************************************************
 */

#ifndef _PUBLISH_SERVIVE_H
#define _PUBLISH_SERVIVE_H

#include <pthread.h>
#include "dbot_list.h"
#include "dbot_common.h"

typedef int(*ATTR_FUNC)(int len, char *req, char **resp);

typedef struct _attr_info_t 
{
    char func_name[MAX_SERVICE_ATTR_FUNC_NAME_LEN];
    char func_param[MAX_SERVICE_ATTR_FUNC_PARAM_LEN];
    ATTR_FUNC func;
}ATTR_INFO_S;

typedef struct _service_node_t 
{
    os_list_node_t node;
    uint32_t service_type;
    uint32_t attr_count;
    ATTR_INFO_S *attrs;
}SERVICE_NODE_T;

typedef struct publish_module_t
{
    os_list_node_t service_list;
    pthread_mutex_t service_list_lock;
    int service_cnt;
    pthread_t hb_pid;
    void *dbot_ctx;
}PUBLISH_MODULE_S;

/*
 * 服务能力类型
 */
typedef enum
{
    CAPABILITY_TYPE_LIGHT = 0,      /* 灯 */
    CAPABILITY_TYPE_TEMP = 1,       /* 温度 */
    CAPABILITY_TYPE_HUMI = 2,       /* 湿度 */
    CAPABILITY_TYPE_LCD = 3,        /* 屏 */
    CAPABILITY_TYPE_ILLU = 4,       /* 光照强度 */
    CAPABILITY_TYPE_CAMERA = 5,     /* 摄像头 */
    CAPABILITY_TYPE_MICROPHONE = 6, /* 麦克风 */
    CAPABILITY_TYPE_SPEAKER = 7,    /* 喇叭 */
    CAPABILITY_TYPE_SWITCH = 8,     /* 开关 */
    CAPABILITY_TYPE_FILESYSTEM,
    CAPABILITY_TYPE_UNKNOWN = 31,   /* 未知设备 */
} capability_list_t;

int dbot_publish_init(void *dbot_ctx);
int dbot_publish_start(void);
int dbot_add_service(uint32_t service, uint32_t attr_num, ATTR_INFO_S *attrs);
int dbot_publish_service(char *dst_ip);
int dbot_publish_all_services(char *dst_ip);
int dbot_publish_call_attr(int len, char *req, char **resp);

#endif /* _PUBLISH_SERVIVE_H */
