#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <pthread.h>
#include <unistd.h>
#include "dbot_common.h"
#include "dbot_json.h"
#include "dbot_log.h"
#include "dbot_errno.h"
#include "publish_service.h"
#include "dbot_service.h"
#include "dtcp_client.h"

static PUBLISH_MODULE_S *s_publish_module = NULL;

static char *pack_attrs(int attr_num, ATTR_INFO_S *attrs)
{
    RETURN_NULL_IF_NULL(attrs);

    cJSON *array = cJSON_CreateArray();
    RETURN_NULL_IF_NULL(array);
    cJSON *attr = NULL;
    for(int i = 0;i < attr_num;i ++)
    {
        attr = cJSON_CreateObject();
        cJSON_AddStringToObject(attr, "func", attrs[i].func_name);
        cJSON *param = cJSON_Parse(attrs[i].func_param);
        cJSON_AddItemToObject(attr, "param", param);
        // cJSON_AddNumberToObject(attr, "protocol", attrs[i].protocol);
        // cJSON_AddNumberToObject(attr, "stream", attrs[i].stream);
        cJSON_AddItemToArray(array, attr);
        attr = NULL;
    }
    char *str = cJSON_PrintUnformatted(array);
    cJSON_Delete(array);

    return str;
}

static int dbot_discovery_handler(void *dbot_ctx, int len, char *req_data, char **resp)
{
    RETURN_IF_NULL(req_data, DBOT_INVALID_PARAM);
    RETURN_IF_NULL(resp, DBOT_INVALID_PARAM);
    RETURN_IF_NULL(dbot_ctx, DBOT_INVALID_PARAM);

    DBOT_SERVICE_CONTEXT_S *ctx = (DBOT_SERVICE_CONTEXT_S*)dbot_ctx;
    DDP_SERVICE_CONTEXT_S *ddp = ctx->ddp;
    RETURN_IF_NULL(ddp, DBOT_INVALID_PARAM);
    cJSON *root = cJSON_Parse(req_data);
    RETURN_IF_NULL(root, DBOT_INVALID_PARAM);
    cJSON *dev = cJSON_GetObjectItem(root, "device_id");
    if(NULL != dev)
    {
        if(dev->valueint != get_device_id())
        {
            dbot_publish_service(inet_ntoa(ddp->src_addr.sin_addr));
        }
    }
    cJSON_Delete(root);
    
    return DBOT_OK;
}

static int dbot_session_handler(void *dbot_ctx, int len, char *req_data, char **resp)
{
    RETURN_IF_NULL(req_data, DBOT_INVALID_PARAM);
    RETURN_IF_NULL(resp, DBOT_INVALID_PARAM);
    RETURN_IF_NULL(dbot_ctx, DBOT_INVALID_PARAM);

    int ret = DBOT_ERROR;
    DBOT_SERVICE_CONTEXT_S *ctx = (DBOT_SERVICE_CONTEXT_S*)dbot_ctx;
    if(NULL != ctx->dtcp_client)
    {
        DBOT_LOG_E("Already connected to other dtcp server\r\n");
        return DBOT_ERROR;
    }

    DDP_SERVICE_CONTEXT_S *ddp = ctx->ddp;
    RETURN_IF_NULL(ddp, DBOT_INVALID_PARAM);

    cJSON *root = cJSON_Parse(req_data);
    RETURN_IF_NULL(root, DBOT_INVALID_PARAM);
    cJSON *dev = cJSON_GetObjectItem(root, "device_id");
    cJSON *ip = cJSON_GetObjectItem(root, "ip");
    cJSON *port = cJSON_GetObjectItem(root, "port");
    if(NULL != dev && NULL != ip && NULL != port)
    {
        if(dev->valueint == get_device_id())
        {
            if(DBOT_OK == dbot_dtcp_client_init(dbot_ctx, dev->valueint, ip->valuestring, port->valueint))
            {
                if(DBOT_OK != dbot_dtcp_client_start())
                {
                    dbot_dtcp_client_stop();
                }
                else
                {
                    ret = DBOT_OK;
                }
            }
        }
    }
    cJSON_Delete(root);
    
    DBOT_LOG_D("Connected to dtcp server %s\r\n", (DBOT_OK == ret)?"succeed":"failed");
    return ret;
}

SERVICE_NODE_T *find_exist_service(uint32_t service)
{
    RETURN_NULL_IF_NULL(s_publish_module);

    SERVICE_NODE_T *service_node = NULL;
    SERVICE_NODE_T *result = NULL;

    pthread_mutex_lock(&s_publish_module->service_list_lock);
    os_list_for_each_entry(service_node, &s_publish_module->service_list, SERVICE_NODE_T, node) 
    {
        if (service == service_node->service_type) 
        {
            result = service_node;
            break;
        }
    }
    pthread_mutex_unlock(&s_publish_module->service_list_lock);

    return result;
}

static int add_new_service(SERVICE_NODE_T *service)
{
    RETURN_IF_NULL(s_publish_module, DBOT_INVALID_PARAM);
    RETURN_IF_NULL(service, DBOT_INVALID_PARAM);

    pthread_mutex_lock(&s_publish_module->service_list_lock);
    os_list_add_tail(&s_publish_module->service_list, &service->node);
    s_publish_module->service_cnt ++;
    pthread_mutex_unlock(&s_publish_module->service_list_lock);

    return DBOT_OK;
}

int dbot_publish_init(void *dbot_ctx)
{
    RETURN_IF_NULL(dbot_ctx, DBOT_INVALID_PARAM);

    DBOT_SERVICE_CONTEXT_S *ctx = (DBOT_SERVICE_CONTEXT_S*)dbot_ctx;
    if(NULL == s_publish_module) 
    {
        s_publish_module = calloc(1, sizeof(PUBLISH_MODULE_S));
        RETURN_IF_NULL(s_publish_module, DBOT_MALLOC_FAIL);

        os_list_init(&s_publish_module->service_list);
        pthread_mutex_init(&s_publish_module->service_list_lock, NULL);
        s_publish_module->dbot_ctx = dbot_ctx;
        ctx->publish = s_publish_module;

        /* 增加发现服务侦听*/
        dbot_ddp_service_add_resource(DBOT_DISCOVERY_SERVICE, dbot_discovery_handler);
        dbot_ddp_service_add_resource(DBOT_SESSION_SERVICE, dbot_session_handler);

        DBOT_LOG_I("dbot publish service inited\r\n");
    }
    
    return DBOT_OK;
}

int dbot_publish_start(void)
{		
	return DBOT_OK;
}

int dbot_add_service(uint32_t service,  uint32_t attr_num, ATTR_INFO_S *attrs)
{
    RETURN_IF_NULL(s_publish_module, DBOT_INVALID_PARAM);
    RETURN_IF_NULL(attrs, DBOT_INVALID_PARAM);

    SERVICE_NODE_T *result = find_exist_service(service);
    if(NULL == result)
    {
        result = calloc(1, sizeof(SERVICE_NODE_T));
        result->attr_count = attr_num;
        result->attrs = attrs;
        result->service_type = service;
        os_list_init(&result->node);
        add_new_service(result);
    }

    return DBOT_OK;
}

int dbot_publish_service(char *dst_ip)
{
    RETURN_IF_NULL(s_publish_module, DBOT_INVALID_PARAM);

    int ret = DBOT_ERROR;
    char *send_buf = NULL;
    char *str = NULL;
    SERVICE_NODE_T *service_node = NULL;
    pthread_mutex_lock(&s_publish_module->service_list_lock);
    os_list_for_each_entry(service_node, &s_publish_module->service_list, SERVICE_NODE_T, node) 
    {
        /* 构建发布服务数据包 */
        str = pack_attrs(service_node->attr_count, service_node->attrs);
        if(NULL != str)
        {
            pack_publish_package(service_node->service_type, &send_buf, str, s_publish_module->service_cnt);
            if(NULL != send_buf)
            {
                ret = dbot_ddp_service_send_request(dst_ip, send_buf, strlen(send_buf));
                // DBOT_LOG_I("dbot reply service[%d] %s\r\n", service_node->service_type, (DBOT_OK == ret)?"succeed":"failed");
                FREE_POINTER(send_buf);
            }
            FREE_POINTER(str);
        }
    }
    pthread_mutex_unlock(&s_publish_module->service_list_lock);

    return ret;
}

int dbot_publish_call_attr(int len, char *req, char **resp)
{
    RETURN_IF_NULL(s_publish_module, DBOT_INVALID_PARAM);
    RETURN_IF_NULL(req, DBOT_INVALID_PARAM);
    RETURN_IF_NULL(resp, DBOT_INVALID_PARAM);

    int i = 0;
    int ret = DBOT_ERROR;
    char *str = NULL;
    cJSON *root = cJSON_Parse(req);
    RETURN_IF_NULL(root, DBOT_INVALID_PARAM);

    do
    {
        cJSON *device_id = cJSON_GetObjectItem(root, "device_id");
        BREAK_IF(NULL == device_id);
        if(get_device_id() != device_id->valueint)
        {
            break;
        }

        cJSON *service = cJSON_GetObjectItem(root, "service");
        BREAK_IF(NULL == service);

        cJSON *func = cJSON_GetObjectItem(root, "func");
        BREAK_IF(NULL == func);

        cJSON *params = cJSON_GetObjectItem(root, "params");

        SERVICE_NODE_T *service_node = NULL;
        pthread_mutex_lock(&s_publish_module->service_list_lock);
        os_list_for_each_entry(service_node, &s_publish_module->service_list, SERVICE_NODE_T, node) 
        {
            if(service->valueint == service_node->service_type)
            {
                for(i = 0;i < service_node->attr_count;i ++)
                {
                    if(!strcmp(func->valuestring, service_node->attrs[i].func_name))
                    {
                        if(NULL != service_node->attrs[i].func)
                        {
                            str = (NULL == params)?NULL:cJSON_PrintUnformatted(params);
                            ret = service_node->attrs[i].func(strlen(str), str, resp);
                            FREE_POINTER(str);
                        }
                        break;
                    }
                }
            }
        }
        pthread_mutex_unlock(&s_publish_module->service_list_lock);
    } while (0);
    DELETE_CJSON(root);    

    return ret;
}




