#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <pthread.h>
#include <unistd.h>
#include "discovery_service.h"
#include "dbot_common.h"
#include "dbot_json.h"
#include "dbot_log.h"
#include "dbot_errno.h"
#include "dbot_service.h"
#include "dtcp_client.h"

#ifdef _WIN32
#include <sys/time.h>
#endif

static DISCOVERY_MODULE_S *s_discovery_module = NULL;

DEVICE_NODE_T *find_exist_device(int device_id)
{
    RETURN_NULL_IF_NULL(s_discovery_module);

    DEVICE_NODE_T *device_node = NULL;
    DEVICE_NODE_T *result = NULL;
    os_list_for_each_entry(device_node, &s_discovery_module->device_list, DEVICE_NODE_T, node)
    {
        if(device_node->device_info.device_id == device_id)
        {
            result = device_node;
            break;
        }
    }

    return result;
}

bool is_service_exists(DEVICE_NODE_T *dev_node, device_info_remote *device)
{
    bool exists = false;
    if(NULL != dev_node &&  NULL != device)
    {
        service_info_t *service = NULL;
        os_list_for_each_entry(service, &dev_node->device_info.services.service_list, service_info_t, node)
        {
            if(service->service_type == device->services.service.service_type)
            {
                exists = true;
                break;
            }
        }
    }

    return exists;
}

void add_service(DEVICE_NODE_T *dev_node, device_info_remote *device)
{
    if(NULL != dev_node && NULL != device)
    {
        if(0 != device->services.service.server_attr_num && NULL != device->services.service.attrs.attr)
        {
            service_info_t *service = calloc(1, sizeof(service_info_t));
            if(NULL != service)
            {
                service->attrs.attr = calloc(1, strlen(device->services.service.attrs.attr) + 1);
                if(NULL != service->attrs.attr)
                {
                    os_list_init(&service->node);
                    service->server_attr_num = device->services.service.server_attr_num;
                    service->service_type = device->services.service.service_type;
                    strcpy(service->attrs.attr, device->services.service.attrs.attr);
                    os_list_add_tail(&dev_node->device_info.services.service_list, &service->node);
                    dev_node->device_info.service_cnt ++;
                    DBOT_LOG_I("get new service [%d][%d]\r\n", device->device_id, device->services.service.service_type);
                }
                else
                {
                    FREE_POINTER(service);
                }
            }
        }
    }
}

void clear_service(DEVICE_NODE_T *dev_node)
{
    if(NULL != dev_node)
    {
        service_info_t *service = NULL;
        service_info_t *service_next = NULL;
        os_list_for_each_entry_safe(service, service_next, &dev_node->device_info.services.service_list, service_info_t, node)
        {
            os_list_del(&service->node);
            FREE_POINTER(service->attrs.attr);
            FREE_POINTER(service);
        }
        dev_node->device_info.service_cnt = 0;
    }
}

void add_device(device_info_remote *device)
{
    if(NULL != device && NULL != s_discovery_module)
    {
        DEVICE_NODE_T *dev = calloc(1, sizeof(DEVICE_NODE_T));
        if(NULL != dev)
        {
            dev->count = 0;
            dev->online = 1;
            dev->last_heartbeat_time = get_current_time();
            dev->device_info.device_id = device->device_id;
            dev->device_info.seq = device->seq;
            dev->device_info.service_total = device->service_total;
            strncpy(dev->device_info.device_ip, device->device_ip, sizeof(dev->device_info.device_ip));
            os_list_init(&dev->node);
            os_list_init(&dev->device_info.services.service_list);
            dev->client.fd = -1;

            // 先将device节点挂上链表
            os_list_add_tail(&s_discovery_module->device_list, &dev->node);
            s_discovery_module->device_num ++;

            // 然后将service节点挂上子链表
            add_service(dev, device);
            DBOT_LOG_I("device upline %d\r\n", device->device_id);
        }
    }
}

device_info_remote *get_remote_device_info(int dev_id)
{
    device_info_remote *result = NULL;
    pthread_mutex_lock(&s_discovery_module->device_list_lock);
    DEVICE_NODE_T *dev = NULL;
    os_list_for_each_entry(dev, &s_discovery_module->device_list, DEVICE_NODE_T, node)
    {
        if(dev_id == dev->device_info.device_id)
        {
            result = &dev->device_info;
            break;
        }
    }
    pthread_mutex_unlock(&s_discovery_module->device_list_lock);
    return result; 
}

static int dbot_publish_handler(void *dbot_ctx, int len, char *req_data, char **resp)
{
    RETURN_IF_NULL(s_discovery_module, DBOT_INVALID_PARAM);
    RETURN_IF_NULL(req_data, DBOT_INVALID_PARAM);

    device_info_remote remote_device;
    memset(&remote_device, 0, sizeof(device_info_remote));
    int ret = unpack_publish_package(req_data, &remote_device);
    if(DBOT_OK == ret) 
    {
        if(remote_device.device_id != get_device_id())
        {
            // 统一上锁，后续流程函数不再上锁，保证数据安全以及防止死锁
            pthread_mutex_lock(&s_discovery_module->device_list_lock);
            DEVICE_NODE_T *dev = find_exist_device(remote_device.device_id);
            // 不存在，则往链表添加新设备
            if(NULL == dev)
            {
                add_device(&remote_device);
            }
            // 存在，则查找该设备下是否存在本条service
            else
            {
                // 收到发布消息，同时更新时间戳，辅助心跳作用
                dev->last_heartbeat_time = get_current_time();
                dev->device_info.service_total = remote_device.service_total;
                // 如果设备是离线状态，则重新标记为在线状态
                if(!dev->online)
                {
                    DBOT_LOG_I("device upline %d\r\n", dev->device_info.device_id);
                    dev->online = 1;
                    dev->last_heartbeat_time = get_current_time();
                }
                
                bool exists = is_service_exists(dev, &remote_device);
                //不存在，则添加service到子链表
                if(!exists)
                {
                    add_service(dev, &remote_device);
                }
            }
            pthread_mutex_unlock(&s_discovery_module->device_list_lock);
        }
        FREE_POINTER(remote_device.services.service.attrs.attr);
    }

    return DBOT_OK;
}

static int dbot_heartbeat_handler(void *dbot_ctx, int len, char *req_data, char **resp)
{
    RETURN_IF_NULL(s_discovery_module, DBOT_INVALID_PARAM);
    RETURN_IF_NULL(req_data, DBOT_INVALID_PARAM);

    cJSON *root = cJSON_Parse(req_data);
    if(NULL != root)
    {
        cJSON *dev_id = cJSON_GetObjectItem(root, "device_id");
        if(NULL != dev_id)
        {
            if(dev_id->valueint != get_device_id())
            {
                pthread_mutex_lock(&s_discovery_module->device_list_lock);
                DEVICE_NODE_T *dev = find_exist_device(dev_id->valueint);
                if(NULL != dev)
                {
                    dev->last_heartbeat_time = get_current_time();
                }
                pthread_mutex_unlock(&s_discovery_module->device_list_lock);
            }
        }
        cJSON_Delete(root);
    }
    return DBOT_OK;
}

int dbot_discover_init(void *dbot_ctx)
{
    RETURN_IF_NULL(dbot_ctx, DBOT_INVALID_PARAM);

    DBOT_SERVICE_CONTEXT_S *ctx = (DBOT_SERVICE_CONTEXT_S*)dbot_ctx;
    if(NULL == s_discovery_module) 
    {
        s_discovery_module = calloc(1, sizeof(DISCOVERY_MODULE_S));
        RETURN_IF_NULL(s_discovery_module, DBOT_MALLOC_FAIL);

        os_list_init(&s_discovery_module->device_list);
        pthread_mutex_init(&s_discovery_module->device_list_lock, NULL);
        s_discovery_module->dbot_ctx = dbot_ctx;
        ctx->discovery = s_discovery_module;

        /* 增加发布服务侦听*/
        dbot_ddp_service_add_resource(DBOT_PUBLISH_SERVICE,  dbot_publish_handler);

        /* 增加发布服务侦听*/
        dbot_ddp_service_add_resource(DBOT_HEARTBEAT_SERVICE,  dbot_heartbeat_handler);

        DBOT_LOG_I("dbot discover service inited\r\n");
    }
    
    return DBOT_OK;
}

int dbot_discovery_start(void)
{
    return DBOT_OK;
}

int dbot_discovery_service(void)
{
    char send_buf[50];
    memset(send_buf, 0, sizeof(send_buf));
    snprintf(send_buf, sizeof(send_buf), "{\"resource\":\"discovery\", \"device_id\":%d}", get_device_id());
    return dbot_ddp_service_send_request(NULL, send_buf, strlen(send_buf));
}




