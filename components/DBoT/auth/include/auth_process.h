/**
 ***********************************************************************************************************************
 * Copyright (c) 2022, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with 
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on 
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the 
 * specific language governing permissions and limitations under the License.
 *
 * @file        auth_process.h
 *
 * @brief       authentication process
 *
 * @revision
 * Date         Author          Notes
 * 2021-12-31   JianQing.Sun    First version
 ***********************************************************************************************************************
 */

#ifndef _AUTH_PROCESS_H
#define _AUTH_PROCESS_H

#include <stdio.h>

#include "dbot_list.h"

typedef struct _auth_conn_t {
    os_list_node_t list;
    int fd;
    char device_ip[MAX_IP_STR_LEN + 1];
    bool auth_result;
} auth_conn_t;

extern int32_t start_auth_process(void);
extern int32_t stop_auth_process(void);
extern auth_conn_t* find_auth_conn_by_ip(const char *ip);

#endif /* _AUTH_PROCESS_H */
