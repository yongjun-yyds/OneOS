/**
 ***********************************************************************************************************************
 * Copyright (c) 2022, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with 
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on 
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the 
 * specific language governing permissions and limitations under the License.
 *
 * @file        auth_process.c
 *
 * @brief       authentication process
 *
 * @revision
 * Date         Author          Notes
 * 2021-12-31   JianQing.Sun    First version
 ***********************************************************************************************************************
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <netdb.h>
#include <arpa/inet.h>

#include "dbot_common.h"
#include "dbot_log.h"
#include "dbot_errno.h"
#include "dbot_lock.h"
#include "dbot_thread.h"
#include "auth_process.h"
#include "tcp_manager.h"

#define MAX_RECV_DATA_LEN   1024        /* 接收数据的最大长度 */
#define AUTH_REQUEST_STR    "Just Demo" /* TODO: 简易通过发送一个字符串进行认证，需要后期增加更安全的认证 */

static dbot_list_manager_t *gs_auth_list_manager = NULL;

/*
 * add_auth_conn_node:
 *      增加认证连接节点
 *      @fd: 认证连接的socket id
 *      @ip: 认证连接的发起端ip
 *
 * Return:
 *      DBOT_OK       -       增加成功
 *      other           -       增加失败
 *
 * Date:
 *      2022-01-19
 *
 * Author:
 *      Jianqing.Sun
 */
static int32_t add_auth_conn_node(int32_t fd, const char *ip)
{
    if (NULL == ip) {
        return DBOT_INVALID_PARAM;
    }

    if (AUTH_CONN_MAX_NUM <= gs_auth_list_manager->num) {
        DBOT_LOG_E("Too many authentication connections\n");
        return DBOT_NUM_EXCESSIVE;
    }

    auth_conn_t *node = calloc(1, sizeof(auth_conn_t));
    if (NULL == node) {
        DBOT_LOG_E("calloc auth_conn_t fail\n");
        return DBOT_OUT_OF_MEMORY;
    }

    node->fd = fd;
    strncpy(node->device_ip, ip, MAX_IP_STR_LEN);
    os_list_add_tail(&gs_auth_list_manager->list, &node->list);
    gs_auth_list_manager->num++;

    DBOT_LOG_D("add new node(%d) ip(%s) success\n", node->fd, ip);
    return DBOT_OK;
}

/*
 * find_auth_conn_by_fd:
 *      通过socket fd查找对应的认证连接节点
 *      @fd: 认证连接的socket id
 *
 * Return:
 *      node        -       认证连接节点的指针,未找到则为NULL
 *
 * Date:
 *      2022-01-19
 *
 * Author:
 *      Jianqing.Sun
 */
static auth_conn_t* find_auth_conn_by_fd(int32_t fd)
{
    auth_conn_t *node = NULL;
    auth_conn_t *next = NULL;

    dbot_mutex_lock(gs_auth_list_manager->lock);
    os_list_for_each_entry_safe(node, next, &gs_auth_list_manager->list, auth_conn_t, list) {
        if (node->fd == fd) {
            dbot_mutex_unlock(gs_auth_list_manager->lock);
            return node;
        }
    }
    dbot_mutex_unlock(gs_auth_list_manager->lock);

    return NULL;
}

/*
 * find_auth_conn_by_ip:
 *      通过ip地址查找对应的认证连接节点
 *      @fd: 认证连接的发起端ip
 *
 * Return:
 *      node        -       认证连接节点的指针,未找到则为NULL
 *
 * Date:
 *      2022-01-19
 *
 * Author:
 *      Jianqing.Sun
 */
auth_conn_t* find_auth_conn_by_ip(const char *ip)
{
    auth_conn_t *node = NULL;
    auth_conn_t *next = NULL;

    if (NULL == ip) {
        return NULL;
    }

    dbot_mutex_lock(gs_auth_list_manager->lock);
    os_list_for_each_entry_safe(node, next, &gs_auth_list_manager->list, auth_conn_t, list) {
        if (0 == strcmp(node->device_ip, ip)) {
            dbot_mutex_unlock(gs_auth_list_manager->lock);
            return node;
        }
    }
    dbot_mutex_unlock(gs_auth_list_manager->lock);

    return NULL;
}

/*
 * close_auth_data_conn:
 *      关闭一个认证数据连接
 *      @node: 认证数据连接节点指针
 *
 * Date:
 *      2022-01-05
 *
 * Author:
 *      Jianqing.Sun
 */
static void close_auth_data_conn(auth_conn_t *node)
{
    if (NULL == node)
        return;

    closesocket(node->fd); /* 关闭连接 */
    os_list_del(&node->list); /* 从认证连接列表中删除 */
    free(node); /* 释放认证节点 */
}

/*
 * process_new_session:
 *      处理新的认证连接
 *      @fd: 认证连接的socket fd
 *      @ip: 认证连接的发起端ip
 *
 * Return:
 *      DBOT_OK       -       连接处理成功
 *      other           -       连接处理失败
 *
 * Date:
 *      2022-01-19
 *
 * Author:
 *      Jianqing.Sun
 */
static int32_t process_new_session(int32_t fd, const char *ip)
{
    auth_conn_t *node = NULL;

    DBOT_LOG_D("fd = %d, ip is %s\n", fd, ip);

    if ((0 > fd)  || (NULL == ip)) {
        return DBOT_INVALID_PARAM;
    }

    node = find_auth_conn_by_fd(fd);
    if (NULL != node) { 
        /* fd被重用，关闭之前的连接并删除对应节点 */
        DBOT_LOG_E("The fd is reused!\n");
        close_auth_data_conn(node);
        return DBOT_FD_REUSED;
    }

    node = find_auth_conn_by_ip(ip);
    if (NULL != node) { 
        /* 此ip已经在进行认证 */
        if (true == node->auth_result) { 
            /* 此ip已经认证成功，不需要再新建认证节点 */
            DBOT_LOG_W("The ip address is authenticated\n");
            return DBOT_AUTHENTICATED;
        } else { 
            /* 此ip尚未认证成功，关闭之前的连接并删除对应节点 */
            DBOT_LOG_W("closesocket old auth conn\n");
            close_auth_data_conn(node);
        }
    }

    return add_auth_conn_node(fd, ip);
}

/*
 * process_new_session:
 *      处理新的认证连接
 *
 * Date:
 *      2022-01-05
 *
 * Author:
 *      Jianqing.Sun
 */
static void process_auth_connect(void)
{
    int32_t accept_fd = -1;
    struct sockaddr_in client_sockaddr = {0};
    socklen_t addrLen = sizeof(client_sockaddr);

    DBOT_LOG_D("recv new auth connection\n");
    accept_fd = accept(gs_auth_list_manager->fd, (struct sockaddr *)(&client_sockaddr), &addrLen);
    if (accept_fd < 0) {
        DBOT_LOG_E("accept data fail, accept_fd is %d!\n", accept_fd);
        return;
    }

    /* 处理新的认证连接 */
    if (DBOT_OK != process_new_session(accept_fd, inet_ntoa(client_sockaddr.sin_addr))) {
        closesocket(accept_fd);
    }

}

/*
 * do_device_auth:
 *      进行设备认证
 *      @node: 认证数据节点指针
 *      @data: 认证数据
 *      @data_len: 认证数据长度
 *
 * Return:
 *      DBOT_OK       -       认证成功
 *      other           -       认证失败
 *
 * Date:
 *      2022-01-06
 *
 * Author:
 *      Jianqing.Sun
 */
static int32_t do_device_auth(auth_conn_t *node, char *data, int32_t data_len)
{
    DBOT_LOG_D("data is %s, len is %d\n", data, data_len);
    if (data_len < strlen(AUTH_REQUEST_STR)) {
        DBOT_LOG_E("The data is too short\n");
        return DBOT_ERROR;
    }

    if (0 == strncmp(data, AUTH_REQUEST_STR, strlen(AUTH_REQUEST_STR))) {
        node->auth_result = true;
        DBOT_LOG_I("ip %s auth success\n", node->device_ip);
        send(node->fd, RPC_SERVER_PORT_STR, strlen(RPC_SERVER_PORT_STR), 0);
    } else {
        DBOT_LOG_E("auth fail\n");
    }
    return DBOT_OK;
}

/*
 * process_auth_data:
 *      处理设备认证数据
 *      @node: 认证数据节点指针
 *
 * Date:
 *      2022-01-06
 *
 * Author:
 *      Jianqing.Sun
 */
static void process_auth_data(auth_conn_t *node)
{
    char *data_recv = NULL;
    int32_t recv_len = 0;

    if (NULL == node) {
        return;
    }

    data_recv = calloc(1, MAX_RECV_DATA_LEN);
    recv_len = recv(node->fd, data_recv, MAX_RECV_DATA_LEN, 0);
    if (0 >= recv_len) {
        DBOT_LOG_E("recv data failed, fd is %d, recv_len is %d\n", node->fd, recv_len);
        close_auth_data_conn(node);
        return;
    }

    do_device_auth(node, data_recv, recv_len);
}

/*
 * process_auth_session:
 *      处理认证会话
 *      @read_set: 认证会话集合
 *
 * Date:
 *      2022-01-06
 *
 * Author:
 *      Jianqing.Sun
 */
static void process_auth_session(fd_set *read_set)
{
    auth_conn_t *node = NULL;
    auth_conn_t *next = NULL;

	if (NULL == read_set) {
		return;
	}

    /* 管理监听端口收到了新的请求认证连接 */
	if (FD_ISSET(gs_auth_list_manager->fd, read_set)) {
        process_auth_connect();
	}

    dbot_mutex_lock(gs_auth_list_manager->lock);
    /* 遍历认证数据连接 */
    os_list_for_each_entry_safe(node, next, &gs_auth_list_manager->list, auth_conn_t, list) {
        /* 认证数据监听端口收到了数据 */
        if (FD_ISSET(node->fd, read_set)) {
            process_auth_data(node);
        }
    }
    dbot_mutex_unlock(gs_auth_list_manager->lock);

	return;
}

/*
 * process_auth_session:
 *      关闭所有认证数据会话
 *
 * Date:
 *      2022-01-06
 *
 * Author:
 *      Jianqing.Sun
 */
static void close_all_data_session(void)
{
    auth_conn_t *node = NULL;
    auth_conn_t *next = NULL;

    /* 遍历认证数据连接，找到最大的资源描述符 */
    dbot_mutex_lock(gs_auth_list_manager->lock);
    os_list_for_each_entry_safe(node, next, &gs_auth_list_manager->list, auth_conn_t, list) {
        close_auth_data_conn(node);
    }
    dbot_mutex_unlock(gs_auth_list_manager->lock);
}


/*
 * init_fd_set:
 *      初始化文件描述符集合
 *      @read_set: 读文件描述符集合
 *      @except_set: 异常文件描述符集合
 *
 * Return:
 *      max_fd: 文件描述符最大值
 *
 * Date:
 *      2022-01-05
 *
 * Author:
 *      Jianqing.Sun
 */
static int32_t init_fd_set(fd_set *read_set, fd_set *except_set)
{
    auth_conn_t *node = NULL;
    auth_conn_t *next = NULL;
    int32_t max_fd = -1;

    FD_ZERO(read_set);
    FD_ZERO(except_set);
    max_fd = gs_auth_list_manager->fd;

    FD_SET(gs_auth_list_manager->fd, read_set);
    dbot_mutex_lock(gs_auth_list_manager->lock);
    /* 遍历所有认证数据连接，将其文件描述符加入读和异常集合 */
    os_list_for_each_entry_safe(node, next, &gs_auth_list_manager->list, auth_conn_t, list) {
        if (max_fd < node->fd) { /* 更新文件描述符最大值 */
            max_fd = node->fd;
        }
        FD_SET(node->fd, read_set);
        FD_SET(node->fd, except_set);
    }
    dbot_mutex_unlock(gs_auth_list_manager->lock);
    return max_fd;
}

/*
 * remove_except_session:
 *      移除所有异常会话
 *      @except_set: 异常会话集合
 *
 * Return:
 *      true        -       移除成功
 *      false       -       移除失败
 *
 * Date:
 *      2022-01-06
 *
 * Author:
 *      Jianqing.Sun
 */
static bool remove_except_session(fd_set *except_set)
{
    auth_conn_t *node = NULL;
    auth_conn_t *next = NULL;

    if (NULL == except_set)
        return false;

    dbot_mutex_lock(gs_auth_list_manager->lock);
    os_list_for_each_entry_safe(node, next, &gs_auth_list_manager->list, auth_conn_t, list) {
        if (FD_ISSET(node->fd, except_set)) {
            close_auth_data_conn(node);
        }
    }
    dbot_mutex_unlock(gs_auth_list_manager->lock);

    return true;
}

/*
 * wait_auth_process:
 *      认证处理主入口,循环处理认证连接
 *      @arg: 未使用
 *
 * Date:
 *      2022-01-05
 *
 * Author:
 *      Jianqing.Sun
 */
static void wait_auth_process(void *arg)
{
    fd_set read_set;
    fd_set except_set;
    int32_t max_fd = -1;

    while (1) {
        max_fd = init_fd_set(&read_set, &except_set);
        if (0 > max_fd)
            break;

        int32_t ret = select(max_fd + 1, &read_set, NULL, &except_set, NULL);
        if (ret > 0) {
            DBOT_LOG_D("recv data\n");
            process_auth_session(&read_set);
        } else if (ret < 0) {
            DBOT_LOG_E("select fail\n");
            /* select错误，先尝试移除异常的连接 */
            if (true != remove_except_session(&except_set)) {
                DBOT_LOG_E("remove except session fail, stop listener\n");
                close_all_data_session();
                continue;
            }
            stop_auth_process();
        }
    }
    return;
}

/*
 * start_auth_process:
 *      开启认证处理
 *
 * Return:
 *      DBOT_OK       -       开启成功
 *      other           -       开启失败
 * Date:
 *      2022-01-05
 *
 * Author:
 *      Jianqing.Sun
 */
int32_t start_auth_process(void)
{
    gs_auth_list_manager = calloc(1, sizeof(dbot_list_manager_t));
    if (NULL == gs_auth_list_manager) {
        DBOT_LOG_E("calloc gs_auth_list_manager fail!\n");
        return DBOT_OUT_OF_MEMORY;
    }
    os_list_init(&gs_auth_list_manager->list);
    gs_auth_list_manager->fd = init_tcp_server(SERVER_IP_STR, AUTH_SERVER_PORT_STR);
    gs_auth_list_manager->num = 0;
    gs_auth_list_manager->lock = dbot_mutex_init();

    if (0 == dbot_create_thread("dbot_auth", wait_auth_process))
        return DBOT_OK;
    else
        return DBOT_ERROR;
}

/*
 * stop_auth_process:
 *      关闭认证处理
 *
 * Return:
 *      DBOT_OK       -       关闭成功
 *      other           -       关闭失败
 * Date:
 *      2022-01-19
 *
 * Author:
 *      Jianqing.Sun
 */
int32_t stop_auth_process(void)
{
    close_all_data_session();

    closesocket(gs_auth_list_manager->fd);
    gs_auth_list_manager->fd = -1;

    return DBOT_OK;
}
