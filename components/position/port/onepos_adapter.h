/**
 ***********************************************************************************************************************
 * Copyright (c) 2021, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        cms_adapter.h
 *
 * @brief       Adaptation layer for onepos.
 *
 * @revision
 * Date         Author          Notes
 * 2022-10-10   OneOS Team      First version.
 ***********************************************************************************************************************/

#ifndef __ONEPOS_ADAPTER_H__
#define __ONEPOS_ADAPTER_H__

/* gnss依赖：driver */
/* 网络定位依赖：cms、molink、cjson */
/* agnss/rtk/rtd依赖：driver、cms */
/* onepos_cmd : shell */

/* using oneos  */
#if 1
#include <stdio.h>
#include <os_sem.h>
#include <stddef.h>
#include <device.h>
#include <os_list.h>
#include <os_task.h>
#include <os_util.h>
#include <os_mutex.h>
#include <os_timer.h>
#include <os_errno.h>
#include <os_types.h>
#include <os_assert.h>
#include <os_memory.h>

typedef os_err_t        err_t;
typedef os_tick_t       tick_t;
typedef os_bool_t       bool_t;
typedef os_slist_node_t slist_node_t;

typedef os_task_id      onepos_task_t;
typedef os_mutex_id     onepos_mutex_t;
typedef os_timer_id     onepos_timer_t;
typedef os_device_t     onepos_device_t;
typedef os_semaphore_id onepos_sem_t;

typedef struct os_device_cb_info onepos_sdevice_cb_info_t;

#define ONEPOS_EOK    OS_SUCCESS
#define ONEPOS_EIO    OS_EIO
#define ONEPOS_ERROR  OS_FAILURE
#define ONEPOS_EBUSY  OS_BUSY
#define ONEPOS_EPERM  OS_EPERM
#define ONEPOS_ENOMEM OS_NOMEM

#define ONEPOS_TICK_MAX        OS_TICK_MAX
#define ONEPOS_TICK_PER_SECOND OS_TICK_PER_SECOND

#define ONEPOS_TRUE  OS_TRUE
#define ONEPOS_FALSE OS_FALSE
#define ONEPOS_NULL  OS_NULL

#define onepos_task_create  os_task_create
#define onepos_task_msleep  os_task_msleep
#define onepos_task_destroy os_task_destroy
#define onepos_task_suspend os_task_suspend
#define onepos_task_startup os_task_startup
#define onepos_task_find    os_task_get_id

#define onepos_sem_post    os_semaphore_post
#define onepos_sem_wait    os_semaphore_wait
#define onepos_sem_create  os_semaphore_create
#define onepos_sem_destroy os_semaphore_destroy

#define onepos_mutex_create           os_mutex_create
#define onepos_mutex_destroy          os_mutex_destroy
#define onepos_mutex_recursive_lock   os_mutex_recursive_lock
#define onepos_mutex_recursive_unlock os_mutex_recursive_unlock

#define onepos_timer_create            os_timer_create
#define onepos_timer_destroy           os_timer_destroy
#define onepos_timer_start             os_timer_start
#define onepos_timer_stop              os_timer_stop
#define onepos_timer_set_timeout_ticks os_timer_set_timeout_ticks

#define ONEPOS_TIMER_FLAG_ONE_SHOT OS_TIMER_FLAG_ONE_SHOT /* Oneshot timer. */
#define ONEPOS_TIMER_FLAG_PERIODIC OS_TIMER_FLAG_PERIODIC /* Periodic timer. */

#define onepos_device_open          os_device_open
#define onepos_device_find          os_device_find
#define onepos_device_close         os_device_close
#define onepos_device_control       os_device_control
#define onepos_device_write_block   os_device_write_block
#define onepos_device_read_nonblock os_device_read_nonblock

#define ONEPOS_ASSERT   OS_ASSERT
#define onepos_printf   os_kprintf
#define onepos_snprintf snprintf

#define onepos_malloc os_malloc
#define onepos_free   os_free

#define onepos_slist_add           os_slist_add
#define onepos_slist_del           os_slist_del
#define onepos_slist_init          os_slist_init
#define onepos_slist_next          os_slist_next
#define onepos_slist_entry         os_slist_entry
#define onepos_slist_add_tail      os_slist_add_tail
#define onepos_slist_for_each_safe os_slist_for_each_safe

#define onepos_schedule_lock   os_schedule_lock
#define onepos_schedule_unlock os_schedule_unlock

/* other system */
#else

#endif

#endif /*__ONEPOS_ADAPTER_H__*/
