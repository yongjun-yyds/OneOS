#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <shell.h>
#include "onepos_adapter.h"
struct onepos_cmd_des
{
    const char *cmd;
    int32_t (*fun)(int32_t argc, int8_t *argv[]);
};

#if defined(ONEPOS_USING_NET_POS)
#include "onepos_net_pos_if.h"

static int32_t onepos_status_cmd_func(int32_t argc, int8_t *argv[])
{
    int8_t sta_str[ONEPOS_MAX_STA][30] = {"closing", "runing", "sig_sev_runing", "will_close"};

    if (2 == argc)    //  read
    {
        onepos_printf("status : %s\r\n", sta_str[onepos_get_server_sta()]);
    }
    else
    {
        onepos_printf("this cmd do not support input param\r\n");
    }

    return 0;
}

static int32_t onepos_interval_cmd_func(int32_t argc, int8_t *argv[])
{
    if (2 == argc)    //  read
    {
        onepos_printf("interval : %d sec\r\n", onepos_get_pos_interval());
    }
    else if (3 == argc && _onepos_isdigit(argv[2]))
    {
        onepos_set_pos_interval(atoi((char *)(char *)argv[2]));
    }
    else
    {
        onepos_printf("this cmd do not support input format, should be (onepos interval <secodes>)\r\n");
    }

    return 0;
}

static int32_t onepos_start_cmd_func(int32_t argc, int8_t *argv[])
{
    if (ONEPOS_CLOSING == onepos_get_server_sta())
    {
        onepos_start_server();
    }
    else if (ONEPOS_WILL_CLOSE == onepos_get_server_sta())
    {
        onepos_printf("onepos is will close, pls wait ...\r\n");
    }
    else
    {
        onepos_printf("onepos is already runing!\r\n");
    }

    return 0;
}

static int32_t onepos_stop_cmd_func(int32_t argc, int8_t *argv[])
{
    if (ONEPOS_RUNING == onepos_get_server_sta())
    {
        onepos_stop_server();
    }
    else if (ONEPOS_SIG_RUNING == onepos_get_server_sta())
    {
        onepos_printf("onepos single server is runing, it will automatic stop!\r\n");
    }
    else if (ONEPOS_WILL_CLOSE == onepos_get_server_sta())
    {
        onepos_printf("onepos is will close, pls wait ...\r\n");
    }
    else
    {
        onepos_printf("onepos is already closed!\r\n");
    }

    return 0;
}

static int32_t onepos_get_latest_position_cmd_func(int32_t argc, int8_t *argv[])
{
    onepos_pos_t pos_info = {0};

    memset(&pos_info, 0, sizeof(onepos_pos_t));

    onepos_get_latest_position(&pos_info);
    onepos_info_print(&pos_info);

    return 0;
}

static int32_t onepos_server_type_cmd_func(int32_t argc, int8_t *argv[])
{
    int8_t server_type_str[ONEPOS_MAX_TYPE][15] = {"circulation", "single"};

    if (2 == argc)    //  read
    {
        onepos_printf("server_type : %s\r\n", server_type_str[onepos_get_server_type()]);
    }
    else if (3 == argc && IS_VAILD_SEV_TYPE(atoi((char *)argv[2])))    // set
    {
        onepos_set_server_type((onepos_serv_type_t)atoi((char *)argv[2]));
    }
    else
    {
        onepos_printf("this cmd do not support input format, shoud be (onepos sev_type <0/1>)\r\n");
    }

    return 0;
}
#endif

static int32_t onepos_test_cmd_func(int32_t argc, int8_t *argv[])
{
    onepos_printf("%s entry!\n", __func__);
    return 0;
}
#if defined(GNSS_USING_RCVR)
#include "rcvr_object.h"
#if defined(GNSS_NMEA_0183_PROT)
#include "nmea_0183.h"
#endif
/* ops_test test_case rcvr_name(or ONEPOS_NULL) */
static int32_t onepos_gnss_rcvr_ops_test(int32_t argc, int8_t *argv[])
{
    err_t          result        = ONEPOS_EOK;
    uint32_t       test_case     = 0;
    rcvr_object_t *rcvr          = ONEPOS_NULL;
    int8_t         temp_str[256] = {0};

    memset(temp_str, 0, sizeof(temp_str));

    if (argc >= 3)
    {
        test_case = atoi((char *)argv[2]);
    }
    else
    {
        onepos_printf("input param is eroor.\r\n");
        return ONEPOS_EPERM;
    }

    if (3 == argc)
        rcvr = rcvr_object_get_default();
    else
        rcvr = rcvr_object_get_by_name((const char *)argv[3]);

    if (rcvr)
    {
        onepos_printf("Using rcvr : %s\t\r\n", rcvr->name);

        switch (test_case)
        {
#ifdef GNSS_NMEA_0183_PROT
        case 0:
        {
            nmea_t nmea_data = {0};
            memset(&nmea_data, 0, sizeof(nmea_t));
            if (ONEPOS_EOK == get_rcvr_data(rcvr, (void *)&nmea_data, sizeof(nmea_t), RCVR_NMEA_0183_PROT))
            {
                onepos_printf("one_position_test get data:\r\n");
                onepos_printf("\t date : 20%02d - %d - %d\r\n",
                              nmea_data.rmc_frame.date.year,
                              nmea_data.rmc_frame.date.month,
                              nmea_data.rmc_frame.date.day);
                onepos_printf("\t time : %d : %d : %d : %d\r\n",
                              nmea_data.rmc_frame.time.hours,
                              nmea_data.rmc_frame.time.minutes,
                              nmea_data.rmc_frame.time.seconds,
                              nmea_data.rmc_frame.time.microseconds);
                onepos_printf("\t latitude : %lld, %d; %f\r\n",
                              nmea_data.rmc_frame.latitude.value,
                              nmea_data.rmc_frame.latitude.dec_len,
                              rcvr_data_to_wgs84(nmea_data.rmc_frame.latitude));
                onepos_printf("\t longitude : %lld, %d; %f\r\n",
                              nmea_data.rmc_frame.longitude.value,
                              nmea_data.rmc_frame.longitude.dec_len,
                              rcvr_data_to_wgs84(nmea_data.rmc_frame.longitude));
                onepos_printf("\t speed : %lld, %d; %f\r\n",
                              nmea_data.rmc_frame.speed.value,
                              nmea_data.rmc_frame.speed.dec_len,
                              tras_loca_float(nmea_data.rmc_frame.speed));

                result = ONEPOS_EOK;
            }
            else
            {
                onepos_printf("get_gnss_data Error\r\n");
                result = ONEPOS_ERROR;
            }
        }
        break;
#endif

#if defined(RCVR_SUPP_RESET)
        case 1:
        {
            result = rcvr_reset(rcvr, RCVR_COLD_START);
            onepos_printf("reset rcvr result : %d\r\n", result);
        }
        break;
#endif

#if defined(RCVR_SUPP_RESET) && defined(RCVR_SUPP_AGNSS)
        case 2:
        {
            result = rcvr_reset(rcvr, RCVR_COLD_START);
            if (ONEPOS_EOK == result)
            {
                onepos_printf("reset rcvr is succ, will agnss\r\n");
                onepos_task_msleep(500);
                result = rcvr_agnss(rcvr, ONEPOS_DEFAULT_LAT, ONEPOS_DEFAULT_LON, ONEPOS_DEFAULT_ALT);
                onepos_printf("agnss rcvr result : %d\r\n", result);
            }
            else
                onepos_printf("reset rcvr is failed, result is %d\n", result);
        }
        break;
#endif

#if defined(RCVR_SUPP_RTK)
        case 3:
        {
            onepos_printf("RTK source node : %d\n", rov_sta_get_src_node(rcvr->sta));
            onepos_printf("RTK coordinate system : %d\n", rov_sta_get_coord_sys(rcvr->sta));
        }
        break;

        case 4:
        {
            onepos_printf("RTK set coordinate system result: %d\n",
                          rov_sta_set_coord_sys(rcvr->sta, ITRF2008_COORD_SYS));
        }
        break;
#endif

        default:
            onepos_printf("no support ops code");
            result = ONEPOS_EPERM;
            break;
        }
    }

    return result;
}

static int32_t onepos_gnss_rcvr_close_test(int32_t argc, int8_t *argv[])
{
    char           tmp[100] = {0};
    int            index    = 0;
    err_t          ret      = ONEPOS_EOK;
    rcvr_object_t *rcvr     = ONEPOS_NULL;

    if (2 == argc)
        rcvr = rcvr_object_get_default();
    else
        rcvr = rcvr_object_get_by_name((const char *)argv[2]);

    if (rcvr)
    {
        index = snprintf(tmp, sizeof(tmp), "Closing rcvr : %s ", rcvr->name);
        ret   = rcvr_object_deinit(rcvr);
        if (ONEPOS_EOK == ret)
        {
            rcvr_object_destroy(rcvr);
            rcvr = (rcvr_object_t *)ONEPOS_NULL;
            snprintf(tmp + index, sizeof(tmp) - index, "succ.\r\n");
        }
        else
        {
            snprintf(tmp + index, sizeof(tmp) - index, "failed.\r\n");
        }
        onepos_printf("%s", tmp);
    }

    return ONEPOS_EOK;
}
#ifdef GNSS_USING_ICOFCHINA
extern rcvr_object_t *icofchina_rcvr_creat(void);
#endif

#ifdef GNSS_USING_UNICORECOMM
extern rcvr_object_t *unicorecomm_rcvr_creat(void);
#endif

#ifdef GNSS_USING_TECHTOTOP
extern rcvr_object_t *techtotop_rcvr_creat(void);
#endif

#ifdef GNSS_USING_UBLOX
extern rcvr_object_t *ublox_rcvr_creat(void);
#endif

#ifdef GNSS_USING_UBLOX7
extern rcvr_object_t *ublox7_rcvr_creat(void);
#endif

#ifdef GNSS_USING_MX
extern rcvr_object_t *mx_rcvr_creat(void);
#endif

static int32_t onepos_gnss_rcvr_test(int32_t argc, int8_t *argv[])
{
#ifdef GNSS_USING_ICOFCHINA
    icofchina_rcvr_creat();
#endif

#ifdef GNSS_USING_UNICORECOMM
    unicorecomm_rcvr_creat();
#endif

#ifdef GNSS_USING_TECHTOTOP
    techtotop_rcvr_creat();
#endif

#ifdef GNSS_USING_UBLOX
    ublox_rcvr_creat();
#endif

#ifdef GNSS_USING_UBLOX7
    ublox7_rcvr_creat();
#endif

#ifdef GNSS_USING_MX
    mx_rcvr_creat();
#endif

    onepos_printf("Default gnss receiver : %s\t\r\n", rcvr_object_get_default()->name);

    return ONEPOS_EOK;
}
#endif

/* cmd table */
static const struct onepos_cmd_des onepos_cmd_tab[] = {
#if defined(ONEPOS_USING_NET_POS)
    {"status", onepos_status_cmd_func},
    {"interval", onepos_interval_cmd_func},
    {"sev_type", onepos_server_type_cmd_func},
    {"start", onepos_start_cmd_func},
    {"stop", onepos_stop_cmd_func},
    {"pos", onepos_get_latest_position_cmd_func},
#endif
#if defined(GNSS_USING_RCVR)
    {"gnss_test", onepos_gnss_rcvr_test},
    {"ops_test", onepos_gnss_rcvr_ops_test},
    {"gnss_close", onepos_gnss_rcvr_close_test},
#endif
    {"test", onepos_test_cmd_func}};

static int32_t onepos_help(int32_t argc, int8_t *argv[])
{
#if defined(ONEPOS_USING_NET_POS)
    onepos_printf("onepos\r\n");
    onepos_printf("onepos help\r\n");
    onepos_printf("onepos status <0/1(0:closing;1:runing)>\r\n");
    onepos_printf("onepos interval <second(>=%d)>\r\n", ONEPOS_MIN_INTERVAL);
    onepos_printf("onepos sev_type <0/1(0:circ;1:single)>\r\n");
    onepos_printf("onepos start\r\n");
    onepos_printf("onepos stop\r\n");
    onepos_printf("onepos pos\r\n");
#endif
#if defined(ONEPOS_USING_GNSS_POS)
    onepos_printf("onepos gnss_test\r\n");
    onepos_printf("onepos ops_test<0:get rcvr data;1:reset rcvr;2:agnss test><rcvr name>\r\n");
    onepos_printf("onepos gnss_close\r\n");
#endif
    onepos_printf("onepos test\r\n");
    return 0;
}

static int32_t onepos_cmd(int32_t argc, int8_t *argv[])
{
    int32_t i;
    int32_t result = 0;

    const struct onepos_cmd_des *run_cmd = ONEPOS_NULL;

    if (argc == 1)
    {
        onepos_help(argc, argv);
        return 0;
    }

    /* find fun */
    for (i = 0; i < sizeof(onepos_cmd_tab) / sizeof(onepos_cmd_tab[0]); i++)
    {
        if (strcmp((const char *)onepos_cmd_tab[i].cmd, (const char *)argv[1]) == 0)
        {
            run_cmd = &onepos_cmd_tab[i];
            break;
        }
    }

    /* not find fun, print help */
    if (run_cmd == ONEPOS_NULL)
    {
        onepos_help(argc, argv);
        return 0;
    }

    /* run fun */
    if (run_cmd->fun != ONEPOS_NULL)
    {
        result = run_cmd->fun(argc, argv);
    }

    return result;
}

SH_CMD_EXPORT(onepos, onepos_cmd, "onepos command.");
