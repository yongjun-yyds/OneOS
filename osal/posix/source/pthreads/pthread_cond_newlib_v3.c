/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        pthread_cond.c
 *
 * @brief       This file provides posix condition functions implementation.
 *
 * @revision
 * Date         Author          Notes
 * 2020-04-28   OneOS team      First Version
 ***********************************************************************************************************************
 */
#include <pthread.h>
#include "pthread_internal.h"

int pthread_condattr_destroy(pthread_condattr_t *attr)
{
    if (!attr)
    {
        return EINVAL;
    }

    attr->is_initialized = 0;

    return 0;
}

int pthread_condattr_init(pthread_condattr_t *attr)
{
    if (!attr)
    {
        return EINVAL;
    }

    attr->process_shared = PTHREAD_PROCESS_PRIVATE;
    attr->is_initialized = 1;

    return 0;
}

int pthread_condattr_getclock(const pthread_condattr_t *attr, clockid_t *clock_id)
{
    return 0;
}

int pthread_condattr_setclock(pthread_condattr_t *attr, clockid_t clock_id)
{
    return 0;
}

int pthread_condattr_getpshared(const pthread_condattr_t *attr, int *pshared)
{
    if (!attr || !pshared)
    {
        return EINVAL;
    }

    *pshared = PTHREAD_PROCESS_PRIVATE;

    return 0;
}

int pthread_condattr_setpshared(pthread_condattr_t *attr, int pshared)
{
    if ((PTHREAD_PROCESS_PRIVATE != pshared) && (PTHREAD_PROCESS_SHARED != pshared))
    {
        return EINVAL;
    }

    if (PTHREAD_PROCESS_PRIVATE != pshared)
    {
        return ENOSYS;
    }

    return 0;
}

int pthread_cond_init(pthread_cond_t *cond, const pthread_condattr_t *attr)
{
    os_err_t              result;
    char                  cond_name[OS_NAME_MAX];
    static uint16_t       cond_num = 0;
    pthread_cond_inner_t *pci      = OS_NULL;

    if (OS_NULL == cond)
    {
        return EINVAL;
    }

    pci = (pthread_cond_inner_t *)(*cond);

    if (OS_TRUE == os_semaphore_is_exist(&pci->sem))
    {
        return EBUSY;
    }

    if ((OS_NULL != attr) && (1 != attr->is_initialized))
    {
        return EINVAL;
    }

    os_snprintf(cond_name, sizeof(cond_name), "cond%02d", cond_num++);

    pci = (pthread_cond_inner_t *)os_malloc(sizeof(pthread_cond_inner_t));

    if (OS_NULL == pci)
    {
        return ENOMEM;
    }

    if (OS_NULL == attr)
    {
        pthread_condattr_init(&pci->attr);
    }
    else
    {
        pci->attr = *attr;
    }

    if (OS_NULL == os_semaphore_create(&pci->sem, cond_name, 0, 1))
    {
        os_free(pci);
        return EBUSY;
    }

    /* Detach the object from system object container. */
    /* os_object_deinit(&cond->sem.parent.parent); */
    /* cond->sem.parent.parent.type = OS_OBJECT_SEMAPHORE; */

    *cond = (pthread_cond_t)pci;

    return OS_SUCCESS;
}

int pthread_cond_destroy(pthread_cond_t *cond)
{
    pthread_cond_inner_t *pci = OS_NULL;
    if (OS_NULL == cond)
    {
        return EINVAL;
    }

    if (-1 == *cond)
    {
        // define, but not init
        return 0;
    }

    pci = (pthread_cond_inner_t *)(*cond);

    /* if (0 == pci->attr.is_initialized) */
    /* { */
    /*     return OS_SUCCESS; */
    /* } */

    if ((OS_NULL == pci) || (OS_FALSE == os_semaphore_is_exist(&pci->sem)))
    {
        return EINVAL;
    }

    (void)os_semaphore_destroy(&pci->sem);

    /* Clean condition. */
    memset(pci, 0, sizeof(pthread_cond_inner_t));
    pthread_condattr_destroy(&pci->attr);
    os_free(pci);
    *cond = 0;

    return OS_SUCCESS;
}

int pthread_cond_broadcast(pthread_cond_t *cond)
{
    os_err_t              result;
    pthread_cond_inner_t *pci = OS_NULL;

    if (OS_NULL == cond)
    {
        return EINVAL;
    }

    pci = (pthread_cond_inner_t *)(*cond);

    if (-1 == *cond)
    {
        pthread_cond_init(cond, OS_NULL);
        pci = (pthread_cond_inner_t *)(*cond);
    }

    if (OS_NULL == pci)
    {
        return EINVAL;
    }

    if (0 == pci->attr.is_initialized)
    {
        pthread_condattr_init(&pci->attr);
    }

    if (OS_FALSE == os_semaphore_is_exist(&pci->sem))
    {
        return EINVAL;
    }

    os_schedule_lock();

    while (1)
    {
        /* Try to take condition semaphore. */
        result = os_semaphore_wait(&pci->sem, OS_NO_WAIT);
        if (OS_BUSY == result)
        {
            /* It's timeout, release this semaphore. */
            os_semaphore_post(&pci->sem);
        }
        else if (OS_SUCCESS == result)
        {
            break;
        }
        else
        {
            os_schedule_unlock();

            return EINVAL;
        }
    }

    os_schedule_unlock();

    return OS_SUCCESS;
}

int pthread_cond_signal(pthread_cond_t *cond)
{
    os_err_t              result;
    pthread_cond_inner_t *pci = OS_NULL;

    if (OS_NULL == cond)
    {
        return EINVAL;
    }

    pci = (pthread_cond_inner_t *)(*cond);

    if (-1 == *cond)
    {
        pthread_cond_init(cond, OS_NULL);
        pci = (pthread_cond_inner_t *)(*cond);
    }

    if (OS_NULL == pci)
    {
        return EINVAL;
    }

    if (0 == pci->attr.is_initialized)
    {
        pthread_condattr_init(&pci->attr);
    }

    if (OS_FALSE == os_semaphore_is_exist(&pci->sem))
    {
        return EINVAL;
    }

    result = os_semaphore_post(&pci->sem);
    if (OS_SUCCESS == result)
    {
        return OS_SUCCESS;
    }

    return OS_SUCCESS;
}

static os_err_t _pthread_cond_timedwait(pthread_cond_t *cond, pthread_mutex_t *mutex, int32_t timeout)
{
    os_err_t               result;
    pthread_cond_inner_t  *pci = OS_NULL;
    pthread_mutex_inner_t *pmi = OS_NULL;
    os_task_id             tid;

    if (!cond || !mutex)
    {
        return OS_INVAL;
    }

    pci = (pthread_cond_inner_t *)(*cond);

    if (-1 == *cond)
    {
        pthread_cond_init(cond, OS_NULL);
        pci = (pthread_cond_inner_t *)(*cond);
    }

    if (OS_NULL == pci)
    {
        return EINVAL;
    }
    /* Check whether initialized */
    if (0 == pci->attr.is_initialized)
    {
        pthread_condattr_init(&pci->attr);
    }

    if (OS_FALSE == os_semaphore_is_exist(&pci->sem))
    {
        return OS_FAILURE;
    }

    pmi = (pthread_mutex_inner_t *)(*mutex);
    /* The mutex was not owned by the current thread at the time of the call. */
    os_mutex_get_owner(&pmi->lock, &tid);
    if (tid != os_get_current_task())
    {
        return OS_EPERM;
    }

    /* Unlock a mutex failed */
    if (0 != pthread_mutex_unlock(mutex))
    {
        return OS_FAILURE;
    }

    result = os_semaphore_wait(&pci->sem, timeout);

    /* Lock mutex again */
    pthread_mutex_lock(mutex);

    return result;
}

int pthread_cond_wait(pthread_cond_t *cond, pthread_mutex_t *mutex)
{
    os_err_t result;

    result = _pthread_cond_timedwait(cond, mutex, OS_WAIT_FOREVER);
    if (OS_EPERM == result)
    {
        return EPERM;
    }
    else if (OS_INVAL == result)
    {
        return EINVAL;
    }
    else if (OS_SUCCESS != result)
    {
        return EINVAL;
    }

    return 0;
}

int pthread_cond_timedwait(pthread_cond_t *cond, pthread_mutex_t *mutex, const struct timespec *abstime)
{
    int      timeout;
    os_err_t result;

    timeout = clock_time_to_tick(abstime);

    result = _pthread_cond_timedwait(cond, mutex, timeout);
    if (OS_SUCCESS == result)
    {
        return 0;
    }

    if ((OS_TIMEOUT == result) || (OS_BUSY == result))
    {
        return ETIMEDOUT;
    }
    else if (OS_EPERM == result)
    {
        return EPERM;
    }

    return EINVAL;
}
