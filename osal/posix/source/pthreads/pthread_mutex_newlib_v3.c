/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        pthread_mutex.c
 *
 * @brief       This file provides posix mutex functions implementation.
 *
 * @revision
 * Date         Author          Notes
 * 2020-04-28   OneOS team      First Version
 ***********************************************************************************************************************
 */
#include <pthread.h>

#define MUTEXATTR_SHARED_MASK 0x0010
#define MUTEXATTR_TYPE_MASK   0x000f

const pthread_mutexattr_t g_pthread_default_mutexattr = {1, PTHREAD_PROCESS_PRIVATE, 0, 0};

int pthread_mutexattr_init(pthread_mutexattr_t *attr)
{
    if (attr)
    {
        *attr = g_pthread_default_mutexattr;

        return 0;
    }

    return EINVAL;
}

int pthread_mutexattr_destroy(pthread_mutexattr_t *attr)
{
    if (attr)
    {
        attr->is_initialized = 0;

        return 0;
    }

    return EINVAL;
}

int pthread_mutexattr_gettype(const pthread_mutexattr_t *attr, int *type)
{
    if ((attr == NULL) || (type == NULL))
    {
        return EINVAL;
    }

    if ((attr->type >= PTHREAD_MUTEX_NORMAL) && (attr->type <= PTHREAD_MUTEX_DEFAULT))
    {
        *type = attr->type;
    }
    else
    {
        return EINVAL;
    }

    return 0;
}

int pthread_mutexattr_settype(pthread_mutexattr_t *attr, int type)
{
    if (attr == NULL)
    {
        return EINVAL;
    }

    if ((type >= PTHREAD_MUTEX_NORMAL) && (type <= PTHREAD_MUTEX_DEFAULT))
    {
        attr->type = type;
    }
    else
    {
        return EINVAL;
    }

    return 0;
}

int pthread_mutexattr_setpshared(pthread_mutexattr_t *attr, int pshared)
{
    if (attr == NULL)
    {
        return EINVAL;
    }

    attr->process_shared = pshared;

    return 0;
}

int pthread_mutexattr_getpshared(pthread_mutexattr_t *attr, int *pshared)
{
    if ((attr == NULL) || (pshared == NULL))
    {
        return EINVAL;
    }

    *pshared = attr->process_shared;

    return 0;
}

int pthread_mutex_init(pthread_mutex_t *mutex, const pthread_mutexattr_t *attr)
{
    char                   name[OS_NAME_MAX];
    os_bool_t              recursive;
    static uint16_t        pthread_mutex_number = 0;
    pthread_mutex_inner_t *pmi                  = OS_NULL;

    if (!mutex)
    {
        return EINVAL;
    }

    pmi = (pthread_mutex_inner_t *)(*mutex);

    if (OS_TRUE == os_mutex_check_exist(&pmi->lock))
    {
        return EBUSY;
    }

    if ((OS_NULL != attr) && (1 != attr->is_initialized))
    {
        return EINVAL;
    }

    os_snprintf(name, sizeof(name), "pmtx%02d", pthread_mutex_number++);

    pmi = (pthread_mutex_inner_t *)os_malloc(sizeof(pthread_mutex_inner_t));

    if (OS_NULL == pmi)
    {
        return ENOMEM;
    }

    if (OS_NULL == attr)
    {
        pmi->attr = g_pthread_default_mutexattr;
    }
    else
    {
        pmi->attr = *attr;
    }

    if (pmi->attr.type == PTHREAD_MUTEX_RECURSIVE)
    {
        recursive = OS_TRUE;
    }
    else
    {
        recursive = OS_FALSE;
    }

    if (OS_NULL == os_mutex_create(&pmi->lock, name, recursive))
    {
        os_free(pmi);
        return EBUSY;
    }

    /* Detach the object from system object container. */
    /* os_object_deinit(&mutex->lock.parent.parent); */
    /* mutex->lock.parent.parent.type = OS_OBJECT_MUTEX; */

    *mutex = (pthread_mutex_t)pmi;

    return 0;
}

int pthread_mutex_destroy(pthread_mutex_t *mutex)
{
    pthread_mutex_inner_t *pmi = OS_NULL;
    os_task_id             tid;

    if (!mutex)
    {
        return EINVAL;
    }

    if (-1 == *mutex)
    {
        return 0;
    }

    pmi = (pthread_mutex_inner_t *)(*mutex);

    if ((OS_NULL == pmi) || (OS_FALSE == os_mutex_check_exist(&pmi->lock)))
    {
        return EINVAL;
    }

    /* It's busy */
    os_mutex_get_owner(&pmi->lock, &tid);
    if (OS_NULL != tid)
    {
        return EBUSY;
    }

    os_mutex_destroy(&pmi->lock);
    pthread_mutexattr_destroy(&pmi->attr);
    os_free(pmi);
    *mutex = 0;

    return 0;
}

int pthread_mutex_lock(pthread_mutex_t *mutex)
{
    int                    mtype;
    os_err_t               result;
    pthread_mutex_inner_t *pmi = OS_NULL;
    os_task_id             tid;

    if (!mutex)
    {
        return EINVAL;
    }

    if (-1 == *mutex)
    {
        pthread_mutex_init(mutex, OS_NULL);
        pmi = (pthread_mutex_inner_t *)(*mutex);
    }

    pmi = (pthread_mutex_inner_t *)(*mutex);

    if (OS_NULL == pmi)
    {
        return EINVAL;
    }

    if (0 == pmi->attr.is_initialized)
    {
        pthread_mutexattr_init(&pmi->attr);
    }

    if (OS_FALSE == os_mutex_check_exist(&pmi->lock))
    {
        return EINVAL;
    }

    mtype = pmi->attr.type;

    os_schedule_lock();

    os_mutex_get_owner(&pmi->lock, &tid);
    if ((tid == os_get_current_task()) && (PTHREAD_MUTEX_RECURSIVE != mtype))
    {
        os_schedule_unlock();

        return EDEADLK;
    }

    os_schedule_unlock();

    if (PTHREAD_MUTEX_RECURSIVE != mtype)
    {
        result = os_mutex_lock(&pmi->lock, OS_WAIT_FOREVER);
    }
    else
    {
        result = os_mutex_recursive_lock(&pmi->lock, OS_WAIT_FOREVER);
    }

    if (OS_SUCCESS == result)
    {
        return 0;
    }

    return EINVAL;
}

int pthread_mutex_unlock(pthread_mutex_t *mutex)
{
    os_err_t               result;
    int                    mtype;
    pthread_mutex_inner_t *pmi = OS_NULL;
    os_task_id             tid;

    if (!mutex)
    {
        return EINVAL;
    }

    if (-1 == *mutex)
    {
        pthread_mutex_init(mutex, OS_NULL);
        pmi = (pthread_mutex_inner_t *)(*mutex);
    }

    pmi = (pthread_mutex_inner_t *)(*mutex);

    if (OS_NULL == pmi)
    {
        return EINVAL;
    }

    if (0 == pmi->attr.is_initialized)
    {
        pthread_mutexattr_init(&pmi->attr);
    }

    if (OS_FALSE == os_mutex_check_exist(&pmi->lock))
    {
        return EINVAL;
    }

    mtype = pmi->attr.type;

    os_mutex_get_owner(&pmi->lock, &tid);
    if (tid != os_get_current_task())
    {
        if ((PTHREAD_MUTEX_ERRORCHECK == mtype) || (PTHREAD_MUTEX_RECURSIVE == mtype))
        {
            return EPERM;
        }

        /* No thread waiting on this mutex. */
        if (OS_NULL == tid)
        {
            return 0;
        }
    }

    if (PTHREAD_MUTEX_RECURSIVE != mtype)
    {
        result = os_mutex_unlock(&pmi->lock);
    }
    else
    {
        result = os_mutex_recursive_unlock(&pmi->lock);
    }

    if (OS_SUCCESS == result)
    {
        return 0;
    }

    return EINVAL;
}

int pthread_mutex_trylock(pthread_mutex_t *mutex)
{
    os_err_t               result;
    int                    mtype;
    pthread_mutex_inner_t *pmi = OS_NULL;
    os_task_id             tid;

    if (!mutex)
    {
        return EINVAL;
    }

    if (-1 == *mutex)
    {
        pthread_mutex_init(mutex, OS_NULL);
        pmi = (pthread_mutex_inner_t *)(*mutex);
    }

    pmi = (pthread_mutex_inner_t *)(*mutex);

    if (OS_NULL == pmi)
    {
        return EINVAL;
    }

    if (0 == pmi->attr.is_initialized)
    {
        pthread_mutexattr_init(&pmi->attr);
    }

    if (OS_FALSE == os_mutex_check_exist(&pmi->lock))
    {
        return EINVAL;
    }

    mtype = pmi->attr.type;

    os_schedule_lock();

    os_mutex_get_owner(&pmi->lock, &tid);
    if ((tid == os_get_current_task()) && (PTHREAD_MUTEX_RECURSIVE != mtype))
    {
        os_schedule_unlock();

        return EDEADLK;
    }

    os_schedule_unlock();

    if (PTHREAD_MUTEX_RECURSIVE != mtype)
    {
        result = os_mutex_lock(&pmi->lock, OS_NO_WAIT);
    }
    else
    {
        result = os_mutex_recursive_lock(&pmi->lock, OS_NO_WAIT);
    }

    if (OS_SUCCESS == result)
    {
        return 0;
    }

    return EBUSY;
}
