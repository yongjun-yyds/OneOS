from oneoslib.build_tools import *
from oneoslib.tools import *


def is_newlib():
    libc_version = IsDefined('LIBC_VERSION')
    return type('str') == type(libc_version) and libc_version.rfind("newlib", 0, len(libc_version)) != -1


def is_newlib_major_greater_than_or_equal_to_3():
    libc_version = IsDefined('LIBC_VERSION')
    if is_newlib():
        newlib_version = libc_version[len("\"newlib "):-1].split('.')
        newlib_major = int(newlib_version[0])

        return newlib_major >= 3
    else:
        return False

def check_libc_version():
    if is_compiler("gcc"):
        return is_newlib_major_greater_than_or_equal_to_3()
    elif is_compiler("armcc"):
        return False
    else:
        return False
