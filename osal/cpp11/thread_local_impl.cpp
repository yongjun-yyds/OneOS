/**
 ***********************************************************************************************************************
 * Copyright (c) 2021, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        thread_local_impl.cpp
 *
 * @brief
 *
 * @revision
 * Date         Author          Notes
 * 2021-11-18   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <pthread.h>
#include <cstdlib>

typedef void (*destructor)(void *);

extern "C" int __cxa_thread_atexit_impl(destructor dtor, void *obj, void *dso_symbol)
{
    pthread_key_t key_tmp;
    if (pthread_key_create(&key_tmp, dtor) != 0)
        abort();
    pthread_setspecific(key_tmp, obj);
    return 0;
}

#if defined(__GNUC__) && !defined(__ARMCC_VERSION) /*GCC*/
#include <cxxabi.h>

extern "C" int __cxxabiv1::__cxa_thread_atexit(destructor dtor, void *obj, void *dso_handle)
{
    return __cxa_thread_atexit_impl(dtor, obj, dso_handle);
}
#endif
