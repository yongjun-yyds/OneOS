/**
 ***********************************************************************************************************************
 * Copyright (c) 2021, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with 
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on 
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the 
 * specific language governing permissions and limitations under the License.
 *
 * @file        atomic_8.c
 *
 * @brief
 *
 * @revision
 * Date         Author          Notes
 * 2021-11-18   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <os_stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <os_spinlock.h>

OS_DEFINE_SPINLOCK(atomic_lk);
/*
* override gcc builtin atomic function for std::atomic<int64_t>, std::atomic<uint64_t>
* @see https://gcc.gnu.org/onlinedocs/gcc/_005f_005fatomic-Builtins.html
*/
uint64_t __atomic_load_8(volatile void *ptr, int memorder)
{
    volatile uint64_t *val_ptr = (volatile uint64_t *)ptr;
    uint64_t tmp;
    os_ubase_t irqsv;
    os_spin_lock_irqsave(&atomic_lk, &irqsv);
    tmp = *val_ptr;
    os_spin_unlock_irqrestore(&atomic_lk, irqsv);
    return tmp;
}

void __atomic_store_8(volatile void *ptr, uint64_t val, int memorder)
{
    volatile uint64_t *val_ptr = (volatile uint64_t *)ptr;
    os_ubase_t irqsv;
    os_spin_lock_irqsave(&atomic_lk, &irqsv);
    *val_ptr = val;
    os_spin_unlock_irqrestore(&atomic_lk, irqsv);
}

uint64_t __atomic_exchange_8(volatile void *ptr, uint64_t val, int memorder)
{
    volatile uint64_t *val_ptr = (volatile uint64_t *)ptr;
    uint64_t tmp;
    os_ubase_t irqsv;
    os_spin_lock_irqsave(&atomic_lk, &irqsv);
    tmp = *val_ptr;
    *val_ptr = val;
    os_spin_unlock_irqrestore(&atomic_lk, irqsv);
    return tmp;
}

bool __atomic_compare_exchange_8(volatile void *ptr, volatile void *expected, uint64_t desired, bool weak, int success_memorder, int failure_memorder)
{
    volatile uint64_t *val_ptr = (volatile uint64_t *)ptr;
    volatile uint64_t *expected_ptr = (volatile uint64_t *)expected;
    bool exchanged;
    os_ubase_t irqsv;
    os_spin_lock_irqsave(&atomic_lk, &irqsv);
    if (*val_ptr == *expected_ptr)
    {
        *val_ptr = desired;
        exchanged = true;
    }
    else
    {
        *expected_ptr = *val_ptr;
        exchanged = false;
    }
    os_spin_unlock_irqrestore(&atomic_lk, irqsv);
    return exchanged;
}

#define __atomic_fetch_op_8(OPNAME, OP) \
uint64_t __atomic_fetch_##OPNAME##_8(volatile void *ptr, uint64_t val, int memorder) {\
    volatile uint64_t* val_ptr = (volatile uint64_t*)ptr;\
    uint64_t tmp;\
    os_ubase_t irqsv;\
    os_spin_lock_irqsave(&atomic_lk, &irqsv);\
    tmp = *val_ptr;\
    *val_ptr OP##= val;\
    os_spin_unlock_irqrestore(&atomic_lk, irqsv);\
    return tmp;\
}

__atomic_fetch_op_8(add, +)
__atomic_fetch_op_8(sub, -)
__atomic_fetch_op_8( and, &)
__atomic_fetch_op_8( or, |)
__atomic_fetch_op_8(xor, ^)
