/**
 ***********************************************************************************************************************
 * Copyright (c) 2021, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        condition_variable.cpp
 *
 * @brief
 *
 * @revision
 * Date         Author          Notes
 * 2021-11-18   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include "condition_variable"

namespace std
{
void condition_variable::wait(unique_lock<mutex> &lock)
{
    int err = pthread_cond_wait(&_m_cond, lock.mutex()->native_handle());

    if (err)
    {
        throw_system_error(err, "condition_variable::wait: failed to wait on a condition");
    }
}

void notify_all_at_thread_exit(condition_variable &cond, unique_lock<mutex> lk)
{
    // TLS currently not available
    mutex *mut = lk.release();
    mut->unlock();
    cond.notify_all();
}

}    // namespace std
