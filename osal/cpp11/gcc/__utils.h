/**
 ***********************************************************************************************************************
 * Copyright (c) 2021, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        __utils.h
 *
 * @brief
 *
 * @revision
 * Date         Author          Notes
 * 2021-11-18   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#pragma once

#include <cstdlib>
#include <system_error>
#include <chrono>
#include <ratio>

#include <os_task.h>

#define OS_USING_CPP_EXCEPTION

inline void throw_system_error(int err, const char *what_msg)
{
#ifdef OS_USING_CPP_EXCEPTION
    throw std::system_error(std::error_code(err, std::system_category()), what_msg);
#else
    (void)err;
    (void)what_msg;
    ::abort();
#endif
}

class tick_clock
{
  public:
    typedef clock_t                           rep;
    typedef std::ratio<1, OS_TICK_PER_SECOND> period;

    typedef std::chrono::duration<tick_clock::rep, tick_clock::period> duration;
    typedef std::chrono::time_point<tick_clock>                        time_point;

    constexpr static bool is_ready = true;

    static time_point now();
};

class real_time_clock
{
  public:
    typedef std::chrono::nanoseconds                           duration;
    typedef duration::rep                                      rep;
    typedef duration::period                                   period;
    typedef std::chrono::time_point<real_time_clock, duration> time_point;

    static constexpr bool is_steady = true;

    static time_point now() noexcept;
};
