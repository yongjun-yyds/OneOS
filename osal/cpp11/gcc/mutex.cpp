/**
 ***********************************************************************************************************************
 * Copyright (c) 2021, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        mutex.cpp
 *
 * @brief
 *
 * @revision
 * Date         Author          Notes
 * 2021-11-18   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include "mutex"

namespace std
{
// use a set of global and static objects
// a proxy function to pthread_once

function<void()> once_functor;

mutex &get_once_mutex()
{
    static mutex once_mutex;
    return once_mutex;
}

inline unique_lock<mutex> *&get_once_functor_lock_ptr()
{
    static unique_lock<mutex> *once_functor_mutex_ptr = nullptr;
    return once_functor_mutex_ptr;
}

void set_once_functor_lock_ptr(unique_lock<mutex> *m_ptr)
{
    get_once_functor_lock_ptr() = m_ptr;
}

extern "C" {
void once_proxy()
{
    // need to first transfer the functor's ownership so as to call it
    function<void()> once_call = std::move(once_functor);

    // no need to hold the lock anymore
    unique_lock<mutex> *lock_ptr = get_once_functor_lock_ptr();
    get_once_functor_lock_ptr()  = nullptr;
    lock_ptr->unlock();

    once_call();
}
}
}    // namespace std
