/**
 ***********************************************************************************************************************
 * Copyright (c) 2021, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        thread.cpp
 *
 * @brief
 *
 * @revision
 * Date         Author          Notes
 * 2021-11-18   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include "thread"
#include "__utils.h"

#define _OS_NPROCS 0

namespace std
{

extern "C" {
static void *execute_native_thread_routine(void *p)
{
    thread::invoker_base    *t = static_cast<thread::invoker_base *>(p);
    thread::invoker_base_ptr local;
    local.swap(t->this_ptr);    // tranfer the ownership of the invoker into the thread entry

    local->invoke();

    return NULL;
}
}

void thread::start_thread(invoker_base_ptr b)
{
    auto raw_ptr = b.get();
    // transfer the ownership of the invoker to the new thread
    raw_ptr->this_ptr = std::move(b);
    int err           = pthread_create(&_m_thr.__cpp_thread_t, NULL, &execute_native_thread_routine, raw_ptr);

    if (err)
    {
        raw_ptr->this_ptr.reset();
        throw_system_error(err, "Failed to create a thread");
    }
}

thread::~thread()
{
    if (joinable())    // when either not joined or not detached
        terminate();
}

void thread::join()
{
    int err = EINVAL;

    if (joinable())
        err = pthread_join(native_handle(), NULL);

    if (err)
    {
        throw_system_error(err, "thread::join failed");
    }

    _m_thr = id();
}

void thread::detach()
{
    int err = EINVAL;

    if (joinable())
        err = pthread_detach(native_handle());
    if (err)
    {
        throw_system_error(err, "thread::detach failed");
    }

    _m_thr = id();
}

// TODO: not yet actually implemented.
// The standard states that the returned value should only be considered a hint.
unsigned thread::hardware_concurrency() noexcept
{
    int __n = _OS_NPROCS;
    if (__n < 0)
        __n = 0;
    return __n;
}
}    // namespace std
