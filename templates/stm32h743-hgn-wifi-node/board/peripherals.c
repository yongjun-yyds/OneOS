extern ADC_HandleTypeDef hadc1;
OS_HAL_DEVICE_DEFINE("ADC_HandleTypeDef", "adc1", hadc1);

extern ADC_HandleTypeDef hadc2;
OS_HAL_DEVICE_DEFINE("ADC_HandleTypeDef", "adc2", hadc2);

extern DAC_HandleTypeDef hdac1;
OS_HAL_DEVICE_DEFINE("DAC_HandleTypeDef", "dac1", hdac1);

extern DMA2D_HandleTypeDef hdma2d;
OS_HAL_DEVICE_DEFINE("DMA2D_HandleTypeDef", "dma2d", hdma2d);

extern ETH_HandleTypeDef heth;
OS_HAL_DEVICE_DEFINE("ETH_HandleTypeDef", "eth", heth);

extern HRTIM_HandleTypeDef hhrtim;
OS_HAL_DEVICE_DEFINE("HRTIM_HandleTypeDef", "hrtim", hhrtim);

extern IWDG_HandleTypeDef hiwdg1;
OS_HAL_DEVICE_DEFINE("IWDG_HandleTypeDef", "iwdg1", hiwdg1);

extern LTDC_HandleTypeDef hltdc;
OS_HAL_DEVICE_DEFINE("LTDC_HandleTypeDef", "ltdc", hltdc);

extern QSPI_HandleTypeDef hqspi;
OS_HAL_DEVICE_DEFINE("QSPI_HandleTypeDef", "qspi", hqspi);

extern RTC_HandleTypeDef hrtc;
OS_HAL_DEVICE_DEFINE("RTC_HandleTypeDef", "rtc", hrtc);

extern SAI_HandleTypeDef hsai_BlockA1;
OS_HAL_DEVICE_DEFINE("SAI_HandleTypeDef", "sai_BlockA1", hsai_BlockA1);

extern SAI_HandleTypeDef hsai_BlockB1;
OS_HAL_DEVICE_DEFINE("SAI_HandleTypeDef", "sai_BlockB1", hsai_BlockB1);

extern DMA_HandleTypeDef hdma_sai1_a;
OS_HAL_DEVICE_DEFINE("DMA_HandleTypeDef", "dma_sai1_a", hdma_sai1_a);

extern DMA_HandleTypeDef hdma_sai1_b;
OS_HAL_DEVICE_DEFINE("DMA_HandleTypeDef", "dma_sai1_b", hdma_sai1_b);

extern SD_HandleTypeDef hsd1;
OS_HAL_DEVICE_DEFINE("SD_HandleTypeDef", "sd1", hsd1);

extern TIM_HandleTypeDef htim1;
OS_HAL_DEVICE_DEFINE("TIM_HandleTypeDef", "tim1", htim1);

extern TIM_HandleTypeDef htim14;
OS_HAL_DEVICE_DEFINE("TIM_HandleTypeDef", "tim14", htim14);

extern UART_HandleTypeDef huart1;
OS_HAL_DEVICE_DEFINE("UART_HandleTypeDef", "uart1", huart1);

extern DMA_HandleTypeDef hdma_usart1_rx;
OS_HAL_DEVICE_DEFINE("DMA_HandleTypeDef", "dma_usart1_rx", hdma_usart1_rx);

extern HCD_HandleTypeDef hhcd_USB_OTG_FS;
struct stm32_hcd_info hcd_USB_OTG_FS_info = {.instance = &hhcd_USB_OTG_FS, .interface_name = "HCD_USB_OTG_FS"};
OS_HAL_DEVICE_DEFINE("HCD_HandleTypeDef", "hard_hcd_USB_OTG_FS", hcd_USB_OTG_FS_info);

extern SDRAM_HandleTypeDef hsdram1;
OS_HAL_DEVICE_DEFINE("SDRAM_HandleTypeDef", "sdram1", hsdram1);

