static const struct nxp_adc_info adc2_info = {ADC2_PERIPHERAL, &ADC2_config};
OS_HAL_DEVICE_DEFINE("ADC_Type", "adc2", adc2_info);

static const struct nxp_gpt_info gpt1_info = {GPT1_PERIPHERAL, &GPT1_config};
OS_HAL_DEVICE_DEFINE("GPT_Type", "gpt1", gpt1_info);

static const struct nxp_gpt_info gpt2_info = {GPT2_PERIPHERAL, &GPT2_config};
OS_HAL_DEVICE_DEFINE("GPT_Type", "gpt2", gpt2_info);

static const struct nxp_lpuart_info lpuart1_info = {LPUART1_PERIPHERAL, &LPUART1_config};
OS_HAL_DEVICE_DEFINE("LPUART_Type", "lpuart1", lpuart1_info);

static const struct nxp_lpuart_info lpuart2_info = {LPUART2_PERIPHERAL, &LPUART2_config};
OS_HAL_DEVICE_DEFINE("LPUART_Type", "lpuart2", lpuart2_info);

static const struct nxp_can_info can2_info = {CAN2_PERIPHERAL, &CAN2_config};
OS_HAL_DEVICE_DEFINE("CAN_Type", "can2", can2_info);

static const struct nxp_lpuart_info lpuart3_info = {LPUART3_PERIPHERAL, &LPUART3_config};
OS_HAL_DEVICE_DEFINE("LPUART_Type", "lpuart3", lpuart3_info);

static const struct nxp_lpuart_info lpuart4_info = {LPUART4_PERIPHERAL, &LPUART4_config};
OS_HAL_DEVICE_DEFINE("LPUART_Type", "lpuart4", lpuart4_info);

static const struct nxp_lpuart_info lpuart6_info = {LPUART6_PERIPHERAL, &LPUART6_config};
OS_HAL_DEVICE_DEFINE("LPUART_Type", "lpuart6", lpuart6_info);

static const struct nxp_can_info can1_info = {CAN1_PERIPHERAL, &CAN1_config};
OS_HAL_DEVICE_DEFINE("CAN_Type", "can1", can1_info);

static const struct nxp_tempmon_info tempmon_info = {TEMPMON_PERIPHERAL, &TEMPMON_config};
OS_HAL_DEVICE_DEFINE("TEMPMON_Type", "tempmon", tempmon_info);
