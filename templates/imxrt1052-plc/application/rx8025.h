#ifndef RX8025_H
#define RX8025_H

#ifdef __cplusplus
extern "C" {
#endif

#include <os_task.h>
#include <device.h>

struct os_rx8025_rtc
{
    struct os_device     dev;
    struct os_i2c_client i2c;
};

typedef struct _rx8025_datetime
{
    uint8_t seconds;  /*!< Range from 0 to 59.*/
    uint8_t minutes;  /*!< Range from 0 to 59.*/
    uint8_t hours;    /*!< Range from 0 to 23.*/
    uint8_t weekdays; /*!< Range from 0 to 6.*/
    uint8_t days;     /*!< Range from 1 to 31 (depending on month).*/
    uint8_t months;   /*!< Range from 1 to 12.*/
    uint8_t years;    /*!< Range from 0 to 99.*/
} rx8025_datetime_t;

extern uint8_t  cal_weekdays(rx8025_datetime_t *pRtcDate);
extern void     rx8025_get_date_time(struct os_i2c_client *dev, rx8025_datetime_t *datetime);
extern os_err_t rx8025_set_date_time(struct os_i2c_client *dev, const rx8025_datetime_t *datetime);

#ifdef __cplusplus
}
#endif

#endif
