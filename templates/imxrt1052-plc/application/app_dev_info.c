#include <string.h>
#include <rtc/rtc.h>
#include "shell.h"
#include "app_dev.h"

const GPIO_Type *array_do_gpio[] = {
    BOARD_DO_1_GPIO,
    BOARD_DO_2_GPIO,
    BOARD_DO_3_GPIO,
    BOARD_DO_4_GPIO,
    BOARD_DO_5_GPIO,
    BOARD_DO_6_GPIO,
    BOARD_DO_7_GPIO,
    BOARD_DO_8_GPIO,
};

const uint32_t array_do_pin[] = {
    BOARD_DO_1_GPIO_PIN,
    BOARD_DO_2_GPIO_PIN,
    BOARD_DO_3_GPIO_PIN,
    BOARD_DO_4_GPIO_PIN,
    BOARD_DO_5_GPIO_PIN,
    BOARD_DO_6_GPIO_PIN,
    BOARD_DO_7_GPIO_PIN,
    BOARD_DO_8_GPIO_PIN,
};

const GPIO_Type *array_di_gpio[] = {
    BOARD_DI_1_GPIO,
    BOARD_DI_2_GPIO,
    BOARD_DI_3_GPIO,
    BOARD_DI_4_GPIO,
    BOARD_DI_5_GPIO,
    BOARD_DI_6_GPIO,
    BOARD_DI_7_GPIO,
    BOARD_DI_8_GPIO,
    BOARD_DI_9_GPIO,
    BOARD_DI_10_GPIO,
    BOARD_DI_11_GPIO,
    BOARD_DI_12_GPIO,
    BOARD_DI_13_GPIO,
    BOARD_DI_14_GPIO,
    BOARD_DI_15_GPIO,
    BOARD_DI_16_GPIO,
};

const uint32_t array_di_pin[] = {
    BOARD_DI_1_GPIO_PIN,
    BOARD_DI_2_GPIO_PIN,
    BOARD_DI_3_GPIO_PIN,
    BOARD_DI_4_GPIO_PIN,
    BOARD_DI_5_GPIO_PIN,
    BOARD_DI_6_GPIO_PIN,
    BOARD_DI_7_GPIO_PIN,
    BOARD_DI_8_GPIO_PIN,
    BOARD_DI_9_GPIO_PIN,
    BOARD_DI_10_GPIO_PIN,
    BOARD_DI_11_GPIO_PIN,
    BOARD_DI_12_GPIO_PIN,
    BOARD_DI_13_GPIO_PIN,
    BOARD_DI_14_GPIO_PIN,
    BOARD_DI_15_GPIO_PIN,
    BOARD_DI_16_GPIO_PIN,
};

const GPIO_Type *array_led_gpio[] = {
    BOARD_LED1_GPIO,
    BOARD_LED2_GPIO,
    BOARD_LED3_GPIO,
    BOARD_LED_NET_GPIO,
};

const uint32_t array_led_pin[] = {
    BOARD_LED1_GPIO_PIN,
    BOARD_LED2_GPIO_PIN,
    BOARD_LED3_GPIO_PIN,
    BOARD_LED_NET_GPIO_PIN,
};

void show_clock(void)
{
    /*
    os_kprintf("OSC clock : %d\r\n",                  CLOCK_GetFreq(kCLOCK_OscClk));
    os_kprintf("RTC clock : %d\r\n",                  CLOCK_GetFreq(kCLOCK_RtcClk));
    */
    os_kprintf("CPU clock: %d(MHz)\r\n", CLOCK_GetFreq(kCLOCK_CpuClk) / 1000000);
    /*
    os_kprintf("AHB clock : %d\r\n",                  CLOCK_GetFreq(kCLOCK_AhbClk));
    os_kprintf("SEMC clock : %d\r\n",                 CLOCK_GetFreq(kCLOCK_SemcClk));
    os_kprintf("IPG clock : %d\r\n",                  CLOCK_GetFreq(kCLOCK_IpgClk));
    os_kprintf("ARMPLLCLK(PLL1) : %d\r\n",            CLOCK_GetFreq(kCLOCK_ArmPllClk));
    os_kprintf("SYSPLLCLK(PLL2/528_PLL) : %d\r\n",    CLOCK_GetFreq(kCLOCK_SysPllClk));
    os_kprintf("SYSPLLPDF0CLK : %d\r\n",              CLOCK_GetFreq(kCLOCK_SysPllPfd0Clk));
    os_kprintf("SYSPLLPFD1CLK : %d\r\n",              CLOCK_GetFreq(kCLOCK_SysPllPfd1Clk));
    os_kprintf("SYSPLLPFD2CLK : %d\r\n",              CLOCK_GetFreq(kCLOCK_SysPllPfd2Clk));
    os_kprintf("SYSPLLPFD3CLK : %d\r\n",              CLOCK_GetFreq(kCLOCK_SysPllPfd3Clk));
    os_kprintf("USB1PLLCLK(PLL3) : %d\r\n",           CLOCK_GetFreq(kCLOCK_Usb1PllClk));
    os_kprintf("USB1PLLPDF0CLK : %d\r\n",             CLOCK_GetFreq(kCLOCK_Usb1PllPfd0Clk));
    os_kprintf("USB1PLLPFD1CLK : %d\r\n",             CLOCK_GetFreq(kCLOCK_Usb1PllPfd1Clk));
    os_kprintf("USB1PLLPFD2CLK : %d\r\n",             CLOCK_GetFreq(kCLOCK_Usb1PllPfd2Clk));
    os_kprintf("USB1PLLPFD3CLK : %d\r\n",             CLOCK_GetFreq(kCLOCK_Usb1PllPfd3Clk));
    os_kprintf("Audio PLLCLK(PLL4) : %d\r\n",         CLOCK_GetFreq(kCLOCK_AudioPllClk));
    os_kprintf("Video PLLCLK(PLL5) : %d\r\n",         CLOCK_GetFreq(kCLOCK_VideoPllClk));
    os_kprintf("Enet PLLCLK ref_enetpll0 : %d\r\n",   CLOCK_GetFreq(kCLOCK_EnetPll0Clk));
    os_kprintf("Enet PLLCLK ref_enetpll1 : %d\r\n",   CLOCK_GetFreq(kCLOCK_EnetPll1Clk));
    os_kprintf("USB2PLLCLK(PLL7) : %d\r\n",           CLOCK_GetFreq(kCLOCK_Usb2PllClk));
    */
}

os_device_t *adc_dev  = OS_NULL;
os_device_t *temp_dev = OS_NULL;

os_err_t open_dev_adc(void)
{
    os_err_t ret = OS_SUCCESS;

    adc_dev = os_device_find("adc2");
    if (adc_dev == OS_NULL)
    {
        os_kprintf("adc device not find! \r\n");
        return OS_FAILURE;
    }

    /* open adc */
    os_device_open(adc_dev);
    ret = os_device_control(adc_dev, OS_ADC_CMD_ENABLE, OS_NULL);
    if (ret != OS_SUCCESS)
    {
        os_kprintf("adc device cannot enable! \r\n");
        os_device_close(adc_dev);
        return OS_FAILURE;
    }

    return OS_SUCCESS;
}

os_err_t get_temp_value(int *p_out_temp)
{
    os_err_t ret = OS_SUCCESS;
    float    fTemp;

    os_device_t *dev = os_device_open_s("tempmon");

    if (dev == OS_NULL)
    {
        os_kprintf("Temperature monitor device not find! \r\n");
        return OS_FAILURE;
    }

    os_device_read_block(dev, 0, &fTemp, 4);

    *p_out_temp = (int)fTemp;

    return OS_SUCCESS;
}

int32_t get_bat_vol(uint8_t loops)
{
    uint8_t     i;
    uint32_t adc_channel = 1;
    int32_t  adc_databuf;
    int32_t  value = 0;

    if (adc_dev == OS_NULL)
    {
        open_dev_adc();
    }

    for (i = 0; i < loops; i++)
    {
        os_device_read_nonblock(adc_dev, adc_channel, &adc_databuf, sizeof(adc_databuf));
        value += adc_databuf;
    }

    return value * 1.1 / loops;
}

int32_t get_pwr_vol(uint8_t loops)
{
    uint8_t     i;
    uint32_t adc_channel = 2;
    int32_t  adc_databuf;
    int32_t  value = 0;

    if (adc_dev == OS_NULL)
    {
        open_dev_adc();
    }

    for (i = 0; i < loops; i++)
    {
        os_device_read_nonblock(adc_dev, adc_channel, &adc_databuf, sizeof(adc_databuf));
        value += adc_databuf;
    }

    return value * 11 / loops;
}

void show_dev_info(int argc, char *argv[])
{
    int    i;
    time_t now;
    int    temp = 0;

    show_clock();

    os_kprintf("PWR CMP: %s\r\n", (GET_PWR_CMP() == 1) ? "high level -> above 13.9V" : "low level -> below 13.9V");

    os_kprintf("BAT Vol: %d(mV)\r\n", get_bat_vol(8));

    os_kprintf("PWR Vol: %d(mV)\r\n", get_pwr_vol(8));

    for (i = 0; i < 8; i++)
    {
        os_kprintf("DO%-2d:%s  ",
                   i + 1,
                   (GPIO_PinRead((GPIO_Type *)array_do_gpio[i], array_do_pin[i]) == 1) ? "H" : "L");
    }
    os_kprintf("\r\n");

    for (i = 0; i < 16; i++)
    {
        os_kprintf("DI%-2d:%s  ",
                   i + 1,
                   (GPIO_PinRead((GPIO_Type *)array_di_gpio[i], array_di_pin[i]) == 1) ? "H" : "L");
        if ((i + 1) % 8 == 0)
        {
            os_kprintf("\r\n");
        }
    }
    get_temp_value(&temp);
    os_kprintf("MCU Temperature: %d C\r\n", temp);

    now = rtc_get();
    os_kprintf("TIME: %s\r\n", ctime(&now));
}

SH_CMD_EXPORT(show_dev_info, show_dev_info, "show_dev_info");

void set_dev_do(int argc, char *argv[])
{
    os_err_t ret;

    uint8_t port;
    uint8_t value;
    int        i;

    if (argc != 3)
    {
        os_kprintf("usage: set_dev_do port value \r\n");
        return;
    }

    port  = strtol(argv[1], OS_NULL, 0);
    value = strtol(argv[2], OS_NULL, 0);

    for (i = 0; i < 8; i++)
    {
        if ((port & (0x01 << i)) == (0x01 << i))
        {
            if (value == 1)
            {
                GPIO_PinWrite((GPIO_Type *)array_do_gpio[i], array_do_pin[i], 1);
            }
            else
            {
                GPIO_PinWrite((GPIO_Type *)array_do_gpio[i], array_do_pin[i], 0);
            }
        }
    }
}
SH_CMD_EXPORT(set_dev_do, set_dev_do, "set_dev_do");

void i2c_test(int argc, char *argv[])
{
    os_err_t   ret;
    uint8_t data[8] = {0, 1, 2, 3, 4, 5, 6, 7};    // page size = 8
    uint8_t buf[8];
    char      *name;

    uint8_t addr;

    struct os_i2c_client client;

    if (argc != 3)
    {
        os_kprintf("usage: i2c_test <i2cx> <addr>\r\n");
        return;
    }

    name = argv[1];

    addr = strtol(argv[2], OS_NULL, 0);

    client.bus = os_i2c_bus_device_find(name);
    if (client.bus == OS_NULL)
    {
        os_kprintf("bus not find\n");
    }

    client.client_addr = (addr >> 1);    //  因为oneos设计原因，地址需要往右移动1位

    memset(buf, 0, 8);
    ret = os_i2c_client_write(&client, 0, 1, buf, 8);
    if (ret != OS_SUCCESS)
    {
        os_kprintf("i2c write error\r\n");
        return;
    }
    os_task_msleep(50);

    memset(buf, 0, 8);
    ret = os_i2c_client_read(&client, 0, 1, buf, 8);
    if (ret != OS_SUCCESS)
    {
        os_kprintf("i2c read error\r\n");
        return;
    }
    else
    {
        os_kprintf("read: %02x %02x %02x %02x %02x %02x %02x %02x\r\n",
                   buf[0],
                   buf[1],
                   buf[2],
                   buf[3],
                   buf[4],
                   buf[5],
                   buf[6],
                   buf[7]);
    }
    os_task_msleep(50);

    ret = os_i2c_client_write(&client, 0, 1, data, 8);
    if (ret != OS_SUCCESS)
    {
        os_kprintf("i2c write error\r\n");
        return;
    }
    os_task_msleep(50);

    memset(buf, 0, 8);
    ret = os_i2c_client_read(&client, 0, 1, buf, 8);
    if (ret != OS_SUCCESS)
    {
        os_kprintf("i2c read error\r\n");
        return;
    }
    else
    {
        os_kprintf("read: %02x %02x %02x %02x %02x %02x %02x %02x\r\n",
                   buf[0],
                   buf[1],
                   buf[2],
                   buf[3],
                   buf[4],
                   buf[5],
                   buf[6],
                   buf[7]);
    }
}
SH_CMD_EXPORT(i2c_test, i2c_test, "i2c_test");
