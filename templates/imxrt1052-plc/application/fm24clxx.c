#include <os_task.h>
#include <drv_cfg.h>
#include <os_memory.h>
#include <shell.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <i2c.h>
#include <stdio.h>
#include "fm24clxx.h"

#define DRV_EXT_LVL LOG_LVL_DEBUG
#define DRV_EXT_TAG "FRAM"
#define DBG_TAG     "FRAM"
#include <drv_log.h>

#define FRAM_ADDR          0xA8
#define FRAM_BUS_NAME      "soft_i2c1"
#define FM24CLXX_SIZE      (512)
#define FM24CLXX_PAGE_SIZE (256)

struct os_fram
{
    struct os_device     dev;
    struct os_i2c_client i2c;
};

struct os_fram *fram;

os_err_t fram_rw_data(uint32_t rw_addr, const uint8_t *buff, uint32_t num, os_bool_t write_flag)
{
    os_err_t ret;
    uint8_t  page_select = 0;
    uint32_t addr;
    uint32_t once_num;
    os_err_t (*os_i2c_client_rw)(struct os_i2c_client * client,
                                 uint32_t cmd,
                                 uint8_t  cmd_len,
                                 uint8_t * buff,
                                 uint32_t len);
    struct os_i2c_client *client = &fram->i2c;

    if (write_flag)
    {
        os_i2c_client_rw = os_i2c_client_write;
    }
    else
    {
        os_i2c_client_rw = os_i2c_client_read;
    }

    if (((rw_addr + num) <= FM24CLXX_PAGE_SIZE)                                       //  write only page1
        || ((rw_addr > FM24CLXX_PAGE_SIZE) && ((rw_addr + num) <= FM24CLXX_SIZE)))    //  write only page2
    {
        page_select = rw_addr / FM24CLXX_PAGE_SIZE;

        addr = rw_addr % FM24CLXX_PAGE_SIZE;

        client->client_addr = ((FRAM_ADDR | page_select << 1) >> 1);

        return os_i2c_client_rw(client, addr, 1, (uint8_t *)buff, num);
    }
    else if ((rw_addr < FM24CLXX_PAGE_SIZE) && ((rw_addr + num) <= FM24CLXX_SIZE))    //  write page1 and page2
    {
        page_select = 0;

        addr = rw_addr;

        client->client_addr = ((FRAM_ADDR | page_select << 1) >> 1);

        once_num = FM24CLXX_PAGE_SIZE - rw_addr;

        ret = os_i2c_client_rw(client, addr, 1, (uint8_t *)buff, once_num);
        if (ret != OS_SUCCESS)
            return ret;

        page_select = 1;

        addr = FM24CLXX_PAGE_SIZE;

        client->client_addr = ((FRAM_ADDR | page_select << 1) >> 1);

        once_num = num - (FM24CLXX_PAGE_SIZE - rw_addr);

        return os_i2c_client_rw(client, addr, 1, (uint8_t *)(buff + FM24CLXX_PAGE_SIZE - rw_addr), once_num);
    }
    else
    {
        LOG_E(DBG_TAG, "%s Invalid argument.", (write_flag == OS_TRUE) ? "Write" : "Read");

        return OS_INVAL;
    }
}

os_err_t fram_write_data(uint32_t write_addr, const uint8_t *buff, uint32_t num)
{
    return fram_rw_data(write_addr, buff, num, OS_TRUE);
}

os_err_t fram_read_data(uint32_t read_addr, const uint8_t *buff, uint32_t num)
{
    return fram_rw_data(read_addr, buff, num, OS_FALSE);
}

static int fram_init(void)
{
    fram = os_calloc(1, sizeof(struct os_fram));
    if (fram == OS_NULL)
    {
        LOG_E(DBG_TAG, "FRAM i2c malloc failed.");
        return OS_FULL;
    }

    fram->i2c.bus = (struct os_i2c_bus_device *)os_device_find(FRAM_BUS_NAME);
    if (fram->i2c.bus == OS_NULL)
    {
        LOG_E(DBG_TAG, "FRAM i2c invalid.");
        return OS_INVAL;
    }

    fram->dev.type = OS_DEVICE_TYPE_BLOCK;

    return os_device_register(&fram->dev, "FM24CL04B");

    return OS_SUCCESS;
}
OS_INIT_CALL(fram_init, OS_INIT_LEVEL_DEVICE, OS_INIT_SUBLEVEL_LOW);

void fram_test(int argc, char *argv[])
{
    int      i;
    uint8_t *pbuf, *ptr;

    pbuf = os_calloc(1, FM24CLXX_SIZE);

    ptr = pbuf;

    for (i = 0; i < FM24CLXX_SIZE; i++)
    {
        *ptr++ = (i % 256);
    }

    if (fram_write_data(0, pbuf, FM24CLXX_SIZE) != OS_SUCCESS)
    {
        LOG_E(DBG_TAG, "fram write error");
        return;
    }

    if (fram_read_data(0, pbuf, FM24CLXX_SIZE) != OS_SUCCESS)
    {
        LOG_E(DBG_TAG, "fram read error");
        return;
    }

    ptr = pbuf;
    for (i = 0; i < FM24CLXX_SIZE; i++)
    {
        if (*ptr++ != (i % 256))
            break;
    }

    LOG_HEX(DBG_TAG, 8, pbuf, FM24CLXX_SIZE);

    if (i < FM24CLXX_SIZE)
        LOG_E(DBG_TAG, "fram test error");
    else
        LOG_I(DBG_TAG, "fram test success");
}
SH_CMD_EXPORT(fram_test, fram_test, "fram_test");
