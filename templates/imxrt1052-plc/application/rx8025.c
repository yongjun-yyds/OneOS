#include <os_task.h>
#include <drv_cfg.h>
#include <os_memory.h>
#include <shell.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <i2c.h>
#include <stdio.h>
#include "rx8025.h"

#define DRV_EXT_LVL         LOG_LVL_DEBUG
#define DRV_EXT_TAG         "rx8025" 
#define DBG_TAG             "rx8025" 
#include <drv_log.h>

#define RX8025_ADDR             0x64
#define RX8025_BUS_NAME         "soft_i2c1"

// �豸��д��ַ
#define RX8025_ADDR_WRITE       0x64            //	RX8025��д��ַ
#define RX8025_ADDR_READ        0x65            //	RX8025�Ķ���ַ

#define RX8025_ADDR_SEC     		(unsigned char)0x00 
#define RX8025_ADDR_MIN     		(unsigned char)0x01
#define RX8025_ADDR_HOUR     		(unsigned char)0x02
#define RX8025_ADDR_WEEK        	(unsigned char)0x03
#define RX8025_ADDR_DAY      		(unsigned char)0x04
#define RX8025_ADDR_MONTH     		(unsigned char)0x05
#define RX8025_ADDR_YEAR       		(unsigned char)0x06
#define RX8025_ADDR_RAM 			(unsigned char)0x07
#define RX8025_ADDR_MIN_ALARM  		(unsigned char)0x08
#define RX8025_ADDR_HOUR_ALARM 		(unsigned char)0x09
#define RX8025_ADDR_WEEK_ALARM 		(unsigned char)0x0A
#define RX8025_ADDR_DAY_ALARM  		(unsigned char)0x0A
#define RX8025_ADDR_TIMER_COUNTER_0	(unsigned char)0x0B
#define RX8025_ADDR_TIMER_COUNTER_1	(unsigned char)0x0C
#define RX8025_ADDR_EXT_REG    		(unsigned char)0x0D
#define RX8025_ADDR_FLAG_REG    	(unsigned char)0x0E
#define RX8025_ADDR_CONTROL_REG    	(unsigned char)0x0F

//	��չ�Ĵ���,��ַ:D
#define REGBIT_TSEL0				(0x01<<0)	//	�����趨�̶����ڵ��ڲ�ʱ��Դ��
#define REGBIT_TSEL1				(0x01<<1)	//	�����趨�̶����ڵ��ڲ�ʱ��Դ��
#define REGBIT_FSEL0				(0x01<<2)	//	FSEL��FOUTƵ��ѡ��λ��		
#define REGBIT_FSEL1				(0x01<<3)	//	FSEL��FOUTƵ��ѡ��λ��
#define REGBIT_TE					(0x01<<4)	//	TE����ʱ����ʹ��λ��
#define REGBIT_USEL					(0x01<<5)	//	USEL(��ʱ�����ж�ѡ��λ)
#define REGBIT_WADA					(0x01<<6)	//	WADA( ���ں���������ѡ��λ)
#define REGBIT_TEST					(0x01<<7)	//	��������λ

//	��־�Ĵ���,��ַ:E
#define REGBIT_VDET					(0x01<<0)	//	��ѹ����־λ
#define REGBIT_VLF					(0x01<<1)	//	��ѹ�ͱ�־λ
#define REGBIT_AF					(0x01<<3)	//	�����жϱ�־λ
#define REGBIT_TF					(0x01<<4)	//	��ʱ�жϱ�־λ
#define REGBIT_UF					(0x01<<5)	//	ʱ����±�־λ

//	���ƼĴ���,��ַ:F
#define REGBIT_RESET				(0x01<<0)	//	д��һ�� ��1������λ�����Ҹ�ֵά�� 1 �����ϣ�����ֹͣ�����������Լ���RTC ģ���ڲ�������ֵ���и�λ��
#define REGBIT_AIE					(0x01<<3)	//	�����ж�ʹ��λ
#define REGBIT_TIE					(0x01<<4)	//	��ʱ�ж�ʹ��λ
#define REGBIT_UIE					(0x01<<5)	//	�����ж�ʹ��λ	
#define REGBIT_CSEL0				(0x01<<6)	//	����������λ�������趨�¶Ȳ�����ʱ������
#define REGBIT_CSEL1				(0x01<<7)	//	����������λ�������趨�¶Ȳ�����ʱ������


char const *WEEKDAYS_STR_INFO_ARRAY[] = {
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
};

/*********************************************************************************************************
** Function name:       dec_to_bcd
** Descriptions:        ʮ����תBCD����
** input parameters:    ��
** output parameters:   ��
** Returned value:      ��
*********************************************************************************************************/
unsigned char dec_to_bcd(unsigned char _dec)
{
    unsigned char temp = 0;

    while (_dec >= 10)
    {
        temp++;
        _dec -= 10;
    }
    
    return ((unsigned char)(temp << 4) | _dec);
}

/*********************************************************************************************************
** Function name:       bcd_to_dec
** Descriptions:        BCD����תʮ����
** input parameters:    ��
** output parameters:   ��
** Returned value:      ��
*********************************************************************************************************/
unsigned char bcd_to_dec(unsigned char _BCD)
{
    unsigned char temp = 0;

    temp = ((unsigned char)(_BCD & (unsigned char)0xF0) >> (unsigned char)0x04) * 10;

    return (temp + (_BCD & (unsigned char)0x0F));
}

static os_err_t rx8025_write_reg(struct os_i2c_client *dev, uint8_t reg, uint8_t data)
{
    return os_i2c_client_write(dev, reg, 1, &data, 1);
}

static os_err_t rx8025_read_regs(struct os_i2c_client *dev, uint8_t reg, uint8_t len, uint8_t *buf)
{
    return os_i2c_client_read(dev, reg, 1, buf, len);
}

uint8_t cal_weekdays(rx8025_datetime_t *pRtcDate)
{
	int years, y, c, m, d, w;

	years = pRtcDate->years + 2000;
	y = years % 100;	//�ꡡ��2015 ������15��
	c = years / 100;	// ���ǰ��λ����2015��20
	m = pRtcDate->months; 
	d = pRtcDate->days;

	if (m == 1 || m == 2) 
	{ //�ж��·��Ƿ�Ϊ1��2
		y--;
		m += 12;//ĳ���1��2��Ҫ������һ���13��14��������
	}
	w = y + y / 4 + c / 4 - 2 * c + 13 * (m + 1) / 5 + d - 1;//���չ�ʽ�Ĺ�ʽ
	while (w < 0) w += 7;//ȷ������Ϊ��
	w %= 7;

	//LOG_I(DRV_EXT_TAG,"%.4d-%.2d-%.2d is: %s\r\n",years, pRtcDate->months, pRtcDate->days, WEEKDAYS_STR_INFO_ARRAY[w]);

	//return WEEKDAYS_VALUE_ARRAY[w];
    return w;
}

void rx8025_get_date_time(struct os_i2c_client *dev, rx8025_datetime_t *datetime)
{
    uint8_t buf[7];

    rx8025_read_regs(dev, RX8025_ADDR_SEC, sizeof(buf), buf);

	datetime->seconds 		= bcd_to_dec(buf[0] & 0X7F);	//	RX8025_ADDR_SEC
    datetime->minutes 		= bcd_to_dec(buf[1] & 0X7F);	//	RX8025_ADDR_MIN
    datetime->hours 		= bcd_to_dec(buf[2] & 0X3F);	//	RX8025_ADDR_HOUR
    datetime->weekdays		= bcd_to_dec(buf[3] & 0X7F);	//	RX8025_ADDR_WEEK 
    datetime->days 			= bcd_to_dec(buf[4] & 0X3F);	//	RX8025_ADDR_DAY
    datetime->months 		= bcd_to_dec(buf[5] & 0X1F);	//	RX8025_ADDR_MONTH
    datetime->years 		= bcd_to_dec(buf[6]);			//	RX8025_ADDR_YEAR   
}

os_err_t rx8025_set_date_time(struct os_i2c_client *dev, const rx8025_datetime_t *datetime)
{
    uint8_t i;
    uint8_t buf[7];

    if(datetime->years > 99)
        return OS_INVAL;
    
    if((datetime->months > 12) || (datetime->months < 1))
        return OS_INVAL;
    
    if((datetime->days > 31) || (datetime->days < 1))
        return OS_INVAL;
    
    if(datetime->hours > 23)
        return OS_INVAL;
    
    if(datetime->minutes > 59)
        return OS_INVAL;
    
    if(datetime->seconds > 59)
        return OS_INVAL;

    buf[0]  = dec_to_bcd(datetime->seconds);
    buf[1]  = dec_to_bcd(datetime->minutes);
    buf[2]  = dec_to_bcd(datetime->hours);
    //buf[3]
    buf[4]  = dec_to_bcd(datetime->days);
    buf[5]  = dec_to_bcd(datetime->months);
    buf[6]  = dec_to_bcd(datetime->years);

    for(i=0; i<7; i++)
    {
        rx8025_write_reg(dev, RX8025_ADDR_SEC + i, buf[i]);
    }

    return OS_SUCCESS;
}


static time_t get_timestamp(os_device_t *dev)
{
    struct tm tm_new = {0};
    rx8025_datetime_t rtcDate = {0};
    
    struct os_rx8025_rtc *rx8025_rtc = (struct os_rx8025_rtc *)dev;

    rx8025_get_date_time(&rx8025_rtc->i2c, &rtcDate);

    tm_new.tm_sec  = rtcDate.seconds;
    tm_new.tm_min  = rtcDate.minutes;
    tm_new.tm_hour = rtcDate.hours;

    tm_new.tm_mday = rtcDate.days;
    tm_new.tm_mon  = rtcDate.months - 1;
    tm_new.tm_year = rtcDate.years  + 100;      /* RX8025 0~99 -> 2000~2099, Pay attention to the conversion */

#if 0
    os_kprintf("get time:\t%.2d-%.2d-%.2d %.2d:%.2d:%.2d\r\n",
						rtcDate.years, 
                        rtcDate.months, 
                        rtcDate.days, 
                        rtcDate.hours, 
                        rtcDate.minutes, 
                        rtcDate.seconds
                        );
#endif

    return mktime(&tm_new);
}

static int set_timestamp(os_device_t *dev, time_t timestamp)
{
    struct tm *p_tm;
    rx8025_datetime_t rtcDate;
    struct os_rx8025_rtc *rx8025_rtc = (struct os_rx8025_rtc *)dev;

    p_tm = localtime(&timestamp);

    rtcDate.seconds = p_tm->tm_sec ;
    rtcDate.minutes = p_tm->tm_min ;
    rtcDate.hours   = p_tm->tm_hour;

    rtcDate.days    = p_tm->tm_mday;
    rtcDate.months  = p_tm->tm_mon  + 1;
    rtcDate.years   = p_tm->tm_year - 100;      /* RX8025 0~99 -> 2000~2099, Pay attention to the conversion */

#if 0
    os_kprintf("set time:\t%.2d-%.2d-%.2d %.2d:%.2d:%.2d\r\n",
                rtcDate.years, 
                rtcDate.months, 
                rtcDate.days, 
                rtcDate.hours, 
                rtcDate.minutes, 
                rtcDate.seconds
                );
#endif

    if (rx8025_set_date_time(&rx8025_rtc->i2c, &rtcDate) != OS_SUCCESS)
    {
        LOG_E(DRV_EXT_TAG, "set rtc date time failed\r\n");
        return OS_FAILURE;
    }

    return OS_SUCCESS;
}

static os_err_t rx8025_rtc_control(os_device_t *dev, int cmd, void *args)
{
    OS_ASSERT(dev != OS_NULL);

    switch(cmd)
    {
        case OS_DEVICE_CTRL_RTC_GET_TIME:
            *(uint32_t *)args = get_timestamp(dev);
            break;
        case OS_DEVICE_CTRL_RTC_SET_TIME:
            set_timestamp(dev, *(time_t *)args);
            break;
        default:
            return OS_INVAL;
    }

    return OS_SUCCESS;
}

const static struct os_device_ops rtc_ops = {
    .control = rx8025_rtc_control,
};

static int rx8025_rtc_init(void)
{
    struct os_rx8025_rtc *rx8025_rtc;

    rx8025_rtc = os_calloc(1, sizeof(struct os_rx8025_rtc));

    OS_ASSERT(rx8025_rtc);

    rx8025_rtc->i2c.bus = os_i2c_bus_device_find(RX8025_BUS_NAME);

    OS_ASSERT(rx8025_rtc->i2c.bus);

    rx8025_rtc->i2c.client_addr = (RX8025_ADDR>>1);
    
    rx8025_rtc->dev.ops = &rtc_ops;

    rx8025_rtc->dev.type = OS_DEVICE_TYPE_RTC;

    return os_device_register(&rx8025_rtc->dev, "rtc");
}

OS_INIT_CALL(rx8025_rtc_init, OS_INIT_LEVEL_DEVICE, OS_INIT_SUBLEVEL_LOW);


static void rtc_get_time(int argc, char *argv[])
{
    time_t now;

    /* Get time */
    now = rtc_get();

    os_kprintf("%s\r\n", ctime(&now));
}
SH_CMD_EXPORT(rtc_get_time, rtc_get_time, "get rtc time");

static void rtc_set_time(int argc, char *argv[])
{
    int         year;
    int         month;
    int         day; 
    int         hour;  
    int         minute; 
    int         second;
    os_err_t    ret;
    time_t      now;

    if (argc != 3)
    {
        os_kprintf("usage: rtc_set_time 2021-10-08 17:54:01\r\n");
        return;
    }

    if ( (3 != sscanf(argv[1], "%d-%d-%d", &year, &month, &day)) || 
         (3 != sscanf(argv[2], "%d:%d:%d", &hour, &minute, &second))
        )
    {
        os_kprintf("usage: rtc_set_time 2021-10-08 17:54:01\r\n");
        return;
    }

    /* Set date */
    ret = set_date(year, month, day);
    if (ret != OS_SUCCESS)
    {
        os_kprintf("set RTC date failed %d\r\n", (int)ret);
        return;
    }

    /* Set time */
    ret = set_time(hour, minute, second);
    if (ret != OS_SUCCESS)
    {
        os_kprintf("set RTC time failed %d\r\n", (int)ret);
        return;
    }

    /* Get time */
    now = rtc_get();
    LOG_I(DRV_EXT_TAG,"NOW TIME: %s", ctime(&now));
}
SH_CMD_EXPORT(rtc_set_time, rtc_set_time, "set rtc time");
