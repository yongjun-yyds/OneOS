/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        app_dev.h
 *
 * @brief       Board resource definition
 *
 * @revision
 * Date         Author          Notes
 * 2021-09-28   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __APP_DEV_H__
#define __APP_DEV_H__

#include "fsl_gpio.h"
#include "fsl_common.h"
#include "board.h"
#include "pin_mux.h"

struct serial_drv_info
{
    os_device_t *dev;
    const char  *uart_name;
    const char  *rx_sem_name;
    const char  *tx_sem_name;
    os_semaphore_id     rx_sem;
    os_semaphore_id     tx_sem;
    uint32_t     baud_rate;
    os_err_t (*rx_done)(os_device_t *uart, struct os_device_cb_info *info);
    os_err_t (*tx_done)(os_device_t *uart, struct os_device_cb_info *info);
};

#define GET_DI_1()  GPIO_PinRead(BOARD_DI_1_GPIO, BOARD_DI_1_GPIO_PIN)
#define GET_DI_2()  GPIO_PinRead(BOARD_DI_2_GPIO, BOARD_DI_2_GPIO_PIN)
#define GET_DI_3()  GPIO_PinRead(BOARD_DI_3_GPIO, BOARD_DI_3_GPIO_PIN)
#define GET_DI_4()  GPIO_PinRead(BOARD_DI_4_GPIO, BOARD_DI_4_GPIO_PIN)
#define GET_DI_5()  GPIO_PinRead(BOARD_DI_5_GPIO, BOARD_DI_5_GPIO_PIN)
#define GET_DI_6()  GPIO_PinRead(BOARD_DI_6_GPIO, BOARD_DI_6_GPIO_PIN)
#define GET_DI_7()  GPIO_PinRead(BOARD_DI_7_GPIO, BOARD_DI_7_GPIO_PIN)
#define GET_DI_8()  GPIO_PinRead(BOARD_DI_8_GPIO, BOARD_DI_8_GPIO_PIN)
#define GET_DI_9()  GPIO_PinRead(BOARD_DI_9_GPIO, BOARD_DI_9_GPIO_PIN)
#define GET_DI_10() GPIO_PinRead(BOARD_DI_10_GPIO, BOARD_DI_10_GPIO_PIN)
#define GET_DI_11() GPIO_PinRead(BOARD_DI_11_GPIO, BOARD_DI_11_GPIO_PIN)
#define GET_DI_12() GPIO_PinRead(BOARD_DI_12_GPIO, BOARD_DI_12_GPIO_PIN)
#define GET_DI_13() GPIO_PinRead(BOARD_DI_13_GPIO, BOARD_DI_13_GPIO_PIN)
#define GET_DI_14() GPIO_PinRead(BOARD_DI_14_GPIO, BOARD_DI_14_GPIO_PIN)
#define GET_DI_15() GPIO_PinRead(BOARD_DI_15_GPIO, BOARD_DI_15_GPIO_PIN)
#define GET_DI_16() GPIO_PinRead(BOARD_DI_16_GPIO, BOARD_DI_16_GPIO_PIN)

#define GET_DO_1() GPIO_PinRead(BOARD_DO_1_GPIO, BOARD_DO_1_GPIO_PIN)
#define GET_DO_2() GPIO_PinRead(BOARD_DO_2_GPIO, BOARD_DO_2_GPIO_PIN)
#define GET_DO_3() GPIO_PinRead(BOARD_DO_3_GPIO, BOARD_DO_3_GPIO_PIN)
#define GET_DO_4() GPIO_PinRead(BOARD_DO_4_GPIO, BOARD_DO_4_GPIO_PIN)
#define GET_DO_5() GPIO_PinRead(BOARD_DO_5_GPIO, BOARD_DO_5_GPIO_PIN)
#define GET_DO_6() GPIO_PinRead(BOARD_DO_6_GPIO, BOARD_DO_6_GPIO_PIN)
#define GET_DO_7() GPIO_PinRead(BOARD_DO_7_GPIO, BOARD_DO_7_GPIO_PIN)
#define GET_DO_8() GPIO_PinRead(BOARD_DO_8_GPIO, BOARD_DO_8_GPIO_PIN)

#define GET_PWR_CMP() GPIO_PinRead(BOARD_ADC2_03_CMP_GPIO, BOARD_ADC2_03_CMP_GPIO_PIN)

#define TURN_ON_DO_1() GPIO_PinWrite(BOARD_DO_1_GPIO, BOARD_DO_1_GPIO_PIN, 0)
#define TURN_ON_DO_2() GPIO_PinWrite(BOARD_DO_2_GPIO, BOARD_DO_2_GPIO_PIN, 0)
#define TURN_ON_DO_3() GPIO_PinWrite(BOARD_DO_3_GPIO, BOARD_DO_3_GPIO_PIN, 0)
#define TURN_ON_DO_4() GPIO_PinWrite(BOARD_DO_4_GPIO, BOARD_DO_4_GPIO_PIN, 0)
#define TURN_ON_DO_5() GPIO_PinWrite(BOARD_DO_5_GPIO, BOARD_DO_5_GPIO_PIN, 0)
#define TURN_ON_DO_6() GPIO_PinWrite(BOARD_DO_6_GPIO, BOARD_DO_6_GPIO_PIN, 0)
#define TURN_ON_DO_7() GPIO_PinWrite(BOARD_DO_7_GPIO, BOARD_DO_7_GPIO_PIN, 0)
#define TURN_ON_DO_8() GPIO_PinWrite(BOARD_DO_8_GPIO, BOARD_DO_8_GPIO_PIN, 0)

#define TURN_OFF_DO_1() GPIO_PinWrite(BOARD_DO_9_GPIO, BOARD_DO_9_GPIO_PIN, 1)
#define TURN_OFF_DO_2() GPIO_PinWrite(BOARD_DO_10_GPIO, BOARD_DO_10_GPIO_PIN, 1)
#define TURN_OFF_DO_3() GPIO_PinWrite(BOARD_DO_11_GPIO, BOARD_DO_11_GPIO_PIN, 1)
#define TURN_OFF_DO_4() GPIO_PinWrite(BOARD_DO_12_GPIO, BOARD_DO_12_GPIO_PIN, 1)
#define TURN_OFF_DO_5() GPIO_PinWrite(BOARD_DO_13_GPIO, BOARD_DO_13_GPIO_PIN, 1)
#define TURN_OFF_DO_6() GPIO_PinWrite(BOARD_DO_14_GPIO, BOARD_DO_14_GPIO_PIN, 1)
#define TURN_OFF_DO_7() GPIO_PinWrite(BOARD_DO_15_GPIO, BOARD_DO_15_GPIO_PIN, 1)
#define TURN_OFF_DO_8() GPIO_PinWrite(BOARD_DO_16_GPIO, BOARD_DO_16_GPIO_PIN, 1)

#define SET_RS485_RDE_1() GPIO_PinWrite(BOARD_RS485_DIR1_GPIO, BOARD_RS485_DIR1_PIN, 1)
#define CLR_RS485_RDE_1() GPIO_PinWrite(BOARD_RS485_DIR1_GPIO, BOARD_RS485_DIR1_PIN, 0)

#define SET_RS485_RDE_2() GPIO_PinWrite(BOARD_RS485_DIR2_GPIO, BOARD_RS485_DIR2_PIN, 1)
#define CLR_RS485_RDE_2() GPIO_PinWrite(BOARD_RS485_DIR2_GPIO, BOARD_RS485_DIR2_PIN, 0)

extern const GPIO_Type *array_do_gpio[];
extern const uint32_t   array_do_pin[];
extern const GPIO_Type *array_di_gpio[];
extern const uint32_t   array_di_pin[];
extern const GPIO_Type *array_led_gpio[];
extern const uint32_t   array_led_pin[];

#endif
