#ifndef FM24CL04_H
#define FM24CL04_H

#ifdef __cplusplus
extern "C" {
#endif

#include <os_task.h>
#include <device.h>

extern os_err_t fram_rw_data(uint32_t rw_addr, const uint8_t *buff, uint32_t num, os_bool_t write_flag);

#ifdef __cplusplus
}
#endif

#endif
