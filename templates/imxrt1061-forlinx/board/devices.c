static const struct nxp_rtc_lp_info rtc_lp_info = {RTC_LP_PERIPHERAL, &RTC_LP_config};
OS_HAL_DEVICE_DEFINE("RTC_LP_Type", "rtc_lp", rtc_lp_info);

static const struct nxp_adc_info adc1_info = {ADC1_PERIPHERAL, &ADC1_config};
OS_HAL_DEVICE_DEFINE("ADC_Type", "adc1", adc1_info);

static const struct nxp_adc_info adc2_info = {ADC2_PERIPHERAL, &ADC2_config};
OS_HAL_DEVICE_DEFINE("ADC_Type", "adc2", adc2_info);

static const struct nxp_can_info can2_info = {CAN2_PERIPHERAL, &CAN2_config};
OS_HAL_DEVICE_DEFINE("CAN_Type", "can2", can2_info);

static const struct nxp_gpt_info gpt1_info = {GPT1_PERIPHERAL, &GPT1_config};
OS_HAL_DEVICE_DEFINE("GPT_Type", "gpt1", gpt1_info);

static const struct nxp_gpt_info gpt2_info = {GPT2_PERIPHERAL, &GPT2_config};
OS_HAL_DEVICE_DEFINE("GPT_Type", "gpt2", gpt2_info);

static const struct nxp_lpuart_info lpuart1_info = {LPUART1_PERIPHERAL, &LPUART1_config};
OS_HAL_DEVICE_DEFINE("LPUART_Type", "lpuart1", lpuart1_info);

static const struct nxp_lpuart_info lpuart2_info = {LPUART2_PERIPHERAL, &LPUART2_config};
OS_HAL_DEVICE_DEFINE("LPUART_Type", "lpuart2", lpuart2_info);

static const struct nxp_lpuart_info lpuart3_info = {LPUART3_PERIPHERAL, &LPUART3_config};
OS_HAL_DEVICE_DEFINE("LPUART_Type", "lpuart3", lpuart3_info);

static const struct nxp_lpuart_info lpuart4_info = {LPUART4_PERIPHERAL, &LPUART4_config};
OS_HAL_DEVICE_DEFINE("LPUART_Type", "lpuart4", lpuart4_info);

static const struct nxp_pwm_info pwm4_info = {PWM4_PERIPHERAL, &PWM4_config};
OS_HAL_DEVICE_DEFINE("PWM_Type", "pwm4", pwm4_info);

static const struct nxp_wdog_info wdog1_info = {WDOG1_PERIPHERAL, &WDOG1_config};
OS_HAL_DEVICE_DEFINE("WDOG_Type", "wdog1", wdog1_info);

