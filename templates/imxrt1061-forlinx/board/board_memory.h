/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        board_memory.h
 *
 * @brief       board memory definition
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __BOARD_MEMORY_H__
#define __BOARD_MEMORY_H__

#define IMXRT_FLASH_START       (0x60000000)
#define IMXRT_FLASH_SIZE        (4 * 1024 * 1024)
#define IMXRT_FLASH_PAGE_SIZE   (256)
#define IMXRT_FLASH_SECTOR_SIZE (IMXRT_FLASH_PAGE_SIZE * 16)
#define IMXRT_FLASH_BLOCK_SIZE  (IMXRT_FLASH_PAGE_SIZE * 128)
#define IMXRT_FLASH_END_ADDRESS (IMXRT_FLASH_START_ADRESS + IMXRT_FLASH_SIZE)

#define IMXRT_FLASH_APP_NAME      "app"
#define IMXRT_FLASH_APP_START     (0)
#define IMXRT_FLASH_APP_SIZE      (1 * 1024 * 1024)
#define IMXRT_FLASH_USER_NAME     "user"
#define IMXRT_FLASH_USER_START    IMXRT_FLASH_APP_START + IMXRT_FLASH_APP_SIZE
#define IMXRT_FLASH_USER_SIZE     (1 * 1024 * 1024)
#define IMXRT_FLASH_CODESYS_NAME  "CoDeSysApp"
#define IMXRT_FLASH_CODESYS_START IMXRT_FLASH_USER_START + IMXRT_FLASH_USER_SIZE
#define IMXRT_FLASH_CODESYS_SIZE  (1 * 1024 * 1024)

#define IMXRT_ITCM_START (0x00000000)
#define IMXRT_ITCM_SIZE  (128 * 1024)

#define IMXRT_DTCM_START (0x20000000)
#define IMXRT_DTCM_SIZE  (128 * 1024)

#define IMXRT_OCTCM_START   (0x20200000)
#define IMXRT_OCTCM_SIZE    (768 * 1024)
#define IMXRT_CODESYS_START (0x20200000)
#define IMXRT_CODESYS_SIZE  (512 * 1024)

#endif
