/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with 
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        dev_adc.c
 *
 * @brief       This file implements adc driver for cm32
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifdef OS_USING_ADC
#include "drv_adc.h"

#ifdef BSP_USING_ADC1

struct cm32_adc_info adc1_info = {
    .adc_base       = ADC1,
    .channel_bitmap = 1 << ADC_CH_6,

    .gpio_cfg[ADC_CH_6] = {GPIOC, GPIO_PIN_0},
};
OS_HAL_DEVICE_DEFINE("ADC_HandleTypeDef", "adc1", adc1_info);

#endif

#ifdef BSP_USING_ADC2

struct cm32_adc_info adc2_info = {
    .adc_base       = ADC2,
    .channel_bitmap = 1 << ADC_CH_3,

    .gpio_cfg[ADC_CH_3] = {GPIOB, GPIO_PIN_1},
};
OS_HAL_DEVICE_DEFINE("ADC_HandleTypeDef", "adc2", adc2_info);

#endif

#ifdef BSP_USING_ADC3

struct cm32_adc_info adc3_info = {
    .adc_base       = ADC3,
    .channel_bitmap = 1 << ADC_CH_4,

    .gpio_cfg[ADC_CH_4] = {GPIOE, GPIO_PIN_12},
};
OS_HAL_DEVICE_DEFINE("ADC_HandleTypeDef", "adc3", adc3_info);

#endif

#ifdef BSP_USING_ADC4

struct cm32_adc_info adc4_info = {
    .adc_base       = ADC4,
    .channel_bitmap = 1 << ADC_CH_7 | 1 << ADC_CH_8,

    .gpio_cfg[ADC_CH_7] = {GPIOD, GPIO_PIN_10},
    .gpio_cfg[ADC_CH_8] = {GPIOD, GPIO_PIN_11},
};
OS_HAL_DEVICE_DEFINE("ADC_HandleTypeDef", "adc4", adc4_info);

#endif

#endif /* OS_USING_ADC */
