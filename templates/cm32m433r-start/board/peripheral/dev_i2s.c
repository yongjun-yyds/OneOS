/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        dev_i2s.c
 *
 * @brief       This file implements spi driver for cm32
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include "drv_i2s.h"

#ifdef BSP_USING_I2S2
struct cm32_i2s_info i2s2_info = {
    .i2s_base           = SPI2,
    .i2s_init_structure = {
        .Standard       = I2S_STD_PHILLIPS,
        .DataFormat     = I2S_DATA_FMT_16BITS,
        .MCLKEnable     = I2S_MCLK_ENABLE,
        .AudioFrequency = I2S_AUDIO_FREQ_22K,
        .CLKPOL         = I2S_CLKPOL_LOW,
        .I2sMode        = I2S_MODE_MASTER_TX,
     },

    .dma_tx_clk         = RCC_AHB_PERIPH_DMA1,
    .dma_tx_irqn        = DMA1_Channel5_IRQn,
    .dma_tx_channel     = DMA1_CH5,

    .dma_rx_clk         = RCC_AHB_PERIPH_DMA1,
    .dma_rx_irqn        = DMA1_Channel4_IRQn,
    .dma_rx_channel     = DMA1_CH4,

    .gpio_remap         = GPIO_RMP3_SPI2,
    .port_ck            = GPIOE,
    .pin_ck             = GPIO_PIN_11,
    .port_sd            = GPIOE,
    .pin_sd             = GPIO_PIN_13,
    .port_ws            = GPIOE,
    .pin_ws             = GPIO_PIN_10,
    .port_mck           = GPIOC,
    .pin_mck            = GPIO_PIN_6,
};
OS_HAL_DEVICE_DEFINE("I2S_HandleTypeDef", "i2s2", i2s2_info);
#endif

#ifdef BSP_USING_I2S3
struct cm32_i2s_info i2s3_info = {
    .i2s_base       = SPI3,
    .i2s_init_structure = {
        .Standard       = I2S_STD_PHILLIPS,
        .DataFormat     = I2S_DATA_FMT_16BITS,
        .MCLKEnable     = I2S_MCLK_ENABLE,
        .AudioFrequency = I2S_AUDIO_FREQ_22K,
        .CLKPOL         = I2S_CLKPOL_LOW,
        .I2sMode        = I2S_MODE_MASTER_RX,
     },

    .dma_tx_clk     = RCC_AHB_PERIPH_DMA2,
    .dma_tx_irqn    = DMA2_Channel2_IRQn,
    .dma_tx_channel = DMA2_CH2,

    .dma_rx_clk     = RCC_AHB_PERIPH_DMA2,
    .dma_rx_irqn    = DMA2_Channel1_IRQn,
    .dma_rx_channel = DMA2_CH1,

    .gpio_remap     = GPIO_RMP2_SPI3,
    .port_ck        = GPIOD,
    .pin_ck         = GPIO_PIN_9,
    .port_sd        = GPIOD,
    .pin_sd         = GPIO_PIN_12,
    .port_ws        = GPIOD,
    .pin_ws         = GPIO_PIN_8,
    .port_mck       = GPIOC,
    .pin_mck        = GPIO_PIN_7,
};
OS_HAL_DEVICE_DEFINE("I2S_HandleTypeDef", "i2s3", i2s3_info);
#endif
