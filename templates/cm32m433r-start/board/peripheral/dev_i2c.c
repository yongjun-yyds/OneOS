/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        dev_i2c.c
 *
 * @brief       This file implements i2c driver for cm32
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include "drv_i2c.h"

#define GPIO_I2C_NO_RMP 0

#ifdef BSP_USING_I2C1
struct cm32_i2c_info i2c1_info = {
    .i2c_base   = I2C1,
    .slave_addr = 0,
    .gpio_remap = GPIO_I2C_NO_RMP, /* remapping: GPIO_RMP_I2C1 */
    .scl_port   = GPIOB,
    .scl_pin    = GPIO_PIN_6, /* remapping: GPIO_PIN_8 */
    .sda_port   = GPIOB,
    .sda_pin    = GPIO_PIN_7, /* remapping: GPIO_PIN_9 */
};
OS_HAL_DEVICE_DEFINE("I2C_HandleTypeDef", "i2c1", i2c1_info);
#endif

#ifdef BSP_USING_I2C2
struct cm32_i2c_info i2c2_info = {
    .i2c_base   = I2C2,
    .slave_addr = 0,
    .gpio_remap = GPIO_I2C_NO_RMP,
    .scl_port   = GPIOB,
    .scl_pin    = GPIO_PIN_10,
    .sda_port   = GPIOB,
    .sda_pin    = GPIO_PIN_11,
};
OS_HAL_DEVICE_DEFINE("I2C_HandleTypeDef", "i2c2", i2c2_info);
#endif

#ifdef BSP_USING_I2C3
struct cm32_i2c_info i2c3_info = {
    .i2c_base   = I2C3,
    .slave_addr = 0,
    .gpio_remap = GPIO_RMP2_I2C3,
    .scl_port   = GPIOF,
    .scl_pin    = GPIO_PIN_4,
    .sda_port   = GPIOF,
    .sda_pin    = GPIO_PIN_5,
};
OS_HAL_DEVICE_DEFINE("I2C_HandleTypeDef", "i2c3", i2c3_info);
#endif

#ifdef BSP_USING_I2C4
struct cm32_i2c_info i2c4_info = {
    .i2c_base   = I2C4,
    .slave_addr = 0,
    .gpio_remap = GPIO_RMP3_I2C4,
    .scl_port   = GPIOA,
    .scl_pin    = GPIO_PIN_9,
    .sda_port   = GPIOA,
    .sda_pin    = GPIO_PIN_10,
};
OS_HAL_DEVICE_DEFINE("I2C_HandleTypeDef", "i2c4", i2c4_info);
#endif
