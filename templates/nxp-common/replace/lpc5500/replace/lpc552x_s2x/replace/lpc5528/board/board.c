/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        board.c
 *
 * @brief       Initializes the CPU, System clocks, and Peripheral device
 *
 * @revision
 * Date          Author          Notes
 * 2020-02-20    OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include "board.h"
#include <drv_gpio.h>
#include <os_stddef.h>
#include <oneos_config.h>

const led_t led_table[] = {
#ifdef TEMP_LPC5528JBD64
    {GET_PIN(0, 8), PIN_LOW}, /* LEDG */
#endif
#ifdef TEMP_LPC5528JEV98
    {GET_PIN(0, 8), PIN_LOW}, /* LEDG */
#endif
#ifdef TEMP_LPC5528JBD100
    {GET_PIN(0, 8), PIN_LOW}, /* LEDG */
#endif
};

const int led_table_size = ARRAY_SIZE(led_table);

const struct push_button key_table[] = {
    {GET_PIN(1, 9), PIN_MODE_INPUT_PULLUP, PIN_IRQ_MODE_FALLING},
};

const int key_table_size = ARRAY_SIZE(key_table);

#if defined(OS_USING_SN)
const int board_no_pin_tab[] = {
    GET_PIN(1, 1),
    GET_PIN(1, 0),
    GET_PIN(0, 27),
    GET_PIN(0, 26),
    GET_PIN(0, 25),
    GET_PIN(0, 23),
    GET_PIN(0, 22),
    GET_PIN(0, 21),
};
const int board_no_pin_tab_size = ARRAY_SIZE(board_no_pin_tab);

const int slot_no_pin_tab[] = {
    GET_PIN(0, 9),
    GET_PIN(0, 10),
    GET_PIN(0, 15),
    GET_PIN(0, 16),
    GET_PIN(0, 20),
};
const int slot_no_pin_tab_size = ARRAY_SIZE(slot_no_pin_tab);
#endif
