@echo off
cls

set prj_path=%~dp0
cd %prj_path%
::set Libraries_PATH=..\..\..\..\..\Libraries
set TOOL_PATH=.\tool
set BT_CODE_PATH=..\..\drivers\hal\cmiot\CM32M1610_HAL\bt_code
set BT_PATCH_PATH=.\bt_code\patch
set OBJ_PATH=.\build\keil\Obj

if exist %OBJ_PATH%\flash.dat (
	del /a /f /q %OBJ_PATH%\*.dat
)

@echo start compile cm0 bin
if exist %OBJ_PATH%\oneos.hex (
	if "%1" equ "1" (
		%TOOL_PATH%\f_hex2rom.exe 0 4000 %OBJ_PATH%\oneos.hex  %OBJ_PATH%\out.rom 4 0 10014800 1001d800
		) else (
			%TOOL_PATH%\f_hex2rom.exe 0 80000 %OBJ_PATH%\oneos.hex  %OBJ_PATH%\out.rom 4 1
			)) else (
	@echo %OBJ_PATH%\oneos.hex not find!
    goto error_end
)

@echo compile cm0 bin ok


::echo start compile full bin
::cd %BT_PATCH_PATH%
::call do.bat eep
::cd %prj_path%

@echo start gen flash.dat
if exist %BT_CODE_PATH%\sched.rom (
	copy %BT_CODE_PATH%\sched.rom %OBJ_PATH%\sched.rom
) else (
	@echo %BT_CODE_PATH%\sched.rom not find!
    goto error_end
)

if exist %BT_CODE_PATH%\memmap.format (
	copy %BT_CODE_PATH%\memmap.format %OBJ_PATH%\memmap.format
) else (
	@echo %BT_CODE_PATH%\memmap.format not find!
    goto error_end
)

if exist %BT_CODE_PATH%\ramcode.rom (
	copy %BT_CODE_PATH%\ramcode.rom %OBJ_PATH%\ramcode.rom
) else (
	@echo %BT_CODE_PATH%\ramcode.rom not find!
    goto error_end
)

cd %OBJ_PATH%
geneep  -n
geneep -f -n
cd %prj_path%
@echo gen flash.dat ok
goto end

:error_end
@echo compile error!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

:end
