CONFIG_BOARD_LONGAN_NANO=y
CONFIG_ARCH_RISCV=y
CONFIG_ARCH_RISCV32=y
CONFIG_ARCH_RISCV_BUMBLEBEE=y

#
# Kernel
#
# CONFIG_OS_NAME_MAX_7 is not set
CONFIG_OS_NAME_MAX_15=y
# CONFIG_OS_NAME_MAX_31 is not set
CONFIG_OS_NAME_MAX=15
# CONFIG_OS_TASK_PRIORITY_8 is not set
# CONFIG_OS_TASK_PRIORITY_16 is not set
CONFIG_OS_TASK_PRIORITY_32=y
# CONFIG_OS_TASK_PRIORITY_64 is not set
# CONFIG_OS_TASK_PRIORITY_128 is not set
# CONFIG_OS_TASK_PRIORITY_256 is not set
CONFIG_OS_TASK_PRIORITY_MAX=32
CONFIG_OS_TICK_PER_SECOND=1000
CONFIG_OS_SCHEDULE_TIME_SLICE=10
# CONFIG_OS_USING_OVERFLOW_CHECK is not set
# CONFIG_OS_USING_INTERRUPT_STACK_OVERFLOW_CHECK is not set
# CONFIG_OS_USING_TASK_HOOK is not set
CONFIG_OS_USING_ASSERT=y
# CONFIG_OS_USING_KERNEL_LOCK_CHECK is not set
# CONFIG_OS_USING_SAFETY_MECHANISM is not set
# CONFIG_OS_USING_KERNEL_DEBUG is not set
CONFIG_OS_MAIN_TASK_STACK_SIZE=2048
CONFIG_OS_IDLE_TASK_STACK_SIZE=512
CONFIG_OS_RECYCLE_TASK_STACK_SIZE=512
CONFIG_OS_USING_KERNEL_TIMER=y
CONFIG_OS_USING_TIMER=y
CONFIG_OS_TIMER_TASK_STACK_SIZE=1024
CONFIG_OS_TIMER_POWER=3
# CONFIG_OS_TIMER_SORT is not set
# CONFIG_OS_USING_SINGLE_LIST_TIMER is not set
CONFIG_OS_USING_WORKQUEUE=y
CONFIG_OS_USING_SYSTEM_WORKQUEUE=y
CONFIG_OS_SYSTEM_WORKQUEUE_STACK_SIZE=2048
CONFIG_OS_SYSTEM_WORKQUEUE_PRIORITY=8

#
# Inter-task communication and synchronization
#
CONFIG_OS_USING_MUTEX=y
# CONFIG_OS_USING_SPINLOCK_CHECK is not set
CONFIG_OS_USING_SEMAPHORE=y
# CONFIG_OS_SEM_WAIT_HOOK is not set
# CONFIG_OS_SEM_POST_HOOK is not set
CONFIG_OS_USING_EVENT=y
CONFIG_OS_USING_MESSAGEQUEUE=y
CONFIG_OS_USING_MAILBOX=y
# end of Inter-task communication and synchronization

#
# Memory management
#
CONFIG_OS_USING_SYS_HEAP=y
CONFIG_OS_USING_MEM_HEAP=y
CONFIG_OS_USING_ALG_FIRSTFIT=y
# CONFIG_OS_USING_ALG_BUDDY is not set
# CONFIG_OS_USING_MEM_TRACE is not set
# CONFIG_OS_USING_MEM_POOL is not set
# end of Memory management

# CONFIG_OS_USING_IPC_TRACE is not set
# CONFIG_OS_USING_IPC_HOOK is not set
# end of Kernel

#
# C standard library adapter
#
CONFIG_OS_USING_LIBC_ADAPTER=y
CONFIG_OS_USING_NEWLIB_ADAPTER=y
CONFIG_OS_USING_ARMCCLIB_ADAPTER=y
# end of C standard library adapter

#
# Osal
#

#
# POSIX compatibility layer
#
# CONFIG_OS_USING_PTHREADS is not set
# end of POSIX compatibility layer

#
# RT-Thread compatibility layer
#
# CONFIG_OS_USING_RTTHREAD_API_V3_1_3 is not set
# end of RT-Thread compatibility layer

#
# CMSIS compatibility layer
#
# CONFIG_OS_USING_CMSIS_RTOS2_API_V2_1_2 is not set
# end of CMSIS compatibility layer

#
# FreeRTOS compatibility layer
#
# CONFIG_OS_USING_FREERTOS_API_V10_4_3 is not set
# end of FreeRTOS compatibility layer

#
# C++ Features
#
# CONFIG_OS_USING_CPLUSPLUS is not set
# end of C++ Features
# end of Osal

#
# Drivers
#
CONFIG_OS_USING_DEVICE=y
CONFIG_OS_USING_DEVICE_NOTIFY=y
# CONFIG_OS_DEVICE_SUPPORT_PLUG is not set

#
# Audio
#
# CONFIG_OS_USING_AUDIO is not set
# end of Audio

#
# BLOCK
#
CONFIG_OS_USING_BLOCK=y
# end of BLOCK

#
# Boot
#

#
# CORTEX-M Boot
#
# end of CORTEX-M Boot
# end of Boot

#
# CAN
#
# CONFIG_OS_USING_CAN is not set
# end of CAN

#
# CONSOLE
#
CONFIG_OS_USING_CONSOLE=y
CONFIG_OS_CONSOLE_DEVICE_NAME="uart0"
# end of CONSOLE

#
# DMA
#
CONFIG_OS_USING_DMA=y
CONFIG_OS_USING_DMA_RAM=y
CONFIG_OS_USING_SOFT_DMA=y
CONFIG_OS_SOFT_DMA_SUPPORT_NORMAL_MODE=y
CONFIG_OS_SOFT_DMA_SUPPORT_CIRCLE_MODE=y
CONFIG_OS_SOFT_DMA_SUPPORT_SIMUL_TIMEOUT=y
# end of DMA

#
# EEPROM
#
# CONFIG_OS_EEPROM_SUPPORT is not set
# end of EEPROM

#
# FAL
#
CONFIG_OS_USING_FAL=y
# CONFIG_OS_FAL_RAM is not set
# end of FAL

#
# Graphic
#
# CONFIG_OS_USING_GRAPHIC is not set
# end of Graphic

#
# HAL
#
CONFIG_SOC_FAMILY_GD32VF103=y
CONFIG_SOC_SERIES_GD32VF1XX=y
CONFIG_SOC_GD32VF103CBT6=y
CONFIG_BSP_USING_PIN=y
CONFIG_BSP_USING_UART=y
CONFIG_OS_USING_USART0=y
CONFIG_OS_USING_USART0_DMA=y
CONFIG_OS_USING_USART1=y
CONFIG_OS_USING_USART1_DMA=y
CONFIG_BSP_UART_NO_IDLE=y
CONFIG_BSP_USING_I2C=y
CONFIG_BSP_USING_I2C0=y
CONFIG_BSP_USING_I2C1=y

#
# Select SPI port
#
CONFIG_BSP_USING_SPI0=y
# CONFIG_BSP_USING_SPI1 is not set
# end of Select SPI port

CONFIG_BSP_USING_FLASH=y
# end of HAL

#
# HwCrypto
#
# CONFIG_OS_USING_HWCRYPTO is not set
# end of HwCrypto

#
# I2C
#
CONFIG_OS_USING_I2C=y
# CONFIG_OS_USING_I2C_BITOPS is not set
# CONFIG_BSP_USING_I2C_AT24CXX is not set
# end of I2C

#
# Infrared
#
# CONFIG_OS_USING_INFRARED is not set
# end of Infrared

#
# LPMGR
#
# CONFIG_OS_USING_LPMGR is not set
# end of LPMGR

#
# MISC
#
CONFIG_OS_USING_PUSH_BUTTON=y
CONFIG_OS_USING_LED=y
# CONFIG_OS_USING_BUZZER is not set
# CONFIG_OS_USING_ADC is not set
# CONFIG_OS_USING_DAC is not set
# CONFIG_OS_USING_PWM is not set
# CONFIG_OS_USING_INPUT_CAPTURE is not set
# CONFIG_OS_USING_PULSE_ENCODER is not set
# end of MISC

#
# MTD
#
CONFIG_OS_USING_MTD=y
# end of MTD

#
# NAND
#
# CONFIG_OS_USING_NAND is not set
# end of NAND

#
# NET
#
# CONFIG_OS_USING_NET_DEVICE is not set
# CONFIG_OS_USING_WLAN is not set
# CONFIG_BSP_USING_AP6181 is not set
# end of NET

#
# PIN
#
CONFIG_OS_USING_PIN=y
CONFIG_OS_PIN_MAX_CHIP=1
# CONFIG_BSP_USING_PIN_PCF8574 is not set
# end of PIN

#
# RTC
#
# CONFIG_OS_USING_RTC is not set
# end of RTC

#
# SDIO
#
# CONFIG_OS_USING_SDIO is not set
# end of SDIO

#
# Sensors
#
# CONFIG_OS_USING_SENSOR is not set
# end of Sensors

#
# Serial
#
CONFIG_OS_USING_SERIAL=y
CONFIG_OS_SERIAL_DELAY_CLOSE=y
CONFIG_OS_SERIAL_RX_BUFSZ=64
CONFIG_OS_SERIAL_TX_BUFSZ=64

#
# posix serial
#
# CONFIG_OS_USING_POSIX_SERIAL is not set
# end of posix serial

#
# rtt uart
#
# CONFIG_OS_USING_RTT is not set
# end of rtt uart
# end of Serial

#
# SFLASH
#
# CONFIG_OS_SFLASH_SUPPORT is not set
# end of SFLASH

#
# SN
#
# CONFIG_OS_USING_SN is not set
# end of SN

#
# SPI
#
CONFIG_OS_USING_SPI=y
# CONFIG_OS_USING_QSPI is not set
# CONFIG_OS_USING_SPI_MSD is not set
# CONFIG_BSP_USING_ENC28J60 is not set
# CONFIG_BSP_USING_SDCARD is not set
# CONFIG_BSP_USING_NRF24L01 is not set
# CONFIG_OS_USING_SFUD is not set
# end of SPI

#
# Timer
#
# CONFIG_OS_USING_TIMER_DRIVER is not set
# end of Timer

#
# TinyUSB
#
# CONFIG_OS_USING_TINYUSB is not set
# end of TinyUSB

#
# Touch
#
# CONFIG_OS_USING_TOUCH is not set
# end of Touch

#
# USB
#
# CONFIG_OS_USING_USB_DEVICE is not set
# CONFIG_OS_USING_USB_HOST is not set
# end of USB

#
# WDG
#
# CONFIG_OS_USING_WDG is not set
# end of WDG
# end of Drivers

#
# Components
#

#
# DBoT
#
# CONFIG_OS_USING_DBOT is not set
# end of DBoT

#
# MicroPython
#
# CONFIG_PKG_USING_MICROPYTHON is not set
# end of MicroPython

#
# WWD
#
# CONFIG_OS_USING_WWD is not set
# end of WWD

#
# AMS
#
# CONFIG_PKG_USING_AMS is not set
# end of AMS

#
# Atest
#
# CONFIG_OS_USING_ATEST is not set
# end of Atest

#
# BLE
#
# CONFIG_OS_USING_BLE is not set
# end of BLE

#
# cJSON
#
# CONFIG_PKG_USING_CJSON is not set
# end of cJSON

#
# CLI
#
# CONFIG_OS_USING_CLI is not set
# end of CLI

#
# Cloud
#

#
# Aliyun
#
# CONFIG_PKG_USING_ALI_IOTKIT is not set
# end of Aliyun

#
# AWS
#
# CONFIG_PKG_USING_AWS_IOT is not set
# end of AWS

#
# Baidu
#
# CONFIG_BAIDUIOT is not set
# end of Baidu

#
# CTWing
#

#
# MQTT
#
# CONFIG_OS_USING_CTWING_MQTT is not set
# end of MQTT
# end of CTWing

#
# Huawei
#
# CONFIG_USING_HUAWEI_CLOUD_CONNECT is not set
# end of Huawei

#
# OneNET
#

#
# MQTT kit
#
# CONFIG_OS_USING_ONENET_MQTTS is not set
# end of MQTT kit

#
# NB-IoT kit
#
# CONFIG_OS_USING_ONENET_NBIOT is not set
# end of NB-IoT kit

#
# EDP
#
# CONFIG_OS_USING_ONENET_EDP is not set
# end of EDP
# end of OneNET
# end of Cloud

#
# CMS
#
CONFIG_CMS_LITE=y
# CONFIG_CMS_STD is not set

#
# CMS Connect
#
# CONFIG_USING_CMS_CONNECT is not set
# end of CMS Connect

#
# CMS ID
#
# CONFIG_CMS_USING_ID is not set
# end of CMS ID
# end of CMS

#
# Diagnose
#
# CONFIG_STACK_TRACE_EN is not set
# CONFIG_OS_USING_CPU_MONITER is not set
# CONFIG_OS_USING_WIRESHARK_DUMP is not set

#
# eCoreDump
#
# CONFIG_USING_ECORE_DUMP is not set
# end of eCoreDump

#
# Memory Monitor
#
# CONFIG_OS_USING_MEM_MONITOR is not set
# end of Memory Monitor
# end of Diagnose

#
# Dlog
#
# CONFIG_OS_USING_DLOG is not set
# end of Dlog

#
# Easyflash
#
# CONFIG_PKG_USING_EASYFLASH is not set
# end of Easyflash

#
# FileSystem
#
# CONFIG_OS_USING_VFS is not set
# end of FileSystem

#
# GUI
#
CONFIG_OS_GUI_DISP_DEV_NAME="lcd"
CONFIG_OS_GUI_INPUT_DEV_NAME="touch"
# CONFIG_OS_USING_GUI_LVGL is not set
# end of GUI

#
# Industrial
#

#
# CANOpen
#
# CONFIG_OS_USING_CANFESTIVAL is not set
# end of CANOpen

#
# CoDeSys
#
# CONFIG_OS_USING_CODESYS is not set
# end of CoDeSys

#
# ModBus
#
# CONFIG_OS_USING_UCMODBUS is not set
# end of ModBus
# end of Industrial

#
# IoTjs
#
# CONFIG_USING_IOTJS is not set
# end of IoTjs

#
# JerryScript
#
# CONFIG_USING_JERRYSCRIPT is not set
# end of JerryScript

#
# Network
#

#
# Acw
#
# CONFIG_NET_USING_ACW is not set
# end of Acw

#
# TCP/IP
#

#
# LwIP
#
# CONFIG_NET_USING_LWIP is not set
# end of LwIP
# end of TCP/IP

#
# Molink
#
# CONFIG_NET_USING_MOLINK is not set
# end of Molink

#
# Protocols
#

#
# CoAP
#
# CONFIG_COAP_Version is not set
# end of CoAP

#
# HTTP
#

#
# httpclient-v1.1.0
#
# CONFIG_NET_USING_HTTPCLIENT is not set
# end of httpclient-v1.1.0
# end of HTTP

#
# LWM2M
#

#
# LWM2M-v1.0.0
#
# CONFIG_NET_USING_LWM2M is not set
# end of LWM2M-v1.0.0
# end of LWM2M

#
# MQTT
#
# CONFIG_NET_USING_MQTT is not set
# end of MQTT

#
# Websocket
#
# CONFIG_NET_USING_WEBSOCKET_CLIENT is not set
# end of Websocket
# end of Protocols

#
# Socket
#
# CONFIG_NET_USING_BSD is not set
# end of Socket
# end of Network

#
# Iotivity
#
# CONFIG_PKG_USING_IOTIVITY is not set
# end of Iotivity

#
# Optparse
#
# CONFIG_TP_USING_OPTPARSE is not set
# end of Optparse

#
# OTA
#

#
# Fota by CMIOT
#
# CONFIG_FOTA_USING_CMIOT is not set
# end of Fota by CMIOT
# end of OTA

#
# Position
#
# CONFIG_OS_USING_ONEPOS is not set
# end of Position

#
# PowerManager
#
# CONFIG_OS_USING_POWER_MANAGER is not set
# end of PowerManager

#
# Ramdisk
#
# CONFIG_OS_USING_RAMDISK is not set
# end of Ramdisk

#
# Security
#
# CONFIG_SECURITY_USING_MBEDTLS is not set

#
# OneTLS
#
# CONFIG_SECURITY_USING_ONETLS is not set
# end of OneTLS
# end of Security

#
# Shell
#
CONFIG_OS_USING_SHELL=y
CONFIG_SHELL_TASK_NAME="shell"
CONFIG_SHELL_TASK_PRIORITY=20
CONFIG_SHELL_TASK_STACK_SIZE=2048
CONFIG_SHELL_USING_HISTORY=y
CONFIG_SHELL_HISTORY_LINES=5
CONFIG_SHELL_USING_DESCRIPTION=y
# CONFIG_SHELL_ECHO_DISABLE_DEFAULT is not set
CONFIG_SHELL_CMD_SIZE=80
CONFIG_SHELL_PROMPT_SIZE=256
CONFIG_SHELL_ARG_MAX=10
# CONFIG_SHELL_USING_AUTH is not set
# end of Shell

#
# SQL
#
# CONFIG_PKG_USING_SQLITE is not set
# end of SQL

#
# telnetd
#
# CONFIG_TELNET_SERVER is not set
# end of telnetd
# end of Components

#
# Debug
#
CONFIG_OS_DEBUG=y
# CONFIG_LOG_BUFF_SIZE_128 is not set
# CONFIG_LOG_BUFF_SIZE_192 is not set
CONFIG_LOG_BUFF_SIZE_256=y
# CONFIG_LOG_BUFF_SIZE_384 is not set
CONFIG_OS_LOG_BUFF_SIZE=256
# end of Debug
