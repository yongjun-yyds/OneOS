/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        board.c
 *
 * @brief       Initializes the CPU, System clocks, and Peripheral device
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <stdint.h>
#include <os_task.h>
#include <device.h>
#include <gd32vf103.h>
#include <gd32vf103_eclic.h>
#include <riscv_encoding.h>
#include <os_memory.h>
#include <os_clock.h>
#include <drv_gpio.h>
#include <oneos_config.h>
#include <board.h>

#define _SCB_BASE      (0xE000E010UL)
#define _SYSTICK_CTRL  (*(uint32_t *)(_SCB_BASE + 0x0))
#define _SYSTICK_LOAD  (*(uint32_t *)(_SCB_BASE + 0x4))
#define _SYSTICK_VAL   (*(uint32_t *)(_SCB_BASE + 0x8))
#define _SYSTICK_CALIB (*(uint32_t *)(_SCB_BASE + 0xC))
#define _SYSTICK_PRI   (*(uint8_t *)(0xE000ED23UL))

#ifdef OS_USING_PUSH_BUTTON
const struct push_button key_table[] = {
    {GET_PIN(B, 9), PIN_MODE_INPUT_PULLUP, PIN_IRQ_MODE_FALLING},
};

const int key_table_size = ARRAY_SIZE(key_table);

#endif

#ifdef OS_USING_LED
const led_t led_table[] = {
    {GET_PIN(A, 1), PIN_LOW},
};

const int led_table_size = ARRAY_SIZE(led_table);
#endif

#ifdef OS_USING_TINYUSB

#include <board.h>
#include <os_clock.h>
#include <tusb.h>

#include <drv_usb_regs.h>

void USBFS_IRQHandler(void)
{
    tud_int_handler(0);
}

#define USB_NO_VBUS_PIN

void usb_rcu_config(void);

void board_init(void)
{
    os_pin_mode(key_table[0].pin, key_table[0].mode);

    rcu_periph_clock_enable(RCU_GPIOA);
    rcu_periph_clock_enable(RCU_AF);

    /* USB D+ and D- pins don't need to be configured. */
    /* Configure VBUS Pin */
#ifndef USB_NO_VBUS_PIN
    gpio_init(GPIOA, GPIO_MODE_IN_FLOATING, GPIO_OSPEED_50MHZ, GPIO_PIN_9);
#endif

    /* This for ID line debug */
    //gpio_init(GPIOA, GPIO_MODE_AF_OD, GPIO_OSPEED_50MHZ, GPIO_PIN_10);

    /* Enable USB OTG clock */
    usb_rcu_config();

    /* Reset USB OTG peripheral */
    rcu_periph_reset_enable(RCU_USBFSRST);
    rcu_periph_reset_disable(RCU_USBFSRST);

    /* Configure USBFS IRQ */
    //ECLIC_Register_IRQ(USBFS_IRQn, ECLIC_NON_VECTOR_INTERRUPT,
    //                   ECLIC_POSTIVE_EDGE_TRIGGER, 3, 0, NULL);

    /* Retrieve otg core registers */
    usb_gr* otg_core_regs = (usb_gr*)(USBFS_REG_BASE + USB_REG_OFFSET_CORE);

#ifdef USB_NO_VBUS_PIN
    /* Disable VBUS sense*/
    otg_core_regs->GCCFG |= GCCFG_VBUSIG | GCCFG_PWRON | GCCFG_VBUSBCEN;
#else
    /* Enable VBUS sense via pin PA9 */
    otg_core_regs->GCCFG |= GCCFG_VBUSIG | GCCFG_PWRON | GCCFG_VBUSBCEN;
    otg_core_regs->GCCFG &= ~GCCFG_VBUSIG;
#endif

    /* Enable interrupts globaly */
    //__enable_irq();    
}

#endif
