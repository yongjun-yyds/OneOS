/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        dev_usart.c
 *
 * @brief       This file define the information of TIMER device
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include "drv_hwtimer.h"

#ifdef BSP_USING_ETIM
const struct fm33_timer_info etimer1_2_info = {
    .type     = TYPE_ETIM12,
    .grp1sel  = ETIMx_ETxINSEL_GRP1SEL_ET1_APBCLK,
    .mode     = TIMER_MODE_TIM,

    .tim_src  = {
        .inst = ETIM1,
        .irqn = ETIM1_IRQn,
        .clk  = ET1CLK,
    }, 
    .tim_irq  = {
        .inst = ETIM2,
        .irqn = ETIM2_IRQn,
        .clk  = ET2CLK,
    }, 
};
OS_HAL_DEVICE_DEFINE("TIMER_Type", "etimer1_2", etimer1_2_info);

const struct fm33_timer_info etimer3_4_info = {
    .type     = TYPE_ETIM34,
    .grp1sel  = ETIMx_ETxINSEL_GRP1SEL_ET3_APBCLK,
    .mode     = TIMER_MODE_TIM,

    .tim_src  = {
        .inst = ETIM3,
        .irqn = ETIM3_IRQn,
        .clk  = ET3CLK,
    }, 
    .tim_irq  = {
        .inst = ETIM4,
        .irqn = ETIM4_IRQn,
        .clk  = ET4CLK,
    }, 
};
OS_HAL_DEVICE_DEFINE("TIMER_Type", "etimer3_4", etimer3_4_info);
#endif

