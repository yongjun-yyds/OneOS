/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        dev_usart.c
 *
 * @brief       This file define the information of TIMER device
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include "drv_hwtimer.h"

#ifdef BSP_USING_ATIM
const struct fm33_timer_info atimer_info = {
    .inst  = ATIM,
    .index = 0,
    .irqn  = ATIM_IRQn,
    .type  = TYPE_ATIM,
    .mode  = TIMER_MODE_TIM,
    .bits  = 16,
};
OS_HAL_DEVICE_DEFINE("TIMER_Type", "atimer", atimer_info);
#endif

#ifdef BSP_USING_BSTIM32
const struct fm33_timer_info btimer32_info = {
    .inst  = BSTIM32,
    .index = 0,
    .irqn  = BSTIM_IRQn,
    .type  = TYPE_BSTIM32,
    .mode  = TIMER_MODE_TIM,
    .bits  = 32,
};
OS_HAL_DEVICE_DEFINE("TIMER_Type", "btimer32", btimer32_info);
#endif

#ifdef BSP_USING_BSTIM16
const struct fm33_timer_info btimer16_info = {
    .inst  = BSTIM16,
    .index = 1,
    .irqn  = BSTIM_IRQn,
    .type  = TYPE_BSTIM16,
    .mode  = TIMER_MODE_TIM,
    .bits  = 16,
};
OS_HAL_DEVICE_DEFINE("TIMER_Type", "btimer16", btimer16_info);
#endif

#ifdef BSP_USING_GPTIM0
const struct fm33_timer_info gtimer0_info = {
    .inst  = GPTIM0,
    .index = 0,
    .irqn  = GPTIM01_IRQn,
    .type  = TYPE_GPTIM0,
    .mode  = TIMER_MODE_TIM,
    .bits  = 16,
};
OS_HAL_DEVICE_DEFINE("TIMER_Type", "gtimer0", gtimer0_info);
#endif

#ifdef BSP_USING_GPTIM1
const struct fm33_timer_info gtimer1_info = {
    .inst  = GPTIM1,
    .index = 1,
    .irqn  = GPTIM01_IRQn,
    .type  = TYPE_GPTIM1,
    .mode  = TIMER_MODE_TIM,
    .bits  = 16,
};
OS_HAL_DEVICE_DEFINE("TIMER_Type", "gtimer1", gtimer1_info);
#endif

#ifdef BSP_USING_GPTIM2
const struct fm33_timer_info gtimer2_info = {
    .inst  = GPTIM2,
    .index = 2,
    .irqn  = GPTIM2_IRQn,
    .type  = TYPE_GPTIM2,
    .mode  = TIMER_MODE_TIM,
    .bits  = 16,
};
OS_HAL_DEVICE_DEFINE("TIMER_Type", "gtimer2", gtimer2_info);
#endif

#ifdef BSP_USING_LPTIM32
const struct fm33_timer_info lptimer32_info = {
    .inst  = LPTIM32,
    .index = 0,
    .irqn  = LPTIMx_IRQn,
    .type  = TYPE_LPTIM32,
    .mode  = TIMER_MODE_TIM,
    .bits  = 32,
};
OS_HAL_DEVICE_DEFINE("TIMER_Type", "lptimer32", lptimer32_info);
#endif

#ifdef BSP_USING_LPTIM16
const struct fm33_timer_info lptimer16_info = {
    .inst  = LPTIM16,
    .index = 1,
    .irqn  = LPTIMx_IRQn,
    .type  = TYPE_LPTIM16,
    .mode  = TIMER_MODE_TIM,
    .bits  = 16,
};
OS_HAL_DEVICE_DEFINE("TIMER_Type", "lptimer16", lptimer16_info);
#endif

