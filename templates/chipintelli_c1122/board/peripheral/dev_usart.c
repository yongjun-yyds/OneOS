/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        dev_usart.c
 *
 * @brief       This file implements usart driver for cm32
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include "drv_usart.h"
#include <oneos_config.h>

//#define BSP_USING_UART0

#ifdef BSP_USING_USART0
struct ci_usart_info uart0_info = {
    .tx_pinpad = UART0_TX_PAD,
    .tx_resue  = SECOND_FUNCTION,

    .rx_pinpad = UART0_RX_PAD,
    .rx_resue  = SECOND_FUNCTION,

    .husart     = UART0,
    .usart_irqn = UART0_IRQn,

#ifdef UART1_USING_DMA
    .dma_enable = 1,
#else
    .dma_enable = 0,
#endif
    .rx_channel    = DMACChannel0,
    .tx_dma        = DMAC_Peripherals_UART0_TX,
    .rx_channel    = DMACChannel0,
    .rx_dma        = DMAC_Peripherals_UART0_RX,
    .dma_uart_addr = UART0_DMA_ADDR,
};
OS_HAL_DEVICE_DEFINE("UART_TypeDef", "uart0", uart0_info);
#endif

#define BSP_USING_USART1
#ifdef BSP_USING_USART1
struct ci_usart_info uart1_info = {
    .tx_pinpad = I2C0_SCL_PAD,
    .tx_resue  = THIRD_FUNCTION,

    .rx_pinpad = I2C0_SDA_PAD,
    .rx_resue  = THIRD_FUNCTION,

    .husart     = UART1,
    .usart_irqn = UART1_IRQn,

#ifdef UART1_USING_DMA
    .dma_enable = 1,
#else
    .dma_enable = 0,
#endif
    .rx_channel    = DMACChannel0,
    .tx_dma        = DMAC_Peripherals_UART1_TX,
    .rx_channel    = DMACChannel0,
    .rx_dma        = DMAC_Peripherals_UART1_RX,
    .dma_uart_addr = UART1_DMA_ADDR,
};
OS_HAL_DEVICE_DEFINE("UART_TypeDef", "uart1", uart1_info);
#endif
