/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        dev_timer.c
 *
 * @brief       This file implements hwtimer driver for cm32
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <drv_hwtimer.h>

#ifdef BSP_USING_TIM0
static const struct ci_timer tim0_info = {
    .mode   = TIMER_MODE_TIM,
    .handle = TIMER0,
};
OS_HAL_DEVICE_DEFINE("timer_base_t", "tim0", tim0_info);
#endif

#ifdef BSP_USING_TIM1
static const struct ci_timer tim1_info = {
    .mode   = TIMER_MODE_TIM,
    .handle = TIMER1,
};
OS_HAL_DEVICE_DEFINE("timer_base_t", "tim1", tim1_info);
#endif

#ifdef BSP_USING_TIM2
static const struct ci_timer tim2_info = {
    .mode   = TIMER_MODE_TIM,
    .handle = TIMER2,
};
OS_HAL_DEVICE_DEFINE("timer_base_t", "tim2", tim2_info);
#endif

#ifdef BSP_USING_TIM3
static const struct ci_timer tim3_info = {
    .mode   = TIMER_MODE_TIM,
    .handle = TIMER3,
};
OS_HAL_DEVICE_DEFINE("timer_base_t", "tim3", tim3_info);
#endif
