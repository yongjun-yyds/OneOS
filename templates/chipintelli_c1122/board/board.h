/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        board.h
 *
 * @brief       Board resource definition
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __BOARD_H__
#define __BOARD_H__

#include <drv_cfg.h>
#include <platform_config.h>
#include <ci112x_system.h>
#include <ci112x_core_eclic.h>
#include <ci112x_scu.h>
#include "user_config.h"
//
#ifdef __cplusplus
extern "C" {
#endif

#define CI_FLASH_START_ADRESS ((uint32_t)0x00000000)
#define CI_FLASH_SIZE         (4 * 1024 * 1024)
#define CI_FLASH_END_ADDRESS  ((uint32_t)(CI_FLASH_START_ADRESS + CI_FLASH_SIZE))

#define CI_SRAM1_SIZE  (512)
#define CI_SRAM1_START (0x1FF00000)
#define CI_SRAM1_END   (CI_SRAM1_START + CI_SRAM1_SIZE * 1024)

#if defined(__CC_ARM) || defined(__CLANG_ARM)
extern int Image$$RW_IRAM1$$ZI$$Limit;
#define HEAP_BEGIN (&Image$$RW_IRAM1$$ZI$$Limit)
#elif __ICCARM__
#pragma section = "HEAP"
#define HEAP_BEGIN (__segment_end("HEAP"))
#else
extern int __FREERTOSHEAP_SIZE;
#define ONEOS_HEAP_SIZE ((int)&__FREERTOSHEAP_SIZE)
extern int __FREERTOSHEAP_START;
#define HEAP_BEGIN      (&__FREERTOSHEAP_START)

#define HEAP_END ((char *)(HEAP_BEGIN) + (ONEOS_HEAP_SIZE))
#endif

//#define HEAP_END CI_SRAM1_END

#ifdef OS_USING_PUSH_BUTTON
extern const struct push_button key_table[];
extern const int                key_table_size;
#endif

#ifdef OS_USING_LED
extern const led_t led_table[];
extern const int   led_table_size;
#endif

#ifdef __cplusplus
}
#endif
//
#endif
