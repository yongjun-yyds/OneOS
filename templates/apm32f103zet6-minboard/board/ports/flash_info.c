static struct onchip_flash_info flash_info = {
    .start_addr = APM32_FLASH_START_ADRESS,
    .capacity   = APM32_FLASH_SIZE,
    .block_size = APM32_FLASH_PAGE_SIZE,
    .page_size  = APM32_FLASH_PAGE_SIZE,
};
OS_HAL_DEVICE_DEFINE("Onchip_Flash_Type", "onchip_flash", flash_info);
