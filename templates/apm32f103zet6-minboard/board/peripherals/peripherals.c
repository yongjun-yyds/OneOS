#include "oneos_config.h"
#include <driver.h>
#include <bus/bus.h>

#ifdef BSP_USING_USART
#include "drv_usart.h"

const struct apm32_usart_info usart1_info = {
    .tx_pin_port = GPIOA,
    .tx_pin_info =
        {
            .pin   = GPIO_PIN_9,
            .speed = GPIO_SPEED_50MHz,
            .mode  = GPIO_MODE_AF_PP,
        },
    .rx_pin_port = GPIOA,
    .rx_pin_info =
        {
            .pin   = GPIO_PIN_10,
            .speed = GPIO_SPEED_50MHz,
            .mode  = GPIO_MODE_IN_PU,
        },

    .gpio_PeriphClock  = RCM_EnableAPB2PeriphClock,
    .gpio_Periph       = RCM_APB2_PERIPH_GPIOA,
    .usart_PeriphClock = RCM_EnableAPB2PeriphClock,
    .usart_Periph      = RCM_APB2_PERIPH_USART1,

    .usart_def_cfg =
        {
            .baudRate     = 115200,
            .wordLength   = USART_WORD_LEN_8B,
            .stopBits     = USART_STOP_BIT_1,
            .parity       = USART_PARITY_NONE,
            .mode         = USART_MODE_TX_RX,
            .hardwareFlow = USART_HARDWARE_FLOW_NONE,
        },

    .husart     = USART1,
    .irq_type   = USART1_IRQn,
    .irq_PrePri = 1,
    .irq_SubPri = 0,
};

OS_HAL_DEVICE_DEFINE("USART_HandleTypeDef", "usart1", usart1_info);

const struct apm32_usart_info usart2_info = {
    .tx_pin_port = GPIOA,
    .tx_pin_info =
        {
            .pin   = GPIO_PIN_2,
            .speed = GPIO_SPEED_50MHz,
            .mode  = GPIO_MODE_AF_PP,
        },
    .rx_pin_port = GPIOA,
    .rx_pin_info =
        {
            .pin   = GPIO_PIN_3,
            .speed = GPIO_SPEED_50MHz,
            .mode  = GPIO_MODE_IN_PU,
        },

    .gpio_PeriphClock  = RCM_EnableAPB2PeriphClock,
    .gpio_Periph       = RCM_APB2_PERIPH_GPIOA,
    .usart_PeriphClock = RCM_EnableAPB1PeriphClock,
    .usart_Periph      = RCM_APB1_PERIPH_USART2,

    .usart_def_cfg =
        {
            .baudRate     = 115200,
            .wordLength   = USART_WORD_LEN_8B,
            .stopBits     = USART_STOP_BIT_1,
            .parity       = USART_PARITY_NONE,
            .mode         = USART_MODE_TX_RX,
            .hardwareFlow = USART_HARDWARE_FLOW_NONE,
        },

    .husart     = USART2,
    .irq_type   = USART2_IRQn,
    .irq_PrePri = 1,
    .irq_SubPri = 0,
};

OS_HAL_DEVICE_DEFINE("USART_HandleTypeDef", "usart2", usart2_info);
#endif

#ifdef BSP_USING_TIM
static const struct mm32_timer_info tim1_info = {
    .htim                                                 = TIM1,
    .tim_clk                                              = RCC_APB2ENR_TIM1,
    .NVIC_InitStructure.NVIC_IRQChannelSubPriority        = 3,
    .NVIC_InitStructure.NVIC_IRQChannel                   = TIM1_UP_IRQn,
    .NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 3,
    .NVIC_InitStructure.NVIC_IRQChannelCmd                = ENABLE,
};
OS_HAL_DEVICE_DEFINE("TIM_TypeDef", "tim1", tim1_info);

static const struct mm32_timer_info tim2_info = {
    .htim                                                 = TIM2,
    .tim_clk                                              = RCC_APB1ENR_TIM2,
    .NVIC_InitStructure.NVIC_IRQChannelSubPriority        = 3,
    .NVIC_InitStructure.NVIC_IRQChannel                   = TIM2_IRQn,
    .NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 3,
    .NVIC_InitStructure.NVIC_IRQChannelCmd                = ENABLE,
};
OS_HAL_DEVICE_DEFINE("TIM_TypeDef", "tim2", tim2_info);
#endif

#ifdef BSP_USING_ADC
static const struct mm32_adc_pin adc1_pin[] = {
    ADC_PIN_SET(A, 6, ADC_Samctl_55_5),
    ADC_PIN_SET(A, 7, ADC_Samctl_55_5),
};
static const struct mm32_adc_info adc1_info = {
    .hadc                               = ADC1,
    .init_struct.ADC_Resolution         = ADC_Resolution_12b,
    .init_struct.ADC_PRESCARE           = ADC_PCLK2_PRESCARE_16,
    .init_struct.ADC_Mode               = ADC_CR_IMM,
    .init_struct.ADC_ContinuousConvMode = DISABLE,
    .init_struct.ADC_ExternalTrigConv   = ADC_ExternalTrigConv_T1_CC1,
    .init_struct.ADC_DataAlign          = ADC_DataAlign_Right,
    .adc_rcc_clk                        = RCC_APB2ENR_ADC1,
    .adc_rcc_clkcmd                     = RCC_APB2PeriphClockCmd,
    .gpio_rcc_clkcmd                    = RCC_AHBPeriphClockCmd,
    .ref_low                            = 0,
    .ref_high                           = 3300, /* ref 0 - 3.3v */
    .pin                                = &adc1_pin[0],
    .pin_num                            = sizeof(adc1_pin) / sizeof(adc1_pin[0]),
};
OS_HAL_DEVICE_DEFINE("ADC_TypeDef", "adc1", adc1_info);
#endif
