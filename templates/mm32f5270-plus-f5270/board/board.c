/* board.c */
#include "board.h"

#include <drv_gpio.h>

#if defined(OS_USING_LED)
const led_t led_table[] = {
    {GET_PIN(2, 4), PIN_LOW},
    {GET_PIN(2, 5), PIN_LOW},
};

const int led_table_size = sizeof(led_table) / sizeof(led_table[0]);
#endif

/* EOF. */
