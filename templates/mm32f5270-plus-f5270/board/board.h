/* board.h */

#ifndef __BOARD_H__
#define __BOARD_H__

#include <hal_common.h>
#include <oneos_config.h>
#include <drv_cfg.h>
#include <device.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus. */

#define SOC_MODEL "MM32F5270"

#define MM32_SRAM_START (0x30000000)
#define MM32_SRAM_SIZE  (0x00020000)
#define MM32_SRAM_END   (MM32_SRAM_START + MM32_SRAM_SIZE)

#define MM32_FLASH_START_ADDR   (0x08000000)
#define MM32_FLASH_SIZE         (0x00040000)
#define MM32_FLASH_BLOCK_SIZE   (0x00000400)
#define MM32_FLASH_END_ADDRESS  (MM32_FLASH_START_ADDR + MM32_FLASH_SIZE)

#if defined(__CC_ARM) || defined(__CLANG_ARM)
extern int Image$$ARM_LIB_HEAP$$ZI$$Base;
extern int Image$$ARM_LIB_HEAP$$ZI$$Limit;
#define HEAP_BEGIN (&Image$$ARM_LIB_HEAP$$ZI$$Base)
#define HEAP_END   (&Image$$ARM_LIB_HEAP$$ZI$$Limit)
#elif __ICCARM__
#pragma section = "HEAP"
#define HEAP_BEGIN (__segment_end("HEAP"))
#else
extern int __bss_end;
#define HEAP_BEGIN (&__bss_end)
#endif

#if defined(OS_USING_PUSH_BUTTON)
extern const struct push_button key_table[];
extern const int                key_table_size;
#endif

#if defined(OS_USING_LED)
extern const led_t led_table[];
extern const int   led_table_size;
#endif

#if defined(OS_USING_SN)
extern const int board_no_pin_tab[];
extern const int board_no_pin_tab_size;
extern const int slot_no_pin_tab[];
extern const int slot_no_pin_tab_size;
#endif

#ifdef __cplusplus
}
#endif /* __cplusplus. */

#endif /* __BOARD_H__. */
