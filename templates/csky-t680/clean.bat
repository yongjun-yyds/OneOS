@echo off

del *.mk
del *.txt
del *.cdkws
del *.bin
del *.pyc
del *.log
del *.db
del .sconsign.*
del Makefile
del .*.old
del cconfig.h

rmdir /q /s Lst
rmdir /q /s Obj
rmdir /q /s build
rmdir /q /s .cdk
rmdir /q /s out
rmdir /q /s .cdk
rmdir /q /s __pycache__
rmdir /q /s __workspace_pack__

