#include <oneos_config.h>
#if defined(OS_USING_SHELL) && defined(OS_USING_I2C)
#include <i2c/i2c.h>
#include <shell.h>
#include <string.h>
#define EEP_ADDR             0x50
uint8_t buf[10];
uint8_t addr[2] = {0x00,0x20};

os_err_t eeprom_iic_write(struct os_i2c_bus_device *bus, uint8_t *write_addr, uint8_t *data, uint32_t number)
{
    struct os_i2c_msg msg;
    os_size_t result;
    memcpy(buf, write_addr, 2);
    memcpy(&buf[2], data, number);

    msg.addr     = EEP_ADDR;
    msg.flags    = OS_I2C_WR;
    msg.buf      = buf;
    msg.len      = 2+number;

    result = os_i2c_transfer(bus, &msg, 1);
    return result;
}

os_err_t eeprom_iic_read(struct os_i2c_bus_device *bus, uint8_t *read_addr, uint8_t *buf,uint32_t len)
{
    struct os_i2c_msg msg[2];

    msg[0].addr     = EEP_ADDR;
    msg[0].flags    = OS_I2C_WR;
    msg[0].buf      = (uint8_t *) read_addr;
    msg[0].len      =  2;

    msg[1].addr     = EEP_ADDR;
    msg[1].flags    = OS_I2C_RD;
    msg[1].buf      = buf;
    msg[1].len      = len;

    return os_i2c_transfer(bus, msg, 2);
}
 
 
void i2c_test(int argc, char *argv[])
{
    os_err_t  ret;
    char     *name;
    struct os_i2c_bus_device *i2c_bus;
    uint8_t data[8] = {0x20, 1, 2, 3, 0x53, 5, 6, 7};

    if (argc != 2)
    {
        os_kprintf("usage: i2c_test <i2cx> \r\n");
        return;
    }

    name = argv[1];
        
    i2c_bus = os_i2c_bus_device_find(name);
    if (i2c_bus == OS_NULL)
    {
        os_kprintf("bus not find\n");
    }

    ret =  eeprom_iic_write(i2c_bus, addr, data, 8);
    if (ret == 0)
    {
        os_kprintf("i2c write error\r\n");
        return;
    }
    os_kprintf("write data:");
    for(int i=0;i<8;i++)
        os_kprintf("0x%02x ", data[i]);
    os_kprintf("\r\n");
    
    os_task_msleep(50);
    
    memset(data, 0, 8);
    ret = eeprom_iic_read(i2c_bus, addr, data,8);
    if (ret == 0)
    {
        os_kprintf("i2c read error\r\n");
        return;
    }
    os_kprintf("read data:");
    for(int i=0;i<8;i++)
        os_kprintf("%02x", data[i]);
    os_kprintf("\r\n");
    os_task_msleep(50); 
}

SH_CMD_EXPORT(i2c_test, i2c_test, "i2c_test");

#endif

