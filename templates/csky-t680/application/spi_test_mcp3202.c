#include "spi/spi.h"
#include "shell.h"
#include <tih/scu.h>

#define MCP_BUS_NAME "mcp3202_spi"

void spi_test(int argc, char *argv[])
{
    char *name;
    struct os_spi_message message;
    uint8_t send_buff[3] = {0x01,0xA0,0x00};//send_buff[2]:0xa0:ch0;0xe0,ch1
    uint8_t recv_buff[3];
    message.send_buf   = send_buff;
    message.recv_buf   = recv_buff;
    message.length     = 3;
    message.cs_take    = 1;
    message.cs_release = 1;
    message.next       = NULL;

    uint16_t adc_num = 0,i = 0;
    struct os_spi_device *os_spi_device;

    /* config spi */
    struct os_spi_configuration cfg;
    cfg.data_width = 8;
    cfg.mode       = OS_SPI_MODE_3 | OS_SPI_MSB; /* SPI Compatible Modes 0 and 3 */
    cfg.max_hz     = 1000000;
    
    if (argc != 2)
    {
        os_kprintf("usage: spi_test <spix> \r\n");
        return;
    }

    name = argv[1];
    
#ifndef CONFIG_MPW
        //scu_pin_mux_set(SCU_PIN_MUX_MODE1_1, SCU_PIN_MUX_MODE2_S_U1);
#endif

    os_hw_spi_device_attach(name, MCP_BUS_NAME, -1);

    os_spi_device = (struct os_spi_device *)os_device_find(MCP_BUS_NAME);
    if (os_spi_device == OS_NULL)
    {
        os_kprintf("spi device %s not found!\r\n", MCP_BUS_NAME);
        return ;
    }

    os_spi_configure(os_spi_device, &cfg);
    os_task_msleep(100);
    for (i=0;i<10;i++)    //ch0
    {
        os_spi_transfer_message(os_spi_device, &message);
        adc_num = ((recv_buff[1] << 8) | recv_buff[2]) & 0x0fff;
        os_kprintf("SPI read adc ch0: %d;\r\n",adc_num);
        adc_num = 0;
		recv_buff[0] = 0;
		recv_buff[1] = 0;
		recv_buff[2] = 0;
		os_task_msleep(1000);
    }
    
    send_buff[1] = 0xE0;    //ch1
    for (i=0;i<10;i++)
    {
        os_spi_transfer_message(os_spi_device, &message);
        adc_num = ((recv_buff[1] << 8) | recv_buff[2]) & 0x0fff;
        os_kprintf("SPI read adc ch1: %d;\r\n",adc_num);
        adc_num = 0;
		recv_buff[0] = 0;
		recv_buff[1] = 0;
		recv_buff[2] = 0;
		os_task_msleep(1000);
    }
}

SH_CMD_EXPORT(spi_test, spi_test, "spi_test");

