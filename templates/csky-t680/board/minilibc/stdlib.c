

#include <oneos_config.h>

//#ifdef OS_USING_NEWLIB_ADAPTER
#include <stdlib.h>
#include <sysdep.h>
#include <os_memory.h>
#ifdef OS_USING_HEAP
void __fast *malloc(size_t size)
{
    return os_malloc(size);
}

void __fast free(void *ptr)
{
    os_free(ptr);
}

void __fast *realloc(void *ptr, size_t size)
{
    return os_realloc(ptr, size);
}

void __fast *calloc(size_t nelem, size_t elsize)
{
    return os_calloc(nelem, elsize);
}
#endif

//#endif


