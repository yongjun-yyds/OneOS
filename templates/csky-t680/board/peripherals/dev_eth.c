#include "oneos_config.h"
#include <driver.h>
#include <bus/bus.h>
#if 0
#ifdef BSP_USING_ETH
#include "drv_eth.h"


extern void __fast eth_isr(int vector, void *param);
extern const phy_ops_t rtl8211_phy_ops;

#ifdef OS_USING_ETH0
#if 0
typedef struct
{
  uint32_t            gmac_id;                     /* GMAC0 or GMAC1 */
  uint8_t             dev_addr[OS_NET_MAC_LENGTH]; /* interface address info, hw address */

  uint32_t            phy_addr;               /* phy addr */
  const phy_ops_t       *phy;                    /* phy control functions */
  link_status_t          link_status;

  uint8_t             tx_desc_num;
  uint32_t           *tx_pbuf_record;         /* TX pbuf recorder */
  uint8_t             rx_desc_num;
  uint32_t           *rx_pbuf_record;         /* RX pbuf recorder */
} ETH_HandleTypeDef;
#endif

CK_ETH_HandleTypeDef csky_eth0 =
{
    .gmac_id     = GMAC0,
    .dev_addr    = {0x00,0x80,0xE1,0xD1,0x01,0x01},
    .phy_addr    = 0,
    .phy         = &rtl8211_phy_ops,
    .link_status = { .linked = FALSE },
    .tx_desc_num    = ETH0_TX_QUEUE_ENTRIES,
    .rx_desc_num    = ETH0_RX_QUEUE_ENTRIES,
    .tx_pbuf_record = eth0_tx_pbuf_record,
    .rx_pbuf_record = eth0_rx_pbuf_record,

    .irq_num        = GMAC0_IRQ_NUM,
    .irq_handler    = eth_isr,
};
//OS_HAL_DEVICE_DEFINE("CK_ETH_HandleTypeDef", "eth0", csky_eth0);

#endif

#ifdef OS_USING_ETH1
CK_ETH_HandleTypeDef csky_eth1 =
{
    .gmac_id        = GMAC1,
    .dev_addr       = {0x00,0x80,0xE1,0xD1,0x01,0x03},
    .phy_addr       = 0,
    .phy            = &rtl8211_phy_ops,
    .link_status    = {.linked    = FALSE },
    .tx_desc_num    = ETH0_TX_QUEUE_ENTRIES,
    .rx_desc_num    = ETH0_RX_QUEUE_ENTRIES,
    .tx_pbuf_record = eth0_tx_pbuf_record,
    .rx_pbuf_record = eth0_rx_pbuf_record,

    .irq_num        = GMAC1_IRQ_NUM,
    .irq_handler    = eth_isr,
};
//OS_HAL_DEVICE_DEFINE("CK_ETH_HandleTypeDef", "eth1", csky_eth1);

#endif
#endif
#endif