#include <oneos_config.h>
#include <bus/bus.h>

#ifdef OS_USING_SPI

#include "drv_spi.h"

static ck_spi_info_t spi0_info = {
    .t680_spi = {SPI_WORD_SIZE_8BIT, SPI_ENDIAN_LSB, SPI_CPOL_HIGH, SPI_CPHA_2EDGE, SPI_SCK_DIV_2},
    .port_num = 0,
};

OS_HAL_DEVICE_DEFINE("SPI_Type", "spi0", spi0_info);

#endif
