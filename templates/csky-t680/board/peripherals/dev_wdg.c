#include <oneos_config.h>
#include <bus/bus.h>
#ifdef OS_USING_WDG
#include <tih/wdt.h>

static wdt_config_t wdt_info = {
    .seconds = 5,                /* 5000*ms */
    .signals = WDT_SIGNAL_RESET, /* time out reset cpu */
};

OS_HAL_DEVICE_DEFINE("WDT_InitStructure", "wdg", wdt_info);

#endif
