#include <oneos_config.h>
#ifdef OS_USING_TIMER_DRIVER
#include <drv_hwtimer.h>
#include <bus/bus.h>

#ifdef BSP_USING_TIM0
static csky_timer_info_t timer0_info = {
    .tim_num             = TIMER0,
    .tim_cfg.us          = 100000,
    .tim_cfg.reload_mode = TIMER_LOAD_AUTO,
    .tim_cfg.work_mode   = TIMER_MODE_TIMER,
    .tim_cfg.pwm_ratio   = 0,
};
OS_HAL_DEVICE_DEFINE("Timer_InitStructure", "tim0", timer0_info);
#endif

#ifdef BSP_USING_TIM1
static csky_timer_info_t timer1_info = {
    .tim_num             = TIMER1,
    .tim_cfg.us          = 100000,    // 100ms
    .tim_cfg.reload_mode = TIMER_LOAD_AUTO,
    .tim_cfg.work_mode   = TIMER_MODE_TIMER,
    .tim_cfg.pwm_ratio   = 0,
};
OS_HAL_DEVICE_DEFINE("Timer_InitStructure", "tim1", timer1_info);
#endif

#ifdef BSP_USING_TIM2
static csky_timer_info_t timer2_info = {
    .tim_num             = TIMER1,
    .tim_cfg.us          = 100000,    // 100ms
    .tim_cfg.reload_mode = TIMER_LOAD_AUTO,
    .tim_cfg.work_mode   = TIMER_MODE_TIMER,
    .tim_cfg.pwm_ratio   = 0,
};
OS_HAL_DEVICE_DEFINE("Timer_InitStructure", "tim2", timer2_info);
#endif

#ifdef BSP_USING_TIM3
static csky_timer_info_t timer3_info = {
    .tim_num             = TIMER1,
    .tim_cfg.us          = 100000,    // 100ms
    .tim_cfg.reload_mode = TIMER_LOAD_AUTO,
    .tim_cfg.work_mode   = TIMER_MODE_TIMER,
    .tim_cfg.pwm_ratio   = 0,
};
OS_HAL_DEVICE_DEFINE("Timer_InitStructure", "tim3", timer3_info);
#endif

#ifdef BSP_USING_TIM4
static csky_timer_info_t timer4_info = {
    .tim_num             = TIMER1,
    .tim_cfg.us          = 100000,    // 100ms
    .tim_cfg.reload_mode = TIMER_LOAD_AUTO,
    .tim_cfg.work_mode   = TIMER_MODE_TIMER,
    .tim_cfg.pwm_ratio   = 0,
};
OS_HAL_DEVICE_DEFINE("Timer_InitStructure", "tim4", timer4_info);
#endif

#ifdef BSP_USING_TIM5
static csky_timer_info_t timer5_info = {
    .tim_num             = TIMER5,
    .tim_cfg.us          = 100000,    // 100ms
    .tim_cfg.reload_mode = TIMER_LOAD_AUTO,
    .tim_cfg.work_mode   = TIMER_MODE_TIMER,
    .tim_cfg.pwm_ratio   = 0,
};
OS_HAL_DEVICE_DEFINE("Timer_InitStructure", "tim5", timer5_info);
#endif

#endif
