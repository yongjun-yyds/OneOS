import os
import sys
import shutil

CUR_BOARD_PATH = os.getcwd()
PRODUCT_BIN_PATH = CUR_BOARD_PATH + '/board/script/config'
PRODUCT_EXE = PRODUCT_BIN_PATH + '/product.exe'
BOOT_IMG_PATH = CUR_BOARD_PATH + '/board/script/bootimgs'
OBJCOPY = "riscv64-unknown-elf-objcopy"
MK_GENERATED_IMGS_PATH = CUR_BOARD_PATH + "/generated"
MK_GRN_DATA_PATH = MK_GENERATED_IMGS_PATH + '/data'
ELF_PATH = CUR_BOARD_PATH + '/out'
OBJ_PATH = CUR_BOARD_PATH + '/Obj'
#ELF_NAME = "oneos.elf"


if __name__ == '__main__':
    ELF_NAME = sys.argv[1]

    if os.path.exists(OBJ_PATH):
        shutil.rmtree(OBJ_PATH)

    os.mkdir(OBJ_PATH)
    src_elf = ELF_PATH + '/' + ELF_NAME
    det_elf = OBJ_PATH + '/' + ELF_NAME
    if os.path.exists(src_elf):
        shutil.copyfile(src_elf, det_elf)
    else:
        print(src_elf + ' is not found\n')
        exit(0)

    if os.path.exists(MK_GENERATED_IMGS_PATH):
        shutil.rmtree(MK_GENERATED_IMGS_PATH)

    os.mkdir(MK_GENERATED_IMGS_PATH)
    os.mkdir(MK_GRN_DATA_PATH)

    print("[INFO] Generated output files ...\r\n")

    # Boot
    mtb_src_path = BOOT_IMG_PATH + '/mtb'
    mtb_det_path = MK_GRN_DATA_PATH + '/mtb'
    if os.path.exists(mtb_src_path):
        shutil.copyfile(mtb_src_path, mtb_det_path)

    boot_src_path = BOOT_IMG_PATH + '/boot'
    boot_det_path = MK_GRN_DATA_PATH + '/boot'

    if os.path.exists(boot_src_path):
        shutil.copyfile(boot_src_path, boot_det_path)


    config_yaml_src_path = PRODUCT_BIN_PATH + '/config.yaml'
    config_yaml_det_path = MK_GRN_DATA_PATH + '/config.yaml'
    if os.path.exists(config_yaml_src_path):
        shutil.copyfile(config_yaml_src_path, config_yaml_det_path)

    objcopy_action = OBJCOPY + ' -O binary ./out/oneos.elf ' + \
        MK_GENERATED_IMGS_PATH + '/data/prim'

    product_action_1 = PRODUCT_EXE + ' image ' + \
        MK_GENERATED_IMGS_PATH + '/images.zip -i ' + MK_GRN_DATA_PATH + ' -l -p'

    product_action_2 = PRODUCT_EXE + ' image ' + \
        MK_GENERATED_IMGS_PATH + '/images.zip -e ' + MK_GENERATED_IMGS_PATH + ' -x'

    os.system(objcopy_action)
    os.system(product_action_1)
    os.system(product_action_2)
