/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        dev_usart.c
 *
 * @brief       This file implements usart driver for cm32
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include "drv_usart.h"
#include <oneos_config.h>

#ifdef BSP_USING_USART0
struct csi_uart csi_uart0 = {0};

struct ch_usart_info uart0_info = {
    .uart_periph = &csi_uart0,
    .idex        = 0,
    .gpio_tx     = PA23,
    .tx_pin_func = PA23_UART0_TX,
    .gpio_rx     = PA24,
    .rx_pin_func = PA24_UART0_RX,
};
OS_HAL_DEVICE_DEFINE("csi_uart_t", "uart0", uart0_info);
#endif

#ifdef BSP_USING_USART1
struct csi_uart csi_uart1 = {0};

struct ch_usart_info uart1_info = {
    .uart_periph = &csi_uart1,
    .idex        = 1,
    .gpio_tx     = PA28,
    .tx_pin_func = PA28_UART1_TX,
    .gpio_rx     = PA27,
    .rx_pin_func = PA27_UART1_RX,
};
OS_HAL_DEVICE_DEFINE("csi_uart_t", "uart1", uart1_info);
#endif
