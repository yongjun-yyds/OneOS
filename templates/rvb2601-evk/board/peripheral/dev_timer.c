#include "oneos_config.h"
#include <driver.h>
#include <bus/bus.h>
#include <oneos_config.h>
#ifdef BSP_USING_TIM
#include "drv_hwtimer.h"
#endif

#ifdef BSP_USING_TIM

static struct csi_timer tim2_info = {
    .dev.idx = 2,
};
OS_HAL_DEVICE_DEFINE("csi_timer", "tim2", tim2_info);

static struct csi_timer tim3_info = {
    .dev.idx = 3,
};
OS_HAL_DEVICE_DEFINE("csi_timer", "tim3", tim3_info);

static struct csi_timer tim4_info = {
    .dev.idx = 4,
};
OS_HAL_DEVICE_DEFINE("csi_timer", "tim4", tim4_info);

#endif
