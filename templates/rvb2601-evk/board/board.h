/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        board.h
 *
 * @brief       Board resource definition
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __BOARD_HEAP_H__
#define __BOARD_HEAP_H__

#include <oneos_config.h>
#include <os_assert.h>
#include <os_types.h>
#include <drv_cfg.h>

#ifdef __cplusplus
extern "C" {
#endif

#define TOGGLE_SP() asm("csrrw sp,mscratch,sp")

#define CH32_SRAM_SIZE  (190)    // total 192k,irq stack used last 2k
#define CH32_SRAM_START (0x20000000)
#define CH32_SRAM_END   (CH32_SRAM_START + CH32_SRAM_SIZE * 1024)

#define CH32_FLASH_START_ADRESS ((uint32_t)0x00000000)
#define CH32_FLASH_SIZE         (256 * 1024)
#define CH32_FLASH_END_ADDRESS  ((uint32_t)(CH32_FLASH_START_ADRESS + CH32_FLASH_SIZE))

#if defined(__CC_ARM) || defined(__CLANG_ARM)
extern int Image$$RW_IRAM1$$ZI$$Limit;
#define HEAP_BEGIN (&Image$$RW_IRAM1$$ZI$$Limit)
#elif __ICCARM__
#pragma section = "HEAP"
#define HEAP_BEGIN (__segment_end("HEAP"))
#else
extern int __heap_start;
#define HEAP_BEGIN (&__heap_start)
#endif
extern int __heap_end;

#define HEAP_END (&__heap_end)

#ifdef OS_USING_PUSH_BUTTON
extern const struct push_button key_table[];
extern const int                key_table_size;
#endif

#ifdef OS_USING_LED
extern const led_t led_table[];
extern const int   led_table_size;
#endif

#ifdef OS_USING_BUZZER
extern const struct buzzer buzzer_table[];
extern const int           buzzer_table_size;
#endif

#ifdef OS_USING_USB_DEVICE
extern const struct usbd usbd_table[];
extern const int         usbd_table_size;
#endif

#ifdef __cplusplus
}
#endif

#endif
