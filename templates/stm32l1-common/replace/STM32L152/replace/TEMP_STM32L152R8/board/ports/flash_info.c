static struct onchip_flash_info onchip_flash = {
    .start_addr = 0x08004000,
    .capacity   = 4 * 1024,
    .block_size = 4 * 1024,
    .page_size  = 256,
};
OS_HAL_DEVICE_DEFINE("Onchip_Flash_Type", "onchip_flash", onchip_flash);
