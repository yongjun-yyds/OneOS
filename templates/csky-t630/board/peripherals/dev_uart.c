#include <oneos_config.h>
#ifdef OS_USING_SERIAL
#include "drv_uart.h"
#include <uart.h>
extern void UART_IRQHandler(void);

#ifdef BSP_USING_UART0
static ck_uart_info_t uart0_info = {
    .uart_num         = UART0,
    .config.baudrate  = UART_BAUDRATE_115200,
    .config.parity    = UART_PARITY_NO,
    .config.stop_bits = UART_STOP_BIT_EQ_1,
    .config.word_size = UART_WORD_SIZE_8BIT,
    .irq_handler      = UART_IRQHandler,
    .irq_num          = UART0_IRQ_NUM,
    .uart_name        = "UART0",
};
OS_HAL_DEVICE_DEFINE("UART_InitStructure", "uart0", uart0_info);
#endif

#ifdef BSP_USING_UART1
static ck_uart_info_t uart1_info = {
    .uart_num         = UART1,
    .config.baudrate  = UART_BAUDRATE_115200,
    .config.parity    = UART_PARITY_NO,
    .config.stop_bits = UART_STOP_BIT_EQ_1,
    .config.word_size = UART_WORD_SIZE_8BIT,
    .irq_handler      = UART_IRQHandler,
    .irq_num          = UART1_IRQ_NUM,
    .uart_name        = "UART1",
};
OS_HAL_DEVICE_DEFINE("UART_InitStructure", "uart1", uart1_info);
#endif

#endif
