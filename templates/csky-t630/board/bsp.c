/*
 * Copyright (C) 2017 C-SKY Microsystems Co., Ltd. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/******************************************************************************
 * @file     system_PHOBOS.c
 * @brief    CSI Device System Source File for PHOBOS
 * @version  V1.0
 * @date     02. June 2017
 ******************************************************************************/
#include <log.h>
#include <stdio.h>
#ifdef __CSKY__
#include <core_ck803.h>
#else
#include <cpu.h>
#endif
#include <scu.h>
#include <uart.h>
#include <h2x.h>
#include <tih/timer.h>
#include <memcpy_hw.h>
#include <interrupt.h>
#include <delay.h>
#include <wdt.h>
#include <gpio.h>
#include <scu.h>
#include <dma.h>
#include <sram.h>
#include <core_timer.h>
#include <os_types.h>
#include <drv_intr.h>
#include <backtrace.h>
#include "drv_coretimer.h"
/*----------------------------------------------------------------------------
  Define clocks
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
  System Core Clock Variable
 *----------------------------------------------------------------------------*/
extern void       print_log(uint32 flag);
void __fast __isr console_isr(void)
{
    uint8  ch;
    uint32 status;
    status = uart_intr_status(UART0);
    uart_intr_clear(UART0, status);
    intr_clear(UART0_IRQ_NUM);

    switch (status & 0xf)
    {
    case UART_INTR_RX_ERROR:
        if (status & UART_ERR_OVERRUN)
        {
            // do TODO
        }

        if (status & UART_ERR_FRAMING)
        {
            // do TODO
        }

        if (status & UART_ERR_PARITY)
        {
            // do TODO
        }
        break;
    case UART_INTR_RX_READY:
        while (uart_rx_ready(UART0))
        {
            ch = uart_ndelay_getc(UART0);
            if (ch == 0x55)
            {
                chip_reset(1);
                while (1)
                    ;
            }
            else if (ch == 0x56)
            {
                chip_reset(1);
            }
            else if (ch == 0x57)
            {
                chip_reset(1);
            }
            uart_ndelay_putc(UART0, ch);
        }
        break;
    case UART_INTR_TX_EMPTY:
        /* TX new data into THR */

        break;
    default:
        break;
    }
}

void console_init(void)
{
    uart_config_t config;
    config.baudrate  = UART_BAUDRATE_115200;
    config.parity    = UART_PARITY_NO;
    config.stop_bits = UART_STOP_BIT_EQ_1;
    config.word_size = UART_WORD_SIZE_8BIT;

    uart_hw_init(UART_CONSOLE, &config);
    uart_intr_enable(UART0, UART_INTR_RX_READY);
    // uart_intr_enable(UART0, UART_INTR_TX_EMPTY);
    uart_intr_enable(UART0, UART_INTR_RX_ERROR);
    intr_connect(UART0_IRQ_NUM, UART0_IRQ_NUM, console_isr);
    intr_enable(UART0_IRQ_NUM);
}

void sys_init(void)
{
    clock_source_set(CLOCK_PLL, 200000000);       // 600M
    clock_freq_set(MODULE_AHB, 150000000);        // 100M
    clock_freq_set(MODULE_AXI, 200000000);        // 100M
    clock_freq_set(MODULE_UART_IO, 150000000);    // 150M
    clock_freq_set(MODULE_MUXIO, 150000000);      // 150M
    clock_freq_set(MODULE_CRYPTO, 150000000);     // 150M

    clock_enable(MODULE_AXI);
    clock_enable(MODULE_APB);
    clock_enable(MODULE_USB);
    clock_enable(MODULE_DMA);

#ifdef OS_USING_SERIAL
    clock_enable(MODULE_UART_IO);
#endif

    clock_enable(MODULE_MUXIO);
    clock_enable(MODULE_CRYPTO);

#ifdef OS_USING_I2C
    clock_enable(MODULE_I2C);
#endif

    module_enable(MODULE_USB);
    module_enable(MODULE_USB3_PHY);
    module_enable(MODULE_DMA);
    module_enable(MODULE_MUXIO);
    module_enable(MODULE_CRYPTO);

    intr_hw_init();
    h2x_hw_init();

#ifdef __CSKY__
    csi_cache_set_range(0, 0, CACHE_CRCR_8K, 1);
    csi_cache_set_range(1, 0x01000000, CACHE_CRCR_128K, 1);
    csi_cache_set_range(2, 0x800000, CACHE_CRCR_32K, 1);    //.....
    csi_icache_enable();
#endif
}

void gpio_init(void)
{

#if EN_AUTO_CHIP_REST
    gpio_config_t config;
    config.pin       = GPIO1_PIN_1;
    config.io_mode   = GPIO_IOMODE_OUT;
    config.trig_mode = GPIO_TRIGMODE_NONE;
    gpio1_hw_init(&config);
    /*pull down FPGA_RESET*/
    gpio1_set(GPIO1_PIN_1, 0);
#endif

    /*T630 gpio1_3<->FPGA config_done*/
    //    config.pin = GPIO_FPGA_CONFIG;
    //    config.io_mode = GPIO_IOMODE_INPD;
    //    config.trig_mode = GPIO_TRIGMODE_NONE;
    //    gpio1_hw_init(&config);
}

void __fast pre_main(void)
{
    uint32_t key;

    sys_init();
    scu_pin_mux_set(SCU_PIN_MUX_MODE1_2, SCU_PIN_MUX_MODE2_GPIO1);
    console_init();
    gpio_init();
    mem_hw_init();
    delay_init();
    irq_enable();
    printf("pre_main start...\r\n");
    mdelay(3000);

#if 0    
    printf("Please select the function.\n");
    printf("0   : reset chip.\r\n");
    printf("1   : continue run.\r\n");
    printf("0x55: reset chip.\r\n");

    while (1){
        scanf ("%d", &key);
        printf("\n");
        if (key == 0)
            chip_reset(1);
        else if (key == 1)
            break;
        else
            chip_reset(1);
    }
#endif
    return;
}

void hardware_init(void)
{
    /* core timer-sys clk init */
    os_hw_interrupt_init();
    core_timer_init(1000000 / OS_TICK_PER_SECOND);
    irq_enable();
}

extern int entry(void);
void pre_entry(void)
{
    pre_main();
    entry();
}