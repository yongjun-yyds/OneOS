/*
 文件: bsp.h
 版本: CMC693 V1.10
 版权: 浙江杰芯科技有限公司
 作者: zhulin(zhulin2@supcon.com)
 描述：
修改记录：
=========
日期                                      作者                       工作                            内容
*/

#ifndef __bsp_h 
#define __bsp_h

#include "datatype.h"

/* 22号优先级固定为powm中断，MAC0中断优先级20，MAC1中断优先级21 */
#define TIMER0_PRIO    15  // timer0 --- the hightest prio
#define TIMER1_PRIO    14   // timer1中断优先级
#define TIMER2_PRIO    13  // timer2中断优先级
#define TIMER3_PRIO    28  // timer3中断优先级
#define GPIO0A_0_PRIO  27  // gpio0a_0中断优先级
#define IIC_PRIO       26  // IIC中断优先级
#define PIPO_PRIO      25  // PIPO中断优先级
#define GPIO1A_PRIO    24
#define GPIO1A_29_PRIO 23
#define WDT_PRIO       21  //22用于电源管理中断
#define UART1_PRIO     20
#define UART0_PRIO     19
#define CAN0_PRIO      18
#define CAN1_PRIO      17
#define RTC_PRIO       16


extern void QF_tick(void);

void Cmc693v3_LowLevelInit(void);
int Uart0Printf(const char *fmt,...);
void DelayNMS(UINT32 ms);

#endif
/********************************************************************************************
***                     文件结束                                                          ***
********************************************************************************************/
