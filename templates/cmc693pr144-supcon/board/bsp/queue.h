/*
 * queue.h
 *
 *  Created on: 2019-3-2
 *      Author: zhulin2
 */

#ifndef QUEUE_H_
#define QUEUE_H_

#include "datatype.h"

/* 定义队列空闲状态宏对象，分别表示 */
#define CQ_LOCK_IDLE	(0)


/* 定义循环队列的结构体，用于初始化配置时指定。包括队列大小，相关标志位等. */
struct CycleQueue_Struct;
typedef struct CycleQueue_Struct CycleQueue_t, *pCycleQueue_t;

struct CycleQueue_Struct
{
	UINT16			CQSize;				//循环队列的大小
	UINT16			CQSizeBackup;		//新的数据长度备份，用于初始化时保存需要初始化的队列大小
	UINT16			FontPoint;			//队列头指针
	UINT16			RearPoint;			//队列尾指针
	UINT16			UsedCount;			//使用计数
	UINT16			Reseved;			//保留

	pCycleQueue_t	This;				//指向本队列结构，用于标识
	UINT8  	 		*pDataBuffer;		//指向数据存放地址
	UINT8			*pDataBufferBackup;	//数据存放地址的备份。重新初始化时不允许直接执行初始化函数。

	struct
	{
		UINT8 		WriteLock;			//写标识符，每次尝试写操作之前都自加一次，操作完成后复位
		UINT8		ReadLock;			//读标识符，每次尝试读操作之前都自加一次，操作完成后复位
		UINT8 		ResetLock;			//复位标识符，每次尝试读操作之前都自加一次，操作完成后复位
		UINT8 		InitLock;			//初始化标识符，每次尝试读操作之前都自加一次，操作完成后复位
	};

};

typedef enum{
	_OPT_CQ_INIT,		//初始化
	_OPT_CQ_WRITE,		//入列
	_OPT_CQ_READ,		//出列
	_OPT_CQ_RESET,		//复位
	_OPT_CQ_QUERY,		//查询剩余空间
}CycleQueue_Opt_t;

//inline BOOL CQ_IsFull(CycleQueue_t *pCQ);
//inline BOOL CQ_IsEmpty(CycleQueue_t *pCQ);
UINT16 CQ_Init(CycleQueue_t *pCQ,UINT8 *pBuf, UINT16 CQSize);
UINT16 CQ_Reset(CycleQueue_t *pCQ);
UINT16 CQ_GetEmptySize(CycleQueue_t *pCQ);
UINT16 CQ_GetUsedSize(CycleQueue_t *pCQ);
UINT16 CQ_WriteChar(CycleQueue_t *pCQ, UINT8 chr);
UINT16 CQ_Write(CycleQueue_t *pCQ, UINT8 *pBuf, UINT16 Length);
UINT16 CQ_Read(CycleQueue_t *pCQ, UINT8 *pBuf, UINT16 Length);


#endif /* QUEUE_H_ */
