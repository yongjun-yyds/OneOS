/*
 文件: flash.c
 版本: CMC693 V1.10
 版权: 浙江杰芯科技有限公司
 作者: 喻文星 (yuwenxing@supcon.com)
 描述：Flash是GD25QXX系列
	 目前使用的Flash大小为2M通过SPI1进行通信
	 此Flash有扇区、块、整片擦除的命令，没有页擦除的命令；页擦除是先将整个扇区的内容拷出来，再扇区擦除，最会再将所有数据写回
	所以效率很低，尽量使用扇区写入

修改记录：
=========
日期                                      作者                       工作                            内容
-------------------------------------------------------
2018-09-27    喻文星                              修改                           规范编程风格
*/
#include <string.h>
#include "flash.h"
#include "lib_spi.h"

pStruct_SPIInfo pFlashSpi = &SPI1;

/******************************GD25QXX************************/
/**
 * @brief  : GD25QXX 通过SPI写数据
 * @param  : This   GD25QXX结构体
 *           pcmd_buffer 命令指针
 *           cmdsize 命令数据长度
 *           ptx_buffer 数据指针
 *           tx_size 数据长度
 * @note   : 调用外部SPI收发函数
 * @retval : None
 */
void GD25QXX_SPI_Write(void *pcmd_buffer,
                       UINT8 cmdsize,
                       void *ptx_buffer,
                       UINT16 tx_size)
{
    SPI_WriteFlash(pFlashSpi, pFlashSpi->SPI_InitStruct.cs, pcmd_buffer, cmdsize, ptx_buffer, tx_size);
}

/**
 * @brief  : GD25QXX 通过SPI接收数据
 * @param  : This   GD25QXX结构体
 *           pcmd_buffer 命令指针
 *           cmdsize 命令数据长度
 *           ptx_buffer 数据指针
 *           tx_size 数据长度
 * @note   : 调用外部SPI收发函数
 * @retval : None
 */
void GD25QXX_SPI_Read(void *pcmd_buffer,
                      UINT8 cmdsize,
                      void *prx_buffer,
                      UINT16 rx_size)
{
    SPI_ReadFlash(pFlashSpi, pFlashSpi->SPI_InitStruct.cs, pcmd_buffer, cmdsize, prx_buffer, rx_size);
}

/**
 * @brief  : GD25QXX 写数据使能
 * @param  : This   GD25QXX结构体
 * @note   : 写数据使能不需要让用户知道
 * @retval : None
 */
static void GD25QXX_WriteEnable(void)
{
    UINT8 Data[1] = {GD25QXX_WRITE_ENABLE};
    GD25QXX_SPI_Write(Data, 1, NULL, 0);
}

/**
 * @brief  : GD25QXX 写数据失能
 * @param  : This   GD25QXX结构体
 * @note   :
 * @retval : None
 */
static void GD25QXX_WriteDisable(void)
{
    UINT8 Data[1] = {GD25QXX_WRITE_DISABLE};
    GD25QXX_SPI_Write(Data, 1, NULL, 0);
}

/**
 * @brief  : GD25QXX读状态寄存器.
 * @param  : This GD25QXX结构体
 *           pStatus 读回的状态寄存器值
 * @note   :
 * @retval : None
 */
void GD25QXX_ReadStatus(pGD25QXX_Status_St pStatus)
{
    //ASSERT( (This != NULL) && (pStatus != NULL), "GD25QXX Read Status Err!", 0);

    UINT16 temp;
    UINT8 TxRxData[2] = {GD25QXX_STATUSREG_H_READ_ENABLE};

    //先获取高字节的状态寄存器值
    GD25QXX_SPI_Read(TxRxData, 1, TxRxData, 1);
    temp = TxRxData[0] << 8;

    //接着获取低字节的状态寄存器值
    TxRxData[0] = GD25QXX_STATUSREG_L_READ_ENABLE;
    GD25QXX_SPI_Read(TxRxData, 1, TxRxData, 1);
    temp |= TxRxData[0];

    *pStatus = *( (pGD25QXX_Status_St)&temp );
}

/**
 * @brief  : GD25QXX 写状态寄存器
 * @param  : Status 状态寄存器的状态值
 * @note   : 对于非易失寄存器位也可以写入
 * @retval : None
 */
void GD25QXX_WriteStatus(GD25QXX_Status_St Status)
{
    UINT8 Data[3] = {GD25QXX_STATUSREG_WRITE_ENABLE};
    UINT16 temp = *( (UINT16 *)&Status );

    GD25QXX_SPI_Write(Data, 1, NULL, 0);//允许写状态寄存器(S0 S1 S15 三位无效)

    GD25QXX_WriteEnable();//写入使能

    Data[0] = GD25QXX_STATUSREG_WRITE;//写状态寄存器使能
    Data[1] = temp & 0xFF;  //SR  7..0
    Data[2] = temp  >> 8;   //SR 15..8

    GD25QXX_SPI_Write(Data, 3, NULL, 0);

    GD25QXX_WriteDisable();//写入禁止
}

/**
 * @brief  : GD25QXX 等待空闲
 * @param  : This   GD25QXX结构体
 * @note   :
 * @retval : None
 */
void GD25QXX_WaitIdle(void)
{
    GD25QXX_Status_St Status;
    do
    {
        GD25QXX_ReadStatus(&Status);
    }
    while(Status.WIP);
}

/**
 * @brief  : GD25QXX 适配默认的工作模式，我们只需要对状态寄存器全部写0即可。
 * @param  :
 * @note   :
 * @retval : None
 */
static void GD25QXX_AdaptDefaultMode(void)
{
    GD25QXX_Status_St GD25QXX_Status;
    GD25QXX_ReadStatus(&GD25QXX_Status);

    if ( (GD25QXX_Status.StateReg_L != 0) ||
         (GD25QXX_Status.StateReg_H != 0)  )
    {
        GD25QXX_Status.StateReg_L = 0;
        GD25QXX_Status.StateReg_H = 0;//我们的默认值配置为全0.
        GD25QXX_WriteStatus(GD25QXX_Status);
    }
}

/**
 * @brief  : GD25QXX 读制造商信息和设备ID.
 * @param  : pID    返回的MID值
 * @note   :
 * @retval : None
 */
void GD25QXX_ReadDID(UINT8 *pID)
{
    UINT8 Data[4] = { GD25QXX_READ_DID,     //sending Read DID command
                      0x00,                 //24Bit ADDR
                      0x00,
                      0x00,                 //ADDR 0000H
                    };

    GD25QXX_SPI_Read(Data, 4, pID, 2);
}

/**
 * @brief  : GD25QXX 读唯一识别码UID.
 * @param  : pID    返回的8Byte的UID值
 * @note   :
 * @retval : None
 */
void GD25QXX_ReadUID(UINT8 *pID)
{
    UINT8 Data[5] = {GD25QXX_READ_UID,          //sending Read Unique ID command
                     GD25QXX_DUMMY_BYTE,        //Dummy Byte1
                     GD25QXX_DUMMY_BYTE,        //Dummy Byte2
                     GD25QXX_DUMMY_BYTE,        //Dummy Byte3
                     GD25QXX_DUMMY_BYTE,        //Dummy Byte4
                                                //128bit
                    };

    GD25QXX_SPI_Read(Data, 5, pID, 8);
}

/**
 * @brief  : GD25QXX 读数据
 * @param  : pToPageBuffer 内存的数据指针
 *           FromFlashBaseAddr 读取的Flash首地址
 * @note   : 由于SPI寄存器限制，一次读取不能超过64KB
 * @retval : 实际读取到的长度
 */
UINT16 GD25QXX_ReadBytes(UINT8 *pToPageBuffer,
                         UINT32 FromFlashBaseAddr,
                         UINT16 DataSize)
{
    UINT8 Data[4] = { GD25QXX_READ_BYTES,
                     (FromFlashBaseAddr >> 16) & 0xFF,
                     (FromFlashBaseAddr >>  8) & 0xFF,
                     (FromFlashBaseAddr >>  0) & 0xFF };

    //等待空闲
    GD25QXX_WaitIdle();

    //写入禁止
    GD25QXX_WriteDisable();

    //读取数据
    GD25QXX_SPI_Read(Data, 4, (void *)pToPageBuffer, DataSize);

    return DataSize;
}

/**
 * @brief  : 数据写入 。
 * @param  : ToFlashBaseAddr    基址，写入数据的首地址
 *           pFromDataBuffer    数据缓存地址指针
 *           DataSize           数据长度
 * @note   : 写之前确保：
 *            1 写入的数据单元已经擦除过（值均为0xFF）；
 *            2 写入长度不超过一页，否则只有最后1页的数据被写入，先前的数据会被丢弃。
 * @retval : 实际写入的长度
 */
UINT16 GD25QXX_WriteBytes(UINT32 ToFlashBaseAddr,
                        UINT8 *pFromDataBuffer,
                        UINT16 DataSize)
{
    UINT8 Data[4] = {GD25QXX_WRITE_BYTES,
                     (ToFlashBaseAddr >> 16) & 0xFF,
                     (ToFlashBaseAddr >>  8) & 0xFF,
                     (ToFlashBaseAddr >>  0) & 0xFF};

    UINT16 maxsize = GD25QXX_PAGE_BYTE - GD25QXX_INPAGE_OFFSET(ToFlashBaseAddr);

    //如果要写入页，必须按页对齐,如果写入的数据大于实际的可写入数据，只写入可以写入的部分。
    if(DataSize > maxsize)
    {
        DataSize = maxsize;
    }

    //等待空闲
    GD25QXX_WaitIdle();

    GD25QXX_WriteEnable();//写入使能

    GD25QXX_SPI_Write(Data, 4, (void *)pFromDataBuffer, DataSize);

    //等待写入完成
    GD25QXX_WaitIdle();

    return DataSize;
}

/**
 * @brief  : 段擦除.
 * @param  : BaseAddr       基址，需要擦除数据的首地址
 * @note   : 擦除4KB大小的段。 按Sector对齐
 * @retval : None
 */
void GD25QXX_EarseSector(UINT32 BaseAddr)
{
    GD25QXX_SECTOR_ALIGN(BaseAddr);

    //段擦除 4K
    UINT8 Data[4] = { GD25QXX_SECTOR_EARSE,
                      (BaseAddr >> 16) & 0xFF,
                      (BaseAddr >>  8) & 0xFF,
                      (BaseAddr >>  0) & 0xFF };
    //等待空闲
    GD25QXX_WaitIdle();

    //写入使能
    GD25QXX_WriteEnable();

    //擦除Sector
    GD25QXX_SPI_Write(Data, 4, NULL, 0);

    //等待擦除完成
    GD25QXX_WaitIdle();
}

/**
 * @brief  : 段擦除.
 * @param  : BaseAddr       基址，需要擦除数据的首地址
 * @note   : 擦除32KB大小的块，以64KB为块单位。按Block对齐
 * @retval : None
 */
void GD25QXX_EarseBlock(UINT32 BaseAddr)
{
    GD25QXX_BLOCK_ALIGN(BaseAddr);

    //块擦除
    UINT8 Data[4] = { GD25QXX_BLOCK_EARSE,
                      (BaseAddr >> 16) & 0xFF,
                      (BaseAddr >>  8) & 0xFF,
                      (BaseAddr >>  0) & 0xFF };

    //等待空闲
    GD25QXX_WaitIdle();

    //写入使能
    GD25QXX_WriteEnable();

    //擦除32KB Block
    GD25QXX_SPI_Write(Data, 4, NULL, 0);

    //等待擦除完成
    GD25QXX_WaitIdle();
}

/**
 * @brief  : 段擦除.
 * @param  :
 * @note   : 擦除整片Flash
 * @retval : None
 */
void GD25QXX_EarseChip(void)
{
    UINT8 Data[1] = { GD25QXX_CHIP_EARSE};

    //等待空闲
    GD25QXX_WaitIdle();

    //写入使能
    GD25QXX_WriteEnable();

    //擦除Chip
    GD25QXX_SPI_Write(Data, 1, NULL, 0);

    //等待擦除完成
    GD25QXX_WaitIdle();
}

/**
 * @brief  : 判断GD25QXX 整页是否为空.
 * @param  : FlashBaseAddr 起始地址
 * @note   : 自动进行页对齐
 * @retval : None
 */
BOOL GD25QXX_IsPageEmpty(UINT32 FlashBaseAddr, UINT8 PageNumber)
{
    UINT32 PageBuffer[(GD25QXX_PAGE_BYTE >> 2)];
    UINT8 i = 0;
    UINT8 cmpcount = GD25QXX_PAGE_BYTE >> 2;

    while(PageNumber--)
    {
		GD25QXX_PAGE_ALIGN(FlashBaseAddr);//对齐到页首地址

		GD25QXX_ReadBytes((UINT8 *)PageBuffer,
						  FlashBaseAddr,
						  GD25QXX_PAGE_BYTE);

		//遍历所有的字节，如果全都为空，返回TRUE，否则返回FALSE。
		for(i = 0; i < cmpcount; i++)
		{
			if(PageBuffer[i] != GD25QXX_FREE_DWORD)
			{
				return FALSE;
			}
		}

		FlashBaseAddr += GD25QXX_PAGE_BYTE;
    }

    return TRUE;
}

/**
 * @brief  : 判断GD25QXX 整段是否为空.
 * @param  : FlashBaseAddr 起始地址
 * @note   : 自动进行Sector对齐
 * @retval : None
 */
BOOL GD25QXX_IsSectorEmpty(UINT32 FlashBaseAddr)
{
    UINT8 i = 0;
    UINT8 pageSize = GD25QXX_SECTOR_BYTE / GD25QXX_PAGE_BYTE;

    GD25QXX_SECTOR_ALIGN(FlashBaseAddr);//FlashBaseAddr 按扇区对齐

    for(i = 0; i < pageSize; i++)
    {
        if( !GD25QXX_IsPageEmpty(FlashBaseAddr, 1) )
        {
            return FALSE;
        }

        FlashBaseAddr += GD25QXX_PAGE_BYTE;
    }

    return TRUE;
}

/**
 * @brief  : GD25QXX 擦除若干页的数据
 * @param  : FlashBaseAddr     要操作的地址
 *           PageNumber		          擦除页数
 * @note   :此函数不支持夸扇区的多页擦除
 * @retval : TRUE, 写入成功；FALSE，写入失败。
 */
BOOL GD25QXX_EarseSomePages(UINT32 FlashBaseAddr, UINT8 PageNumber)
{
    GD25QXX_PAGE_ALIGN(FlashBaseAddr);//对齐到页首地址

    //如果待擦除页未使用，直接返回TRUE
    if ( GD25QXX_IsPageEmpty(FlashBaseAddr, PageNumber) )
    {
        return TRUE;
    }

#if GD25QXX_UNLOAD_EN //使能Flash内部转存
    UINT8  DataBuffer[GD25QXX_PAGE_BYTE];
    GD25QXX_EarseSector(GD25QXX_UNLOAD_BASEADDR);//擦除转存区域

#else
    static UINT8 GD25QXX_UNLOAD_BASEADDR[GD25QXX_SECTOR_BYTE];//如果使用内存转存，定义足够大的数组
    //暂时如此定义，后续会尝试使用内存分配。
#endif

    UINT16 i = 0, TargetPageOffset = 0;
    UINT16 PagesInSector = GD25QXX_SECTOR_BYTE / GD25QXX_PAGE_BYTE;//总共包含的页大小
    UINT32 SectorBaseAddr = FlashBaseAddr;
    GD25QXX_SECTOR_ALIGN(SectorBaseAddr);//定位 指定Flash地址所在的段
    UINT32 StoreOffsetAddr = 0;//Flash的存储偏移地址

    TargetPageOffset = (FlashBaseAddr - SectorBaseAddr) / GD25QXX_PAGE_BYTE;//定位需要擦除的页索引值

    for(i = 0, StoreOffsetAddr = 0; i < PagesInSector; i++)
    {
        if((i >= TargetPageOffset) && (i < (TargetPageOffset + PageNumber)))
        {
            StoreOffsetAddr += GD25QXX_PAGE_BYTE;
            continue;
        }

        #if GD25QXX_UNLOAD_EN
        GD25QXX_ReadBytes((UINT8 *)DataBuffer,
                                (UINT32)(SectorBaseAddr + StoreOffsetAddr),
                                GD25QXX_PAGE_BYTE);

        GD25QXX_WriteBytes((UINT32)(GD25QXX_UNLOAD_BASEADDR + StoreOffsetAddr),
                                 (UINT8 *)DataBuffer,
                                 GD25QXX_PAGE_BYTE);

        #else
        GD25QXX_ReadBytes((UINT8 *)(GD25QXX_UNLOAD_BASEADDR + StoreOffsetAddr),
                                (UINT32)(SectorBaseAddr + StoreOffsetAddr),
                                GD25QXX_PAGE_BYTE);
        #endif

        StoreOffsetAddr += GD25QXX_PAGE_BYTE;
    }

    GD25QXX_EarseSector(SectorBaseAddr);//擦除需要擦除的区域

    //恢复其他页
    for(i = 0, StoreOffsetAddr = 0; i < PagesInSector; i++)
    {
        if((i >= TargetPageOffset) && (i < (TargetPageOffset + PageNumber)))
        {
        	StoreOffsetAddr += GD25QXX_PAGE_BYTE;
            continue;
        }

        #if GD25QXX_UNLOAD_EN
        GD25QXX_ReadBytes((UINT8 *)DataBuffer,
                           (UINT32)(GD25QXX_UNLOAD_BASEADDR + StoreOffsetAddr),
                           GD25QXX_PAGE_BYTE);

        GD25QXX_WriteBytes((UINT32)(SectorBaseAddr + StoreOffsetAddr),
                            (UINT8 *)DataBuffer,
                            GD25QXX_PAGE_BYTE);
        #else
        GD25QXX_WriteBytes((UINT32)(SectorBaseAddr + StoreOffsetAddr),
                            (UINT8 *)(GD25QXX_UNLOAD_BASEADDR + StoreOffsetAddr),
                            GD25QXX_PAGE_BYTE);
        #endif

        StoreOffsetAddr += GD25QXX_PAGE_BYTE;
    }

    return TRUE;
}

/**
 * @brief  : GD25QXX 写入一整页数据
 * @param  : ToPageBaseAddr         GD25QXX结构体
 *           FromDataBaseAddr   初始化结构体
 * @note   : 使用内存或Flash转存根据宏定义切换
 * @retval : TRUE, 写入成功；FALSE，写入失败。
 */
BOOL GD25QXX_WritePage(UINT32 ToFlashBaseAddr, UINT32 *FromPageBaseAddr)
{
    GD25QXX_EarseSomePages((UINT32)ToFlashBaseAddr, 1);

    GD25QXX_WriteBytes((UINT32 )ToFlashBaseAddr,
                        (UINT8 *)FromPageBaseAddr,
                        GD25QXX_PAGE_BYTE);

    return TRUE;
}

/**
 * @brief  : GD25QXX 写入一扇区数据
 *
 * @param  : ToFlashBaseAddr	Flash地址
 *           FromPageBaseAddr   RAM地址
 *
 * @note   : 使用内存或Flash转存根据宏定义切换
 * 			   已确保Flash地址按扇区对齐
 *
 * @retval : TRUE, 写入成功；FALSE，写入失败。
 */
BOOL GD25QXX_WriteSector(UINT32 ToFlashBaseAddr, UINT8 *FromPageBaseAddr)
{
	GD25QXX_EarseSector(ToFlashBaseAddr);

	UINT8 i = 0;
	for(i = 0; i < (GD25QXX_SECTOR_BYTE / GD25QXX_PAGE_BYTE); i++)
	{
		//单次写入最多一页
		GD25QXX_WriteBytes(ToFlashBaseAddr, (UINT8 *)FromPageBaseAddr, GD25QXX_PAGE_BYTE);

		ToFlashBaseAddr += GD25QXX_PAGE_BYTE;
		FromPageBaseAddr += GD25QXX_PAGE_BYTE;
	}

    return TRUE;
}

/**
 * @brief  : GD25QXX 写入一整页数据
 * @param  : ToPageBaseAddr     GD25QXX结构体
 *           FromDataBaseAddr   初始化结构体
 * @note   :
 * @retval : TRUE, 写入成功；FALSE，写入失败。
 */
BOOL GD25QXX_ReadPage(UINT32 *ToPageBaseAddr, UINT32 FromFlashBaseAddr)
{
    GD25QXX_ReadBytes((UINT8 *)ToPageBaseAddr,
                      (UINT32 )FromFlashBaseAddr,
                      GD25QXX_PAGE_BYTE);

    return TRUE;
}

/*****************************end GD25QXX*****************************/


/*****************************Flash 通用函数****************************/
/*
 * 等待进入Flash进入空闲状态
 * 输入参数：无
 * 输出参数：返回 TRUE 初始化成功   FALSE 初始化失败
 * 说明：无用 待删除
 */
BOOL WaitFlashIdle(void)
{
    GD25QXX_WaitIdle();

    return TRUE;
}

/*
 * Flash  跨扇区写入操作（即写入操作涉及两个及两个以上的扇区）
 * 输入参数：dwFlashAddr  				Flash写入的起始地址，要求地址页对齐
 * 		  dwFlashAddrSector	        Flash起始地址所在的扇区的首扇区地址
 *        pcData      				缓存地址
 *        dwByteLength     			写入长度
 * 输出参数：返回 TRUE 初始化成功   FALSE 初始化失败
 * 说明：只能用于跨扇区写入，如果调用函数进行单个扇区内的写入操作会出错
 */
static BOOL Flash_WriteCrossSector(UINT32 dwFlashAddr, UINT32 dwFlashAddrSector, UINT8* pcData, UINT32 dwByteLength)
{
	//当前扇区末地址
	UINT32 dwFirstSectorEndAddr = dwFlashAddrSector + SECTOR_SIZE_BYTE;
	//当前扇区需要写入的页个数
	UINT8 dwFirstSectorPageNumber = (dwFirstSectorEndAddr - dwFlashAddr) / PAGE_SIZE_BYTE;

	//末扇区对齐首地址
	UINT32 dwLastSectorAddrAligned = FLASH_SECTOR_ALIGN(dwFlashAddr + dwByteLength);
	//范围跨完整扇区个数，因为第一个扇区不全部写入，所以需要把第一个扇区减去
	UINT8 cSectorNumber = (dwLastSectorAddrAligned - dwFlashAddrSector) / SECTOR_SIZE_BYTE - 1;


	//末扇区需要写入的页个数，不足页按页写入
	UINT8 cLastSectorPageNumber = ((dwFlashAddr + dwByteLength - dwLastSectorAddrAligned + PAGE_SIZE_BYTE - 1) \
									& 0xFFFFFF00) / PAGE_SIZE_BYTE;

	//当前扇区写入
	//擦除
	GD25QXX_EarseSomePages(dwFlashAddr, dwFirstSectorPageNumber);

	//按页写入
	while(dwFirstSectorPageNumber--)
	{
		GD25QXX_WriteBytes(dwFlashAddr, (UINT8 *)pcData, PAGE_SIZE_BYTE);

		dwFlashAddr += PAGE_SIZE_BYTE;
		pcData += PAGE_SIZE_BYTE;
	}

	//中间完整扇区写入
	while(cSectorNumber--)
	{
		GD25QXX_WriteSector(dwFlashAddr, (UINT8 *)pcData);

		//调整地址指针
		dwFlashAddr += SECTOR_SIZE_BYTE;
		pcData += SECTOR_SIZE_BYTE;
	}

	//末扇区写入
	if(0 != cLastSectorPageNumber)
	{
		//擦除
		GD25QXX_EarseSomePages(dwFlashAddr, cLastSectorPageNumber);

		//按页写入
		while(cLastSectorPageNumber--)
		{
			GD25QXX_WriteBytes(dwFlashAddr, (UINT8 *)pcData, PAGE_SIZE_BYTE);

			dwFlashAddr += PAGE_SIZE_BYTE;
			pcData += PAGE_SIZE_BYTE;
		}
	}

	return TRUE;
}

/*
 * Flash 按页写入
 * 输入参数：dwFlashAddr  	Flash写入的起始地址
 *        pcData      	缓存地址
 *        dwByteLength    写入字节长度
 * 输出参数：返回 TRUE 初始化成功   FALSE 初始化失败
 * 说明：如果起始地址不是页对齐，则直接退出
 * 	        写入Flash时以页为单位写入（256Bytes）,不足一页的会按一页写入
 * 	        所以使用时需注意当不足一页的会按一页写入会导致读RAM中超出ByteLength长度的空间继续写入Flash
 * 	       会进行输入地址范围检测，写入地址和写入长度超过范围则失败退出  包括RAM地址范围检测和Flash地址范围检测
 * 	   Flash的扇区写入效率高于页写入，所以当写入Flash的范围包括一个完整的扇区使用扇区写入
 */
BOOL Flash_PageWrite(UINT32 dwFlashAddr, UINT8* pcData, UINT32 dwByteLength)
{
	//长度为0，无需操作
	if(0 == dwByteLength)
	{
		return TRUE;
	}

	UINT32 dwLengthPageAligned = ((dwByteLength + PAGE_SIZE_BYTE - 1) & 0xFFFFFF00);//向高地址对齐
	UINT32 dwLengthSectorAligned = FLASH_SECTOR_ALIGN(dwByteLength);

	//地址范围检测
	if((FLASH_BOUNDARY_CHECK(dwFlashAddr)) || (FLASH_BOUNDARY_CHECK(dwFlashAddr + dwLengthPageAligned))
		|| (RAM_BOUNDARY_CHECK(pcData)) || (RAM_BOUNDARY_CHECK(pcData + dwLengthPageAligned)))
	{
		return FALSE;
	}

	//对齐地址
	UINT32 dwAddrPageAligned = FLASH_PAGE_ALIGN(dwFlashAddr);
	UINT32 dwAddrSectorAligned = FLASH_SECTOR_ALIGN(dwFlashAddr);

	//写入地址必须页对齐
	if(dwFlashAddr != dwAddrPageAligned)
	{
		return FALSE;
	}

	//如果地址和长度都是扇区对齐
	if((dwFlashAddr == dwAddrSectorAligned)
		&& (dwByteLength == dwLengthSectorAligned))
	{
		UINT8 cSectorNumber = dwByteLength / SECTOR_SIZE_BYTE;

		//按扇区写入
		while(cSectorNumber--)
		{
			GD25QXX_WriteSector(dwFlashAddr, (UINT8 *)pcData);

			//调整地址指针
			dwFlashAddr += SECTOR_SIZE_BYTE;
			pcData += SECTOR_SIZE_BYTE;

		}
	}
	//需要地址和长度都是页对齐
	else
	{
		//写入范围是否超出当前扇区
		if((dwFlashAddr + dwLengthPageAligned) <= (dwAddrSectorAligned + SECTOR_SIZE_BYTE))
		{
			UINT8 cPageNumber = dwLengthPageAligned / PAGE_SIZE_BYTE;

			//擦除
			GD25QXX_EarseSomePages(dwFlashAddr, cPageNumber);

			//按页写入
			while(cPageNumber--)
			{
				GD25QXX_WriteBytes(dwFlashAddr, (UINT8 *)pcData, PAGE_SIZE_BYTE);

				dwFlashAddr += PAGE_SIZE_BYTE;
				pcData += PAGE_SIZE_BYTE;
			}
		}
		//超出当前扇区
		else
		{
			Flash_WriteCrossSector(dwFlashAddr, dwAddrSectorAligned, pcData, dwByteLength);
		}
	}

	return TRUE;
}

/*
 * Flash 根据操作模式写入
 * 输入参数：dwFlashAddr  	Flash写入的起始地址
 *        pcData      	缓存地址
 *        dwByteLength    写入字节长度
 *        cMode         写flash的模式，0：通过扇区写入,地址必须是扇区对齐的，否则直接返回
 *                                     该模式效率高，速度快，多余的空间写入ff。
 *                                   1：正常方式写入，即当数据不足一个扇区时，
 *                                     按照页进行写入，当数据超过一个扇区时，
 *                                     通过扇区写入加页写入的方式操作。
 * 输出参数：返回 TRUE 初始化成功   FALSE 初始化失败
 * 说明：如果起始地址不是页对齐，则直接退出
 * 	        写入Flash时以页为单位写入（256Bytes）,不足一页的会按一页写入
 * 	        所以使用时需注意当不足一页的会按一页写入会，超过写入长度的数据用0xff写入Flash
 * 	       会进行输入地址范围检测，写入地址和写入长度超过范围则失败退出  包括RAM地址范围检测和Flash地址范围检测
 * 	   Flash的扇区写入效率高于页写入，所以当写入Flash的范围包括一个完整的扇区使用扇区写入
 */
BOOL Flash_FastPageWrite(UINT32 dwFlashAddr, UINT8* pcData, UINT32 dwByteLength, UINT8 cMode)
{
	UINT16 wLen;
	UINT8 cSectorNumber;
	UINT8 cPageNumber;

	//长度为0，无需操作
	if(0 == dwByteLength)
	{
		return TRUE;
	}

	UINT32 dwLengthPageAligned = ((dwByteLength + PAGE_SIZE_BYTE - 1) & 0xFFFFFF00);//向高地址对齐
	UINT32 dwLengthSectorAligned = FLASH_SECTOR_ALIGN(dwByteLength);

	//地址范围检测
	if((FLASH_BOUNDARY_CHECK(dwFlashAddr)) || (FLASH_BOUNDARY_CHECK(dwFlashAddr + dwLengthPageAligned))
		|| (RAM_BOUNDARY_CHECK(pcData)) || (RAM_BOUNDARY_CHECK(pcData + dwLengthPageAligned)))
	{
		return FALSE;
	}

	//对齐地址
	UINT32 dwAddrPageAligned = FLASH_PAGE_ALIGN(dwFlashAddr);
	UINT32 dwAddrSectorAligned = FLASH_SECTOR_ALIGN(dwFlashAddr);

	//写入地址必须页对齐
	if(dwFlashAddr != dwAddrPageAligned)
	{
		return FALSE;
	}

	/* 无论数据长度位多少，均按扇区写入，速度快 */
	if(cMode == 0)
	{
		if(dwFlashAddr != dwAddrSectorAligned)                  /* 快速模式下，地址必须是扇区对齐的，否则直接退出 */
		{
			return FALSE;
		}

		wLen = dwByteLength - dwLengthSectorAligned;
		cSectorNumber = dwLengthSectorAligned / SECTOR_SIZE_BYTE;
		//按扇区写入
		while(cSectorNumber--)
		{
			GD25QXX_WriteSector(dwFlashAddr, (UINT8 *)pcData);

			//调整地址指针
			dwFlashAddr += SECTOR_SIZE_BYTE;
			pcData += SECTOR_SIZE_BYTE;

		}

		if(wLen > 0)
		{
			GD25QXX_EarseSector(dwFlashAddr);
			cPageNumber = wLen / PAGE_SIZE_BYTE;
			//按页写入
			while(cPageNumber--)
			{
				GD25QXX_WriteBytes(dwFlashAddr, (UINT8 *)pcData, PAGE_SIZE_BYTE);

				dwFlashAddr += PAGE_SIZE_BYTE;
				pcData += PAGE_SIZE_BYTE;
			}

			wLen = wLen % PAGE_SIZE_BYTE;
			if(wLen > 0)
			{
			    GD25QXX_WriteBytes(dwFlashAddr, (UINT8 *)pcData, wLen);
			}
		}
	}
	else if(cMode == 1)
	{
		//如果地址和长度都是扇区对齐
		if((dwFlashAddr == dwAddrSectorAligned)
			&& (dwByteLength == dwLengthSectorAligned))
		{
			cSectorNumber = dwByteLength / SECTOR_SIZE_BYTE;

			//按扇区写入
			while(cSectorNumber--)
			{
				GD25QXX_WriteSector(dwFlashAddr, (UINT8 *)pcData);

				//调整地址指针
				dwFlashAddr += SECTOR_SIZE_BYTE;
				pcData += SECTOR_SIZE_BYTE;

			}
		}
		//需要地址和长度都是页对齐
		else
		{
			//写入范围是否超出当前扇区
			if((dwFlashAddr + dwLengthPageAligned) <= (dwAddrSectorAligned + SECTOR_SIZE_BYTE))
			{
				cPageNumber = dwLengthPageAligned / PAGE_SIZE_BYTE;

				//擦除
				GD25QXX_EarseSomePages(dwFlashAddr, cPageNumber);

				//按页写入
				while(cPageNumber--)
				{
					GD25QXX_WriteBytes(dwFlashAddr, (UINT8 *)pcData, PAGE_SIZE_BYTE);

					dwFlashAddr += PAGE_SIZE_BYTE;
					pcData += PAGE_SIZE_BYTE;
				}
			}
			//超出当前扇区
			else
			{
				Flash_WriteCrossSector(dwFlashAddr, dwAddrSectorAligned, pcData, dwByteLength);
			}
		}
	}
	return TRUE;
}

/*
 * Flash 页擦除
 * 输入参数：AddressInPage 页地址
 * 输出参数：返回 TRUE 初始化成功   FALSE 初始化失败
 */
BOOL Flash_PageErase(UINT32 dwAddressInPage)
{
    GD25QXX_EarseSomePages(dwAddressInPage, 1);
    return TRUE;
}

/*
 * Flash 扇区擦除
 * 输入参数：AddressInPage 地址
 * 输出参数：返回 TRUE 初始化成功   FALSE 初始化失败
 */
BOOL Flash_SectocErase(UINT32 dwAddressInPage)
{
    GD25QXX_EarseSector(dwAddressInPage);
    return TRUE;
}

/*
 * Flash  片擦除
 * 输入参数：无
 * 输出参数：返回 TRUE 初始化成功   FALSE 初始化失败
 */
BOOL Flash_ChipErase(void)
{
    GD25QXX_EarseChip();
    return TRUE;
}

/*
 * Flash 读取指定长度数据 (Byte)
 * 输入参数：AddressRead Flash地址
 *        pcData 缓存地址
 *        size   读取数据长度
 * 输出参数：返回 TRUE 初始化成功   FALSE 初始化失败
 */
BOOL Flash_ReadBytes(UINT32 dwAddressRead, UINT8 *pcData, UINT32 dwSize)
{
	//地址范围检测
	if((FLASH_BOUNDARY_CHECK(dwAddressRead)) || (FLASH_BOUNDARY_CHECK(dwAddressRead + dwSize))
		|| (RAM_BOUNDARY_CHECK(pcData)) || (RAM_BOUNDARY_CHECK(pcData + dwSize)))
	{
		return FALSE;
	}

	//扇区读取
	while(dwSize > SECTOR_SIZE_BYTE)
	{
		GD25QXX_ReadBytes(pcData, dwAddressRead, SECTOR_SIZE_BYTE);

		dwAddressRead += SECTOR_SIZE_BYTE;
		pcData += SECTOR_SIZE_BYTE;
		dwSize -= SECTOR_SIZE_BYTE;
	}

	GD25QXX_ReadBytes(pcData, dwAddressRead, dwSize);

    return TRUE;
}

//无用函数 调用的地方需改为memcpy
BOOL Flash_WriteData( UINT32 ToFlashAddr, void *pcData, UINT16 datasize )
{

    memcpy((void *)ToFlashAddr, pcData, datasize);

    return TRUE;
}

/* 说明：将RAM中的数据写入到Flash中，主要用于组态和设备配置写入Flash，写入数据单位为Flash块
 * 参数：dwFlashAddr：Flash中的 起始地址
	   pcData：RAM起始地址
	   dwNumberByte：写入的字节数
 * 返回：TURE、Flash
 */
BOOL RAMToFlash( UINT32 dwFlashAddr, UINT8* pcData, UINT32 dwNumberByte )
{
	return Flash_PageWrite(dwFlashAddr, pcData, dwNumberByte);
}

/* 说明：将Flash中的数据写入到RAM中，主要用于Flash中组态和设备配置写入RAM，写入数据单位为Flash块
 * 参数：ToFlashAddr：Flash中的 起始地址
	   pcData：RAM起始地址
	   Num_Bity：写入的字节数量
 * 返回：TURE、Flash
 */
BOOL FlashToRAM( UINT32 dwFlashAddr, UINT8* pcData, UINT32 dwNumberByte )
{
	return Flash_ReadBytes(dwFlashAddr, pcData, dwNumberByte);
}

/* 说明：根据配置参数配置FLASH（只需配置flash使用的spi1即可）
 * 参数：pFlashInfo: flash配置信息结构体
 * 返回：
 */
void Flash_Config(PFlash_InitStructure pFlashInfo)
{
	SPI_InitStructure SPI_InitInfo;

	SPI_InitInfo.SPI_BaseAddr = pFlashInfo->SPI_BaseAddr;
	SPI_InitInfo.baudrate   = pFlashInfo->baudrate;//50K
	SPI_InitInfo.datawidth  = pFlashInfo->datawidth;
	SPI_InitInfo.cs         = pFlashInfo->cs;
	SPI_InitInfo.polarity   = pFlashInfo->polarity;
	SPI_InitInfo.phase      = pFlashInfo->phase;
	SPI_InitInfo.TxFTLevel  = pFlashInfo->TxFTLevel;
	SPI_InitInfo.RxFTLevel  = pFlashInfo->RxFTLevel;
	SPI_InitInfo.intrmask   = pFlashInfo->intrmask;

	SPI_Config(&SPI_InitInfo);

	GD25QXX_AdaptDefaultMode();
}
/*****************************end Flash 通用函数****************************/
