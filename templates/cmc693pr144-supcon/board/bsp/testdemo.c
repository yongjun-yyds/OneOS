/*
 * test_demo.c
 *
 *  Created on: 2018-11-1
 *      Author: zhulin2
 */
#if 0
#include "datatype.h"
#include "gpio.h"
#include "flash.h"
#include "i2c.h"
#include "lib_spi.h"
#include "uart.h"
#include "bsp.h"
#include "lib_timer.h"
#include "ApiRTC.h"
#include "lib_can.h"
#include "powm.h"
#include "wdt.h"
#include "netif/dmac.h"
#include "adapter.h"
#include "lwip/ip_addr.h"
#include "lwip/netif.h"
#include "netif/etharp.h"
#include "lwip/init.h"
#include "lwip/tcp.h"
#include "lwip/udp.h"
#include "lwip/inet.h"
#include "string.h"

extern INT32U wdTimeFlag;

struct netif lwip_netif1,lwip_netif2;

extern void ethernetif_input(struct netif *netif);

#define DELAY(t) {int i; for(i=0;i<t;i++);}

/* gpio功能测试 */
void GPIO1A_Pin31_Reversal(void)
{
	static UINT8 cSta = 0;

	GPIO_WritePins(GPIO1, PORTA, GPIO_P28, cSta);
	cSta = ~cSta;
}

void GPIO_Test(void)
{
	UINT32 v;

	while(1)
	{
		if(GPIO_ReadInputPinData(GPIO1, PORTA, GPIO_P30) == 0)
		{
			Uart0Printf("input\r\n");
			v = GPIO_ReadInputPortData(GPIO1,PORTA);

		}
	}
}
/* gpio功能测试结束 */

/* CAN测试，可在bsp的can初始化中选择使用CAN0还是CAN1 */
void CAN_test(void)
{
	CANFrameInfo_st recvCan;

	while(1)
	{
		if(sizeof(CANFrameInfo_st) == CAN_CQ_PopFrame(pCAN1, &recvCan))   /* 接收到有效数据 */
		{
			CAN_Transmit(pCAN1, &recvCan);
		}
	}
}
/* CAN测试结束 */

/* spi0 ADC测试（ADS1148）,且ADC支持的spi频率最高100k  */
void SPI0_ADC_test(void)
{
	UINT8 cSendLen = 4;                           /* 在读取ADC寄存器值时，需要在SPI总线上发送的字节个数，
	                                                                                                                          其中1个字节的命令和1个字节读取寄存器的个数，以及2个nop指令
	                                                                                                                          由于只有在发送数据时才会产生时钟，所以当发送完读命令和读取寄存器
	                                                                                                                         个数的字节后，需要继续发送两个nop，用于产生时钟接收读取的数据 */
	UINT8 cWriteBuf[12] = {0x40,0x01,0xc1,0x01};  /* 写ADC寄存器命令，0x40表示写命令以及所写寄存器地址为0，
	                                                 0x01表示要写入的字节数减1，实际写两个字节，
	                                                 0xc1表示向寄存器0写入的值
	                                                 0x01表示向寄存器1写入的值 */
	UINT8 cSendBuf[12] = {0x20,0x01,0xff,0xff};   /* 读取ADC寄存器的命令，当需要读取ADC指定寄存器的值时，需要先
	                                                                                                                          写入该命令，0x20表示读命令以及所要读取的寄存器地址为0，
	                                                 0x01表示要读取的字节数减1，实际读取两个字节 */
	UINT8 cRecvBuf[10];

	while(1)
	{
		/* 若需要读写ADS1148寄存器，必须将ADS1148的START拉高，若值进行RDATA、RDATAC、SDATAC、WAKEUP、NOP命令时，可不要拉高start */
//		cWriteBuf[3] = 0x01;
//		cWriteBuf[2] = 0xc0;
//		memset(cRecvBuf,0,10);
//		SPI_TxRx(&SPI,SPI_CS0,cWriteBuf,cRecvBuf,4);                  /* 写ADS1148寄存器0，写连续两个寄存器 */
//		memset(cRecvBuf,0,10);
//		SPI_TxRx(&SPI,SPI_CS0,cSendBuf,cRecvBuf,cSendLen);            /* 读ADS1148寄存器0，连续读两个寄存器 */
//
//		cWriteBuf[3] = 0x03;
//		cWriteBuf[2] = 0xc1;
//		memset(cRecvBuf,0,10);
//		SPI_TxRx(&SPI,SPI_CS0,cWriteBuf,cRecvBuf,4);                  /* 写ADS1148寄存器0，写连续两个寄存器 */
//		memset(cRecvBuf,0,10);
//		SPI_TxRx(&SPI,SPI_CS0,cSendBuf,cRecvBuf,cSendLen);

		/* DMA方式进行SPI通信 */
		cWriteBuf[3] = 0x01;
		cWriteBuf[2] = 0xc0;
		memset(cRecvBuf,0,10);
		SPI_TxRx_DMA(&SPI,SPI_CS0,cWriteBuf,cRecvBuf,4,NULL);                  /* 写ADS1148寄存器0，写连续两个寄存器 */
		memset(cRecvBuf,0,10);
		SPI_TxRx_DMA(&SPI,SPI_CS0,cSendBuf,cRecvBuf,cSendLen,NULL);            /* 读ADS1148寄存器0，连续读两个寄存器 */

		cWriteBuf[3] = 0x03;
		cWriteBuf[2] = 0xc1;
		memset(cRecvBuf,0,10);
		SPI_TxRx_DMA(&SPI,SPI_CS0,cWriteBuf,cRecvBuf,4,NULL);                  /* 写ADS1148寄存器0，写连续两个寄存器 */
		memset(cRecvBuf,0,10);
		SPI_TxRx_DMA(&SPI,SPI_CS0,cSendBuf,cRecvBuf,cSendLen,NULL);
	}
}
/* spi0 ADC测试结束 */

/* spi1 Flash测试，由于该flash为芯片内置的，而spi1专用于该flash，不需要引脚复用，直接初始化spi1即可 */
#define DATA_LEN 200
void SPI1_Flash_test(void)
{
	UINT32 dwFlashAddr = 0x00;
	UINT8 cDataBuf[DATA_LEN];
	UINT8 cReadBuf[DATA_LEN];
	UINT16 cDataLen = DATA_LEN;
    UINT16 i;


	Flash_ReadBytes(dwFlashAddr,cReadBuf,cDataLen);               /* 读flash地址为0的20个数据*/
	for(i=0;i<DATA_LEN;i++ )
	{
		cDataBuf[i] = i;
	}
	Flash_PageWrite(dwFlashAddr,cDataBuf,cDataLen);               /* 写flash */
//	Flash_FastPageWrite(dwFlashAddr,cDataBuf,cDataLen,1);         /* 快速写flash */
	Flash_ReadBytes(dwFlashAddr,cReadBuf,cDataLen);               /* 读flash地址为0的20个数据*/
	for(i=0; i<DATA_LEN; i++)
	{
		if(cReadBuf[i] != cDataBuf[i])
		{
			asm("nop");
		}
	}

	for(i=0;i<DATA_LEN;i++)
	{
		cDataBuf[i] = ~i;
	}

	Flash_PageWrite(dwFlashAddr,cDataBuf,cDataLen);               /* 写flash */
//	Flash_FastPageWrite(dwFlashAddr,cDataBuf,cDataLen,1);         /* 快速写flash */
	Flash_ReadBytes(dwFlashAddr,cReadBuf,cDataLen);               /* 读flash地址为0的20个数据*/
	for(i=0; i<DATA_LEN; i++)
	{
		if(cReadBuf[i] != cDataBuf[i])
		{
			asm("nop");
		}
	}

	while(1);                                                     /* 上述过程执行完，死循环在这里，以防多次擦除flash，
	                                                                                                                                                                   导致flash报废 */
}

/* IIC测试开始 */
/* 与IIC通讯的从设备为母版上的外部RTC芯片5c372a */
void IIC_Test(void)
{
	UINT8 RTC_Recv[8] = {0};
	UINT8 RTC_Send[2] = {0xe0,0x31};    /* RTC_Send[0]为地址信息，其中的高4位为RTC寄存器地址，
	                                                                                                 低4位为传输格式寄存器，RTC_Send[1]为要写入的数据 */
	UINT8 j = 0;

	/* 该模块仅仅用于测试IIC是否能够正常通讯，这里只要能够读出正确数据，和写入正确数据即可，不管RTC的实际配置 */
	while(1)
	{
		for(j=0;j<8;j++)
		{
			RTC_Recv[j] = 0;
		}

		/* 读取RTC从0xe开始的8个寄存器值，需要先写入1个字节的寄存器地址，然后读出8个寄存器值 */
		IIC_Transceive_Interrupt(&IIC0,0x32,RTC_Send,1,RTC_Recv,8,NULL);         /* 中断方式，需要在初始化IIC时打开中断配置  */
//		IIC_Transceive(&IIC0,0x32,RTC_Send,1,RTC_Recv,8);                        /* 查询方式，需要在初始化IIC时关闭中断配置  */
		/* 延时作用是等待中断读取数据完成 */
		DELAY(80000);

		/* 向指定寄存器写数据 */
		IIC_Transceive_Interrupt(&IIC0,0x32,RTC_Send,2,NULL,0,NULL);             /* 中断方式，需要在初始化IIC时打开中断配置  */
//		IIC_Transceive(&IIC0,0x32,RTC_Send,2,NULL,0);                            /* 查询方式 ，需要在初始化IIC时关闭中断配置 */
		DELAY(80000);
	}
}
/* IIC测试结束 */

/* UART测试 */
void UART_Test(void)
{
	UINT8 cData;

    while(1)
    {
		while(Uart_GetChar(UART0,&cData) != FAILURE)
		{
			Uart0Printf("%x ",cData);
		}
    }
}
/* UART测试结束 */

/* 低功耗测试 */
void LowPower_test(void)
{
	Uart0Printf("test start!\r\n");
	while(1)
	{
#if 0
		/* 进入WAIT模式，可通过GPIO0 PORTA的pin0中断唤醒MCU或者RTC或者timer */
		Uart0Printf("wait!\r\n");
		GPIO_EnableInterrupt(GPIO0, GPIO_P0);                 /* 通过GPIO0 PORTA的pin0中断唤醒MCU */
		POWM_ToLowPower(POWM_LP_WAIT,0xfffffb);
		DELAY(1000000);                                       /* 等待唤醒后稳定 */
		Uart0Printf("run!\r\n");
		DELAY(1000000);                                       /* 等待一段时间重新进入WAIT模式 */
#endif

#if 0
		/* 进入DOZE模式，可通过GPIO0 PORTA的pin0中断唤醒MCU或者RTC或者timer */
		Uart0Printf("doze!\r\n");
		GPIO_EnableInterrupt(GPIO0, GPIO_P0);                 /* 通过GPIO0 PORTA的pin0中断唤醒MCU */
		POWM_ToLowPower(POWM_LP_DOZE,0xfffffb);
		DELAY(1000000);                                       /* 等待唤醒后稳定 */
		Uart0Printf("run!\r\n");
		DELAY(1000000);                                       /* 等待一段时间重新进入DOZE模式 */
#endif

#if 1
		/* 进入STOP模式，可通过STOP计数器或者GPIO0 PORTA的pin0中断唤醒MCU，仅支持电平触发中断唤醒 */
		Uart0Printf("stop!\r\n");
		POWM_StopAwake(100000000);                            /* 通过STOP计数器唤醒MCU */
		GPIO_EnableInterrupt(GPIO0, GPIO_P0);                 /* 通过GPIO0 PORTA的pin0中断唤醒MCU */
		POWM_ToLowPower(POWM_LP_STOP,0xfffffb);
		DELAY(1000000);                                       /* 等待唤醒后稳定 */
		Uart0Printf("run!\r\n");
		DELAY(1000000);                                       /* 等待一段时间重新进入STOP模式 */
#endif
	}
}
/* 低功耗测试结束 */

/* RTC测试 */
static void RTC_GetTime(void)
{
	Struct_TimeInfo TimerPara;

	if(wdTimeFlag)
	{
		wdTimeFlag = 0;
		RTC_GetRealTime(&TimerPara);
		Uart0Printf("%d-%d-%d %d:%d:%d %d\r\n",TimerPara.wYear,TimerPara.cMonth,\
				TimerPara.cDay,TimerPara.cHour,TimerPara.cMinute,TimerPara.cSecond,\
				TimerPara.cWeek);
	}
}

UINT8 cTimeArr[14];
UINT8 cAlarmArr[14];
static void Set_RTC(void)
{
	UINT8 cData,i=0;
	Struct_TimeInfo TimerPara;

	while(Uart_GetChar(UART0,&cData) != FAILURE)    /* 通过串口设置当前时间和闹钟 */
	{
		if(cData == '.')                            /* 通过串口输入“yyyymmddhhmmss.”来设置当前时间 */
		{
			TimerPara.wYear = (cTimeArr[0]-0x30)*1000+(cTimeArr[1]-0x30)*100\
					+(cTimeArr[2]-0x30)*10+(cTimeArr[3]-0x30);
			TimerPara.cMonth = (cTimeArr[4]-0x30)*10+(cTimeArr[5]-0x30);
			TimerPara.cDay = (cTimeArr[6]-0x30)*10+(cTimeArr[7]-0x30);
			TimerPara.cHour = (cTimeArr[8]-0x30)*10+(cTimeArr[9]-0x30);
			TimerPara.cMinute = (cTimeArr[10]-0x30)*10+(cTimeArr[11]-0x30);
			TimerPara.cSecond = (cTimeArr[12]-0x30)*10+(cTimeArr[13]-0x30);
			RTC_SetRealTime(&TimerPara);
			break;
		}

		if(cData == '*')                            /* 通过串口输入“yyyymmddhhmmss*”来设置闹钟 */
		{
			TimerPara.wYear = (cTimeArr[0]-0x30)*1000+(cTimeArr[1]-0x30)*100\
					+(cTimeArr[2]-0x30)*10+(cTimeArr[3]-0x30);
			TimerPara.cMonth = (cTimeArr[4]-0x30)*10+(cTimeArr[5]-0x30);
			TimerPara.cDay = (cTimeArr[6]-0x30)*10+(cTimeArr[7]-0x30);
			TimerPara.cHour = (cTimeArr[8]-0x30)*10+(cTimeArr[9]-0x30);
			TimerPara.cMinute = (cTimeArr[10]-0x30)*10+(cTimeArr[11]-0x30);
			TimerPara.cSecond = (cTimeArr[12]-0x30)*10+(cTimeArr[13]-0x30);
			RTC_SetAlarmTime(&TimerPara);
			break;
		}
		cTimeArr[i++] = cData;
		Uart0Printf("%x ",cData);
	}
}

void RTC_Test(void)
{
    Uart0Printf("test start!\r\n");

    while(1)
    {
	    RTC_GetTime();
	    Set_RTC();
    }
}
/* RTC测试结束 */

/* WDT测试，WDT的初始化配置在bsp中，可选择两种模式，中断模式下的中断处理函数也在bsp中 */
void WDT_Test(void)
{
	Uart0Printf("system!\r\n");
	while(1)
	{
		DELAY(10000);
//		WDT_Restart();                          /* 喂狗 */
	}
}
/* WDT测试结束 */


/* lwip测试开始 */
unsigned char eth_mac_address[2][6] = {
		{0x00, 0x0a, 0x35, 0x00, 0x01, 0x01},       /* MAC0地址 */
		{0x00, 0x0a, 0x35, 0x00, 0x01, 0x02}};      /* MAC1地址 */
void LwipInit(void)
{
	struct ip_addr ipaddr,ipaddr1,netmask,gw;

	lwip_init();

	IP4_ADDR(&gw,10,30,23,1);
	IP4_ADDR(&ipaddr,10,30,23,53);
	IP4_ADDR(&ipaddr1,10,30,23,54);
	IP4_ADDR(&netmask,255,255,255,0);

    xemac_add(&lwip_netif1, &ipaddr, &netmask, &gw, eth_mac_address[0], MAC0_BASE_ADDR);
    xemac_add(&lwip_netif2, &ipaddr1, &netmask, &gw, eth_mac_address[1], MAC1_BASE_ADDR);
	netif_set_default(&lwip_netif1);
	netif_set_up(&lwip_netif1);
	netif_set_up(&lwip_netif2);
}

err_t recv_callback(void *arg, struct tcp_pcb *tpcb,
                               struct pbuf *p, err_t err)
{
	/* do not read the packet if we are not in ESTABLISHED state */
	if (!p) {
		tcp_close(tpcb);
		tcp_recv(tpcb, NULL);
		return ERR_OK;
	}

	/* indicate that the packet has been received */
	tcp_recved(tpcb, p->len);

	/* echo back the payload */
	/* in this case, we assume that the payload is < TCP_SND_BUF */
	if (tcp_sndbuf(tpcb) > p->len) {
		err = tcp_write(tpcb, p->payload, p->len, 1);
	} else
		Uart0Printf("no space in tcp_sndbuf\n\r");

	/* free the received pbuf */
	pbuf_free(p);

	return ERR_OK;
}

err_t accept_callback(void *arg, struct tcp_pcb *newpcb, err_t err)
{
	static UINT32 connection = 1;

	tcp_recv(newpcb, recv_callback);

	/* just use an integer number indicating the connection id as the
	   callback argument */
	tcp_arg(newpcb, (void*)connection);

	/* increment for subsequent accepted connections */
	connection++;

	return ERR_OK;
}

void TCPServer_Start_application(void)
{
	struct tcp_pcb *pcb1,*pcb2;
	ip_addr_t ipAddr1,ipAddr2;
	err_t err;
    UINT16 wPort2=5002,wPort1=5001;

    ipAddr1.addr = inet_addr("10.30.23.53");
    ipAddr2.addr = inet_addr("10.30.23.54");

    /* ip10.30.23.53，TCP初始化，端口为5001 */
	pcb1 = tcp_new();
	if(!pcb1)
	{
		Uart0Printf("Error creating PCB. Out of Memory\n\r");
		return ;
	}
//	err = tcp_bind(pcb1,IP_ADDR_ANY,wPort1);
	err = tcp_bind(pcb1,&ipAddr1,wPort1);
	if(err!=ERR_OK)
	{
		Uart0Printf("Unable to bind to port %d: err = %d\n\r", wPort1, err);
		return ;
	}
	tcp_arg(pcb1,NULL);
	pcb1 = tcp_listen(pcb1);
	if(!pcb1)
	{
		Uart0Printf("Out of memory while tcp_listen\n\r");
		return ;
	}
	tcp_accept(pcb1,accept_callback);
	Uart0Printf("TCP echo server started @ port %d\n\r",wPort1);

    /* ip10.30.23.54，TCP初始化，端口为5002 */
	pcb2 = tcp_new();

	if(!pcb2)
	{
		Uart0Printf("Error creating PCB. Out of Memory\n\r");
		return ;
	}

//	err = tcp_bind(pcb2,IP_ADDR_ANY,wPort2);
	err = tcp_bind(pcb2,&ipAddr2,wPort2);
	if(err!=ERR_OK)
	{
		Uart0Printf("Unable to bind to port %d: err = %d\n\r", wPort2, err);
		return ;
	}

	tcp_arg(pcb2,NULL);

	pcb2 = tcp_listen(pcb2);
	if(!pcb2)
	{
		Uart0Printf("Out of memory while tcp_listen\n\r");
		return ;
	}

	tcp_accept(pcb2,accept_callback);

	Uart0Printf("TCP echo server started @ port %d\n\r",wPort2);
}

void UDP_Callback(void *arg, struct udp_pcb *pcb, struct pbuf *p,
	    ip_addr_t *addr, u16_t port)
{
	udp_sendto(pcb, p, addr, port);
	pbuf_free(p);
}

void UDP_Start_application(void)
{
	struct udp_pcb *pcb1,*pcb2;
	ip_addr_t ipAddr1,ipAddr2;
	err_t err;
    UINT16 wPort2=5002,wPort1=5001;

    ipAddr1.addr = inet_addr("10.30.23.53");
    ipAddr2.addr = inet_addr("10.30.23.54");

    /* MAC0网口，ip10.30.23.53，UDP初始化，端口为5001 */
	pcb1 = udp_new();
	if(!pcb1)
	{
		Uart0Printf("Error creating PCB. Out of Memory\n\r");
		return ;
	}
//	err = tcp_bind(pcb1,IP_ADDR_ANY,wPort1);
	err = udp_bind(pcb1,&ipAddr1,wPort1);

	if(err!=ERR_OK)
	{
		Uart0Printf("Unable to bind to port %d: err = %d\n\r", wPort1, err);
		return ;
	}
	udp_recv(pcb1, UDP_Callback, NULL);

    /* MAC1网口，ip10.30.23.54，UDP初始化，端口为5002 */
	pcb2 = udp_new();
	if(!pcb2)
	{
		Uart0Printf("Error creating PCB. Out of Memory\n\r");
		return ;
	}
//	err = tcp_bind(pcb2,IP_ADDR_ANY,wPort2);
	err = udp_bind(pcb2,&ipAddr2,wPort2);
	if(err!=ERR_OK)
	{
		Uart0Printf("Unable to bind to port %d: err = %d\n\r", wPort2, err);
		return ;
	}
	udp_recv(pcb2, UDP_Callback, NULL);
}

/* 使用TCP服务器模式进行测试，两个网口独立运行 */
void Lwip_Test(void)
{
	LwipInit();
//	TCPServer_Start_application();
	UDP_Start_application();
	while(1)
	{
		ethernetif_input(&lwip_netif1);
		ethernetif_input(&lwip_netif2);
	}
}
/* lwip测试结束 */


#endif





