/*
 文件: flash.h
 版本: CMC693 V1.10
 版权: 浙江杰芯科技有限公司
 作者: 喻文星 (yuwenxing@supcon.com)
 描述：Flash是GD25QXX系列
	 目前使用的Flash大小为2M通过SPI1进行通信
	 此Flash有扇区、块、整片擦除的命令，没有页擦除的命令；页擦除是先将整个扇区的内容拷出来，再扇区擦除，最会再将所有数据写回
	所以效率很低，尽量使用扇区写入

修改记录：
=========
日期                                      作者                       工作                            内容
-------------------------------------------------------
2018-09-27    喻文星                              修改                           规范编程风格
*/
#ifndef __FLASH_H_
#define __FLASH_H_

#include "datatype.h"
#include "lib_spi.h"

/* 大小定义 */
#define PAGE_SIZE_BYTE     			GD25QXX_PAGE_BYTE
#define SECTOR_SIZE_BYTE			GD25QXX_SECTOR_BYTE
#define BLOCK_SIZE_BYTE				GD25QXX_BLOCK_BYTE

#define PAGE_SIZE_WORD      		(PAGE_SIZE_BYTE / 4)

/* 地址范围 */
#define FLASH_PAGE_SIZE             (256)           //Flash一页字节数
#define FLASH_START_ADDR            (0x00000)
#define FLASH_END_ADDR              (0x200000)
#define RAM_START_ADDR              (0x100000)      //RAM起始地址
#define RAM_END_ADDR                (0x180000)      //RAM结束地址

/*地址对其*/
#define FLASH_PAGE_ALIGN(Addr)		((Addr) & 0xFFFFFF00)
#define FLASH_SECTOR_ALIGN(Addr)	((Addr) & 0xFFFFF000)

/*地址判断*/
#define FLASH_BOUNDARY_CHECK(Addr)		((Addr) > FLASH_END_ADDR)
#define RAM_BOUNDARY_CHECK(Addr)		(((UINT32)(Addr) < RAM_START_ADDR) || ((UINT32)(Addr) > RAM_END_ADDR))

//命令码的定义
#define GD25QXX_WRITE_ENABLE            (0x06)
#define GD25QXX_WRITE_DISABLE           (0x04)
#define GD25QXX_STATUSREG_WRITE_ENABLE  (0x50)
#define GD25QXX_STATUSREG_L_READ_ENABLE (0x05)
#define GD25QXX_STATUSREG_H_READ_ENABLE (0x35)
#define GD25QXX_STATUSREG_WRITE         (0x01)

#define GD25QXX_READ_BYTES              (0x03)
#define GD25QXX_WRITE_BYTES             (0x02)
#define GD25QXX_SECTOR_EARSE            (0x20)
#define GD25QXX_BLOCK_EARSE             (0x52)
#define GD25QXX_CHIP_EARSE              (0xC7)

#define GD25QXX_RESET_ENABLE            (0x66)
#define GD25QXX_DEVICE_RESET            (0x99)

#define GD25QXX_SUSPEND                 (0x75)
#define GD25QXX_RESUME                  (0x7A)

#define GD25QXX_POWERDOWN               (0xB9)
#define GD25QXX_WAKEUP                  (0xAB)

#define GD25QXX_READ_DID                (0x90)
#define GD25QXX_READ_UID                (0x4B)
#define GD25QXX_READ_ID                 (0x9F)

#define GD25QXX_SECURITYREG_EARSE       (0x44)
#define GD25QXX_SECURITYREG_WRITE       (0x42)
#define GD25QXX_SECURITYREG_READ        (0x48)


#define GD25QXX_DUMMY_BYTE              (0xFF)      /*假读一个字节*/
#define GD25QXX_PAGE_BYTE               (0x100)     /*页大小*/
#define GD25QXX_SECTOR_BYTE             (0x1000)    /*扇区字节大小*/
#define GD25QXX_BLOCK_BYTE              (0x8000)    /*块大小*/
#define GD25QXX_FREE_BYTE               (0xFF)      /*擦除之后空闲字节为0xFF*/
#define GD25QXX_FREE_DWORD              (0xFFFFFFFF)/*擦除之后空闲32位为0xFFFFFFFF*/

#define GD25QXX_INPAGE_OFFSET(Addr)     ( (Addr) & (GD25QXX_PAGE_BYTE - 1) )//页内偏移
#define GD25QXX_PAGECOUNT(DataSize)     ( ((DataSize) + GD25QXX_PAGE_BYTE - 1) >> 8)//页数

//按页/段对齐
#define GD25QXX_PAGE_ALIGN(Addr)        (Addr) -= ( (Addr) & (GD25QXX_PAGE_BYTE   - 1) )
#define GD25QXX_SECTOR_ALIGN(Addr)      (Addr) -= ( (Addr) & (GD25QXX_SECTOR_BYTE - 1) )
#define GD25QXX_BLOCK_ALIGN(Addr)       (Addr) -= ( (Addr) & (GD25QXX_BLOCK_BYTE  - 1) )

//Flash片内转存
#define GD25QXX_UNLOAD_EN               (0)

//定义转存区域的地址
#if GD25QXX_UNLOAD_EN
    #define GD25QXX_UNLOAD_BASEADDR     (0x0D000)
#endif

//空间节约 关闭不必须的功能
#define GD25QXX_SPACE_SAVE_NO           (0)

//GD25QXX的状态寄存器定义
typedef struct
{
    union
    {
        UINT8 StateReg_L;
        struct
        {
            UINT8   WIP  : 1;// 0 [Read-Only] Write in Progress 1表示正在进行写操作.包含写入、擦除、写状态寄存器。
            UINT8   WEL  : 1;// 1 写使能信号，为1时可写入
            UINT8   BP   : 5;// 2 [non-volatile]块保护位，用于防止保护区域被意外删除。DataSheet Table1
            UINT8   SRP0 : 1;// 7 [non-volatile]状态寄存器保护位0
        };
    };

    union
    {
        UINT8 StateReg_H;
        struct
        {
            UINT8   SRP1 : 1;// 8 [non-volatile]状态寄存器保护位1
            UINT8   QE   : 1;// 9 [non-volatile] Quad Enable
            UINT8   LB   : 1;// 10[non-volatile]只允许写一次，OTP，写入1之后状态寄存器变成永久只读
            UINT8   RSD  : 2;
            UINT8   HPM  : 1;// 13 指示是否进入高性能模式
            UINT8   CMP  : 1;// 14 [non-volatile] 配合BP0:BP4进行操作，主要是决定选中上半部分还是下半部分。
            UINT8   SUS  : 1;// 15 [Read-Only]只读寄存器，用于指示写入/擦除过程是否挂起。
        };
    };

}GD25QXX_Status_St, *pGD25QXX_Status_St;

/* flash初始化只需配置好所使用的SPI即可 */
typedef struct{
	UINT32                          SPI_BaseAddr;   //SPI基地址
    UINT32                          baudrate;       //SPI 波特率
    Enum_SPI_DataWidth              datawidth;      //SPI的数据宽度类型8/16 Bit
    Enum_SPI_CS                     cs;             //片选选择
    Enum_SPI_Polarity               polarity;       //极性
    Enum_SPI_Phase                  phase;          //相位
    UINT8                           TxFTLevel;      //发送缓冲区长度。
    UINT8                           RxFTLevel;      //接收缓冲区长度。
    UINT8                           intrmask;       //中断屏蔽配置
}Flash_InitStructure,*PFlash_InitStructure;

extern pStruct_SPIInfo pFlashSpi;

/* 说明：根据配置参数配置FLASH（只需配置flash使用的spi即可）
 * 参数：pFlashInfo: flash配置信息结构体
 * 返回：
 */
void Flash_Config(PFlash_InitStructure pFlashInfo);

/*
 * 等待进入Flash进入空闲状态
 * 输入参数：无
 * 输出参数：返回 TRUE 初始化成功   FALSE 初始化失败
 */
BOOL WaitFlashIdle(void);

/*
 * Flash 按页写入
 * 输入参数：FlashAddr  	Flash写入的起始地址
 *        pdata      	缓存地址
 *        ByteLength    写入字节长度
 * 输出参数：返回 TRUE 初始化成功   FALSE 初始化失败
 * 说明：如果起始地址不是页对齐，则直接退出
 * 	        写入Flash时以页为单位写入（256Bytes）,不足一页的会按一页写入
 * 	        所以使用时需注意当不足一页的会按一页写入会导致读RAM中超出ByteLength长度的空间继续写入Flash
 * 	       会进行输入地址范围检测，写入地址和写入长度超过范围则失败退出  包括RAM地址范围检测和Flash地址范围检测
 * 	   Flash的扇区写入效率高于页写入，所以当写入Flash的范围包括一个完整的扇区使用扇区写入
 */
BOOL Flash_PageWrite(UINT32 FlashAddr, UINT8* pdata, UINT32 ByteLength);

/*
 * Flash 根据操作模式写入
 * 输入参数：dwFlashAddr  	Flash写入的起始地址
 *        pcData      	缓存地址
 *        dwByteLength    写入字节长度
 *        cMode         写flash的模式，0：通过扇区写入，即使所写数据不足一个扇区，
 *                                     该模式效率高，速度快，多余的空间写入ff。
 *                                   1：正常方式写入，即当数据不足一个扇区时，
 *                                     按照页进行写入，当数据超过一个扇区时，
 *                                     通过扇区写入加页写入的方式操作。
 * 输出参数：返回 TRUE 初始化成功   FALSE 初始化失败
 * 说明：如果起始地址不是页对齐，则直接退出
 * 	        写入Flash时以页为单位写入（256Bytes）,不足一页的会按一页写入
 * 	        所以使用时需注意当不足一页的会按一页写入会，超过写入长度的数据用0xff写入Flash
 * 	       会进行输入地址范围检测，写入地址和写入长度超过范围则失败退出  包括RAM地址范围检测和Flash地址范围检测
 * 	   Flash的扇区写入效率高于页写入，所以当写入Flash的范围包括一个完整的扇区使用扇区写入
 */
BOOL Flash_FastPageWrite(UINT32 dwFlashAddr, UINT8* pcData, UINT32 dwByteLength, UINT8 cMode);

/*
 * Flash 读取指定长度数据 (Byte)
 * 输入参数：AddressRead Flash地址
 *        pbData 缓存地址
 *        size   读取数据长度
 * 输出参数：返回 TRUE 初始化成功   FALSE 初始化失败
 */
BOOL Flash_ReadBytes(INT32U AddressRead, INT8U *pbData, UINT32 size);

/*
 * Flash 页擦除
 * 输入参数：AddressInPage 页地址
 * 输出参数：返回 TRUE 初始化成功   FALSE 初始化失败
 */
BOOL Flash_PageErase(INT32U AddressInPage);

/*
 * Flash 扇区擦除
 * 输入参数：AddressInPage 地址
 * 输出参数：返回 TRUE 初始化成功   FALSE 初始化失败
 */
BOOL Flash_SectocErase(INT32U AddressInPage);

/*
 * Flash  片擦除
 * 输入参数：无
 * 输出参数：返回 TRUE 初始化成功   FALSE 初始化失败
 */
BOOL Flash_ChipErase(void);

/*无用函数 调用的地方需改为memcpy*/
BOOL Flash_WriteData( UINT32 addr, void *pdata, UINT16 size );

/* 说明：将RAM中的数据写入到Flash中，主要用于组态和设备配置写入Flash，写入数据单位为Flash块
 * 参数：ToFlashAddr：Flash中的 起始地址
	   pdata：RAM起始地址
	   Num_Bity：写入的字节数
 * 返回：TURE、Flash
 */
BOOL RAMToFlash( UINT32 ToFlashAddr, UINT8* pdata, UINT32 Num_Byte );

/* 说明：将Flash中的数据写入到RAM中，主要用于Flash中组态和设备配置写入RAM，写入数据单位为Flash块
 * 参数：ToFlashAddr：Flash中的 起始地址
	   pdata：RAM起始地址
	   Num_Bity：写入的字节数量
 * 返回：TURE、Flash
 */
BOOL FlashToRAM( UINT32 FlashAddr, UINT8* Topdata, UINT32 Num_Byte );

#endif /* FLASH_H_ */
