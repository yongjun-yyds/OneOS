/*
 文件: ApiRTC.h
 版本: CMC693 V1.10
 版权: 浙江杰芯科技有限公司
 作者: zhulin(zhulin2@supcon.com)
 描述：
修改记录：
=========
日期                                      作者                       工作                            内容
*/

#ifndef __APIRTC_H_
#define __APIRTC_H_

#include "datatype.h"

typedef struct
{
    UINT16 wYear;
    UINT8 cMonth;
    UINT8 cDay;
    UINT8 cHour;
    UINT8 cMinute;
    UINT8 cSecond;
    UINT8 cWeek;
}Struct_TimeInfo, *pStruct_TimeInfo;

/*
 * 向RTC设置实时时间
 * 输入参数：pTime：时间参数，为pStruct_TimeInfo类型，包括年、月、日、时、分、秒、星期
 * 输出参数：TRUE:设置成功
 *         FALSE:设置失败
 */
UINT8 RTC_SetRealTime(pStruct_TimeInfo pTime);

/*
 * 从RTC获取实时时间
 * 输入参数：pTime：获得的时钟，为pStruct_TimeInfo类型，包括年、月、日、时、分、秒、星期
 * 输出参数：
 */
void RTC_GetRealTime(pStruct_TimeInfo pTime);

/*
 * 设置闹钟的定时时间
 * 输入参数：pTime：闹钟的定时时间，为pStruct_TimeInfo类型，包括年、月、日、时、分、秒、星期
 * 输出参数：
 * 说明：在第一次调用此函数时，函数内部需要注册中断回调函数，之后的调用不会重复注册中断回调函数，中断
 *      回调函数的实体在bsp.c中定义，具体功能由用户自定义。
 */
void RTC_SetAlarmTime(pStruct_TimeInfo pTime);

/*
 * 从RTC获取实时秒数，自1970年01月01日 00:00:00
 * 输入参数：
 * 输出参数：秒数
 */
UINT32 RTC_GetSeconds(void);

/*
 * 获取当天的秒数
 * 输入参数：
 * 输出参数：秒数
 */
UINT32 RTC_GetSecondsOfDay(void);

/*
 * 获取实时毫秒数
 * 输入参数：
 * 输出参数：毫秒数
 */
UINT64 RTC_GetMilliSeconds(void);

void RTC_SecondToTime(UINT64 qwAllSecond, pStruct_TimeInfo pTime);
UINT64 RTC_TimeToSecond(pStruct_TimeInfo pTime);



#endif /* __APIRTC_H_ */
