/*
 * testDemo.h
 *
 *  Created on: 2018-11-1
 *      Author: zhulin2
 */

#ifndef TESTDEMO_H_
#define TESTDEMO_H_

#define LWIP_TEST                 0
#define CAN_TEST                  0
#define IIC_EXTRTC_TEST           0
#define LOWPOWER_TEST             0
#define RTC_TEST                  0
#define WDT_TEST                  0
#define GPIO_TEST                 1
#define SPI0_ADC_TEST             0
#define SPI1_FLASH_TEST           0
#define UART_TEST                 0
#define PIPO_TEST                 0

void Lwip_Test(void);
void SPI1_Flash_test(void);
void UART_Test(void);
void IIC_Test(void);
void RTC_Test(void);
void SPI0_ADC_test(void);
void CAN_test(void);
void LowPower_test(void);
void WDT_Test(void);
void GPIO_Test(void);
void GPIO1A_Pin31_Reversal(void);
#endif /* TESTDEMO_H_ */
