/*
 * queue.c
 *
 *  Created on: 2019-3-2
 *      Author: zhulin2
 */

#include 	"queue.h"
#include 	"datatype.h"
#include 	"INTC.h"

/*
 * @brief  : 循环队列尝试操作
 * @param  : pCQ 队列结构指针
 * 			 opt 操作选项。见CycleQueue函数
 * @note   : 查询操作没有进行限制。
 *
 * @retval : TRUE，当前线程获取操作权限；FALSE，已经被其他线程占用，无法操作。
 */
static BOOL CQ_TryOperation(CycleQueue_t *pCQ, CycleQueue_Opt_t opt)
{
	UINT8 CurFlag = 0;//前一时刻的当前Flag
	UINT8 CASFlag = 0;//比较并改写之后的Flag

	//未初始化或当前处于延迟初始化情况下，只允许初始化操作
	if((pCQ->This != pCQ) || (pCQ->InitLock != CQ_LOCK_IDLE))
	{
		if(_OPT_CQ_INIT != opt)
		{
			return FALSE;
		}
	}
	else
	{
		if((pCQ->ResetLock != CQ_LOCK_IDLE) && (_OPT_CQ_RESET != opt))
		{
			return FALSE; //如果已经处于复位延迟状态，只允许复位操作进行。其他操作全部禁止。
		}
	}

	/*如果读->写->读的过程被其他线程打断，那么Flag发生变化，这个函数最后一次读的时候就会返回FALSE*/
	switch(opt)
	{
	case _OPT_CQ_WRITE:
		if(CQ_LOCK_IDLE == pCQ->WriteLock)//如果当前没有其他写操作进行
		{
			CurFlag = pCQ->WriteLock;//获取当前标记值
			CASFlag = ++pCQ->WriteLock;//可能存在多次操作同时进行的情况，所以根据标记值判断哪个线程获得操作权限
		}
		break;

	case _OPT_CQ_READ:
		if(CQ_LOCK_IDLE == pCQ->ReadLock)
		{
			CurFlag = pCQ->ReadLock;
			CASFlag = ++pCQ->ReadLock;
		}
		break;

	case _OPT_CQ_RESET:
		//如果有其他操作正在进行，则延迟复位；否则立即执行
		CurFlag = pCQ->ResetLock;
		++pCQ->ResetLock;

		//没有其他操作执行，立即执行
		if( (pCQ->WriteLock == CQ_LOCK_IDLE) &&
			(pCQ->ReadLock  == CQ_LOCK_IDLE)  )
		{
			CASFlag = pCQ->ResetLock;
		}
		break;

	case _OPT_CQ_INIT:

		CurFlag = pCQ->InitLock;
		++pCQ->InitLock;

		if( pCQ->This != pCQ )//没有初始化时直接执行
		{
			CASFlag = pCQ->InitLock;//初始化操作时可能并不能保证得到正确的状态
		}
		else//已经初始化还需要等待其他操作为空
		{
			//没有其他操作执行，立即执行
			if( (pCQ->WriteLock == CQ_LOCK_IDLE) &&
				(pCQ->ReadLock  == CQ_LOCK_IDLE)  )
			{
				CASFlag = pCQ->InitLock;
			}
		}
		break;

	case _OPT_CQ_QUERY://查询操作可多线程同时进行
		CASFlag = CurFlag + 1;
		break;

	default:
		break;
	}

	//判断是否已经获取了操作权限
	//如果该过程有被打断，则CASFlag就不会只自加一次，该条件不成立，即当前线程不具备操作权限。
	if( ++CurFlag == CASFlag)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

/*
 * @brief  : 循环队列的操作退出
 * @param  : pCQ 队列结构指针
 * 			 opt 操作选项。见CycleQueue函数
 * @note   : 如果有延时复位，等待所有当前操作完成后再复位
 *
 * @retval : None
 */
static void CQ_ExitOperation(CycleQueue_t *pCQ, CycleQueue_Opt_t opt)
{
	switch(opt)
	{
	case _OPT_CQ_WRITE:
		pCQ->WriteLock 	= CQ_LOCK_IDLE;
		break;

	case _OPT_CQ_READ:
		pCQ->ReadLock 	= CQ_LOCK_IDLE;
		break;

	case _OPT_CQ_RESET:
		pCQ->ResetLock 	= CQ_LOCK_IDLE;
		break;

	case _OPT_CQ_INIT:
		pCQ->WriteLock 	= CQ_LOCK_IDLE;
		pCQ->ReadLock 	= CQ_LOCK_IDLE;
		pCQ->ResetLock 	= CQ_LOCK_IDLE;
		pCQ->InitLock	= CQ_LOCK_IDLE;
		break;

	default:
		break;
	}

	//如果处于延时初始化或延时复位状态，当前操作完成后尝试判断状态是否为空闲，空闲时直接复位。
	//如果还有其他操作在执行，在尝试操作时就会退出，不必要在此处重复判断状态是否为空。
	if(pCQ->InitLock != CQ_LOCK_IDLE)
	{
		CQ_Init(pCQ, NULL, 0);//初始化
	}
	else if(pCQ->ResetLock != CQ_LOCK_IDLE)
	{
		CQ_Reset(pCQ);//复位
	}

}


/*
 * @brief  : 判断队列是否已经满
 * @param  : pCQ 队列结构指针
 * @note   : None
 *
 * @retval : TURE,队列已经满；FALSE，队列还可以存储
 */
inline BOOL CQ_IsFull(CycleQueue_t *pCQ)
{
	return (pCQ->CQSize == pCQ->UsedCount);
}

/*
 * @brief  : 判断队列是否已经为空
 * @param  : pCQ 队列结构指针
 * @note   : None
 *
 * @retval : TURE,队列已经空；FALSE，队列还可以读取
 */
inline BOOL CQ_IsEmpty(CycleQueue_t *pCQ)
{
	return (0x00 == pCQ->UsedCount);
}


/*
 * @brief  : 获取队列可用容量
 * @param  : pCQ 队列结构指针
 * @note   : 线程安全的函数
 *
 * @retval : 可用容量长度
 */
inline static UINT16 CQ_Capacity(CycleQueue_t *pCQ)
{
	return (pCQ->CQSize - pCQ->UsedCount);
}

/*
 * @brief  : 获取队列已用容量
 * @param  : pCQ 队列结构指针
 * @note   : 线程安全的函数
 *
 * @retval : 可用容量长度
 */
inline static UINT16 CQ_Used(CycleQueue_t *pCQ)
{
	return pCQ->UsedCount;
}

/*
 * @brief  : 循环队列初始化操作
 * @param  : pCQ 队列结构指针
 * 			 pBuf 队列缓存的首地址
 * 			 Length 队列长度
 * @note   : 由于需要支持重新初始化，该操作产生的时候可能正在读写，此时应当等待当前读写操作完毕后再执行初始化。
 * 			   涉及到数据存储区及队列大小的保存，这里采用的方法是结构体中增加字段存储。
 *
 * @retval : 队列长度
 */
UINT16 CQ_Init(CycleQueue_t *pCQ, UINT8 *pBuf, UINT16 CQSize)
{
	//首先要将重新初始化的值存储到备份区
	if( (pBuf != NULL) && (CQSize != 0) )
	{
		pCQ->pDataBufferBackup = pBuf;
		pCQ->CQSizeBackup = CQSize;
	}

	//如果尝试操作失败，返回0x0000
	if( !CQ_TryOperation(pCQ, _OPT_CQ_INIT) )
	{
		return 0x0000;
	}

	//如果初始化操作允许，则将备份的数据复制到
	pCQ->CQSize = pCQ->CQSizeBackup;
	pCQ->pDataBuffer = pCQ->pDataBufferBackup;
	pCQ->FontPoint = 0;
	pCQ->RearPoint = 0;
	pCQ->UsedCount = 0;
	pCQ->This = pCQ;

	//退出操作
	CQ_ExitOperation(pCQ, _OPT_CQ_INIT);

	return CQSize;
}


/*
 * @brief  : 循环队列写入操作
 * @param  : pCQ 队列结构指针
 * 			 pBuf 队列缓存的首地址
 * 			 Length 队列长度
 * @note   : 可能因写入长度太大，导致队列满，部分数据没有写入。
 *
 * @retval : 队列实际写入长度
 */
UINT16 CQ_Write(CycleQueue_t *pCQ, UINT8 *pBuf, UINT16 Length)
{
	UINT16 i = 0;
	UINT32 psr;
	UINT16 capacity;//可用空间

	//如果尝试操作失败，返回0x0000
	if( !CQ_TryOperation(pCQ, _OPT_CQ_WRITE) )
	{
		return 0x0000;
	}

	capacity = CQ_Capacity(pCQ);//获取可用空间

	//如果需要写入的长度过大，超过了队列的剩余容量，会被截断。
	if(Length > capacity)
	{
		Length = capacity;
	}

	//如果数据区可以用于存储数据，则将数据写入
	//为提高效率，等待全部数据写入后再更新尾指针
	if( (pCQ->pDataBuffer != NULL) &&
		(pBuf != NULL) )
	{
		for(i = 0; i < Length; i++)
		{
			pCQ->pDataBuffer[ (pCQ->RearPoint + i) % pCQ->CQSize ] = pBuf[i];
		}
	}

	//更新尾指针和使用计数
	CPU_EnterCritical(&psr);
	pCQ->RearPoint = (pCQ->RearPoint + Length) % pCQ->CQSize;
	pCQ->UsedCount += Length;
	CPU_ExitCritical(psr);

	//退出操作
	CQ_ExitOperation(pCQ, _OPT_CQ_WRITE);

	return Length;
}

/*
 * @brief  : 循环队列出列操作
 * @param  : pCQ 队列结构指针
 * 			 pBuf 队列缓存的首地址
 * 			 Length 队列长度
 * @note   : 读取长度可能比想要读取的长度小
 *
 * @retval : 队列读取长度
 */
UINT16 CQ_Read(CycleQueue_t *pCQ, UINT8 *pBuf, UINT16 Length)
{
	UINT16 i = 0;
	UINT32 psr;
	UINT16 used;//可用空间

	//如果尝试操作失败，返回0x0000
	if( !CQ_TryOperation(pCQ, _OPT_CQ_READ) )
	{
		return 0x0000;
	}

	used = CQ_Used(pCQ);//获取当前可用空间

	//最多只能读取已经读取的数量
	if(Length > used)
	{
		Length = used;
	}


	//如果数据区可以用于存储数据，则读取数据
	//为提高效率，等待全部数据写入后再更新尾指针
	if( (pCQ->pDataBuffer != NULL) &&
		(pBuf != NULL)	)
	{
		for(i = 0; i < Length; i++)
		{
			pBuf[i] = pCQ->pDataBuffer[ (pCQ->FontPoint + i) % pCQ->CQSize ];
		}
	}

	//更新头指针和使用计数
	CPU_EnterCritical(&psr);
	pCQ->FontPoint = (pCQ->FontPoint + Length) % pCQ->CQSize;
	pCQ->UsedCount -= Length;
	CPU_ExitCritical(psr);

	//退出操作
	CQ_ExitOperation(pCQ, _OPT_CQ_READ);

	return Length;
}

/*
 * @brief  : 循环队列队复位操作
 * @param  : pCQ 队列结构指针
 * @note   :
 *
 * @retval : 队列长度
 */
UINT16 CQ_Reset(CycleQueue_t *pCQ)
{
	//如果尝试操作失败，返回0x0000
	if( !CQ_TryOperation(pCQ, _OPT_CQ_RESET) )
	{
		return 0x0000;
	}

	pCQ->RearPoint = 0;
	pCQ->FontPoint = 0;
	pCQ->UsedCount = 0;

	//退出操作
	CQ_ExitOperation(pCQ, _OPT_CQ_RESET);

	return pCQ->CQSize;
}


//获取当前空余空间（字节）
UINT16 CQ_GetEmptySize(CycleQueue_t *pCQ)
{
	return CQ_Capacity(pCQ);
}


//获取当前已存储的数据量（字节）
UINT16 CQ_GetUsedSize(CycleQueue_t *pCQ)
{
	return CQ_Used(pCQ);
}


//写入一个字节数据
UINT16 CQ_WriteChar(CycleQueue_t *pCQ, UINT8 chr)
{
	return CQ_Write(pCQ, &chr, 1);
}








