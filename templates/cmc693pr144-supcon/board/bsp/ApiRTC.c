/*
 文件: ApiRTC.c
 版本: CMC693 V1.10
 版权: 浙江杰芯科技有限公司
 作者: zhulin(zhulin2@supcon.com)
 描述：
修改记录：
=========
日期                                      作者                       工作                            内容
*/
#include "ApiRTC.h"
#include "lib_rtc.h"

#define RTC_LEAP_YEAR_SECOND      ((UINT32)31622400)       /* 闰年一年的秒数 */
#define RTC_NOTLEAP_YEAR_SECOND   ((UINT32)31536000)       /* 平年一年的秒数 */
#define RTC_DAY_SECOND            ((UINT32)86400)          /* 一天的秒数 */

#define RTC_SOURCE_CLK            ((UINT32)32768)          /* RTC外部时钟源 */

const UINT8 MonTable_NotLeapYear[13] = {0,31,28,31,30,31,30,31,31,30,31,30,31};

/*
 * 判断是否是闰年
 * 输入参数：wYear，年
 * 输出参数：1:是闰年
 *         0:不是闰年
 */
UINT8 IsLeapYear(UINT16 wYear)
{
	return (((wYear%4 == 0) && (wYear%100 != 0)) || (wYear%400 == 0));
}

/*
 * 校验时钟格式
 * 输入参数：pTime：时间参数，为pStruct_TimeInfo类型，包括年、月、日、时、分、秒、星期
 * 输出参数：TRUE:时钟格式正确
 *         FALSE:时钟格式错误
 * 说明：由于RTC计数器为64位，所以年只要大于1970年即可，无需判断其最大值
 */
UINT8 RTC_CheckTimeFormat(pStruct_TimeInfo pTime)
{
	UINT8 cTemp,cMonDays;

	if((pTime->cSecond <= 59) && (pTime->cMinute <= 59) && (pTime->cHour <= 23))  /* 判断时分秒是否合法*/
	{
		cTemp = pTime->cMonth;
		if((cTemp > 0) && (cTemp <= 12))                                          /* 判断月是否合法 */
		{
			cMonDays = MonTable_NotLeapYear[cTemp];
			if(pTime->wYear >= 1970)                                              /* 判断年是否合法 */
			{
				if(IsLeapYear(pTime->wYear) && (cTemp == 2))                      /* 判断是否是闰年且是2月 */
				{
					cMonDays++;                                                   /* 闰月需要在正常天数的基础上加1 */
				}

				if(pTime->cDay <= cMonDays)                                       /* 判断日是否合法 */
				{
					return TRUE;                                                  /* 时间格式正确 */
				}
			}
		}
	}

	return FALSE;                                                                 /* 时间格式错误 */
}

/*
 * 将时钟转换为以基准日开始的秒数
 * 输入参数：pTime：时间参数，为pStruct_TimeInfo类型，包括年、月、日、时、分、秒、星期
 * 输出参数：以基准日开始的秒数
 * 说明：由于RTC计数器为64位，所以年只要大于1970年即可，无需判断其最大值。调用此函数之前需要
 *     先校验时钟格式
 */
UINT64 RTC_TimeToSecond(pStruct_TimeInfo pTime)
{
	UINT64 qwSecond = 0;
	UINT16 i;

	for(i = 1970; i < pTime->wYear; i++)                         /* 将年转换为以基准日开始的秒数 */
	{
		if(IsLeapYear(i))                                         /* 如果是闰年则一年的秒数为31622400*/
		{
			qwSecond += RTC_LEAP_YEAR_SECOND;
		}
		else                                                      /* 平年一年的秒数为31536000 */
		{
			qwSecond += RTC_NOTLEAP_YEAR_SECOND;
		}
	}

	for(i = 1; i < pTime->cMonth; i++)                            /* 将月转换为秒数，添加到以基准日开始的总秒数上 */
	{
		qwSecond += MonTable_NotLeapYear[i] * RTC_DAY_SECOND;
		if((i == 2) && (IsLeapYear(pTime->wYear)))                /* 如果存在闰年的2月份，则需要多加一天的秒数 */
		{
			qwSecond += RTC_DAY_SECOND;
		}
	}

	qwSecond += (pTime->cDay-1)*RTC_DAY_SECOND + pTime->cHour*3600 \
			    + pTime->cMinute*60 + pTime->cSecond;

	return qwSecond;
}

/*
 * 将以基准日开始的秒数转换为时间
 * 输入参数：qwAllSecond:以基准日开始的秒数
 *        pTime:转换后的时间
 * 输出参数：无
 * 说明：
 */
void RTC_SecondToTime(UINT64 qwAllSecond, pStruct_TimeInfo pTime)
{
	UINT64 qwTemp;
	UINT32 i;

	pTime->cSecond = qwAllSecond%60;                       /* 获得当前时间的秒 */
	qwTemp = qwAllSecond/60;                               /* 总分钟数 */
	pTime->cMinute = qwTemp%60;                            /* 获得当前分 */
	qwTemp = qwTemp/60;                                    /* 总小时数 */
	pTime->cHour = qwTemp%24;                              /* 获得当前时 */
	qwTemp = qwTemp/24;                                    /* 总天数 */

	for(i=1970; qwTemp >= 365; i++)
	{
		if(IsLeapYear(i))                                  /* 闰年 */
		{
			qwTemp -= 366;                                 /* 闰年一年366天 */
		}
		else
		{
			qwTemp -= 365;                                 /* 平年一年365天 */
		}
	}
	pTime->wYear = i;                                      /* 获得当前年 */

	for(i=1; i<=12; i++)
	{
		if(qwTemp >= MonTable_NotLeapYear[i])
		{
			qwTemp -= MonTable_NotLeapYear[i];
			if((i==2) && (IsLeapYear(pTime->wYear)))       /* 闰年的2月为29天 */
			{
				if(qwTemp > 1)
				{
					qwTemp--;
				}
				else
				{
					qwTemp = 28;
					break;
				}
			}
		}
		else
		{
			break;
		}
	}
	pTime->cMonth = i;                                    /* 获得当前月 */
	pTime->cDay = qwTemp + 1;                             /* 获得当前日 */
	pTime->cWeek = (qwAllSecond/RTC_DAY_SECOND + 4)%7;    /* 获得当前星期 */
}

/*
 * 向RTC设置实时时间
 * 输入参数：pTime：时间参数，为pStruct_TimeInfo类型，包括年、月、日、时、分、秒、星期
 * 输出参数：TRUE:设置成功
 *         FALSE:设置失败
 */
UINT8 RTC_SetRealTime(pStruct_TimeInfo pTime)
{
	UINT64 qwSecond;
	UINT64 qwCount;

	if(RTC_CheckTimeFormat(pTime) == FALSE)                                      /* 检查时钟格式错误 */
	{
		return FALSE;
	}

	qwSecond = RTC_TimeToSecond(pTime);                                          /* 将时钟转换为秒 */
    qwCount = qwSecond * RTC_SOURCE_CLK;                                         /* 秒数转换为计数值 */

    RTC_DisableCounter();                                                        /* 禁能RTC计数器 */
    RTC_ClearIntr();                                                             /* 清中断状态 */
    RTC_SetCounterLoad(qwCount);                                                 /* 设置计数器 */
    RTC_EnableCounter();                                                         /* 使能RTC计数器 */

    return TRUE;
}

/*
 * 从RTC获取实时时间
 * 输入参数：pTime：获得的时钟，为pStruct_TimeInfo类型，包括年、月、日、时、分、秒、星期
 * 输出参数：
 */
void RTC_GetRealTime(pStruct_TimeInfo pTime)
{
	UINT64 qwSecond;
	UINT64 qwCount;

	qwCount = RTC_GetCounter();                                                  /* 从RTC获取实时的计数值 */
	qwSecond = qwCount/RTC_SOURCE_CLK;                                           /* 计数值转换为秒数 */

	RTC_SecondToTime(qwSecond, pTime);
}

/*
 * 从RTC获取实时秒数，自1970年01月01日 00:00:00
 * 输入参数：
 * 输出参数：秒数
 * 说明：这里仅取4个字节的秒数，当时间超过1970年+136年时，该函数失效
 */
UINT32 RTC_GetSeconds(void)
{
	return (UINT32)(RTC_GetCounter()/RTC_SOURCE_CLK);
}

/*
 * 获取当天的秒数
 * 输入参数：
 * 输出参数：秒数
 */
UINT32 RTC_GetSecondsOfDay(void)
{
	Struct_TimeInfo Time;

	RTC_GetRealTime(&Time);
	return Time.cHour * 3600 + Time.cMinute * 60 + Time.cSecond;
}

/*
 * 获取实时毫秒数
 * 输入参数：
 * 输出参数：毫秒数
 */
UINT64 RTC_GetMilliSeconds(void)
{
    return 	RTC_GetCounter()*1000/RTC_SOURCE_CLK;
}

/*
 * 设置闹钟的定时时间
 * 输入参数：pTime：闹钟的定时时间，为pStruct_TimeInfo类型，包括年、月、日、时、分、秒、星期
 * 输出参数：
 * 说明：在第一次调用此函数时，函数内部需要注册中断回调函数，之后的调用不会重复注册中断回调函数，中断
 *      回调函数的实体在bsp.c中定义，具体功能由用户自定义。
 */
void RTC_SetAlarmTime(pStruct_TimeInfo pTime)
{
	UINT64 qwSecond,qwCount;

	qwSecond = RTC_TimeToSecond(pTime);                                          /* 时间转换为总秒数 */
	qwCount = qwSecond * RTC_SOURCE_CLK;                                         /* 秒数转换为计数值 */
	RTC_SetCounterMatch(qwCount);                                                /* 设置闹钟计数器值 */
}
