/*
 文件: bps.c
 版本: CMC693 V1.10
 版权: 浙江杰芯科技有限公司
 作者: zhulin(zhulin2@supcon.com)
 描述：
修改记录：
=========
日期                                      作者                       工作                            内容
*/

#include "config.h"
#if 1
#include "pipo.h"

#define ONE_MS_TICKS 50000

void Timer_Init(void);
void UART_Init(void);
void GPIO_init(void);
void SPI_Init(void);
void IIC_Init(void);
void Flash_Init(void);
void PIPO_Init(void);
void WDT_Init(void);
void CAN_Init(void);
void RTC_Init(void);

void UART0_ISRCallback(UINT32 dwIrqid);             /* 用户自定义uart0中断函数 */
void UART1_ISRCallback(UINT32 dwIrqid);             /* 用户自定义uart1中断函数 */
void RTC_ISRCallback(UINT32 irqid);                 /* 用户自定义RTC中断函数 */
void GPIO1_PortA_ISRCallback(UINT32 irqid);         /* 用户自定义GPIO1 PORTA的中断函数 */
void WDT_ISRCallback(UINT32 irqid);                 /* 用户自定义WDT中断函数 */
void GPIO0_PortA_Pin0_ISRCallback(UINT32 irqid);    /* 用户自定义GPIO0 PORTA PIN0的中断函数 */
void IIC_ISRCallback(UINT32 irqid);                 /* 用户自定义IIC中断函数 */
void CAN0_ISRHandler(UINT32 dwIrqid);               /* 用户自定义CAN0中断函数 */
void CAN1_ISRHandler(UINT32 dwIrqid);               /* 用户自定义CAN1中断函数 */
void Timer0_ISRCallback(UINT32 irqid);              /* 用户自定义timer0中断函数 */
void Timer1_ISRCallback(UINT32 irqid);              /* 用户自定义timer1中断函数 */
void Timer2_ISRCallback(UINT32 irqid);              /* 用户自定义timer2中断函数 */
void Timer3_ISRCallback(UINT32 irqid);              /* 用户自定义timer3中断函数 */
void PIPO0_ISRCallback(UINT32 irqid);               /* 用户自定义PIPO0中断函数 */
void PIPO1_ISRCallback(UINT32 irqid);               /* 用户自定义PIPO1中断函数 */
void PIPO2_ISRCallback(UINT32 irqid);               /* 用户自定义PIPO2中断函数 */
void PIPO3_ISRCallback(UINT32 irqid);               /* 用户自定义PIPO3中断函数 */
void PIPO4_ISRCallback(UINT32 irqid);               /* 用户自定义PIPO4中断函数 */
void PIPO5_ISRCallback(UINT32 irqid);               /* 用户自定义PIPO5中断函数 */

INT32U wdTimeFlag;                                  /* Timer0定时器计数标志 */
Struct_IRQHandler GPIO0A_Pin0IntrInfo;              /* 定义GPIO0A_0引脚的中断数据结构体，用于注册中断 */


/*
* 函数名称：  Uart0Printf
* 函数描述：  通过串口格式化输出字符串
* 入    口：  字符串（可变变量个数）
* 出    口：  输出字节数
*/
int	Uart0Printf(const char *fmt,...){

	int	cnt;
    va_list ap;
    static char	string[512];
	static char *s;

	s = string;
    va_start(ap,fmt);
    cnt = vsprintf(string,fmt,ap);
    while(*s){
    	Uart_PutChar(UART0,*s++);
	}
    va_end(ap);

    return	cnt;
}

void DelayNMS(UINT32 ms){
	UINT32 i;
   	for(; ms>0; ms--){
   		for(i=0; i<ONE_MS_TICKS; i++);
	}
}

/*
* 函数名称：  Cmc693v3_LowLevelInit
* 函数描述：  底层驱动初始化接口
* 入    口：
* 出    口：
*/
void Cmc693v3_LowLevelInit(void)
{
	CPU_DisAllNormalIrq();                       /* 禁能所有中断 */

	Exception_Init();                            /* ckcore 向量表初始化，必须在开启任何中断之前调用 */

	INTC_Init();                                 /* 中断模块初始化，必须在开启任何中断之前调用 */

	ConfigClk(10000000, 200000000, 3);           /* 配置200M时钟 */

	GPIO_init();                                 /* 根据需求初始化相关GPIO工作模式 */

#if SPI0_ADC_TEST == 1
	SPI_Init();
#endif

#if CAN_TEST == 1
	CAN_Init();
#endif

#if IIC_EXTRTC_TEST == 1
	IIC_Init();
#endif

#if RTC_TEST == 1
	RTC_Init();
#endif

	Timer_Init();                                /* 根据需求初始化相关TIMER */

	UART_Init();                                 /* 根据需求初始化相关UART */

#if SPI1_FLASH_TEST == 1
	Flash_Init();                                /* flash初始化 */
#endif

#if PIPO_TEST == 1
	PIPO_Init();
#endif

#if WDT_TEST == 1
	WDT_Init();                                  /* 看门狗初始化 */
#endif

	CPU_EnAllNormalIrq();
}

/*
* 函数名称：  RTC_Init
* 函数描述：  根据具体需求对RTC进行初始化
* 入    口：
* 出    口：
* 说    明：  设置初始时间，并配置RTC中断，中断用于闹钟
*/
#if 0
void RTC_Init(void)
{
	Struct_TimeInfo TimerPara;

	TimerPara.wYear = 2019;
	TimerPara.cMonth = 1;
	TimerPara.cDay = 19;
	TimerPara.cHour = 14;
	TimerPara.cMinute = 2;
	TimerPara.cSecond = 0;

	RTC_SetRealTime(&TimerPara);

	RTC_ConfigIntr(RTC_PRIO, RTC_ISRCallback);
}
#endif
/*
* 函数名称：  CAN_Init
* 函数描述：  根据具体需求对CAN进行初始化
* 入    口：
* 出    口：
*/
void CAN_Init(void)
{
	Struct_CANInit CAN_InitInfo;

	CAN_InitInfo.pCAN = (pStruct_CAN)CAN1_BASE_ADDR;
	CAN_InitInfo.buadrate = 1000000;
	CAN_InitInfo.canacr = 0x12385678;                     /* 验收位 */
	CAN_InitInfo.canamr = 0xffffffff;                     /* 屏蔽位 */
	CAN_InitInfo.canmode = CAN_MODE_EXTEND;
	CAN_InitInfo.intrVector = CAN_IT_RIE | CAN_IT_OIE;    /* 接收中断和溢出中断 */
	CAN_InitInfo.priority = CAN1_PRIO;
	CAN_InitInfo.CAN_ISR = NULL;                          /* 为NULL时，使用驱动文件中默认的中断处理函数 */

	CAN_Config(&CAN_InitInfo);
}

/*
* 函数名称：  UART_Init
* 函数描述：  根据具体需求对UART进行初始化
* 入    口：
* 出    口：
*/
void UART_Init(void)
{
	UART_InitStructure UART_InitInfo;

	UART_InitInfo.UartId = UART0;
	UART_InitInfo.BaudRate = B38400;
	UART_InitInfo.WordSize = WORD_SIZE_8;
	UART_InitInfo.StopBit = LCR_STOP_BIT_1;
	UART_InitInfo.Parity = NONE;
	UART_InitInfo.RxMode = UART_Intr;                /* 接收开启中断模式 */
	UART_InitInfo.TxMode = UART_Query;
	UART_InitInfo.Priority = UART0_PRIO;             /* 设置UART0的中断优先级 */
	UART_InitInfo.UART_ISR = UART0_ISRCallback;      /* 为NULL时使用默认的中断处理函数，用户可在此文件下自定义中断处理函数，
	                                                                                                                                  请参考默认中断处理函数的实现方法，方可正常使用uart驱动文件内的发送
	                                                                                                                                  接收函数接口*/

	Uart_Config(&UART_InitInfo);
}

/*
* 函数名称：  Timer_Init
* 函数描述：  初始化Timer，可根据实际应用对Timer0、Timer1、Timer2、Timer3
*         独立的进行初始化
* 入    口：
* 出    口：
*/
void Timer_Init(void)
{
	Timer_InitStructure Timer_InitInfo;

	/* timer0初始化 */
	Timer_InitInfo.enTimerID = TIMER0;
	Timer_InitInfo.dwTimeout = 1000000/OS_TICKS_PER_SEC;
	Timer_InitInfo.wPriority = TIMER0_PRIO;
	Timer_InitInfo.TIMER_ISR = Timer0_ISRCallback;             /* 用户自定义的TIMER0中断处理函数，为NULL时，使用驱动文件内默认的中断函数 */

	Timer_Config(&Timer_InitInfo);

/*	 timer1初始化
	Timer_InitInfo.enTimerID = TIMER1;
	Timer_InitInfo.dwTimeout = 1000000;                         1s定时
	Timer_InitInfo.wPriority = TIMER1_PRIO;
	Timer_InitInfo.TIMER_ISR = Timer1_ISRCallback;              用户自定义的TIMER1中断处理函数，为NULL时，使用驱动文件内默认的中断函数

	Timer_Config(&Timer_InitInfo);

	timer2初始化
	Timer_InitInfo.enTimerID = TIMER2;
	Timer_InitInfo.dwTimeout = 10000;                           10ms定时
	Timer_InitInfo.wPriority = TIMER2_PRIO;
	Timer_InitInfo.TIMER_ISR = Timer2_ISRCallback;              用户自定义的TIMER2中断处理函数，为NULL时，使用驱动文件内默认的中断函数

	Timer_Config(&Timer_InitInfo);

	 timer3初始化
	Timer_InitInfo.enTimerID = TIMER3;
	Timer_InitInfo.dwTimeout = 1000;                            1ms定时
	Timer_InitInfo.wPriority = TIMER3_PRIO;
	Timer_InitInfo.TIMER_ISR = Timer3_ISRCallback;              用户自定义的TIMER3中断处理函数，为NULL时，使用驱动文件内默认的中断函数

	Timer_Config(&Timer_InitInfo);*/
}

/*
* 函数名称：  GPIO_init
* 函数描述：  初始化GPIO，可根据实际应用对GPIO0、GPIO1、PORTA、PORTB
*         独立的进行初始化
* 入    口：
* 出    口：
*/
void GPIO_init(void)
{
	GPIO_InitStructure GPIO_InitInfo;

#if GPIO_TEST == 1
	/* 配置GPIO1_PORTA pin31为输出模式，通过定时器1的定时，用于测试GPIO输出方波  */
	GPIO_InitInfo.GPIOAddr = GPIO1;
	GPIO_InitInfo.GPIOPort = PORTA;
	GPIO_InitInfo.GPIODirection = GPIO_OUTPUT;
	GPIO_InitInfo.GPIOPin = GPIO_P28;
	GPIO_InitInfo.bHardwareMode = FALSE;
	GPIO_InitInfo.GPIOIntr = GPIO_NULL_INTR;                               /* 禁止中断开启 */
	GPIO_Config(&GPIO_InitInfo);
#endif

#if UART_TEST == 1
	/* 配置开启UART0、UART1的TX、RX引脚复用功能 */
	GPIO_InitInfo.GPIOAddr = GPIO1;
	GPIO_InitInfo.GPIOPort = PORTA;
	GPIO_InitInfo.GPIOPin = GPIO_P2|GPIO_P3;
	GPIO_InitInfo.bHardwareMode = TRUE;
	GPIO_Config(&GPIO_InitInfo);

	GPIO_InitInfo.GPIOPort = PORTB;
	GPIO_InitInfo.GPIOPin = GPIO_P26|GPIO_P27;
	GPIO_Config(&GPIO_InitInfo);
#endif

#if LOWPOWER_TEST == 1
	/* 配置GPIO0_PORTA_PIN0为输入模式、关闭硬件模式、开启中断、低电平触发、优先级为GPIO0A_0_PRIO
	 * 用于将MCU从低功耗模式下唤醒，若MCU进入STOP模式只能电平触发唤醒，其他低功耗模式支持边沿触发唤醒 */
	GPIO_InitInfo.GPIOAddr = GPIO0;
	GPIO_InitInfo.GPIOPort = PORTA;
	GPIO_InitInfo.GPIODirection = GPIO_INPUT;
	GPIO_InitInfo.GPIOPin = GPIO_P0;
	GPIO_InitInfo.bHardwareMode = FALSE;
	GPIO_InitInfo.GPIOIntr = GPIO0A_0_INTR;                                /* 使能中断 */
	GPIO_InitInfo.priority = GPIO0A_0_PRIO;
	GPIO_InitInfo.BurstMode = GPIO_Intact_Low_Level;
	GPIO_InitInfo.GPIO_ISR = GPIO0_PortA_Pin0_ISRCallback;
	GPIO_Config(&GPIO_InitInfo);
#endif

#if PIPO_TEST == 1
	/* 配置PIPO_0引脚复用功能 */
	GPIO_InitInfo.GPIOPort = PORTB;
	GPIO_InitInfo.GPIOPin = GPIO_P8|GPIO_P9;
	GPIO_Config(&GPIO_InitInfo);
#endif

#if SPI0_ADC_TEST == 1
	/* 配置SPI0引脚的复用功能 */
	GPIO_InitInfo.GPIOAddr = GPIO1;
	GPIO_InitInfo.GPIOPort = PORTB;
	GPIO_InitInfo.GPIOPin = GPIO_P0|GPIO_P2|GPIO_P3|GPIO_P4;
	GPIO_InitInfo.bHardwareMode = TRUE;
	GPIO_Config(&GPIO_InitInfo);

	/* 配置GPIO1B[8]为输出状态，用于控制ADS1148的START信号 */
	GPIO_InitInfo.GPIOAddr = GPIO1;
	GPIO_InitInfo.GPIOPort = PORTB;
	GPIO_InitInfo.GPIODirection = GPIO_OUTPUT;
	GPIO_InitInfo.GPIOPin = GPIO_P8;
	GPIO_InitInfo.bHardwareMode = FALSE;
	GPIO_InitInfo.GPIOIntr = GPIO_NULL_INTR;                               /* 禁止中断开启 */
	GPIO_Config(&GPIO_InitInfo);
	GPIO_WritePins(GPIO1,PORTB,GPIO_P8,1);
#endif

#if IIC_EXTRTC_TEST == 1
	/* 配置IIC引脚的复用功能 */
	GPIO_InitInfo.GPIOAddr = GPIO1;
	GPIO_InitInfo.GPIOPort = PORTB;
	GPIO_InitInfo.GPIOPin = GPIO_P30|GPIO_P31;
	GPIO_InitInfo.bHardwareMode = TRUE;
	GPIO_Config(&GPIO_InitInfo);
#endif

#if CAN_TEST == 1
	/* 配置CAN0引脚的复用功能 */
	GPIO_InitInfo.GPIOAddr = GPIO1;
	GPIO_InitInfo.GPIOPort = PORTB;
	GPIO_InitInfo.GPIOPin = GPIO_P5|GPIO_P28|GPIO_P29;
	GPIO_InitInfo.bHardwareMode = TRUE;
	GPIO_Config(&GPIO_InitInfo);

	/* 配置CAN1引脚的复用功能 */
	GPIO_InitInfo.GPIOAddr = GPIO1;
	GPIO_InitInfo.GPIOPort = PORTB;
	GPIO_InitInfo.GPIOPin = GPIO_P6|GPIO_P24|GPIO_P25;
	GPIO_InitInfo.bHardwareMode = TRUE;
	GPIO_Config(&GPIO_InitInfo);
#endif

#if LWIP_TEST == 1
	/* 配置MAC1引脚复用功能 */
	GPIO_InitInfo.GPIOAddr = GPIO0;
	GPIO_InitInfo.GPIOPort = PORTB;
	GPIO_InitInfo.GPIODirection = GPIO_INPUT;
	GPIO_InitInfo.GPIOPin = GPIO_P13|GPIO_P14|GPIO_P15;
	GPIO_InitInfo.bHardwareMode = TRUE;
	GPIO_InitInfo.GPIOIntr = GPIO_NULL_INTR;
	GPIO_Config(&GPIO_InitInfo);

	GPIO_InitInfo.GPIOAddr = GPIO1;
	GPIO_InitInfo.GPIOPort = PORTA;
	GPIO_InitInfo.GPIOPin = GPIO_P14|GPIO_P15|GPIO_P16|GPIO_P17|GPIO_P18|GPIO_P19;
	GPIO_Config(&GPIO_InitInfo);

	/* 配置MAC0引脚复用功能 */
	GPIO_InitInfo.GPIOAddr = GPIO0;
	GPIO_InitInfo.GPIOPort = PORTB;
	GPIO_InitInfo.GPIODirection = GPIO_INPUT;
	GPIO_InitInfo.GPIOPin = GPIO_P0|GPIO_P1|GPIO_P2|GPIO_P3|GPIO_P4|GPIO_P5|GPIO_P6|GPIO_P7|GPIO_P8;
	GPIO_InitInfo.bHardwareMode = TRUE;
	GPIO_InitInfo.GPIOIntr = GPIO_NULL_INTR;
	GPIO_Config(&GPIO_InitInfo);
#endif
}

/*
* 函数名称：  SPI_Init
* 函数描述：  初始化SPI，可根据实际应用对SPI、SPI1独立的进行初始化
* 入    口：
* 出    口：
*/
void SPI_Init(void)
{
	SPI_InitStructure SPI_InitInfo;

	SPI_InitInfo.SPI_BaseAddr = (UINT32)SPI_BASE_ADDR;
	SPI_InitInfo.baudrate   = APB_FREQ / 100;//50K
	SPI_InitInfo.datawidth  = SPI_DataWidth_8;
	SPI_InitInfo.cs         = SPI_CS0;
	SPI_InitInfo.polarity   = SPI_CLOCK_POLARITY_LOW;
	SPI_InitInfo.phase      = SPI_CLOCK_PHASE_SECOND_EDGE;
	SPI_InitInfo.TxFTLevel  = SPI_FIFO_MAX_LEVEL;
	SPI_InitInfo.RxFTLevel  = SPI_FIFO_MAX_LEVEL;
	SPI_InitInfo.intrmask   = 0;

	SPI_Config(&SPI_InitInfo);
}

/*
* 函数名称：  IIC_Init
* 函数描述：  初始化IIC，可根据实际应用对IIC进行初始化
* 入    口：
* 出    口：
*/
void IIC_Init(void)
{
	IIC_InitStruct IIC_InitInfo;

	IIC_InitInfo.IIC_BaseAddr = IIC0_ADDR;
	IIC_InitInfo.IIC_Addr = 0x30;
	IIC_InitInfo.IIC_AddrMode  = IIC_AddressMode_7bits;
	IIC_InitInfo.IIC_Character = IIC_Character_Master;
	IIC_InitInfo.IIC_SpeedMode = IIC_SpeedMode_Fast;
	IIC_InitInfo.RxFTLevel = 4;
	IIC_InitInfo.TxFTLevel = 3;

	IIC_InitInfo.cIICIntr = 1;                                 /* 开IIC中断  */
	/*如果需要用中断方式处理，则还需要增加一块缓冲区用于存放数据*/
	IIC_InitInfo.dwPriority = IIC_PRIO;                        /* 中断优先级 */
	IIC_InitInfo.IIC_ISR = IIC_ISRCallback;                    /* 注册用户自定义的中断处理函数 */
	IIC_InitInfo.IIC_RxBuffer.pBuffer = NULL;
	IIC_InitInfo.IIC_RxBuffer.size = 0;
	IIC_InitInfo.IIC_RxBuffer.index = 0;

	IIC_InitInfo.IIC_TxBuffer.pBuffer = NULL;
	IIC_InitInfo.IIC_TxBuffer.size = 0;
	IIC_InitInfo.IIC_TxBuffer.index = 0;

	IIC_Config(&IIC_InitInfo);
}

/*
* 函数名称：  Flash_Init
* 函数描述：  初始化Flash，可根据实际应用对Flash进行初始化,Flash初始化
*         仅需要初始化Flash相关的SPI1接口即可
* 入    口：
* 出    口：
*/
void Flash_Init(void)
{
	Flash_InitStructure Flash_InitInfo;

    /* 初始化Flash所使用的SPI1接口信息,flash初始化只需配置SPI接口即可  */
	Flash_InitInfo.SPI_BaseAddr= (UINT32)SPI1_BASE_ADDR;
    Flash_InitInfo.baudrate   = APB_FREQ / 6;//50K
    Flash_InitInfo.datawidth  = SPI_DataWidth_8;
    Flash_InitInfo.cs         = SPI_CS0;
    Flash_InitInfo.polarity   = SPI_CLOCK_POLARITY_LOW;
    Flash_InitInfo.phase      = SPI_CLOCK_PHASE_FIRST_EDGE;
    Flash_InitInfo.TxFTLevel  = SPI_FIFO_MAX_LEVEL;
    Flash_InitInfo.RxFTLevel  = SPI_FIFO_MAX_LEVEL;
    Flash_InitInfo.intrmask   = 0;

    Flash_Config(&Flash_InitInfo);
}
/*
 * 函数名称：  PIPO_Init
 * 函数描述：  初始化PIPO，可根据实际应用对PIPO进行初始化
 * 入    口：
 * 出    口：
 */
void PIPO_Init(void)
{
	PIPO_InitStructure PIPO_InitInfo;
	PIPO_TimerStructure Timer;
	PIPO_PwmStructure PWM;
	PIPO_PulseMeasureStructure PulseMeasure;
	PIPO_PulseCountStructure PulseCount;
	PIPO_SinglePulseStructure SinglePulse;
	PIPO_COP_Structure pCOP;

	PIPO_InitInfo.Mode = PIPO_MODE_PWM;
	PIPO_InitInfo.PIPO_Index = 0;
	PIPO_InitInfo.pPIPOAddr = PIPO0;
	PIPO_InitInfo.priority = PIPO_PRIO;
	PIPO_InitInfo.PIPO_ISR = PIPO0_ISRCallback;          /* 用户自定义的中断处理函数,为NULL时，使用驱动文件中默认的中断函数 */

	switch(PIPO_InitInfo.Mode)
	{
	case PIPO_MODE_IDLE:
		/*复位该模块*/
		break;
	case PIPO_MODE_TIMER:
		Timer.Dir = PIPO_TIMER_UP;
		Timer.InterruptEnable = 1;
		Timer.Unit = UNIT_MS;
		Timer.dwTimeout = 1000;
		PIPO_InitInfo.Function_t = &Timer;
		PIPO_TimerInit(&PIPO_InitInfo);
		PIPO_UpdateAndStart(PIPO_InitInfo.pPIPOAddr);
		break;
	case PIPO_MODE_PWM:
		PWM.Freq_Hz = 100000;
		PWM.Polarity = 0;
		PWM.DTimeEnable = 0;
		PWM.Align = PIPO_PWM_ALIGN_LEFT;
		PWM.DtimePost = 0;
		PWM.DtimePre = 0;
		PWM.DutyRatioA = 5000;
		PWM.DutyRatioB = 5000;
		PIPO_InitInfo.Function_t = &PWM;
		PIPO_PwmInit(&PIPO_InitInfo);
		PIPO_UpdateAndStart(PIPO_InitInfo.pPIPOAddr);
		break;
	case PIPO_MODE_PulseWideMeasure:
		PulseMeasure.Mode = PULSE_MEASURE_MultHigh;
		PulseMeasure.Unit = UNIT_MS;
		PulseMeasure.Period = 1000;
		PulseMeasure.IntEnable_A = 1;
		PulseMeasure.IntEnable_B = 1;
		PIPO_InitInfo.Function_t = &PulseMeasure;
		PIPO_PulseMeasureInit(&PIPO_InitInfo);
		PIPO_UpdateAndStart(PIPO_InitInfo.pPIPOAddr);
		break;
	case PIPO_MODE_PulseCount:
		PulseCount.Unit = UNIT_MS;
		PulseCount.Period = 1000;
		PulseCount.Trigger_A = PIPO_RISING_EDGE;
		PulseCount.Trigger_B = PIPO_RISING_EDGE;
		PulseCount.IntEnable_A = 1;
		PulseCount.IntEnable_B = 1;
		PIPO_InitInfo.Function_t = &PulseCount;
		PIPO_PulseCountInit(&PIPO_InitInfo);
		PIPO_UpdateAndStart(PIPO_InitInfo.pPIPOAddr);
		break;
	case PIPO_MODE_OutputSingle:
		SinglePulse.PulseWidth = 500;
		SinglePulse.TriggerDelay = 500;
		SinglePulse.Unit = UNIT_MS;
		SinglePulse.IntEnable = 1;
		PIPO_InitInfo.Function_t = &SinglePulse;
		PIPO_SinglePulseInit(&PIPO_InitInfo);
		PIPO_UpdateAndStart(PIPO_InitInfo.pPIPOAddr);
		break;
	case PIPO_MODE_MotorDir:
		pCOP.IntEnableDownflow = 1;
		pCOP.IntEnableOverflow = 1;
		pCOP.MaxAddr = 0x7FFFFFFF;
		pCOP.MinAddr = 0x80000000;
		pCOP.SignEnable = 0;
		PIPO_InitInfo.Function_t = &pCOP;
		PIPO_COPInit(&PIPO_InitInfo);
		PIPO_UpdateAndStart(PIPO_InitInfo.pPIPOAddr);
		break;
	default:
		return ;
	}

}

/*
 * 函数名称：  WDT_Init
 * 函数描述：  看门狗初始化
 * 入    口：
 * 出    口：
 */
void WDT_Init(void)
{
	WDT_InitStructure WDT_InitInfo;

	WDT_InitInfo.cMode = WDT_CR_Mode_SYSReset;   /* WDT超时直接进入系统复位 */
//	WDT_InitInfo.cMode = WDT_CR_Mode_Intr;       /* 当计数器第一次溢出时产生中断，第二次溢出且中断标志未清除则产生系统复位 */
	WDT_InitInfo.dwMs = 5000;
	WDT_InitInfo.dwPrio = WDT_PRIO;
	WDT_InitInfo.WDT_ISR = WDT_ISRCallback;

	//WDT_Config(&WDT_InitInfo);
}

/****************************中断处理函数**************************/
/* Timer0 中断处理函数 */
/*void Timer0_ISRCallback(UINT32 irqid)
{
	static INT32U wdTimeCount;

	Timer_ClearIrqFlag(TIMER0);

	wdTimeCount++;
	if(wdTimeCount>100)
	{
	  wdTimeCount = 0;
	  wdTimeFlag = 1;
	}
	GPIO1A_Pin31_Reversal();
}*/
extern void os_tick_handler(void);
void Timer0_ISRCallback(UINT32 irqid)
{
	Timer_ClearIrqFlag(TIMER0);
	os_tick_handler();
}

/* Timer1 中断处理函数 */
void Timer1_ISRCallback(UINT32 irqid)
{
	Timer_ClearIrqFlag(TIMER1);
}

/* Timer2 中断处理函数 */
void Timer2_ISRCallback(UINT32 irqid)
{
	Timer_ClearIrqFlag(TIMER2);
}

/* Timer3 中断处理函数 */
void Timer3_ISRCallback(UINT32 irqid)
{
	Timer_ClearIrqFlag(TIMER3);
}

/*
 * GPIO1ProtA中断处理函数
 * 输入参数：无
 * 输出参数：无
 * 说明：GPIO1的PortA共用同一中断源，使用同一函数处理
 */
void GPIO1_PortA_ISRCallback(UINT32 irqid)
{
	/* 电平触发 */
	if(GPIO1->INTSTATUS & GPIO_P28)
	{
		Uart0Printf("Generate GPIO1_PortA_Pin28 Intr！\r\n");
		GPIO_ClearInterrupt(GPIO1, GPIO_P28);                    /*清除中断标志*/
//		GPIO_DisableInterrupt(GPIO1,GPIO_P28);
	}

	/* 边沿触发 */
	if(GPIO1->INTSTATUS & GPIO_P29)
	{
		Uart0Printf("Generate GPIO1_PortA_Pin29 Intr！\r\n");
		GPIO_ClearInterrupt(GPIO1, GPIO_P29);                    /*清除中断标志*/
	}
}

/*
 * GPIO0ProtA Pin0中断处理函数
 * 输入参数：无
 * 输出参数：无
 * 说明：中断处理函数内的功能仅做测试用，正常使用时不要讲串口打印放在中断内！
 *     1.在使用电平触发中断时，需要在此函数内禁能中断，且不能再次打开中断，否则
 *     会反复进入该中断，实际应用时在需要的地方重新使能中断。
 *     2.当使用边沿触发时，只需清除中断标志，便不会反复进入中断。
 */
void GPIO0_PortA_Pin0_ISRCallback(UINT32 irqid)
{
	GPIO_DisableInterrupt(GPIO0, GPIO_P0);                     /* 禁能中断 */

	Uart0Printf("Generate GPIO0_PortA_Pin0 Intr！\r\n");

	GPIO_ClearInterrupt(GPIO0, GPIO_P0);                       /* 清除中断标志 */
//	GPIO_EnableInterrupt(GPIO0, GPIO_P0);                      /* 使能中断 */
}

/*
 * GPIO0ProtA Pin1中断处理函数
 * 输入参数：无
 * 输出参数：无
 */
void GPIO0_PortA_Pin1_ISRCallback(UINT32 irqid)
{
	GPIO_ClearInterrupt(GPIO0, GPIO_P1);                       /*清除中断标志*/
}

/*
 * GPIO0ProtA Pin2中断处理函数
 * 输入参数：无
 * 输出参数：无
 */
void GPIO0_PortA_Pin2_ISRCallback(UINT32 irqid)
{
	GPIO_ClearInterrupt(GPIO0, GPIO_P2);	                   /*清除中断标志*/
}

/*
 * GPIO0ProtA Pin3中断处理函数
 * 输入参数：无
 * 输出参数：无
 */
void GPIO0_PortA_Pin3_ISRCallback(UINT32 irqid)
{
	GPIO_ClearInterrupt(GPIO0, GPIO_P3);	                  /*清除中断标志*/
}

/*
 * 看门狗WDT中断处理函数
 * 输入参数：无
 * 输出参数：无
 */
void WDT_ISRCallback(UINT32 irqid)
{
	UINT32 dwV;
	volatile UINT32 dwSta;

	dwV = WDT_GetCounter();
	dwSta = WDT_ClearIntr();
	Uart0Printf("WDT_ISR!%x!,%x!\r\n",dwV,dwSta);
}

/*
 * RTC计数器匹配中断处理函数
 * 输入参数：无
 * 输出参数：无
 */
void RTC_ISRCallback(UINT32 irqid)
{
	if(RTC_GetIntrStatus() == RTC_INTR_ACTIVE)
	{
		RTC_ClearIntr();
		Uart0Printf("alarm!\r\n");
	}
}

/*
 * 中断服务函数
 * 输入参数：
 * 输出参数：无
 */
void IIC_ISRCallback(UINT32 irqid)
{
	UINT32 dwIntrSta = IIC0.IIC_BaseAddr->IC_INTR_STAT;
	UINT8 cTxLevel = 0;
	pStruct_IICInfo pIICInfo = &IIC0;

	// 清除中断
	IIC_ClearInterrupt(&IIC0, dwIntrSta);

	if (dwIntrSta & (IIC_INT_STOP_DET | IIC_INT_TX_ABRT) )  /* I2C收到结束信号 或 发送终止 */
	{
		IIC_Read_RxFIFO(&IIC0, 0);                          /* 读取 RxFIFO中剩余数据 */

		/* 关闭IIC是为了防止数据操作过程中又有新的中断到来，造成数据错误。 */
		IIC_Close(&IIC0);
		IIC_UnLock(&IIC0);

		if(IIC0.IIC_TxRx_CBFunc != NULL)
		{
			IIC0.IIC_TxRx_CBFunc();
		}
	}

	if (dwIntrSta & (IIC_INT_SRD_RES | IIC_INT_TXFTL_BELOW) )/* I2C从机回应读请求 */
	{
		if(IIC_IS_RXFTL_ABOVE(pIICInfo) )                       /* 如果RxFIFO已经超过阈值，应当控制TxFIFO的写入速度。 */
		{
			cTxLevel = 1;
		}
		else
		{
			cTxLevel = (8 - IIC0.IIC_Initstruct.TxFTLevel);
		}

		/* 向TxFIFO写入数据 如果实际写入了0长度字节的数据，表示需要额外填充，此时填充0. */
		if (!IIC_Write_TxFIFO(&IIC0, cTxLevel) )
		{
			IIC_DummyWrite(&IIC0, cTxLevel);
		}
	}

	if (dwIntrSta & IIC_INT_RXFTL_ABOVE)                    /* RX FIFO超过缓存阈值 */
	{
		IIC_Read_RxFIFO(&IIC0, 0);                          /* 读取 RxFIFO数据 */
	}
}


/*
 * 用户自定义的UART0中断处理函数
 * 输入参数：dwIrqid 中断号
 * 输出参数：
 */
void UART0_ISRCallback(UINT32 dwIrqid)
{
    pStruct_UartInfo pInfo;
    pStruct_UartType pUart;
    UINT8 cState;
    UINT8 cData;

    pInfo = &(Uart_Table[UART0]);
    pUart = pInfo->addr;

    cState = pUart->IIR_FCR & 0xf;                               /* 获取中断标号 */
	if((cState == 0x2) && !(pInfo->btxquery))
	{
/*	    if(!CK_CircleBuffer_IsEmpty(&(pInfo->txcirclebuffer)))
	    {
	        CK_CircleBuffer_Read(&(pInfo->txcirclebuffer), &cData);*/
	        pUart->RBR_THR_DLL = cData;
/*	    }*/
	}
	if((cState == 0x4) && !(pInfo->brxquery))
	{
		cData = pUart->RBR_THR_DLL;
/*	    CK_CircleBuffer_Write(&(pInfo->rxcirclebuffer), cData);*/
	}
}

/*
 * 用户自定义的UART1中断处理函数
 * 输入参数：dwIrqid 中断号
 * 输出参数：
 */
void UART1_ISRCallback(UINT32 dwIrqid)
{

}

/*
 * 用户自定义的CAN0中断处理函数
 * 输入参数：dwIrqid 中断号
 * 输出参数：
 */
void CAN0_ISRHandler(UINT32 dwIrqid)
{

}

/*
 * 用户自定义的CAN1中断处理函数
 * 输入参数：dwIrqid 中断号
 * 输出参数：
 */
void CAN1_ISRHandler(UINT32 dwIrqid)
{

}

void PIPO0_ISRCallback(UINT32 irqid)
{
	PIPO_ClearInterrupt(PIPO0);
}
void PIPO1_ISRCallback(UINT32 irqid)
{
	PIPO_ClearInterrupt(PIPO1);
}
void PIPO2_ISRCallback(UINT32 irqid)
{
	PIPO_ClearInterrupt(PIPO2);
}
void PIPO3_ISRCallback(UINT32 irqid)
{
	PIPO_ClearInterrupt(PIPO3);
}
void PIPO4_ISRCallback(UINT32 irqid)
{
	PIPO_ClearInterrupt(PIPO4);
}
void PIPO5_ISRCallback(UINT32 irqid)
{
	PIPO_ClearInterrupt(PIPO5);
}
/**************************end**********************************/
#endif
