#include <oneos_config.h>
#include <bus/bus.h>

#ifdef OS_USING_SPI
#include "drv_spi.h"

static ck_spi_info_t spi0_info = {
    .GPIOAddr     = GPIO1,
    .GPIOPort     = PORTB,
    .spi_miso_pin = GPIO_P3,
    .spi_mosi_pin = GPIO_P4,
    .spi_sck_pin  = GPIO_P2,
    {GPIO1, PORTB, GPIO_P1},    // spi_cs_pin
    .SPI_InitStruct.SPI_BaseAddr = (UINT32)SPI_BASE_ADDR,
    .SPI_InitStruct.datawidth    = SPI_DataWidth_8,
    .SPI_InitStruct.cs           = SPI_CS1,
    .SPI_InitStruct.polarity     = SPI_CLOCK_POLARITY_LOW,
    .SPI_InitStruct.phase        = SPI_CLOCK_PHASE_SECOND_EDGE,
    .SPI_InitStruct.TxFTLevel    = SPI_FIFO_MAX_LEVEL,
    .SPI_InitStruct.RxFTLevel    = SPI_FIFO_MAX_LEVEL,
    .SPI_InitStruct.intrmask     = 0,
};

OS_HAL_DEVICE_DEFINE("SPI_InitStructure", "spi0", spi0_info);

#endif
