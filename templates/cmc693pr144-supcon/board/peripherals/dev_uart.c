//#pragma GCC push_options
//#pragma GCC optimize ("O0")

#include <oneos_config.h>
#include <bsp.h>
#ifdef OS_USING_SERIAL
#include "drv_uart.h"
extern void UART0_IRQHandler(UINT32 dwIrqid);
extern void UART1_IRQHandler(UINT32 dwIrqid);

static ck_uart_info_t uart0_info = {
    .GPIOAddr                 = GPIO1,
    .GPIOPort                 = PORTB,
    .tx_pin                   = GPIO_P26,
    .rx_pin                   = GPIO_P27,
    .UART_Initstruct.UartId   = UART0,
    .UART_Initstruct.BaudRate = B115200,
    .UART_Initstruct.WordSize = WORD_SIZE_8,
    .UART_Initstruct.StopBit  = LCR_STOP_BIT_1,
    .UART_Initstruct.Parity   = NONE,
    .UART_Initstruct.RxMode   = UART_Intr,
    .UART_Initstruct.TxMode   = UART_Query,
    .UART_Initstruct.Priority = UART0_PRIO,
    .UART_Initstruct.UART_ISR = UART0_IRQHandler,
    .UartInfo                 = OS_NULL,
    .use_dma                  = 0,

};
OS_HAL_DEVICE_DEFINE("UART_InitStructure", "uart0", uart0_info);

static ck_uart_info_t uart1_info = {
    .GPIOAddr                 = GPIO1,
    .GPIOPort                 = PORTA,
    .tx_pin                   = GPIO_P2,
    .rx_pin                   = GPIO_P3,
    .UART_Initstruct.UartId   = UART1,
    .UART_Initstruct.BaudRate = B115200,
    .UART_Initstruct.WordSize = WORD_SIZE_8,
    .UART_Initstruct.StopBit  = LCR_STOP_BIT_1,
    .UART_Initstruct.Parity   = NONE,
    .UART_Initstruct.RxMode   = UART_Intr,
    .UART_Initstruct.TxMode   = UART_Query,
    .UART_Initstruct.Priority = UART1_PRIO,
    .UART_Initstruct.UART_ISR = UART1_IRQHandler,
    .UartInfo                 = OS_NULL,
    .use_dma                  = 0,

};
OS_HAL_DEVICE_DEFINE("UART_InitStructure", "uart1", uart1_info);
#endif
//#pragma GCC pop_options
