
#include <bsp.h>
#include <oneos_config.h>
#include <bus/bus.h>
#ifdef OS_USING_CAN
#include <drv_can.h>

extern void CAN0_IRQHandler(UINT32 dwIrqid);
extern void CAN1_IRQHandler(UINT32 dwIrqid);

static ck_can_info_t can0_info = {
    .GPIOAddr            = GPIO1,
    .GPIOPort            = PORTB,
    .tx_pin              = GPIO_P29,
    .rx_pin              = GPIO_P28,
    .can_busoff          = GPIO_P5,
    .can_init.buadrate   = 1000000,
    .can_init.canacr     = 0x12385678, /* 验收位 */
    .can_init.canamr     = 0xffffffff, /* 屏蔽位 */
    .can_init.canmode    = CAN_MODE_STANDARD,
    .can_init.intrVector = CAN_IT_RIE | CAN_IT_OIE, /* 接收中断和溢出中断 */
    .can_init.priority   = CAN0_PRIO,
    .can_init.CAN_ISR    = CAN0_IRQHandler, /* 为NULL时，使用驱动文件中默认的中断处理函数 */
    .can_init.pCAN       = (pStruct_CAN)CAN0_BASE_ADDR,
};

OS_HAL_DEVICE_DEFINE("Struct_CANInit", "CAN0", can0_info);

static ck_can_info_t can1_info = {
    .GPIOAddr            = GPIO1,
    .GPIOPort            = PORTB,
    .tx_pin              = GPIO_P25,
    .rx_pin              = GPIO_P24,
    .can_busoff          = GPIO_P6,
    .can_init.buadrate   = 1000000,
    .can_init.canacr     = 0x12385678, /* 验收位 */
    .can_init.canamr     = 0xffffffff, /* 屏蔽位 */
    .can_init.canmode    = CAN_MODE_STANDARD,
    .can_init.intrVector = CAN_IT_RIE | CAN_IT_OIE, /* 接收中断和溢出中断 */
    .can_init.priority   = CAN1_PRIO,
    .can_init.CAN_ISR    = CAN1_IRQHandler, /* 为NULL时，使用驱动文件中默认的中断处理函数 */
    .can_init.pCAN       = (pStruct_CAN)CAN1_BASE_ADDR,
};
OS_HAL_DEVICE_DEFINE("Struct_CANInit", "CAN1", can1_info);

#endif
