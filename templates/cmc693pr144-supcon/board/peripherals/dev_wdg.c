#include <oneos_config.h>
#include <bsp.h>
#include <bus/bus.h>
#ifdef OS_USING_WDG
#include <wdt.h>

static WDT_InitStructure wdt_info = {

#if 1
    .cMode = WDT_CR_Mode_SYSReset, /* WDT超时直接进入系统复位 */
    .dwMs  = 10000,                // 5000*ms
#else
    .cMode   = WDT_CR_Mode_Intr, /* 当计数器第一次溢出时产生中断，第二次溢出且中断标志未清除则产生系统复位 */
    .dwMs    = 5000,    // 5000*ms
    .dwPrio  = WDT_PRIO,
    .WDT_ISR = WDT_ISRCallback,
#endif
};

OS_HAL_DEVICE_DEFINE("WDT_InitStructure", "wdg", wdt_info);

#endif
