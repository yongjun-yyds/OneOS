//#pragma GCC push_options
//#pragma GCC optimize ("O0")

#include <oneos_config.h>
#include <bsp.h>
#ifdef BSP_USING_TIMER
#include <drv_hwtimer.h>
#include <bus/bus.h>
#include <os_stddef.h>
extern void TIMn_IRQHandler(UINT32 irqid);
extern void TIM2_IRQHandler(UINT32 irqid);

#ifdef BSP_USING_TIM0
static Timer_InitStructure timer0_info = {
    .enTimerID = TIMER0,
    .dwTimeout = 0x3fffffful,
    .wPriority = TIMER0_PRIO,
    .TIMER_ISR = TIMn_IRQHandler,
};
OS_HAL_DEVICE_DEFINE("Timer_InitStructure", "timer0", timer0_info);
#endif

#ifdef BSP_USING_TIM1
static Timer_InitStructure timer1_info = {
    .enTimerID = TIMER1,
    .dwTimeout = 1000000 / OS_TICK_PER_SECOND,
    .wPriority = TIMER1_PRIO,
    .TIMER_ISR = TIMn_IRQHandler,
};
OS_HAL_DEVICE_DEFINE("Timer_InitStructure", "timer1", timer1_info);
#endif

#ifdef BSP_USING_TIM2
static Timer_InitStructure timer2_info = {
    .enTimerID = TIMER2,
    .dwTimeout = 1000000 / OS_TICK_PER_SECOND,
    .wPriority = TIMER2_PRIO,
    .TIMER_ISR = TIMn_IRQHandler,
};
OS_HAL_DEVICE_DEFINE("Timer_InitStructure", "timer2", timer2_info);
#endif

#if 0
void tick_timer_Init(void)
{
    Timer_InitStructure Timer_InitInfo;

    /* timer0��ʼ�� */
    Timer_InitInfo.enTimerID = TIMER2;
    Timer_InitInfo.dwTimeout = 1000000/OS_TICK_PER_SECOND;
    Timer_InitInfo.wPriority = TIMER2_PRIO;
    Timer_InitInfo.TIMER_ISR = TIM2_IRQHandler;
    Timer_Config(&Timer_InitInfo);
}

OS_INIT_CALL(tick_timer_Init, OS_INIT_LEVEL_DEVICE, OS_INIT_SUBLEVEL_LOW);
#endif

#endif

//#pragma GCC pop_options
