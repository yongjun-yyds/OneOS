.file     "ExtendFunc.S"

.export SPI_ReadFlash_asm
//EEPROM-READ模式下SPI连续数据读取函数，仅包含FIFO读写操作。
//void SPI_ReadFlash_asm(pStruct_SPIInfo pSpiInfo, void *pcmd_buffer, UINT32 cmdsize, void *prx_buffer, UINT32 buffersize, UINT8 typewidth);
//参数： R2 pSpiInfo; R3 pcmd_buffer; R4 cmdsize; R5 prx_buffer; R6 buffersize; R7 typewidth_Bits.
.align 4
SPI_ReadFlash_asm:
    subi    R0, 28                  //R0 -= 28
    stw     R2, (R0, 0)             //R2 pSpiInfo
    stw     R3, (R0, 4)             //R3 pcmd_buffer
    stw     R4, (R0, 8)             //R4 cmdsize
    stw     R5, (R0, 12)            //R5 prx_buffer. R6 R7 not changed
    stw     R8, (R0, 16)            //R8 Received Number
    stw     R9, (R0, 20)            //R9 Temp
    stw     R10,(R0, 24)            //R10 SPI->RXFLR

    movi    R9, 64                  //R9 = 64
    addu    R2, R9                  //R2 += R9
    ldw     R2, (R2, 0)             //R2 SPI
    movi    R10, 36                 //R10 = 0x24
    addu    R10, R2                 //R10: SPI->RXFLR
    movi    R9, 96                  //R9 = 0x60
    addu    R2, R9                  //R2: SPI->DR

    movi    R8, 0                   //R8 = 0
    cmplti  R7, 9                   //IF R7 < 9 THEN C = 1
    bf      Label_RE_CmdLoop_16     //IF C == 0 THEN goto Label_RE_CmdLoop_16

Label_RE_CmdLoop_8:
    cmpnei  R4, 0                   //IF R4 != 0 THEN C = 1
    bf      Label_RE_ReadLoop_8     //IF C == 0 THEN goto Label_RE_ReadLoop_8
    ldb     R9, (R3, 0)             //R9 = *(R3)
    stb     R9, (R2, 0)             //SPI->DR = R9
    addi    R3, 1                   //R3 += 1
    subi    R4, 1                   //R4 -= 1
    br      Label_RE_CmdLoop_8      //goto Label_RE_CmdLoop_8
Label_RE_ReadLoop_8:
    cmplt   R8, R6                  //IF R8 < R6 THEN C = 1
    bf      Label_RE_ReadExit       //IF C == 0 THEN Exit
    ldb     R9, (R10, 0)            //R9 = SPI->RXFLR
    cmpnei  R9, 0                   //IF R9 != 0 THEN C = 1
    bf      Label_RE_ReadLoop_8     //IF C == 0 THEN LOOP
    ldb     R9, (R2, 0)             //R9 = SPI->DR
    stb     R9, (R5, 0)             //*(R5) = R9
    addi    R5, 1                   //R5 += 1
    addi    R8, 1                   //R8 += 1
    br      Label_RE_ReadLoop_8     //goto Label_RE_ReadLoop_8

Label_RE_CmdLoop_16:
    cmpnei  R4, 0                   //IF R4 != 0 THEN C = 1
    bf      Label_RE_ReadLoop_16    //IF C == 0 THEN goto Label_RE_ReadLoop_16
    ldh     R9, (R3, 0)             //R9 = *(R3)
    sth     R9, (R2, 0)             //SPI->DR = R9
    addi    R3, 2                   //R3 += 2
    subi    R4, 1                   //R4 -= 1
    br      Label_RE_CmdLoop_16    //goto Label_RE_CmdLoop_16
Label_RE_ReadLoop_16:
    cmplt   R8, R6                  //IF R8 < R6 THEN C = 1
    bf      Label_RE_ReadExit       //IF C == 0 THEN Exit
    ldb     R9, (R10, 0)            //R9 = SPI->RXFLR
    cmpnei  R9, 0                   //IF R9 != 0 THEN C = 1
    bf      Label_RE_ReadLoop_16    //IF C == 0 THEN LOOP
    ldh     R9, (R2, 0)             //R9 = SPI->DR
    sth     R9, (R5, 0)             //*(R5) = R9
    addi    R5, 2                   //R5 += 2
    addi    R8, 1                   //R8 += 1
    br      Label_RE_ReadLoop_16    //goto Label_RE_ReadLoop_16

Label_RE_ReadExit:
    ldw     R2, (R0, 0)             //R2 This
    ldw     R3, (R0, 4)             //R3 pcmd_buffer
    ldw     R4, (R0, 8)             //R4 cmdsize
    ldw     R5, (R0, 12)            //R5 prx_buffer. R6 R7 not changed
    ldw     R8, (R0, 16)            //R8 Received Number
    ldw     R9, (R0, 20)            //R9 Temp
    ldw     R10,(R0, 24)            //R10
    addi    R0, 28                  //R0 += 28
    jmp     R15                     //return


.export SPI_WriteFlash_asm
//Transmit Only模式下SPI连续数发送函数，仅包含FIFO写操作。
//void SPI_WriteFlash_asm(pStruct_SPIInfo pSpiInfo, void *pcmd_buffer, UINT32 cmdsize, void *ptx_buffer, UINT32 buffersize, UINT8 typewidth);
//参数： R2 pSpiInfo; R3 pcmd_buffer; R4 cmdsize; R5 ptx_buffer; R6 buffersize; R7 typewidth_Bits.
.align 4
SPI_WriteFlash_asm:
    subi    R0, 28                  //R0 -= 28
    stw     R2, (R0, 0)             //R2 pSpiInfo
    stw     R3, (R0, 4)             //R3 pcmd_buffer
    stw     R4, (R0, 8)             //R4 cmdsize
    stw     R5, (R0, 12)            //R5 ptx_buffer. R6 R7 not changed
    stw     R8, (R0, 16)            //R8 Transmited Number
    stw     R9, (R0, 20)            //R9 Temp
    stw     R10,(R0, 24)            //R10 SPI->TXFLR

    movi    R9, 64                  //R9 = 64
    addu    R2, R9                  //R2 += R9
    ldw     R2, (R2, 0)             //R2 SPI
    movi    R10, 32                 //R10 = 0x20
    addu    R10, R2                 //R10: SPI->TXFLR
    movi    R9, 96                  //R9 = 0x60
    addu    R2, R9                  //R2: SPI->DR

    movi    R8, 0                   //R8 = 0
    cmplti  R7, 9                   //IF R7 < 9 THEN C = 1
    bf      Label_WE_CmdLoop_16     //IF C == 0 THEN goto Label_WE_CmdLoop_16

Label_WE_CmdLoop_8:
    cmpnei  R4, 0                   //IF R4 != 0 THEN C = 1
    bf      Label_WE_WriteLoop_8    //IF C == 0 THEN goto Label_WE_WriteLoop_8
    ldb     R9, (R3, 0)             //R9 = *(R3)
    stb     R9, (R2, 0)             //SPI->DR = R9
    addi    R3, 1                   //R3 += 1
    subi    R4, 1                   //R4 -= 1
    br      Label_WE_CmdLoop_8      //goto Label_RE_CmdLoop_8
Label_WE_WriteLoop_8:
    cmplt   R8, R6                  //IF R8 < R6 THEN C = 1
    bf      Label_WE_ReadExit       //IF C == 0 THEN Exit
    ldb     R9, (R10, 0)            //R9 = SPI->TXFLR
    cmplti  R9, 15                  //IF R9 < 15 THEN C = 1
    bf      Label_WE_WriteLoop_8    //IF C == 0 THEN LOOP
    ldb     R9, (R5, 0)             //R9 = *(R5)
    stb     R9, (R2, 0)             //SPI->DR = R9
    addi    R5, 1                   //R5 += 1
    addi    R8, 1                   //R8 += 1
    br      Label_WE_WriteLoop_8    //goto Label_WE_WriteLoop_8

Label_WE_CmdLoop_16:
    cmpnei  R4, 0                   //IF R4 != 0 THEN C = 1
    bf      Label_WE_WriteLoop_16   //IF C == 0 THEN goto Label_WE_WriteLoop_16
    ldh     R9, (R3, 0)             //R9 = *(R3)
    sth     R9, (R2, 0)             //SPI->DR = R9
    addi    R3, 2                   //R3 += 2
    subi    R4, 1                   //R4 -= 1
    br      Label_WE_CmdLoop_16     //goto Label_WE_CmdLoop_16
Label_WE_WriteLoop_16:
    cmplt   R8, R6                  //IF R8 < R6 THEN C = 1
    bf      Label_WE_ReadExit       //IF C == 0 THEN Exit
    ldb     R9, (R10, 0)            //R9 = SPI->TXFLR
    cmplti  R9, 15                  //IF R9 < 15 THEN C = 1
    bf      Label_WE_WriteLoop_16   //IF C == 0 THEN LOOP
    ldh     R9, (R5, 0)             //R9 = *(R5)
    sth     R9, (R2, 0)             //SPI->DR = R9
    addi    R5, 2                   //R5 += 2
    addi    R8, 1                   //R8 += 1
    br      Label_WE_WriteLoop_16   //goto Label_WE_WriteLoop_16

Label_WE_ReadExit:
    ldw     R2, (R0, 0)             //R2 This
    ldw     R3, (R0, 4)             //R3 pcmd_buffer
    ldw     R4, (R0, 8)             //R4 cmdsize
    ldw     R5, (R0, 12)            //R5 prx_buffer. R6 R7 not changed
    ldw     R8, (R0, 16)            //R8 Received Number
    ldw     R9, (R0, 20)            //R9 Temp
    ldw     R10,(R0, 24)            //R10
    addi    R0, 28                  //R0 += 28
    jmp     R15                     //return


