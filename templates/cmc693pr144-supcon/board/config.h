/****************************************Copyright (c)**************************************************
**                         Guangzhou ZHIYUAN electronics Co.,LTD.
**
**                               http://www.embedtools.com
**
**--------------File Info-------------------------------------------------------------------------------
** File Name			: config.h
** Last modified Date	: 2004-09-17
** Last Version			: V1.00
** Descriptions			: User Configurable File
**
**------------------------------------------------------------------------------------------------------
** Created By			: Chenmingji
** Created date			: 2004-09-17
** Version				: V1.00
** Descriptions			: First version
**
**------------------------------------------------------------------------------------------------------
** Modified by			: LinEnqiang
** Modified date		: 2007-05-15
** Version				: V1.01
** Descriptions			: Modified for LPC23xx
**
********************************************************************************************************/
#ifndef __CONFIG_H
#define __CONFIG_H

#ifdef __cplusplus
extern "C" {
#endif

#include "ckcore.h"
#include "cache.h"
#include "datatype.h"
#include "misc.h"
#include "circlebuffer.h"
#include "ckpowm.h"

#include "lib_timer.h"
#include "uart.h"
#include "intc.h"
#include "powm.h"
#include "gpio.h"
#include "lib_spi.h"
#include "lib_i2c.h"
#include "flash.h"
#include "pipo.h"
#include "wdt.h"
#include "lib_rtc.h"
#include "lib_can.h"

//#include "bsp.h"

#include "ApiRTC.h"
/********************************
**     应用程序配置
*********************************/

#include "stdio.h"
#include "string.h"
#include "stdlib.h"
#include "setjmp.h"


#define     FLASH_TYPE_NONE                         (0x00)          //NO Flash
#define     FLASH_TYPE_NOR                          (0x01)          //NorFlash (build-in flash)
#define     FLASH_TYPE_SPI                          (0x02)          //SPIFlash

// 选择平台
#define PLATFORM                                PLATFORM_CHIP   /* 选择何种平台，芯片还是S2C */
#define FLASH_TYPE                              FLASH_TYPE_SPI /* 选择Flash类型*/

#define OS_TICKS_PER_SEC    100//50 zhulin change    /* Set the number of ticks in one second  */

#ifdef __cplusplus
	}
#endif

#endif
/*********************************************************************************************************
**                            End Of File
********************************************************************************************************/
