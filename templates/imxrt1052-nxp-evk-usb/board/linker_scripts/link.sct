#!armclang --target=arm-arm-none-eabi -mcpu=cortex-m7 -E -x c
/*
** ###################################################################
**     Processors:          MIMXRT1052CVJ5B
**                          MIMXRT1052CVL5B
**                          MIMXRT1052DVJ6B
**                          MIMXRT1052DVL6B
**
**     Compiler:            Keil ARM C/C++ Compiler
**     Reference manual:    IMXRT1050RM Rev.2.1, 12/2018 | IMXRT1050SRM Rev.2
**     Version:             rev. 1.0, 2018-09-21
**     Build:               b191015
**
**     Abstract:
**         Linker file for the Keil ARM C/C++ Compiler
**
**     Copyright 2016 Freescale Semiconductor, Inc.
**     Copyright 2016-2019 NXP
**     All rights reserved.
**
**     SPDX-License-Identifier: BSD-3-Clause
**
**     http:                 www.nxp.com
**     mail:                 support@nxp.com
**NDEBUG, CPU_MIMXRT1052DVL6B, XIP_BOOT_HEADER_ENABLE=1, XIP_EXTERNAL_FLASH=1,XIP_BOOT_HEADER_DCD_ENABLE=1,SKIP_SYSCLK_INIT, DATA_SECTION_IS_CACHEABLE=1,
** ###################################################################
*/
#include "../../oneos_config.h"
#include "../board_memory.h"
    
#define m_itcm_start                        IMXRT_ITCM_START
#define m_itcm_size                         IMXRT_ITCM_SIZE

#define m_dtcm_start                        IMXRT_DTCM_START
#define m_dtcm_size                         IMXRT_DTCM_SIZE

#define m_octcm_start                       IMXRT_OCTCM_START
#define m_octcm_size                        IMXRT_OCTCM_SIZE

#define m_sdram_start                       IMXRT_SDRAM_START
#define m_sdram_size                        IMXRT_SDRAM_SIZE

#define m_flash_start                       IMXRT_FLASH_START
#define m_flash_size                        IMXRT_FLASH_SIZE

/* Sizes */
#if (defined(__stack_size__))
    #define Stack_Size                      __stack_size__
#else
    #define Stack_Size                      0x1000
#endif

#if (defined(__heap_size__))
    #define Heap_Size                       __heap_size__
#else
    #define Heap_Size                       0x0400
#endif

/* itcm */

/* dtcm */
#define vtor_reserved_ram_start             m_dtcm_start
#define vtor_reserved_ram_size              0x40

#define heap_start                          vtor_reserved_ram_start + vtor_reserved_ram_size
#define heap_size                           Heap_Size

#define m_dmaram_start                      heap_start + heap_size
#define m_dmaram_size                       m_dtcm_size - vtor_reserved_ram_size - heap_size - Stack_Size

#define stack_start                         m_dtcm_start + m_dtcm_size
#define stack_size                          Stack_Size

/* octcm */
#define m_rw_zi_data_start                  m_octcm_start
#define m_rw_zi_data_size                   m_octcm_size

/* sdram */
#define OS_HEAP_START                       IMXRT_OS_HEAP_START
#define OS_HEAP_SIZE                        IMXRT_OS_HEAP_SIZE

#define m_nocache_start                     IMXRT_SDRAM_NOCACHE_START
#define m_nocache_size                      IMXRT_SDRAM_NOCACHE_SIZE

/* flash */
#define m_flash_config_start                m_flash_start
#define m_flash_config_size                 0x00001000

#define m_ivt_start                         m_flash_start + m_flash_config_size
#define m_ivt_size                          0x00001000

#define m_interrupts_start                  m_ivt_start + m_ivt_size
#define m_interrupts_size                   0x00000400

#define m_text_start                        m_interrupts_start + m_interrupts_size
#define m_text_size                         IMXRT_FLASH_APP_SIZE

#if defined(XIP_BOOT_HEADER_ENABLE) && (XIP_BOOT_HEADER_ENABLE == 1)
LR_m_text m_flash_start m_flash_size {   ; load region size_region
    RW_m_config_text m_flash_config_start FIXED m_flash_config_size { ; load address = execution address
        * (.boot_hdr.conf, +FIRST)
    }

    RW_m_ivt_text m_ivt_start FIXED m_ivt_size { ; load address = execution address
        * (.boot_hdr.ivt, +FIRST)
        * (.boot_hdr.boot_data)
        * (.boot_hdr.dcd_data)
    }
#else
LR_m_text m_interrupts_start m_flash_size-m_flash_config_size-m_ivt_size {   ; load region size_region
#endif
    
    VECTOR_ROM m_interrupts_start FIXED m_interrupts_size { ; load address = execution address
        * (vtor_table,+FIRST)
    }

    ER_m_text m_text_start FIXED m_text_size { ; load address = execution address
        * (InRoot$$Sections)
        * (MSymTab)
        .ANY (+RO)
    }

/* itcm */
    ram_function m_itcm_start m_itcm_start {
        * (RamFunction)
    }

/* dtcm */
    reserved_ram m_dtcm_start vtor_reserved_ram_size {
        * (reserved_ram)
    }
    ARM_LIB_HEAP heap_start EMPTY heap_size {    ; Heap region growing up
    }

    DMA_RAM m_dmaram_start EMPTY m_dmaram_size {
    }
    
    ARM_LIB_STACK stack_start EMPTY -stack_size { ; Stack region growing down
    }
    
/* octcm */
    rw_zi_data m_rw_zi_data_start m_rw_zi_data_size {
        .ANY (+RW +ZI)
    }
    
/* sdram */
    OS_HEAP OS_HEAP_START EMPTY OS_HEAP_SIZE {
    }
    
    RW_nancache_data m_nocache_start m_nocache_size {
        * (NonCacheable.init)
        * (*NonCacheable)
        * (.usb_ram)
    }
}
