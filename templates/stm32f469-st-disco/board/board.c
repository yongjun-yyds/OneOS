/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        board.c
 *
 * @brief       Initializes the CPU, System clocks, and Peripheral device
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include "board.h"
#include <drv_gpio.h>

const led_t led_table[] = {
    {GET_PIN(G, 6), PIN_LOW},
    {GET_PIN(D, 4), PIN_LOW},
    {GET_PIN(D, 5), PIN_LOW},
    {GET_PIN(K, 3), PIN_LOW},
};
const int led_table_size = ARRAY_SIZE(led_table);

const struct push_button key_table[] = {
    {GET_PIN(A, 0), PIN_MODE_INPUT_PULLDOWN, PIN_IRQ_MODE_RISING},
};

const int key_table_size = ARRAY_SIZE(key_table);

#ifdef OS_USING_USB_DEVICE
const struct usbd usbd_table[] = {
#ifdef ENABLE_USBD_CDC_CLASS
    {
        .name            = "PCD_USB_OTG_HS",
        .usbd_desc       = &USBD_CDC_Desc,
        .usbd_class      = &USBD_CDC,
        .class_data_size = sizeof(USBD_CDC_HandleTypeDef),
        .interface_type  = PCD_USB_OTG_HS,
    },
#endif
    {
        .name            = "NULL",
        .usbd_desc       = NULL,
        .usbd_class      = NULL,
        .class_data_size = 0,
        .interface_type  = 0,
    },
};

const int usbd_table_size = ARRAY_SIZE(usbd_table);
#endif

/* board-specific init for LCD */
#include "graphic.h"

#define LCD_VSA 1
#define LCD_VBP 15
#define LCD_VFP 16
#define LCD_HSA 2
#define LCD_HBP 34
#define LCD_HFP 34

extern DMA2D_HandleTypeDef hdma2d;
extern LTDC_HandleTypeDef  hltdc;
extern DSI_HandleTypeDef   hdsi;
static DSI_VidCfgTypeDef   hdsivideo_handle;

void MX_LTDC_Init(void)
{
    LTDC_LayerCfgTypeDef pLayerCfg, pLayerCfg1;
    uint32_t             layer_pixel_format = LTDC_PIXEL_FORMAT_RGB565;

    if (OS_GRAPHIC_LCD_FORMAT == OS_GRAPHIC_PIXEL_FORMAT_ARGB8888)
        layer_pixel_format = LTDC_PIXEL_FORMAT_ARGB8888;

    /* Timing Configuration */
    hltdc.Init.HorizontalSync     = (LCD_HSA - 1);
    hltdc.Init.AccumulatedHBP     = (LCD_HSA + LCD_HBP - 1);
    hltdc.Init.AccumulatedActiveW = (OS_GRAPHIC_LCD_WIDTH + LCD_HSA + LCD_HBP - 1);
    hltdc.Init.TotalWidth         = (OS_GRAPHIC_LCD_WIDTH + LCD_HSA + LCD_HBP + LCD_HFP - 1);

    /* Background value */
    hltdc.Init.Backcolor.Blue  = 0;
    hltdc.Init.Backcolor.Green = 0;
    hltdc.Init.Backcolor.Red   = 0;
    hltdc.Init.PCPolarity      = LTDC_PCPOLARITY_IPC;
    hltdc.Instance             = LTDC;

    /* Get LTDC Configuration from DSI Configuration */
    HAL_LTDCEx_StructInitFromVideoConfig(&(hltdc), &(hdsivideo_handle));

    if (HAL_LTDC_Init(&hltdc) != HAL_OK)
    {
        Error_Handler();
    }

    pLayerCfg.WindowX0        = 0;
    pLayerCfg.WindowX1        = OS_GRAPHIC_LCD_WIDTH;
    pLayerCfg.WindowY0        = 0;
    pLayerCfg.WindowY1        = OS_GRAPHIC_LCD_HEIGHT;
    pLayerCfg.PixelFormat     = layer_pixel_format;
    pLayerCfg.Alpha           = 255;
    pLayerCfg.Alpha0          = 0;
    pLayerCfg.BlendingFactor1 = LTDC_BLENDING_FACTOR1_CA;
    pLayerCfg.BlendingFactor2 = LTDC_BLENDING_FACTOR2_CA;
    pLayerCfg.FBStartAdress   = 0;
    pLayerCfg.ImageWidth      = OS_GRAPHIC_LCD_WIDTH;
    pLayerCfg.ImageHeight     = OS_GRAPHIC_LCD_HEIGHT;
    pLayerCfg.Backcolor.Blue  = 0;
    pLayerCfg.Backcolor.Green = 0;
    pLayerCfg.Backcolor.Red   = 0;
    if (HAL_LTDC_ConfigLayer(&hltdc, &pLayerCfg, 0) != HAL_OK)
    {
        Error_Handler();
    }

    // Layer1 is not used
    pLayerCfg1.WindowX0        = 0;
    pLayerCfg1.WindowX1        = 0;
    pLayerCfg1.WindowY0        = 0;
    pLayerCfg1.WindowY1        = 0;
    pLayerCfg1.PixelFormat     = layer_pixel_format;
    pLayerCfg1.Alpha           = 0;
    pLayerCfg1.Alpha0          = 0;
    pLayerCfg1.BlendingFactor1 = LTDC_BLENDING_FACTOR1_CA;
    pLayerCfg1.BlendingFactor2 = LTDC_BLENDING_FACTOR2_CA;
    pLayerCfg1.FBStartAdress   = 0;
    pLayerCfg1.ImageWidth      = 0;
    pLayerCfg1.ImageHeight     = 0;
    pLayerCfg1.Backcolor.Blue  = 0;
    pLayerCfg1.Backcolor.Green = 0;
    pLayerCfg1.Backcolor.Red   = 0;
    if (HAL_LTDC_ConfigLayer(&hltdc, &pLayerCfg1, 1) != HAL_OK)
    {
        Error_Handler();
    }
}

void MX_DSIHOST_DSI_Init(void)
{
    DSI_PLLInitTypeDef   dsiPllInit;
    DSI_PHY_TimerTypeDef PhyTimings;
    uint32_t             color_coding = DSI_RGB565;
    /* The clock must be the same with the config in stm32cube */
    uint32_t LcdClock        = 27429; /* LcdClk = 27429 kHz */
    uint32_t laneByteClk_kHz = 62500; /* 500 MHz / 8 = 62.5 MHz = 62500 kHz */

    uint32_t HACT = OS_GRAPHIC_LCD_WIDTH;
    uint32_t VACT = OS_GRAPHIC_LCD_HEIGHT;

    /* The following values are same for portrait and landscape orientations */
    uint32_t VSA = LCD_VSA;
    uint32_t VBP = LCD_VBP;
    uint32_t VFP = LCD_VFP;
    uint32_t HSA = LCD_HSA;
    uint32_t HBP = LCD_HBP;
    uint32_t HFP = LCD_HFP;

    if (OS_GRAPHIC_LCD_FORMAT == OS_GRAPHIC_PIXEL_FORMAT_ARGB8888)
        color_coding = DSI_RGB888;

    /* Base address of DSI Host/Wrapper registers to be set before calling De-Init */
    hdsi.Instance = DSI;
    HAL_DSI_DeInit(&(hdsi));

    // PLL DSI Config
    dsiPllInit.PLLNDIV = 125;
    dsiPllInit.PLLIDF  = DSI_PLL_IN_DIV2;
    dsiPllInit.PLLODF  = DSI_PLL_OUT_DIV1;

    /* Set number of Lanes */
    hdsi.Init.NumberOfLanes = DSI_TWO_DATA_LANES;
    /* Txclkesc=15625=laneByteClk_kHz/4 */
    hdsi.Init.TXEscapeCkdiv = 4;

    HAL_DSI_Init(&(hdsi), &(dsiPllInit));

    hdsivideo_handle.VirtualChannelID     = 0;
    hdsivideo_handle.ColorCoding          = color_coding;
    hdsivideo_handle.VSPolarity           = DSI_VSYNC_ACTIVE_HIGH;
    hdsivideo_handle.HSPolarity           = DSI_HSYNC_ACTIVE_HIGH;
    hdsivideo_handle.DEPolarity           = DSI_DATA_ENABLE_ACTIVE_HIGH;
    hdsivideo_handle.Mode                 = DSI_VID_MODE_BURST; /* Mode Video burst ie : one LgP per line */
    hdsivideo_handle.NullPacketSize       = 0xFFF;
    hdsivideo_handle.NumberOfChunks       = 0;
    hdsivideo_handle.PacketSize           = HACT; /* Value depending on display orientation choice portrait/landscape */
    hdsivideo_handle.HorizontalSyncActive = (HSA * laneByteClk_kHz) / LcdClock;
    hdsivideo_handle.HorizontalBackPorch  = (HBP * laneByteClk_kHz) / LcdClock;
    hdsivideo_handle.HorizontalLine       = ((HACT + HSA + HBP + HFP) * laneByteClk_kHz) / LcdClock; /* Value depending on display orientation choice portrait/landscape */
    hdsivideo_handle.VerticalSyncActive = VSA;
    hdsivideo_handle.VerticalBackPorch  = VBP;
    hdsivideo_handle.VerticalFrontPorch = VFP;
    hdsivideo_handle.VerticalActive     = VACT; /* Value depending on display orientation choice portrait/landscape */

    /* Enable or disable sending LP command while streaming is active in video mode */
    hdsivideo_handle.LPCommandEnable = DSI_LP_COMMAND_ENABLE; /* Enable sending commands in mode LP (Low Power) */

    /* Largest packet size possible to transmit in LP mode in VSA, VBP, VFP regions */
    /* Only useful when sending LP packets is allowed while streaming is active in video mode */
    hdsivideo_handle.LPLargestPacketSize = 16;

    /* Largest packet size possible to transmit in LP mode in HFP region during VACT period */
    /* Only useful when sending LP packets is allowed while streaming is active in video mode */
    hdsivideo_handle.LPVACTLargestPacketSize = 0;

    /* Specify for each region of the video frame, if the transmission of command in LP mode is allowed in this region
     */
    /* while streaming is active in video mode */
    hdsivideo_handle.LPHorizontalFrontPorchEnable = DSI_LP_HFP_ENABLE; /* Allow sending LP commands during HFP period */
    hdsivideo_handle.LPHorizontalBackPorchEnable  = DSI_LP_HBP_ENABLE; /* Allow sending LP commands during HBP period */
    hdsivideo_handle.LPVerticalActiveEnable     = DSI_LP_VACT_ENABLE; /* Allow sending LP commands during VACT period */
    hdsivideo_handle.LPVerticalFrontPorchEnable = DSI_LP_VFP_ENABLE;  /* Allow sending LP commands during VFP period */
    hdsivideo_handle.LPVerticalBackPorchEnable  = DSI_LP_VBP_ENABLE;  /* Allow sending LP commands during VBP period */
    hdsivideo_handle.LPVerticalSyncActiveEnable = DSI_LP_VSYNC_ENABLE; /* Allow sending LP commands during VSync = VSA period */

    /* Configure DSI Video mode timings with settings set above */
    HAL_DSI_ConfigVideoMode(&(hdsi), &(hdsivideo_handle));

    /* Configure DSI PHY HS2LP and LP2HS timings */
    PhyTimings.ClockLaneHS2LPTime  = 35;
    PhyTimings.ClockLaneLP2HSTime  = 35;
    PhyTimings.DataLaneHS2LPTime   = 35;
    PhyTimings.DataLaneLP2HSTime   = 35;
    PhyTimings.DataLaneMaxReadTime = 0;
    PhyTimings.StopWaitTime        = 10;
    HAL_DSI_ConfigPhyTimer(&hdsi, &PhyTimings);
}
