#ifndef __ONEOS_CONFIG_H__
#define __ONEOS_CONFIG_H__

/* Generated by Kconfiglib (https://github.com/ulfalizer/Kconfiglib) */
#define BOARD_F469_DISCO
#define ARCH_ARM
#define ARCH_ARM_CORTEX_M
#define ARCH_ARM_CORTEX_M4

/* Kernel */

#define OS_NAME_MAX_15
#define OS_NAME_MAX 15
#define OS_TASK_PRIORITY_32
#define OS_TASK_PRIORITY_MAX 32
#define OS_TICK_PER_SECOND 100
#define OS_SCHEDULE_TIME_SLICE 10
#define OS_USING_OVERFLOW_CHECK
#define OS_USING_ASSERT
#define OS_USING_KERNEL_LOCK_CHECK
#define OS_USING_KERNEL_DEBUG
#define KLOG_GLOBAL_LEVEL_WARNING
#define KLOG_GLOBAL_LEVEL 1
#define KLOG_USING_COLOR
#define KLOG_WITH_FUNC_LINE
#define OS_MAIN_TASK_STACK_SIZE 2048
#define OS_IDLE_TASK_STACK_SIZE 512
#define OS_RECYCLE_TASK_STACK_SIZE 512
#define OS_USING_KERNEL_TIMER
#define OS_USING_TIMER
#define OS_TIMER_TASK_STACK_SIZE 512
#define OS_TIMER_POWER 3
#define OS_USING_WORKQUEUE
#define OS_USING_SYSTEM_WORKQUEUE
#define OS_SYSTEM_WORKQUEUE_STACK_SIZE 2048
#define OS_SYSTEM_WORKQUEUE_PRIORITY 8

/* Inter-task communication and synchronization */

#define OS_USING_MUTEX
#define OS_USING_SEMAPHORE
#define OS_USING_EVENT
#define OS_USING_MESSAGEQUEUE
#define OS_USING_MAILBOX
/* end of Inter-task communication and synchronization */

/* Memory management */

#define OS_USING_SYS_HEAP
#define OS_USING_MEM_HEAP
#define OS_USING_ALG_FIRSTFIT
#define OS_USING_MEM_POOL
/* end of Memory management */
/* end of Kernel */

/* C standard library adapter */

#define OS_USING_LIBC_ADAPTER
#define OS_USING_NEWLIB_ADAPTER
#define OS_USING_ARMCCLIB_ADAPTER
/* end of C standard library adapter */

/* Osal */

/* POSIX compatibility layer */

/* end of POSIX compatibility layer */

/* RT-Thread compatibility layer */

/* end of RT-Thread compatibility layer */

/* CMSIS compatibility layer */

/* end of CMSIS compatibility layer */

/* FreeRTOS compatibility layer */

/* end of FreeRTOS compatibility layer */

/* C++ Features */

/* end of C++ Features */
/* end of Osal */

/* Drivers */

#define OS_USING_DEVICE
#define OS_USING_DEVICE_NOTIFY
#define OS_DEVICE_SUPPORT_PLUG

/* Audio */

#define OS_USING_AUDIO
#define OS_AUDIO_DATA_CONFIG
#define OS_AUDIO_REPLAY_MP_BLOCK_SIZE 2048
#define OS_AUDIO_REPLAY_MP_BLOCK_COUNT 5
#define OS_AUDIO_RECORD_FIFO_COUNT 4
#define OS_USING_SAI
#define BSP_AUDIO_DATA_TX_BUS "sai_BlockA1"
#define BSP_USING_CS43L22
#define BSP_USING_CS43L22_CONFIG
#define BSP_CS43L22_I2C_BUS "soft_i2c2"
#define BSP_CS43L22_I2C_ADDR 0x4a
#define BSP_USING_CS43L22_DATA
#define BSP_CS43L22_RST_PIN 66
/* end of Audio */

/* BLOCK */

#define OS_USING_BLOCK
/* end of BLOCK */

/* Boot */

/* CORTEX-M Boot */

#define BSP_INCLUDE_VECTOR_TABLE
#define BSP_BOOT_OPTION
#define BSP_TEXT_SECTION_ADDR 0x08000000
#define BSP_TEXT_SECTION_SIZE 0x00200000
#define BSP_DATA_SECTION_ADDR 0x20000000
#define BSP_DATA_SECTION_SIZE 0x00050000
/* end of CORTEX-M Boot */
/* end of Boot */

/* CAN */

#define OS_USING_CAN
/* end of CAN */

/* CONSOLE */

#define OS_USING_CONSOLE
#define OS_CONSOLE_DEVICE_NAME "uart3"
/* end of CONSOLE */

/* DMA */

#define OS_USING_DMA
#define OS_USING_DMA_RAM
#define OS_USING_SOFT_DMA
#define OS_SOFT_DMA_SUPPORT_NORMAL_MODE
#define OS_SOFT_DMA_SUPPORT_CIRCLE_MODE
/* end of DMA */

/* EEPROM */

/* end of EEPROM */

/* FAL */

/* end of FAL */

/* Graphic */

#define OS_USING_GRAPHIC
#define OS_GRAPHIC_LCD_WIDTH 800
#define OS_GRAPHIC_LCD_HEIGHT 480
#define OS_GRAPHIC_LCD_FORMAT_RGB565
#define OS_USING_DSI
#define OS_USING_DSI_RST 119
#define BSP_USING_OTM8009A
#define BSP_OTM8009A_DSI_CHANNEL 0
/* end of Graphic */

/* HAL */

#define MANUFACTOR_STM32
#define SERIES_STM32F4
#define SOC_STM32F469xx

/* Configure base hal in STM32CubeMX */

/* end of HAL */

/* HwCrypto */

/* end of HwCrypto */

/* I2C */

#define OS_USING_I2C
#define OS_USING_I2C_BITOPS
#define SOFT_I2C_BUS_DELAY_US 10
#define BSP_USING_SOFT_I2C1
#define BSP_SOFT_I2C1_SCL_PIN 24
#define BSP_SOFT_I2C1_SDA_PIN 25
#define BSP_USING_SOFT_I2C2
#define BSP_SOFT_I2C2_SCL_PIN 116
#define BSP_SOFT_I2C2_SDA_PIN 117
/* end of I2C */

/* Infrared */

/* end of Infrared */

/* LPMGR */

/* end of LPMGR */

/* MISC */

#define OS_USING_PUSH_BUTTON
#define OS_USING_LED
#define OS_USING_ADC
#define OS_USING_DAC
/* end of MISC */

/* MTD */

#define OS_USING_MTD
/* end of MTD */

/* NAND */

/* end of NAND */

/* NET */

/* end of NET */

/* PIN */

#define OS_USING_PIN
#define OS_PIN_MAX_CHIP 1
/* end of PIN */

/* RTC */

#define OS_USING_RTC
#define OS_RTC_DEV_NAME "rtc"
/* end of RTC */

/* SDIO */

#define OS_USING_SDIO
#define OS_SDIO_STACK_SIZE 512
#define OS_SDIO_TASK_PRIORITY 15
#define OS_MMCSD_STACK_SIZE 1024
#define OS_MMCSD_TASK_PREORITY 22
#define OS_MMCSD_MAX_PARTITION 16
/* end of SDIO */

/* Sensors */

/* end of Sensors */

/* Serial */

#define OS_USING_SERIAL
#define OS_SERIAL_DELAY_CLOSE
#define OS_SERIAL_RX_BUFSZ 64
#define OS_SERIAL_TX_BUFSZ 64

/* posix serial */

/* end of posix serial */

/* rtt uart */

/* end of rtt uart */
/* end of Serial */

/* SFLASH */

/* end of SFLASH */

/* SN */

/* end of SN */

/* SPI */

#define OS_USING_SPI
/* end of SPI */

/* Timer */

#define OS_USING_TIMER_DRIVER
#define OS_USING_CLOCKSOURCE
#define OS_CLOCKSOURCE_BEST ""
#define OS_USING_CLOCKEVENT
#define OS_CLOCKEVENT_BEST ""
#define OS_USING_HRTIMER
#define OS_USING_HRTIMER_FOR_KERNEL_TICK

/* cortex-m & riscv hardware timer config */

#define OS_USING_SYSTICK_FOR_CLOCKSOURCE
/* end of cortex-m & riscv hardware timer config */
/* end of Timer */

/* TinyUSB */

/* end of TinyUSB */

/* Touch */

#define OS_USING_TOUCH
#define OS_TOUCH_X_REVERSE
#define OS_TOUCH_XY_SWAP
#define OS_USING_FT6X06
#define OS_FT6X060_I2C_BUS_NAME "soft_i2c1"
#define OS_FT6X06_I2C_ADDR 0x2a
#define OS_FT6X06_IRQ_PIN 149
/* end of Touch */

/* USB */

#define OS_USING_USB_DEVICE
#define OS_USBD_TASK_STACK_SZ 4096
/* end of USB */

/* WDG */

#define OS_USING_WDG
/* end of WDG */
/* end of Drivers */

/* Components */

/* MicroPython */

/* end of MicroPython */

/* WWD */

/* end of WWD */

/* AMS */

/* end of AMS */

/* Atest */

#define OS_USING_ATEST
#define ATEST_TASK_STACK_SIZE 4096
#define ATEST_TASK_PRIORITY 20
/* end of Atest */

/* BLE */

/* end of BLE */

/* cJSON */

/* end of cJSON */

/* CLI */

/* end of CLI */

/* Cloud */

/* Aliyun */

/* end of Aliyun */

/* AWS */

/* end of AWS */

/* Baidu */

/* end of Baidu */

/* CTWing */

/* MQTT */

/* end of MQTT */
/* end of CTWing */

/* Huawei */

/* end of Huawei */

/* OneNET */

/* MQTT kit */

/* end of MQTT kit */

/* NB-IoT kit */

/* end of NB-IoT kit */

/* EDP */

/* end of EDP */
/* end of OneNET */
/* end of Cloud */

/* CMS */

#define CMS_LITE

/* CMS Connect */

/* end of CMS Connect */

/* CMS ID */

/* end of CMS ID */
/* end of CMS */

/* Diagnose */


/* eCoreDump */

/* end of eCoreDump */

/* Memory Monitor */

/* end of Memory Monitor */
/* end of Diagnose */

/* Dlog */

#define OS_USING_DLOG
#define DLOG_PRINT_LVL_W
#define DLOG_GLOBAL_PRINT_LEVEL 4
#define DLOG_COMPILE_LVL_D
#define DLOG_COMPILE_LEVEL 7
#define DLOG_USING_FILTER

/* Log format */

#define DLOG_WITH_FUNC_LINE
#define DLOG_USING_COLOR
#define DLOG_OUTPUT_TIME_INFO
/* end of Log format */

/* Dlog backend option */

#define DLOG_BACKEND_USING_CONSOLE
/* end of Dlog backend option */
/* end of Dlog */

/* Easyflash */

/* end of Easyflash */

/* FileSystem */

#define OS_USING_VFS
#define VFS_MOUNTPOINT_MAX 4
#define VFS_FILESYSTEM_TYPES_MAX 2
#define VFS_FD_MAX 16
#define OS_USING_VFS_DEVFS
#define OS_USING_VFS_FATFS

/* Elm-ChaN's FatFs, generic FAT filesystem module */

#define OS_VFS_FAT_CODE_PAGE 437
#define OS_VFS_FAT_USE_LFN_3
#define OS_VFS_FAT_USE_LFN 3
#define OS_VFS_FAT_MAX_LFN 255
#define OS_VFS_FAT_DRIVES 2
#define OS_VFS_FAT_MAX_SECTOR_SIZE 1024
#define OS_VFS_FAT_REENTRANT
/* end of Elm-ChaN's FatFs, generic FAT filesystem module */
/* end of FileSystem */

/* GUI */

#define OS_GUI_DISP_DEV_NAME "lcd"
#define OS_GUI_INPUT_DEV_NAME "touch"
#define OS_USING_GUI_LVGL
#define OS_USING_GUI_LVGL_8_2

/* LVGL basic menu */

#define OS_LV_BUFF_LINES 120
#define LV_DISP_DEF_REFR_PERIOD 20
#define LV_INDEV_DEF_READ_PERIOD 30
#define OS_USING_GPU_STM32_DMA2D
#define OS_USING_DRAW_COMPLEX
#define LV_SHADOW_CACHE_SIZE 0
#define LV_CIRCLE_CACHE_SIZE 4
/* end of LVGL basic menu */

/* Widget usage */

#define _LV_USE_ARC
#define _LV_USE_BAR
#define _LV_USE_BTN
#define _LV_USE_BTNMATRIX
#define _LV_USE_CANVAS
#define _LV_USE_CHECKBOX
#define _LV_USE_DROPDOWN
#define _LV_USE_IMG
#define _LV_USE_LABEL
#define _LV_LABEL_TEXT_SELECTION
#define _LV_LABEL_LONG_TXT_HINT
#define _LV_USE_LINE
#define _LV_USE_ROLLER
#define _LV_ROLLER_INF_PAGES 7
#define _LV_USE_SLIDER
#define _LV_USE_SWITCH
#define _LV_USE_TEXTAREA
#define _LV_TEXTAREA_DEF_PWD_SHOW_TIME 1500
#define _LV_USE_TABLE
/* end of Widget usage */

/* Extra Widgets */

#define _LV_USE_ANIMIMG
#define _LV_USE_CALENDAR
#define _LV_USE_CALENDAR_HEADER_ARROW
#define _LV_USE_CALENDAR_HEADER_DROPDOWN
#define _LV_USE_CHART
#define _LV_USE_COLORWHEEL
#define _LV_USE_IMGBTN
#define _LV_USE_KEYBOARD
#define _LV_USE_LED
#define _LV_USE_LIST
#define _LV_USE_MENU
#define _LV_USE_METER
#define _LV_USE_MSGBOX
#define _LV_USE_SPINBOX
#define _LV_USE_SPINNER
#define _LV_USE_TABVIEW
#define _LV_USE_TILEVIEW
#define _LV_USE_WIN
#define _LV_USE_SPAN
#define _LV_SPAN_SNIPPET_STACK_SIZE 64
/* end of Extra Widgets */

/* Themes */

#define _LV_USE_THEME_DEFAULT
#define _LV_THEME_DEFAULT_GROW
#define _LV_THEME_DEFAULT_TRANSITION_TIME 80
#define _LV_USE_THEME_BASIC
/* end of Themes */

/* Layouts */

#define _LV_USE_FLEX
#define _LV_USE_GRID
/* end of Layouts */

/* Text Settings */

#define _LV_TXT_ENC_UTF8
/* end of Text Settings */

/* Font usage */

/* Enable built-in fonts */

#define _LV_FONT_MONTSERRAT_12
#define _LV_FONT_MONTSERRAT_14
#define _LV_FONT_MONTSERRAT_16
/* end of Enable built-in fonts */
#define _LV_FONT_DEFAULT_MONTSERRAT_14
/* end of Font usage */

/* LVGL FileSystem */

/* end of LVGL FileSystem */

/* LVGL Log */

/* end of LVGL Log */

/* LVGL Asserts */

#define _LV_USE_ASSERT_NULL
#define _LV_USE_ASSERT_MALLOC
/* end of LVGL Asserts */

/* Third party Lib */

/* end of Third party Lib */

/* Extra */

/* Libs */

/* FreeType */

/* end of FreeType */
/* end of Libs */
/* end of Extra */
#define OS_USING_GUI_LVGL_DEMO
#define LV_USE_DEMO_BENCHMARK 1
#define LV_USE_DEMO_MUSIC 0
#define LV_USE_DEMO_KEYPAD_AND_ENCODER 0
#define LV_USE_DEMO_STRESS 0
#define LV_USE_DEMO_WIDGETS 0
/* end of GUI */

/* Industrial */

/* CANOpen */

/* end of CANOpen */

/* CoDeSys */

/* end of CoDeSys */

/* ModBus */

/* end of ModBus */
/* end of Industrial */

/* IoTjs */

/* end of IoTjs */

/* JerryScript */

/* end of JerryScript */

/* Network */

/* Acw */

/* end of Acw */

/* TCP/IP */

/* LwIP */

/* end of LwIP */
/* end of TCP/IP */

/* Molink */

/* end of Molink */

/* Protocols */

/* CoAP */

/* end of CoAP */

/* HTTP */

/* httpclient-v1.1.0 */

/* end of httpclient-v1.1.0 */
/* end of HTTP */

/* LWM2M */

/* LWM2M-v1.0.0 */

/* end of LWM2M-v1.0.0 */
/* end of LWM2M */

/* MQTT */

/* end of MQTT */

/* Websocket */

/* end of Websocket */
/* end of Protocols */

/* Socket */

/* end of Socket */
/* end of Network */

/* Iotivity */

/* end of Iotivity */

/* Optparse */

/* end of Optparse */

/* OTA */

/* Fota by CMIOT */

/* end of Fota by CMIOT */
/* end of OTA */

/* Position */

/* end of Position */

/* PowerManager */

/* end of PowerManager */

/* Ramdisk */

/* end of Ramdisk */

/* Security */


/* OneTLS */

/* end of OneTLS */
/* end of Security */

/* Shell */

#define OS_USING_SHELL
#define SHELL_TASK_NAME "tshell"
#define SHELL_TASK_PRIORITY 20
#define SHELL_TASK_STACK_SIZE 2048
#define SHELL_USING_HISTORY
#define SHELL_HISTORY_LINES 5
#define SHELL_USING_DESCRIPTION
#define SHELL_CMD_SIZE 80
#define SHELL_PROMPT_SIZE 256
#define SHELL_ARG_MAX 10
/* end of Shell */

/* SQL */

/* end of SQL */

/* telnetd */

/* end of telnetd */
/* end of Components */

/* Debug */

#define OS_DEBUG
#define LOG_BUFF_SIZE_256
#define OS_LOG_BUFF_SIZE 256
/* end of Debug */

#endif /* __ONEOS_CONFIG_H__ */

