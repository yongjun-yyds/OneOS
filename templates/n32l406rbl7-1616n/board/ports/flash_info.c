static struct onchip_flash_info flash_info = {
    .start_addr = N32_FLASH_START_ADDR,
    .capacity   = N32_FLASH_SIZE,
    .block_size = N32_FLASH_BLOCK_SIZE,
    .page_size  = N32_FLASH_PAGE_SIZE,
};
OS_HAL_DEVICE_DEFINE("N32GL40X_Onchip_Flash", "onchip_flash", flash_info);
