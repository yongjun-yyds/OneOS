#include <fal.h>
#include <driver.h>
#include <board.h>

static struct onchip_flash_info onchip_flash = {
    .start_addr = CH32_FLASH_START_ADRESS,
    .capacity   = CH32_FLASH_SIZE,
    .block_size = CH32_FLASH_BLOCK_SIZE,
    .page_size  = CH32_FLASH_PAGE_SIZE,
};
OS_HAL_DEVICE_DEFINE("Onchip_Flash_Type", "onchip_flash", onchip_flash);
