static struct onchip_flash_info onchip_flash = {
    .start_addr = CH_FLASH_START_ADRESS,
    .capacity   = CH_FLASH_SIZE,
    .block_size = CH_FLASH_BLOCK_SIZE,
    .page_size  = CH_FLASH_BLOCK_SIZE,
};
OS_HAL_DEVICE_DEFINE("Onchip_Flash_Type", "onchip_flash", onchip_flash);
