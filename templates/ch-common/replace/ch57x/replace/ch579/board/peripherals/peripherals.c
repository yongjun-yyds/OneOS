#include <oneos_config.h>
#include <driver.h>
#include <bus/bus.h>
#include "CH57x_common.h"
#include "CH57x_uart.h"
#ifdef BSP_USING_TIM
#include "drv_hwtimer.h"
#endif

#ifdef BSP_USING_UART
#include "drv_uart.h"

#ifdef BSP_USING_UART0
/* UART0 TX-->B.7  RX-->B.4 */
const struct ch_uart_info uart0_info = {
    .tx_port = GPIOB,
    .tx_pin  = GPIO_Pin_7,

    .rx_port = GPIOB,
    .rx_pin  = GPIO_Pin_4,
    
    .huart   = UART0,

    .uart_def_cfg =
        {
            .UART_BaudRate            = 115200,
            .UART_WordLength          = CH_DATA_BITS_8,
            .UART_StopBits            = CH_STOP_BITS_1,
            .UART_Parity              = CH_PARITY_NONE,
            .UART_HardwareFlowControl = DISABLE,
            .UART_RxFIFO              = ENABLE,
            .UART_RxFIFOTrigBits      = CH_FIFO_TRIG_BITS_4,
            .UART_Irq                 = UART0_IRQn,
            .UART_IrqMode             = RB_IER_RECV_RDY | RB_IER_LINE_STAT,
        },
};

OS_HAL_DEVICE_DEFINE("UART_TypeDef", "uart0", uart0_info);
#endif

#ifdef BSP_USING_UART1
/* UART1 TX-->A.9  RX-->A.8 */
const struct ch_uart_info uart1_info = {
    .tx_port = GPIOA,
    .tx_pin  = GPIO_Pin_9,

    .rx_port = GPIOA,
    .rx_pin  = GPIO_Pin_8,
    
    .huart   = UART1,

    .uart_def_cfg =
        {
            .UART_BaudRate            = 115200,
            .UART_WordLength          = CH_DATA_BITS_8,
            .UART_StopBits            = CH_STOP_BITS_1,
            .UART_Parity              = CH_PARITY_NONE,
            .UART_HardwareFlowControl = DISABLE,
            .UART_RxFIFO              = ENABLE,
            .UART_RxFIFOTrigBits      = CH_FIFO_TRIG_BITS_4,
            .UART_Irq                 = UART1_IRQn,
            .UART_IrqMode             = RB_IER_RECV_RDY | RB_IER_LINE_STAT,
        },
};

OS_HAL_DEVICE_DEFINE("UART_TypeDef", "uart1", uart1_info);
#endif

#ifdef BSP_USING_UART2
/* UART2 TX-->A.7  RX-->A.6 */
const struct ch_uart_info uart2_info = {
    .tx_port = GPIOA,
    .tx_pin  = GPIO_Pin_7,

    .rx_port = GPIOA,
    .rx_pin  = GPIO_Pin_6,
    
    .huart   = UART2,

    .uart_def_cfg =
        {
            .UART_BaudRate            = 115200,
            .UART_WordLength          = CH_DATA_BITS_8,
            .UART_StopBits            = CH_STOP_BITS_1,
            .UART_Parity              = CH_PARITY_NONE,
            .UART_HardwareFlowControl = DISABLE,
            .UART_RxFIFO              = ENABLE,
            .UART_RxFIFOTrigBits      = CH_FIFO_TRIG_BITS_4,
            .UART_Irq                 = UART2_IRQn,
            .UART_IrqMode             = RB_IER_RECV_RDY | RB_IER_LINE_STAT,
        },
};

OS_HAL_DEVICE_DEFINE("UART_TypeDef", "uart2", uart2_info);
#endif

#ifdef BSP_USING_UART3
/* UART3 TX-->A.5  RX-->A.4 */
const struct ch_uart_info uart3_info = {
    .tx_port = GPIOA,
    .tx_pin  = GPIO_Pin_5,

    .rx_port = GPIOA,
    .rx_pin  = GPIO_Pin_4,
    
    .huart   = UART3,

    .uart_def_cfg =
        {
            .UART_BaudRate            = 115200,
            .UART_WordLength          = CH_DATA_BITS_8,
            .UART_StopBits            = CH_STOP_BITS_1,
            .UART_Parity              = CH_PARITY_NONE,
            .UART_HardwareFlowControl = DISABLE,
            .UART_RxFIFO              = ENABLE,
            .UART_RxFIFOTrigBits      = CH_FIFO_TRIG_BITS_4,
            .UART_Irq                 = UART3_IRQn,
            .UART_IrqMode             = RB_IER_RECV_RDY | RB_IER_LINE_STAT,
        },
};

OS_HAL_DEVICE_DEFINE("UART_TypeDef", "uart3", uart3_info);
#endif
#endif


#ifdef BSP_USING_TIM

#ifdef BSP_USING_TIM0
static const struct ch_timer_info tim0_info = {
    .htim    = TIM0,
    .IT_mode = TMR0_3_IT_CYC_END,
    .irq_num = TMR0_IRQn,
};
OS_HAL_DEVICE_DEFINE("TIM_TypeDef", "tim0", tim0_info);
#endif

#ifdef BSP_USING_TIM1
static const struct ch_timer_info tim1_info = {
    .htim    = TIM1,
    .IT_mode = TMR0_3_IT_CYC_END,
    .irq_num = TMR1_IRQn,
};
OS_HAL_DEVICE_DEFINE("TIM_TypeDef", "tim1", tim1_info);
#endif

#ifdef BSP_USING_TIM2
static const struct ch_timer_info tim2_info = {
    .htim    = TIM2,
    .IT_mode = TMR0_3_IT_CYC_END,
    .irq_num = TMR2_IRQn,
};
OS_HAL_DEVICE_DEFINE("TIM_TypeDef", "tim2", tim2_info);
#endif

#ifdef BSP_USING_TIM3
static const struct ch_timer_info tim3_info = {
    .htim    = TIM3,
    .IT_mode = TMR0_3_IT_CYC_END,
    .irq_num = TMR3_IRQn,
};
OS_HAL_DEVICE_DEFINE("TIM_TypeDef", "tim3", tim3_info);
#endif

#endif

