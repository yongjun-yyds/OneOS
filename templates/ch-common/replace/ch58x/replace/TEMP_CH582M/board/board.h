/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        board.h
 *
 * @brief       Board resource definition
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __BOARD_H__
#define __BOARD_H__

#include "CH58x_common.h"
#include <oneos_config.h>
#include <os_assert.h>
#include <os_types.h>
#include <drv_cfg.h>

#ifdef __cplusplus
extern "C" {
#endif

#define SOC_MODEL "CH582M"

#define TOGGLE_SP() asm("csrrw sp,mscratch,sp")

#define CH_SRAM_SIZE  (30 * 1024)   /* total 32KB, irq stack used last 2k */
#define CH_SRAM_START (0x20000000)
#define CH_SRAM_END   (CH_SRAM_START + CH_SRAM_SIZE)

#define CH_FLASH_BLOCK_SIZE    (4 * 1024)

#define CH_FLASH_START_ADDRESS ((uint32_t)0x00000000)
#define CH_FLASH_SIZE          (448 * 1024)
#define CH_FLASH_END_ADDRESS   ((uint32_t)(CH_FLASH_START_ADDRESS + CH_FLASH_SIZE))

#if defined(__CC_ARM) || defined(__CLANG_ARM)
extern int Image$$RW_IRAM1$$ZI$$Limit;
#define HEAP_BEGIN (&Image$$RW_IRAM1$$ZI$$Limit)
#elif __ICCARM__
#pragma section = "HEAP"
#define HEAP_BEGIN (__segment_end("HEAP"))
#else
extern int __bss_end;
#define HEAP_BEGIN (&__bss_end)
#endif

#define HEAP_END CH_SRAM_END

#ifdef OS_USING_PUSH_BUTTON
extern const struct push_button key_table[];
extern const int                key_table_size;
#endif

#ifdef OS_USING_LED
extern const led_t led_table[];
extern const int   led_table_size;
#endif

#ifdef OS_USING_SN
extern const int board_no_pin_tab[];
extern const int board_no_pin_tab_size;

extern const int slot_no_pin_tab[];
extern const int slot_no_pin_tab_size;
#endif

#ifdef __cplusplus
}
#endif

#endif

