/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        dev_spi.c
 *
 * @brief       This file implements spi driver for cm32
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include "drv_spi.h"

#ifdef BSP_USING_SPI1
struct cm32_spi_info spi1_info = {
    .spi_base  = SPI1,

    .port_sck  = GPIOA,
    .pin_sck   = GPIO_PIN_5,
    .sck_af    = GPIO_AF0_SPI1,

    .port_miso = GPIOA,
    .pin_miso  = GPIO_PIN_6,
    .miso_af   = GPIO_AF0_SPI1,

    .port_mosi = GPIOA,
    .pin_mosi  = GPIO_PIN_7,
    .mosi_af   = GPIO_AF0_SPI1,
};
OS_HAL_DEVICE_DEFINE("SPI_HandleTypeDef", "spi1", spi1_info);
#endif

#ifdef BSP_USING_SPI2
struct cm32_spi_info spi2_info = {
    .spi_base  = SPI2,

    .port_sck  = GPIOC,
    .pin_sck   = GPIO_PIN_7,
    .sck_af    = GPIO_AF5_SPI2,

    .port_miso = GPIOC,
    .pin_miso  = GPIO_PIN_8,
    .miso_af   = GPIO_AF5_SPI2,

    .port_mosi = GPIOC,
    .pin_mosi  = GPIO_PIN_9,
    .mosi_af   = GPIO_AF5_SPI2,
};
OS_HAL_DEVICE_DEFINE("SPI_HandleTypeDef", "spi2", spi2_info);
#endif
