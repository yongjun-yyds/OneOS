/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        dev_usart.c
 *
 * @brief       This file implements usart driver for cm32
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include "drv_usart.h"

#ifdef BSP_USING_UART1
struct cm32_usart_info uart1_info = {
    .uart_device   = 1,
    .irqn          = USART1_IRQn,
    .idx           = USART1,
    .uart_clk      = RCC_APB2_PERIPH_USART1,
    .uart_gpio_clk = RCC_APB2_PERIPH_GPIOA,
    .tx_port       = GPIOA,
    .tx_pin        = GPIO_PIN_9,
    .tx_af         = GPIO_AF4_USART1,
    .rx_port       = GPIOA,
    .rx_pin        = GPIO_PIN_10,
    .rx_af         = GPIO_AF4_USART1,

#ifdef UART1_USING_DMA
    .dma_support   = 1,
    .dma_channel   = DMA_CH2,
    .periph_addr   = USART1_BASE,
    .dma_irqn      = DMA_Channel2_IRQn,
    .dma_remap     = DMA_REMAP_USART1_RX
#else
    .dma_support   = 0
#endif
};
OS_HAL_DEVICE_DEFINE("Usart_Type", "uart1", uart1_info);
#endif

#ifdef BSP_USING_UART2
struct cm32_usart_info uart2_info = {
    .uart_device   = 2,
    .irqn          = USART2_IRQn,
    .idx           = USART2,
    .uart_clk      = RCC_APB1_PERIPH_USART2,
    .uart_gpio_clk = RCC_APB2_PERIPH_GPIOA,
    .tx_port       = GPIOA,
    .tx_pin        = GPIO_PIN_2,
    .tx_af         = GPIO_AF4_USART2,
    .rx_port       = GPIOA,
    .rx_pin        = GPIO_PIN_3,
    .rx_af         = GPIO_AF4_USART2,

#ifdef UART2_USING_DMA
    .dma_support   = 1,
    .dma_channel   = DMA_CH3,
    .periph_addr   = USART2_BASE,
    .dma_irqn      = DMA_Channel3_IRQn,
    .dma_remap     = DMA_REMAP_USART2_RX
#else
    .dma_support   = 0
#endif
};
OS_HAL_DEVICE_DEFINE("Usart_Type", "uart2", uart2_info);
#endif

#ifdef BSP_USING_UART3
struct cm32_usart_info uart3_info = {
    .uart_device   = 3,
    .irqn          = USART3_IRQn,
    .idx           = USART3,
    .uart_clk      = RCC_APB1_PERIPH_USART3,
    .uart_gpio_clk = RCC_APB2_PERIPH_GPIOC,
    .tx_port       = GPIOC,
    .tx_pin        = GPIO_PIN_10,
    .tx_af         = GPIO_AF5_USART3,
    .rx_port       = GPIOC,
    .rx_pin        = GPIO_PIN_11,
    .rx_af         = GPIO_AF5_USART3,

#ifdef UART3_USING_DMA
    .dma_support   = 1,
    .dma_channel   = DMA_CH4,
    .periph_addr   = USART3_BASE,
    .dma_irqn      = DMA_Channel4_IRQn,
    .dma_remap     = DMA_REMAP_USART3_RX
#else
    .dma_support   = 0
#endif
};
OS_HAL_DEVICE_DEFINE("Usart_Type", "uart3", uart3_info);
#endif

#ifdef BSP_USING_UART4
struct cm32_usart_info uart4_info = {
    .uart_device   = 4,
    .irqn          = UART4_IRQn,
    .idx           = UART4,
    .uart_clk      = RCC_APB2_PERIPH_UART4,
    .uart_gpio_clk = RCC_APB2_PERIPH_GPIOB,
    .tx_port       = GPIOB,
    .tx_pin        = GPIO_PIN_14,
    .tx_af         = GPIO_AF6_UART4,
    .rx_port       = GPIOB,
    .rx_pin        = GPIO_PIN_15,
    .rx_af         = GPIO_AF6_UART4,

#ifdef UART4_USING_DMA
    .dma_support   = 1,
    .dma_channel   = DMA_CH5,
    .periph_addr   = UART4_BASE,
    .dma_irqn      = DMA_Channel5_IRQn,
    .dma_remap     = DMA_REMAP_UART4_RX
#else
    .dma_support   = 0
#endif
};
OS_HAL_DEVICE_DEFINE("Usart_Type", "uart4", uart4_info);
#endif

#ifdef BSP_USING_UART5
struct cm32_usart_info uart5_info = {
    .uart_device   = 5,
    .irqn          = UART5_IRQn,
    .idx           = UART5,
    .uart_clk      = RCC_APB2_PERIPH_UART5,
    .uart_gpio_clk = RCC_APB2_PERIPH_GPIOB,
    .tx_port       = GPIOB,
    .tx_pin        = GPIO_PIN_4,
    .tx_af         = GPIO_AF6_UART5,
    .rx_port       = GPIOB,
    .rx_pin        = GPIO_PIN_5,
    .rx_af         = GPIO_AF7_UART5,

#ifdef UART5_USING_DMA
    .dma_support   = 1,
    .dma_channel   = DMA_CH6,
    .periph_addr   = UART5_BASE,
    .dma_irqn      = DMA_Channel6_IRQn,
    .dma_remap     = DMA_REMAP_UART5_RX
#else
    .dma_support   = 0
#endif
};
OS_HAL_DEVICE_DEFINE("Usart_Type", "uart5", uart5_info);
#endif
