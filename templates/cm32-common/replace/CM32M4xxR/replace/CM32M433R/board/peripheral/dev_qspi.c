/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        dev_qspi.c
 *
 * @brief       This file implements qspi driver for cm32
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include "drv_qspi.h"

#define GPIO_QSPI_NO_RMP 0

#ifdef OS_USING_QSPI
struct cm32_qspi_info qspi_info = {
    .nss_port   = QSPI_NSS_PORTA,
    .gpio_remap = GPIO_QSPI_NO_RMP,
};
OS_HAL_DEVICE_DEFINE("QSPI_HandleTypeDef", "qspi", qspi_info);
#endif
