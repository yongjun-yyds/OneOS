/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_lcd_st7789vw.h
 *
 * @brief       LCD driver header file
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __DRV_LCD_H__
#define __DRV_LCD_H__

#include <os_task.h>
#ifdef PKG_USING_QRCODE
#include <qrcode.h>
#endif

#define LCD_W 240
#define LCD_H 240

/* POINT_COLOR */
#define WHITE   0xFFFF
#define BLACK   0x0000
#define BLUE    0x001F
#define BRED    0XF81F
#define GRED    0XFFE0
#define GBLUE   0X07FF
#define RED     0xF800
#define MAGENTA 0xF81F
#define GREEN   0x07E0
#define CYAN    0x7FFF
#define YELLOW  0xFFE0
#define BROWN   0XBC40
#define BRRED   0XFC07
#define GRAY    0X8430
#define GRAY175 0XAD75
#define GRAY151 0X94B2
#define GRAY187 0XBDD7
#define GRAY240 0XF79E

void lcd_clear(uint16_t color);
void lcd_address_set(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2);
void lcd_set_color(uint16_t back, uint16_t fore);

void lcd_draw_point(uint16_t x, uint16_t y);
void lcd_draw_circle(uint16_t x0, uint16_t y0, uint8_t r);
void lcd_draw_line(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2);
void lcd_draw_rectangle(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2);
void lcd_fill(uint16_t x_start, uint16_t y_start, uint16_t x_end, uint16_t y_end, uint16_t color);

void     lcd_show_num(uint16_t x, uint16_t y, uint32_t num, uint8_t len, uint32_t size);
os_err_t lcd_show_string(uint16_t x, uint16_t y, uint32_t size, const char *fmt, ...);
os_err_t lcd_show_image(uint16_t x, uint16_t y, uint16_t length, uint16_t wide, const uint8_t *p);
#ifdef PKG_USING_QRCODE
os_err_t lcd_show_qrcode(uint16_t x,
                         uint16_t y,
                         uint8_t  version,
                         uint8_t  ecc,
                         const char *data,
                         uint8_t  enlargement);
#endif

void lcd_enter_sleep(void);
void lcd_exit_sleep(void);
void lcd_display_on(void);
void lcd_display_off(void);

#endif
