/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with 
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on 
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the 
 * specific language governing permissions and limitations under the License.
 *
 * @file        board.c
 *
 * @brief       The 7231n board init of OneOS.
 *
 *
 * @revision
 * Date         Author          Notes
 * 2020-12-18   OneOS Team      First version
 ***********************************************************************************************************************
 */

#include "os_task.h"
#include "os_types.h"
#include "os_memory.h"
#include "os_sem.h"
#include "shell.h"

#include "board.h"
#include "interrupt.h"
#include "driver_pub.h"
#include "drv_uart.h"
#include "include.h"
#include "func_pub.h"
#include "gpio_pub.h"

#include "icu.h"
#include "fake_clock_pub.h"
#include "arm_arch.h"
#include "intc.h"
#include "portmacro.h"


enum wdg_status {
    WDG_STATUS_STOP,
    WDG_STATUS_REBOOT,
    WDG_STATUS_WATCH,
    WDG_STATUS_MAX,
};

struct wdg_context {
    enum wdg_status wdg_flag;
    int threshold_in_tick;
    int consumed_in_tick;
    os_tick_t last_fresh_in_tick; /* add time to dealing with power save mode */
};

static struct wdg_context g_wdg_context = { WDG_STATUS_STOP, 0, 0 };

extern UINT32 soc_driver_init(void);
extern void os_clk_init(void);
extern void bk_reboot(void);


os_err_t os_hw_board_init(void)
{ 
    os_irq_enable();   
    
    portENABLE_INTERRUPTS();
    
/* Heap initialization */
#ifdef OS_USING_HEAP
        if ((os_size_t)OS_HW_TCM_END > (os_size_t)OS_HW_TCM_BEGIN)
        {
            os_default_heap_init();
            os_default_heap_add((void *)OS_HW_TCM_BEGIN, (os_size_t)OS_HW_TCM_END - (os_size_t)OS_HW_TCM_BEGIN, OS_MEM_ALG_DEFAULT);
        }
#endif

    /* init hardware */
    soc_driver_init();
    /* init system tick */
    os_clk_init();

    return OS_SUCCESS;
}

OS_INIT_CALL(os_hw_board_init, OS_INIT_LEVEL_PRE_KERNEL_1, OS_INIT_SUBLEVEL_HIGH);

#define WDT_DEV_NAME "wdt"
/**
 * reset cpu by dog's time-out
 */
void beken_hw_cpu_reset(void)
{
    bk_reboot();

    while (1);
}

#ifdef BEKEN_USING_WLAN
static int auto_func_init(void)
{
    func_init_basic();

    func_init_extended();
    
    return 0;
}
OS_INIT_CALL(auto_func_init, OS_INIT_LEVEL_PRE_DEVICE, OS_INIT_SUBLEVEL_LOW);
#endif

extern void cp15_enable_alignfault(void);
static int auto_enable_alignfault(void)
{
    cp15_enable_alignfault();

    return 0;
}
OS_INIT_CALL(auto_enable_alignfault, OS_INIT_LEVEL_POST_KERNEL, OS_INIT_SUBLEVEL_MIDDLE);

void print_exception_addr(unsigned int pc, unsigned int lr, unsigned int sp)
{
    os_kprintf("pc is %x, lr is %x, sp is %x\n", pc, lr, sp);
    while (1);
}

void os_hw_cpu_reset(void)
{
    bk_reboot();

    while (1);
}



