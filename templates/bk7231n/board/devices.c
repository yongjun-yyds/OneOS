#include "intc_pub.h"
#include "drv_uart.h"
#include "uart_pub.h"
#include "bus.h"
#include "drv_wlan.h"

#include "uart.h"
#include "icu_pub.h"

#ifdef BEKEN_USING_UART1
struct beken_uart_info uart1_info = {
    .port             = UART1_PORT,
    .irqno            = IRQ_UART1,
    .inter_reg_addr   = REG_UART1_INTR_ENABLE,
    .fifo_status      = REG_UART1_FIFO_STATUS,
    .irq_uart_bit     = IRQ_UART1_BIT
};

OS_HAL_DEVICE_DEFINE("uart_Type","uart1", uart1_info);
#endif

#ifdef BEKEN_USING_UART2
struct beken_uart_info uart2_info = {
    .port  = UART2_PORT,
    .irqno = IRQ_UART2,
    .inter_reg_addr   = REG_UART2_INTR_ENABLE,
    .fifo_status      = REG_UART2_FIFO_STATUS,
    .irq_uart_bit     = IRQ_UART2_BIT
};

OS_HAL_DEVICE_DEFINE("uart_Type","uart2", uart2_info);
#endif

#ifdef BEKEN_USING_WLAN

#ifdef BSP_USING_BK_STA
struct beken_wifi_info  wlan_sta_info = {  .mac   = {0},
                                           .state = 0,
                                           .mode  = 0,
                                           .work_mode = net_dev_mode_sta
                                     };
OS_HAL_DEVICE_DEFINE("Wlan_Type",OS_WLAN_DEVICE_STA_NAME, wlan_sta_info);
#endif /* BEKEN_USING_WLAN_STA */

#ifdef BSP_USING_BK_AP
struct beken_wifi_info  wlan_ap_info = {   .mac   = {0},
                                           .state = 0,
                                           .mode  = 0,
                                           .work_mode = net_dev_mode_ap
                                       };
OS_HAL_DEVICE_DEFINE("Wlan_Type",OS_WLAN_DEVICE_AP_NAME, wlan_ap_info);
#endif /* #ifdef BEKEN_USING_WLAN_AP */

#endif








