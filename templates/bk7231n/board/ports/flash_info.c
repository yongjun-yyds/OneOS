static struct onchip_flash_info onchip_flash = 
{
    .start_addr = BK7231N_FLASH_START_ADRESS,
    .capacity   = BK7231N_FLASH_SIZE,
    .block_size = BK7231N_FLASH_BLOCK_SIZE,
    .page_size  = BK7231N_FLASH_PAGE_SIZE,
};
OS_HAL_DEVICE_DEFINE("Onchip_Flash_Type", "onchip_flash", onchip_flash);
