/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        wlan_test.c
 *
 * @brief       The test file for wlan.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <drv_cfg.h>
#include <wlan_dev.h>
#include <shell.h>
#include <string.h>
#include <stdlib.h>

#define DRV_EXT_LVL DBG_EXT_INFO
#define DRV_EXT_TAG "wlan_test"
#include <drv_log.h>

#ifdef OS_USING_BK_AP
    #define wlan_name OS_WLAN_DEVICE_AP_NAME
#else
    #define wlan_name OS_WLAN_DEVICE_STA_NAME
#endif


static int wifi_scan(int argc, char **argv)
{
    int                         index;
    char                       *security;
    struct os_wlan_device      *wlan_dev    = OS_NULL;
    struct os_wlan_scan_result *scan_result = OS_NULL;

    wlan_dev = (struct os_wlan_device *)os_device_find(wlan_name);
    if (wlan_dev == OS_NULL)
    {
        LOG_E(DRV_EXT_TAG, "os_device_find failed!");
        return OS_FAILURE;
    }

    scan_result = os_wlan_scan(wlan_dev, 2000, 30);
    if (scan_result == OS_NULL)
    {
        LOG_E(DRV_EXT_TAG, "os_wlan_scan failed!");
        return OS_FAILURE;
    }

    os_kprintf("has find %d wifi sorce\r\n", scan_result->count);
    os_kprintf("             SSID                      MAC            security    rssi chn Mbps\r\n");
    os_kprintf("------------------------------- -----------------  -------------- ---- --- ----\r\n");
    for (index = 0; index < scan_result->count; index++)
    {
        os_kprintf("%-32.32s", &scan_result->scan_info[index].ssid.val[0]);
        os_kprintf("%02x:%02x:%02x:%02x:%02x:%02x  ",
                   scan_result->scan_info[index].bssid[0],
                   scan_result->scan_info[index].bssid[1],
                   scan_result->scan_info[index].bssid[2],
                   scan_result->scan_info[index].bssid[3],
                   scan_result->scan_info[index].bssid[4],
                   scan_result->scan_info[index].bssid[5]);
        switch (scan_result->scan_info[index].security)
        {
        case OS_WLAN_SECURITY_OPEN:
            security = "OPEN";
            break;
        case OS_WLAN_SECURITY_WEP_PSK:
            security = "WEP_PSK";
            break;
        case OS_WLAN_SECURITY_WEP_SHARED:
            security = "WEP_SHARED";
            break;
        case OS_WLAN_SECURITY_WPA_TKIP_PSK:
            security = "WPA_TKIP_PSK";
            break;
        case OS_WLAN_SECURITY_WPA_AES_PSK:
            security = "WPA_AES_PSK";
            break;
        case OS_WLAN_SECURITY_WPA2_AES_PSK:
            security = "WPA2_AES_PSK";
            break;
        case OS_WLAN_SECURITY_WPA2_TKIP_PSK:
            security = "WPA2_TKIP_PSK";
            break;
        case OS_WLAN_SECURITY_WPA2_MIXED_PSK:
            security = "WPA2_MIXED_PSK";
            break;
        case OS_WLAN_SECURITY_WPS_OPEN:
            security = "WPS_OPEN";
            break;
        case OS_WLAN_SECURITY_WPS_SECURE:
            security = "WPS_SECURE";
            break;
        default:
            security = "UNKNOWN";
            break;
        }
        os_kprintf("%-14.14s ", security);
        os_kprintf("%-4d ", scan_result->scan_info[index].signal_strength);
        os_kprintf("%3d ", scan_result->scan_info[index].channel);
        os_kprintf("%4d\r\n", scan_result->scan_info[index].max_data_rate / 1000);
    }

    os_wlan_scan_clean_result(wlan_dev);

    return 0;
}
SH_CMD_EXPORT(wifi_scan, wifi_scan, "wifi_scan");

static os_wlan_security_t security_map_from_str(char *str)
{
    os_wlan_security_t security;

    if (strcmp("OPEN", str) == 0)
        security = OS_WLAN_SECURITY_OPEN;
    else if (strcmp("WPA_AES_PSK", str) == 0)
        security = OS_WLAN_SECURITY_WPA_AES_PSK;
    else if (strcmp("WPA2_AES_PSK", str) == 0)
        security = OS_WLAN_SECURITY_WPA2_AES_PSK;
    else if (strcmp("WPA2_MIXED_PSK", str) == 0)
        security = OS_WLAN_SECURITY_WPA2_MIXED_PSK;
    else
        security = OS_WLAN_SECURITY_WPA2_AES_PSK;

    return security;
}

static int wifi_join(int argc, char **argv)
{
    os_wlan_security_t     security;
    const char            *ssid     = OS_NULL;
    const char            *key      = OS_NULL;
    struct os_wlan_device *wlan_dev = OS_NULL;

    if (argc != 4)
    {
        os_kprintf("usage: wifi_join ssid key security\r\n");
        os_kprintf("       wifi_join mywifi 12345678 WPA2_AES_PSK\r\n");
        os_kprintf("       wifi_join mywifi null OPEN\r\n");
        return -1;
    }

    /* ssid */
    ssid = argv[1];

    /* Password */
    key = argv[2];

    /* security */
    security = security_map_from_str(argv[3]);

    wlan_dev = (struct os_wlan_device *)os_device_find(wlan_name);
    if (wlan_dev == OS_NULL)
    {
        LOG_E(DRV_EXT_TAG, "os_device_find failed!");
    }

    if (os_wlan_join(wlan_dev, ssid, key, security) != OS_SUCCESS)
        LOG_E(DRV_EXT_TAG, "os_wlan_join failed! %s %s", ssid, key);
    else
        LOG_I(DRV_EXT_TAG, "os_wlan_join success! %s %s", ssid, key);

    return 0;
}
SH_CMD_EXPORT(wifi_join, wifi_join, "wifi_join");

static int wifi_leave(int argc, char *argv[])
{
    struct os_wlan_device *wlan_dev = OS_NULL;

    wlan_dev = (struct os_wlan_device *)os_device_find(wlan_name);
    if (wlan_dev == OS_NULL)
    {
        LOG_E(DRV_EXT_TAG, "os_device_find failed!");
    }

    if (os_wlan_leave(wlan_dev) != OS_SUCCESS)
        LOG_E(DRV_EXT_TAG, "os_wlan_leave failed!");
    else
        LOG_I(DRV_EXT_TAG, "os_wlan_leave success!");
    return 0;
}
SH_CMD_EXPORT(wifi_leave, wifi_leave, "wifi_leave");
