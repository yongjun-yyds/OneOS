/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * @file        application_demo.c
 *
 * @brief       This file provides a app demo 
 *
 * @revision
 * Date         Author          Notes
 * 2021-01-5   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <stdio.h>
#include "shell.h"
#include "rtos_pub.h"
#include "drv_flash.h"
#include "wlan_dev.h"
#include "error.h"
#include <sys/socket.h>
#include "os_mq.h"
#include "os_types.h"
#include "mem_pub.h"
#include "str_pub.h"
#include "shell.h"
#include "pin.h"

#include <string.h>
#include <shell.h>
#include <sys/time.h>
#include <lwip/sockets.h>
#include <lwip/netdb.h>

#include <os_task.h>
#include <os_assert.h>
#include <dlog.h>

#define DRV_EXT_LVL DBG_EXT_INFO
#define DRV_EXT_TAG "beken_wifi"
#include <drv_log.h>

#define CONFIG_INFO_ADDR      (0x1FE000)
#define LISTEN_SERVER_PORT    (28000)
#define MSG_SIZE        24

struct config_info_manager {
	os_net_mode_t mode;
	char ssid[32];
	char password[32];
};


static struct config_info_manager g_cfg_info;
static beken_thread_t thread_recv_deal;
static beken_thread_t thread_server;
static int            g_msg[32];
static char           result[64];


#define wlan_name "wlan0"

#ifdef BSP_USING_BK_AP
void demo_ap(void)
{
	struct os_wlan_device *wlan_dev = OS_NULL;    

    wlan_dev = (struct os_wlan_device *)os_device_find(OS_WLAN_DEVICE_AP_NAME);
    if (wlan_dev == OS_NULL)
    {
        LOG_E(DRV_EXT_TAG, "wifi_dev cannot find!");
        return OS_SUCCESS;
    }    

	wlan_dev->info.ssid                  = "bsp-bk";
    wlan_dev->info.password              = "12345678";
	wlan_dev->info.security              = OS_WLAN_SECURITY_WPA2_AES_PSK;
	wlan_dev->info.country               = OS_WLAN_COUNTRY_CHINA;
	wlan_dev->info.channel               = 1;

    os_kprintf("demo_ap_start_test\r\n");
    os_wlan_start(wlan_dev);
}
SH_CMD_EXPORT(demo_ap,demo_ap,"ap startup of ssid:beken pass:12345678 test.");
#endif

