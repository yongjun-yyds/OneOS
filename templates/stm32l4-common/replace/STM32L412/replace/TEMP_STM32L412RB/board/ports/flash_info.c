static struct onchip_flash_info onchip_flash = {
    .start_addr = 0x08005000,
    .capacity   = 2 * 1024,
    .block_size = 2 * 1024,
    .page_size  = 4,
};
OS_HAL_DEVICE_DEFINE("Onchip_Flash_Type", "onchip_flash", onchip_flash);
