import os
import sys
import shutil
import flash_size_table
import sram_size_table
import sector_size_table


def modify(series,model,soc):
    old_path = os.getcwd()
    source_path = os.path.dirname(os.path.realpath(__file__))
    os.chdir(source_path+'/../target')
    flash_size = flash_size_table.get_size_kb(soc)
    
    if flash_size_table.get_size_kb(soc) <= 64 and series != "GD32F4xx":
        bootloader_size = 16
    else:
        bootloader_size = sector_size_table.get_fal_part_info_size(series, 'bootloader')
        
    cfg_size = sector_size_table.get_fal_part_info_size(series, 'cfg')
    download_size = sector_size_table.get_fal_part_info_size(series, 'download')
    app_size = flash_size - bootloader_size - cfg_size - download_size
    app_size = app_size * 1024
    app_addr = 0x08000000 + bootloader_size * 1024 + cfg_size * 1024
    
    addr_idx = ''
    with open('.config', 'r') as f:
        with open('.config.new', 'w') as g:
            for line in f.readlines():
                if 'CONFIG_BSP_BOOT_OPTION=y' in line:
                    g.write(line)
                elif 'CONFIG_BSP_TEXT_SECTION_ADDR' in line:
                    g.write("CONFIG_BSP_TEXT_SECTION_ADDR=0x%08x\n" % app_addr)
                elif 'CONFIG_BSP_TEXT_SECTION_SIZE' in line:
                    g.write("CONFIG_BSP_TEXT_SECTION_SIZE=0x%08x\n" % app_size)
                elif 'CONFIG_BSP_DATA_SECTION_ADDR' in line:
                    g.write('CONFIG_BSP_DATA_SECTION_ADDR=0x%s\n' % sram_size_table.get_startaddr(soc, 'sram_1'))
                elif 'CONFIG_BSP_DATA_SECTION_SIZE' in line:
                    sram1_size = int(sram_size_table.get_size_kb(soc, 'sram_1'))
                    sram1_size = sram1_size * 1024
                    g.write('CONFIG_BSP_DATA_SECTION_SIZE=0x%08x\n' % sram1_size)
                else:
                    g.write(line)
                    
    shutil.move('.config.new', '.config')
    os.chdir(old_path)