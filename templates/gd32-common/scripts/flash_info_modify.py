import os
import sys
import shutil
import flash_size_table
import sram_size_table
import sector_size_table


def get_series_for_page_size(series, model, soc):
    name = None
    if (series == "GD32E10x") or (series == "GD32E50x") or (series == "GD32F3x0") or (series == "GD32F4xx") or \
       (series == "GD32C10x") or (series == "GD32F403") or (series == "GD32F20x"):
        name = series

    elif series == "GD32F30x":
        if (model == "GD32F305") or (model == "GD32F307"):
            name = series + "_CL"
        elif model == "GD32F303":
            flash_size = flash_size_table.get_size_kb(soc)
            if flash_size <= 512:
                name = series + "_HD"
            else:
                name = series + "_XD"

    elif series == "GD32F10x":
        if (model == "GD32F105") or (model == "GD32F107"):
            name = series + "_CL"
        elif (model == "GD32F101") or (model == "GD32F103"):
            flash_size = flash_size_table.get_size_kb(soc)
            if flash_size <= 128:
                name = series + "_MD"
            elif flash_size <= 512:
                name = series + "_HD"
            else:
                name = series + "_XD"

    if name is None:
        print("series is error!")

    return name


def modify(series, model, soc):
    old_path = os.getcwd()
    source_path = os.path.dirname(os.path.realpath(__file__))
    os.chdir(source_path+'/../target/board/ports/')

    flash_size = flash_size_table.get_size_kb(soc)
    page_size = flash_size_table.get_bank0_page_size_kb(get_series_for_page_size(series, model, soc))

    with open('flash_info.c', 'r') as f:
        with open('flash_info.new', 'w') as g:
            for line in f.readlines():
                if '.capacity' in line:
                    g.write('    .capacity   = (%d * 1024),\n' % flash_size)
                elif '.start_addr' in line:
                    g.write('    .start_addr = 0x%08x,\n' % 0x08000000)
                elif '.block_size' in line:
                    g.write('    .block_size = (%d * 1024),\n' % page_size)
                elif '.page_size' in line:
                    g.write('    .page_size  = (%d * 1024),\n' % page_size)
                else:
                    g.write(line)
                    
    shutil.move('flash_info.new', 'flash_info.c')
    os.chdir(old_path)

