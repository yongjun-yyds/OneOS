flash_size_table = {}
flash_size_table['4'] = 16
flash_size_table['6'] = 32
flash_size_table['8'] = 64
flash_size_table['B'] = 128
flash_size_table['C'] = 256
flash_size_table['D'] = 384
flash_size_table['E'] = 512
flash_size_table['F'] = 768
flash_size_table['G'] = 1024
flash_size_table['I'] = 2048
flash_size_table['K'] = 3072

page_size_table = {} # KB

# GD32C10x
page_size_table['GD32C10x'] = {'bank0': 1, 'bank1': None}

# GD32F10x
page_size_table['GD32F10x_MD'] = {'bank0': 1, 'bank1': None}
page_size_table['GD32F10x_CL'] = {'bank0': 2, 'bank1': 4}
page_size_table['GD32F10x_HD'] = {'bank0': 2, 'bank1': None}
page_size_table['GD32F10x_XD'] = {'bank0': 2, 'bank1': 4}

# GD32F30x
page_size_table['GD32F30x_CL'] = {'bank0': 2, 'bank1': 4}
page_size_table['GD32F30x_HD'] = {'bank0': 2, 'bank1': None}
page_size_table['GD32F30x_XD'] = {'bank0': 2, 'bank1': 4}

# GD32F4xx
page_size_table['GD32F4xx'] = {'bank0': 4, 'bank1': 4}

# GD32E10x
page_size_table['GD32E10x'] = {'bank0': 1, 'bank1': None}

# GD32E50x
page_size_table['GD32E50x'] = {'bank0': 8, 'bank1': None}

# GD32F3x0
page_size_table['GD32F3x0'] = {'bank0': 1, 'bank1': None}

# GD32F20x
page_size_table['GD32F20x'] = {'bank0': 2, 'bank1': 4}

# GD32F403
page_size_table['GD32F403'] = {'bank0': 2, 'bank1': 4}


def get_bank0_page_size_kb(series):
    if series in page_size_table:
        return page_size_table[series]['bank0']
    else:
        print("Error in obtaining page size!")
        return 0

def get_size_kb(soc):
    flash_size =  flash_size_table[soc[-1]]
    if flash_size != 0:
        return flash_size
    else:
        return 0

def get_size_byte(soc):
    flash_size =  flash_size_table[soc[-1]]
    if flash_size != 0:
        return flash_size * 1024
    else:
        return 0

def get_startaddr(soc):
    return '08000000'

