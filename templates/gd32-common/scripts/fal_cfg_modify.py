import os
import sys
import shutil
import flash_size_table
import sram_size_table
import sector_size_table


def modify(series,model,soc):
    old_path = os.getcwd()
    source_path = os.path.dirname(os.path.realpath(__file__))
    os.chdir(source_path+'/../target/board/ports/')
    
    flash_size = flash_size_table.get_size_kb(soc)
    
    if flash_size <= 64 and series != "GD32F4xx":
        bootloader_size = 16
    else:
        bootloader_size = sector_size_table.get_fal_part_info_size(series, 'bootloader')
    
    cfg_size = sector_size_table.get_fal_part_info_size(series, 'cfg')

    with open('fal_cfg.c', 'r') as f:
        with open('fal_cfg.new', 'w') as g:
            for line in f.readlines():
                if '{ "bootloader", "flash_0"' in line:
                    g.write('    { "bootloader", "flash_0", 0x00000000, 0x%08x,   FAL_PART_INFO_FLAGS_LOCKED},\n' % (bootloader_size * 1024))
                    continue
                if '{ "cfg",        "flash_0"' in line:
                    g.write('    { "cfg",        "flash_0", 0x%08x, 0x%08x, FAL_PART_INFO_FLAGS_UNLOCKED},\n' % (bootloader_size * 1024, cfg_size * 1024))
                    continue
                else:
                    g.write(line)

    shutil.move('fal_cfg.new', 'fal_cfg.c')
    os.chdir(old_path)
