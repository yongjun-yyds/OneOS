import os
import sys
import shutil
import flash_size_table
import sram_size_table


def modify(series, model, soc):
    old_path = os.getcwd()
    source_path = os.path.dirname(os.path.realpath(__file__))
    os.chdir(source_path+'/../target')
    with open('Kconfig', 'r') as f:
        with open('Kconfig.new', 'w') as g:
            for line in f.readlines():
                if 'config BOARD_' in line:
                    g.write('config ' + soc + '\n')
                    continue
                if 'select SOC_' in line: 
                    g.write('    select SOC_' + model.upper() + "XX" + '\n')
                    continue
                else:
                    g.write(line)
    shutil.move('Kconfig.new', 'Kconfig')
    os.chdir(old_path)


