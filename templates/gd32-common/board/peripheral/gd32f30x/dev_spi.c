/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_usart.c
 *
 * @brief       This file implements usart driver for stm32
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#ifdef BSP_USING_SPI
#include <board.h>
#include <drv_spi.h>

#ifdef BSP_USING_SPI0
struct gd_spi_info spi0_info = {
    .instance = SPI0,

    .rcu_gpio = RCU_GPIOA,
    .rcu_spi  = RCU_SPI0,

    .port = GPIOA,
    .miso = GPIO_PIN_6,
    .mosi = GPIO_PIN_7,
    .sck  = GPIO_PIN_5,
};
OS_HAL_DEVICE_DEFINE("SPI_HandleTypeDef", "spi0", spi0_info);
#endif

#ifdef BSP_USING_SPI1
struct gd_spi_info spi1_info = {
    .instance = SPI1,

    .rcu_gpio = RCU_GPIOB,
    .rcu_spi  = RCU_SPI1,

    .port = GPIOB,
    .miso = GPIO_PIN_14,
    .mosi = GPIO_PIN_15,
    .sck  = GPIO_PIN_13,
};
OS_HAL_DEVICE_DEFINE("SPI_HandleTypeDef", "spi1", spi1_info);
#endif

#ifdef BSP_USING_SPI2
struct gd_spi_info spi2_info = {
    .instance = SPI2,

    .rcu_gpio = RCU_GPIOB,
    .rcu_spi  = RCU_SPI2,

    .port = GPIOB,
    .miso = GPIO_PIN_4,
    .mosi = GPIO_PIN_5,
    .sck  = GPIO_PIN_3,
};
OS_HAL_DEVICE_DEFINE("SPI_HandleTypeDef", "spi2", spi2_info);
#endif

#endif
