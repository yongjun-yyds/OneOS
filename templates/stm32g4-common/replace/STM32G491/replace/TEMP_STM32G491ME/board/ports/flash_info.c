static struct onchip_flash_info flash_0_info = {
    .start_addr = STM32_FLASH_START_ADRESS,
    .capacity   = STM32_FLASH_SIZE,
    .block_size = 2048,
    .page_size  = 4,
};
OS_HAL_DEVICE_DEFINE("Onchip_Flash_Type", "flash0", flash_0_info);
