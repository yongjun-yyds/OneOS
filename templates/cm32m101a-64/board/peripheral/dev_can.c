#include <oneos_config.h>
#include <bus/bus.h>
#ifdef BSP_USING_CAN
#include <drv_can.h>

#ifdef BSP_USING_CAN
static struct cm32_can_info can_info = {
    .can_instance           = CAN,

    .gpio                   = GPIOA,
    .gpio_rcc               = RCC_APB2_PERIPH_AFIO | RCC_APB2_PERIPH_GPIOA,
    .gpio_init_structure_rx =
        {
            .Pin            = GPIO_PIN_11,
            .GPIO_Current   = GPIO_DC_2mA,
            .GPIO_Slew_Rate = GPIO_Slew_Rate_High,
            .GPIO_Pull      = GPIO_Pull_Up,
            .GPIO_Mode      = GPIO_Mode_Input,
            .GPIO_Alternate = GPIO_AF1_CAN,
        },
    .gpio_init_structure_tx =
        {
            .Pin            = GPIO_PIN_12,
            .GPIO_Current   = GPIO_DC_2mA,
            .GPIO_Slew_Rate = GPIO_Slew_Rate_High,
            .GPIO_Pull      = GPIO_Pull_Up,
            .GPIO_Mode      = GPIO_Mode_AF_PP,
            .GPIO_Alternate = GPIO_AF1_CAN,
        },
};

OS_HAL_DEVICE_DEFINE("CAN_HandleTypeDef", "can", can_info);
#endif

#endif
