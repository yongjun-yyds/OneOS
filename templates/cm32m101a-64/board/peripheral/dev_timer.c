/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        dev_timer.c
 *
 * @brief       This file implements hwtimer driver for cm32
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <drv_hwtimer.h>

#ifdef BSP_USING_TIM2
static const struct cm32_timer_info tim2_info = {
    .mode       = TIMER_MODE_TIM,
    .htim       = TIM2,
    .periph_clk = RCC_APB1_PERIPH_TIM2,
    .irqn       = TIM2_IRQn,
};
OS_HAL_DEVICE_DEFINE("TIMER_Type", "tim2", tim2_info);
#endif

#ifdef BSP_USING_TIM3
static const struct cm32_timer_info tim3_info = {
    .mode       = TIMER_MODE_TIM,
    .htim       = TIM3,
    .periph_clk = RCC_APB1_PERIPH_TIM3,
    .irqn       = TIM3_IRQn,
};
OS_HAL_DEVICE_DEFINE("TIMER_Type", "tim3", tim3_info);
#endif

#ifdef BSP_USING_TIM4
static const struct cm32_timer_info tim4_info = {
    .mode       = TIMER_MODE_TIM,
    .htim       = TIM4,
    .periph_clk = RCC_APB1_PERIPH_TIM4,
    .irqn       = TIM4_IRQn,
};
OS_HAL_DEVICE_DEFINE("TIMER_Type", "tim4", tim4_info);
#endif

#ifdef BSP_USING_TIM5
static const struct cm32_timer_info tim5_info = {
    .mode       = TIMER_MODE_TIM,
    .htim       = TIM5,
    .periph_clk = RCC_APB1_PERIPH_TIM5,
    .irqn       = TIM5_IRQn,
};
OS_HAL_DEVICE_DEFINE("TIMER_Type", "tim5", tim5_info);
#endif
