static const struct nxp_gpt_info gpt1_info = {GPT1_PERIPHERAL, &GPT1_config};
OS_HAL_DEVICE_DEFINE("GPT_Type", "gpt1", gpt1_info);

static const struct nxp_gpt_info gpt2_info = {GPT2_PERIPHERAL, &GPT2_config};
OS_HAL_DEVICE_DEFINE("GPT_Type", "gpt2", gpt2_info);

static const struct nxp_lcdif_info lcdif_info = {LCDIF_PERIPHERAL, &LCDIF_config};
OS_HAL_DEVICE_DEFINE("LCDIF_Type", "lcdif", lcdif_info);

static const struct nxp_lpuart_info lpuart1_info = {LPUART1_PERIPHERAL, &LPUART1_config};
OS_HAL_DEVICE_DEFINE("LPUART_Type", "lpuart1", lpuart1_info);

