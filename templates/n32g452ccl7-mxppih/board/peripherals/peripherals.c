#include "oneos_config.h"
#include <driver.h>
#include <bus/bus.h>

#include "drv_common.h"

#ifdef BSP_USING_USART
#include "drv_usart.h"
const struct n32_usart_info usart1_info = {
	.huart = USART1,
	.rcc_type = 2,
	.rcc = RCC_APB2_PERIPH_USART1,
	.irq = USART1_IRQn,
	
	.dma_channel = NULL, //DMA1_CH5, 	// 用作Console, 不能启用DMA
	.dma_rcc = 1,
	.dma_irq = DMA1_Channel5_IRQn,
	
    .tx_port = GPIOA,
	.tx_pin  = GPIO_PIN_9,
    .tx_rcc  = RCC_APB2_PERIPH_GPIOA,
	
	.rx_port = GPIOA,
	.rx_pin  = GPIO_PIN_10,
	.rx_rcc  = RCC_APB2_PERIPH_GPIOA
};

OS_HAL_DEVICE_DEFINE("USART_Module", "usart1", usart1_info);

const struct n32_usart_info usart3_info = {
	.huart = USART3,
	.rcc_type = 1,
	.rcc = RCC_APB1_PERIPH_USART3,
	.irq = USART3_IRQn,
	
	.dma_channel = DMA1_CH3,
	.dma_rcc = 1,
	.dma_irq = DMA1_Channel3_IRQn,
	
    .tx_port = GPIOB,
	.tx_pin  = GPIO_PIN_10,
    .tx_rcc  = RCC_APB2_PERIPH_GPIOB,
	
	.rx_port = GPIOB,
	.rx_pin  = GPIO_PIN_11,
	.rx_rcc  = RCC_APB2_PERIPH_GPIOB
};

OS_HAL_DEVICE_DEFINE("USART_Module", "usart3", usart3_info);
#endif
