/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_usart.c
 *
 * @brief       This file implements usart driver for stm32
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#ifdef BSP_USING_QSPI
#include <board.h>
#include <drv_qspi.h>

#ifdef BSP_USING_QSPI
static os_err_t qspi_hw_init(spi_parameter_struct *spi_init_struct)
{
    rcu_periph_clock_enable(RCU_GPIOG);
    rcu_periph_clock_enable(RCU_GPIOI);
    rcu_periph_clock_enable(RCU_SPI5);

    /* clang-format off */
    /* SPI5_CLK(PG13), SPI5_MISO(PG12), SPI5_MOSI(PG14),SPI5_IO2(PG10) and SPI5_IO3(PG11) GPIO pin configuration */
    gpio_af_set(GPIOG, GPIO_AF_5, GPIO_PIN_10|GPIO_PIN_11| GPIO_PIN_12|GPIO_PIN_13| GPIO_PIN_14);
    gpio_mode_set(GPIOG, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO_PIN_10|GPIO_PIN_11| GPIO_PIN_12|GPIO_PIN_13| GPIO_PIN_14);
    gpio_output_options_set(GPIOG, GPIO_OTYPE_PP, GPIO_OSPEED_25MHZ, GPIO_PIN_10|GPIO_PIN_11| GPIO_PIN_12|GPIO_PIN_13| GPIO_PIN_14);
    /* clang-format on */

    /* SPI5_CS(PI8) GPIO pin configuration */
    gpio_mode_set(GPIOI, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO_PIN_8);
    gpio_output_options_set(GPIOI, GPIO_OTYPE_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_8);
    gpio_bit_set(GPIOI, GPIO_PIN_8);

    spi_init(SPI5, spi_init_struct);

    /* quad wire SPI_IO2 and SPI_IO3 pin output enable */
    qspi_io23_output_enable(SPI5);

    /* enable SPI5 */
    spi_enable(SPI5);

    return OS_SUCCESS;
}

/* clang-format off */
struct gd32_qspi_info qspi_info = {
    .hqspi         = SPI5,
    .rcu_qspi_base = RCU_SPI5,
    .hw_init       = qspi_hw_init,
    /* SPI5 parameter config */
    .qspi_init_struct = {
        .trans_mode           = SPI_TRANSMODE_FULLDUPLEX,
        .device_mode          = SPI_MASTER,
        .frame_size           = SPI_FRAMESIZE_8BIT,
        .clock_polarity_phase = SPI_CK_PL_LOW_PH_1EDGE,
        .nss                  = SPI_NSS_SOFT,
        .prescale             = SPI_PSC_32,
        .endian               = SPI_ENDIAN_MSB,
    }
};
/* clang-format on */
OS_HAL_DEVICE_DEFINE("QSPI_HandleTypeDef", "qspi", qspi_info);
#endif

#endif
