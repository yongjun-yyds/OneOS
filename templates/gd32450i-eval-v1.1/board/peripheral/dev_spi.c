/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_spi.c
 *
 * @brief       This file implements usart driver for stm32
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#ifdef BSP_USING_SPI
#include <board.h>
#include <drv_spi.h>

#ifdef BSP_USING_SPI0
static os_err_t spi0_hw_init(spi_parameter_struct *spi_init_struct)
{
    /* configure SPI0 GPIO */
    gpio_af_set(GPIOA, GPIO_AF_5, GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7);
    gpio_mode_set(GPIOA, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7);
    gpio_output_options_set(GPIOA, GPIO_OTYPE_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7);

    /* set SPI0_NSS as GPIO*/
    gpio_mode_set(GPIOA, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO_PIN_4);
    gpio_output_options_set(GPIOA, GPIO_OTYPE_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_4);
    gpio_bit_set(GPIOA, GPIO_PIN_4);

    return OS_SUCCESS;
}

/* clang-format off */
struct gd32_spi_info spi0_info = {
    .hspi         = SPI0,
    .rcu_spi_base = RCU_SPI0,
    .hw_init      = spi0_hw_init,
    /* SPI0 parameter config */
    .spi_init_struct = 
    {
        .trans_mode           = SPI_TRANSMODE_FULLDUPLEX,
        .device_mode          = SPI_MASTER,
        .frame_size           = SPI_FRAMESIZE_8BIT,
        .clock_polarity_phase = SPI_CK_PL_LOW_PH_2EDGE,
        .nss                  = SPI_NSS_SOFT,
        .prescale             = SPI_PSC_32,
        .endian               = SPI_ENDIAN_MSB,
    }
};
/* clang-format on */
OS_HAL_DEVICE_DEFINE("SPI_HandleTypeDef", "spi0", spi0_info);
#endif

#ifdef BSP_USING_SPI1
static os_err_t spi1_hw_init(spi_parameter_struct *spi_init_struct)
{
    rcu_periph_clock_enable(RCU_GPIOI);
    rcu_periph_clock_enable(RCU_SPI1);
    /* configure SPI1 GPIO */
    gpio_af_set(GPIOI, GPIO_AF_5, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3);
    gpio_mode_set(GPIOI, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3);
    gpio_output_options_set(GPIOI, GPIO_OTYPE_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3);

    /* set SPI1_NSS as GPIO*/
    gpio_mode_set(GPIOI, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO_PIN_0);
    gpio_output_options_set(GPIOI, GPIO_OTYPE_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_0);
    gpio_bit_set(GPIOI, GPIO_PIN_0);

    spi_init(SPI1, spi_init_struct);

    spi_crc_off(SPI1);

    /* enable SPI1 */
    spi_enable(SPI1);
    return OS_SUCCESS;
}

/* clang-format off */
struct gd32_spi_info spi1_info = {
    .hspi         = SPI1,
    .rcu_spi_base = RCU_SPI1,
    .hw_init      = spi1_hw_init,
    /* SPI1 parameter config */
    .spi_init_struct = {
        .trans_mode           = SPI_TRANSMODE_FULLDUPLEX,
        .device_mode          = SPI_MASTER,
        .frame_size           = SPI_FRAMESIZE_8BIT,
        .clock_polarity_phase = SPI_CK_PL_LOW_PH_1EDGE,
        .nss                  = SPI_NSS_SOFT,
        .prescale             = SPI_PSC_64,
        .endian               = SPI_ENDIAN_MSB,
    }
};
/* clang-format on */
OS_HAL_DEVICE_DEFINE("SPI_HandleTypeDef", "spi1", spi1_info);
#endif

#ifdef BSP_USING_SPI2
static os_err_t spi2_hw_init(spi_parameter_struct *spi_init_struct)
{
    /* configure SPI2 GPIO */
    gpio_af_set(GPIOC, GPIO_AF_5, GPIO_PIN_10 | GPIO_PIN_11 | GPIO_PIN_12);
    gpio_mode_set(GPIOC, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO_PIN_10 | GPIO_PIN_11 | GPIO_PIN_12);
    gpio_output_options_set(GPIOC, GPIO_OTYPE_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_10 | GPIO_PIN_11 | GPIO_PIN_12);

    /* set SPI2_NSS as GPIO*/
    gpio_mode_set(GPIOA, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO_PIN_15);
    gpio_output_options_set(GPIOA, GPIO_OTYPE_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_15);
    gpio_bit_set(GPIOA, GPIO_PIN_15);

    return OS_SUCCESS;
}

/* clang-format off */
struct gd32_spi_info spi2_info = {
    .hspi         = SPI2,
    .rcu_spi_base = RCU_SPI2,
    .hw_init      = spi2_hw_init,
    /* SPI2 parameter config */
    .spi_init_struct = {
        .trans_mode           = SPI_TRANSMODE_FULLDUPLEX,
        .device_mode          = SPI_MASTER,
        .frame_size           = SPI_FRAMESIZE_8BIT,
        .clock_polarity_phase = SPI_CK_PL_LOW_PH_2EDGE,
        .nss                  = SPI_NSS_SOFT,
        .prescale             = SPI_PSC_32,
        .endian               = SPI_ENDIAN_MSB,
    }
};
/* clang-format on */
OS_HAL_DEVICE_DEFINE("SPI_HandleTypeDef", "spi2", spi2_info);
#endif

#ifdef BSP_USING_SPI3
static os_err_t spi3_hw_init(spi_parameter_struct *spi_init_struct)
{
    /* configure SPI3 GPIO */
    gpio_af_set(GPIOE, GPIO_AF_5, GPIO_PIN_12 | GPIO_PIN_13 | GPIO_PIN_14);
    gpio_mode_set(GPIOE, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO_PIN_12 | GPIO_PIN_13 | GPIO_PIN_14);
    gpio_output_options_set(GPIOE, GPIO_OTYPE_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_12 | GPIO_PIN_13 | GPIO_PIN_14);

    /* set SPI3_NSS as GPIO*/
    gpio_mode_set(GPIOE, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO_PIN_11);
    gpio_output_options_set(GPIOE, GPIO_OTYPE_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_11);
    gpio_bit_set(GPIOE, GPIO_PIN_11);

    return OS_SUCCESS;
}

/* clang-format off */
struct gd32_spi_info spi3_info = {
    .hspi         = SPI3,
    .rcu_spi_base = RCU_SPI3,
    .hw_init      = spi1_hw_init,
    /* SPI3 parameter config */
    .spi_init_struct = {
        .trans_mode           = SPI_TRANSMODE_FULLDUPLEX,
        .device_mode          = SPI_MASTER,
        .frame_size           = SPI_FRAMESIZE_8BIT,
        .clock_polarity_phase = SPI_CK_PL_LOW_PH_2EDGE,
        .nss                  = SPI_NSS_SOFT,
        .prescale             = SPI_PSC_32,
        .endian               = SPI_ENDIAN_MSB,
    }
};
/* clang-format on */
OS_HAL_DEVICE_DEFINE("SPI_HandleTypeDef", "spi3", spi3_info);
#endif

#ifdef BSP_USING_SPI4
static os_err_t spi4_hw_init(spi_parameter_struct *spi_init_struct)
{
    /* configure SPI4 GPIO */
    gpio_af_set(GPIOF, GPIO_AF_5, GPIO_PIN_7 | GPIO_PIN_8 | GPIO_PIN_9);
    gpio_mode_set(GPIOF, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO_PIN_7 | GPIO_PIN_8 | GPIO_PIN_9);
    gpio_output_options_set(GPIOF, GPIO_OTYPE_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_7 | GPIO_PIN_8 | GPIO_PIN_9);

    /* set SPI4_NSS as GPIO*/
    gpio_mode_set(GPIOF, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO_PIN_6);
    gpio_output_options_set(GPIOF, GPIO_OTYPE_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_6);
    gpio_bit_set(GPIOF, GPIO_PIN_6);

    return OS_SUCCESS;
}

/* clang-format off */
struct gd32_spi_info spi4_info = {
    .hspi         = SPI4,
    .rcu_spi_base = RCU_SPI4,
    .hw_init      = spi4_hw_init,
    /* SPI4 parameter config */
    .spi_init_struct = {
        .trans_mode           = SPI_TRANSMODE_FULLDUPLEX,
        .device_mode          = SPI_MASTER,
        .frame_size           = SPI_FRAMESIZE_8BIT,
        .clock_polarity_phase = SPI_CK_PL_LOW_PH_2EDGE,
        .nss                  = SPI_NSS_SOFT,
        .prescale             = SPI_PSC_32,
        .endian               = SPI_ENDIAN_MSB,
    }
};
/* clang-format on */
OS_HAL_DEVICE_DEFINE("SPI_HandleTypeDef", "spi4", spi4_info);
#endif

#ifdef BSP_USING_SPI5
#define USE_SPI_FLASH
static os_err_t spi5_hw_init(spi_parameter_struct *spi_init_struct)
{
    rcu_periph_clock_enable(RCU_GPIOG);
    rcu_periph_clock_enable(RCU_GPIOI);
    rcu_periph_clock_enable(RCU_SPI5);

    /* SPI5_CLK(PG13), SPI5_MISO(PG12), SPI5_MOSI(PG14) GPIO pin configuration */
    gpio_af_set(GPIOG, GPIO_AF_5, GPIO_PIN_12 | GPIO_PIN_13 | GPIO_PIN_14);
    gpio_mode_set(GPIOG, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO_PIN_12 | GPIO_PIN_13 | GPIO_PIN_14);
    gpio_output_options_set(GPIOG, GPIO_OTYPE_PP, GPIO_OSPEED_25MHZ, GPIO_PIN_12 | GPIO_PIN_13 | GPIO_PIN_14);

    /* SPI5_CS(PI8) GPIO pin configuration */
    gpio_mode_set(GPIOI, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO_PIN_8);
    gpio_output_options_set(GPIOI, GPIO_OTYPE_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_8);
    gpio_bit_set(GPIOI, GPIO_PIN_8);

#ifdef USE_SPI_FLASH
    gpio_mode_set(GPIOG, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO_PIN_10 | GPIO_PIN_11);
    gpio_output_options_set(GPIOG, GPIO_OTYPE_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_10 | GPIO_PIN_11);
    gpio_bit_set(GPIOG, GPIO_PIN_10 | GPIO_PIN_11);
#endif

    spi_init(SPI5, spi_init_struct);

    /* enable SPI5 */
    spi_enable(SPI5);

    return OS_SUCCESS;
}

/* clang-format off */
struct gd32_spi_info spi5_info = {
    .hspi         = SPI5,
    .rcu_spi_base = RCU_SPI5,
    .hw_init      = spi5_hw_init,
    /* SPI5 parameter config */
    .spi_init_struct = {
        .trans_mode           = SPI_TRANSMODE_FULLDUPLEX,
        .device_mode          = SPI_MASTER,
        .frame_size           = SPI_FRAMESIZE_8BIT,
        .clock_polarity_phase = SPI_CK_PL_LOW_PH_1EDGE,
        .nss                  = SPI_NSS_SOFT,
        .prescale             = SPI_PSC_128,
        .endian               = SPI_ENDIAN_MSB,
    }
};
/* clang-format on */
OS_HAL_DEVICE_DEFINE("SPI_HandleTypeDef", "spi5", spi5_info);
#endif

#endif
