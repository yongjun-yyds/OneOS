/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_usart.c
 *
 * @brief       This file implements usart driver for stm32
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#ifdef BSP_USING_TIMER

static struct gd32_timer_info timer4_info = {
    .timer_periph = TIMER4,
    .periph       = RCU_TIMER4,
    .nvic_irq     = TIMER4_IRQn,

    .timer_initpara =
        {
            .prescaler         = 119,
            .alignedmode       = TIMER_COUNTER_EDGE,
            .counterdirection  = TIMER_COUNTER_UP,
            .period            = 500,
            .clockdivision     = TIMER_CKDIV_DIV1,
            .repetitioncounter = 0,
        },
    .mode = TIMER_MODE_TIM,
};
OS_HAL_DEVICE_DEFINE("TIMER_Type", "timer4", timer4_info);

static struct gd32_timer_info timer2_info = {
    .timer_periph = TIMER2,
    .periph       = RCU_TIMER2,
    .nvic_irq     = TIMER2_IRQn,

    .timer_initpara =
        {
            .prescaler         = 119,
            .alignedmode       = TIMER_COUNTER_EDGE,
            .counterdirection  = TIMER_COUNTER_UP,
            .period            = 500,
            .clockdivision     = TIMER_CKDIV_DIV1,
            .repetitioncounter = 0,
        },
    .mode = TIMER_MODE_TIM,

};
OS_HAL_DEVICE_DEFINE("TIMER_Type", "timer2", timer2_info);

static struct gd32_timer_info timer3_info = {
    .timer_periph = TIMER3,
    .periph       = RCU_TIMER3,
    .nvic_irq     = TIMER3_IRQn,

    .timer_initpara =
        {
            .prescaler         = 119,
            .alignedmode       = TIMER_COUNTER_EDGE,
            .counterdirection  = TIMER_COUNTER_UP,
            .period            = 500,
            .clockdivision     = TIMER_CKDIV_DIV1,
            .repetitioncounter = 0,
        },
    .mode = TIMER_MODE_TIM,
};
OS_HAL_DEVICE_DEFINE("TIMER_Type", "timer3", timer3_info);

static struct gd32_timer_info timer1_info = {
    .timer_periph = TIMER1,
    .periph       = RCU_TIMER1,
    .nvic_irq     = TIMER1_IRQn,

    .timer_initpara =
        {
            .prescaler         = 119,
            .alignedmode       = TIMER_COUNTER_EDGE,
            .counterdirection  = TIMER_COUNTER_UP,
            .period            = 500,
            .clockdivision     = TIMER_CKDIV_DIV1,
            .repetitioncounter = 0,
        },
#ifdef OS_USING_PWM
    .pwm_info =
        {
            .pin_info =
                {
                    .periph       = RCU_GPIOB,
                    .pwm_port     = GPIOB,
                    .mode         = GPIO_MODE_AF,
                    .pwm_pin      = GPIO_PIN_10,
                    .speed        = GPIO_OSPEED_50MHZ,
                    .alt_func_num = GPIO_AF_1,
                },
            .timer_ocintpara =
                {
                    .ocpolarity   = TIMER_OC_POLARITY_HIGH,
                    .outputstate  = TIMER_CCX_ENABLE,
                    .ocnpolarity  = TIMER_OCN_POLARITY_HIGH,
                    .outputnstate = TIMER_CCXN_DISABLE,
                    .ocidlestate  = TIMER_OC_IDLE_STATE_LOW,
                    .ocnidlestate = TIMER_OCN_IDLE_STATE_LOW,
                },
            .channel = TIMER_CH_2,
            .ocmode  = TIMER_OC_MODE_PWM0,
        },
#endif
    .mode = TIMER_MODE_PWM,

};
OS_HAL_DEVICE_DEFINE("TIMER_Type", "timer1", timer1_info);

#endif
