#include <oneos_config.h>
#include <bus/bus.h>
#ifdef BSP_USING_CAN
#include <drv_can.h>

#ifdef BSP_USING_CAN0
static gd32_can_info_t can0_info = {
    .can_periph = CAN0,
    .rcu_periph = RCU_CAN0,
    .can_parameter_init =
        {
            .time_triggered        = DISABLE,
            .auto_bus_off_recovery = ENABLE,
            .auto_wake_up          = DISABLE,
            .auto_retrans          = DISABLE,
            .rec_fifo_overwrite    = DISABLE,
            .trans_fifo_order      = ENABLE,
            .working_mode          = CAN_NORMAL_MODE,
            .resync_jump_width     = CAN_BT_SJW_1TQ,
            .time_segment_1        = CAN_BT_BS1_5TQ,
            .time_segment_2        = CAN_BT_BS2_4TQ,
            .prescaler             = 5,
        },
    .can_filter_parameter_init =
        {
            .filter_number      = 0,
            .filter_mode        = CAN_FILTERMODE_MASK,
            .filter_bits        = CAN_FILTERBITS_32BIT,
            .filter_list_high   = 0x0000,
            .filter_list_low    = 0x0000,
            .filter_mask_high   = 0x0000,
            .filter_mask_low    = 0x0000,
            .filter_fifo_number = CAN_FIFO0,
            .filter_enable      = ENABLE,
        },
    .can_pin_info =
        {
            .periph       = RCU_GPIOB,
            .can_tx_port  = GPIOB,
            .can_tx_pin   = GPIO_PIN_9,
            .can_rx_port  = GPIOB,
            .can_rx_pin   = GPIO_PIN_8,
            .alt_func_num = GPIO_AF_9,
        },
};

OS_HAL_DEVICE_DEFINE("CAN_HandleTypeDef", "CAN0", can0_info);
#endif

#ifdef BSP_USING_CAN1
static gd32_can_info_t can1_info = {

};
OS_HAL_DEVICE_DEFINE("CAN_HandleTypeDef", "CAN1", can1_info);
#endif

#endif
