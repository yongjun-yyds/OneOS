#include "os_types.h"
#include "os_stddef.h"

#include <sdrv_ckgen.h>
#include <clock_ip.h>

const sdrv_ckgen_bus_config_t g_pre_bus_config = {
#if CONFIG_XIP_MODE
    .config_num = 0,
#else
    .config_num = 4,

    .config_nodes[0].clk_node = CLK_NODE(g_ckgen_bus_cr5_sf),
    .config_nodes[0].rate = 24000000,
    .config_nodes[0].post_div = CKGEN_BUS_DIV_4_2_1,

    .config_nodes[1].clk_node = CLK_NODE(g_ckgen_bus_ap_bus),
    .config_nodes[1].rate = 24000000,
    .config_nodes[1].post_div = CKGEN_BUS_DIV_4_2_1,

    .config_nodes[2].clk_node = CLK_NODE(g_ckgen_bus_disp_bus),
    .config_nodes[2].rate = 24000000,
    .config_nodes[2].post_div = CKGEN_BUS_DIV_4_2_1,

    .config_nodes[3].clk_node = CLK_NODE(g_ckgen_bus_seip),
    .config_nodes[3].rate = 24000000,
    .config_nodes[3].post_div = CKGEN_BUS_DIV_4_2_1,
#endif
};

const sdrv_ckgen_rate_config_t g_pll_config = {
#if CONFIG_XIP_MODE
    .config_num = 0,
#else
    .config_num = 10,

    .config_nodes[0].clk_node = CLK_NODE(g_pll1_root),
    .config_nodes[0].rate = 500000000,

    .config_nodes[1].clk_node = CLK_NODE(g_pll2_root),
    .config_nodes[1].rate = 600000000,

    .config_nodes[2].clk_node = CLK_NODE(g_pll3_root),
    .config_nodes[2].rate = 532000000,

    .config_nodes[3].clk_node = CLK_NODE(g_pll4_root),
    .config_nodes[3].rate = 600000000,

    .config_nodes[4].clk_node = CLK_NODE(g_pll5_root),
    .config_nodes[4].rate = 786432000,

    .config_nodes[5].clk_node = CLK_NODE(g_pll_lvds_root),
    .config_nodes[5].rate = 1039500000,

    .config_nodes[6].clk_node = CLK_NODE(g_pll_lvds_nodiv),
    .config_nodes[6].rate = 1039500000,

    .config_nodes[7].clk_node = CLK_NODE(g_pll_lvds_div2),
    .config_nodes[7].rate = 519750000,

    .config_nodes[8].clk_node = CLK_NODE(g_pll_lvds_div7),
    .config_nodes[8].rate = 74250000,

    .config_nodes[9].clk_node = CLK_NODE(g_pll_lvds_ckgen),
    .config_nodes[9].rate = 519750000,
#endif
};

const sdrv_ckgen_bus_config_t g_bus_config = {
#if CONFIG_XIP_MODE
    .config_num = 0,
#else
    .config_num = 4,

    .config_nodes[0].clk_node = CLK_NODE(g_ckgen_bus_cr5_sf),
    .config_nodes[0].rate = 300000000,
    .config_nodes[0].post_div = CKGEN_BUS_DIV_4_2_1,

    .config_nodes[1].clk_node = CLK_NODE(g_ckgen_bus_ap_bus),
    .config_nodes[1].rate = 300000000,
    .config_nodes[1].post_div = CKGEN_BUS_DIV_4_2_1,

    .config_nodes[2].clk_node = CLK_NODE(g_ckgen_bus_disp_bus),
    .config_nodes[2].rate = 300000000,
    .config_nodes[2].post_div = CKGEN_BUS_DIV_4_2_1,

    .config_nodes[3].clk_node = CLK_NODE(g_ckgen_bus_seip),
    .config_nodes[3].rate = 300000000,
    .config_nodes[3].post_div = CKGEN_BUS_DIV_4_2_1,
#endif
};

const sdrv_ckgen_rate_config_t g_ip_config = {
    .config_num = 12,
	
	/* UART频率 */
    .config_nodes[0].clk_node = CLK_NODE(g_ckgen_ip_uart_sf_1_to_8),
    .config_nodes[0].rate = 83000000,
	
	
	/* xtrg频率 */
    .config_nodes[1].clk_node = CLK_NODE(g_ckgen_ip_xtrg),
    .config_nodes[1].rate = 250000000,
	
	/* epwm频率 */
    .config_nodes[2].clk_node = CLK_NODE(g_ckgen_ip_epwm1),
    .config_nodes[2].rate = 250000000,
    .config_nodes[3].clk_node = CLK_NODE(g_ckgen_ip_epwm2),
    .config_nodes[3].rate = 250000000,
	
	/* SPI频率 */
	.config_nodes[4].clk_node = CLK_NODE(g_ckgen_ip_spi_sf_1_to_4),
    .config_nodes[4].rate = 133000000,
    .config_nodes[5].clk_node = CLK_NODE(g_ckgen_ip_spi_sf_5_to_8),
    .config_nodes[5].rate = 133000000,
	
	/* etimer频率 */
	.config_nodes[6].clk_node = CLK_NODE(g_ckgen_ip_etmr1),
    .config_nodes[6].rate = 250000000,
    .config_nodes[7].clk_node = CLK_NODE(g_ckgen_ip_etmr2),
    .config_nodes[7].rate = 250000000,
	
	/* CAN频率 */
	.config_nodes[8].clk_node = CLK_NODE(g_ckgen_ip_can),
    .config_nodes[8].rate = 40000000,
	
	/* ADC频率 */
	.config_nodes[9].clk_node = CLK_NODE(g_ckgen_ip_adc1),
    .config_nodes[9].rate = 24000000,

    .config_nodes[10].clk_node = CLK_NODE(g_ckgen_ip_adc2),
    .config_nodes[10].rate = 24000000,

    .config_nodes[11].clk_node = CLK_NODE(g_ckgen_ip_adc3),
    .config_nodes[11].rate = 48000000,
};

sdrv_ckgen_config_t g_clock_config = {
    .pre_bus_config     = &g_pre_bus_config,
    .pll_config         = &g_pll_config,
    .bus_config         = &g_bus_config,
    .core_config        = OS_NULL,
    .ip_config          = &g_ip_config,
    .gating_config      = OS_NULL,
};
