#include "oneos_config.h"
#include "os_types.h"

#include "drv_cfg.h"

#include "irq_num.h"
#include "regs_base.h"
#include "clock_ip.h"

#include "sdrv_pinctrl.h"
#include <drv_uart.h>
#include "drv_hwtimer.h"

sdrv_btm_cfg_t  g_btm_systick_config = {
    .base               = APB_BTM3_BASE,
    .irq                = BTM3_O_BTM_INTR_NUM,
    .tmr_id             = SDRV_BTM_G0,
    .tmr_cfg            =
    {
        .si_val         = 74,                       /*输入时钟每(si_val + 1)cycle, G0计数器增加inc_val个*/
        .inc_val        = 1,
        .frc_rld_rst_cnt_en = true,                 /*默认是true，表示初始化时是否对counter清零。*/
        .term_use_mode  = SDRV_BTM_DIRECT,          /*设置overflow值立即生效，而不是等到下一周期生效。*/
        .cmp_use_mode   = SDRV_BTM_DIRECT,          /*设置compare值立即生效，而不是等到下一周期生效。*/
        .cnt_dir        = SDRV_BTM_CNT_UP,          /*设置计数器的值是向上增长*/
        .cnt_mode       = SDRV_BTM_CONTINOUS_MODE,  /*设置计数器是连续模式*/
    }
};

#ifdef BSP_USING_UART5
const pin_settings_config_t g_uart5_pin_config[] = {
    /* Pin 19, GPIO_Y2, Mux:UART5.TXD */
    {
        .pin_index         =  GPIO_Y2,
        .mux               =  PIN_MUX_ALT4,
        .open_drain        =  PIN_PUSH_PULL,
        .pull_config       =  PIN_PULL_UP,
        .drive_strength    =  PIN_DS_8MA,
        .slew_rate         =  PIN_SR_FAST,
        .input_select      =  PIN_IS_CMOS,
        .data_direction    =  PIN_OUTPUT_DIRECTION,
        .interrupt_config  =  PIN_INTERRUPT_DISABLED,
        .initial_value     =  PIN_LEVEL_LOW,
        .force_input       =  PIN_FORCE_INPUT_NORMAL,
        .mode_select       =  PIN_MODE_DIGITAL,
    },

    /* Pin 20, GPIO_Y3, Mux:UART6.RXD */
    {
        .pin_index         =  GPIO_Y3,
        .mux               =  PIN_MUX_ALT4,
        .open_drain        =  PIN_PUSH_PULL,
        .pull_config       =  PIN_PULL_UP,
        .drive_strength    =  PIN_DS_8MA,
        .slew_rate         =  PIN_SR_FAST,
        .input_select      =  PIN_IS_CMOS_SCHMITT,
        .data_direction    =  PIN_OUTPUT_DIRECTION,
        .interrupt_config  =  PIN_INTERRUPT_DISABLED,
        .initial_value     =  PIN_LEVEL_LOW,
        .force_input       =  PIN_FORCE_INPUT_NORMAL,
        .mode_select       =  PIN_MODE_DIGITAL,
    },
};
const uint8_t g_uart5_pin_num = sizeof(g_uart5_pin_config) / sizeof(pin_settings_config_t);

static semidrive_uart_info_t uart5_info = {
    .base       = APB_UART5_BASE,
    .irq        = UART5_INTR_NUM,
    .pin_config         = &g_uart5_pin_config[0],
    .pin_num            = g_uart5_pin_num,
    .irq_priority       = 10,
    .ckgen              = CLK_NODE(g_ckgen_ip_uart_sf_1_to_8),
};
OS_HAL_DEVICE_DEFINE("semidrive_uart", "uart5", uart5_info);
#endif

#ifdef BSP_USING_UART6
const pin_settings_config_t g_uart6_pin_config[] = {
    /* Pin 21, GPIO_Y4, Mux:UART6.TXD */
    {
        .pin_index         =  GPIO_Y4,
        .mux               =  PIN_MUX_ALT4,
        .open_drain        =  PIN_PUSH_PULL,
        .pull_config       =  PIN_PULL_UP,
        .drive_strength    =  PIN_DS_8MA,
        .slew_rate         =  PIN_SR_FAST,
        .input_select      =  PIN_IS_CMOS,
        .data_direction    =  PIN_OUTPUT_DIRECTION,
        .interrupt_config  =  PIN_INTERRUPT_DISABLED,
        .initial_value     =  PIN_LEVEL_LOW,
        .force_input       =  PIN_FORCE_INPUT_NORMAL,
        .mode_select       =  PIN_MODE_DIGITAL,
    },

    /* Pin 22, GPIO_Y5, Mux:UART6.RXD */
    {
        .pin_index         =  GPIO_Y5,
        .mux               =  PIN_MUX_ALT4,
        .open_drain        =  PIN_PUSH_PULL,
        .pull_config       =  PIN_PULL_UP,
        .drive_strength    =  PIN_DS_8MA,
        .slew_rate         =  PIN_SR_FAST,
        .input_select      =  PIN_IS_CMOS_SCHMITT,
        .data_direction    =  PIN_OUTPUT_DIRECTION,
        .interrupt_config  =  PIN_INTERRUPT_DISABLED,
        .initial_value     =  PIN_LEVEL_LOW,
        .force_input       =  PIN_FORCE_INPUT_NORMAL,
        .mode_select       =  PIN_MODE_DIGITAL,
    },
};
const uint8_t g_uart6_pin_num = sizeof(g_uart6_pin_config) / sizeof(pin_settings_config_t);

static semidrive_uart_info_t uart6_info = {
    .base       = APB_UART6_BASE,
    .irq        = UART6_INTR_NUM,
    .pin_config         = &g_uart6_pin_config[0],
    .pin_num            = g_uart6_pin_num,
    .irq_priority       = 10,
    .ckgen              = CLK_NODE(g_ckgen_ip_uart_sf_1_to_8),
};
OS_HAL_DEVICE_DEFINE("semidrive_uart", "uart6", uart6_info);
#endif

#ifdef BSP_USING_UART7
const pin_settings_config_t g_uart7_pin_config[] = {
    /* Pin 23, GPIO_Y6, Mux:UART7.TXD */
    {
        .pin_index         =  GPIO_Y6,
        .mux               =  PIN_MUX_ALT4,
        .open_drain        =  PIN_PUSH_PULL,
        .pull_config       =  PIN_PULL_UP,
        .drive_strength    =  PIN_DS_8MA,
        .slew_rate         =  PIN_SR_FAST,
        .input_select      =  PIN_IS_CMOS,
        .data_direction    =  PIN_OUTPUT_DIRECTION,
        .interrupt_config  =  PIN_INTERRUPT_DISABLED,
        .initial_value     =  PIN_LEVEL_LOW,
        .force_input       =  PIN_FORCE_INPUT_NORMAL,
        .mode_select       =  PIN_MODE_DIGITAL,
    },

    /* Pin 24, GPIO_Y7, Mux:UART7.RXD */
    {
        .pin_index         =  GPIO_Y7,
        .mux               =  PIN_MUX_ALT4,
        .open_drain        =  PIN_PUSH_PULL,
        .pull_config       =  PIN_PULL_UP,
        .drive_strength    =  PIN_DS_8MA,
        .slew_rate         =  PIN_SR_FAST,
        .input_select      =  PIN_IS_CMOS_SCHMITT,
        .data_direction    =  PIN_OUTPUT_DIRECTION,
        .interrupt_config  =  PIN_INTERRUPT_DISABLED,
        .initial_value     =  PIN_LEVEL_LOW,
        .force_input       =  PIN_FORCE_INPUT_NORMAL,
        .mode_select       =  PIN_MODE_DIGITAL,
    },
};
const uint8_t g_uart7_pin_num = sizeof(g_uart7_pin_config) / sizeof(pin_settings_config_t);

static semidrive_uart_info_t uart7_info = {
    .base       = APB_UART7_BASE,
    .irq        = UART7_INTR_NUM,
    .pin_config         = &g_uart7_pin_config[0],
    .pin_num            = g_uart7_pin_num,
    .irq_priority       = 10,
    .ckgen              = CLK_NODE(g_ckgen_ip_uart_sf_1_to_8),
};
OS_HAL_DEVICE_DEFINE("semidrive_uart", "uart7", uart7_info);
#endif

#ifdef BSP_USING_BTM1
static semidrive_tim_info_t timer1_info = {
    .base = APB_BTM1_BASE,
    .irq = BTM1_O_BTM_INTR_NUM,
    .tmr_id = SDRV_BTM_G0,
};
OS_HAL_DEVICE_DEFINE("btm_timer", "btm1", timer1_info);
#endif

#ifdef BSP_USING_BTM2
static semidrive_tim_info_t timer2_info = {
    .base = APB_BTM2_BASE,
    .irq = BTM2_O_BTM_INTR_NUM,
    .tmr_id = SDRV_BTM_G0,
};
OS_HAL_DEVICE_DEFINE("btm_timer", "btm2", timer2_info);
#endif

#if 0
#ifdef BSP_USING_BTM3
static semidrive_tim_info_t timer3_info = {
    .base = APB_BTM3_BASE,
    .irq = BTM3_O_BTM_INTR_NUM,
    .tmr_id = SDRV_BTM_G0,
};
OS_HAL_DEVICE_DEFINE("btm_timer", "btm3", timer3_info);
#endif
#endif


#ifdef BSP_USING_BTM4
static semidrive_tim_info_t timer4_info = {
    .base = APB_BTM4_BASE,
    .irq = BTM4_O_BTM_INTR_NUM,
    .tmr_id = SDRV_BTM_G0,
};
OS_HAL_DEVICE_DEFINE("btm_timer", "btm4", timer4_info);
#endif

#ifdef BSP_USING_BTM5
static semidrive_tim_info_t timer5_info = {
    .base = APB_BTM5_BASE,
    .irq = BTM5_O_BTM_INTR_NUM,
    .tmr_id = SDRV_BTM_G0,
};
OS_HAL_DEVICE_DEFINE("btm_timer", "btm5", timer5_info);
#endif

#ifdef BSP_USING_BTM6
static semidrive_tim_info_t timer6_info = {
    .base = APB_BTM6_BASE,
    .irq = BTM6_O_BTM_INTR_NUM,
    .tmr_id = SDRV_BTM_G0,
};
OS_HAL_DEVICE_DEFINE("btm_timer", "btm6", timer6_info);
#endif

