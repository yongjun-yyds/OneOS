#include "board.h"
#include <sdrv_rstgen.h>
#include "reset_ip.h"



#if CONFIG_ARCH_WITH_MPU

__WEAK const mpu_config_t board_mpu_config[] = {
    /* background region, 4GB size, not accessable. */
    {0, 0x100000000, MPU_REGION_NO_ACCESS},

#if IRAM1_BASE
#if ((CONFIG_E3210) || (CONFIG_E3110))
    /* iram region, normal */
    {IRAM1_BASE, 0x100000, MPU_REGION_NORMAL},
#elif (CONFIG_E3106)
    /* iram region, normal */
    {IRAM1_BASE, 0x80000, MPU_REGION_NORMAL},
#elif (CONFIG_E3104)
    /* iram region, normal */
    {IRAM1_BASE, 0x40000, MPU_REGION_NORMAL},
#endif

    /* iram ecc region, normal */
    {IRAM1_ECC_BASE, 0x20000, MPU_REGION_NORMAL},
#endif

    /* hsm reg region, device */
    {SEIP_SEIP_BASE, 0x100000, MPU_REGION_DEVICE},

    /* xspi1 region, for flash, read only */
    {XSPI1_BASE, 0x10000000, MPU_REGION_NORMAL_RO},

#if ((CONFIG_E3210) || (CONFIG_E3110))
    /* IP module, device */
    {0xF0000000, 0x4000000, MPU_REGION_DEVICE},
#else
    /* IP module, device */
    {0xF0000000, 0x2000000, MPU_REGION_DEVICE},
#endif

    /* tcm region, normal */
    {CONFIG_ARMV7R_TCMB_BASE, 0x20000, MPU_REGION_NORMAL},

    /* int vector, read only */
    {(addr_t)&__vector, 0x40, MPU_REGION_NORMAL_RO},

    /* int vector, read only */
    {0, 0x40, MPU_REGION_NORMAL_RO},
};

__WEAK uint32_t num_of_mpu_cfg = ARRAY_SIZE(board_mpu_config);

void board_mpu_init(void)
{
    uint32_t index = 0;

    mpu_enable(false);

    mpu_clear_region();

    for (uint32_t i = 0; i < num_of_mpu_cfg; i++) {
        const mpu_config_t *config = &board_mpu_config[i];
        if (config->size > 0) {
            mpu_add_region(index++, config->addr, config->size, config->type);
        }
    }

    if (index > 0) {
        mpu_enable(true);
    }
}
#endif

/*
 * Call constructor functions.
 * This is required when the project has C++ modules.
 */
void call_constructors(void)
{
#if __GNUC__
    extern void (*__ctor_list[])(void);
    extern void (*__ctor_end[])(void);
    void (**ctor)(void);

    for (ctor = __ctor_list; ctor != __ctor_end; ctor++)
        (*ctor)();
#elif __ICCARM__
    extern void __iar_dynamic_initialization(void);
    __iar_dynamic_initialization();
#endif
}

extern uint32_t __vector;
void copy_intvec(void)
{
#if ((CONFIG_ARMV7R_USE_TCMB) && (CONFIG_ARMV7R_TCMB_BASE == 0x0))

    if ((uint32_t)(&__vector) != (uint32_t)0x0) {
        /* copy intvec to tcm */
        volatile uint64_t *p_dst = (uint64_t *)0x0;
        volatile uint64_t *p_src = (uint64_t *)(&__vector);

        for (int i = 0; i < 8; i++) {
            *p_dst++ = *p_src++;
        }
    }

#endif
}


sdrv_rstgen_sig_t *g_board_reset_array[] = {
#if 0
    &rstsig_dma_rst0,
    &rstsig_canfd9_16,
    &rstsig_canfd17_24,
    &rstsig_vic1,
    &rstsig_xtrg,
    &rstsig_xspi1a,
    &rstsig_xspi1b,
#else
    &rstsig_vic1,
#endif
};
const uint16_t g_board_reset_array_size = sizeof(g_board_reset_array) / sizeof(sdrv_rstgen_sig_t *);

static void chip_reset_init(void)
{
    uint32_t i = 0;

    for (i = 0; i < g_board_reset_array_size; i++) {
        sdrv_rstgen_reset(g_board_reset_array[i]);
    }

    sdrv_recovery_module(&recovery_module_array);
}

extern sdrv_btm_cfg_t  g_btm_systick_config;
extern sdrv_ckgen_config_t g_clock_config;
void semidrive_chip_init(void)
{
    if (sdrv_ckgen_init(&g_clock_config) != 0)
        while(1);

    chip_reset_init();

    irq_initialize(VIC1_BASE, IRQ_MAX_INTR_NUM);

    sdrv_pinctrl_clear_dispmux_config();
}

#ifdef OS_USING_LED
const led_t led_table[] = {
    {25, PIN_LOW},
    {26, PIN_LOW},
   // {18, PIN_LOW},
};

const int led_table_size = ARRAY_SIZE(led_table);
#endif


#ifdef OS_USING_PUSH_BUTTON
const struct push_button key_table[] = 
{
    {GPIO_A9,     PIN_MODE_INPUT_PULLUP, PIN_IRQ_MODE_FALLING},
    {GPIO_Y10,    PIN_MODE_INPUT_PULLDOWN, PIN_IRQ_MODE_RISING},
    {GPIO_Y11,    PIN_MODE_INPUT_PULLDOWN, PIN_IRQ_MODE_RISING},
};

const int key_table_size = ARRAY_SIZE(key_table);

#endif



