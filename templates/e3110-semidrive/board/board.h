
#ifndef __BOARD__
#define __BOARD__

#include <sdrv_gpio.h>
#include <device_pin.h>
#include <regs_base.h>
#include <reg.h>
#include <sdrv_btm.h>
#include <sdrv_btm_hw.h>
#include <sdrv_ckgen.h>
#include <clock_ip.h>
#include <sdrv_uart.h>
#include <irq.h>
#include <irq_num.h>

#include <os_types.h>
#include <drv_cfg.h>
#include <string.h>
#include <os_stddef.h>
#include <drv_gpio.h>

#if defined(__CC_ARM) || defined(__CLANG_ARM)
extern int Image$$RW_IRAM1$$ZI$$Limit;
#define HEAP_BEGIN 0
#elif __ICCARM__
#pragma section = "HEAP"
#define HEAP_BEGIN 0
#else
extern int __heap_start;
extern int __heap_end;
#define HEAP_BEGIN (&__heap_start)
#define HEAP_END   (&__heap_end)
#endif

#ifdef OS_USING_LED
extern const led_t led_table[];
extern const int   led_table_size;
#endif

#ifdef OS_USING_PUSH_BUTTON
extern const struct push_button key_table[];
extern const int                key_table_size;
#endif



extern void os_hw_console_output(char *log_buff);
void semidrive_chip_init(void);

#endif
