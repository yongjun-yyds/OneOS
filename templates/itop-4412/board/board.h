/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with 
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        board.h
 *
 * @brief       Board resource definition
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __BOARD_H__
#define __BOARD_H__

#include <drv_cfg.h>
#include <os_types.h>
#include <arch_mmu.h>

#define ARM_GIC_MAX_NR  (4)
#define ARM_GIC_NR_IRQS (160)
#define EXYNOS4412_GIC_COMBINE_BASE (0x10440000)

extern unsigned char __bss_start;
extern unsigned char __bss_end;

#define HEAP_BEGIN    ((void*)&__bss_end + 10*1024)
#define HEAP_END      (void*)(0xA0000000)

uint32_t platform_get_gic_dist_base(void);

uint32_t platform_get_gic_cpu_base(void);

#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif

#ifdef OS_USING_PUSH_BUTTON
    extern const struct push_button key_table[];
    extern const int                key_table_size;
#endif

#ifdef OS_USING_LED
extern const led_t led_table[];
extern const int   led_table_size;
#endif

#endif

