/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with 
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        board.c
 *
 * @brief       Initializes the CPU, System clocks, and Peripheral device
 *
 * @revision
 * Date         Author          Notes
 * 2020-03-10   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include "board.h"
#include <drv_gpio.h>
#include <arch_mmu.h>

#ifdef OS_USING_PUSH_BUTTON
const struct push_button key_table[] = 
{
    {GET_PIN(X3, 3),     PIN_MODE_INPUT_PULLUP, PIN_IRQ_MODE_FALLING},
    {GET_PIN(X2, 1),     PIN_MODE_INPUT_PULLUP, PIN_IRQ_MODE_FALLING},
    {GET_PIN(X2, 0),     PIN_MODE_INPUT_PULLUP, PIN_IRQ_MODE_FALLING},
    {GET_PIN(X1, 1),     PIN_MODE_INPUT_PULLUP, PIN_IRQ_MODE_FALLING},
    {GET_PIN(X1, 2),     PIN_MODE_INPUT_PULLUP, PIN_IRQ_MODE_FALLING},
};

const int key_table_size = ARRAY_SIZE(key_table);

#endif

const led_t led_table[] = 
{
    {GET_PIN(L2, 0), PIN_LOW},
    {GET_PIN(K1, 1), PIN_LOW},
};

const int led_table_size = ARRAY_SIZE(led_table);

/* struct mem_desc platform_mem_desc[] =  */
/* { */
/*     {0x02000000, 0x03000000, 0x02000000, DEVICE_MEM}, */
/*     {0x10000000, 0x40000000, 0x10000000, DEVICE_MEM}, */
/*     {0x40000000, 0xa0000000, 0x40000000, NORMAL_MEM}//MMU_ATTR_SEC_X_WT | DES_SEC_AP_KRW | DES_SEC_S} */
/* }; */
/*  */
/* const uint32_t platform_mem_desc_size = sizeof(platform_mem_desc)/sizeof(platform_mem_desc[0]); */


struct mem_desc platform_mem_desc[] = 
{
    {0x10000000, 0x14000000, 0x10000000, DEVICE_MEM},
    {0x02020000, 0x02060000, 0x02020000, DEVICE_MEM},
    {0x40000000, 0xa0000000, 0x40000000, NORMAL_MEM}
};

const uint32_t platform_mem_desc_size = sizeof(platform_mem_desc)/sizeof(platform_mem_desc[0]);
