#include <os_stddef.h>
#include <arch_smp.h>
#include <arch_misc.h>
#include <os_util.h>
#include <interrupt.h>
#include <arch_barrier.h>
#include <os_task.h>
#include <os_errno.h>


#ifdef OS_USING_SMP
#define IPI_RESCHEDULE 3
volatile os_base_t pen_release = -1;
#endif

#ifdef OS_USING_SMP
volatile  void * volatile smp_core_wait[OS_SMP_MAX_CPUS] = {0};
os_ubase_t        g_cpu_bootup[OS_SMP_MAX_CPUS]      = {0};


void arch_enable_cpu(int cpu_index,void (*smp_entry)(void *arg))
{
    smp_core_wait[cpu_index] = smp_entry;
    __dmb();
    __dsb();

    return;
}
#endif

void arch_wait_cpu(void)
{
#ifdef OS_USING_SMP
    int cpu_index;
    void (*smp_entry)(void *arg);    
    cpu_index = os_cpu_id_get();
    os_kprintf("%s:%d -- cpu id = %d\n\r", __FUNCTION__, __LINE__, cpu_index);
    /* os_hw_interrupt_init(); */
    pen_release = 1;

    while(smp_core_wait[cpu_index] == 0)
    {
        __dmb();
        __dsb();
    }

    os_kprintf("%s:%d -- cpu id = %d start run\n\r", __FUNCTION__, __LINE__, cpu_index);
    smp_entry = smp_core_wait[cpu_index];
    smp_entry((void *)cpu_index);
#endif    
    return;  
}

#ifdef OS_USING_SMP

#define SMC_CMD_CPU1BOOT	(-4)
void _reset(void);

static inline void __raw_writel(uint32_t val, volatile void  *addr)
{
	asm volatile("str %1, %0"
		     : : "Qo" (*(volatile uint32_t  *)addr), "r" (val));
}


#define writel_relaxed(v,c)	__raw_writel((uint32_t) (v),c)

static inline uint32_t exynos_smc(uint32_t cmd, uint32_t arg1, uint32_t arg2, uint32_t arg3)
{
	register uint32_t reg0 __asm__("r0") = cmd;
	register uint32_t reg1 __asm__("r1") = arg1;
	register uint32_t reg2 __asm__("r2") = arg2;
	register uint32_t reg3 __asm__("r3") = arg3;

     __asm__ volatile (
        ".arch_extension sec    \n"
         "smc    0\n"
         : "+r"(reg0), "+r"(reg1), "+r"(reg2), "+r"(reg3)
     );

	return reg0;
}

void os_hw_secondary_boot(uint32_t cpu)
{
    exynos_smc(SMC_CMD_CPU1BOOT,cpu, 0, 0);
    writel_relaxed((uint32_t)_reset, (volatile void *)(0x0204F01C+cpu*4));
    os_task_msleep(500);
    //os_hw_ipi_send(IPI_RESCHEDULE, 1 << cpu);
}


extern void arch_enable_cpu(int cpu_index,void (*smp_entry)(void *arg));

void secondary_cpu_start(void * arg);

void k_enable_cpu(int cpu_index)
{
    arch_enable_cpu(cpu_index,secondary_cpu_start);

    return;
}


void k_enable_smp_cpus(void)
{
    int i;
    /*Enable all cores except 0 core*/ 
    for (i = 1; i < OS_SMP_MAX_CPUS; i++)
    {
        k_enable_cpu(i);
    }
}


/* this func only be called in first task start under enter critical */
void k_smp_cpu_bootup(int cpu_index)
{
    if (cpu_index < OS_SMP_MAX_CPUS)
    {
        g_cpu_bootup[cpu_index] = 1;
    }
}


os_ubase_t k_smp_cpu_boot_stat(int cpu_index)
{
    if (cpu_index < OS_SMP_MAX_CPUS)
    {
        return g_cpu_bootup[cpu_index];
    }
    else
    {
        return 0;
    }
}

os_err_t boot_smp_cpu(void)
{
    extern void os_isr_ipi_handler(int vector, void *param);

    os_hw_ipi_handler_install(OS_SMP_IPI_SCHED, os_isr_ipi_handler);

    os_hw_interrupt_umask(OS_SMP_IPI_SCHED);

    void smp_secondary_cpu_boot(void);
    smp_secondary_cpu_boot();

    k_enable_smp_cpus();

    return OS_SUCCESS;
}

OS_INIT_CALL(boot_smp_cpu, OS_INIT_LEVEL_SMP, OS_INIT_SUBLEVEL_HIGH);

void smp_secondary_cpu_boot(void)
{
    int i = 0;

    for (i = 1; i < OS_SMP_MAX_CPUS; i++) 
    {
        do {
            pen_release = -1;
            __dmb();
            __dsb();
            os_kprintf("bootup cpu %d\r\n", i);
            os_hw_secondary_boot(i);
            //os_task_msleep(100);
        }
        while(-1 == pen_release);
        os_kprintf("bootup cpu %d over\r\n", i);
        os_task_msleep(400);
    }
}


void os_isr_ipi_handler(int vector, void *param)
{
}

#include <shell.h>
static int smp_boot(int argc, char *argv[])
{
    os_hw_secondary_boot(3);
    return 0;
}

SH_CMD_EXPORT(smp_boot, smp_boot, "smp_boot");
#endif

