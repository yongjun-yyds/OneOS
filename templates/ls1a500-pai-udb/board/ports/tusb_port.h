/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with 
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        tusb_port.h
 *
 * @brief       This file provides macro definition for tinyusb.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef TUSB_PORT_H_
#define TUSB_PORT_H_

#include <oneos_config.h>
#include <os_stddef.h>
#include <os_task.h>

#define CFG_TUSB_MCU OPT_MCU_L2K500
#define CFG_TUSB_RHPORT1_MODE       (OPT_MODE_HOST | OPT_MODE_FULL_SPEED)
#define CFG_TUSB_OS OPT_OS_NONE
#define CFG_TUSB_DEBUG           2

#endif /* TUSB_PORT_H_ */
