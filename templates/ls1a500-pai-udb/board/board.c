/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        board.c
 *
 * @brief       Initializes the CPU, System clocks, and Peripheral device
 *
 * @revision
 * Date         Author          Notes
 * 2020-11-17   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include <stdint.h>
#include <os_task.h>
#include <device.h>
#include <os_memory.h>
#include <os_clock.h>
#include <drv_gpio.h>
#include <oneos_config.h>
#include <board.h>

#ifdef OS_USING_PUSH_BUTTON
const struct push_button key_table[] = 
{
    {100,PIN_MODE_INPUT,PIN_IRQ_MODE_RISING},
};

const int key_table_size = ARRAY_SIZE(key_table);

#endif

#ifdef OS_USING_LED
const led_t led_table[] = 
{
    {104, PIN_LOW},
};

const int led_table_size = ARRAY_SIZE(led_table);
#endif

#ifdef OS_USING_TINYUSB_HW_OHCI
#include <tusb.h>

void hcd_int_enable(uint8_t rhport)
{
    (void) rhport;
    os_hw_interrupt_mask(72);
}

void hcd_int_disable(uint8_t rhport)
{
    (void) rhport;
    os_hw_interrupt_umask(72);
}

static void loongson_usb_ohci_irq(int vector, void *param)
{
    //os_kprintf("%s:%d\r\n", __func__, __LINE__);
    
    tuh_int_handler(1);
}

static os_err_t loongson_usb_ohci_init(void)
{
    //os_kprintf("%s:%d\r\n", __func__, __LINE__);

    os_hw_interrupt_install(72, loongson_usb_ohci_irq, OS_NULL, "tud");
    os_hw_interrupt_umask(72);

    return 0;
}

OS_INIT_CALL(loongson_usb_ohci_init, OS_INIT_LEVEL_DEVICE, OS_INIT_SUBLEVEL_MIDDLE);
#endif

