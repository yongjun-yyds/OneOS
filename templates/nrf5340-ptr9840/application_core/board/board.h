/**
 * Copyright (c) 2014 - 2020, Nordic Semiconductor ASA
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 *
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 *
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef BOARD_H
#define BOARD_H

#include <drv_cfg.h>
#include <drv_common.h>
#include "nrfx.h"

/* On-chip flash config */

#ifdef BLE_SOFTDEVICE
#define BLE_SOFTDEVICE_KB   (153)
#define BLE_SOFTDEVICE_SIZE (BLE_SOFTDEVICE_KB * 1024)
#else
#define BLE_SOFTDEVICE_KB   (0)
#define BLE_SOFTDEVICE_SIZE (BLE_SOFTDEVICE_KB * 1024)
#endif

#define NRF53_FLASH_START_ADDRESS ((uint32_t)0x00000000)
#define NRF53_FLASH_SIZE          (1024 * 1024)
#define NRF53_FLASH_END_ADDRESS   ((uint32_t)(NRF53_FLASH_START_ADDRESS + NRF53_FLASH_SIZE))

#define NRF53_SRAM1_START_ADDRESS ((uint32_t)0x20000000)
#define NRF53_SRAM1_SIZE          (256 * 1024)
#define NRF53_SRAM1_END_ADDRESS   (NRF53_SRAM1_START_ADDRESS + NRF53_SRAM1_SIZE)

#define NRF53_SRAM2_START_ADDRESS ((uint32_t)0x20040000)
#define NRF53_SRAM2_SIZE          (256 * 1024)
#define NRF53_SRAM2_END_ADDRESS   (NRF53_SRAM2_START_ADDRESS + NRF53_SRAM2_SIZE)

#if defined(__CC_ARM) || defined(__CLANG_ARM)
extern int Image$$RW_IRAM1$$ZI$$Limit;
#define HEAP_BEGIN (&Image$$RW_IRAM1$$ZI$$Limit)
#elif __ICCARM__
#pragma section = "HEAP"
#define HEAP_BEGIN (__segment_end("HEAP"))
#else
extern int _heap_start;
extern int _heap_end;

#define HEAP_BEGIN (&_heap_start)
//#define HEAP_END   (&_heap_end)

#endif

#define HEAP_END NRF53_SRAM1_END_ADDRESS

// extern const struct push_button key_table[];
// extern const int                key_table_size;

extern const led_t led_table[];
extern const int   led_table_size;

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif
