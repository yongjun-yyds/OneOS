extern TIM_HandleTypeDef htim2;
OS_HAL_DEVICE_DEFINE("TIM_HandleTypeDef", "tim2", htim2);

extern TIM_HandleTypeDef htim15;
OS_HAL_DEVICE_DEFINE("TIM_HandleTypeDef", "tim15", htim15);

extern TIM_HandleTypeDef htim16;
OS_HAL_DEVICE_DEFINE("TIM_HandleTypeDef", "tim16", htim16);

extern UART_HandleTypeDef huart2;
OS_HAL_DEVICE_DEFINE("UART_HandleTypeDef", "uart2", huart2);

extern DMA_HandleTypeDef hdma_usart2_rx;
OS_HAL_DEVICE_DEFINE("DMA_HandleTypeDef", "dma_usart2_rx", hdma_usart2_rx);

