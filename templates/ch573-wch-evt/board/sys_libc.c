/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        sys_libc.c
 *
 * @brief       This file provides system call related adaptation, including  memory management, etc.
 *
 * @revision
 * Date         Author          Notes
 * 2020-04-17   OneOS Team      First version.
 ***********************************************************************************************************************
 */
#include <stdio.h>
#include <board.h>
#include <os_stddef.h>
#include <os_assert.h>
#include <os_util.h>
#include <os_task.h>
#include <os_memory.h>
#include <os_errno.h>

#ifndef _REENT_ONLY
int *__errno()
{
    return os_errno();
}
#endif

void *_malloc_r(struct _reent *ptr, size_t size)
{
    void *result;
#if defined(OS_USING_HEAP)
    result = (void *)os_malloc(size);
#else
    result = OS_NULL;
#endif
    if (result == OS_NULL)
    {
        ptr->_errno = OS_NOMEM;
    }

    return result;
}

void *_realloc_r(struct _reent *ptr, void *old, size_t newlen)
{
    void *result;

#if defined(OS_USING_HEAP)
    result = (void *)os_realloc(old, newlen);
#else
    result = OS_NULL;
#endif
    if (result == OS_NULL)
    {
        ptr->_errno = OS_NOMEM;
    }

    return result;
}

void *_calloc_r(struct _reent *ptr, size_t size, size_t len)
{
    void *result;

#if defined(OS_USING_HEAP)
    result = (void *)os_calloc(size, len);
#else
    result = OS_NULL;
#endif
    if (result == OS_NULL)
    {
        ptr->_errno = OS_NOMEM;
    }

    return result;
}

void _free_r(struct _reent *ptr, void *addr)
{
#if defined(OS_USING_HEAP)
    if (addr != OS_NULL)
    {
        os_free(addr);
    }
#endif
}

/* clang-format off */
void exit(int status)
{
    os_kprintf("task:%s exit with %d\n", os_task_get_name(os_get_current_task()), status);
    OS_ASSERT(0);

    while (1);
}
/* clang-format on */

void __libc_init_array(void)
{
    /* We not use __libc init_aray to initialize C++ objects. */
}

extern os_err_t os_task_suspend(os_task_id tid);
extern void     os_schedule(void);
/* clang-format off */
void abort(void)
{
    if (os_get_current_task())
    {
        os_task_id self = os_get_current_task();

        os_kprintf("task:%-8.*s abort!\n", OS_NAME_MAX, os_task_get_name(self));
        os_task_suspend(self);
    }

    while (1);
}
/* clang-format on */
