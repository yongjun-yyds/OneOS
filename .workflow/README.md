# 主仓库CI能力

------

## 简介

主仓库CI工具拥有以下能力：代码格式审查、代码静态检查、代码编译审查。因为耗时原因目前代码编译审查不进行。

## 目录结构

GitLab CI相关文件目录结构如下表所示：

| 目录                                | 说明                                                     |
| ----------------------------------- | -------------------------------------------------------- |
| oneos/.gitlab-ci.yml                | GitLab CI配置文件                                        |
| oneos/.clang-format                 | 主仓库代码格式化配置文件（也是CI代码格式审查的配置文件） |
| oneos/.clang-format-ignore          | 主仓库CI代码格式审查忽略路径或文件                       |
| oneos/.cppcheck-ignore              | 主仓库CI代码静态检查忽略路径或文件                       |
| oneos/.workflow/ci/format_check     | CI格式审查的工具及脚本文件                               |
| oneos/.workflow/ci/static_analysis  | CI静态检查的脚本文件                                     |
| oneos/.workflow/ci/build_scripts    | CI编译审查的脚本文件（暂不进行）                         |
| oneos/.workflow/ci/projects_compile | CI编译审查的MDK工程（暂不进行）                          |

## 使用说明

- 目前主仓库拥有CI能力，子仓库若需要部署CI，则需要在子仓库的根目录下做相同配置；

- 需要格式化文件时，将VS Code设置为保存时自动格式化，保存即可对想要格式化的文件进行格式化操作，配置是参照VS Code打开的文件夹的根目录下的.clang-format文件；

- CI中不想进行代码格式审查的文件，需要将文件路径添加到根目录下的.clang-format-ignore文件（此操作对VS Code文件保存自动格式化无效）；

- 某个文件的一处或几处特殊格式的代码不想进行代码格式审查的，可以在代码的开始插入这样一行 /\* clang-format off \*/和代码的结束插入一行/\* clang-format on \*/来屏蔽格式审查；

- CI中不想进行代码静态的文件，需要将文件路径添加到根目录下的.cppcheck-ignore文件；

- CI中的代码编译审查需要将想要编译的代码配置进MDK工程才能进行代码的编译审查（更新.config和oneos_config），也支持新增工程。（暂不进行）；

- CI的审查对象是本次Push或Merge的改动文件或新增文件;

- 三方源码可以不进行格式审查和静态检查，只对OneOS适配层或其他开发代码进行格式审查和静态检查;

- 自研代码必须进行格式审查和静态检查。

  **详细操作见**[用户指南](ci/doc/USER_GUIDE.md)。

## 注意事项

1. CI不通过仍然可以合并代码，审核人可以查看CI失败的原因。
2. 若使用fork方式开发项目，需要将代码仓库的审核人加入到个人仓库的成员中(Reporter权限即可)，否则审核人无法看到CI的状态。
3. 主仓库CI的Runner是一台服务器（oneos-primary-runner），会比较繁忙，子仓库的Runner可以指定为（oneos-runner）或自行搭建Runner。
4. 个人首次使用CI时会比较耗时（拉取代码仓库），非首次会大大提速。
5. 开发人数较多，为减轻oneos-primary-runner服务器负担，建议减少push频率，建议多条commit一起push。

