# Copyright (c) 2021, China Mobile Communications Group Co.,Ltd.
# Copyright (c) 2017 Guillaume Papin

# SPDX-License-Identifier: MIT License

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

#!/usr/bin/env python
"""A wrapper script around clang-format, suitable for linting multiple files
and to use for continuous integration.

This is an alternative API for the clang-format command line.
It runs over multiple files and directories in parallel.
A diff output is produced and a sensible exit code is returned.

"""

from __future__ import print_function, unicode_literals

import argparse
import codecs
import difflib
import errno
import fnmatch
import io
import multiprocessing
import os
import signal
import subprocess
import sys
import traceback
from distutils.util import strtobool

from functools import partial

try:
    from subprocess import DEVNULL  # py3k
except ImportError:
    DEVNULL = open(os.devnull, "wb")


DEFAULT_EXTENSIONS = 'c,h,C,H,cpp,hpp,cc,hh,c++,h++,cxx,hxx'
DEFAULT_CPPCHECK_IGNORE = '.cppcheck-ignore'


class ExitStatus:
    SUCCESS = 0
    TROUBLE = 1


class Changes:
    def __init__(self, base_url, base_branch):
        self.base_url = base_url
        self.base_branch = base_branch

    def get_change_files(self, extensions, exclude):
        file_list = []
        fpaths = []
        try:
            os.system('git remote add base {}'.format(self.base_url))
            os.system('git fetch base')
            os.system('git reset base/{} --soft'.format(self.base_branch))
            os.system('git status > git_changes.txt')
        except Exception as e:
            print(e)
            return None
        try:
            with open('git_changes.txt', 'r') as f:
                file_lines = f.readlines()
        except Exception as e:
            print(e)
            return None
        file_path = ''
        print('Changes files:'.format(file_path))
        for line in file_lines:
            if 'new file' in line:
                file_path = line.split('new file:')[1].strip()
                print('new file:   {}'.format(file_path))
            elif 'deleted' in line:
                print('deleted file:   {}'.format(line.split('deleted:')[1].strip()))
            elif 'modified' in line:
                file_path = line.split('modified:')[1].strip()
                print('modified file:   {}'.format(file_path))
            else:
                continue

            count = len(exclude)
            i = 0
            for pattern in exclude:
                i = i + 1
                if not fnmatch.fnmatch(file_path, pattern):
                    if i == count:
                        fpaths.append(file_path)
                else:
                    break

        for f in fpaths:
            ext = os.path.splitext(f)[1][1:]
            if ext in extensions:
                file_list.append(f)

        try:
            os.system('del git_changes.txt')
        except Exception as e:
            print(e)

        return file_list


def excludes_from_file(ignore_file):
    excludes = []
    try:
        with io.open(ignore_file, 'r', encoding='utf-8') as f:
            for line in f:
                if line.startswith('#'):
                    # ignore comments
                    continue
                pattern = line.rstrip()
                if not pattern:
                    # allow empty lines
                    continue
                excludes.append(pattern)
    except EnvironmentError as e:
        if e.errno != errno.ENOENT:
            raise
    return excludes


def list_files_via_compare(compare=False, extensions=None, exclude=None):
    if extensions is None:
        extensions = []
    if exclude is None:
        exclude = []

    out = []
    if compare:
        #list change files by comparing to public master branch
        changes = Changes('git@10.12.3.198:cd/oneos/oneos-one/oneos/oneos.git', 'dev')
        out = changes.get_change_files(extensions, exclude)

    return out


def list_files_via_directory(files, recursive=False, extensions=None, exclude=None):
    if extensions is None:
        extensions = []
    if exclude is None:
        exclude = []

    out = []
    for file in files:
        if recursive and os.path.isdir(file):
            for dirpath, dnames, fnames in os.walk(file):
                fpaths = [os.path.join(dirpath, fname) for fname in fnames]
                for pattern in exclude:
                    # os.walk() supports trimming down the dnames list
                    # by modifying it in-place,
                    # to avoid unnecessary directory listings.
                    dnames[:] = [
                        x for x in dnames
                        if
                        not fnmatch.fnmatch(os.path.join(dirpath, x), pattern)
                    ]
                    fpaths = [
                        x for x in fpaths if not fnmatch.fnmatch(x, pattern)
                    ]
                for f in fpaths:
                    ext = os.path.splitext(f)[1][1:]
                    if ext in extensions:
                        out.append(f)
        else:
            out.append(file)
    return out


def bold_red(s):
    return '\x1b[1m\x1b[31m' + s + '\x1b[0m'


def colorize(diff_lines):
    def bold(s):
        return '\x1b[1m' + s + '\x1b[0m'

    def cyan(s):
        return '\x1b[36m' + s + '\x1b[0m'

    def green(s):
        return '\x1b[32m' + s + '\x1b[0m'

    def red(s):
        return '\x1b[31m' + s + '\x1b[0m'

    for line in diff_lines:
        if line[:4] in ['--- ', '+++ ']:
            yield bold(line)
        elif line.startswith('@@ '):
            yield cyan(line)
        elif line.startswith('+'):
            yield green(line)
        elif line.startswith('-'):
            yield red(line)
        else:
            yield line


def print_process(diff_lines, use_color):
    if use_color:
        diff_lines = colorize(diff_lines)
    if sys.version_info[0] < 3:
        sys.stdout.writelines((l.encode('utf-8') for l in diff_lines))
    else:
        sys.stdout.writelines(diff_lines)


def print_trouble(prog, message, use_colors):
    error_text = 'error:'
    if use_colors:
        error_text = bold_red(error_text)
    print("{}: {} {}".format(prog, error_text, message), file=sys.stderr)


def split_list_arg(arg):
    """
    If arg is a list containing a single argument it is split into multiple elements.
    Otherwise it is returned unchanged
    Workaround for GHA not allowing list arguments
    """
    return arg[0].split() if len(arg) == 1 else arg


def main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument(
        '--extensions',
        help='comma separated list of file extensions (default: {})'.format(
            DEFAULT_EXTENSIONS),
        default=DEFAULT_EXTENSIONS)
    parser.add_argument(
        '-r',
        '--recursive',
        action='store_true',
        help='run recursively over directories')
    parser.add_argument('files', metavar='file', nargs='*')
    parser.add_argument(
        '-c',
        '--compare',
        action='store_true',
        help='compare files with public master branch')
    parser.add_argument(
        '-q',
        '--quiet',
        action='store_true',
        help="disable output, useful for the exit code")
    parser.add_argument(
        '-j',
        metavar='N',
        type=int,
        default=0,
        help='run N cppcheck jobs in parallel'
        ' (default number of cpus + 1)')
    parser.add_argument(
        '--color',
        default='auto',
        choices=['auto', 'always', 'never'],
        help='show colored diff (default: auto)')
    parser.add_argument(
        '-e',
        '--exclude',
        metavar='PATTERN',
        action='append',
        default=[],
        help='exclude paths matching the given glob-like pattern(s)'
        ' from recursive search')

    args = parser.parse_args()

    # use default signal handling, like diff return SIGINT value on ^C
    # https://bugs.python.org/issue14229#msg156446
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    try:
        signal.SIGPIPE
    except AttributeError:
        # compatibility, SIGPIPE does not exist on Windows
        pass
    else:
        signal.signal(signal.SIGPIPE, signal.SIG_DFL)

    colored_stdout = False
    colored_stderr = False
    if args.color == 'always':
        colored_stdout = True
        colored_stderr = True
    elif args.color == 'auto':
        colored_stdout = sys.stdout.isatty()
        colored_stderr = sys.stderr.isatty()

    try:
        subprocess.check_call('cppcheck --version', stdout=DEVNULL)
    except subprocess.CalledProcessError as e:
        print_trouble(parser.prog, str(e), use_colors=colored_stderr)
        return ExitStatus.TROUBLE
    except OSError as e:
        print_trouble(
            parser.prog,
            "Command '{}' failed to start: {}".format(
                subprocess.list2cmdline('cppcheck --version'), e
            ),
            use_colors=colored_stderr,
        )
        return ExitStatus.TROUBLE

    retcode = ExitStatus.SUCCESS

    excludes = excludes_from_file(DEFAULT_CPPCHECK_IGNORE)
    excludes.extend(split_list_arg(args.exclude))

    files = []
    if args.compare:
        files = list_files_via_compare(
            compare=args.compare,
            exclude=excludes,
            extensions=args.extensions.split(','))
    elif args.recursive:
        files = list_files_via_directory(
            split_list_arg(args.files),
            recursive=args.recursive,
            exclude=excludes,
            extensions=args.extensions.split(','))

    if not files:
        print_trouble(parser.prog, 'No files found', use_colors=colored_stderr)
        return ExitStatus.SUCCESS

    if not args.quiet:
        print("Processing {} files:".format(len(files)))
        for file in files: 
            print(file)

    error_list = []
    for file in files:
        os.system('cppcheck {} -j8 --template="{{severity}}\\t{{file}}:{{line}}\\t{{id}}: {{message}}" --output-file=cppcheck_result.txt --suppress=unusedFunction --suppress=preprocessorErrorDirective --force'.format(file))
        try:
            with io.open('cppcheck_result.txt', 'r', encoding='utf-8') as f:
                for line in f:
                    error = line.rstrip()
                    if not error:
                        continue
                    error_list.append(error)
        except EnvironmentError as e:
            if e.errno != errno.ENOENT:
                raise

    if not error_list:
        print('Cppcheck result: no error')
    else:
        print('Cppcheck result: {} errors'.format(len(error_list)))
        for error in error_list: 
            print(error)
        retcode = ExitStatus.TROUBLE


    try:
        os.system('del cppcheck_result.txt')
    except Exception as e:
        print(e)

    return retcode


if __name__ == '__main__':
    sys.exit(main())
