# 用户指南

## 目录

- 如何设置VS Code进行格式化代码
- 如何忽略文件中一处代码的格式审查
- CI中如何忽略文件的格式审查
- CI中如何忽略文件的静态审查

## 具体操作

### 如何设置VS Code进行格式化代码

1. 安装插件C/C++插件，保证插件版本在v1.9.2或以上，否则指针符号会飘在中间。（实际用于格式化的是clang-format.exe执行文件，版本要求是13.0.0，只要满足C/C++插件版本在v1.9.2或以上即可）

![1_1](image/1_1.jpg)

2. 需要格式化代码时，将VS Code设置为保存时自动格式化，格式化的配置文件放在在VS Code打开文件夹的根目录下，名为.clang-format。

![1_0](image/1_0.png)

3. 需要批量格式化一个文件夹下的代码文件时，安装Format Files插件，找到在对应格式化的文件夹右键Start Format Files: This Folder即可，可以实现递归查找并格式化文件。

![1_3](image/1_3.jpg)

### 如何忽略文件中一处代码的格式审查

代码的开始插入一行 /\* clang-format off \*/和代码的结束插入一行/\* clang-format on \*/来屏蔽代码格式化和CI中的代码格式审查。

![2_0](image/2_0.png)

VS Code保存时这段代码将不会被格式化，同时CI中的代码格式审查也会忽略这段代码。

### CI中如何忽略文件的格式审查

找到根目录下的.clang-format-ignore文件，将文件路径描述进来，同时也支持路径+通配符的方式屏蔽文件夹下所有文件的CI格式审查。.clang-format-ignore只对CI代码格式审查有效，对VS Code的保存自动格式化无效。CI只对文件扩展名为"c,h,C,H,cpp,hpp,cc,hh,c++,h++,cxx,hxx"的文件进行格式审查，非此类文件自动忽略。

![3_0](image/3_0.png)

### CI中如何忽略文件的静态检查

找到根目录下的.cppcheck-ignore文件，将文件路径描述进来，同时也支持路径+通配符的方式屏蔽文件夹下所有文件的CI格式审查。CI只对文件扩展名为"c,h,C,H,cpp,hpp,cc,hh,c++,h++,cxx,hxx"的文件进行静态检查，非此类文件自动忽略。

![4_0](image/4_0.png)

## 注意事项

1. CI代码格式审查和静态检查只针对扩展名为"c,h,C,H,cpp,hpp,cc,hh,c++,h++,cxx,hxx"的文件，非此类文件自动忽略，无需手动加入到.ignore文件。
2. 若使用fork方式开发项目，需要将代码仓库的审核人加入到个人仓库的成员中(Reporter权限即可)，否则审核人无法看到CI的状态。
3. 主仓库CI的Runner是一台服务器（oneos-primary-runner），会比较繁忙，子仓库的Runner可以指定为（oneos-runner）或自行搭建Runner。
4. 个人首次使用CI时会比较耗时（拉取代码仓库），非首次会大大提速。
5. 开发人数较多，为减轻oneos-primary-runner服务器负担，建议减少push频率，建议多条commit一起push。

