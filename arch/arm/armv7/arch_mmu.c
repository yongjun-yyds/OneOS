/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with 
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on 
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the 
 * specific language governing permissions and limitations under the License.
 *
 * @file        mmu.c
 *
 * @brief       This file implements the mmu functions.
 *
 * @revision
 * Date         Author          Notes
 * 2020-06-22   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include <oneos_config.h>
#include <os_util.h>

#include "arch_cp15.h"
#include "arch_mmu.h"

extern os_ubase_t os_cpu_id_get(void);
extern void os_hw_cpu_dcache_disable(void);
extern void os_hw_cpu_icache_disable(void);
extern void scu_enable(void);
extern void arm_branch_target_cache_invalidate(void);
extern void arm_branch_prediction_enable(void);
extern void scu_secure_invalidate(unsigned int cpu, unsigned int ways);
extern void scu_join_smp(void);
extern void scu_enable_maintenance_broadcast(void);
extern void os_hw_cpu_icache_enable(void);
extern void os_hw_cpu_dcache_enable(void);

/* dump 2nd level page table */
void os_hw_cpu_dump_page_table_2nd(uint32_t *ptb)
{
    int i;
    int fcnt = 0;

    for (i = 0; i < 256; i++)
    {
        uint32_t pte2 = ptb[i];
        if ((pte2 & 0x3) == 0)
        {
            if (fcnt == 0)
                os_kprintf("    ");
            os_kprintf("%04x: ", i);
            fcnt++;
            if (fcnt == 16)
            {
                os_kprintf("fault\n");
                fcnt = 0;
            }
            continue;
        }
        if (fcnt != 0)
        {
            os_kprintf("fault\n");
            fcnt = 0;
        }

        os_kprintf("    %04x: %x: ", i, pte2);
        if ((pte2 & 0x3) == 0x1)
        {
            os_kprintf("L,ap:%x,xn:%d,texcb:%02x\n",
                       ((pte2 >> 7) | (pte2 >> 4))& 0xf,
                       (pte2 >> 15) & 0x1,
                       ((pte2 >> 10) | (pte2 >> 2)) & 0x1f);
        }
        else
        {
            os_kprintf("S,ap:%x,xn:%d,texcb:%02x\n",
                       ((pte2 >> 7) | (pte2 >> 4))& 0xf, pte2 & 0x1,
                       ((pte2 >> 4) | (pte2 >> 2)) & 0x1f);
        }
    }
}

void os_hw_cpu_dump_page_table(uint32_t *ptb)
{
    int i;
    int fcnt = 0;

    os_kprintf("page table@%p\n", ptb);
    for (i = 0; i < 1024*4; i++)
    {
        uint32_t pte1 = ptb[i];
        if ((pte1 & 0x3) == 0)
        {
            os_kprintf("%03x: ", i);
            fcnt++;
            if (fcnt == 16)
            {
                os_kprintf("fault\n");
                fcnt = 0;
            }
            continue;
        }
        if (fcnt != 0)
        {
            os_kprintf("fault\n");
            fcnt = 0;
        }

        os_kprintf("%03x: %08x: ", i, pte1);
        if ((pte1 & 0x3) == 0x3)
        {
            os_kprintf("LPAE\n");
        }
        else if ((pte1 & 0x3) == 0x1)
        {
            os_kprintf("pte,ns:%d,domain:%d\n",
                       (pte1 >> 3) & 0x1, (pte1 >> 5) & 0xf);
            /*
             *os_hw_cpu_dump_page_table_2nd((void*)((pte1 & 0xfffffc000)
             *                               - 0x80000000 + 0xC0000000));
             */
        }
        else if (pte1 & (1 << 18))
        {
            os_kprintf("super section,ns:%d,ap:%x,xn:%d,texcb:%02x\n",
                       (pte1 >> 19) & 0x1,
                       ((pte1 >> 13) | (pte1 >> 10))& 0xf,
                       (pte1 >> 4) & 0x1,
                       ((pte1 >> 10) | (pte1 >> 2)) & 0x1f);
        }
        else
        {
            os_kprintf("section,ns:%d,ap:%x,"
                       "xn:%d,texcb:%02x,domain:%d\n",
                       (pte1 >> 19) & 0x1,
                       ((pte1 >> 13) | (pte1 >> 10))& 0xf,
                       (pte1 >> 4) & 0x1,
                       (((pte1 & (0x7 << 12)) >> 10) |
                       ((pte1 &        0x0c) >>  2)) & 0x1f,
                       (pte1 >> 5) & 0xf);
        }
    }
}

/* level1 page table, each entry for 1MB memory. */
volatile static unsigned long MMUTable[4 * 1024] __attribute__((aligned(16 * 1024)));
void os_hw_mmu_setmtt(uint32_t vaddrStart,
                      uint32_t vaddrEnd,
                      uint32_t paddrStart,
                      uint32_t attr)
{
    volatile uint32_t *pTT;
    volatile int i, nSec;
    pTT  = (uint32_t *)MMUTable + (vaddrStart >> 20);
    nSec = (vaddrEnd >> 20) - (vaddrStart >> 20);
    for(i = 0; i <= nSec; i++)
    {
        *pTT = attr | (((paddrStart >> 20) + i) << 20);
        pTT++;
    }
}

unsigned long os_hw_set_domain_register(unsigned long domain_val)
{
    unsigned long old_domain;

    asm volatile ("mrc p15, 0, %0, c3, c0\n" : "=r" (old_domain));
    asm volatile ("mcr p15, 0, %0, c3, c0\n" : :"r" (domain_val) : "memory");

    return old_domain;
}

/**
 ***********************************************************************************************************************
 * @brief           os_hw_init_mmu_table.
 *
 * @attention       This function will initialize mmu table.
 *
 * @param[in]       mdesc         The memory description.
 * @param[in]       size          The size of memory description.
 *
 * @return          None.
 ***********************************************************************************************************************
 */
void os_hw_init_mmu_table(struct mem_desc *mdesc, uint32_t size)
{
    /* set page table */
    for(; size > 0; size--)
    {
        os_hw_mmu_setmtt(mdesc->vaddr_start, mdesc->vaddr_end, mdesc->paddr_start, mdesc->attr);
        mdesc++;
    }
}

/**
 ***********************************************************************************************************************
 * @brief           os_hw_mmu_init.
 *
 * @attention       This function will initialize mmu.
 *
 * @param           None.
 *
 * @return          None.
 ***********************************************************************************************************************
 */
void os_hw_mmu_init(void)
{
    int cpu_id = 0;

    cpu_id = os_cpu_id_get();

    if (0 == cpu_id)
    {
        os_cpu_dcache_clean_flush();
        os_cpu_icache_flush();
    }

    os_hw_cpu_dcache_disable();
    os_hw_cpu_icache_disable();
    os_cpu_mmu_disable();

    /*os_hw_cpu_dump_page_table(MMUTable);*/
    os_hw_set_domain_register(0x55555555);

    os_cpu_tlb_set(MMUTable);

    os_cpu_mmu_enable();

    scu_enable();

    const unsigned int all_ways = 0xf;

    // Enable branch prediction
    arm_branch_target_cache_invalidate();
    arm_branch_prediction_enable();

    // Invalidate SCU copy of TAG RAMs
    scu_secure_invalidate(cpu_id, all_ways);

#ifdef OS_USING_SMP
    // Join SMP
    scu_join_smp();
#endif
    scu_enable_maintenance_broadcast();

    __asm volatile ("dsb sy":::"memory");
    __asm volatile ("isb sy":::"memory");

    os_hw_cpu_icache_enable();
    os_hw_cpu_dcache_enable();
}

