#ifndef __GIC_H__
#define __GIC_H__

#include <board.h>
#include <os_types.h>

int arm_gic_get_active_irq(uint32_t index);
void arm_gic_ack(uint32_t index, int irq);

void arm_gic_mask(uint32_t index, int irq);
void arm_gic_umask(uint32_t index, int irq);

uint32_t arm_gic_get_pending_irq(uint32_t index, int irq);
void arm_gic_set_pending_irq(uint32_t index, int irq);
void arm_gic_clear_pending_irq(uint32_t index, int irq);

void arm_gic_set_configuration(uint32_t index, int irq, uint32_t config);
uint32_t arm_gic_get_configuration(uint32_t index, int irq);

void arm_gic_clear_active(uint32_t index, int irq);

void arm_gic_set_cpu(uint32_t index, int irq, unsigned int cpumask);
uint32_t arm_gic_get_target_cpu(uint32_t index, int irq);

void arm_gic_set_priority(uint32_t index, int irq, uint32_t priority);
uint32_t arm_gic_get_priority(uint32_t index, int irq);

void arm_gic_set_interface_prior_mask(uint32_t index, uint32_t priority);
uint32_t arm_gic_get_interface_prior_mask(uint32_t index);

void arm_gic_set_binary_point(uint32_t index, uint32_t binary_point);
uint32_t arm_gic_get_binary_point(uint32_t index);

uint32_t arm_gic_get_irq_status(uint32_t index, int irq);

void arm_gic_send_sgi(uint32_t index, int irq, uint32_t target_list, uint32_t filter_list);

uint32_t arm_gic_get_high_pending_irq(uint32_t index);

uint32_t arm_gic_get_interface_id(uint32_t index);

void arm_gic_set_group(uint32_t index, int irq, uint32_t group);
uint32_t arm_gic_get_group(uint32_t index, int irq);

int arm_gic_dist_init(uint32_t index, uint32_t dist_base, int irq_start);
int arm_gic_cpu_init(uint32_t index, uint32_t cpu_base);

void arm_gic_dump_type(uint32_t index);
void arm_gic_dump(uint32_t index);

#endif

