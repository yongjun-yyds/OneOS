#ifndef __ARCH_TASK_H__
#define __ARCH_TASK_H__

#include <os_types.h>

#ifdef __cplusplus
    extern "C" {
#endif

#define OS_ARCH_STACK_ALIGN_SIZE   8

struct exc_stack_frame_common
{   
    uint32_t r4;
    uint32_t r5;
    uint32_t r6;
    uint32_t r7;
    uint32_t r8;
    uint32_t r9;
    uint32_t r10;
    uint32_t r11;
    uint32_t exc_return;
};

struct exc_stack_frame
{
    /* Push or pop stack manually */
    uint32_t cpsr;
    uint32_t r0;
    uint32_t r1;
    uint32_t r2;
    uint32_t r3;
    uint32_t r4;
    uint32_t r5;
    uint32_t r6;
    uint32_t r7;
    uint32_t r8;
    uint32_t r9;
    uint32_t r10;
    uint32_t r11;
    uint32_t r12;
    uint32_t lr;
    uint32_t pc;

};

struct exc_stack_frame_fpu
{
    /* begin: Push or pop stack manually */
    uint32_t r4;
    uint32_t r5;
    uint32_t r6;
    uint32_t r7;
    uint32_t r8;
    uint32_t r9;
    uint32_t r10;
    uint32_t r11;
    uint32_t exc_return;

    /* FPU registers */
    uint32_t s16;
    uint32_t s17;
    uint32_t s18;
    uint32_t s19;
    uint32_t s20;
    uint32_t s21;
    uint32_t s22;
    uint32_t s23;
    uint32_t s24;
    uint32_t s25;
    uint32_t s26;
    uint32_t s27;
    uint32_t s28;
    uint32_t s29;
    uint32_t s30;
    uint32_t s31;
    /* end: Push or pop stack manually */

    /* begin: Push or pop stack automatically */
    uint32_t r0;
    uint32_t r1;
    uint32_t r2;
    uint32_t r3;
    uint32_t r12;
    uint32_t lr;
    uint32_t pc;
    uint32_t psr;

    /* FPU registers */
    uint32_t S0;
    uint32_t S1;
    uint32_t S2;
    uint32_t S3;
    uint32_t S4;
    uint32_t S5;
    uint32_t S6;
    uint32_t S7;
    uint32_t S8;
    uint32_t S9;
    uint32_t S10;
    uint32_t S11;
    uint32_t S12;
    uint32_t S13;
    uint32_t S14;
    uint32_t S15;
    uint32_t FPSCR;
    uint32_t NO_NAME;
    /* end: Push or pop stack automatically */
};

extern void       *os_hw_stack_init(void        (*task_entry)(void *arg),
                                    void         *arg,
                                    void         *stack_begin,
                                    uint32_t   stack_size,
                                    void        (*task_exit)(void));

extern uint32_t os_hw_stack_max_used(void *stack_begin, uint32_t stack_size);

extern void        os_first_task_start(void);
extern void        os_task_switch(void);
extern void        os_int_task_switch(void *context);

#ifdef OS_USING_OVERFLOW_CHECK
extern os_bool_t os_task_stack_is_overflow(void *stack_top, void *stack_begin, void *stack_end);
#endif

#ifdef __cplusplus
    }
#endif

#endif /* __ARCH_TASK_H__ */

