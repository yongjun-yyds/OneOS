/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with 
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on 
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the 
 * specific language governing permissions and limitations under the License.
 *
 * @file        mmu.h
 *
 * @brief       Header file for mmu interface. 
 *
 * @revision
 * Date         Author          Notes
 * 2020-06-22   OneOS Team      First version.
 ***********************************************************************************************************************
 */
#ifndef __MMU_H_
#define __MMU_H_

#include <oneos_config.h>

#define DESC_SEC       (0x2)
#define MEMWBWA        ((1 << 12)|(3 << 2))     /* write back, write allocate */
#define MEMWB          (3 << 2)  /* write back, no write allocate */
#define MEMWT          (2 << 2)  /* write through, no write allocate */
#define SHAREDEVICE    (1 << 2)  /* shared device */
#define STRONGORDER    (0 << 2)  /* strong ordered */
#define XN             (1 << 4)  /* eXecute Never */
#define AP_RW          (3 << 10) /* supervisor=RW, user=RW */
#define AP_RO          (2 << 10) /* supervisor=RW, user=RO */
#define SHARED         (1 << 16) /* shareable */

#define DOMAIN_FAULT   (0x0)
#define DOMAIN_CHK     (0x1)
#define DOMAIN_NOTCHK  (0x3)
#define DOMAIN0        (0x0 << 5)
#define DOMAIN1        (0x1 << 5)

#define DOMAIN0_ATTR   (DOMAIN_CHK << 0)
#define DOMAIN1_ATTR   (DOMAIN_FAULT << 2)

/* device mapping type */
#define DEVICE_MEM     (SHARED | AP_RW | DOMAIN0 | SHAREDEVICE | DESC_SEC | XN)
/* normal memory mapping type */
#define NORMAL_MEM     (SHARED | AP_RW | DOMAIN0 | MEMWB | DESC_SEC)

struct mem_desc
{
    uint32_t vaddr_start;
    uint32_t vaddr_end;
    uint32_t paddr_start;
    uint32_t attr;
};



/* Secure + Global + Non-Shareable + AP(No access) + Domain 0 + Executable + strongly-ordered */
#define DES_SEC_BASE                        0x2

/* XN, The Execute-never bit. */
#define DES_SEC_XN                          (1<<4)

/* Domain field, n = 0~15 */
#define DES_SEC_DOMAIN(n)                   ((n)<<5)

/* Memory Type, TEX[2:0] C B (TEX, C, and B encodings when SCTLR.TRE == 0):
MT_STRONGLY_ORDERED:    Strongly-ordered
MT_SHARED_DEVICE:       Shareable Device
MT_NONSHARED_DEVICE:    Non-shareable Device
MT_NORMAL_NONCACHE:     Normal + Non-cacheable
MT_NORMAL_CACHE_WB:     Normal + Write-Back, Write-Allocate
MT_NORMAL_CACHE_WT:     Normal + Write-Through, no Write-Allocate
*/
#define DES_SEC_MT_STRONGLY_ORDERED         ((0<<12) | (0<<3) | (0<<2))
#define DES_SEC_MT_SHARED_DEVICE            ((0<<12) | (0<<3) | (1<<2))
#define DES_SEC_MT_NONSHARED_DEVICE         ((2<<12) | (0<<3) | (0<<2))
#define DES_SEC_MT_NORMAL_NONCACHE          ((1<<12) | (0<<3) | (0<<2))
#define DES_SEC_MT_NORMAL_CACHE_WB          ((1<<12) | (1<<3) | (1<<2))
#define DES_SEC_MT_NORMAL_CACHE_WT          ((0<<12) | (1<<3) | (0<<2))

/* Access Permissions, AP[2:0], (When SCTLR.AFE == 0):
   000      PL1, No access      PL0, No access
   001      PL1, Full access    PL0, No access
   010      PL1, Full access    PL0, Read-Only
   011      PL1, Full access    PL0, Full access
   101      PL1, Read-only      PL0, No access

   KRM - kernel mode read write ( user mode no access )
   URW - user read write ( kernel mode read write )
*/
#define DES_SEC_AP_KRW                      ((0<<15) | (1<<10))
#define DES_SEC_AP_KRO                      ((1<<15) | (1<<10))
#define DES_SEC_AP_URW                      ((0<<15) | (3<<10))
#define DES_SEC_AP_URO                      ((0<<15) | (2<<10))

/* S, The Shareable bit */
#define DES_SEC_S                           (1<<16)

/* nG, The not global bit. */
#define DES_SEC_nG                          (1<<17)

/* NS, Non-secure bit. */
#define DES_SEC_NS                          (1<<19)

/********** mem_attr for Normal memory (e.g. ddr, sram ...) **********/
/* Secure + Global + Non-Shareable + Domain 0 + Executable + Cache write back */
#define MMU_ATTR_SEC_X_WB    \
        (DES_SEC_BASE | DES_SEC_MT_NORMAL_CACHE_WB)
/* Secure + Global + Non-Shareable + Domain 0 + Non-Executable + Cache write back */
#define MMU_ATTR_SEC_NX_WB   \
        (DES_SEC_BASE | DES_SEC_XN | DES_SEC_MT_NORMAL_CACHE_WB)
/* Secure + Global + Non-Shareable + Domain 0 + Executable + Cache Write through */
#define MMU_ATTR_SEC_X_WT    \
        (DES_SEC_BASE | DES_SEC_MT_NORMAL_CACHE_WT)
/* Secure + Global + Non-Shareable + Domain 0 + Non-Executable + Cache Write through */
#define MMU_ATTR_SEC_NX_WT    \
        (DES_SEC_BASE | DES_SEC_XN  | DES_SEC_MT_NORMAL_CACHE_WT)
/* Secure + Global + Non-Shareable + Domain 0 + Executable + Non-cacheable */
#define MMU_ATTR_SEC_X_NC    \
        (DES_SEC_BASE | DES_SEC_MT_NORMAL_NONCACHE)
/* Secure + Global + Non-Shareable + Domain 0 + Non-Executable + Non-cacheable */
#define MMU_ATTR_SEC_NX_NC    \
        (DES_SEC_BASE | DES_SEC_XN | DES_SEC_MT_NORMAL_NONCACHE)
/*******************************************************/

/********** mem_attr for MMIO (e.g. regs, dma ...) **********/
/* Secure + Global + Domain 0 + Non-Executable + Strongly-ordered */
#define MMU_ATTR_SEC_NX_SO   \
        (DES_SEC_BASE | DES_SEC_XN | DES_SEC_MT_STRONGLY_ORDERED)
/* Secure + Global + Domain 0 + Non-Executable + Shareable Device */
#define MMU_ATTR_SEC_NX_DEV   \
        (DES_SEC_BASE | DES_SEC_XN | DES_SEC_MT_SHARED_DEVICE)
/* Secure + Global + Domain 0 + Non-Executable + Non-shareable Device */
#define MMU_ATTR_SEC_NX_DEV_NSH   \
        (DES_SEC_BASE | DES_SEC_XN | DES_SEC_MT_NONSHARED_DEVICE)
/*******************************************************/

/******************** mmu_mem_attr_t for app: ***********************/
typedef enum {
    /********** mem_attr for MMIO (e.g. regs, dma ...) **********/
    /* Strongly-ordered MMIO
       Non-Executable + Full access + Non-cacheable */
    MMU_ATTR_SO,
    /* Shareable Device MMIO
       Non-Executable + Full access + Non-cacheable */
    MMU_ATTR_DEV,
    /* Non-Shareable Device MMIO
       Non-Executable + Full access + Non-cacheable */
    MMU_ATTR_DEV_NSH,
    /* Non-cacheable Memory (DMA)
       Non-Executable + Full access + Non-cacheable */
    MMU_ATTR_NON_CACHE,

    /********** mem_attr for Normal memory (e.g. ddr, sram ...) **********/
    /* Normal memory for text / rodata
       Executable + Read-only + Cache write back */
    MMU_ATTR_EXEC,
    /* Normal memory for data / bss
       Non-Executable + Read-Write + Cache write back */
    MMU_ATTR_DATA,
    /* Normal memory for read-only data
       Non-Executable + Read-only + Cache write back */
    MMU_ATTR_DATA_RO,
    /* Normal memory for all (mixing text and data)
       Executable + Read-Write + Cache write back */
    MMU_ATTR_EXEC_DATA,

    /********** mem_attr for Normal memory in multicore system  **********/
    /* in multicore system, when ACTLR.SMP == 1,
       mem_attr should be with "shareable", for Cache coherency */
    /* Normal memory for text / rodata, shareable
       Executable + Read-only + Cache write back */
    MMU_ATTR_EXEC_SH,
    /* Normal memory for data / bss, shareable
       Non-Executable + Read-Write + Cache write back */
    MMU_ATTR_DATA_SH,
    /* Normal memory for read-only data, shareable
       Non-Executable + Read-only + Cache write back */
    MMU_ATTR_DATA_RO_SH,
    /* Normal memory for all (mixing text and data), shareable
       Executable + Read-Write + Cache write back */
    MMU_ATTR_EXEC_DATA_SH,

    /* memory for More combinations, add below */

    MMU_ATTR_BUTT
} mmu_mem_attr_t;
#endif
