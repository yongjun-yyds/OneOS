#ifndef __ARCH_BARRIER_H__
#define __ARCH_BARRIER_H__

#include <os_types.h>
#include <os_stddef.h>

#ifdef __cplusplus
    extern "C" {
#endif

#define barrier()   __asm__ __volatile__("": : :"memory")

#define isb(option) __asm__ __volatile__ ("isb " #option : : : "memory")
#define dsb(option) __asm__ __volatile__ ("dsb " #option : : : "memory")
#define dmb(option) __asm__ __volatile__ ("dmb " #option : : : "memory")

#define smp_mb()    dmb(ish)
#define smp_rmb()   smp_mb()
#define smp_wmb()   dmb(ishst)

#define __dmb()     smp_mb()
#define __dsb()     dsb(ishst)
#define dsb_sev()   __asm__ __volatile__ ("dsb ishst\nsev" : : : "memory")

#define wfe()   __asm__ __volatile__ ("wfe" : : : "memory")
#define wfi()   __asm__ __volatile__ ("wfi" : : : "memory")

#ifdef __cplusplus
    }
#endif

#endif /* __ARCH_MISC_H__ */

