#ifndef __OS_ARCH_SPINLOCK_H__
#define __OS_ARCH_SPINLOCK_H__

#include <oneos_config.h>
#include <os_types.h>
#include <os_stddef.h>
#include <os_assert.h>
#include <arch_barrier.h>


#ifdef __cplusplus
extern "C" {
#endif
typedef long    atomic_t;

#define SPINLOCK_ARCH_UNLOCKED 0

#define OS_USING_TICKET_SPINLOCK

#ifdef OS_USING_TICKET_SPINLOCK

#define TICKET_SHIFT    16
#define SPINLOCK_ARCH_LOCKED (1 << TICKET_SHIFT)


//#define __ARMEB__         /*  big endian enable. */

typedef struct 
{
    union 
    {
        atomic_t slock;
        struct __raw_tickets 
        {
#ifdef __ARMEB__
            uint16_t next;
            uint16_t owner;
#else
            uint16_t owner;
            uint16_t next;
#endif
        }tickets;
    };
}arch_spinlock_t;

#else
#define SPINLOCK_ARCH_LOCKED (1)
typedef struct 
{
    atomic_t slock;
}arch_spinlock_t;

#endif

#define SPINLOCK_ARCH_UNLOCKED 0
    
#define ARCH_SPINLOCK_INIT_VALUE(val)                                                                                  \
    {                                                                                                                  \
        .slock = (val)                                                                                                 \
    }


extern int os_atomic_cas(atomic_t *target, int old_value, int new_value);
extern atomic_t os_atomic_get   (atomic_t * target);
extern atomic_t os_atomic_set   (atomic_t * target, int value);

static void arch_spin_lock_init(arch_spinlock_t *lock)
{
    OS_ASSERT(lock);

    lock->slock = SPINLOCK_ARCH_UNLOCKED;

    return;
}

/**
 ***********************************************************************************************************************
 * @brief           This function implement Test-and-set spin-locking.
 *
 * @details         Both control block and stack of the task are allocated in memory heap.
 *
 * @attention       A memory barrier is required after we get a lock, and before we
 *                  release it, because CPUs are assumed to have weakly ordered
 *                  memory.
 *
 * @param[in]       lock            spinlock.
 * 
 * @return          None.
 *
 ***********************************************************************************************************************
 */
OS_INLINE void arch_spin_lock(arch_spinlock_t *lock)
{
#ifdef OS_USING_TICKET_SPINLOCK

    unsigned long tmp;
    uint32_t newval;
    arch_spinlock_t lockval;

    //prefetchw(&lock->slock);
    __asm__ __volatile__(
    "1: ldrex   %0, [%3]\n"
    "   add %1, %0, %4\n"
    "   strex   %2, %1, [%3]\n"
    "   teq %2, #0\n"
    "   bne 1b"
        : "=&r" (lockval), "=&r" (newval), "=&r" (tmp)
        : "r" (&lock->slock), "I" (1 << TICKET_SHIFT)
        : "cc");

    while (lockval.tickets.next != lockval.tickets.owner) {
        wfe();
        lockval.tickets.owner = *(volatile unsigned short *)(&lock->tickets.owner);
    }

    smp_mb();
#else

    while (!os_atomic_cas(&lock->slock, 0, 1)) 
    {
        ;
    }

    smp_mb();

#endif
}

OS_INLINE int arch_spin_trylock(arch_spinlock_t *lock)
{
#ifdef OS_USING_TICKET_SPINLOCK

    unsigned long contended, res;
    uint32_t slock;

    //prefetchw(&lock->slock);
    do {
        __asm__ __volatile__(
        "   ldrex   %0, [%3]\n"
        "   mov %2, #0\n"
        "   subs    %1, %0, %0, ror #16\n"
        "   addeq   %0, %0, %4\n"
        "   strexeq %2, %0, [%3]"
        : "=&r" (slock), "=&r" (contended), "=&r" (res)
        : "r" (&lock->slock), "I" (1 << TICKET_SHIFT)
        : "cc");
    } while (res);

    if (!contended) {
        smp_mb();
        return 1;
    } else {
        return 0;
    }

#else
    
    int ret = os_atomic_cas(&lock->slock, 0, 1);

    smp_mb();

    return ret;

#endif

}


/**
 ***********************************************************************************************************************
 * @brief           This function implement Test-and-set spin-locking.
 *
 * @details         Both control block and stack of the task are allocated in memory heap.
 *
 * @attention       A memory barrier is required after we get a lock, and before we
 *                  release it, because CPUs are assumed to have weakly ordered
 *                  memory.
 *
 * @param[in]       lock            spinlock.
 * 
 * @return          None.
 *
 ***********************************************************************************************************************
 */
OS_INLINE void arch_spin_unlock(arch_spinlock_t *lock)
{
#ifdef OS_USING_TICKET_SPINLOCK

    smp_mb();
    lock->tickets.owner++;
    dsb_sev();
    
#else
        
    smp_mb();
    
    lock->slock = 0;

    __dsb();

#endif
}

#ifdef __cplusplus
}
#endif

#endif /* __OS_ARCH_SPINLOCK_H__ */

