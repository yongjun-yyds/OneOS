#ifndef __ARCH_SMP_H__
#define __ARCH_SMP_H__

/*Inter-Processor Interrupts*/
#define IPI_ID_SCHED 	0

void arch_enable_cpu(int cpu_index,void (*smp_entry)(void *arg));
void arch_cpu_ipi(int cpu_index, int ipi_id);

#endif /* __ARCH_SMP_H__ */

