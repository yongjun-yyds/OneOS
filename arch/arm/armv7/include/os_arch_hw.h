#ifndef __OS_ARCH_HW_H__
#define __OS_ARCH_HW_H__

#include <os_types.h>
#include <os_task.h>

extern os_ubase_t os_arch_irq_lock(void);
extern void       os_arch_irq_unlock(os_ubase_t irq_save);

extern os_bool_t  os_arch_is_irq_active(void);
extern os_bool_t  os_arch_is_irq_disabled(void);

extern void      *os_arch_hw_stack_init(void        (*task_entry)(void *arg),
                                        void         *arg,
                                        void         *stack_begin,
                                        uint32_t   stack_size,
                                        void        (*task_exit)(void));

extern void       os_arch_first_task_start(void);
extern void       os_arch_task_switch(void);

extern int32_t os_ffs(uint32_t value);

#define OS_ARCH_STACK_ALIGN_SIZE   8

#endif /* __OS_ARCH_HW_H__ */

