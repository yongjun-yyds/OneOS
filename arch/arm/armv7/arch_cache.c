#include <oneos_config.h>
#include <os_stddef.h>
#include <arch_cache.h>

OS_INLINE uint32_t os_cpu_icache_line_size(void)
{
    uint32_t ctr;
    asm volatile ("mrc p15, 0, %0, c0, c0, 1" : "=r"(ctr));
    return 4 << (ctr & 0xF);
}

OS_INLINE uint32_t os_cpu_dcache_line_size(void)
{
    uint32_t ctr;
    asm volatile ("mrc p15, 0, %0, c0, c0, 1" : "=r"(ctr));
    return 4 << ((ctr >> 16) & 0xF);
}

void os_hw_cpu_icache_invalidate(void *addr, int size)
{
    uint32_t line_size  = os_cpu_icache_line_size();
    uint32_t start_addr = (uint32_t)addr;
    uint32_t end_addr   = (uint32_t) addr + size + line_size - 1;

    asm volatile ("dmb":::"memory");
    start_addr &= ~(line_size-1);
    end_addr   &= ~(line_size-1);
    while (start_addr < end_addr)
    {
        asm volatile ("mcr p15, 0, %0, c7, c5, 1" :: "r"(start_addr));
        start_addr += line_size;
    }
    asm volatile ("dsb\n\tisb":::"memory");
}

void os_hw_cpu_dcache_invalidate(void *addr, int size)
{
    uint32_t line_size  = os_cpu_dcache_line_size();
    uint32_t start_addr = (uint32_t)addr;
    uint32_t end_addr   = (uint32_t) addr + size + line_size - 1;

    asm volatile ("dmb":::"memory");
    start_addr &= ~(line_size-1);
    end_addr   &= ~(line_size-1);
    while (start_addr < end_addr)
    {
        asm volatile ("mcr p15, 0, %0, c7, c6, 1" :: "r"(start_addr));
        start_addr += line_size;
    }
    asm volatile ("dsb":::"memory");
}

void os_hw_cpu_dcache_clean(void *addr, int size)
{
    uint32_t line_size  = os_cpu_dcache_line_size();
    uint32_t start_addr = (uint32_t)addr;
    uint32_t end_addr   = (uint32_t) addr + size + line_size - 1;

    asm volatile ("dmb":::"memory");
    start_addr &= ~(line_size-1);
    end_addr   &= ~(line_size-1);
    while (start_addr < end_addr)
    {
        asm volatile ("mcr p15, 0, %0, c7, c10, 1" :: "r"(start_addr)); /* dccmvac */
        start_addr += line_size;
    }
    asm volatile ("dsb":::"memory");
}

void os_hw_cpu_icache_ops(int ops, void *addr, int size)
{
    if (ops == OS_HW_CACHE_INVALIDATE)
    {
        os_hw_cpu_icache_invalidate(addr, size);
    }
}

void os_hw_cpu_dcache_ops(int ops, void *addr, int size)
{
    if (ops == OS_HW_CACHE_FLUSH)
    {
        os_hw_cpu_dcache_clean(addr, size);
    }
    else if (ops == OS_HW_CACHE_INVALIDATE)
    {
        os_hw_cpu_dcache_invalidate(addr, size);
    }
}

os_base_t os_hw_cpu_icache_status(void)
{
    return 0;
}

os_base_t os_hw_cpu_dcache_status(void)
{
    return 0;
}
