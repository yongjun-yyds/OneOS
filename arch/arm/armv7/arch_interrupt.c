/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with 
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on 
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the 
 * specific language governing permissions and limitations under the License.
 *
 * @file        cpuport.c
 *
 * @brief       This file provides functions related to the ARM M4 architecture.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-23   OneOS Team      First version.
 ***********************************************************************************************************************
 */
#include <oneos_config.h>
#include <os_types.h>
#include <os_stddef.h>
#include <arch_misc.h>

#ifdef OS_USING_SMP
int32_t g_os_int_nest_cnt[OS_SMP_MAX_CPUS]    = {0};
#else
int32_t g_os_int_nest_cnt = 0;
#endif

os_ubase_t os_irq_lock(void)
{
    os_ubase_t cpsr;

    __asm__ __volatile__(
        "MRS     %0, CPSR\n"
        "CPSID   I"
        : "=r"(cpsr)
        : 
        : "memory");

    return cpsr;
}

void os_irq_unlock(os_ubase_t irq_save)
{
    __asm__ __volatile__(
        "MSR     CPSR_c, %0"
        : 
        : "r"(irq_save)
        : "memory");

    return;

}



void os_irq_enable(void)
{
	__asm__ __volatile__(
		"	cpsie i "
		:
		:
		: "memory" );
}
void os_irq_disable(void)
{
	__asm__ __volatile__(
		"	cpsid i "
		:
		:
		: "memory" );
}

os_bool_t os_is_irq_active(void)
{
#ifdef OS_USING_SMP
    int32_t current_cpu_index;
    
    current_cpu_index = os_cpu_id_get();

    return g_os_int_nest_cnt[current_cpu_index] > 0 ? 1 : 0;
#else
    return g_os_int_nest_cnt > 0 ? 1 : 0;
#endif
}

os_bool_t os_is_irq_disabled(void)
{
    os_bool_t state;

    __asm__ volatile("mrs %0, cpsr" : "=r"(state));
    state &= (1 << 7);
    return !!state;
}

uint32_t os_irq_num(void)
{
    extern int os_hw_interrupt_get_irq(void);
    
    return os_hw_interrupt_get_irq();
}


#ifdef OS_USING_SMP
void k_exit_int(void *context);

void os_int_enter(void)
{
    g_os_int_nest_cnt[os_cpu_id_get()]++;
    return;
}

void os_int_exit(void *context)
{
    int32_t current_cpu_index;

    current_cpu_index = os_cpu_id_get();
    --g_os_int_nest_cnt[current_cpu_index];
    if (g_os_int_nest_cnt[current_cpu_index] == 0)
    {
        k_exit_int(context);
    }

    return;
}

#else
extern uint32_t g_os_task_switch_interrupt_flag;
extern void     os_int_task_switch(void *context);

void os_int_enter(void)
{
    g_os_int_nest_cnt++;
    
    return;
}

void os_int_exit(void *context)
{
    if (0 == --g_os_int_nest_cnt)
    {
        if(g_os_task_switch_interrupt_flag == 1)
        {
            g_os_task_switch_interrupt_flag = 0;
            os_int_task_switch(context);
        }
    }

    return;
}
#endif


