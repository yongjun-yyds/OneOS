#include <os_types.h>
#include <os_task.h>
#include <os_util.h>
#include <arch_misc.h>
#include <os_safety.h>
#include <../../../kernel/source/os_prototypes.h>


void x_dump_stack(uint32_t stack_start_addr, uint32_t stack_size, os_size_t *stack_pointer) 
{    
    os_size_t *sp;
    
    os_kprintf("=======================   Stack info  =====================\r\n");
    for (sp = stack_pointer; (uint32_t) sp < stack_start_addr + stack_size; sp++) 
    {
        os_kprintf("        addr: 0x%08x      data: %08x\r\n", sp, *sp);
    }
    
    os_kprintf("-------------------------------------------------------------\r\n");    
}

void exc_analysis(void)
{
    os_ubase_t val;
    uint32_t stack_start_addr;
    uint32_t stack_size;
    os_size_t *stack_pointer;
    #if 0
    os_task_t *current_task;

    
    void task_switch_info_show(uint32_t context);
    task_switch_info_show(0);
    

    current_task = (os_task_t * )os_get_current_task();
    stack_size = os_hw_stack_max_used(current_task->stack_begin, 
                       (uint32_t)current_task->stack_end - (uint32_t)current_task->stack_begin);
    stack_start_addr = (uint32_t)current_task->stack_end - stack_size; 
    stack_pointer = (os_size_t *)stack_start_addr;
    x_dump_stack(stack_start_addr, stack_size, stack_pointer);
    
    os_kprintf("os_is_irq_active is %d\r\n",os_is_irq_active());

    os_kprintf("os_task_self is %s\r\n",current_task->name);
    os_kprintf("os_cpu_id_get is %d\r\n",os_cpu_id_get());
    #endif
    
    __asm__ volatile ( "mrc p15, 0, %0, c5, c0, 0" : "=r"(val)); // data fault status 
    os_kprintf("%s:%d data fault status = %#x\r\n", __FUNCTION__, __LINE__, val);

    __asm__ volatile ( "mrc p15, 0, %0, c5, c0, 1" : "=r"(val)); // instruction fault status 
    os_kprintf("%s:%d instruction fault status = %#x\r\n", __FUNCTION__, __LINE__, val);
    
    __asm__ volatile ( "mrc p15, 0, %0, c5, c1, 0" : "=r"(val)); // aux data fault status 
    os_kprintf("%s:%d aux data fault status = %#x\r\n", __FUNCTION__, __LINE__, val); 
    
    __asm__ volatile ( "mrc p15, 0, %0, c5, c1, 1" : "=r"(val)); // aux instruction fault status 
    os_kprintf("%s:%d aux instruction fault status = %#x\r\n", __FUNCTION__, __LINE__, val); 
    
    __asm__ volatile ( "mrc p15, 0, %0, c6, c0, 0" : "=r"(val)); // data fault address 
    os_kprintf("%s:%d data fault addr = %#x\r\n", __FUNCTION__, __LINE__, val); 
    
    __asm__ volatile ( "mrc p15, 0, %0, c6, c0, 2" : "=r"(val)); // instruction fault address 
    os_kprintf("%s:%d int fault addr = %#x\r\n", __FUNCTION__, __LINE__, val); 

    os_safety_exception_process();
}

