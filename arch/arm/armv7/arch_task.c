#include <os_types.h>
#include <os_task.h>
#include <os_assert.h>
#include <arch_task.h>
#include <os_arch_hw.h>
#include <string.h>

/* Stack down */
void *os_hw_stack_init(void        (*task_entry)(void *arg),
                            void         *arg,
                            void         *stack_begin,
                            uint32_t   stack_size,
                            void        (*task_exit)(void))
{
    struct exc_stack_frame *stack_frame;
    uint8_t             *stack_top;
    uint32_t             index;

    memset(stack_begin, '$', stack_size);

    stack_top  = (uint8_t *)stack_begin + stack_size;
    /*Compatible with old interfaces*/
    stack_top  = (uint8_t *)OS_ALIGN_DOWN((uint32_t)stack_top, OS_ARCH_STACK_ALIGN_SIZE);
    stack_top -= sizeof(struct exc_stack_frame);

    /* TODO: Need to check stack size */

    stack_frame = (struct exc_stack_frame *)stack_top;

    /* Initialize all registers */
    for (index = 0; index < sizeof(struct exc_stack_frame) / sizeof(uint32_t); index++)
    {
        ((uint32_t *)stack_frame)[index] = 0xDEADBEEF;
    }

    stack_frame->r0  = (uint32_t)arg;            /* r0 : argument */
    stack_frame->r1  = 0;                           /* r1 */
    stack_frame->r2  = 0;                           /* r2 */
    stack_frame->r3  = 0;                           /* r3 */
    stack_frame->r12 = 0;                           /* r12 */
    stack_frame->lr  = (uint32_t)task_exit;      /* lr */
    stack_frame->pc  = (uint32_t)task_entry;     /* entry point, pc */

    if ((os_ubase_t)task_entry & 0x01)
    {
        stack_frame->cpsr = 0x13 | 0x20;          /* thumb mode */
    }
    else
    {
        stack_frame->cpsr = 0x13;          /* arm mode */
    }
    
    return (void *)stack_top;
}

uint32_t os_hw_stack_max_used(void *stack_begin, uint32_t stack_size)
{
    uint8_t *addr;
    uint32_t max_used;

    addr = (uint8_t *)stack_begin;
    while (*addr == '$')
    {
        addr++;
    }

    max_used = (uint32_t)((os_ubase_t)stack_begin + stack_size - (os_ubase_t)addr);

    return max_used;
}

#ifndef OS_USING_SMP

uint32_t g_os_task_switch_interrupt_flag = 0;
extern int32_t g_os_int_nest_cnt;

void os_task_switch(void)
{
    if (g_os_int_nest_cnt == 0)
    {
        extern void os_hw_context_switch_from_task(void);
        os_hw_context_switch_from_task();
    }
    else
    {
        g_os_task_switch_interrupt_flag = 1;
    }
}

#endif

#ifdef OS_USING_OVERFLOW_CHECK
/**
 ***********************************************************************************************************************
 * @brief           This function is used to check the stack of task is overflow or not.
 *
 * @param[in]       task            The descriptor of task control block
 *
 * @return          Whether the stack of this task overflows.
 * @retval          OS_TRUE         The stack of this task is overflow.
 * @retval          OS_FALSE        The stack of this task is not overflow.
 ***********************************************************************************************************************
 */
os_bool_t os_task_stack_is_overflow(void *stack_top, void *stack_begin, void *stack_end)
{
    os_bool_t is_overflow;

    OS_ASSERT(OS_NULL != stack_top);
    OS_ASSERT(OS_NULL != stack_begin);
    OS_ASSERT(OS_NULL != stack_end);

    if ((*((uint8_t *)stack_begin) != '$') || ((os_ubase_t)stack_top < (os_ubase_t)stack_begin) ||
        ((os_ubase_t)stack_top >= (os_ubase_t)stack_end))
    {
        is_overflow = OS_TRUE;
    }
    else
    {
        is_overflow = OS_FALSE;
    }

    return is_overflow;
}
#endif /* OS_USING_OVERFLOW_CHECK */

