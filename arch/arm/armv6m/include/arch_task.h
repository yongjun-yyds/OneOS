/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        arch_task.h
 *
 * @brief       Header file for task stack functions.
 *
 * @revision
 * Date         Author          Notes
 * 2021-10-22   OneOS Team      First version.
 ***********************************************************************************************************************
 */

#ifndef __ARCH_TASK_H__
#define __ARCH_TASK_H__

#include <os_types.h>

#ifdef __cplusplus
extern "C" {
#endif

#define OS_ARCH_STACK_ALIGN_SIZE 8

struct cpu_stack_frame
{
    uint32_t r0;
    uint32_t r1;
    uint32_t r2;
    uint32_t r3;
    uint32_t r12;
    uint32_t lr;
    uint32_t pc;
    uint32_t psr;
};

struct manual_stack_frame
{
    uint32_t r4;
    uint32_t r5;
    uint32_t r6;
    uint32_t r7;
    uint32_t r8;
    uint32_t r9;
    uint32_t r10;
    uint32_t r11;
    uint32_t exc_return;
};

struct stack_frame
{
    /* Push or pop stack manually */
    struct manual_stack_frame manual_frame;

    /* Push or pop stack automatically */
    struct cpu_stack_frame cpu_frame;
};

extern void *os_hw_stack_init(void (*task_entry)(void *arg),
                              void       *arg,
                              void       *stack_begin,
                              uint32_t stack_size,
                              void (*task_exit)(void));

extern uint32_t os_hw_stack_max_used(void *stack_begin, uint32_t stack_size);

extern void os_first_task_start(void);
extern void os_task_switch(void);

#ifdef OS_USING_OVERFLOW_CHECK
extern os_bool_t os_task_stack_is_overflow(void *stack_top, void *stack_begin, void *stack_end);
#endif

#ifdef __cplusplus
}
#endif

#endif /* __ARCH_TASK_H__ */
