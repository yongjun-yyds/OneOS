 /**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with 
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on 
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the 
 * specific language governing permissions and limitations under the License.
 *
 * @file        arch_task_switch_gcc.S
 *
 * @brief       This file provides context switch functions related to the ARMv5te architecture, 
 *              and the compiler uses gcc.
 *
 * @revision
 * Date         Author          Notes
 * 2021-10-22   OneOS Team      First version.
 ***********************************************************************************************************************
 */

.extern g_os_current_task
.extern g_os_next_task
.extern os_task_switch_interrupt_flag
.extern os_task_switch_notify

.global os_task_switch
.type   os_task_switch, %function
.global os_first_task_start
.type   os_first_task_start, %function

/**
 ***********************************************************************************************************************
 * @brief           This function is called when the scheduler starts the first task, Only used once.
 *
 * @param           None.
 *
 * @return          None.
 ***********************************************************************************************************************
 */
os_first_task_start:
    /*disable interrupt*/
    MRS     R0, CPSR
    ORR     R0, R0, #0xc0
    MSR     CPSR_c, R0
    /* save g_os_next_task to g_os_current_task */
    LDR     R2, =g_os_next_task                    /* R2 = &g_os_next_task */
    LDR     R2, [R2]                               /* R2 = g_os_next_task */
    LDR     R1, =g_os_current_task                 /* R1 = &gs_os_current_task */
    STR     R2, [R1]                               /* g_os_current_task = g_os_next_task */
    /* pick up g_os_next_task->stack_top to sp */
    LDR     SP, [R2, #0]                           /* SP = g_os_next_task->stack_top; Pickup task stack pointer */
    /* restore CPSR and r0-r15 registers */
    LDMFD   SP!, {R4}                              @; pop new task cpsr
    MSR     SPSR_cxsf, R4
    LDMFD   SP!, {R0-R12, LR, PC}^                 @; pop new task r0-r12, lr & pc

/**
 ***********************************************************************************************************************
 * @brief           Start the task swtich process.
 *
 * @param           None.
 *
 * @return          None.
 ***********************************************************************************************************************
 */
os_task_switch:
    /* save CPSR and r0-r15 registers */
    STMFD   SP!, {LR}                              @; push pc (lr should be pushed in place of pc)
    STMFD   SP!, {R0-R12, LR}                      @; push lr and registers
    /* Check if into irq context */
    MRS     R1, CPSR
    AND     R1, R1, #0x1f
    CMP     R1, #0x11                              /* into the FIQ mode */
    BEQ     irq_context_switch
    CMP     R1, #0x12                              /* into the IRQ mode */
    BEQ     irq_context_switch

    MRS     R4, CPSR
    TST     LR, #0x01
    BEQ     _ARM_MODE
    ORR     R4, R4, #0x20                          @; it's thumb code set T=1
_ARM_MODE:
    STMFD   SP!, {R4}                              @; push cpsr
    /* save sp to g_os_current_task->stack_top */
    LDR     R1, =g_os_current_task                 /* R1 = &gs_os_current_task */
    LDR     R0, [R1]                               /* R0 = gs_os_current_task */
    STR     SP, [R0, #0]                           /* Update g_os_current_task->stack_top */

    #if defined (OS_TASK_SWITCH_NOTIFY)
    BL    os_task_switch_notify
    #endif

switch_to_task:
    /* save g_os_next_task to g_os_current_task */
    LDR     R2, =g_os_next_task                    /* R2 = &g_os_next_task */
    LDR     R2, [R2]                               /* R2 = g_os_next_task */
    LDR     R1, =g_os_current_task                 /* R1 = &gs_os_current_task */
    STR     R2, [R1]                               /* g_os_current_task = g_os_next_task */
    /* pick up g_os_next_task->stack_top to sp */
    LDR     SP, [R2, #0]                           /* SP = g_os_next_task->stack_top; Pickup task stack pointer */
    /* restore CPSR and r0-r15 registers */
    LDMFD   SP!, {R4}                              @; pop new task cpsr
    MSR     SPSR_cxsf, R4
    LDMFD   SP!, {R0-R12, LR, PC}^                 @; pop new task r0-r12, lr & pc

irq_context_switch:
    /* if switch task in irq context, only set switch_interrupt_flag then return */
    LDR     R2, =os_task_switch_interrupt_flag
    MOV     R3, #1
    STR     R3, [R2]
    /* restore r0-r15 registers */
    LDMFD   SP!, {R0-R12, LR, PC}                  @; pop r0-r12, lr & pc without changing CPSR

