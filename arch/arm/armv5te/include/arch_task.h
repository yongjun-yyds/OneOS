/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        arch_task.h
 *
 * @brief       Header file for task stack functions.
 *
 * @revision
 * Date         Author          Notes
 * 2021-10-22   OneOS Team      First version.
 ***********************************************************************************************************************
 */

#ifndef __ARCH_TASK_H__
#define __ARCH_TASK_H__

#include <os_types.h>

#ifdef __cplusplus
extern "C" {
#endif

#define OS_ARCH_STACK_ALIGN_SIZE 8
/*****************************/
/* CPU Mode                  */
/*****************************/
#define USERMODE  0x10
#define FIQMODE   0x11
#define IRQMODE   0x12
#define SVCMODE   0x13
#define ABORTMODE 0x17
#define UNDEFMODE 0x1b
#define MODEMASK  0x1f
#define NOINT     0xc0

struct stack_frame
{
    os_ubase_t cpsr; /* CPSR       - current program status register */
    os_ubase_t r0;   /* R0 - A1    - return value or function argument 1 */
    os_ubase_t r1;   /* R1 - A2    - return value or function argument 2 */
    os_ubase_t r2;   /* R2 - A3    - return value or function argument 3 */
    os_ubase_t r3;   /* R3 - A4    - return value or function argument 4 */
    os_ubase_t r4;   /* R4 - V1    - Local variable register 1 */
    os_ubase_t r5;   /* R5 - V2    - Local variable register 2 */
    os_ubase_t r6;   /* R6 - V3    - Local variable register 3 */
    os_ubase_t r7;   /* R7 - V4    - Local variable register 4 */
    os_ubase_t r8;   /* R8 - V5    - Local variable register 5 */
    os_ubase_t r9;   /* R9 - V6    - Local variable register 6 */
    os_ubase_t r10;  /* R10 - V7   - Local variable register 7 */
    os_ubase_t r11;  /* R11 - V8   - Local variable register 8 */
    os_ubase_t r12;  /* R12 - ip */
    /* R13 == sp, needn`t to save it */
    os_ubase_t r14; /* R14 - lr */
    os_ubase_t r15; /* R15 - pc */
};

extern void *os_hw_stack_init(void (*task_entry)(void *arg),
                              void       *arg,
                              void       *stack_begin,
                              uint32_t stack_size,
                              void (*task_exit)(void));

extern uint32_t os_hw_stack_max_used(void *stack_begin, uint32_t stack_size);

extern void os_first_task_start(void);
extern void os_task_switch(void);

#ifdef OS_USING_OVERFLOW_CHECK
extern os_bool_t os_task_stack_is_overflow(void *stack_top, void *stack_begin, void *stack_end);
#endif

#ifdef __cplusplus
}
#endif

#endif /* __ARCH_TASK_H__ */
