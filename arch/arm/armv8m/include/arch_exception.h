/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with 
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on 
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the 
 * specific language governing permissions and limitations under the License.
 *
 * @file        arch_exception.h
 *
 * @brief       Header file for exception functions.
 *
 * @revision
 * Date         Author          Notes
 * 2021-10-22   OneOS Team      First version.
 ***********************************************************************************************************************
 */

#ifndef __OS_ARCH_EXCEPTION_H__
#define __OS_ARCH_EXCEPTION_H__

#include <os_types.h>
#include <oneos_config.h>
#include <os_task.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifdef STACK_TRACE_EN
extern os_err_t task_stack_show(os_task_id task, uint32_t context);

extern os_err_t interrupt_stack_show(void);

extern os_err_t arch_call_back_trace_init(void);

#endif

#ifdef __cplusplus
}
#endif

#endif /* __OS_ARCH_EXCEPTION_H__ */
