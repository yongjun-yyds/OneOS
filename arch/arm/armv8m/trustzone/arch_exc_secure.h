/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        arch_exc_secure.h
 *
 * @brief       A head file of exception functions related to the Cortex-V8M secure zone.
 *
 * @revision
 * Date         Author          Notes
 * 2021-02-25   OneOS Team      First version.
 ***********************************************************************************************************************
 */

#ifndef __OS_ARCH_EXC_SECURE_H__
#define __OS_ARCH_EXC_SECURE_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <os_types.h>

#define SAU_SFSR (*(volatile const unsigned int *)0xE000EDE4) /* Secure Fault Status Register */
#define SAU_SFAR (*(volatile const unsigned int *)0xE000EDE8) /* Secure Fault Address Register */

#define SCB_CFSR        (*(volatile const unsigned int *)0xE000ED28) /* Configurable Fault Status Register */
#define SCB_HFSR        (*(volatile const unsigned int *)0xE000ED2C) /* HardFault Status Register */
#define SCB_MMAR        (*(volatile const unsigned int *)0xE000ED34) /* MemManage Fault Address register */
#define SCB_BFAR        (*(volatile const unsigned int *)0xE000ED38) /* Bus Fault Address Register */
#define SCB_AIRCR       (*(volatile unsigned int *)0xE000ED0C)       /* Reset control Address Register */
#define SCB_RESET_VALUE 0x05FA0004                                   /* Reset value, write to SCB_AIRCR can reset cpu */

#define SCB_CFSR_MFSR (*(volatile const unsigned char *)0xE000ED28)  /* Memory-management Fault Status Register */
#define SCB_CFSR_BFSR (*(volatile const unsigned char *)0xE000ED29)  /* Bus Fault Status Register */
#define SCB_CFSR_UFSR (*(volatile const unsigned short *)0xE000ED2A) /* Usage Fault Status Register */

/* 1 (Stack frame contains floating point) or 0 (Stack frame does not contain floating point) */
#define EXCEPTION_STACK_FRAME_TYPE_MASK 0x00000010
/* 1 (Return to Thread) or 0 (Return to Handler) */
#define EXCEPTION_RETURN_MODE_MASK    0x00000008
#define EXCEPTION_RETURN_MODE_THREAD  0x00000008
#define EXCEPTION_RETURN_MODE_HANDLER 0x00000000

/* 1 (Return with Process Stack) or 0 (Return with Main Stack)*/
#define EXCEPTION_RETURN_STACK_MASK 0x00000004


struct cpu_stack_frame
{
    uint32_t r0;
    uint32_t r1;
    uint32_t r2;
    uint32_t r3;
    uint32_t r12;
    uint32_t lr;
    uint32_t pc;
    uint32_t psr;
};

struct cpu_stack_frame_fpu
{
    uint32_t r0;
    uint32_t r1;
    uint32_t r2;
    uint32_t r3;
    uint32_t r12;
    uint32_t lr;
    uint32_t pc;
    uint32_t psr;

    /* FPU registers */
    uint32_t S0;
    uint32_t S1;
    uint32_t S2;
    uint32_t S3;
    uint32_t S4;
    uint32_t S5;
    uint32_t S6;
    uint32_t S7;
    uint32_t S8;
    uint32_t S9;
    uint32_t S10;
    uint32_t S11;
    uint32_t S12;
    uint32_t S13;
    uint32_t S14;
    uint32_t S15;
    uint32_t FPSCR;
    uint32_t NO_NAME;
};

struct stack_frame_common
{
    uint32_t r4;
    uint32_t r5;
    uint32_t r6;
    uint32_t r7;
    uint32_t r8;
    uint32_t r9;
    uint32_t r10;
    uint32_t r11;
    uint32_t exc_return;
};

struct stack_frame_common_fpu
{
    uint32_t r4;
    uint32_t r5;
    uint32_t r6;
    uint32_t r7;
    uint32_t r8;
    uint32_t r9;
    uint32_t r10;
    uint32_t r11;
    uint32_t exc_return;

    /* FPU registers */
    uint32_t s16;
    uint32_t s17;
    uint32_t s18;
    uint32_t s19;
    uint32_t s20;
    uint32_t s21;
    uint32_t s22;
    uint32_t s23;
    uint32_t s24;
    uint32_t s25;
    uint32_t s26;
    uint32_t s27;
    uint32_t s28;
    uint32_t s29;
    uint32_t s30;
    uint32_t s31;
};

struct stack_frame
{
    /* Push or pop stack manually */
    struct stack_frame_common frame_common;

    /* Push or pop stack automatically */
    struct cpu_stack_frame cpu_frame;
};

struct stack_frame_fpu
{
    /* Push or pop stack manually */
    struct stack_frame_common_fpu frame_common_fpu;

    /* Push or pop stack automatically */
    struct cpu_stack_frame_fpu cpu_frame_fpu;
};

extern void _arch_hard_fault_track(void);

#ifdef __cplusplus
}
#endif

#endif /* __OS_ARCH_EXCEPTION_H__ */
