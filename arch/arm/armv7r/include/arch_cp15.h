/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with 
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on 
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the 
 * specific language governing permissions and limitations under the License.
 *
 * @file        cp15.h
 *
 * @brief       Header file for cpu interface. 
 *
 * @revision
 * Date         Author          Notes
 * 2020-06-22   OneOS Team      First version.
 ***********************************************************************************************************************
 */
#ifndef __CP15_H__
#define __CP15_H__

unsigned long os_cpu_get_smp_id(void);

void os_cpu_mmu_disable(void);
void os_cpu_mmu_enable(void);
void os_cpu_tlb_set(volatile unsigned long*);

void os_cpu_dcache_clean_flush(void);
void os_cpu_icache_flush(void);

void os_cpu_vector_set_base(unsigned int addr);

#endif
