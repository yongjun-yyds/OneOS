#ifndef __ARCH_MISC_H__
#define __ARCH_MISC_H__

#include <os_types.h>

#ifdef __cplusplus
    extern "C" {
#endif

extern int32_t  os_ffs(uint32_t value);
extern int32_t  os_fls(uint32_t value);
extern void       *os_get_current_task_sp(void);
extern int32_t  os_cpu_id_get(void);

#ifdef __cplusplus
    }
#endif

#endif /* __ARCH_MISC_H__ */

