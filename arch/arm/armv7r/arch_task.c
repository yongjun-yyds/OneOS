#include <os_types.h>
#include <os_task.h>
#include <os_assert.h>

#include <arch_task.h>
#include <os_arch_hw.h>
#include <string.h>

#if 0
#ifdef CONFIG_ONEOS_FPU
uint32_t get_fpexc(void) 
{
    register uint32_t result;

    __asm__ __volatile__(
        "   VMRS R0, fpexc\n"
        "   MOV %0, R0\n"
        :   "=r"(result));

    return result;
}

uint32_t get_fpscr(void) 
{
    register uint32_t result;

    __asm__ __volatile__(
        "   VMRS R0, fpscr\n"
        "   MOV %0, R0\n"
        :   "=r"(result));

    return result;
}
#endif

#else

extern uint32_t get_fpexc(void);
extern uint32_t get_fpscr(void);

#endif
static os_arch_ins_mode gs_arch_ins_mode = OS_ARCH_INS_MODE_ERR;
static void os_arch_set_ins_mode(os_arch_ins_mode mode)
{
    gs_arch_ins_mode = mode;
    return;
}

os_arch_ins_mode os_arch_get_ins_mode(void)
{
    return gs_arch_ins_mode;
}

/* Stack down */
void *os_hw_stack_init(void        (*task_entry)(void *arg),
                            void         *arg,
                            void         *stack_begin,
                            uint32_t   stack_size,
                            void        (*task_exit)(void))
{
    struct exc_stack_frame *stack_frame;
    uint8_t             *stack_top;
    uint32_t             index;

    memset(stack_begin, '$', stack_size);

    stack_top  = (uint8_t *)stack_begin + stack_size;
    /*Compatible with old interfaces*/
    stack_top  = (uint8_t *)OS_ALIGN_DOWN((uint32_t)stack_top, OS_ARCH_STACK_ALIGN_SIZE);
    stack_top -= sizeof(struct exc_stack_frame);

    /* TODO: Need to check stack size */

    stack_frame = (struct exc_stack_frame *)stack_top;

    /* Initialize all registers */
    for (index = 0; index < sizeof(struct exc_stack_frame) / sizeof(uint32_t); index++)
    {
        ((uint32_t *)stack_frame)[index] = 0xDEADBEEF;
    }
#ifdef CONFIG_ONEOS_FPU
    stack_frame->fpexc = get_fpexc();
    stack_frame->fpscr = get_fpscr();
    stack_frame->S0 = 0;
    stack_frame->S1 = 0;
    stack_frame->S2 = 0;
    stack_frame->S3 = 0;
    stack_frame->S4 = 0;
    stack_frame->S5 = 0;
    stack_frame->S6 = 0;
    stack_frame->S7 = 0;
    stack_frame->S8 = 0;
    stack_frame->S9 = 0;
    stack_frame->S10 = 0;
    stack_frame->S11 = 0;
    stack_frame->S12 = 0;
    stack_frame->S13 = 0;
    stack_frame->S14 = 0;
    stack_frame->S15 = 0;
    stack_frame->s16 = 0;
    stack_frame->s17 = 0;
    stack_frame->s18 = 0;
    stack_frame->s19 = 0;
    stack_frame->s20 = 0;
    stack_frame->s21 = 0;
    stack_frame->s22 = 0;
    stack_frame->s23 = 0;
    stack_frame->s24 = 0;
    stack_frame->s25 = 0;
    stack_frame->s26 = 0;
    stack_frame->s27 = 0;
    stack_frame->s28 = 0;
    stack_frame->s29 = 0;
    stack_frame->s30 = 0;
    stack_frame->s31 = 0;
#endif
    stack_frame->r0  = (uint32_t)arg;            /* r0 : argument */
    stack_frame->r1  = 0;                           /* r1 */
    stack_frame->r2  = 0;                           /* r2 */
    stack_frame->r3  = 0;                           /* r3 */
    stack_frame->r12 = 0;                           /* r12 */
    stack_frame->lr  = (uint32_t)task_exit;      /* lr */
    stack_frame->pc  = (uint32_t)task_entry;     /* entry point, pc */

    if ((os_ubase_t)task_entry & 0x01)
    {
        stack_frame->cpsr = 0x13 | 0x20;          /* thumb mode */
        os_arch_set_ins_mode(OS_ARCH_INS_MODE_THUMB);
    }
    else
    {
        stack_frame->cpsr = 0x13;          /* arm mode */
        os_arch_set_ins_mode(OS_ARCH_INS_MODE_ARM);
    }
    
    return (void *)stack_top;
}

uint32_t os_hw_stack_max_used(void *stack_begin, uint32_t stack_size)
{
    uint8_t *addr;
    uint32_t max_used;

    addr = (uint8_t *)stack_begin;
    while (*addr == '$')
    {
        addr++;
    }

    max_used = (uint32_t)((os_ubase_t)stack_begin + stack_size - (os_ubase_t)addr);

    return max_used;
}

#ifdef OS_USING_OVERFLOW_CHECK
/**
 ***********************************************************************************************************************
 * @brief           This function is used to check the stack of task is overflow or not.
 *
 * @param[in]       task            The descriptor of task control block
 *
 * @return          Whether the stack of this task overflows.
 * @retval          OS_TRUE         The stack of this task is overflow.
 * @retval          OS_FALSE        The stack of this task is not overflow.
 ***********************************************************************************************************************
 */
os_bool_t os_task_stack_is_overflow(void *stack_top, void *stack_begin, void *stack_end)
{
    os_bool_t is_overflow;

    OS_ASSERT(OS_NULL != stack_top);
    OS_ASSERT(OS_NULL != stack_begin);
    OS_ASSERT(OS_NULL != stack_end);

    if ((*((uint8_t *)stack_begin) != '$') || ((os_ubase_t)stack_top < (os_ubase_t)stack_begin) ||
        ((os_ubase_t)stack_top >= (os_ubase_t)stack_end))
    {
        is_overflow = OS_TRUE;
    }
    else
    {
        is_overflow = OS_FALSE;
    }

    return is_overflow;
}
#endif /* OS_USING_OVERFLOW_CHECK */
