#include "interrupt.h"
#include "gic.h"
#include <os_stddef.h>
#include <board.h>

#define GIC_IRQ_START   0
#define GIC_CPU_OFFSET  (0x4000)
/* exception and interrupt handler table */
struct os_irq_desc isr_table[ARM_GIC_NR_IRQS];

static void combiner_init()
{
    int i;

    for(i = 0; i < 5; i++)
        *(volatile unsigned int *)(EXYNOS4412_GIC_COMBINE_BASE + 0x04 + i * 0x10) = 0xffffffff;
}

void irq_enable(void)
{
    uint32_t tmp;

    __asm__ __volatile__(
            "mrs %0, cpsr\n"
            "bic %0, %0, #(1<<7)\n"
            "msr cpsr_cxsf, %0"
            : "=r" (tmp)
            :
            : "memory");
}

void fiq_enable(void)
{
    uint32_t tmp;

    __asm__ __volatile__(
            "mrs %0, cpsr\n"
            "bic %0, %0, #(1<<6)\n"
            "msr cpsr_cxsf, %0"
            : "=r" (tmp)
            :
            : "memory");
}

/**
 * This function will initialize hardware interrupt
 */

void os_hw_interrupt_init(void)
{
    uint32_t isr = 0;
    uint32_t gic_cpu_base;
    uint32_t gic_dist_base;
    uint32_t gic_irq_start;
    uint32_t cpu_index = 0;

    cpu_index = os_cpu_id_get();

    if(cpu_index == 0){
        /* initialize exceptions table */
        for(isr=0;isr<sizeof(isr_table);isr++)
        {
            isr_table[isr].handler = OS_NULL;
            isr_table[isr].param = OS_NULL;
        }
    }
    
    /* initialize vector table */
    os_hw_vector_init();
    /* initialize ARM GIC */
    gic_dist_base = platform_get_gic_dist_base() + (cpu_index * GIC_CPU_OFFSET);
    gic_cpu_base = platform_get_gic_cpu_base() + (cpu_index * GIC_CPU_OFFSET);

    gic_irq_start = GIC_IRQ_START;

    arm_gic_dist_init(cpu_index, gic_dist_base, gic_irq_start);
    arm_gic_cpu_init(cpu_index, gic_cpu_base);

    if(cpu_index == 0){
        
        combiner_init();
        vic_enable();
        /* irq_enable(); */
        /* fiq_enable(); */
    }

}

/**
 * This function will mask a interrupt.
 * @param vector the interrupt number
 */
void os_hw_interrupt_mask(int vector)
{    
    arm_gic_mask(0, vector);
}

/**
 * This function will un-mask a interrupt.
 * @param vector the interrupt number
 */
void os_hw_interrupt_umask(int vector)
{
    arm_gic_umask(0, vector);
}

/**
 * This function returns the active interrupt number.
 * @param none
 */
int os_hw_interrupt_get_irq(void)
{
    return arm_gic_get_active_irq(os_cpu_id_get());
}

/**
 * This function acknowledges the interrupt.
 * @param vector the interrupt number
 */
void os_hw_interrupt_ack(int vector)
{
    arm_gic_ack(os_cpu_id_get(), vector);
}

/**
 * This function set interrupt CPU targets.
 * @param vector:   the interrupt number
 *        cpu_mask: target cpus mask, one bit for one core
 */
void os_hw_interrupt_set_target_cpus(int vector, unsigned int cpu_mask)
{
    arm_gic_set_cpu(0, vector, cpu_mask);
}

/**
 * This function get interrupt CPU targets.
 * @param vector: the interrupt number
 * @return target cpus mask, one bit for one core
 */
unsigned int os_hw_interrupt_get_target_cpus(int vector)
{
    return arm_gic_get_target_cpu(0, vector);
}

/**
 * This function set interrupt triger mode.
 * @param vector: the interrupt number
 *        mode:   interrupt triger mode; 0: level triger, 1: edge triger
 */
void os_hw_interrupt_set_triger_mode(int vector, unsigned int mode)
{
    arm_gic_set_configuration(0, vector, mode);
}

/**
 * This function get interrupt triger mode.
 * @param vector: the interrupt number
 * @return interrupt triger mode; 0: level triger, 1: edge triger
 */
unsigned int os_hw_interrupt_get_triger_mode(int vector)
{
    return arm_gic_get_configuration(0, vector);
}

/**
 * This function set interrupt pending flag.
 * @param vector: the interrupt number
 */
void os_hw_interrupt_set_pending(int vector)
{
    arm_gic_set_pending_irq(0, vector);
}

/**
 * This function get interrupt pending flag.
 * @param vector: the interrupt number
 * @return interrupt pending flag, 0: not pending; 1: pending
 */
unsigned int os_hw_interrupt_get_pending(int vector)
{
    return arm_gic_get_pending_irq(0, vector);
}

/**
 * This function clear interrupt pending flag.
 * @param vector: the interrupt number
 */
void os_hw_interrupt_clear_pending(int vector)
{
    arm_gic_clear_pending_irq(0, vector);
}

/**
 * This function set interrupt priority value.
 * @param vector: the interrupt number
 *        priority: the priority of interrupt to set
 */
void os_hw_interrupt_set_priority(int vector, unsigned int priority)
{
    arm_gic_set_priority(0, vector, priority);
}

/**
 * This function get interrupt priority.
 * @param vector: the interrupt number
 * @return interrupt priority value
 */
unsigned int os_hw_interrupt_get_priority(int vector)
{
    return arm_gic_get_priority(0, vector);
}

/**
 * This function set priority masking threshold.
 * @param priority: priority masking threshold
 */
void os_hw_interrupt_set_priority_mask(unsigned int priority)
{
    arm_gic_set_interface_prior_mask(0, priority);
}

/**
 * This function get priority masking threshold.
 * @param none
 * @return priority masking threshold
 */
unsigned int os_hw_interrupt_get_priority_mask(void)
{
    return arm_gic_get_interface_prior_mask(0);
}

/**
 * This function set priority grouping field split point.
 * @param bits: priority grouping field split point
 * @return 0: success; -1: failed
 */
int os_hw_interrupt_set_prior_group_bits(unsigned int bits)
{
    int status;

    if (bits < 8)
    {
        arm_gic_set_binary_point(0, (7 - bits));
        status = 0;
    }
    else
    {
        status = -1;
    }

    return (status);
}

/**
 * This function get priority grouping field split point.
 * @param none
 * @return priority grouping field split point
 */
unsigned int os_hw_interrupt_get_prior_group_bits(void)
{
    unsigned int bp;

    bp = arm_gic_get_binary_point(0) & 0x07;

    return (7 - bp);
}

/**
 * This function will install a interrupt service routine to a interrupt.
 * @param vector the interrupt number
 * @param new_handler the interrupt service routine to be installed
 * @param old_handler the old interrupt service routine
 */
os_isr_handler_t os_hw_interrupt_install(int vector, os_isr_handler_t handler,
        void *param, const char *name)
{
    os_isr_handler_t old_handler = OS_NULL;

    if (vector < ARM_GIC_NR_IRQS)
    {
        old_handler = isr_table[vector].handler;

        if (handler != OS_NULL)
        {
#ifdef OS_USING_INTERRUPT_INFO
            strncpy(isr_table[vector].name, name, OS_NAME_MAX);
#endif /* OS_USING_INTERRUPT_INFO */
            isr_table[vector].handler = handler;
            isr_table[vector].param = param;
        }
    }

    return old_handler;
}

int irq_no_record = 0;
void os_hw_trap_irq(void)
{
    int irq_no;

    irq_no = os_hw_interrupt_get_irq();
    irq_no_record = irq_no;

    if (irq_no >= ARM_GIC_NR_IRQS)
    {
        return;
    }

    os_hw_interrupt_ack(irq_no);

    if(isr_table[irq_no].handler)
    {
        isr_table[irq_no].handler(irq_no,isr_table[irq_no].param);
    }

    return;
}

void os_hw_ipi_send(int ipi_vector, unsigned int cpu_mask)
{
    arm_gic_send_sgi(0, ipi_vector, cpu_mask, 0);
}

void os_hw_ipi_handler_install(int ipi_vector, os_isr_handler_t ipi_isr_handler)
{
    /* note: ipi_vector maybe different with irq_vector */
    os_hw_interrupt_install(ipi_vector, ipi_isr_handler, 0, "IPI_HANDLER");
}

