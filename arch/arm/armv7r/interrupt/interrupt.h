#ifndef __INTERRUPT_H__
#define __INTERRUPT_H__

#include <cp15.h>
//#include <board.h>
#include <irqno.h>
#include <arch_misc.h>

#define INT_IRQ     0x00
#define INT_FIQ     0x01

#define IRQ_MODE_TRIG_LEVEL         (0x00) /* Trigger: level triggered interrupt */
#define IRQ_MODE_TRIG_EDGE          (0x01) /* Trigger: edge triggered interrupt */

#define     OS_SMP_IPI_SCHED        0
#define     OS_SMP_IPI_CPC          1

typedef void (*os_isr_handler_t)(int vector, void *param);

struct os_irq_desc
{
    os_isr_handler_t handler;
    void            *param;

#ifdef OS_USING_INTERRUPT_INFO
    char             name[RT_NAME_MAX];
    uint32_t      counter;
#endif
};

void os_hw_vector_init(void);

void os_hw_interrupt_init(void);
void os_hw_interrupt_mask(int vector);
void os_hw_interrupt_umask(int vector);

int os_hw_interrupt_get_irq(void);
void os_hw_interrupt_ack(int vector);

void os_hw_interrupt_set_target_cpus(int vector, unsigned int cpu_mask);
unsigned int os_hw_interrupt_get_target_cpus(int vector);

void os_hw_interrupt_set_triger_mode(int vector, unsigned int mode);
unsigned int os_hw_interrupt_get_triger_mode(int vector);

void os_hw_interrupt_set_pending(int vector);
unsigned int os_hw_interrupt_get_pending(int vector);
void os_hw_interrupt_clear_pending(int vector);

void os_hw_interrupt_set_priority(int vector, unsigned int priority);
unsigned int os_hw_interrupt_get_priority(int vector);

void os_hw_interrupt_set_priority_mask(unsigned int priority);
unsigned int os_hw_interrupt_get_priority_mask(void);

int os_hw_interrupt_set_prior_group_bits(unsigned int bits);
unsigned int os_hw_interrupt_get_prior_group_bits(void);

os_isr_handler_t os_hw_interrupt_install(int vector, os_isr_handler_t handler,
        void *param, const char *name);

#ifdef OS_USING_SMP
void os_hw_ipi_send(int ipi_vector, unsigned int cpu_mask);
void os_hw_ipi_handler_install(int ipi_vector, os_isr_handler_t ipi_isr_handler);
#endif

#endif

