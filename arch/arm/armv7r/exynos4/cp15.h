#ifndef __CP15_H__
#define __CP15_H__

#include <os_types.h>

void enable_cntv(void);
void disable_cntv(void);
void mask_cntv(void);
void unmask_cntv(void);
uint64_t read_cntvct(void);
uint64_t read_cntvoff(void);
uint32_t read_cntv_tval(void);
void write_cntv_tval(uint32_t val);
uint32_t read_cntfrq(void);
uint32_t read_cntctrl(void);
uint32_t write_cntctrl(uint32_t val);
os_ubase_t cbar_read(void);
uint32_t read_p15_c1(void);
void write_p15_c1(uint32_t value);
void vic_enable(void);
void arch_cpu_mmu_disable(void);
void os_hw_vector_init(void);

#endif
