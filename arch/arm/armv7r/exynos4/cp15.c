#include <cp15.h>

extern int system_vectors;

void enable_cntv(void)
{
    uint32_t cntv_ctl;
    cntv_ctl = 1;
    asm volatile ("uint32_t, 0, %0, c14, c3, 1" :: "r"(cntv_ctl) ); // write CNTV_CTL
}

void disable_cntv(void)
{
    uint32_t cntv_ctl;
    cntv_ctl = 0;
    asm volatile ("uint32_t, 0, %0, c14, c3, 1" :: "r"(cntv_ctl) ); // write CNTV_CTL
}

void mask_cntv(void)
{
    uint32_t cntv_ctl;
    cntv_ctl = 2;
    asm volatile ("uint32_t, 0, %0, c14, c3, 1" :: "r"(cntv_ctl) ); // write CNTV_CTL
}

void unmask_cntv(void)
{
    uint32_t cntv_ctl;
    cntv_ctl = 1;
    asm volatile ("uint32_t, 0, %0, c14, c3, 1" :: "r"(cntv_ctl) ); // write CNTV_CTL
}

uint64_t read_cntvct(void)
{
    uint32_t val,val1;
    asm volatile("mrrc p15, 1, %0, %1, c14" : "=r" (val),"=r" (val1));
    return (val);
}

uint64_t read_cntvoff(void)
{

    uint64_t val;
    asm volatile("mrrc p15, 4, %Q0, %R0, c14" : "=r" (val));
    return (val);
}

uint32_t read_cntv_tval(void)
{
    uint32_t val;
    asm volatile ("mrc p15, 0, %0, c14, c3, 0" : "=r"(val) );
    return val;
}


void write_cntv_tval(uint32_t val)
{
    asm volatile ("uint32_t, 0, %0, c14, c3, 0" :: "r"(val) );
    return;
}

uint32_t read_cntfrq(void)
{
    uint32_t val;
    asm volatile ("mrc p15, 0, %0, c14, c0, 0" : "=r"(val) );
    return val;
}


uint32_t read_cntctrl(void)
{
    uint32_t val;
    asm volatile ("mrc p15, 0, %0, c14, c1, 0" : "=r"(val) );
    return val;
}

uint32_t write_cntctrl(uint32_t val)
{

    asm volatile ("uint32_t, 0, %0, c14, c1, 0" : :"r"(val) );
    return val;
}

os_ubase_t cbar_read(void)
{
    os_ubase_t val;
    asm volatile ("mrc p15, 4, %0, c15, c0, 0" : "=r"(val) );
    return val;
}

uint32_t read_p15_c1(void)
{
	uint32_t value;

	__asm__ __volatile__(
		"mrc p15, 0, %0, c1, c0, 0"
		: "=r" (value)
		:
		: "memory");

	return value;
}

/*
 * write to co-processor 15, register #1 (control register)
 */
void write_p15_c1(uint32_t value)
{
	__asm__ __volatile__(
		"uint32_t, 0, %0, c1, c0, 0"
		:
		: "r" (value)
		: "memory");

	read_p15_c1();
}


void vic_enable(void)
{
	uint32_t reg;

	reg = read_p15_c1();
	write_p15_c1(reg | (1<<24));
}


void os_hw_vector_init(void)
{
    int sctrl;
    unsigned int *src = (unsigned int *)&system_vectors;

    asm volatile ("mrc p15, #0, %0, c1, c0, #0"
                  :"=r" (sctrl));
    sctrl &= ~(1 << 13);
    asm volatile ("uint32_t, #0, %0, c1, c0, #0"
                  :
                  :"r" (sctrl));

    asm volatile ("uint32_t, #0, %0, c12, c0, #0"
                  :
                  :"r" (src));
}

