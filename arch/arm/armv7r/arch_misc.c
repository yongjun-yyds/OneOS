#include <oneos_config.h>
#include <os_types.h>

#if defined(__GNUC__)
int32_t os_ffs(uint32_t value)
{
    return __builtin_ffs(value);
}
#endif


void *os_get_current_task_sp(void) 
{
    register void *result;

    __asm__ __volatile__(
        "   MOV %0, SP\n"
        :   "=r"(result));

    return result;
}

int32_t os_fls(uint32_t value)
{
    int32_t pos;

    pos = 32;
     
    if (!value)
    {
        pos = 0;
    }
    else
    {
        if (!(value & 0xFFFF0000U))
        {
            value <<= 16;
            pos    -= 16;
        }
        
        if (!(value & 0xFF000000U))
        {
            value <<= 8;
            pos    -= 8;
        }
        
        if (!(value & 0xF0000000U))
        {
            value <<= 4;
            pos -= 4;
        }
        
        if (!(value & 0xC0000000U))
        {
            value <<= 2;
            pos -= 2;
        }
        
        if (!(value & 0x80000000U))
        {
            value <<= 1;
            pos -= 1;
        }
    }
    
    return pos;
}


