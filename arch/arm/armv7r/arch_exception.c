#include <os_types.h>
#include <os_safety.h>
#include <os_task.h>
#include <os_util.h>
#include <arch_misc.h>

#define OS_EXEC_PRINT_PROMPT    \
    do{ \
        os_kprintf("Please use OBJDUMP to get disassembly information "\
        "from the ELF file, and then use ADDRESS to check.\r\n");\
    }while(0);

#define OS_EXEC_PRINT_STACK_INFO    \
    do {                                        \
        os_kprintf("stack trace(ADDRESS): \r\n");   \
        for (i = 0; i < record->depth; i++)     \
        {                                       \
            os_kprintf("ADDRESS[%d]: 0x%08x\r\n", i, record->back_trace[i]);   \
        }                                       \
        os_kprintf("\r\n");                     \
    } while(0);

static os_size_t g_intr_stack_end;
static os_size_t g_intr_stack_start;

os_bool_t   os_is_fault_active(void)
{
    return OS_TRUE;
}

void os_arch_exception_handler(void *stack, uint32_t source)
{
    os_ubase_t val;
    os_kprintf("source : 0x%08x\r\n", source);
    struct exc_stack_frame *stack_frame = (struct exc_stack_frame *)stack;
    
    os_kprintf("R0 : 0x%08x\r\n", stack_frame->r0);
    os_kprintf("R1 : 0x%08x\r\n", stack_frame->r1);
    os_kprintf("R2 : 0x%08x\r\n", stack_frame->r2);
    os_kprintf("R3 : 0x%08x\r\n", stack_frame->r3);
    os_kprintf("R4 : 0x%08x\r\n", stack_frame->r4);
    os_kprintf("R5 : 0x%08x\r\n", stack_frame->r5);
    os_kprintf("R6 : 0x%08x\r\n", stack_frame->r6);
    os_kprintf("R7 : 0x%08x\r\n", stack_frame->r7);
    os_kprintf("R8 : 0x%08x\r\n", stack_frame->r8);
    os_kprintf("R9 : 0x%08x\r\n", stack_frame->r9);
    os_kprintf("R10: 0x%08x\r\n", stack_frame->r10);
    os_kprintf("R11: 0x%08x\r\n", stack_frame->r11);
    os_kprintf("R12: 0x%08x\r\n", stack_frame->r12);
    os_kprintf("LR : 0x%08x\r\n", stack_frame->lr);
    os_kprintf("PC : 0x%08x\r\n", stack_frame->pc);
    os_kprintf("PSR: 0x%08x\r\n", stack_frame->cpsr);
    
    __asm__ volatile ( "mrc p15, 0, %0, c5, c0, 0" : "=r"(val)); // data fault status 
    os_kprintf("%s:%d data fault status = %#x\r\n", __FUNCTION__, __LINE__, val);

    __asm__ volatile ( "mrc p15, 0, %0, c5, c0, 1" : "=r"(val)); // instruction fault status 
    os_kprintf("%s:%d instruction fault status = %#x\r\n", __FUNCTION__, __LINE__, val);
    
    __asm__ volatile ( "mrc p15, 0, %0, c5, c1, 0" : "=r"(val)); // aux data fault status 
    os_kprintf("%s:%d aux data fault status = %#x\r\n", __FUNCTION__, __LINE__, val); 
    
    __asm__ volatile ( "mrc p15, 0, %0, c5, c1, 1" : "=r"(val)); // aux instruction fault status 
    os_kprintf("%s:%d aux instruction fault status = %#x\r\n", __FUNCTION__, __LINE__, val); 
    
    __asm__ volatile ( "mrc p15, 0, %0, c6, c0, 0" : "=r"(val)); // data fault address 
    os_kprintf("%s:%d data fault addr = %#x\r\n", __FUNCTION__, __LINE__, val); 
    
    __asm__ volatile ( "mrc p15, 0, %0, c6, c0, 2" : "=r"(val)); // instruction fault address 
    os_kprintf("%s:%d int fault addr = %#x\r\n", __FUNCTION__, __LINE__, val); 

    os_safety_exception_process();
}

#ifdef STACK_TRACE_EN
#include <stack_trace.h>
#include <os_errno.h>
#include "../../../kernel/source/os_kernel_internal.h"
#include <arch_exception.h>
#ifdef OS_USING_SMP
static call_back_trace_t gs_record_global[OS_SMP_MAX_CPUS];
#else
static call_back_trace_t gs_record_global;
#endif

/**
 ***********************************************************************************************************************
 * @brief           This function displays a list of stack backtraces for the specified task.
 *
 * @attention       This function cannot be used in an exception context for the currently running task.
 *                  Because the processing method of the currently running task and other tasks is different.
 *
 * @param[in]       task            Task control block.
 * @param[in]       context         1:the exception context 0:other context
 *
 * @return          On success, return OS_SUCCESS; on error, OS_FAILURE is returned.
 * @retval          OS_SUCCESS         Success.
 * @retval          OS_FAILURE         There is no task with in the system.
 ***********************************************************************************************************************
 */
os_err_t task_stack_show(os_task_id tid, uint32_t context)
{
    /* The place where the function call backtrace starts after removing the registers in the stack. */
    os_size_t *sp_func_call;
    os_size_t *sp_top;
    os_size_t *sp_bottom;
    uint16_t    i;
    os_task_t *task;
    os_ubase_t irq_save;

    call_back_trace_t *record;

    if (OS_NULL == tid)
    {
        return OS_FAILURE;
    }

    irq_save = os_irq_lock();
    task = k_task_self();
    if (tid != (os_task_id)task) 
    {
        os_kprintf("Only trace current task on this core.\r\n");
        return OS_FAILURE;
    }

    if (os_is_irq_active() == OS_TRUE)
    {
        return OS_FAILURE;
    }

    sp_top = (os_size_t *)os_get_current_task_sp();
    sp_func_call = sp_top;

    /*Task overflow.*/
    if ((sp_top >= (os_size_t *)task->stack_end) || (sp_top < (os_size_t *)task->stack_begin))
    {
        sp_bottom = sp_top + TASK_STACK_OVERFLOW_STACK_SIZE / sizeof(os_size_t);
        os_kprintf("The stack of task %s is overflow!\r\n", ((os_task_t *)task)->name);
    }
    else
    {
        sp_bottom = task->stack_end;
    }

#ifdef OS_USING_SMP
    record = &gs_record_global[os_cpu_id_get()];
#else
    record = &gs_record_global;
#endif
    record->depth = 0;
    trace_stack(sp_func_call, sp_bottom, g_code_start_addr, g_code_end_addr, record);

    OS_EXEC_PRINT_STACK_INFO;

#ifdef EXC_DUMP_STACK
    dump_stack((uint32_t)sp_func_call,
            (uint32_t)sp_bottom - (uint32_t)sp_func_call,
            (os_size_t *)sp_func_call);
#endif

    os_irq_unlock(irq_save);
    OS_EXEC_PRINT_PROMPT;
    return OS_SUCCESS;
}


/**
 ***********************************************************************************************************************
 * @brief           This function displays a list of stack backtraces for interrupt.
 *
 * @attention       This function can only be used in interrupt context, not in task and exception context.
 *
 * @param[in]       None.
 *
 * @return          On success, return OS_SUCCESS; on error, OS_FAILURE is returned.
 * @retval          OS_SUCCESS         Success.
 * @retval          OS_FAILURE         Context is not an interrupt context.
 ***********************************************************************************************************************
 */
os_err_t interrupt_stack_show(void)
{
    /* The place where the function call backtrace starts after removing the registers in the stack. */
    os_size_t *sp_func_call;
    os_size_t *sp_top; /* Stack position to start backtracking. */
    os_size_t *sp_bottom;
    uint16_t   i;
    os_ubase_t irq_save;
    call_back_trace_t   *record;
    os_task_t *task;
    if (os_is_irq_active() == OS_FALSE)
    {
        return OS_FAILURE;
    }

    irq_save = os_irq_lock();
    task = k_task_self();
    // sp_top       = os_get_current_task_sp();
    // sp_func_call = sp_top;

    // sp_bottom = (os_size_t *)(task->stack_end);
    sp_top = (os_size_t *)g_intr_stack_start;
    sp_func_call = sp_top;
    sp_bottom = (os_size_t *)g_intr_stack_end;
    os_kprintf("task:%s, sp_top: 0x%08x, sp_bottom: 0x%08x\r\n", task->name, sp_top, sp_bottom);
#ifdef OS_USING_SMP
    record = &gs_record_global[os_cpu_id_get()];
#else
    record = &gs_record_global;
#endif
    
    record->depth = 0;

    trace_stack(sp_func_call, sp_bottom, g_code_start_addr, g_code_end_addr, record);

    OS_EXEC_PRINT_STACK_INFO;

#ifdef EXC_DUMP_STACK
    dump_stack((uint32_t)sp_func_call,
               (uint32_t)sp_bottom - (uint32_t)sp_func_call,
               (os_size_t *)sp_func_call);
#endif

    os_irq_unlock(irq_save);
    OS_EXEC_PRINT_PROMPT;
    return OS_SUCCESS;
}

#define BL_IMMEDIATE_MASK   0x0F000000
#define BL_REGISTER_MASK    0x0F000000
#define BL_IMMEDIATE_VALUE  0x0B000000
#define BL_REGISTER_VALUE   0x0B000000
#define BLX_IMMEDIATE_MASK  0xFFFF0000
#define BLX_REGISTER_MASK   0x0FFFFFF0
#define BLX_IMMEDIATE_VALUE 0xFA000000
#define BLX_REGISTER_VALUE  0x012FFF30

static os_bool_t disassembly_ins_arm_is_bl_blx(uint32_t addr)
{
    uint32_t ins = *(uint32_t *)addr;
    /* bl */
    if (((ins & BL_IMMEDIATE_MASK) == BL_IMMEDIATE_VALUE) || 
        ((ins & BL_REGISTER_MASK) == BL_REGISTER_VALUE))
    {
        //os_kprintf("disassembly_ins_arm_is_bl_blx 0x%08x is BL\r\n", ins);
        return OS_TRUE;
    }
    /* blx */
    if (((ins & BLX_IMMEDIATE_MASK) == BLX_IMMEDIATE_VALUE) || 
        ((ins & BLX_REGISTER_MASK) == BLX_REGISTER_VALUE))
    {
        //os_kprintf("disassembly_ins_arm_is_bl_blx 0x%08x is BLX\r\n", ins);
        return OS_TRUE;
    }

    //os_kprintf("disassembly_ins_arm_is_bl_blx 0x%08x is NOT BL or BLX\r\n", ins);
    return OS_FALSE;
}

static os_bool_t disassembly_ins_thumb_is_bl_blx(uint32_t addr)
{
    os_kprintf("[%s]thumb mode is not supported yet.\r\n", __FUNCTION__);
    return OS_FALSE;
}
/**
 ***********************************************************************************************************************
 * @brief           This function check the disassembly instruction is 'BL' or 'BLX'.
 *
 * @param[in]       addr    Address of instruction
 *
 * @return          On success, return OS_TRUE; on error, return OS_FALSE.
 ***********************************************************************************************************************
 */
os_bool_t disassembly_ins_is_bl_blx(uint32_t addr)
{
    os_bool_t ret = OS_FALSE;
    
    switch (os_arch_get_ins_mode())
    {
    case OS_ARCH_INS_MODE_ARM:
        ret = disassembly_ins_arm_is_bl_blx(addr);
        break;
    case OS_ARCH_INS_MODE_THUMB:
        ret = disassembly_ins_thumb_is_bl_blx(addr);
        break;
    default:
        break;
    }

    return ret;
}

/**
 ***********************************************************************************************************************
 * @brief           The function will find and record all function calls in the stack.
 *
 * @attention
 *
 * @param[in]       stack_top       The top of stack.
 * @param[in]       stack_bottom    The bottom of stack.
 * @param[out]      trace           a memory area is used to record function calls
 *
 * @return          No return value.
 ***********************************************************************************************************************
 */
void trace_stack(os_size_t         *stack_top,
                 os_size_t         *stack_bottom,
                 os_size_t           code_start,
                 os_size_t           code_end,
                 call_back_trace_t *trace)
{
    os_size_t   pc;
    os_size_t   *sp;
    uint32_t    base_depth;

    base_depth = trace->depth;
    for (sp = stack_top; sp < stack_bottom; sp++)
    {
        pc = *((os_size_t *)sp);
        // os_kprintf("trace_stack, pc [0x%08x]\r\n", pc);
        if (trace->depth < CALL_BACK_TRACE_MAX_DEPTH)
        {
            if ((pc > code_start) && (pc < code_end) && disassembly_ins_is_bl_blx(pc - sizeof(os_size_t)))
            {
                //os_kprintf("trace_stack, [0x%08x, 0x%08x, 0x%08x]\r\n", code_start, pc, code_end);
                /* ignore repeat */
                if ((base_depth > 0) && (base_depth == trace->depth) && (pc == trace->back_trace[base_depth - 1]))
                {
                    continue;
                }

                trace->back_trace[trace->depth++] = pc;
                //os_kprintf("trace_stack, depth %d, 0x%08x\r\n", trace->depth, pc);
            }
        }
        else
        {
            break;
        }
    }

    //os_kprintf("trace_stack end, depth %d\r\n", trace->depth);
}

/**
***********************************************************************************************************************
* @brief           Initialize address range of code segment and main stack.
*
* @param           None
*
* @return          On success, return OS_SUCCESS; on error, return OS_FAILURE.
* @retval          OS_SUCCESS       Main stack space address range correct.
* @retval          OS_FAILURE       Main stack space address range error.
***********************************************************************************************************************
*/
os_err_t arch_call_back_trace_init(void)
{

#if defined(__GNUC__)
    g_main_stack_start_addr = (os_size_t)(&CSTACK_BLOCK_START);
    g_main_stack_end_addr   = (os_size_t)(&CSTACK_BLOCK_END);
    g_code_start_addr = (os_size_t)(&CODE_SECTION_START);
    g_code_end_addr   = (os_size_t)(&CODE_SECTION_END);
    g_intr_stack_end = (os_size_t)(&INTERRUPT_STACK_END);
    g_intr_stack_start = g_intr_stack_end - OS_ARMV7R_IRQ_STACK_SIZE;
#endif

// #define STACK_TRACE_DEBUG

#ifdef STACK_TRACE_DEBUG
    os_kprintf("g_main_stack_start_addr 0x%08x\r\n", g_main_stack_start_addr);
    os_kprintf("g_main_stack_end_addr 0x%08x\r\n", g_main_stack_end_addr);
    os_kprintf("g_code_start_addr 0x%08x\r\n", g_code_start_addr);
    os_kprintf("g_code_end_addr 0x%08x\r\n", g_code_end_addr);
    os_kprintf("g_intr_stack_end 0x%08x\r\n", g_intr_stack_start);
    os_kprintf("g_intr_stack_end 0x%08x\r\n", g_intr_stack_end);
#endif

    if (g_main_stack_start_addr >= g_main_stack_end_addr)
    {
        os_kprintf("ERROR: Unable to get the main stack information!\r\n");
        return OS_FAILURE;
    }
    
    return OS_SUCCESS;
}

#endif /* STACK_TRACE_EN */
