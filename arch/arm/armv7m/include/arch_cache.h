/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        arch_cache.h
 *
 * @brief       Header file for cache interface.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-23   OneOS Team      First version.
 ***********************************************************************************************************************
 */

#ifndef __ARCH_CACHE_H__
#define __ARCH_CACHE_H__

#include <os_types.h>

#ifdef __cplusplus
extern "C" {
#endif

enum OS_HW_CACHE_OPS
{
    OS_HW_CACHE_FLUSH      = 0x01,
    OS_HW_CACHE_INVALIDATE = 0x02,
};

extern void      os_cpu_icache_enable(void);
extern void      os_cpu_icache_disable(void);
extern os_base_t os_cpu_icache_status(void);
extern void      os_cpu_icache_ops(int32_t ops, void *addr, int32_t size);

extern void      os_cpu_dcache_enable(void);
extern void      os_cpu_dcache_disable(void);
extern os_base_t os_cpu_dcache_status(void);
extern void      os_cpu_dcache_ops(int32_t ops, void *addr, int32_t size);

#ifdef __cplusplus
}
#endif

#endif /* __ARCH_CACHE_H__ */
