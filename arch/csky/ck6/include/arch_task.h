/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        arch_task.h
 *
 * @brief       Header file for task stack functions.
 *
 * @revision
 * Date         Author          Notes
 * 2021-10-22   OneOS Team      First version.
 ***********************************************************************************************************************
 */

#ifndef __ARCH_TASK_H__
#define __ARCH_TASK_H__

#include <os_types.h>

#ifdef __cplusplus
extern "C" {
#endif

#define OS_ARCH_STACK_ALIGN_SIZE 4

extern volatile uint32_t os_task_interrupt_nesting_flag;
extern volatile uint32_t os_task_exception_nesting_flag;

struct os_hw_stack_frame
{
    os_ubase_t epc;  /* epc          - exception program counter           */
    os_ubase_t epsr; /* epsr         - exception processor status register */
    os_ubase_t hi;   /* HI           - The HI register                     */
    os_ubase_t lo;   /* LO           - The LO register                     */
    os_ubase_t r1;   /*              -                                     */
    os_ubase_t r2;   /* a0           - function argument 0                 */
    os_ubase_t r3;   /* a1           - function argument 1                 */
    os_ubase_t r4;   /* a2           - function argument 2                 */
    os_ubase_t r5;   /* a3           - function argument 3                 */
    os_ubase_t r6;   /* a4           - function argument 4                 */
    os_ubase_t r7;   /* a5           - function argument 5                 */
    os_ubase_t r8;   /* L0           - Local register 0                    */
    os_ubase_t r9;   /* L1           - Local register 1                    */
    os_ubase_t r10;  /* L2           - Local register 2                    */
    os_ubase_t r11;  /* L3           - Local register 3                    */
    os_ubase_t r12;  /* L4           - Local register 4                    */
    os_ubase_t r13;  /* L5           - Local register 5                    */
    os_ubase_t r14;  /* L10/gb       - Local register 10 / GOT Base Address*/
    os_ubase_t r15;  /* lr           - return address for jumps            */
};

extern void *os_hw_stack_init(void *tentry, void *parameter, void *stack_begin, uint32_t stack_size, void *texit);

extern uint32_t os_hw_stack_max_used(void *stack_begin, uint32_t stack_size);

extern void os_first_task_start(void);
extern void os_task_switch(void);

#ifdef OS_USING_OVERFLOW_CHECK
extern os_bool_t os_task_stack_is_overflow(void *stack_top, void *stack_begin, void *stack_end);
#endif

#ifdef __cplusplus
}
#endif

#endif /* __ARCH_TASK_H__ */
