/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        arch_exception.c
 *
 * @brief       This file implements the exception functions.
 *
 * @revision
 * Date         Author          Notes
 * 2021-10-22   OneOS Team      First version.
 ***********************************************************************************************************************
 */

#include <os_types.h>
#include <os_util.h>
#include <arch_task.h>
#include <arch_interrupt.h>
#include <os_task.h>
#include <os_safety.h>

OS_UNUSED static char *arch_exception_names[] = {
    "[0]Reset",
    "[1]Unaligned access",
    "[2]Access error",
    "[3]Division by zero",
    "[4]Illegal instruction",
    "[5]Privilege violation",
    "[6]Tracking",
    "[7]Breakpoint",
    "[8]Unrecoverable",
    "[9]Idly4",
    "[10]Interrupt",
    "[11]Fast interrupt",
    "[12]Reserved(HAI)",
    "[13]TLB Unrecoverable(610M)",
    "[14]TLB mismatch(610M)",
    "[15]TLB modification(610M)",
    "[16]Trap#0",
    "[17]Trap#1",
    "[18]Trap#2",
    "[19]Trap#3",
    "[20]TLB read invalid(610M)",
    "[21]TLB write invalid(610M)",
    "[22]Reserved",
    "[23]Reserved",
    "[24]Reserved",
    "[25]Reserved",
    "[26]Reserved",
    "[27]Reserved",
    "[28]Reserved",
    "[29]Reserved",
    "[30]Coprocessor interrupts or exceptions(CP)",
    "[31]System descriptor pointer",
};

/**
 ***********************************************************************************************************************
 * @brief           This function handles hard fault exception.
 *
 * @param[in]       stack_frame     The start address of the stack frame when the exception occurs.
 * @param[in]       msp             Interrupt stack pointer.
 * @param[in]       psp             Currently running task stack pointer.
 *
 * @return          No return value.
 ***********************************************************************************************************************
 */
#ifndef OS_USING_SIMPLE_EXCEPTION
void os_arch_fault_exception(int vector, void *stack_frame)
{
#ifdef STACK_TRACE_EN
    //_arch_exception_stack_show(stack_frame, msp , psp);

    os_safety_exception_process();

#else

    struct os_hw_stack_frame *stack_common;

    stack_common = (struct os_hw_stack_frame *)stack_frame;

    /* Stack frame with floating point storage */
    os_kprintf("epc : 0x%08x\r\n", stack_common->epc);
    os_kprintf("epsr: 0x%08x\r\n", stack_common->epsr);
    os_kprintf("hi  : 0x%08x\r\n", stack_common->hi);
    os_kprintf("lo  : 0x%08x\r\n", stack_common->lo);
    os_kprintf("r1  : 0x%08x\r\n", stack_common->r1);
    os_kprintf("r2  : 0x%08x\r\n", stack_common->r2);
    os_kprintf("r3  : 0x%08x\r\n", stack_common->r3);
    os_kprintf("r4  : 0x%08x\r\n", stack_common->r4);
    os_kprintf("r5  : 0x%08x\r\n", stack_common->r5);
    os_kprintf("r6  : 0x%08x\r\n", stack_common->r6);
    os_kprintf("r7  : 0x%08x\r\n", stack_common->r7);
    os_kprintf("r8  : 0x%08x\r\n", stack_common->r8);
    os_kprintf("r9  : 0x%08x\r\n", stack_common->r9);
    os_kprintf("r10 : 0x%08x\r\n", stack_common->r10);
    os_kprintf("r11 : 0x%08x\r\n", stack_common->r11);
    os_kprintf("r12 : 0x%08x\r\n", stack_common->r12);
    os_kprintf("r13 : 0x%08x\r\n", stack_common->r13);
    os_kprintf("r14 : 0x%08x\r\n", stack_common->r14);
    os_kprintf("r15 : 0x%08x\r\n", stack_common->r15);

    /* Exception generated in task context. */
    if (os_task_interrupt_nesting_flag == 0)
    {
        os_kprintf("exception generated in task, task name: %s\r\n", os_task_get_name(os_get_current_task()));
    }
    /* Exception generated in interrupt context. */
    else
    {
        os_kprintf("exception generated in interrupt, irq number: 0x%X\r\n", os_irq_num());
    }

    os_kprintf("Exception name is: %s\n", arch_exception_names[vector]);

    os_safety_exception_process();
#endif /* STACK_TRACE_EN */
}
#else
void os_arch_fault_exception(int vector, void *stack_frame)
{
    while (1)
        ;
}
#endif
