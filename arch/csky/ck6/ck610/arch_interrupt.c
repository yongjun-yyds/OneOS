/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with 
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on 
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the 
 * specific language governing permissions and limitations under the License.
 *
 * @file        arch_interrupt.c
 *
 * @brief       This file provides interrupt related functions under the csky architecture.
 *
 * @revision
 * Date         Author          Notes
 * 2021-01-12   OneOS Team      First version.
 ***********************************************************************************************************************
 */

#include <os_types.h>
#include <arch_task.h>

/**
 ***********************************************************************************************************************
 * @brief           Disable interrupt.
 *
 * @param           None.
 *
 * @return          The state before disable interrupt.
 ***********************************************************************************************************************
 */
os_ubase_t os_irq_lock(void)
{
    __volatile__ os_ubase_t psr_ie;
    __volatile__ os_ubase_t tmpval;

    __asm__ __volatile__("mfcr %0, psr\n"
                         "movi %1, 0x40\n"
                         "and  %0, %1\n"
                         "psrclr ie\n"
                         : "=r"(psr_ie), "=r"(tmpval)
                         :
                         : "memory");

    return psr_ie;
}

/**
 ***********************************************************************************************************************
 * @brief           Restore interrupt status.
 *
 * @param[in]       The status need be restore.
 *
 * @return          None.
 ***********************************************************************************************************************
 */
void os_irq_unlock(os_ubase_t irq_save)
{
    __volatile__ os_ubase_t tmpval = 0;

    __asm__ __volatile__("mfcr %1, psr\n"
                         "or   %1, %0\n"
                         "mtcr %1, psr\n"
                         :
                         : "r"(irq_save), "r"(tmpval)
                         : "memory");

    return;
}

/**
 ***********************************************************************************************************************
 * @brief           Disable interrupt.
 *
 * @param           None.
 *
 * @return          None.
 ***********************************************************************************************************************
 */
void os_irq_disable(void)
{
    __asm__ __volatile__("psrclr ie\n" : : :);

    return;
}

/**
 ***********************************************************************************************************************
 * @brief           Enable interrupt.
 *
 * @param           None.
 *
 * @return          None.
 ***********************************************************************************************************************
 */
void os_irq_enable(void)
{
    __asm__ __volatile__("psrset ie\n" : : :);

    return;
}

/**
 ***********************************************************************************************************************
 * @brief           Get the current context state, 1: irq context  0: task context.
 *
 * @param           None
 *
 * @return          0:        task context.
 *                  1:        irq context.
 ***********************************************************************************************************************
 */
os_bool_t os_is_irq_active(void)
{
    if (os_task_interrupt_nesting_flag == 0)
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

/**
 ***********************************************************************************************************************
 * @brief           Get irq status.
 *
 * @param           None
 *
 * @return          0:    irq enable
 *                  1:    irq disable
 ***********************************************************************************************************************
 */
os_bool_t os_is_irq_disabled(void)
{
    __volatile__ os_bool_t  ret;
    __volatile__ os_ubase_t oldie  = 0;
    __volatile__ os_ubase_t tmpval = 0;

    __asm__ __volatile__("movi   %0, 0\n"
                         "mfcr   %1, psr\n"
                         "movi   %2, 0x40\n"
                         "and    %1, %2\n"
                         "cmpnei %1, 0\n"
                         "bt     in_irq_enable\n"
                         "movi   %0, 1\n"
                         "in_irq_enable:"
                         : "=r"(ret), "+r"(oldie), "+r"(tmpval)
                         :
                         : "cc", "memory");

    return ret;
}
/**
 ***********************************************************************************************************************
 * @brief           Get irq num.
 *
 * @param           None
 *
 * @return          "CSR_MCAUSE" num
 ***********************************************************************************************************************
 */
uint32_t os_irq_num(void)
{
    __volatile__ uint32_t ret;
    __volatile__ uint32_t tmpval;

    __asm__ __volatile__("lrw    %1, 0x10010000\n"
                         "ldw    %0, (%1, 0x0)\n"
                         "movi   %1, 0x7f\n"
                         "and    %0, %1\n"

                         : "=r"(ret), "=r"(tmpval)
                         :
                         : "memory");

    return ret;
}
/**
 ***********************************************************************************************************************
 * @brief           Determine whether the current context is an exception context.
 *
 * @detail          If return 0, context may running into one status of these: "task", "interrupt" and "NMI".
 *
 * @param           None.
 *
 * @return          Return 1 in exception context, otherwise return 0.
 * @retval          1               In exception context.
 * @retval          0               In other context.
 ***********************************************************************************************************************
 */
os_bool_t os_is_fault_active(void)
{
    if (os_task_exception_nesting_flag == 0)
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

/**
 ***********************************************************************************************************************
 * @brief           Get PSR.
 *
 * @param           None
 *
 * @return          PSR value.
 ***********************************************************************************************************************
 */
uint32_t os_get_psr(void)
{
    __volatile__ uint32_t psr;

    __asm__ __volatile__("mfcr %0, psr\n" : "=r"(psr) : : "memory");

    return psr;
}
