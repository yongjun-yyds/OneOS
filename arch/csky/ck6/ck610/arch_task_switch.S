/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with 
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on 
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the 
 * specific language governing permissions and limitations under the License.
 *
 * @file        arch_task_switch.S
 *
 * @brief       This file provides context switch functions related to the C-sky architecture.
 *
 * @revision
 * Date         Author          Notes
 * 2021-11-10   OneOS Team      First version.
 ***********************************************************************************************************************
 */

 #include "oneos_config.h"

.global os_first_task_start
.global os_task_switch
.global ck_pendSV_handler

.extern g_os_current_task
.extern g_os_next_task
.extern os_task_switch_interrupt_flag
.extern os_task_switch_notify
.extern os_task_interrupt_nesting_flag

.text
.balign 4
/**
 ***********************************************************************************************************************
 * @brief           This function is called when the scheduler starts the first task, Only used once.
 *
 * @param           None.
 *
 * @return          None.
 ***********************************************************************************************************************
 */
os_first_task_start:

    psrclr ie                        /* disable interrupt */
    /* save g_os_next_task to g_os_current_task */
    lrw    r2, g_os_next_task        /* r2 = &g_os_next_task */
    ldw    r2, (r2)                  /* r2 = g_os_next_task */
    lrw    r3, g_os_current_task     /* r3 = &g_os_current_task */
    stw    r2, (r3)                  /* g_os_current_task = g_os_next_task */
    /* pick up g_os_next_task->stack_top to sp */
    ldw    sp, (r2)
   
    /* restore the epc and the epsr */
    ldw    r1, (sp, 0)
    mtcr   r1, epc
    ldw    r1, (sp, 4)
    mtcr   r1, epsr
    addi   sp, 8
    /* restore the sp space of the HI and the LO registers */
    addi   sp, 8
    /* restore the parameter and the texit */
    ldw    r2, (sp, 4)
    addi   sp, 28
    ldw    r15, (sp, 28)
    addi   sp, 32

    rte

/**
 ***********************************************************************************************************************
 * @brief           Start the task swtich process.
 *
 * @param           None.
 *
 * @return          None.
 ***********************************************************************************************************************
 */
os_task_switch:
    /* Allocate space for r1-r15 registers */
    subi    sp, 32
    stw     r8, (sp, 0)
    stw     r9, (sp, 4)
    /* Check if into irq context */
    lrw     r8, os_task_interrupt_nesting_flag
    ldw     r8, (r8)
    cmpnei  r8, 0                    /* if nesting_flag!=0 then into the irq context */
    bt      irq_context_switch       /* if nesting_flag!=0 then jump to irq_context_switch */
    /* restore r8 */
    ldw     r8, (sp, 0)
    ldw     r9, (sp, 4)
    /* save r1-r15 registers */
    subi    sp, 28
    stm     r1-r15, (sp)
    /* save HI and LO */
    subi    sp, 8
    mflo    r1
    stw     r1, (sp, 4)
    mfhi    r1
    stw     r1, (sp, 0)
    /* save pc and psr */
    subi    sp, 8
    mfcr    r1, psr
    stw     r1, (sp, 4)              /* save epsr registwr */
    stw     r15, (sp, 0)             /* save lr as the pc  */
    /* save sp to g_os_current_task->stack_top */
    lrw     r2, g_os_current_task    /* r2 = &g_os_current_task */
    ldw     r1, (r2)                 /* r1 = g_os_current_task */
    stw     sp, (r1)                 /* g_os_current_taskk->stack_top = sp */


    /* Check either task stack during task switching. */
    #if defined (OS_TASK_SWITCH_NOTIFY)
    jsri    os_task_switch_notify
    #endif

    /* switch to task */
    lrw     r3, g_os_next_task       /* r3 = &g_os_next_task */
    ldw     r3, (r3)                 /* r3 = g_os_next_task */
    lrw     r2, g_os_current_task    /* r2 = &g_os_current_task */
    stw     r3, (r2)                 /* g_os_current_task = g_os_next_task */
    /* pick up g_os_next_task->stack_top to sp */
    ldw     sp, (r3)
    
    /* restore the epc and the epsr */
    ldw     r1, (sp, 0)
    mtcr    r1, epc
    ldw     r1, (sp, 4)
    mtcr    r1, epsr
    addi    sp, 8
    /* restore HI and LO */
    ldw     r1, (sp,0)
    mthi    r1
    ldw     r1, (sp,4)
    mtlo    r1
    addi    sp, 8
    /* restore r1-r15 */
    ldm     r1-r15, (sp)
    addi    sp, 28
    addi    sp, 32

    rte

irq_context_switch:
    /* if switch task in irq context, only set switch_interrupt_flag then return */
    lrw     r8, os_task_switch_interrupt_flag
    movi    r9, 1
    stw     r9, (r8)
    /* restore r8 r9 */
    ldw     r8, (sp, 0)
    ldw     r9, (sp, 4)
    addi    sp, 32

    jmp     r15

