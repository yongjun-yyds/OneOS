/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with 
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on 
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the 
 * specific language governing permissions and limitations under the License.
 *
 * @file        arch_atomic.c
 *
 * @brief       This file implements the atomic functions.
 *
 * @revision
 * Date         Author          Notes
 * 2021-01-08   OneOS Team      First version.
 ***********************************************************************************************************************
 */

#include <oneos_config.h>
#include <os_types.h>
#include <os_stddef.h>
#include "arch_atomic.h"

void os_atomic_add(os_atomic_t *mem, int32_t value)
{
}

void os_atomic_sub(os_atomic_t *mem, int32_t value)
{
}

void os_atomic_inc(os_atomic_t *mem)
{
}

void os_atomic_dec(os_atomic_t *mem)
{
}

int32_t os_atomic_xchg(os_atomic_t *mem, int32_t value)
{
    return 0;
}

os_bool_t os_atomic_cmpxchg(os_atomic_t *mem, int32_t old, int32_t new)
{
    return 0;
}

void os_atomic_and(os_atomic_t *mem, int32_t value)
{
}

void os_atomic_or(os_atomic_t *mem, int32_t value)
{
}

void os_atomic_nand(os_atomic_t *mem, int32_t value)
{
}

void os_atomic_xor(os_atomic_t *mem, int32_t value)
{
}

os_bool_t os_atomic_test_bit(os_atomic_t *mem, int32_t nr)
{
    return 0;
}

void os_atomic_set_bit(os_atomic_t *mem, int32_t nr)
{
}

void os_atomic_clear_bit(os_atomic_t *mem, int32_t nr)
{
}

void os_atomic_change_bit(os_atomic_t *mem, int32_t nr)
{
}
