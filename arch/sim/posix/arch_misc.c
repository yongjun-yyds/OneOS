/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with 
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on 
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the 
 * specific language governing permissions and limitations under the License.
 *
 * @file        os_arch_misc.c
 *
 * @brief       This file provides misc related functions under the sim architecture.
 *
 * @revision
 * Date         Author          Notes
 * 2021-01-12   OneOS Team      First version.
 ***********************************************************************************************************************
 */

#include <oneos_config.h>
#include <os_types.h>
#include <arch_task.h>

#if defined(__CC_ARM)
__asm int32_t os_ffs(uint32_t value)
{
    CMP     R0, #0x00
    BEQ     exit

    RBIT    R0, R0
    CLZ     R0, R0
    ADDS    R0, R0, #0x01

exit
    BX      LR
}

__asm void *os_get_current_task_sp(void) 
{
    MRS     R0, PSP
    BX      LR
}

#elif defined(__GNUC__) || defined(__CLANG_ARM)
int32_t os_ffs(uint32_t value)
{
    return __builtin_ffs(value);
}

void *os_get_current_task_sp(void) 
{
    return 0;//os_task_self()->stack_top;
}

#endif

int32_t os_fls(uint32_t value)
{
    int32_t pos;

    pos = 32;
     
    if (!value)
    {
        pos = 0;
    }
    else
    {
        if (!(value & 0xFFFF0000U))
        {
            value <<= 16;
            pos    -= 16;
        }
        
        if (!(value & 0xFF000000U))
        {
            value <<= 8;
            pos    -= 8;
        }
        
        if (!(value & 0xF0000000U))
        {
            value <<= 4;
            pos -= 4;
        }
        
        if (!(value & 0xC0000000U))
        {
            value <<= 2;
            pos -= 2;
        }
        
        if (!(value & 0x80000000U))
        {
            value <<= 1;
            pos -= 1;
        }
    }
    
    return pos;
}

