/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with 
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on 
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the 
 * specific language governing permissions and limitations under the License.
 *
 * @file        wait_for_event.h
 *
 * @brief       Header file for event functions.
 *
 * @revision
 * Date         Author          Notes
 * 2020-11-17   OneOS Team      First version.
 ***********************************************************************************************************************
 */

#ifndef _WAIT_FOR_EVENT_H_
#define _WAIT_FOR_EVENT_H_

#include <stdbool.h>
#include <time.h>

struct event
{
    pthread_mutex_t mutex;
    pthread_cond_t  cond;
    volatile bool   event_triggered;
};

struct event *event_create();
void          event_delete(struct event *);
bool          event_wait(struct event *ev);
bool          event_wait_timed(struct event *ev, time_t ms);
void          event_signal(struct event *ev);

#endif /* ifndef _WAIT_FOR_EVENT_H_ */
