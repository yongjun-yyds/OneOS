/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with 
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on 
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the 
 * specific language governing permissions and limitations under the License.
 *
 * @file        arch_exception.h
 *
 * @brief       Header file for exception functions.
 *
 * @revision
 * Date         Author          Notes
 * 2021-10-22   OneOS Team      First version.
 ***********************************************************************************************************************
 */

#ifndef __OS_ARCH_EXCEPTION_H__
#define __OS_ARCH_EXCEPTION_H__

#include <oneos_config.h>
#include <os_types.h>
#include <os_task.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifndef MSTATUS_FS
#define MSTATUS_FS (0x03 << 13) /* The FS bits of MSTATUS. */
#endif

#define MSTATUS_FS_DIRTY (0x03 << 13) /*  mstatus_fs is indicating the "Dirty". */

#if defined(__GNUC__)
#ifndef CSTACK_BLOCK_START
#define CSTACK_BLOCK_START _sstack
#endif

#ifndef CSTACK_BLOCK_END
#define CSTACK_BLOCK_END _estack
#endif

#ifndef CODE_SECTION_START
#define CODE_SECTION_START _stext
#endif

#ifndef CODE_SECTION_END
#define CODE_SECTION_END _etext
#endif
#else
#error "not supported compiler"
#endif

#if defined(__GNUC__)
extern const int CSTACK_BLOCK_START;
extern const int CSTACK_BLOCK_END;
extern const int CODE_SECTION_START;
extern const int CODE_SECTION_END;
#else
#error "not supported compiler"
#endif

#ifdef STACK_TRACE_EN

#include <stack_trace.h>

extern os_err_t task_stack_show(os_task_id tid, uint32_t context);

extern os_err_t interrupt_stack_show(void);

extern os_bool_t disassembly_ins_is_jal_jalr(uint32_t addr);

extern void trace_stack(os_size_t *stack_top, os_size_t *stack_bottom, call_back_trace_t *trace);

extern os_err_t arch_call_back_trace_init(void);

#endif /* STACK_TRACE_EN */

#ifdef __cplusplus
}
#endif

#endif /* __OS_ARCH_EXCEPTION_H__ */
