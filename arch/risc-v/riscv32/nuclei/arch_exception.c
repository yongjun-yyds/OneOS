/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        arch_exception.c
 *
 * @brief       This file implements the exception functions.
 *
 * @revision
 * Date         Author          Notes
 * 2021-10-22   OneOS Team      First version.
 ***********************************************************************************************************************
 */

#include <os_types.h>
#include <os_util.h>
#include <arch_task.h>
#include <arch_interrupt.h>
#include <arch_misc.h>
#include <os_task.h>
#include <os_errno.h>
#include <os_safety.h>
#include <../../../kernel/source/os_prototypes.h>

#include <arch_exception.h>

OS_UNUSED static char *arch_exception_names[] = {
    "[0]Instruction address misaligned",
    "[1]Instruction access fault",
    "[2]Illegal instruction",
    "[3]Breakpoint",
    "[4]Load address misaligned",
    "[5]Load access fault",
    "[6]Store address misaligned",
    "[7]Store access fault",
    "[8]Environment call from U-mode",
    "[9]Environment call from S-mode",
    "[10]Reserved",
    "[11]Environment call from M-mode",
    "[12]Instruction page fault",
    "[13]Load page fault",
    "[14]Reserved",
    "[15]Store page fault",
};

/**
 ***********************************************************************************************************************
 * @brief           This function handles fault exception.
 *
 * @param[in]       vector          Exception vector number.
 * @param[in]       stack_frame     The start address of the stack frame when the exception occurs.
 *
 * @return          No return value.
 ***********************************************************************************************************************
 */
/* clang-format off */
#ifndef OS_USING_SIMPLE_EXCEPTION
void os_arch_fault_exception(int vector, void *stack_frame)
{
#ifdef STACK_TRACE_EN
    extern void _arch_exception_stack_show(int vector, void *stack_frame);

    _arch_exception_stack_show(vector, stack_frame);

    os_safety_exception_process();
#else

    struct os_hw_stack_frame *stack_common;
    os_ubase_t                msubm_ptyp;

    stack_common = (struct os_hw_stack_frame *)stack_frame;

    /* Stack frame with floating point storage */
    os_kprintf("mstatus: 0x%08x\r\n", stack_common->mstatus);
    os_kprintf("mepc   : 0x%08x\r\n", stack_common->epc);
    os_kprintf("ra     : 0x%08x\r\n", stack_common->ra);
    os_kprintf("mcause : 0x%08x\r\n", stack_common->gp);
    os_kprintf("msubm  : 0x%08x\r\n", stack_common->tp);
    os_kprintf("t0     : 0x%08x\r\n", stack_common->t0);
    os_kprintf("t1     : 0x%08x\r\n", stack_common->t1);
    os_kprintf("t2     : 0x%08x\r\n", stack_common->t2);
    os_kprintf("s0     : 0x%08x\r\n", stack_common->s0_fp);
    os_kprintf("s1     : 0x%08x\r\n", stack_common->s1);
    os_kprintf("a0     : 0x%08x\r\n", stack_common->a0);
    os_kprintf("a1     : 0x%08x\r\n", stack_common->a1);
    os_kprintf("a2     : 0x%08x\r\n", stack_common->a2);
    os_kprintf("a3     : 0x%08x\r\n", stack_common->a3);
    os_kprintf("a4     : 0x%08x\r\n", stack_common->a4);
    os_kprintf("a5     : 0x%08x\r\n", stack_common->a5);
    os_kprintf("a6     : 0x%08x\r\n", stack_common->a6);
    os_kprintf("a7     : 0x%08x\r\n", stack_common->a7);
    os_kprintf("s2     : 0x%08x\r\n", stack_common->s2);
    os_kprintf("s3     : 0x%08x\r\n", stack_common->s3);
    os_kprintf("s4     : 0x%08x\r\n", stack_common->s4);
    os_kprintf("s5     : 0x%08x\r\n", stack_common->s5);
    os_kprintf("s6     : 0x%08x\r\n", stack_common->s6);
    os_kprintf("s7     : 0x%08x\r\n", stack_common->s7);
    os_kprintf("s8     : 0x%08x\r\n", stack_common->s8);
    os_kprintf("s9     : 0x%08x\r\n", stack_common->s9);
    os_kprintf("s10    : 0x%08x\r\n", stack_common->s10);
    os_kprintf("s11    : 0x%08x\r\n", stack_common->s11);
    os_kprintf("t3     : 0x%08x\r\n", stack_common->t3);
    os_kprintf("t4     : 0x%08x\r\n", stack_common->t4);
    os_kprintf("t5     : 0x%08x\r\n", stack_common->t5);
    os_kprintf("t6     : 0x%08x\r\n", stack_common->t6);
#ifdef __riscv_flen
    os_kprintf("ft0    : 0x%08x\r\n", stack_common->f0);
    os_kprintf("ft1    : 0x%08x\r\n", stack_common->f1);
    os_kprintf("ft2    : 0x%08x\r\n", stack_common->f2);
    os_kprintf("ft3    : 0x%08x\r\n", stack_common->f3);
    os_kprintf("ft4    : 0x%08x\r\n", stack_common->f4);
    os_kprintf("ft5    : 0x%08x\r\n", stack_common->f5);
    os_kprintf("ft6    : 0x%08x\r\n", stack_common->f6);
    os_kprintf("ft7    : 0x%08x\r\n", stack_common->f7);
    os_kprintf("fs0    : 0x%08x\r\n", stack_common->f8);
    os_kprintf("fs1    : 0x%08x\r\n", stack_common->f9);
    os_kprintf("fa0    : 0x%08x\r\n", stack_common->f10);
    os_kprintf("fa1    : 0x%08x\r\n", stack_common->f11);
    os_kprintf("fa2    : 0x%08x\r\n", stack_common->f12);
    os_kprintf("fa3    : 0x%08x\r\n", stack_common->f13);
    os_kprintf("fa4    : 0x%08x\r\n", stack_common->f14);
    os_kprintf("fa5    : 0x%08x\r\n", stack_common->f15);
    os_kprintf("fa6    : 0x%08x\r\n", stack_common->f16);
    os_kprintf("fa7    : 0x%08x\r\n", stack_common->f17);
    os_kprintf("fs2    : 0x%08x\r\n", stack_common->f18);
    os_kprintf("fs3    : 0x%08x\r\n", stack_common->f19);
    os_kprintf("fs4    : 0x%08x\r\n", stack_common->f20);
    os_kprintf("fs5    : 0x%08x\r\n", stack_common->f21);
    os_kprintf("fs6    : 0x%08x\r\n", stack_common->f22);
    os_kprintf("fs7    : 0x%08x\r\n", stack_common->f23);
    os_kprintf("fs8    : 0x%08x\r\n", stack_common->f24);
    os_kprintf("fs9    : 0x%08x\r\n", stack_common->f25);
    os_kprintf("fs10   : 0x%08x\r\n", stack_common->f26);
    os_kprintf("fs11   : 0x%08x\r\n", stack_common->f27);
    os_kprintf("ft8    : 0x%08x\r\n", stack_common->f28);
    os_kprintf("ft9    : 0x%08x\r\n", stack_common->f29);
    os_kprintf("ft10   : 0x%08x\r\n", stack_common->f30);
    os_kprintf("ft11   : 0x%08x\r\n", stack_common->f31);
#endif

    msubm_ptyp = stack_common->tp & 0x300;
    /* Exception generated in task context. */
    if (msubm_ptyp == 0)
    {
        os_kprintf("exception generated in task, task name: %s\r\n", os_task_get_name(os_get_current_task()));
    }
    /* Exception generated in interrupt context. */
    else if (msubm_ptyp == 0x100)
    {
        os_kprintf("Exception generated in interrupt.\r\n");
    }
    /* Exception generated in other exception. */
    else
    {
        os_kprintf("Exception generated in other exception.\r\n");
    }

    if (vector == 0xfff)
    {
        os_kprintf("Exception type is: NMI\r\n");
    }
    else
    {
        os_kprintf("Exception name is: %s\r\n", arch_exception_names[vector]);
    }

    os_safety_exception_process();
#endif /* STACK_TRACE_EN */
}
#else
void os_arch_fault_exception(int vector, void *stack_frame)
{
    while(1);
}
#endif

/* clang-format on */

#ifdef STACK_TRACE_EN

#include <stack_trace.h>

/**
 ***********************************************************************************************************************
 * @brief           This function displays a list of stack backtraces for non-running tasks.
 *
 * @attention       The input parameter "task" must be a non-running task.
 *
 * @param[in]       task            Task control block.
 *
 * @return          On success, return OS_SUCCESS; on error, OS_FAILURE is returned.
 * @retval          OS_SUCCESS           Success.
 ***********************************************************************************************************************
 */
os_err_t _noncurrent_task_stack_show(os_task_t *task)
{
    /* The place where the function call backtrace starts after removing the registers in the stack. */
    os_size_t *sp_func_call;

    os_size_t *sp_top;
    os_size_t *sp_bottom;

    os_ubase_t        mstatus;
    uint32_t          float_mode;
    uint32_t          i;
    call_back_trace_t record;

    struct stack_frame_nofpu *nofpu_frame;
    struct stack_frame_fpu   *fpu_frame;

    /* After the task switch occurs, task->sp records the stack pointer of the task. */
    sp_top  = task->stack_top;
    mstatus = ((struct os_hw_stack_frame *)sp_top)->mstatus;

    if ((mstatus & MSTATUS_FS) != MSTATUS_FS_DIRTY)
    {
        float_mode = 0;
    }
    else
    {
        float_mode = 1;
    }

    sp_func_call = sp_top + sizeof(struct stack_frame_fpu) / sizeof(os_size_t);

    if (1 == float_mode)
    {
        fpu_frame = (struct stack_frame_fpu *)sp_top;

        os_kprintf("Float active! \r\n");

        os_kprintf("mstatus: 0x%08x\r\n", fpu_frame->mstatus);
        os_kprintf("mepc   : 0x%08x\r\n", fpu_frame->epc);

        /* x4 */
        os_kprintf("tp     : 0x%08x\r\n", fpu_frame->tp);

        /* x8 - x9 */
        os_kprintf("s0     : 0x%08x\r\n", fpu_frame->s0_fp);
        os_kprintf("s1     : 0x%08x\r\n", fpu_frame->s1);

        /* x18 - x27 */
        os_kprintf("s2     : 0x%08x\r\n", fpu_frame->s2);
        os_kprintf("s3     : 0x%08x\r\n", fpu_frame->s3);
        os_kprintf("s4     : 0x%08x\r\n", fpu_frame->s4);
        os_kprintf("s5     : 0x%08x\r\n", fpu_frame->s5);
        os_kprintf("s6     : 0x%08x\r\n", fpu_frame->s6);
        os_kprintf("s7     : 0x%08x\r\n", fpu_frame->s7);
        os_kprintf("s8     : 0x%08x\r\n", fpu_frame->s8);
        os_kprintf("s9     : 0x%08x\r\n", fpu_frame->s9);
        os_kprintf("s10    : 0x%08x\r\n", fpu_frame->s10);
        os_kprintf("s11    : 0x%08x\r\n", fpu_frame->s11);

/* Stack frame with floating point storage */
#ifdef __riscv_flen
        /* f8 - f9 */
        os_kprintf("fs0    : 0x%08x\r\n", fpu_frame->f8);
        os_kprintf("fs1    : 0x%08x\r\n", fpu_frame->f9);

        /* f18 - f27 */
        os_kprintf("fs2    : 0x%08x\r\n", fpu_frame->f18);
        os_kprintf("fs3    : 0x%08x\r\n", fpu_frame->f19);
        os_kprintf("fs4    : 0x%08x\r\n", fpu_frame->f20);
        os_kprintf("fs5    : 0x%08x\r\n", fpu_frame->f21);
        os_kprintf("fs6    : 0x%08x\r\n", fpu_frame->f22);
        os_kprintf("fs7    : 0x%08x\r\n", fpu_frame->f23);
        os_kprintf("fs8    : 0x%08x\r\n", fpu_frame->f24);
        os_kprintf("fs9    : 0x%08x\r\n", fpu_frame->f25);
        os_kprintf("fs10   : 0x%08x\r\n", fpu_frame->f26);
        os_kprintf("fs11   : 0x%08x\r\n", fpu_frame->f27);
        os_kprintf("ft8    : 0x%08x\r\n", fpu_frame->f28);
#endif

        record.depth                      = 0;
        record.back_trace[record.depth++] = fpu_frame->epc;
    }
    /* Stack frame without floating point storage. */
    else
    {
        nofpu_frame = (struct stack_frame_nofpu *)sp_top;

        os_kprintf("mstatus: 0x%08x\r\n", nofpu_frame->mstatus);
        os_kprintf("mepc   : 0x%08x\r\n", nofpu_frame->epc);

        /* x4 */
        os_kprintf("tp     : 0x%08x\r\n", nofpu_frame->tp);

        /* x8 - x9 */
        os_kprintf("s0     : 0x%08x\r\n", nofpu_frame->s0_fp);
        os_kprintf("s1     : 0x%08x\r\n", nofpu_frame->s1);

        /* x18 - x27 */
        os_kprintf("s2     : 0x%08x\r\n", nofpu_frame->s2);
        os_kprintf("s3     : 0x%08x\r\n", nofpu_frame->s3);
        os_kprintf("s4     : 0x%08x\r\n", nofpu_frame->s4);
        os_kprintf("s5     : 0x%08x\r\n", nofpu_frame->s5);
        os_kprintf("s6     : 0x%08x\r\n", nofpu_frame->s6);
        os_kprintf("s7     : 0x%08x\r\n", nofpu_frame->s7);
        os_kprintf("s8     : 0x%08x\r\n", nofpu_frame->s8);
        os_kprintf("s9     : 0x%08x\r\n", nofpu_frame->s9);
        os_kprintf("s10    : 0x%08x\r\n", nofpu_frame->s10);
        os_kprintf("s11    : 0x%08x\r\n", nofpu_frame->s11);

        record.depth                      = 0;
        record.back_trace[record.depth++] = nofpu_frame->epc;
    }

    /* Task overflow. */
    if ((os_size_t)sp_top >= ((os_size_t)task->stack_end) || (sp_top < (os_size_t *)(task->stack_begin)))
    {
        sp_bottom = sp_func_call + (TASK_STACK_OVERFLOW_STACK_SIZE) / sizeof(os_size_t);

        os_kprintf("The stack of task %s is overflow!\r\n", task->name);
    }
    else
    {
        sp_bottom = task->stack_end;
    }

    trace_stack(sp_func_call, sp_bottom, &record);

#ifdef EXC_DUMP_STACK
    dump_stack((uint32_t)sp_top, (uint32_t)sp_bottom - (uint32_t)sp_top, (os_size_t *)sp_top);
#endif

    os_kprintf("You can use follow command for backtrace:\r\n");
    os_kprintf("addr2line -e *.axf/*.elf -a -f ");
    for (i = 0; i < record.depth; i++)
    {
        os_kprintf("0x%08x ", record.back_trace[i]);
    }
    os_kprintf("\r\n");

    return OS_SUCCESS;
}

/**
 ***********************************************************************************************************************
 * @brief           This function displays a list of stack backtraces for current running tasks.
 *
 * @attention       This function must be used in interrupt context.The input parameter "task" must be a running task.
 *
 * @param[in]       task            Task control block.
 *
 * @return          On success, return OS_SUCCESS; on error, OS_FAILURE is returned.
 * @retval          OS_SUCCESS           Success.
 * @retval          OS_FAILURE         Interrupt and task use the same "sp", can't get the task's "sp".
 ***********************************************************************************************************************
 */
os_err_t _context_irq_current_task_stack_show(os_task_t *task)
{
    os_kprintf("The interrupt context does not support stacktracking to the currently running task!\r\n");
    return OS_FAILURE;
}

/**
 ***********************************************************************************************************************
 * @brief           This function displays a list of stack backtraces for current running tasks.
 *
 * @attention       This function must be used in current running tasks context.
 *
 * @param[in]       task            Task control block.
 * @param[in]       sp_top          The sp of the currently running task.
 *
 * @return          On success, return OS_SUCCESS; on error, OS_FAILURE is returned.
 * @retval          OS_SUCCESS           Success.
 ***********************************************************************************************************************
 */
os_err_t _context_task_current_task_stack_show(os_task_t *task, os_size_t *sp_top)
{
    /* The place where the function call backtrace starts after removing the registers in the stack. */
    os_size_t *sp_func_call;

    os_size_t *sp_bottom;

    uint32_t          i;
    call_back_trace_t record;

    os_kprintf("SP : 0x%08x\r\n", sp_top);
    record.depth = 0;
    sp_func_call = sp_top;

    /*Task overflow.*/
    if ((os_size_t)sp_top >= ((os_size_t)task->stack_end) || (sp_top < (os_size_t *)(task->stack_begin)))
    {
        sp_bottom = sp_top + TASK_STACK_OVERFLOW_STACK_SIZE / sizeof(os_size_t);
        os_kprintf("The stack of task %s is overflow!\r\n", task->name);
    }
    else
    {
        sp_bottom = task->stack_end;
    }

    trace_stack(sp_func_call, sp_bottom, &record);

#ifdef EXC_DUMP_STACK
    dump_stack((uint32_t)sp_top, (uint32_t)sp_bottom - (uint32_t)sp_top, (os_size_t *)sp_top);
#endif

    os_kprintf("You can use follow command for backtrace:\r\n");
    os_kprintf("addr2line -e *.axf/*.elf -a -f ");
    for (i = 0; i < record.depth; i++)
    {
        os_kprintf("0x%08x ", record.back_trace[i]);
    }

    os_kprintf("\r\n");

    return OS_SUCCESS;
}

/**
 ***********************************************************************************************************************
 * @brief           This function displays a list of stack backtraces for the specified task.
 *
 * @attention       This function cannot be used in an exception context for the currently running task.
 *                  Because the processing method of the currently running task and other tasks is different.
 *
 * @param[in]       tid             Task ID.
 * @param[in]       context         1:the exception context 0:other context
 *
 * @return          On success, return OS_SUCCESS; on error, OS_FAILURE is returned.
 * @retval          OS_SUCCESS         Success.
 * @retval          OS_FAILURE         There is no task with in the system.
 ***********************************************************************************************************************
 */
os_err_t task_stack_show(os_task_id tid, uint32_t context)
{
    os_size_t *sp_top;
    os_task_t *task;

    if (OS_NULL == tid)
    {
        return OS_FAILURE;
    }

    task = (os_task_t *)tid;
    if (os_task_check_exist(tid) == OS_FALSE)
    {
        os_kprintf("TID 0x%x was not found. \r\n", tid);
        return OS_FAILURE;
    }

    os_kprintf("=================Task %s stack trace======================\r\n", task->name);

    if (tid != os_get_current_task())
    {
        return _noncurrent_task_stack_show(task);
    }
    else
    {
        if (1 == context)
        {
            os_kprintf("Task %s stack trace has been processed by exc_entry\r\n", task->name);
        }
        else if (0 == context)
        {
            /* The running task in interrupt context. */
            if (os_is_irq_active() > 0)
            {
                return _context_irq_current_task_stack_show(task);
            }
            /* The running task in task context. */
            else
            {
                sp_top = os_get_current_task_sp();
                return _context_task_current_task_stack_show(task, sp_top);
            }
        }
    }

    return OS_SUCCESS;
}

/**
 ***********************************************************************************************************************
 * @brief           This function displays a list of stack backtraces for interrupt.
 *
 * @attention       This function can only be used in interrupt context, not in task and exception context.
 *
 * @param[in]       None.
 *
 * @return          On success, return OS_SUCCESS; on error, OS_FAILURE is returned.
 * @retval          OS_SUCCESS           Success.
 * @retval          OS_FAILURE         Context is not an interrupt context.
 ***********************************************************************************************************************
 */
os_err_t interrupt_stack_show(void)
{
    /* The place where the function call backtrace starts after removing the registers in the stack. */
    os_size_t *sp_func_call;

    os_size_t *sp_top; /* Stack position to start backtracking. */
    os_size_t *sp_bottom;
    uint16_t   i;

    call_back_trace_t record;

    if (os_is_irq_active() == OS_FALSE)
    {
        return OS_FAILURE;
    }

    sp_top       = _arch_get_interrupt_sp();
    sp_func_call = sp_top;

    sp_bottom = (os_size_t *)g_main_stack_end_addr;

    record.depth = 0;

    trace_stack(sp_func_call, sp_bottom, &record);

    os_kprintf("You can user follow command for backtrace:\r\n");
    os_kprintf("addr2line -e *.axf/*.elf -a -f ");
    for (i = 0; i < record.depth; i++)
    {
        os_kprintf("0x%08x ", record.back_trace[i]);
    }
    os_kprintf("\r\n");

#ifdef EXC_DUMP_STACK
    dump_stack((uint32_t)sp_top, (uint32_t)sp_bottom - (uint32_t)sp_top, (os_size_t *)sp_top);
#endif

    return OS_SUCCESS;
}

/**
 ***********************************************************************************************************************
 * @brief           This function handles fault exception.
 *
 * @param[in]       vector          Exception vector number.
 * @param[in]       stack_frame     The start address of the stack frame when the exception occurs.
 *
 * @return          No return value.
 ***********************************************************************************************************************
 */
/* clang-format off */
void _arch_exception_stack_show(int vector, void *stack_frame)
{
    /* The place where the function call backtrace starts after removing the registers in the stack. */
    os_size_t *sp_func_call;

    os_size_t  *sp_top; /* Stack position to start backtracking. */
    os_size_t  *sp_bottom;
    uint16_t i;
    uint32_t thread_mode;

    call_back_trace_t       record;
    struct stack_frame_fpu *fpu_frame;
    os_ubase_t              msubm_ptyp;

    fpu_frame = (struct stack_frame_fpu *)stack_frame;

    /* Stack frame with floating point storage */
    os_kprintf("mstatus: 0x%08x\r\n", fpu_frame->mstatus);
    os_kprintf("mepc   : 0x%08x\r\n", fpu_frame->epc);
    os_kprintf("ra     : 0x%08x\r\n", fpu_frame->ra);
    os_kprintf("mcause : 0x%08x\r\n", fpu_frame->gp);
    os_kprintf("msubm  : 0x%08x\r\n", fpu_frame->tp);
    os_kprintf("t0     : 0x%08x\r\n", fpu_frame->t0);
    os_kprintf("t1     : 0x%08x\r\n", fpu_frame->t1);
    os_kprintf("t2     : 0x%08x\r\n", fpu_frame->t2);
    os_kprintf("s0     : 0x%08x\r\n", fpu_frame->s0_fp);
    os_kprintf("s1     : 0x%08x\r\n", fpu_frame->s1);
    os_kprintf("a0     : 0x%08x\r\n", fpu_frame->a0);
    os_kprintf("a1     : 0x%08x\r\n", fpu_frame->a1);
    os_kprintf("a2     : 0x%08x\r\n", fpu_frame->a2);
    os_kprintf("a3     : 0x%08x\r\n", fpu_frame->a3);
    os_kprintf("a4     : 0x%08x\r\n", fpu_frame->a4);
    os_kprintf("a5     : 0x%08x\r\n", fpu_frame->a5);
    os_kprintf("a6     : 0x%08x\r\n", fpu_frame->a6);
    os_kprintf("a7     : 0x%08x\r\n", fpu_frame->a7);
    os_kprintf("s2     : 0x%08x\r\n", fpu_frame->s2);
    os_kprintf("s3     : 0x%08x\r\n", fpu_frame->s3);
    os_kprintf("s4     : 0x%08x\r\n", fpu_frame->s4);
    os_kprintf("s5     : 0x%08x\r\n", fpu_frame->s5);
    os_kprintf("s6     : 0x%08x\r\n", fpu_frame->s6);
    os_kprintf("s7     : 0x%08x\r\n", fpu_frame->s7);
    os_kprintf("s8     : 0x%08x\r\n", fpu_frame->s8);
    os_kprintf("s9     : 0x%08x\r\n", fpu_frame->s9);
    os_kprintf("s10    : 0x%08x\r\n", fpu_frame->s10);
    os_kprintf("s11    : 0x%08x\r\n", fpu_frame->s11);
    os_kprintf("t3     : 0x%08x\r\n", fpu_frame->t3);
    os_kprintf("t4     : 0x%08x\r\n", fpu_frame->t4);
    os_kprintf("t5     : 0x%08x\r\n", fpu_frame->t5);
    os_kprintf("t6     : 0x%08x\r\n", fpu_frame->t6);
#ifdef __riscv_flen
    os_kprintf("ft0    : 0x%08x\r\n", fpu_frame->f0);
    os_kprintf("ft1    : 0x%08x\r\n", fpu_frame->f1);
    os_kprintf("ft2    : 0x%08x\r\n", fpu_frame->f2);
    os_kprintf("ft3    : 0x%08x\r\n", fpu_frame->f3);
    os_kprintf("ft4    : 0x%08x\r\n", fpu_frame->f4);
    os_kprintf("ft5    : 0x%08x\r\n", fpu_frame->f5);
    os_kprintf("ft6    : 0x%08x\r\n", fpu_frame->f6);
    os_kprintf("ft7    : 0x%08x\r\n", fpu_frame->f7);
    os_kprintf("fs0    : 0x%08x\r\n", fpu_frame->f8);
    os_kprintf("fs1    : 0x%08x\r\n", fpu_frame->f9);
    os_kprintf("fa0    : 0x%08x\r\n", fpu_frame->f10);
    os_kprintf("fa1    : 0x%08x\r\n", fpu_frame->f11);
    os_kprintf("fa2    : 0x%08x\r\n", fpu_frame->f12);
    os_kprintf("fa3    : 0x%08x\r\n", fpu_frame->f13);
    os_kprintf("fa4    : 0x%08x\r\n", fpu_frame->f14);
    os_kprintf("fa5    : 0x%08x\r\n", fpu_frame->f15);
    os_kprintf("fa6    : 0x%08x\r\n", fpu_frame->f16);
    os_kprintf("fa7    : 0x%08x\r\n", fpu_frame->f17);
    os_kprintf("fs2    : 0x%08x\r\n", fpu_frame->f18);
    os_kprintf("fs3    : 0x%08x\r\n", fpu_frame->f19);
    os_kprintf("fs4    : 0x%08x\r\n", fpu_frame->f20);
    os_kprintf("fs5    : 0x%08x\r\n", fpu_frame->f21);
    os_kprintf("fs6    : 0x%08x\r\n", fpu_frame->f22);
    os_kprintf("fs7    : 0x%08x\r\n", fpu_frame->f23);
    os_kprintf("fs8    : 0x%08x\r\n", fpu_frame->f24);
    os_kprintf("fs9    : 0x%08x\r\n", fpu_frame->f25);
    os_kprintf("fs10   : 0x%08x\r\n", fpu_frame->f26);
    os_kprintf("fs11   : 0x%08x\r\n", fpu_frame->f27);
    os_kprintf("ft8    : 0x%08x\r\n", fpu_frame->f28);
    os_kprintf("ft9    : 0x%08x\r\n", fpu_frame->f29);
    os_kprintf("ft10   : 0x%08x\r\n", fpu_frame->f30);
    os_kprintf("ft11   : 0x%08x\r\n", fpu_frame->f31);
#endif

    sp_top       = (os_size_t *)stack_frame;
    sp_func_call = sp_top + sizeof(struct stack_frame_fpu) / sizeof(os_size_t);

    msubm_ptyp = fpu_frame->tp & 0x300;
    /* Exception generated in task context. */
    if (msubm_ptyp == 0)
    {
        thread_mode = 1;

        os_kprintf("exception generated in task, task name: %s\r\n", os_task_get_name(os_get_current_task()));

        /* Task overflow. */
        if ((os_size_t)sp_top >= ((os_size_t)os_task_get_stack_end(os_get_current_task())) ||
            (sp_top < (os_size_t *)os_task_get_stack_begin(os_get_current_task())))
        {

            sp_bottom = sp_top + (sizeof(struct stack_frame_fpu) + TASK_STACK_OVERFLOW_STACK_SIZE) / sizeof(os_size_t);

            os_kprintf("The stack of task %s is overflow!\r\n", os_task_get_name(os_get_current_task()));
        }
        else
        {
            sp_bottom = (os_size_t *)(os_task_get_stack_end(os_get_current_task()));
        }
    }
    /* Exception generated in interrupt context. */
    else if (msubm_ptyp == 0x100)
    {
        thread_mode = 0;
        sp_bottom   = (os_size_t *)g_main_stack_end_addr;

        os_kprintf("Exception generated in interrupt.\r\n");
    }
    /* Exception generated in other exception. */
    else
    {
        thread_mode = 2;
        os_kprintf("Exception generated in other exception.\r\n");
    }

    if (vector == 0xfff)
    {
        os_kprintf("Exception type is: NMI\r\n");
    }
    else
    {
        os_kprintf("Exception name is: %s\r\n", arch_exception_names[vector]);
    }

    if (thread_mode < 2)
    {
        record.depth                      = 0;
        record.back_trace[record.depth++] = fpu_frame->epc;
        if (fpu_frame->epc != fpu_frame->ra)
        {
            record.back_trace[record.depth++] = fpu_frame->ra;
        }

        trace_stack(sp_func_call, sp_bottom, &record);

        os_kprintf("You can user follow command for backtrace:\r\n");
        os_kprintf("addr2line -e *.axf/*.elf -a -f ");
        for (i = 0; i < record.depth; i++)
        {
            os_kprintf("0x%08x ", record.back_trace[i]);
        }
        os_kprintf("\r\n");

#ifdef EXC_DUMP_STACK
        dump_stack((uint32_t)sp_top, (uint32_t)sp_bottom - (uint32_t)sp_top, (os_size_t *)sp_top);
#endif
    }

#ifdef OS_USING_SHELL

    os_kprintf("=======================   Heap Info   =======================\r\n");
    extern os_err_t sh_memshow(int32_t argc, char **argv);
    sh_memshow(0, OS_NULL);

    os_kprintf("\r\n");

    extern os_err_t sh_show_task_info(int32_t argc, char **argv);
    sh_show_task_info(0, OS_NULL);

#endif /* OS_USING_SHELL */

    while (1);
}
/* clang-format on */

/**
 ***********************************************************************************************************************
 * @brief           This function check the disassembly instruction is 'JAL' or 'JALR'.
 *
 * @param[in]       addr    Address of instruction
 *
 * @return          On success, return OS_TRUE; on error, return OS_FALSE.
 ***********************************************************************************************************************
 */
os_bool_t disassembly_ins_is_jal_jalr(uint32_t addr)
{
    uint32_t ins32 = *((uint32_t *)addr);
    uint16_t ins16 = *((uint16_t *)(addr + 2));

    /* 32-bit instruction is 'jal' */
    if ((ins32 & 0b1111111) == 0b1101111)
    {
        return OS_TRUE;
    }

    /* 32-bit instruction is 'jalr' */
    if (((ins32 & 0b1111111) == 0b1100111) && (((ins32 >> 12) & 0b111) == 0b000))
    {
        return OS_TRUE;
    }

    /* 16-bit instruction is 'jal' */
    if (((ins16 & 0b11) == 0b01) && (((ins16 >> 13) & 0b111) == 0b001))
    {
        return OS_TRUE;
    }

    /* 16-bit instruction is 'jalr' */
    if (((ins16 & 0b1111111) == 0b10) && (((ins16 >> 12) & 0b1111) == 0b1001))
    {
        return OS_TRUE;
    }

    return OS_FALSE;
}

/**
 ***********************************************************************************************************************
 * @brief           The function will find and record all function calls in the stack.
 *
 * @attention
 *
 * @param[in]       stack_top       The top of stack.
 * @param[in]       stack_bottom    The bottom of stack.
 * @param[out]      trace           a memory area is used to record function calls
 *
 * @return          No return value.
 ***********************************************************************************************************************
 */
void trace_stack(os_size_t *stack_top, os_size_t *stack_bottom, call_back_trace_t *trace)
{
    uint32_t   pc;
    os_size_t *sp;
    uint32_t   base_depth;

    base_depth = trace->depth;

    pc = 0;
    for (sp = stack_top; sp < stack_bottom; sp++)
    {
        /* the *sp value may be ra, so need decrease a word to PC */
        pc = *((uint32_t *)sp) - sizeof(os_size_t);

        /* the RISC-V Mixing 16-bit and 32-bit instructions, so the pc must be an even number. */
        if (pc % 2 == 1)
        {
            continue;
        }

        /* fix the PC address in thumb mode */
        pc = *((uint32_t *)sp);

        if (trace->depth < CALL_BACK_TRACE_MAX_DEPTH)
        {
            if ((pc >= g_code_start_addr) && (pc < g_code_end_addr) &&
                disassembly_ins_is_jal_jalr(pc - sizeof(os_size_t)))
            {

                /* ignore repeat */
                if ((base_depth > 0) && (base_depth == trace->depth) && (pc == trace->back_trace[base_depth - 1]))
                {
                    continue;
                }

                trace->back_trace[trace->depth++] = pc;
            }
        }
        else
        {
            break;
        }
    }
}

/**
***********************************************************************************************************************
* @brief           Initialize address range of code segment and main stack.
*
* @param           None
*
* @return          On success, return OS_SUCCESS; on error, return OS_FAILURE.
* @retval          OS_SUCCESS         Main stack space address range correct.
* @retval          OS_FAILURE       Main stack space address range error.
***********************************************************************************************************************
*/
os_err_t arch_call_back_trace_init(void)
{

#if defined(__GNUC__)

    g_main_stack_start_addr = (uint32_t)(&CSTACK_BLOCK_START);
    g_main_stack_end_addr   = (uint32_t)(&CSTACK_BLOCK_END);
    g_code_start_addr       = (uint32_t)(&CODE_SECTION_START);
    g_code_end_addr         = (uint32_t)(&CODE_SECTION_END);

#endif

    if (g_main_stack_start_addr >= g_main_stack_end_addr)
    {
        os_kprintf("ERROR: Unable to get the main stack information!\r\n");
        return OS_FAILURE;
    }

    return OS_SUCCESS;
}

#endif /* STACK_TRACE_EN */
