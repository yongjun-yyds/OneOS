/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        arch_task.h
 *
 * @brief       Header file for task stack functions.
 *
 * @revision
 * Date         Author          Notes
 * 2021-10-22   OneOS Team      First version.
 ***********************************************************************************************************************
 */

#ifndef __ARCH_TASK_H__
#define __ARCH_TASK_H__

#include <os_types.h>

#ifdef __cplusplus
extern "C" {
#endif

#define OS_ARCH_STACK_ALIGN_SIZE 16 /* task stack pointer shall be aligned to a 128-bit boundary */

struct os_hw_stack_frame
{
    os_ubase_t mstatus; /*              - machine status register             */
    os_ubase_t epc;     /* epc - epc    - program counter                     */
    /* x0  - zero          - x0==0,          needn`t save it                     */
    os_ubase_t ra; /* x1  - ra     - return address for jumps            */
    /* x2  - sp            - stack pointer,  needn`t save it                     */
    os_ubase_t gp;    /* x3  - gp     - global pointer                      */
    os_ubase_t tp;    /* x4  - tp     - task pointer                        */
    os_ubase_t t0;    /* x5  - t0     - temporary register 0                */
    os_ubase_t t1;    /* x6  - t1     - temporary register 1                */
    os_ubase_t t2;    /* x7  - t2     - temporary register 2                */
    os_ubase_t s0_fp; /* x8  - s0/fp  - saved register 0 or frame pointer   */
    os_ubase_t s1;    /* x9  - s1     - saved register 1                    */
    os_ubase_t a0;    /* x10 - a0     - return value or function argument 0 */
    os_ubase_t a1;    /* x11 - a1     - return value or function argument 1 */
    os_ubase_t a2;    /* x12 - a2     - function argument 2                 */
    os_ubase_t a3;    /* x13 - a3     - function argument 3                 */
    os_ubase_t a4;    /* x14 - a4     - function argument 4                 */
    os_ubase_t a5;    /* x15 - a5     - function argument 5                 */
    os_ubase_t a6;    /* x16 - a6     - function argument 6                 */
    os_ubase_t a7;    /* x17 - a7     - function argument 7                 */
    os_ubase_t s2;    /* x18 - s2     - saved register 2                    */
    os_ubase_t s3;    /* x19 - s3     - saved register 3                    */
    os_ubase_t s4;    /* x20 - s4     - saved register 4                    */
    os_ubase_t s5;    /* x21 - s5     - saved register 5                    */
    os_ubase_t s6;    /* x22 - s6     - saved register 6                    */
    os_ubase_t s7;    /* x23 - s7     - saved register 7                    */
    os_ubase_t s8;    /* x24 - s8     - saved register 8                    */
    os_ubase_t s9;    /* x25 - s9     - saved register 9                    */
    os_ubase_t s10;   /* x26 - s10    - saved register 10                   */
    os_ubase_t s11;   /* x27 - s11    - saved register 11                   */
    os_ubase_t t3;    /* x28 - t3     - temporary register 3                */
    os_ubase_t t4;    /* x29 - t4     - temporary register 4                */
    os_ubase_t t5;    /* x30 - t5     - temporary register 5                */
    os_ubase_t t6;    /* x31 - t6     - temporary register 6                */
#ifdef ARCH_RISCV_FPU
    /* FPU registers */
    os_rvfd_t f0;  /* ft0          - FP temporaries                      */
    os_rvfd_t f1;  /* ft1          - FP temporaries                      */
    os_rvfd_t f2;  /* ft2          - FP temporaries                      */
    os_rvfd_t f3;  /* ft3          - FP temporaries                      */
    os_rvfd_t f4;  /* ft4          - FP temporaries                      */
    os_rvfd_t f5;  /* ft5          - FP temporaries                      */
    os_rvfd_t f6;  /* ft6          - FP temporaries                      */
    os_rvfd_t f7;  /* ft7          - FP temporaries                      */
    os_rvfd_t f8;  /* fs0          - FP saved registers                  */
    os_rvfd_t f9;  /* fs1          - FP saved registers                  */
    os_rvfd_t f10; /* fa0          - FP arguments/return values          */
    os_rvfd_t f11; /* fa1          - FP arguments/return values          */
    os_rvfd_t f12; /* fa2          - FP arguments                        */
    os_rvfd_t f13; /* fa3          - FP arguments                        */
    os_rvfd_t f14; /* fa4          - FP arguments                        */
    os_rvfd_t f15; /* fa5          - FP arguments                        */
    os_rvfd_t f16; /* fa6          - FP arguments                        */
    os_rvfd_t f17; /* fa7          - FP arguments                        */
    os_rvfd_t f18; /* fs2          - FP saved registers                  */
    os_rvfd_t f19; /* fs3          - FP saved registers                  */
    os_rvfd_t f20; /* fs4          - FP saved registers                  */
    os_rvfd_t f21; /* fs5          - FP saved registers                  */
    os_rvfd_t f22; /* fs6          - FP saved registers                  */
    os_rvfd_t f23; /* fs7          - FP saved registers                  */
    os_rvfd_t f24; /* fs8          - FP saved registers                  */
    os_rvfd_t f25; /* fs9          - FP saved registers                  */
    os_rvfd_t f26; /* fs10         - FP saved registers                  */
    os_rvfd_t f27; /* fs11         - FP saved registers                  */
    os_rvfd_t f28; /* ft8          - FP temporaries                      */
    os_rvfd_t f29; /* ft9          - FP temporaries                      */
    os_rvfd_t f30; /* ft10         - FP temporaries                      */
    os_rvfd_t f31; /* ft11         - FP temporaries                      */
#endif
};

extern void *os_hw_stack_init(void *tentry, void *parameter, void *stack_begin, uint32_t stack_size, void *texit);

extern uint32_t os_hw_stack_max_used(void *stack_begin, uint32_t stack_size);

extern void os_first_task_start(void);
extern void os_task_switch(void);

#ifdef OS_USING_OVERFLOW_CHECK
extern os_bool_t os_task_stack_is_overflow(void *stack_top, void *stack_begin, void *stack_end);
#endif

#ifdef __cplusplus
}
#endif

#endif /* __ARCH_TASK_H__ */
