/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with 
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on 
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the 
 * specific language governing permissions and limitations under the License.
 *
 * @file        os_arch_misc.c
 *
 * @brief       This file provides misc related functions under the mips architecture.
 *
 * @revision
 * Date         Author          Notes
 * 2021-01-12   OneOS Team      First version.
 ***********************************************************************************************************************
 */

#include <oneos_config.h>
#include <os_types.h>

int32_t os_ffs(uint32_t value)
{
    return __builtin_ffs(value);
}

/**
 ***********************************************************************************************************************
 * @brief           This function will return stack pointer of the current running task.
 *
 * @param           No parameter.
 *
 * @return          Return stack pointer of the current running task.
 ***********************************************************************************************************************
 */
void *os_get_current_task_sp(void)
{
    register void *sp;

    __asm__ volatile("move %0, $29" : "=r"(sp));

    return sp;
}

int32_t os_fls(uint32_t value)
{
    int32_t pos;

    pos = 32;

    if (!value)
    {
        pos = 0;
    }
    else
    {
        if (!(value & 0xFFFF0000U))
        {
            value <<= 16;
            pos -= 16;
        }

        if (!(value & 0xFF000000U))
        {
            value <<= 8;
            pos -= 8;
        }

        if (!(value & 0xF0000000U))
        {
            value <<= 4;
            pos -= 4;
        }

        if (!(value & 0xC0000000U))
        {
            value <<= 2;
            pos -= 2;
        }

        if (!(value & 0x80000000U))
        {
            value <<= 1;
            pos -= 1;
        }
    }

    return pos;
}
