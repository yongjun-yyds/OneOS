/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        arch_interrupt.c
 *
 * @brief       This file provides interrupt related functions under the mips architecture.
 *
 * @revision
 * Date         Author          Notes
 * 2021-04-16   OneOS Team      First version.
 ***********************************************************************************************************************
 */
 
#include <oneos_config.h>
#include <os_types.h>
#include <os_stddef.h>

#ifdef OS_USING_INTERRUPT_STACK_OVERFLOW_CHECK
#include <os_assert.h>  
#include <os_errno.h>
#include <arch_exception.h>
#include <shell.h>

static uint32_t gs_main_stack_start_addr;
static uint32_t gs_main_stack_end_addr;

extern uint32_t irq_stack_low;
extern uint32_t irq_stack_top;

#endif /* OS_USING_INTERRUPT_STACK_OVERFLOW_CHECK */

volatile uint32_t g_os_interrupt_nest = 0;

#if defined(__GNUC__)
OS_WEAK os_ubase_t os_irq_lock(void)
{
    /*Implemented in mips_context_gcc.S file.*/
    return 0;
}

OS_WEAK void os_irq_unlock(os_ubase_t irq_save)
{
    /*Implemented in mips_context_gcc.S file.*/
    return;
}

OS_WEAK void os_irq_disable(void)
{
    /*Implemented in mips_context_gcc.S file.*/
    return;
}

OS_WEAK void os_irq_enable(void)
{
    /*Implemented in mips_context_gcc.S file.*/
    return;
}

OS_WEAK os_bool_t os_is_irq_active(void)
{
    if (g_os_interrupt_nest > 0)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

OS_WEAK os_bool_t os_is_irq_disabled(void)
{

    /*Implemented in mips_context_gcc.S file.*/
    return 0;
}

OS_WEAK uint32_t os_irq_num(void)
{
    return 0;
}

/**
 ***********************************************************************************************************************
 * @brief           Determine whether the current context is an exception context.
 *
 * @detail          All exception vector numbers are as follows: Hard Fault:3 MemManage Fault:4 Bus Fault:5
 *                  Usage Fault:6.Therefore, in the exception context, the range of interrupt vector number is [3,6].
 *
 * @param           None.
 *
 * @return          Return 1 in exception context, otherwise return 0.
 * @retval          1               In exception context.
 * @retval          0               In other context.
 ***********************************************************************************************************************
 */
OS_WEAK os_bool_t os_is_fault_active(void)
{
    if (g_os_interrupt_nest > 0)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

#endif

/**
 ***********************************************************************************************************************
 * @brief           This function increase the interrupt nesting level by 1.
 *
 * @attention       This function is supposed to be called every time we enter an interrupt.
 *
 * @param           None
 *
 * @return          No return value.
 ***********************************************************************************************************************
 */
void os_interrupt_enter(void)
{
    os_base_t level;

    level = os_irq_lock();

    g_os_interrupt_nest++;

    os_irq_unlock(level);
}

/**
 ***********************************************************************************************************************
 * @brief           This function decrease the interrupt nesting level by 1.
 *
 * @attention       This function is supposed to be called every time we leave an interrupt.
 *
 * @param           None
 *
 * @return          No return value.
 ***********************************************************************************************************************
 */
void os_interrupt_leave(void)
{
    os_base_t level;

    level = os_irq_lock();

    g_os_interrupt_nest--;

    os_irq_unlock(level);
}

/**
 ***********************************************************************************************************************
 * @brief           This function get the interrupt nesting level.
 *
 * @param           None
 *
 * @return          Return the interrupt nesting level.
 ***********************************************************************************************************************
 */
uint32_t os_interrupt_get_nest(void)
{
    return g_os_interrupt_nest;
}

#ifdef OS_USING_INTERRUPT_STACK_OVERFLOW_CHECK

/**
 ***********************************************************************************************************************
 * @brief           Check whether the interrupt stack overflows.
 *
 * @detail          Traverse the values of the first 16 bytes of the interrupt stack. If the value is not equal to 
 *                  the value of the character $,it is judged that the interrupt stack overflows.
 *
 * @param           None.
 *
 * @return          None.
 ***********************************************************************************************************************
 */
void os_interrupt_stack_check(void)
{
    int i;
    uint32_t *addr;
    
    addr = (uint32_t *)gs_main_stack_start_addr;

    for (i = 0; i < 4; i++)
    {
        if (*addr == 0x24242424)
        {
            addr++;
        }
        else
        {
            os_kprintf("ERROR: Interrupt stack overflow!\r\n");
            OS_ASSERT(0);
        }
            
    }
}

/**
 ***********************************************************************************************************************
 * @brief           Interrupt stack initialization.
 *
 * @detail          The stack is initialized to the character $,range: stack_begin ~ current_msp - 4.
 *
 * @param           None.
 *
 * @return          OS_SUCCESS.
 ***********************************************************************************************************************
 */
os_err_t os_interrupt_stack_init(void)
{
    os_base_t level;
    uint32_t *addr;

    gs_main_stack_start_addr = (uint32_t)&irq_stack_low;
    gs_main_stack_end_addr = (uint32_t)&irq_stack_top;

    if (gs_main_stack_start_addr >= gs_main_stack_end_addr)
    {
        os_kprintf("ERROR: Unable to get the main stack information!\r\n");
        OS_ASSERT(0);
    }

    level = os_irq_lock();

    for (addr = (uint32_t *)gs_main_stack_start_addr; 
         addr < (uint32_t *)gs_main_stack_end_addr; 
         addr++)
    {
        *addr = 0x24242424;
    }

    os_irq_unlock(level);

    return OS_SUCCESS;
}

/**
 ***********************************************************************************************************************
 * @brief           Interrupt stack information display.
 *
 * @detail          The interrupt stack information includes: 
 *                  interrupt stack start and end address, interrupt stack size, 
 *                  and interrupt stack maximum utilization rate.
 *
 * @param           None.
 *
 * @return          OS_SUCCESS.
 ***********************************************************************************************************************
 */
os_err_t sh_show_interrupt_stack_info(void)
{
    uint32_t stack_size;
    uint32_t max_used;

    stack_size = gs_main_stack_end_addr - gs_main_stack_start_addr;

    max_used = os_hw_stack_max_used((void *)gs_main_stack_start_addr, stack_size);

    os_kprintf("stack_begin     stack_end     stack_size    max_used\r\n");
    os_kprintf("-----------    -----------    ----------    --------\r\n");

    os_kprintf("0x%p     0x%p     %-10u    %3u%%\r\n",
               gs_main_stack_start_addr,
               gs_main_stack_end_addr,
               stack_size,
               (max_used * 100) / stack_size
               );

    return OS_SUCCESS;
}
SH_CMD_EXPORT(show_intstk, sh_show_interrupt_stack_info, "Show interrupt stack information");

#endif

