/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with 
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on 
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the 
 * specific language governing permissions and limitations under the License.
 *
 * @file        arch_interrupt.h
 *
 * @brief       Header file for interrupt related functions.
 *
 * @revision
 * Date         Author          Notes
 * 2021-01-12   OneOS Team      First version.
 ***********************************************************************************************************************
 */

#ifndef __ARCH_INTERRUPT_H__
#define __ARCH_INTERRUPT_H__

#include <os_types.h>
#include <oneos_config.h>

#ifdef __cplusplus
extern "C" {
#endif

extern os_ubase_t os_irq_lock(void);
extern void       os_irq_unlock(os_ubase_t irq_save);

extern void os_irq_disable(void);
extern void os_irq_enable(void);

extern os_bool_t   os_is_irq_active(void);
extern os_bool_t   os_is_irq_disabled(void);
extern uint32_t os_irq_num(void);

extern os_bool_t os_is_fault_active(void);

extern void os_interrupt_enter(void);

extern void os_interrupt_leave(void);

extern uint32_t os_interrupt_get_nest(void);

#ifdef OS_USING_INTERRUPT_STACK_OVERFLOW_CHECK
extern os_err_t os_interrupt_stack_init(void);
extern void os_interrupt_stack_check(void);
#else
#define os_interrupt_stack_check()
#define os_interrupt_stack_init()
#endif

#ifdef __cplusplus
}
#endif

#endif /* __ARCH_INTERRUPT_H__ */
