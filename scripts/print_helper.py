# -*- coding: utf-8 -*-
import os
from inspect import currentframe, stack, getmodule


colors = {'cyan': '\033[96m', 'purple': '\033[95m', 'blue': '\033[94m', 'green': '\033[92m', 'yellow': '\033[93m',
          'red': '\033[91m', 'end': '\033[0m'}


def print_error(*args):
    if os.getenv("logging.level") == "debug":
        print(colors.get('red') + "OneOS_ERROR" + "(" + str(stack()[1][1]) + ":" + str(currentframe().f_back.f_lineno) + "):\n" +
                  colors.get('end'), ' '.join(args))
    else:
        print(colors.get('red') + "OneOS_ERROR:" + colors.get('end'), ' '.join(args))


def print_warn(*args):
    if os.getenv("logging.level") == "debug":
        print(colors.get('yellow') + "OneOS_WARN" + "(" + str(stack()[1][1]) + ":" + str(currentframe().f_back.f_lineno) + "):\n" +
                  colors.get('end'), ' '.join(args))
    else:
        print(colors.get('yellow') + "OneOS_WARN:" + colors.get('end'), ' '.join(args))


def print_info(*args):
    if os.getenv("logging.level") == "debug":
        print(colors.get('blue') + "OneOS_INFO" + "(" + str(stack()[1][1]) + ":" + str(currentframe().f_back.f_lineno) + "):\n" +
              colors.get('end'), ' '.join(args))
    else:
        print(' '.join(args))


def print_debug(*args):
    if os.getenv("logging.level") == "debug":
        print(colors.get('cyan') +
              "OneOS_DEBUG" + "(" + str(stack()[1][1]) + ":" + str(currentframe().f_back.f_lineno) + "):\n" +
              colors.get('end'),
              ' '.join(args))