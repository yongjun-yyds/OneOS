# -*- coding: utf-8 -*-
import os
import sys
import shutil
import importlib.machinery
import git
import json
import ntpath
from tempfile import TemporaryDirectory
from pathlib import Path
from rich.progress import Progress, BarColumn

from print_helper import print_info, print_warn, print_error

ONEOS_VERSION_FILE = 'kernel/include/os_version.h'  # 对于oneos源码路径而言
ONEOS_VERSION_KEYWORD = '#define ONEOS_VERSION'
DOWNLOAD_DRIVER_MANUFACTORS = ('STM32', 'NXP', 'NRF5')
# 概览仓库地址
OVERVIEW_REPOSITORY_URL = 'https://gitee.com/oneos-ability/bsp-index.git'
# OVERVIEW_REPOSITORY_URL = 'https://gitee.com/kai-shen/projects_and_dependencies.git'

OVERVIEW_REPOSITORY_BRANCH = 'master'
# OVERVIEW_REPOSITORY_BRANCH = 'dev4'

# 概览仓库
OVERVIEW_REPOSITORY = 'bsp-index'
# OVERVIEW_REPOSITORY = 'projects_and_dependencies'

# json文件名
JSON_FILE = 'bsp.json'
K_CONFIG = 'Kconfig'
# nxp驱动
LPC5500 = 'LPC5500'
IMXRT10XX = 'IMXRT10XX'


class ProjectInfo:

    def __init__(self):
        self.oneos_version = ''  # OneOS版本
        self.model_type = ''  # 芯片类型
        self.soc_type = ''  # 芯片型号
        self.if_exists = False  # 工程是否已存在


def get_oneos_version(oneos_path):
    """初始化oos_info信息(oneos_version)"""

    oneos_version = ''
    oneos_version_file = Path(oneos_path) / ONEOS_VERSION_FILE
    with open(oneos_version_file, 'r', encoding='utf-8') as fp:
        for line_ in fp:
            if line_.find(ONEOS_VERSION_KEYWORD) != -1:
                oneos_version = line_[len(ONEOS_VERSION_KEYWORD):].strip().strip('"').strip("'")
                break
    return oneos_version


def target():
    global g
    g = {}
    manufactor = None
    series = None
    model_type = None
    soc_type = None
    project_info = ProjectInfo()
    oneos_path = Path.cwd().resolve().parent
    project_info.oneos_version = get_oneos_version(oneos_path)

    with open(".config", 'r') as f:
        for ss in f.readlines():
            if '#' in ss or 'is not set' in ss or '=y' not in ss:
                continue

            key = ss.split('CONFIG_')[1].split('=y')[0]
            if 'CONFIG_MANUFACTOR_' in ss:
                manufactor = key[len('MANUFACTOR_'):]

            if 'CONFIG_SERIES_' in ss:
                series = key[len('SERIES_'):]

            if 'CONFIG_MODEL_' in ss:
                model_type = key[len('MODEL_'):]

            elif 'CONFIG_TEMP_' in ss:
                soc_type = key.split('TEMP_')[1]

            g[key] = 1

    project_info.model_type = model_type
    project_info.soc_type = soc_type

    # if model_type is None:
    #     print_warn('unknown model_type')

    if soc_type is None:
        print_error("unknown soc_type")
        return project_info
    print_info("soc_type: %s" % soc_type)
    target_path = './' + soc_type

    if os.path.exists(target_path):
        print_warn("soc_type: %s already exist" % soc_type)
        project_info.if_exists = True
        return project_info

    mod = importlib.machinery.SourceFileLoader('SConscript', "../templates/configs/SConscript").load_module()
    # spec = importlib.util.spec_from_loader(loader.name, loader)
    # mod = importlib.util.module_from_spec(spec)
    # loader.exec_module(mod)
    source_path = mod.soc_type_inq(g)  # copy project_templates to project/target
    if source_path is None or not os.path.exists(source_path):
        print_warn("not support %s" % soc_type)
        return project_info

    try:
        shutil.copytree(source_path, target_path)
    except shutil.Error as exc:
        errors = exc.args[0]
        for error in errors:
            src, dst, msg = error
            if not os.path.exists(src):
                shutil.copytree(src, dst)
    shutil.rmtree(source_path)

    # 针对部分驱动拆分后做的改造，若本地检测没有所需驱动，则从gitee下载
    if manufactor in DOWNLOAD_DRIVER_MANUFACTORS:
        download_driver(manufactor, series, target_path)

    return project_info


def download_driver(manufactor, series, project_path):
    # nxp驱动kconfig编写不规范，做相应适配
    series = series.replace('NXP_', '')
    kconfig_path = os.path.join(project_path, K_CONFIG)
    driver = None
    if not os.path.exists(kconfig_path):
        kconfig_path = find_file(project_path, K_CONFIG)
    with open(kconfig_path, 'r') as f:
        for ss in f.readlines():
            if 'SRC_HAL' in ss:
                driver = ss[len('SRC_HAL='):].strip()
                continue
    hal_path = os.path.join(Path.cwd().resolve().parent, 'drivers', 'hal', driver)
    # 如果本地hal目录下没有相应驱动，则从gitee仓库下载
    if not find_driver(hal_path, K_CONFIG, series):
        # print_info(f'start downloading {series} driver from gitee')
        with TemporaryDirectory(dir='./', prefix='drivers_info_') as dirname:
            repo = git.Repo.init(path=dirname)
            git_tool = repo.git
            git_tool.clone('--branch', OVERVIEW_REPOSITORY_BRANCH, OVERVIEW_REPOSITORY_URL)
            for root, dirs, files in os.walk(dirname):
                for file in files:
                    file_path = os.path.join(root, file)
                    os.chmod(file_path, 0o777)
            with open(os.path.join(dirname, os.path.join(OVERVIEW_REPOSITORY, JSON_FILE)), 'r', encoding='utf-8') as f:
                bsp_info = json.load(f)
                for key, value in bsp_info.get(manufactor).items():
                    if value.get('series') == series:
                        driver_url = value.get('url')
                        driver_version = value.get('version')
                        download_location = os.path.join(hal_path, key)
                        print_info(f"download {key} please wait")
                        with Progress('downloading', BarColumn(),
                                      "[progress.percentage]{task.percentage:>3.1f}%") as progress:
                            task = progress.add_task("downloading...", start=False, total=2)
                            git_tool.clone('--branch', driver_version, driver_url, download_location)
                            progress.update(task, completed=100)
                        print_info('download success')
                        return
                print_warn('未找到匹配BSP文件，请前往社区联系开发人员补充')


def find_driver(directory, target_filename, series):
    """
    遍历hal下指定目录所有的kconfig文件，推断是否有支持该板子系列的驱动
    """
    # print_info(f'directory:{directory},target_file:{target_filename},series:{series}')
    # nxp驱动kconfig编写不规范，做相应适配特殊处理
    if series == LPC5500:
        full_path = os.path.join(directory, LPC5500.lower())
        return os.path.exists(full_path) and os.listdir(full_path)
    elif series == IMXRT10XX:
        full_path = os.path.join(directory, IMXRT10XX.lower())
        return os.path.exists(full_path) and os.listdir(full_path)

    for root, dirs, files in os.walk(directory):
        if target_filename in files:
            with open(os.path.join(root, target_filename), 'r') as f:
                for ss in f.readlines():
                    if f'config SERIES_{series}' in ss:
                        print_info('driver exists locally')
                        return True
    print_info('lack of driver locally')
    return False


def find_file(directory, target_filename):
    for root, dirs, files in os.walk(directory):
        if target_filename in files:
            return os.path.join(root, target_filename)
