#ifndef __CLI_INPUT_H__
#define __CLI_INPUT_H__

typedef enum {
    cli_input_num,
    cli_input_bool
} cli_input_t;


int cli_input_receive(cli_input_t type, int *console_key);

void cli_input_init(void);

#endif
