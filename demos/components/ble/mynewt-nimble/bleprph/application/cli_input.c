#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include <os_errno.h>
#include <os_clock.h>
#include <os_sem.h>
#include <shell.h>

#include "cli_input.h"

os_semaphore_id gs_sem_cli_input;
static char gs_console_key[17];

int cli_input_receive(cli_input_t type, int *console_key)
{
    char *p;

    for (;;)
    {
        if (OS_BUSY == os_semaphore_wait(gs_sem_cli_input, OS_NO_WAIT))
        {
            break;
        }
    }
    if (OS_SUCCESS == os_semaphore_wait(gs_sem_cli_input, os_tick_from_ms(80000)))
    {
        switch (type)
        {
        case cli_input_num:
            p = gs_console_key;
            while (*p)
            {
                if (isalpha(*p))
                {
                    return 0;
                }
                p++;
            }
            sscanf(gs_console_key, "%d", console_key);
            return 1;
            break;
        case cli_input_bool:
            if ((strcasecmp(gs_console_key, "Y") == 0) || (strcasecmp(gs_console_key, "Yes") == 0))
            {
                *console_key = 1;
                return 1;
            }
            else if ((strcasecmp(gs_console_key, "N") == 0) || (strcasecmp(gs_console_key, "No") == 0))
            {
                *console_key = 0;
                return 1;
            }
            else
            {
                return 0;
            }
            break;
        }
        return 1;
    }
    else
    {
        return 0;
    }
}

static os_err_t cli_input(int32_t argc, char **argv)
{
    int len = strlen(argv[1]);

    if (2 != argc)
    {
        return OS_INVAL;
    }
    if (16 < len)
    {
        return OS_FAILURE;
    }
    memcpy(gs_console_key, argv[1], len);
    gs_console_key[len] = '\0';
    os_semaphore_post(gs_sem_cli_input);

    return 0;
}

void cli_input_init(void)
{
    gs_sem_cli_input = os_semaphore_create(OS_NULL, "gs_sem_cli_input", 0, OS_SEM_MAX_VALUE);
}

SH_CMD_EXPORT(cli_input, cli_input, "cli input");