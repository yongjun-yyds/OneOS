/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

#include <os_assert.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include <oneos_config.h>
#include "nimble-console/console.h"
#include "shell.h"
#include "os/os.h"
#include "sysinit/sysinit.h"
#include "log/log.h"
#include "host/ble_hs.h"
#include "host/util/util.h"
#include "services/gap/ble_svc_gap.h"
#include "store/ram/ble_store_ram.h"

#include <os_task.h>

static uint8_t g_own_addr_type;
static uint16_t conn_handle;
static const char *device_name = "Mynewt";

/* adv_event() calls advertise(), so forward declaration is required */
static void advertise(void);

#define MODLOG_DFLT(ml_lvl_, ...) console_printf(__VA_ARGS__)

#define USE_PASSKEY 0

static void
set_ble_addr(void)
{
    int rc;
    ble_addr_t addr;

    /* generate new non-resolvable private address */
    rc = ble_hs_id_gen_rnd(1, &addr);
    OS_ASSERT(rc == 0);

    /* set generated address */
    rc = ble_hs_id_set_rnd(addr.val);
    OS_ASSERT(rc == 0);
}


static int
adv_event(struct ble_gap_event *event, void *arg)
{
    int rc;
    struct ble_gap_conn_desc desc;
    
    switch (event->type) {
    case BLE_GAP_EVENT_ADV_COMPLETE:
        MODLOG_DFLT(INFO,"Advertising completed, termination code: %d\n",
                    event->adv_complete.reason);
        advertise();
        break;
    case BLE_GAP_EVENT_CONNECT:
        if (event->connect.status == 0) {
            MODLOG_DFLT(INFO, "connection %s; status=%d\n",
                        event->connect.status == 0 ? "established" : "failed",
                        event->connect.status);
        }
        else
        {
            MODLOG_DFLT(INFO, "Connection failed, error code: %i\n",
                        event->connect.status);
        }
        break;
    case BLE_GAP_EVENT_CONN_UPDATE_REQ:
        /* connected device requests update of connection parameters,
           and these are being filled in - NULL sets default values */
        MODLOG_DFLT(INFO, "updating conncetion parameters...\n");
        event->conn_update_req.conn_handle = conn_handle;
        event->conn_update_req.peer_params = NULL;
        MODLOG_DFLT(INFO, "connection parameters updated!\n");
        break;
    case BLE_GAP_EVENT_DISCONNECT:
        MODLOG_DFLT(INFO, "disconnect; reason=%d\n",
        event->disconnect.reason);

        /* reset conn_handle */
        conn_handle = BLE_HS_CONN_HANDLE_NONE;

        /* Connection terminated; resume advertising */
        advertise();
        break;

#if USE_PASSKEY
    case BLE_GAP_EVENT_PASSKEY_ACTION:
        MODLOG_DFLT(INFO, "PASSKEY_ACTION_EVENT started \n");
        struct ble_sm_io pkey = {0};
        int key = 0;
        int rc;

        if (event->passkey.params.action == BLE_SM_IOACT_INPUT)
        {
            MODLOG_DFLT(INFO, "Enter the passkey through console in this format-> cli_input 123456");
            pkey.action = event->passkey.params.action;
            pkey.passkey = 111111;
            rc = ble_sm_inject_io(event->passkey.conn_handle, &pkey);
            MODLOG_DFLT(INFO, "ble_sm_inject_io result: %d\n", rc);
        }
        break;

    case BLE_GAP_EVENT_REPEAT_PAIRING:
        /* We already have a bond with the peer, but it is attempting to
         * establish a new secure link.  This app sacrifices security for
         * convenience: just throw away the old bond and accept the new link.
         */

        MODLOG_DFLT(INFO, "BLE_GAP_EVENT_REPEAT_PAIRING started \n");

        /* Delete the old bond. */
        rc = ble_gap_conn_find(event->repeat_pairing.conn_handle, &desc);
        assert(rc == 0);
        ble_store_util_delete_peer(&desc.peer_id_addr);

        /* Return BLE_GAP_REPEAT_PAIRING_RETRY to indicate that the host should
         * continue with the pairing operation.
         */
        return BLE_GAP_REPEAT_PAIRING_RETRY;
#endif

    default:
        MODLOG_DFLT(ERROR, "Advertising event not handled,"
                    "event code: %u\n", event->type);
        break;
    }
    return 0;
}

static void
advertise(void)
{
    int rc;

    /* set adv parameters */
    struct ble_gap_adv_params adv_params;
    struct ble_hs_adv_fields fields;
    /* advertising payload is split into advertising data and advertising
       response, because all data cannot fit into single packet; name of device
       is sent as response to scan request */
    struct ble_hs_adv_fields rsp_fields;

    /* fill all fields and parameters with zeros */
    memset(&adv_params, 0, sizeof(adv_params));
    memset(&fields, 0, sizeof(fields));
    memset(&rsp_fields, 0, sizeof(rsp_fields));

    adv_params.conn_mode = BLE_GAP_CONN_MODE_UND;
    adv_params.disc_mode = BLE_GAP_DISC_MODE_GEN;

    fields.flags = BLE_HS_ADV_F_DISC_GEN |
                   BLE_HS_ADV_F_BREDR_UNSUP;
    fields.uuids128 = BLE_UUID128(BLE_UUID128_DECLARE(
        0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77,
        0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff));
    fields.num_uuids128 = 1;
    fields.uuids128_is_complete = 0;;
    fields.tx_pwr_lvl = BLE_HS_ADV_TX_PWR_LVL_AUTO;

    rsp_fields.name = (uint8_t *)device_name;
    rsp_fields.name_len = strlen(device_name);
    rsp_fields.name_is_complete = 1;

    rc = ble_gap_adv_set_fields(&fields);
    OS_ASSERT(rc == 0);

    rc = ble_gap_adv_rsp_set_fields(&rsp_fields);

    MODLOG_DFLT(INFO,"Starting advertising...\n");

    rc = ble_gap_adv_start(g_own_addr_type, NULL, 100,
                           &adv_params, adv_event, NULL);
    OS_ASSERT(rc == 0);
}

static void
on_sync(void)
{
    int rc;

    set_ble_addr();

    /* g_own_addr_type will store type of addres our BSP uses */
    rc = ble_hs_util_ensure_addr(0);
    OS_ASSERT(rc == 0);
    rc = ble_hs_id_infer_auto(0, &g_own_addr_type);
    OS_ASSERT(rc == 0);
    /* begin advertising */
    advertise();
}

static void
on_reset(int reason)
{
    MODLOG_DFLT(INFO, "Resetting state; reason=%d\n", reason);
}

int
main(int argc, char **argv)
{
    int rc;

    /* Initialize all packages. */
    nimble_port_oneos_init();

    ble_hs_cfg.sync_cb = on_sync;
    ble_hs_cfg.reset_cb = on_reset;

    ble_store_ram_init();

    rc = ble_svc_gap_device_name_set(device_name);
    OS_ASSERT(rc == 0);

    /* As the last thing, process events from default event queue. */
    ble_hs_task_startup();

    while (1)
    {
        os_task_msleep(100);
    }

    return 0;
}
