
## Nimble注意事项

1. 使用Nrf52系列平台并且使能controller时，协议栈会使用RTC0和Timer0，所以在menuconfig里不能使能Drivers->HAL下面的timer0和rtc。
2. 如果选择keil编译，keil arm compiler版本需支持version 6，并在"Options->Target"下选择"Use default compiler version 6"，并取消对"Use MicroLIB"的勾选。
3. blecsc_host和blecsc_controller分别运行在stm32l475-atk-pandora和nrf52840-DK开发板上，通过串口进行通信，stm32l475-atk-pandora上使用uart1的PA9/PA10引脚，nrf52840-DK使用uart0的P0.06/P0.08引脚。

## BLE例程简介

低功耗蓝牙应用有四种基础的配置场景
- 广播者（Broadcaster）：开发板发送广播报文。
- 观察者（Observer）：开发板监听广播报文。
- 外围设备（peripheral）：开发板发送广播报文，并与中央设备建立连接。
- 中央设备（central）：开发板监听广播报文，并与外围设备建立连接。

NimBLE例程，advertiser、scanner、peripheral、central、blecsc、blecent、ext_advertiser、ext_scanner、blecsc_controller、blecsc_host、btshell、bleprph。

### 广播例程（advertiser）

程序运行后会一直进行"开始广播->广播->结束广播"过程，通过观察例程或者APP（LightBlue、nRF Connect）可以接收到广播信息，广播信息中包含设备名"Apache Mynewt"。


### 观察例程（scanner）

程序运行后会一直进行发现过程。配合广播例程使用时，在打印的信息中可以找到广播设备"Apache Mynewt"。因为存在其他低功耗蓝牙设备在广播，所以扫描到的设备会有很多其他设备。


### 外围设备例程（peripheral）

该例程没有实现具体服务，只是配合中央设备例程实现与中央设备建立、结束连接。程序运行后会一直进行"开始广播->广播->等待连接->建立连接->结束链接"（如果有中央设备进行扫描）或者"开始广播->广播->等待连接->结束广播"（如果没有设备进行扫描）过程，广播数据中包含特定信息，通过中央设备例程可以接收到广播信息并建立连接。


### 中央设备例程（central）

该例程配合外围设备例程使用，只是实现与外围设备建立、结束连接，不发现、使用服务。程序运行后会一直进行发现过程，如果扫描到广播设备则进行"判断是否应连接->进行连接->结束连接->重新发现"，在收到广播后会根据广播数据是否包含特定信息从而判断是否连接。


### 外围设备BLE Cycling Speed and Cadence peripheral app例程（blecsc）

该例程具有gatt服务，在手机上使用APP即可连接设备并使用服务。在ios上使用"LightBlue"即可。在APP内可以搜索到"blecsc_sensor"设备，点击连接。进入后点击"Cycling Speed and Cadence"下面的"0x2A5B"特性进行设置，点击"Listen for notification"后即可接收信息。

返回进入"0x2A55"特性进行设置，点击"Listen for notification"，然后点击"Write new value"，输入"0388"并确定，设备输出"SC Control Point; opcode=3  SC Control Point; Sensor location update =136"，同时在手机上将收到提示。

该例程具有两种配置。

模式1：使用 '.config' 配置，使用该配置时Nimble实现ble host和ble controller，运行在同一个mcu中。

模式2：使用 '.config_host_only' 配置，使用该配置时Nimble实现ble host, ble controller运行在另一个mcu中。这个例程需配合controller使用。controller使用nrf52实现ble controller部分的功能，blecsc_host在stm32上实现ble host部分的功能。他们通过uart或者usb cdc虚拟串口实现HCI uart接口从而完成这个ble协议栈的功能。对于stm32系列Oneos现在使用官方的usb协议栈，使用usb cdc时需打开usb host功能并选择'enable usbh specific cdc class',并将'usbh_specific_cdc.c'中的内容替换到'drivers/usb/usb_host/Class/Specific_CDC/Src/usbh_specific_cdc.c'中。


### blecent

该例程扫描并分析广播，当发现广播中带有"Alert Notification"的uuid时，连接该设备并启动发现服务规程。打开LightBlue，在"Virtual Devices"中添加"Alert Notification"并使能，该例程将连接APP。


### bleprph

该例程需要开启easyflash组件。该例程需配合shell使用。使用手机APP扫描到设备并连接后，访问特定特性时需要用户在设备shell中输入APP中显示的PIN码，输入成功后，设备即与APP完成绑定。当设备重启后使用APP再次连接并访问特性时将不再需要输入密码。当前协议栈只支持使用easyflash组件来保存配置。

### 拓展广播例程（ext_advertiser）

程序运行后会依次开始5个广播实例。

第1个实例是使用Extended PDUs的Non-connectable and non-scannable undirected event，primary phy是1M，secondary phy是2M，使用固定广播地址"CC,BB, AA, 33, 22, 11"，在广播数据中有设备名"NimBLE-NonConn"，开始后会一直进行广播，不会停止。

第2个实例是使用Extended PDUs的Scannable undirected event，primary phy是coded phy，secondary phy是2M，使用不可解析私有广播地址，在扫描应答数据中有设备名"NimBLE-Scann"，开始后会一直进行广播，不会停止；

第3个实例是使用Legacy PUD的Scannable undirected event，使用不可解析私有广播地址，在广播PDU数据中有设备名"NimBLE-ScannLegacy"，在扫描应答数据中有设备名"NimBLE-ScannLegacyResp"，开始后会一直进行广播，不会停止；

第4个实例是使用使用Legacy PUD的Non-connectable and non-scannable undirected event，程序运行后会一直进行"开始广播->广播->结束广播"过程，在广播数据中有设备名"NimBLE-Legacy"；

第5个实例是使用Extended PDUs的Non-connectable and non-scannable undirected event，primary phy是1M，secondary phy是1M，使用不可解析私有广播地址，在广播数据中有设备名"NimBLE-ExtMax"，程序运行后会一直进行"开始广播->广播->结束广播"过程。


### 拓展观察例程（ext_scanner）

本例程配合拓展广播例程使用。程序运行后会一直进行发现过程，在打印的信息中可以找到扫描到的广播设备，包括"NimBLE-NonConn"、"NimBLE-Scann"、"NimBLE-ScannLegacy"、"NimBLE-ScannLegacyResp"、"NimBLE-Legacy"、"NimBLE-ExtMax"，扫描应答数据中的设备只会被打印一次，其他会一直打印。


### controller

controller实现了ble 5 spec中controller的基本功能。

该例程具有两种模式。

模式1：使用'.config_tinyusb_bth'配置，通过hci usb连接其他设备。

模式2：使用'.config_uart'配置，通过hci uart连接其他设备。hci uart可以通过串口和tinyusb cdc虚拟串口来实现。使用tinyusb cdc时，打开tinyusb，选择'tinyusb device user_example'。


### btshell

该例程实现使用shell调用协议栈接口函数的功能。具体使用方法参见"https://mynewt.apache.org/latest/network/btshell/btshell_api.html"。


## BLE Mesh例程简介

### blemesh_light

blemesh_light是一个简单的ble mesh的demo，该demo实现在"components/ble/mynewt-nimble/nimble/host/mesh/src/shell.c"中，通过menuconfig在"Components → BLE → NimBLE → Mesh"中设置"Configuration client model"和"Shell demo model implementation"可以开启关闭该demo。程序启动后，在shell中输入"ble_mesh_shell init"以初始化mesh协议栈。mesh协议栈初始化后，可以输入"pb-gatt on"以使能pb-gatt，并配合nRF Mesh软件使用。

基于该例程，简单介绍Nimble低功耗蓝牙协议栈proxy、relay、friend、lpn四特性的使用方法。

#### Proxy

Proxy默认已打开。使用"nRF mesh"添加device后，进入"Elements->Element 1->Modles->Configuration Server"，刷新页面，并点击"GATT Proxy"，之后可在"Node Features"看到"Proxy: Enabled"。

#### Relay

Relay默认已打开。使用"nRF mesh"添加device后，进入"Elements->Element 1->Modles->Configuration Server"，刷新页面，并点击"Set Relay"，之后可在"Node Features"看到"Relay: Enabled"。

#### Friend

Friend需手动打开。使用menuconfig选中"Components→ BLE→ NimBLE→ HOST→ Mesh→ Friend setting"中的"Friend Node"。 使用"nRF mesh"添加device后，进入"Elements->Element 1->Modles->Configuration Server"，刷新页面，并点击"FRIEND FEATURE"，之后可在"Node Features"看到"Friend: Enabled"。

#### Lpn

Lpn需手动打开。使用menuconfig选中"Components→ BLE→ NimBLE→ HOST→ Mesh→ LPN settings"中的" Low Power Node"。在" Components→ BLE→ NimBLE→ HOST→ Mesh→ LPN settings"中选中"LPN functionality Automatically enable"，device将在privision完成后自动进入lpn模式。或者在节点通过privision完成后调用bt_mesh_lpn_set(1)进入lpn模式，寻找friend。

#### Mesh settings storage enable

该项位于"Components→ BLE→ HOST→ Mesh→ storage"中。当使能该项时，mesh相关配置会保存在Flash中，设备断电后复位将读取Flash中的配置，该项默认打开，关闭后配置将不会被保存到Flash中。当前协议栈只支持使用easyflash组件来保存配置。

### easyflash配置

以下操作均基于nrf52840。

1. 在menuconfig中使能"Components→ Easyflash→ EasyFlash"项，并且使能"on chip flash",设置"The flash erase granularity"为4096，"ENV area size"为8192。
2. 在$project/board/ports/fal_cfg.c文件中的fal_part_info添加配置项，如下。

```c
static const fal_part_info_t fal_part_info[] = 
{
    /* part,       flash,       addr,       size,                       lock */
    { "cfg"      , "onchip_flash" , 0xc0000, 0x00001000, FAL_PART_INFO_FLAGS_UNLOCKED},
    { "easyflash", "onchip_flash" , 0xc1000, 0x00002000, FAL_PART_INFO_FLAGS_UNLOCKED},
#ifdef BSP_USING_I2C_AT24CXX
    { "eeprom", "at24cxx_eeprom", 0x00000000, 0x00000100,   FAL_PART_INFO_FLAGS_UNLOCKED},
#endif
};
```

3. 更改$project/board/ports/flash_info.c文件中的page_size的值为4。