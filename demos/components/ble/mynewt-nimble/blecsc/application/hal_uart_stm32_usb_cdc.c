#include <drv_cfg.h>
#include <shell.h>

#include <oneos_config.h>
#include <os_errno.h>
#include <os_task.h>
#include <os_event.h>

#include "hal/hal_uart.h"

#define HAL_UART_TX_BUF_MAX 0x10
#define HAL_UART_RX_BUF_MAX 0x20

struct hal_uart_cfg
{
    hal_uart_tx_char tx_func;
    hal_uart_rx_char rx_func;
    uint8_t          tx_buf[HAL_UART_TX_BUF_MAX];
    uint8_t          rx_buf[HAL_UART_RX_BUF_MAX];
    uint8_t          tx_cnt;
};

os_device_t *hal_uart;

struct hal_uart_cfg hal_uart_cfg;

void hal_uart_task(void *parameter)
{
    int      i;
    uint8_t  ch;
    int      count;
    uint8_t *buf;

    buf = hal_uart_cfg.rx_buf;
    while (1)
    {
        count = os_device_read_block(hal_uart, 0, buf, HAL_UART_RX_BUF_MAX);
        if (0 < count)
        {
            for (i = 0; i < count; i++)
            {
                ch = buf[i];
                hal_uart_cfg.rx_func(NULL, ch);
            }
        }
    }
}

void hal_uart_start_tx(int port)
{
    int      i;
    int      data;
    int      count;
    uint8_t *buf;

    buf = hal_uart_cfg.tx_buf;
    while (1)
    {
        for (i = 0; i < HAL_UART_TX_BUF_MAX; i++)
        {
            data = hal_uart_cfg.tx_func(NULL);
            if (data < 0)
            {
                break;
            }
            buf[i] = (uint8_t)data;
        }
        count = i;

        if (0 < count)
        {
            if(!hal_uart) {
                continue;
            }
            hal_uart_cfg.tx_cnt = count;
            os_device_write_block(hal_uart, 0, buf, count);
        }
        else
        {
            break;
        }
    }
}

int hal_uart_close(int uart)
{
    return 0;    // success
}

int hal_uart_init_cbs(int port, hal_uart_tx_char tx_func, hal_uart_tx_done tx_done, hal_uart_rx_char rx_func, void *arg)
{
    hal_uart_cfg.rx_func = rx_func;
    hal_uart_cfg.tx_func = tx_func;
    return 0;
}

int hal_uart_config(int                    port,
                    int32_t                baudrate,
                    uint8_t                databits,
                    uint8_t                stopbits,
                    enum hal_uart_parity   parity,
                    enum hal_uart_flow_ctl flow_ctl)
{
    static int flag = 0;

    // only init once.
    if (!flag)
    {
        // hal_uart = os_device_open_s("usbh_cdc_hs");
        // OS_ASSERT(hal_uart);
        while(!(hal_uart = os_device_open_s("usbh_cdc_fs"))) {
            os_task_msleep(1000);
        }
        flag = 1;
        os_task_msleep(2000);

        os_task_id task;
        task = os_task_create(OS_NULL, OS_NULL, 1024, "hal_uart", hal_uart_task, OS_NULL, 6);
        OS_ASSERT(OS_NULL != task);
        os_task_startup(task);
    }
    return 0;
}

int hal_uart_init(int uart, void *cfg)
{
    return 0;
}
