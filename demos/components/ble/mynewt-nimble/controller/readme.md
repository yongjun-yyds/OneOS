## config_uart

This config for controller use uart as hci.

## config_tinyusb_bth

This config for controller use tinyusb bth as hci.

The controller using tinyusb bth as hci can work with Ubuntu. You can use 'python_ble.py' to find device that runs demo 'blecsc'. 

If meet error "bluepy.btle.BTLEManagementError: Failed to execute management command 'scanend' (code: 11, error: Rejected)", run 'bluetoothctl power on'. 

Finally, you can find "blecsc_sensor" in print.

```shell
Device e4:22:33:aa:bb:cc (public), RSSI=-27 dB
  Flags = 06
  Complete Local Name = blecsc_sensor
[1, 12]
  00001816-0000-1000-8000-00805f9b34fb
    00002a5b-0000-1000-8000-00805f9b34fb
    00002a5c-0000-1000-8000-00805f9b34fb
    00002a5d-0000-1000-8000-00805f9b34fb
    00002a55-0000-1000-8000-00805f9b34fb
  0000180a-0000-1000-8000-00805f9b34fb
    00002a29-0000-1000-8000-00805f9b34fb
    00002a24-0000-1000-8000-00805f9b34fb
  Tx Power = 00
  Appearance = 8504
```
