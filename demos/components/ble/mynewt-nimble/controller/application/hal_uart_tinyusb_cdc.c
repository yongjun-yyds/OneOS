#include <drv_cfg.h>
#include <shell.h>

#include <oneos_config.h>
#include <os_errno.h>
#include <os_task.h>

#include "hal/hal_uart.h"
#include "cdc_device.h"

#define HAL_UART_TX_BUF_MAX 0x40
#define HAL_UART_RX_BUF_MAX 0x40

struct hal_uart_cfg
{
    hal_uart_tx_char tx_func;
    hal_uart_rx_char rx_func;
    uint8_t tx_buf[HAL_UART_TX_BUF_MAX];
    uint8_t rx_buf[HAL_UART_RX_BUF_MAX];
    uint8_t tx_cnt;
};

os_device_t *hal_uart;

struct hal_uart_cfg hal_uart_cfg;

void hal_uart_read(const char * buffer, uint32_t bufsiz) {
    int i;
    uint8_t ch;

    for (i = 0; i < bufsiz; i++)
    {
        ch = buffer[i];
        hal_uart_cfg.rx_func(NULL, ch);
    }
}

#include "usbd.h"
void hal_uart_start_tx(int port)
{
    int i;
    int data;
    int count;
    uint8_t *buf;

    buf = hal_uart_cfg.tx_buf;
    while(1) {
        for (i = 0; i < HAL_UART_TX_BUF_MAX; i++)
        {
            data = hal_uart_cfg.tx_func(NULL);
            if (data < 0)
            {
                break;
            }
            buf[i] = (uint8_t)data;
        }
        count = i;

        if (0 < count)
        {
            if(!tud_ready()) {
                continue;
            }
            hal_uart_cfg.tx_cnt = count;
            tud_cdc_write(buf, count);
            tud_cdc_write_flush();
        }
        else {
            break;
        }
    }
}

int hal_uart_close(int uart)
{
    return 0; // success
}

int hal_uart_init_cbs(int port, hal_uart_tx_char tx_func, hal_uart_tx_done tx_done,
                      hal_uart_rx_char rx_func, void *arg)
{
    hal_uart_cfg.rx_func = rx_func;
    hal_uart_cfg.tx_func = tx_func;
    return 0;
}

int hal_uart_config(int port, int32_t baudrate, uint8_t databits, uint8_t stopbits,
                    enum hal_uart_parity parity, enum hal_uart_flow_ctl flow_ctl)
{
    return 0;
}

int hal_uart_init(int uart, void *cfg)
{
    return 0;
}

