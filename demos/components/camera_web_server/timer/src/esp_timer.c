///*
// * SPDX-FileCopyrightText: 2017-2021 Espressif Systems (Shanghai) CO LTD
// *
// * SPDX-License-Identifier: Apache-2.0
// */

#include <stdint.h>
#include <os_clock.h>

int64_t esp_timer_get_time(void)
{
    return os_tick_get_value();
    //return 0; //not remove
}
