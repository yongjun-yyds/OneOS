# Camera Demo API手册

------

## 简介

Camera_demo移植自乐鑫，http server部分移植自乐鑫 IoT 开发框架v4.3.4 ([esp-idf](https://github.com/espressif/esp-idf))，camera driver部分移植自乐鑫esp32-camera-2.0.2([esp32-camera](https://github.com/espressif/esp32-camera)) 、camare demo部分移植自Arduino AI Thinker ESP32-CAM。

具有以下功能特点：

1. 设备做http server端；
2. 局域网以太有线网络；
3. 支持多款摄像头（OV5640、OV2640）；
4. 支持展示摄像头图片；
5. 支持网页配置摄像头参数；

------

## 重要定义及数据结构

### camera_config_t

摄像头相关的配置信息保存在camera_config_t结构体中，其定义如下：

```c
typedef struct {
    int pin_pwdn;                   /*!< GPIO pin for camera power down line */
    int pin_reset;                  /*!< GPIO pin for camera reset line */
    int pin_xclk;                   /*!< GPIO pin for camera XCLK line */
    union {
        int pin_sccb_sda;           /*!< GPIO pin for camera SDA line */
        int pin_sscb_sda __attribute__((deprecated("please use pin_sccb_sda instead")));           /*!< GPIO pin for camera SDA line (legacy name) */
    };
    union {
        int pin_sccb_scl;           /*!< GPIO pin for camera SCL line */
        int pin_sscb_scl __attribute__((deprecated("please use pin_sccb_scl instead")));           /*!< GPIO pin for camera SCL line (legacy name) */
    };
    int pin_d7;                     /*!< GPIO pin for camera D7 line */
    int pin_d6;                     /*!< GPIO pin for camera D6 line */
    int pin_d5;                     /*!< GPIO pin for camera D5 line */
    int pin_d4;                     /*!< GPIO pin for camera D4 line */
    int pin_d3;                     /*!< GPIO pin for camera D3 line */
    int pin_d2;                     /*!< GPIO pin for camera D2 line */
    int pin_d1;                     /*!< GPIO pin for camera D1 line */
    int pin_d0;                     /*!< GPIO pin for camera D0 line */
    int pin_vsync;                  /*!< GPIO pin for camera VSYNC line */
    int pin_href;                   /*!< GPIO pin for camera HREF line */
    int pin_pclk;                   /*!< GPIO pin for camera PCLK line */

    int xclk_freq_hz;               /*!< Frequency of XCLK signal, in Hz. EXPERIMENTAL: Set to 16MHz on ESP32-S2 or ESP32-S3 to enable EDMA mode */

    ledc_timer_t ledc_timer;        /*!< LEDC timer to be used for generating XCLK  */
    ledc_channel_t ledc_channel;    /*!< LEDC channel to be used for generating XCLK  */

    pixformat_t pixel_format;       /*!< Format of the pixel data: PIXFORMAT_ + YUV422|GRAYSCALE|RGB565|JPEG  */
    framesize_t frame_size;         /*!< Size of the output image: FRAMESIZE_ + QVGA|CIF|VGA|SVGA|XGA|SXGA|UXGA  */

    int jpeg_quality;               /*!< Quality of JPEG output. 0-63 lower means higher quality  */
    size_t fb_count;                /*!< Number of frame buffers to be allocated. If more than one, then each frame will be acquired (double speed)  */
    camera_fb_location_t fb_location; /*!< The location where the frame buffer will be allocated */
    camera_grab_mode_t grab_mode;   /*!< When buffers should be filled */
#if CONFIG_CAMERA_CONVERTER_ENABLED
    camera_conv_mode_t conv_mode;   /*!< RGB<->YUV Conversion mode */
#endif

    int sccb_i2c_port;              /*!< If pin_sccb_sda is -1, use the already configured I2C bus by number */
} camera_config_t;
```

| **重要成员** | **说明** |
| :----------- | :------- |
|              |          |
|              |          |
|              |          |
|              |          |
|              |          |
|              |          |
|              |          |
|              |          |
|              |          |
|              |          |

### httpd_handle_t

http server句柄，用于标识http服务，其定义如下：

```c
typedef void* httpd_handle_t;
```

| **重要成员** | **说明** |
| :----------- | :------- |
| 无           | 无       |

### httpd_config

用于保存http server配置，包含任务优先级、端口、最大连接数等配置，其定义如下：

```c
typedef struct httpd_config {
    unsigned    task_priority;      /*!< Priority of FreeRTOS task which runs the server */
    size_t      stack_size;         /*!< The maximum stack size allowed for the server task */
    BaseType_t  core_id;            /*!< The core the HTTP server task will run on */

    /**
     * TCP Port number for receiving and transmitting HTTP traffic
     */
    uint16_t    server_port;

    /**
     * UDP Port number for asynchronously exchanging control signals
     * between various components of the server
     */
    uint16_t    ctrl_port;

    uint16_t    max_open_sockets;   /*!< Max number of sockets/clients connected at any time*/
    uint16_t    max_uri_handlers;   /*!< Maximum allowed uri handlers */
    uint16_t    max_resp_headers;   /*!< Maximum allowed additional headers in HTTP response */
    uint16_t    backlog_conn;       /*!< Number of backlog connections */
    bool        lru_purge_enable;   /*!< Purge "Least Recently Used" connection */
    uint16_t    recv_wait_timeout;  /*!< Timeout for recv function (in seconds)*/
    uint16_t    send_wait_timeout;  /*!< Timeout for send function (in seconds)*/

    /**
     * Global user context.
     *
     * This field can be used to store arbitrary user data within the server context.
     * The value can be retrieved using the server handle, available e.g. in the httpd_req_t struct.
     *
     * When shutting down, the server frees up the user context by
     * calling free() on the global_user_ctx field. If you wish to use a custom
     * function for freeing the global user context, please specify that here.
     */
    void * global_user_ctx;

    /**
     * Free function for global user context
     */
    httpd_free_ctx_fn_t global_user_ctx_free_fn;

    /**
     * Global transport context.
     *
     * Similar to global_user_ctx, but used for session encoding or encryption (e.g. to hold the SSL context).
     * It will be freed using free(), unless global_transport_ctx_free_fn is specified.
     */
    void * global_transport_ctx;

    /**
     * Free function for global transport context
     */
    httpd_free_ctx_fn_t global_transport_ctx_free_fn;

    /**
     * Custom session opening callback.
     *
     * Called on a new session socket just after accept(), but before reading any data.
     *
     * This is an opportunity to set up e.g. SSL encryption using global_transport_ctx
     * and the send/recv/pending session overrides.
     *
     * If a context needs to be maintained between these functions, store it in the session using
     * httpd_sess_set_transport_ctx() and retrieve it later with httpd_sess_get_transport_ctx()
     *
     * Returning a value other than ESP_OK will immediately close the new socket.
     */
    httpd_open_func_t open_fn;

    /**
     * Custom session closing callback.
     *
     * Called when a session is deleted, before freeing user and transport contexts and before
     * closing the socket. This is a place for custom de-init code common to all sockets.
     *
     * Set the user or transport context to NULL if it was freed here, so the server does not
     * try to free it again.
     *
     * This function is run for all terminated sessions, including sessions where the socket
     * was closed by the network stack - that is, the file descriptor may not be valid anymore.
     */
    httpd_close_func_t close_fn;

    /**
     * URI matcher function.
     *
     * Called when searching for a matching URI:
     *     1) whose request handler is to be executed right
     *        after an HTTP request is successfully parsed
     *     2) in order to prevent duplication while registering
     *        a new URI handler using `httpd_register_uri_handler()`
     *
     * Available options are:
     *     1) NULL : Internally do basic matching using `strncmp()`
     *     2) `httpd_uri_match_wildcard()` : URI wildcard matcher
     *
     * Users can implement their own matching functions (See description
     * of the `httpd_uri_match_func_t` function prototype)
     */
    httpd_uri_match_func_t uri_match_fn;
} httpd_config_t;
```

| **重要成员**                 | **说明**                           |
| :--------------------------- | :--------------------------------- |
| task_priority                | 任务优先级                         |
| stack_size                   | 任务栈大小                         |
| core_id                      | 多核场景，core配置                 |
| server_port                  | 服务端口                           |
| max_open_sockets             | 最大允许连接数                     |
| max_resp_headers             | http应答报文中最大允许添加的头数量 |
| backlog_conn                 | 最大连接队列                       |
| lru_purge_enable             | 使能最近一次的连接                 |
| recv_wait_timeout            | socket接收超时时间                 |
| send_wait_timeout            | socket发送超时时间                 |
| global_user_ctx              | 用户上下文                         |
| global_user_ctx_free_fn      | 释放用户上下文资源                 |
| global_transport_ctx         | 加密场景的传输上下文资源           |
| global_transport_ctx_free_fn | 释放加密场景的传输上下文资源       |
| open_fn                      | 自定义会话打开                     |
| close_fn                     | 自定义会话关闭                     |
| uri_match_fn                 | URI资源匹配的回调                  |
| max_uri_handlers             | 最大允许添加的URI资源              |

### httpd_uri_t

uri资源属性结构体，用于在http server注册相关的可用资源，如下：

```c
typedef struct httpd_uri {
    const char       *uri;    /*!< The URI to handle */
    httpd_method_t    method; /*!< Method supported by the URI */

    /**
     * Handler to call for supported request method. This must
     * return ESP_OK, or else the underlying socket will be closed.
     */
    esp_err_t (*handler)(httpd_req_t *r);

    /**
     * Pointer to user context data which will be available to handler
     */
    void *user_ctx;

#ifdef CONFIG_HTTPD_WS_SUPPORT
    /**
     * Flag for indicating a WebSocket endpoint.
     * If this flag is true, then method must be HTTP_GET. Otherwise the handshake will not be handled.
     */
    bool is_websocket;

    /**
     * Flag indicating that control frames (PING, PONG, CLOSE) are also passed to the handler
     * This is used if a custom processing of the control frames is needed
     */
    bool handle_ws_control_frames;

    /**
     * Pointer to subprotocol supported by URI
     */
    const char *supported_subprotocol;
#endif
} httpd_uri_t;
```

| **重要成员** | **说明**         |
| :----------- | :--------------- |
| uri          | URI资源字符串    |
| method       | 资源请求方式     |
| handler      | 资源请求回调     |
| user_ctx     | 用户上下文       |

## API列表

| **接口**                   | **说明**                  |
| :------------------------- | :------------------------ |
| esp_camera_init            | 摄像头硬件初始化          |
| esp_camera_sensor_get      |                           |
| startCameraServer          | 开启摄像头相关web服务     |
| httpd_start                | 开启http server           |
| httpd_register_uri_handler | 向http server注册可用资源 |

## esp_camera_init

该函数用于摄像头硬件初始化，其函数原型如下:

```c
os_err_t esp_camera_init(const camera_config_t *config);
```

| **参数** | **说明**                              |
| :------- | :------------------------------------ |
| config   |                                       |
| **返回** | **说明**                              |
| os_err_t | 成功：OS_SUCCESS； 失败：非OS_SUCCESS |

## esp_camera_sensor_get

该函数用于，其函数原型如下:

```c
sensor_t *esp_camera_sensor_get(void);
```

| **参数**   | **说明** |
| :--------- | :------- |
| 无         | 无       |
| **返回**   | **说明** |
| sensor_t * |          |

## startCameraServer

该函数用于开启摄像头web服务，函数原型如下：

```c
int startCameraServer(void);
```

| **参数** | **说明**                     |
| :------- | :--------------------------- |
| 无       | 无                           |
| **返回** | **说明**                     |
| int      | 成功：ESP_OK；失败：ESP_FAIL |

## httpd_start

该函数用于开启web server，其函数原型如下：

```c
esp_err_t httpd_start(httpd_handle_t *handle, const httpd_config_t *config);
```

| **参数**  | **说明**                     |
| :-------- | :--------------------------- |
| handle    | http server句柄              |
| config    | http server配置              |
| **返回**  | **说明**                     |
| esp_err_t | 成功：ESP_OK；失败：非ESP_OK |

## httpd_register_uri_handler

该函数用于在web服务注册可用资源，其函数原型如下：

```c
esp_err_t httpd_register_uri_handler(httpd_handle_t handle, const httpd_uri_t *uri_handler);
```

| **参数**       | **说明**                     |
| :------------- | :--------------------------- |
| httpd_handle_t | http server句柄              |
| httpd_uri_t    | uri资源属性                  |
| **返回**       | **说明**                     |
| esp_err_t      | 成功：ESP_OK；失败：非ESP_OK |
