# Camera Demo用户指南

------

## 简介

Camera_demo移植自乐鑫，http server部分移植自乐鑫 IoT 开发框架v4.3.4 ([esp-idf](https://github.com/espressif/esp-idf))，camera driver部分移植自乐鑫esp32-camera-2.0.2([esp32-camera](https://github.com/espressif/esp32-camera)) 、camare demo部分移植自Arduino AI Thinker ESP32-CAM。

具有以下功能特点：

1. 设备做http server端；
2. 局域网以太有线网络；
3. 支持多款摄像头（OV5640、OV2640）；
4. 支持展示摄像头图片；
5. 支持网页配置摄像头参数；

## 目录结构

Camera_Demo源代码目录结构如下表所示：

| 目录               | 说明                 |
| ------------------ | -------------------- |
| cam_web_server_app | camera应用实现       |
| camera-2.0.2       | jpg解码、yuv转码实现 |
| http_server        | http部分实现         |
| common             | 错误码相关           |
| log                | log相关实现          |
| nghttp             | http部分实现         |
| timer              | 时间相关             |

## 使用说明

### 图形化配置

使用Camera_Demo需要通过Menuconfig的图形化工具进行配置选择，配置的路径如下所示：

```
(Top) → Demos→ Component demos→ Camera web server
                              OneOS Configuration
[*] Enable camera web server
        HTTP Server  --->
        Camera web server application  --->
        Log output  --->
```

进行Camera_Demo选项配置需要先在Menuconfig中选中Enable camera web server，然后再进行其他的配置选择。

- HTTP Server：http server 配置。
- Camera web server application：使能camera应用示例。
- Log output：打印配置。

### API使用说明手册

[Camera_Demo API使用说明手册](doc/camera_demo_api.md)

## 编译选项

## 注意事项

#### 1.配置相关

- 默认配置支持keil编译，版本在5.31.0以上。
- GCC编译支持需要手动去勾选 OS_USING_CPLUSPLUS11，但与keil互斥。
- 因为设备端是http server端，需要更改lwip配置。
  (16)    the number of struct netconns
  (32)    the number of PBUF
  (10)    the number of raw connection
  (10)    the number of UDP socket
  (10)    the number of TCP socket
  (60)    the number of TCP segment
  (8196)  the size of send buffer
  (8196)  the size of TCP send window
  (10)    the priority level value of lwIP task
  (16)    the number of mail in the lwIP task mailbox

- GCC编译支持需要手动去勾选 OS_USING_CPLUSPLUS11，但与keil互斥。
- 最好降低user任务优先级到20。

#### 2.使用相关

- 同一局域网内，需要查询设备获取到ip后再开启demo。
- 页面配置的部分功能是不支持的，取决于该摄像头的功能支持。



