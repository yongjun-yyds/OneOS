/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        camera_web_server_app.c
 *
 * @brief       User application entry
 *
 * @revision
 * Date         Author          Notes
 * 2022-11-10   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <os_util.h>
#include <os_errno.h>
#include <os_task.h>
#include <drv_gpio.h>
#include "esp_err.h"
#include "camera.h"
#include "app_httpd.h"
#if defined(NET_USING_LWIP)
#include "lwip/netif.h"
#elif defined(NET_USING_MOLINK)
#include "mo_api.h"
#endif

//
// WARNING!!! PSRAM IC required for UXGA resolution and high JPEG quality
//            Ensure ESP32 Wrover Module or other board with PSRAM is selected
//            Partial images will be transmitted if image exceeds buffer size
//
//            You must select partition scheme from the board menu that has at least 3MB APP space.
//            Face Recognition is DISABLED for ESP32 and ESP32-S2, because it takes up from 15 
//            seconds to process single frame. Face Detection is ENABLED if PSRAM is enabled as well

// ===================
// Select camera model
// ===================
//#define CAMERA_MODEL_WROVER_KIT // Has PSRAM
//#define CAMERA_MODEL_ESP_EYE // Has PSRAM
//#define CAMERA_MODEL_ESP32S3_EYE // Has PSRAM
//#define CAMERA_MODEL_M5STACK_PSRAM // Has PSRAM
//#define CAMERA_MODEL_M5STACK_V2_PSRAM // M5Camera version B Has PSRAM
//#define CAMERA_MODEL_M5STACK_WIDE // Has PSRAM
//#define CAMERA_MODEL_M5STACK_ESP32CAM // No PSRAM
//#define CAMERA_MODEL_M5STACK_UNITCAM // No PSRAM
#define CAMERA_MODEL_AI_THINKER // Has PSRAM
//#define CAMERA_MODEL_TTGO_T_JOURNAL // No PSRAM
// ** Espressif Internal Boards **
//#define CAMERA_MODEL_ESP32_CAM_BOARD
//#define CAMERA_MODEL_ESP32S2_CAM_BOARD
//#define CAMERA_MODEL_ESP32S3_CAM_LCD


#define CAM_PIN_PWDN 1002  //power down is not used
#define CAM_PIN_RESET 15 //software reset will be performed
#define CAM_PIN_XCLK -1
#define CAM_PIN_SIOD 20
#define CAM_PIN_SIOC 19

#define CAM_PIN_D7 GET_PIN(B,9)
#define CAM_PIN_D6 GET_PIN(B,8)
#define CAM_PIN_D5 GET_PIN(D,3)
#define CAM_PIN_D4 GET_PIN(C,11)
#define CAM_PIN_D3 GET_PIN(C,9)
#define CAM_PIN_D2 GET_PIN(C,8)
#define CAM_PIN_D1 GET_PIN(C,7)
#define CAM_PIN_D0 GET_PIN(C,6)
#define CAM_PIN_VSYNC GET_PIN(B,7)
#define CAM_PIN_HREF GET_PIN(H,8)
#define CAM_PIN_PCLK GET_PIN(A,6)

static camera_config_t config = {
    .pin_pwdn = CAM_PIN_PWDN,
    .pin_reset = CAM_PIN_RESET,
    .pin_xclk = CAM_PIN_XCLK,
    .pin_sscb_sda = CAM_PIN_SIOD,
    .pin_sscb_scl = CAM_PIN_SIOC,

    .pin_d7 = CAM_PIN_D7,
    .pin_d6 = CAM_PIN_D6,
    .pin_d5 = CAM_PIN_D5,
    .pin_d4 = CAM_PIN_D4,
    .pin_d3 = CAM_PIN_D3,
    .pin_d2 = CAM_PIN_D2,
    .pin_d1 = CAM_PIN_D1,
    .pin_d0 = CAM_PIN_D0,
    .pin_vsync = CAM_PIN_VSYNC,
    .pin_href = CAM_PIN_HREF,
    .pin_pclk = CAM_PIN_PCLK,

    //XCLK 20MHz or 10MHz for OV2640 double FPS (Experimental)
    .xclk_freq_hz = 20000000,
    .sscb_name = SCCB_I2C_PORT,
    .pixel_format = PIXFORMAT_JPEG, //PIXFORMAT_RGB565, //YUV422,GRAYSCALE,RGB565,JPEG
    .frame_size = FRAMESIZE_240X240,    //QQVGA-UXGA Do not use sizes above QVGA when not JPEG

    .jpeg_quality = 12, //0-63 lower number means higher quality
    .fb_count = 1,       //if more than one, i2s runs in continuous mode. Use only with JPEG
    .grab_mode = CAMERA_GRAB_WHEN_EMPTY,
};

void camera_server_setup(void) 
{
  int result = 0;

  os_kprintf("---Start---\r\n");
  // if PSRAM IC present, init with UXGA resolution and higher JPEG quality
  //                      for larger pre-allocated frame buffer.
  if(config.pixel_format == PIXFORMAT_JPEG){
    //if(psramFound()){
    if(0){
      config.jpeg_quality = 10;
      config.fb_count = 2;
      config.grab_mode = CAMERA_GRAB_LATEST;
    } else {
      // Limit the frame size when PSRAM is not available
      config.frame_size = FRAMESIZE_SVGA;
      config.fb_location = CAMERA_FB_IN_DRAM;
    }
  } else {
    // Best option for face detection/recognition
    config.frame_size = FRAMESIZE_240X240;
#if CONFIG_IDF_TARGET_ESP32S3
    config.fb_count = 2;
#endif
  }

#if defined(CAMERA_MODEL_ESP_EYE)
  pinMode(13, INPUT_PULLUP);
  pinMode(14, INPUT_PULLUP);
#endif

  // camera init
  os_err_t err = esp_camera_init(&config);
  if (err != OS_SUCCESS)
  {
      os_kprintf("Camera Init Failed\r\n");
      return;
  }
  os_kprintf("Camera Init ok\r\n");

  sensor_t * s = esp_camera_sensor_get();
  // initial sensors are flipped vertically and colors are a bit saturated
  if (s->id.PID == OV3660_PID) {
    s->set_vflip(s, 1); // flip it back
    s->set_brightness(s, 1); // up the brightness just a bit
    s->set_saturation(s, -2); // lower the saturation
  }
  // drop down frame size for higher initial frame rate
  if(config.pixel_format == PIXFORMAT_JPEG){
    s->set_framesize(s, FRAMESIZE_QVGA);
  }

#if defined(CAMERA_MODEL_M5STACK_WIDE) || defined(CAMERA_MODEL_M5STACK_ESP32CAM)
  s->set_vflip(s, 1);
  s->set_hmirror(s, 1);
#endif

#if defined(CAMERA_MODEL_ESP32S3_EYE)
  s->set_vflip(s, 1);
#endif

  result = startCameraServer();
  if (result != ESP_OK)
  {
      os_kprintf("\r\nstart camera server failed\r\n");
      return;
  }

#if defined(NET_USING_LWIP)
  struct netif *netif;
  netif = netif_list;
  if(netif != OS_NULL)
  {
    os_kprintf("Camera Ready! Use 'http://");
    os_kprintf(ipaddr_ntoa(&(netif->ip_addr)));
    os_kprintf("' to connect\r\n");
  }
#elif defined(NET_USING_MOLINK)
    mo_object_t *mo_obj;
    mo_obj = mo_get_default();
    char ipaddr[32] = {0};
    if (mo_obj == OS_NULL)
    {
        os_kprintf("Ping: get defmo obj failed, module is not create.\r\n");
        return;
    }

    if (mo_get_ipaddr(mo_obj, ipaddr) != OS_SUCCESS)
    {
        os_kprintf("Ping: get module intf address failed, module is not ready for ping\r\n");
        return;
    }

    os_kprintf("Camera Ready! Use 'http://%s' to connect\r\n", ipaddr);
#endif

}

#include <shell.h>
SH_CMD_EXPORT(camera_server_setup, camera_server_setup, "camera server setup");
