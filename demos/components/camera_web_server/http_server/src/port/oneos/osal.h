/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        osal.h
 *
 * @brief       User application entry
 *
 * @revision
 * Date         Author          Notes
 * 2022-10-27   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef _OSAL_H_
#define _OSAL_H_

#include <os_task.h>
#include <unistd.h>
#include <stdint.h>
#include <esp_err.h>

#ifdef __cplusplus
extern "C" {
#endif

#define OS_SUCCESS ESP_OK
#define OS_FAIL    ESP_FAIL

#define tskIDLE_PRIORITY    ( ( uint8_t ) 31U )
#define tskNO_AFFINITY	    ( 0x7FFFFFFF )

typedef long BaseType_t;

typedef os_task_id othread_t;

/* The old tskTCB name is maintained above then typedefed to the new TCB_t name
below to enable the use of older kernel aware debuggers. */

int httpd_os_thread_create(othread_t *thread,
                           const char *name, uint16_t stacksize, int prio,
                           void (*thread_routine)(void *arg), void *arg,
                           BaseType_t core_id);

/* Only self delete is supported */
void httpd_os_thread_delete(void);

void httpd_os_thread_sleep(int msecs);

othread_t httpd_os_thread_handle(void);

#ifdef __cplusplus
}
#endif

#endif /* ! _OSAL_H_ */
