/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        osal.c
 *
 * @brief
 *
 * @revision
 * Date         Author          Notes
 * 2022-10-27   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include "osal.h"

#ifdef __cplusplus
extern "C" {
#endif


int httpd_os_thread_create(othread_t *thread,
                           const char *name, uint16_t stacksize, int prio,
                           void (*thread_routine)(void *arg), void *arg,
                           BaseType_t core_id)
{
    (void) core_id;

    os_task_id tid = NULL;
    char task_name[OS_NAME_MAX] = {0};
    static uint16_t thread_num = 0;
    snprintf(task_name, sizeof(task_name), "httpthd%02d", thread_num++);

    tid = os_task_create(NULL,
                         NULL,
                         stacksize,
                         task_name,
                         thread_routine,
                         (void *)arg,
                         prio);
    if (tid)
    {
        *thread = tid;
        os_task_startup(tid);
        return OS_SUCCESS;
    }

    return OS_FAIL;
}

/* Only self delete is supported */
void httpd_os_thread_delete(void)
{
    os_task_destroy(os_get_current_task());
}

inline void httpd_os_thread_sleep(int msecs)
{
    os_task_msleep(msecs);
}

othread_t httpd_os_thread_handle(void)
{
    return os_get_current_task();
}

#ifdef __cplusplus
}
#endif
