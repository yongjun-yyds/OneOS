#include <os_task.h>
#include <device.h>
#include <cam_hal.h>
#include <shell.h>
#include <fcntl.h>
#include <unistd.h>
#include <cam_hal.h>
#include <board.h>
#include <vfs_fs.h>
#include <vfs_posix.h>
#include <dlog.h>
#define DBG_TAG "cam_test"

/* demo1:save jpeg into sd card,not support */
static int sd_mount(char *argv)
{
    if (vfs_mount(argv, "/", "fat", 0, 0) == 0)
    {
        os_kprintf("Filesystem initialized!\r\n");
        return OS_SUCCESS;
    }
    else
    {
        os_kprintf("Failed to initialize filesystem!\r\n");
        return OS_FAILURE;
    }
}

void save_jpeg(char *name, uint8_t *data, uint32_t length)
{
    int fd = -1;
    sd_mount("sd0");
    fd = open(name, O_WRONLY | O_CREAT);
    write(fd, data, length);
    close(fd);
}

/* demo2:rgb565 display on lcd */
#ifdef BSP_USING_ATKRGBLCD
#include <graphic.h>

static os_device_t *disp_dev = 0;
static uint8_t     *lcd_data;

static os_err_t enable_lcd(void)
{
    disp_dev = os_device_find(OS_GUI_DISP_DEV_NAME);
    OS_ASSERT(disp_dev);
    os_device_open(disp_dev);
    extern void stm32_lcd_display_on(struct os_device * dev, os_bool_t on_off);
    stm32_lcd_display_on(disp_dev, 1);

    os_graphic_t *graphic = (os_graphic_t *)disp_dev;
    lcd_data              = graphic->info.framebuffer_curr;

    return OS_SUCCESS;
}

static void pic_to_lcd(void *parameter)
{
    uint16_t  width;
    uint16_t  height;
    uint16_t *input;

    uint32_t     row  = 0;
    uint32_t     line = 0;
    camera_fb_t *pic;

    enable_lcd();

    uint16_t *out = (uint16_t *)lcd_data;
    while (1)
    {

        pic = esp_camera_fb_get();

        if (pic == NULL)
            continue;

        width  = pic->width;
        height = pic->height;
        input  = (uint16_t *)pic->buf;

        for (line = 0; line < OS_GRAPHIC_LCD_HEIGHT; line++)
        {
            for (row = 0; row < OS_GRAPHIC_LCD_WIDTH; row++)
            {
                if (line < height && row < width)
                    out[line * OS_GRAPHIC_LCD_WIDTH + row] = input[line * width + row];
                else
                    out[line * OS_GRAPHIC_LCD_WIDTH + row] = 0xffff;
            }
        }

        esp_camera_fb_return(pic);
    }
}

static void rgb_pic_to_lcd(int argc, char *argv[])
{
    os_task_id task = os_task_create(OS_NULL, OS_NULL, 2048, "lcd_dis_task", pic_to_lcd, NULL, 4);
    OS_ASSERT(task);
    os_task_startup(task);
}
SH_CMD_EXPORT(cam_lcd_rgb, rgb_pic_to_lcd, "rgb_pic_lcd_test");

#if 0
void data_to_lcd1(uint16_t *input,cam_obj_t *cam)
{
    uint16_t width  = cam->width;
    uint16_t length = cam->height;
    uint32_t row = 0;
    uint32_t line = 0;
    uint32_t line_num = cam->dma_half_buffer_size/(cam->width*2);
    static uint32_t lcd_line_num =0;
    static uint32_t count =0;
    
    if (lcd_line_num+line_num <= OS_GRAPHIC_LCD_HEIGHT)
    {
        uint16_t *out = (uint16_t *)(lcd_data);
        for(line=0;line<line_num;line++)
        {    for(row=0;row<480;row++)
            {
                if(line<length && row<width)
                {
                    out[(lcd_line_num+line) * 480 + row] = input[line * width + row];
                }
                else
                    out[(lcd_line_num+line) * 480 + row] = 0xffff;
            }
        }
    }
    count++;
    lcd_line_num += line_num;
    if(count>=cam->dma_half_buffer_cnt)
    {
        count = 0;
        lcd_line_num =0;
    }
}

/* Portrait display conversion,out[num*WIDTH]=input[height-1-num] */
static void data_convert(uint16_t *input)
{
    uint16_t num = 0;
    uint16_t line = 0;
    uint16_t *out = (uint16_t *)lcd_data;
    for(line=0;line<OS_GRAPHIC_LCD_WIDTH;line++)
        for(num=0;num<OS_GRAPHIC_LCD_HEIGHT;num++)
            out[line+num*OS_GRAPHIC_LCD_WIDTH] = input[line*OS_GRAPHIC_LCD_HEIGHT+num];
}

static void data_copy_to_lcd(uint32_t *input,uint32_t length)
{
    uint32_t send_size = OS_GRAPHIC_LCD_HEIGHT*OS_GRAPHIC_LCD_WIDTH*2/4;
    uint16_t num = 0;
    uint32_t *out = (uint32_t *)lcd_data;
    for(num=0;num<send_size;num++)
        out[num] = input[num];
}

void LTDC_Color_Fill(uint32_t *color)
{
    uint32_t timeout=0; 

    //RCC->AHB1ENR|=1<<23;
    DMA2D->CR=0<<16;
    DMA2D->FGPFCCR=DMA2D_OUTPUT_RGB565;
    DMA2D->FGOR=0;
    DMA2D->OOR=(480-240);
    DMA2D->CR&=~(1<<0);
    DMA2D->FGMAR=(uint32_t)color;	
    DMA2D->OMAR=(uint32_t)lcd_data;
    DMA2D->NLR=(0xF0<<16| 0XF0);
    DMA2D->CR|=1<<0;
    while((DMA2D->ISR&(1<<1))==0)
    {
    	timeout++;
    	if(timeout>0X1FFFFF)break;
    } 
    DMA2D->IFCR|=1<<1;
} 



static void dis_task(void *parameter)
{
    os_semaphore_create(&dcmi_sem,"dcmi_sem",0,1);
    while (1)
    {
        os_semaphore_wait(&dcmi_sem,OS_WAIT_FOREVER);
        //data_to_lcd1((uint16_t *)pbuf,dcmi_cam->cam);
        LTDC_Color_Fill((uint32_t *)pbuf);
    }
}

void create_dis(void)
{
    os_task_id task;

    task = os_task_create(OS_NULL, OS_NULL, 4096, "user", dis_task, NULL, 3);
    OS_ASSERT(task);
    os_task_startup(task);
}
#endif

#endif

// demo3:
static os_err_t jpeg_serial_init(char *name)
{
    int          ret;
    os_device_t *uart;

    uart = os_device_open_s(name);
    OS_ASSERT(uart);

    /* uart config */
    struct serial_configure config = OS_SERIAL_CONFIG_DEFAULT;
    config.baud_rate               = BAUD_RATE_921600;
    ret                            = os_device_control(uart, OS_DEVICE_CTRL_CONFIG, &config);
    if (ret != 0)
    {
        os_kprintf("serial control fail %d\r\n", ret);
        return OS_FAILURE;
    }
    else
        os_kprintf("serial init ok\r\n");
    return OS_SUCCESS;
}

static os_err_t send_jpeg_by_uart(int argc, char *argv[])
{
    int32_t      i, jpegstart;
    camera_fb_t *pic;
    uint8_t     *p;
    char        *name = "uart2";

    jpeg_serial_init(name);
    while (1)
    {
        pic = esp_camera_fb_get();
        if (pic == NULL)
            continue;

        p         = pic->buf;
        jpegstart = -1;

        jpegstart = cam_verify_jpeg_soi(p, pic->len);
        os_kprintf("jpeg_data_len:%d\r\n", pic->len - jpegstart);
        for (i = jpegstart; i < pic->len; i++)
        {
            USART2->TDR = p[i];
            while ((USART2->ISR & 0X40) == 0)
                ;
        }
        esp_camera_fb_return(pic);
    }
}
SH_CMD_EXPORT(cam_uart_jpeg, send_jpeg_by_uart, "jpeg_uart_test");

// demo4:
#if defined(BSP_USING_ETH) || defined(OS_USING_USB_WIFI)
#include <sys/types.h>
#include "lwip/sockets.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

#define SERVPORT    3333
#define BACKLOG     10
#define MAXDATASIZE 5

const char *stream_head = "HTTP/1.1 200 OK\r\n"
                          "Content-Type: multipart/x-mixed-replace;boundary=123456789000000000000987654321\r\n"
                          "Transfer-Encoding: chunked\r\n"
                          "Access-Control-Allow-Origin: *\r\n"
                          "X-Framerate: 60\r\n\r\n";

const char *boundary_head = "\r\n--123456789000000000000987654321\r\n";

const char *_jpeg_start = "Content-Type: image/jpeg\r\n"
                          "Content-Length: %d\r\n"
                          "X-Timestamp: 2502.945851\r\n"
                          "\r\n";

static os_err_t httpd_resp_send_chunk(int r, const char *buf, size_t buf_len)
{
    char len_str[10];

    if (r == -1)
    {
        return OS_FAILURE;
    }

    snprintf(len_str, sizeof(len_str), "%x\r\n", buf_len);
    if (lwip_send(r, len_str, strlen(len_str), 0) == OS_FAILURE)
    {
        return OS_FAILURE;
    }
    if (buf)
    {
        if (lwip_send(r, buf, buf_len, 0) == OS_FAILURE)
        {
            return OS_FAILURE;
        }
    }

    /* Indicate end of chunk */
    if (lwip_send(r, "\r\n", strlen("\r\n"), 0) == OS_FAILURE)
    {
        return OS_FAILURE;
    }
    return OS_SUCCESS;
}

static void send_jpeg_by_http(void *parameter)
{
    char               http_img_buff[200];
    int                client_fd = -1;
    struct sockaddr_in server_sockaddr, client_sockaddr;
    int                sin_size;
    int                sockfd;
    char               buf[MAXDATASIZE];
    camera_fb_t       *pic;
#if 0
    char *eth_dev ="eth";
    ip4_addr_t ip_addr ={0};
    ip4_addr_t gw_addr;
    ip4_addr_t nm_addr;
    do
    {
        os_task_msleep(100);
        os_lwip_netif_get_ipv4_info(eth_dev, &ip_addr, &gw_addr, &nm_addr);
    }while(ip_addr.addr==0);
#endif
    if ((sockfd = lwip_socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
        os_kprintf("Socket error\r\n");
    }

    os_kprintf("Socket success!,sockfd=%d\r\n", sockfd);

    server_sockaddr.sin_family      = AF_INET;
    server_sockaddr.sin_port        = PP_HTONS(SERVPORT);
    server_sockaddr.sin_addr.s_addr = PP_HTONL(INADDR_ANY);
    memset(&(server_sockaddr.sin_zero), 0, 8);

    if ((lwip_bind(sockfd, (struct sockaddr *)&server_sockaddr, sizeof(struct sockaddr))) == -1)
    {
        os_kprintf("bind error\r\n");
    }

    os_kprintf("bind success!\r\n");

    if (listen(sockfd, BACKLOG) == -1)
    {
        os_kprintf("listen error");
    }

    os_kprintf("listening ...\r\n");

    int jpegstart  = 0;
    int start_len  = 0;
    int send_legth = 0;

    while (1)
    {
        if ((client_fd = lwip_accept(sockfd, (struct sockaddr *)&client_sockaddr, (socklen_t *)&sin_size)) == -1)
        {
            os_kprintf("accept error\r\n");
            os_task_msleep(100);
            continue;
        }

        os_kprintf("accept success!\r\n");
        os_task_msleep(10);
        if (lwip_recv(client_fd, buf, MAXDATASIZE, 0) == -1)
        {
            os_kprintf("recv error\r\n");
            continue;
        }

        os_kprintf("received a connection : %s\r\n", buf);

        if ((lwip_send(client_fd, stream_head, strlen(stream_head), 0)) == -1)
        {
            os_kprintf("send error1\r\n");
            continue;
        }
        os_kprintf("send 1\r\n");

        while (2)
        {
            pic = esp_camera_fb_get();

            if (pic == NULL)
                continue;

            jpegstart = cam_verify_jpeg_soi(pic->buf, pic->len);
            start_len = sprintf(http_img_buff, _jpeg_start, pic->len - jpegstart);
            char *p   = (char *)pic->buf;

            if (httpd_resp_send_chunk(client_fd, boundary_head, strlen(boundary_head)) == -1)
            {
                os_kprintf("send error2\r\n");
                break;
            }

            if (httpd_resp_send_chunk(client_fd, http_img_buff, start_len) == -1)
            {
                os_kprintf("send error3\r\n");
                break;
            }

            LOG_I(DBG_TAG, "send head ok\r\n");

            send_legth = pic->len - jpegstart;

            if (httpd_resp_send_chunk(client_fd, &p[jpegstart], send_legth) == -1)
            {
                os_kprintf("send error4\r\n");
                break;
            }

            LOG_I(DBG_TAG, "send jpeg ok\r\n");
            esp_camera_fb_return(pic);
        }
        esp_camera_fb_return(pic);
        lwip_close(client_fd);
    }
}

static void jpeg_to_http_task(int argc, char *argv[])
{
    os_task_id task = os_task_create(OS_NULL, OS_NULL, 2048, "html_dis_task", send_jpeg_by_http, NULL, 4);
    OS_ASSERT(task);
    os_task_startup(task);
}
SH_CMD_EXPORT(cam_html_jpeg, jpeg_to_http_task, "jpeg_dis_by_http");

#endif

static void jpeg_lcd(void *parameter)
{
    camera_fb_t *pic;
    uint8_t     *p;
    while (1)
    {
        pic = esp_camera_fb_get();
        if (pic == NULL)
            continue;

        p = pic->buf;
        extern int jpeg_hardware_decode(uint8_t *pic_data, uint32_t pic_data_len);
        jpeg_hardware_decode(p, pic->len);
        esp_camera_fb_return(pic);
        os_task_msleep(50); 
    }
}

static os_err_t cam_jpeg_to_lcd(int argc, char *argv[])
{
    os_task_id task;

    task = os_task_create(OS_NULL, OS_NULL, 10240, "jpeg_lcd", jpeg_lcd, OS_NULL, 3);
    OS_ASSERT(task);
    os_task_startup(task);

    return 0;
}
SH_CMD_EXPORT(cam_jpeg_to_lcd, cam_jpeg_to_lcd, "cam_jpeg_to_lcd");

static os_err_t jpeg_destroy(int argc, char *argv[])
{
    os_task_id task;

    task = os_task_get_id( "user1");
    OS_ASSERT(task);
    os_task_destroy(task);

    return 0;
}
SH_CMD_EXPORT(jpeg_destroy, jpeg_destroy, "display_jpeg_destroy");

static os_err_t set_cam_framesize(int argc, char *argv[])
{
    if (argc != 2)
    {
        os_kprintf( "argc is not error\r\n");
        return -1;
    }
    
    framesize_t val = (framesize_t)strtol(argv[1], OS_NULL, 0);

    sensor_t *s = esp_camera_sensor_get();
    
    if (s->pixformat == PIXFORMAT_JPEG) {
        s->set_framesize(s, val);
    }
    return OS_SUCCESS;
}
SH_CMD_EXPORT(set_cam_framesize, set_cam_framesize, "set_cam_framesize");




