/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        lpmgr_test.c
 *
 * @brief       The test file for lpmgr.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <board.h>
#include <arch_interrupt.h>
#include <device.h>
#include <drv_common.h>
#include <drv_cfg.h>
#include <stdio.h>
#include <shell.h>
#include "os_stddef.h"
#include <os_event.h>
#include <os_clock.h>
#include <stdlib.h>
#include <drv_gpio.h>

#ifdef OS_USING_TICKLESS_LPMGR

#include <lpmgr/lpmgr.h>

/*
 * example 1:
 * enter sleep mode, Calculate sleep time according to all tasks of the system
 * The user task needs to call os_lpmgr_request(SYS_SLEEP_MODE_IDLE); and os_lpmgr_release(SYS_SLEEP_MODE_IDLE);
 */
void request_sleep(int argc, char *argv[])
{
    if (argc != 2)
    {
        os_kprintf("usage: request_sleep <sleep_mode>\r\n");
        os_kprintf("example: request_sleep 2\r\n");
        return;
    }

    os_lpmgr_request((lpmgr_sleep_mode_e)atoi(argv[1])); /* SYS_SLEEP_MODE_LIGHT */
}

/* Called when the system is initialized, the system calculates the sleep time
 * according to the task   */
SH_CMD_EXPORT(request_sleep, request_sleep, "request_sleep <sleep_mode>");

/*
 * example 2:
 * Setting the timer time is also the time for regular sleep;
 * Note: To make the timer time effective, the time of the next priority task calculated
 * automatically by the system is greater than this timer time
 */
static os_timer_id timer1;

static void request_entry(void *parameter)
{
    os_lpmgr_request(SYS_SLEEP_MODE_IDLE);
    os_kprintf("user task, start current tick: %d\r\n", os_tick_get_value());
    os_lpmgr_release(SYS_SLEEP_MODE_IDLE);
}

/* clang-format off */
void request_wakeup(int mode, int timeout)
{
    timer1 = os_timer_create(OS_NULL, "requst_wake_up", request_entry, OS_NULL, OS_TICK_PER_SECOND * timeout, OS_TIMER_FLAG_PERIODIC);
    if (timer1 == OS_NULL)
    {
        os_kprintf("[%s]-[%d], os_timer_create err!\r\n", __FILE__, __LINE__);
        return;
    }

    os_timer_start(timer1);
    os_lpmgr_request((lpmgr_sleep_mode_e)mode);
}
/* clang-format on */

void request_wake_up(int argc, char *argv[])
{
    uint8_t timeout;
    uint8_t mode;

    if (argc != 3)
    {
        os_kprintf("usage: requst_wake_up <mode> <timeout_tick>\r\n");
        os_kprintf("example: requst_wake_up 2 5\r\n");
        return;
    }

    mode    = atoi(argv[1]);
    timeout = atoi(argv[2]);
    os_kprintf("[%s]-[%d], mode[%d], timeout[%d]\r\n", __FILE__, __LINE__, mode, timeout);
    request_wakeup(mode, timeout);
}

SH_CMD_EXPORT(request_wake_up, request_wake_up, "request_wake_up");

/*
 * example 3:
 * Wake up sleep mode by external interrupt, continue to sleep after processing some tasks
 *
 */
#define WAKEUP_EVENT_BUTTON   (1 << 0)
#define LED_PIN               led_table[0].pin
#define WAKEUP_PIN            key_table[0].pin
#define WAKEUP_APP_STACK_SIZE 512

typedef struct _tag_sleep_info_s
{
    os_semaphore_dummy_t  *wakeup_sem;
    uint8_t mode;
} sleep_info_s;

static sleep_info_s sleep_info;

static void wakeup_callback(void *args)
{
    os_kprintf("wake up[%s]-[%d], pin[%d], tick[%d]\r\n", __FILE__, __LINE__, (uint32_t)args, os_tick_get_value());
    os_semaphore_post(sleep_info.wakeup_sem);
}

static void wakeup_irq_init(void)
{
    os_pin_mode(WAKEUP_PIN, PIN_MODE_INPUT_PULLUP);
    os_pin_attach_irq(WAKEUP_PIN, PIN_IRQ_MODE_FALLING, wakeup_callback, (void *)WAKEUP_PIN);
    os_pin_irq_enable(WAKEUP_PIN, PIN_IRQ_ENABLE);
}

static void led_work(uint32_t cnt)
{
#if defined(OS_USING_LED)
    os_pin_mode(LED_PIN, PIN_MODE_OUTPUT);

    do
    {
        os_pin_write(LED_PIN, PIN_LOW);
        os_task_msleep(500);
        os_pin_write(LED_PIN, PIN_HIGH);
        os_task_msleep(500);
    } while (cnt-- > 0);
#endif
}

static void wakeup_app_entry(void *parameter)
{
    sleep_info_s *sleep_info;
    OS_ASSERT(parameter != OS_NULL);

    sleep_info = (sleep_info_s *)parameter;

    wakeup_irq_init();
    os_kprintf("request sleep [%s]-[%d], mode[%d]\r\n", __FILE__, __LINE__, sleep_info->mode);
    os_lpmgr_request((lpmgr_sleep_mode_e)sleep_info->mode);

    while (1)
    {
        os_semaphore_wait(sleep_info->wakeup_sem, OS_WAIT_FOREVER);
        os_lpmgr_request(SYS_SLEEP_MODE_NONE);
        os_kprintf("wake up, enter user code[%s]-[%d], tick[%d]\r\n", __FILE__, __LINE__, os_tick_get_value());

        led_work(6);

        os_kprintf("wake up, exit user code[%s]-[%d], tick[%d]\r\n", __FILE__, __LINE__, os_tick_get_value());
        os_lpmgr_release(SYS_SLEEP_MODE_NONE);
    }
}

static int wakeup_irq(int argc, char *argv[])
{
    os_task_id tid;

    if (argc != 2)
    {
        os_kprintf("usage: wakeup_irq <mode>\r\n");
        os_kprintf("example: wakeup_irq 2\r\n");
        return -1;
    }

    sleep_info.mode = atoi(argv[1]);

    sleep_info.wakeup_sem = os_semaphore_create(OS_NULL, "wakeup_irq", 0, 1);
    OS_ASSERT(sleep_info.wakeup_sem != OS_NULL);

    tid = os_task_create(OS_NULL, OS_NULL, WAKEUP_APP_STACK_SIZE, "wakeup_app", wakeup_app_entry, (void *)&sleep_info, 5);
    OS_ASSERT(tid != OS_NULL);

    os_task_startup(tid);

    return 0;
}

SH_CMD_EXPORT(wakeup_irq, wakeup_irq, "wakeup_irq");

os_err_t dev_suspend(void *priv, uint8_t mode)
{
    //    os_kprintf("[%s]-[%d], suspend mode[%d]\r\n", __FILE__, __LINE__, mode);
    return OS_SUCCESS;
}

void dev_resume(void *priv, uint8_t mode)
{
    os_kprintf("[%s]-[%d], resume mode[%d]\r\n", __FILE__, __LINE__, mode);
}

const struct os_lpmgr_device_ops dev_ops = {
    .suspend = dev_suspend,
    .resume  = dev_resume,
};

void lp_register_dev(void)
{
    os_err_t          ret;
    struct os_device *device;

    device = os_device_find(OS_CONSOLE_DEVICE_NAME);

    ret = os_lpmgr_device_register(device, &dev_ops);
    os_kprintf("dev [%s] register ret[%d]\r\n", OS_CONSOLE_DEVICE_NAME, ret);
}

SH_CMD_EXPORT(lp_register_dev, lp_register_dev, "lp_register_dev");

/**
 用户notify回调函数注册,进入低功耗前不能用串口打印，会影响低功耗的进入
***/

void user_notify(os_lpmgr_sys_e event, lpmgr_sleep_mode_e mode, void *data)
{
    if (event == SYS_ENTER_SLEEP)
    {
    }
    else
    {
        os_kprintf("[%s]-[%d], event[%d], mode[%d],data[%d]\r\n",
                   __FILE__,
                   __LINE__,
                   event,
                   mode,
                   *(uint16_t *)data);
    }
}

void lp_register_notify(int argc, char *argv[])
{
    uint16_t         *data            = os_malloc(sizeof(uint16_t));
    struct lpmgr_notify *gs_lpmgr_notify = os_malloc(sizeof(struct lpmgr_notify));

    *data = strtol(argv[1], OS_NULL, 0);

    gs_lpmgr_notify->notify = user_notify;
    gs_lpmgr_notify->data   = (void *)data;

    os_lpmgr_notify_register(gs_lpmgr_notify);
}

SH_CMD_EXPORT(lp_register_notify, lp_register_notify, "lp_register_notify");

static os_err_t pin_suspend(void *priv, uint8_t mode)
{
    os_pin_write(73, 1);    //(E,9):led off
    return 0;
}

static void pin_resume(void *priv, uint8_t mode)
{
    os_pin_write(73, 0);    //(E,9):led on
}

static struct os_lpmgr_device_ops pin_lpmgr_ops = {
    .suspend = pin_suspend,
    .resume  = pin_resume,
};

os_err_t pin_lpmgr_test(int argc, char *argv[])
{
    struct os_device *device = os_device_find("pin_0");
    os_pin_mode(73, PIN_MODE_OUTPUT);
    os_pin_write(73, 0);    //(E,9):led on
    os_lpmgr_device_register(device, &pin_lpmgr_ops);

    return 0;
}

SH_CMD_EXPORT(pin_lpmgr_test, pin_lpmgr_test, "pin_lpmgr_test");

#endif /* OS_USING_TICKLESS_LPMGR */

#ifdef OS_USING_SIMPLE_LPM

#include <lpm.h>

static void simple_lpm_entry(void *parameter)
{
    uint32_t sleep_cnt = 10;
    uint32_t timeout_ms;

    timeout_ms = *(uint32_t *)parameter;
    while (sleep_cnt--)
    {
        os_task_msleep(3000);
        lpm_start(timeout_ms);
        os_kprintf("[%s]-[%d], timeout[%d ms], cur_tick[%d]\r\n", __FILE__, __LINE__, timeout_ms, os_tick_get_value());
    }
}

void lpm_start_test(int argc, char *argv[])
{
    os_task_id  tid;
    uint32_t timeout_ms;

    if (argc != 2)
    {
        os_kprintf("usage: lpm_start_test <time ms>\r\n");
        os_kprintf("example: lpm_start_test  5000\r\n");
        return;
    }

    timeout_ms = atoi(argv[1]);

    tid = os_task_create(OS_NULL, OS_NULL, 512, "simple lpm", simple_lpm_entry, (void *)&timeout_ms, 5);
    OS_ASSERT(tid != OS_NULL);
    os_task_startup(tid);
}

SH_CMD_EXPORT(lpm_start_test, lpm_start_test, "lpm_start_test");

#endif
