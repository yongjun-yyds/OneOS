/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        touch_test.c
 *
 * @brief       touch_test
 *
 * @details     touch_test
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <os_task.h>
#include <os_memory.h>
#include <touch.h>

#ifdef OS_USING_SHELL
#include <shell.h>
#endif

#define THREAD_PRIORITY   25
#define THREAD_STACK_SIZE 1024
#define THREAD_TIMESLICE  5

static os_task_id            touch_task = OS_NULL;
static os_semaphore_dummy_t             *touch_sem  = OS_NULL;
static os_device_t          *dev        = OS_NULL;
static struct os_touch_data *read_data;
static struct os_touch_info  info;

static void touch_entry(void *parameter)
{
    os_device_control(dev, OS_TOUCH_CTRL_GET_INFO, &info);

    read_data = (struct os_touch_data *)os_calloc(1, sizeof(struct os_touch_data) * info.point_num);

    while (1)
    {
        os_semaphore_wait(touch_sem, OS_WAIT_FOREVER);

        if (os_device_read_nonblock(dev, 0, read_data, info.point_num) == info.point_num)
        {
            for (uint8_t i = 0; i < info.point_num; i++)
            {
                os_kprintf("event:%d,x:%d,y:%d,timestamp:%d\r\n",
                           read_data[i].event,
                           read_data[i].x_coordinate,
                           read_data[i].y_coordinate,
                           read_data[i].timestamp);
            }
        }
    }
}

static os_err_t rx_callback(os_device_t *dev, struct os_device_cb_info *info)
{
    os_semaphore_post(touch_sem);
    return OS_SUCCESS;
}

static os_err_t touch_test(void)
{
    const char           *name = "touch";
    struct os_touch_info *touch_info;

    dev = os_device_find(name);
    if (dev == OS_NULL)
    {
        os_kprintf("can't find device\r\n");
        return OS_FAILURE;
    }

    if (os_device_open(dev) != OS_SUCCESS)
    {
        os_kprintf("open device failed!\r\n");
        return OS_FAILURE;
    }

    touch_info = os_calloc(1, sizeof(struct os_touch_info));
    if (touch_info == OS_NULL)
    {
        os_kprintf("out of memory!\r\n");
        return OS_FAILURE;
    }

    os_device_control(dev, OS_TOUCH_CTRL_GET_INFO, touch_info);
    os_kprintf("range_x = %d\r\n", touch_info->range_x);
    os_kprintf("range_y = %d\r\n", touch_info->range_y);
    os_kprintf("point_num = %d\r\n", touch_info->point_num);
    os_free(touch_info);

    struct os_device_cb_info cb_info = {
        .type = OS_DEVICE_CB_TYPE_RX,
        .cb   = rx_callback,
    };
    os_device_control(dev, OS_DEVICE_CTRL_SET_CB, &cb_info);
    os_device_control(dev, OS_TOUCH_CTRL_ENABLE_INT, OS_NULL);

    touch_sem = os_semaphore_create(OS_NULL, "touch_irq", 0, 1);
    if (touch_sem == OS_NULL)
    {
        os_kprintf("create dynamic semaphore failed.\r\n");
        return OS_FAILURE;
    }

    touch_task = os_task_create(OS_NULL, OS_NULL, THREAD_STACK_SIZE, "touch", touch_entry, OS_NULL, THREAD_PRIORITY);
    if (touch_task != OS_NULL)
    {
        os_task_startup(touch_task);
    }

    return OS_SUCCESS;
}

#ifdef OS_USING_SHELL
SH_CMD_EXPORT(touch_test, touch_test, "get touch point");
#endif
