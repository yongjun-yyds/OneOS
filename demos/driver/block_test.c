/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        sdmmc_test.c
 *
 * @brief       The test file for sdmmc.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <arch_interrupt.h>
#include <device.h>
#include <shell.h>
#include <os_errno.h>
#include <board.h>
#include <os_clock.h>

static int block_test(int argc, char *argv[])
{
    int i, j;
    char *bdev_name = "msc0";
    uint32_t pos = 32333952;
    uint32_t buff_size = 512;
    uint32_t times = 1;

    if (strcmp(argv[1], "?") == 0 
    ||  strcmp(argv[1], "help") == 0)
    {
        os_kprintf("usage: block_test [dev] [addr] [size] [times]\r\n");
        os_kprintf("       block_test msc0 32333952 512 1\r\n");
        return -1;
    }

    if (argc > 1)
    {
        bdev_name = argv[1];
    }

    if (argc > 2)
    {
        pos = strtol(argv[2], OS_NULL, 0);
    }

    if (argc > 3)
    {
        buff_size = strtol(argv[3], OS_NULL, 0);
    }

    if (argc > 4)
    {
        times = strtol(argv[4], OS_NULL, 0);
    }
    
    os_device_t *msc = os_device_open_s(bdev_name);
    OS_ASSERT(msc);

    struct os_blk_geometry geometry;
    os_device_control(msc, OS_DEVICE_CTRL_BLK_GETGEOME, &geometry);
    
    buff_size = (buff_size + geometry.block_size - 1) & ~(geometry.block_size - 1);
    
    uint8_t *rbuf = os_malloc(buff_size);
    uint8_t *wbuf = os_malloc(buff_size);

    OS_ASSERT(rbuf);
    OS_ASSERT(wbuf);

    uint16_t wcrc = 0;
    uint16_t rcrc = 0;

    os_kprintf("dev:%s, addr:%d size:%d times:%d\r\n",
                bdev_name, pos, buff_size, times);

    for (j = 0; j < times; j++)
    {
        os_kprintf("loop:%d\r\n", j);
    
        srand(os_tick_get_value());

        for (i = 0; i < buff_size; i++)
            wbuf[i] = rand();

        wcrc = crc16(wcrc, wbuf, buff_size);
        
        os_device_write_nonblock(msc, pos, wbuf, buff_size / geometry.block_size);

        memset(rbuf, 0, buff_size);
        os_device_read_nonblock(msc, pos, rbuf, buff_size / geometry.block_size);
        
        rcrc = crc16(rcrc, rbuf, buff_size);

        //hex_dump(rbuf, buff_size);
    }

    if (wcrc != rcrc)
    {
        os_kprintf("crc fail 0x%04x, 0x%04x\r\n", wcrc, rcrc);
    }
    else
    {
        os_kprintf("crc success 0x%04x, 0x%04x\r\n", wcrc, rcrc);
    }

    os_free(rbuf);
    os_free(wbuf);
    os_device_close(msc);

    return OS_SUCCESS;
}
SH_CMD_EXPORT(block_test, block_test, "block_test");
