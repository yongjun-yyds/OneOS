#include <os_task.h>
#include <device.h>
#include <dlog.h>
#include <graphic.h>
#include <drv_jpeg.h>
#include <oneos_config.h>
#include <picture_jpeg.h>
#define DBG_TAG "jpeg_test"
int jpeg_hardware_decode(uint8_t *pic_data, uint32_t pic_data_len);

static os_device_t *disp_dev = 0;
static uint8_t Jpeg_HWDecodingEnd = 0;
static uint8_t dma2d_trans_ok     = 0;

static os_err_t enable_lcd(void)
{
    if(NULL == disp_dev)
    {
        disp_dev = os_device_find(OS_GUI_DISP_DEV_NAME);
        OS_ASSERT(disp_dev);
        os_device_open(disp_dev);
    }
    return OS_SUCCESS;
}

void Jpeg_Decode_CpltCallback(void)
{
    Jpeg_HWDecodingEnd = 1;
}
void dma2d_trans_complete(void)
{
    dma2d_trans_ok = 1;
}

static void
gpu_info_set(struct os_device_gpu_info *gpu_info, JPEG_ConfTypeDef jpeg_info, os_graphic_t *graphic, char *source)
{
    uint32_t xPos = 0;
    uint32_t yPos = 0;

    gpu_info->alpha             = 256;
    gpu_info->src_format        = OS_GRAPHIC_PIXEL_FORMAT_YUV;
    gpu_info->yuv_sampling_mode = jpeg_info.ChromaSubsampling;
    gpu_info->width             = jpeg_info.ImageWidth;
    gpu_info->height            = jpeg_info.ImageHeight;

    xPos = (graphic->info.width - jpeg_info.ImageWidth) / 2;
    yPos = (graphic->info.height - jpeg_info.ImageHeight) / 2;
    /* centre operation, 2:bytes of per_pixel */
    gpu_info->dest = (char *)((uint32_t)graphic->info.framebuffer_curr + ((yPos * graphic->info.width) + xPos) * 2);
    gpu_info->src  = source;
}

extern int cam_verify_jpeg_soi(const uint8_t *inbuf, uint32_t length);
extern int cam_verify_jpeg_eoi(const uint8_t *inbuf, uint32_t length);
int jpeg_hardware_decode(uint8_t *pic_data, uint32_t pic_data_len)
{
    JPEG_ConfTypeDef jpeg_info;
    int32_t jpegstart = 0;
    uint32_t jpeg_len;
    struct stm32_jpeg_info *stm32_jpeg;
    static os_graphic_t *graphic = 0;
    uint8_t *source_buff;
    uint8_t *yuv_buff;
    uint32_t yuv_size;
    struct os_device_gpu_info gpu_info;
    uint32_t time_count = 0;
    
    stm32_jpeg = (struct stm32_jpeg_info *)os_device_find("jpeg");
    if (disp_dev == OS_NULL)  
    {
        enable_lcd();
        graphic = (os_graphic_t *)disp_dev;
    }

    source_buff = pic_data;
    jpegstart = cam_verify_jpeg_soi(pic_data, pic_data_len);
    if(cam_verify_jpeg_soi(pic_data, pic_data_len)==-1 || cam_verify_jpeg_eoi(pic_data, pic_data_len)==-1)
    {
        os_kprintf("jpeg_data_error\r\n");
        return -1;
    }
    jpeg_len = pic_data_len - jpegstart;
    
    if (stm32_jpeg != OS_NULL)
    {
        yuv_size = graphic->info.width * graphic->info.height * 3 + 1024;
        yuv_buff = os_dma_malloc_align(yuv_size, 32);

        memset(yuv_buff, 0xaa, yuv_size);
        Jpeg_HWDecodingEnd = 0;
        dma2d_trans_ok = 0;
        time_count = 0;
        os_clean_dcache(yuv_buff,yuv_size);
        os_invalid_dcache(yuv_buff,yuv_size);
        jpeg_start_decode(stm32_jpeg->hjpeg,&source_buff[jpegstart],jpeg_len,yuv_buff,yuv_size);

        while (Jpeg_HWDecodingEnd == 0 && time_count < 20)
        {
            os_task_msleep(100);
            time_count++;
        }
        if(time_count>=20 )
        {
            os_kprintf("jpeg_start_decode error\r\n");
            os_dma_free_align(yuv_buff);
            return -1;
        }
        os_clean_dcache(yuv_buff,yuv_size);
        os_invalid_dcache(yuv_buff,yuv_size);
        
        HAL_JPEG_GetInfo(stm32_jpeg->hjpeg, &jpeg_info);
        gpu_info_set(&gpu_info, jpeg_info, graphic,(char *)yuv_buff);

        os_device_control(disp_dev, OS_GRAPHIC_CTRL_GPU_BLEND, (void *)&gpu_info);
        os_device_control(disp_dev, OS_GRAPHIC_CTRL_FRAME_FLUSH, (void *)0);
        
        while(dma2d_trans_ok == 0)
        {
            os_task_msleep(10);
        }
        os_dma_free_align(yuv_buff);
    }
    
    return 0;
}

static int jpeg_decode_test(int argc, char *argv[])
{

    uint8_t             *source;
    uint32_t             source_length;
    uint8_t              pic_num;

    if (argc != 2)
    {
        os_kprintf("usage: jpeg_test  pic_num\r\n");
        os_kprintf("       jpeg_test  0\r\n");
        return -1;
    }

    pic_num = strtol(argv[1], OS_NULL, 0);
        
    if (pic_num == 1)
    {
        source        = (uint8_t *)yidong;
        source_length = sizeof(yidong);
    }
    else if (pic_num == 2)
    {
        source        = (uint8_t *)color;
        source_length = sizeof(color);
    }
    else
    {
        source        = (uint8_t *)oneos;
        source_length = sizeof(oneos);
    }

    jpeg_hardware_decode(source,source_length);
    return 0;
}

SH_CMD_EXPORT(jpeg_decode_test, jpeg_decode_test, "jpeg_decode_test");






