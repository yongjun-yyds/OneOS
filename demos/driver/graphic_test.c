
#include "graphic/graphic.h"
#include <shell.h>
#include <os_task.h>
#include <os_assert.h>
#include <os_errno.h>

static os_err_t graphic_test_fill_color(os_device_t *dev, os_color_t color)
{
    os_graphic_area_t area;
    os_graphic_t     *graphic = (os_graphic_t *)dev;
    char             *fb;

    area.x      = 0;
    area.y      = 0;
    area.w      = graphic->info.width;
    area.h      = graphic->info.height;
    area.color  = color;
    area.buffer = OS_NULL;

    if (graphic->info.framebuffer_num > 0)
    {
        os_device_control(dev, OS_GRAPHIC_CTRL_FRAME_NEXT, (void *)(&fb));
        os_device_control(dev, OS_GRAPHIC_CTRL_FRAME_SET, (void *)(fb));
        os_device_control(dev, OS_GRAPHIC_CTRL_FRAME_FILL, (void *)(&area));
        os_device_control(dev, OS_GRAPHIC_CTRL_FRAME_FLUSH, (void *)0);
    }
    else
    {
        os_device_control(dev, OS_GRAPHIC_CTRL_DISP_AREA, (void *)(&area));
    }

    return OS_SUCCESS;
}

static void graphic_test_clear_screen(os_device_t *dev, os_color_t color)
{
    int8_t     i;
    os_graphic_t *graphic = (os_graphic_t *)dev;

    if (graphic->info.framebuffer_num == 0)
    {
        graphic_test_fill_color(dev, OS_COLOR_BLACK);
    }
    else
    {
        for (i = 0; i < graphic->info.framebuffer_num; i++)
        {
            graphic_test_fill_color(dev, OS_COLOR_BLACK);
        }
    }
}

static os_err_t graphic_test_color(int argc, char **argv)
{
    os_device_t      *disp_dev = 0;
    os_color_t        color;
    os_graphic_info_t info;

    if (argc != 2)
    {
        os_kprintf("usage: graphic_test_color <dev> \r\n");
        os_kprintf("       graphic_test_color lcd \r\n");
        return -1;
    }

    disp_dev = os_device_find(argv[1]);
    OS_ASSERT(disp_dev);
    os_device_open(disp_dev);
    os_device_control(disp_dev, OS_GRAPHIC_CTRL_GET_INFO, (void *)(&info));

    os_kprintf("red\r\n");
    color = OS_COLOR_RED;
    graphic_test_fill_color(disp_dev, color);
    os_task_tsleep(OS_TICK_PER_SECOND);

    os_kprintf("green\r\n");
    color = OS_COLOR_GREEN;
    graphic_test_fill_color(disp_dev, color);
    os_task_tsleep(OS_TICK_PER_SECOND);

    os_kprintf("blue\r\n");
    color = OS_COLOR_BLUE;
    graphic_test_fill_color(disp_dev, color);
    os_task_tsleep(OS_TICK_PER_SECOND);

    os_kprintf("black\r\n");
    color = OS_COLOR_BLACK;
    graphic_test_fill_color(disp_dev, color);
    os_task_tsleep(OS_TICK_PER_SECOND);

    os_kprintf("white\r\n");
    color = OS_COLOR_WHITE;
    graphic_test_fill_color(disp_dev, color);
    os_task_tsleep(OS_TICK_PER_SECOND);

    os_device_close(disp_dev);

    return OS_SUCCESS;
}

SH_CMD_EXPORT(graphic_test_color, graphic_test_color, "graphic_test_color");

#if 0

#include "img_rgb565_320_240_1.h"
#include "img_rgb565_320_240_2.h"

#define IMG_W 320
#define IMG_H 240

static const uint8_t * images[] = 
{
  img_rgb565_320_240_1,
  img_rgb565_320_240_2,  
};

static os_err_t graphic_test_image(int argc, char **argv)
{
    int32_t   count    = 10;
    int32_t   image_id =  0;
    os_device_t *disp_dev =  0;
    char        *fb       =  0;
    os_graphic_info_t info;
    os_graphic_area_t area;

    if (argc != 2)
    {
        os_kprintf("usage: graphic_test_image <dev> \r\n");
        os_kprintf("       graphic_test_image lcd \r\n");
        return OS_FAILURE;
    }
        
    disp_dev = os_device_find(argv[1]);
    OS_ASSERT(disp_dev);
    os_device_open(disp_dev);
    os_device_control(disp_dev, OS_GRAPHIC_CTRL_GET_INFO, (void *)(&info));

    //Clear screen
    graphic_test_clear_screen(disp_dev, OS_COLOR_BLACK);

    while (count--)
    {
        area.x    = (info.width  - IMG_W) / 2;
        area.y    = (info.height - IMG_H) / 2;
        area.w    = IMG_W;
        area.h    = IMG_H;
        area.buffer  = (uint8_t *)images[image_id++];
        
        if(image_id >= 2)
            image_id = 0;
        
        if(info.framebuffer_num == 0)
        {
            os_device_control(disp_dev, OS_GRAPHIC_CTRL_DISP_AREA, (void *)(&area));
        }else
        {
            os_device_control(disp_dev, OS_GRAPHIC_CTRL_FRAME_NEXT, (void *)(&fb));
            os_device_control(disp_dev, OS_GRAPHIC_CTRL_FRAME_SET, (void *)(fb));
            os_device_control(disp_dev, OS_GRAPHIC_CTRL_FRAME_FILL, (void *)(&area));
            os_device_control(disp_dev, OS_GRAPHIC_CTRL_FRAME_FLUSH, (void *)0);
        }
        
        os_task_msleep(1000);
    }

    os_device_close(disp_dev);
    
    return OS_SUCCESS;
}

SH_CMD_EXPORT(graphic_test_image, graphic_test_image, "graphic_test_image");
#endif

#ifdef OS_GRAPHIC_DRAW_ENABLE

#include "font_asc2_1608.h"
#include "font_asc2_2412.h"
#include "font_asc2_3216.h"

#define MARGIN 10
static os_err_t graphic_test_draw(int argc, char **argv)
{
    os_err_t          ret;
    os_device_t      *disp_dev = 0;
    char             *fb;
    os_graphic_info_t info;

    if (argc != 2)
    {
        os_kprintf("usage: graphic_test_draw <dev> \r\n");
        os_kprintf("       graphic_test_draw lcd \r\n");
        return OS_FAILURE;
    }

    disp_dev = os_device_find("lcd");
    OS_ASSERT(disp_dev);
    os_device_open(disp_dev);
    os_device_control(disp_dev, OS_GRAPHIC_CTRL_GET_INFO, (void *)(&info));

    // Clear screen
    graphic_test_clear_screen(disp_dev, OS_COLOR_BLACK);

    if (info.framebuffer_num > 0)
    {
        os_device_control(disp_dev, OS_GRAPHIC_CTRL_FRAME_NEXT, (void *)(&fb));
        os_device_control(disp_dev, OS_GRAPHIC_CTRL_FRAME_SET, (void *)(fb));
    }

    uint32_t w = (info.width > info.height) ? info.height : info.width;

    // Draw point
    os_graphic_point_t point;
    for (int i = 0; i < w / 2 - 2 * MARGIN; i++)
    {
        point.x     = MARGIN + i;
        point.y     = w / 4;
        point.color = OS_COLOR_RED;
        os_device_control(disp_dev, OS_GRAPHIC_CTRL_DRAW_POINT, (void *)(&point));
    }

    // Draw line
    os_graphic_line_t line;
    line.x1    = MARGIN;
    line.y1    = MARGIN;
    line.x2    = w / 2 - MARGIN;
    line.y2    = w / 2 - MARGIN;
    line.color = OS_COLOR_GREEN;
    os_device_control(disp_dev, OS_GRAPHIC_CTRL_DRAW_LINE, (void *)(&line));

    // Draw rect
    os_graphic_rect_t rectangle;
    rectangle.x     = MARGIN;
    rectangle.y     = MARGIN + w / 2;
    rectangle.w     = w / 2 - 2 * MARGIN;
    rectangle.h     = w / 2 - 2 * MARGIN;
    rectangle.color = OS_COLOR_BLUE;
    os_device_control(disp_dev, OS_GRAPHIC_CTRL_DRAW_RECT, (void *)(&rectangle));

    // Draw solid rect
    rectangle.x     = MARGIN * 2;
    rectangle.y     = MARGIN * 2 + info.height / 2;
    rectangle.w     = w / 2 - 4 * MARGIN;
    rectangle.h     = w / 2 - 4 * MARGIN;
    rectangle.color = OS_COLOR_BLUE;
    os_device_control(disp_dev, OS_GRAPHIC_CTRL_DRAW_SOLID_RECT, (void *)(&rectangle));

    // Draw circle
    os_graphic_circle_t circle;
    circle.x     = 3 * w / 4;
    circle.y     = w / 4;
    circle.r     = w / 4 - MARGIN;
    circle.color = OS_COLOR_YELLOW;
    os_device_control(disp_dev, OS_GRAPHIC_CTRL_DRAW_CIRCLE, (void *)(&circle));

    // Draw solid circle
    circle.x     = 3 * w / 4;
    circle.y     = w / 4;
    circle.r     = w / 4 - 2 * MARGIN;
    circle.color = OS_COLOR_YELLOW;
    os_device_control(disp_dev, OS_GRAPHIC_CTRL_DRAW_SOLID_CIRCLE, (void *)(&circle));

    // Add font
    ret = os_device_control(disp_dev, OS_GRAPHIC_CTRL_DRAW_ADD_FONT, (void *)&font_asc2_1608);
    if (ret != OS_SUCCESS)
        os_kprintf("add asc2_1608 error\r\n");

    ret = os_device_control(disp_dev, OS_GRAPHIC_CTRL_DRAW_ADD_FONT, (void *)&font_asc2_2412);
    if (ret != OS_SUCCESS)
        os_kprintf("add asc2_2412 error\r\n");
    ret = os_device_control(disp_dev, OS_GRAPHIC_CTRL_DRAW_ADD_FONT, (void *)&font_asc2_3216);
    if (ret != OS_SUCCESS)
        os_kprintf("add asc2_3216 error\r\n");

    // Draw text
    os_graphic_pos_t txt_pos;

    txt_pos.x = w / 2 + MARGIN;
    txt_pos.y = w / 2 + MARGIN;
    os_device_control(disp_dev, OS_GRAPHIC_CTRL_DRAW_SET_TXT_CURSOR, (void *)&txt_pos);
    os_device_control(disp_dev, OS_GRAPHIC_CTRL_DRAW_SET_TXT_COLOR, (void *)&OS_COLOR_BLUE);
    os_device_control(disp_dev, OS_GRAPHIC_CTRL_DRAW_SET_FONT, (void *)"asc2_1608");
    if (ret != OS_SUCCESS)
        os_kprintf("set asc2_1608 error\r\n");
    os_device_control(disp_dev, OS_GRAPHIC_CTRL_DRAW_CHAR, (void *)'A');
    os_device_control(disp_dev, OS_GRAPHIC_CTRL_DRAW_STRING, (void *)"Hello!");

    txt_pos.x = w / 2 + MARGIN;
    txt_pos.y = w / 2 + MARGIN + 30;
    os_device_control(disp_dev, OS_GRAPHIC_CTRL_DRAW_SET_TXT_CURSOR, (void *)&txt_pos);
    os_device_control(disp_dev, OS_GRAPHIC_CTRL_DRAW_SET_TXT_COLOR, (void *)&OS_COLOR_GREEN);
    os_device_control(disp_dev, OS_GRAPHIC_CTRL_DRAW_SET_FONT, (void *)"asc2_2412");
    if (ret != OS_SUCCESS)
        os_kprintf("set asc2_2412 error\r\n");
    os_device_control(disp_dev, OS_GRAPHIC_CTRL_DRAW_STRING, (void *)"Hello!");

    txt_pos.x = w / 2 + MARGIN;
    txt_pos.y = w / 2 + MARGIN + 60;
    os_device_control(disp_dev, OS_GRAPHIC_CTRL_DRAW_SET_TXT_CURSOR, (void *)&txt_pos);
    os_device_control(disp_dev, OS_GRAPHIC_CTRL_DRAW_SET_TXT_COLOR, (void *)&OS_COLOR_RED);
    ret = os_device_control(disp_dev, OS_GRAPHIC_CTRL_DRAW_SET_FONT, (void *)"asc2_3216");
    if (ret != OS_SUCCESS)
        os_kprintf("set asc2_3216 error\r\n");
    os_device_control(disp_dev, OS_GRAPHIC_CTRL_DRAW_STRING, (void *)"Hello!");

    if (info.framebuffer_num > 0)
    {
        os_device_control(disp_dev, OS_GRAPHIC_CTRL_FRAME_FLUSH, (void *)0);
    }

    return OS_SUCCESS;
}

SH_CMD_EXPORT(graphic_test_draw, graphic_test_draw, "graphic_test_draw");

#endif
