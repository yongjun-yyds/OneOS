#include <os_task.h>
#include <device.h>
#include <unistd.h>
#include <stdio.h>
#include <shell.h>

#include <spi.h>
#include <string.h>
#include <stdlib.h>
#include <board.h>
#include <timer/clocksource.h>
#include <timer/clockevent.h>

static int spidev_transfer(int argc, char **argv)
{
    /* 1. parameter init */
    if (argc < 4)
    {
        os_kprintf("usage: spidev_transfer <spi_bus_name> <cs_pin> <send_data> [recv_length] [clock(kHz)(50)] "
                   "[repeat(1)]\r\n");
        os_kprintf("       spidev_transfer spi1 10 0001020304050607 8\r\n");
        os_kprintf("       spidev_transfer spi1 10 0001020304050607 8 50\r\n");
        os_kprintf("       spidev_transfer spi1 10 0001020304050607 8 50 1\r\n");
        return OS_FAILURE;
    }

    int   ret;
    char *spi_bus_name = argv[1];
    int   spi_cs_pin   = strtol(argv[2], OS_NULL, 0);
    char *send_data    = argv[3];
    int   recv_length  = 0;
    int   clock        = 50 * 1000;
    int   repeat       = 1;

    char spi_device_name[16];

    snprintf(spi_device_name, sizeof(spi_device_name) - 1, "sd_%s", spi_bus_name);

    if (argc > 4)
    {
        recv_length = strtol(argv[4], OS_NULL, 0);
    }

    if (argc > 5)
    {
        clock = strtol(argv[5], OS_NULL, 0) * 1000;
    }

    if (argc > 6)
    {
        repeat = strtol(argv[6], OS_NULL, 0);
    }

    /* 2. attach spidev */
    struct os_spi_device *spidev = (struct os_spi_device *)os_device_open_s(spi_device_name);

    if (spidev == OS_NULL)
    {
        ret = os_hw_spi_device_attach(spi_bus_name, spi_device_name, spi_cs_pin);
        if (ret != OS_SUCCESS)
        {
            os_kprintf("can not attach the spi device!\r\n");
            return OS_FAILURE;
        }

        spidev = (struct os_spi_device *)os_device_open_s(spi_device_name);
    }

    OS_ASSERT(spidev != OS_NULL);

    /* 3. config spi */
    struct os_spi_configuration cfg = {
        .mode       = OS_SPI_MODE_0 | OS_SPI_MSB,
        .data_width = 8,
        .max_hz     = clock,
    };

    os_spi_configure(spidev, &cfg);

    /* 4. transfer */
    if (strlen(send_data) % 2 != 0)
    {
        os_kprintf("invalide send data\r\n");
        return OS_SUCCESS;
    }

    int  i;
    int  send_length = strlen(send_data) / 2;
    char buff[5]     = "0x00";

    unsigned char *send_buff = os_calloc(1, send_length);

    OS_ASSERT(send_buff != OS_NULL);

    unsigned char *recv_buff = OS_NULL;

    if (recv_length != 0)
    {
        recv_buff = os_calloc(1, recv_length);
        OS_ASSERT(recv_buff != OS_NULL);
    }

    for (i = 0; i < send_length; i++)
    {
        buff[2]      = send_data[i * 2];
        buff[3]      = send_data[i * 2 + 1];
        send_buff[i] = strtol(buff, OS_NULL, 16);
    }

    uint64_t start = os_clocksource_gettime();

    for (i = 0; i < repeat; i++)
    {
        os_spi_send_then_recv(spidev, send_buff, send_length, recv_buff, recv_length);
    }

    uint64_t now = os_clocksource_gettime();

    int delta_time_us = (now - start) / 1000;
    int speed         = repeat * (send_length + recv_length) * USEC_PER_SEC / delta_time_us;

    os_kprintf("delta time: %d us, speed: %d\r\n", delta_time_us, speed);

    os_kprintf("send data:\r\n");
    hex_dump(send_buff, send_length);

    os_kprintf("recv data:\r\n");
    hex_dump(recv_buff, recv_length);

    os_free(send_buff);

    if (recv_length != 0)
    {
        os_free(recv_buff);
    }

    os_device_close((os_device_t *)spidev);

    return OS_SUCCESS;
}

SH_CMD_EXPORT(spidev_transfer, spidev_transfer, "spidev transfer");
