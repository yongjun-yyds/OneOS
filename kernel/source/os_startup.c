/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        os_startup.c
 *
 * @brief       The starting process of OneOS.
 *
 * @details     During starting process, do something as follows:
 *                  1) Board initialization before scheduler is started (OS_CORE_INIT).
 *                  2) Tick queue initialization.
 *                  3) Scheduler system initialization.
 *                  4) Timer system initialization.
 *                  5) Recycle task initialization.
 *                  6) Idle task initialization.
 *                  7) Application initialization. The important job is to create a main task.
 *                  8) Start scheduler
 *
 * @revision
 * Date         Author          Notes
 * 2020-03-01   OneOS Team      First version
 * 2020-11-05   OneOS Team      Refactor starting process.
 ***********************************************************************************************************************
 */

#include <oneos_config.h>
#include <os_types.h>
#include <os_errno.h>
#include <os_version.h>
#include <os_util.h>

#include "os_kernel_internal.h"

#define STARTUP_TAG "STARTUP"

#ifndef OS_USING_HEAP
static OS_TASK_STACK_DEFINE(gs_os_sys_task_stack, OS_SYS_TASK_STACK_SIZE);
static OS_TASK_DEFINE(gs_os_sys_task);

#endif /* OS_USING_HEAP */

#if defined(__CC_ARM) || defined(__CLANG_ARM) /* ARM MDK Compiler */

static os_err_t pre_kernel_1_start(void)
{
    return OS_SUCCESS;
}
OS_INIT_CALL(pre_kernel_1_start, OS_INIT_LEVEL_PRE_KERNEL_1, "");

static os_err_t pre_kernel_2_start(void)
{
    return OS_SUCCESS;
}
OS_INIT_CALL(pre_kernel_2_start, OS_INIT_LEVEL_PRE_KERNEL_2, "");

static os_err_t post_kernel_start(void)
{
    return OS_SUCCESS;
}
OS_INIT_CALL(post_kernel_start, OS_INIT_LEVEL_POST_KERNEL, "");

static os_err_t pre_device_start(void)
{
    return OS_SUCCESS;
}
OS_INIT_CALL(pre_device_start, OS_INIT_LEVEL_PRE_DEVICE, "");

static os_err_t device_start(void)
{
    return OS_SUCCESS;
}
OS_INIT_CALL(device_start, OS_INIT_LEVEL_DEVICE, "");

static os_err_t component_start(void)
{
    return OS_SUCCESS;
}
OS_INIT_CALL(component_start, OS_INIT_LEVEL_COMPONENT, "");

static os_err_t application_start(void)
{
    return OS_SUCCESS;
}
OS_INIT_CALL(application_start, OS_INIT_LEVEL_APPLICATION, "");

#ifdef OS_USING_SMP
static os_err_t smp_start(void)
{
    return OS_SUCCESS;
}
OS_INIT_CALL(smp_start, OS_INIT_LEVEL_SMP, "");
#endif /*OS_USING_SMP*/

static os_err_t os_init_end(void)
{
    return OS_SUCCESS;
}
OS_INIT_CALL(os_init_end, OS_INIT_END_FLAG, "");

#elif defined(__GNUC__) /* for GCC Compiler */
extern const init_call_entry_t __os_init_start[];
extern const init_call_entry_t __init_call_pre_kernel_1_start[];
extern const init_call_entry_t __init_call_pre_kernel_2_start[];
extern const init_call_entry_t __init_call_post_kernel_start[];
extern const init_call_entry_t __init_call_pre_device_start[];
extern const init_call_entry_t __init_call_device_start[];
extern const init_call_entry_t __init_call_component_start[];
extern const init_call_entry_t __init_call_application_start[];
#ifdef OS_USING_SMP
extern const init_call_entry_t __init_call_smp_start[];
#endif /*OS_USING_SMP*/
extern const init_call_entry_t __os_init_end[];

#else
#error "not supported compiler"
#endif /*__CC_ARM*/

#if defined(__CC_ARM) || defined(__CLANG_ARM) /* ARM MDK Compiler */
static const init_call_entry_t *gs_init_call_table[] = {&_os_init_call_pre_kernel_1_start + 1,
                                                        &_os_init_call_pre_kernel_2_start + 1,
                                                        &_os_init_call_post_kernel_start + 1,
                                                        &_os_init_call_pre_device_start + 1,
                                                        &_os_init_call_device_start + 1,
                                                        &_os_init_call_component_start + 1,
                                                        &_os_init_call_application_start + 1,
#ifdef OS_USING_SMP
                                                        &_os_init_call_smp_start + 1,
#endif /*OS_USING_SMP*/
                                                        &_os_init_call_os_init_end};

#elif defined(__GNUC__) /* for GCC Compiler */
static const init_call_entry_t *gs_init_call_table[] = {__init_call_pre_kernel_1_start,
                                                        __init_call_pre_kernel_2_start,
                                                        __init_call_post_kernel_start,
                                                        __init_call_pre_device_start,
                                                        __init_call_device_start,
                                                        __init_call_component_start,
                                                        __init_call_application_start,
#ifdef OS_USING_SMP
                                                        __init_call_smp_start,
#endif /*OS_USING_SMP*/
                                                        __os_init_end};

#else
#error "not supported compiler"
#endif /*__CC_ARM*/

/**
 ***********************************************************************************************************************
 * @brief           Automatic initialization.
 *
 * @param           No parameter.
 *
 * @return          No return value.
 ***********************************************************************************************************************
 */
static os_err_t _k_run_init_call(int32_t level)
{
    os_err_t                          ret;
    volatile const init_call_entry_t *entry;

    for (entry = gs_init_call_table[level - 1]; entry < gs_init_call_table[level]; entry++)
    {
        ret = (*entry->func)();
        if (ret != OS_SUCCESS)
        {
#ifdef OS_INIT_CALL_DEBUG_EN
            OS_ASSERT_EX(OS_FALSE, "level %d automatic initialization faild of %s", level, entry->name);
#else
            OS_ASSERT_EX(OS_FALSE, "level %d automatic initialization faild", level);
#endif
        }
    }

    return OS_SUCCESS;
}

static void _k_sys_task_entry(void *arg)
{
    OS_UNREFERENCE(arg);

    /* post kernel init call */
    _k_run_init_call(OS_INIT_LEVEL_POST_KERNEL);

#ifdef OS_USING_SMP
    /* smp percpu init call*/
    _k_run_init_call(OS_INIT_LEVEL_SMP);
#endif

    /* prev device init call */
    _k_run_init_call(OS_INIT_LEVEL_PRE_DEVICE);

    /* device init call */
    _k_run_init_call(OS_INIT_LEVEL_DEVICE);

    /* component init call */
    _k_run_init_call(OS_INIT_LEVEL_COMPONENT);

    /* application init call */
    _k_run_init_call(OS_INIT_LEVEL_APPLICATION);

    void app_entry(void);
    app_entry();

    return;
}

static void _k_sys_task_init(void)
{
    os_err_t   ret;
    os_task_id tid;

#ifdef OS_USING_HEAP
    tid = os_task_create(OS_NULL, OS_NULL, OS_SYS_TASK_STACK_SIZE, "sys_task", _k_sys_task_entry, OS_NULL, 0U);
#else
    tid = os_task_create(&gs_os_sys_task,
                         OS_TASK_STACK_BEGIN_ADDR(gs_os_sys_task_stack),
                         OS_SYS_TASK_STACK_SIZE,
                         "sys_task",
                         _k_sys_task_entry,
                         OS_NULL,
                         0U);
#endif /* OS_USING_HEAP */

    if (!tid)
    {
        OS_ASSERT_EX(OS_FALSE, "Why initialize sys_task failed?");
    }

    /* Startup */
    ret = os_task_startup(tid);
    if (OS_SUCCESS != ret)
    {
        OS_ASSERT_EX(OS_FALSE, "Why startup sys_task failed?");
    }

    return;
}

static void _k_show_sys_info(void)
{
#ifdef OS_USING_KERNEL_DEBUG
    os_kprintf("OneOS start, version:%s\r\n", ONEOS_VERSION);
#endif
}

void os_kernel_init(void)
{
    /* pre kernel 1 init call */
    _k_run_init_call(OS_INIT_LEVEL_PRE_KERNEL_1);

    /* Show version information. */
    _k_show_sys_info();

    k_tickq_init();

    k_sched_init();

#ifdef OS_USING_HASH_BUCKET_TIMER
    k_timer_module_init();
#endif

#ifdef OS_USING_SINGLE_LIST_TIMER
    k_single_list_timer_module_init();
#endif

    k_recycle_task_init();

    k_idle_task_init();

    _k_sys_task_init();
}

void os_kernel_start(void)
{
    /* pre kernel 2 init call */
    _k_run_init_call(OS_INIT_LEVEL_PRE_KERNEL_2);

    /* start first task */
    k_start();
}
