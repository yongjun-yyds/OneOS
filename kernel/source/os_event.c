/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        os_event.c
 *
 * @brief       This file implements the event functions.
 *
 * @revision
 * Date         Author          Notes
 * 2020-12-24   OneOS team      First Version
 ***********************************************************************************************************************
 */

#include <oneos_config.h>
#include <os_errno.h>
#include <os_event.h>
#include <arch_interrupt.h>
#include <string.h>
#include <os_spinlock.h>

#include "os_kernel_internal.h"

#ifdef OS_USING_EVENT

#define EVENT_TAG "EVENT"

static os_list_node_t gs_os_event_resource_list_head = OS_LIST_INIT(gs_os_event_resource_list_head);
static OS_DEFINE_SPINLOCK(gs_os_event_resource_list_lock);

OS_INLINE uint32_t _k_event_flag_check(os_event_t *event, uint32_t interested_set, uint32_t option)
{
    uint32_t recved_set;

    recved_set = 0;

    if ((option & OS_EVENT_OPTION_AND) != 0U)
    {
        if ((event->set & interested_set) == interested_set)
        {
            recved_set = interested_set;
        }
    }
    else if ((option & OS_EVENT_OPTION_OR) != 0U)
    {
        if ((event->set & interested_set) != 0U)
        {
            recved_set = interested_set & event->set;
        }
    }
    else
    {
        OS_ASSERT_EX(OS_FALSE, "Check event option parameter error [%s]", _k_task_self()->name);
    }

    if (0U != recved_set)
    {
        /* Received event. */
        if ((option & OS_EVENT_OPTION_CLEAR) != 0U)
        {
            event->set &= ~recved_set;
        }
    }

    return recved_set;
}

OS_INLINE void _k_event_init(os_event_t *event, const char *name, uint8_t object_alloc_type)
{
    os_list_init(&event->task_list_head);

    if (OS_NULL != name)
    {
        (void)strncpy(&event->name[0], name, OS_NAME_MAX);
        event->name[OS_NAME_MAX] = '\0';
    }
    else
    {
        event->name[0] = '\0';
    }

#if defined(OS_USING_SMP) && !defined(OS_IPC_MONOLOCK_MODE)
    os_spin_lock_init(&event->lock);
#endif

    event->set               = 0U;
    event->wake_type         = OS_EVENT_WAKE_TYPE_PRIO;
    event->object_alloc_type = object_alloc_type;
    event->object_inited     = OS_KOBJ_INITED;

    return;
}

/**
 ***********************************************************************************************************************
 * @brief           This function will create an event object.
 *
 * @param[in]       event_cb       The static cb of event to create(if NULL, dynamic create will do).
 * @param[in]       name           The name of event.
 *
 * @return          The ID of event.
 * @retval          The ID of event.
 ***********************************************************************************************************************
 */
os_event_id os_event_create(os_event_dummy_t *event_cb, const char *name)
{
    const os_event_t     *iter_event;
    const os_list_node_t *pos;
    os_event_t           *event;
    uint8_t               alloc_type;
    os_bool_t             flag;

#ifndef OS_USING_HEAP
    OS_ASSERT(OS_NULL != event_cb);
#endif
    OS_ASSERT(OS_FALSE == os_is_irq_active());

    flag = OS_TRUE;

    if (OS_NULL != event_cb)
    {
        event = OS_TYPE_CONVERT(os_event_t *, event_cb);

        alloc_type = OS_ALLOC_TYPE_STATIC;

        os_spin_lock(&gs_os_event_resource_list_lock);

        os_list_for_each(pos, &gs_os_event_resource_list_head)
        {
            iter_event = os_list_entry(pos, os_event_t, resource_node);
            if (iter_event == event)
            {
                event = OS_NULL;
                os_spin_unlock(&gs_os_event_resource_list_lock);
                OS_KERN_LOG(KERN_ERROR, EVENT_TAG, "%s", (OS_KERNEL_ERR_INFO));
                flag = OS_FALSE;
                break;
            }
        }
    }
#ifdef OS_USING_HEAP
    else
    {
        alloc_type = OS_KOBJ_ALLOC_TYPE_DYNAMIC;
        event      = (os_event_t *)OS_KERNEL_MALLOC(sizeof(os_event_t));
        if (OS_NULL == event)
        {
            OS_KERN_LOG(KERN_ERROR, EVENT_TAG, "Malloc event memory failed");
            flag = OS_FALSE;
        }
    }
#endif

    if (OS_TRUE == flag)
    {
        _k_event_init(event, name, alloc_type);
        if (OS_KOBJ_ALLOC_TYPE_DYNAMIC == (alloc_type & OS_KOBJ_ALLOC_TYPE_DYNAMIC))
        {
            os_spin_lock(&gs_os_event_resource_list_lock);
        }
        os_list_add_tail(&gs_os_event_resource_list_head, &event->resource_node);
        os_spin_unlock(&gs_os_event_resource_list_lock);
    }

    return OS_TYPE_CONVERT(os_event_id, event);
}

/**
 ***********************************************************************************************************************
 * @brief           Destroy a event obj.
 *
 * @param[in]       event_id        The id of event object.
 *
 * @return          The result of destroy the event.
 * @retval          OS_SUCCESS      Destroy the event successfully.
 * @retval          else            Destroy the event failed.
 ***********************************************************************************************************************
 */
os_err_t os_event_destroy(os_event_id event_id)
{
    os_bool_t   need_sched;
    os_event_t *event;

    OS_KERNEL_INIT();

    event = OS_TYPE_CONVERT(os_event_t *, event_id);

    OS_ASSERT(OS_NULL != event);
    OS_ASSERT(OS_KOBJ_INITED == event->object_inited);
    OS_ASSERT(OS_FALSE == os_is_irq_active());

#ifndef OS_USING_HEAP
    OS_ASSERT(OS_ALLOC_TYPE_STATIC == event->object_alloc_type);
#endif

#if defined(OS_USING_SMP) && !defined(OS_IPC_MONOLOCK_MODE)
    os_spin_lock_irq(&event->lock, &irq_save);
#else
    OS_KERNEL_ENTER();
#endif

    event->object_inited = OS_KOBJ_DEINITED;
    event->set           = 0U;

    /* Wakeup all suspend tasks */
    need_sched = k_cancel_all_blocked_task(&event->task_list_head);

#if defined(OS_USING_SMP) && !defined(OS_IPC_MONOLOCK_MODE)
    os_spin_unlock_irq(&event->lock, irq_save);

    if (OS_TRUE == need_sched)
    {
        OS_KERNEL_ENTER();
        OS_KERNEL_EXIT_SCHED();
    }
#else
    if (OS_TRUE == need_sched)
    {
        OS_KERNEL_EXIT_SCHED();
    }
    else
    {
        OS_KERNEL_EXIT();
    }
#endif

    os_spin_lock(&gs_os_event_resource_list_lock);
    os_list_del(&event->resource_node);
    os_spin_unlock(&gs_os_event_resource_list_lock);
#ifdef OS_USING_HEAP
    if (OS_KOBJ_ALLOC_TYPE_DYNAMIC == event->object_alloc_type)
    {
        OS_KERNEL_FREE(event);
    }
#endif

    return OS_SUCCESS;
}

/**
 ***********************************************************************************************************************
 * @brief           This function sends an event to event object. If there are tasks blocked on the event object, tasks
 *                  interested this specific event will be woken up.
 *
 * @param[in]       event_id        The id of event object.
 * @param[in]       set             The specific event set.
 *
 * @return          The operation result.
 * @retval          OS_SUCCESS      If the operation successful.
 * @retval          else            Error code.
 ***********************************************************************************************************************
 */
os_err_t os_event_send(os_event_id event_id, uint32_t set)
{
    os_bool_t   need_sched;
    uint32_t    recved_set_check;
    os_task_t  *block_task;
    os_task_t  *block_task_next;
    os_event_t *event;

    OS_KERNEL_INIT();

    event = OS_TYPE_CONVERT(os_event_t *, event_id);

    OS_ASSERT(OS_NULL != event);
    OS_ASSERT(OS_KOBJ_INITED == event->object_inited);
    OS_ASSERT(0U != set);

    need_sched = OS_FALSE;

#if defined(OS_USING_SMP) && !defined(OS_IPC_MONOLOCK_MODE)
    os_spin_lock_irq(&event->lock, &irq_save);
    k_kernel_spin_lock();
#else
    OS_KERNEL_ENTER();
#endif

    event->set |= set;

    /* Search block task list and unblock suitable tasks. */
    os_list_for_each_entry_safe(block_task, block_task_next, &event->task_list_head, os_task_t, task_node)
    {
        recved_set_check = _k_event_flag_check(event, block_task->event_set, block_task->event_option);
        if (0U != recved_set_check)
        {
            block_task->event_set = recved_set_check;

            if (OS_TRUE == k_unblock_task(block_task))
            {
                need_sched = OS_TRUE;
            }
        }

        if (0U == event->set)
        {
            break;
        }
    }

#if defined(OS_USING_SMP) && !defined(OS_IPC_MONOLOCK_MODE)
    k_kernel_spin_unlock();
    os_spin_unlock_irq(&event->lock, irq_save);

    if (OS_TRUE == need_sched)
    {
        OS_KERNEL_ENTER();
        OS_KERNEL_EXIT_SCHED();
    }
#else
    if (OS_TRUE == need_sched)
    {
        OS_KERNEL_EXIT_SCHED();
    }
    else
    {
        OS_KERNEL_EXIT();
    }
#endif

    return OS_SUCCESS;
}

/**
 ***********************************************************************************************************************
 * @brief           This function recieves an event from event object. If there is no interested event has been sent,
 *the calling task will be blocked until either interested event is sent by other task or waiting time expires.
 *
 * @param[in]       event_id        The id of event object.
 * @param[in]       interested_set  The intereseted event set.
 * @param[in]       option          The option of OS_EVENT_OPTION_AND, OS_EVENT_OPTION_OR and OS_EVENT_OPTION_CLEAR.
 * @param[in]       timeout         Waiting time (in clock ticks).
 * @param[out]      recved_set      The actual recieved event set.
 *
 * @return          The operation result.
 * @retval          OS_SUCCESS      If the operation successful.
 * @retval          else            Error code.
 ***********************************************************************************************************************
 */
os_err_t
os_event_recv(os_event_id event_id, uint32_t interested_set, uint32_t option, os_tick_t timeout, uint32_t *recved_set)
{
    os_task_t  *current_task;
    uint32_t    recved_set_check;
    os_err_t    ret;
    os_event_t *event;

    OS_KERNEL_INIT();

    event = OS_TYPE_CONVERT(os_event_t *, event_id);

    OS_ASSERT(OS_NULL != event);
    OS_ASSERT(OS_KOBJ_INITED == event->object_inited);
    OS_ASSERT((OS_NO_WAIT == timeout) || (OS_NULL == k_task_self()) || (OS_FALSE == os_is_irq_active()));
    OS_ASSERT((OS_NO_WAIT == timeout) || (OS_NULL == k_task_self()) || (OS_FALSE == os_is_irq_disabled()));
    OS_ASSERT((OS_NO_WAIT == timeout) || (OS_NULL == k_task_self()) || (OS_FALSE == os_is_schedule_locked()));
    OS_ASSERT((timeout < (OS_TICK_MAX / 2)) || (OS_WAIT_FOREVER == timeout));
    OS_ASSERT(0U != interested_set);

    ret = OS_SUCCESS;

    /*Check parameter*/
    if (((option & (OS_EVENT_OPTION_OR | OS_EVENT_OPTION_AND)) == (OS_EVENT_OPTION_OR | OS_EVENT_OPTION_AND)) ||
        ((option & (OS_EVENT_OPTION_OR | OS_EVENT_OPTION_AND)) == 0))
    {
        OS_KERN_LOG(KERN_ERROR, EVENT_TAG, "Check event option parameter error [%s]", k_task_self()->name);
        ret = OS_INVAL;
    }

    if (OS_SUCCESS == ret)
    {
#if defined(OS_USING_SMP) && !defined(OS_IPC_MONOLOCK_MODE)
        os_spin_lock_irq(&event->lock, &irq_save);
#else
        OS_KERNEL_ENTER();
#endif

        recved_set_check = _k_event_flag_check(event, interested_set, option);
        if (0U != recved_set_check)
        {
            if (OS_NULL != (recved_set))
            {
                *recved_set = recved_set_check;
            }
        }
        else
        {
            if (OS_NO_WAIT == timeout)
            {
                ret = OS_EMPTY;
            }
            else
            {
                current_task = _k_task_self();
                OS_ASSERT((OS_NULL != current_task));
                current_task->event_set    = interested_set;
                current_task->event_option = option;

                /* Block current task */

                if (OS_EVENT_WAKE_TYPE_PRIO == event->wake_type)
                {
                    ret = k_block_task(&event->lock, irq_save, &event->task_list_head, timeout, OS_TRUE);
                }
                else
                {
                    ret = k_block_task(&event->lock, irq_save, &event->task_list_head, timeout, OS_FALSE);
                }

                if (OS_SUCCESS == ret)
                {
                    if ((OS_NULL != recved_set))
                    {
                        *recved_set = current_task->event_set;
                    }
                }
                else if (OS_TIMEOUT == ret)
                {
#if defined(OS_USING_SMP) && !defined(OS_IPC_MONOLOCK_MODE)
                    os_spin_lock_irq(&event->lock, &irq_save);
#else
                    OS_KERNEL_ENTER();
#endif

                    recved_set_check = _k_event_flag_check(event, interested_set, option);

                    if (0U != recved_set_check)
                    {
                        if ((OS_NULL != recved_set))
                        {
                            *recved_set = recved_set_check;
                        }
                        ret = OS_SUCCESS;
                    }
                    else
                    {
                        if ((OS_NULL != recved_set))
                        {
                            *recved_set = interested_set & event->set;
                        }
                    }

#if defined(OS_USING_SMP) && !defined(OS_IPC_MONOLOCK_MODE)
                    os_spin_unlock_irq(&event->lock, irq_save);
#else
                    OS_KERNEL_EXIT();
#endif
                }
                else if (OS_FAILURE == ret)
                {
                    if ((OS_NULL != recved_set))
                    {
                        *recved_set = interested_set & event->set;
                    }
                }
                else
                {
                    ;
                }

                return ret;
            }
        }

#if defined(OS_USING_SMP) && !defined(OS_IPC_MONOLOCK_MODE)
        os_spin_unlock_irq(&event->lock, irq_save);
#else
        OS_KERNEL_EXIT();
#endif
    }

    return ret;
}

os_err_t
os_event_sync(os_event_id event_id, uint32_t set, uint32_t interested_set, os_tick_t timeout, uint32_t *recved_set)
{
    os_task_t  *current_task;
    os_err_t    ret;
    uint32_t    recved_set_check;
    uint32_t    original_set;
    uint32_t    clear_bit;
    os_bool_t   need_sched;
    os_task_t  *block_task;
    os_task_t  *block_task_next;
    os_event_t *event;

    OS_KERNEL_INIT();

    event = OS_TYPE_CONVERT(os_event_t *, event_id);

    OS_ASSERT(OS_NULL != event);
    OS_ASSERT(OS_KOBJ_INITED == event->object_inited);
    OS_ASSERT((OS_NO_WAIT == timeout) || (OS_NULL == k_task_self()) || (OS_FALSE == os_is_irq_active()));
    OS_ASSERT((OS_NO_WAIT == timeout) || (OS_NULL == k_task_self()) || (OS_FALSE == os_is_irq_disabled()));
    OS_ASSERT((OS_NO_WAIT == timeout) || (OS_NULL == k_task_self()) || (OS_FALSE == os_is_schedule_locked()));
    OS_ASSERT((timeout < (OS_TICK_MAX / 2)) || (OS_WAIT_FOREVER == timeout));
    OS_ASSERT(0U != interested_set);

    ret              = OS_SUCCESS;
    recved_set_check = 0;
    clear_bit        = 0;
    need_sched       = OS_FALSE;

#if defined(OS_USING_SMP) && !defined(OS_IPC_MONOLOCK_MODE)
    os_spin_lock_irq(&event->lock, &irq_save);
#else
    OS_KERNEL_ENTER();
#endif

    event->set |= set;
    original_set = event->set;

#if defined(OS_USING_SMP) && !defined(OS_IPC_MONOLOCK_MODE)
    k_kernel_spin_lock();
#endif

    /* Search block task list and unblock suitable tasks. */
    os_list_for_each_entry_safe(block_task, block_task_next, &event->task_list_head, os_task_t, task_node)
    {
        if ((block_task->event_option & OS_EVENT_OPTION_AND) != 0U)
        {
            if ((event->set & block_task->event_set) == block_task->event_set)
            {
                recved_set_check = block_task->event_set;
            }
        }
        else if ((block_task->event_option & OS_EVENT_OPTION_OR) != 0U)
        {
            if ((event->set & block_task->event_set) != 0U)
            {
                recved_set_check = block_task->event_set & event->set;
            }
        }
        else
        {
            OS_ASSERT_EX(OS_FALSE, "Check event option parameter error [%s]", block_task->name);
        }

        if (0U != recved_set_check)
        {
            /* Received event. */
            if ((block_task->event_option & OS_EVENT_OPTION_CLEAR) != 0U)
            {
                /* store clear bit in temporary varialble. */
                clear_bit |= block_task->event_set;
            }

            block_task->event_set = recved_set_check;

            if (OS_TRUE == k_unblock_task(block_task))
            {
                need_sched = OS_TRUE;
            }
        }
    }

#if defined(OS_USING_SMP) && !defined(OS_IPC_MONOLOCK_MODE)
    k_kernel_spin_unlock();
#endif

    /* Now clear event bit. */
    event->set &= ~clear_bit;

    /* check if all bit has be set */
    if ((original_set & interested_set) == interested_set)
    {
        /* all bit has be set , always clear event here*/
        event->set &= ~interested_set;

        if (recved_set != OS_NULL)
        {
            *recved_set = interested_set;
        }

        /* all bit has be set, there must be an unlocked task , schedule */
        need_sched = OS_TRUE;
    }
    else
    {
        if (OS_NO_WAIT == timeout)
        {
            /* not all bit has be set. */
            if (recved_set != OS_NULL)
            {
                *recved_set = interested_set & event->set;
            }

            ret = OS_TIMEOUT;
        }
        else
        {
            current_task = _k_task_self();
            OS_ASSERT((OS_NULL != current_task));
            current_task->event_set    = interested_set;
            current_task->event_option = OS_EVENT_OPTION_AND | OS_EVENT_OPTION_CLEAR;

            /* Block current task */
            if (OS_EVENT_WAKE_TYPE_PRIO == event->wake_type)
            {
                ret = k_block_task(&event->lock, irq_save, &event->task_list_head, timeout, OS_TRUE);
            }
            else
            {
                ret = k_block_task(&event->lock, irq_save, &event->task_list_head, timeout, OS_FALSE);
            }

            if (OS_SUCCESS == ret)
            {
                if (recved_set != OS_NULL)
                {
                    *recved_set = current_task->event_set;
                }
            }
            else if (OS_TIMEOUT == ret)
            {
#if defined(OS_USING_SMP) && !defined(OS_IPC_MONOLOCK_MODE)
                os_spin_lock_irq(&event->lock, &irq_save);
#else
                OS_KERNEL_ENTER();
#endif

                if (recved_set != OS_NULL)
                {
                    *recved_set = interested_set & event->set;
                }

                if ((event->set & interested_set) == interested_set)
                {
                    event->set &= ~interested_set;

                    ret = OS_SUCCESS;
                }

#if defined(OS_USING_SMP) && !defined(OS_IPC_MONOLOCK_MODE)
                os_spin_unlock_irq(&event->lock, irq_save);
#else
                OS_KERNEL_EXIT();
#endif
            }
            else if (OS_FAILURE == ret)
            {
                if (recved_set != OS_NULL)
                {
                    *recved_set = interested_set & event->set;
                }
            }
            else
            {
                ;
            }

            return ret;
        }
    }

#if defined(OS_USING_SMP) && !defined(OS_IPC_MONOLOCK_MODE)
    os_spin_unlock_irq(&event->lock, irq_save);

    if (OS_TRUE == need_sched)
    {
        OS_KERNEL_ENTER();
        OS_KERNEL_EXIT_SCHED();
    }
#else
    if (OS_TRUE == need_sched)
    {
        OS_KERNEL_EXIT_SCHED();
    }
    else
    {
        OS_KERNEL_EXIT();
    }
#endif

    return ret;
}

/**
 ***********************************************************************************************************************
 * @brief           This function clear event's mask.
 *
 * @param[in]       event_id          The id of event object.
 * @param[in]       interested_clear  Clear the bits.
 *
 * @return          The operation result.
 * @retval          OS_SUCCESS      If the operation successful.
 * @retval          else            Error code.
 ***********************************************************************************************************************
 */
os_err_t os_event_clear(os_event_id event_id, uint32_t interested_clear)
{
    os_event_t *event;

    OS_KERNEL_INIT();

    event = OS_TYPE_CONVERT(os_event_t *, event_id);

    OS_ASSERT(OS_NULL != event);
    OS_ASSERT(OS_KOBJ_INITED == event->object_inited);

#if defined(OS_USING_SMP) && !defined(OS_IPC_MONOLOCK_MODE)
    os_spin_lock_irq(&event->lock, &irq_save);
#else
    OS_KERNEL_ENTER();
#endif

    event->set &= ~interested_clear;

#if defined(OS_USING_SMP) && !defined(OS_IPC_MONOLOCK_MODE)
    os_spin_unlock_irq(&event->lock, irq_save);
#else
    OS_KERNEL_EXIT();
#endif

    return OS_SUCCESS;
}

/**
 ***********************************************************************************************************************
 * @brief           This function get event's value.
 *
 * @param[in]       event_id        The id of event object.
 * @param[out]      set             A pointer to return the set of events.
 *
 * @return          The operation result.
 * @retval          OS_SUCCESS      If the operation successful.
 * @retval          else            Error code.
 ***********************************************************************************************************************
 */
os_err_t os_event_get(os_event_id event_id, uint32_t *set)
{
    os_event_t *event;

    event = OS_TYPE_CONVERT(os_event_t *, event_id);

    OS_ASSERT(OS_NULL != event);
    OS_ASSERT(OS_KOBJ_INITED == event->object_inited);
    OS_ASSERT(OS_NULL != set);

    *set = event->set;

    return OS_SUCCESS;
}

/**
 ***********************************************************************************************************************
 * @brief           This function to set the event wake type(by prio or fifo)
 *
 * @param[in]       event_id        The message queue id to set.
 * @param[in]       wake_type       The message queue wake type to set.
 *
 * @return          The operation result.
 * @retval          OS_SUCCESS      If the operation successful.
 * @retval          else            Error code.
 ***********************************************************************************************************************
 */
os_err_t os_event_set_wake_type(os_event_id event_id, uint8_t wake_type)
{
    os_err_t    ret;
    os_event_t *event;

    OS_KERNEL_INIT();

    event = OS_TYPE_CONVERT(os_event_t *, event_id);

    OS_ASSERT(OS_NULL != event);
    OS_ASSERT(OS_KOBJ_INITED == event->object_inited);
    OS_ASSERT((OS_EVENT_WAKE_TYPE_PRIO == wake_type) || (OS_EVENT_WAKE_TYPE_FIFO == wake_type));

    ret = OS_BUSY;

#if defined(OS_USING_SMP) && !defined(OS_IPC_MONOLOCK_MODE)
    os_spin_lock_irq(&event->lock, &irq_save);
    k_kernel_spin_lock();
#else
    OS_KERNEL_ENTER();
#endif

    if (os_list_empty(&event->task_list_head))
    {
        event->wake_type = wake_type;
        ret              = OS_SUCCESS;
    }

#if defined(OS_USING_SMP) && !defined(OS_IPC_MONOLOCK_MODE)
    k_kernel_spin_unlock();
    os_spin_unlock_irq(&event->lock, irq_save);
#else
    OS_KERNEL_EXIT();
#endif

    return ret;
}

#if defined(OS_USING_SHELL) && defined(OS_USING_HEAP)
#include <shell.h>

#define SH_SHOW_TASK_CNT_MAX 10

typedef struct
{
    os_event_t *event;
    uint32_t    set;

    uint16_t          block_task_count;
    block_task_info_t block_info[SH_SHOW_TASK_CNT_MAX];
} sh_event_info_t;

static os_err_t os_event_show(os_event_t *event)
{
    sh_event_info_t event_info;
    os_task_t      *iter_task;
    uint16_t        task_index;

    OS_KERNEL_INIT();

    if ((OS_NULL == event) || (OS_KOBJ_INITED != event->object_inited))
    {
        os_kprintf("The input parameter is an illegal event object.\r\n");
        return OS_FAILURE;
    }

#if defined(OS_USING_SMP) && !defined(OS_IPC_MONOLOCK_MODE)
    os_spin_lock_irq(&event->lock, &irq_save);
    k_kernel_spin_lock();
#else
    OS_KERNEL_ENTER();
#endif

    event_info.event            = event;
    event_info.set              = event->set;
    event_info.block_task_count = os_list_len(&event->task_list_head);

    task_index = 0;
    os_list_for_each_entry(iter_task, &event->task_list_head, os_task_t, task_node)
    {
        event_info.block_info[task_index].current_priority = iter_task->current_priority;
        event_info.block_info[task_index].name             = iter_task->name;
        task_index++;

        if (task_index >= SH_SHOW_TASK_CNT_MAX)
        {
            break;
        }
    }

#if defined(OS_USING_SMP) && !defined(OS_IPC_MONOLOCK_MODE)
    k_kernel_spin_unlock();
    os_spin_unlock_irq(&event->lock, irq_save);
#else
    OS_KERNEL_EXIT();
#endif

    os_kprintf("%-*s 0x%-8x ",
               OS_NAME_MAX,
               (event_info.event->name[0] != '\0') ? event_info.event->name : "-",
               event_info.set);

    if (event_info.block_task_count > 0)
    {
        os_kprintf("    %-6u:", event_info.block_task_count);
        k_show_blocked_task(event_info.block_info, task_index);
        if (event_info.block_task_count > task_index)
        {
            os_kprintf("/...\r\n");
        }
        else
        {
            os_kprintf("\r\n");
        }
    }
    else
    {
        os_kprintf("    %-6u\r\n", 0);
    }

    return OS_SUCCESS;
}

/**
 ***********************************************************************************************************************
 * @brief           This function prints information about all the event and it's corresponding blokced tasks.
 *
 * @param[in]       argc            Argment count.
 * @param[in]       argv            Argment list.
 *
 * @return          The operation result.
 * @retval          OS_SUCCESS      If the operation successful.
 * @retval          else            Error code.
 ***********************************************************************************************************************
 */
static os_err_t sh_show_event_info(int32_t argc, char *const *argv)
{
    os_event_t *iter_event;
    os_event_t *event_tmp;
    uint16_t    len;

    OS_UNREFERENCE(argc);
    OS_UNREFERENCE(argv);

    os_kprintf("%-*s %-10s %-10s\r\n", OS_NAME_MAX, "Event", "Set", "Block Task");

    len = OS_NAME_MAX;
    while ((len--) != 0)
    {
        os_kprintf("-");
    }
    os_kprintf(" ---------- ----------\r\n");

    if (argc >= 2)
    {
        if ((OS_NULL != argv[1]))
        {
            event_tmp = (os_event_t *)strtoul(argv[1], OS_NULL, 0);

            os_spin_lock(&gs_os_event_resource_list_lock);
            os_list_for_each_entry(iter_event, &gs_os_event_resource_list_head, os_event_t, resource_node)
            {
                if (event_tmp == iter_event)
                {
                    os_event_show(event_tmp);
                    os_spin_unlock(&gs_os_event_resource_list_lock);
                    return OS_SUCCESS;
                }
            }
            os_spin_unlock(&gs_os_event_resource_list_lock);
        }

        os_kprintf("Invalid Event Object.\r\n");
        return OS_FAILURE;
    }

    os_spin_lock(&gs_os_event_resource_list_lock);
    os_list_for_each_entry(iter_event, &gs_os_event_resource_list_head, os_event_t, resource_node)
    {
        os_event_show(iter_event);
    }
    os_spin_unlock(&gs_os_event_resource_list_lock);

    return OS_SUCCESS;
}
SH_CMD_EXPORT(show_event, sh_show_event_info, "show event information");

#endif /* defined(OS_USING_SHELL) */

#endif /* OS_USING_EVENT */
