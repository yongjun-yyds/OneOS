/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        os_idle.c
 *
 * @brief       This file implements the idle task.
 *
 * @revision
 * Date         Author          Notes
 * 2020-12-24   OneOS team      First Version
 ***********************************************************************************************************************
 */

#include <oneos_config.h>
#include <os_types.h>
#include <os_stddef.h>
#include <os_errno.h>
#include <stdio.h>
#include <os_task.h>

#include "os_kernel_internal.h"

#define IDLE_TAG "IDLE"
#ifdef OS_USING_SMP
OS_ALIGN(OS_ARCH_STACK_ALIGN_SIZE)
uint8_t gs_os_idle_stack[OS_SMP_MAX_CPUS][OS_ALIGN_UP(OS_IDLE_TASK_STACK_SIZE, OS_ARCH_STACK_ALIGN_SIZE)];
static os_task_dummy_t gs_os_idle_task[OS_SMP_MAX_CPUS];
#else
static OS_TASK_STACK_DEFINE(gs_os_idle_stack, OS_IDLE_TASK_STACK_SIZE);
static os_task_dummy_t gs_os_idle_task;
#endif

#ifdef OS_USING_TICKLESS_LPMGR
extern void os_low_power_manager(void);
#endif

static void _k_idle_task_entry(void *arg)
{
    OS_UNREFERENCE(arg);

    while (1)
    {
        /* TODO: */
        ;
#ifdef OS_USING_TICKLESS_LPMGR
        os_low_power_manager();
#endif
    }
}

void k_idle_task_init(void)
{
    os_err_t   ret;
    os_task_id tid;

#ifdef OS_USING_SMP
    char    task_name[OS_NAME_MAX + 1];
    int32_t i;

    task_name[OS_NAME_MAX] = '\0';
    for (i = 0; i < OS_SMP_MAX_CPUS; i++)
    {
        snprintf(task_name, OS_NAME_MAX, "idle%d", i);

        tid = os_task_create(&gs_os_idle_task[i],
                             &gs_os_idle_stack[i],
                             OS_IDLE_TASK_STACK_SIZE,
                             task_name,
                             _k_idle_task_entry,
                             OS_NULL,

                             OS_TASK_PRIORITY_MAX - 1);

        if (!tid)
        {
            OS_ASSERT_EX(0, "Why initialize idle task failed?");
        }

        os_task_set_cpu_affinity(tid, i);
        /* Startup */
        ret = os_task_startup(tid);
        if (OS_SUCCESS != ret)
        {
            OS_ASSERT_EX(0, "Why startup idle task failed?");
        }
    }
#else
    /* Initialize idle task */
    tid = os_task_create(&gs_os_idle_task,
                         OS_TASK_STACK_BEGIN_ADDR(gs_os_idle_stack),
                         OS_TASK_STACK_SIZE(gs_os_idle_stack),
                         OS_IDLE_TASK_NAME,
                         _k_idle_task_entry,
                         OS_NULL,

                         OS_TASK_PRIORITY_MAX - 1);

    if (!tid)
    {
        OS_ASSERT_EX(0, "Why initialize idle task failed?");
    }

    /* Startup */
    ret = os_task_startup(tid);
    if (OS_SUCCESS != ret)
    {
        OS_ASSERT_EX(0, "Why startup idle task failed?");
    }
#endif

    return;
}
