/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        os_prototypes.h
 *
 * @brief       Provides some macro definitions and function declarations, only use for kernel.
 *
 * @revision
 * Date         Author          Notes
 * 2020-03-03   OneOS Team      First version.
 ***********************************************************************************************************************
 */
#ifndef __OS_PROTOTYPES_H__
#define __OS_PROTOTYPES_H__

#include <oneos_config.h>
#include <os_types.h>
#include <os_stddef.h>
#include <os_list.h>
#include <os_task.h>
#include <os_spinlock.h>
#include <os_timer.h>
#include <os_dummy.h>

#if defined(OS_USING_CPU_MONITOR)
#include <cpu_monitor.h>
#endif

/**
 ***********************************************************************************************************************
 * @struct      struct os_task
 *
 * @brief       Define control block of task.
 ***********************************************************************************************************************
 */
struct os_task
{
    /* begin: The order, position and content cannot be changed */
    void *stack_top;   /* Point to SP */
    void *stack_begin; /* The begin address of task stack */
    void *stack_end;   /* The end address of task stack */

    uint16_t state; /* Task state, refer to OS_TASK_STATE_INIT, OS_TASK_STATE_READY ... */
    /* end:   The order, position and content cannot be changed */

    uint8_t current_priority; /* Current priority. */
    uint8_t backup_priority;  /* Backup priority. */

    os_err_t err_code;      /* Error code. */
    os_err_t switch_retval; /* Task switch return value, defined in os_errno.h*/

    uint8_t object_inited;     /* If task object is inited, value is OS_KOBJ_INITED */
    uint8_t object_alloc_type; /* Indicates whether memory is allocated dynamically or statically,
                                   value is OS_KOBJ_ALLOC_TYPE_STATIC or OS_KOBJ_ALLOC_TYPE_DYNAMIC */
    uint8_t pad[2];

    os_list_node_t resource_node; /* Node in resource list */
    os_list_node_t task_node;     /* Node in ready queue or blocking queue */

    os_list_node_t tick_node;     /* Node in tick queue */
    os_tick_t      tick_timeout;  /* Timeout */
    os_tick_t      tick_absolute; /* Absolute time of timeout */

    os_tick_t time_slice;           /* Task's slice time (unit: tick). */
    os_tick_t remaining_time_slice; /* Task's remaining slice time (unit: tick). */

    os_list_node_t *block_list_head; /* Where the task is blocked at */
    os_bool_t       is_wake_prio;    /* The wake type to task at block_list_head, according to priority or not */

#if defined(OS_USING_EVENT)
    uint32_t event_set;
    uint32_t event_option;
#endif

#if defined(OS_USING_MUTEX)
    os_list_node_t hold_mutex_list_head;
#endif

#if defined(OS_USING_SMP)
    int32_t cpu_index; /* The number of the cpu that the task is running on.
                             -1 means the task is not running. */
    int32_t cpu_affinity;

    os_list_node_t join_node; /* Node in join queue */
#endif

    os_ubase_t swap_data;

    void (*cleanup)(void *user_data); /* The cleanup function is provided by the user */
    void *user_data;                  /* Private user data beyond this task. */

    char name[OS_NAME_MAX + 1]; /* Task name */

#if defined(OS_USING_CPU_MONITOR)
    cpu_use_info_t usage_info;
#endif
};
typedef struct os_task os_task_t;

/**
 ***********************************************************************************************************************
 * @struct      struct os_mutex
 *
 * @brief       Define control block of mutex.
 ***********************************************************************************************************************
 */
struct os_mutex
{
    os_list_node_t task_list_head; /* Block task list head */
    os_list_node_t resource_node;  /* Node in resource list */
    os_list_node_t hold_node;

    os_task_t *owner; /* Mutex owner */

    uint32_t  lock_count;   /* Current lock count */
    os_bool_t is_recursive; /* Support recursive call. */

    uint8_t object_inited;     /* If mutex object is inited, value is OS_KOBJ_INITED */
    uint8_t object_alloc_type; /* Indicates whether memory is allocated dynamically or statically,
                                  value is OS_ALLOC_TYPE_STATIC or OS_KOBJ_ALLOC_TYPE_DYNAMIC */
    uint8_t wake_type;         /* The type to wake up blocking tasks, value is OS_MUTEX_WAKE_TYPE_PRIO
                                  or OS_MUTEX_WAKE_TYPE_FIFO */
    uint8_t original_priority;

    char name[OS_NAME_MAX + 1]; /* Mutex name */

    os_spinlock_t lock; /* Spin lock used under SMP */
};
typedef struct os_mutex os_mutex_t;

/**
 ***********************************************************************************************************************
 * @struct      struct os_mb
 *
 * @brief       Define control block of mailbox.
 ***********************************************************************************************************************
 */
struct os_mailbox
{
    void *mail_pool;       /* The address that doesn't do alignment for mail pool */
    void *mail_pool_align; /* Aligned address of mail pool. */

    os_list_node_t send_task_list_head; /* Sender tasks blocked on this mailbox. */
    os_list_node_t recv_task_list_head; /* Receiver tasks blocked on this mailbox. */

    os_list_node_t resource_node; /* Node in resource list */

    uint16_t capacity;    /* Max number of mails for this mailbox. */
    uint16_t used_mails;  /* Numbers of mails into mailbox. */
    uint16_t read_index;  /* Read index of mail pool. */
    uint16_t write_index; /* Write index of mail pool. */

    uint8_t object_inited;     /* Indicates whether object is inited or deinited, value is
                                  OS_KOBJ_INITED or OS_KOBJ_DEINITED */
    uint8_t object_alloc_type; /* Indicates whether object is allocated dynamically or statically,
                                  value is OS_ALLOC_TYPE_STATIC or OS_KOBJ_ALLOC_TYPE_DYNAMIC */
    uint8_t wake_type;         /* The type to wake up blocking tasks, value is OS_MB_WAKE_TYPE_PRIO
                                  or OS_MB_WAKE_TYPE_FIFO */

    char name[OS_NAME_MAX + 1]; /* Mailbox name. */

    os_spinlock_t lock; /* Spin lock used under SMP */
};
typedef struct os_mailbox os_mailbox_t;

/**
 ***********************************************************************************************************************
 * @struct      struct os_semaphore
 *
 * @brief       Define control block of semaphore.
 ***********************************************************************************************************************
 */
struct os_semaphore
{
    os_list_node_t task_list_head; /* Block task list head  */
    os_list_node_t resource_node;  /* Node in resource list */

    uint32_t count;             /* Current count */
    uint32_t max_count;         /* Semaphore support maximum value */
    uint8_t  object_inited;     /* If semaphore object is inited, value is OS_KOBJ_INITED */
    uint8_t  object_alloc_type; /* Indicates whether memory is allocated dynamically or statically,
                                   value is OS_ALLOC_TYPE_STATIC or OS_KOBJ_ALLOC_TYPE_DYNAMIC */
    uint8_t wake_type;          /* The type to wake up blocking tasks, value is OS_SEM_WAKE_TYPE_PRIO
                                   or OS_SEM_WAKE_TYPE_FIFO */

    char name[OS_NAME_MAX + 1]; /* Semaphore name */

    os_spinlock_t lock; /* Spin lock used under SMP */
};

typedef struct os_semaphore os_semaphore_t;

/**
 ***********************************************************************************************************************
 * @struct      struct os_event
 *
 * @brief       Define control block of event.
 ***********************************************************************************************************************
 */
struct os_event
{
    os_list_node_t task_list_head; /* Block task list head */
    os_list_node_t resource_node;  /* Node in resource list */

    uint32_t set;               /* Event set. */
    uint8_t  object_inited;     /* If mutex object is inited, value is OS_KOBJ_INITED */
    uint8_t  object_alloc_type; /* Indicates whether memory is allocated dynamically or statically,
                                   value is OS_ALLOC_TYPE_STATIC or OS_KOBJ_ALLOC_TYPE_DYNAMIC */
    uint8_t wake_type;          /* The type to wake up blocking tasks, value is OS_EVENT_WAKE_TYPE_PRIO
                                   or OS_EVENT_WAKE_TYPE_FIFO */
    char name[OS_NAME_MAX + 1]; /* Mutex name */

    os_spinlock_t lock; /* Spin lock used under SMP */
};
typedef struct os_event os_event_t;

/**
 ***********************************************************************************************************************
 * @struct      struct os_mq
 *
 * @brief       Define control block of message queue.
 ***********************************************************************************************************************
 */
struct os_mq_msg
{
    struct os_mq_msg *next;    /* Point to the next message */
    os_size_t         msg_len; /* message size */
};
typedef struct os_mq_msg os_mq_msg_t;
typedef struct os_mq_msg os_mq_msg_hdr_t;

struct os_msgqueue
{
    void        *msg_pool;       /* Point to message pool. */
    os_mq_msg_t *msg_queue_head; /* Point to first entry of message queue. */
    os_mq_msg_t *msg_queue_tail; /* Point to last entry of message queue. */
    os_mq_msg_t *msg_queue_free; /* Point to first entry of free message resource. */

    os_list_node_t send_task_list_head; /* Sender tasks blocked on this messages queue. */
    os_list_node_t recv_task_list_head; /* Receiver tasks blocked on this messages queue. */

    os_list_node_t resource_node; /* Node in resource list */

    os_size_t max_msg_size; /* Max message size of each message. */
    uint16_t  queue_depth;  /* Maximum number of messages that can be put into message queue. */
    uint16_t  used_msgs;    /* Number of messages queued. */

    uint8_t object_inited;     /* Indicates whether object is inited or deinited, value is
                                  OS_KOBJ_INITED or OS_KOBJ_DEINITED */
    uint8_t object_alloc_type; /* Indicates whether object is allocated dynamically or statically,
                                  value is OS_ALLOC_TYPE_STATIC or OS_KOBJ_ALLOC_TYPE_DYNAMIC */
    uint8_t wake_type;         /* The type to wake up blocking tasks, value is OS_MQ_WAKE_TYPE_PRIO
                                  or OS_MQ_WAKE_TYPE_FIFO */

    char name[OS_NAME_MAX + 1]; /* Message queue name. */

    os_spinlock_t lock; /* Spin lock used under SMP */
};

typedef struct os_msgqueue os_msgqueue_t;

/**
 ***********************************************************************************************************************
 * @struct      struct os_timer
 *
 * @brief       Define control block of timer.
 ***********************************************************************************************************************
 */
struct os_timer_active_node
{
#ifdef OS_USING_HASH_BUCKET_TIMER
    os_list_node_t active_list;
    uint8_t        info;
#endif

/* used for compensate periodic timer, if the timeout cb do than init_tick, can't compensate it */
#if OS_TIMER_PERIODIC_COMPENSATE
    os_tick_t timeout_tick;
#endif
    uint8_t flag;
};

typedef struct os_timer_active_node os_timer_active_node_t;

struct os_timer
{
    void (*timeout_func)(void *timeout_param); /* Timeout function. */
    void     *parameter;                       /* Timeout function's parameter. */
    os_tick_t init_ticks;                      /* Timer timeout tick. */
    os_tick_t round_ticks;                     /* Timeout tick remaining. */
#ifdef OS_USING_HASH_BUCKET_TIMER
    uint32_t index;
#endif
    os_list_node_t         list;
    os_timer_active_node_t active_node;
    char                   name[OS_NAME_MAX + 1];
};

typedef struct os_timer os_timer_t;

/**
 ***********************************************************************************************************************
 * @struct      struct os_mempool
 *
 * @brief       Define control block of mempool.
 ***********************************************************************************************************************
 */
struct os_mempool
{
    void          *start_addr;            /* Memory pool start. */
    void          *free_list;             /* Avaliable memory blocks list. */
    os_size_t      size;                  /* Size of memory pool. */
    os_size_t      blk_size;              /* Size of memory blocks, maybe not the size for users. */
    os_size_t      blk_total_num;         /* Numbers of memory block. */
    os_size_t      blk_free_num;          /* Numbers of free memory block. */
    os_list_node_t task_list_head;        /* Task suspend on this memory pool. */
    os_list_node_t resource_node;         /* Node in resource list */
    uint8_t        object_alloc_type;     /* Indicates whether object is allocated */
    uint8_t        object_inited;         /* Whether inited. */
    char           name[OS_NAME_MAX + 1]; /* mempool name. */

    os_spinlock_t lock; /* Spin lock used under SMP */
};
typedef struct os_mempool os_mempool_t;

/**
 ***********************************************************************************************************************
 * @struct      struct os_heap
 *
 * @brief       Define control block of memheap.
 ***********************************************************************************************************************
 */
struct heap_mem
{
    void *(*k_alloc)(struct heap_mem *h_mem, os_size_t size);
    void *(*k_aligned_alloc)(struct heap_mem *h_mem, os_size_t align, os_size_t size);
    void (*k_free)(struct heap_mem *h_mem, void *mem);
    void *(*k_realloc)(struct heap_mem *h_mem, void *mem, os_size_t newsize);
    void (*k_deinit)(struct heap_mem *h_mem);
    os_err_t (*k_mem_check)(struct heap_mem *h_mem);
#ifdef OS_USING_MEM_TRACE
    os_err_t (*k_mem_trace)(struct heap_mem *h_mem);
#endif
    os_size_t (*k_ptr_to_size)(struct heap_mem *h_mem, void *mem);
    void             *header; /* Pointer to current memory's start address. */
    struct heap_mem  *next;   /* Pointer to next memory's heap_mem object. */
    os_size_t         mem_total;
    os_size_t         mem_used;
    os_size_t         mem_maxused;
    dummy_semaphore_t sem_id;
    enum os_mem_alg   alg;
};

struct os_heap
{
    struct heap_mem *h_mem;
    uint8_t          object_inited;
    char             name[OS_NAME_MAX + 1];
    os_list_node_t   resource_node; /* Node in resource list */
};
typedef struct os_heap os_heap_t;

/**
 ***********************************************************************************************************************
 * @struct      struct os_work
 *
 * @brief       Define control block of work.
 ***********************************************************************************************************************
 */
struct os_workqueue;
struct os_work
{
    os_list_node_t work_node;

    void (*work_func)(void *data); /* Callback function for work. */
    void         *data;            /* Private data for callback function. */
    dummy_timer_t timer;           /* Using for delayed work. */

    struct os_workqueue *volatile workqueue; /* Workqueue used to execute work. */

    uint8_t flag;
    uint8_t object_inited; /* If os_work is inited, value is OS_KOBJ_INITED */
};
typedef struct os_work os_work_t;

/**
 ***********************************************************************************************************************
 * @struct      struct os_workqueue
 *
 * @brief       Define control block of workqueue.
 ***********************************************************************************************************************
 */
struct os_workqueue
{
    os_list_node_t work_list_head;
    os_work_t     *work_current; /* Work in progress on workqueue. */
    dummy_task_t   worker_task;

    os_spinlock_t     lock;          /* Spin lock. */
    dummy_semaphore_t sem_id;        /* Semaphore for synchronization. */
    uint8_t           object_inited; /* If os_workqueue is inited, value is OS_KOBJ_INITED */
};
typedef struct os_workqueue os_workqueue_t;
#endif /* __OS_PROTOTYPES_H__ */
