/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        os_kernel_lock.c
 *
 * @brief       This file implements the operations of kernel lock.
 *
 * @revision
 * Date         Author          Notes
 * 2020-10-13   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <oneos_config.h>
#include <os_types.h>
#include <os_stddef.h>
#include <os_assert.h>
#include <os_errno.h>
#include <os_spinlock.h>
#include <arch_interrupt.h>
#include "os_kernel_internal.h"

#ifdef OS_USING_SMP

struct kernel_lock
{
#ifdef OS_USING_KERNEL_LOCK_CHECK
    int32_t cpu_index; /*cpu + 1*/
#endif

    arch_spinlock_t lock;
};

static struct kernel_lock gs_os_kernel_lock = {
#ifdef OS_USING_KERNEL_LOCK_CHECK
    .cpu_index = 0,
#endif
    .lock = ARCH_SPINLOCK_INIT_VALUE(SPINLOCK_ARCH_UNLOCKED),
};

/**
 ***********************************************************************************************************************
 * @brief           This function is used to enter the kernel critical.
 *
 * @param[in]       None
 *
 * @return          the status of irq_save before os_irq_lock
 ***********************************************************************************************************************
 */
os_ubase_t k_kernel_enter(void)
{
#ifdef OS_USING_KERNEL_LOCK_CHECK
    int32_t current_cpu_index;
#endif

    register os_ubase_t irq_save;

    irq_save = os_irq_lock();

#ifdef OS_USING_KERNEL_LOCK_CHECK
    current_cpu_index = os_cpu_id_get();
    OS_ASSERT_EX(gs_os_kernel_lock.cpu_index != (current_cpu_index + 1), "Kernl lock owner nest");
#endif

    arch_spin_lock(&gs_os_kernel_lock.lock);

#ifdef OS_USING_KERNEL_LOCK_CHECK
    gs_os_kernel_lock.cpu_index = current_cpu_index + 1;
#endif

    return irq_save;
}

/**
 ***********************************************************************************************************************
 * @brief           This function is used to exit the kernel critical.
 *
 * @param[in]       irq_save    the status irq_save of os_irq_unlock
 *
 * @return          None
 ***********************************************************************************************************************
 */
void k_kernel_exit(os_ubase_t irq_save)
{

#ifdef OS_USING_KERNEL_LOCK_CHECK
    OS_ASSERT_EX(gs_os_kernel_lock.cpu_index == (os_cpu_id_get() + 1), "Kernl lock owner nest");
    gs_os_kernel_lock.cpu_index = 0;
#endif

    arch_spin_unlock(&gs_os_kernel_lock.lock);

    os_irq_unlock(irq_save);

    return;
}

/**
 ***********************************************************************************************************************
 * @brief           This function is is used to acquire kernel lock.
 *
 * @param[in]       None
 *
 * @return          None
 ***********************************************************************************************************************
 */
void k_kernel_spin_lock(void)
{

#ifdef OS_USING_KERNEL_LOCK_CHECK
    int32_t current_cpu_index;

    current_cpu_index = os_cpu_id_get();
    OS_ASSERT_EX(gs_os_kernel_lock.cpu_index != (current_cpu_index + 1), "Kernl lock owner nest");
#endif

    arch_spin_lock(&gs_os_kernel_lock.lock);

#ifdef OS_USING_KERNEL_LOCK_CHECK
    gs_os_kernel_lock.cpu_index = current_cpu_index + 1;
#endif

    return;
}

/**
 ***********************************************************************************************************************
 * @brief           This function is is used to release kernel lock.
 *
 * @param[in]       None
 *
 * @return          None
 ***********************************************************************************************************************
 */
void k_kernel_spin_unlock(void)
{
#ifdef OS_USING_KERNEL_LOCK_CHECK
    int32_t current_cpu_index;
    current_cpu_index = os_cpu_id_get();

    OS_ASSERT_EX(gs_os_kernel_lock.cpu_index == (current_cpu_index + 1), "Kernl lock owner nest");
    gs_os_kernel_lock.cpu_index = 0;
#endif

    arch_spin_unlock(&gs_os_kernel_lock.lock);

    return;
}

#ifdef OS_USING_KERNEL_LOCK_CHECK

/**
 ***********************************************************************************************************************
 * @brief           This function is used to determine whether the kernel lock is held by the current CPU.
 *
 * @param[in]       None
 *
 * @return          None
 * @retval          1:The current CPU holds kernel lock
 * @retval          0:The current CPU is not holding kernel lock
 ***********************************************************************************************************************
 */
int32_t k_kernel_lock_owne(void)
{
    int32_t    current_cpu_index;
    os_ubase_t irq_save;
    int32_t    owne;

    irq_save = os_irq_lock();

    current_cpu_index = os_cpu_id_get();
    owne              = (gs_os_kernel_lock.cpu_index == current_cpu_index + 1);

    os_irq_unlock(irq_save);

    return owne;
}

/**
 ***********************************************************************************************************************
 * @brief           This function returns the CPU number holding the kernel lock.
 *
 * @param[in]       None
 *
 * @return          the CPU number holding the kernel lock
 ***********************************************************************************************************************
 */
os_ubase_t k_kernel_lock_owner_catch(void)
{
    return gs_os_kernel_lock.cpu_index;
}

#endif

#else

int32_t g_os_kernel_lock_cnt = 0;

/**
 ***********************************************************************************************************************
 * @brief           This function is used to detect whether the entry into the kernel critical is recursive.
 *
 * @param[in]       None
 *
 * @return          None
 ***********************************************************************************************************************
 */
void k_kernel_enter_check(void)
{
#ifdef OS_USING_KERNEL_LOCK_CHECK
    os_bool_t fault_active;

    fault_active = os_is_fault_active();

    if (OS_NULL != g_os_current_task)
    {
        /* Exception context. */
        if (OS_TRUE == fault_active)
        {
            /* Do nothing. */
            ;
        }
        else
        {
            OS_ASSERT_EX(0 == g_os_kernel_lock_cnt,
                         "Kernel enter check failed, task(%s), irq(%u), ref_cnt(%d)",
                         g_os_current_task->name,
                         os_irq_num(),
                         g_os_kernel_lock_cnt);

            g_os_kernel_lock_cnt++;
        }
    }
#endif

    return;
}

/**
 ***********************************************************************************************************************
 * @brief           This function is used to detect error exit kernel critical.
 *
 * @param[in]       None
 *
 * @return          None
 ***********************************************************************************************************************
 */
void k_kernel_exit_check(void)
{
#ifdef OS_USING_KERNEL_LOCK_CHECK
    os_bool_t fault_active;

    fault_active = os_is_fault_active();

    if (OS_NULL != g_os_current_task)
    {
        /* Exception context. */
        if (OS_TRUE == fault_active)
        {
            /* Do nothing. */
            ;
        }
        else
        {
            OS_ASSERT_EX(1 == g_os_kernel_lock_cnt,
                         "Kernel exit check failed, task(%s), irq(%u), ref_cnt(%d)",
                         g_os_current_task->name,
                         os_irq_num(),
                         g_os_kernel_lock_cnt);

            g_os_kernel_lock_cnt--;
        }
    }
#endif

    return;
}
#endif /* OS_USING_SMP */
