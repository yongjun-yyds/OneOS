/**
 ***********************************************************************************************************************
 * Copyright (c) 2021, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        os_ipc_trace.c
 *
 * @brief       Provide ipc trace
 *
 * @revision
 * Date         Author          Notes
 * 2021-12-31   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include <os_errno.h>
#include <os_stddef.h>
#include <os_ipc_trace.h>

#ifdef OS_USING_IPC_TRACE

os_err_t ipc_trace_rec_enqueue(os_ipc_trace_t *ipct, os_ipc_trace_item_t *trace_data)
{
    if ((OS_NULL == ipct) || (OS_NULL == trace_data))
    {
        os_kprintf("IPC TRACE : Invalid Parameter\r\n");
        return OS_INVAL;
    }

    rb_ring_buff_put_force(&ipct->trace.rb, (uint8_t *)trace_data, sizeof(os_ipc_trace_item_t));

    ipct->trace.total_cnt++;
    if (ipct->trace.valid_cnt < OS_IPC_TRACE_ITEM_NUM)
    {
        ipct->trace.valid_cnt++;
    }

    return OS_SUCCESS;
}

os_err_t ipc_trace_rec_dequeue(os_ipc_trace_t *ipct, os_ipc_trace_item_t *trace_data)
{
    if ((OS_NULL == ipct) || (OS_NULL == trace_data))
    {
        os_kprintf("IPC TRACE : Invalid Parameter\r\n");
        return OS_INVAL;
    }

    if (0 == ipct->trace.valid_cnt)
    {
        /* os_kprintf("IPC TRACE : Trace Queue Empty\r\n"); */
        return OS_FAILURE;
    }

    if (rb_ring_buff_get(&ipct->trace.rb, (uint8_t *)trace_data, sizeof(os_ipc_trace_item_t)) <
        sizeof(os_ipc_trace_item_t))
    {
        return OS_FAILURE;
    }

    ipct->trace.total_cnt--;

    if (ipct->trace.valid_cnt >= 1)
    {
        ipct->trace.valid_cnt--;
    }

    return OS_SUCCESS;
}

os_err_t ipc_trace_rec_init(os_ipc_trace_t *ipct)
{
    os_ubase_t i = 0;

    if (OS_NULL == ipct)
    {
        os_kprintf("IPC TRACE : Invalid Parameter\r\n");
        return OS_INVAL;
    }

    ipct->ipc_obj         = 0;
    ipct->trace.total_cnt = 0;
    ipct->trace.valid_cnt = 0;

    for (i = 0; i < OS_IPC_TRACE_ITEM_NUM; i++)
    {
        ipct->trace.record[i].desc = 0;
        ipct->trace.record[i].task = 0;
    }

    rb_ring_buff_init(&ipct->trace.rb, (uint8_t *)ipct->trace.record, sizeof(ipct->trace.record));

    return OS_SUCCESS;
}

os_err_t ipc_trace_rec_add_kobj(os_ipc_trace_t *ipct, os_ubase_t ipc_obj)
{
    if (OS_NULL == ipct)
    {
        os_kprintf("IPC TRACE : Invalid Parameter\r\n");
        return OS_INVAL;
    }

    ipct->ipc_obj = ipc_obj;

    return OS_SUCCESS;
}

os_err_t ipc_trace_rec_del_kobj(os_ipc_trace_t *ipct)
{
    if (OS_NULL == ipct)
    {
        os_kprintf("IPC TRACE : Invalid Parameter\r\n");
        return OS_INVAL;
    }

    ipc_trace_rec_init(ipct);

    return OS_SUCCESS;
}

os_err_t ipc_trace_hook(os_ipc_trace_t *ipct, os_ubase_t ipc_obj, os_ipc_trace_item_t *trace_data)
{
    if ((OS_NULL == ipct) || (OS_NULL == trace_data))
    {
        os_kprintf("IPC TRACE : Invalid Parameter\r\n");
        return OS_INVAL;
    }

    if (ipct->ipc_obj != ipc_obj)
    {
        return OS_FAILURE;
    }

    return ipc_trace_rec_enqueue(ipct, trace_data);
}

#endif
