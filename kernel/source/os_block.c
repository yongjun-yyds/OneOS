/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        os_ipc.c
 *
 * @brief       This file implements the IPC public functions.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-11   OneOS team      First Version
 ***********************************************************************************************************************
 */

#include <os_errno.h>
#include <arch_interrupt.h>
#include <os_list.h>
#include <os_task.h>
#include <string.h>

#include "os_kernel_internal.h"

/**
 ***********************************************************************************************************************
 * @brief           This function will insert the task to IPC queue. The higher priority task will be inserted before
 *                  the other task.
 *
 * @param[in]       head             The IPC queue.
 * @param[in]       task             The task to be inserted
 *
 * @return          None
 ***********************************************************************************************************************
 */
void k_blockq_insert(os_list_node_t *head, struct os_task *task)
{
    os_list_node_t *pos;
    os_task_t      *task_iter;

    os_list_for_each(pos, head)
    {
        task_iter = os_list_entry(pos, os_task_t, task_node);
        if (task_iter->current_priority > task->current_priority)
        {
            /* Insert this task_node before the task_iter. */
            os_list_add_tail(&task_iter->task_node, &task->task_node);
            break;
        }
    }

    if (pos == head)
    {
        os_list_add_tail(head, &task->task_node);
    }

    return;
}

/**
 ***********************************************************************************************************************
 * @brief           This function will block the specific task. and if the task not wait forever it will be insert in
 *                  tick queue.
 *
 * @param[in]       head            The IPC queue.
 * @param[in]       task            The task wait to block.
 * @param[in]       timeout         Wait time (in clock ticks).
 *
 * @return          None
 ***********************************************************************************************************************
 */
/* clang-format off */   
os_err_t k_block_task(  os_spinlock_t  *lock, 
                        os_ubase_t      irq_save, 
                        os_list_node_t *head, 
                        os_tick_t       timeout, 
                        os_bool_t       is_wake_prio)
/* clang-format on */
{
    os_task_t *task;

    task = _k_task_self();

#if defined(OS_USING_SMP) && !defined(OS_IPC_MONOLOCK_MODE)
    k_kernel_spin_lock();
#else
    OS_UNREFERENCE(lock);
#endif

    k_readyq_remove(task);

    if (OS_TRUE == is_wake_prio)
    {
        k_blockq_insert(head, task);
    }
    else
    {
        os_list_add_tail(head, &task->task_node);
    }

    task->state &= ~OS_TASK_STATE_READY;
    task->state |= OS_TASK_STATE_BLOCK;

    task->block_list_head = head;
    task->is_wake_prio    = is_wake_prio;

    if (OS_WAIT_FOREVER != timeout)
    {
        task->state |= OS_TASK_STATE_SLEEP;
        k_tickq_put(task, timeout);
    }

#if defined(OS_USING_SMP) && !defined(OS_IPC_MONOLOCK_MODE)

    k_kernel_spin_unlock();
    os_spin_unlock_irq(lock, irq_save);

    OS_KERNEL_ENTER();
    OS_KERNEL_EXIT_SCHED();

#else
    OS_KERNEL_EXIT_SCHED();
#endif

    return task->switch_retval;
}

/**
 ***********************************************************************************************************************
 * @brief           This function will unblock the highest priority task. and it will be removed from tick queue if the
 *                  task wait in the queue.
 *
 * @param[in]       task            The task to unblock.
 *
 * @return          None
 ***********************************************************************************************************************
 */
os_bool_t k_unblock_task(os_task_t *task)
{
    os_bool_t need_sched;

    os_list_del(&task->task_node);

    task->state &= ~OS_TASK_STATE_BLOCK;
    task->block_list_head = OS_NULL;
    task->switch_retval   = OS_SUCCESS;

    if ((task->state & OS_TASK_STATE_SLEEP) != 0)
    {
        k_tickq_remove(task);
        task->state &= ~OS_TASK_STATE_SLEEP;
    }

    /* The task state may be suspend, empty or running */
    if (OS_TASK_STATE_SUSPEND != task->state)
    {
        task->state |= OS_TASK_STATE_READY;
        k_readyq_put(task);

        need_sched = OS_TRUE;
    }
    else
    {
        need_sched = OS_FALSE;
    }

    return need_sched;
}

/**
 ***********************************************************************************************************************
 * @brief           This function will activate all the task wait in the IPC queue.
 *
 * @param[in]       head            The IPC queue.
 *
 * @return          None
 ***********************************************************************************************************************
 */
os_bool_t k_cancel_all_blocked_task(const os_list_node_t *head)
{
    os_task_t *task;
    os_bool_t  need_sched;

    need_sched = OS_FALSE;

#if defined(OS_USING_SMP) && !defined(OS_IPC_MONOLOCK_MODE)
    k_kernel_spin_lock();
#endif

    while (!os_list_empty(head))
    {
        task = os_list_first_entry(head, os_task_t, task_node);

        os_list_del(&task->task_node);

        task->state &= ~OS_TASK_STATE_BLOCK;
        task->block_list_head = OS_NULL;

        if ((task->state & OS_TASK_STATE_SLEEP) != 0)
        {
            k_tickq_remove(task);
            task->state &= ~OS_TASK_STATE_SLEEP;
        }

        if (OS_TASK_STATE_SUSPEND != task->state)
        {
            task->state |= OS_TASK_STATE_READY;
            k_readyq_put(task);

            need_sched = OS_TRUE;
        }

        task->switch_retval = OS_FAILURE;
    }

#if defined(OS_USING_SMP) && !defined(OS_IPC_MONOLOCK_MODE)
    k_kernel_spin_unlock();
#endif

    return need_sched;
}

#ifdef OS_USING_SHELL
#include <shell.h>

void k_show_blocked_task(block_task_info_t *info, uint16_t block_task_cnt)
{
    uint16_t i;
    uint16_t j;
    char     name[OS_NAME_MAX + 1];

    name[OS_NAME_MAX] = '\0';
    for (i = 0; i < block_task_cnt; i++)
    {
        for (j = 0; j < OS_NAME_MAX; j++)
        {
            name[j] = info[i].name[j];
            if (name[j] == '\0')
            {
                break;
            }
        }
        os_kprintf("%s(%d)", (name[0] != '\0') ? name : "-", info[i].current_priority);

        if (i != block_task_cnt - 1)
        {
            os_kprintf("/");
        }
    }
}

#endif
