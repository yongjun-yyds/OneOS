/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        mem_monitor.c
 *
 * @brief       This file implements memory monitor.
 *
 * @revision
 * Date         Author          Notes
 * 2021-12-03   OneOS team      First Version
 ***********************************************************************************************************************
 */
#include <oneos_config.h>
#include <os_types.h>
#include <os_stddef.h>
#include <os_assert.h>
#include <arch_interrupt.h>
#include <os_errno.h>
#include <os_clock.h>
#include <string.h>
#include <os_spinlock.h>
#include <arch_misc.h>
#include <arch_exception.h>
#include <os_safety.h>
#include "../../os_kernel_internal.h"

#include <mem_monitor.h>
#include <crc16.h>
#include <../../../kernel/source/os_prototypes.h>

static mem_monitor_t gs_mem_monitor_obj;
mem_monitor_t       *gs_mem_monitor = &gs_mem_monitor_obj;

/**
 ***********************************************************************************************************************
 * @brief           begin the memory monitor.
 *
 * @details
 *
 * @attention       This function start a memory monitor check, only check the given address hash anyway
 *
 * @param[in]       mm              memory monitor pointer
 *
 * @return          The result of start memory monitor
 * @retval          OS_SUCCESS      start memory monitor successfully.
 * @retval          else            start memory monitor fail
 ***********************************************************************************************************************
 */
os_err_t mem_monitor_begin(mem_monitor_t *mm)
{
    if (OS_NULL == mm)
    {
        os_kprintf("mem monitor is NULL\r\n");
        return OS_INVAL;
    }

    if (METHOD_CRC == mm->method)
    {
        if (mm->hash_sig_calc)
        {
            mm->hash_sig = mm->hash_sig_calc((uint8_t *)mm->addr, mm->len);
        }
        mm->prev_hash_sig = mm->hash_sig;
    }

    return OS_SUCCESS;
}

/**
 ***********************************************************************************************************************
 * @brief           end the memory monitor.
 *
 * @details
 *
 * @attention       This function end a memory monitor check, only check the given address hash anyway
 *
 * @param[in]       mm              memory monitor pointer
 *
 * @return          The result of ending memory monitor.
 * @retval          OS_SUCCESS      end memory monitor successfully.
 * @retval          else            end memory monitor fail
 ***********************************************************************************************************************
 */
os_err_t mem_monitor_end(mem_monitor_t *mm)
{
    if (OS_NULL == mm)
    {
        os_kprintf("mem monitor is NULL\r\n");
        return OS_INVAL;
    }

    if (METHOD_CRC == mm->method)
    {
        if (mm->hash_sig_calc)
        {
            mm->hash_sig = mm->hash_sig_calc((uint8_t *)mm->addr, mm->len);
        }

        if (mm->prev_hash_sig != mm->hash_sig)
        {
            os_kprintf("[mem monitor]:  (addr=%#x len=%d) modify in task:[%s].\r\n",
                       mm->addr,
                       mm->len,
                       k_task_self()->name);

            return OS_FAILURE;
        }
    }

    return OS_SUCCESS;
}

/**
 ***********************************************************************************************************************
 * @brief           memory monitor task switch hook
 *
 * @details         register the task hook, will be called by task switch
 *
 * @attention
 *
 * @param[in]       from            not care
 * @param[in]       to              not care
 *
 * @return          no return
 ***********************************************************************************************************************
 */
#ifdef OS_USING_SMP
void mem_monitor_task_switch_hook(os_task_id from, os_task_id to, int32_t cpu)
#else
void mem_monitor_task_switch_hook(os_task_id from, os_task_id to)
#endif
{
#if defined(OS_USING_MEM_MONITOR)
    if (gs_mem_monitor->handle)
    {
        gs_mem_monitor->handle(gs_mem_monitor);
    }
#endif
}

/**
 ***********************************************************************************************************************
 * @brief           memory monitor default handle.
 *
 * @details
 *
 * @attention       This function is the default memory monitor handle
 *
 * @param[in]       mm              memory monitor pointer
 *
 * @return          The result of handling process.
 * @retval          OS_SUCCESS      memory monitor handle successfully.
 * @retval          else            memory monitor handle fail
 ***********************************************************************************************************************
 */
os_err_t mem_monitor_def_handle(mem_monitor_t *mm)
{
    if (OS_NULL == mm)
    {
        os_kprintf("mem monitor is NULL\r\n");
        return OS_INVAL;
    }

    if (mm->hash_sig_calc)
    {
        mm->hash_sig = mm->hash_sig_calc((uint8_t *)mm->addr, mm->len);
    }

    if (0 == mm->prev_hash_sig)    // as the first time
    {
        mm->prev_hash_sig = mm->hash_sig;
        return OS_SUCCESS;
    }

    /* os_kprintf("prev_hash_sig = %#x\r\n", mm->prev_hash_sig); */
    /* os_kprintf("hash_sig      = %#x\r\n", mm->hash_sig); */

    if (mm->prev_hash_sig != mm->hash_sig)
    {
        os_kprintf("[mem monitor]:  (addr=%#x len=%d) modify in task:[%s].\r\n",
                   mm->addr,
                   mm->len,
                   k_task_self()->name);
        mm->prev_hash_sig = mm->hash_sig;
        return OS_FAILURE;
    }

    return OS_SUCCESS;
}

/**
 ***********************************************************************************************************************
 * @brief           crc16 hash calculate callback.
 *
 * @details
 *
 * @attention       This function is called by memmory monitor to calc crc16 hash
 *
 * @param[in]       pdata           the memory address to be calculate crc16
 * @param[in]       len             the memory data length
 *
 * @return          The crc16 hash
 ***********************************************************************************************************************
 */
os_ubase_t hash_sig_crc16_calc(uint8_t *pdata, os_ubase_t len)
{
    return crc16_calc(pdata, len);
}

/**
 ***********************************************************************************************************************
 * @brief           init a memory monitor
 *
 * @details
 *
 * @attention       use this function to do a memory monitor init
 *
 * @param[in]       mm             memory monitor pointer
 * @param[in]       addr           the memory address to be monitor
 * @param[in]       len            the memory data length
 * @param[in]       method         the method to be used for calculating the hash
 * @param[in]       func           the function to be uesed for calculating the hash
 * @param[in]       handle         the default handle of memory monitor
 *
 * @return          The result of init a memory monitor
 * @retval          OS_SUCCESS      init a memory monitor handle successfully.
 * @retval          else            init a memory monitor handle fail
 ***********************************************************************************************************************
 */
os_err_t mem_monitor_init(mem_monitor_t   *mm,
                          os_ubase_t       addr,
                          os_ubase_t       len,
                          os_ubase_t       method,
                          hash_sig_calc_t  func,
                          monitor_handle_t handle)
{
    if ((OS_NULL == mm) || (OS_NULL == func))
    {
        return OS_INVAL;
    }

    mm->addr          = addr;
    mm->len           = len;
    mm->method        = method;
    mm->hash_sig_calc = func;
    mm->prev_hash_sig = 0;
    mm->hash_sig      = 0;
    mm->task          = OS_NULL;
    mm->handle        = handle;

    return OS_SUCCESS;
}

/**
 ***********************************************************************************************************************
 * @brief           deinit a memory monitor
 *
 * @details
 *
 * @attention       use this function to do a memory monitor delete
 *
 * @param[in]       mm             memory monitor pointer
 *
 * @return          The result of init a memory monitor
 * @retval          OS_SUCCESS      init a memory monitor handle successfully.
 * @retval          else            init a memory monitor handle fail
 ***********************************************************************************************************************
 */
os_err_t mem_monitor_deinit(mem_monitor_t *mm)
{
    if (OS_NULL == mm)
    {
        return OS_INVAL;
    }
    memset(mm, 0, sizeof(mem_monitor_t));
    return OS_SUCCESS;
}

/**
 ***********************************************************************************************************************
 * @brief           init a memory auto monitor
 *
 * @details
 *
 * @attention       use this function to do a memory auto monitor init
 *
 * @param[in]       mm             memory monitor pointer
 * @param[in]       addr           the memory address to be monitor
 * @param[in]       len            the memory data length
 * @param[in]       method         the method to be used for calculating the hash
 * @param[in]       func           the function to be uesed for calculating the hash
 * @param[in]       handle         the default handle of memory monitor
 *
 * @return          The result of init a memory monitor
 * @retval          OS_SUCCESS      init a memory monitor handle successfully.
 * @retval          else            init a memory monitor handle fail
 ***********************************************************************************************************************
 */
os_err_t
mem_monitor_auto_init(os_ubase_t addr, os_ubase_t len, os_ubase_t method, hash_sig_calc_t func, monitor_handle_t handle)
{
    // add task switch hook
    if (OS_FALSE == os_task_switch_hook_add(mem_monitor_task_switch_hook))
    {
        return OS_FAILURE;
    }

    return mem_monitor_init(gs_mem_monitor, addr, len, method, func, handle);
}

/**
 ***********************************************************************************************************************
 * @brief           deinit a memory auto monitor
 *
 * @details
 *
 * @attention       use this function to do a memory auto monitor deinit
 *
 * @param[in]       mm             memory monitor pointer
 *
 * @return          The result of init a memory monitor
 * @retval          OS_SUCCESS      init a memory monitor handle successfully.
 * @retval          else            init a memory monitor handle fail
 ***********************************************************************************************************************
 */
os_err_t mem_monitor_auto_deinit(void)
{
    // del task switch hook
    if (OS_FALSE == os_task_switch_hook_delete(mem_monitor_task_switch_hook))
    {
        return OS_FAILURE;
    }

    return mem_monitor_deinit(gs_mem_monitor);
}
