/**
 ***********************************************************************************************************************
 * Copyright (c) 2021, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        cpu_monitor.c
 *
 * @brief       This file implements a cpu monitor.
 *
 * @revision
 * Date         Author          Notes
 * 2021-11-29   OneOS team      First Version
 * 2024-11-20   weiyongjun      add irq monitor when task monitoring
 ***********************************************************************************************************************
 */
#include <oneos_config.h>
#include <os_types.h>
#include <os_task.h>
#include <clocksource.h>
#include <os_spinlock.h>
#include <stdlib.h>
#include <arch_interrupt.h>
#include <cpu_monitor.h>
#include <../../../kernel/source/os_prototypes.h>

#ifdef OS_USING_CPU_MONITOR

#ifdef OS_USING_IRQ_MONITOR
#include <string.h>

/* Maximum number of interrupts supported for monitoring */
#define IRQ_MONITOR_NUM OS_IRQ_MONITOR_NUM

/* Signs for monitoring CPU interrupt information */
#define IRQ_MONITOR_START 0XAAA
#define IRQ_MONITOR_STOP  0X555

/* A structure used to record interrupt runtime */
typedef struct cpu_irq_info
{
    int      status;
    int      irq;
    uint64_t start_time; /* Record the time of switching to the irq.*/
    uint64_t total_time; /* Accumulation of running time. */
} cpu_irq_info_t;

struct
{
    int status;              /* Interrupt monitoring status */
    int cur_index;           /* current IRQ index of irq_info */
    int nest_count;          /* Save interrupt nesting depth */
    int already_monitor_num; /* Save the number of monitored interrupts */
} irq_monitor_state = {0};

static char           cpu_nest_info[IRQ_MONITOR_NUM] = {0}; /* Used to find the nested order of interrupts */
static cpu_irq_info_t irq_info[IRQ_MONITOR_NUM]      = {0}; /* Used to save the running status of interrupts */

/**
 ***********************************************************************************************************************
 * @brief           Start monitoring of the current interrupt.
 *
 * @param[in]       irq             The current interrupt num.
 * @param[in]       time            The current interrupt needs to be updated start time.
 *
 * @return          None.
 ***********************************************************************************************************************
 */

static void current_irq_timestamp_start(int irq)
{
    int irq_exist = OS_FALSE;
    int irq_save  = 0;

    /* The current IRQ has been running within the monitoring time */
    for (int index = 0; index < IRQ_MONITOR_NUM; index++)
    {
        if ((irq_info[index].irq == irq) && (irq_info[index].status == 1))
        {
            irq_info[index].start_time  = os_clocksource_gettime();
            irq_monitor_state.cur_index = index;
            irq_exist                   = OS_TRUE;
            break;
        }
    }

    /* The current IRQ is not running within the monitoring time */
    if (irq_exist == OS_FALSE)
    {
        if ((irq_monitor_state.already_monitor_num == IRQ_MONITOR_NUM) &&
            (irq_monitor_state.already_monitor_num != (IRQ_MONITOR_NUM + 1)))
        {
            os_kprintf("The current monitoring interrupt count of %d has reached the maximum monitoring count.\r\n",
                       "To monitor more interrupts, please reconfigure OS_IRQ_MONITOR_NUM\r\n",
                       IRQ_MONITOR_NUM);
            irq_monitor_state.already_monitor_num++;
            return;
        }

        for (int index = 0; index < IRQ_MONITOR_NUM; index++)
        {
            irq_save = os_irq_lock();
            if (irq_info[index].status == 0)
            {
                irq_info[index].status      = 1;
                os_irq_unlock(irq_save);
                irq_info[index].irq         = irq;
                irq_info[index].start_time  = os_clocksource_gettime();
                irq_info[index].total_time  = 0;
                irq_monitor_state.cur_index = index;

                irq_monitor_state.already_monitor_num++;
                break;
            }
            os_irq_unlock(irq_save);
        }
    }

    return;
}

/**
 ***********************************************************************************************************************
 * @brief           Interrupt entry, start monitoring.
 *
 * @param[in]       None.
 *
 * @return          None.
 ***********************************************************************************************************************
 */

void cpu_usage_irq_monitor_start(void)
{
    os_task_t *current_task;

    if (irq_monitor_state.status == IRQ_MONITOR_START)
    {
        uint64_t time = os_clocksource_gettime();
        int      irq  = os_irq_num();

        irq_monitor_state.nest_count++;
        cpu_nest_info[irq_monitor_state.nest_count - 1] = irq;

        /* if irq_monitor_state.nest_count > 1, interrupt nesting occurred */
        if (irq_monitor_state.nest_count > 1)
        {
            /* first: stop prev irq monitor, update prev irq running time */
            int prev_irq = cpu_nest_info[irq_monitor_state.nest_count - 2];
            for (int index = 0; index < IRQ_MONITOR_NUM; index++)
            {
                if ((irq_info[index].irq == prev_irq) && (irq_info[index].status == 1))
                {
                    irq_info[index].total_time += time - irq_info[index].start_time;
                    break;
                }
            }
        }
        else
        {
            /* first: stop current task monitor */
            current_task = (os_task_t *)os_get_current_task();
            current_task->usage_info.total_time += time - current_task->usage_info.start_time;
        }

        /* second: start current irq monitor */
        current_irq_timestamp_start(irq);
    }

    return;
}

/**
 ***********************************************************************************************************************
 * @brief           Interrupt exit, exit monitoring.
 *
 * @param[in]       None.
 *
 * @return          None.
 ***********************************************************************************************************************
 */

void cpu_usage_irq_monitor_stop(void)
{
    os_task_t *current_task;

    if (irq_monitor_state.status == IRQ_MONITOR_START)
    {
        int irq = os_irq_num();

        uint64_t time = os_clocksource_gettime();

        /* first: stop current irq monitor, update current irq running time */
        irq_info[irq_monitor_state.cur_index].total_time += time - irq_info[irq_monitor_state.cur_index].start_time;

        if (irq_monitor_state.nest_count > 1)
        {
            /* second: start prev irq monitor, update prev irq start time */
            int prev_irq = cpu_nest_info[irq_monitor_state.nest_count - 2];
            for (int index = 0; index < IRQ_MONITOR_NUM; index++)
            {
                if ((irq_info[index].irq == prev_irq) && (irq_info[index].status == 1))
                {
                    irq_info[index].start_time  = os_clocksource_gettime();
                    irq_monitor_state.cur_index = index;
                    break;
                }
            }
        }
        else
        {
            /* second: start current task monitor */
            current_task                        = (os_task_t *)os_get_current_task();
            current_task->usage_info.start_time = os_clocksource_gettime();
        }

        irq_monitor_state.nest_count--;
    }

    return;
}
#endif

/**
 ***********************************************************************************************************************
 * @brief           This function accumulates the running time of the current task during task switching.
 *
 * @param[in]       None.
 *
 * @return          None.
 ***********************************************************************************************************************
 */
#ifdef OS_USING_SMP
static void cpu_use_switch_hook(os_task_id from, os_task_id to, int32_t cpu)
#else
static void cpu_use_switch_hook(os_task_id from, os_task_id to)
#endif
{
    uint64_t time;

#ifdef OS_USING_SMP
    OS_UNREFERENCE(cpu);
#endif

    os_task_t *from_task = (os_task_t *)from;
    os_task_t *to_task   = (os_task_t *)to;

    time = os_clocksource_gettime();

    from_task->usage_info.total_time += time - from_task->usage_info.start_time;

    to_task->usage_info.start_time = time;
}

/**
 ***********************************************************************************************************************
 * @brief           This function accumulates the running time of the current task during task switching.
 *
 * @param[in]       None.
 *
 * @return          Whether the start monitoring is successful.
 * @retval          OS_SUCCESS      Start monitoring successfully
 * @retval          OS_FAILURE      Start monitoring failed.
 ***********************************************************************************************************************
 */

static os_err_t get_current_task_timestamp_start(void)
{
    uint64_t   time;
    os_task_id task_id;
    os_task_t *task;

    task_id = os_get_current_task();
    if (0 == task_id)
    {
        os_kprintf("The system does not start\r\n");
        return OS_FAILURE;
    }

    task = (os_task_t *)task_id;

    time = os_clocksource_gettime();

    task->usage_info.start_time = time;
    task->usage_info.total_time = 0;

    return OS_SUCCESS;
}

/**
 ***********************************************************************************************************************
 * @brief           This function starts monitoring the CPU usage in the system.
 *
 * @param[in]       None.
 *
 * @return          Whether the start monitoring is successful.
 * @retval          OS_SUCCESS      Start monitoring successfully
 * @retval          OS_FAILURE      Start monitoring failed.
 ***********************************************************************************************************************
 */
static os_err_t cpu_usage_monitor_start(void)
{
    os_err_t ret;

    os_schedule_lock();

#ifdef OS_USING_IRQ_MONITOR
    irq_monitor_state.status = IRQ_MONITOR_START;
#endif

    ret = get_current_task_timestamp_start();

    if (0 == ret)
    {
        if (OS_TRUE == os_task_switch_hook_add(cpu_use_switch_hook))
        {
            ret = OS_SUCCESS;
        }
        else
        {
#ifdef OS_USING_IRQ_MONITOR
            irq_monitor_state.status = IRQ_MONITOR_STOP;
#endif

            os_kprintf(" %s failed to add switch hook function.\r\n", __FUNCTION__, __LINE__);
            ret = OS_FAILURE;
        }
    }

    os_schedule_unlock();

    return ret;
}

/**
 ***********************************************************************************************************************
 * @brief           This function stops monitoring the CPU usage in the system.
 *
 * @param[in]       None.
 *
 * @return          Whether the start monitoring is successful.
 * @retval          OS_SUCCESS      Start monitoring successfully
 * @retval          OS_FAILURE      Start monitoring failed.
 ***********************************************************************************************************************
 */
static os_err_t cpu_usage_monitor_stop(void)
{
    os_task_t      *iter_task;
    uint64_t        total;
    uint64_t        rate;
    uint32_t        integer_part;
    uint32_t        fractional_part;
    os_spinlock_t  *task_resource_list_lock;
    os_list_node_t *task_resource_list_head;

    os_schedule_lock();

    if (OS_FALSE == os_task_switch_hook_delete(cpu_use_switch_hook))
    {
        os_schedule_unlock();
        return OS_FAILURE;
    }

#ifdef OS_USING_IRQ_MONITOR
    irq_monitor_state.status = IRQ_MONITOR_STOP;
#endif

    os_schedule_unlock();

    /* Count the total time spent. */
    total                   = 0;
    task_resource_list_lock = k_task_resource_list_lock_get();
    task_resource_list_head = k_task_resource_list_head_get();

    os_spin_lock(task_resource_list_lock);
    os_list_for_each_entry(iter_task, task_resource_list_head, os_task_t, resource_node)
    {
        total += iter_task->usage_info.total_time;
    }
    os_spin_unlock(task_resource_list_lock);

#ifdef OS_USING_IRQ_MONITOR
    for (int index = 0; index < IRQ_MONITOR_NUM; index++)
    {
        if (irq_info[index].status == 1)
        {
            total += irq_info[index].total_time;
        }
    }
#endif

    // os_kprintf("total %lld\r\n",total);

    os_kprintf("TID         %-*s  Ratio\r\n", OS_NAME_MAX, "Task");

    os_spin_lock(task_resource_list_lock);
    os_list_for_each_entry(iter_task, task_resource_list_head, os_task_t, resource_node)
    {

        os_kprintf("0x%x  %-*s", iter_task, OS_NAME_MAX, iter_task->name);

        rate            = iter_task->usage_info.total_time * 10000 / total;
        integer_part    = rate / 100;
        fractional_part = rate % 100;

        if (0 == iter_task->usage_info.total_time)
        {
            os_kprintf("    -\r\n");
        }
        else
        {
            if ((0 == integer_part) && (0 == fractional_part))
            {
                // os_kprintf("iter_task->usage_info.total_time %lld ",iter_task->usage_info.total_time);
                os_kprintf("   0.01%c\r\n", '%');
            }
            else
            {
                if (fractional_part >= 10)
                {
                    os_kprintf("  %2d.%d%c\r\n", integer_part, fractional_part, '%');
                }
                else
                {
                    os_kprintf("  %2d.0%d%c\r\n", integer_part, fractional_part, '%');
                }
            }
        }

        iter_task->usage_info.total_time = 0;
    }
    os_spin_unlock(task_resource_list_lock);

#ifdef OS_USING_IRQ_MONITOR
    os_kprintf("IRQ         %-*s  Ratio\r\n", OS_NAME_MAX, "    ");

    for (int index = 0; index < IRQ_MONITOR_NUM; index++)
    {
        if (irq_info[index].status == 1)
        {
            os_kprintf("0x%-8x  %-*s", irq_info[index].irq, OS_NAME_MAX, "    ");
            rate            = irq_info[index].total_time * 10000 / total;
            integer_part    = rate / 100;
            fractional_part = rate % 100;

            if ((0 == integer_part) && (0 == fractional_part))
            {
                os_kprintf("   0.01%c\r\n", '%');
            }
            else
            {
                if (fractional_part >= 10)
                {
                    os_kprintf("  %2d.%d%c\r\n", integer_part, fractional_part, '%');
                }
                else
                {
                    os_kprintf("  %2d.0%d%c\r\n", integer_part, fractional_part, '%');
                }
            }
        }
    }

    memset(irq_info, 0, sizeof(irq_info));
    memset(cpu_nest_info, 0, sizeof(cpu_nest_info));
    memset(&irq_monitor_state, 0, sizeof(irq_monitor_state));
#endif

    return OS_SUCCESS;
}

/**
 ***********************************************************************************************************************
 * @brief           This function shows CPU usage monitoring data.
 *
 * @param[in]       sec         Monitoring period (between 1 second and 30 seconds)
 *
 * @return          None.
 ***********************************************************************************************************************
 */
os_err_t cpu_usage_show(int sec)
{
    OS_ASSERT(OS_FALSE == os_is_irq_active());

    if ((sec < 1) || (sec > 30))
    {
        sec = 5;
    }

    if (OS_SUCCESS != cpu_usage_monitor_start())
    {
        return OS_FAILURE;
    }

    os_kprintf("start cpu monitor! please wait %dS\r\n", sec);

    os_task_tsleep(sec * OS_TICK_PER_SECOND);

    os_kprintf("\r\n********* Monitor Times: %dS ***********\r\n", sec);

    if (OS_SUCCESS != cpu_usage_monitor_stop())
    {
        return OS_FAILURE;
    }

    return OS_SUCCESS;
}

#if defined(OS_USING_SHELL)

#include <shell.h>

static os_err_t sh_cpu_usage_show(int32_t argc, char **argv)
{
    int sec;

    if (argc >= 2)
    {
        sec = atoi(argv[1]);
        if ((sec < 1) || (sec > 30))
        {
            os_kprintf("\r\n The statistics time is incorrect, and the valid range of the statistics time is "
                       "1-30 seconds\r\n");
            return OS_FAILURE;
        }
        else
        {
            return cpu_usage_show(atoi(argv[1]));
        }
    }
    else
    {
        return cpu_usage_show(5);
    }
}
SH_CMD_EXPORT(cpu_usage, sh_cpu_usage_show, "Show cpu usage information");

#endif

#endif
