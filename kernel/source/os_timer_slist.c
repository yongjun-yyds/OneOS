/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        os_timer.c
 *
 * @brief       This file implements the timer functions.
 *
 * @revision
 * Date         Author          Notes
 * 2020-11-18   OneOS Team      First version.
 ***********************************************************************************************************************
 */

#include <oneos_config.h>
#include <os_errno.h>
#include <os_timer.h>
#include <os_task.h>
#include <os_assert.h>
#include <os_clock.h>
#include <string.h>
#include <os_spinlock.h>
#include <arch_misc.h>
#include "os_kernel_internal.h"

#ifdef OS_USING_SINGLE_LIST_TIMER

#if (!defined OS_HARD_SINGLE_LIST_TIMER) && (!defined OS_SOFT_SINGLE_LIST_TIMER)
#error "Please select timer of hard or soft"
#endif

/* the 0x01 used to Periodic timer */

#define OS_TIMER_FLAG_DEACTIVATED 0x0U /* Timer is deactive. */
#define OS_TIMER_FLAG_ACTIVATED   0x2U /* Timer is active. */

#define OS_TIMER_FLAG_TICK_SCHED 0x00 /* 0x00 timer cb be called in tick isr. */
#define OS_TIMER_FLAG_TASK_SCHED                                                                                       \
    0x04 /* 0x04 timer cb be called in timer task, remember the same as OS_TIMER_FLAG_SOFT_TIMER */

#define OS_TIMER_FLAG_NOT_INITED 0x0U
#define OS_TIMER_FLAG_INITED     0x8U

#define OS_TIMER_FLAG_STATIC  0x0U
#define OS_TIMER_FLAG_DYNAMIC 0x10U

#define OS_TIMER_FLAG_DO_TIMEOUT 0x20U /* now timer is timeout. */

#define TIMER_TAG "TIMER"

#define OS_TIMER_IS_TASK_SCHED(timer)                                                                                  \
    ((OS_TIMER_FLAG_TASK_SCHED == (timer->active_node.flag & OS_TIMER_FLAG_TASK_SCHED)) ? 1 : 0)

#ifdef OS_HARD_SINGLE_LIST_TIMER
static os_timer_t    *gs_tick_sched_timer_head;
static os_list_node_t gs_tick_sched_timer_list = OS_LIST_INIT(gs_tick_sched_timer_list);
#endif /* end of OS_HARD_SINGLE_LIST_TIMER */

#ifdef OS_SOFT_SINGLE_LIST_TIMER
#define OS_TIMER_TASK_PRIO 0

/* for soft timer, used for periodic timer compensate, if need define it */
#if OS_TIMER_PERIODIC_COMPENSATE
#define OS_TIMER_TICK_DIFF(a, b) (a >= b) ? (a - b) : (OS_UINT32_MAX - b + a + 1)
#endif /* end of OS_TIMER_PERIODIC_COMPENSATE */

OS_ALIGN(OS_ALIGN_SIZE)
static uint8_t        gs_sched_timer_task_stack[OS_TIMER_TASK_STACK_SIZE];
static os_task_t      gs_task_sched_timer_task;
static os_timer_t    *gs_task_sched_timer_head;
static os_bool_t      gs_task_sched_timer_need_sched;
static os_list_node_t gs_task_sched_timer_list = OS_LIST_INIT(gs_task_sched_timer_list);
#endif /* end of OS_SOFT_SINGLE_LIST_TIMER */

static inline os_list_node_t *k_single_list_get_timer_head_list(os_timer_t *timer)
{
    os_list_node_t *timer_head_list;

    timer_head_list = OS_NULL;
    if (OS_TIMER_IS_TASK_SCHED(timer))
    {
#ifdef OS_SOFT_SINGLE_LIST_TIMER
        timer_head_list = &gs_task_sched_timer_list;
#endif /* end of OS_SOFT_SINGLE_LIST_TIMER */
    }
    else
    {
#ifdef OS_HARD_SINGLE_LIST_TIMER
        timer_head_list = &gs_tick_sched_timer_list;
#endif /* end of OS_HARD_SINGLE_LIST_TIMER */
    }

    if (OS_NULL == timer_head_list)
    {
        OS_ASSERT_EX(0, "timer execption: %p", timer);
    }

    return timer_head_list;
}

static void k_single_list_timer_insert_sort_list(os_timer_t *timer)
{
    os_list_node_t *timer_head_list;
    os_timer_t     *iter;

    timer_head_list = k_single_list_get_timer_head_list(timer);
    os_list_for_each_entry(iter, timer_head_list, os_timer_t, list)
    {
        /* if the add timer round_ticks is the same as the exist timer, add the last */
        if (timer->round_ticks >= iter->round_ticks)
        {
            timer->round_ticks -= iter->round_ticks;
        }
        else
        {
            /* add before the iter, so the iter->round_ticks should delete timer->round_ticks */
            iter->round_ticks -= timer->round_ticks;
            break;
        }
    }

    /* if the timer list is null or the timer is add the last of list, the iter = timer_head_list */
    os_list_add_tail(&iter->list, &timer->list);

    if (OS_TIMER_IS_TASK_SCHED(timer))
    {
#ifdef OS_SOFT_SINGLE_LIST_TIMER
        gs_task_sched_timer_head = os_list_first_entry_or_null(timer_head_list, os_timer_t, list);
#endif
    }
    else
    {
#ifdef OS_HARD_SINGLE_LIST_TIMER
        gs_tick_sched_timer_head = os_list_first_entry_or_null(timer_head_list, os_timer_t, list);
#endif
    }

    return;
}

static void k_single_list_timer_del_sort_list(os_timer_t *timer)
{
    os_list_node_t *timer_head_list;
    os_timer_t     *next_timer;

    timer_head_list = k_single_list_get_timer_head_list(timer);

    /* the timer is the last node, direct delete, no need to proc round_ticks */
    if (timer_head_list == timer->list.next)
    {
        os_list_del(&timer->list);
    }
    else
    {
        next_timer = os_list_entry(timer->list.next, os_timer_t, list);
        next_timer->round_ticks += timer->round_ticks;
        os_list_del(&timer->list);
    }

    if (OS_TIMER_IS_TASK_SCHED(timer))
    {
#ifdef OS_SOFT_SINGLE_LIST_TIMER
        gs_task_sched_timer_head = os_list_first_entry_or_null(timer_head_list, os_timer_t, list);
#endif
    }
    else
    {
#ifdef OS_HARD_SINGLE_LIST_TIMER
        gs_tick_sched_timer_head = os_list_first_entry_or_null(timer_head_list, os_timer_t, list);
#endif
    }

    return;
}

static void k_single_list_timer_activate(os_timer_t *timer)
{
    uint8_t test_flag;

    if (OS_TIMER_FLAG_ACTIVATED != (timer->active_node.flag & OS_TIMER_FLAG_ACTIVATED))
    {
        timer->active_node.flag |= OS_TIMER_FLAG_ACTIVATED;

        /* in timer timeout callback call this func, just modify flag, enter out the call back will add in sort list */
        test_flag = OS_TIMER_FLAG_DO_TIMEOUT;
        if (test_flag != (timer->active_node.flag & test_flag))
        {
            timer->round_ticks = timer->init_ticks;
            k_single_list_timer_insert_sort_list(timer);
        }
    }

    return;
}

static void k_single_list_timer_deactivate(os_timer_t *timer)
{
    uint8_t test_flag;

    if (timer->active_node.flag & OS_TIMER_FLAG_ACTIVATED)
    {
        timer->active_node.flag &= ~OS_TIMER_FLAG_ACTIVATED;
        /* in timer timeout callback call this func, one shot timer is already deactive,
         * peridoc timer not in sort list, so not need to del */
        test_flag = OS_TIMER_FLAG_DO_TIMEOUT;
        if (test_flag != (timer->active_node.flag & test_flag))
        {
            k_single_list_timer_del_sort_list(timer);
        }
    }

    return;
}

/* if the timer not in the list(periodic timer in timeout cb) or timer not actived, the remian_tick will be 0 */
static os_tick_t k_single_list_timer_calc_remain_ticks(os_timer_t *timer)
{
    os_list_node_t *timer_head_list;
    os_tick_t       remain_tick;
    os_timer_t     *iter;

    remain_tick     = 0;
    timer_head_list = k_single_list_get_timer_head_list(timer);

    if (timer->active_node.flag & OS_TIMER_FLAG_ACTIVATED)
    {
        os_list_for_each_entry(iter, timer_head_list, os_timer_t, list)
        {
            remain_tick += iter->round_ticks;
            if (iter == timer)
            {
                break;
            }
        }
    }

    return remain_tick;
}

static void k_single_list_timer_do_init(os_timer_t *timer,
                                        const char *name,
                                        void (*function)(void *parameter),
                                        void    *parameter,
                                        uint32_t timeout,
                                        uint8_t  flag)
{
    timer->active_node.flag = (flag & (~OS_TIMER_FLAG_ACTIVATED)) | OS_TIMER_FLAG_INITED;
    timer->timeout_func     = function;
    timer->parameter        = parameter;
    timer->init_ticks       = ((0 == timeout) ? 1 : timeout);
    timer->round_ticks      = timer->init_ticks;

    if (OS_NULL != name)
    {
        strncpy(&timer->name[0], name, OS_NAME_MAX);
        timer->name[OS_NAME_MAX] = '\0';
    }
    else
    {
        timer->name[0] = '\0';
    }

    os_list_init(&timer->list);
}

os_tick_t k_single_list_timer_get_next_remain_ticks(void)
{
    os_tick_t min_timeout_tick;

    min_timeout_tick = OS_UINT32_MAX;
#ifdef OS_SOFT_SINGLE_LIST_TIMER
    if (OS_NULL != gs_task_sched_timer_head)
    {
        min_timeout_tick = gs_task_sched_timer_head->round_ticks;
    }
#endif /* end of OS_SOFT_SINGLE_LIST_TIMER */

#ifdef OS_HARD_SINGLE_LIST_TIMER
    if (OS_NULL != gs_tick_sched_timer_head)
    {
        if (gs_tick_sched_timer_head->round_ticks < min_timeout_tick)
        {
            min_timeout_tick = gs_tick_sched_timer_head->round_ticks;
            /* for hard timer, the timeout is sched in tick irq, when the mcu is wakeup, it needs to wait tick irq, so
             * do -- */
            if (min_timeout_tick >= 1)
            {
                min_timeout_tick--;
            }
        }
    }
#endif /* end of OS_HARD_SINGLE_LIST_TIMER */

    return min_timeout_tick;
}

os_bool_t k_single_list_timer_update_active_list(os_tick_t ticks)
{
    os_bool_t   need_sched;
    os_timer_t *iter;
    os_tick_t   temp;

    need_sched = OS_FALSE;

#ifdef OS_SOFT_SINGLE_LIST_TIMER
    if (OS_NULL != gs_task_sched_timer_head)
    {
        temp = ticks;
        os_list_for_each_entry(iter, &gs_task_sched_timer_list, os_timer_t, list)
        {
            if (iter->round_ticks > temp)
            {
                iter->round_ticks -= temp;
                break;
            }
            else
            {
                iter->round_ticks              = 0;
                gs_task_sched_timer_need_sched = OS_TRUE;

                if (iter->round_ticks < temp)
                {
                    temp -= iter->round_ticks;
                }
                else
                {
                    break;
                }
            }
        }
    }
#endif /* end of OS_SOFT_SINGLE_LIST_TIMER */

#ifdef OS_HARD_SINGLE_LIST_TIMER
    if (OS_NULL != gs_tick_sched_timer_head)
    {
        temp = ticks;
        os_list_for_each_entry(iter, &gs_tick_sched_timer_list, os_timer_t, list)
        {
            if (iter->round_ticks > temp)
            {
                iter->round_ticks -= temp;
                break;
            }
            else
            {
                iter->round_ticks = 0;
                if (iter->round_ticks < temp)
                {
                    temp -= iter->round_ticks;
                }
            }
        }
    }
#endif /* end of OS_HARD_SINGLE_LIST_TIMER */

#ifdef OS_SOFT_SINGLE_LIST_TIMER
    if (gs_task_sched_timer_need_sched && (OS_TASK_STATE_SUSPEND == gs_task_sched_timer_task.state))
    {
        gs_task_sched_timer_task.state &= ~OS_TASK_STATE_SUSPEND;
        gs_task_sched_timer_task.state |= OS_TASK_STATE_READY;
        k_readyq_put(&gs_task_sched_timer_task);
        need_sched = OS_TRUE;
    }
#endif /* end of OS_SOFT_SINGLE_LIST_TIMER */

    return need_sched;
}

static inline uint8_t k_single_list_timer_check_flag(uint8_t flag)
{
#ifdef OS_HARD_SINGLE_LIST_TIMER
#ifndef OS_SOFT_SINGLE_LIST_TIMER
    /* just conf hard timer, change default flag to hard timer */
    flag &= ~OS_TIMER_FLAG_TASK_SCHED;
#endif
#else
#ifdef OS_SOFT_SINGLE_LIST_TIMER
    /* just conf soft timer, change default flag to soft timer */
    flag |= OS_TIMER_FLAG_TASK_SCHED;
#endif
#endif

    return flag;
}

os_timer_id os_timer_create(os_timer_dummy_t *timer_cb,
                            const char       *name,
                            void (*function)(void *parameter),
                            void     *parameter,
                            os_tick_t timeout,
                            uint8_t   flag)
{
    os_timer_t *timer;

    OS_KERNEL_INIT();

#ifndef OS_USING_HEAP
    OS_ASSERT(OS_NULL != timer_cb);
#endif

    OS_ASSERT(OS_FALSE == os_is_irq_active());

    if (timer_cb != OS_NULL)
    {
        timer = (os_timer_t *)timer_cb;

        flag = k_single_list_timer_check_flag(flag);

        OS_KERNEL_ENTER();
        k_single_list_timer_do_init(timer, name, function, parameter, timeout, flag & (~OS_TIMER_FLAG_DYNAMIC));
        OS_KERNEL_EXIT();
    }
#ifdef OS_USING_HEAP
    else
    {
        OS_ASSERT(OS_FALSE == os_is_irq_active());

        timer = (os_timer_t *)OS_KERNEL_MALLOC(sizeof(os_timer_t));
        if (OS_NULL == timer)
        {
            OS_KERN_LOG(KERN_ERROR, TIMER_TAG, "Malloc timer memory failed");
        }
        else
        {
            flag = k_single_list_timer_check_flag(flag);

            OS_KERNEL_ENTER();
            k_single_list_timer_do_init(timer, name, function, parameter, timeout, flag | OS_TIMER_FLAG_DYNAMIC);
            OS_KERNEL_EXIT();
        }
    }
#endif

    return OS_TYPE_CONVERT(os_timer_id, timer);
}

os_err_t os_timer_destroy(os_timer_id timer_id)
{
    os_timer_t *timer;

    OS_KERNEL_INIT();

    timer = OS_TYPE_CONVERT(os_timer_t *, timer_id);

    OS_ASSERT(OS_NULL != timer);
    OS_ASSERT(0 != (timer->active_node.flag & OS_TIMER_FLAG_INITED));
    OS_ASSERT(OS_FALSE == os_is_irq_active());
#ifndef OS_USING_HEAP
    OS_ASSERT(OS_TIMER_FLAG_STATIC == (timer->active_node.flag & OS_TIMER_FLAG_DYNAMIC));
#endif

    OS_KERNEL_ENTER();

    /* in timer timeout callback func call os_timer_destroy,
     * because the timer is delete from single-list, and add to the timerout list, so just delete form timerout list
     */
    if (OS_TIMER_FLAG_DO_TIMEOUT == (timer->active_node.flag & OS_TIMER_FLAG_DO_TIMEOUT))
    {
        timer->active_node.flag &= ~OS_TIMER_FLAG_DO_TIMEOUT;
        timer->active_node.flag &= ~OS_TIMER_FLAG_ACTIVATED;
        os_list_del(&timer->list);
    }
    else
    {
        k_single_list_timer_deactivate(timer);
    }

    timer->active_node.flag &= ~OS_TIMER_FLAG_INITED;

    OS_KERNEL_EXIT();

#ifdef OS_USING_HEAP
    if (OS_TIMER_FLAG_DYNAMIC == (timer->active_node.flag & OS_TIMER_FLAG_DYNAMIC))
    {
        OS_KERNEL_FREE(timer);
    }
#endif

    return OS_SUCCESS;
}

#ifdef OS_SOFT_SINGLE_LIST_TIMER
static void k_single_list_timer_task_entry(void *parameter)
{
    os_timer_t    *head_timer;
    os_task_t     *current_task;
    os_list_node_t timeout_timer_list;
    uint8_t        test_flag;
    void (*timeout_func)(void *timeout_param);
    void *timeout_param;

    OS_KERNEL_INIT();

    OS_UNREFERENCE(parameter);

    while (1)
    {
        OS_KERNEL_ENTER();

        if (!gs_task_sched_timer_need_sched || OS_NULL == gs_task_sched_timer_head)
        {
            /* No timer needs handle,suspend myself. */
            current_task = k_task_self();
            k_readyq_remove(current_task);
            current_task->state &= ~OS_TASK_STATE_READY;
            current_task->state |= OS_TASK_STATE_SUSPEND;
            gs_task_sched_timer_need_sched = OS_FALSE;
            /* give up cpu use right, so need sched */
            OS_KERNEL_EXIT_SCHED();

            /* when return from task, do continue and will return to while start; */
            continue;
        }

        head_timer = gs_task_sched_timer_head;

        /* the timeout timer may delete or next timer is not timeout */
        if (head_timer->round_ticks > 0)
        {
            gs_task_sched_timer_need_sched = OS_FALSE;
            OS_KERNEL_EXIT();
            continue;
        }

        timeout_func  = head_timer->timeout_func;
        timeout_param = head_timer->parameter;

        head_timer->active_node.flag |= OS_TIMER_FLAG_DO_TIMEOUT;
        /* delete timerout timer from single-list, beacuse of timer timeout, so not need to compensate round_ticks */
        os_list_del(&head_timer->list);
        /* update the gs_task_sched_timer_head*/
        gs_task_sched_timer_head = os_list_first_entry_or_null(&gs_task_sched_timer_list, os_timer_t, list);

        test_flag = OS_TIMER_FLAG_PERIODIC;
        /* if the timer is one shot timer, stop it */
        if (test_flag != (head_timer->active_node.flag & test_flag))
        {
            head_timer->active_node.flag &= ~OS_TIMER_FLAG_ACTIVATED;
        }

        /* if the timer timeout call os_timer_start/os_timer_stop/os_timer_destroy,
         * if os_timer_destory called, the head_timer param is not exist
         */
        os_list_init(&timeout_timer_list);
        os_list_add(&timeout_timer_list, &head_timer->list);
        OS_KERNEL_EXIT();

        if (OS_NULL != timeout_func)
        {
            timeout_func(timeout_param);
        }

        OS_KERNEL_ENTER();

        /* the timeout_func may call timer api, for example: os_timer_destroy/os_timer_start/os_timer_stop,
         * if os_timer_destroy called, the head_timer will be null
         */
        head_timer = os_list_first_entry_or_null(&timeout_timer_list, os_timer_t, list);
        if (OS_NULL != head_timer)
        {
            head_timer->active_node.flag &= ~OS_TIMER_FLAG_DO_TIMEOUT;
            os_list_del(&head_timer->list);

            /* no need to test the timer is one shot or perdoic,
             * one shot timer start in timeout func or other task, perdoic timer should re-insert into list
             */
            test_flag = OS_TIMER_FLAG_ACTIVATED;
            if (test_flag == (head_timer->active_node.flag & test_flag))
            {
                /* reset the round_tick, then add to the list */
                head_timer->round_ticks = head_timer->init_ticks;
#if OS_TIMER_PERIODIC_COMPENSATE
                os_tick_t diff;
                /* compensate for periodic timer's timeout cb exec time */
                test_flag = OS_TIMER_FLAG_PERIODIC;
                if (test_flag == (head_timer->active_node.flag & test_flag))
                {
                    diff = OS_TIMER_TICK_DIFF(os_tick_get(), head_timer->active_node.timeout_tick);
                    if (diff < head_timer->round_ticks)
                    {
                        head_timer->round_ticks -= diff;
                    }
                }
#endif
                k_single_list_timer_insert_sort_list(head_timer);
            }
        }

        OS_KERNEL_EXIT();
    }
}

os_bool_t k_single_list_timer_need_handle(void)
{
    os_bool_t   need_handle;
    os_timer_t *iter;

    if (OS_NULL == gs_task_sched_timer_head)
    {
        return OS_FALSE;
    }

    need_handle = OS_FALSE;

    if (gs_task_sched_timer_head->round_ticks == 1)
    {
        /* now the timer arrived the timeout tick */
        gs_task_sched_timer_need_sched        = OS_TRUE;
        gs_task_sched_timer_head->round_ticks = 0;
#if OS_TIMER_PERIODIC_COMPENSATE
        os_tick_t now;

        now = os_tick_get();
        /* update all timerout timers' who's round_ticks = 0 timeout tick */
        os_list_for_each_entry(iter, &gs_task_sched_timer_list, os_timer_t, list)
        {
            if (!iter->round_ticks)
            {
                iter->active_node.timeout_tick = now;
            }
            else
            {
                break;
            }
        }
#endif
    }
    else if (!gs_task_sched_timer_head->round_ticks)
    {
        gs_task_sched_timer_need_sched = OS_TRUE;
        /* maybe the timer timeout cb do a long time */
        os_list_for_each_entry(iter, &gs_task_sched_timer_list, os_timer_t, list)
        {
            if (!iter->round_ticks)
            {
                continue;
            }
            else
            {
                iter->round_ticks--;
                break;
            }
        }
    }
    else
    {
        gs_task_sched_timer_head->round_ticks--;
    }

    if (gs_task_sched_timer_need_sched && (OS_TASK_STATE_SUSPEND == gs_task_sched_timer_task.state))
    {
        gs_task_sched_timer_task.state &= ~OS_TASK_STATE_SUSPEND;
        gs_task_sched_timer_task.state |= OS_TASK_STATE_READY;
        k_readyq_put(&gs_task_sched_timer_task);
        need_handle = OS_TRUE;
    }

    return need_handle;
}
#endif /* end of OS_SOFT_SINGLE_LIST_TIMER */

#ifdef OS_HARD_SINGLE_LIST_TIMER
void k_single_list_timer_tick_proc(void)
{
    os_timer_t    *head_timer;
    os_list_node_t timeout_timer_list;
    uint8_t        test_flag;
    void (*timeout_func)(void *timeout_param);
    void *timeout_param;

    OS_KERNEL_INIT();

    OS_KERNEL_ENTER();
    if (OS_NULL == gs_tick_sched_timer_head)
    {
        OS_KERNEL_EXIT();

        return;
    }

    /* compatibility for lowpower sleep */
    if (gs_tick_sched_timer_head->round_ticks > 1)
    {
        gs_tick_sched_timer_head->round_ticks--;
        OS_KERNEL_EXIT();

        return;
    }
    else
    {
        gs_tick_sched_timer_head->round_ticks = 0;
    }

    while (1)
    {
        head_timer = gs_tick_sched_timer_head;
        if (OS_NULL == head_timer || head_timer->round_ticks)
        {
            break;
        }

        timeout_func  = head_timer->timeout_func;
        timeout_param = head_timer->parameter;

        head_timer->active_node.flag |= OS_TIMER_FLAG_DO_TIMEOUT;
        os_list_del(&head_timer->list);
        /* update the gs_tick_sched_timer_head */
        gs_tick_sched_timer_head = os_list_first_entry_or_null(&gs_tick_sched_timer_list, os_timer_t, list);

        test_flag = OS_TIMER_FLAG_PERIODIC;
        /* if the timer is one shot timer, stop it */
        if (test_flag != (head_timer->active_node.flag & test_flag))
        {
            head_timer->active_node.flag &= ~OS_TIMER_FLAG_ACTIVATED;
        }

        /* if the timer timeout call os_timer_start/os_timer_stop/os_timer_destroy,
         * if os_timer_destory called, the head_timer param is not exist
         */
        os_list_init(&timeout_timer_list);
        os_list_add(&timeout_timer_list, &head_timer->list);

        OS_KERNEL_EXIT();
        if (OS_NULL != timeout_func)
        {
            timeout_func(timeout_param);
        }
        OS_KERNEL_ENTER();

        /* the timeout_func may call timer api, for example: os_timer_destroy/os_timer_start/os_timer_stop,
         * if os_timer_destroy called, the head_timer will be null
         */
        head_timer = os_list_first_entry_or_null(&timeout_timer_list, os_timer_t, list);
        if (OS_NULL != head_timer)
        {
            head_timer->active_node.flag &= ~OS_TIMER_FLAG_DO_TIMEOUT;
            os_list_del(&head_timer->list);

            /* no need to test the timer is one shot or perdoic,
             * one shot timer start in timeout func or other task, perdoic timer should re-insert into list
             */
            test_flag = OS_TIMER_FLAG_ACTIVATED;
            if (test_flag == (head_timer->active_node.flag & test_flag))
            {
                /* reset the round_tick, then add to the list */
                head_timer->round_ticks = head_timer->init_ticks;
                k_single_list_timer_insert_sort_list(head_timer);
            }
        }
    }

    OS_KERNEL_EXIT();

    return;
}
#endif /* end of OS_HARD_SINGLE_LIST_TIMER */

void k_single_list_timer_module_init(void)
{
#ifdef OS_HARD_SINGLE_LIST_TIMER
    gs_tick_sched_timer_head = OS_NULL;
#endif

#ifdef OS_SOFT_SINGLE_LIST_TIMER
    os_task_id tid;

    gs_task_sched_timer_need_sched = OS_FALSE;
    gs_task_sched_timer_head       = OS_NULL;

    /* Create and start timer task. */
    tid = os_task_create((os_task_dummy_t *)&gs_task_sched_timer_task,
                         gs_sched_timer_task_stack,
                         sizeof(gs_sched_timer_task_stack),
                         "timer",
                         k_single_list_timer_task_entry,
                         OS_NULL,
                         OS_TIMER_TASK_PRIO);
    if (!tid)
    {
        OS_ASSERT_EX(0, "Why initialize timer task failed?");
    }
    else
    {
        if (OS_SUCCESS != os_task_startup(tid))
        {
            OS_ASSERT_EX(0, "Why startup timer task failed?");
        }
    }
#endif

    return;
}

/**
 ***********************************************************************************************************************
 * @brief           This function will starup the specified timer by timer descriptor.
 *
 * @param[in]       timer_id           The ID of timer control block
 *
 * @return          Will only return OS_SUCCESS
 ***********************************************************************************************************************
 */
os_err_t os_timer_start(os_timer_id timer_id)
{
    os_timer_t *timer;

    OS_KERNEL_INIT();

    timer = OS_TYPE_CONVERT(os_timer_t *, timer_id);

    OS_ASSERT(OS_NULL != timer);
    OS_ASSERT(0 != (timer->active_node.flag & OS_TIMER_FLAG_INITED));

    OS_KERNEL_ENTER();

    /* First deactivate timer, then start it. */
    k_single_list_timer_deactivate(timer);
    k_single_list_timer_activate(timer);

    OS_KERNEL_EXIT();

    return OS_SUCCESS;
}

/**
 ***********************************************************************************************************************
 * @brief           This function will stop the specified timer by timer descriptor.
 *
 * @param[in]       timer_id           The ID of timer control block
 *
 * @return          Stop success or fail.
 * @retval          OS_SUCCESS          success.
 * @retval          OS_FAILURE        fail.
 ***********************************************************************************************************************
 */
os_err_t os_timer_stop(os_timer_id timer_id)
{
    os_err_t    ret;
    os_timer_t *timer;

    OS_KERNEL_INIT();

    timer = OS_TYPE_CONVERT(os_timer_t *, timer_id);

    OS_ASSERT(OS_NULL != timer);
    OS_ASSERT(0 != (timer->active_node.flag & OS_TIMER_FLAG_INITED));

    OS_KERNEL_ENTER();
    ret = OS_SUCCESS;
    if (!(timer->active_node.flag & OS_TIMER_FLAG_ACTIVATED))
    {
        ret = OS_FAILURE;
    }
    else
    {
        k_single_list_timer_deactivate(timer);
    }
    OS_KERNEL_EXIT();

    return ret;
}

/**
 ***********************************************************************************************************************
 * @brief           This function set delay time of timer.
 *
 * @param           timer_id           The ID of timer control block.
 *
 * @return          Will only return OS_SUCCESS
 ***********************************************************************************************************************
 */
os_err_t os_timer_set_timeout_ticks(os_timer_id timer_id, os_tick_t timeout)
{
    os_timer_t *timer;

    OS_KERNEL_INIT();

    timer = OS_TYPE_CONVERT(os_timer_t *, timer_id);

    OS_ASSERT(OS_NULL != timer);
    OS_ASSERT(0 != (timer->active_node.flag & OS_TIMER_FLAG_INITED));

    OS_KERNEL_ENTER();
    timer->init_ticks = timeout;
    OS_KERNEL_EXIT();

    return OS_SUCCESS;
}

/**
 ***********************************************************************************************************************
 * @brief           This function gets the initial ticks that timer needs to delay.
 *
 * @param           timer_id           The ID of timer control block.
 *
 * @return          Number of ticks.
 ***********************************************************************************************************************
 */
os_tick_t os_timer_get_timeout_ticks(os_timer_id timer_id)
{
    os_tick_t   init_ticks;
    os_timer_t *timer;

    OS_KERNEL_INIT();

    timer = OS_TYPE_CONVERT(os_timer_t *, timer_id);

    OS_ASSERT(OS_NULL != timer);
    OS_ASSERT(0 != (timer->active_node.flag & OS_TIMER_FLAG_INITED));

    OS_KERNEL_ENTER();
    init_ticks = timer->init_ticks;
    OS_KERNEL_EXIT();

    return init_ticks;
}

/**
 ***********************************************************************************************************************
 * @brief           This function gets the remaining ticks before expiration.
 *
 * @param           timer_id           The ID of timer control block.
 *
 * @return          Number of ticks.
 ***********************************************************************************************************************
 */
os_tick_t os_timer_get_remain_ticks(os_timer_id timer_id)
{
    os_tick_t   remain_ticks;
    os_timer_t *timer;

    OS_KERNEL_INIT();

    timer = OS_TYPE_CONVERT(os_timer_t *, timer_id);

    OS_ASSERT(OS_NULL != timer);
    OS_ASSERT(0 != (timer->active_node.flag & OS_TIMER_FLAG_INITED));

    OS_KERNEL_ENTER();
    remain_ticks = k_single_list_timer_calc_remain_ticks(timer);
    OS_KERNEL_EXIT();

    return remain_ticks;
}

/**
 ***********************************************************************************************************************
 * @brief           Get the name of the specified timer.
 *
 * @param[in]       timer_id        The ID of timer control block.
 *
 * @return          The name of the specified timer.
 ***********************************************************************************************************************
 */
const char *os_timer_get_name(os_timer_id timer_id)
{
    os_timer_t *timer;

    timer = OS_TYPE_CONVERT(os_timer_t *, timer_id);

    OS_ASSERT(OS_NULL != timer);
    OS_ASSERT(0 != (timer->active_node.flag & OS_TIMER_FLAG_INITED));

    return timer->name;
}

/**
 ***********************************************************************************************************************
 * @brief           This function set the timer to be oneshot.
 *
 * @param           timer_id           The ID of timer control block.
 *
 * @return          Will only return OS_SUCCESS
 ***********************************************************************************************************************
 */
os_err_t os_timer_set_oneshot(os_timer_id timer_id)
{
    os_timer_t *timer;

    OS_KERNEL_INIT();

    timer = OS_TYPE_CONVERT(os_timer_t *, timer_id);

    OS_ASSERT(OS_NULL != timer);
    OS_ASSERT(0 != (timer->active_node.flag & OS_TIMER_FLAG_INITED));

    OS_KERNEL_ENTER();
    timer->active_node.flag &= ~OS_TIMER_FLAG_PERIODIC;
    OS_KERNEL_EXIT();

    return OS_SUCCESS;
}

/**
 ***********************************************************************************************************************
 * @brief           This function set the timer to be periodic.
 *
 * @param           timer_id           The ID of timer control block.
 *
 * @return          Will only return OS_SUCCESS
 ***********************************************************************************************************************
 */
os_err_t os_timer_set_periodic(os_timer_id timer_id)
{
    os_timer_t *timer;

    OS_KERNEL_INIT();

    timer = OS_TYPE_CONVERT(os_timer_t *, timer_id);

    OS_ASSERT(OS_NULL != timer);
    OS_ASSERT(0 != (timer->active_node.flag & OS_TIMER_FLAG_INITED));

    OS_KERNEL_ENTER();
    timer->active_node.flag |= OS_TIMER_FLAG_PERIODIC;
    OS_KERNEL_EXIT();

    return OS_SUCCESS;
}

/**
 ***********************************************************************************************************************
 * @brief           This function return the timer is hard or soft.
 *
 * @param           timer_id           The ID of timer control block.
 *
 * @return          Timer is hard or soft.
 * @retval          OS_FASLE        Timer is hard.
 * @retval          OS_TRUE         Timer is soft.
 ***********************************************************************************************************************
 */
os_bool_t os_timer_is_soft(os_timer_id timer_id)
{
    os_bool_t   is_soft;
    os_timer_t *timer;

    OS_KERNEL_INIT();

    timer = OS_TYPE_CONVERT(os_timer_t *, timer_id);

    OS_ASSERT(OS_NULL != timer);
    OS_ASSERT(0 != (timer->active_node.flag & OS_TIMER_FLAG_INITED));

    OS_KERNEL_ENTER();
    is_soft = (timer->active_node.flag & OS_TIMER_FLAG_TASK_SCHED) ? OS_TRUE : OS_FALSE;
    OS_KERNEL_EXIT();

    return is_soft;
}

/**
 ***********************************************************************************************************************
 * @brief           This function return the timer is one shot or periodic.
 *
 * @param           timer_id           The ID of timer control block.
 *
 * @return          Timer is one shot or periodic.
 * @retval          OS_FASLE        Timer is one shot.
 * @retval          OS_TRUE         Timer is periodic.
 ***********************************************************************************************************************
 */
os_bool_t os_timer_is_periodic(os_timer_id timer_id)
{
    os_bool_t   is_periodic;
    os_timer_t *timer;

    OS_KERNEL_INIT();

    timer = OS_TYPE_CONVERT(os_timer_t *, timer_id);

    OS_ASSERT(OS_NULL != timer);
    OS_ASSERT(0 != (timer->active_node.flag & OS_TIMER_FLAG_INITED));

    OS_KERNEL_ENTER();
    is_periodic = (timer->active_node.flag & OS_TIMER_FLAG_PERIODIC) ? OS_TRUE : OS_FALSE;
    OS_KERNEL_EXIT();

    return is_periodic;
}

/**
 ***********************************************************************************************************************
 * @brief           This function return the timer is active or inactive.
 *
 * @param           timer_id           The ID of timer control block.
 *
 * @return          Timer is active or inactive.
 * @retval          OS_FASLE        Timer is inactive.
 * @retval          OS_TRUE         Timer is active.
 ***********************************************************************************************************************
 */
os_bool_t os_timer_is_active(os_timer_id timer_id)
{
    os_bool_t   is_active;
    os_timer_t *timer;

    OS_KERNEL_INIT();

    timer = OS_TYPE_CONVERT(os_timer_t *, timer_id);

    OS_ASSERT(OS_NULL != timer);
    OS_ASSERT(0 != (timer->active_node.flag & OS_TIMER_FLAG_INITED));

    OS_KERNEL_ENTER();
    is_active = (timer->active_node.flag & OS_TIMER_FLAG_ACTIVATED) ? OS_TRUE : OS_FALSE;
    OS_KERNEL_EXIT();

    return is_active;
}

os_bool_t os_timer_check_exist(os_timer_id timer_id)
{
    os_timer_t *timer;

    timer = OS_TYPE_CONVERT(os_timer_t *, timer_id);

    OS_ASSERT(OS_NULL != timer);

    if (0 != (timer->active_node.flag & OS_TIMER_FLAG_INITED))
    {
        return OS_TRUE;
    }
    else
    {
        return OS_FALSE;
    }
}

#if defined(OS_USING_SHELL)
#include <shell.h>

typedef struct
{
    os_timer_t *timer;
    os_tick_t   init_ticks;
    os_tick_t   remain_ticks;
    uint8_t     flag;
} sh_timer_info_t;

static OS_DEFINE_SPINLOCK(gs_os_timer_list_lock);

static void sh_show_one_timer_info(os_timer_t *timer)
{
    sh_timer_info_t iter;

    OS_KERNEL_INIT();

    OS_KERNEL_ENTER();

    iter.timer        = timer;
    iter.remain_ticks = k_single_list_timer_calc_remain_ticks(timer);
    iter.init_ticks   = timer->init_ticks;
    iter.flag         = timer->active_node.flag;

    OS_KERNEL_EXIT();

    os_kprintf("%-*.*s %-12lu %-12lu ", OS_NAME_MAX, OS_NAME_MAX, iter.timer->name, iter.init_ticks, iter.remain_ticks);

    if (iter.flag & OS_TIMER_FLAG_ACTIVATED)
    {
        os_kprintf("%-*s ", strlen("inactive"), "active");
    }
    else
    {
        os_kprintf("%-*s ", strlen("inactive"), "inactive");
    }

    if (iter.flag & OS_TIMER_FLAG_PERIODIC)
    {
        os_kprintf("%-*s", strlen("periodic,"), "periodic,");
    }
    else
    {
        os_kprintf("%-*s", strlen("periodic,"), "oneshot,");
    }

    if (iter.flag & OS_TIMER_FLAG_DYNAMIC)
    {
        os_kprintf("%-*s\r\n", strlen("dynamic"), "dynamic");
    }
    else
    {
        os_kprintf("%-*s\r\n", strlen("dynamic"), "static");
    }
}

os_err_t sh_show_timer_info(int32_t argc, char **argv)
{
    os_tick_t current_tick;
    uint32_t  i;

    OS_UNREFERENCE(argc);
    OS_UNREFERENCE(argv);

#ifdef OS_SOFT_SINGLE_LIST_TIMER
    if (OS_NULL != gs_task_sched_timer_head)
    {
        os_kprintf("%-*s Interval     Remain       State    Type\r\n", OS_NAME_MAX, "Soft-timer");
        i = OS_NAME_MAX;
        while (i--)
        {
            os_kprintf("-");
        }
        os_kprintf(" ------------ ------------ -------- ---------------------\r\n");

        os_timer_t *iter;

        os_spin_lock(&gs_os_timer_list_lock);
        os_list_for_each_entry(iter, &gs_task_sched_timer_list, os_timer_t, list)
        {
            sh_show_one_timer_info(iter);
        }
        os_spin_unlock(&gs_os_timer_list_lock);
    }
#endif

#ifdef OS_HARD_SINGLE_LIST_TIMER
    if (OS_NULL != gs_tick_sched_timer_head)
    {
        os_kprintf("%-*s Interval     Remain       State    Type\r\n", OS_NAME_MAX, "Hard-timer");
        i = OS_NAME_MAX;
        while (i--)
        {
            os_kprintf("-");
        }
        os_kprintf(" ------------ ------------ -------- ---------------------\r\n");

        os_timer_t *iter;

        os_spin_lock(&gs_os_timer_list_lock);
        os_list_for_each_entry(iter, &gs_tick_sched_timer_list, os_timer_t, list)
        {
            sh_show_one_timer_info(iter);
        }
        os_spin_unlock(&gs_os_timer_list_lock);
    }
#endif

    OS_KERNEL_INIT();

    OS_KERNEL_ENTER();
    current_tick = os_tick_get_value();
    OS_KERNEL_EXIT();

    os_kprintf("current tick:%-lu\r\n", current_tick);

    return OS_SUCCESS;
}
SH_CMD_EXPORT(show_timer, sh_show_timer_info, "Show timer information");

#endif /* End of OS_USING_SHELL */

#endif /* End of OS_USING_SINGLE_LIST_TIMER */
