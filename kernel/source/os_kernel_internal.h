/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        os_kernel_internal.h
 *
 * @brief       Provides some macro definitions and function declarations, only use for kernel.
 *
 * @revision
 * Date         Author          Notes
 * 2020-03-03   OneOS Team      First version.
 ***********************************************************************************************************************
 */
#ifndef __OS_KERNEL_INTERNAL_H__
#define __OS_KERNEL_INTERNAL_H__

#include <oneos_config.h>
#include <os_types.h>
#include <os_stddef.h>
#include <os_assert.h>
#include "os_prototypes.h"
#include <os_task.h>
#include <stdlib.h>
#include <os_memory.h>
#include <arch_interrupt.h>
#include <arch_misc.h>

#ifdef __cplusplus
extern "C" {
#endif

#define OS_KERNEL_VERSION "Kernel-V3.0.0"

#define OS_KERNEL_ERR_INFO "The created object already exists."

#ifdef OS_USING_HEAP
#define OS_KERNEL_MALLOC(size)                  os_malloc(size)
#define OS_KERNEL_MALLOC_ALIGN(alignment, size) os_aligned_malloc(alignment, size)

#define OS_KERNEL_FREE(ptr)          os_free(ptr)
#define OS_KERNEL_REALLOC(ptr, size) os_realloc(ptr, size)
#else
#define OS_KERNEL_MALLOC(size)
#define OS_KERNEL_MALLOC_ALIGN(alignment, size)

#define OS_KERNEL_FREE(ptr)
#define OS_KERNEL_REALLOC(ptr, size)
#endif /* OS_USING_HEAP */

#define OS_KOBJ_INITED   0x55
#define OS_KOBJ_DEINITED 0xAA

#define OS_ALLOC_TYPE_STATIC        0
#define OS_KOBJ_ALLOC_TYPE_DYNAMIC  (1 << 0)
#define OS_KDATA_ALLOC_TYPE_DYNAMIC (1 << 1)

#define OS_TYPE_CONVERT(type, obj) ((type)(obj))

#ifdef OS_USING_KERNEL_DEBUG

/* Kernel log level */
#define KERN_ERROR   0 /* Error conditions */
#define KERN_WARNING 1 /* Warning conditions */
#define KERN_INFO    2 /* Informational */
#define KERN_DEBUG   3 /* Debug-level messages */

#ifdef KLOG_WITH_FUNC_LINE

/**
 ***********************************************************************************************************************
 * @def         OS_KERN_LOG
 *
 * @brief       Print kernel log with function name and file line number.
 *
 * @param       level           The log level.
 * @param       tag             The log tag.
 * @param       fmt             The format.
 ***********************************************************************************************************************
 */
#define OS_KERN_LOG(level, tag, fmt, ...)                                                                              \
    do                                                                                                                 \
    {                                                                                                                  \
        if ((level <= *g_klog_global_lvl) || (g_klog_tag_lvl_list->next != g_klog_tag_lvl_list))                       \
        {                                                                                                              \
            os_kernel_print(level, tag, OS_TRUE, fmt " [%s][%d]", ##__VA_ARGS__, __FUNCTION__, __LINE__);              \
        }                                                                                                              \
    } while (0)

#else

/**
 ***********************************************************************************************************************
 * @def         OS_KERN_LOG
 *
 * @brief       Print kernel log without function name and file line number.
 *
 * @param       level           The log level.
 * @param       tag             The log tag.
 * @param       fmt             The format.
 ***********************************************************************************************************************
 */
#define OS_KERN_LOG(level, tag, fmt, ...)                                                                              \
    do                                                                                                                 \
    {                                                                                                                  \
        if ((level <= *g_klog_global_lvl) || (g_klog_tag_lvl_list->next != g_klog_tag_lvl_list))                       \
        {                                                                                                              \
            os_kernel_print(level, tag, OS_TRUE, fmt, ##__VA_ARGS__);                                                  \
        }                                                                                                              \
    } while (0)

#endif /* KLOG_WITH_FUNC_LINE */

extern const int16_t        *g_klog_global_lvl;
extern const os_list_node_t *g_klog_tag_lvl_list;

extern void os_kernel_print(uint16_t level, const char *tag, os_bool_t newline, const char *fmt, ...);

#else
#define OS_KERN_LOG(level, tag, fmt, ...)
#endif /* OS_USING_KERNEL_DEBUG */

#ifdef OS_USING_SMP
extern os_task_t *g_os_current_task[OS_SMP_MAX_CPUS];
extern os_task_t *g_os_next_task[OS_SMP_MAX_CPUS];

extern void _k_task_close(os_task_t *task);

OS_INLINE os_task_t *k_task_self(void)
{
    os_ubase_t irq_save;
    os_task_t *task;

    irq_save = os_irq_lock();

    task = g_os_current_task[os_cpu_id_get()];

    os_irq_unlock(irq_save);

    return task;
}

#define _k_task_self() g_os_current_task[os_cpu_id_get()]

#else
extern os_task_t *g_os_current_task;
extern os_task_t *g_os_next_task;
extern os_task_t *g_os_high_task;

extern int16_t g_os_sched_lock_cnt;

#define k_task_self() g_os_current_task

#define _k_task_self() g_os_current_task

#endif

extern void k_tickq_init(void);
extern void k_tickq_put(struct os_task *task, os_tick_t timeout);
extern void k_tickq_remove(struct os_task *task);

extern void      k_readyq_put(struct os_task *task);
extern void      k_readyq_remove(struct os_task *task);
extern os_bool_t k_readyq_move_tail(struct os_task *task);

extern void k_sched_init(void);

extern void k_start(void);

extern void k_recycle_task_init(void);
extern void k_idle_task_init(void);

extern void k_blockq_insert(os_list_node_t *head, struct os_task *task);
extern os_err_t
k_block_task(os_spinlock_t *lock, os_ubase_t irq_save, os_list_node_t *head, os_tick_t timeout, os_bool_t is_wake_prio);

extern os_bool_t k_unblock_task(os_task_t *task);
extern os_bool_t k_cancel_all_blocked_task(const os_list_node_t *head);

#ifdef OS_USING_SHELL
struct block_task_info
{
    char   *name;             /* Task name */
    uint8_t current_priority; /* Task current priority. */
};
typedef struct block_task_info block_task_info_t;

extern void k_show_blocked_task(block_task_info_t *info, uint16_t block_task_cnt);
#endif

#ifdef OS_USING_HASH_BUCKET_TIMER
extern void      k_timer_module_init(void);
extern os_bool_t k_timer_need_handle(void);
extern void      k_move_timer_list_one_step(void);

#ifdef OS_HASH_BUCKET_TIMER_SORT
extern os_tick_t k_timer_get_next_remain_ticks(void);
extern void      k_timer_update_active_list(os_tick_t ticks);
#endif

#endif

#ifdef OS_USING_SINGLE_LIST_TIMER
extern void      k_single_list_timer_module_init(void);
extern os_tick_t k_single_list_timer_get_next_remain_ticks(void);
extern os_bool_t k_single_list_timer_update_active_list(os_tick_t ticks);

#ifdef OS_SOFT_SINGLE_LIST_TIMER
extern os_bool_t k_single_list_timer_need_handle(void);
#endif

#ifdef OS_HARD_SINGLE_LIST_TIMER
extern void k_single_list_timer_tick_proc(void);
#endif

#endif

#ifdef OS_USING_SMP
extern void       k_kernel_spin_lock(void);
extern void       k_kernel_spin_unlock(void);
extern os_ubase_t k_kernel_enter(void);
extern void       k_kernel_exit(os_ubase_t irq_save);
extern void       k_kernel_irq_enable(void);
extern os_ubase_t k_kernel_irq_get(void);
extern void       k_smp_cpu_bootup(int cpu_index);
extern os_ubase_t k_smp_cpu_boot_stat(int cpu_index);
extern void       k_sched_send_ipi(int32_t cpu, uint32_t need_sched_bits);
extern int32_t    k_kernel_lock_owne(void);
extern os_ubase_t k_kernel_lock_owner_catch(void);
extern void       exc_analysis(void);
extern void       k_kernel_exit_sched(os_ubase_t irq_save);

#define OS_KERNEL_ENTER()      irq_save = k_kernel_enter()
#define OS_KERNEL_EXIT()       k_kernel_exit(irq_save)
#define OS_KERNEL_EXIT_SCHED() k_kernel_exit_sched(irq_save)
#define OS_KERNEL_INIT()       os_ubase_t irq_save

#else

extern int32_t g_os_kernel_lock_cnt;

#define OS_KERNEL_INIT() os_ubase_t irq_save

#ifdef OS_USING_KERNEL_LOCK_CHECK
#define OS_KERNEL_ENTER()                                                                                              \
    do                                                                                                                 \
    {                                                                                                                  \
        irq_save = os_irq_lock();                                                                                      \
        k_kernel_enter_check();                                                                                        \
    } while (0)

#define OS_KERNEL_EXIT()                                                                                               \
    do                                                                                                                 \
    {                                                                                                                  \
        k_kernel_exit_check();                                                                                         \
        os_irq_unlock(irq_save);                                                                                       \
    } while (0)

#define OS_KERNEL_LOCK_REF_DEC()                                                                                       \
    do                                                                                                                 \
    {                                                                                                                  \
        g_os_kernel_lock_cnt--;                                                                                        \
    } while (0)

#define OS_KERNEL_LOCK_REF_INC()                                                                                       \
    do                                                                                                                 \
    {                                                                                                                  \
        g_os_kernel_lock_cnt++;                                                                                        \
    } while (0)

#else
#define OS_KERNEL_ENTER()                                                                                              \
    do                                                                                                                 \
    {                                                                                                                  \
        irq_save = os_irq_lock();                                                                                      \
    } while (0)

#define OS_KERNEL_EXIT()                                                                                               \
    do                                                                                                                 \
    {                                                                                                                  \
        os_irq_unlock(irq_save);                                                                                       \
    } while (0)

#define OS_KERNEL_LOCK_REF_DEC()

#define OS_KERNEL_LOCK_REF_INC()

#endif /* OS_USING_KERNEL_LOCK_CHECK */

#define OS_KERNEL_EXIT_SCHED()                                                                                         \
    do                                                                                                                 \
    {                                                                                                                  \
        if ((OS_NULL == g_os_current_task) || (0 != g_os_sched_lock_cnt))                                              \
        {                                                                                                              \
            OS_KERNEL_EXIT();                                                                                          \
        }                                                                                                              \
        else                                                                                                           \
        {                                                                                                              \
            g_os_next_task = g_os_high_task;                                                                           \
                                                                                                                       \
            if (g_os_current_task != g_os_next_task)                                                                   \
            {                                                                                                          \
                OS_KERNEL_LOCK_REF_DEC();                                                                              \
                os_task_switch();                                                                                      \
                OS_KERNEL_LOCK_REF_INC();                                                                              \
            }                                                                                                          \
                                                                                                                       \
            OS_KERNEL_EXIT();                                                                                          \
        }                                                                                                              \
    } while (0)

extern void k_kernel_enter_check(void);
extern void k_kernel_exit_check(void);

#endif /* OS_USING_SMP */

#ifdef __cplusplus
}
#endif

#endif /* __OS_KERNEL_INTERNAL_H__ */
