/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        os_memory.h
 *
 * @brief       Header file for memory management interface.
 *
 * @revision
 * Date         Author          Notes
 * 2020-11-18   OneOS team      First Version
 ***********************************************************************************************************************
 */

#ifndef __OS_MEMORY_H__
#define __OS_MEMORY_H__

#include <oneos_config.h>
#include <os_types.h>
#include <os_sem.h>
#include <os_spinlock.h>
#include <os_dummy.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifdef OS_USING_HEAP

#ifndef OS_USING_SEMAPHORE
#error "Macro OS_USING_SEMAPHORE must be set."
#endif

#if !defined(OS_USING_ALG_FIRSTFIT) && !defined(OS_USING_ALG_BUDDY)
#error "ERROR! At least one memory algorithm must be choose."
#endif

struct os_meminfo
{
    os_size_t mem_total;
    os_size_t mem_used;
    os_size_t mem_maxused;
};
typedef struct os_meminfo os_meminfo_t;

#ifdef OS_USING_HEAP
extern void     os_default_heap_init(void);
extern os_err_t os_default_heap_add(void *start_addr, os_size_t size, enum os_mem_alg alg);
extern void    *os_malloc(os_size_t size);
extern void    *os_aligned_malloc(os_size_t align, os_size_t size);
extern void     os_free(void *ptr);
extern void    *os_realloc(void *ptr, os_size_t size);
extern void    *os_calloc(os_size_t count, os_size_t size);
extern void     os_default_heap_info(os_meminfo_t *info);
extern os_err_t os_default_heap_check(void);
#ifdef OS_USING_MEM_TRACE
extern os_err_t os_default_heap_trace(void);
#endif

#endif /* end of OS_USING_HEAP */
typedef struct dummy_heap os_heap_dummy_t;

typedef os_heap_dummy_t *os_heap_id;

#define OS_HEAP_DEFINE(name) os_heap_dummy_t name

extern os_heap_id os_heap_init(os_heap_dummy_t *heap_cb, const char *name);
extern os_err_t   os_heap_add(os_heap_id heap_id, void *start_addr, os_size_t size, enum os_mem_alg alg);
extern void      *os_heap_alloc(os_heap_id heap_id, os_size_t size);
extern void      *os_heap_aligned_alloc(os_heap_id heap_id, os_size_t align, os_size_t size);
extern void      *os_heap_realloc(os_heap_id heap_id, void *ptr, os_size_t size);
extern void       os_heap_free(os_heap_id heap_id, void *ptr);
extern void       os_heap_info(os_heap_id heap_id, os_meminfo_t *info);
extern os_err_t   os_heap_check(os_heap_id heap_id);
#ifdef OS_USING_MEM_TRACE
extern os_err_t os_heap_trace(os_heap_id heap_id);
#endif

#endif /* end of OS_USING_HEAP */

#ifdef OS_USING_MEM_POOL

#ifdef OS_USING_MEMPOOL_CHECK_TAG
#define OS_MEMPOOL_BLK_HEAD_SIZE 4
#else
#define OS_MEMPOOL_BLK_HEAD_SIZE 0
#endif
#define OS_MEMPOOL_SIZE(block_count, block_size)                                                                       \
    ((OS_ALIGN_UP(block_size + OS_MEMPOOL_BLK_HEAD_SIZE, OS_ALIGN_SIZE)) * block_count)

typedef struct dummy_mempool os_mempool_dummy_t;

typedef os_mempool_dummy_t *os_mempool_id;

#define OS_MEM_POOL_DEFINE(name) os_mempool_dummy_t name

struct os_mpinfo
{
    os_size_t blk_size;      /* Size can be used for users in one memory block. */
    os_size_t blk_total_num; /* Numbers of memory block. */
    os_size_t blk_free_num;  /* Numbers of free memory block. */
};
typedef struct os_mpinfo os_mpinfo_t;

extern os_mempool_id
os_mempool_init(os_mempool_dummy_t *mempool_cb, const char *name, void *start, os_size_t size, os_size_t blk_size);

extern os_err_t os_mempool_deinit(os_mempool_id mempool_id);
extern void    *os_mempool_alloc(os_mempool_id mempool_id, os_tick_t timeout);
extern void     os_mempool_free(os_mempool_id mempool_id, void *mem);
extern void     os_mempool_info(os_mempool_id mempool_id, os_mpinfo_t *info);

#endif /*  end of OS_USING_MEM_POOL */

#ifdef __cplusplus
}
#endif

#endif /* __OS_MEMORY_H__ */
