/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        os_errno.h
 *
 * @brief       Error code macro definition.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-25   OneOS Team      First version.
 ***********************************************************************************************************************
 */
#ifndef __OS_ERRNO_H__
#define __OS_ERRNO_H__

#ifdef __cplusplus
extern "C" {
#endif

#define OS_SUCCESS    0   /* There is no error. */
#define OS_FAILURE    -1  /* A generic error happens. */
#define OS_TIMEOUT    -2  /* Timed out. */
#define OS_FULL       -3  /* The resource is full. */
#define OS_EMPTY      -4  /* The resource is empty. */
#define OS_NOMEM      -5  /* No memory. */
#define OS_NOSYS      -6  /* Function not implemented. */
#define OS_BUSY       -7  /* Device or resource busy. */
#define OS_EIO        -8  /* IO error. */
#define OS_INTR       -9  /* Interrupted system call. */
#define OS_INVAL      -10 /* Invalid argument. */
#define OS_NODEV      -11 /* No such device */
#define OS_EPERM      -12 /* Operation not permitted */
#define OS_EBADF      -13 /* Bad file number */
#define OS_EACCES     -14 /* Permission denied */
#define OS_EFAULT     -15 /* Bad address */
#define OS_EDEADLK    -16 /* Resource deadlock would occur */
#define OS_ENXIO      -17 /* No such device or address */
#define OS_E2BIG      -18 /* Arg list too long */
#define OS_ENOSYS     -19 /* Function not implemented */
#define OS_EAGAIN     -20 /* Try again */
#define OS_ENODATA    -21 /* No data available */
#define OS_EADDRINUSE -22 /* Address already in use */

#ifdef __cplusplus
}
#endif

#endif
