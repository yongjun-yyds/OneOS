/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        os_task.h
 *
 * @brief       Header file for task interface.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-18   OneOS Team      First Version.
 * 2020-11-10   OneOS Team      Refactor header file for task interface.
 ***********************************************************************************************************************
 */

#ifndef __OS_TASK_H__
#define __OS_TASK_H__

#include <oneos_config.h>
#include <os_types.h>
#include <os_stddef.h>
#include <os_list.h>
#include <arch_interrupt.h>
#include <arch_task.h>
#include <os_dummy.h>

#if defined(OS_USING_CPU_MONITOR)
#include <cpu_monitor.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif

#define OS_TASK_STATE_EMPTY   0x0000U
#define OS_TASK_STATE_INIT    0x0001U
#define OS_TASK_STATE_READY   0x0002U
#define OS_TASK_STATE_RUNNING 0x0004U
#define OS_TASK_STATE_SLEEP   0x0008U
#define OS_TASK_STATE_BLOCK   0x0010U
#define OS_TASK_STATE_SUSPEND 0x0020U
#define OS_TASK_STATE_CLOSE   0x8000U
#define OS_TASK_STATE_MASK    0xFFFFU

#define OS_IDLE_TASK_NAME    "idle"
#define OS_RECYCLE_TASK_NAME "recycle"

typedef struct dummy_task os_task_dummy_t;
typedef os_task_dummy_t  *os_task_id;

#ifdef OS_USING_TASK_HOOK
#define OS_TASK_HOOK_CALL(func, argv)                                                                                  \
    do                                                                                                                 \
    {                                                                                                                  \
        if ((func) != OS_NULL)                                                                                         \
        {                                                                                                              \
            func argv;                                                                                                 \
        }                                                                                                              \
    } while (0)

#ifdef OS_USING_SMP
typedef void (*os_task_switch_hook_t)(os_task_id from, os_task_id to, int32_t cpu);
#else
typedef void (*os_task_switch_hook_t)(os_task_id from, os_task_id to);
#endif

extern os_bool_t os_task_switch_hook_add(os_task_switch_hook_t hook);

extern os_bool_t os_task_switch_hook_delete(os_task_switch_hook_t hook);

#else
#define OS_TASK_HOOK_CALL(func, argv)
#endif

#define OS_TASK_DEFINE(name) os_task_dummy_t name

#define OS_TASK_STACK_DEFINE(name, size)                                                                               \
    OS_ALIGN(OS_ARCH_STACK_ALIGN_SIZE) uint8_t (name)[OS_ALIGN_UP(size, OS_ARCH_STACK_ALIGN_SIZE)]
#define OS_TASK_STACK_BEGIN_ADDR(name) (void *)&((name)[0])
#define OS_TASK_STACK_SIZE(name)       sizeof(name)

extern os_task_id os_task_create(os_task_dummy_t *task_cb,
                                 void            *stack_begin,
                                 uint32_t         stack_size,
                                 const char      *name,
                                 void (*entry)(void *arg),
                                 void   *arg,
                                 uint8_t priority);

extern os_err_t os_task_destroy(os_task_id tid);

extern os_err_t os_task_set_cleanup_callback(os_task_id tid, void (*cleanup)(void *user_data));
extern os_err_t os_task_set_user_data(os_task_id tid, void *user_data);
extern void    *os_task_get_user_data(os_task_id tid);

#ifdef OS_USING_SMP
extern os_err_t os_task_set_cpu_affinity(os_task_id tid, int32_t new_affinity);
extern os_err_t os_task_get_cpu_affinity(os_task_id tid, int32_t *affinity);
#endif

extern os_err_t os_task_startup(os_task_id tid);
extern os_err_t os_task_suspend(os_task_id tid);
extern os_err_t os_task_resume(os_task_id tid);
extern os_err_t os_task_yield(void);

extern os_err_t os_task_set_time_slice(os_task_id tid, os_tick_t new_time_slice);
extern os_err_t os_task_get_time_slice(os_task_id tid, os_tick_t *time_slice);
extern os_err_t os_task_get_remaining_time_slice(os_task_id tid, os_tick_t *time_slice);
extern os_err_t os_task_set_priority(os_task_id tid, uint8_t new_priority);
extern os_err_t os_task_get_priority(os_task_id tid, uint8_t *priority);

extern os_task_id  os_get_current_task(void);
extern os_task_id  os_task_get_id(const char *name);
extern os_bool_t   os_task_check_exist(os_task_id tid);
extern const char *os_task_get_name(os_task_id tid);
extern os_err_t    os_task_get_state(os_task_id tid, uint16_t *status);
extern void       *os_task_get_stack_top(os_task_id tid);
extern void       *os_task_get_stack_begin(os_task_id tid);
extern void       *os_task_get_stack_end(os_task_id tid);
extern uint32_t    os_task_get_total_count(void);

extern os_err_t os_task_tsleep(os_tick_t tick);
extern os_err_t os_task_msleep(uint32_t ms);

extern void      os_schedule_lock(void);
extern void      os_schedule_unlock(void);
extern os_bool_t os_is_schedule_locked(void);

extern os_err_t *os_errno(void);

extern os_spinlock_t  *k_task_resource_list_lock_get(void);
extern os_list_node_t *k_task_resource_list_head_get(void);

#ifdef OS_TASK_SWITCH_NOTIFY
extern void os_task_switch_notify(void);
#endif

#define OS_SET_ERRNO(err_code) *os_errno() = (err_code)
#define OS_GET_ERRNO()         *os_errno()

#ifdef __cplusplus
}
#endif

#endif /* __OS_TASK_H__ */
