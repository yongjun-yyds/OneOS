/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        os_mutes.h
 *
 * @brief       Header file for mutex interface.
 *
 * @revision
 * Date         Author          Notes
 * 2020-12-28   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __OS_MUTEX_H__
#define __OS_MUTEX_H__

#include <oneos_config.h>
#include <os_errno.h>
#include <os_types.h>
#include <os_list.h>
#include <os_stddef.h>
#include <os_task.h>
#include <os_spinlock.h>
#include <os_dummy.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifdef OS_USING_MUTEX

#define OS_MUTEX_WAKE_TYPE_PRIO 0x55
#define OS_MUTEX_WAKE_TYPE_FIFO 0xAA

typedef struct dummy_mutex os_mutex_dummy_t;
typedef os_mutex_dummy_t  *os_mutex_id;

/* Here, actually, it is an array of mutex control structure size.
 * The purpose is to serve as a mutex control block for
 * static create of mutex.
 */
#define OS_MUTEX_DEFINE(name) os_mutex_dummy_t name

extern os_mutex_id os_mutex_create(os_mutex_dummy_t *mutex_cb, const char *name, os_bool_t recursive);

extern os_err_t os_mutex_destroy(os_mutex_id mutex_id);

extern os_err_t os_mutex_lock(os_mutex_id mutex_id, os_tick_t timeout);
extern os_err_t os_mutex_unlock(os_mutex_id mutex_id);
extern os_err_t os_mutex_recursive_lock(os_mutex_id mutex_id, os_tick_t timeout);
extern os_err_t os_mutex_recursive_unlock(os_mutex_id mutex_id);

extern os_err_t  os_mutex_set_wake_type(os_mutex_id mutex_id, uint8_t wake_type);
extern os_err_t  os_mutex_get_owner(os_mutex_id mutex_id, os_task_id *tid);
extern os_bool_t os_mutex_check_exist(os_mutex_id mutex_id);
extern os_err_t  os_mutex_is_recursive(os_mutex_id mutex_id, os_bool_t *is_recursive);

#endif /* OS_USING_MUTEX */

#ifdef __cplusplus
}
#endif

#endif /* __OS_MUTEX_H__ */
