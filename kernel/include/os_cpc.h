/**
 ***********************************************************************************************************************
 * Copyright (c) 2022, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        os_cpc.h
 *
 * @brief       cross processor call header file.
 *
 * @revision
 * Date         Author          Notes
 * 2022-01-12   OneOS Team      First version
 ***********************************************************************************************************************
 */

#ifndef __OS_CPC_H__
#define __OS_CPC_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <oneos_config.h>
#include <os_types.h>

#ifdef OS_USING_SMP

/* clang-format off */    
#define OS_CPC_SYNC        1  /* The calling core waits until the called core executes the CPC call function. */
    
#define OS_CPC_ASYNC       2  /* The called core first responds to the calling core, 
                                     and then executes the CPC call function. */
                                     
#define OS_CPC_SYNC_MANUAL 3  /* The synchronization between the calling core and the called core 
                                     is handled by the CPC call function. This synchronization can be 
                                     done by calling the os_cpc_sync() function. */
    
typedef os_err_t (*os_cpc_func_t) (void *);  /* cpc function pointer type. */

/* clang-format on */
extern os_err_t os_cpc_init(void);

extern os_err_t os_cpc_call(int32_t cpu, os_cpc_func_t func, void *arg, int32_t flag);

extern os_err_t os_cpc_broadcast(uint32_t cpuset, os_cpc_func_t func, void *arg, int32_t flag);

extern void os_cpc_sync(os_err_t ret);

#endif /* OS_USING_SMP */

#ifdef __cplusplus
}
#endif

#endif /* __OS_CPC_H__ */
