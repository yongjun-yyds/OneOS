/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        mem_monitor.h
 *
 * @brief       This file implements memory monitor.
 *
 * @revision
 * Date         Author          Notes
 * 2021-12-03   OneOS team      First Version
 ***********************************************************************************************************************
 */
#ifndef __MEM_MONITOR_H__
#define __MEM_MONITOR_H__

#include <os_list.h>
#include <os_task.h>

#ifdef __cplusplus
extern "C" {
#endif

#define METHOD_CRC 0

typedef struct _mem_monitor
{
    os_ubase_t addr;                                                // the ram address to be check
    os_ubase_t len;                                                 // the ram length
    uint8_t    method;                                              // CRC, RC4 ...
    os_ubase_t prev_hash_sig;                                       // prev hash signature
    os_ubase_t hash_sig;                                            // current hash signature
    os_ubase_t (*hash_sig_calc)(uint8_t *pdata, os_ubase_t len);    // crc calc, RC4 calc, MD5Sum ...
    os_err_t (*handle)(struct _mem_monitor *mm);
    os_list_node_t *task;    // those task to calc check value
} mem_monitor_t;

typedef os_ubase_t (*hash_sig_calc_t)(uint8_t *, os_ubase_t);
typedef os_err_t (*monitor_handle_t)(struct _mem_monitor *mm);

extern os_err_t mem_monitor_begin(mem_monitor_t *mm);
extern os_err_t mem_monitor_end(mem_monitor_t *mm);
#ifdef OS_USING_SMP
extern void mem_monitor_task_switch_hook(os_task_id from, os_task_id to, int32_t cpu);
#else
extern void mem_monitor_task_switch_hook(os_task_id from, os_task_id to);
#endif
extern os_err_t   mem_monitor_def_handle(mem_monitor_t *mm);
extern os_ubase_t hash_sig_crc16_calc(uint8_t *pdata, os_ubase_t len);
extern os_err_t   mem_monitor_init(mem_monitor_t   *mm,
                                   os_ubase_t       addr,
                                   os_ubase_t       len,
                                   os_ubase_t       method,
                                   hash_sig_calc_t  func,
                                   monitor_handle_t handle);
os_err_t          mem_monitor_deinit(mem_monitor_t *mm);
os_err_t          mem_monitor_auto_init(os_ubase_t       addr,
                                        os_ubase_t       len,
                                        os_ubase_t       method,
                                        hash_sig_calc_t  func,
                                        monitor_handle_t handle);
os_err_t          mem_monitor_auto_deinit(void);

#ifdef __cplusplus
}
#endif
#endif
