/**
 ***********************************************************************************************************************
 * Copyright (c) 2021, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        cpu_monitor.h
 *
 * @brief       Header file for cpu monitor interface.
 *
 * @revision
 * Date         Author          Notes
 * 2021-11-29   OneOS Team      First Version.
 * 2024-11-20   weiyongjun      add irq monitor when task monitoring
 ***********************************************************************************************************************
 */

#ifndef __OS_CPU_MONITOR_H__
#define __OS_CPU_MONITOR_H__

#include <oneos_config.h>
#include <os_types.h>

#ifdef OS_USING_CPU_MONITOR

#ifdef __cplusplus
extern "C" {
#endif

struct cpu_use_info
{
    uint64_t start_time; /* Record the time of switching to the task.*/
    uint64_t total_time; /* Accumulation of running time. */
};

typedef struct cpu_use_info cpu_use_info_t;

#ifdef OS_USING_IRQ_MONITOR
extern void cpu_usage_irq_monitor_start(void);
extern void cpu_usage_irq_monitor_stop(void);
#endif

#ifdef __cplusplus
}
#endif

#endif /* OS_USING_CPU_MONITOR */

#endif /* __OS_CPU_MONITOR_H__ */
