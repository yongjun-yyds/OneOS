/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        os_sem.h
 *
 * @brief       Header file for semaphore interface.
 *
 * @revision
 * Date         Author          Notes
 * 2020-12-28   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __OS_SEM_H__
#define __OS_SEM_H__

#include <oneos_config.h>
#include <os_types.h>
#include <os_list.h>
#include <os_spinlock.h>
#include <os_dummy.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifdef OS_USING_SEMAPHORE

#define OS_SEM_WAKE_TYPE_PRIO 0x55
#define OS_SEM_WAKE_TYPE_FIFO 0xAA

#define OS_SEM_MAX_VALUE OS_UINT32_MAX

typedef struct dummy_semaphore os_semaphore_dummy_t;
typedef os_semaphore_dummy_t  *os_semaphore_id;

/*
 * Here, actually, it is an array of semaphore control structure size.
 * The purpose is to serve as a semaphore control block for
 * static create of semaphore.
 */
#define OS_SEMAPHORE_DEFINE(name) os_semaphore_dummy_t name

extern os_semaphore_id
os_semaphore_create(os_semaphore_dummy_t *semaphore_cb, const char *name, uint32_t value, uint32_t max_value);

extern os_err_t os_semaphore_destroy(os_semaphore_id semaphore_id);

extern os_err_t os_semaphore_wait(os_semaphore_id semaphore_id, os_tick_t timeout);
extern os_err_t os_semaphore_post(os_semaphore_id semaphore_id);

extern os_err_t    os_semaphore_set_wake_type(os_semaphore_id semaphore_id, uint8_t wake_type);
extern os_err_t    os_semaphore_get_value(os_semaphore_id semaphore_id, uint32_t *count);
extern os_err_t    os_semaphore_get_max_value(os_semaphore_id semaphore_id, uint32_t *max_count);
extern os_bool_t   os_semaphore_is_exist(os_semaphore_id semaphore_id);
extern const char *os_semaphore_get_name(os_semaphore_id semaphore_id);

#endif /* OS_USING_SEMAPHORE */

#ifdef __cplusplus
}
#endif

#endif /* __OS_SEM_H__ */
