/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        os_workqueue.h
 *
 * @brief       Header file for workqueue interface.
 *
 * @revision
 * Date         Author          Notes
 * 2020-04-06   OneOS Team      First version.
 ***********************************************************************************************************************
 */
#ifndef __OS_WORKQUEUE_H__
#define __OS_WORKQUEUE_H__

#include <oneos_config.h>
#include <os_stddef.h>
#include <os_list.h>
#include <os_timer.h>
#include <os_task.h>
#include <os_sem.h>
#include <os_spinlock.h>
#include <os_dummy.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifdef OS_USING_WORKQUEUE

#define OS_WORK_STAGE_IDLE    0x0
#define OS_WORK_STAGE_DELAY   0x1
#define OS_WORK_STAGE_PENDING 0x2

typedef struct dummy_workqueue os_workqueue_dummy_t;
typedef os_workqueue_dummy_t  *os_workqueue_id;

/*
 * Here, actually, it is an array of workqueue control structure size.
 * The purpose is to serve as a workqueue control block for
 * static create of workqueue.
 */
#define OS_WORKQUEUE_DEFINE(name) os_workqueue_dummy_t name
typedef struct dummy_work os_work_dummy_t;
typedef os_work_dummy_t  *os_work_id;

/*
 * Here, actually, it is an array of work control structure size.
 * The purpose is to serve as a work control block for
 * static create of workqueue.
 */
#define OS_WORK_DEFINE(name) os_work_dummy_t name

extern os_workqueue_id os_workqueue_create(os_workqueue_dummy_t *queue_cb,
                                           const char           *name,
                                           void                 *stack_begin,
                                           uint32_t              stack_size,
                                           uint8_t               priority,
                                           int32_t               cpu_index);

extern os_work_id os_work_init(os_work_dummy_t *work, void (*func)(void *data), void *data);
extern void       os_work_deinit(os_work_id work_id);

extern os_err_t os_work_run(os_workqueue_id queue_id, os_work_id work_id, os_tick_t delay_time);
extern os_err_t os_work_stop(os_work_id work_id, os_bool_t sync);

#ifdef OS_USING_SYSTEM_WORKQUEUE
extern os_err_t os_sys_work_run(os_work_id work_id, os_tick_t delay_time);
#endif /* OS_USING_SYSTEM_WORKQUEUE */

#endif /* OS_USING_WORKQUEUE */

#ifdef __cplusplus
}
#endif

#endif /* __OS_WORKQUEUE_H__ */
