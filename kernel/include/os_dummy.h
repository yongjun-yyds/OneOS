/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        os_dummy.h
 *
 * @brief       Provides some macro definitions and function declarations, only use for user.
 *
 * @revision
 * Date         Author          Notes
 * 2020-03-03   OneOS Team      First version.
 ***********************************************************************************************************************
 */
#ifndef __OS_DUMMY_H__
#define __OS_DUMMY_H__

#include <oneos_config.h>
#include <os_types.h>
#include <os_stddef.h>
#include <os_list.h>
#include <os_spinlock.h>

#if defined(OS_USING_CPU_MONITOR)
#include <cpu_monitor.h>
#endif

struct dummy_task
{
    /* begin: The order, position and content cannot be changed */
    void *stack_top_dummy;   /* Point to SP */
    void *stack_begin_dummy; /* The begin address of task stack */
    void *stack_end_dummy;   /* The end address of task stack */

    uint16_t state; /* Task state, refer to OS_TASK_STATE_INIT, OS_TASK_STATE_READY ... */
    /* end:   The order, position and content cannot be changed */

    uint8_t current_priority_dummy; /* Current priority. */
    uint8_t backup_priority_dummy;  /* Backup priority. */

    os_err_t err_code_dummy;      /* Error code. */
    os_err_t switch_retval_dummy; /* Task switch return value, defined in os_errno.h*/

    uint8_t object_inited_dummy;     /* If task object is inited, value is OS_KOBJ_INITED */
    uint8_t object_alloc_type_dummy; /* Indicates whether memory is allocated dynamically or statically,
                                     value is OS_KOBJ_ALLOC_TYPE_STATIC or OS_KOBJ_ALLOC_TYPE_DYNAMIC */
    uint8_t pad[2];

    os_list_node_t resource_node_dummy; /* Node in resource list */
    os_list_node_t task_node_dummy;     /* Node in ready queue or blocking queue */

    os_list_node_t tick_node_dummy;     /* Node in tick queue */
    os_tick_t      tick_timeout_dummy;  /* Timeout */
    os_tick_t      tick_absolute_dummy; /* Absolute time of timeout */

    os_tick_t time_slice_dummy;           /* Task's slice time (unit: tick). */
    os_tick_t remaining_time_slice_dummy; /* Task's remaining slice time (unit: tick). */

    os_list_node_t *block_list_head_dummy; /* Where the task is blocked at */
    os_bool_t       is_wake_prio_dummy;    /* The wake type to task at block_list_head, according to priority or not */

#if defined(OS_USING_EVENT)
    uint32_t event_set_dummy;
    uint32_t event_option_dummy;
#endif

#if defined(OS_USING_MUTEX)
    os_list_node_t hold_mutex_list_head_dummy;
#endif

#if defined(OS_USING_SMP)
    int32_t cpu_index_dummy; /* The number of the cpu that the task is running on.
                             -1 means the task is not running. */
    int32_t cpu_affinity_dummy;

    os_list_node_t join_node_dummy; /* Node in join queue */
#endif

    os_ubase_t swap_data_dummy;

    void (*cleanup_dummy)(void *user_data); /* The cleanup function is provided by the user */
    void *user_data_dummy;                  /* Private user data beyond this task. */

    char name_dummy[OS_NAME_MAX + 1]; /* Task name */

#if defined(OS_USING_CPU_MONITOR)
    cpu_use_info_t usage_info_dummy;
#endif
};

typedef struct dummy_task dummy_task_t;

struct dummy_mutex
{
    os_list_node_t task_list_head_dummy; /* Block task list head */
    os_list_node_t resource_node_dummy;  /* Node in resource list */
    os_list_node_t hold_node_dummy;

    dummy_task_t *owner_dummy; /* Mutex owner */

    uint32_t  lock_count_dummy;   /* Current lock count */
    os_bool_t is_recursive_dummy; /* Support recursive call. */

    uint8_t object_inited_dummy;     /* If mutex object is inited, value is OS_KOBJ_INITED */
    uint8_t object_alloc_type_dummy; /* Indicates whether memory is allocated dynamically or statically,
                                  value is OS_ALLOC_TYPE_STATIC or OS_KOBJ_ALLOC_TYPE_DYNAMIC */
    uint8_t wake_type_dummy;         /* The type to wake up blocking tasks, value is OS_MUTEX_WAKE_TYPE_PRIO
                                  or OS_MUTEX_WAKE_TYPE_FIFO */
    uint8_t original_priority_dummy;

    char name_dummy[OS_NAME_MAX + 1]; /* Mutex name */

    os_spinlock_t lock_dummy; /* Spin lock used under SMP */
};
typedef struct dummy_mutex dummy_mutex_t;

struct dummy_mailbox
{
    void *mail_pool_dummy;       /* The address that doesn't do alignment for mail pool */
    void *mail_pool_align_dummy; /* Aligned address of mail pool. */

    os_list_node_t send_task_list_head_dummy; /* Sender tasks blocked on this mailbox. */
    os_list_node_t recv_task_list_head_dummy; /* Receiver tasks blocked on this mailbox. */

    os_list_node_t resource_node_dummy; /* Node in resource list */

    uint16_t capacity_dummy;    /* Max number of mails for this mailbox. */
    uint16_t used_mails_dummy;  /* Numbers of mails into mailbox. */
    uint16_t read_index_dummy;  /* Read index of mail pool. */
    uint16_t write_index_dummy; /* Write index of mail pool. */

    uint8_t object_inited_dummy;     /* Indicates whether object is inited or deinited, value is
                                  OS_KOBJ_INITED or OS_KOBJ_DEINITED */
    uint8_t object_alloc_type_dummy; /* Indicates whether object is allocated dynamically or statically,
                                  value is OS_ALLOC_TYPE_STATIC or OS_KOBJ_ALLOC_TYPE_DYNAMIC */
    uint8_t wake_type_dummy;         /* The type to wake up blocking tasks, value is OS_MB_WAKE_TYPE_PRIO
                                  or OS_MB_WAKE_TYPE_FIFO */

    char name_dummy[OS_NAME_MAX + 1]; /* Mailbox name. */

    os_spinlock_t lock_dummy; /* Spin lock used under SMP */
};
typedef struct dummy_mailbox dummy_mailbox_t;

struct dummy_semaphore
{
    os_list_node_t task_list_head_dummy; /* Block task list head  */
    os_list_node_t resource_node_dummy;  /* Node in resource list */

    uint32_t count_dummy;             /* Current count */
    uint32_t max_count_dummy;         /* Semaphore support maximum value */
    uint8_t  object_inited_dummy;     /* If semaphore object is inited, value is OS_KOBJ_INITED */
    uint8_t  object_alloc_type_dummy; /* Indicates whether memory is allocated dynamically or statically,
                                   value is OS_ALLOC_TYPE_STATIC or OS_KOBJ_ALLOC_TYPE_DYNAMIC */
    uint8_t wake_type_dummy;          /* The type to wake up blocking tasks, value is OS_SEM_WAKE_TYPE_PRIO
                                   or OS_SEM_WAKE_TYPE_FIFO */

    char name_dummy[OS_NAME_MAX + 1]; /* Semaphore name */

    os_spinlock_t lock_dummy; /* Spin lock used under SMP */
};
typedef struct dummy_semaphore dummy_semaphore_t;

struct dummy_event
{
    os_list_node_t task_list_head_dummy; /* Block task list head */
    os_list_node_t resource_node_dummy;  /* Node in resource list */

    uint32_t set_dummy;               /* Event set. */
    uint8_t  object_inited_dummy;     /* If mutex object is inited, value is OS_KOBJ_INITED */
    uint8_t  object_alloc_type_dummy; /* Indicates whether memory is allocated dynamically or statically,
                                   value is OS_ALLOC_TYPE_STATIC or OS_KOBJ_ALLOC_TYPE_DYNAMIC */
    uint8_t wake_type_dummy;          /* The type to wake up blocking tasks, value is OS_EVENT_WAKE_TYPE_PRIO
                                   or OS_EVENT_WAKE_TYPE_FIFO */
    char name_dummy[OS_NAME_MAX + 1]; /* Mutex name */

    os_spinlock_t lock_dummy; /* Spin lock used under SMP */
};
typedef struct dummy_event dummy_event_t;

struct dummy_mq_msg
{
    struct dummy_mq_msg *next_dummy;    /* Point to the next message */
    os_size_t            msg_len_dummy; /* message size */
};
typedef struct dummy_mq_msg dummy_mq_msg_t;
typedef struct dummy_mq_msg dummy_mq_msg_hdr_t;

struct dummy_msgqueue
{
    void           *msg_pool_dummy;       /* Point to message pool. */
    dummy_mq_msg_t *msg_queue_head_dummy; /* Point to first entry of message queue. */
    dummy_mq_msg_t *msg_queue_tail_dummy; /* Point to last entry of message queue. */
    dummy_mq_msg_t *msg_queue_free_dummy; /* Point to first entry of free message resource. */

    os_list_node_t send_task_list_head_dummy; /* Sender tasks blocked on this messages queue. */
    os_list_node_t recv_task_list_head_dummy; /* Receiver tasks blocked on this messages queue. */

    os_list_node_t resource_node_dummy; /* Node in resource list */

    os_size_t max_msg_size_dummy; /* Max message size of each message. */
    uint16_t  queue_depth_dummy;  /* Maximum number of messages that can be put into message queue. */
    uint16_t  used_msgs_dummy;    /* Number of messages queued. */

    uint8_t object_inited_dummy;     /* Indicates whether object is inited or deinited, value is
                                  OS_KOBJ_INITED or OS_KOBJ_DEINITED */
    uint8_t object_alloc_type_dummy; /* Indicates whether object is allocated dynamically or statically,
                                  value is OS_ALLOC_TYPE_STATIC or OS_KOBJ_ALLOC_TYPE_DYNAMIC */
    uint8_t wake_type_dummy;         /* The type to wake up blocking tasks, value is OS_MQ_WAKE_TYPE_PRIO
                                  or OS_MQ_WAKE_TYPE_FIFO */

    char name_dummy[OS_NAME_MAX + 1]; /* Message queue name. */

    os_spinlock_t lock_dummy; /* Spin lock used under SMP */
};

typedef struct dummy_msgqueue dummy_msgqueue_t;

struct dummy_timer_active_node
{
#ifdef OS_USING_HASH_BUCKET_TIMER
    os_list_node_t active_list_dummy;
    uint8_t        info_dummy;
#endif

/* used for compensate periodic timer, if the timeout cb do than init_tick, can't compensate it */
#if OS_TIMER_PERIODIC_COMPENSATE
    os_tick_t timeout_tick_dummy;
#endif
    uint8_t flag_dummy;
};

typedef struct dummy_timer_active_node dummy_timer_active_node_t;

struct dummy_timer
{
    void (*timeout_func_dummy)(void *timeout_param); /* Timeout function. */
    void     *parameter_dummy;                       /* Timeout function's parameter. */
    os_tick_t init_ticks_dummy;                      /* Timer timeout tick. */
    os_tick_t round_ticks_dummy;                     /* Timeout tick remaining. */
#ifdef OS_USING_HASH_BUCKET_TIMER
    uint32_t index_dummy;
#endif
    os_list_node_t            list_dummy;
    dummy_timer_active_node_t active_node_dummy;
    char                      name_dummy[OS_NAME_MAX + 1];
};

typedef struct dummy_timer dummy_timer_t;

struct dummy_mempool
{
    void          *start_addr_dummy;            /* Memory pool start. */
    void          *free_list_dummy;             /* Avaliable memory blocks list. */
    os_size_t      size_dummy;                  /* Size of memory pool. */
    os_size_t      blk_size_dummy;              /* Size of memory blocks, maybe not the size for users. */
    os_size_t      blk_total_num_dummy;         /* Numbers of memory block. */
    os_size_t      blk_free_num_dummy;          /* Numbers of free memory block. */
    os_list_node_t task_list_head_dummy;        /* Task suspend on this memory pool. */
    os_list_node_t resource_node_dummy;         /* Node in resource list */
    uint8_t        object_alloc_type_dummy;     /* Indicates whether object is allocated */
    uint8_t        object_inited_dummy;         /* Whether inited. */
    char           name_dummy[OS_NAME_MAX + 1]; /* mempool name. */

    os_spinlock_t lock_dummy; /* Spin lock used under SMP */
};
typedef struct dummy_mempool dummy_mempool_t;

enum os_mem_alg
{
#ifdef OS_USING_ALG_FIRSTFIT
    OS_MEM_ALG_FIRSTFIT,
#endif
#ifdef OS_USING_ALG_BUDDY
    OS_MEM_ALG_BUDDY,
#endif
    OS_MEM_ALG_DEFAULT /*Use default memory algorithm */
};

struct dummy_heap_mem
{
    void *(*k_alloc_dummy)(struct dummy_heap_mem *h_mem, os_size_t size);
    void *(*k_aligned_alloc_dummy)(struct dummy_heap_mem *h_mem, os_size_t align, os_size_t size);
    void (*k_free_dummy)(struct dummy_heap_mem *h_mem, void *mem);
    void *(*k_realloc_dummy)(struct dummy_heap_mem *h_mem, void *mem, os_size_t newsize);
    void (*k_deinit_dummy)(struct dummy_heap_mem *h_mem);
    os_err_t (*k_mem_check_dummy)(struct dummy_heap_mem *h_mem);
#ifdef OS_USING_MEM_TRACE
    os_err_t (*k_mem_trace_dummy)(struct dummy_heap_mem *h_mem);
#endif
    os_size_t (*k_ptr_to_size_dummy)(struct dummy_heap_mem *h_mem, void *mem);
    void                  *header_dummy; /* Pointer to current memory's start address. */
    struct dummy_heap_mem *next_dummy;   /* Pointer to next memory's heap_mem object. */
    os_size_t              mem_total_dummy;
    os_size_t              mem_used_dummy;
    os_size_t              mem_maxused_dummy;
    dummy_semaphore_t      sem_dummy;
    enum os_mem_alg        alg_dummy;
};

struct dummy_heap
{
    struct dummy_heap_mem *h_mem_dummy;
    uint8_t                object_inited_dummy;
    char                   name_dummy[OS_NAME_MAX + 1];
    os_list_node_t         resource_node_dummy; /* Node in resource list */
};
typedef struct dummy_heap dummy_heap_t;

struct dummy_workqueue;

struct dummy_work
{
    os_list_node_t work_node_dummy;

    void (*work_func_dummy)(void *data); /* Callback function for work. */
    void         *data_dummy;            /* Private data for callback function. */
    dummy_timer_t work_timer_dummy;

    struct dummy_workqueue *volatile workqueue_dummy; /* Workqueue used to execute work. */

    uint8_t flag_dummy;
    uint8_t object_inited_dummy; /* If os_work is inited, value is OS_KOBJ_INITED */
};
typedef struct dummy_work dummy_work_t;

struct dummy_workqueue
{
    os_list_node_t work_list_head_dummy;
    dummy_work_t  *work_current_dummy; /* Work in progress on workqueue. */
    dummy_task_t   queue_task_dummy;

    os_spinlock_t     lock_dummy;   /* Spin lock. */
    dummy_semaphore_t sem_id_dummy; /* Semaphore for synchronization. */

    uint8_t object_inited_dummy; /* If os_workqueue is inited, value is OS_KOBJ_INITED */
};
typedef struct dummy_workqueue dummy_workqueue_t;
#endif
