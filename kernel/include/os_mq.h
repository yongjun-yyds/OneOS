/**
 ***********************************************************************************************************************
 * Copyright (c) 2020-2021, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        os_mq.h
 *
 * @brief       Header file for message queue interface.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-14   OneOS Team      First Version
 * 2021-01-08   OneOS team      Refactor header file for message queue interface interface.
 ***********************************************************************************************************************
 */

#ifndef __OS_MQ_H__
#define __OS_MQ_H__

#include <oneos_config.h>
#include <os_types.h>
#include <os_list.h>
#include <os_dummy.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifdef OS_USING_MESSAGEQUEUE

#define OS_MQ_WAKE_TYPE_PRIO 0x55
#define OS_MQ_WAKE_TYPE_FIFO 0xAA

typedef struct dummy_msgqueue os_msgqueue_dummy_t;
typedef os_msgqueue_dummy_t  *os_msgqueue_id;

/*
 * Here, actually, it is an array of message queue control structure size.
 * The purpose is to serve as a message queue control block for
 * static create of message queue.
 */
#define OS_MSGQUEUE_DEFINE(name) os_msgqueue_dummy_t name

/*
 * OS_MQ_CALC_POOL_SIZE is used to calc the pool size
 * OS_MQ_POOL_DEFINE is used to define message queue's memory pool and the address is aligned.
 * OS_MQ_POOL_ADDR is used to get address of message queue's memory pool.
 * OS_MQ_POOL_SIZE is used to get size of message queue's memory pool.
 */
#define OS_MSGQUEUE_CALC_POOL_SIZE(msg_count, msg_size)                                                                \
    (sizeof(dummy_mq_msg_hdr_t) + OS_ALIGN_UP((msg_size), OS_ALIGN_SIZE)) * (msg_count)
#define OS_MSGQUEUE_POOL_DEFINE(name, msg_count, msg_size)                                                             \
    OS_ALIGN(OS_ALIGN_SIZE)                                                                                            \
    uint8_t pool_##name[(sizeof(dummy_mq_msg_hdr_t) + OS_ALIGN_UP(msg_size, OS_ALIGN_SIZE)) * msg_count]
#define OS_MSGQUEUE_POOL_ADDR(name) ((void *)pool_##name)
#define OS_MSGQUEUE_POOL_SIZE(name) sizeof(pool_##name)

#ifdef OS_USING_HEAP
extern os_msgqueue_id os_msgqueue_create_dynamic(const char *name, os_size_t msg_size, os_size_t max_msgs);
#endif

extern os_msgqueue_id os_msgqueue_create_static(os_msgqueue_dummy_t *msgqueue,
                                                const char          *name,
                                                void                *msg_pool,
                                                os_size_t            msg_pool_size,
                                                os_size_t            msg_size);

extern os_err_t os_msgqueue_destroy(os_msgqueue_id msgqueue_id);

extern os_err_t
os_msgqueue_send(os_msgqueue_id msgqueue_id, const void *buffer, os_size_t buff_size, os_tick_t timeout);

extern os_err_t
os_msgqueue_send_urgent(os_msgqueue_id msgqueue_id, const void *buffer, os_size_t buff_size, os_tick_t timeout);

extern os_err_t os_msgqueue_recv(os_msgqueue_id msgqueue_id,
                                 void          *buffer,
                                 os_size_t      buff_size,
                                 os_tick_t      timeout,
                                 os_size_t     *recv_msg_size);

extern os_err_t    os_msgqueue_set_wake_type(os_msgqueue_id msgqueue_id, uint8_t wake_type);
extern os_err_t    os_msgqueue_reset(os_msgqueue_id msgqueue_id);
extern os_err_t    os_msgqueue_is_empty(os_msgqueue_id msgqueue_id, os_bool_t *empty_flag);
extern os_err_t    os_msgqueue_is_full(os_msgqueue_id msgqueue_id, os_bool_t *full_flag);
extern os_err_t    os_msgqueue_get_queue_depth(os_msgqueue_id msgqueue_id, uint16_t *msgqueue_depth);
extern os_err_t    os_msgqueue_get_max_msg_size(os_msgqueue_id msgqueue_id, os_size_t *max_msg_size);
extern os_err_t    os_msgqueue_get_used_msgs(os_msgqueue_id msgqueue_id, uint16_t *used_msgs);
extern os_err_t    os_msgqueue_get_unused_msgs(os_msgqueue_id msgqueue_id, uint16_t *unused_msgs);
extern const char *os_msgqueue_get_name(os_msgqueue_id msgqueue_id);

#endif /* OS_USING_MESSAGEQUEUE */

#ifdef __cplusplus
}
#endif

#endif /* __OS_MQ_H__ */
