/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        os_ipc_trace.h
 *
 * @brief       Provide ipc trace
 *
 * @revision
 * Date         Author          Notes
 * 2021-12-31   OneOS Team      First version
 ***********************************************************************************************************************
 */
#ifndef __OS_IPC_TRACE_H__
#define __OS_IPC_TRACE_H__

#include <oneos_config.h>
#include <os_types.h>
#include <os_task.h>
#include <ring_buff.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifdef OS_USING_IPC_TRACE

#ifndef OS_IPC_TRACE_ITEM_NUM
#define OS_IPC_TRACE_ITEM_NUM 10
#endif

struct os_ipc_trace_item
{
    os_ubase_t desc;
    os_task_id task;
    os_ubase_t irq;
};
typedef struct os_ipc_trace_item os_ipc_trace_item_t;

struct os_ipc_trace_rec
{
    os_ipc_trace_item_t record[OS_IPC_TRACE_ITEM_NUM];
    os_ubase_t          total_cnt;
    os_ubase_t          valid_cnt;
    rb_ring_buff_t      rb;
};
typedef struct os_ipc_trace_rec os_ipc_trace_rec_t;

struct os_ipc_trace
{
    os_ubase_t         ipc_obj;
    os_ipc_trace_rec_t trace;
};
typedef struct os_ipc_trace os_ipc_trace_t;

os_err_t ipc_trace_rec_enqueue(os_ipc_trace_t *ipct, os_ipc_trace_item_t *trace_data);
os_err_t ipc_trace_rec_dequeue(os_ipc_trace_t *ipct, os_ipc_trace_item_t *trace_data);
os_err_t ipc_trace_rec_init(os_ipc_trace_t *ipct);
os_err_t ipc_trace_rec_add_kobj(os_ipc_trace_t *ipct, os_ubase_t ipc_obj);
os_err_t ipc_trace_rec_del_kobj(os_ipc_trace_t *ipct);
os_err_t ipc_trace_hook(os_ipc_trace_t *ipct, os_ubase_t ipc_obj, os_ipc_trace_item_t *trace_data);

#endif

#ifdef __cplusplus
}
#endif

#endif
