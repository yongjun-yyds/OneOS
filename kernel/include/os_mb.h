/**
 ***********************************************************************************************************************
 * Copyright (c) 2021, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        os_mb.h
 *
 * @brief       Header file for mailbox interface.
 *
 * @revision
 * Date         Author          Notes
 * 2021-01-08   OneOS team      First version.
 ***********************************************************************************************************************
 */

#ifndef __OS_MB_H__
#define __OS_MB_H__

#include <oneos_config.h>
#include <os_types.h>
#include <os_list.h>
#include <os_spinlock.h>
#include <os_dummy.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifdef OS_USING_MAILBOX

#define OS_MB_WAKE_TYPE_PRIO 0x55
#define OS_MB_WAKE_TYPE_FIFO 0xAA

typedef struct dummy_mailbox os_mailbox_dummy_t;
typedef os_mailbox_dummy_t  *os_mailbox_id;

/*
 * Here, actually, it is an array of mailbox control structure size.
 * The purpose is to serve as a mailbox control block for
 * static create of mailbox.
 */
#define OS_MAILBOX_DEFINE(name) os_mailbox_dummy_t name

/*
 * OS_MB_POOL_DEFINE is used to define mailbox's memory pool and the address is aligned.
 * OS_MB_POOL_ADDR is used to get address of mailbox's memory pool.
 * OS_MB_POOL_SIZE is used to get size of mailbox's memory pool.
 *
 * These macros are used when creating a mailbox with the function os_mb_init().
 */
#define OS_MAILBOX_POOL_DEFINE(name, mail_count) os_ubase_t pool_##name[mail_count]
#define OS_MAILBOX_POOL_ADDR(name)               ((void *)pool_##name)
#define OS_MAILBOX_POOL_SIZE(name)               sizeof(pool_##name)

#ifdef OS_USING_HEAP
extern os_mailbox_id os_mailbox_create_dynamic(const char *name, os_size_t max_mails);
#endif
extern os_mailbox_id
os_mailbox_create_static(os_mailbox_dummy_t *mailbox_cb, const char *name, void *mail_pool, os_size_t mail_pool_size);
extern os_err_t os_mailbox_destroy(os_mailbox_id mailbox_id);

extern os_err_t os_mailbox_send(os_mailbox_id mailbox_id, os_ubase_t value, os_tick_t timeout);
extern os_err_t os_mailbox_recv(os_mailbox_id mailbox_id, os_ubase_t *value, os_tick_t timeout);

extern os_err_t os_mailbox_set_wake_type(os_mailbox_id mailbox_id, uint8_t wake_type);
extern os_err_t os_mailbox_reset(os_mailbox_id mailbox_id);
extern os_err_t os_mailbox_is_empty(os_mailbox_id mailbox_id, os_bool_t *empty_flag);
extern os_err_t os_mailbox_is_full(os_mailbox_id mailbox_id, os_bool_t *full_flag);
extern os_err_t os_mailbox_get_capacity(os_mailbox_id mailbox_id, uint16_t *capacity);
extern os_err_t os_mailbox_get_used_mails(os_mailbox_id mailbox_id, uint16_t *used_mails);
extern os_err_t os_mailbox_get_unused_mails(os_mailbox_id mailbox_id, uint16_t *unused_mails);

#endif /* OS_USING_MAILBOX */

#ifdef __cplusplus
}
#endif

#endif /* __OS_MB_H__ */
