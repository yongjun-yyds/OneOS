/**
 ***********************************************************************************************************************
 * Copyright (c) 2022, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        os_sem_trace.h
 *
 * @brief       Provide sem trace
 *
 * @revision
 * Date         Author          Notes
 * 2022-3-22   OneOS Team      First version
 ***********************************************************************************************************************
 */
#ifndef __OS_SEM_TRACE_H__
#define __OS_SEM_TRACE_H__

#include <oneos_config.h>
#include <os_types.h>
#include <os_ipc_hook.h>
#include <os_sem.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifdef OS_USING_SEM_TRACE

#ifdef OS_SEM_WAIT_HOOK
OS_IPC_HOOK_DECLARE(sem, wait, success, os_semaphore_id);
OS_IPC_HOOK_DECLARE(sem, wait, fail, os_semaphore_id);
OS_IPC_HOOK_DECLARE(sem, wait, block, os_semaphore_id);
OS_IPC_HOOK_DECLARE(sem, wait, timeout, os_semaphore_id);
#endif

#ifdef OS_SEM_POST_HOOK
OS_IPC_HOOK_DECLARE(sem, post, success, os_semaphore_id);
OS_IPC_HOOK_DECLARE(sem, post, full, os_semaphore_id);
OS_IPC_HOOK_DECLARE(sem, post, wake, os_semaphore_id);
#endif

#define OS_SEM_HOOK_CALL(sem, hp, sta, ...) OS_IPC_HOOK_CALL(sem, hp, sta, __VA_ARGS__)

#else

#define OS_SEM_HOOK_CALL(sem, hp, sta, ...)

#endif

#ifdef __cplusplus
}
#endif

#endif
