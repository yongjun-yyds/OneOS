/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        os_event.h
 *
 * @brief       Header file for event interface.
 *
 * @revision
 * Date         Author          Notes
 * 2020-12-28   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __OS_EVENT_H__
#define __OS_EVENT_H__

#include <oneos_config.h>
#include <os_types.h>
#include <os_list.h>
#include <os_spinlock.h>
#include <os_dummy.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifdef OS_USING_EVENT

#define OS_EVENT_WAKE_TYPE_PRIO 0x55
#define OS_EVENT_WAKE_TYPE_FIFO 0xAA

#define OS_EVENT_OPTION_AND   0x00000001U
#define OS_EVENT_OPTION_OR    0x00000002U
#define OS_EVENT_OPTION_CLEAR 0x00000004U
#define OS_EVENT_OPTION_MASK  0x00000007U

typedef struct dummy_event os_event_dummy_t;
typedef os_event_dummy_t  *os_event_id;

/*
 * Here, actually, it is an array of event control structure size.
 * The purpose is to serve as a event control block for
 * static create of event.
 */
#define OS_EVENT_DEFINE(name) os_event_dummy_t name

extern os_event_id os_event_create(os_event_dummy_t *event_cb, const char *name);
extern os_err_t    os_event_destroy(os_event_id event_id);

extern os_err_t os_event_send(os_event_id event_id, uint32_t set);
extern os_err_t
os_event_recv(os_event_id event_id, uint32_t interested_set, uint32_t option, os_tick_t timeout, uint32_t *recved_set);

extern os_err_t os_event_clear(os_event_id event_id, uint32_t interested_clear);
extern os_err_t os_event_get(os_event_id event_id, uint32_t *set);

extern os_err_t os_event_set_wake_type(os_event_id event_id, uint8_t wake_type);

extern os_err_t
os_event_sync(os_event_id event_id, uint32_t set, uint32_t interested_set, os_tick_t timeout, uint32_t *recved_set);

#endif /* OS_USING_EVENT */

#ifdef __cplusplus
}
#endif

#endif /* __OS_EVENT_H__ */
