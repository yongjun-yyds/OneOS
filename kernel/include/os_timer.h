/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        os_timer.h
 *
 * @brief       Header file for timer interface.
 *
 * @revision
 * Date         Author          Notes
 * 2020-11-18   OneOS Team      First version.
 ***********************************************************************************************************************
 */

#ifndef __OS_TIMER_LIST_H__
#define __OS_TIMER_LIST_H__

#include <oneos_config.h>
#include <os_types.h>
#include <os_list.h>
#include <os_dummy.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifdef OS_USING_KERNEL_TIMER

#define OS_TIMER_FLAG_ONE_SHOT 0x0U /* Oneshot timer. */
#define OS_TIMER_FLAG_PERIODIC 0x1U /* Periodic timer. */

typedef struct dummy_timer os_timer_dummy_t;
typedef os_timer_dummy_t  *os_timer_id;

#define OS_TIMER_DEFINE(name)                                                                                          \
    OS_ALIGN(OS_ARCH_STACK_ALIGN_SIZE)                                                                                 \
    os_timer_dummy_t name /* Here, actually, it is an array of os_timer_t structure size.                              \
                             The purpose is to serve as a timer control block for                                      \
                             static create of timer. */

#ifdef OS_USING_SINGLE_LIST_TIMER
#if defined(OS_HARD_SINGLE_LIST_TIMER) && defined(OS_SOFT_SINGLE_LIST_TIMER)
#define OS_TIMER_FLAG_HARD_TIMER 0x0U /* Hard timer,the timer's callback function will be called in tick isr. */
#define OS_TIMER_FLAG_SOFT_TIMER 0x4U /* Soft timer,the timer's callback function will be called in timer task. */
#endif                                /* end of OS_HARD_SINGLE_LIST_TIMER & OS_SOFT_SINGLE_LIST_TIMER */
/* used to cfg periodic timer compensate(timeout cb still too long) */
#define OS_TIMER_PERIODIC_COMPENSATE 0
#endif /* end of OS_USING_SINGLE_LIST_TIMER */

extern os_timer_id os_timer_create(os_timer_dummy_t *timer_cb,
                                   const char       *name,
                                   void (*function)(void *parameter),
                                   void     *parameter,
                                   os_tick_t timeout,
                                   uint8_t   flag);

extern os_err_t os_timer_destroy(os_timer_id timer_id);

extern os_err_t os_timer_start(os_timer_id timer_id);
extern os_err_t os_timer_stop(os_timer_id timer_id);

extern os_err_t  os_timer_set_timeout_ticks(os_timer_id timer_id, os_tick_t timeout);
extern os_tick_t os_timer_get_timeout_ticks(os_timer_id timer_id);
extern os_tick_t os_timer_get_remain_ticks(os_timer_id timer_id);
extern const char *os_timer_get_name(os_timer_id timer_id);
extern os_bool_t   os_timer_is_active(os_timer_id timer_id);

extern os_err_t  os_timer_set_oneshot(os_timer_id timer_id);
extern os_err_t  os_timer_set_periodic(os_timer_id timer_id);
extern os_bool_t os_timer_is_periodic(os_timer_id timer_id);
extern os_bool_t os_timer_check_exist(os_timer_id timer_id);

/* Hash timers do not have hardware timers, only software timers */
#ifdef OS_USING_SINGLE_LIST_TIMER
extern os_bool_t os_timer_is_soft(os_timer_id timer_id);
#endif /* end of OS_USING_SINGLE_LIST_TIMER */

#endif /* End of OS_USING_KERNEL_TIMER */

#ifdef __cplusplus
}
#endif

#endif
