/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        limits_ext.h
 *
 * @brief       Supplement to the standard C library file.
 *
 * @revision
 * Date         Author          Notes
 * 2020-06-15   OneOS team      First Version
 ***********************************************************************************************************************
 */
#ifndef __LIMITS_EXT_H__
#define __LIMITS_EXT_H__

#include <oneos_config.h>
#include <limits.h>

#define PTHREAD_STACK_MIN  512
#define PTHREAD_THREADS_MAX PTHREAD_NUM_MAX

#endif /* __LIMITS_EXT_H__ */
