/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        time.c
 *
 * @brief       This file provides some standard time interfaces.
 *
 * @revision
 * Date         Author          Notes
 * 2020-04-17   OneOS Team      First version.
 ***********************************************************************************************************************
 */
#include <sys/time.h>
#include <oneos_config.h>
#include <os_stddef.h>
#include <os_errno.h>
#include <os_assert.h>
#include <os_clock.h>
#include <device.h>
#if defined(OS_USING_TIMER_DRIVER)
#include <timekeeping.h>
#endif

/**
 ***********************************************************************************************************************
 * @brief           Get the current time,unsupport us.
 *
 * @param[in]       ignore    Ignored.
 * @param[out]      tp        The timestamp pointer, if not used, keep NULL.
 *
 * @return          Returns the current time.
 ***********************************************************************************************************************
 */
int gettimeofday(struct timeval *tp, void *ignore)
{
#if defined(OS_USING_TIMER_DRIVER) && defined(OS_USING_TIMEKEEPING)
    timekeeping_gettimeofday(tp, OS_NULL);
#else
    if (tp)
    {
        tp->tv_sec = 0;
        tp->tv_usec = 0;
    }
#endif
    return 0;
}

/**
 ***********************************************************************************************************************
 * @brief           Get the current time.
 *
 * @param[out]      t         The timestamp pointer, if not used, keep NULL.
 *
 * @return          Returns the current timestamp.
 ***********************************************************************************************************************
 */
time_t time(time_t *t)
{
    struct timeval tp = {0, 0};

#if defined(OS_USING_TIMER_DRIVER) && defined(OS_USING_TIMEKEEPING)
    timekeeping_gettimeofday(&tp, OS_NULL);
#endif

    /* if t is not NULL, write timestamp to *t */
    if (t != OS_NULL)
    {
        *t = tp.tv_sec;
    }

    return tp.tv_sec;
}

/**
 ***********************************************************************************************************************
 * @brief           Get current clock.
 *
 * @attention       This function is weak, and could be implemented in App.
 *
 * @param           No parameter.
 *
 * @return          Current tick count.
 ***********************************************************************************************************************
 */
OS_WEAK clock_t clock(void)
{
    return os_tick_get_value();
}
