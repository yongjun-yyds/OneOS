#include <inttypes.h>
#include <stdlib.h>

uintmax_t strtoumax(const char *restrict nptr, char **restrict endptr, int base)
{
    unsigned long long ret_ull = 0;
    uintmax_t ret_umax = 0;

    ret_ull = strtoull(nptr, endptr, base);
    ret_umax = ret_ull;
    return ret_umax;
}
