#include <inttypes.h>
#include <stdlib.h>

intmax_t strtoimax(const char *restrict nptr, char **restrict endptr, int base)
{
    long long ret_ll = 0;
    intmax_t ret_imax = 0;

    ret_ll = strtoll(nptr, endptr, base);
    ret_imax = ret_ll;
    return ret_imax;
}
