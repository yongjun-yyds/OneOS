#include <errno.h>
#include <string.h>
#include <sys/utsname.h>
#include <unistd.h>
#include <oneos_config.h>

#define UNAME_SYSNAME "OneOS"
#define UNAME_RELEASE "V2.3.0"
#define UNAME_VERSION (__DATE__ " " __TIME__)

#if defined(ARCH_ARM)
#define UNAME_MACHINE "arm"
#elif defined(ARCH_LOONGARCH)
#define UNAME_MACHINE "loongarch"
#elif defined(ARCH_MIPS)
#define UNAME_MACHINE "mips"
#elif defined(ARCH_RISCV)
#define UNAME_MACHINE "riscv"
#else
#define UNAME_MACHINE "unknown"
#endif

int uname(struct utsname *name)
{
    if (name == NULL)
    {
        errno = (EFAULT);
        return -1;
    }

    strncpy(name->sysname, UNAME_SYSNAME, sizeof(name->sysname));
    strncpy(name->release, UNAME_RELEASE, sizeof(name->release));
    strncpy(name->version, UNAME_VERSION, sizeof(name->version));
    strncpy(name->machine, UNAME_MACHINE, sizeof(name->machine));

    return 0;
}
