#include <stddef.h>
#include <errno_ext.h>
#include <unistd_ext.h>
#include <string.h>

#define CS_PATH "/bin:/usr/bin"

/* If BUF is not NULL and LEN > 0, fill in at most LEN - 1 bytes
   of BUF with the value corresponding to NAME and zero-terminate BUF.
   Return the number of bytes required to hold NAME's entire value.  */
size_t confstr(name, buf, len)
int    name;
char  *buf;
size_t len;
{
    const char *string;
    size_t      string_len;

    switch (name)
    {
    case _CS_PATH:
    {
        static const char cs_path[] = CS_PATH;
        string                      = cs_path;
        string_len                  = sizeof(cs_path);
    }
    break;

    case _CS_POSIX_V6_ILP32_OFFBIG_CFLAGS:
#if _XBS5_LP64_OFF64 == -1 && _XBS5_LPBIG_OFFBIG == -1 && _XBS5_ILP32_OFFBIG == 1
        /* Signal that we want the new ABI.  */
        {
            static const char file_offset[] = "-D_FILE_OFFSET_BITS=64";
            string                          = file_offset;
            string_len                      = sizeof(file_offset);
        }
        break;
#endif
        /* FALLTHROUGH */

    case _CS_XBS5_ILP32_OFF32_LINTFLAGS:
    case _CS_XBS5_ILP32_OFFBIG_LINTFLAGS:
    case _CS_XBS5_LP64_OFF64_LINTFLAGS:
    case _CS_XBS5_LPBIG_OFFBIG_LINTFLAGS:

    case _CS_POSIX_V6_ILP32_OFF32_CFLAGS:
    case _CS_POSIX_V6_ILP32_OFF32_LDFLAGS:
    case _CS_POSIX_V6_ILP32_OFF32_LIBS:
    case _CS_POSIX_V6_ILP32_OFFBIG_LDFLAGS:
    case _CS_POSIX_V6_ILP32_OFFBIG_LIBS:
    case _CS_POSIX_V6_LP64_OFF64_CFLAGS:
    case _CS_POSIX_V6_LP64_OFF64_LDFLAGS:
    case _CS_POSIX_V6_LP64_OFF64_LIBS:
    case _CS_POSIX_V6_LPBIG_OFFBIG_CFLAGS:
    case _CS_POSIX_V6_LPBIG_OFFBIG_LDFLAGS:
    case _CS_POSIX_V6_LPBIG_OFFBIG_LIBS:
        /* GNU libc does not require special actions to use LFS functions.  */
        string     = "";
        string_len = 1;
        break;

    default:
        errno = (EINVAL);
        return 0;
    }

    if (len > 0 && buf != NULL)
    {
        if (string_len <= len)
            memcpy(buf, string, string_len);
        else
        {
            memcpy(buf, string, len - 1);
            buf[len - 1] = '\0';
        }
    }
    return string_len;
}
