#include <oneos_config.h>
#include <stddef.h>
#include <errno_ext.h>
#include <stdio.h>
#include <os_mutex.h>
#include <os_task.h>

#ifdef _RETARGETABLE_LOCKING    // struct _lock for _flock_t

/* All these mutex operate should be work after oneos kernel start, at boot stage, It's not work. */

struct __lock __lock___sinit_recursive_mutex;
struct __lock __lock___sfp_recursive_mutex;
struct __lock __lock___atexit_recursive_mutex;
struct __lock __lock___at_quick_exit_mutex;
struct __lock __lock___malloc_recursive_mutex;
struct __lock __lock___env_recursive_mutex;
struct __lock __lock___tz_mutex;
struct __lock __lock___dd_hash_mutex;
struct __lock __lock___arc4random_mutex;

struct __lock
{
    os_mutex_dummy_t mutex;
};

int __retarget_lock_sys_init(void)
{
    os_mutex_create(&__lock___sinit_recursive_mutex.mutex, "sinit_flock_r", OS_TRUE);
    os_mutex_create(&__lock___sfp_recursive_mutex.mutex, "sfp_flock_r", OS_TRUE);
    os_mutex_create(&__lock___atexit_recursive_mutex.mutex, "atexit_flock_r", OS_TRUE);
    os_mutex_create(&__lock___at_quick_exit_mutex.mutex, "quit_exit_flock", OS_FALSE);
    os_mutex_create(&__lock___malloc_recursive_mutex.mutex, "malloc_flock_r", OS_TRUE);
    os_mutex_create(&__lock___env_recursive_mutex.mutex, "env_flock_r", OS_TRUE);
    os_mutex_create(&__lock___tz_mutex.mutex, "tz_flock", OS_FALSE);
    os_mutex_create(&__lock___dd_hash_mutex.mutex, "dd_flock", OS_FALSE);
    os_mutex_create(&__lock___arc4random_mutex.mutex, "arc4ramdom_flock", OS_FALSE);

    return 0;
}
OS_INIT_CALL(__retarget_lock_sys_init, OS_INIT_LEVEL_POST_KERNEL, OS_INIT_SUBLEVEL_LOW);

/* FILE->_flock_t */
/* _flock_t == _LOCK_T == struct __lock * */

void __retarget_lock_init(_LOCK_T *lock)
{
    if (!os_get_current_task())
    {
        return;
    }
    *lock = (_LOCK_T)os_mutex_create(OS_NULL, "flock", OS_FALSE);
}

void __retarget_lock_init_recursive(_LOCK_T *lock)
{
    if (!os_get_current_task())
    {
        return;
    }
    *lock = (_LOCK_T)os_mutex_create(OS_NULL, "flock_r", OS_TRUE);
}

void __retarget_lock_close(_LOCK_T lock)
{
    if (!os_get_current_task())
    {
        return;
    }
    os_mutex_destroy(&lock->mutex);
}

void __retarget_lock_close_recursive(_LOCK_T lock)
{
    if (!os_get_current_task())
    {
        return;
    }
    os_mutex_destroy(&lock->mutex);
}

void __retarget_lock_acquire(_LOCK_T lock)
{
    if (!os_get_current_task())
    {
        return;
    }
    os_mutex_lock(&lock->mutex, OS_WAIT_FOREVER);
}

void __retarget_lock_acquire_recursive(_LOCK_T lock)
{
    if (!os_get_current_task())
    {
        return;
    }
    os_mutex_recursive_lock(&lock->mutex, OS_WAIT_FOREVER);
}

int __retarget_lock_try_acquire(_LOCK_T lock)
{
    if (!os_get_current_task())
    {
        return 1; // keep the same as libc default return code
    }

    if (OS_SUCCESS != os_mutex_lock(&lock->mutex, OS_NO_WAIT))
    {
        return -1;
    }

    return 0;
}

int __retarget_lock_try_acquire_recursive(_LOCK_T lock)
{
    if (!os_get_current_task())
    {
        return 1; // keep the same as libc default return code
    }

    if (OS_SUCCESS != os_mutex_recursive_lock(&lock->mutex, OS_NO_WAIT))
    {
        return -1;
    }

    return 0;
}

void __retarget_lock_release(_LOCK_T lock)
{
    if (!os_get_current_task())
    {
        return;
    }
    os_mutex_unlock(&lock->mutex);
}

void __retarget_lock_release_recursive(_LOCK_T lock)
{
    if (!os_get_current_task())
    {
        return;
    }
    os_mutex_recursive_unlock(&lock->mutex);
}

void flockfile(FILE *fp)
{
    if (!fp)
    {
        return;
    }
    _flockfile(fp);
}

int ftrylockfile(FILE *fp)
{
    if (!fp)
    {
        return EBADF;
    }

#if !defined(_ftrylockfile)
#ifndef __SINGLE_THREAD__
#define _ftrylockfile(fp) (((fp)->_flags & __SSTR) ? 0 : __lock_try_acquire_recursive((fp)->_lock))
#else
#define _ftrylockfile(fp) (_CAST_VOID 0)
#endif
#endif

    if (-1 == _ftrylockfile(fp))
    {
        return EBUSY;
    }

    return 0;
}

void funlockfile(FILE *fp)
{
    if (!fp)
    {
        return;
    }
    _funlockfile(fp);
}

#endif
